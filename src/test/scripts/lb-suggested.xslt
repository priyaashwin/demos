<xsl:stylesheet version="2.0" 
	xmlns="http://www.liquibase.org/xml/ns/dbchangelog" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:lqb="http://www.liquibase.org/xml/ns/dbchangelog">
  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes"/>
  <xsl:param name="PREVIOUS_VERSION"/>
  <xsl:param name="NEXT_VERSION"/>

  <!-- Add logicalFilePath to databaseChangeLog  -->
  <xsl:template name="databaseChangeLog" match="/">
    <databaseChangeLog xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.4.xsd">
    <xsl:attribute name="logicalFilePath">__CHANGEME__</xsl:attribute>

    <xsl:comment>
    	WARNING - This Liquibase script shouldn't be blindly copied into Eclipse. Things to consider:

    		* Should this be split up into multiple changeset files? Keep seperate ligical areas in different files
    		* Chould the change be added to an existing file or placed into a new one? If in doubt, use a new file
    		* Does data need to be preserved? If it does, don't take a success run of the lb_run script as an ok. Make sure you test with real data

    </xsl:comment>

	<xsl:for-each select="lqb:databaseChangeLog/lqb:changeSet">
		<xsl:choose>
			<!-- Adds -->
			<xsl:when test="lqb:createSequence">
			  <xsl:call-template name="createSequence"/>
			</xsl:when>
			<xsl:when test="lqb:createTable">
			  <xsl:call-template name="createTable"/>
			</xsl:when>
			<xsl:when test="lqb:addColumn">
			  <xsl:call-template name="addColumn"/>
			</xsl:when>
			
			<!-- drops -->
			<xsl:when test="lqb:dropColumn">
				<xsl:call-template name="dropColumn"/>
			</xsl:when>
			<xsl:when test="lqb:dropTable">
				<xsl:call-template name="dropTable"/>
			</xsl:when>

			<!-- Mods -->
			<xsl:when test="lqb:addNotNullConstraint">
				<xsl:call-template name="addNotNullConstraint"/>
			</xsl:when>

		    <xsl:when test="lqb:dropSequence|lqb:dropIndex|lqb:dropForeignKeyConstraint|lqb:dropNotNullConstraint">
		    	<xsl:call-template name="otherwise"/>
			</xsl:when>

			<xsl:otherwise>
			  <xsl:call-template name="otherwise"/>
			</xsl:otherwise>
		</xsl:choose> 
    </xsl:for-each>	
	</databaseChangeLog>
  </xsl:template> 
 
<!-- Add startValue=1 to createSequences from liquibase --> 
  <xsl:template name="createSequence">
    <xsl:element name="changeSet" inherit-namespaces="no">
       <xsl:attribute name="author">
			<xsl:value-of select="@author"/>
		</xsl:attribute>
       <xsl:attribute name="id">
			<xsl:value-of select="@id"/>
		</xsl:attribute>
        <xsl:element name="createSequence" inherit-namespaces="no">
			<xsl:attribute name="sequenceName">
				<xsl:value-of select="lqb:createSequence/@sequenceName"/>
			</xsl:attribute>
			<xsl:attribute name="startValue">1</xsl:attribute>
		</xsl:element>			
    </xsl:element>
  </xsl:template>
  
<!-- translate postgres datatypes in created tables to vendor independent types -->
  <xsl:template name="createTable">
    <changeSet>
       <xsl:attribute name="author">
			<xsl:value-of select="@author"/>
		</xsl:attribute>
       <xsl:attribute name="id">
			<xsl:value-of select="@id"/>
		</xsl:attribute>
        <createTable>
			<xsl:attribute name="tableName">
				<xsl:value-of select="lqb:createTable/@tableName"/>
			</xsl:attribute>
			<xsl:for-each select="lqb:createTable/lqb:column">
				<xsl:call-template name="column"/>
			</xsl:for-each>
		</createTable>			
    </changeSet>
  </xsl:template>

  <!-- translate postgres datatypes in add columns to vendor independent types -->
  <xsl:template name="addColumn">

  	<xsl:param name="table"/>

  	<xsl:comment>
A column '<xsl:value-of select="@name"/>' in table '<xsl:value-of select="$table"/>' has been added to an existing table. Do values need assigning to this column in existing rows?
	</xsl:comment>
    <changeSet>
       <xsl:attribute name="author">
			<xsl:value-of select="@author"/>
		</xsl:attribute>
       <xsl:attribute name="id">
			<xsl:value-of select="@id"/>
		</xsl:attribute>
        <addColumn>
			<xsl:attribute name="tableName">
				<xsl:value-of select="lqb:addColumn/@tableName"/>
			</xsl:attribute>
			<xsl:for-each select="lqb:addColumn/lqb:column">
				<xsl:call-template name="column"/>
			</xsl:for-each>
		</addColumn>			
    </changeSet>
  </xsl:template>

  <xsl:template name="dropColumn">
	<xsl:comment>
A column '<xsl:value-of select="lqb:dropColumn/@columnName"/>' in table '<xsl:value-of select="lqb:dropColumn/@tableName"/>' is to be dropped. Does data in it need to be preserved before being dropped?
	</xsl:comment>
	
	<xsl:copy-of copy-namespaces="no" select="."/>

  </xsl:template>
  
  <xsl:template name="dropTable">
    <!-- xsl:message>at dropColumn [<xsl:value-of select="name(.)"/>: <xsl:value-of select="."/>].</xsl:message -->
	<xsl:comment>
A table '<xsl:value-of select="lqb:dropTable/@tableName"/>' is to be dropped. Does data in it need to be preserved before being dropped?
	</xsl:comment>
	
	<xsl:copy-of copy-namespaces="no" select="."/>

  </xsl:template>

  <xsl:template name="addNotNullConstraint">
	<xsl:comment>
Not null constraint added to column '<xsl:value-of select="lqb:addNotNullConstraint/@columnName"/>' in table '<xsl:value-of select="lqb:addNotNullConstraint/@tableName"/>'. Should existing rows have not null values for this column?
	</xsl:comment>
	<xsl:element name="include">
		<xsl:attribute name="file">upgrade_value_<xsl:value-of select="lqb:addNotNullConstraint/@tableName"/>_<xsl:value-of select="lqb:addNotNullConstraint/@columnName"/>.xml</xsl:attribute>
		<xsl:attribute name="relativeToChangelogFile">true</xsl:attribute>
	</xsl:element>
  </xsl:template>


<!-- clone in to out for other changeset types -->  
  <xsl:template name="otherwise">
    <xsl:copy-of copy-namespaces="no" select="."/>
  </xsl:template>

<!-- convert table columns -->
  <xsl:template name="column">
	<column>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"/>
		</xsl:attribute>
		<xsl:attribute name="type"><xsl:call-template name="dataType"/></xsl:attribute>
		<xsl:copy-of copy-namespaces="no" select="lqb:constraints"/>
	</column>
  </xsl:template>  

  <!-- convert postgres datatype to vendor independant types -->  
  <xsl:template name="dataType">
		<xsl:choose>
			<xsl:when test="upper-case(@type)='DATE'">${type.date}</xsl:when>
			<xsl:when test="upper-case(@type)='DATE(13)'">${type.date}</xsl:when>
			<xsl:when test="upper-case(@type)='TIMESTAMP WITH TIME ZONE'">${type.timestamptz}</xsl:when>
			<xsl:when test="upper-case(@type)='TIMESTAMP'">${type.timestamptz}</xsl:when>
			<xsl:when test="upper-case(@type)='TIMESTAMPTZ'">${type.timestamptz}</xsl:when>
			<xsl:when test="upper-case(@type)='BOOL'">${type.boolean}</xsl:when>
			<xsl:when test="upper-case(@type)='INT'">${type.int}</xsl:when>
			<xsl:when test="upper-case(@type)='BIGINT'">${type.bigint}</xsl:when>
			<xsl:when test="upper-case(@type)='INT8'">${type.bigint}</xsl:when>
			<xsl:when test="upper-case(@type)='NUMERIC(19, 2)'">${type.bigint2}</xsl:when>
			<xsl:when test="upper-case(@type)='FLOAT8'">${type.float}</xsl:when>
			<xsl:when test="upper-case(@type)='VARCHAR(255)'">${type.varchar}</xsl:when>
			<xsl:when test="upper-case(@type)='TEXT'">${type.clob}</xsl:when>
			<xsl:when test="upper-case(@type)='BYTEA'">${type.blob}</xsl:when>
			<xsl:otherwise>unknown postgres datatype=<xsl:value-of select="@type"/>'.</xsl:otherwise>
		</xsl:choose>  
  </xsl:template>
</xsl:stylesheet>