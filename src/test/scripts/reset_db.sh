#!/bin/bash

echo "This script will drop and re-create the relationshipsrecording_APP and relationshipsrecording_APP_TEST databases."
read -p "Do you want to proceed (y/n)? [n]" -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]; then

  export PGPASSWORD="postgres"
  export PGUSER="postgres"
  psql --echo-all -d postgres -f reset_target.sql -h 127.0.0.1 -U $PGUSER -v schema_name=relationshipsrecording_APP
  psql --echo-all -d postgres -f reset_target.sql -h 127.0.0.1 -U $PGUSER -v schema_name=relationshipsrecording_APP_TEST

  echo $'\nDatabases re-created.'
else
  echo $'\nExiting without action.'
fi
