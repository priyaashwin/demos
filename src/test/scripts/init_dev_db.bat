@ECHO OFF

CHOICE /M "If you proceed, the  relationshipsrecording_APP and relationshipsrecording_APP_TEST databases will be dropped and re-created"
ECHO %ERRORLEVEL%

IF ERRORLEVEL 2 GOTO END
SET PGPASSWORD=postgres
psql --echo-all -d postgres -f reset_target.sql -h 127.0.0.1 -U postgres -v schema_name=relationshipsrecording_APP
psql --echo-all -d postgres -f reset_target.sql -h 127.0.0.1 -U postgres -v schema_name=relationshipsrecording_APP_TEST

call mvn --file ..\..\..\pom.xml surefire:test -DargLine="-Xmx2048m -XX:MaxPermSize=512m" -Dgroups="com.olmgroup.usp.facets.test.junit.category.InitDB"

echo Databases re-created

:END
