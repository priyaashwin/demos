\set quoted_schema_name '\'' :schema_name '\''
\set database_name '\"' :schema_name '\"'

SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = :quoted_schema_name
  AND pid <> pg_backend_pid();

SELECT datname FROM pg_database WHERE datname = :quoted_schema_name;

DROP DATABASE IF EXISTS :database_name;

SELECT datname FROM pg_database WHERE datname = :quoted_schema_name;

CREATE DATABASE :database_name
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;

SELECT datname FROM pg_database WHERE datname = :quoted_schema_name;

COMMENT ON DATABASE :database_name
  IS 'Local Eclipse DB';