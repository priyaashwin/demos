#!/bin/bash

echo "This script will drop and re-create the relationshipsrecording_APP and relationshipsrecording_APP_TEST databases."
read -p "Do you want to proceed (y/n)? [n]" -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]; then
  if [ ! -f ../../../target/test-classes/com/olmgroup/usp/apps/relationshipsrecording/InitDBTest.class ]; then
    echo $'\nInitDBTest class not found.'
    exit 1
  fi

  export PGPASSWORD="postgres"
  export PGUSER="postgres"
  psql --echo-all -d postgres -f reset_target.sql -h 127.0.0.1 -U $PGUSER -v schema_name=relationshipsrecording_APP
  psql --echo-all -d postgres -f reset_target.sql -h 127.0.0.1 -U $PGUSER -v schema_name=relationshipsrecording_APP_TEST

  mvn --file ../../../pom.xml surefire:test -DargLine="-Xmx2048m -XX:MaxPermSize=512m" -Dgroups="com.olmgroup.usp.facets.test.junit.category.InitDB" 

  echo $'\nDatabases re-created.'
else
  echo $'\nExiting without action.'
fi
