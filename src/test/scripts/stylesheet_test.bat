REM This script runs our Liquibase stylesheet against a test Liquibase file. It's here to support testing.

IF NOT Exist "saxon.jar" (
	call mvn org.apache.maven.plugins:maven-dependency-plugin:get -Dartifact=net.sf.saxon:Saxon-HE:9.8.0-4:jar -Ddest=saxon.jar
)

java -jar saxon.jar -s:lb-diff-schema-test.xml -xsl:lb-suggested.xslt >lb-diff-schema-test.out.xml