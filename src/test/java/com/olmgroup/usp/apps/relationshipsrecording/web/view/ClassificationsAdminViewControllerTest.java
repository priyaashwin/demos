// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ClassificationsAdminViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ClassificationsAdminViewController
 */

public class ClassificationsAdminViewControllerTest
    extends ClassificationsAdminViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ClassificationsAdminViewControllerTest#testViewClassifications()
   */
  protected void handleTestViewClassifications() throws Exception {
    this.getMockMvc().perform(get("/admin/classifications").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
