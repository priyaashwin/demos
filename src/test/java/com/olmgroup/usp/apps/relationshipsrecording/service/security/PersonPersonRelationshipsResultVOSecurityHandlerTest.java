// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    security/VOSecurityHandlerTestImpl.vsl in usp-spring-cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::vo::PersonPersonRelationshipsResultVO
 * STEREOTYPE:  ValueObject
 */
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonPersonRelationshipsResultVO;
import com.olmgroup.usp.components.relationship.service.PersonPersonRelationshipService;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import javax.inject.Inject;

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonPersonRelationshipsResultVOSecurityHandlerTest extends
    PersonPersonRelationshipsResultVOSecurityHandlerTestBase {

  @Inject
  private PersonPersonRelationshipService personPersonRelationshipService;

  @Before
  public final void handleInitializeTestSuite() {
    this.login("admin", "admin123");
  }

  @Override
  protected final void handleTestSupportsRelationalSecurityByTargetDomainObject()
      throws Exception {
    final PersonPersonRelationshipsResultVO personPersonRelationshipsResultVO = new PersonPersonRelationshipsResultVO();

    final boolean supportsRelationalSecurityByTargetDomainObject = this
        .getPersonPersonRelationshipsResultVOSecurityHandler()
        .supportsRelationalSecurity(personPersonRelationshipsResultVO);

    Assert.assertTrue(supportsRelationalSecurityByTargetDomainObject);
  }

  @Override
  protected final void handleTestGetSubjectsByTargetDomainObject()
      throws Exception {
    final PersonPersonRelationshipsResultVO personPersonRelationshipsResultVO = this.personPersonRelationshipService
        .findById(-10L, PersonPersonRelationshipsResultVO.class);

    final SubjectIdTypeVO expectedSubjectA = new SubjectIdTypeVO(-1L,
        SubjectType.PERSON);
    final SubjectIdTypeVO expectedSubjectB = new SubjectIdTypeVO(-2L,
        SubjectType.PERSON);

    final Set<SubjectIdTypeVO> subjects = this
        .getPersonPersonRelationshipsResultVOSecurityHandler()
        .getSubjects(personPersonRelationshipsResultVO);

    Assert.assertEquals(2, subjects.size());
    Assert.assertTrue(subjects.contains(expectedSubjectA));
    Assert.assertTrue(subjects.contains(expectedSubjectB));
  }

}