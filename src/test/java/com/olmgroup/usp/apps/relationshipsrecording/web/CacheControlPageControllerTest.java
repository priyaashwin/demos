package com.olmgroup.usp.apps.relationshipsrecording.web;

import static com.olmgroup.usp.facets.test.hamcrest.RegexMatcher.matches;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.ws.rs.core.HttpHeaders;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development" })
@WithUserDetails
public class CacheControlPageControllerTest extends AbstractUnifiedTestBase {

  @SuppressWarnings("unused")
  private static final Logger LOGGER = LoggerFactory
      .getLogger(CacheControlPageControllerTest.class);

  private static final String NO_CACHE = "no-cache, no-store, max-age=0, must-revalidate";
  private static final String CATEGORY_CC_HEADER_VAL = "max-age=3600, must-revalidate";
  private static final String COMBO_CC_HEADER_VAL = "max-age=31536000";
  private static final String PRAGMA_HEADER = "Pragma";
  private static final String PRAGMA_VAL = "no-cache";

  @Test
  public void testHomeAllocationCacheHeaders() throws Exception {
    getMockMvc()
        .perform(get("/home/allocation"))
        .andExpect(status().isOk())
        .andExpect(
            header().string(HttpHeaders.CACHE_CONTROL,
                matches(NO_CACHE)))
        .andExpect(header().string(HttpHeaders.EXPIRES, "0"))
        .andExpect(header().string(PRAGMA_HEADER, matches(PRAGMA_VAL)));
  }

  @Test
  @DatabaseSetup(value = { "/dbunit/CacheControlPageControllerTest/CacheControlPageController.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testYUIControllerCacheHeaders() throws Exception {
    getMockMvc()
        .perform(
            get("/rest/yui-modules/category/com.olmgroup.usp.components.person.category.title/"))
        .andExpect(status().isOk())
        .andExpect(
            header().string(HttpHeaders.CACHE_CONTROL,
                matches(CATEGORY_CC_HEADER_VAL)));
  }

  @Test
  public void testComboLoaderCacheHeaders() throws Exception {
    getMockMvc()
        .perform(
            get("/combo2?ver=1.1.1-SNAPSHOT-20151110-1157&modules=facets/yui/3.16.0/build/array-extras/array-extras-min.js"))
        .andExpect(status().isOk())
        .andExpect(
            header().string(HttpHeaders.CACHE_CONTROL,
                matches(COMBO_CC_HEADER_VAL)));
  }
}
