// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::SystemConfigurationViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SystemConfigurationViewController
 */
@WithUserDetails("super")
public class SystemConfigurationViewControllerTest
    extends SystemConfigurationViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SystemConfigurationViewControllerTest#testViewConfigurationDetials()
   */
  protected void handleTestViewConfigurationDetials() throws Exception {
    getMockMvc().perform(
        get("/admin/system/config").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
