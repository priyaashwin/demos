// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ReviewViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ReviewViewController
 */

@WithUserDetails("super")
public class ReviewViewControllerTest extends ReviewViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ReviewViewControllerTest#testPersonReview()
   */
  protected void handleTestPersonReview() throws Exception {
    /*
     * getMockMvc().perform(get("/review/person?id=703")
     * .accept(MediaType.TEXT_HTML))
     * .andExpect(status().isOk());
     */
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ReviewViewControllerTest#testGroupReview()
   */
  protected void handleTestGroupReview() throws Exception {
    /*
     * getMockMvc().perform(get("/review/group?id=-54")
     * .accept(MediaType.TEXT_HTML))
     * .andExpect(status().isOk());
     */
  }

  // Add additional test cases here
}
