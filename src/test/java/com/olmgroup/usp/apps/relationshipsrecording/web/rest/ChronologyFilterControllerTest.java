// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::rest::ChronologyFilterController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.ChronologyFilterController
 */

public class ChronologyFilterControllerTest extends ChronologyFilterControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.ChronologyFilterControllerTest#testGetFilterItems()
   */
  protected void handleTestGetFilterItems() throws Exception {
    MvcResult result = getMockMvc().perform(
        get(getRootURL() + "/chronology/-1/filterItems")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andReturn();

    String response = result.getResponse().getContentAsString();
    JSONObject responseContentJSON = new JSONObject(response);
    Assert.assertNotNull(responseContentJSON);

    JSONArray sources = responseContentJSON.getJSONArray("sources");
    Assert.assertNotNull(sources);
    Assert.assertEquals(3, sources.length());
    Assert.assertEquals("LEGAL", sources.get(0));
    Assert.assertEquals("MENTALHEALTH", sources.get(1));
    Assert.assertEquals("MIDWIFE", sources.get(2));

    JSONArray sourceOrgs = responseContentJSON.getJSONArray("sourceOrgs");
    Assert.assertNotNull(sourceOrgs);
    Assert.assertEquals(3, sourceOrgs.length());
    Assert.assertEquals("FRENCHEMBASSY", sourceOrgs.get(0));
    Assert.assertEquals("POLICE", sourceOrgs.get(1));
    Assert.assertEquals("PRISON", sourceOrgs.get(2));

    JSONArray entryTypes = responseContentJSON.getJSONArray("entryTypes");
    Assert.assertNotNull(entryTypes);
    Assert.assertEquals(3, entryTypes.length());
    Assert.assertEquals("Agency Contact", entryTypes.get(0));
    Assert.assertEquals("Carer Contact", entryTypes.get(1));
    Assert.assertEquals("Day Care", entryTypes.get(2));

    JSONArray impacts = responseContentJSON.getJSONArray("impacts");
    Assert.assertNotNull(impacts);
    Assert.assertEquals(2, impacts.length());
    Assert.assertEquals("NEGATIVE", impacts.get(0));
    Assert.assertEquals("POSITIVE", impacts.get(1));

    JSONArray practitionerOrgs = responseContentJSON.getJSONArray("practitionerOrgs");
    Assert.assertNotNull(practitionerOrgs);
    Assert.assertEquals(2, practitionerOrgs.length());
    Assert.assertEquals("Hong Kong Sanctum", practitionerOrgs.get(0));
    Assert.assertEquals("Marvel Universe", practitionerOrgs.get(1));

    JSONArray practitioners = responseContentJSON.getJSONArray("practitioners");
    Assert.assertNotNull(practitioners);
    Assert.assertEquals(2, practitioners.length());
    Assert.assertEquals("Benedict Cumberbatch", ((JSONObject) practitioners.get(0)).getString("name"));
    Assert.assertEquals("Tilda Swinton", ((JSONObject) practitioners.get(1)).getString("name"));

    JSONArray statuses = responseContentJSON.getJSONArray("statuses");
    Assert.assertNotNull(statuses);
    Assert.assertEquals(2, statuses.length());
    Assert.assertEquals("COMPLETE", statuses.get(0));
    Assert.assertEquals("DRAFT", statuses.get(1));
  }

  // Add additional test cases here
}
