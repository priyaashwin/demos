package com.olmgroup.usp.apps.relationshipsrecording.config;

import com.olmgroup.usp.facets.spring.StandardUSPProfiles;
import com.olmgroup.usp.facets.spring.SuppressingProfile;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * Spring configuration class used to define spring test context for the component.
 * <p>
 * TEMPLATE: ConfigurationTestBean.vsl in usp-spring-cartridge.
 * </p>
 *
 * @author Template author: Rob D.
 */
@Configuration
@ImportResource(locations = { "classpath:META-INF/authenticationRepository.xml" })
@Profile({ StandardUSPProfiles.DEVELOPMENT })
class RelationshipsrecordingApplicationTestConfig {

  // Will probably need another for InitDB test 
  @Configuration
  @Profile({ StandardUSPProfiles.DEVELOPMENT })
  @SuppressingProfile("initdb")
  @PropertySource("classpath:relationshipsrecording.UNIT.properties")
  class AppTestProperties {

  }

  @Configuration
  @Profile({ "initdb" })
  @PropertySource("classpath:relationshipsrecording.properties")
  class InitDBProperties {

  }

}