// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    security/VOSecurityHandlerTestImpl.vsl in usp-spring-cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::vo::UpdateDeletePersonReferenceNumbersVO
 * STEREOTYPE:  ValueObject
 */
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonReferenceNumbersVO;
import com.olmgroup.usp.components.person.vo.UpdatePersonReferenceNumberVO;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
public class UpdateDeletePersonReferenceNumbersVOSecurityHandlerTest extends
    UpdateDeletePersonReferenceNumbersVOSecurityHandlerTestBase {
  @Override
  protected final void handleTestSupportsRelationalSecurityByTargetDomainObject()
      throws Exception {
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();

    final boolean supportsRelationalSecurityByTargetDomainObject = this
        .getUpdateDeletePersonReferenceNumbersVOSecurityHandler()
        .supportsRelationalSecurity(updateDeletePersonReferenceNumbersVO);

    Assert.assertTrue(supportsRelationalSecurityByTargetDomainObject);
  }

  @Override
  protected final void handleTestGetSubjectsByTargetDomainObject()
      throws Exception {
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();

    final UpdatePersonReferenceNumberVO updatePersonReferenceNumberVO_A = new UpdatePersonReferenceNumberVO(-20L);
    final UpdatePersonReferenceNumberVO updatePersonReferenceNumberVO_B = new UpdatePersonReferenceNumberVO(-21L);

    final Long deletePersonReferenceNumberId = -30L;

    updateDeletePersonReferenceNumbersVO.setUpdateReferenceNumberVOs(Arrays
        .asList(updatePersonReferenceNumberVO_A,
            updatePersonReferenceNumberVO_B));
    updateDeletePersonReferenceNumbersVO
        .setDeletePersonReferenceNumberIds(Arrays
            .asList(deletePersonReferenceNumberId));

    final SubjectIdTypeVO expectedSubjectA = new SubjectIdTypeVO(-1L,
        SubjectType.PERSON);
    final SubjectIdTypeVO expectedSubjectB = new SubjectIdTypeVO(-2L,
        SubjectType.PERSON);
    final SubjectIdTypeVO expectedSubjectC = new SubjectIdTypeVO(-3L,
        SubjectType.PERSON);

    final Set<SubjectIdTypeVO> subjects = this
        .getUpdateDeletePersonReferenceNumbersVOSecurityHandler()
        .getSubjects(updateDeletePersonReferenceNumbersVO);

    Assert.assertEquals(3, subjects.size());
    Assert.assertTrue(subjects.contains(expectedSubjectA));
    Assert.assertTrue(subjects.contains(expectedSubjectB));
    Assert.assertTrue(subjects.contains(expectedSubjectC));
  }

  @Test
  @DatabaseSetup(
      value = "/dbunit/security/UpdateDeletePersonReferenceNumbersVOSecurityHandler.setup.xml",
      type = DatabaseOperation.CLEAN_INSERT)
  public final void handleTestGetSubjectsByTargetDomainObject_InvalidReferenceNumber()
      throws Exception {
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO.setDeletePersonReferenceNumberIds(Arrays
        .asList(-9990L, -9991L));

    final Set<SubjectIdTypeVO> subjects = this
        .getUpdateDeletePersonReferenceNumbersVOSecurityHandler()
        .getSubjects(updateDeletePersonReferenceNumbersVO);

    Assert.assertTrue(subjects.isEmpty());
  }

  @Test
  @DatabaseSetup(
      value = "/dbunit/security/UpdateDeletePersonReferenceNumbersVOSecurityHandler.setup.xml",
      type = DatabaseOperation.CLEAN_INSERT)
  public final void handleTestGetSubjectsByTargetDomainObject_ReferenceNumberListsAreNull()
      throws Exception {
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO.setUpdateReferenceNumberVOs(null);
    updateDeletePersonReferenceNumbersVO.setDeletePersonReferenceNumberIds(null);

    final Set<SubjectIdTypeVO> subjects = this
        .getUpdateDeletePersonReferenceNumbersVOSecurityHandler()
        .getSubjects(updateDeletePersonReferenceNumbersVO);

    Assert.assertTrue(subjects.isEmpty());
  }

}