package com.olmgroup.usp.apps.relationshipsrecording;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;
import com.olmgroup.usp.facets.test.junit.OrderedSpringJUnit4ClassRunner;
import com.olmgroup.usp.facets.test.junit.TestOrder;
import com.olmgroup.usp.facets.test.junit.category.InitDB;
import com.olmgroup.usp.liquibase.build.InitializeTestDBConfiguration;
import com.olmgroup.usp.liquibase.data.common.DataPurpose;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;

@RunWith(OrderedSpringJUnit4ClassRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development" })
@Commit
@Category(InitDB.class)
public class DBUnitDataFileTest extends AbstractUnifiedTestBase {

  private static final Logger logger = LoggerFactory
      .getLogger(DBUnitDataFileTest.class);

  private static final String CODE_TYPES = "CATEGORY,CATEGORY_CONTEXT,CODED_ENTRY,CONTEXT_BINDING";
  private static final String PER_PER_REL_TYPES = "FAMILIAL_RELATIONSHIP_TYPE,SOCIAL_RELATIONSHIP_TYPE,PROFESSIONAL_RELATIONSHIP_TYPE,RELATIONSHIP_ROLE_GENDER_TYPE";
  private static final String PER_ORG_REL_TYPES = "PROFESSIONAL_PO_RELATIONSHIP_TYPE,SOCIAL_PO_RELATIONSHIP_TYPE,POR_ORG_TYPE_BINDING";
  private static final String PER_TEAM_REL_TYPES = "PROFESSIONAL_TEAM_RELATIONSHIP_TYPE,CLIENT_TEAM_RELATIONSHIP_TYPE,PTR_PER_TYPE_BINDING";
  private static final String CLASSIFICATION_TYPES = "CLASSIFICATION_GROUP,CLASSIFICATION,CLASSIFICATION_CODED_ENTRY_ASSIGNMENT";
  private static final String MO_TYPES = "CODED_ENTRY,CONTEXT_BINDING,PROFESSIONAL_RELATIONSHIP_TYPE,CLASSIFICATION_GROUP,CLASSIFICATION";

  @Test
  @TestOrder(order = 1)
  public void testLoadProductConfiguration() throws Exception {
    String requiredTypes = CODE_TYPES + "," + PER_PER_REL_TYPES + "," + PER_ORG_REL_TYPES + "," + PER_TEAM_REL_TYPES
        + "," + CLASSIFICATION_TYPES;
    String excelFileName = "src/main/resources/product-configuration/Eclipse_Product_Configuration.xlsx";
    Connection conn = usp().getDataSource().getConnection();
    boolean autoCommit = conn.getAutoCommit();
    InitializeTestDBConfiguration.loadConfiguration(DataPurpose.PRODUCT, requiredTypes, excelFileName, conn);
    conn.setAutoCommit(autoCommit);
  }

  @Test
  @TestOrder(order = 2)
  public void testLoadModelOfficeConfiguration() throws Exception {
    String requiredTypes = MO_TYPES;
    String excelFileName = "src/test/resources/product-configuration/Test_Eclipse_ModelOffice_Configuration.xlsx";
    Connection conn = usp().getDataSource().getConnection();
    boolean autoCommit = conn.getAutoCommit();
    InitializeTestDBConfiguration.loadConfiguration(DataPurpose.MODEL_OFFICE, requiredTypes, excelFileName, conn);
    conn.setAutoCommit(autoCommit);
  }

  @Test
  @TestOrder(order = 10)
  @DatabaseSetup(value = "/ENVS/DEV/DATA/DEV.init-db.xml", type = DatabaseOperation.CLEAN_INSERT)
  @Rollback
  public void testDEVInitDBDataFile() {

    logger.debug("No test code - testing load of ENVS/DEV/DATA/DEV.init-db.xml");
  }

  @Test
  @TestOrder(order = 998)
  public void testDeleteModelOfficeConfiguration() throws Exception {
    String requiredTypes = MO_TYPES;
    String excelFileName = "src/test/resources/product-configuration/Test_Eclipse_ModelOffice_Configuration.xlsx";
    Connection conn = usp().getDataSource().getConnection();
    boolean autoCommit = conn.getAutoCommit();
    // Deleting the model office data as "PRODUCT" to prevent issues with model office deleting data.
    InitializeTestDBConfiguration.deleteConfiguration(DataPurpose.PRODUCT, requiredTypes, excelFileName, conn);
    conn.setAutoCommit(autoCommit);
  }

  @Test
  @TestOrder(order = 999)
  public void testDeleteProductConfiguration() throws Exception {
    String requiredTypes = CODE_TYPES + "," + PER_PER_REL_TYPES + "," + PER_ORG_REL_TYPES + "," + PER_TEAM_REL_TYPES
        + ","
        + CLASSIFICATION_TYPES;
    String excelFileName = "src/main/resources/product-configuration/Eclipse_Product_Configuration.xlsx";
    Connection conn = usp().getDataSource().getConnection();
    boolean autoCommit = conn.getAutoCommit();
    InitializeTestDBConfiguration.deleteConfiguration(DataPurpose.PRODUCT, requiredTypes, excelFileName, conn);
    conn.setAutoCommit(autoCommit);
  }

  /*
   * @Test
   * @DatabaseSetup( value="/ENVS/PERF/DATA/PERF.init-db.xml", type=DatabaseOperation.CLEAN_INSERT ) public void
   * testPERFInitDBDataFile() { logger.debug("No test code - testing load of ENVS/PERF/DATA/PERF.init-db.xml" ); }
   */

  // @Test
  // @DatabaseSetup(value = "/ENVS/SALESVIEW/DATA/SALESVIEW.init-db.xml", type = DatabaseOperation.CLEAN_INSERT)
  // public void testSALESVIEWInitDBDataFile() {
  //
  // logger.debug("No test code - testing load of ENVS/SALESVIEW/DATA/SALESVIEW.init-db.xml");
  // }
  //
  // @Test
  // @DatabaseSetup(value = "/ENVS/PRODSUPP/DATA/PRODSUPP.init-db.xml", type = DatabaseOperation.CLEAN_INSERT)
  // public void testPRODSUPPInitDBDataFile() {
  //
  // logger.debug("No test code - testing load of ENVS/PRODSUPP/DATA/PRODSUPP.init-db.xml");
  // }
  //
  // @Test
  // @DatabaseSetup(value = "/ENVS/MASTER/DATA/MASTER.init-db.xml", type = DatabaseOperation.CLEAN_INSERT)
  // public void testMASTERInitDBDataFile() {
  //
  // logger.debug("No test code - testing load of ENVS/MASTER/DATA/MASTER.init-db.xml");
  // }
  //
  // @Test
  // @DatabaseSetup(value = "/ENVS/SCTRIAL/DATA/SCTRIAL.init-db.xml", type = DatabaseOperation.CLEAN_INSERT)
  // public void testSCTRIALInitDBDataFile() {
  //
  // logger.debug("No test code - testing load of ENVS/SCTRIAL/DATA/SCTRIAL.init-db.xml");
  // }
  //
  // @Test
  // @DatabaseSetup(value = "/ENVS/REPORTS/DATA/REPORTS.init-db.xml", type = DatabaseOperation.CLEAN_INSERT)
  // public void testREPORTSInitDBDataFile() {
  //
  // logger.debug("No test code - testing load of /ENVS/REPORTS/DATA/REPORTS.init-db.xml");
  // }

}