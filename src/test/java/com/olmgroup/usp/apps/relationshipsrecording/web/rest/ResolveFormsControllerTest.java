// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::rest::ResolveFormsController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.ResolveFormsController
 */
public class ResolveFormsControllerTest extends ResolveFormsControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.ResolveFormsControllerTest#testCanAddForms()
   */
  @Override
  protected void handleTestCanAddForms() throws Exception {
  }

  // Add additional test cases here
}
