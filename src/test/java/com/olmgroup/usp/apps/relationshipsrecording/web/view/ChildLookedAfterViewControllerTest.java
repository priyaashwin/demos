// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ChildLookedAfterViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildLookedAfterViewController
 */

public class ChildLookedAfterViewControllerTest
    extends ChildLookedAfterViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildLookedAfterViewControllerTest#testChildLookedAfterView()
   */
  protected void handleTestChildLookedAfterView() throws Exception {
    getMockMvc().perform(get("/childlookedafter/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("childlookedafter"));

  }

  @WithUserDetails("scottishUser")
  @Test
  @DatabaseSetup(
      value = { "/dbunit/ChildLookedAfterViewController/ChildLookedAfterViewController.setup.xml",
          "/dbunit/ChildLookedAfterViewController/childLookedAfterView.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testChildLookedAfterView_ScottishContext() throws Exception {
    getMockMvc().perform(get("/childlookedafter/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("childlookedafter"));
  }
  
  @WithUserDetails("welshUser")
  @Test
  @DatabaseSetup(
		  value={"/dbunit/ChildLookedAfterViewController/ChildLookedAfterViewController.setup.xml",
				  "/dbunit/ChildLookedAfterViewController/childLookedAfterView.setup.xml"},
      type = DatabaseOperation.CLEAN_INSERT)
  public void testChildLookedAfterView_WelshContext() throws Exception {
    getMockMvc().perform(get("/childlookedafter/person?id=-1")
        .accept(MediaType.TEXT_HTML))    
    .andExpect(model().attributeExists("person"))
    .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
    .andExpect(view().name("childlookedafter"));
  }
}
