// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    security/VOSecurityHandlerTestImpl.vsl in usp-spring-cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::vo::PersonGroupMembershipDetailsWithAddressWarningCountVO
 * STEREOTYPE:  ValueObject
 */
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonGroupMembershipDetailsWithAddressWarningCountVO;
import com.olmgroup.usp.components.person.service.PersonGroupMembershipService;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import javax.inject.Inject;

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandlerTest
    extends
    PersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandlerTestBase {

  @Inject
  private PersonGroupMembershipService personGroupMembershipService;

  @Before
  public final void handleInitializeTestSuite() {
    this.login("admin", "admin123");
  }

  @Override
  protected final void handleTestSupportsRelationalSecurityByTargetDomainObject()
      throws Exception {
    final PersonGroupMembershipDetailsWithAddressWarningCountVO personGroupMembershipDetailsWithAddressWarningCountVO = new PersonGroupMembershipDetailsWithAddressWarningCountVO();

    final boolean supportsRelationalSecurityByTargetDomainObject = this
        .getPersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandler()
        .supportsRelationalSecurity(
            personGroupMembershipDetailsWithAddressWarningCountVO);

    Assert.assertTrue(supportsRelationalSecurityByTargetDomainObject);
  }

  @Override
  protected final void handleTestGetSubjectsByTargetDomainObject()
      throws Exception {
    final PersonGroupMembershipDetailsWithAddressWarningCountVO personGroupMembershipDetailsWithAddressWarningCountVO = this.personGroupMembershipService
        .findById(-20L, PersonGroupMembershipDetailsWithAddressWarningCountVO.class);

    final SubjectIdTypeVO expectedPersonSubject = new SubjectIdTypeVO(-1L,
        SubjectType.PERSON);
    final SubjectIdTypeVO expectedGroupSubject = new SubjectIdTypeVO(-5L,
        SubjectType.GROUP);

    final Set<SubjectIdTypeVO> subjects = this
        .getPersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandler()
        .getSubjects(personGroupMembershipDetailsWithAddressWarningCountVO);

    Assert.assertEquals(2, subjects.size());
    Assert.assertTrue(subjects.contains(expectedPersonSubject));
    Assert.assertTrue(subjects.contains(expectedGroupSubject));
  }

}