package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import javax.inject.Named;

@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development" })
public class AdvancedSearchContextResolverTest extends AbstractUnifiedTestBase {

  @Inject
  @Named("advancedSearchContextResolver")
  private AdvancedSearchContextResolver advancedSearchContextResolver;

  @Before
  public void setUp() {
    login("admin", "admin123");
  }

  @Test
  public void testDefaultContext() throws Exception {
    assertThat(advancedSearchContextResolver.resolveAdvancedSearchContext(),
        equalTo(AdvancedSearchContext.DEFAULT));
  }

  @Test
  public void testScottishAdvancedSearchContext() throws Exception {
    usp().getContextAttributesHandler().set(AdvancedSearchContextResolver.ADVANCED_SEARCH_CONTEXT,
        AdvancedSearchContext.SCOTTISH.toString());
    assertThat(advancedSearchContextResolver.resolveAdvancedSearchContext(),
        equalTo(AdvancedSearchContext.SCOTTISH));
  }

}
