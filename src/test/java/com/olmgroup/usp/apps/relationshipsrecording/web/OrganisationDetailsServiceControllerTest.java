// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringServiceControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: $controller.validationName
 * STEREOTYPE:  Service
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.web;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.apps.relationshipsrecording.vo.OrganisationDetailsWithProfessionalWarningListVO;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.Calendar;

/**
 * @see OrganisationDetailsServiceController
 */
@WithUserDetails("super")
public class OrganisationDetailsServiceControllerTest extends OrganisationDetailsServiceControllerTestBase {

  /**
   * @see OrganisationDetailsServiceControllerTest#testCombinedUpdateDeleteOrganisationContact()
   */
  @Override
  protected final void handleTestCombinedUpdateDeleteOrganisationContact() throws Exception {

    final JSONObject updateDeleteOrganisationContact = new JSONObject();
    updateDeleteOrganisationContact.put("_type", "UpdateDeleteOrganisationContacts");

    final String content = updateDeleteOrganisationContact.toString();

    getMockMvc()
        .perform(post(getRootURL() + "/contact/invoke").param("action", "combinedUpdateDeleteOrganisation")
            .param("organisationId", "-1").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

  }

  // Test2 - Update an existing contact(-3). Operation should be successfull.
  // Delete an existing contact(-1). Operation should be successfull.
  @DatabaseSetup(value = {
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void testUpdateOrganisationContacts_MultipleUpdateDeleteSuccessfuly() throws Exception {
    // Update Contact -3
    final JSONArray updateOrganisationContacts = new JSONArray();
    final JSONObject updateOrganisationContact = new JSONObject();
    updateOrganisationContact.put("_type", "UpdateTelephoneContact");
    updateOrganisationContact.put("id", -3);
    updateOrganisationContact.put("objectVersion", 1);
    updateOrganisationContact.put("number", "02058585858");
    updateOrganisationContacts.put(updateOrganisationContact);

    // Delete reference number -1
    final JSONArray deleteOrganisationContacts = new JSONArray();
    deleteOrganisationContacts.put(-1);

    // Combine into one big VO
    final JSONObject updateDeleteOrganisationContact = new JSONObject();
    updateDeleteOrganisationContact.put("_type", "UpdateDeleteOrganisationContacts");
    updateDeleteOrganisationContact.put("updateContactVOs", updateOrganisationContacts);
    updateDeleteOrganisationContact.put("deleteContactIds", deleteOrganisationContacts);

    final String content = updateDeleteOrganisationContact.toString();

    // Issue the combined update/delete
    getMockMvc()
        .perform(post(getRootURL() + "/contact/invoke").param("action", "combinedUpdateDeleteOrganisation")
            .param("organisationId", "-1").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    // Verify that it's gone
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-1")).andExpect(status().isNotFound());

    // Check if the update is successful
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-3").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-3)).andExpect(jsonPath("number").value("02058585858"));
  }

  // Test3 - Try to update both valid and invalid contacts
  // Valid (-3, -5) and Invalid contacts (-99) to update & valid (-1, -2, -4)
  // and invalid (-99) contacts to delete is provided
  // Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/OrganisationDetailsService.setup.xml",
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void testDeleteMultipleValidAndInvalidContact_ContactForOrganisationNotExits() throws Exception {
    // Update Contact -3 : a valid contact
    final JSONArray updateOrganisationContacts = new JSONArray();
    JSONObject updateOrganisationContact = new JSONObject();
    updateOrganisationContact.put("_type", "UpdateTelephoneContact");
    updateOrganisationContact.put("id", -3);
    updateOrganisationContact.put("objectVersion", 1);
    updateOrganisationContact.put("number", "02058585858");
    updateOrganisationContacts.put(updateOrganisationContact);

    // Update Contact -99 : an invalid contact
    updateOrganisationContact = new JSONObject();
    updateOrganisationContact.put("_type", "UpdateTelephoneContact");
    updateOrganisationContact.put("id", -99);
    updateOrganisationContact.put("objectVersion", 1);
    updateOrganisationContact.put("number", "0200101010101");
    updateOrganisationContacts.put(updateOrganisationContact);

    // Update Contact -5 : a valid contact
    updateOrganisationContact = new JSONObject();
    updateOrganisationContact.put("_type", "UpdateTelephoneContact");
    updateOrganisationContact.put("id", -5);
    updateOrganisationContact.put("objectVersion", 1);
    updateOrganisationContact.put("number", "0200101010101");
    updateOrganisationContacts.put(updateOrganisationContact);

    final JSONArray deleteOrganisationContacts = new JSONArray();

    // Delete reference number -4
    deleteOrganisationContacts.put(-4);
    // Delete reference number -99
    deleteOrganisationContacts.put(-99);
    // Delete reference number -1
    deleteOrganisationContacts.put(-1);
    // Delete reference number -2
    deleteOrganisationContacts.put(-2);

    // Combine into one big VO
    final JSONObject updateDeleteOrganisationContact = new JSONObject();
    updateDeleteOrganisationContact.put("_type", "UpdateDeleteOrganisationContacts");
    updateDeleteOrganisationContact.put("updateContactVOs", updateOrganisationContacts);
    updateDeleteOrganisationContact.put("deleteContactIds", deleteOrganisationContacts);

    final String content = updateDeleteOrganisationContact.toString();

    // Issue the combined update/delete
    getMockMvc()
        .perform(post(getRootURL() + "/contact/invoke").param("action", "combinedUpdateDeleteOrganisation")
            .param("organisationId", "-1").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("ContactForOrganisationNotFoundException"));

    // Verify that -4 gone
    getMockMvc().perform(get(getRootURL() + "/webContact/-4")).andExpect(status().isOk());

    // Verify that -1 gone
    getMockMvc().perform(get(getRootURL() + "/emailContact/-1")).andExpect(status().isOk());

    // Verify that -2 gone
    getMockMvc().perform(get(getRootURL() + "/emailContact/-2")).andExpect(status().isOk());

    // Check if the number is updated successfully for contact -3
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-3").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-3)).andExpect(jsonPath("number").value("02058585858"));

    // Check if the number is NOT updated successfully for contact -5
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-5").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-5)).andExpect(jsonPath("number").value("07589365852"));

  }

  // Test4 - Update & Delete Contact for Organisation -1
  // Invalid contact (-99) is provided to update
  // Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = {
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void testUpdateInvalidContact_ContactForOrganisationNotExits() throws Exception {
    // Update Contact -99
    final JSONArray updateOrganisationContacts = new JSONArray();
    final JSONObject updateOrganisationContact = new JSONObject();
    updateOrganisationContact.put("_type", "UpdateTelephoneContact");
    updateOrganisationContact.put("id", -99);
    updateOrganisationContact.put("objectVersion", 1);
    updateOrganisationContact.put("number", "02058585858");
    updateOrganisationContacts.put(updateOrganisationContact);

    // Delete reference number -4
    final JSONArray deleteOrganisationContacts = new JSONArray();
    deleteOrganisationContacts.put(-4);

    // Combine into one big VO
    final JSONObject updateDeleteOrganisationContact = new JSONObject();
    updateDeleteOrganisationContact.put("_type", "UpdateDeleteOrganisationContacts");
    updateDeleteOrganisationContact.put("updateContactVOs", updateOrganisationContacts);
    updateDeleteOrganisationContact.put("deleteContactIds", deleteOrganisationContacts);

    final String content = updateDeleteOrganisationContact.toString();

    // Issue the combined update/delete
    getMockMvc()
        .perform(post(getRootURL() + "/contact/invoke").param("action", "combinedUpdateDeleteOrganisation")
            .param("organisationId", "-1").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("ContactForOrganisationNotFoundException"));

    // Verify that it's NOT deleted
    getMockMvc().perform(get(getRootURL() + "/webContact/-4")).andExpect(status().isOk());

    // Check if the number has previous value
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-3").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-3)).andExpect(jsonPath("number").value("07589365852"));

  }

  // Test5 - Delete Contact for Organisation -1
  // Invalid contact (-99) is provided to delete
  // Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = {
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void testDeleteInvalidContact_ContactForOrganisationNotExits() throws Exception {
    // Update Contact -3
    final JSONArray updateOrganisationContacts = new JSONArray();
    final JSONObject updateOrganisationContact = new JSONObject();
    updateOrganisationContact.put("_type", "UpdateTelephoneContact");
    updateOrganisationContact.put("id", -3);
    updateOrganisationContact.put("objectVersion", 1);
    updateOrganisationContact.put("number", "02058585858");
    updateOrganisationContacts.put(updateOrganisationContact);

    // Delete reference number -1, -99, -4
    final JSONArray deleteOrganisationContacts = new JSONArray();
    deleteOrganisationContacts.put(-4);
    deleteOrganisationContacts.put(-99);
    deleteOrganisationContacts.put(-1);

    // Combine into one big VO
    final JSONObject updateDeleteOrganisationContact = new JSONObject();
    updateDeleteOrganisationContact.put("_type", "UpdateDeleteOrganisationContacts");
    updateDeleteOrganisationContact.put("updateContactVOs", updateOrganisationContacts);
    updateDeleteOrganisationContact.put("deleteContactIds", deleteOrganisationContacts);

    final String content = updateDeleteOrganisationContact.toString();

    // Verify that -99 NOT exist
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-99")).andExpect(status().isNotFound());

    // Issue the combined update/delete
    getMockMvc()
        .perform(post(getRootURL() + "/contact/invoke").param("action", "combinedUpdateDeleteOrganisation")
            .param("organisationId", "-1").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("ContactForOrganisationNotFoundException"));

    // Check if the number is updated successfully for contact -3
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-3").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-3)).andExpect(jsonPath("number").value("02058585858"));

    // Verify that -4 deleted
    getMockMvc().perform(get(getRootURL() + "/webContact/-4")).andExpect(status().isNotFound());

    // Verify that -1 NOT deleted
    getMockMvc().perform(get(getRootURL() + "/emailContact/-1")).andExpect(status().isOk());
  }

  // Test1 - Empty update & delete attributes.
  // Operation should be successful (no any delete or update action).
  @Override
  protected final void handleTestCombinedUpdateDeleteOrganisationReferenceNumber() throws Exception {
    final JSONObject updateDeleteOrganisationReferenceNumber = new JSONObject();
    updateDeleteOrganisationReferenceNumber.put("_type", "UpdateDeleteOrganisationReferenceNumbers");

    final String content = updateDeleteOrganisationReferenceNumber.toString();

    getMockMvc().perform(post(getRootURL() + "/organisationReferenceNumber/invoke")
        .param("action", "combinedUpdateDelete").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

  }

  // Test2 - Update an existing reference number(-1). Operation should be
  // successfull.
  // Delete an existing reference number(-3). Operation should be successfull.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/OrganisationDetailsService.setup.xml",
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void testUpdateOrganisationReferenceNumbers_MultipleUpdateDeleteSuccessfuly() throws Exception {
    // Update Reference Number -1
    final JSONArray updateOrganisationReferenceNumbers = new JSONArray();
    final JSONObject updateOrganisationReferenceNumber = new JSONObject();
    updateOrganisationReferenceNumber.put("_type", "UpdateOrganisationReferenceNumber");
    updateOrganisationReferenceNumber.put("id", -1);
    updateOrganisationReferenceNumber.put("objectVersion", 1);
    updateOrganisationReferenceNumber.put("referenceNumber", "N111999999");
    updateOrganisationReferenceNumbers.put(updateOrganisationReferenceNumber);

    // Delete reference number -3
    final JSONArray deleteOrganisationReferenceNumbers = new JSONArray();
    deleteOrganisationReferenceNumbers.put(-3);

    // Combine into one big VO
    final JSONObject updateDeleteOrganisationReferenceNumber = new JSONObject();
    updateDeleteOrganisationReferenceNumber.put("_type", "UpdateDeleteOrganisationReferenceNumbers");
    updateDeleteOrganisationReferenceNumber.put("updateReferenceNumberVOs", updateOrganisationReferenceNumbers);
    updateDeleteOrganisationReferenceNumber.put("deleteOrganisationReferenceNumberIds",
        deleteOrganisationReferenceNumbers);

    final String content = updateDeleteOrganisationReferenceNumber.toString();

    // Issue the combined update/delete
    getMockMvc().perform(post(getRootURL() + "/organisationReferenceNumber/invoke")
        .param("action", "combinedUpdateDelete").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    // Check if the reference number with id -3 does not exists (deleted
    // successfully)
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-3").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound()).andExpect(jsonPath("status").value(404));

    // Check if the reference number with id -1 has a ref num "N111999999"
    // (updated successfully)
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-1").accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI")).andExpect(jsonPath("referenceNumber").value("N111999999"));
  }

  // Test3 - Update multiple reference number(-1, -99, -3). -99 is invalid.
  // Delete multiple reference number(-3,-99,-4). -99 is invalid.
  // The operations should throw OrganisationReferenceNumberNotFoundException
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/OrganisationDetailsService.setup.xml",
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void testUpdateOrganisationReferenceNumbers_MultipleUpdateDeleteSomeInvalid() throws Exception {
    // Update Reference Number -1
    final JSONArray updateOrganisationReferenceNumbers = new JSONArray();
    JSONObject updateOrganisationReferenceNumber = new JSONObject();
    updateOrganisationReferenceNumber.put("_type", "UpdateOrganisationReferenceNumber");
    updateOrganisationReferenceNumber.put("id", -1);
    updateOrganisationReferenceNumber.put("objectVersion", 1);
    updateOrganisationReferenceNumber.put("referenceNumber", "N111999999");
    updateOrganisationReferenceNumbers.put(updateOrganisationReferenceNumber);
    // Update Reference Number -99
    updateOrganisationReferenceNumber = new JSONObject();
    updateOrganisationReferenceNumber.put("_type", "UpdateOrganisationReferenceNumber");
    updateOrganisationReferenceNumber.put("id", -99);
    updateOrganisationReferenceNumber.put("objectVersion", 1);
    updateOrganisationReferenceNumber.put("referenceNumber", "N111999999");
    updateOrganisationReferenceNumbers.put(updateOrganisationReferenceNumber);
    // Update Reference Number -2
    updateOrganisationReferenceNumber = new JSONObject();
    updateOrganisationReferenceNumber.put("_type", "UpdateOrganisationReferenceNumber");
    updateOrganisationReferenceNumber.put("id", -2);
    updateOrganisationReferenceNumber.put("objectVersion", 1);
    updateOrganisationReferenceNumber.put("referenceNumber", "N111888888");
    updateOrganisationReferenceNumbers.put(updateOrganisationReferenceNumber);

    final JSONArray deleteOrganisationReferenceNumbers = new JSONArray();

    // Delete reference number -3
    deleteOrganisationReferenceNumbers.put(-3);

    // Delete reference number -99
    deleteOrganisationReferenceNumbers.put(-99);

    // Delete reference number -4
    deleteOrganisationReferenceNumbers.put(-4);

    // Combine into one big VO
    final JSONObject updateDeleteOrganisationReferenceNumber = new JSONObject();
    updateDeleteOrganisationReferenceNumber.put("_type", "UpdateDeleteOrganisationReferenceNumbers");
    updateDeleteOrganisationReferenceNumber.put("updateReferenceNumberVOs", updateOrganisationReferenceNumbers);
    updateDeleteOrganisationReferenceNumber.put("deleteOrganisationReferenceNumberIds",
        deleteOrganisationReferenceNumbers);

    final String content = updateDeleteOrganisationReferenceNumber.toString();

    // Issue the combined update/delete
    getMockMvc()
        .perform(post(getRootURL() + "/organisationReferenceNumber/invoke")
            .param("action", "combinedUpdateDelete").content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("OrganisationReferenceNumberNotFoundException"));

    // Check if the reference number with id -1 has a ref num "N123456789"
    // (NOT updated)
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-1").accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI")).andExpect(jsonPath("referenceNumber").value("N111999999"));

    // Check if the reference number with id -2 has a ref num "N123456789"
    // (NOT updated)
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-2").accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI")).andExpect(jsonPath("referenceNumber").value("N123456789"));

    // Check if the reference number with id -3 exists / NOT deleted
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-3").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    // Check if the reference number with id -4 exists / NOT deleted
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-4").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

  }

  // Test4 - Delete Reference number for Organisation -1
  // Invalid reference number (-99) is provided to delete
  // Operation should raise OrganisationReferenceNumberNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/OrganisationDetailsService.setup.xml",
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void testDeleteInvalidOrganisationRefNumber_OrganisationReferenceNumberNotExits() throws Exception {
    // Update Reference Number -1
    final JSONArray updateOrganisationReferenceNumbers = new JSONArray();
    final JSONObject updateOrganisationReferenceNumber = new JSONObject();
    updateOrganisationReferenceNumber.put("_type", "UpdateOrganisationReferenceNumber");
    updateOrganisationReferenceNumber.put("id", -1);
    updateOrganisationReferenceNumber.put("objectVersion", 1);
    updateOrganisationReferenceNumber.put("referenceNumber", "N111999999");
    updateOrganisationReferenceNumbers.put(updateOrganisationReferenceNumber);

    final JSONArray deleteOrganisationReferenceNumbers = new JSONArray();

    // Delete reference number -3
    deleteOrganisationReferenceNumbers.put(-3);

    // Delete reference number -99
    deleteOrganisationReferenceNumbers.put(-99);

    // Delete reference number -4
    deleteOrganisationReferenceNumbers.put(-4);

    // Combine into one big VO
    final JSONObject updateDeleteOrganisationReferenceNumber = new JSONObject();
    updateDeleteOrganisationReferenceNumber.put("_type", "UpdateDeleteOrganisationReferenceNumbers");
    updateDeleteOrganisationReferenceNumber.put("updateReferenceNumberVOs", updateOrganisationReferenceNumbers);
    updateDeleteOrganisationReferenceNumber.put("deleteOrganisationReferenceNumberIds",
        deleteOrganisationReferenceNumbers);

    final String content = updateDeleteOrganisationReferenceNumber.toString();

    // Issue the combined update/delete
    getMockMvc()
        .perform(post(getRootURL() + "/organisationReferenceNumber/invoke")
            .param("action", "combinedUpdateDelete").content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("OrganisationReferenceNumberNotFoundException"));

    // Check if the reference number with id -1 has a ref num "N123456789"
    // (NOT updated)
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-1").accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI")).andExpect(jsonPath("referenceNumber").value("N111999999"));

    // Check if the reference number with id -3 deleted successfully
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-3").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());

    // Check if the reference number with id -4 exists / NOT deleted
    getMockMvc().perform(get(getRootURL() + "/organisationReferenceNumber/-4").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  // Test where a newTeam contains a NewLocation does not have
  // primaryNameOrNumber
  // Result - The constraint is thrown and the message is presented in the
  // exeception payload
  @Test
  public void testAddTeamWithNoPrimaryNameOrNumber() throws Exception {
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    JSONObject newTeam = new JSONObject();
    newTeam.put("name", "The C team");
    newTeam.put("objectVersion", 0);
    newTeam.put("objectVersion", 0);
    newTeam.put("description", "Testing add via controller.");
    newTeam.put("securityRecordTypeConfigurationId", -1);
    newTeam.put("_type", "NewTeam");
    newTeam.put("externalResource", newExternalResource);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Magilligan");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Martello Tower");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TW11 9AA");
    newLocationVO.put("county", "Derry");

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newTeam.put("addresses", newAddresses);

    String content = newTeam.toString();

    getMockMvc().perform(post(getRootURL() + "/team").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest()).andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(jsonPath("validationErrors",
            Matchers.hasEntry("NewLocationVO", "Building name or number must be specified.")));
  }

  // Test where a newOrganisation contains a NewLocation that has an invalid
  // Postcode
  // Result - The contraint is thrown and the message provides information
  // about the invalid postcode and what the
  // invalid value is
  @Test
  public void testAddOrganisationNoPostcode() throws Exception {
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    JSONObject newOrganisation = new JSONObject();
    newOrganisation.put("name", "The C team");
    newOrganisation.put("objectVersion", 0);
    newOrganisation.put("description", "Testing add via controller.");
    newOrganisation.put("_type", "NewOrganisation");
    newOrganisation.put("externalResource", newExternalResource);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Magilligan");
    newLocationVO.put("primaryNameOrNumber", "Martello Tower");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("county", "Derry");

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newOrganisation.put("addresses", newAddresses);

    String content = newOrganisation.toString();

    getMockMvc()
        .perform(post(getRootURL() + "/organisation").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest()).andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(jsonPath("validationErrors",
            Matchers.hasEntry("NewLocationVO", "Postcode must be specified.")));
  }

  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/OrganisationDetailsService.setup.xml",
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddOrganisationAddressesAllowOverlapping_InvalidPostcode() throws Exception {

    String content;
    JSONArray organisationAddressList = new JSONArray();

    // Setup addresses and add it to organisationAddressList

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "111111111111111111");
    newLocationVO.put("county", "Devon");

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 10);
    startDateAddressA.put("month", 10);
    startDateAddressA.put("year", 2016);

    JSONObject endDateAddressA = new JSONObject();
    endDateAddressA.put("_type", "SimpleFuzzyDate");
    endDateAddressA.put("day", 10);
    endDateAddressA.put("month", 11);
    endDateAddressA.put("year", 2016);

    JSONObject newOrganisationAddressA = new JSONObject();
    newOrganisationAddressA.put("type", "HOME");
    newOrganisationAddressA.put("usage", "PLACEMENT");
    newOrganisationAddressA.put("startDate", startDateAddressA);
    newOrganisationAddressA.put("newLocationVO", newLocationVO);
    newOrganisationAddressA.put("endDate", endDateAddressA);
    newOrganisationAddressA.put("_type", "NewOrganisationAddress");
    organisationAddressList.put(newOrganisationAddressA);

    JSONObject newOrganisationAddresses = new JSONObject();
    newOrganisationAddresses.put("_type", "NewOrganisationAddresses");
    newOrganisationAddresses.put("newOrganisationAddressList", organisationAddressList);

    content = newOrganisationAddresses.toString();

    getMockMvc()
        .perform(post(getRootURL() + "/organisation/-1/address").param("multiple", "true").content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andDo(print()).andExpect(status().isBadRequest()).andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("validationErrors", Matchers.hasEntry("NewLocationVO", "Postcode is not valid.")));
  }

  @Test
  @DatabaseSetup(value = {
      "/dbunit/OrganisationDetailsService/findProfessionalsWithWarningByOrganisationIdAndWarningDate.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testFindProfessionalWithWarningsByOrganisationIdAndWarningDate() throws Exception {
    // Organisation id -1
    // person -1 has Employer Employee Relationship, 3 warnings, 2 active, 1
    // inactive
    // person -2 has Employer Employee Relationship, 2 warnings both
    // inactive
    // person -3 has Employer Employee Relationship no warnings
    // person -4 has Employer Employee Relationship 1 active warning
    // person -6 has PUPILSCHOOL relationship 2 warnings both active

    getMockMvc()
        .perform(get(getRootURL() + "/organisation/-1").accept(getMIMETypeFromVOClass(
            MediaType.APPLICATION_JSON, OrganisationDetailsWithProfessionalWarningListVO.class)))
        .andExpect(status().isOk())

        .andExpect(content().contentTypeCompatibleWith(getMIMETypeFromVOClass(MediaType.APPLICATION_JSON,
            OrganisationDetailsWithProfessionalWarningListVO.class)))
        .andExpect(jsonPath("id").value(-1)) // organisation id
        .andExpect(jsonPath("$.warningPersonNameAndIdList[*].name", containsInAnyOrder("Adam Towner", "Angel Tils")))
        .andExpect(jsonPath("warningPersonNameAndIdList", hasSize(2)));
  }

}
