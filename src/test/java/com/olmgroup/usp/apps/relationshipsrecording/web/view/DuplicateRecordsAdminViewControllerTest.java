// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::DuplicateRecordAdminViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.DuplicateRecordAdminViewController
 */

public class DuplicateRecordsAdminViewControllerTest
    extends DuplicateRecordsAdminViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.DuplicateRecordsAdminViewControllerTest#testViewDuplicateRecords()
   */
  @Override
  protected void handleTestViewDuplicateRecords() throws Exception {
    this.getMockMvc().perform(get("/admin/duplicaterecords").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
