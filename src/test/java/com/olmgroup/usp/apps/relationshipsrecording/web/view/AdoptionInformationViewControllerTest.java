// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::AdoptionInformationViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.AdoptionInformationViewController
 */

public class AdoptionInformationViewControllerTest
    extends AdoptionInformationViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.AdoptionInformationViewControllerTest#testViewAdoptionInformation()
   */
  protected void handleTestViewAdoptionInformation() throws Exception {
    getMockMvc().perform(get("/adoptioninformation/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("adoptioninformation"))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
