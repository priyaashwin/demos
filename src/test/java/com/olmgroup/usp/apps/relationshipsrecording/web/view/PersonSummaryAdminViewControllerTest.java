// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::PersonSummaryAdminViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonSummaryAdminViewController
 */

@WithUserDetails("super")
public class PersonSummaryAdminViewControllerTest
    extends PersonSummaryAdminViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonSummaryAdminViewControllerTest#testViewPersonSummary()
   */
  protected void handleTestViewPersonSummary() throws Exception {
    this.getMockMvc().perform(get("/admin/personsummary").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewFormConfigurableTypeReference() throws Exception {
    this.getMockMvc().perform(get("/admin/personsummary/form").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewCaseNoteConfigurableTypeReference() throws Exception {
    this.getMockMvc().perform(get("/admin/personsummary/casenote").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
