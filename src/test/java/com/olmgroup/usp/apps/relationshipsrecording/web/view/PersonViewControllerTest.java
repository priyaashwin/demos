// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::PersonViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonViewController
 */

@WithUserDetails("super")
public class PersonViewControllerTest extends PersonViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonViewControllerTest#testPersonView()
   */
  protected void handleTestPersonView() throws Exception {
    getMockMvc().perform(get("/person/?id=-10")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-10L))))
        .andExpect(view().name("person"))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
