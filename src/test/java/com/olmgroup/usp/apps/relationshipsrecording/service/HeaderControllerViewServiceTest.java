// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::HeaderControllerViewService
 * STEREOTYPE:  Service
 * STEREOTYPE:  Unsecured
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.olmgroup.usp.components.person.service.PersonService;
import com.olmgroup.usp.components.person.utils.PersonTypesHelperBean;
import com.olmgroup.usp.components.person.vo.ManagedClassificationAssignmentVO;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.HeaderControllerViewService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class HeaderControllerViewServiceTest extends HeaderControllerViewServiceTestBase {
  @Before
  public void handleInitializeTestSuite() {
    login("manager", "manager");
  }

  @Inject
  @Named("personService")
  private PersonService personService;

  private PersonService getPersonService() {
    return this.personService;
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.service.HeaderControllerViewServiceTest#testSetupModelForPerson()
   */
  @Override
  protected void handleTestSetupModelForPerson() throws Exception {
    // /dbunit/HeaderControllerViewService/HeaderControllerViewService.setup.xml
    // /dbunit/HeaderControllerViewService/setupModelForPerson.setup.xml

    // Used in all tests
    PersonVO personVO;
    long personId;
    Model model;
    Map<String, Object> modelMap;

    PaginationResult<ManagedClassificationAssignmentVO> classificationAssignments;

    // Test values
    Long expectedNumberOfPersonWarnings;
    Long actualNumberOfPersonWarnings;
    String expectedLoginName;
    String actualLoginName;
    int expectedNumberOfClassifications;
    int actualNumberOfClassifications;

    // Person has no warnings and no classifications
    personId = -1;
    model = new ExtendedModelMap();
    personVO = getPersonService().findById(personId);

    expectedNumberOfPersonWarnings = Long.valueOf(0);
    expectedNumberOfClassifications = 0;
    expectedLoginName = null;

    model.addAttribute("person", personVO);

    getHeaderControllerViewService().setupModelForPerson(model, personId);
    modelMap = model.asMap();
    actualNumberOfPersonWarnings = (Long) modelMap
        .get("noOfPersonWarnings");
    classificationAssignments = (PaginationResult<ManagedClassificationAssignmentVO>) modelMap
        .get("classifications");

    actualNumberOfClassifications = classificationAssignments.getResults()
        .size();

    actualLoginName = (String) modelMap.get("loginPersonName");

    Assert.assertEquals(expectedNumberOfPersonWarnings,
        actualNumberOfPersonWarnings);
    Assert.assertEquals(expectedNumberOfClassifications,
        actualNumberOfClassifications);
    Assert.assertEquals(expectedLoginName, actualLoginName);

    // Person -2 has 2 warnings and 2 classifications
    personId = -2;
    model = new ExtendedModelMap();
    personVO = getPersonService().findById(personId);

    expectedNumberOfPersonWarnings = Long.valueOf(2);
    expectedNumberOfClassifications = 2;

    model.addAttribute("person", personVO);

    getHeaderControllerViewService().setupModelForPerson(model, personId);
    modelMap = model.asMap();
    actualNumberOfPersonWarnings = (Long) modelMap
        .get("noOfPersonWarnings");
    classificationAssignments = (PaginationResult<ManagedClassificationAssignmentVO>) modelMap
        .get("classifications");

    actualNumberOfClassifications = classificationAssignments.getResults()
        .size();

    Assert.assertEquals(expectedNumberOfPersonWarnings,
        actualNumberOfPersonWarnings);
    Assert.assertEquals(expectedNumberOfClassifications,
        actualNumberOfClassifications);

  }
  // Add additional test cases here

  @Override
  protected void handleTestSetupModelWithSubject() throws Exception {
    final long personId = -1;
    final Model model = new ExtendedModelMap();

    getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);
    final Map<String, Object> modelMap = model.asMap();
    final PersonTypesHelperBean personTypesHelper = PersonTypesHelperBean.class.cast(modelMap
        .get("personTypesHelper"));

    assertThat(personTypesHelper.isClient(), equalTo(true));
    assertThat(personTypesHelper.isFosterCarer(), equalTo(false));
  }
}