// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::rest::RelationshipsController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

import static com.olmgroup.usp.facets.test.hamcrest.ArraySizeMatcher.size;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import org.junit.Test;
import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.RelationshipsController
 */
public class RelationshipsControllerTest extends RelationshipsControllerTestBase {

  @Override
  protected final void handleTestFindAllByPersonId() throws Exception {
    getMockMvc().perform(
        get(getRootURL() + "/relationships/person/-10/all")
            .param("excludeInactive", "false")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("rootId").value(-10))
        .andExpect(jsonPath("rootClassifier").value("Person"))
        .andExpect(jsonPath("people[0].id").value(-10))
        .andExpect(jsonPath("people[0].personId").value("PER-10"))
        .andExpect(jsonPath("people[0].surname").value("Cook"))
        .andExpect(jsonPath("people[0].personalRelationships").isArray());
    // .andExpect(jsonPath("people[0].surname",Matchers.equalTo()));
    // .andExpect(jsonPath("people[0].dateOfBirth").value(new Integer(28)));
  }

  @Test
  @DatabaseSetup(value = { "/dbunit/RelationshipsController/RelationshipsController.setup.xml",
      "/dbunit/RelationshipsController/findAllByPersonId.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public final void testFindAllByPersonId_onlyOneLayerOfProfessionalRelationships() throws Exception {
    getMockMvc().perform(
        get(getRootURL() + "/relationships/person/-10/all")
            .param("excludeInactive", "false")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("rootId").value(-10))
        .andExpect(jsonPath("rootClassifier").value("Person"))
        .andExpect(jsonPath("people[0].id").value(-10))
        .andExpect(jsonPath("people[0].personalRelationships").isArray())
        .andExpect(jsonPath("people[0].professionalRelationships").isNotEmpty())
        .andExpect(jsonPath("people[1].id").value(-20))
        .andExpect(jsonPath("people[1].personalRelationships").isArray())
        .andExpect(jsonPath("people[1].organisationRelationships").isEmpty())
        .andExpect(jsonPath("people[1].professionalRelationships").isEmpty());

  }

  @DatabaseSetup(value = { "/dbunit/RelationshipsController/RelationshipsController.setup.xml",
      "/dbunit/RelationshipsController/findAllByPersonId_suggestions.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void handleTestFindAllByPersonIdWithSuggestions() throws Exception {
    final org.springframework.test.web.servlet.MvcResult result = getMockMvc().perform(
        get(getRootURL() + "/relationships/person/-10/all?includeSuggestions=true&suggestionsRelationshipId=-2")
            .param("excludeInactive", "false")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("rootId").value(-10))
        .andExpect(jsonPath("rootClassifier").value("Person"))
        .andExpect(jsonPath("people[0].id").value(-10))
        .andExpect(jsonPath("people[0].personId").value("PER-10"))
        .andExpect(jsonPath("people[0].surname").value("Cook"))
        .andExpect(jsonPath("people[0].dateOfBirthEstimated").value(false))
        .andExpect(jsonPath("people[0].personalRelationships").isArray())
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships").isArray())
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships", size(1)))
        .andExpect(jsonPath("people[2].id").value(-30))
        .andExpect(jsonPath("people[2].dateOfBirthEstimated").value(true))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].classifier").value("SUGGESTION"))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].label").value("Suggestion"))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].target").value(-20))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].relationshipClassName").value("SUGGESTION"))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].descrimatorType")
            .value("SuggestedPersonPersonRelationship"))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].suggestedRelationshipVOs", size(3)))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].suggestedRelationshipVOs[0].relationshipLabel")
            .value("Brother"))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].suggestedRelationshipVOs[1].relationshipLabel")
            .value("Step-Brother"))
        .andExpect(jsonPath("people[2].suggestedPersonalRelationships[0].suggestedRelationshipVOs[2].relationshipLabel")
            .value("Half-Brother"))
        .andReturn();
  }

  @DatabaseSetup(value = { "/dbunit/RelationshipsController/RelationshipsController.setup.xml",
      "/dbunit/RelationshipsController/findAllByPersonId_suggestions.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void handleTestFindByPersonIdForUnbornPerson() throws Exception {
    final org.springframework.test.web.servlet.MvcResult result = getMockMvc().perform(
        get(getRootURL() + "/relationships/person/-40/all")
            .param("excludeInactive", "false")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("rootId").value(-40))
        .andExpect(jsonPath("rootClassifier").value("Person"))
        .andExpect(jsonPath("people[0].id").value(-40))
        .andExpect(jsonPath("people[0].personId").value("PER-40"))
        .andExpect(jsonPath("people[0].surname").value("Chrowe"))
        .andExpect(jsonPath("people[0].lifeState").value("UNBORN"))
        .andExpect(jsonPath("people[0].dueDate").exists())
        .andReturn();
  }

  @DatabaseSetup(value = { "/dbunit/RelationshipsController/RelationshipsController.setup.xml",
      "/dbunit/RelationshipsController/findAllByPersonId_suggestions.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void handleTestFindByPersonIdForDeceasedPerson() throws Exception {
    final org.springframework.test.web.servlet.MvcResult result = getMockMvc().perform(
        get(getRootURL() + "/relationships/person/-50/all")
            .param("excludeInactive", "false")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("rootId").value(-50))
        .andExpect(jsonPath("rootClassifier").value("Person"))
        .andExpect(jsonPath("people[0].id").value(-50))
        .andExpect(jsonPath("people[0].personId").value("PER-50"))
        .andExpect(jsonPath("people[0].surname").value("Chrowe"))
        .andExpect(jsonPath("people[0].lifeState").value("DECEASED"))
        .andExpect(jsonPath("people[0].diedDate").exists())
        .andExpect(jsonPath("people[0].age").exists())
        .andReturn();
  }
}
