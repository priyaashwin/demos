// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::PersonDetailsService
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonContactsVO;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonReferenceNumbersVO;
import com.olmgroup.usp.components.contact.vo.UpdateContactVO;
import com.olmgroup.usp.components.contact.vo.UpdateEmailContactVO;
import com.olmgroup.usp.components.contact.vo.UpdateTelephoneContactVO;
import com.olmgroup.usp.components.contact.vo.UpdateWebContactVO;
import com.olmgroup.usp.components.person.exception.ContactForPersonNotFoundException;
import com.olmgroup.usp.components.person.exception.DuplicateReferenceNumberException;
import com.olmgroup.usp.components.person.exception.PersonNotFoundException;
import com.olmgroup.usp.components.person.exception.PersonReferenceNumberNotFoundException;
import com.olmgroup.usp.components.person.vo.UpdatePersonReferenceNumberVO;
import com.olmgroup.usp.facets.configuration.validation.CodedEntryMissingException;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.PersonDetailsService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonDetailsServiceTest extends PersonDetailsServiceTestBase {
  @Before
  public void handleInitializeTestSuite() {
    login("super", "testpassword");
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.service.PersonDetailsServiceTest#testUpdateDeletePersonReferenceNumbers()
   */
  // Test1 - Update an existing reference number(-1). Operation should be
  // successfull.
  // Delete an existing reference number(-3). Operation should be successfull.

  @Override
  protected void handleTestCombinedUpdateDeletePersonReferenceNumber()
      throws Exception {
    // Update Preference Number -1
    final UpdatePersonReferenceNumberVO updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-1);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdatePersonReferenceNumberVO> updateList = new ArrayList<UpdatePersonReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Delete reference number -3
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-3));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);
    updateDeletePersonReferenceNumbersVO
        .setDeletePersonReferenceNumberIds(deleteList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);

  }

  public void test_IfUpdateIsSuccessful() {

  }

  // Test2 - Update Person Reference Number
  // Invalid person reference (-99) is provided.
  // Operation should raise PersonReferenceNumberNotFoundException.
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = PersonReferenceNumberNotFoundException.class)
  public void testUpdatePersonReferenceNumbers_PersonReferenceNumberNotExits()
      throws Exception {
    // Update Preference Number -99
    final UpdatePersonReferenceNumberVO updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-99);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdatePersonReferenceNumberVO> updateList = new ArrayList<UpdatePersonReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);

  }

  // Test3 - Delete Person Reference Number
  // Invalid person reference (-99) is provided.
  // Operation should raise PersonReferenceNumberNotFoundException.
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = PersonReferenceNumberNotFoundException.class)
  public void testDeletePersonReferenceNumbers_PersonReferenceNumberNotExits()
      throws Exception {
    // Delete reference number -3
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setDeletePersonReferenceNumberIds(deleteList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);

  }

  // Test4 - Existing Reference number record with id:-2, and invalid person
  // reference (-99)
  // - Update above records
  // - Operation should raise PersonReferenceNumberNotFoundException.
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = PersonReferenceNumberNotFoundException.class)
  public void testUpdateMultiplePersonReferenceNumber_PersonReferenceNumberNotFound()
      throws Exception {
    // First Record
    UpdatePersonReferenceNumberVO updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-2);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdatePersonReferenceNumberVO> updateList = new ArrayList<UpdatePersonReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Second Record
    updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-99);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999955");

    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);
  }

  // Test5 - Existing Reference number record with id:-2, and invalid person
  // reference (-99)
  // - Delete above records
  // - Operation should raise PersonReferenceNumberNotFoundException.
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = PersonReferenceNumberNotFoundException.class)
  public void testDeleteMultiplePersonReferenceNumber_PersonReferenceNumberNotFound()
      throws Exception {
    // Delete reference number -2, and -99
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-2));
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setDeletePersonReferenceNumberIds(deleteList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);
  }

  // Test6 - Existing Reference number record with id:-2, and invalid person
  // reference (-99)
  // - Update existing one, delete invalid one
  // - Operation should raise PersonReferenceNumberNotFoundException.
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = PersonReferenceNumberNotFoundException.class)
  public void testUpdateDeleteMultiplePersonReferenceNumber_PersonReferenceNumberNotFound()
      throws Exception {
    // First Record
    final UpdatePersonReferenceNumberVO updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-2);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdatePersonReferenceNumberVO> updateList = new ArrayList<UpdatePersonReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Second Record
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);
    updateDeletePersonReferenceNumbersVO
        .setDeletePersonReferenceNumberIds(deleteList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);
  }

  // Test7 - An existing Reference number record has id:-3,
  // ReferenceNumber:N123456789, StartDate:01/01/2014 and EndDate:01/05/2014
  // - Update above record with Reference Number set to blank
  // - Operation should raise ConstraintViolation Exception.
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = PersonDetailsServiceException.class)
  // @Test(expected=ConstraintViolationException.class)
  public void testUpdatePersonReferenceNumber_NotBlankifSet()
      throws Exception {
    // Update Preference Number -3
    final UpdatePersonReferenceNumberVO updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-3);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("");

    final List<UpdatePersonReferenceNumberVO> updateList = new ArrayList<UpdatePersonReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);
  }

  // Test8 - There are two reference number records for person 504
  // id:-2, Type:NI, ReferenceNumber:N123456789, StartDate:01/01/2014 and
  // EndDate:01/05/2014
  // - id:-6, Type:NI, ReferenceNumber:D123456789
  // - Update second record (-6) with StartDate:01/03/2014
  // - Operation should be raise DuplicateReferenceNumberException
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = DuplicateReferenceNumberException.class)
  public void testUpdatePersonReferenceNumber_Overlapping() throws Exception {

    // Update Preference Number -6
    final UpdatePersonReferenceNumberVO updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-6);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setStartDate(new DateTime(2014, 03, 01, 0, 0).toDate());

    final List<UpdatePersonReferenceNumberVO> updateList = new ArrayList<UpdatePersonReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);

  }

  // Test1 - Update an existing contact(-1) for person -1. Operation should be
  // successfull.
  // Delete an existing contact(-3) for person -1. Operation should be
  // successfull.
  @Override
  protected void handleTestCombinedUpdateDeletePersonContact()
      throws Exception {
    // Update Contact -1
    final UpdateTelephoneContactVO updateTelephoneContactVO = new UpdateTelephoneContactVO();
    updateTelephoneContactVO.setId(-1);
    updateTelephoneContactVO.setObjectVersion(1);
    updateTelephoneContactVO.setNumber("02058585858");
    updateTelephoneContactVO.setTelephoneType("TELEPHONE");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateTelephoneContactVO);

    // Delete contact -3
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-3));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setUpdateContactVOs(updateList);
    updateDeletePersonContactsVO.setDeleteContactIds(deleteList);

    // set person id
    final long personId = -1;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);

  }

  // Test2 - Update Contact for Person -1
  // Invalid contact (-99) is provided.
  // Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForPersonNotFoundException.class)
  public void testUpdatePersonContacts_ContactNotExits() throws Exception {
    // Update Preference Number -99
    final UpdateContactVO updateContactVO = new UpdateContactVO();
    updateContactVO.setId(-99);
    updateContactVO.setObjectVersion(1);

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateContactVO);

    // Set update list to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setUpdateContactVOs(updateList);

    // set person id
    final long personId = -1;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);

  }

  // Test3 - Delete Contact for Person -1
  // Invalid contact (-99) is provided.
  // Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForPersonNotFoundException.class)
  public void testDeleteContacts_ContactForPersonNotExits() throws Exception {
    // Delete contact -99
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setDeleteContactIds(deleteList);

    // set person id
    final long personId = -1;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);

  }

  // Test4 - Existing contact record with id:-4, and invalid contact (-99)
  // - Update above records
  // - Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForPersonNotFoundException.class)
  public void testUpdateMultipleContact_ContactforPersonNotFoundException()
      throws Exception {
    // First Record
    UpdateEmailContactVO updateEmailContactVO = new UpdateEmailContactVO();
    updateEmailContactVO.setId(-4);
    updateEmailContactVO.setObjectVersion(1);
    updateEmailContactVO.setAddress("mynewemail@gmail.com");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateEmailContactVO);

    // Second Record
    updateEmailContactVO = new UpdateEmailContactVO();
    updateEmailContactVO.setId(-99);
    updateEmailContactVO.setObjectVersion(1);

    updateList.add(updateEmailContactVO);

    // Set update list to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setUpdateContactVOs(updateList);

    // set person id
    final long personId = -1;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);

  }

  // Test5 - Existing contact record with id:-2, and invalid contact (-99)
  // - Delete above records
  // - Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForPersonNotFoundException.class)
  public void testDeleteMultipleContact_ContactForPersonNotFound()
      throws Exception {
    // Delete reference number -2, and -99
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-2));
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setDeleteContactIds(deleteList);

    // set person id
    final long personId = -1;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);
  }

  // Test6 - Existing contact record with id:-4, and invalid contact (-99)
  // - Update existing one, delete invalid one
  // - Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForPersonNotFoundException.class)
  public void testUpdateDeleteMultipleContact_ContactForPersonNotFound()
      throws Exception {
    // First Record
    final UpdateEmailContactVO updateEmailContactVO = new UpdateEmailContactVO();
    updateEmailContactVO.setId(-4);
    updateEmailContactVO.setObjectVersion(1);
    updateEmailContactVO.setAddress("mynewemail@gmail.com");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateEmailContactVO);

    // Second Record
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setUpdateContactVOs(updateList);
    updateDeletePersonContactsVO.setDeleteContactIds(deleteList);

    // set person id
    final long personId = -1;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);
  }

  // Test7 - An existing contact record has id:-3, for an INVALID Person id
  // -99
  // - Operation should raise Person Not Found Exception.
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = PersonNotFoundException.class)
  public void testUpdateContact_PersonNotFoundException() throws Exception {
    // Update Preference Number -3
    final UpdateWebContactVO updateWebContactVO = new UpdateWebContactVO();
    updateWebContactVO.setId(-3);
    updateWebContactVO.setObjectVersion(1);
    updateWebContactVO.setAddress("my.updatedweb.com");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateWebContactVO);

    // Set update list to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setUpdateContactVOs(updateList);

    // set person id
    final long personId = -99;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);
  }

  // Test8 - Try to update a contact with invalid contact type (coded entry)
  // - Operation should be raise CodedEntryMissingException
  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = CodedEntryMissingException.class)
  public void testUpdatePersonContact_InvalidCodedEntryForContactType()
      throws Exception {

    // Update Preference Number -3
    final UpdateWebContactVO updateWebContactVO = new UpdateWebContactVO();
    updateWebContactVO.setId(-3);
    updateWebContactVO.setObjectVersion(1);
    updateWebContactVO.setContactType("InvalidContactType");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateWebContactVO);

    // Set update list to UpdateDelete VO
    final UpdateDeletePersonContactsVO updateDeletePersonContactsVO = new UpdateDeletePersonContactsVO();
    updateDeletePersonContactsVO.setUpdateContactVOs(updateList);

    // set person id
    final long personId = -1;

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService.combinedUpdateDeletePersonContact(
        updateDeletePersonContactsVO, personId);

  }

  // Add additional test cases here

  @DatabaseSetup(value = {
      "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = DuplicateReferenceNumberException.class)
  public void testUpdatePersonDuplicateUPN()
      throws Exception {

    // Update Preference Number -9

    final UpdatePersonReferenceNumberVO updateRefVO = new UpdatePersonReferenceNumberVO();
    updateRefVO.setId(-9);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("L860102015001");

    final List<UpdatePersonReferenceNumberVO> updateList = new ArrayList<UpdatePersonReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Delete reference number -3
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-3));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO = new UpdateDeletePersonReferenceNumbersVO();
    updateDeletePersonReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);
    updateDeletePersonReferenceNumbersVO
        .setDeletePersonReferenceNumberIds(deleteList);

    // call updateDelete service
    final PersonDetailsService personDetailsService = getPersonDetailsService();
    personDetailsService
        .combinedUpdateDeletePersonReferenceNumber(updateDeletePersonReferenceNumbersVO);

  }

}