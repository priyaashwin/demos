// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringServiceControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: $controller.validationName
 * STEREOTYPE:  Service
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonDetailsVO;
import com.olmgroup.usp.components.person.domain.LifeState;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;
import com.olmgroup.usp.facets.test.util.TestUtils;

import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @see PersonDetailsServiceController
 */

@WithUserDetails("super")
public class PersonDetailsServiceControllerTest extends PersonDetailsServiceControllerTestBase {

  // Test1 - Empty update & delete attributes.
  // Operation should be successful (no any delete or update action).
  @Override
  protected void handleTestCombinedUpdateDeletePersonReferenceNumber()
      throws Exception {

    JSONObject updateDeletePersonReferenceNumber = new JSONObject();
    updateDeletePersonReferenceNumber.put("_type", "UpdateDeletePersonReferenceNumbers");

    String content = updateDeletePersonReferenceNumber.toString();

    getMockMvc().perform(
        post(getRootURL() + "/personReferenceNumber/invoke")
            .param("action", "combinedUpdateDelete")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

  }

  // Test2 - Update an existing reference number(-1). Operation should be successfull.
  // Delete an existing reference number(-3). Operation should be successfull.
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
          "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testUpdatePersonReferenceNumbers_MultipleUpdateDeleteSuccessfuly() throws Exception {
    // Update Reference Number -1
    JSONArray updatePersonReferenceNumbers = new JSONArray();
    JSONObject updatePersonReferenceNumber = new JSONObject();
    updatePersonReferenceNumber.put("_type", "UpdatePersonReferenceNumber");
    updatePersonReferenceNumber.put("id", -1);
    updatePersonReferenceNumber.put("objectVersion", 1);
    updatePersonReferenceNumber.put("referenceNumber", "N111999999");
    updatePersonReferenceNumbers.put(updatePersonReferenceNumber);

    // Delete reference number -3
    JSONArray deletePersonReferenceNumbers = new JSONArray();
    deletePersonReferenceNumbers.put(-3);

    // Combine into one big VO
    JSONObject updateDeletePersonReferenceNumber = new JSONObject();
    updateDeletePersonReferenceNumber.put("_type", "UpdateDeletePersonReferenceNumbers");
    updateDeletePersonReferenceNumber.put("updateReferenceNumberVOs", updatePersonReferenceNumbers);
    updateDeletePersonReferenceNumber.put("deletePersonReferenceNumberIds", deletePersonReferenceNumbers);

    String content = updateDeletePersonReferenceNumber.toString();

    // Issue the combined update/delete
    getMockMvc().perform(
        post(getRootURL() + "/personReferenceNumber/invoke")
            .param("action", "combinedUpdateDelete")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    // Check if the reference number with id -3 does not exists (deleted successfully)
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-3")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("status").value(404));

    // Check if the reference number with id -1 has a ref num "N111999999" (updated successfully)
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-1")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI"))
        .andExpect(jsonPath("referenceNumber").value("N111999999"));
  }

  // Test3 - Update multiple reference number(-1, -99, -3). -99 is invalid.
  // Delete multiple reference number(-3,-99,-4). -99 is invalid.
  // The operations should throw PersonReferenceNumberNotFoundException
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
          "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testUpdatePersonReferenceNumbers_MultipleUpdateDeleteSomeInvalid() throws Exception {
    // Update Reference Number -1
    JSONArray updatePersonReferenceNumbers = new JSONArray();
    JSONObject updatePersonReferenceNumber = new JSONObject();
    updatePersonReferenceNumber.put("_type", "UpdatePersonReferenceNumber");
    updatePersonReferenceNumber.put("id", -1);
    updatePersonReferenceNumber.put("objectVersion", 1);
    updatePersonReferenceNumber.put("referenceNumber", "N111999999");
    updatePersonReferenceNumbers.put(updatePersonReferenceNumber);
    // Update Reference Number -99
    updatePersonReferenceNumber = new JSONObject();
    updatePersonReferenceNumber.put("_type", "UpdatePersonReferenceNumber");
    updatePersonReferenceNumber.put("id", -99);
    updatePersonReferenceNumber.put("objectVersion", 1);
    updatePersonReferenceNumber.put("referenceNumber", "N111999999");
    updatePersonReferenceNumbers.put(updatePersonReferenceNumber);
    // Update Reference Number -2
    updatePersonReferenceNumber = new JSONObject();
    updatePersonReferenceNumber.put("_type", "UpdatePersonReferenceNumber");
    updatePersonReferenceNumber.put("id", -2);
    updatePersonReferenceNumber.put("objectVersion", 1);
    updatePersonReferenceNumber.put("referenceNumber", "N111888888");
    updatePersonReferenceNumbers.put(updatePersonReferenceNumber);

    JSONArray deletePersonReferenceNumbers = new JSONArray();

    // Delete reference number -3
    deletePersonReferenceNumbers.put(-3);

    // Delete reference number -99
    deletePersonReferenceNumbers.put(-99);

    // Delete reference number -4
    deletePersonReferenceNumbers.put(-4);

    // Combine into one big VO
    JSONObject updateDeletePersonReferenceNumber = new JSONObject();
    updateDeletePersonReferenceNumber.put("_type", "UpdateDeletePersonReferenceNumbers");
    updateDeletePersonReferenceNumber.put("updateReferenceNumberVOs", updatePersonReferenceNumbers);
    updateDeletePersonReferenceNumber.put("deletePersonReferenceNumberIds", deletePersonReferenceNumbers);

    String content = updateDeletePersonReferenceNumber.toString();

    // Issue the combined update/delete
    getMockMvc().perform(
        post(getRootURL() + "/personReferenceNumber/invoke")
            .param("action", "combinedUpdateDelete")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("PersonReferenceNumberNotFoundException"));

    // Check if the reference number with id -1 has a ref num "N123456789" (NOT updated)
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-1")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI"))
        .andExpect(jsonPath("referenceNumber").value("N111999999"));

    // Check if the reference number with id -2 has a ref num "N123456789" (NOT updated)
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-2")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI"))
        .andExpect(jsonPath("referenceNumber").value("N123456789"));

    // Check if the reference number with id -3 exists / NOT deleted
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-3")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    // Check if the reference number with id -4 exists / NOT deleted
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-4")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

  }

  // Test4 - Delete Reference number for Person -1
  // Invalid reference number (-99) is provided to delete
  // Operation should raise PersonReferenceNumberNotFoundException.
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonReferenceNumber.setup.xml",
          "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testDeleteInvalidPersonRefNumber_PersonReferenceNumberNotExits() throws Exception {
    // Update Reference Number -1
    JSONArray updatePersonReferenceNumbers = new JSONArray();
    JSONObject updatePersonReferenceNumber = new JSONObject();
    updatePersonReferenceNumber.put("_type", "UpdatePersonReferenceNumber");
    updatePersonReferenceNumber.put("id", -1);
    updatePersonReferenceNumber.put("objectVersion", 1);
    updatePersonReferenceNumber.put("referenceNumber", "N111999999");
    updatePersonReferenceNumbers.put(updatePersonReferenceNumber);

    JSONArray deletePersonReferenceNumbers = new JSONArray();

    // Delete reference number -3
    deletePersonReferenceNumbers.put(-3);

    // Delete reference number -99
    deletePersonReferenceNumbers.put(-99);

    // Delete reference number -4
    deletePersonReferenceNumbers.put(-4);

    // Combine into one big VO
    JSONObject updateDeletePersonReferenceNumber = new JSONObject();
    updateDeletePersonReferenceNumber.put("_type", "UpdateDeletePersonReferenceNumbers");
    updateDeletePersonReferenceNumber.put("updateReferenceNumberVOs", updatePersonReferenceNumbers);
    updateDeletePersonReferenceNumber.put("deletePersonReferenceNumberIds", deletePersonReferenceNumbers);

    String content = updateDeletePersonReferenceNumber.toString();

    // Issue the combined update/delete
    getMockMvc().perform(
        post(getRootURL() + "/personReferenceNumber/invoke")
            .param("action", "combinedUpdateDelete")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("PersonReferenceNumberNotFoundException"));

    // Check if the reference number with id -1 has a ref num "N123456789" (NOT updated)
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-1")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("type").value("NI"))
        .andExpect(jsonPath("referenceNumber").value("N111999999"));

    // Check if the reference number with id -3 deleted successfully
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-3")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());

    // Check if the reference number with id -4 exists / NOT deleted
    getMockMvc().perform(
        get(getRootURL() + "/personReferenceNumber/-4")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  // Test1 - Empty update & delete attributes.
  // Operation should be successful (no any delete or update action).
  @Override
  protected void handleTestCombinedUpdateDeletePersonContact()
      throws Exception {

    JSONObject updateDeletePersonContact = new JSONObject();
    updateDeletePersonContact.put("_type", "UpdateDeletePersonContacts");

    String content = updateDeletePersonContact.toString();

    getMockMvc().perform(
        post(getRootURL() + "/contact/invoke")
            .param("action", "combinedUpdateDeletePerson")
            .param("personId", "-1")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

  }

  // Test2 - Update an existing contact(-1). Operation should be successfull.
  // Delete an existing contact(-3). Operation should be successfull.
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testUpdatePersonContacts_MultipleUpdateDeleteSuccessfuly() throws Exception {
    // Update Contact -1
    JSONArray updatePersonContacts = new JSONArray();
    JSONObject updatePersonContact = new JSONObject();
    updatePersonContact.put("_type", "UpdateTelephoneContact");
    updatePersonContact.put("id", -1);
    updatePersonContact.put("objectVersion", 1);
    updatePersonContact.put("number", "02058585858");
    updatePersonContacts.put(updatePersonContact);

    // Delete reference number -3
    JSONArray deletePersonContacts = new JSONArray();
    deletePersonContacts.put(-3);

    // Combine into one big VO
    JSONObject updateDeletePersonContact = new JSONObject();
    updateDeletePersonContact.put("_type", "UpdateDeletePersonContacts");
    updateDeletePersonContact.put("updateContactVOs", updatePersonContacts);
    updateDeletePersonContact.put("deleteContactIds", deletePersonContacts);

    String content = updateDeletePersonContact.toString();

    // Issue the combined update/delete
    getMockMvc().perform(
        post(getRootURL() + "/contact/invoke")
            .param("action", "combinedUpdateDeletePerson")
            .param("personId", "-1")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    // Verify that it's gone
    getMockMvc().perform(
        get(getRootURL() + "/telephoneContact/-3"))
        .andExpect(status().isNotFound());

    // Check if the update is successful
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-1")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-1))
        .andExpect(jsonPath("number").value("02058585858"));
  }

  // Test3 - Try to update both valid and invalid contacts
  // Valid (-1, -2) and Invalid contacts (-99) to update & valid (-3, -4, -5) and invalid (-99) contacts to delete is
  // provided
  // Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testDeleteMultipleValidAndInvalidContact_ContactForPersonNotExits() throws Exception {
    // Update Contact -1 : a valid contact
    JSONArray updatePersonContacts = new JSONArray();
    JSONObject updatePersonContact = new JSONObject();
    updatePersonContact.put("_type", "UpdateTelephoneContact");
    updatePersonContact.put("id", -1);
    updatePersonContact.put("objectVersion", 1);
    updatePersonContact.put("number", "02058585858");
    updatePersonContacts.put(updatePersonContact);

    // Update Contact -99 : an invalid contact
    updatePersonContact = new JSONObject();
    updatePersonContact.put("_type", "UpdateTelephoneContact");
    updatePersonContact.put("id", -99);
    updatePersonContact.put("objectVersion", 1);
    updatePersonContact.put("number", "0200101010101");
    updatePersonContacts.put(updatePersonContact);

    // Update Contact -2 : a valid contact
    updatePersonContact = new JSONObject();
    updatePersonContact.put("_type", "UpdateTelephoneContact");
    updatePersonContact.put("id", -2);
    updatePersonContact.put("objectVersion", 1);
    updatePersonContact.put("number", "0200101010101");
    updatePersonContacts.put(updatePersonContact);

    JSONArray deletePersonContacts = new JSONArray();

    // Delete reference number -3
    deletePersonContacts.put(-3);
    // Delete reference number -99
    deletePersonContacts.put(-99);
    // Delete reference number -4
    deletePersonContacts.put(-4);
    // Delete reference number -5
    deletePersonContacts.put(-5);

    // Combine into one big VO
    JSONObject updateDeletePersonContact = new JSONObject();
    updateDeletePersonContact.put("_type", "UpdateDeletePersonContacts");
    updateDeletePersonContact.put("updateContactVOs", updatePersonContacts);
    updateDeletePersonContact.put("deleteContactIds", deletePersonContacts);

    String content = updateDeletePersonContact.toString();

    // Issue the combined update/delete
    getMockMvc().perform(
        post(getRootURL() + "/contact/invoke")
            .param("action", "combinedUpdateDeletePerson")
            .param("personId", "-1")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("ContactForPersonNotFoundException"));

    // Verify that -3 gone
    getMockMvc().perform(
        get(getRootURL() + "/webContact/-3"))
        .andExpect(status().isOk());

    // Verify that -4 gone
    getMockMvc().perform(
        get(getRootURL() + "/emailContact/-4"))
        .andExpect(status().isOk());

    // Verify that -5 gone
    getMockMvc().perform(
        get(getRootURL() + "/emailContact/-5"))
        .andExpect(status().isOk());

    // Check if the number is updated successfully for contact -1
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-1")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-1))
        .andExpect(jsonPath("number").value("02058585858"));

    // Check if the number is NOT updated successfully for contact -2
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-2")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-2))
        .andExpect(jsonPath("number").value("02058965444"));

  }

  // Test4 - Update & Delete Contact for Person -1
  // Invalid contact (-99) is provided to update
  // Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testUpdateInvalidContact_ContactForPersonNotExits() throws Exception {
    // Update Contact -99
    JSONArray updatePersonContacts = new JSONArray();
    JSONObject updatePersonContact = new JSONObject();
    updatePersonContact.put("_type", "UpdateTelephoneContact");
    updatePersonContact.put("id", -99);
    updatePersonContact.put("objectVersion", 1);
    updatePersonContact.put("number", "02058585858");
    updatePersonContacts.put(updatePersonContact);

    // Delete reference number -3
    JSONArray deletePersonContacts = new JSONArray();
    deletePersonContacts.put(-3);

    // Combine into one big VO
    JSONObject updateDeletePersonContact = new JSONObject();
    updateDeletePersonContact.put("_type", "UpdateDeletePersonContacts");
    updateDeletePersonContact.put("updateContactVOs", updatePersonContacts);
    updateDeletePersonContact.put("deleteContactIds", deletePersonContacts);

    String content = updateDeletePersonContact.toString();

    // Issue the combined update/delete
    getMockMvc().perform(
        post(getRootURL() + "/contact/invoke")
            .param("action", "combinedUpdateDeletePerson")
            .param("personId", "-1")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("ContactForPersonNotFoundException"));

    // Verify that it's NOT deleted
    getMockMvc().perform(
        get(getRootURL() + "/webContact/-3"))
        .andExpect(status().isOk());

    // Check if the number has previous value
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-1")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-1))
        .andExpect(jsonPath("number").value("02058965478"));

  }

  // Test5 - Delete Contact for Person -1
  // Invalid contact (-99) is provided to delete
  // Operation should raise ContactForPersonNotFoundException.
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/combinedUpdateDeletePersonContact.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testDeleteInvalidContact_ContactForPersonNotExits() throws Exception {
    // Update Contact -1
    JSONArray updatePersonContacts = new JSONArray();
    JSONObject updatePersonContact = new JSONObject();
    updatePersonContact.put("_type", "UpdateTelephoneContact");
    updatePersonContact.put("id", -1);
    updatePersonContact.put("objectVersion", 1);
    updatePersonContact.put("number", "02058585858");
    updatePersonContacts.put(updatePersonContact);

    // Delete reference number -3, -99, -4
    JSONArray deletePersonContacts = new JSONArray();
    deletePersonContacts.put(-3);
    deletePersonContacts.put(-99);
    deletePersonContacts.put(-4);

    // Combine into one big VO
    JSONObject updateDeletePersonContact = new JSONObject();
    updateDeletePersonContact.put("_type", "UpdateDeletePersonContacts");
    updateDeletePersonContact.put("updateContactVOs", updatePersonContacts);
    updateDeletePersonContact.put("deleteContactIds", deletePersonContacts);

    String content = updateDeletePersonContact.toString();

    // Verify that -99 NOT exist
    getMockMvc().perform(
        get(getRootURL() + "/telephoneContact/-99"))
        .andExpect(status().isNotFound());

    // Issue the combined update/delete
    getMockMvc().perform(
        post(getRootURL() + "/contact/invoke")
            .param("action", "combinedUpdateDeletePerson")
            .param("personId", "-1")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(404))
        .andExpect(jsonPath("type").value("ContactForPersonNotFoundException"));

    // Check if the number is updated successfully for contact -1
    getMockMvc().perform(get(getRootURL() + "/telephoneContact/-1")
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("id").value(-1))
        .andExpect(jsonPath("number").value("02058585858"));

    // Verify that -3 deleted
    getMockMvc().perform(
        get(getRootURL() + "/webContact/-3"))
        .andExpect(status().isNotFound());

    // Verify that -4 NOT deleted
    getMockMvc().perform(
        get(getRootURL() + "/emailContact/-4"))
        .andExpect(status().isOk());
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestPersonViewPersonDetailsVO() throws Exception {
    getMockMvc().perform(
        get(getRootURL() + "/person/-10")
            .accept(getMIMETypeFromVOClass(MediaType.APPLICATION_JSON, PersonDetailsVO.class)))
        .andExpect(status().isOk())
        .andExpect(content()
            .contentTypeCompatibleWith(getMIMETypeFromVOClass(MediaType.APPLICATION_JSON, PersonDetailsVO.class)))
        .andExpect(jsonPath("_type").value("PersonDetails"))
        .andExpect(jsonPath("personIdentifier").value("PER-10"))
        .andExpect(jsonPath("title").value("MR"))
        .andExpect(jsonPath("forename").value("Alastair"))
        .andExpect(jsonPath("surname").value("Cook"))
        .andExpect(jsonPath("ethnicity").value("ENGLISH"))
        .andExpect(jsonPath("middleNames").value("Nathan"))
        .andExpect(jsonPath("name").value("Alastair Cook"))
        .andExpect(jsonPath("dateOfBirth.calculatedDate").value("1984-12-25"))
        .andExpect(jsonPath("nhsNumber").doesNotExist())
        .andExpect(jsonPath("referenceNumbers[0].referenceNumber").value("N123456789"))
        .andExpect(jsonPath("referenceNumbers[0].type").value("NI"))
        .andExpect(jsonPath("referenceNumbers[0].id").value(-7))
        .andExpect(jsonPath("referenceNumbers[0]._type").value("PersonReferenceNumberWithReferencedPersonIdentifiers"))
        .andExpect(jsonPath("referenceNumbers[0].startDate").value(TestUtils.getDate("2014-07-01")))
        .andExpect(jsonPath("referenceNumbers[0].endDate").value(TestUtils.getDate("2014-07-08")))
        .andExpect(jsonPath("telephoneContacts[0]._type").value("TelephoneContact"))
        .andExpect(jsonPath("telephoneContacts[0].id").value(-6))
        .andExpect(jsonPath("telephoneContacts[0].contactUsage").value("PERMANENT"))
        .andExpect(jsonPath("telephoneContacts[0].contactType").value("HOME"))
        .andExpect(jsonPath("telephoneContacts[0].number").value("020889900"))
        .andExpect(jsonPath("telephoneContacts[0].telephoneType").value("LANDLINE"))
        .andExpect(jsonPath("telephoneContacts[0].preferred").value(true))
        .andExpect(jsonPath("telephoneContacts[0].startDate").value(TestUtils.getDate("2013-10-10")))
        .andExpect(jsonPath("telephoneContacts[0].endDate").doesNotExist())
        .andExpect(jsonPath("emailContacts[0]._type").value("EmailContact"))
        .andExpect(jsonPath("emailContacts[0].id").value(-8))
        .andExpect(jsonPath("emailContacts[0].contactUsage").value("PLACEMENT"))
        .andExpect(jsonPath("emailContacts[0].contactType").value("WORK"))
        .andExpect(jsonPath("emailContacts[0].address").value("alister.cook@ecb.co.uk"))
        .andExpect(jsonPath("emailContacts[0].preferred").value(true))
        .andExpect(jsonPath("emailContacts[0].startDate").value(TestUtils.getDate("2013-01-20")))
        .andExpect(jsonPath("emailContacts[0].endDate").doesNotExist())
        .andExpect(jsonPath("webContacts[0]._type").value("WebContact"))
        .andExpect(jsonPath("webContacts[0].id").value(-7))
        .andExpect(jsonPath("webContacts[0].contactUsage").value("TEMPORARY"))
        .andExpect(jsonPath("webContacts[0].contactType").value("HOME"))
        .andExpect(jsonPath("webContacts[0].address").value("www.ecb.com"))
        .andExpect(jsonPath("webContacts[0].preferred").value(false))
        .andExpect(jsonPath("webContacts[0].startDate").value(TestUtils.getDate("2013-10-06")))
        .andExpect(jsonPath("webContacts[0].endDate").doesNotExist())
        .andExpect(jsonPath("otherNames[0].forename").value("Andrew"))
        .andExpect(jsonPath("otherNames[0].surname").value("Strauss"))
        .andExpect(jsonPath("otherNames[0].title").value("MR"))
        .andExpect(jsonPath("otherNames[0].type").value("PREVIOUS"))
        .andExpect(jsonPath("otherNames[0].suffix").doesNotExist())
        .andExpect(jsonPath("otherNames[0].startDate").value(TestUtils.getDate("2005-05-09")))
        .andExpect(jsonPath("otherNames[0].closeDate").value(TestUtils.getDate("2010-05-09")))
        .andExpect(jsonPath("hasDemographicsForChildCarer").value(true));

  }

  @Test
  // Adding a person with a list of new addresses.
  // Do not provide a locationId or an external identifier
  // Result - A newPersonAddress must have at least one of a locationId or external resource
  public void testAddPersonWithAddresses() throws Exception {
    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");
    JSONObject newPerson = new JSONObject();
    newPerson.put("forename", "Address");
    newPerson.put("surname", "Test");
    newPerson.put("personTypes", personType);
    newPerson.put("_type", "NewPerson");
    newPerson.put("lifeState", LifeState.ALIVE);
    // Create first address VO
    JSONObject newAddressVO = new JSONObject();
    JSONObject simpleFuzzyDate = new JSONObject();
    simpleFuzzyDate.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate.put("year", 2015);
    simpleFuzzyDate.put("month", Calendar.JUNE);
    simpleFuzzyDate.put("day", 05);
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provder");
    newExternalResource.put("resourceIdentifier", "L123");
    newExternalResource.put("_type", "NewExternalResource");
    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("ExternalResource", newLocationVO);
    newAddressVO.put("type", "HOME");
    newAddressVO.put("usage", "PLACEMENT");
    newAddressVO.put("startDate", simpleFuzzyDate);
    newAddressVO.put("_type", "NewAddress");
    List<JSONObject> list = new ArrayList<JSONObject>();
    list.add(newAddressVO);
    newPerson.put("addresses", list);
    String content = newPerson.toString();
    getMockMvc().perform(post(getRootURL() + "/person").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("status").value(400)).andExpect(jsonPath("code").value(400))
        .andExpect(jsonPath("validationErrors.NewPersonAddressVO")
            .value("Location or unknown location or LocationId must be chosen."))
        .andExpect(jsonPath("message")
            .value(
                "org.springframework.validation.BeanPropertyBindingResult: 1 errors\nError in object 'NewPersonAddressVO': codes [ConstraintViolationException.newPersonVO,ConstraintViolationException]; arguments [locationId]; default message [Location or unknown location or LocationId must be chosen.]"))
        .andExpect(jsonPath("developerMessage")
            .value(
                "org.springframework.validation.BeanPropertyBindingResult: 1 errors\nError in object 'NewPersonAddressVO': codes [ConstraintViolationException.newPersonVO,ConstraintViolationException]; arguments [locationId]; default message [Location or unknown location or LocationId must be chosen.]"))
        .andExpect(jsonPath("moreInfoUrl").value("mailto:supportDept@mycompany.com"));
  }

  @Test
  // Adding a person with a list of new addresses.
  // Provide both locationId or an external identifier
  // Result - Person is created
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWithAddressesBothLocationIdAndExternalAddress() throws Exception {
    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");
    JSONObject newPerson = new JSONObject();
    newPerson.put("forename", "Address");
    newPerson.put("surname", "Test");
    newPerson.put("personTypes", personType);
    newPerson.put("_type", "NewPerson");
    newPerson.put("lifeState", LifeState.ALIVE);
    // Create first address VO
    JSONObject newAddressVO = new JSONObject();
    JSONObject simpleFuzzyDate = new JSONObject();
    simpleFuzzyDate.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate.put("year", 2015);
    simpleFuzzyDate.put("month", Calendar.JUNE);
    simpleFuzzyDate.put("day", 05);
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provder");
    newExternalResource.put("resourceIdentifier", "E1234");
    newExternalResource.put("_type", "NewExternalResource");
    newAddressVO.put("type", "HOME");
    newAddressVO.put("usage", "PLACEMENT");
    newAddressVO.put("startDate", simpleFuzzyDate);
    newAddressVO.put("_type", "NewAddress");
    newAddressVO.put("externalResource", newExternalResource);
    newAddressVO.put("locationId", -1);
    // Add address VO to address VO list
    List<JSONObject> list = new ArrayList<JSONObject>();
    list.add(newAddressVO);
    // Add the address VOs into the new person VO
    newPerson.put("addresses", list);
    String content = newPerson.toString();
    getMockMvc().perform(post(getRootURL() + "/person").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonDetailsVOForChildCarerValidity() throws Exception {
    // Test for person with all demographic details (-10)
    testPersonForChildCarerValidity("-10", true);

    // Test for person with no forename (-600)
    testPersonForChildCarerValidity("-600", false);

    // Test for person with no surname (-700)
    testPersonForChildCarerValidity("-700", false);

    // Test for person with no date Of birth (-800)
    testPersonForChildCarerValidity("-800", false);

    // Test for person with no ethnicity (-900)
    testPersonForChildCarerValidity("-900", false);

    // Test for person with no gender (-1000)
    testPersonForChildCarerValidity("-1000", false);

    // Test for person with no home permanent address (-1100)
    testPersonForChildCarerValidity("-1100", false);

    // Test for person with no gender and ethnicity (-1200)
    testPersonForChildCarerValidity("-1200", false);

    // Test for person with no home address and date of birth (-1300)
    testPersonForChildCarerValidity("-1300", false);

  }

  // Adding a person address without a location id
  // Result - locationId is mandatory when adding an address via Eclipse
  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWithoutLocationId() throws Exception {
    // Create address VO
    JSONObject newAddressVO = new JSONObject();
    JSONObject simpleFuzzyDate = new JSONObject();
    simpleFuzzyDate.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate.put("year", 2015);
    simpleFuzzyDate.put("month", Calendar.JUNE);
    simpleFuzzyDate.put("day", 05);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provder");
    newExternalResource.put("resourceIdentifier", "E1234");
    newExternalResource.put("_type", "NewExternalResource");

    newAddressVO.put("type", "HOME");
    newAddressVO.put("usage", "PLACEMENT");
    newAddressVO.put("startDate", simpleFuzzyDate);
    newAddressVO.put("_type", "NewPersonAddress");

    String content = newAddressVO.toString();

    // Perform the REST post
    getMockMvc()
        .perform(post(getRootURL() + "/person/-10/address").content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("status").value(400)).andExpect(jsonPath("code").value(400))
        .andExpect(jsonPath("validationErrors.locationId").value("Location is mandatory"));
  }

  /**
   * Tests and verifies that the person is invalid child carer.
   * 
   * @param personId
   * @throws Exception
   */
  private void testPersonForChildCarerValidity(String personId, boolean isValidChildCarer) throws Exception {
    getMockMvc().perform(
        get(getRootURL() + "/person/" + personId)
            .accept(getMIMETypeFromVOClass(MediaType.APPLICATION_JSON, PersonDetailsVO.class)))
        .andExpect(status().isOk())
        .andExpect(content()
            .contentTypeCompatibleWith(getMIMETypeFromVOClass(MediaType.APPLICATION_JSON, PersonDetailsVO.class)))
        .andExpect(jsonPath("_type").value("PersonDetails"))
        .andExpect(jsonPath("hasDemographicsForChildCarer").value(isValidChildCarer));
  }

  // Test where a newPerson contains a NewLocation that is already held in DB
  // Result - The newLocation attribute is nulled and the locationId is set to that of the location held in the DB and
  // the person is created
  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWhereLocationAlreadyExistsAsExternalResource()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newExternalLocationResourceVO = new JSONObject();
    newExternalLocationResourceVO.put("_type", "NewExternalResource");
    newExternalLocationResourceVO.put("resourceIdentifier", "P1653105");
    newExternalLocationResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("primaryNameOrNumber", "Estuary House");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TQ12 4PH");
    newLocationVO.put("county", "Devon");
    newLocationVO.put("externalResource", newExternalLocationResourceVO);

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Test that submitting a new person with both newLocation AND locationId
  // Result - Cannot submit both newLocation AND locationId
  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWithNewLocationAndLocationId()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("primaryNameOrNumber", "Estuary House");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TQ12 4PH");
    newLocationVO.put("county", "Devon");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("locationId", -1);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("status").value(400)).andExpect(jsonPath("code").value(400))
        .andExpect(jsonPath("validationErrors",
            Matchers.hasEntry("addresses[0].locationId", "Either of location or new location can be supplied")));
  }

  // Test where a newPerson contains a NewLocation that has an invalid Postcode
  // Result - The contraint is thown and the message provides information about the invalid postcode and what the
  // invalid value is
  @Test
  public void testAddPersonWithInvalidPostcode()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("primaryNameOrNumber", "Estuary House");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "111111111111111111");
    newLocationVO.put("county", "Devon");

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("validationErrors",
            Matchers.hasEntry("NewLocationVO",
                "Postcode is not valid.")));
  }

  // Test where a newPerson contains a NewLocation does not have primaryNameOrNumber
  // Result - The constraint is thrown and the message is presented in the exeception payload
  @Test
  public void testAddPersonWithNoPrimaryNameOrNumber()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TW11 9AA");
    newLocationVO.put("county", "Devon");

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(jsonPath("validationErrors",
            Matchers.hasEntry("NewLocationVO",
                "Building name or number must be specified.")));
  }

  // Test where a newPerson contains a NewLocation does not have primaryNameOrNumber and an invalid postcode
  // Result - The constraint is thrown and the message is presents both the invalid payload an building name/number is
  // missing
  @Test
  public void testAddPersonWithNoPrimaryNameOrNumberAndInvalidPostcode()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "111111111111111111");
    newLocationVO.put("county", "Devon");

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"));
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddAddressesForPerson_NoPostCodeNoPrimaryNumber()
      throws Exception {

    String content;
    JSONArray addressList = new JSONArray();

    // Setup addresses and add it to addressList
    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("county", "Devon");

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 25);
    startDateAddressA.put("month", 10);
    startDateAddressA.put("year", 2010);

    JSONObject endDateAddressA = new JSONObject();
    endDateAddressA.put("_type", "SimpleFuzzyDate");
    endDateAddressA.put("day", 13);
    endDateAddressA.put("month", 2);
    endDateAddressA.put("year", 2015);

    JSONObject newAddressA = new JSONObject();
    newAddressA.put("type", "HOME");
    newAddressA.put("usage", "CF_OTHER");
    newAddressA.put("startDate", startDateAddressA);
    newAddressA.put("newLocation", newLocationVO);
    newAddressA.put("endDate", endDateAddressA);
    newAddressA.put("_type", "NewAddress");
    addressList.put(newAddressA);

    JSONObject newAddresses = new JSONObject();
    newAddresses.put("_type", "NewAddresses");
    newAddresses.put("newAddressList", addressList);

    content = newAddresses.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person/500/address")
                .param("multiple", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(400))
        .andExpect(jsonPath("type").value("DataBindException"));
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddAddressesForPerson_NoPrimaryNumber()
      throws Exception {

    String content;
    JSONArray addressList = new JSONArray();

    // Setup addresses and add it to addressList
    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("postcode", "BT48 7ER");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("county", "Devon");

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 25);
    startDateAddressA.put("month", 10);
    startDateAddressA.put("year", 2010);

    JSONObject endDateAddressA = new JSONObject();
    endDateAddressA.put("_type", "SimpleFuzzyDate");
    endDateAddressA.put("day", 13);
    endDateAddressA.put("month", 2);
    endDateAddressA.put("year", 2015);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    JSONObject newAddressA = new JSONObject();
    newAddressA.put("type", "HOME");
    newAddressA.put("usage", "CF_OTHER");
    newAddressA.put("startDate", startDateAddressA);
    newAddressA.put("newLocation", newLocationVO);
    newAddressA.put("endDate", endDateAddressA);
    newAddressA.put("externalResource", newExternalResource);
    newAddressA.put("_type", "NewAddress");
    addressList.put(newAddressA);

    JSONObject newAddresses = new JSONObject();
    newAddresses.put("_type", "NewAddresses");
    newAddresses.put("newAddressList", addressList);

    content = newAddresses.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person/-1300/address")
                .param("multiple", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value(400))
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(jsonPath("validationErrors",
            Matchers.hasEntry("NewLocationVO",
                "Building name or number must be specified.")));
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/isPersonAvailableAsFosterCarer.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonDetailsVOForAvailabilityAsFosterCarer() throws Exception {
    // case 1: Person -100 has two suspension records - both in the past
    testPersonForavailabilityAsFosterCarer("-100", true);

    // case 2: Person -200 has two suspension records - both in the future
    testPersonForavailabilityAsFosterCarer("-200", true);

    // case 3: Person -300 has two suspension records - one in the past and one in the future
    testPersonForavailabilityAsFosterCarer("-300", true);

    // case 4: Person -400 has two suspension records - one in the past and one open
    testPersonForavailabilityAsFosterCarer("-400", false);

    // case 5: Person -500 has two suspension records - one in the past and one ends in future
    testPersonForavailabilityAsFosterCarer("-500", false);

    // case 6 : Person -600 has no suspensions
    testPersonForavailabilityAsFosterCarer("-600", true);

  }

  /**
   * Tests and verifies whether a person is available as a foster carer.
   * 
   * @param personId
   * @param expectedIsAvailableAsFosterCarer
   * @throws Exception
   */
  private void testPersonForavailabilityAsFosterCarer(String personId, boolean expectedIsAvailableAsFosterCarer)
      throws Exception {
    getMockMvc().perform(
        get(getRootURL() + "/person/" + personId)
            .accept(getMIMETypeFromVOClass(MediaType.APPLICATION_JSON, PersonDetailsVO.class)))
        .andExpect(status().isOk())
        .andExpect(content()
            .contentTypeCompatibleWith(getMIMETypeFromVOClass(MediaType.APPLICATION_JSON, PersonDetailsVO.class)))
        .andExpect(jsonPath("_type").value("PersonDetails"))
        .andExpect(jsonPath("isAvailableAsFosterCarer").value(expectedIsAvailableAsFosterCarer));
  }

}
