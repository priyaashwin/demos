// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::TeamAdministrationViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.TeamAdministrationViewController
 */

public class TeamAdministrationViewControllerTest
    extends TeamAdministrationViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.TeamAdministrationViewControllerTest#testViewTeam()
   */
  protected void handleTestViewTeam() throws Exception {
    getMockMvc().perform(
        get("/admin/team/list").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.team.page"))
        .andExpect(model().attribute("defaultSecurityDomainCode", notNullValue()))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewTeams() throws Exception {
    getMockMvc().perform(
        get("/admin/team/list/teams").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.team.page"))
        .andExpect(model().attribute("defaultSecurityDomainCode", notNullValue()))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewProfiles() throws Exception {
    getMockMvc().perform(
        get("/admin/team/list/profiles").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.team.page"))
        .andExpect(model().attribute("defaultSecurityDomainCode", notNullValue()))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewDomains() throws Exception {
    getMockMvc().perform(
        get("/admin/team/list/domains").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.team.page"))
        .andExpect(model().attribute("defaultSecurityDomainCode", notNullValue()))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewAccessLevels() throws Exception {
    getMockMvc().perform(
        get("/admin/team/list/profile/-99/accessLevels").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.team.page"))
        .andExpect(model().attribute("defaultSecurityDomainCode", notNullValue()))
        .andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewDomainContents() throws Exception {
    getMockMvc().perform(
        get("/admin/team/list/domain/-99/contents").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.team.page"))
        .andExpect(model().attribute("defaultSecurityDomainCode", notNullValue()))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
