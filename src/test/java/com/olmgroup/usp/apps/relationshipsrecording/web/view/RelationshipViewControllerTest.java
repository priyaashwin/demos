// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::RelationshipViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.RelationshipViewController
 */

@WithUserDetails("super")
public class RelationshipViewControllerTest extends RelationshipViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.RelationshipViewControllerTest#testPersonRelationships()
   */
  protected void handleTestPersonRelationships() throws Exception {
    getMockMvc().perform(get("/relationships/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestPersonRelationshipsDiagram() throws Exception {
    getMockMvc().perform(get("/relationships/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());

  }

  @Override
  protected void handleTestOrganisationRelationships() throws Exception {
    getMockMvc().perform(get("/relationships/organisation?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("organisation"))
        .andExpect(model().attribute("organisation", not(nullValue())))
        .andExpect(model().attributeExists("subjectType"))
        .andExpect(model().attribute("subjectType", equalTo("organisation")))
        .andExpect(view().name("organisationrelationships"))
        .andExpect(status().isOk());

  }

  // Add additional test cases here
}
