// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::OrganisationViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OrganisationViewController
 */

@WithUserDetails("super")
public class OrganisationViewControllerTest extends OrganisationViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OrganisationViewControllerTest#testGetOrganisation()
   */
  protected final void handleTestGetOrganisation() throws Exception {
    getMockMvc().perform(get("/organisation/?id=-10")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("organisation"))
        .andExpect(model().attribute("organisation", hasProperty("id", equalTo(-10L))))
        .andExpect(model().attributeExists("organisationContacts"))
        .andExpect(view().name("organisation"))
        .andExpect(model().attributeExists("subjectType"))
        .andExpect(model().attribute("subjectType", equalTo(SubjectType.ORGANISATION.value().toLowerCase())))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
