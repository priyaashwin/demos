// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ApprovalsViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ApprovalsViewController
 */

public class ApprovalsViewControllerTest
    extends ApprovalsViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ApprovalsViewControllerTest#testApprovalsView()
   */
  // protected void handleTestApprovalsView() throws Exception
  // {
  // getMockMvc().perform(get("/approvals/person?id=-1")
  // .accept(MediaType.TEXT_HTML))
  // .andExpect(status().isOk());
  //
  // }

  @Override
  protected void handleTestViewPersonApprovals() throws Exception {
    getMockMvc().perform(get("/approvals/person/-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewPersonApprovalsTab() throws Exception {
    getMockMvc().perform(get("/approvals/person/-1/training")
        .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("approvals"))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
