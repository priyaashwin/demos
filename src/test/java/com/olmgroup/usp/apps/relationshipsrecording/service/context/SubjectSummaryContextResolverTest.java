package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import javax.inject.Named;

@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development" })
public class SubjectSummaryContextResolverTest extends AbstractUnifiedTestBase {

  @Inject
  @Named("subjectSummaryContextResolver")
  private SubjectSummaryContextResolver subjectSummaryContextResolver;

  @Before
  public void setUp() {
    login("admin", "admin123");
  }

  @Test
  public void testDefaultContext() throws Exception {
    assertThat(subjectSummaryContextResolver.resolveSubjectSummaryContext(),
        equalTo(SubjectSummaryContext.DEFAULT));
  }

  @Test
  public void testScottishSubjectSummaryContext() throws Exception {
    usp().getContextAttributesHandler().set(SubjectSummaryContextResolver.SUBJECT_SUMMARY_CONTEXT,
        SubjectSummaryContext.SCOTTISH.toString());
    assertThat(subjectSummaryContextResolver.resolveSubjectSummaryContext(),
        equalTo(SubjectSummaryContext.SCOTTISH));
  }
  
  @Test
  public void testWelshSubjectSummaryContext() throws Exception {
    usp().getContextAttributesHandler().set(SubjectSummaryContextResolver.SUBJECT_SUMMARY_CONTEXT,
        SubjectSummaryContext.WELSH.toString());
    assertThat(subjectSummaryContextResolver.resolveSubjectSummaryContext(),
        equalTo(SubjectSummaryContext.WELSH));
  }

}
