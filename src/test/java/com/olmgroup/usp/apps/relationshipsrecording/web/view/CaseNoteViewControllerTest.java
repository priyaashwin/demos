// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::CaseNoteViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.CaseNoteViewController
 */

@WithUserDetails(value = "manager")
public class CaseNoteViewControllerTest extends CaseNoteViewControllerTestBase {

  @SuppressWarnings("unused")
  private static final Logger LOGGER = LoggerFactory.getLogger(CaseNoteViewControllerTest.class);

  @Override
  protected void handleTestPersonCaseNoteRecord() throws Exception {
    getMockMvc().perform(
        get("/casenote/person?id=-500")
            .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestGroupCaseNoteRecord() throws Exception {
    getMockMvc().perform(
        get("/casenote/group?id=-1")
            .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  /**
   * Test that an AccessDeniedException is thrown when CaseNotes's are
   * requested for a subject that the authenticated user does not have
   * relational access to.
   */
  @Test
  @DatabaseSetup(value = {
      "/dbunit/CaseNoteViewController/CaseNoteViewController.setup.xml",
      "/dbunit/CaseNoteViewController/personCaseNoteRecord.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @WithUserDetails(value = "worker")
  public void testPersonCaseNoteRecord_userDeniedAccessToSubject()
      throws Exception {
    getMockMvc().perform(
        get("/casenote/person?id=-500")
            .accept(MediaType.TEXT_HTML))
        .andExpect(status().is3xxRedirection());
  }

  /**
   * Test that an AccessDeniedException is thrown when CaseNotes's are
   * requested for a subject that the authenticated user does not have
   * relational access to.
   */
  @Test
  @DatabaseSetup(value = {
      "/dbunit/CaseNoteViewController/CaseNoteViewController.setup.xml",
      "/dbunit/CaseNoteViewController/groupCaseNoteRecord.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @WithUserDetails(value = "worker")
  public void testGroupCaseNoteRecord_userDeniedAccessToSubject()
      throws Exception {

    usp().getRelationalSecurityChecker().defineGroupMember(-500L, new SubjectIdTypeVO(1L, SubjectType.PERSON));
    getMockMvc().perform(
        get("/casenote/group?id=-500").accept(MediaType.TEXT_HTML))
        .andExpect(status().is3xxRedirection());
  }

  @Override
  protected void handleTestOrganisationCaseNoteRecord() throws Exception {
    // /dbunit/CaseNoteViewController/CaseNoteViewController.setup.xml
    // /dbunit/CaseNoteViewController/organisationCaseNoteRecord.setup.xml
    getMockMvc().perform(
        get("/casenote/organisation?id=-1")
            .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("organisation.casenote"))
        .andExpect(status().isOk());

  }

}
