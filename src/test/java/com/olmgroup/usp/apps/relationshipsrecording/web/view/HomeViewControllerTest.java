// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::HomeViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.HomeViewController
 */

@WithUserDetails("super")
public class HomeViewControllerTest
    extends HomeViewControllerTestBase {
  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.HomeViewControllerTest#testChecklists()
   */
  protected void handleTestChecklists() throws Exception {
    getMockMvc().perform(get("/home/checklists").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.checklists"))
        .andExpect(status().isOk());
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.HomeViewControllerTest#testAllocation()
   */
  protected void handleTestAllocation() throws Exception {
    getMockMvc().perform(get("/home/allocation").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.allocation"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestSummaryChecklists() throws Exception {
    getMockMvc().perform(get("/home/checklists/summary").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.checklists"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestOwnChecklists() throws Exception {
    getMockMvc().perform(get("/home/checklists/own").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.checklists"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestTeamChecklists() throws Exception {
    getMockMvc().perform(get("/home/checklists/team").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.checklists"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestTeamMembersChecklists() throws Exception {
    getMockMvc().perform(get("/home/checklists/members").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.checklists"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestForms() throws Exception {
    getMockMvc().perform(get("/home/authorisation/forms").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.authorisation"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestAuthorisation() throws Exception {
    getMockMvc().perform(get("/home/authorisation").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.authorisation"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestWarnings() throws Exception {
    getMockMvc().perform(get("/home/authorisation/warnings").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.authorisation"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestDashboard() throws Exception {
    getMockMvc().perform(get("/home/dashboard").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("home.dashboard"))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
