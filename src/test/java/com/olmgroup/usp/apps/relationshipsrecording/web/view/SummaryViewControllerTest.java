// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::PersonSummaryViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonSummaryViewController
 */

@WithUserDetails("super")
public class SummaryViewControllerTest extends SummaryViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SummaryViewControllerTest#testPersonSummaryView()
   */
  @Override
  protected void handleTestPersonSummaryView() throws Exception {
    getMockMvc().perform(get("/summary/person/?id=-10")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-10L))))
        .andExpect(model().attribute("childProtectionClassificationGroupPath", equalTo("CP->CP")))
        .andExpect(model().attribute("childProtectionClassificationPathSeparator", equalTo("->")))
        // childProtectionSummaryEnabled not enabled for non-scottish context
        .andExpect(model().attribute("childProtectionSummaryEnabled", equalTo(null)))
        .andExpect(view().name("personSummary"))
        .andExpect(status().isOk());
  }
  
  @WithUserDetails("welshUser")
  @Test
  @DatabaseSetup(
      value = { "/dbunit/SummaryViewController/SummaryViewController.setup.xml",
          "/dbunit/SummaryViewController/personSummaryView.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testWelshPersonSummaryView() throws Exception {
    getMockMvc().perform(get("/summary/person/?id=-10")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-10L))))
        .andExpect(model().attribute("childProtectionClassificationGroupPath", equalTo("CPR->CPR_CPREGISTER")))
        .andExpect(model().attribute("childProtectionClassificationPathSeparator", equalTo("->")))
        // childProtectionSummaryEnabled should be enabled
        .andExpect(model().attribute("childProtectionSummaryEnabled", equalTo(true)))
        .andExpect(view().name("personSummary"))
        .andExpect(status().isOk());
  }

  @WithUserDetails("scottishUser")
  @Test
  @DatabaseSetup(
      value = { "/dbunit/SummaryViewController/SummaryViewController.setup.xml",
          "/dbunit/SummaryViewController/personSummaryView.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testScottishPersonSummaryView() throws Exception {
    getMockMvc().perform(get("/summary/person/?id=-10")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-10L))))
        .andExpect(model().attribute("childProtectionClassificationGroupPath", equalTo("CP->CP")))
        .andExpect(model().attribute("childProtectionClassificationPathSeparator", equalTo("->")))
        // childProtectionSummaryEnabled should be enabled
        .andExpect(model().attribute("childProtectionSummaryEnabled", equalTo(true)))
        .andExpect(view().name("personSummary"))
        .andExpect(status().isOk());
  }

  @WithUserDetails("scottishRestricted")
  @Test
  @DatabaseSetup(
      value = { "/dbunit/SummaryViewController/SummaryViewController.setup.xml",
          "/dbunit/SummaryViewController/personSummaryView.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testScottishPersonSummaryViewNoAccess() throws Exception {
    getMockMvc().perform(get("/summary/person/?id=-10")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-10L))))
        .andExpect(model().attribute("childProtectionClassificationGroupPath", equalTo("CP->CP")))
        .andExpect(model().attribute("childProtectionClassificationPathSeparator", equalTo("->")))
        // childProtectionSummaryEnabled should not be enabled
        .andExpect(model().attribute("childProtectionSummaryEnabled", equalTo(null)))
        .andExpect(view().name("personSummary"))
        .andExpect(status().isOk());
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SummaryViewControllerTest#testGroupSummaryView()
   */
  @Override
  protected void handleTestGroupSummaryView() throws Exception {
    getMockMvc().perform(get("/summary/group/?id=-20")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("group"))
        .andExpect(model().attribute("group", hasProperty("id", equalTo(-20L))))
        .andExpect(view().name("groupSummary"))
        .andExpect(status().isOk());
  }
  
  // Add additional test cases here
}
