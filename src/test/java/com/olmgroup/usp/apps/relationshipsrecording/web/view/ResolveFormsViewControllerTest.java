// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ResolveFormsViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ResolveFormsViewController
 */

@WithUserDetails("super")
public class ResolveFormsViewControllerTest extends ResolveFormsViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ResolveFormsViewControllerTest#testViewResolveForms()
   */
  protected void handleTestViewResolveForms() throws Exception {
    // Nothing useful can be tested here.
  }

  @Override
  protected void handleTestViewPlacementReport() throws Exception {
    getMockMvc().perform(get("/forms/resolve/place/-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-10L))))
        .andExpect(view().name("prform.view"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewCPRA() throws Exception {
    getMockMvc().perform(get("/forms/resolve/cpra/-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-12L))))
        .andExpect(view().name("cpraform.view"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewIntake() throws Exception {
    getMockMvc().perform(get("/forms/resolve/intake/-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("intakeform.view"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewNonAuthorisedIntake() throws Exception {
    // Nothing useful can be tested here.
  }

  @Override
  protected void handleTestViewNonAuthorisedPlacementReport() throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  protected void handleTestViewNonAuthorisedCPRA() throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  protected void handleTestViewPlacementReportTab() throws Exception {
    getMockMvc().perform(get("/forms/resolve/place/-1/background")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-10L))))
        .andExpect(view().name("prform.view"))
        .andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewIntakeTab() throws Exception {
    getMockMvc().perform(get("/forms/resolve/intake/-1/background")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("intakeform.view"))
        .andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewCPRATab() throws Exception {
    getMockMvc().perform(get("/forms/resolve/cpra/-1/background")
        .accept(MediaType.TEXT_HTML))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-12L))))
        .andExpect(view().name("cpraform.view"))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
