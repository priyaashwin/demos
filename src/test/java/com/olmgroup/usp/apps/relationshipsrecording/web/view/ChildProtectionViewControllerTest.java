// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ChildProtectionViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildProtectionViewController
 */

public class ChildProtectionViewControllerTest
    extends ChildProtectionViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildProtectionViewControllerTest#testViewChildProtection()
   */
  protected void handleTestViewChildProtection() throws Exception {
    getMockMvc().perform(get("/childprotection/person?id=-500")
        .accept(MediaType.TEXT_HTML))
        .andExpect(view().name("childprotection"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-500L))));
  }

  // Add additional test cases here
}
