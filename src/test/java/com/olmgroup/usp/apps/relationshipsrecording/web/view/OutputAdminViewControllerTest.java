// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::OutputAdminViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OutputAdminViewController
 */
@WithUserDetails("super")
public class OutputAdminViewControllerTest
    extends OutputAdminViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OutputAdminViewControllerTest#testViewOutputs()
   */
  protected void handleTestViewOutputs() throws Exception {
    getMockMvc().perform(get("/admin/outputs").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewCaseNoteOutputs() throws Exception {
    getMockMvc().perform(get("/admin/outputs/casenote").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.outputs"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewChronologyOutputs() throws Exception {
    getMockMvc().perform(get("/admin/outputs/chronology").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.outputs"))
        .andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewFormOutputs() throws Exception {
    getMockMvc().perform(get("/admin/outputs/form").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.outputs"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewPersonOutputs() throws Exception {
    getMockMvc().perform(get("/admin/outputs/person").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.outputs"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewPersonLetterOutputs() throws Exception {
    getMockMvc().perform(get("/admin/outputs/personLetter").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.outputs"))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewChildLookedAfterOutputs() throws Exception {
    getMockMvc().perform(get("/admin/outputs/childLookedAfter").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.outputs"))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
