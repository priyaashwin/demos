// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::FormsViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsViewController
 */
@WithUserDetails("super")
public class FormsViewControllerTest extends FormsViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsViewControllerTest#testViewPersonForms()
   */
  protected void handleTestViewPersonForms() throws Exception {
    getMockMvc().perform(get("/forms/person/-1/view")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("forms"));
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsViewControllerTest#testViewGroupForms()
   */
  protected void handleTestViewGroupForms() throws Exception {
    getMockMvc().perform(get("/forms/group/-10/view")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("group"))
        .andExpect(model().attribute("group", hasProperty("id", equalTo(-10L))))
        .andExpect(view().name("forms"));
  }

  @Override
  protected void handleTestViewAddPersonForm() throws Exception {
    getMockMvc().perform(get("/forms/person/-1/add")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("forms"));
  }

  @Override
  protected void handleTestViewAddGroupForm() throws Exception {
    getMockMvc().perform(get("/forms/group/-10/add")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("group"))
        .andExpect(model().attribute("group", hasProperty("id", equalTo(-10L))))
        .andExpect(view().name("forms"));
  }

  @Override
  protected void handleTestViewFormReviews() throws Exception {
    getMockMvc().perform(get("/forms/reviews?id=-11")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("formId"))
        .andExpect(model().attribute("formId", equalTo(-11L)))
        .andExpect(model().attributeExists("subjectType"))
        .andExpect(model().attribute("subjectType", equalTo("person")))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("form.reviews"));
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/FormsViewController/FormsViewController.setup.xml", "/dbunit/FormsViewController/viewFormReviewsGroup.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testViewFormReviews_Groups() throws Exception {
    getMockMvc().perform(get("/forms/reviews?id=-22")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("formId"))
        .andExpect(model().attribute("formId", equalTo(-22L)))
        .andExpect(model().attributeExists("subjectType"))
        .andExpect(model().attribute("subjectType", equalTo("group")))
        .andExpect(model().attributeExists("group"))
        .andExpect(model().attribute("group", hasProperty("id", equalTo(-212L))))
        .andExpect(view().name("form.reviews"));
  }

  @Override
  protected void handleTestViewForm() throws Exception {
    getMockMvc().perform(get("/forms/load/?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
