// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ChronologyViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChronologyViewController
 */
@WithUserDetails("manager")
public class ChronologyViewControllerTest extends
    ChronologyViewControllerTestBase {
  @SuppressWarnings("unused")
  private static final Logger LOGGER = LoggerFactory.getLogger(ChronologyViewControllerTest.class);

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChronologyViewControllerTest#testPersonChronology()
   */
  protected void handleTestPersonChronology() throws Exception {
    getMockMvc().perform(
        get("/chronology/person?id=-10").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestGroupChronology() throws Exception {
    getMockMvc().perform(
        get("/chronology/group?id=-500").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());

  }

  /**
   * Test that an AccessDeniedException is thrown when ChronologyEntry's are
   * requested for a subject that the authenticated user does not have
   * relational access to.
   */
  @Test
  @DatabaseSetup(value = {
      "/dbunit/ChronologyViewController/ChronologyViewController.setup.xml",
      "/dbunit/ChronologyViewController/personChronology.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @WithUserDetails("worker")
  public void testPersonChronology_userDeniedAccessToSubject()
      throws Exception {
    getMockMvc().perform(
        get("/chronology/person?id=-10").accept(MediaType.TEXT_HTML))
        .andExpect(status().is3xxRedirection());
  }

  /**
   * Test that an AccessDeniedException is thrown when ChronologyEntry's are
   * requested for a subject that the authenticated user does not have
   * relational access to.
   */
  @Test
  @DatabaseSetup(value = {
      "/dbunit/ChronologyViewController/ChronologyViewController.setup.xml",
      "/dbunit/ChronologyViewController/groupChronology.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @WithUserDetails("worker")
  public void testGroupChronology_userDeniedAccessToSubject()
      throws Exception {

    usp().getRelationalSecurityChecker().defineGroupMember(-500L, new SubjectIdTypeVO(1L, SubjectType.PERSON));

    getMockMvc().perform(
        get("/chronology/group?id=-500").accept(MediaType.TEXT_HTML))
        .andExpect(status().is3xxRedirection());
  }
}
