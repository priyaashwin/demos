// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::FormsAdminViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsAdminViewController
 */

@WithUserDetails("super")
public class FormsAdminViewControllerTest extends FormsAdminViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsAdminViewControllerTest#testViewMappings()
   */
  protected void handleTestViewMappings() throws Exception {
    getMockMvc().perform(get("/admin/forms/mappings?formDefinitionId=-1").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsAdminViewControllerTest#testViewType()
   */
  protected void handleTestViewTypes() throws Exception {
    getMockMvc().perform(get("/admin/forms").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  // Add additional test cases here
}
