// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    security/VOSecurityHandlerTestImpl.vsl in usp-spring-cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::vo::OrganisationPersonOrganisationRelationshipsResultVO
 * STEREOTYPE:  ValueObject
 */
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.olmgroup.usp.apps.relationshipsrecording.vo.OrganisationPersonOrganisationRelationshipsResultVO;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
public class OrganisationPersonOrganisationRelationshipsResultVOSecurityHandlerTest
    extends OrganisationPersonOrganisationRelationshipsResultVOSecurityHandlerTestBase {

  private static final long NOT_ALLOCATED_RELATIONSHIP_ID = -2L;
  private static final long PERSON_ID = -1L;
  private static final long NOT_ALLOCATED_ORGANISATION_ID = -2L;

  @Before
  public final void handleInitializeTestSuite() {
    login("admin", "admin123");
  }

  private OrganisationPersonOrganisationRelationshipsResultVO getTargetDomainObject(long targetId) {
    final OrganisationPersonOrganisationRelationshipsResultVO vo = new OrganisationPersonOrganisationRelationshipsResultVO();
    vo.setId(targetId);
    return vo;
  }

  @Override
  protected final void handleTestSupportsRelationalSecurityByTargetDomainObject()
      throws Exception {

    OrganisationPersonOrganisationRelationshipsResultVO targetDomainObject = getTargetDomainObject(
        NOT_ALLOCATED_RELATIONSHIP_ID);

    assertThat(
        "Expected security handler to support relational security",
        this.getOrganisationPersonOrganisationRelationshipsResultVOSecurityHandler()
            .supportsRelationalSecurity(targetDomainObject),
        equalTo(true));
  }

  @Override
  protected final void handleTestGetSubjectsByTargetDomainObject()
      throws Exception {
    final OrganisationPersonOrganisationRelationshipsResultVO targetDomainObject = getTargetDomainObject(
        NOT_ALLOCATED_RELATIONSHIP_ID);

    final Set<SubjectIdTypeVO> subjects = this
        .getOrganisationPersonOrganisationRelationshipsResultVOSecurityHandler().getSubjects(
            targetDomainObject);

    final SubjectIdTypeVO personSubject = new SubjectIdTypeVO(PERSON_ID,
        SubjectType.PERSON);
    final SubjectIdTypeVO organisationSubject = new SubjectIdTypeVO(NOT_ALLOCATED_ORGANISATION_ID,
        SubjectType.ORGANISATION);

    assertThat("Unexpected number of subjects", subjects.size(), equalTo(2));
    assertThat("Expected subjects contain role A person",
        subjects.contains(personSubject), equalTo(true));
    assertThat("Expected subjects contain role B person",
        subjects.contains(organisationSubject), equalTo(true));
  }

}