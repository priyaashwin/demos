// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::CalendarAdminViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.CalendarAdminViewController
 */

@WithUserDetails("super")
public class CalendarAdminViewControllerTest extends com.olmgroup.usp.apps.relationshipsrecording.web.view.CalendarAdminViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.CalendarAdminViewControllerTest#testViewCalendars()
   */
  protected void handleTestViewCalendars() throws Exception {
    getMockMvc().perform(
        get("/admin/calendar").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewDays() throws Exception {
    getMockMvc().perform(
        get("/admin/calendar/days?calendarId=-1").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewHolidays() throws Exception {
    getMockMvc().perform(
        get("/admin/calendar/holidays").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewHolidayEvents() throws Exception {
    getMockMvc().perform(
        get("/admin/calendar/holidays/events?holidayId=-1").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());

  }

  // Add additional test cases here
}
