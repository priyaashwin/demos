// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.ModelAndView;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildCareLeaverViewController
 */
public class ChildCareLeaverViewControllerTest extends ChildCareLeaverViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildCareLeaverViewControllerTest#testViewCareLeaver()
   */
  protected void handleTestViewCareLeaver() throws Exception {
    final ModelAndView model = getMockMvc().perform(get("/careleaver/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andReturn()
        .getModelAndView();

    assertThat(model.getModel().keySet(), hasItem("canAddCareLeaverDetails"));
    assertThat(model.getModel().keySet(), hasItem("canAddAfterCareDetails"));
    assertThat(model.getModel().keySet(), hasItem("careLeaverDefault"));
    assertThat(model.getModel().keySet(), hasItem("careLeaverAfterCare"));

    assertThat("Should be able to add care leaver details", (Boolean) model.getModel().get("canAddCareLeaverDetails"),
        is(true));
    assertThat("Should be not able to add after care details", (Boolean) model.getModel().get("canAddAfterCareDetails"),
        is(false));
    assertThat("Should be default care leaver mode", (Boolean) model.getModel().get("careLeaverDefault"), is(true));
    assertThat("Should not be after care mode", (Boolean) model.getModel().get("careLeaverAfterCare"), is(false));
  }
 
  @Override
  protected void handleTestViewAfterCare() {
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/ChildCareLeaverViewController/ChildCareLeaverViewController.setup.xml", "/dbunit/ChildCareLeaverViewController/viewAfterCare.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @WithUserDetails("scottishUser")
  public void testViewAfterCare_ScottishContext() throws Exception {
    final ModelAndView model = getMockMvc().perform(get("/aftercare/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andReturn()
        .getModelAndView();

    assertThat(model.getModel().keySet(), hasItem("canAddCareLeaverDetails"));
    assertThat(model.getModel().keySet(), hasItem("canAddAfterCareDetails"));
    assertThat(model.getModel().keySet(), hasItem("careLeaverDefault"));
    assertThat(model.getModel().keySet(), hasItem("careLeaverAfterCare"));
    assertThat(model.getModel().keySet(), hasItem("afterCareEligibility"));

    assertThat("Should not be able to add care leaver details",
        (Boolean) model.getModel().get("canAddCareLeaverDetails"), is(false));
    assertThat("Should be able to add after care details", (Boolean) model.getModel().get("canAddAfterCareDetails"),
        is(true));
    assertThat("Should not be default care leaver mode", (Boolean) model.getModel().get("careLeaverDefault"),
        is(false));
    assertThat("Should be after care mode", (Boolean) model.getModel().get("careLeaverAfterCare"), is(true));
    assertThat("Incorrect eligibility", model.getModel().get("afterCareEligibility").toString(), is("DISCRETIONARY"));

  }
  
  @Test
  @DatabaseSetup(
      value = { "/dbunit/ChildCareLeaverViewController/ChildCareLeaverViewController.setup.xml", "/dbunit/ChildCareLeaverViewController/viewAfterCare.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @WithUserDetails("welshUser")
  public void testViewAfterCare_WelshContext() throws Exception {
    final ModelAndView model = getMockMvc().perform(get("/careleaver/person?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andReturn()
        .getModelAndView();
    
    assertThat(model.getModel().keySet(), hasItem("canAddCareLeaverDetails"));
    assertThat(model.getModel().keySet(), hasItem("canAddAfterCareDetails"));
    assertThat(model.getModel().keySet(), hasItem("careLeaverDefault"));
    assertThat(model.getModel().keySet(), hasItem("careLeaverAfterCare"));
    assertThat(model.getModel().keySet(), hasItem("careLeaverWelsh"));

    assertThat("Should be able to add care leaver details", (Boolean) model.getModel().get("canAddCareLeaverDetails"),
        is(true));
    assertThat("Should be not able to add after care details", (Boolean) model.getModel().get("canAddAfterCareDetails"),
        is(false));
    assertThat("Should not be default care leaver mode", (Boolean) model.getModel().get("careLeaverDefault"), is(false));
    assertThat("Should be Welsh care leaver mode", (Boolean) model.getModel().get("careLeaverWelsh"), is(true));    
    assertThat("Should not be after care mode", (Boolean) model.getModel().get("careLeaverAfterCare"), is(false));
  }

  // Add additional test cases here
}
