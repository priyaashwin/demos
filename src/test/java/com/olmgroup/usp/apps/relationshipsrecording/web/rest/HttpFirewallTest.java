package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Verifies that certain URLs can pass the Spring HTTP firewall
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development" })
@WithUserDetails
public class HttpFirewallTest extends AbstractUnifiedTestBase {

  /**
   * Verifies that % is accepted (if it is safe-encoded) by Spring's
   * HTTP firewall. This is signalled by a 404 response.
   * If the Spring Firewall is in operation then we would encounter
   * a 500 response instead.
   */
  @Test
  public final void testUrlWithEncodedPercent() throws Exception {
    getMockMvc().perform(
        get("/gazetteer-provider%getaddress:TR19 7AA:1216701749")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
}
