// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::SecurityUserAdminViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SecurityUserAdminViewController
 */
public class SecurityUserAdminViewControllerTest
    extends SecurityUserAdminViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SecurityUserAdminViewControllerTest#testViewUsers()
   */
  protected void handleTestViewUsers() throws Exception {
    getMockMvc().perform(
        get("/admin/users/list").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk());
  }

  @Test
  public void testViewUsers_APPURL() throws Exception {
    getMockMvc()
        .perform(get("/admin/users/list").accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("appLoginURL"));
  }

  // Add additional test cases here
}
