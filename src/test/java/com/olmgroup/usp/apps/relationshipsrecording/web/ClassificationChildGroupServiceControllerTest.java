// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringServiceControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: $controller.validationName
 * STEREOTYPE:  Service
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.components.classification.vo.ClassificationGroupVO;
import com.olmgroup.usp.components.classification.web.ClassificationGroupServiceController;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

/**
 * @see ClassificationGroupServiceController
 */

@WithUserDetails("super")
public class ClassificationChildGroupServiceControllerTest extends ClassificationChildGroupServiceControllerTestBase {

  @Override
  protected void handleTestAddClassificationGroup() throws Exception {

    // Add a casenote entry

    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("code", "CLAS1");
    classificationGroup.put("name", "Classification1");
    classificationGroup.put("mutuallyExclusive", false);
    classificationGroup.put("parentGroupId", -1L);

    final String content = classificationGroup.toString();
    final MvcResult result = getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andDo(print()).andReturn();

    final String myContent = result.getResponse().getContentAsString();

    final Integer idGenerated = Integer.parseInt(myContent);

    getMockMvc()
        .perform(
            get(
                getRootURL()
                    + "/classificationGroup/"
                        .concat(idGenerated.toString()))
                            .accept(getMIMETypeFromVOClass(
                                MediaType.APPLICATION_JSON,

                                ClassificationGroupVO.class)))
        .andExpect(status().isOk())
        .andExpect(
            content().contentTypeCompatibleWith(
                getMIMETypeFromVOClass(
                    MediaType.APPLICATION_JSON,
                    ClassificationGroupVO.class)))
        .andExpect(jsonPath("id").value(idGenerated));

  }

  @Test
  public void testAddClassificationGroupCodeIsMandatory() throws Exception {
    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("name", "Classification1");
    classificationGroup.put("mutuallyExclusive", false);

    final String content = classificationGroup.toString();
    getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(
            jsonPath("validationErrors.code")
                .value("Code is mandatory."));
  }

  @Test
  public void testAddClassificationGroupNameIsMandatory() throws Exception {
    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("code", "CLAS1");
    classificationGroup.put("mutuallyExclusive", false);
    classificationGroup.put("parentGroupId", -1L);

    final String content = classificationGroup.toString();
    getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(
            jsonPath("validationErrors.name")
                .value("Name is mandatory."));
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddClassificationGroupCodeIsEmpty() throws Exception {

    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("name", "Classification1");
    classificationGroup.put("code", "");
    classificationGroup.put("mutuallyExclusive", false);
    classificationGroup.put("parentGroupId", -1L);

    final String content = classificationGroup.toString();
    getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(
            jsonPath("validationErrors.code")
                .value("Code is mandatory."));

  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddClassificationGroupCodeIsWithSpaces() throws Exception {

    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("name", "Classification1");
    classificationGroup.put("code", "   ");
    classificationGroup.put("mutuallyExclusive", false);
    classificationGroup.put("parentGroupId", -1L);

    final String content = classificationGroup.toString();
    getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(
            jsonPath("validationErrors.code")
                .value("Code is mandatory."));

  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddClassificationGroupCodeIsNotAlphanumeric() throws Exception {
    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("name", "Classification1");
    classificationGroup.put("code", "CL-1");
    classificationGroup.put("mutuallyExclusive", false);
    classificationGroup.put("parentGroupId", -1L);

    final String content = classificationGroup.toString();
    getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(
            jsonPath("validationErrors.code")
                .value(
                    "Sorry, there was a problem with the entered Code. Code must contain uppercase characters,0-9 and be a maximum of 100 characters in length"));
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddClassificationGroupCode_CodeTooLong() throws Exception {

    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("name", "Classification1");
    classificationGroup.put("code",
        "CLASSIFICATION_CODE_AT_RISK_OF_BEING_A_VICTIM_OF_HARMFUL_BEHAVIOUR_AND_AT_RISK_OF_INITIATING_HARMFUL_BEHAVIOUR");
    classificationGroup.put("mutuallyExclusive", false);
    classificationGroup.put("parentGroupId", -1L);

    final String content = classificationGroup.toString();
    getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(
            jsonPath("validationErrors.code")
                .value(
                    "Sorry, there was a problem with the entered Code. Code must contain uppercase characters,0-9 and be a maximum of 100 characters in length"));

  }

  @Ignore
  @Test
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddClassificationGroupDuplicateCode() throws Exception {

    final JSONObject classificationGroup = new JSONObject();
    classificationGroup.put("_type", "NewClassificationGroup");
    classificationGroup.put("name", "Classification1");
    classificationGroup.put("code", "CG1");
    classificationGroup.put("mutuallyExclusive", false);
    classificationGroup.put("parentGroupId", -1L);

    final String content = classificationGroup.toString();
    getMockMvc()
        .perform(
            post(
                getRootURL()
                    + "/classificationGroup/-1/childGroup")
                        .content(content).contentType(
                            MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("type").value("DataBindException"))
        .andExpect(
            jsonPath("validationErrors.code")
                .value("Duplicate code with the within the same Classification hierarchy detected"));

  }

  // Add additional test cases here
}
