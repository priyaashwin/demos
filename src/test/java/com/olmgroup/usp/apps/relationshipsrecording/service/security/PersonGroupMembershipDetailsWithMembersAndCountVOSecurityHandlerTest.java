// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    security/VOSecurityHandlerTestImpl.vsl in usp-spring-cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::vo::PersonGroupMembershipDetailsWithMembersAndCountVO
 * STEREOTYPE:  ValueObject
 */
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonGroupMembershipDetailsWithMembersAndCountVO;
import com.olmgroup.usp.components.person.service.PersonGroupMembershipService;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandlerTest
    extends
    PersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandlerTestBase {
  @Inject
  PersonGroupMembershipService personGroupMembershipService;

  @Before
  public final void handleInitializeTestSuite() {
    this.login("admin", "admin123");
  }

  @Override
  protected final void handleTestSupportsRelationalSecurityByTargetDomainObject()
      throws Exception {
    final PersonGroupMembershipDetailsWithMembersAndCountVO personGroupMembershipDetailsWithMembersAndCountVO = new PersonGroupMembershipDetailsWithMembersAndCountVO();

    final boolean supportsRelationalSecurityByTargetDomainObject = this
        .getPersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandler()
        .supportsRelationalSecurity(
            personGroupMembershipDetailsWithMembersAndCountVO);

    Assert.assertTrue(supportsRelationalSecurityByTargetDomainObject);
  }

  @Override
  protected final void handleTestGetSubjectsByTargetDomainObject()
      throws Exception {
    final List<PersonGroupMembershipDetailsWithMembersAndCountVO> personGroupMembershipList = this.personGroupMembershipService
        .findByGroupId(-5L, -1, -1, null,
            PersonGroupMembershipDetailsWithMembersAndCountVO.class)
        .getResults();
    final SubjectIdTypeVO expectedPersonSubject = new SubjectIdTypeVO(-1L,
        SubjectType.PERSON);
    final SubjectIdTypeVO expectedGroupSubject = new SubjectIdTypeVO(-5L,
        SubjectType.GROUP);

    final Set<SubjectIdTypeVO> subjects = this
        .getPersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandler()
        .getSubjects(personGroupMembershipList.get(0));

    Assert.assertEquals(2, subjects.size());
    Assert.assertTrue(subjects.contains(expectedPersonSubject));
    Assert.assertTrue(subjects.contains(expectedGroupSubject));
  }

}