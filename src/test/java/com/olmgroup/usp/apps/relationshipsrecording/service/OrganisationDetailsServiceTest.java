// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::OrganisationDetailsService
 * STEREOTYPE:  Service
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeleteOrganisationContactsVO;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeleteOrganisationReferenceNumbersVO;
import com.olmgroup.usp.components.contact.vo.UpdateContactVO;
import com.olmgroup.usp.components.contact.vo.UpdateEmailContactVO;
import com.olmgroup.usp.components.contact.vo.UpdateTelephoneContactVO;
import com.olmgroup.usp.components.contact.vo.UpdateWebContactVO;
import com.olmgroup.usp.components.organisation.exception.ContactForOrganisationNotFoundException;
import com.olmgroup.usp.components.organisation.exception.DuplicateOrganisationReferenceNumberException;
import com.olmgroup.usp.components.organisation.exception.OrganisationNotFoundException;
import com.olmgroup.usp.components.organisation.exception.OrganisationReferenceNumberNotFoundException;
import com.olmgroup.usp.components.organisation.vo.UpdateOrganisationReferenceNumberVO;
import com.olmgroup.usp.facets.configuration.validation.CodedEntryMissingException;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.OrganisationDetailsService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class OrganisationDetailsServiceTest extends
    OrganisationDetailsServiceTestBase {
  @Before
  public final void handleInitializeTestSuite() {
    // TODO add any additional initialization for your test suite here
    login("super", "testpassword");
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.service.OrganisationDetailsServiceTest#testCombinedUpdateDeleteOrganisationContact()
   */
  @Override
  protected final void handleTestCombinedUpdateDeleteOrganisationContact()
      throws Exception {

    // Update Contact -1
    final UpdateEmailContactVO updateEmailContactVO = new UpdateEmailContactVO();
    updateEmailContactVO.setId(-1);
    updateEmailContactVO.setObjectVersion(1);
    updateEmailContactVO.setAddress("olm@newschcool.com");

    final UpdateTelephoneContactVO updateTelephoneContactVO = new UpdateTelephoneContactVO();
    updateTelephoneContactVO.setId(-3);
    updateTelephoneContactVO.setObjectVersion(1);
    updateTelephoneContactVO.setNumber("02058585858");
    updateTelephoneContactVO.setTelephoneType("TELEPHONE");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateEmailContactVO);
    updateList.add(updateTelephoneContactVO);

    // Delete contact -3
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-4));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setUpdateContactVOs(updateList);
    updateDeleteOrganisationContactsVO.setDeleteContactIds(deleteList);

    // set organisation id
    final long organisationId = -1L;

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);

  }

  // Test2 - Update Contact for Organisation -1
  // Invalid contact (-99) is provided.
  // Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForOrganisationNotFoundException.class)
  public final void testUpdateOrganisationContacts_ContactNotExits()
      throws Exception {
    // Update Preference Number -99
    final UpdateContactVO updateContactVO = new UpdateContactVO();
    updateContactVO.setId(-99);
    updateContactVO.setObjectVersion(1);

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateContactVO);

    // Set update list to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setUpdateContactVOs(updateList);

    // set organisation id
    final long organisationId = -1;

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);

  }

  // Test3 - Delete Contact for Organisation -1
  // Invalid contact (-99) is provided.
  // Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForOrganisationNotFoundException.class)
  public final void testDeleteContacts_ContactForOrganisationNotExits()
      throws Exception {
    // Delete contact -99
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setDeleteContactIds(deleteList);

    // set organisation id
    final long organisationId = -1;

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);

  }

  // Test4 - Existing contact record with id:-1, and invalid contact (-99)
  // - Update above records
  // - Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForOrganisationNotFoundException.class)
  public final void testUpdateMultipleContact_ContactforOrganisationNotFoundException()
      throws Exception {
    // First Record
    // Update Contact -1
    UpdateEmailContactVO updateEmailContactVO = new UpdateEmailContactVO();
    updateEmailContactVO.setId(-1);
    updateEmailContactVO.setObjectVersion(1);
    updateEmailContactVO.setAddress("olm@newschcool.com");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateEmailContactVO);

    // Second Record
    updateEmailContactVO = new UpdateEmailContactVO();
    updateEmailContactVO.setId(-99);
    updateEmailContactVO.setObjectVersion(1);

    updateList.add(updateEmailContactVO);

    // Set delete list to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setUpdateContactVOs(updateList);

    // set organisation id
    final long organisationId = -1;

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);

  }

  // Test5 - Existing contact record with id:-2, and invalid contact (-99)
  // - Delete above records
  // - Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForOrganisationNotFoundException.class)
  public final void testDeleteMultipleContact_ContactForOrganisationNotFound()
      throws Exception {
    // Delete reference number -2, and -99
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-2));
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setDeleteContactIds(deleteList);

    // set organisation id
    final long organisationId = -1;

    // call updateDelete service
    final OrganisationDetailsService OrganisationDetailsService = getOrganisationDetailsService();
    OrganisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);
  }

  // Test6 - Existing contact record with id:-1, and invalid contact (-99)
  // - Update existing one, delete invalid one
  // - Operation should raise ContactForOrganisationNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = ContactForOrganisationNotFoundException.class)
  public final void testUpdateDeleteMultipleContact_ContactForOrganisationNotFound()
      throws Exception {
    // First Record
    final UpdateEmailContactVO updateEmailContactVO = new UpdateEmailContactVO();
    updateEmailContactVO.setId(-1);
    updateEmailContactVO.setObjectVersion(1);
    updateEmailContactVO.setAddress("olm@newschcool.com");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateEmailContactVO);

    // Second Record
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setUpdateContactVOs(updateList);
    updateDeleteOrganisationContactsVO.setDeleteContactIds(deleteList);

    // set organisation id
    final long organisationId = -1;

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);
  }

  // Test7 - An existing contact record has id:-4, for an INVALID Organisation
  // id -99
  // - Operation should raise Organisation Not Found Exception.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = OrganisationNotFoundException.class)
  public final void testUpdateContact_OrganisationNotFoundException()
      throws Exception {
    // Update Preference Number -4
    final UpdateWebContactVO updateWebContactVO = new UpdateWebContactVO();
    updateWebContactVO.setId(-4);
    updateWebContactVO.setObjectVersion(1);
    updateWebContactVO.setAddress("my.updatedweb.com");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateWebContactVO);

    // Set update list to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setUpdateContactVOs(updateList);

    // set organisation id
    final long organisationId = -99;

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);
  }

  // Test8 - Try to update a contact with invalid contact type (coded entry)
  // - Operation should be raise CodedEntryMissingException
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationContact.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = CodedEntryMissingException.class)
  public final void testUpdateOrganisationContact_InvalidCodedEntryForContactType()
      throws Exception {

    // Update Preference Number -4
    final UpdateWebContactVO updateWebContactVO = new UpdateWebContactVO();
    updateWebContactVO.setId(-4);
    updateWebContactVO.setObjectVersion(1);
    updateWebContactVO.setContactType("InvalidContactType");

    final List<UpdateContactVO> updateList = new ArrayList<UpdateContactVO>();
    updateList.add(updateWebContactVO);

    // Set update list to UpdateDelete VO
    final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO = new UpdateDeleteOrganisationContactsVO();
    updateDeleteOrganisationContactsVO.setUpdateContactVOs(updateList);

    // set organisation id
    final long organisationId = -1;

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService.combinedUpdateDeleteOrganisationContact(
        updateDeleteOrganisationContactsVO, organisationId);

  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.service.OrganisationDetailsServiceTest#testCombinedUpdateDeleteOrganisationReferenceNumber()
   */
  // Test1 - Update an existing reference number(-1). Operation should be
  // successfull.
  // Delete an existing reference number(-3). Operation should be successfull.

  @Override
  protected void handleTestCombinedUpdateDeleteOrganisationReferenceNumber()
      throws Exception {
    logout();
    login("super", "testpassword");

    // Update Preference Number -1
    final UpdateOrganisationReferenceNumberVO updateRefVO = new UpdateOrganisationReferenceNumberVO();
    updateRefVO.setId(-1);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdateOrganisationReferenceNumberVO> updateList = new ArrayList<UpdateOrganisationReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Delete reference number -3
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-3));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);
    updateDeleteOrganisationReferenceNumbersVO
        .setDeleteOrganisationReferenceNumberIds(deleteList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);

  }

  // Test2 - Update Organisation Reference Number
  // Invalid organisation reference (-99) is provided.
  // Operation should raise OrganisationReferenceNumberNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = OrganisationReferenceNumberNotFoundException.class)
  public final void testUpdateOrganisationReferenceNumbers_OrganisationReferenceNumberNotExits()
      throws Exception {
    logout();
    login("super", "testpassword");

    // Update Preference Number -99
    final UpdateOrganisationReferenceNumberVO updateRefVO = new UpdateOrganisationReferenceNumberVO();
    updateRefVO.setId(-99);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdateOrganisationReferenceNumberVO> updateList = new ArrayList<UpdateOrganisationReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);

  }

  // Test3 - Delete Organisation Reference Number
  // Invalid organisation reference (-99) is provided.
  // Operation should raise OrganisationReferenceNumberNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = OrganisationReferenceNumberNotFoundException.class)
  public final void testDeleteOrganisationReferenceNumbers_OrganisationReferenceNumberNotExits()
      throws Exception {
    logout();
    login("super", "testpassword");

    // Delete reference number -3
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setDeleteOrganisationReferenceNumberIds(deleteList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);

  }

  // Test4 - Existing Reference number record with id:-2, and invalid
  // organisation reference (-99)
  // - Update above records
  // - Operation should raise OrganisationReferenceNumberNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/OrganisationDetailsService.setup.xml",
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = OrganisationReferenceNumberNotFoundException.class)
  public final void testUpdateMultipleOrganisationReferenceNumber_OrganisationReferenceNumberNotFound()
      throws Exception {
    logout();
    login("super", "testpassword");

    // First Record
    UpdateOrganisationReferenceNumberVO updateRefVO = new UpdateOrganisationReferenceNumberVO();
    updateRefVO.setId(-2);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdateOrganisationReferenceNumberVO> updateList = new ArrayList<UpdateOrganisationReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Second Record
    updateRefVO = new UpdateOrganisationReferenceNumberVO();
    updateRefVO.setId(-99);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999955");

    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);
  }

  // Test5 - Existing Reference number record with id:-2, and invalid
  // organisation reference (-99)
  // - Delete above records
  // - Operation should raise OrganisationReferenceNumberNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = OrganisationReferenceNumberNotFoundException.class)
  public final void testDeleteMultipleOrganisationReferenceNumber_OrganisationReferenceNumberNotFound()
      throws Exception {
    logout();
    login("super", "testpassword");

    // Delete reference number -2, and -99
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-2));
    deleteList.add(new Long(-99));

    // Set delete list to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setDeleteOrganisationReferenceNumberIds(deleteList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);
  }

  // Test6 - Existing Reference number record with id:-2, and invalid
  // organisation reference (-99)
  // - Update existing one, delete invalid one
  // - Operation should raise OrganisationReferenceNumberNotFoundException.
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/OrganisationDetailsService.setup.xml",
      "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = OrganisationReferenceNumberNotFoundException.class)
  public final void testUpdateDeleteMultipleOrganisationReferenceNumber_OrganisationReferenceNumberNotFound()
      throws Exception {
    logout();
    login("super", "testpassword");

    // First Record
    final UpdateOrganisationReferenceNumberVO updateRefVO = new UpdateOrganisationReferenceNumberVO();
    updateRefVO.setId(-2);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("N111999999");

    final List<UpdateOrganisationReferenceNumberVO> updateList = new ArrayList<UpdateOrganisationReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Second Record
    final List<Long> deleteList = new ArrayList<Long>();
    deleteList.add(new Long(-99));

    // Set both update and delete lists to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);
    updateDeleteOrganisationReferenceNumbersVO
        .setDeleteOrganisationReferenceNumberIds(deleteList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);
  }

  // Test7 - An existing Reference number record has id:-3,
  // ReferenceNumber:N123456789, StartDate:01/01/2014 and EndDate:01/05/2014
  // - Update above record with Reference Number set to blank
  // - Operation should raise ConstraintViolation Exception.

  // @Test(expected=ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = OrganisationDetailsServiceException.class)
  public final void testUpdateOrganisationReferenceNumber_NotBlankifSet()
      throws Exception {
    logout();
    login("super", "testpassword");

    // Update Preference Number -3
    final UpdateOrganisationReferenceNumberVO updateRefVO = new UpdateOrganisationReferenceNumberVO();
    updateRefVO.setId(-3);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setReferenceNumber("");

    final List<UpdateOrganisationReferenceNumberVO> updateList = new ArrayList<UpdateOrganisationReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);
  }

  // Test8 - There are two reference number records for organisation 504
  // id:-2, Type:NI, ReferenceNumber:N123456789, StartDate:01/01/2014 and
  // EndDate:01/05/2014
  // - id:-6, Type:NI, ReferenceNumber:D123456789
  // - Update second record (-6) with StartDate:01/03/2014
  // - Operation should be raise DuplicateOrganisationReferenceNumberException
  @DatabaseSetup(value = { "/dbunit/OrganisationDetailsService/combinedUpdateDeleteOrganisationReferenceNumber.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = DuplicateOrganisationReferenceNumberException.class)
  public final void testUpdateOrganisationReferenceNumber_Overlapping()
      throws Exception {
    logout();
    login("super", "testpassword");

    // Update Preference Number -6
    final UpdateOrganisationReferenceNumberVO updateRefVO = new UpdateOrganisationReferenceNumberVO();
    updateRefVO.setId(-6);
    updateRefVO.setObjectVersion(1);
    updateRefVO.setStartDate(new DateTime(2014, 03, 01, 0, 0).toDate());

    final List<UpdateOrganisationReferenceNumberVO> updateList = new ArrayList<UpdateOrganisationReferenceNumberVO>();
    updateList.add(updateRefVO);

    // Set update list to UpdateDelete VO
    final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO = new UpdateDeleteOrganisationReferenceNumbersVO();
    updateDeleteOrganisationReferenceNumbersVO
        .setUpdateReferenceNumberVOs(updateList);

    // call updateDelete service
    final OrganisationDetailsService organisationDetailsService = getOrganisationDetailsService();
    organisationDetailsService
        .combinedUpdateDeleteOrganisationReferenceNumber(updateDeleteOrganisationReferenceNumbersVO);

  }
}