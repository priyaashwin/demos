// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::ChecklistViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChecklistViewController
 */
@WithUserDetails("super")
public class ChecklistViewControllerTest
    extends ChecklistViewControllerTestBase {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChecklistViewControllerTest#testViewPersonChecklists()
   */
  protected void handleTestViewPersonChecklists() throws Exception {
    getMockMvc().perform(get("/checklist/person/-1/view")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("checklist"));
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChecklistViewControllerTest#testViewGroupChecklists()
   */
  protected void handleTestViewGroupChecklists() throws Exception {
    getMockMvc().perform(get("/checklist/group/-201/view")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("group"))
        .andExpect(model().attribute("group", hasProperty("id", equalTo(-201L))))
        .andExpect(view().name("checklist"));
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChecklistViewControllerTest#testViewAddPersonChecklist()
   */
  protected void handleTestViewAddPersonChecklist() throws Exception {
    getMockMvc().perform(get("/checklist/person/-1/add")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))))
        .andExpect(view().name("checklist"));
  }

  @Override
  protected void handleTestViewAddGroupMemberChecklists() throws Exception {
    getMockMvc().perform(get("/checklist/group/-201/add")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("group"))
        .andExpect(model().attribute("group", hasProperty("id", equalTo(-201L))))
        .andExpect(view().name("checklist"));
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChecklistViewControllerTest#testViewChecklist()
   */
  protected void handleTestViewChecklist() throws Exception {
    getMockMvc().perform(get("/checklist/view?id=-1")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(view().name("checklist.view"))
        .andExpect(model().attributeExists("person"))
        .andExpect(model().attribute("person", hasProperty("id", equalTo(-1L))));
  }

  void testViewChecklistForGroup() throws Exception {
    getMockMvc().perform(get("/checklist/view?id=-1&isGroupContext=true")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(view().name("checklist.view"))
        .andExpect(model().attributeExists("group"))
        .andExpect(model().attribute("group", hasProperty("id", equalTo(-201L))));
  }

  // Add additional test cases here
}
