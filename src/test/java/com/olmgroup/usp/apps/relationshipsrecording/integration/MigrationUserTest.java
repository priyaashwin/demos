package com.olmgroup.usp.apps.relationshipsrecording.integration;

import static com.olmgroup.usp.facets.test.hamcrest.RegexMatcher.matches;
import static org.hamcrest.core.IsNull.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.components.casenote.vo.CaseNoteEntryVO;
import com.olmgroup.usp.components.person.domain.LifeState;
import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;
import com.olmgroup.usp.facets.test.security.context.WithUserDetails;
import com.olmgroup.usp.facets.test.util.TestUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.core.HttpHeaders;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development", "migration" })
@WithUserDetails("migration_user")
public class MigrationUserTest extends AbstractUnifiedTestBase {

  protected static final Logger LOGGER = LoggerFactory.getLogger(MigrationUserTest.class);

  // Adding a person with a list of new addresses and new contacts.
  // Using the migration user, the person is added
  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWithAddressesUsingMigrationUser() throws Exception {

    // Create first locationVO
    final JSONObject newExternalLocationResource1 = new JSONObject();
    newExternalLocationResource1.put("systemIdentifier", "carefirst-provder");
    newExternalLocationResource1.put("resourceIdentifier", "LOC1");
    newExternalLocationResource1.put("_type", "NewExternalResource");

    final JSONObject locationVO1 = new JSONObject();
    locationVO1.put("_type", "NewLocation");
    locationVO1.put("uprn", 4510106287l);
    locationVO1.put("town", "Newcastle Upon Tyne");
    locationVO1.put("primaryNameOrNumber", "Wordsworth House Nursing Home");
    locationVO1.put("nonShareable", true);
    locationVO1.put("street", "Clayton Road");
    locationVO1.put("postcode", "NE2 1TL");
    locationVO1.put("administrativeArea", "Jesmond");
    locationVO1.put("externalResource", newExternalLocationResource1);
    locationVO1.put("administrativeArea", "Jesmond");

    final JSONObject simpleFuzzyDate1 = new JSONObject();
    simpleFuzzyDate1.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate1.put("year", 2011);
    simpleFuzzyDate1.put("month", Calendar.OCTOBER);
    simpleFuzzyDate1.put("day", 05);

    final JSONObject newExternalAddressResource1 = new JSONObject();
    newExternalAddressResource1.put("systemIdentifier", "carefirst-provder");
    newExternalAddressResource1.put("resourceIdentifier", "ADD1");
    newExternalAddressResource1.put("_type", "NewExternalResource");

    // Create first address VO
    final JSONObject newAddressVO1 = new JSONObject();
    newAddressVO1.put("type", "HOME");
    newAddressVO1.put("usage", "PLACEMENT");
    newAddressVO1.put("startDate", simpleFuzzyDate1);
    newAddressVO1.put("_type", "NewAddress");
    newAddressVO1.put("externalResource", newExternalAddressResource1);
    newAddressVO1.put("newLocation", locationVO1);

    // Create first locationVO
    final JSONObject newExternalLocationResource2 = new JSONObject();
    newExternalLocationResource2.put("systemIdentifier", "carefirst-provder");
    newExternalLocationResource2.put("resourceIdentifier", "LOC2");
    newExternalLocationResource2.put("_type", "NewExternalResource");

    final JSONObject locationVO2 = new JSONObject();
    locationVO2.put("_type", "NewLocation");
    locationVO2.put("uprn", 4510039601l);
    locationVO2.put("town", "Newcastle Upon Tyne");
    locationVO2.put("primaryNameOrNumber", "26");
    locationVO2.put("nonShareable", true);
    locationVO2.put("street", "Slingsby Gardens");
    locationVO2.put("postcode", "NE7 7RX");
    locationVO2.put("administrativeArea", "Jesmond");
    locationVO2.put("externalResource", newExternalLocationResource2);
    locationVO2.put("administrativeArea", "Jesmond");

    final JSONObject simpleFuzzyDate2 = new JSONObject();
    simpleFuzzyDate2.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate2.put("year", 2000);
    simpleFuzzyDate2.put("month", Calendar.OCTOBER);
    simpleFuzzyDate2.put("day", 20);

    final JSONObject simpleFuzzyDate3 = new JSONObject();
    simpleFuzzyDate3.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate3.put("year", 2011);
    simpleFuzzyDate3.put("month", Calendar.SEPTEMBER);
    simpleFuzzyDate3.put("day", 30);

    final JSONObject newExternalAddressResource2 = new JSONObject();
    newExternalAddressResource2.put("systemIdentifier", "carefirst-provder");
    newExternalAddressResource2.put("resourceIdentifier", "ADD2");
    newExternalAddressResource2.put("_type", "NewExternalResource");

    // Create first address VO
    final JSONObject newAddressVO2 = new JSONObject();
    newAddressVO2.put("type", "HOME");
    newAddressVO2.put("usage", "PLACEMENT");
    newAddressVO2.put("startDate", simpleFuzzyDate2);
    newAddressVO2.put("endDate", simpleFuzzyDate3);
    newAddressVO2.put("_type", "NewAddress");
    newAddressVO2.put("externalResource", newExternalAddressResource2);
    newAddressVO2.put("newLocation", locationVO2);

    final JSONObject newExternalContactResource1 = new JSONObject();
    newExternalContactResource1.put("systemIdentifier", "carefirst-provder");
    newExternalContactResource1.put("resourceIdentifier", "E1234");
    newExternalContactResource1.put("_type", "NewExternalResource");

    final JSONObject newContact1 = new JSONObject();
    newContact1.put("number", "2700851");
    newContact1.put("_type", "NewTelephoneContact");
    newContact1.put("contactType", "HOME");
    newContact1.put("telephoneType", "MOBILE");
    newContact1.put("contactUsage", "PLACEMENT");
    newContact1.put("startDate", -2208988800000l);
    newContact1.put("externalResource", newExternalContactResource1);

    // Create a person VO
    final List<String> personType = new ArrayList<String>();
    personType.add("OTHER");
    final JSONObject newPerson = new JSONObject();
    newPerson.put("forename", "Address");
    newPerson.put("surname", "Test");
    newPerson.put("personTypes", personType);
    newPerson.put("_type", "NewPerson");
    newPerson.put("lifeState", LifeState.ALIVE);

    // Add address VO to address VO list
    final List<JSONObject> contactList = new ArrayList<JSONObject>();
    contactList.add(newContact1);

    // Add address VO to address VO list
    final List<JSONObject> addressList = new ArrayList<JSONObject>();
    addressList.add(newAddressVO1);
    addressList.add(newAddressVO2);

    // Add the address VOs into the new person VO
    newPerson.put("addresses", addressList);
    newPerson.put("contacts", contactList);

    final String content = newPerson.toString();

    // Perform the REST post
    getMockMvc().perform(post(getRootURL() + "/person").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(
            header().string(HttpHeaders.LOCATION,
                matches(getRootURL() + "/person/\\d+")));
  }

  // Test Add multiple casenote entries with External resources
  @Test
  @DatabaseSetup(value = {
      "/dbunit/MigrationUserData/addCaseNoteEntries.setup.xml",
      "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public final void handleTestMultipeCaseNoteEntries() throws Exception {
    final JSONObject fuzzyDate = new JSONObject();
    fuzzyDate.put("_type", "FuzzyDate");
    fuzzyDate.put("month", 1);
    fuzzyDate.put("year", 2011);

    final JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "CAREFIST");
    newExternalResource.put("resourceIdentifier", "X1");
    newExternalResource.put("_type", "NewExternalResource");

    final JSONObject casenoteEntry = new JSONObject();
    casenoteEntry.put("_type", "NewCaseNoteEntry");
    casenoteEntry.put("eventDate", fuzzyDate);
    casenoteEntry.put("event", "Event 1");
    casenoteEntry.put("entryType", "CaseNoteEntryType");
    casenoteEntry.put("practitionerOrganisation", "Pract 1");
    casenoteEntry.put("externalResource", newExternalResource);

    final JSONObject fuzzyDate2 = new JSONObject();
    fuzzyDate2.put("_type", "FuzzyDate");
    fuzzyDate2.put("month", 1);
    fuzzyDate2.put("year", 2012);

    final JSONObject newExternalResource2 = new JSONObject();
    newExternalResource2.put("systemIdentifier", "CAREFIST");
    newExternalResource2.put("resourceIdentifier", "X2");
    newExternalResource2.put("_type", "NewExternalResource");

    final JSONObject casenoteEntry2 = new JSONObject();
    casenoteEntry2.put("_type", "NewCaseNoteEntry");
    casenoteEntry2.put("eventDate", fuzzyDate);
    casenoteEntry2.put("event", "Event 2");
    casenoteEntry2.put("entryType", "CaseNoteEntryType");
    casenoteEntry2.put("practitionerOrganisation", "Pract 2");
    casenoteEntry2.put("externalResource", newExternalResource2);

    final JSONObject fuzzyDate3 = new JSONObject();
    fuzzyDate3.put("_type", "FuzzyDate");
    fuzzyDate3.put("month", 1);
    fuzzyDate3.put("year", 2013);

    final JSONObject newExternalResource3 = new JSONObject();
    newExternalResource3.put("systemIdentifier", "CAREFIST");
    newExternalResource3.put("resourceIdentifier", "X3");
    newExternalResource3.put("_type", "NewExternalResource");

    final JSONObject casenoteEntry3 = new JSONObject();
    casenoteEntry3.put("_type", "NewCaseNoteEntry");
    casenoteEntry3.put("eventDate", fuzzyDate);
    casenoteEntry3.put("event", "Event 3");
    casenoteEntry3.put("entryType", "CaseNoteEntryType");
    casenoteEntry3.put("practitionerOrganisation", "Pract 3");
    casenoteEntry3.put("externalResource", newExternalResource3);

    JSONArray newCaseNoteEntries = new JSONArray();
    newCaseNoteEntries.put(casenoteEntry);
    newCaseNoteEntries.put(casenoteEntry2);
    newCaseNoteEntries.put(casenoteEntry3);

    final JSONObject caseNoteEntries = new JSONObject();
    caseNoteEntries.put("_type", "NewCaseNoteEntries");
    caseNoteEntries.put("newCaseNoteEntries", newCaseNoteEntries);

    getMockMvc()
        .perform(
            post(
                getRootURL() + "/person/-10/caseNoteEntries").content(caseNoteEntries.toString())
                    .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(header().string(HttpHeaders.LOCATION,
            matches(getRootURL() + "/personCaseNoteEntry/\\d+")))
        .andReturn();

    getMockMvc()
        .perform(
            get(
                getRootURL() + "/person/-10/caseNoteEntry")
                    .param("s", "[{ \"id\": \"desc\"}]")
                    .accept(getMIMETypeFromVOClass(
                        MediaType.APPLICATION_JSON, CaseNoteEntryVO.class)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("pageNumber").value(1))
        .andExpect(jsonPath("pageSize").value(10))
        .andExpect(jsonPath("totalSize").value(3));
  }

  // Test Add NewFormDefinition with External resources
  // NB As the form component and designer are updated this test might start to fail
  //    if the minimum designer version is set higher than the version used in this
  //    test. The fix would be to import this form definition into the latest version
  //    of the designer and then export back to JSON and saved at the path defined in the test
  @Test
  public final void handleTestAddExternalFormDefinition() throws Exception {
    // Read in form definition from classpath
    String newFormDefinition = readFile("/input/formDefinition/externalFormDefinition.json");

    MvcResult result = getMockMvc().perform(
        post(getRootURL() + "/formDefinition").param("external", "true")
            .content(newFormDefinition)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(header().string(HttpHeaders.LOCATION, matches(getRootURL() + "/formDefinition/\\d+")))
        .andReturn();

    String formDefinitionId = StringUtils.substringAfter(result.getResponse()
        .getHeaders(HttpHeaders.LOCATION).get(0), "/formDefinition/");

    getMockMvc().perform(
        get(getRootURL() + "/formDefinition/" + formDefinitionId + "/design")
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
        .andExpect(jsonPath("formDefinitionId").value("3806f64d-9ca9-45f6-aca0-31b5140301dd"))
        .andExpect(jsonPath("name").value("CF Initial Contacts"))
        .andExpect(jsonPath("designerVersion").value("3.1.0"));
  }

  // Test Add NewFormInstance with External resources
  @DatabaseSetup(value = {
      "/dbunit/MigrationUserData/FormInstanceService.setup.xml",
      "/dbunit/MigrationUserData/addExternalFormInstanceForSubject.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public final void handleTestAddExternalFormInstance() throws Exception {
    // Read in form definition from classpath
    String newFormInstance = readFile(
        "/input/formInstance/newFormInstanceFromExternalSourceWithSubjectTypeVO.json");

    MvcResult result = getMockMvc().perform(
        post(getRootURL() + "/externalPerson/P2001/formInstance")
            .content(newFormInstance)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(header().string(HttpHeaders.LOCATION, matches(getRootURL() + "/formInstance/\\d+")))
        .andReturn();

    String formInstanceId = StringUtils.substringAfter(result.getResponse()
        .getHeaders(HttpHeaders.LOCATION).get(0), "/formInstance/");

    getMockMvc().perform(
        get(getRootURL() + "/formInstance/" + formInstanceId)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("status").value("COMPLETE"))
        .andExpect(jsonPath("initiatorName").value("Johannes Vermeer"))
        .andExpect(jsonPath("completerName").value("Johannes Vermeer"))
        .andExpect(jsonPath("submitterName").value(nullValue()))
        .andExpect(jsonPath("subjectId").value(-2001));
  }

  @Test
  public void testMigrationUserAddPersonWithOverlappingAddresses()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", 8);
    dateOfBirth.put("year", 1952);

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("_type", "NewPerson");
    newPerson.put("lifeState", LifeState.ALIVE);
    newPerson.put("personTypes", personType);

    JSONObject locationAExRes = new JSONObject();
    locationAExRes.put("resourceIdentifier", "E10573:L183");
    locationAExRes.put("systemIdentifier", "carefirst-provider");
    locationAExRes.put("_type", "NewExternalResource");

    JSONObject locationA = new JSONObject();
    locationA.put("_type", "NewLocation");
    locationA.put("objectVersion", 0);
    locationA.put("primaryNameOrNumber", "18");
    locationA.put("town", "Feltham");
    locationA.put("nonShareable", true);
    locationA.put("street", "Winslow Way");
    locationA.put("resolveLocation", true);
    locationA.put("postcode", "TW13 6QD");
    locationA.put("county", "Middlesex");
    locationA.put("externalResource", locationAExRes);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 1);
    startDateAddressA.put("month", 1);
    startDateAddressA.put("year", 1900);

    JSONObject addressAExRes = new JSONObject();
    addressAExRes.put("resourceIdentifier", "E10573");
    addressAExRes.put("systemIdentifier", "carefirst-provider");
    addressAExRes.put("_type", "NewExternalResource");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("objectVersion", 0);
    addressA.put("newLocation", locationA);
    addressA.put("startDate", startDateAddressA);
    addressA.put("externalResource", addressAExRes);
    addressA.put("newLocation", locationA);

    JSONObject locationBExRes = new JSONObject();
    locationBExRes.put("resourceIdentifier", "E10574:L143");
    locationBExRes.put("systemIdentifier", "carefirst-provider");
    locationBExRes.put("_type", "NewExternalResource");

    JSONObject locationB = new JSONObject();
    locationB.put("_type", "NewLocation");
    locationB.put("objectVersion", 0);
    locationB.put("primaryNameOrNumber", "199");
    locationB.put("town", "London");
    locationB.put("nonShareable", true);
    locationB.put("street", "Teddington");
    locationB.put("resolveLocation", true);
    locationB.put("postcode", "TE3 3RS");
    locationB.put("county", "Middlesex");
    locationB.put("externalResource", locationBExRes);

    JSONObject startDateAddressB = new JSONObject();
    startDateAddressB.put("_type", "SimpleFuzzyDate");
    startDateAddressB.put("day", 2);
    startDateAddressB.put("month", 8);
    startDateAddressB.put("year", 2014);

    JSONObject endDateAddressB = new JSONObject();
    endDateAddressB.put("_type", "SimpleFuzzyDate");
    endDateAddressB.put("day", 9);
    endDateAddressB.put("month", 3);
    endDateAddressB.put("year", 2017);

    JSONObject addressBExRes = new JSONObject();
    addressBExRes.put("resourceIdentifier", "E10574");
    addressBExRes.put("systemIdentifier", "carefirst-provider");
    addressBExRes.put("_type", "NewExternalResource");

    JSONObject addressB = new JSONObject();
    addressB.put("_type", "NewAddress");
    addressB.put("objectVersion", 0);
    addressB.put("type", "HOME");
    addressB.put("usage", "CF_OTHER");
    addressB.put("newLocation", locationB);
    addressB.put("startDate", startDateAddressB);
    addressB.put("endDate", endDateAddressB);
    addressB.put("externalResource", addressBExRes);

    JSONObject locationCExRes = new JSONObject();
    locationCExRes.put("resourceIdentifier", "E10575:L83");
    locationCExRes.put("systemIdentifier", "carefirst-provider");
    locationCExRes.put("_type", "NewExternalResource");

    JSONObject locationC = new JSONObject();
    locationC.put("_type", "NewLocation");
    locationC.put("objectVersion", 0);
    locationC.put("primaryNameOrNumber", "6 The Grange");
    locationC.put("town", "Haverford");
    locationC.put("nonShareable", true);
    locationC.put("street", "High Street, Cresent Avenue");
    locationC.put("locality", "Little Worthing");
    locationC.put("resolveLocation", true);
    locationC.put("postcode", "D45 3PA");
    locationC.put("county", "Chester");
    locationC.put("administrativeArea", "Sandycombe");
    locationC.put("externalResource", locationCExRes);

    JSONObject startDateAddressC = new JSONObject();
    startDateAddressC.put("_type", "SimpleFuzzyDate");
    startDateAddressC.put("day", 4);
    startDateAddressC.put("month", 3);
    startDateAddressC.put("year", 2017);

    JSONObject addressCExRes = new JSONObject();
    addressCExRes.put("resourceIdentifier", "E10575");
    addressCExRes.put("systemIdentifier", "carefirst-provider");
    addressCExRes.put("_type", "NewExternalResource");

    JSONObject addressC = new JSONObject();
    addressC.put("_type", "NewAddress");
    addressC.put("objectVersion", 0);
    addressC.put("type", "HOME");
    addressC.put("usage", "CF_OTHER");
    addressC.put("newLocation", locationC);
    addressC.put("startDate", startDateAddressC);
    addressC.put("externalResource", addressCExRes);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);
    newAddresses.put(addressB);
    newAddresses.put(addressC);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(
            header().string(HttpHeaders.LOCATION,
                matches(getRootURL() + "/person/\\d+")));
  }

  private static String readFile(String path)
      throws IOException {
    Resource resource = new ClassPathResource(path);
    return FileUtils.readFileToString(resource.getFile());
  }

  @Test
  // Adding a person with a list of new addresses.
  // Do not provide a locationId or an external identifier
  // Result - A newPersonAddress must have at least one of a locationId or external resource
  public void testAddPersonWithAddresses() throws Exception {
    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");
    JSONObject newPerson = new JSONObject();
    newPerson.put("forename", "Address");
    newPerson.put("surname", "Test");
    newPerson.put("personTypes", personType);
    newPerson.put("_type", "NewPerson");
    newPerson.put("lifeState", LifeState.ALIVE);

    // Create first address VO
    JSONObject newAddressVO = new JSONObject();
    JSONObject simpleFuzzyDate = new JSONObject();
    simpleFuzzyDate.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate.put("year", 2015);
    simpleFuzzyDate.put("month", Calendar.JUNE);
    simpleFuzzyDate.put("day", 05);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provder");
    newExternalResource.put("resourceIdentifier", "L123");
    newExternalResource.put("_type", "NewExternalResource");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("ExternalResource", newLocationVO);

    newAddressVO.put("type", "HOME");
    newAddressVO.put("usage", "PLACEMENT");
    newAddressVO.put("startDate", simpleFuzzyDate);
    newAddressVO.put("_type", "NewAddress");

    // Add address VO to address VO list
    List<JSONObject> list = new ArrayList<JSONObject>();
    list.add(newAddressVO);
    // Add the address VOs into the new person VO
    newPerson.put("addresses", list);

    String content = newPerson.toString();

    // Perform the REST post
    getMockMvc().perform(post(getRootURL() + "/person").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("status").value(400)).andExpect(jsonPath("code").value(400))
        .andExpect(jsonPath("validationErrors.NewPersonAddressVO")
            .value("Location or unknown location or LocationId must be chosen."))
        .andExpect(jsonPath("message")
            .value(
                "org.springframework.validation.BeanPropertyBindingResult: 1 errors\nError in object 'NewPersonAddressVO': codes [ConstraintViolationException.newPersonVO,ConstraintViolationException]; arguments [locationId]; default message [Location or unknown location or LocationId must be chosen.]"))
        .andExpect(jsonPath("developerMessage")
            .value(
                "org.springframework.validation.BeanPropertyBindingResult: 1 errors\nError in object 'NewPersonAddressVO': codes [ConstraintViolationException.newPersonVO,ConstraintViolationException]; arguments [locationId]; default message [Location or unknown location or LocationId must be chosen.]"))
        .andExpect(jsonPath("moreInfoUrl").value("mailto:supportDept@mycompany.com"));
  }

  @Test
  // Adding a person with a list of new addresses.
  // Provide both locationId or an external identifier
  // Result - Person is created
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWithAddressesBothLocationIdAndExternalAddress() throws Exception {
    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");
    JSONObject newPerson = new JSONObject();
    newPerson.put("forename", "Address");
    newPerson.put("surname", "Test");
    newPerson.put("personTypes", personType);
    newPerson.put("_type", "NewPerson");
    newPerson.put("lifeState", LifeState.ALIVE);

    // Create first address VO
    JSONObject newAddressVO = new JSONObject();
    JSONObject simpleFuzzyDate = new JSONObject();
    simpleFuzzyDate.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate.put("year", 2015);
    simpleFuzzyDate.put("month", Calendar.JUNE);
    simpleFuzzyDate.put("day", 05);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provder");
    newExternalResource.put("resourceIdentifier", "E1234");
    newExternalResource.put("_type", "NewExternalResource");

    newAddressVO.put("type", "HOME");
    newAddressVO.put("usage", "PLACEMENT");
    newAddressVO.put("startDate", simpleFuzzyDate);
    newAddressVO.put("_type", "NewAddress");
    newAddressVO.put("externalResource", newExternalResource);
    newAddressVO.put("locationId", -1);

    // Add address VO to address VO list
    List<JSONObject> list = new ArrayList<JSONObject>();
    list.add(newAddressVO);
    // Add the address VOs into the new person VO
    newPerson.put("addresses", list);

    String content = newPerson.toString();

    // Perform the REST post
    getMockMvc().perform(post(getRootURL() + "/person").content(content).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Adding a person address without a location id
  // Result - locationId is mandatory when adding an address via Eclipse
  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWithoutLocationId() throws Exception {
    // Create address VO
    JSONObject newAddressVO = new JSONObject();
    JSONObject simpleFuzzyDate = new JSONObject();
    simpleFuzzyDate.put("_type", "SimpleFuzzyDate");
    simpleFuzzyDate.put("year", 2015);
    simpleFuzzyDate.put("month", Calendar.JUNE);
    simpleFuzzyDate.put("day", 05);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provder");
    newExternalResource.put("resourceIdentifier", "E1234");
    newExternalResource.put("_type", "NewExternalResource");

    newAddressVO.put("type", "HOME");
    newAddressVO.put("usage", "PLACEMENT");
    newAddressVO.put("startDate", simpleFuzzyDate);
    newAddressVO.put("_type", "NewPersonAddress");

    String content = newAddressVO.toString();

    // Perform the REST post
    getMockMvc()
        .perform(post(getRootURL() + "/person/-10/address").content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("status").value(400)).andExpect(jsonPath("code").value(400))
        .andExpect(jsonPath("validationErrors.locationId").value("Location is mandatory"));
  }

  // Test where a newPerson contains a NewLocation that is already held in DB
  // Result - The newLocation attribute is nulled and the locationId is set to that of the location held in the DB and
  // the person is created
  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWhereLocationAlreadyExistsAsExternalResource()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newExternalLocationResourceVO = new JSONObject();
    newExternalLocationResourceVO.put("_type", "NewExternalResource");
    newExternalLocationResourceVO.put("resourceIdentifier", "P1653105");
    newExternalLocationResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("primaryNameOrNumber", "Estuary House");
    newLocationVO.put("nonShareable", true);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TQ12 4PH");
    newLocationVO.put("county", "Devon");
    newLocationVO.put("externalResource", newExternalLocationResourceVO);

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Test that submitting a new person with both newLocation AND locationId
  // Result - Cannot submit both newLocation AND locationId
  @Test
  @DatabaseSetup(
      value = { "/dbunit/PersonDetailsService/PersonDetailsService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPersonWithNewLocationAndLocationId()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("primaryNameOrNumber", "Estuary House");
    newLocationVO.put("nonShareable", true);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TQ12 4PH");
    newLocationVO.put("county", "Devon");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("locationId", -1);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("status").value(400)).andExpect(jsonPath("code").value(400))
        .andExpect(jsonPath("validationErrors",
            Matchers.hasEntry("addresses[0].locationId", "Either of location or new location can be supplied")));
  }

  // Test where a newPerson contains a NewLocation that has an invalid Postcode
  // Result - The contraint is by passed as the NewLocation has an external resource
  @Test
  public void testAddPersonWithInvalidPostcode()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newExternalLocationResourceVO = new JSONObject();
    newExternalLocationResourceVO.put("_type", "NewExternalResource");
    newExternalLocationResourceVO.put("resourceIdentifier", "P1653105");
    newExternalLocationResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("primaryNameOrNumber", "Estuary House");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "111111111111111111");
    newLocationVO.put("county", "Devon");
    newLocationVO.put("externalResource", newExternalLocationResourceVO);

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Test where a newPerson contains a NewLocation does not have primaryNameOrNumber
  // Result - The constraint is by passed as the NewLocation has an external resource
  @Test
  public void testAddPersonWithNoPrimaryNameOrNumber()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newExternalLocationResourceVO = new JSONObject();
    newExternalLocationResourceVO.put("_type", "NewExternalResource");
    newExternalLocationResourceVO.put("resourceIdentifier", "P1653105");
    newExternalLocationResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TW11 9AA");
    newLocationVO.put("county", "Devon");
    newLocationVO.put("externalResource", newExternalLocationResourceVO);

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Test where a newPerson contains a NewLocation does not have primaryNameOrNumber and an invalid postcode
  // Result - The constraint is by passed as the NewLocation has an external resource
  @Test
  public void testAddPersonWithNoPrimaryNameOrNumberAndInvalidPostcode()
      throws Exception {
    JSONObject dateOfBirth = new JSONObject();
    dateOfBirth.put("_type", "SimpleFuzzyDate");
    dateOfBirth.put("day", 21);
    dateOfBirth.put("month", Calendar.SEPTEMBER);
    dateOfBirth.put("year", 1952);

    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    // Create a person VO
    List<String> personType = new ArrayList<String>();
    personType.add("OTHER");

    JSONObject newPerson = new JSONObject();
    newPerson.put("title", "MR");
    newPerson.put("forename", "John");
    newPerson.put("middleNames", "Graham");
    newPerson.put("surname", "Mellor");
    newPerson.put("knownAs", "JG");
    newPerson.put("suffix", "C.D.");
    newPerson.put("dateOfBirth", dateOfBirth);
    newPerson.put("gender", "male");
    newPerson.put("ethnicity", "WBRI");
    newPerson.put("externalResource", newExternalResource);
    newPerson.put("_type", "NewPerson");
    newPerson.put("personTypes", personType);
    newPerson.put("lifeState", LifeState.ALIVE);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newExternalLocationResourceVO = new JSONObject();
    newExternalLocationResourceVO.put("_type", "NewExternalResource");
    newExternalLocationResourceVO.put("resourceIdentifier", "P1653105");
    newExternalLocationResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Newton Abbot");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "111111111111111111");
    newLocationVO.put("county", "Devon");
    newLocationVO.put("externalResource", newExternalLocationResourceVO);

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);
    newPerson.put("addresses", newAddresses);

    String content = newPerson.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/person")
                .param("allowOverlappingAddresses", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Test where a newTeam contains a NewLocation does not have primaryNameOrNumber
  // Result - The constraint is by passed as the newLocation has an external resource
  @DatabaseSetup(value = { "/dbunit/MigrationUserData/addTeam.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void testAddTeamWithNoPrimaryNameOrNumber()
      throws Exception {
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    JSONObject newTeam = new JSONObject();
    newTeam.put("name", "The C team");
    newTeam.put("objectVersion", 0);
    newTeam.put("description", "Testing add via controller.");
    newTeam.put("_type", "NewTeam");
    newTeam.put("securityRecordTypeConfigurationId", -1);
    newTeam.put("externalResource", newExternalResource);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newExternalLocationResourceVO = new JSONObject();
    newExternalLocationResourceVO.put("_type", "NewExternalResource");
    newExternalLocationResourceVO.put("resourceIdentifier", "P1653105");
    newExternalLocationResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Magilligan");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Martello Tower");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "TW11 9AA");
    newLocationVO.put("county", "Derry");
    newLocationVO.put("externalResource", newExternalLocationResourceVO);

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newTeam.put("addresses", newAddresses);

    String content = newTeam.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/team")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Test where a newOrganisation contains a NewLocation that has an invalid Postcode
  // Result - The contraint is by passed as the NewLocation has an external resource
  @Test
  public void testAddOrganisationWithInvalidPostcode()
      throws Exception {
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("_type", "NewExternalResource");
    newExternalResource.put("resourceIdentifier", "E1047");
    newExternalResource.put("systemIdentifier", "carefirst-provider");

    JSONObject newOrganisation = new JSONObject();
    newOrganisation.put("name", "The C team");
    newOrganisation.put("objectVersion", 0);
    newOrganisation.put("description", "Testing add via controller.");
    newOrganisation.put("_type", "NewOrganisation");
    newOrganisation.put("externalResource", newExternalResource);

    JSONObject startDateAddressA = new JSONObject();
    startDateAddressA.put("_type", "SimpleFuzzyDate");
    startDateAddressA.put("day", 23);
    startDateAddressA.put("month", Calendar.NOVEMBER);
    startDateAddressA.put("year", 2008);

    JSONObject newExternalLocationResourceVO = new JSONObject();
    newExternalLocationResourceVO.put("_type", "NewExternalResource");
    newExternalLocationResourceVO.put("resourceIdentifier", "P1653105");
    newExternalLocationResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject newLocationVO = new JSONObject();
    newLocationVO.put("objectVersion", 0);
    newLocationVO.put("uprn", 10032983668l);
    newLocationVO.put("town", "Magilligan");
    newLocationVO.put("primaryNameOrNumber", "Martello Tower");
    newLocationVO.put("nonShareable", false);
    newLocationVO.put("street", "Collett Way");
    newLocationVO.put("resolveLocation", true);
    newLocationVO.put("_type", "NewLocation");
    newLocationVO.put("postcode", "111111111111111111");
    newLocationVO.put("county", "Derry");
    newLocationVO.put("externalResource", newExternalLocationResourceVO);

    JSONObject newExternalAddressResourceVO = new JSONObject();
    newExternalAddressResourceVO.put("_type", "NewExternalResource");
    newExternalAddressResourceVO.put("resourceIdentifier", "J630773");
    newExternalAddressResourceVO.put("systemIdentifier", "carefirst-provider");

    JSONObject addressA = new JSONObject();
    addressA.put("_type", "NewAddress");
    addressA.put("type", "HOME");
    addressA.put("usage", "PLACEMENT");
    addressA.put("startDate", startDateAddressA);
    addressA.put("newLocation", newLocationVO);
    addressA.put("externalResource", newExternalAddressResourceVO);

    JSONArray newAddresses = new JSONArray();
    newAddresses.put(addressA);

    newOrganisation.put("addresses", newAddresses);

    String content = newOrganisation.toString();

    getMockMvc()
        .perform(
            post(getRootURL() + "/organisation")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  // Test that migration user has the permission to add locations
  @Test
  public void addLocation() throws Exception {

    // Create a Location VO
    JSONObject newLocation = new JSONObject();
    newLocation.put("street", "Station Road");
    newLocation.put("postcode", "TW11 9AA");
    newLocation.put("uprn", 100041142520l);
    newLocation.put("town", "Teddington");
    newLocation.put("primaryNameOrNumber", "11");
    newLocation.put("nonShareable", false);
    newLocation.put("_type", "NewLocation");

    // Create an external resource VO
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provider");
    newExternalResource.put("resourceIdentifier", "L300");
    newExternalResource.put("_type", "NewExternalResource");

    // Set the external resource VO into the new location VO
    newLocation.put("externalResource", newExternalResource);

    String content = newLocation.toString();
    // Perform the REST post
    getMockMvc()
        .perform(
            post(getRootURL() + "/location/")
                .param("initiateSearch", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(
            header().string(HttpHeaders.LOCATION, matches(getRootURL() + "/location/\\d+")))
        .andReturn();
  }

  // Test that migration user adding a newLocation with external resource does
  // have the constraints applied.
  @Test
  public void addLocationNoConstraints() throws Exception {

    // Create a Location VO
    JSONObject newLocation = new JSONObject();
    newLocation.put("uprn", 100041142520l);
    newLocation.put("nonShareable", false);
    newLocation.put("_type", "NewLocation");

    // Create an external resource VO
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provider");
    newExternalResource.put("resourceIdentifier", "L300");
    newExternalResource.put("_type", "NewExternalResource");

    // Set the external resource VO into the new location VO
    newLocation.put("externalResource", newExternalResource);

    String content = newLocation.toString();
    // Perform the REST post
    getMockMvc()
        .perform(
            post(getRootURL() + "/location/")
                .param("initiateSearch", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(
            header().string(HttpHeaders.LOCATION, matches(getRootURL() + "/location/\\d+")))
        .andReturn();
  }

  // Test that migration user has the permission to add locations, but if the
  // location fails for constraint validation as it has not external resource
  // link
  @Test
  public void addLocationConstraints() throws Exception {

    // Create a Location VO
    JSONObject newLocation = new JSONObject();
    newLocation.put("uprn", 100041142520l);
    newLocation.put("nonShareable", true);
    newLocation.put("_type", "NewLocation");

    String content = newLocation.toString();
    // Perform the REST post
    getMockMvc()
        .perform(
            post(getRootURL() + "/location/")
                .param("initiateSearch", "true")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andReturn();
  }

  @Ignore // CLA changing alot so I've decided to ignore until we see which end points are actually required.
  @Test
  @DatabaseSetup(value = {
      "/dbunit/MigrationUserData/FormInstanceService.setup.xml",
      "/dbunit/MigrationUserData/addExternalFormInstanceForSubject.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testAddFormInstanceImageAttachment() throws Exception {
    InputStream in = this.getClass().getResourceAsStream("/milkmaid.gif");
    byte[] bytes = IOUtils.toByteArray(in);
    in.close();
    // Create an image to get the initial image height and width
    // BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));

    MockMultipartFile file = new MockMultipartFile("file", "milkmaid.gif",
        "image/gif", bytes);

    // Create an external resource VO
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provider");
    newExternalResource.put("resourceIdentifier", "O_ASSESSMENTS_A123");
    newExternalResource.put("_type", "NewExternalResource");

    // The VO part
    JSONObject newFormInstanceAttachmentVO = new JSONObject();
    newFormInstanceAttachmentVO.put("_type", "NewFormInstanceAttachment");
    newFormInstanceAttachmentVO.put("formInstanceId", -1000l);
    newFormInstanceAttachmentVO.put("title", "Hello");

    // Set the external resource VO into the new location VO
    newFormInstanceAttachmentVO.put("externalResource", newExternalResource);

    MockMultipartFile vo = new MockMultipartFile(
        "NewFormInstanceAttachment", "vo",
        MediaType.APPLICATION_JSON.toString(),
        newFormInstanceAttachmentVO.toString().getBytes());

    String fileUploadURL = getRootURL() + "/formInstance/-1001/attachment";
    getMockMvc().perform(
        fileUpload(fileUploadURL).file(file).file(vo).param("completedFormInstance", "true"))
        .andExpect(status().isCreated())
        .andReturn();
  }

  @Test
  public void testGetProperties() throws Exception {
    getMockMvc()
        .perform(
            get(
                getRootURL() + "/application/properties"))
        .andExpect(status().isOk());
  }

  @Test
  public void testAddSecurityUser() throws Exception {
    // Create an external resource VO
    JSONObject newExternalResource = new JSONObject();
    newExternalResource.put("systemIdentifier", "carefirst-provider");
    newExternalResource.put("resourceIdentifier", "678");
    newExternalResource.put("_type", "NewExternalResource");

    // Create a NewSecurityUserVO in JSON form
    final JSONObject newSecurityUser = new JSONObject();

    newSecurityUser.put("enabled", true);
    newSecurityUser.put("expiryDate", TestUtils.getDate("2999-01-01"));
    newSecurityUser.put("name", "Mario Bava");
    newSecurityUser.put("passwordExpiryDate",
        TestUtils.getDate("2999-01-01"));
    newSecurityUser.put("passwordReset", false);
    newSecurityUser.put("userName", "mbava");
    newSecurityUser.put("password", "myV4lidP4ssword!");
    newSecurityUser.put("confirmPassword", "myV4lidP4ssword!");
    newSecurityUser.put("_type", "NewSecurityUser");
    newSecurityUser.put("externalResource", newExternalResource);

    // Serialize to JSON format
    final String content = newSecurityUser.toString();

    // Perform the POST, supplying the content and asserting both that HTTP
    // 201 (Created) is returned
    // and there is a Location response header containing the URL of the new
    // resource.
    getMockMvc()
        .perform(
            post(getRootURL() + "/securityUser").content(content)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(
            header().string(HttpHeaders.LOCATION,
                matches(getRootURL() + "/securityUser/\\d+")));
  }

  @DatabaseSetup(value = { "/dbunit/MigrationUserData/addPeriodOfCare.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddPeriodOfCare() throws Exception {
    JSONObject newPeriodOfCare = new JSONObject();
    newPeriodOfCare.put("previouslyLookedAfter", "false");
    newPeriodOfCare.put("siblingGroup", "true");
    newPeriodOfCare.put("siblingsAssessed", "true");
    newPeriodOfCare.put("personId", "-102");
    newPeriodOfCare.put("placementTypeId", "-113");
    newPeriodOfCare.put("placementProvision", "PR0");
    newPeriodOfCare.put("carerSubjectType", "PERSON");
    newPeriodOfCare.put("carerSubjectId", "-103");
    newPeriodOfCare.put("startDate", TestUtils.getDate("2017-03-08"));
    newPeriodOfCare.put("legalStatusId", "-2");
    newPeriodOfCare.put("typeOfCare", "RESPITE");
    newPeriodOfCare.put("_type", "NewPeriodOfCareWithEpisodeOfCare");
    newPeriodOfCare.put("categoryOfNeedId", "-1");
    // Serialize to JSON format
    String content = newPeriodOfCare.toString();

    getMockMvc().perform(
        post(getRootURL() + "/periodOfCare")
            .content(content)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  public void testAddPeriodOfCare_WithNoCategoryOfNeed() throws Exception {
    JSONObject newPeriodOfCare = new JSONObject();
    newPeriodOfCare.put("previouslyLookedAfter", "true");
    newPeriodOfCare.put("siblingGroup", "true");
    newPeriodOfCare.put("siblingsAssessed", "true");
    newPeriodOfCare.put("personId", "-102");
    newPeriodOfCare.put("temporaryPlacement", "false");
    newPeriodOfCare.put("mainPlacement", "false");
    newPeriodOfCare.put("startDate", TestUtils.getDate("2017-03-08"));
    newPeriodOfCare.put("placementTypeId", "-3");
    newPeriodOfCare.put("carerSubjectType", "PERSON");
    newPeriodOfCare.put("carerSubjectId", "-103");
    newPeriodOfCare.put("startDate", TestUtils.getDate("2017-03-08"));
    newPeriodOfCare.put("legalStatusId", "-2");
    newPeriodOfCare.put("_type", "NewPeriodOfCareWithEpisodeOfCare");
  }

}