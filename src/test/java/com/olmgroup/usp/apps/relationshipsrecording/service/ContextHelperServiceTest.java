// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::ContextHelperService
 * STEREOTYPE:  Service
 * STEREOTYPE:  Unsecured
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import static org.mockito.Mockito.verify;

import com.olmgroup.usp.components.careleaver.service.context.CareLeaverContext;
import com.olmgroup.usp.components.careleaver.service.context.CareLeaverContextResolver;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.Model;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.ContextHelperService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ContextHelperServiceTest extends ContextHelperServiceTestBase {

  @Before
  public void setUp() {
    login("admin", "admin123");
  }

  @Test
  public void testCareLeaverAndAfterCareMenuContextAttributesInDefaultContext() {
    usp().getContextAttributesHandler().set(CareLeaverContextResolver.CARE_LEAVER_CONTEXT, CareLeaverContext.DEFAULT.toString());

    Model mockModel = Mockito.mock(Model.class);
    getContextHelperService().setupModelForContext(mockModel);

    verify(mockModel).addAttribute("careLeaverDefault", true);
    verify(mockModel).addAttribute("careLeaverAfterCare", false);
  }

  @Test
  public void testCareLeaverAndAfterCareMenuContextAttributesInScottishContext() {
    usp().getContextAttributesHandler().set(CareLeaverContextResolver.CARE_LEAVER_CONTEXT, CareLeaverContext.SCOTTISH.toString());

    Model mockModel = Mockito.mock(Model.class);
    getContextHelperService().setupModelForContext(mockModel);

    verify(mockModel).addAttribute("careLeaverDefault", false);
    verify(mockModel).addAttribute("careLeaverAfterCare", true);
  }
  
  @Test
  public void testCareLeaverAndAfterCareMenuContextAttributesInWelshContext() {
    usp().getContextAttributesHandler().set(CareLeaverContextResolver.CARE_LEAVER_CONTEXT, CareLeaverContext.WELSH.toString());

    Model mockModel = Mockito.mock(Model.class);
    getContextHelperService().setupModelForContext(mockModel);

    verify(mockModel).addAttribute("careLeaverDefault", false);
    verify(mockModel).addAttribute("careLeaverAfterCare", false);
    verify(mockModel).addAttribute("careLeaverWelsh", true);
  }

  @Override
  protected void handleTestSetupModelForContext() {
  }

  // Add additional test cases here
}