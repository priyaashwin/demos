// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::ClassificationGroupService
 * STEREOTYPE:  Service
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.components.classification.domain.ClassificationGroupStatus;
import com.olmgroup.usp.components.classification.exception.DuplicateClassificationCodeException;
import com.olmgroup.usp.components.classification.exception.InvalidClassificationGroupLevelException;
import com.olmgroup.usp.components.classification.service.ClassificationGroupService;
import com.olmgroup.usp.components.classification.vo.ClassificationGroupVO;
import com.olmgroup.usp.components.classification.vo.NewClassificationGroupVO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.ClassificationGroupService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ClassificationChildGroupServiceTest extends ClassificationChildGroupServiceTestBase {
  @Before
  public void handleInitializeTestSuite() {
    login("super", "testpassword");
  }

  @Inject
  ClassificationGroupService classificationComponentGroupService;

  @Override
  protected void handleTestAddClassificationGroup() throws Exception {

    final String expectedCode = "CLAS1";
    final String expectedName = "ClassificationGroup1";
    final boolean expectedMutuallyExclusive = false;
    final ClassificationGroupStatus expectedStatus = ClassificationGroupStatus.ACTIVE;
    final boolean expectedSystem = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    long classificationGroupId = getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    assertThat(classificationGroupId, greaterThan(0L));

    final long expectedId = classificationGroupId;

    ClassificationGroupVO classificationGroupVO = classificationComponentGroupService.findById(classificationGroupId);
    // Assert retrieved values
    testAssertClassificationGroup(expectedId, expectedCode, expectedName,
        expectedMutuallyExclusive, expectedStatus, expectedSystem,
        classificationGroupVO);

  }

  @Test(expected = InvalidClassificationGroupLevelException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddClassificationTopLevelClassificationGroup() throws Exception {
    final String expectedName = "ClassificationGroup1";
    final String expectedCode = "CGG1";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected InvalidClassificationGroupLevelException");
  }

  @Test(expected = InvalidClassificationGroupLevelException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/addClassificationGroupLevel5.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddClassificationClassificationGroupAtLevel5() throws Exception {
    final String expectedName = "ClassificationGroup5";
    final String expectedCode = "CG5";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-4L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected InvalidClassificationGroupLevelException");
    ;
  }

  @Ignore("Need to fix this test case once the requirement is confirmed")
  @Test(expected = DuplicateClassificationCodeException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddClassificationDuplicateCode() throws Exception {
    final String expectedName = "ClassificationGroup1";
    final String expectedCode = "CG1";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected DuplicateClassificationCodeException");
  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddClassificationNameIsMandatory() throws Exception {
    final String expectedCode = "CLAS1";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected ConstraintViolationException for classification name is mandatory");
  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddClassificationCodeIsMandatory() throws Exception {
    final String expectedName = "ClassificationGroup1";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected ConstraintViolationException for classification code is mandatory");
  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddClassificationCodeIsEmpty() throws Exception {
    final String expectedName = "ClassificationGroup1";
    final String expectedCode = "";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail(
        "Expected ConstraintViolationException for classification code is mandatory when empty code is submitted");
  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddClassificationCodeIsWithSpaces() throws Exception {
    final String expectedName = "ClassificationGroup1";
    final String expectedCode = "    ";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected ConstraintViolationException for classification code is mandatory");
  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddClassificationCodeIsNotAlphanumeric() throws Exception {
    final String expectedName = "ClassificationGroup1";
    final String expectedCode = "CLA_&1";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected ConstraintViolationException for classification code is not alphanumeric except _");
  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/ClassificationChildGroupService/ClassificationChildGroupService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public void testAddClassification_CodeTooLong() throws Exception {
    final String expectedName = "ClassificationGroup1";
    final String expectedCode = "CLASSIFICATION_CODE_AT_RISK_OF_BEING_A_VICTIM_OF_HARMFUL_BEHAVIOUR_AND_AT_RISK_OF_INITIATING_HARMFUL_BEHAVIOUR";
    final boolean expectedMutuallyExclusive = false;

    NewClassificationGroupVO newClassificationGroupVO = new NewClassificationGroupVO();
    newClassificationGroupVO.setCode(expectedCode);
    newClassificationGroupVO.setName(expectedName);
    newClassificationGroupVO.setMutuallyExclusive(expectedMutuallyExclusive);
    newClassificationGroupVO.setParentGroupId(-1L);

    getClassificationChildGroupService().addClassificationGroup(newClassificationGroupVO);
    Assert.fail("Expected ConstraintViolationException for classification code not more than 100 chars");
  }

  private void testAssertClassificationGroup(long expectedId, String expectedCode, String expectedName,
      boolean expectedMutuallyExclusive, ClassificationGroupStatus expectedStatus, boolean expectedSystem,
      ClassificationGroupVO classificationGroupVO) {

    Assert.assertEquals("testing expectedId", expectedId,
        classificationGroupVO.getId());
    Assert.assertEquals("testing expectedCode", expectedCode,
        classificationGroupVO.getCode());
    Assert.assertEquals("testing expectedName", expectedName,
        classificationGroupVO.getName());
    Assert.assertEquals("testing expectedMutuallyExclusive",
        expectedMutuallyExclusive, classificationGroupVO.getMutuallyExclusive());
    Assert.assertEquals("testing expectedStatus", expectedStatus,
        classificationGroupVO.getStatus());
    Assert.assertEquals("testing System", expectedSystem,
        classificationGroupVO.getSystem());

  }

  // Add additional test cases here
}