// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::web::view::SettingsViewController
 * STEREOTYPE:  Controller
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.olmgroup.usp.facets.test.security.context.WithUserDetails;

import org.springframework.http.MediaType;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SettingsViewController
 */
@WithUserDetails("super")
public class SettingsViewControllerTest extends SettingsViewControllerTestBase {

  protected void handleTestViewSettings() throws Exception {
    getMockMvc().perform(get("/admin").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }

  protected void handleTestViewCaseNoteEntryType() throws Exception {
    getMockMvc().perform(get("/admin/caseNoteEntryType").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewChronologyEntryType() throws Exception {
    getMockMvc().perform(get("/admin/chronologyEntryType").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewPersonReferenceType() throws Exception {
    getMockMvc().perform(get("/admin/personReferenceType").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewPersonWarnings() throws Exception {
    getMockMvc().perform(get("/admin/personWarnings").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewPersonAlerts() throws Exception {
    getMockMvc().perform(get("/admin/personAlerts").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.codedEntries.page"))
        .andExpect(model().attribute("personAlertsCategoryId", -100L))
        .andExpect(model().attribute("personAlertsCategoryCode",
            "com.olmgroup.usp.components.person.category.alerts"))
        .andExpect(model().attribute("personAlertsSystem", true))
        .andExpect(model().attribute("personAlertsAdditionsAllowed", true))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewChecklistPauseReason() throws Exception {
    getMockMvc().perform(get("/admin/checklistPauseReason").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewPersonProfessionalTitle() throws Exception {
    getMockMvc().perform(get("/admin/personProfessionalTitle").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }
  // Add additional test cases here

  @Override
  protected void handleTestViewTypeOfCare() throws Exception {
    getMockMvc().perform(get("/admin/typeOfCare").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());

  }

  @Override
  protected void handleTestViewReasonForUnavailability() throws Exception {
    getMockMvc().perform(get("/admin/reasonForUnavailability").accept(MediaType.TEXT_HTML)).andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewOutputCategory() throws Exception {
    getMockMvc().perform(get("/admin/outputCategory").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.codedEntries.page"))
        .andExpect(model().attribute("outputCategoryCategoryId", -1L))
        .andExpect(model().attribute("outputCategoryCategoryCode",
            "com.olmgroup.usp.components.output.category.outputCategory"))
        .andExpect(model().attribute("outputCategorySystem", true))
        .andExpect(model().attribute("outputCategoryAdditionsAllowed", true))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewSourceOrganisations() throws Exception {
    getMockMvc().perform(get("/admin/sourceOrganisation").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.codedEntries.page"))
        .andExpect(model().attribute("sourceOrganisationCategoryId", -1L))
        .andExpect(model().attribute("sourceOrganisationCategoryCode",
            "com.olmgroup.usp.facets.core.category.sourceOrganisation"))
        .andExpect(model().attribute("sourceOrganisationSystem", true))
        .andExpect(model().attribute("sourceOrganisationAdditionsAllowed", true))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewNonVerbalLanguages() throws Exception {
    getMockMvc().perform(get("/admin/nonVerbalLanguages").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.codedEntries.page"))
        .andExpect(status().isOk())
        .andExpect(model().attribute("nonVerbalLanguageCategoryId", -1L))
        .andExpect(model().attribute("nonVerbalLanguageCategoryCode",
            "com.olmgroup.usp.components.person.category.nonVerbalLanguages"))
        .andExpect(model().attribute("nonVerbalLanguageSystem", true))
        .andExpect(model().attribute("nonVerbalLanguageAdditionsAllowed", true))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewBelongingsType() throws Exception {
    getMockMvc().perform(get("/admin/belongingsType").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.codedEntries.page"))
        .andExpect(model().attribute("belongingsTypeCategoryId", -1L))
        .andExpect(model().attribute("belongingsTypeCategoryCode",
            "com.olmgroup.usp.components.belongings.category.type"))
        .andExpect(model().attribute("belongingsTypeSystem", false))
        .andExpect(model().attribute("belongingsTypeAdditionsAllowed", true))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewBelongingsLocation() throws Exception {
    getMockMvc().perform(get("/admin/belongingsLocation").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.codedEntries.page"))
        .andExpect(model().attribute("belongingsLocationCategoryId", -1L))
        .andExpect(model().attribute("belongingsLocationCategoryCode",
            "com.olmgroup.usp.components.belongings.category.location"))
        .andExpect(model().attribute("belongingsLocationSystem", false))
        .andExpect(model().attribute("belongingsLocationAdditionsAllowed", true))
        .andExpect(status().isOk());
  }

  @Override
  protected void handleTestViewClassificationEndReason() throws Exception {
    getMockMvc().perform(get("/admin/classificationEndReason").accept(MediaType.TEXT_HTML))
        .andExpect(view().name("admin.codedEntries.page"))
        .andExpect(model().attribute("classificationEndReasonCategoryId", -1L))
        .andExpect(model().attribute("classificationEndReasonCategoryCode",
            "com.olmgroup.usp.components.classification.category.endReason"))
        .andExpect(model().attribute("classificationEndReasonSystem", false))
        .andExpect(model().attribute("classificationEndReasonAdditionsAllowed", true))
        .andExpect(status().isOk());

  }
}
