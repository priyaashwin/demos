package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import javax.inject.Named;

@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development" })
public class PrimaryReferenceNumberContextResolverTest extends AbstractUnifiedTestBase {

  @Inject
  @Named("primaryReferenceNumberContextResolver")
  private PrimaryReferenceNumberContextResolver primaryReferenceNumberContextResolver;

  @Before
  public void setUp() {
    login("admin", "admin123");
  }

  @Test
  public void testPrimaryReferenceNumberContextIsDefault() throws Exception {
    assertThat(primaryReferenceNumberContextResolver.resolvePrimaryReferenceNumberContext(),
        equalTo(PrimaryReferenceNumberContext.DEFAULT));
  }

  @Test
  public void testSetPrimaryReferenceNumberContextToCHI() throws Exception {
    usp().getContextAttributesHandler().set(PrimaryReferenceNumberContextResolver.PRIMARY_REFERENCE_NUMBER_CONTEXT,
        PrimaryReferenceNumberContext.CHI.toString());
    assertThat(primaryReferenceNumberContextResolver.resolvePrimaryReferenceNumberContext(),
        equalTo(PrimaryReferenceNumberContext.CHI));
  }

}
