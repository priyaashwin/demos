// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: component::com.olmgroup.usp.components.organisation::service::OrganisationAddressService
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.apps.organisation.service;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.components.organisation.exception.OrganisationNotFoundException;
import com.olmgroup.usp.components.organisation.service.OrganisationAddressService;
import com.olmgroup.usp.components.organisation.vo.NewOrganisationAddressVO;
import com.olmgroup.usp.components.organisation.vo.OrganisationAddressVO;
import com.olmgroup.usp.facets.fuzzydate.types.SimpleFuzzyDate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Month;

import javax.validation.ConstraintViolationException;

/**
 * @see com.olmgroup.usp.components.organisation.service.OrganisationAddressService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class OrganisationAddressServiceTest extends
    OrganisationAddressServiceTestBase {
  @Before
  public void handleInitializeTestSuite() {
    login("admin", "admin123");
  }

  @Override
  protected void handleTestAddOrganisationAddress() throws Exception {
    logout();
    login("super", "testpassword");
    SimpleFuzzyDate startDate = new SimpleFuzzyDate(2012, Month.JUNE, 5);
    SimpleFuzzyDate endDate = new SimpleFuzzyDate(2013, Month.JUNE, 5);

    OrganisationAddressService organisationAddressService = getOrganisationAddressService();
    NewOrganisationAddressVO newOrganisationAddressVO = new NewOrganisationAddressVO();

    newOrganisationAddressVO.setStartDate(startDate);
    newOrganisationAddressVO.setEndDate(endDate);
    newOrganisationAddressVO.setOrganisationId(Long.valueOf(-10001));
    newOrganisationAddressVO.setLocationId(Long.valueOf(-10));
    newOrganisationAddressVO.setType("HOME");
    newOrganisationAddressVO.setUsage("PLACEMENT");

    long newOrganisationAddressId = organisationAddressService
        .addOrganisationAddress(newOrganisationAddressVO);
    Assert.assertTrue(newOrganisationAddressId > 0);

  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = {
      "/dbunit/OrganisationAddressService/OrganisationAddressService.setup.xml",
      "/dbunit/OrganisationAddressService/addOrganisationAddress.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddOrganisationAddress_WithoutType() throws Exception {
    logout();
    login("super", "testpassword");
    SimpleFuzzyDate startDate = new SimpleFuzzyDate(2012, Month.JUNE, 5);
    SimpleFuzzyDate endDate = new SimpleFuzzyDate(2013, Month.JUNE, 5);

    NewOrganisationAddressVO newOrganisationAddressVO = new NewOrganisationAddressVO();
    newOrganisationAddressVO.setStartDate(startDate);
    newOrganisationAddressVO.setEndDate(endDate);
    newOrganisationAddressVO.setOrganisationId(Long.valueOf(-10001));

    getOrganisationAddressService().addOrganisationAddress(
        newOrganisationAddressVO);
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/OrganisationAddressService/OrganisationAddressService.setup.xml", "/dbunit/OrganisationAddressService/findPreferredOrganisationAddressByOrganisationId.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testFindPreferredByOrganisationId_EMERGENCYPERMANENT_tieOnType_tieOnUsage_selectedOnCreatedDate() throws Exception {
    final OrganisationAddressVO organisationAddressVO = getOrganisationAddressService().findPreferredOrganisationAddressByOrganisationId(506);

    Assert.assertNotNull(organisationAddressVO);
    Assert.assertEquals(-9, organisationAddressVO.getId());
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/OrganisationAddressService/OrganisationAddressService.setup.xml", "/dbunit/OrganisationAddressService/findPreferredOrganisationAddressByOrganisationId.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testFindPreferredByOrganisationId_EMERGENCYPERMANENT_tieOnType_selectedOnUsage() throws Exception {
    final OrganisationAddressVO organisationAddressVO = getOrganisationAddressService().findPreferredOrganisationAddressByOrganisationId(507);

    Assert.assertNotNull(organisationAddressVO);
    Assert.assertEquals(-15, organisationAddressVO.getId());
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/OrganisationAddressService/OrganisationAddressService.setup.xml", "/dbunit/OrganisationAddressService/findPreferredOrganisationAddressByOrganisationId.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testFindPreferredByOrganisationId_PLACEMENTTEMPORARY_selectedOnType() throws Exception {
    final OrganisationAddressVO organisationAddressVO = getOrganisationAddressService().findPreferredOrganisationAddressByOrganisationId(509);

    Assert.assertNotNull(organisationAddressVO);
    Assert.assertEquals(-23, organisationAddressVO.getId());
  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/OrganisationAddressService/OrganisationAddressService.setup.xml", "/dbunit/OrganisationAddressService/findPreferredOrganisationAddressByOrganisationId.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testFindPreferredByOrganisationId_EMERGENCYTEMPORARY_selectedOnType() throws Exception {
    final OrganisationAddressVO organisationAddressVO = getOrganisationAddressService().findPreferredOrganisationAddressByOrganisationId(508);

    Assert.assertNotNull(organisationAddressVO);
    Assert.assertEquals(-19, organisationAddressVO.getId());
  }

  @Test(expected = OrganisationNotFoundException.class)
  @DatabaseSetup(
      value = { "/dbunit/OrganisationAddressService/OrganisationAddressService.setup.xml", "/dbunit/OrganisationAddressService/findPreferredOrganisationAddressByOrganisationId.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testFindPreferredByOrganisationId_OrganisationNotFound() throws Exception {
    getOrganisationAddressService().findPreferredOrganisationAddressByOrganisationId(999);

  }

}