// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: component::com.olmgroup.usp.components.casenote::service::CaseNoteEntryAttachmentService
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.apps.casenote.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.components.casenote.service.CaseNoteEntryAttachmentService;
import com.olmgroup.usp.components.casenote.service.TemporaryAttachmentService;
import com.olmgroup.usp.components.casenote.vo.NewCaseNoteEntryAttachmentVO;
import com.olmgroup.usp.components.casenote.vo.NewGroupTemporaryAttachmentVO;
import com.olmgroup.usp.components.casenote.vo.NewPersonTemporaryAttachmentVO;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.ByteArrayInputStream;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * @see com.olmgroup.usp.components.casenote.service.CaseNoteEntryAttachmentService
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
public class CaseNoteEntryAttachmentServiceTest extends
    CaseNoteEntryAttachmentServiceTestBase {
  private static final String FILE_SCHEME = "file";
  private static final String contents = "The only difference between the saint and the sinner is that every saint has a past, and every sinner has a future.";

  @Inject
  @Named("temporaryAttachmentService")
  private TemporaryAttachmentService temporaryAttachmentService;

  protected final TemporaryAttachmentService getTemporaryAttachmentService() {
    return temporaryAttachmentService;
  }

  @Before
  public final void handleInitializeTestSuite() {
    login("admin", "admin123");
  }

  /**
   * @see com.olmgroup.usp.components.casenote.service.CaseNoteEntryAttachmentServiceTest#testAddCaseNoteEntryAttachment()
   */
  @Override
  protected final void handleTestAddCaseNoteEntryAttachment() throws Exception {

    final CaseNoteEntryAttachmentService caseNoteEntryAttachmentService = getCaseNoteEntryAttachmentService();

    final long caseNoteEntryAttachmentId;
    // content for attachment
    final byte[] file = contents.getBytes();

    final NewCaseNoteEntryAttachmentVO newAttachment = new NewCaseNoteEntryAttachmentVO();
    newAttachment.setCaseNoteEntryId(-1000);
    newAttachment.setContentType("text/plain");
    newAttachment.setFilename("testFile.txt");
    newAttachment.setScheme(FILE_SCHEME);
    newAttachment.setSize(file.length);
    newAttachment.setInline(true);
    // set the input stream
    newAttachment.setInputStream(new ByteArrayInputStream(file));

    newAttachment.setAttachmentVersion("1.0");
    newAttachment.setDescription("This is my description");
    newAttachment.setTitle("This is my title");

    caseNoteEntryAttachmentId = caseNoteEntryAttachmentService
        .addCaseNoteEntryAttachment(newAttachment);

    Assert.assertTrue(caseNoteEntryAttachmentId > 0);

    // check VO checkCaseNoteEntryAttachmentVOs( newAttachment, file,
    Assert.assertNotNull(caseNoteEntryAttachmentService
        .findById(caseNoteEntryAttachmentId));

  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/CaseNoteEntryAttachmentService/CaseNoteEntryAttachmentService.setup.xml",
          "/dbunit/CaseNoteEntryAttachmentService/addCaseNoteEntryAttachment.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testAddCaseNoteEntryAttachment_NotInlineAttachment_TitleConstraint() throws Exception {

    final CaseNoteEntryAttachmentService caseNoteEntryAttachmentService = getCaseNoteEntryAttachmentService();

    // content for attachment
    final byte[] file = contents.getBytes();

    final NewCaseNoteEntryAttachmentVO newAttachment = new NewCaseNoteEntryAttachmentVO();
    newAttachment.setCaseNoteEntryId(-1000);
    newAttachment.setContentType("text/plain");
    newAttachment.setFilename("testFile.txt");
    newAttachment.setScheme(FILE_SCHEME);
    newAttachment.setSize(file.length);

    // set the input stream
    newAttachment.setInputStream(new ByteArrayInputStream(file));
    newAttachment.setAttachmentVersion("1.0");
    newAttachment.setDescription("description");
    newAttachment.setInline(false);
    try {
      caseNoteEntryAttachmentService
          .addCaseNoteEntryAttachment(newAttachment);
      Assert.fail("Should throw validation error");
    } catch (ConstraintViolationException exception) {
      final Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();
      assertThat(constraintViolations.size(), equalTo(1));
      ConstraintViolation<?> contraintViolation = constraintViolations
          .iterator().next();
      assertThat(contraintViolation.getPropertyPath().toString(), equalTo("title"));
      assertThat(contraintViolation.getConstraintDescriptor()
          .getAnnotation(), IsInstanceOf.instanceOf(org.hibernate.validator.constraints.NotBlank.class));
    }

  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/CaseNoteEntryAttachmentService/CaseNoteEntryAttachmentService.setup.xml",
          "/dbunit/CaseNoteEntryAttachmentService/addCaseNoteEntryAttachment.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testAddCaseNoteEntryAttachment_NotInlineAttachment_DescriptionConstraint() throws Exception {

    final CaseNoteEntryAttachmentService caseNoteEntryAttachmentService = getCaseNoteEntryAttachmentService();

    // content for attachment
    final byte[] file = contents.getBytes();

    final NewCaseNoteEntryAttachmentVO newAttachment = new NewCaseNoteEntryAttachmentVO();
    newAttachment.setCaseNoteEntryId(-1000);
    newAttachment.setContentType("text/plain");
    newAttachment.setFilename("testFile.txt");
    newAttachment.setScheme(FILE_SCHEME);
    newAttachment.setSize(file.length);

    // set the input stream
    newAttachment.setInputStream(new ByteArrayInputStream(file));
    newAttachment.setAttachmentVersion("1.0");
    newAttachment.setTitle("title");
    newAttachment.setInline(false);
    try {
      caseNoteEntryAttachmentService
          .addCaseNoteEntryAttachment(newAttachment);
      Assert.fail("Should throw validation error");
    } catch (ConstraintViolationException exception) {
      final Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();
      assertThat(constraintViolations.size(), equalTo(1));
      ConstraintViolation<?> contraintViolation = constraintViolations
          .iterator().next();
      assertThat(contraintViolation.getPropertyPath().toString(), equalTo("description"));
      assertThat(contraintViolation.getConstraintDescriptor()
          .getAnnotation(), IsInstanceOf.instanceOf(org.hibernate.validator.constraints.NotBlank.class));
    }

  }

  @Test
  @DatabaseSetup(
      value = { "/dbunit/CaseNoteEntryAttachmentService/CaseNoteEntryAttachmentService.setup.xml",
          "/dbunit/CaseNoteEntryAttachmentService/addCaseNoteEntryAttachment.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testAddCaseNoteEntryAttachment_InlineAttachment() throws Exception {

    final CaseNoteEntryAttachmentService caseNoteEntryAttachmentService = getCaseNoteEntryAttachmentService();

    final long caseNoteEntryAttachmentId;
    // content for attachment
    final byte[] file = contents.getBytes();

    final NewCaseNoteEntryAttachmentVO newAttachment = new NewCaseNoteEntryAttachmentVO();
    newAttachment.setCaseNoteEntryId(-1000);
    newAttachment.setContentType("text/plain");
    newAttachment.setFilename("testFile.txt");
    newAttachment.setScheme(FILE_SCHEME);
    newAttachment.setSize(file.length);

    // set the input stream
    newAttachment.setInputStream(new ByteArrayInputStream(file));

    newAttachment.setAttachmentVersion("1.0");
    newAttachment.setInline(true);

    caseNoteEntryAttachmentId = caseNoteEntryAttachmentService
        .addCaseNoteEntryAttachment(newAttachment);
    // Should not trigger any constraint violations on the title and description for inline attachment
    Assert.assertTrue(caseNoteEntryAttachmentId > 0);

    // check VO checkCaseNoteEntryAttachmentVOs( newAttachment, file,
    Assert.assertNotNull(caseNoteEntryAttachmentService
        .findById(caseNoteEntryAttachmentId));

  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/TemporaryAttachmentService/TemporaryAttachmentService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testAddTemporaryAttachmentForPerson_Validation() throws Exception {
    final TemporaryAttachmentService temporaryAttachmentService = getTemporaryAttachmentService();

    final long temporaryAttachmentId;
    // content for attachment
    final byte[] file = contents.getBytes();

    final NewPersonTemporaryAttachmentVO newAttachment = new NewPersonTemporaryAttachmentVO();
    newAttachment.setPersonId(-500);
    newAttachment.setContentType("text/plain");
    newAttachment.setFilename("testFile.txt");
    newAttachment.setScheme(FILE_SCHEME);
    newAttachment.setSize(file.length);

    // set the input stream
    newAttachment.setInputStream(new ByteArrayInputStream(file));

    newAttachment.setAttachmentVersion("1.0");

    temporaryAttachmentId = temporaryAttachmentService
        .addTemporaryAttachmentForPerson(newAttachment);

    Assert.fail("Expected constraint violation exception");
  }

  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(
      value = { "/dbunit/TemporaryAttachmentService/TemporaryAttachmentService.setup.xml" },
      type = DatabaseOperation.CLEAN_INSERT)
  public final void testAddTemporaryAttachmentForGroup_Validation() throws Exception {
    final TemporaryAttachmentService temporaryAttachmentService = getTemporaryAttachmentService();

    final long temporaryAttachmentId;
    // content for attachment
    final byte[] file = contents.getBytes();

    final NewGroupTemporaryAttachmentVO newAttachment = new NewGroupTemporaryAttachmentVO();
    newAttachment.setGroupId(-1);
    newAttachment.setContentType("text/plain");
    newAttachment.setFilename("testFile.txt");
    newAttachment.setScheme(FILE_SCHEME);
    newAttachment.setSize(file.length);

    // set the input stream
    newAttachment.setInputStream(new ByteArrayInputStream(file));

    newAttachment.setAttachmentVersion("1.0");

    temporaryAttachmentId = temporaryAttachmentService
        .addTemporaryAttachmentForGroup(newAttachment);

    Assert.fail("Expected constraint violation exception");
  }

}