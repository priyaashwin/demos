package com.olmgroup.usp.apps.person.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonGroupMembershipDetailsWithAddressWarningCountVO;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PersonGroupMembershipServiceTest extends PersonGroupMembershipServiceTestBase {

  @Before
  public void handleInitializeTestSuite() {
    login("admin", "admin123");
  }

  @Override
  protected void handleTestFindById() throws Exception {

    PersonGroupMembershipDetailsWithAddressWarningCountVO personGroupMembershipDetailsWithAddressWarningCountVO = getPersonGroupMembershipService()
        .findById(-3, PersonGroupMembershipDetailsWithAddressWarningCountVO.class);

    assertThat("Address text is incorrect for address with no location",
        personGroupMembershipDetailsWithAddressWarningCountVO.getAddress(), equalTo("Address not known"));

    assertThat("Address should be Do Not Disclose",
        personGroupMembershipDetailsWithAddressWarningCountVO.getDoNotDisclose(), equalTo(true));

    personGroupMembershipDetailsWithAddressWarningCountVO = getPersonGroupMembershipService()
        .findById(-4, PersonGroupMembershipDetailsWithAddressWarningCountVO.class);
    assertThat("Address should NOT be Do Not Disclose",
        personGroupMembershipDetailsWithAddressWarningCountVO.getDoNotDisclose(), equalTo(false));
  }

}
