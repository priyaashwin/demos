//license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: component::com.olmgroup.usp.components.person::service::PersonService
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.apps.person.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThan;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.apps.relationshipsrecording.domain.PersonHeaderDetailsVO;
import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonAccessWarningCountVO;
import com.olmgroup.usp.components.access.service.PersonAccessService;
import com.olmgroup.usp.components.contact.exception.InvalidContactVerificationDateException;
import com.olmgroup.usp.components.contact.vo.ContactVO;
import com.olmgroup.usp.components.contact.vo.EmailContactVO;
import com.olmgroup.usp.components.contact.vo.NewContactVO;
import com.olmgroup.usp.components.contact.vo.SocialMediaContactVO;
import com.olmgroup.usp.components.contact.vo.TelephoneContactVO;
import com.olmgroup.usp.components.contact.vo.UpdateContactVO;
import com.olmgroup.usp.components.contact.vo.WebContactVO;
import com.olmgroup.usp.components.person.domain.LifeState;
import com.olmgroup.usp.components.person.exception.DiedDateBeforeDateOfBirthException;
import com.olmgroup.usp.components.person.exception.DuplicateNhsNumberException;
import com.olmgroup.usp.components.person.exception.PersonNotFoundException;
import com.olmgroup.usp.components.person.service.PersonService;
import com.olmgroup.usp.components.person.vo.NewPersonVO;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.components.person.vo.PersonWarningVO;
import com.olmgroup.usp.components.person.vo.PersonWithNamesVO;
import com.olmgroup.usp.components.person.vo.UpdateNotCarriedPersonUnbornVO;
import com.olmgroup.usp.components.person.vo.UpdatePersonVO;
import com.olmgroup.usp.facets.configuration.validation.CodedEntryInactiveException;
import com.olmgroup.usp.facets.configuration.validation.CodedEntryMissingException;
import com.olmgroup.usp.facets.fuzzydate.types.FuzzyDateMask;
import com.olmgroup.usp.facets.fuzzydate.types.SimpleFuzzyDate;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;
import com.olmgroup.usp.facets.test.util.TestUtils;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.CollectionUtils;

import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * @see com.olmgroup.usp.components.person.service.PersonService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonServiceTest extends PersonServiceTestBase {

  @Before
  public void handleInitializeTestSuite() {
    login("admin", "admin123");
  }

  @Inject
  private PersonAccessService personAccessService;

  /**
   * The starting of testing teh personHeaderDetails... There are more tests
   * needed here, but I added the one I really needed.
   *
   * @throws Exception
   */
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsVO() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(500, PersonHeaderDetailsVO.class);

    assertThat("Should get a personHeaderDetails back.", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedTeams().size(), equalTo(2));
    assertThat(personHeaderDetailsVO.getAllocatedTeams().get(0).getStartDate(),
        greaterThan(personHeaderDetailsVO.getAllocatedTeams().get(1).getStartDate()));

  }

  /**
   * The starting of testing the RecentlyAccessedPersons list.
   *
   * @throws Exception
   */

  // Test 1 When logged in as an Admin User with open access, recently accessed
  // person list will show all information.
  @Test
  @DatabaseSetup(value = {
      "/dbunit/PersonService/PersonAccessService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonAccessWarningCountVO_withAccessibleClient() throws Exception {

    // Logged in as admin; relational access is allowed
    final long accessor = -507L;
    final long accessedPerson = -503L;
    final String addressString = "10 ACACIA AVENUE, MIDDLESEX, TW15 2NS";

    final PersonAccessWarningCountVO personAccessWarningCountVO = this.personAccessService
        .findByAccessorAndAccessedPersonId(accessedPerson, accessor, PersonAccessWarningCountVO.class);
    assertThat(personAccessWarningCountVO, is(notNullValue()));

    // Check that access record has correct person
    assertThat(personAccessWarningCountVO.getAccessedPerson().getId(), is(accessedPerson));

    // Check warnings
    assertThat(personAccessWarningCountVO.getPersonWarnings(), is(notNullValue()));

    final List<PersonWarningVO> warningVOs = personAccessWarningCountVO.getPersonWarnings();

    assertThat(warningVOs.get(0).getId(), is(-1L));
    assertThat(warningVOs.get(0).getSubjectInformed(), is(true));
    assertThat(warningVOs.get(0).getDateSubjectInformed(), is(new DateTime(2015, 10, 11, 0, 0).toDate()));

    // Check the address string
    assertThat(personAccessWarningCountVO.getAddress(), is(addressString));

    // Check that all contacts are as expected
    assertThat(personAccessWarningCountVO.getContacts().size(), is(5));
    assertThat(personAccessWarningCountVO.getDoNotDisclose(), is(false));

    for (ContactVO contactVO : personAccessWarningCountVO.getContacts()) {
      if (contactVO instanceof EmailContactVO && contactVO.getContactType().equals("WORK")) {
        assertThat(((EmailContactVO) contactVO).getAddress(), equalTo("niels.bohr@ku.dk"));
      } else if (contactVO instanceof TelephoneContactVO) {
        final TelephoneContactVO telephoneContactVO = (TelephoneContactVO) contactVO;
        if (telephoneContactVO.getPreferred() && contactVO.getContactType().equals("WORK")
            && contactVO.getContactUsage().equals("PLACEMENT")) {
          assertThat(telephoneContactVO.getNumber(), equalTo("01865 270000"));
          assertThat(telephoneContactVO.getTelephoneType(), equalTo("LANDLINE"));
        } else if (telephoneContactVO.getTelephoneType().equals("MOBILE")
            && contactVO.getContactType().equals("WORK")
            && contactVO.getContactUsage().equals("PLACEMENT")) {
          assertThat(telephoneContactVO.getNumber(), equalTo("07790 588233"));
        }
      } else if (contactVO instanceof WebContactVO) {
        assertThat(((WebContactVO) contactVO).getAddress(), equalTo("home.web.cern.ch"));
      } else if (contactVO instanceof SocialMediaContactVO) {
        assertThat(((SocialMediaContactVO) contactVO).getIdentifier(), equalTo("@cambridge_uni"));
        assertThat(((SocialMediaContactVO) contactVO).getSocialMediaType(), equalTo("TWITTER"));
      }
    }

    // Check that accessAllowed is set to True.
    assertThat(personAccessWarningCountVO.getAccessAllowed(), is(true));
  }

  // Test 2 When logged in as a user with no relational access allowed, recently
  // accessed person list will hide secure attributes like
  // Address, Postcode and Contacts.
  @Test
  @DatabaseSetup(value = {
      "/dbunit/PersonService/PersonAccessService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonAccessWarningCountVO_withNoAccessToClient() throws Exception {

    // Logged in as gopher; relational access is not allowed
    this.logout();
    this.login("gopher", "testpassword");

    final long accessor = -507L;
    final long accessedPerson = -503L;

    final PersonAccessWarningCountVO personAccessWarningCountVO = this.personAccessService
        .findByAccessorAndAccessedPersonId(accessedPerson, accessor, PersonAccessWarningCountVO.class);

    // Check that access record is returned
    assertThat(personAccessWarningCountVO, is(notNullValue()));

    // Check that access record has correct person
    assertThat(personAccessWarningCountVO.getAccessedPerson().getId(), is(accessedPerson));

    // Check warnings
    assertThat(personAccessWarningCountVO.getPersonWarnings(), is(notNullValue()));

    final List<PersonWarningVO> warningVOs = personAccessWarningCountVO.getPersonWarnings();

    assertThat(warningVOs.get(0).getId(), is(-1L));
    assertThat(warningVOs.get(0).getSubjectInformed(), is(true));
    assertThat(warningVOs.get(0).getDateSubjectInformed(), is(new DateTime(2015, 10, 11, 0, 0).toDate()));

    // Since Address, Postcode and Contacts are secured, check that these attributes
    // have returned value as null
    assertThat(personAccessWarningCountVO.getAddress(), is(nullValue()));
    assertThat(personAccessWarningCountVO.getPostcode(), is(nullValue()));
    assertThat(personAccessWarningCountVO.getContacts().isEmpty(), is(true));

    // Check that accessAllowed is set to False.
    assertThat(personAccessWarningCountVO.getAccessAllowed(), is(false));
    /// Check that doNotDisclose is set to false because it is set as false in the
    /// database.
    assertThat(personAccessWarningCountVO.getDoNotDisclose(), is(false));

  }

  // Test 3 When logged in as a user with no relational access allowed, and person
  // has a type of 'CLIENT' and 'PROFESSIONAL', recently accessed person list will
  // hide Contacts.
  // However work address will still be returned.
  @Test
  @DatabaseSetup(value = {
      "/dbunit/PersonService/PersonAccessService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonAccessWarningCountVO_withNoAccessToClient_withWorAddress() throws Exception {

    // Logged in as gopher; relational access is not allowed
    this.logout();
    this.login("gopher", "testpassword");

    final long accessor = -507L;
    final long accessedPerson = -504L;
    final String homeAddressString = "15 VIOLA AVENUE, MIDDLESEX, TW15 2NN";
    final String workAddressString = "20 STATION ROAD, MIDDLESEX, TW15 2SS";

    final PersonAccessWarningCountVO personAccessWarningCountVO = this.personAccessService
        .findByAccessorAndAccessedPersonId(accessedPerson, accessor, PersonAccessWarningCountVO.class);

    // Check that access record is returned
    assertThat(personAccessWarningCountVO, is(notNullValue()));

    // Check that access record has correct person
    assertThat(personAccessWarningCountVO.getAccessedPerson().getId(), is(accessedPerson));

    // Since Contacts are secured and has a type of 'HOME', check that these are
    // returned as null
    assertThat(personAccessWarningCountVO.getContacts().isEmpty(), is(true));

    // Check that address with type 'WORK' is still returned
    assertThat(personAccessWarningCountVO.getAddress(), is(workAddressString));

    // Check that accessAllowed is set to False.
    assertThat(personAccessWarningCountVO.getAccessAllowed(), is(false));
    // Check that doNotDisclose is set to false because it is not set in the
    // database.
    assertThat(personAccessWarningCountVO.getDoNotDisclose(), is(false));
  }

  @Test
  @DatabaseSetup(value = {
      "/dbunit/PersonService/PersonAccessService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonAccessWarningCountVO_withDoNotDisclose() throws Exception {

    // Logged in as gopher; relational access is not allowed
    this.logout();
    this.login("gopher", "testpassword");

    final long accessor = -507L;
    final long accessedPerson = -505L;

    final PersonAccessWarningCountVO personAccessWarningCountVO = this.personAccessService
        .findByAccessorAndAccessedPersonId(accessedPerson, accessor, PersonAccessWarningCountVO.class);
    // Check that accessAllowed is set to False.
    assertThat(personAccessWarningCountVO.getAccessAllowed(), is(false));
    // Check that doNotDisclose is set to true because it is set in the
    // database as true.
    assertThat(personAccessWarningCountVO.getDoNotDisclose(), is(true));
  }

  // Test 1 - Add a person with all attributes populated
  @SuppressWarnings("unchecked")
  // Result - The person is added
  @Override
  protected final void handleTestAddPerson() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, Month.NOVEMBER, 29);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setTitle("MR");
    newPersonVO.setForename("John");
    newPersonVO.setMiddleNames("Graham");
    newPersonVO.setKnownAs("JG");
    newPersonVO.setAlternateNames("Jonathan");
    newPersonVO.setSurname("Mellor");
    newPersonVO.setSuffix("C.D.");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));

    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getTitle(), personVO.getTitle());
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getMiddleNames(), personVO.getMiddleNames());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getSuffix(), personVO.getSuffix());
    Assert.assertEquals(newPersonVO.getDateOfBirth(), personVO.getDateOfBirth());
    Assert.assertEquals(newPersonVO.getGender(), personVO.getGender());
    Assert.assertEquals(newPersonVO.getEthnicity(), personVO.getEthnicity());
    Assert.assertEquals(newPersonVO.getKnownAs(), personVO.getKnownAs());
    Assert.assertEquals(newPersonVO.getAlternateNames(), personVO.getAlternateNames());
  }

  // Test 2 - Add a person with null date of birth for other
  @SuppressWarnings("unchecked")
  // Result - The person is added
  @Test
  public void handleTestAddPersonWithNullDOB() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(FuzzyDateMask.NONE, null);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);
    final PersonVO personVO = personService.findById(personId);
    Assert.assertNull(personVO.getTitle());
    Assert.assertNull(personVO.getMiddleNames());
    Assert.assertEquals("wain", personVO.getSurname());
    Assert.assertNull(personVO.getSuffix());
    Assert.assertNull(personVO.getDateOfBirth().getCalculatedDate());
    Assert.assertNull(personVO.getGender());
    Assert.assertNull(personVO.getEthnicity());
  }

  // Test 3 - Add a person with Fuzzy year only. Estimated flag is set to true
  @SuppressWarnings("unchecked")
  // Result - The calculated date will be set to the last
  // second-minute-day-month-year of the year provided.
  // Even though the DOB is estimated to a year, as the DOB estimated flag is
  // set to true, we indicate the DOB is accurate (not estimated)
  @Test
  public void handleTestAddPersonWithFuzzyYearOnly() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, null, null);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("To Year");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(true);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getDateOfBirth().getCalculatedDate(),
        new DateTime(2012, 12, 31, 00, 00, 00).toDate());
    Assert.assertEquals(newPersonVO.getDateOfBirth().getFuzzyDateMask(), FuzzyDateMask.YEAR);
    Assert.assertEquals(newPersonVO.getDateOfBirthEstimated(), true);
  }

  // Test 4 - Add a person with Fuzzy year and month only. Estimated flag set
  @SuppressWarnings("unchecked")
  // to true
  // Result - The calculated date will be set to the last
  // second-minute-day-month-year of the month provided
  // Even though the DOB is estimated to a month, as the DOB estimated flag is
  // set to true, we indicate the DOB is accurate (not estimated)
  @Test
  public void handleTestAddPersonWithFuzzyYearAndMonthOnly() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, null);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("To Month");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(true);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getDateOfBirth().getCalculatedDate(),
        new DateTime(1978, 02, 28, 00, 00, 00).toDate());
    Assert.assertEquals(newPersonVO.getDateOfBirth().getFuzzyDateMask(), FuzzyDateMask.MONTH);
    Assert.assertEquals(newPersonVO.getDateOfBirthEstimated(), true);
  }

  // Test 5 - Add a person with Fuzzy year,month and day only. The estimated
  @SuppressWarnings("unchecked")
  // flag is initially set to false.
  // Result - The calculated date will be set to the last
  // second-minute-day-month-year of the date provided.
  // Also, the estimated flag is set to false as the date provided is exact to
  // the day, we leave the flag as is.
  @Test
  public void handleTestAddPersonWithFuzzyYearAndMonthAndDayOnlyEstimatedEqualsFalse() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("To Day");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(false);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getDateOfBirth().getCalculatedDate(),
        new DateTime(1978, 02, 13, 00, 00, 00).toDate());
    Assert.assertEquals(newPersonVO.getDateOfBirth().getFuzzyDateMask(), FuzzyDateMask.DAY);
    Assert.assertEquals(newPersonVO.getDateOfBirthEstimated(), false);
  }

  // Test 6 - Add a person with estimated DOB set to true and the FuzzyDate
  @SuppressWarnings("unchecked")
  // estimated to the DAY
  // Result - Estimated DOB flag is true, therefore, we accept the user input
  @Test
  public void handleTestAddPersonEstimatedDOBSetTrueDOBMaskDay() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Estimated");
    newPersonVO.setSurname("True");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(true);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(true, personVO.getDateOfBirthEstimated());
  }

  // Test 7 - Add a person with estimated DOB to the year with the estimated
  @SuppressWarnings("unchecked")
  // flag set to false
  // Result - As the estimated DOB is not exact to a day, we will set the
  // estimated flag to true
  @Test
  public void handleTestAddPersonEstimatedDOBSetFalseDOBMaskYear() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, null, null);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Estimated");
    newPersonVO.setSurname("false");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(false);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(true, personVO.getDateOfBirthEstimated());
  }

  // Test 8 - Add a person with estimated DOB to the month with the estimated
  @SuppressWarnings("unchecked")
  // flag set to false
  // Result - As the DOB is not exact to the day, we update the flag to true
  // as it is an estimate
  @Test
  public void handleTestAddPersonEstimatedDOBSetFalseDOBMaskMonth() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, null);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Estimated");
    newPersonVO.setSurname("false");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(false);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(true, personVO.getDateOfBirthEstimated());
  }

  // Test 9 - Add a person with estimated DOB to the year with the estimated
  @SuppressWarnings("unchecked")
  // flag set to null
  // Result - As the estimated DOB is not exact to a day, we update the
  // estimated flag to true
  @Test
  public void handleTestAddPersonEstimatedDOBSetNullDOBMaskYear() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, null, null);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Estimated");
    newPersonVO.setSurname("null");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(true, personVO.getDateOfBirthEstimated());
  }

  // Test 10 - Add a person with estimated DOB to the month with the estimated
  // flag set to null
  // Result - As the DOB is not exact to the day, therefore we update the
  // estimate to true
  @SuppressWarnings("unchecked")
  @Test
  public void handleTestAddPersonEstimatedDOBSetNullDOBMaskMonth() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, null);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Estimated");
    newPersonVO.setSurname("Null");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(true, personVO.getDateOfBirthEstimated());
  }

  // Test 11 - Add a person with estimated DOB to the day with the estimated
  // flag set to null
  // Result - As the DOB is exact to the day, but no explicit value is
  // indicated for whether it is estimated, the estimated flag remains null
  @Test
  public void handleTestAddPersonEstimatedDOBSetNullDOBMaskDay() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Estimated");
    newPersonVO.setSurname("Null");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertNull(personVO.getDateOfBirthEstimated());
  }

  // Test 12 - Add a person with NHS Number
  // Result - A new person is added successfully with given NHS Number
  @SuppressWarnings("unchecked")
  @Test
  public void handleTestAddPersonWithNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("NhsNumber");
    newPersonVO.setSurname("Added");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("9434765919");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getNhsNumber(), personVO.getNhsNumber());

  }

  // Test 13 - Add two people with same NHS Number (default checkNhsNumber is
  @SuppressWarnings("unchecked")
  // false)
  // Result - Two new people is added successfully with the same NHS Number
  @Test
  public void handleTestAddPersonWithMultipleSameNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("MultipleSameNhsNumber1");
    newPersonVO.setSurname("FirstPerson");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("9434765919");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    long personId = personService.addPerson(newPersonVO, false);

    PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getNhsNumber(), personVO.getNhsNumber());

    // Second person with same NHS number
    newPersonVO = new NewPersonVO();
    newPersonVO.setForename("MultipleSameNhsNumber2");
    newPersonVO.setSurname("SecondPerson");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("9434765919");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));

    personId = personService.addPerson(newPersonVO, false);

    personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getNhsNumber(), personVO.getNhsNumber());

  }

  // Test 14 - Add two people with same NHS Number and checkNhsNumber is TRUE
  @SuppressWarnings("unchecked")
  // Result - First person is added successfully, second person with same NHS
  // number of first person throws DuplicateNhsNumberException Exception
  @Test(expected = DuplicateNhsNumberException.class)
  public void handleTestAddPersonWithUniqueNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("UniqueNhsNumber1");
    newPersonVO.setSurname("FirstPerson");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("9434765919");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, true);

    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals(newPersonVO.getForename(), personVO.getForename());
    Assert.assertEquals(newPersonVO.getSurname(), personVO.getSurname());
    Assert.assertEquals(newPersonVO.getNhsNumber(), personVO.getNhsNumber());

    // Second person with same NHS number
    newPersonVO = new NewPersonVO();
    newPersonVO.setForename("UniqueNhsNumber2");
    newPersonVO.setSurname("SecondPerson");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("9434765919");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    personService.addPerson(newPersonVO, true);

  }

  // Test 15 - Add a person with NHS Number digits more than 10
  // Result - The operation fails with Bad Request Error Code
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithNHSNumberMoreThan10Digits() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("LongNHSNumber");
    newPersonVO.setSurname("NHSNumberTest");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("123456789122222222");

    final PersonService personService = getPersonService();

    personService.addPerson(newPersonVO, true);

  }

  // Test 16 - Add a person with NHS Number non-digit characters
  @SuppressWarnings("unchecked")
  // Result - The operation fails with Bad Request Error Code
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithNHSNumberIncludesNonDigitCharacters() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("LongNHSNumber");
    newPersonVO.setSurname("NHSNumberTest");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("K?_;567CH12");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    personService.addPerson(newPersonVO, true);

  }

  // Test 17 - Add a person with not valid NHS Number
  @SuppressWarnings("unchecked")
  // Result - The operation raises ConstraintViolationException
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithNotValidNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("NotValidNHSNumber");
    newPersonVO.setSurname("NHSNumberTest");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setNhsNumber("1111111112");
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    final PersonService personService = getPersonService();

    personService.addPerson(newPersonVO, true);

  }

  // Test 18 -Add a person with personType (Professional and Other)
  @Test
  public void handleTestAddPersonWithPersonType() throws Exception {

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    personTypes.add("PROFESSIONAL");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setProfessionalTitle("DOCTOR");
    newPersonVO.setOrganisationName("SURGERY");
    newPersonVO.setLifeState(LifeState.ALIVE);
    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertEquals("OTHER", personTypesAdded.get(0));
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(1));
  }

  // Test 19 add person with professional type and professional title and
  // organisation name
  @Test
  public void handleAddPersonProfessionalType() throws Exception {

    final List<String> personTypes = new ArrayList<String>();
    personTypes.add("PROFESSIONAL");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setProfessionalTitle("DOCTOR");
    newPersonVO.setOrganisationName("SURGERY");

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(0));
    Assert.assertEquals("DOCTOR", personVO.getProfessionalTitle());
    Assert.assertEquals("SURGERY", personVO.getOrganisationName());
  }

  // Test 20 add person with other type duplicate
  @Test
  public void handleAddPersonOtheType_Duplicate() throws Exception {

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    personTypes.add("OTHER");
    personTypes.add("PROFESSIONAL");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setProfessionalTitle("DOCTOR");
    newPersonVO.setOrganisationName("SURGERY");
    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertEquals("OTHER", personTypesAdded.get(0));
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(1));
  }

  // Test 21 add person with person type codedEntry not found exception
  @Test(expected = CodedEntryMissingException.class)
  public void handleAddPersonAndPersonTypeCodedEntryNotFounException() throws Exception {

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHEEERRRR");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.ALIVE);
    getPersonService().addPerson(newPersonVO, false);

  }

  // Test 22 add person with person type codedEntry Inactive exception
  @Test(expected = CodedEntryInactiveException.class)
  public void handleAddPersonAndPersonTypeInactiveCodedEntryException() throws Exception {

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("TEMPORALYINACTIVE");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.ALIVE);
    getPersonService().addPerson(newPersonVO, false);

  }

  // Test 23 add person other and don't set name we expected
  // ConstraintViolationException
  // Missing surname
  @Test(expected = ConstraintViolationException.class)
  public void handleAddPersonOtherTypeContraintValidationException() throws Exception {

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setPersonTypes(personTypes);
    getPersonService().addPerson(newPersonVO, false);

  }

  // Test 24 add person with Professional and other type
  // Missing organisation name
  @Test(expected = ConstraintViolationException.class)
  public void handleAddPersonProfessionalOtherTypeContraintValidationException() throws Exception {

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("PROFESSIONAL");
    personTypes.add("OTHER");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setProfessionalTitle("DOCTOR");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.ALIVE);
    getPersonService().addPerson(newPersonVO, false);

  }

  // Test 25 add person type All types
  @Test
  public void handleAddPersonProfessionalAllType() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, Month.NOVEMBER, 29);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("PROFESSIONAL");
    personTypes.add("OTHER");
    personTypes.add("CLIENT");
    final NewPersonVO newPersonVO = new NewPersonVO();

    newPersonVO.setTitle("MR");
    newPersonVO.setForename("John");
    newPersonVO.setMiddleNames("Graham");
    newPersonVO.setSurname("Mellor");
    newPersonVO.setSuffix("C.D.");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setProfessionalTitle("DOCTOR");
    newPersonVO.setOrganisationName("SURGERY");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.ALIVE);
    getPersonService().addPerson(newPersonVO, false);

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);

    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertEquals("CLIENT", personTypesAdded.get(0));
    Assert.assertEquals("OTHER", personTypesAdded.get(1));
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(2));

    Assert.assertEquals("DOCTOR", personVO.getProfessionalTitle());
    Assert.assertEquals("SURGERY", personVO.getOrganisationName());
  }

  // Test 26 - Add a person with null date of birth will fail for
  // LifeState ALIVE and PersonType CLIENT
  @SuppressWarnings("unchecked")
  // Result - The person is added
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddAliveClientWithOutDOB() throws Exception {
    final SimpleFuzzyDate dateOfBirth = null;

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);
    final PersonVO personVO = personService.findById(personId);
    Assert.assertNull(personVO.getTitle());
    Assert.assertNull(personVO.getMiddleNames());
    Assert.assertEquals("wain", personVO.getSurname());
    Assert.assertNull(personVO.getSuffix());
    Assert.assertNotNull(personVO.getGender());
    Assert.assertNotNull(personVO.getEthnicity());
  }

  // Test 27 :Add Person should fail if no life state
  // available in supplied VO
  @SuppressWarnings("unchecked")
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithOutLifeState() throws Exception {
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);
    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals("wain", personVO.getSurname());
  }

  // Test 28 :Add Person should fail if no person type is added
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithOutPersonTypes() throws Exception {
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);
    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals("wain", personVO.getSurname());
  }

  @SuppressWarnings("unchecked")
  // Test 29: Add person should fail when due date is not available for Unborn
  // Client
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithDueDate_UnbornClient() throws Exception {
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.UNBORN);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);
    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals("wain", personVO.getSurname());
  }

  @SuppressWarnings("unchecked")
  // Test 30: Add person should fail when gender is not available for Unborn
  // Client or Alive ClientPersonType
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithOutGender_ClientPersonType() throws Exception {
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.UNBORN);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setEthnicity("WBRI");
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);
    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals("wain", personVO.getSurname());
  }

  @SuppressWarnings("unchecked")
  // Test 31: Add person should fail when ethnicity is not available for
  // Unborn Client or Alive ClientPersonType
  @Test(expected = ConstraintViolationException.class)
  public void handleTestAddPersonWithOutEthnicity_ClientPersonType() throws Exception {
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    final PersonService personService = getPersonService();

    final long personId = personService.addPerson(newPersonVO, false);
    final PersonVO personVO = personService.findById(personId);
    Assert.assertEquals("wain", personVO.getSurname());
  }

  @SuppressWarnings("unchecked")
  // Test 32: Add person should fail when ethnicity is not available for
  // Unborn Client or Alive ClientPersonType
  @Test
  public void handleTestAddPerson_LifeState_Validation() throws Exception {
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.UNBORN);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "OTHER" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    final PersonService personService = getPersonService();
    try {
      personService.addPerson(newPersonVO, false);
    } catch (ConstraintViolationException violationException) {
      final Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();

      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.RestrictedEnumeration);
      Assert.assertEquals("The UNBORN status cannot be applied to others.", constraintViolation.getMessage());
    }
  }

  @SuppressWarnings("unchecked")
  // Test 33: Test for unborn client DOB
  // Date of birth should be null for unborn client.
  @Test
  public void handleTestAddUnbornClientWithNullDOB() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, Month.NOVEMBER, 29);
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.UNBORN);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setDueDate(Calendar.getInstance().getTime());
    newPersonVO.setDateOfBirth(dateOfBirth);
    final PersonService personService = getPersonService();
    try {
      personService.addPerson(newPersonVO, false);
    } catch (ConstraintViolationException violationException) {
      final Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();

      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("Date of birth must be null.", constraintViolation.getMessage());
    }
  }

  @SuppressWarnings("unchecked")
  // Test 34: Test for unborn client DOB estimated
  // Date of birth estimates should be null for unborn client.
  @Test
  public void handleTestAddUnbornClientWithNullDOBEstimated() throws Exception {

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.UNBORN);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setDueDate(Calendar.getInstance().getTime());
    newPersonVO.setDateOfBirth(null);
    newPersonVO.setDateOfBirthEstimated(true);
    final PersonService personService = getPersonService();
    try {
      personService.addPerson(newPersonVO, false);
    } catch (ConstraintViolationException violationException) {
      final Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();

      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("Date of birth estimated must be null.", constraintViolation.getMessage());
    }
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, Month.NOVEMBER, 29);
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setDueDate(null);
    final long personId = personService.addPerson(newPersonVO, false);
    Assert.assertTrue(personId > -1);
  }

  @SuppressWarnings("unchecked")
  // Test 35: Test for unborn client nhs number
  // NHS number must be null for unborn client.
  @Test
  public void handleTestAddUnbornClientWithNullNHSNumber() throws Exception {

    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.UNBORN);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setDueDate(Calendar.getInstance().getTime());
    newPersonVO.setNhsNumber("9434765919");
    newPersonVO.setDateOfBirth(null);
    newPersonVO.setDateOfBirthEstimated(null);
    final PersonService personService = getPersonService();
    try {
      personService.addPerson(newPersonVO, false);
    } catch (ConstraintViolationException violationException) {
      final Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();

      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("NHS number must be null.", constraintViolation.getMessage());
    }
  }

  @SuppressWarnings("unchecked")
  // Test 36: Test for alive client, other, professional due date
  // Due date must be null for alive client, other, professional person type.
  @Test
  public void handleTestAddAliveClientOrOtherOrProfessionalWithNullDueDate() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, Month.NOVEMBER, 29);
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDueDate(Calendar.getInstance().getTime());
    final PersonService personService = getPersonService();
    try {
      personService.addPerson(newPersonVO, false);
    } catch (ConstraintViolationException violationException) {
      final Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();

      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("Due date must be null.", constraintViolation.getMessage());
    }
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "PROFESSIONAL" }));
    newPersonVO.setOrganisationName("OLM");
    newPersonVO.setProfessionalTitle("DOCTOR");
    try {
      personService.addPerson(newPersonVO, false);
    } catch (ConstraintViolationException violationException) {
      final Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();

      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("Due date must be null.", constraintViolation.getMessage());
    }
  }

  // Test 37: Test for before date not more than 280days
  // Result: Should throw validation on constraint @BeforeDate
  @SuppressWarnings("unchecked")
  @Test
  public void handleTestAddUnbornPerson_Morethan280Days() throws Exception {
    final Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, 285);
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setLifeState(LifeState.UNBORN);
    newPersonVO.setPersonTypes(CollectionUtils.arrayToList(new String[] { "CLIENT" }));
    newPersonVO.setForename("Fuzzy");
    newPersonVO.setSurname("wain");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setDueDate(cal.getTime());
    final PersonService personService = getPersonService();
    try {
      personService.addPerson(newPersonVO, false);
    } catch (ConstraintViolationException violationException) {
      final Set<ConstraintViolation<?>> constraintViolations = violationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();

      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.BeforeDate);
      Assert.assertEquals("The due date must be less than 280 days from the current date",
          constraintViolation.getMessage());
    }
  }

  // Test 1 - Update a person with all attributes populated
  // Result - The person is updated
  @Override
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePerson() throws Exception {

    final PersonService personService = getPersonService();

    final PersonVO personVO = personService.findById(600);

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, Month.NOVEMBER, 29);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setKnownAs("JG");
    updatePersonVO.setAlternateNames("Jonathan");
    updatePersonVO.setSuffix("C.D.");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getTitle(), updatedPersonVO.getTitle());
    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getMiddleNames(), updatedPersonVO.getMiddleNames());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getSuffix(), updatedPersonVO.getSuffix());
    Assert.assertEquals(updatePersonVO.getDateOfBirth(), updatedPersonVO.getDateOfBirth());
    Assert.assertEquals(updatePersonVO.getGender(), updatedPersonVO.getGender());
    Assert.assertEquals(updatePersonVO.getEthnicity(), updatedPersonVO.getEthnicity());
    Assert.assertEquals(updatePersonVO.getKnownAs(), updatedPersonVO.getKnownAs());
    Assert.assertEquals(updatePersonVO.getAlternateNames(), updatedPersonVO.getAlternateNames());
  }

  // Test 2 - Update a CLIENT person with date of birth as null
  // Result - Should throw constraint exception
  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithNullDOB() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(FuzzyDateMask.NONE, null);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertNull(updatedPersonVO.getDateOfBirth().getCalculatedDate());

  }

  // Test 3- Update a person of OTHER person type with date of birth as null
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdateOtherPersonWithNullDOB() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(803);

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(FuzzyDateMask.NONE, null);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertNull(updatedPersonVO.getDateOfBirth().getCalculatedDate());
    Assert.assertEquals(1, updatedPersonVO.getPersonTypes().size());
  }

  // Test 4 - Update a person with Fuzzy year only. Estimated flag is set to
  // true
  // Result - The calculated date will be set to the last
  // second-minute-day-month-year of the year provided.
  // Even though the DOB is estimated to a year, as the DOB estimated flag is
  // set to true, we indicate the DOB is accurate (not estimated)
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithFuzzyYearOnly() throws Exception {

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, null, null);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Fuzzy");
    updatePersonVO.setSurname("To Year");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(true);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getDateOfBirth().getCalculatedDate(),
        new DateTime(2012, 12, 31, 00, 00, 00).toDate());
    Assert.assertEquals(updatePersonVO.getDateOfBirth().getFuzzyDateMask(), FuzzyDateMask.YEAR);
    Assert.assertEquals(updatePersonVO.getDateOfBirthEstimated(), true);
  }

  // Test 5 - Update a person with Fuzzy year and month only. Estimated flag
  // set to true
  // Result - The calculated date will be set to the last
  // second-minute-day-month-year of the month provided
  // Even though the DOB is estimated to a month, as the DOB estimated flag is
  // set to true, we indicate the DOB is accurate (not estimated)
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithFuzzyYearAndMonthOnly() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, null);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Fuzzy");
    updatePersonVO.setSurname("To Month");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirthEstimated(true);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getDateOfBirth().getCalculatedDate(),
        new DateTime(1978, 02, 28, 00, 00, 00).toDate());
    Assert.assertEquals(updatePersonVO.getDateOfBirth().getFuzzyDateMask(), FuzzyDateMask.MONTH);
    Assert.assertEquals(updatePersonVO.getDateOfBirthEstimated(), true);
  }

  // Test 6 - Update a person with Fuzzy year,month and day only. The
  // estimated flag is initially set to false.
  // Result - The calculated date will be set to the last
  // second-minute-day-month-year of the date provided.
  // Also, the estimated flag is set to false as the date provided is exact to
  // the day, we leave the flag as is.
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithFuzzyYearAndMonthAndDayOnlyEstimatedEqualsFalse() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Fuzzy");
    updatePersonVO.setSurname("To Day");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    updatePersonVO.setGender("male");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getDateOfBirth().getCalculatedDate(),
        new DateTime(1978, 02, 13, 00, 00, 00).toDate());
    Assert.assertEquals(updatePersonVO.getDateOfBirth().getFuzzyDateMask(), FuzzyDateMask.DAY);
    Assert.assertEquals(updatePersonVO.getDateOfBirthEstimated(), false);
  }

  // Test 7 - Update a person with estimated DOB set to true and the FuzzyDate
  // estimated to the DAY
  // Result - Estimated DOB flag is true, therefore, we accept the user input
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonEstimatedDOBSetTrueDOBMaskDay() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Estimated");
    updatePersonVO.setSurname("True");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirthEstimated(true);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(true, updatedPersonVO.getDateOfBirthEstimated());
  }

  // Test 8 - Update a person with estimated DOB to the year with the
  // estimated flag set to false
  // Result - As the estimated DOB is not exact to a day, we will set the
  // estimated flag to true
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonEstimatedDOBSetFalseDOBMaskYear() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, null, null);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Estimated");
    updatePersonVO.setSurname("false");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(true, updatedPersonVO.getDateOfBirthEstimated());
  }

  // Test 9 - Update a person with estimated DOB to the month with the
  // estimated flag set to false
  // Result - As the DOB is not exact to the day, we update the flag to true
  // as it is an estimate
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonEstimatedDOBSetFalseDOBMaskMonth() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, null);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Estimated");
    updatePersonVO.setSurname("False");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(true, updatedPersonVO.getDateOfBirthEstimated());
  }

  // Test 10 - Update a person with estimated DOB to the year with the
  // estimated flag set to null
  // Result - As the estimated DOB is not exact to a day, we update the
  // estimated flag to true
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonEstimatedDOBSetNullDOBMaskYear() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, null, null);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Estimated");
    updatePersonVO.setSurname("Null");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirthEstimated(null);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(true, updatedPersonVO.getDateOfBirthEstimated());
  }

  // Test 11 - Update a person with estimated DOB to the month with the
  // estimated flag set to null
  // Result - As the DOB is not exact to the day, therefore we update the
  // estimate to true
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonEstimatedDOBSetNullDOBMaskMonth() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, null);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Estimated");
    updatePersonVO.setSurname("Null");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(null);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(true, updatedPersonVO.getDateOfBirthEstimated());
  }

  // Test 12 - Update a person with estimated DOB to the day with the
  // estimated flag set to null
  // Result - Although the DOB is exact to the day, the estimated flag should
  // remain null as explicitly
  // requested
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonEstimatedDOBSetNullDOBMaskDay() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Estimated");
    updatePersonVO.setSurname("Null");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(null);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertNull(updatedPersonVO.getDateOfBirthEstimated());
  }

  // Test 13 - Update a person with a valid NHS Number
  // Result - Person is updated successfully with provided NHS Number
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, null);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("NhsNumber");
    updatePersonVO.setSurname("Added");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("male");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(null);
    updatePersonVO.setNhsNumber("9434765919");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getNhsNumber(), updatedPersonVO.getNhsNumber());

  }

  // Test 14 - Update a person with NHS Number "9434765919" (default
  // checkNhsNumber is false), there is already a person (id=601) with the
  // same NHS number
  // Result - Person (id=602) is updated successfully with the same NHS Number
  // with person (id=601)
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithMultipleSameNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(602);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("MultipleSameNhsNumber");
    updatePersonVO.setSurname("SecondPerson");
    updatePersonVO.setGender(personVO.getGender());
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setNhsNumber("9434765919");
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getNhsNumber(), updatedPersonVO.getNhsNumber());

  }

  // Test 15 - Try to update a person (id=900) who doean't exist
  // Result - Updating a non-existing person (id=900) throws
  // PersonNotFoundException
  @Test(expected = PersonNotFoundException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePerson_PersonNotFoundException() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(900);
    updatePersonVO.setForename("testPersonNorFoundException");
    updatePersonVO.setSurname("TestPersonNorFoundException");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    updatePersonVO.setNhsNumber("9434765919");
    updatePersonVO.setObjectVersion(1);

    personService.updatePerson(updatePersonVO, false, false);

  }

  // Test 16 - Update a person with NHS Number "9434765919" and checkNhsNumber
  // is TRUE, there is already a person (id=601) with the same NHS number
  // Result - Updating a person (id=602) with same NHS number of person
  // (id=601) throws DuplicateNhsNumberException
  @Test(expected = DuplicateNhsNumberException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithUniqueNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(602);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("UniqueNhsNumber");
    updatePersonVO.setSurname("SecondPerson");
    updatePersonVO.setGender("MALE");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setNhsNumber("9434765919");
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, true, false);

  }

  // Test 17 - Update a person with NHS Number digits more than 10
  // Result - The operation fails with Bad Request Error Code
  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithNHSNumberMoreThan10Digits() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("LongNHSNumber");
    updatePersonVO.setSurname("NHSNumberTest");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    updatePersonVO.setNhsNumber("1234567891222222228889989");
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, true, false);

  }

  // Test 18 - Update a person with NHS Number non-digit characters
  // Result - The operation fails with Bad Request Error Code
  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithNHSNumberIncludesNonDigitCharacters() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("NonValidNhsNumber");
    updatePersonVO.setSurname("NHSNumberTest");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    updatePersonVO.setNhsNumber("K?_;567CH12");
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, true, false);

  }

  // Test 19 - Update a person with not valid NHS Number
  // Result - The operation raises ConstraintViolationException
  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithNotValidNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(600);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("NotValidNHSNumber");
    updatePersonVO.setSurname("NHSNumberTest");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setDateOfBirthEstimated(false);
    updatePersonVO.setNhsNumber("1111111112");
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, true, false);

  }

  // Test 20 - Update a person with the same NHS Number he/she already has
  // "9434765919",and checkNhsNumber is TRUE
  // Result - Updating a person (id=601) with same NHS number will result
  // successful
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithSameNHSNumber() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(601);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("UpdateWithSameNhsNumber");
    updatePersonVO.setSurname("Person");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirthEstimated(false);
    updatePersonVO.setNhsNumber("9434765919");
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, true, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getNhsNumber(), updatedPersonVO.getNhsNumber());
  }

  // Test type 1 - Update a person with types to null
  // Result - The person is updated
  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonTypeTwoToNull() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(700);

    final List<String> personTypes = new ArrayList<>();

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();

    Assert.assertEquals(0, personTypesAdded.size());

  }

  // Test type 2 - Update a person with types null to professional
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonTypeNullToOne() throws Exception {
    final PersonService personService = getPersonService();

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("PROFESSIONAL");

    final PersonVO personVO = personService.findById(701);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setProfessionalTitle("DOCTOR");
    updatePersonVO.setOrganisationName("OLMGROUP");

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();

    Assert.assertEquals(1, personTypesAdded.size());
    Assert.assertEquals("John", updatedPersonVO.getForename());
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(0));
  }

  // Test type 3 - Update a person with types client and professional to
  // client
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonTypeTwoToOne() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(702);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();
    Assert.assertEquals(1, personTypesAdded.size());
    Assert.assertEquals("CLIENT", personTypesAdded.get(0));

  }

  // Test type 4 - Update a person with types from professional and client to
  // professional and other.
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonTypeTwoToAnotherTwo() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(702);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("PROFESSIONAL");
    personTypes.add("OTHER");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setProfessionalTitle("DOCTOR");
    updatePersonVO.setOrganisationName("OLMGROUP");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertEquals(2, personTypesAdded.size());
    Assert.assertEquals("OTHER", personTypesAdded.get(0));
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(1));

    Assert.assertEquals("DOCTOR", updatedPersonVO.getProfessionalTitle());
    Assert.assertEquals("OLMGROUP", updatedPersonVO.getOrganisationName());

  }

  // Test type 5 - Update a person with types client to all three
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonOneToThree() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(703);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("PROFESSIONAL");
    personTypes.add("CLIENT");
    personTypes.add("OTHER");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setProfessionalTitle("DOCTOR");
    updatePersonVO.setOrganisationName("OLMGROUP");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertEquals(3, personTypesAdded.size());
    Assert.assertEquals("CLIENT", personTypesAdded.get(0));
    Assert.assertEquals("OTHER", personTypesAdded.get(1));
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(2));
  }

  // Test type 6 - Update a person with types three to two
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonThreeToTwo() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(704);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    personTypes.add("OTHER");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();
    // sort so we get repeatable order
    Collections.sort(personTypesAdded);
    Assert.assertEquals(2, personTypesAdded.size());
    Assert.assertEquals("CLIENT", personTypesAdded.get(0));
    Assert.assertEquals("OTHER", personTypesAdded.get(1));

  }

  // Test type 7 - Update a person with adding and removing types
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonAddNewRemoveOne() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(702);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    personTypes.add("OTHER");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertEquals(2, personTypesAdded.size());
    Assert.assertEquals("CLIENT", personTypesAdded.get(0));
    Assert.assertEquals("OTHER", personTypesAdded.get(1));
  }

  // Test type 8 - Update a person with keeping inactive types
  // TEST UPDATE PROFESSIONAL TITLE AND ORG
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonKeepInactiveTypes() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(705);

    // SET UP TYPES
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("TEMPORALYINACTIVE");
    personTypes.add("OTHER");
    personTypes.add("PROFESSIONAL");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setProfessionalTitle("DOCTOR");
    updatePersonVO.setOrganisationName("OLMGROUP");

    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertEquals(3, personTypesAdded.size());
    Assert.assertEquals("OTHER", personTypesAdded.get(0));
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(1));
    Assert.assertEquals("TEMPORALYINACTIVE", personTypesAdded.get(2));

    Assert.assertEquals("DOCTOR", updatedPersonVO.getProfessionalTitle());
    Assert.assertEquals("OLMGROUP", updatedPersonVO.getOrganisationName());

  }

  // Test type 9 - Update a person with types not updated
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonUpdateNothing() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(702);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setProfessionalTitle("DOCTOR");
    updatePersonVO.setOrganisationName("OLMGROUP");
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();

    // sort so we get repeatable order
    Collections.sort(personTypesAdded);

    Assert.assertNotNull(updatedPersonVO.getPersonTypes());
    Assert.assertEquals(2, updatedPersonVO.getPersonTypes().size());
    Assert.assertEquals("CLIENT", personTypesAdded.get(0));
    Assert.assertEquals("PROFESSIONAL", personTypesAdded.get(1));

    Assert.assertEquals("DOCTOR", updatedPersonVO.getProfessionalTitle());
    Assert.assertEquals("OLMGROUP", updatedPersonVO.getOrganisationName());

  }

  // Test type 10 - Update a person with types doesn't exist
  // Result - exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = CodedEntryMissingException.class)
  public void handleTestUpdatePersonThreeToWrongType() throws Exception {

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(704);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    personTypes.add("TEST");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");

    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

  }

  // Test type 11 - Update a person with types witch inactive type
  // Result - exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test(expected = CodedEntryInactiveException.class)
  public void handleTestUpdatePersonInActiveCodedEntry() throws Exception {

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(704);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    personTypes.add("TEMPORALYINACTIVE");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");

    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

  }

  // Test type 12 - Update a person with DUPLICATE TYPES
  // Result - exception
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonDuplicateTypes() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(704);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("CLIENT");
    personTypes.add("CLIENT");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setDateOfBirth(personVO.getDateOfBirth());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    final List<String> personTypesAdded = updatedPersonVO.getPersonTypes();
    Assert.assertEquals(1, personTypesAdded.size());
    Assert.assertEquals("CLIENT", personTypesAdded.get(0));
  }

  // Test type 13 - Update a person with known as field as null (can be blank)
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithNullKnownAs() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(706);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setSurname(personVO.getSurname());
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setGender("male");
    updatePersonVO.setKnownAs(null);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatedPersonVO.getKnownAs(), null);

  }

  // Test type 14 - Update a person with alternate names field as null (can be
  // blank)
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdatePersonWithNullAlternateNames() throws Exception {

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(706);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setSurname(personVO.getSurname());
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setAlternateNames(null);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatedPersonVO.getAlternateNames(), null);

  }

  // Test type constraint 1 add person other and don't set name we expected
  // ConstraintViolationException
  // Missing surname
  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleUpdatePersonOtherTypeContraintValidationException() throws Exception {

    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(704);

    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("Mary");
    updatePersonVO.setSurname(null);
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);
  }

  // Test 1 - Update a person with a new main name
  // Result - A new previous main name is created and the existing name is
  // updated with the new name
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdatePersonWithNewMainName() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(601);
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(FuzzyDateMask.NONE, null);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setForename("New forename");
    updatePersonVO.setSurname("New surname");
    updatePersonVO.setMiddleNames("New middlenames");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setGender("male");
    updatePersonVO.setTitle("MRS");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, true);

    final PersonWithNamesVO updatedPersonWithNamesVO = personService.findById(updatePersonVO.getId(),
        PersonWithNamesVO.class);

    // Checking the persons main name has been updated.
    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonWithNamesVO.getForename());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonWithNamesVO.getSurname());
    Assert.assertEquals(updatePersonVO.getTitle(), updatedPersonWithNamesVO.getTitle());
    Assert.assertEquals(updatePersonVO.getMiddleNames(), updatedPersonWithNamesVO.getMiddleNames());
    Assert.assertEquals(2, updatedPersonWithNamesVO.getOtherNames().size());

    // Checking the previous name is now saved in the otherNames table
    Assert.assertEquals(personVO.getForename(), updatedPersonWithNamesVO.getOtherNames().get(0).getForename());
    Assert.assertEquals(personVO.getSurname(), updatedPersonWithNamesVO.getOtherNames().get(0).getSurname());
    Assert.assertEquals(personVO.getTitle(), updatedPersonWithNamesVO.getOtherNames().get(0).getTitle());
    Assert.assertEquals(personVO.getMiddleNames(),
        updatedPersonWithNamesVO.getOtherNames().get(0).getMiddleNames());
    Assert.assertEquals("PREVIOUS", updatedPersonWithNamesVO.getOtherNames().get(0).getType());
    Assert.assertEquals("M.S.C", updatedPersonWithNamesVO.getOtherNames().get(0).getSuffix());
    // New previous name start date = old previous name end date is. New
    // previous name close date = current date.
    Assert.assertEquals(new Date(TestUtils.getDate("2014-05-18")),
        updatedPersonWithNamesVO.getOtherNames().get(0).getStartDate());
    Assert.assertEquals(new Date(TestUtils.getTodaysDate()),
        updatedPersonWithNamesVO.getOtherNames().get(0).getCloseDate());

  }

  // Test 1: Validation Rules Update a Unborn Client person with DOB
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateUnbornClientForNullDOB() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(801);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity(personVO.getEthnicity());
    updatePersonVO.setGender(personVO.getGender());
    updatePersonVO.setDueDate(Calendar.getInstance().getTime());
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("Date of birth must be null.", constraintViolation.getMessage());
    }

  }

  // Test 2: Validation Rules Update a Client person or Null life state for
  // unchanged DOB
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateAliveOrNullClientWithNullDOB() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(803);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity(personVO.getEthnicity());
    updatePersonVO.setGender(personVO.getGender());
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDateOfBirth(null);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.NotBlankIfSet);
      Assert.assertEquals("Date of birth must be specified.", constraintViolation.getMessage());
    }

  }

  // Test 3: Validation Rules Update a Client person when Ethnicity changed
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateClientEthnicityChanged() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(803);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity(null);
    updatePersonVO.setGender("male");
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.NotBlankIfSet);
      Assert.assertEquals("Ethnicity must be specified.", constraintViolation.getMessage());
    }

  }

  // Test 4: Validation Rules Update any person with changed forename and
  // surname
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdatePersonWithNullForenameOrSurname() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(803);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity(personVO.getEthnicity());
    updatePersonVO.setGender(personVO.getGender());
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(null);
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setSurname(null);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      Assert.assertEquals(2, constraintViolations.size());
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.NotBlankIfSet);
    }

  }

  // Test 5: Validation Rules Update any person with changed gender
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdatePersonWithNullGender() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(803);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setGender(null);
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      Assert.assertEquals(1, constraintViolations.size());
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.NotBlankIfSet);
      Assert.assertEquals("Gender must be specified.", constraintViolation.getMessage());
    }

  }

  // Test 6: Validation Rules Update Unborn Client person with Null Due date
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateUnbornClientWithNullDueDate() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(804);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity(personVO.getEthnicity());
    updatePersonVO.setGender(personVO.getGender());
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDueDate(null);
    updatePersonVO.setDateOfBirth(null);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      Assert.assertEquals(1, constraintViolations.size());
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.NotBlankIfSet);
      Assert.assertEquals("Due date must be specified.", constraintViolation.getMessage());
    }

  }

  // Test 7: Validation Rules Update Alive Client or Professional person
  // with Due Date
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateOtherThanUnbornClientWithDueDate() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(803);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender(personVO.getGender());
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDueDate(Calendar.getInstance().getTime());
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      Assert.assertEquals(1, constraintViolations.size());
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("Due date must be null.", constraintViolation.getMessage());
    }

  }

  // Test 8: Validation Rules Update Null Client or Professional person
  // with Due Date
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateNullLifeStatePersonWithDueDate() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(702);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("MALE");
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDueDate(Calendar.getInstance().getTime());
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      Assert.assertEquals(1, constraintViolations.size());
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("Due date must be null.", constraintViolation.getMessage());
    }

  }

  // Test 9: Validation Rules Update Unborn Client person with NHS Number
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateUnbornClientWithNHSNumber() throws Exception {
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(804);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity(personVO.getEthnicity());
    updatePersonVO.setGender(personVO.getGender());
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setDueDate(personVO.getDueDate());
    updatePersonVO.setNhsNumber("9434765919");
    updatePersonVO.setDateOfBirth(null);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      Assert.assertEquals(1, constraintViolations.size());
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof javax.validation.constraints.Null);
      Assert.assertEquals("NHS number must be null.", constraintViolation.getMessage());
    }

  }

  // Test 10: Validation Rules Update Professional Client person with Null
  // professional title and organisation
  // Result - throw constraint exception
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  @Test
  public void handleTestUpdateProfessionalClientWithNullProfessionalTitleAndOrganisation() throws Exception {
    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1978, Month.FEBRUARY, 13);
    final PersonService personService = getPersonService();
    final PersonVO personVO = personService.findById(702);
    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setEthnicity("WBRI");
    updatePersonVO.setGender("MALE");
    updatePersonVO.setPersonTypes(personVO.getPersonTypes());
    updatePersonVO.setForename(personVO.getForename());
    updatePersonVO.setNhsNumber("9434765919");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setSurname(personVO.getSurname());
    updatePersonVO.setProfessionalTitle(null);
    updatePersonVO.setOrganisationName(null);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());
    try {
      personService.updatePerson(updatePersonVO, false, false);
      Assert.fail();
    } catch (ConstraintViolationException constraintViolationException) {
      final Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
      Assert.assertEquals(2, constraintViolations.size());
      final ConstraintViolation<?> constraintViolation = constraintViolations.iterator().next();
      Assert.assertTrue(constraintViolation.getConstraintDescriptor()
          .getAnnotation() instanceof com.olmgroup.usp.facets.validation.NotBlankIfSet);
    }

  }

  // Validating due date for not more than 280 days for
  // UpdateNotCarriedPersonUnbornVO
  @Test(expected = ConstraintViolationException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestSetNotCarriedPersonAsUnbornWithDueDateMoreThan280Days() throws Exception {
    final PersonService personService = getPersonService();

    final UpdateNotCarriedPersonUnbornVO updateNotCarriedPersonUnbornVO = new UpdateNotCarriedPersonUnbornVO();

    updateNotCarriedPersonUnbornVO.setId(-6001);

    final Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, 285);
    updateNotCarriedPersonUnbornVO.setDueDate(cal.getTime());

    updateNotCarriedPersonUnbornVO.setObjectVersion(1);

    personService.setPersonAsNotCarriedToUnborn(updateNotCarriedPersonUnbornVO);
  }

  // Test: Add Other person with life state as Deceased
  // Result: Person should be added
  @Test
  public void handleAddDeceasedPerson() throws Exception {

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1965, Month.FEBRUARY, 13);
    final SimpleFuzzyDate diedDate = new SimpleFuzzyDate(2014, Month.JULY, 13);
    final List<String> personTypes = new ArrayList<String>();
    personTypes.add("OTHER");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.DECEASED);
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setDiedDate(diedDate);
    newPersonVO.setDateOfBirthEstimated(null);

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);
    Assert.assertTrue(personId > 0);
    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();
    Assert.assertEquals("OTHER", personTypesAdded.get(0));
  }

  // Test: Add Other person with life state as Deceased with out Died Date
  // Result: Person should be added
  @Test
  public void handleAddDeceasedPersonWithOutDiedDate() throws Exception {

    final List<String> personTypes = new ArrayList<String>();
    personTypes.add("OTHER");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.DECEASED);
    newPersonVO.setDateOfBirth(null);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setDiedDate(null);
    newPersonVO.setDateOfBirthEstimated(null);

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);
    Assert.assertTrue(personId > 0);
    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();
    Assert.assertEquals("OTHER", personTypesAdded.get(0));
  }

  // Test: Add Other and Client person with life state as Deceased
  // Result: Should throw exception
  @Test(expected = ConstraintViolationException.class)
  public void handleAddDeceasedPersonWithClientPersonType() throws Exception {

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(1965, Month.FEBRUARY, 13);
    final SimpleFuzzyDate diedDate = new SimpleFuzzyDate(2014, Month.JULY, 13);
    final List<String> personTypes = new ArrayList<String>();
    personTypes.add("OTHER");
    personTypes.add("CLIENT");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.DECEASED);
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setDiedDate(diedDate);
    newPersonVO.setDateOfBirthEstimated(null);

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);
    Assert.assertTrue(personId > 0);
  }

  // Test: Add Other person with life state as Deceased with Died date less than
  // Date of birth
  // Result: Should throw exception
  @Test(expected = DiedDateBeforeDateOfBirthException.class)
  public void handleAddDeceasedPersonWithDiedDateEarlierThanDob() throws Exception {

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2009, Month.JULY, 13);
    final SimpleFuzzyDate diedDate = new SimpleFuzzyDate(2008, Month.JULY, 13);
    final List<String> personTypes = new ArrayList<String>();
    personTypes.add("OTHER");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.DECEASED);
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setDiedDate(diedDate);
    newPersonVO.setDateOfBirthEstimated(null);

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);
    Assert.assertTrue(personId > 0);
    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();
    Assert.assertEquals("OTHER", personTypesAdded.get(0));
  }

  // Test: Add Other person with life state as Deceased with Died date in futre
  // Result: Should throw exception
  @Test(expected = ConstraintViolationException.class)
  public void handleAddDeceasedPersonWithDiedDateInFuture() throws Exception {

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2009, Month.FEBRUARY, 13);
    final Calendar futureCal = Calendar.getInstance();
    futureCal.add(Calendar.DAY_OF_MONTH, 1);
    final DateTime futureDate = new DateTime(futureCal.getTime());
    final SimpleFuzzyDate diedDate = new SimpleFuzzyDate(futureDate.getYear(), Month.of(futureDate.getMonthOfYear()),
        futureDate.getDayOfMonth());
    final List<String> personTypes = new ArrayList<String>();
    personTypes.add("OTHER");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.DECEASED);
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setDiedDate(diedDate);
    newPersonVO.setDateOfBirthEstimated(null);

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);
    Assert.assertTrue(personId > 0);
    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();
    Assert.assertEquals("OTHER", personTypesAdded.get(0));
  }

  // Test: Add Other person with life state as ALIVE with Died date
  // Result: Should throw exception
  @Test(expected = ConstraintViolationException.class)
  public void handleAddClientOrProfessionalPersonWithDiedDate() throws Exception {

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2009, Month.FEBRUARY, 13);
    final SimpleFuzzyDate diedDate = new SimpleFuzzyDate(2009, Month.JUNE, 15);
    final List<String> personTypes = new ArrayList<String>();
    personTypes.add("CLIENT");
    personTypes.add("PROFESSIONAL");
    final NewPersonVO newPersonVO = new NewPersonVO();
    newPersonVO.setForename("Mary");
    newPersonVO.setSurname("Green");
    newPersonVO.setGender("male");
    newPersonVO.setEthnicity("WBRI");
    newPersonVO.setPersonTypes(personTypes);
    newPersonVO.setLifeState(LifeState.ALIVE);
    newPersonVO.setDateOfBirth(dateOfBirth);
    newPersonVO.setDateOfBirthEstimated(null);
    newPersonVO.setDiedDate(diedDate);
    newPersonVO.setDateOfBirthEstimated(null);

    final PersonService personService = getPersonService();
    final long personId = personService.addPerson(newPersonVO, false);
    Assert.assertTrue(personId > 0);
    final PersonVO personVO = personService.findById(personId);
    final List<String> personTypesAdded = personVO.getPersonTypes();
    Assert.assertEquals("OTHER", personTypesAdded.get(0));
  }

  // Test - Update a Deceased person with all attributes populated
  // Result - The person is updated
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdateDeceasedPerson() throws Exception {

    final PersonService personService = getPersonService();

    final PersonVO personVO = personService.findById(806);

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2010, Month.NOVEMBER, 29);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setKnownAs("JG");
    updatePersonVO.setAlternateNames("Jonathan");
    updatePersonVO.setSuffix("C.D.");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getTitle(), updatedPersonVO.getTitle());
    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getMiddleNames(), updatedPersonVO.getMiddleNames());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getSuffix(), updatedPersonVO.getSuffix());
  }

  // Test - Update a Deceased person with all attributes populated
  // Result - The person is updated
  @Test(expected = DiedDateBeforeDateOfBirthException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdateDeceasedPersonWithDOBMorethanDiedDate() throws Exception {

    final PersonService personService = getPersonService();

    final PersonVO personVO = personService.findById(806);

    final SimpleFuzzyDate dateOfBirth = new SimpleFuzzyDate(2012, Month.NOVEMBER, 29);

    final UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setId(personVO.getId());
    updatePersonVO.setTitle("MR");
    updatePersonVO.setForename("John");
    updatePersonVO.setMiddleNames("Graham");
    updatePersonVO.setSurname("Mellor");
    updatePersonVO.setKnownAs("JG");
    updatePersonVO.setAlternateNames("Jonathan");
    updatePersonVO.setSuffix("C.D.");
    updatePersonVO.setDateOfBirth(dateOfBirth);
    updatePersonVO.setGender("male");
    updatePersonVO.setEthnicity("WBRI");
    final List<String> personTypes = new ArrayList<>();
    personTypes.add("OTHER");
    updatePersonVO.setPersonTypes(personTypes);
    updatePersonVO.setObjectVersion(personVO.getObjectVersion());

    personService.updatePerson(updatePersonVO, false, false);

    final PersonVO updatedPersonVO = personService.findById(updatePersonVO.getId());

    Assert.assertEquals(updatePersonVO.getTitle(), updatedPersonVO.getTitle());
    Assert.assertEquals(updatePersonVO.getForename(), updatedPersonVO.getForename());
    Assert.assertEquals(updatePersonVO.getMiddleNames(), updatedPersonVO.getMiddleNames());
    Assert.assertEquals(updatePersonVO.getSurname(), updatedPersonVO.getSurname());
    Assert.assertEquals(updatePersonVO.getSuffix(), updatedPersonVO.getSuffix());
  }

  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddContactForPerson() throws Exception {

    final PersonService personService = getPersonService();

    final NewContactVO newContactVO = new NewContactVO();
    newContactVO.setContactType("HOME");
    newContactVO.setContactUsage("TEMPORARY");
    newContactVO.setStartDate(new DateTime(2014, 1, 1, 0, 0)
        .toDate());
    newContactVO.setEndDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    // set the associated subject
    newContactVO.setSubject(new SubjectIdTypeVO(-1L, SubjectType.PERSON));
    newContactVO.setVerified(true);
    newContactVO.setVerifiedDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());

    personService.addContactForPerson(600L, newContactVO);
  }

  @Test(expected = InvalidContactVerificationDateException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestAddContactForPersonInvalidVerificationDate() throws Exception {

    final PersonService personService = getPersonService();

    final NewContactVO newContactVO = new NewContactVO();

    newContactVO.setContactType("HOME");
    newContactVO.setContactUsage("TEMPORARY");
    newContactVO.setStartDate(new DateTime(2014, 1, 1, 0, 0)
        .toDate());
    newContactVO.setEndDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    // set the associated subject
    newContactVO.setSubject(new SubjectIdTypeVO(-1L, SubjectType.PERSON));
    newContactVO.setVerified(true);
    newContactVO.setVerifiedDate(null);

    personService.addContactForPerson(600L, newContactVO);
  }

  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdateContactForPerson() throws Exception {

    final PersonService personService = getPersonService();

    final NewContactVO newContactVO = new NewContactVO();
    newContactVO.setContactType("HOME");
    newContactVO.setContactUsage("TEMPORARY");
    newContactVO.setStartDate(new DateTime(2014, 1, 1, 0, 0)
        .toDate());
    newContactVO.setEndDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    // set the associated subject
    newContactVO.setSubject(new SubjectIdTypeVO(-1L, SubjectType.PERSON));
    newContactVO.setVerified(true);
    newContactVO.setVerifiedDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    personService.addContactForPerson(600L, newContactVO);

    final UpdateContactVO updateContactVO = new UpdateContactVO();
    updateContactVO.setId(1L);
    updateContactVO.setContactType("HOME");
    updateContactVO.setContactUsage("TEMPORARY");
    updateContactVO.setStartDate(new DateTime(2014, 1, 1, 0, 0)
        .toDate());
    updateContactVO.setEndDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    // set the associated subject
    updateContactVO.setVerified(true);
    updateContactVO.setVerifiedDate(new DateTime(2015, 12, 31, 0, 0)
        .toDate());

    personService.updateContact(updateContactVO, 600L);
  }

  //  @Test(expected = InvalidContactVerificationDateException.class)
  @DatabaseSetup(value = { "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void handleTestUpdateContactForPersonInvalidVerificationDate() throws Exception {

    final PersonService personService = getPersonService();

    final NewContactVO newContactVO = new NewContactVO();
    newContactVO.setContactType("HOME");
    newContactVO.setContactUsage("TEMPORARY");
    newContactVO.setStartDate(new DateTime(2014, 1, 1, 0, 0)
        .toDate());
    newContactVO.setEndDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    // set the associated subject
    newContactVO.setSubject(new SubjectIdTypeVO(-1L, SubjectType.PERSON));
    newContactVO.setVerified(true);
    newContactVO.setVerifiedDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    personService.addContactForPerson(600L, newContactVO);

    final UpdateContactVO updateContactVO = new UpdateContactVO();

    updateContactVO.setId(1L);
    updateContactVO.setContactType("HOME");
    updateContactVO.setContactUsage("TEMPORARY");
    updateContactVO.setStartDate(new DateTime(2014, 1, 1, 0, 0)
        .toDate());
    updateContactVO.setEndDate(new DateTime(2014, 12, 31, 0, 0)
        .toDate());
    updateContactVO.setVerified(true);
    updateContactVO.setVerifiedDate(null);

    personService.updateContact(updateContactVO, 600L);
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypeProfessional() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(505, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), is(empty()));
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypeOther() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(501, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), is(empty()));
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypeClient() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(504, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), notNullValue());
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypeFosterCarer() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(503, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), notNullValue());
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypeAdopter() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(502, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), notNullValue());
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypesProfessionalAndOther() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(100, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), is(empty()));
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypesClientWithFostercarerAndAdopter() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(101, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), notNullValue());
  }
  
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/personHeaderDetails.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public void testPersonHeaderDetailsForPersonTypeProfessionalAndClient() throws Exception {
    final PersonHeaderDetailsVO personHeaderDetailsVO = getPersonService().get(510, PersonHeaderDetailsVO.class);
    assertThat("Expected personHeaderDetails", personHeaderDetailsVO, notNullValue());
    assertThat(personHeaderDetailsVO.getAllocatedWorkers(), notNullValue());
  }
}
