package com.olmgroup.usp.apps.person.service;

import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.olmgroup.usp.components.person.service.PersonService;
import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;
import com.olmgroup.usp.facets.test.junit.AbstractUnifiedTestBase;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <p>
 * Spring Service base test class for <code>PersonService</code>, provides
 * access to all services and entities referenced by this service.
 * </p>
 * 
 * @see PersonService
 */
@ContextHierarchy({
    @ContextConfiguration(classes = { CoreConfiguration.class }),
    @ContextConfiguration(classes = { SpringMvcControllerConfig.class })
})
@WebAppConfiguration
@ActiveProfiles({ "development" })
public abstract class PersonServiceTestBase extends
    AbstractUnifiedTestBase {
  protected static final Logger LOGGER = LoggerFactory
      .getLogger(PersonServiceTestBase.class);

  @Inject
  @Named("personService")
  private PersonService personService;

  protected PersonService getPersonService() {
    return personService;
  }

  /**
   * {@inheritDoc}
   */
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/addPerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public final void testAddPerson() throws Exception {
    handleTestAddPerson();
    this.flush();
  }

  /**
   * Performs the core logic for {@link
   * #testAddPerson(@com.olmgroup.usp.facets.spring.SpelParameter(
   * "newPersonVO")
   * 
   * @com.olmgroup.usp.facets.event.EventSubject(useReturnForPostEvent=true)
   *                                                                         com.olmgroup.usp.components.person.vo.NewPersonVO newPersonVO, boolean
   *                                                                         checkNhsNumber)}
   * @return long
   * @throws Exception
   */
  protected abstract void handleTestAddPerson() throws Exception;

  /**
   * {@inheritDoc}
   */
  @Test
  @DatabaseSetup(value = { "/dbunit/PersonService/PersonService.setup.xml",
      "/dbunit/PersonService/updatePerson.setup.xml" }, type = DatabaseOperation.CLEAN_INSERT)
  public final void testUpdatePerson() throws Exception {
    handleTestUpdatePerson();
    this.flush();
  }

  /**
   * Performs the core logic for {@link
   * #testUpdatePerson(@com.olmgroup.usp.facets.spring.SpelParameter(
   * "updatePersonVO") @com.olmgroup.usp.facets.event.EventSubject
   * com.olmgroup.usp.components.person.vo.UpdatePersonVO updatePersonVO,
   * boolean checkNhsNumber, boolean isNewMainName)}
   * 
   * @return void
   * @throws Exception
   */
  protected abstract void handleTestUpdatePerson() throws Exception;

}