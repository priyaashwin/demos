package com.olmgroup.usp.components.relationship.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.olmgroup.usp.apps.relationshipsrecording.vo.AsymmetricPersonPersonRelationshipResultVO;
import com.olmgroup.usp.facets.core.util.TemporalStatusFilter;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.search.SortSelection;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @see com.olmgroup.usp.components.relationship.service.PersonPersonRelationshipService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonPersonRelationshipServiceTest extends
    PersonPersonRelationshipServiceTestBase {

  @Before
  public void handleInitializeTestSuite() {
    login("admin", "admin123");
  }

  @Override
  protected void handleTestFindPersonalByPersonId() throws Exception {

    // Test case for Both(Active & InActive) Relationships

    final SortSelection sort = new SortSelection().addAscendingOrderedField(
        "startDate").addDescendingOrderedField("closeDate");
    final PaginationResult<AsymmetricPersonPersonRelationshipResultVO> paginationResultVO = getPersonPersonRelationshipService()
        .findPersonalByPersonId(-10, 1, 1, 10, sort,
            TemporalStatusFilter.ALL, AsymmetricPersonPersonRelationshipResultVO.class);
    final List<AsymmetricPersonPersonRelationshipResultVO> resultList = paginationResultVO
        .getResults();

    assertThat("TestFindPersonalByPersonId records exist", paginationResultVO, not(nullValue()));
    assertThat("TestFindPersonalByPersonId records exist", resultList, not(nullValue()));
    assertThat("TestFindPersonalByPersonId resultList size", resultList.size(), equalTo(6));
    assertThat("Address text is incorrect for address with no location", resultList.get(0).getAddress(),
        equalTo("Address not known"));

    assertThat("Address should be Do Not Disclose", resultList.get(0).getDoNotDisclose(), equalTo(true));
    assertThat("Address should NOT be Do Not Disclose", resultList.get(1).getDoNotDisclose(), equalTo(false));
  }

}
