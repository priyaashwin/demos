package com.olmgroup.usp.components.relationship.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonOrganisationRelationshipsResultVO;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.search.SortSelection;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @see com.olmgroup.usp.components.relationship.service.PersonOrganisationRelationshipService
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonOrganisationRelationshipServiceTest extends
    PersonOrganisationRelationshipServiceTestBase {

  @Before
  public void handleInitializeTestSuite() {
    login("admin", "admin123");
  }

  @Override
  protected void handleTestFindByPersonId() throws Exception {

    final SortSelection sort = new SortSelection().addAscendingOrderedField(
        "startDate").addDescendingOrderedField("closeDate");
    long personId = -10L;
    int pageNumber = 1;
    int pageSize = 10;

    final PaginationResult<PersonOrganisationRelationshipsResultVO> paginationResultVO = getPersonOrganisationRelationshipService()
        .findByPersonId(personId, pageNumber, pageSize, sort, PersonOrganisationRelationshipsResultVO.class);
    final List<PersonOrganisationRelationshipsResultVO> resultList = paginationResultVO.getResults();

    assertThat("TestFindPersonalByPersonId records exist", paginationResultVO, not(nullValue()));
    assertThat("TestFindPersonalByPersonId records exist", resultList, not(nullValue()));
    assertThat("TestFindPersonalByPersonId resultList size", resultList.size(), equalTo(4));
    assertThat("Address should be null", resultList.get(0).getAddress(),
        nullValue());
    assertThat("Address shouldn't be null", resultList.get(1).getAddress(),
        notNullValue());
    assertThat("Address should be null", resultList.get(2).getAddress(),
        nullValue());
    assertThat("Address shouldn't be null", resultList.get(3).getAddress(),
        notNullValue());

    assertThat("Address should be Do Not Disclose", resultList.get(1).getDoNotDisclose(), equalTo(true));
    assertThat("Address should NOT be Do Not Disclose", resultList.get(3).getDoNotDisclose(), equalTo(false));
  }

}
