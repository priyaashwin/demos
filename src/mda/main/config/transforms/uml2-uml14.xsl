<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : uml2-uml14.xsl
    Created on : 16 June 2006, 14:34
    Author     : Ian
    Description:
        Take a UML2 XMI file and transform it into a UML1.4 XMI file.
    Version    : "$Revision: 1.2 $"
-->

<xsl:stylesheet version="1.1"
    xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:UML="org.omg.xmi.namespace.UML"
    xmlns:UML2="org.omg.xmi.namespace.UML2">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="UML2:TypedElement.type">
        <xsl:choose>
            <xsl:when test="ancestor::UML:Operation">
                <UML:Parameter.type xsl:exclude-result-prefixes="fn xsl UML2">
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates/>
                </UML:Parameter.type>
            </xsl:when>
            <xsl:when test="ancestor::UML:Classifier.feature">
                <UML:StructuralFeature.type xsl:exclude-result-prefixes="fn xsl UML2">
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates/>
                </UML:StructuralFeature.type>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="@*|*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>