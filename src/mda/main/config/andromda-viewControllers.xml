<andromda xmlns="http://andromda.org/core/configuration">
    <!-- See http://docs.andromda.org/configuration.html for andromda.xml properties documentation -->
    <properties>
        <property name="modelValidation">${andromda.validation}</property>
        <property name="xmlValidation">${andromda.xmlValidation}</property>
        <property name="failOnValidationErrors">${andromda.failOnValidationErrors}</property>
        <property name="outputEncoding">${andromda.outputEncoding}</property>
        <!-- Comma separated list of cartridges to be processed by namespace name, such as java,webservice. Empty = true = process all -->
        <property name="cartridgeFilter">${andromda.filter}</property>
        <!-- Debug andromda processing using example log4j.xml file -->
        <!--property name="loggingConfigurationUri">file:${project.basedir}/log4j.xml</property-->
    </properties>
    <!-- Server will load and validate model in the background automatically when anything changes.
    To start server from mda directory: mvn andromda:start-server.
    Leave commented, or else AndroMDA will try to connect each time.>
    <server>
        <host>localhost</host>
        <port>4447</port>
    </server -->
    <repositories>
        <repository name="netBeansMDR">
            <models>
                <model>
                    <uri>${viewControllers.model.uri}</uri>
                   <moduleSearchLocations>
                        <location patterns="**/${andromda.version}/*.xml.zip">${settings.localRepository}/org/andromda/profiles/uml14</location>
                        <location patterns="**/*.xml">jar:file:${settings.localRepository}/org/omg/profiles/uml14/UML_Standard_Profile/${uml.profile.version}/UML_Standard_Profile-${uml.profile.version}.jar!/</location>
                    </moduleSearchLocations>
                    <!-- Can filter in/out specific model packages (in UML notation) to generate code from (default true=process all) -->
                    <modelPackages>
                        <modelPackage process="false">java::**</modelPackage>
                        <modelPackage process="false">com::olmgroup::usp::facets::**</modelPackage>
                        <modelPackage process="false">com::olmgroup::usp::components::**</modelPackage>
                        <modelPackage process="false">com::olmgroup::usp::apps::relationshipsrecording::service</modelPackage>
                        <modelPackage process="false">com::olmgroup::usp::apps::relationshipsrecording::domain</modelPackage>
                        <modelPackage process="false">com::olmgroup::usp::apps::relationshipsrecording::vo</modelPackage>
                    </modelPackages>
                    <!-- Can filter in/out specific andromda model validation constraints (default true=validate all) -->
                    <!--constraints enforceAll="true">
                        <constraint enforce="false">**::wrapped primitive attribute should not be required</constraint>
                        <constraint enforce="false">**::wrapped primitive parameter should not be required</constraint>
                        <constraint enforce="false">**::wrapped primitive operation return should not be required</constraint>
                        <constraint enforce="false">**::primitive attribute must be required</constraint>
                        <constraint enforce="false">**::primitive operation return must be required</constraint>
                        <constraint enforce="false">**::primitive parameter must be required</constraint>
                        <constraint enforce="false">org::andromda::metafacades::uml::Entity::entities can only generalize other entities</constraint>
                        <constraint enforce="false">org::andromda::metafacades::uml::Entity::entity must have at least one primary key</constraint>
                    </constraints-->
                </model>
            </models>
        </repository>
    </repositories>
    <!-- The mappingsSearchLocations are used by the below '*MappingsUri' namespace
      properties (e.g. languageMappingsUri) so that we can use a "logical" name from the content
      of any *.xml files in the search locations (instead of having to specify explicit paths...).
      Except for 'profileMappingsUri', this approach did not seem to work as there is no
      "logical" mapping name. mappingsUri is specified without 'Mappings' suffix on file name.
      See http://docs.andromda.org/mappings.html.
      A default empty mda/src/main/config/mappings/JavaMappings.xml is provided with the generated project.
      It can be used to customize mappings from UML to Java datatypes used by all cartridges. -->
    <mappingsSearchLocations>
        <!-- Add additional mappings files under mda/src/main/config/mappings, referenced by languageMappingsUri -->
        <location patterns="*.xml">${andromda.conf.dir}/mappings</location>
    </mappingsSearchLocations>
    <!-- To add/remove an additional cartridge to the code generation process (i.e. modeldump):
    - Add the namespace elements to andromda.xml
    - Add the cartridge dependency (including version) to mda/pom.xml
    - Add additional build dependencies to individual project pom.xml files -->
    <namespaces>
        <!-- namespace property values used by ALL cartridges -->
        <!-- Variable references refer to properties defined in root component pom.xml or componentParent/pom.xml files -->
        <!-- See http://www.andromda.org/docs/andromda-metafacades/andromda-uml-metafacades/andromda-metafacades-emf-uml2/namespace.html -->
        <namespace name="default">
            <properties>
                <!-- Use enums, strongly typed collections with templates, and @annotation markups -->
                <property name="enableTemplating">${andromda.default.enableTemplating}</property>
                <property name="enableAnnotations">${andromda.default.enableAnnotations}</property>
                <property name="typeSafeEnumsEnabled">${andromda.default.typeSafeEnumsEnabled}</property>
                <!-- Replaces default JavaMappings with customized mappings which extend Java, for PrimitiveTypes etc. -->
                <property name="languageMappingsUri">${andromda.default.languageMappingsUri}</property>
                <property name="wrapperMappingsUri">${andromda.default.wrapperMappingsUri}</property>
                <property name="sqlMappingsUri">${sql.mappings}</property>
                <property name="jdbcMappingsUri">${andromda.default.jdbcMappingsUri}</property>
                <property name="maxSqlNameLength">${andromda.default.maxSqlNameLength}</property>
                <property name="foreignKeySuffix">${andromda.default.foreignKeySuffix}</property>
                <property name="ejbJndiNamePrefix">${project.artifactId}-${project.version}</property>
                <!-- masking properties configure the way your model element names are generated -->
                <property name="modelElementNameMask">${andromda.default.modelElementNameMask}</property>
                <property name="xmlEncoding">${andromda.default.xmlEncoding}</property>
                <property name="enumerationLiteralNameMask">${andromda.default.enumerationLiteralNameMask}</property>
                <property name="hibernateVersion">${andromda.hibernate.hibernateVersion}</property>
                <property name="hibernateEntityAnnotations">${andromda.hibernate.hibernateEntityAnnotations}</property>
                <property name="hibernateMappingStrategy">${andromda.hibernate.hibernateMappingStrategy}</property>
                <property name="hibernateQueryUseNamedParameters">${andromda.hibernate.hibernateQueryUseNamedParameters}</property>
                <property name="persistenceContainerName">${andromda.hibernate.persistenceContainerName}</property>                
                <property name="hibernateInheritanceStrategy">${andromda.hibernate.hibernateInheritanceStrategy}</property>
                <property name="hibernateEnableCache">${andromda.hibernate.hibernateEnableCache}</property>
                <!-- Override default @todo to set to Eclipse default>
                <property name="toDoTag">TODO</property -->
                <!-- Automatically makes English names plural (generally adds s if missing).
                default=true, set to false if using non-English models or Jaxb/CXF 2.0 or JAXB simpleBindingMode=false.>
                <property name="pluralizeAssociationEndNames">true</property>
                <property name="pluralizeAttributeNames">true</property>
                <property name="pluralizeParameterNames">true</property -->
                <!-- JDK5 error on @Override on methods implemented from Interfaces -->
                <property name="javaVersion">${andromda.default.javaVersion}</property>
                <property name="xmlAttributes">${andromda.default.xmlAttributes}</property>
                <property name="generateEqualPropertiesMethod">${andromda.default.generateEqualPropertiesMethod}</property>
                <property name="hibernateSearch">${andromda.hibernate.hibernateSearch}</property>
                <property name="hibernateUseQueryCache">${andromda.hibernate.hibernateUseQueryCache}</property>
                <property name="springVersion">${andromda.spring.springVersion}</property>         
                <property name="useBeanValidation">${andromda.default.useBeanValidation}</property>   
                <property name="compositeIdentifierTypeNameSuffix">${andromda.hibernate.compositeIdentifierTypeNameSuffix}</property>

				<!-- Enable entity auditing -->
                <property name="entityAuditing">${andromda.hibernate.entityAuditing}</property>
                <property name="entityAuditPackage">${usp.facet.audit.package}</property>
                <property name="entityEventPackage">${usp.facet.event.package}</property>
                
                <!-- Enable change tracking -->
                <property name="enableChangeTracking">${andromda.spring.enableChangeTracking}</property>   
                
                <!-- Entity versioning -->
                <property name="versionProperty">${andromda.hibernate.version.property}</property>
                <property name="enableVersionChecking">${andromda.default.enableVersionChecking}</property> 
                
                <!-- Permission evaluator and security expression handlers -->
                <property name="permissionEvaluatorRef">${andromda.spring-mvc.permissionEvaluatorRef}</property>
                <property name="securityExpressionHandler">${andromda.spring-mvc.securityExpressionHandler}</property>
                <property name="webSecurityExpressionHandler">${andromda.spring-mvc.webSecurityExpressionHandler}</property> 
                <property name="securitySpelPackage">${usp.facet.security.package}.authorisation.spel</property>
                
                <!-- Uncomment the following when we need to enforce permissions based on target object instances -->
                <!--  <property name="securityExpressionHandler">com.olmgroup.usp.facets.security.authorisation.spel.SpelMethodSecurityExpressionHandler</property> -->
                
                <property name="propertyPlaceholders">${andromda.spring.propertyPlaceholders}</property>
                <property name="allowEncryptedProperties">${andromda.spring.allowEncryptedProperties}</property>
                <property name="propertyEncryptionPasswordEnvironmentName">${andromda.spring.propertyEncryptionPasswordEnvironmentName}</property>
                
                <property name="applicationName">${app.final.name}</property>                         
                
                <!-- Application naming and version for e.g. schemas -->
               	<property name="moduleVersion">${andromda.java.applicationVersion}</property>                           
			   	<property name="moduleName">${andromda.java.applicationNamespace}</property>
			   	<property name="moduleDisplayName">${andromda.java.applicationName}</property>
			   	<property name="moduleDescription">${andromda.java.applicationDescription}</property> 
                <property name="generateUnifiedTests">true</property>
            </properties>
        </namespace>
		
        <!-- See http://http://devdocs.group.olm.int/cartridges/spring/namespace.html -->
        <namespace name="spring">
            <properties>
                <property name="dataSourceEnabled">${andromda.spring.dataSourceEnabled}</property>
            	<property name="declareHibernateConfiguration">${andromda.spring.declareHibernateConfiguration}</property>
            	<property name="declareGlobalMethodSecurityConfiguration">${andromda.spring.declareGlobalMethodSecurityConfiguration}</property>
            	<property name="declareMessageConfiguration">${andromda.spring.declareMessageConfiguration}</property>
            	<property name="generateBeanRefFactory">${andromda.spring.generateBeanRefFactory}</property>
            	<!-- These properties would normally be set outwith this file in external properties -->
                <property name="dataSource">${dataSource}</property>
                <property name="username">${jdbc.username}</property>
                <property name="password">${jdbc.password}</property>
                <property name="driver">${jdbc.driver}</property>
                <property name="connectionUrl">${jdbc.url}</property>
                <property name="hibernateDialect">${hibernate.dialect}</property>
                <!-- These properties would normally be set outwith this file in external properties; they may be set by particular
                     maven profiles to allow for different RDBMS to be used to back tests -->
                <property name="testUsername">${testdb.username}</property>
                <property name="testPassword">${testdb.password}</property>
                <property name="testDriver">${testdb.driver}</property>
                <property name="testConnectionUrl">${testdb.url}</property>
                <!--  Other hibernate related properties -->
                <property name="hibernateHbm2DDLAuto">update</property>
                <property name="sessionFactory">${andromda.spring.sessionFactory}</property>
                <property name="localTransactionManager">${andromda.spring.localTransactionManager}</property>
                <property name="serviceHibernateInterceptorEnabled">${andromda.spring.serviceHibernateInterceptorEnabled}</property>
                <property name="hibernateQueryCacheFactory">${andromda.spring.hibernateQueryCacheFactory}</property>
                <property name="mergeMappingsUri">${andromda.spring.mergeMappingsUri}</property>
                <property name="springTypesPackage">${application.package}.spring</property>
                <property name="springCommonTypesPackage">${andromda.spring.springCommonTypesPackage}</property>
                <!-- Various outlets -->
                <property name="daos">${andromda.spring.daos}</property>
                <property name="dao-impls">${andromda.spring.dao-impls}</property>
                <property name="spring-configuration">${andromda.spring.spring-configuration}</property>
                <property name="server-test-config">${andromda.spring.server-test-config}</property>
                <property name="server-test">${andromda.spring.server-test}</property>
                <property name="services">${andromda.spring.services}</property>
                <property name="service-impls">${andromda.spring.service-impls}</property>
                <property name="service-interfaces">${andromda.spring.service-interfaces}</property>
                <property name="criteria">${andromda.spring.criteria}</property>				
                <property name="spring-value-objects">${andromda.spring.spring-value-objects}</property>
                <property name="spring-value-object-impls">${andromda.spring.spring-value-object-impls}</property>                
                <property name="useValueObjectTransforms">${andromda.spring.useValueObjectTransforms}</property>
                <property name="valueObjectTransformTypesPackage">${andromda.spring.valueObjectTransformTypesPackage}</property>
                <property name="searchServicePackage">${andromda.spring.searchServicePackage}</property>
				<property name="hibernateAnnotationsPackagesToScan">${andromda.spring.hibernateAnnotationsPackagesToScan}</property>
				<property name="additionalSpringBeansPackagesToScan">${andromda.spring.additionalSpringBeansPackagesToScan}</property>              
                <property name="messageBundlePath">${andromda.spring.messageBundlePath}</property>
                <property name="hibernateSearchDefaultDirectoryProvider">${andromda.spring.hibernateSearchDefaultDirectoryProvider}</property>
                <property name="hibernateSearchIndexBase">${andromda.spring.hibernateSearchIndexBase}</property>
				<!-- Generate Service Tests -->
				<property name="generateServiceTests">${andromda.spring.generateServiceTests}</property>
				<property name="serviceTestFramework">${andromda.spring.serviceTestFramework}</property>
				<property name="testContextConfiguration">/relationshipsrecording-context-test.xml</property>
				<property name="service-tests">${andromda.spring.service-tests}</property>
				<property name="service-test-impls">${andromda.spring.service-test-impls}</property>
                <property name="dbUnitTestTypesPackage">${andromda.spring.dbUnitTestTypesPackage}</property>
                <property name="springDbUnitServiceDatabaseOperationLookupClass">${andromda.spring.springDbUnitServiceDatabaseOperationLookupClass}</property>
                <property name="springDbUnitServiceDataSetLoaderClass">${andromda.spring.springDbUnitServiceDataSetLoaderClass}</property>
                <!-- Generate Dao Tests -->
                <property name="generateDaoTests">${andromda.spring.generateDaoTests}</property>
                <property name="dao-tests">${andromda.spring.dao-tests}</property>
                <property name="dao-test-impls">${andromda.spring.dao-test-impls}</property>
                <property name="dao-test-data">${andromda.spring.dao-test-data}</property>                
                <property name="springDbUnitDaoDatabaseOperationLookupClass">${andromda.spring.springDbUnitDaoDatabaseOperationLookupClass}</property>                
				<property name="springDbUnitDaoDataSetLoaderClass">${andromda.spring.springDbUnitDaoDataSetLoaderClass}</property>
                <!-- Annotations -->
				<property name="enableSpringTransactionAnnotations">${andromda.spring.enableSpringTransactionAnnotations}</property>
                <property name="enableSpringDeadLockRetryAnnotations">${andromda.spring.enableSpringDeadLockRetryAnnotations}</property>
				<property name="enableSpringBeanAnnotations">${andromda.spring.enableSpringBeanAnnotations}</property>
				<property name="enableSpringTaskAnnotations">${andromda.spring.enableSpringTaskAnnotations}</property>
				<!-- Naming strategy -->
				<property name="hibernateNamingStrategy">${andromda.spring.hibernateNamingStrategy}</property>
                <!-- Validation -->
                <property name="enableServiceMethodParameterValidation">${andromda.spring.enableServiceMethodParameterValidation}</property>
                <property name="alwaysValidateValueObjects">${andromda.spring.alwaysValidateValueObjects}</property>
                <!-- Generated DAO interfaces and base implementations are genericised -->
                <property name="hibernateUseGenericisedDaos">${andromda.spring.hibernateUseGenericisedDaos}</property>
                <!-- Naming constraints -->
                <property name="serviceNamePattern">${andromda.spring.serviceNamePattern}</property>
                <property name="serviceNamePatternError">${andromda.spring.serviceNamePatternError}</property>
                <!-- Application context -->
                <property name="enableAspectjAutoProxy">${andromda.spring.enableAspectjAutoProxy}</property>
                <property name="applicationContext">${andromda.spring.applicationContext}</property>
                <property name="generateConfigurationFile">${andromda.spring.generateConfigurationFile}</property>
                <!-- Hibernate statistics -->
                <property name="hibernateGenerateStatistics">${andromda.spring.hibernateGenerateStatistics}</property>
                <property name="facetMode">${andromda.spring.facetMode}</property>
                <property name="importResources">${andromda.spring.importResources}</property>
                <property name="appConfigurationContextLocation">${andromda.spring.appConfigurationContextLocation}</property>
                <!-- Version information -->
                <property name="generateBuildVersionImpl">${andromda.spring.generateBuildVersionImpl}</property>
                <property name="buildVersionBaseName">${andromda.spring.buildVersionBaseName}</property>
                <!-- Generate standard spring context test -->
                <property name="rootPackage">${andromda.spring.rootPackage}</property>           
                <property name="generateSpringContextTest">${andromda.spring.generateSpringContextTest}</property>     
                <!-- Coded Entry Validation -->
                <property name="codedEntryValidation">${andromda.spring.codedEntryValidation}</property>
                <property name="configurationFacetPackage">${andromda.spring.configurationFacetPackage}</property>

                <!-- Security configuration -->                 
 		       	<property name="secureServicesByDefault">${andromda.spring.secureServicesByDefault}</property>
                <property name="validateMethodsSecured">${andromda.spring.validateMethodsSecured}</property>
        		<property name="secureVOsByDefault">${andromda.spring.secureVOsByDefault}</property>
        		<property name="dynamic-security-resources">${andromda.spring.dynamicSecurityResources}</property>
        		<property name="securityPermissions">${andromda.spring.securityPermissions}</property>
                <property name="securityNamespace">${andromda.spring.securityNamespace}</property>
                <property name="securitySchemaLocation">${andromda.spring.securitySchemaLocation}</property>
                <property name="catchAllPostOpSecurity">${andromda.spring.catchAllPostOpSecurity}</property> 
            </properties>
        </namespace>
        
        <!-- See http://http://devdocs.group.olm.int/cartridges/spring-mvc/namespace.html -->
        <namespace name="springmvc">
            <properties>
                <property name="displayName">${andromda.spring-mvc.displayName}</property>
                <property name="configuration">${andromda.spring-mvc.configuration}</property>
                <property name="spring-configuration">${andromda.spring-mvc.spring-configuration}</property>  
                <property name="spring-test-configuration">${andromda.spring-mvc.spring-test-configuration}</property>          
                <property name="controllers">${andromda.spring-mvc.controllers}</property>
                <property name="controller-impls">${andromda.spring-mvc.controller-impls}</property>
                <property name="merge-mappings">${andromda.spring-mvc.merge-mappings}</property>
                <property name="mergeMappingsUri">${andromda.spring-mvc.mergeMappingsUri}</property>
                <property name="contextConfigLocation">${andromda.spring-mvc.contextConfigLocation}</property>
                <property name="useExpressionsForInterceptURLs">${andromda.spring-mvc.useExpressionsForInterceptURLs}</property>
                <property name="login-page">${andromda.spring-mvc.login-page}</property>
                <property name="loginChannel">${andromda.spring-mvc.loginChannel}</property>
                <property name="authenticationSuccessHandler">${andromda.spring-mvc.authenticationSuccessHandler}</property>  
                <property name="defaultURLAccess">${andromda.spring-mvc.defaultURLAccess}</property> 
                <property name="exposedContextBeanNames">${andromda.spring-mvc.exposedContextBeanNames}</property>
                <property name="authenticationProvider">${andromda.spring-mvc.authenticationProvider}</property>
                <property name="ldapServerUrl">${andromda.spring-mvc.ldapServerUrl}</property>  
                <property name="ldapServerManagerDN">${andromda.spring-mvc.ldapServerManagerDN}</property>
                <property name="ldapServerManagerPassword">${andromda.spring-mvc.ldapServerManagerPassword}</property>                 
                <property name="ldapEmbeddedServerLdifResource">${andromda.spring-mvc.ldapEmbeddedServerLdifResource}</property>
                <property name="ldapEmbeddedServerPort">${andromda.spring-mvc.ldapEmbeddedServerPort}</property>
                <property name="ldapEmbeddedServerRoot">${andromda.spring-mvc.ldapEmbeddedServerRoot}</property>
                <property name="ldapGroupRoleAttribute">${andromda.spring-mvc.ldapGroupRoleAttribute}</property>                
                <property name="ldapGroupSearchBase">${andromda.spring-mvc.ldapGroupSearchBase}</property>
                <property name="ldapGroupSearchFilter">${andromda.spring-mvc.ldapGroupSearchFilter}</property>
                <property name="ldapRolePrefix">${andromda.spring-mvc.ldapRolePrefix}</property>
                <property name="ldapUserContextMapperRef">${andromda.spring-mvc.ldapUserContextMapperRef}</property>
                <property name="ldapUserDetailsClass">${andromda.spring-mvc.ldapUserDetailsClass}</property>
                <property name="ldapUserDNPattern">${andromda.spring-mvc.ldapUserDNPattern}</property>
                <property name="ldapUserSearchFilter">${andromda.spring-mvc.ldapUserSearchFilter}</property>
                <property name="ldapUserSearchBase">${andromda.spring-mvc.ldapUserSearchBase}</property>
                <property name="userProfileAuthorization">${andromda.spring-mvc.userProfileAuthorization}</property>
                <property name="maxUploadSize">${andromda.spring-mvc.maxUploadSize}</property>
                <property name="alwaysValidateValueObjects">${andromda.spring-mvc.alwaysValidateValueObjects}</property>
                <property name="logout-url">${andromda.spring-mvc.logout-url}</property>
                <property name="logout-success-url">${andromda.spring-mvc.logout-success-url}</property>
                <property name="logout-success-handler-ref">${andromda.spring-mvc.logout-success-handler-ref}</property>
                <property name="invalidateSessionOnLogout">${andromda.spring-mvc.invalidateSessionOnLogout}</property>
                <property name="deleteCookies">${andromda.spring-mvc.deleteCookies}</property>
                <property name="importResources">${andromda.spring-mvc.importResources}</property>
                <property name="importSecurityResources">${andromda.spring-mvc.importSecurityResources}</property>
                <property name="permissionVoters">${andromda.spring-mvc.permissionVoters}</property>

                <!-- Naming constraints -->
                <property name="controllerNameValidPattern">${andromda.spring-mvc.controllerNameValidPattern}</property>
                <property name="controllerNamePatternError">${andromda.spring-mvc.controllerNamePatternError}</property>
                <!-- Controller tests -->
                <property name="generateControllerTests">${andromda.spring-mvc.generateControllerTests}</property>
                <property name="testContextConfiguration">/relationshipsrecording-context-test.xml</property>
                <property name="testServletContextConfiguration">/relationshipsrecording-servlet-context-test.xml</property>
                <property name="controller-tests">${andromda.spring-mvc.controller-tests}</property>
                <property name="controller-test-impls">${andromda.spring-mvc.controller-test-impls}</property>
                <property name="controller-test-data">${andromda.spring-mvc.controller-test-data}</property>
                <property name="dbUnitTestTypesPackage">${andromda.spring-mvc.dbUnitTestTypesPackage}</property>		
                <property name="springDbUnitControllerDatabaseOperationLookupClass">${andromda.spring-mvc.springDbUnitControllerDatabaseOperationLookupClass}</property>
                <property name="springDbUnitControllerDataSetLoaderClass">${andromda.spring-mvc.springDbUnitControllerDataSetLoaderClass}</property>
                <!-- Generation control properties -->
                <property name="generateWebXml">${andromda.spring-mvc.generateWebXml}</property>
                <property name="generateAppContext">${andromda.spring-mvc.generateAppContext}</property>
                <property name="staticWebXml">${andromda.spring-mvc.staticWebXml}</property>
                <property name="staticAppContext">${andromda.spring-mvc.staticAppContext}</property>
                <property name="projectArtifact">${andromda.spring-mvc.projectArtifact}</property>
                <property name="rootPackage">${andromda.spring.rootPackage}</property>
                <property name="auto-config">${andromda.spring-mvc.autoConfig}</property>
                <property name="alwaysUseDefaultTarget">${andromda.spring-mvc.alwaysUseDefaultTarget}</property>
                <property name="login-processing-url">${andromda.spring-mvc.loginProcessingURL}</property>
                <property name="generateInitDBTest">${andromda.spring-mvc.generateInitDBTest}</property>
                <property name="dbunitInitDBCommand">${andromda.spring-mvc.dbunitInitDBCommand}</property>
				<!-- Security Configuration -->
                <property name="enableRestSecurity">${andromda.spring-mvc.enableRestSecurity}</property>
                <property name="enableMultiProfiles">${andromda.spring-mvc.enableMultiProfiles}</property>                
                <property name="generateLoginController">${andromda.spring-mvc.generateLoginController}</property>
                <property name="generateLoginRESTController">${andromda.spring-mvc.generateLoginRESTController}</property>
                <property name="index-page">${andromda.spring-mvc.indexPage}</property>
                <property name="enableExcelProductConfiguration">${andromda.spring-mvc.enableExcelProductConfiguration}</property>
                <property name="productConfigurationFile">${andromda.spring-mvc.productConfigurationFile}</property>
                <property name="productDataTypes">${andromda.spring-mvc.productDataTypes}</property>
                <property name="enableExcelModelOfficeConfiguration">${andromda.spring-mvc.enableExcelModelOfficeConfiguration}</property>
                <property name="modelOfficeConfigurationFile">${andromda.spring-mvc.modelOfficeConfigurationFile}</property>
                <property name="modelOfficeDataTypes">${andromda.spring-mvc.modelOfficeDataTypes}</property>
            </properties>
        </namespace>
        
        <!-- See http://http://devdocs.group.olm.int/cartridges/hibernate/namespace.html -->
        <namespace name="hibernate">
            <properties>
                <property name="hibernateEnableAssociationsCache">${andromda.hibernate.hibernateEnableAssociationsCache}</property>
                <property name="hibernateEntityCache">${andromda.hibernate.hibernateEntityCache}</property>
                <property name="hibernateAssociationCache">${andromda.hibernate.hibernateAssociationCache}</property>
                <property name="hibernateTypeMappingsUri">${andromda.hibernate.hibernateTypeMappingsUri}</property>
                <property name="defaultHibernateGeneratorClass">${andromda.hibernate.defaultHibernateGeneratorClass}</property>
                <!-- <property name="hibernateAggregationCascade">${andromda.hibernate.hibernateAggregationCascade}</property> -->
                <property name="hibernateDefaultCascade">${andromda.hibernate.hibernateDefaultCascade}</property>
                <property name="entities">${andromda.hibernate.entities}</property>
                <property name="entity-impls">${andromda.hibernate.entity-impls}</property>
                <property name="entity-mappings">${andromda.hibernate.entity-mappings}</property>
                <property name="user-types">${andromda.hibernate.user-types}</property>
                <property name="customTypesPackage">${application.package}.hibernate</property>
                <property name="userTypesPackage"></property>
                <property name="compositionDefinesEagerLoading">${andromda.hibernate.compositionDefinesEagerLoading}</property>
                <property name="cache">${andromda.hibernate.cache}</property>
                <property name="defaultEntityDiscriminatorColumn">${andromda.hibernate.defaultEntityDiscriminatorColumn}</property>
                <property name="mergeMappingsUri">${andromda.hibernate.mergeMappingsUri}</property>
                <property name="searchBridgePackage">${andromda.hibernate.searchBridgePackage}</property>
                <property name="defaultAnalyzer">${andromda.hibernate.defaultAnalyzer}</property>
                <!-- Naming constraints -->
                <property name="entityNameValidPattern">${andromda.hibernate.entityNameValidPattern}</property>
                <property name="entityNamePatternError">${andromda.hibernate.entityNamePatternError}</property>
                <!-- Change tracking -->
                <property name="changeTracking">${andromda.hibernate.changeTracking}</property>
                <property name="changeTrackAllEntities">${andromda.hibernate.changeTrackAllEntities}</property>
            </properties>
        </namespace>
		
        <!-- See http://http://devdocs.group.olm.int/cartridges/java/namespace.html -->
        <namespace name="java">
            <properties>
                <property name="enumerations">${andromda.java.enumerations}</property>
                <property name="exceptions">${andromda.java.exceptions}</property>
                <property name="value-objects">${andromda.java.value-objects}</property>
                <property name="interfaces">${andromda.java.interfaces}</property>
                <property name="vo-json-schemas">${andromda.java.value-object-schemas}</property>
				<property name="useAutopopulatingLists">${andromda.java.useAutopopulatingLists}</property>
				<property name="JSONBindingAnnotations">${andromda.java.JSONBindingAnnotations}</property>
				<property name="serializable">${andromda.java.serializable}</property>
				<property name="XMLBindingAnnotations">${andromda.java.XMLBindingAnnotations}</property>
                <!-- Naming constraints -->
                <property name="valueObjectNameValidPattern">${andromda.java.valueObjectNameValidPattern}</property>
                <property name="valueObjectNamePatternError">${andromda.java.valueObjectNamePatternError}</property>
            </properties>
        </namespace>
        <namespace name="uml">
          <properties>
            <property name="uml-description">${andromda.uml.description}</property>
            <property name="umlDescription">${viewControllers.uml.description.file}</property>
          </properties>
        </namespace>
    </namespaces>
</andromda>