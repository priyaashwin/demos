#! /bin/sh
# Set the Classpath to include our conf directory
export CLASSPATH=$CATALINA_BASE/conf

#Ensure that the environment name has been set and not left at default
if [ $OLM_ENV = "__ENV_NAME__" ] || [ $OLM_ENV = "Kubernetes-Eclipse-Cluster-Build" ] ; then
	echo "Env Name is ${OLM_ENV}. Please set it to something unique. App will not start"
	exit 1
fi

#Memory Settings
APP_JVMMAX="${APP_JVMMAX:-1536}"
APP_JVMMIN="${APP_JVMMIN:-512}"

#Monitoring - enabled by default
# kubectl -nasa port-forward [your-pod-name-here] 1088
APP_JMXPORT="${APP_JMXPORT:-1088}"
APP_JMXENABLE="${APP_JMXENABLE:-T}"
JMXSETTINGS=""
if [ $APP_JMXENABLE = "T" ]; then
  JMXSETTINGS="-Dcom.sun.management.jmxremote \
	-Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Dcom.sun.management.jmxremote.local.only=false \
    -Dcom.sun.management.jmxremote.port=${APP_JMXPORT} \
    -Dcom.sun.management.jmxremote.rmi.port=${APP_JMXPORT} \
    -Djava.rmi.server.hostname=127.0.0.1"
fi

SPRING_PROFILES=""
# Replace the  fqdn token in the template with a real value for Fiscal
if [ "$app_event_broker_host" ]; then
  SPRING_PROFILES="-Dspring.profiles.active=production,integration"
else
  echo "No Event Broker specified (app_event_broker_host), Integration profile not enabled"
fi

# ITS-15144 ECM k8s lucene index PV review reindex
[ "${app_reindex_on_startup}" = "true" ]&& rm -rf /usr/local/tomcat/data/lucene/*

# check if sql proxy is set via env variable - if so  ping sql proxy until it gets available
# this prevents eclipse from starting up without sqlproxy being available
sql_proxy=$(printenv | grep sqlproxy-dep | awk -F/ '{print $3}' | awk -F: '{print $1}')
if [[ ! -z $sql_proxy  ]] ; then
  while ! nc -z -w 1 $sql_proxy 5432 ; do sleep 1 ; done
fi

export CATALINA_OPTS="-Xmx${APP_JVMMAX}M -Xms${APP_JVMMIN}M -Dolm.env.name=$OLM_ENV -Dfile.encoding=UTF-8 -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+UseParNewGC ${JMXSETTINGS} ${SPRING_PROFILES} -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps"

#Run the setdiscovery.sh file to populat eureka-client.properties
source /usr/local/tomcat/conf/setdiscovery.sh
