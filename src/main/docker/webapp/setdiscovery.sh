#! /bin/sh
echo app_discovery_eureka_port=$app_discovery_eureka_port
echo app_discovery_eureka_region=$app_discovery_eureka_region
echo app_discovery_eureka_prefersamezone=$app_discovery_eureka_prefersamezone
echo app_discovery_eureka_shouldusedns=$app_discovery_eureka_shouldusedns
echo app_discovery_eureka_defaultserviceurl=$app_discovery_eureka_defaultserviceurl

# Set defaults 
if [ "$app_discovery_eureka_name" = "" ]; then
    app_discovery_eureka_name=relationshipsrecording
fi

if [ "$app_discovery_eureka_vip_address" = "" ]; then
    app_discovery_eureka_vip_address=relationshipsrecording
fi

if [ "$app_discovery_eureka_port" = "" ]; then
    app_discovery_eureka_port=80
fi

if [ "$app_discovery_eureka_region" = "" ]; then
    app_discovery_eureka_region=default
fi

if [ "$app_discovery_eureka_prefersamezone" = "" ]; then
    app_discovery_eureka_prefersamezone=true
fi

if [ "$app_discovery_eureka_shouldusedns" = "" ]; then
    app_discovery_eureka_shouldusedns=false
fi

if [ "$app_discovery_eureka_defaultserviceurl" = "" ]; then    
    app_discovery_eureka_defaultserviceurl="EUREKA_SERVICEURL_HAS_NOT_BEEN_SET" 
fi

#Update the eureka-client.properties file with env vars
sed -i -e "s/EUREKA_NAME/$app_discovery_eureka_name/" /usr/local/tomcat/conf/eureka-client.properties
sed -i -e "s/EUREKA_VIP_ADDRESS/$app_discovery_eureka_vip_address/" /usr/local/tomcat/conf/eureka-client.properties
sed -i -e "s/EUREKA_REGION/$app_discovery_eureka_region/" /usr/local/tomcat/conf/eureka-client.properties
sed -i -e "s/EUREKA_PORT/$app_discovery_eureka_port/" /usr/local/tomcat/conf/eureka-client.properties
sed -i -e "s/EUREKA_PREFER_SAME_ZONE/$app_discovery_eureka_prefersamezone/" /usr/local/tomcat/conf/eureka-client.properties
sed -i -e "s/EUREKA_USE_DNS/$app_discovery_eureka_shouldusedns/" /usr/local/tomcat/conf/eureka-client.properties

#Since the url will contain slashes we need to use a different separator for the sed expression, in this case a semi-colon
sed -i -e 's;EUREKA_SERVICE_URL_DEFAULTZONE;'"$app_discovery_eureka_defaultserviceurl"';' /usr/local/tomcat/conf/eureka-client.properties
