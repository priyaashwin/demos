#FROM eu.gcr.io/usp-dev/liquibase:3.4.2
FROM eu.gcr.io/usp-dev/liquibase:3.4.2-jre-8
MAINTAINER Barny Relph <barnaby.relph@olmgroup.com>

ENV POSTGRES_VERSION=9.4.1208 \
  JRE_VERSION=7

#Download the correct Postgres driver for this product
RUN wget -q --no-check-certificate \
  https://jdbc.postgresql.org/download/postgresql-${POSTGRES_VERSION}.jre${JRE_VERSION}.jar \
  -O /opt/liquibase/lib/postgresql-${POSTGRES_VERSION}.jre${JRE_VERSION}.jar

#Place our liquibase scripts into the image
COPY scripts/ .

#Copy lockback xml (a personalised version for docker)
COPY logback.xml .

#Liquibase scripts for Eclipse require a liquibase-tools JAR. Copy this into Liquibase's /lib directory
COPY liquibase-tools-with-dependencies.jar /opt/liquibase/lib/

#Copy and mark executable our main script
COPY usp-upgrade.sh /
RUN chmod u+x /usp-upgrade.sh

#Mark the entrypoint for our script
ENTRYPOINT ["/usp-upgrade.sh"]
