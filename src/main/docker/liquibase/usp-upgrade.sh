#!/bin/sh

# If this container is called as an init-container, then there are situations where we may not want to run Liquibase
skip_liquibase="${skip_liquibase:-F}"
if [ $skip_liquibase = "T" ]; then
	echo "skip_liquibase has been set. Exiting cleanly without running Liquibase scripts"
	exit 0
fi

#Check mandatory variables
if [ -z "$app_jdbc_url" ]
then
  echo "app_jdbc_url is not defined"
  exit 1
fi
if [ -z "$app_jdbc_username" ]
then
  echo "app_jdbc_username is not defined"
  exit 1
fi
if [ -z "app_jdbc_password" ]
then
  echo "app_jdbc_password is not defined"
  exit 1
fi

JAVA_OPTS='$JAVA_OPTS -Dlogback.configurationFile=./logback.xml'

#Execute Liquibase
liquibase \
--changeLogFile=/changelogs/db.eclmanagement.xml \
--url=$app_jdbc_url \
--username=$app_jdbc_username \
--password=$app_jdbc_password \
--driver=org.postgresql.Driver \
--classpath=/changelogs/ \
--logLevel=info \
update
