<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<% 
//HttpSession session = request.getSession();
String timeout =request.getParameter("timeout");
String message="No timeout parameter specified";
if(timeout!=null){
	try{
		Integer timeoutMinutes = Integer.parseInt(timeout);
		if(60*timeoutMinutes<=session.getMaxInactiveInterval()){
			session.setMaxInactiveInterval(60*timeoutMinutes);
		  message="Your session is now "+session.getMaxInactiveInterval()/60+" minutes. Enjoy.";  
		}else{
		  message="You can only reduce your session from: "+(session.getMaxInactiveInterval()/60);
		}
		
	}catch(NumberFormatException e){
		message="value for timeout: "+timeout+" is not a number";
	}

}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Session timeout change - Temporary page</title>
</head>
<body>
<%=message %>
</body>
</html>