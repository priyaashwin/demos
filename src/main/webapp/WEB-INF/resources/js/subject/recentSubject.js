YUI.add('usp-recent-subject', function(Y) {
	var L=Y.Lang, url, data;
	var createAccess = function(url, data){
		Y.io(url, {
			method: 'POST',
			data: Y.JSON.stringify(data),
			headers: {
		        'Content-Type': 'application/json',
		    },
			
		    on : {
		        success : function (tx, r) {
		            try {
		            }
		            catch(e) {
		                	Y.log('Failed to add subject context','debug');
		                return;
		            }
		        }
		    }
		});
	};
	
	Y.on('currentSubject:changed', function(e) {	
		type = e.after.type;
		
		switch(type){
		case "person":
				url = L.sub('/eclipse/rest/person/{id}/access/',{id:e.after.id});
				data = {
					_type : "NewPersonAccess",
					personId : e.after.id
				};
				createAccess(url, data);
			break;
		default:
		}
			

		
	});
	
}, '0.0.1', {
	requires : ['io-base', 'yui-base', 'json-stringify', 'event-custom' ]});