

YUI.add('app-button-model', function(Y) {
	
	'use-strict';

	Y.namespace('app').ButtonModel = Y.Base.create('buttonModel',	
		Y.Model, [], {}, {
		ATTRS: {
			
			action: {
				value: ''
			},
			
			event: {
				value: ''
			},
			
			cssActive: {
				value: ''
			},
	
			cssButton: {
				value: ''
			},
			
			cssDisabled: {
				value: ''
			},
			
			label: {
				value: ''
			},
			
			target: {
				value: ''
			},
			
			title: {
				value: ''
			},
			
			url: {
				value: '#'
			}
		}
	});
	
	
}, '0.0.1', {
	requires : ['model']
});