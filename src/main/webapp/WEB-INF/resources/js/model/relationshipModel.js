YUI.add("person-relationship-model", function(Y) {

	var J=Y.JSON;
	
	Y.namespace("usp.relationships").PersonModel = Y.Base.create('personModel', Y.Model, [],
			{
			/*
			 * Model methods
			 */
			}, {
				/*
				 * Model attributes
				 */
				ATTRS : {
					name : {
						value: ""
					},
					forename : {
						value : ""
					},
					surname : {
						value : ""
					},
					gender : {

					},
					isDeceased : {
						value : false
					},
					dateOfBirth : {},
					age : {
						validator : function(val) {
							return Y.Lang.isNumber(val);
						}
					},
					/**
					 * All relationship objects are of the form {
					 * classifier:"xxx", personOrganisation, personPerson etc
					 * targetId:"yyy" }
					 * 
					 * 
					 */
					personalRelationships : {
						valueFn : function() {
							return [];
						}
					},
					professionalRelationships : {
						valueFn : function() {
							return [];
						}
					},
					organisationRelationships : {
						valueFn : function() {
							return [];
						}
					},
					_type:{
						value:"PERSON"
					}
				}
			});

	Y.namespace("usp.relationships").OrganisationModel = Y.Base.create('organisationModel',
			Y.Model, [], {
			/*
			 * Model methods
			 */
			}, {
				/*
				 * Model attributes
				 */
				ATTRS : {
					name : {
						value : ""
					},
					_type:{
						value:"ORGANISATION"
					}

				}
			});

	// Create a new Y.usp.relationships.RelationshipModel class that extends Y.Model.
	Y.namespace("usp.relationships").RelationshipsModel = Y.Base.create('relationshipsModel', Y.Model, [Y.ModelSync.REST], {
		parse : function (response){
			var result={};
			
			Y.log("Parsing response");
			Y.log(response);
						
			try{
				
				result=J.parse(response);
				
				//create new Person List
				result.people=new Y.usp.relationships.PersonList({items:result.people});
				//create new Organisation List				
				result.organisations=new Y.usp.relationships.PersonList({items:result.organisations});				
			}catch(ex){
				Y.log("Loading of data from server for Y.usp.relationships.RelationshipsModel failed. No results defined in response body "+ex);
				
				//fire an error
				this.fire('error', {
                    error   : ex,
                    response: response,
                    src     : 'parse'
                });
			}
					
			//return results
			return result;
		},		
		getRoot : function() {
			var classifier = this.get('rootClassifier');
			switch (classifier) {
			case "Person":
				return this.get('people').getById(this.get('rootId'));
				break;
			case "Organisation":
				return this.get('organisations').getById(
						this.get('rootId'));
				break;
			}
		}
	}, {
		ATTRS : {
			rootId : {
				value : ""
			},
			rootClassifier : {
				setter : function(val) {
					if (val === "Person" || val === "Organisation") {
						return val;
					}
					return Y.Attribute.INVALID_VALUE;
				}
			},
			people : {
	
			},
			organisations : {
	
			}
		}
	});

	// Create a new Y.usp.relationships.PersonList class that extends Y.ModelList.
	Y.namespace("usp.relationships").PersonList = Y.Base.create('personList', Y.ModelList,
			[], {
				// This tells the list that it will hold instances of the
				// PersonModel class.
				model : Y.usp.relationships.PersonModel
			});

	// Create a new Y.usp.relationships.OrganisationList class that extends Y.ModelList.
	Y.namespace("usp.relationships").OrganisationList = Y.Base.create('organisationList',
			Y.ModelList, [], {
				// This tells the list that it will hold instances of the
				// OrganisationModel class.
				model : Y.usp.relationships.OrganisationModel
			});

}, '0.0.1', {
	requires : [ 'model', 'model-list', 'model-sync-rest', 'yui-base','json-parse' ]
});