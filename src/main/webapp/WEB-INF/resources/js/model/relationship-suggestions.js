

'use-strict';

YUI.add('app-relationship-suggestions-list', function(Y) {
	
	var MIME_TYPE = '';
	
	/**
	 * @namespace Y.app.RelationshipSuggestionsList
	 * @description Temporary fix to ensure suggestions are working. See sync method.
	 */
	Y.namespace('app').RelationshipSuggestionsList = Y.Base.create('relationshipSuggestionsList', 	
		Y.usp.relationship.SuggestedPersonPersonRelationshipsList, [], {
			
		MIME_TYPE: MIME_TYPE,
			
		/**
		 * @method sync
		 * @description overrides auto-generated sync method, to prevent original 'Accept' header.
		 *
		 */
		sync: function (action, options, callback) {
			if (!options) options = {};
			switch (action) {
			case 'read':
				if (!options.headers) options.headers = {};
				Y.mix(options.headers, {"Accept": '' });
				break;
			}
			Y.ModelSync.REST.prototype.sync.apply(this, [action, options, callback]);
		}
	});
	
	
}, '0.0.1', {
	requires : ['usp-relationship-SuggestedPersonPersonRelationships']
});