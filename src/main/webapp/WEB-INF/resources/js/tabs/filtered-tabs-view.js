YUI.add('filtered-tabs-view', function(Y) {
    var L = Y.Lang,
        O = Y.Object,
        A = Y.Array;
   var TAB_LIST_SELECTOR='[role="tablist"]';
   
    Y.namespace('app.tab').FilteredTabsView = Y.Base.create('filteredTabsView', Y.usp.app.AppTabbedPage, [], {
        initializer:function(config){
          this._toolbar=new Y.usp.app.AppToolbar(config);
          
          this._toolbar.addTarget(this);
          
          //add an event handler to the filter node
            this._filteredTabsViewEvtHandlers = [
                                 this.before('activeViewChange', this._handleBeforeNavigate, this),
                                 this.after('activeViewChange', this.enableButtons, this)
                                 ];
          },
          destructor: function() {
              //unbind event handles
              if (this._filteredTabsViewEvtHandlers) {
                  A.each(this._filteredTabsViewEvtHandlers, function(item) {
                      item.detach();
                  });
                  //null out
                  this._filteredTabsViewEvtHandlers = null;
              }
              
              this._toolbar.removeTarget(this);
              this._toolbar.destroy();
              delete this._toolbar;
          },
          
          /**
           * Call back handler for navigation
           */
          _handleBeforeNavigate: function() {
              //get the active view
              var activeView = this.get('activeView');
              if (activeView) {
                  //hide the filter
                  activeView.set('showFilter', false);
                  
                  this.disableButtons();
              }
          },
        /**
         * Disables buttons while navigation is in progress
         *
         * @method disableButtons
         * @protected
         */
        disableButtons: function() {
          this._toolbar.disableButtons();
        },
        /**
         * Enables buttons when navigation is complete
         *
         * @method enableButtons
         * @protected
         */
        enableButtons: function() {
            var toolbarNode = this.get('toolbarNode');
            this._toolbar._toggleDisabledClass(toolbarNode.one('[data-button-action="filter"], .filter'), false);
        },
        /**
         * Enables the button specified by the action string
         *
         * @method enableButton
         * @param string action
         * @protected
         */        
        enableButton: function(action){
          var toolbarNode = this.get('toolbarNode');
          this._toolbar._toggleDisabledClass(toolbarNode.one('[data-button-action="'+action+'"]'), false);
        },
        /**
         * Disable the button specified by the action string
         *
         * @method disableButton
         * @param string action
         * @protected
         */        
        disableButton: function(action){
          this._toolbar.disableButton(action);
        },        
        /**
         * Hide the tabs
         *
         * @method hideTabs
         * @public
         */        
        hide:function(){
          this.tabsNode.one(TAB_LIST_SELECTOR).hide('fadeOut');
        },
        /**
         * Show the tabs
         *
         * @method showTabs
         * @public
         */               
        show:function(){
          this.tabsNode.one(TAB_LIST_SELECTOR).show('fadeIn');
        }
    }, {
        ATTRS: {
            /**
             * @attribute toolbarNode
             * @description The node that hosts the buttons for driving the filter
             * @default null
             * @type HTMLElement|Node|String
             */
            toolbarNode: {
                getter: Y.one,
                writeOnce: true
            },
            /**
             * @attribute toolbar
             * @description A getter for the internal toolbar node
             * @default internal reference
             * @type Y.usp.app.AppToolbar
             */            
            toolbar:{
              getter:function(){
                return this._toolbar;
              },
              readOnly:true
            },
            /**
             * @attribute tabsNode
             * @description The node that hosts the tabs
             * @default null
             * @type HTMLElement|Node|String
             */            
            tabsNode: {
              getter: Y.one,
              writeOnce: true
            }
        }
    });
}, '0.0.1', {
    requires: [
               'yui-base', 
               'app-tabbed-page',
               'app-toolbar'
               ]
});