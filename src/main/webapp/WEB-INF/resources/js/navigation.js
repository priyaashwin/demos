Y.use('usp-navigation', 'usp-current-subject', 'usp-recent-subject', 'querystring', function(Y) {
  var currSubject,
    CurrentSubject = Y.currentSubject,
    path = window.location.pathname,
    queryParams = Y.QueryString.parse(window.location.search.replace('?', '')),
    subjectId = queryParams['id'],
    subjectType;

  var personMatch = path.match(/\/[\w\/]+person(diagram)?(\/[\w\/]*)?$/);
  var groupMatch = path.match(/\/[\w\/]+group(\/[\w\/]*)?$/);
  var organisationMatch = path.match(/\/[\w\/]+organisation(\/[\w\/]*)?$/);

  if (personMatch) {
    subjectType = 'person';
  } else if (groupMatch) {
    subjectType = 'group';
  } else if (organisationMatch) {
    subjectType = 'organisation';
  }

  // Handle person ID specified in the path, rather than as a query parameter e.g. /approvals/person/123
  if (!subjectId) {
    var personIdPathMatch = path.match(/\/person\/([-]?[\d]+)/);
    if (personIdPathMatch) {
      subjectId = personIdPathMatch[1];
    } else {
      var groupIdPathMatch = path.match(/\/group\/([-]?[\d]+)/);
      if (groupIdPathMatch) {
        subjectId = groupIdPathMatch[1];
      }
    }
  }

  Y.on('currentSubject:changed', Y.bind(function(e) {
    Y.app.navigationReplace.setUpNavigation(e.after);
  }));


  // graph URL is atypical. Ideally our routing would be consistent.
  // Checking with an indexOf might cause unintended consequences, so..    
  if (subjectType && subjectId !== undefined) {

    Y.log('current subject is ' + subjectType);

    currSubject = CurrentSubject.getCurrentSubject();

    if (subjectType !== currSubject.type || subjectId != currSubject.id) {
      CurrentSubject.setCurrentSubject(subjectId, subjectType);
    }
  }
});