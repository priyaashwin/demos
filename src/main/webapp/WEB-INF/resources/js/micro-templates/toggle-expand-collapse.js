

YUI.add('template-toggle-expand-collapse', function(Y) {
	
	Y.namespace('app.templates').TOGGLE_EXPAND_COLLAPSE = Y.Template.Micro.compile(
		'<a class="<%= this.cssButton %> <%= this.cssActive %>"\
			data-action="<%= this.action %>"\
			data-event="<%= this.event %>"\
			data-trigger\
			href="<%= this.url %>"\
			title="<%= this.title %>"\
			aria-label="<%= this.title %>">\
				<i class="<%= this.cssIconClosed %>"></i>\
				<i class="<%= this.cssIconOpen %>"></i>\
		</a>'
	);

}, '0.0.1', {
    requires: ['template-micro']
});