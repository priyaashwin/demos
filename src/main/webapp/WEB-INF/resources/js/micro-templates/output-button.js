

YUI.add('template-output-button', function(Y) {
	
	Y.namespace('app.templates').OUTPUT_BUTTON = Y.Template.Micro.compile(
		'<a href="<%=this.href%>" target="_blank" class="pure-button usp-fx-all" title="<%=this.buttonTitle%>" aria-label="<%=this.buttonTitle%>"><i class="<%=this.className%>"></i><span><%=this.buttonText%></span></a>'
	);


}, '0.0.1', {
    requires: ['template-micro']
});