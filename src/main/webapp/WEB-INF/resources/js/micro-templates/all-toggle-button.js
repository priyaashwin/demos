YUI.add('template-all-toggle-button',function(Y) {
  var html = '<a class="pure-button all-toggle-button expand-collapse-all <%= this.collapsedClass %>"'
    + 'title="<%= this.title %>"'
    + 'aria-label="<%= this.title %>">'
    + '<i class="fa fa-chevron-circle-down"></i>'
    + '<i class="fa fa-chevron-circle-up"></i>' + '</a>';

  Y.namespace('app.templates').ALL_TOGGLE_BUTTON = Y.Template.Micro.compile(html);
}, '0.0.1', {
  requires : [ 'template-micro' ]
});