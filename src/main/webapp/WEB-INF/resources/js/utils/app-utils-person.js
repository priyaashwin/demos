YUI.add('app-utils-person', function (Y) {
	
	
	var O = Y.Object,
	    A = Y.Array,
	    E = Y.Escape;
	//To resolve existing person records.
	var getImpliedLifeState = function(lifeState){
		
		if(!lifeState){
			lifeState = 'ALIVE';
		}
		return lifeState;
	},
	getCodedEntryDetails = function( personWarningsAssigned) {
		var activeWarningName = null;
		A.each(O.values(Y.secured.uspCategory.person.warnings.category.getActiveCodedEntries(null, 'read')), function(activeWarning) {	
           if (activeWarning.code === personWarningsAssigned.codedEntryVO.code) {
        	   activeWarningName = activeWarning.name;
           }
        });
		return activeWarningName;
	},
	isFirstGTSecondDate=function(first, second){
		return setToMidnight(first) > setToMidnight(second);
	},
	setToMidnight=function(value) {
		var dateTime = new Date(value);
	  dateTime.setHours(0, 0, 0, 0);
	  return dateTime.getTime();
	};
	
	
	var extractWarningsArray = function(modelData){
        var personWarningsDisplay = [],
        personWarningsAssigned = null,
        todaysDate = new Date();
        
        if (modelData.personWarnings && modelData.personWarnings !== null && modelData.personWarnings.length > 0) {
            personWarningsAssigned = modelData.personWarnings[0].personWarningAssignmentVOs;
            
            personWarningsDisplay = personWarningsAssigned.filter(function (warning) {
            	return (!warning.authorisation || (warning.authorisation.authorisationState!=='PENDING' && warning.authorisation.authorisationState!=='REJECTED'))
                    && (warning.endDate===undefined || warning.endDate===null || isFirstGTSecondDate(warning.endDate, todaysDate.getTime()))
            }).map(function (warning) {
            	return (getCodedEntryDetails(warning));
            });
        }
        
        return personWarningsDisplay;
    };
	Y.namespace('app.utils.person').GetImpliedLifeState = getImpliedLifeState;
	Y.namespace('app.utils.person').GetWarningsArray = extractWarningsArray;
	Y.namespace('app.utils.person').IsFirstGTSecondDate = isFirstGTSecondDate;
	Y.namespace('app.utils.person').SetToMidnight = setToMidnight;
}, '0.0.1', {
    requires: ['yui-base', 'secured-categories-person-component-Warnings','escape']
});