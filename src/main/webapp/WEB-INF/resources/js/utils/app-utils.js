YUI.add('app-utils', function (Y) {
	
	"use strict";
	//To only accept numeric values in a field.
	var acceptNumeric = function(inputValue, maxLength){
		
		// Regex returns true if number between 0-9 or an empty string
        var pattern = new RegExp(eval('/^[0-9]{1,'+maxLength+'}$|^$/'));
        
        // Keep trimming the inputValue until a valid digits are entered or there is nothing left to slice
        if(!pattern.test(inputValue)){
        	inputValue = inputValue.slice(0,-1);
        }
        return inputValue;
	};
	
	var resolveFileContentType = function(contentType) {
		var fileTypeData = {
				contentType : 'NONE',
				view: false
		},
		
		PATTERN_VIDEO_TYPE = new RegExp("^video\\/(.*)$","i"),
		PATTERN_IMAGE_TYPE = new RegExp("^image\\/(.*)$","i"),
		PATTERN_AUDIO_TYPE = new RegExp("^audio\\/(.*)$","i");
		if(PATTERN_VIDEO_TYPE.test(contentType)) {
			fileTypeData.contentType = 'VIDEO';
			fileTypeData.view = true;
		} else if(PATTERN_IMAGE_TYPE.test(contentType)) {
			fileTypeData.contentType = 'IMAGE';
			fileTypeData.view = true;
		} else if(PATTERN_AUDIO_TYPE.test(contentType)) {
			fileTypeData.contentType = 'AUDIO';
			fileTypeData.view = true;
		} 
		
		return fileTypeData;
		
	};
	
	Y.namespace('app.utils').AcceptNumeric = acceptNumeric;
	Y.namespace('app.utils').ResolveFileContentType = resolveFileContentType;
}, '0.0.1', {
    requires: ['yui-base','escape']
});