YUI.add('usp-relationship-helper', function(Y) {
	var A = Y.Array, O = Y.Object;
	var uspRelationshipHelper = {
		logMyMessage : function(message) {
			console.log('---------'+message+'-----------');
		},
		
		getGenderedRelationshipRole : function(firstPerson, secondPerson, suggestion, firstPersonRole){
			var role;
			
			if(suggestion.personPersonRelationshipType.description === 'UNION' && 
					( (firstPerson.gender === secondPerson.gender) || (firstPerson.gender === 'UNKNOWN')
							|| (firstPerson.gender === 'INDETERMINATE') || (secondPerson.gender === 'UNKNOWN') || (secondPerson.gender === 'INDETERMINATE') )){
				
				var relClass=suggestion.personPersonRelationshipType.relationshipClassName.toLowerCase();
        		var displayClass=relClass.charAt(0).toUpperCase()+ relClass.slice(1);
        		if(displayClass.indexOf('relationship') > -1){
        			displayClass =  displayClass.substring(0,displayClass.indexOf('relationship'));
        			displayClass = displayClass.trim();
        		}
        		if(displayClass === 'Engaged'){
        			displayClass = displayClass+' partner';
        		}else if(displayClass.indexOf('partner') == -1){
        			displayClass = displayClass+' partner';
        		}
        		role = displayClass;
				
			}else if(suggestion.personPersonRelationshipType.relationshipRoleGenderTypeVOs != null
					&& suggestion.personPersonRelationshipType.relationshipRoleGenderTypeVOs.MALE != null
					&& suggestion.personPersonRelationshipType.relationshipRoleGenderTypeVOs.FEMALE != null){
					
				if(firstPerson.gender === 'MALE'){
					if(firstPersonRole === 'ROLE_A'){
						role = suggestion.personPersonRelationshipType.relationshipRoleGenderTypeVOs.MALE.roleAName;
					}else{
						role = suggestion.personPersonRelationshipType.relationshipRoleGenderTypeVOs.MALE.roleBName;
					}
				}else if(firstPerson.gender === 'FEMALE') {
					if(firstPersonRole === 'ROLE_A'){
						role = suggestion.personPersonRelationshipType.relationshipRoleGenderTypeVOs.FEMALE.roleAName;
					}else{
						role = suggestion.personPersonRelationshipType.relationshipRoleGenderTypeVOs.FEMALE.roleBName;
					}
				}else{
					// UNKNOWN or INDETERMINATE
					if(firstPersonRole === 'ROLE_A'){
						role = suggestion.personPersonRelationshipType.roleAName;
					}else{
						role = suggestion.personPersonRelationshipType.roleBName;
					}
				}
				
			}else{
				if(firstPersonRole === 'ROLE_A'){
					role = suggestion.personPersonRelationshipType.roleAName;
				}else{
					role = suggestion.personPersonRelationshipType.roleBName;
				}
			}
			
			return role;
		}, 
		 getFilterMessage: function(nominatedPersonId,roleAPersonId,roleBPersonId,relationshipStartDate,relationshipType){
		   //load the relationship filter and re-hydrate - this will restore the saved search state
		   var filterModel=new Y.app.relationship.RelationshipFilterModel();
       var searchState = sessionStorage.getItem('paginatedFilteredAsymmetricPersonPersonRelationshipResultList');
       try{
         filterModel.initFromSearchState(searchState?JSON.parse(searchState):{});  
       }catch(e){
         //ignored
       }
       
       var relationshipTypes=filterModel.get('relationshipTypes')||[];
       var familialChecked=relationshipTypes.indexOf('FamilialRelationshipType')!==-1;
       var socialChecked=relationshipTypes.indexOf('SocialRelationshipType')!==-1;
       var professionalChecked=relationshipTypes.indexOf('ProfessionalRelationshipType')!==-1;
       var anyRelationshipTypeFilterNotSelected = (!familialChecked && !socialChecked && !professionalChecked);

       var status=filterModel.get('status')||[];
       var pendingChecked = status.indexOf('INACTIVE_PENDING')!==-1;
       var currentChecked = status.indexOf('ACTIVE')!==-1;
       var endedChecked = status.indexOf('INACTIVE_HISTORIC')!==-1;
       var anyStateFilterNotSelected = (!pendingChecked && !currentChecked && !endedChecked);
       
       var today = new Date((new Date()).setHours(0, 0, 0, 0));
       var relationshipStartDateinFuture = relationshipStartDate > today;
       
       var dateFromSelected=filterModel.get('dateForm');
       var dateToSelected=filterModel.get('dateTo');
       var anyDateFilterNotSelected=(!dateFromSelected && !dateToSelected);
       
       var dateToBeforeStartDate= new Date(relationshipStartDate).getTime() > new Date(dateToSelected).getTime();

       var filterMessage;

		 	if(roleAPersonId == nominatedPersonId || roleBPersonId == nominatedPersonId){
		 		// new relationship is added to nominated person
   		 		// if all check boxes are disabled then all relationship types are visible by default
   		 		if(!anyRelationshipTypeFilterNotSelected || !anyStateFilterNotSelected || !anyDateFilterNotSelected){
   		 			// type filter not selected and state filter selected
   		 			if(anyRelationshipTypeFilterNotSelected && !anyStateFilterNotSelected && anyDateFilterNotSelected){	                       		 				
   		 				if(relationshipStartDateinFuture && !pendingChecked){
   		 					filterMessage = 'Please enable the Pending filter';
   		 				}else if(!relationshipStartDateinFuture && !currentChecked){
   		 					filterMessage = 'Please enable the Current filter';
   		 				}
   		 			}else if(!anyRelationshipTypeFilterNotSelected && anyStateFilterNotSelected && anyDateFilterNotSelected){	                       		 				
       		 			if(relationshipType == 'FamilialRelationshipType' && !familialChecked){
       		 				filterMessage = 'Please enable the Personal-familial filter';
       		 			}else if(relationshipType == 'SocialRelationshipType'  && !socialChecked){
       		 				filterMessage = 'Please enable the Personal-social filter';
       		 			}else if(relationshipType == 'ProfessionalRelationshipType' && !professionalChecked){
       		 				filterMessage = 'Please enable the Professional filter';
       		 			}
   		 			}else if(!anyRelationshipTypeFilterNotSelected && !anyStateFilterNotSelected && anyDateFilterNotSelected){
       		 			if(relationshipType == 'FamilialRelationshipType'){
       		 				if(!familialChecked && relationshipStartDateinFuture && !pendingChecked){
       		 					filterMessage = 'Please enable the Personal-familial and Pending filters';
       		 				}else if(!familialChecked && !relationshipStartDateinFuture && !currentChecked){
       		 					filterMessage = 'Please enable the Personal-familial and Current filters';
       		 				}else if(familialChecked && relationshipStartDateinFuture && !pendingChecked){
       		 					filterMessage = 'Please enable the Pending filter';
       		 				}else if(familialChecked && !relationshipStartDateinFuture && !currentChecked){
       		 					filterMessage = 'Please enable the Current filter';
       		 				}else if(!familialChecked && relationshipStartDateinFuture && pendingChecked){
       		 					filterMessage = 'Please enable the Personal-familial filter';
       		 				}if(!familialChecked && !relationshipStartDateinFuture && currentChecked){
       		 					filterMessage = 'Please enable the Personal-familial filter';
       		 				}
       		 			}else if(relationshipType == 'SocialRelationshipType'){
           		 			if(!socialChecked && relationshipStartDateinFuture && !pendingChecked){
       		 					filterMessage = 'Please enable the Personal-Social and Pending filters';
       		 				}else if(!socialChecked && !relationshipStartDateinFuture && !currentChecked){
       		 					filterMessage = 'Please enable the Personal-Social and Current filters';
       		 				}else if(socialChecked && relationshipStartDateinFuture && !pendingChecked){
       		 					filterMessage = 'Please enable the Pending filter';
       		 				}else if(socialChecked && !relationshipStartDateinFuture && !currentChecked){
       		 					filterMessage = 'Please enable the Current filter';
       		 				}else if(!socialChecked && relationshipStartDateinFuture && pendingChecked){
       		 					filterMessage = 'Please enable the Personal-Social filter';
       		 				}if(!socialChecked && !relationshipStartDateinFuture && currentChecked){
       		 					filterMessage = 'Please enable the Personal-Social filter';
       		 				}
       		 			}else if(relationshipType == 'ProfessionalRelationshipType'){
           		 			if(!professionalChecked && relationshipStartDateinFuture && !pendingChecked){
       		 					filterMessage = 'Please enable the Professional and Pending filters';
       		 				}else if(!professionalChecked && !relationshipStartDateinFuture && !currentChecked){
       		 					filterMessage = 'Please enable the Professional and Current filters';
       		 				}else if(professionalChecked && relationshipStartDateinFuture && !pendingChecked){
       		 					filterMessage = 'Please enable the Pending filter';
       		 				}else if(professionalChecked && !relationshipStartDateinFuture && !currentChecked){
       		 					filterMessage = 'Please enable the Current filter';
       		 				}else if(!professionalChecked && relationshipStartDateinFuture && pendingChecked){
       		 					filterMessage = 'Please enable the Professional filter';
       		 				}if(!professionalChecked && !relationshipStartDateinFuture && currentChecked){
       		 					filterMessage = 'Please enable the Professional filter';
       		 				}		                       		 				
       		 			}
   		 			   }else if(anyRelationshipTypeFilterNotSelected && anyStateFilterNotSelected &&!anyDateFilterNotSelected){
				  		 		 if(dateToBeforeStartDate && dateToSelected){
						 		   filterMessage='Please reset the Date filter';
						 		  }
		 	           }else if(!anyRelationshipTypeFilterNotSelected && anyStateFilterNotSelected &&!anyDateFilterNotSelected){
    		 	            if(relationshipType == 'FamilialRelationshipType'){
	           		 			if(!familialChecked && dateToBeforeStartDate && dateToSelected){
	             		 			filterMessage = 'Please enable the Personal-familial and reset the Date filters';
	                              }else if(!familialChecked && !dateToBeforeStartDate){
	                              	filterMessage = 'Please enable the Personal-familial';	
	                              }else if(familialChecked && dateToBeforeStartDate && dateToSelected){
	                              	filterMessage = 'Please reset the Date filter';	
	                              }
       		 			}else if(relationshipType == 'SocialRelationshipType'){
	           		 			if(!socialChecked && dateToBeforeStartDate && dateToSelected){
	            		 		   filterMessage = 'Please enable the Personal-Social and reset the Date filters';
	                             }else if(!socialChecked && !dateToBeforeStartDate){
	                          	   filterMessage = 'Please enable the Personal-Social filter';	
	                             }else if(socialChecked && dateToBeforeStartDate &&dateToSelected){
	                          	   filterMessage = 'Please reset the Date filter';	
	                             }   
       		 			}else if(relationshipType == 'ProfessionalRelationshipType'){
	       		 				if(!professionalChecked && dateToBeforeStartDate &&dateToSelected){
	                               filterMessage = 'Please enable the Professional and reset the Date filters';
	                            }else if(!professionalChecked && !dateToBeforeStartDate){
	                          	   filterMessage = 'Please enable the Professional filter';	
	                            }else if(professionalChecked && dateToBeforeStartDate &&dateToSelected){
	                          	   filterMessage = 'Please reset the Date filter';	
	                            }
   		 			     }
		 	           }
   		 		}
   		 		
		 	}else {	//secondary relationship
       		 	if(Y.one('.usp-graph-content')){
           		 	if(Y.one('#secondaryToggle').hasClass('inactive') && relationshipType != 'ProfessionalRelationshipType'){
       		 			// alert user about the secondary button
       		 			filterMessage = 'Please enable the secondary graph tool';
       		 		}
       		 	}
       		}
		 	
		 	return filterMessage;
		},
		filterRelatiosnhipAttributes : function(attributeInputNodes){
			var checkedAttributeNodes = [];
			
			 attributeInputNodes.each(function(attributeInputNode){
				if(attributeInputNode.get('checked')){
					checkedAttributeNodes.push(attributeInputNode);
				}
			});
			
			//return all check boxes which are checked. 
			return checkedAttributeNodes;
		},
		getRelationshipAttributes : function(attributeInputNodes, roleAId, roleBId){
			var checkedAttributeNodes = this.filterRelatiosnhipAttributes(attributeInputNodes), 
			attrs = {};
			A.each(checkedAttributeNodes, function(checkedAttribute){
				attrs[checkedAttribute.getData('attribute')]=attrs[checkedAttribute.getData('attribute')]||[];
				attrs[checkedAttribute.getData('attribute')].push(checkedAttribute.getData('entity-id'));
			});
			
			O.each(attrs, function(value, key, attrs){
				if(value.length==1){
					if(Number(value[0]) == roleAId){
						attrs[key] = 'ROLE_A';
					}
					if(Number(value[0]) == roleBId){
						attrs[key] = 'ROLE_B';
					}
				}
				if(value.length == 2){
					attrs[key] = 'BOTH';
				}
			});
					
			return attrs;
		}
	};
	Y.uspRelationshipHelper = uspRelationshipHelper;
	
},'1.0.0', {
    requires: [
               'yui-base',
               'relationship-filter'
               ]
});