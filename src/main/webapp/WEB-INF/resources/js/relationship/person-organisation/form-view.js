YUI.add('app-person-organisation-relationship-form-view', function (Y) {
	
	var E = Y.Escape,
    L = Y.Lang,
    A = Y.Array,
    O = Y.Object;
	
	Y.namespace('app.relationship').ViewPersonOrganisationRelationshipView =Y.Base.create('ViewPersonOrganisationRelationshipView', Y.usp.relationship.PersonOrganisationRelationshipView,[],{
		template:Y.Handlebars.templates["relationshipOrganisationDialog"],
		render: function(){
			  // superclass call
			var container = this.get('container');
			html = this.template({
				modelData: this.get('model').toJSON(),
				codedEntries: this.get('codedEntries'),
				labels: this.get('labels'),
				deleteRelationship: this.get('deleteRelationship'),
				teamView:  this.get('model').toJSON().organisationVO.organisationType === 'TEAM'
			});
			container.setHTML(html);
		}
	}, {
		ATTRS: {
			codedEntries: {
				value: {
					organisationType: Y.uspCategory.organisation.organisationType.category.codedEntries,
					organisationSubType: Y.uspCategory.organisation.organisationSubType.category.codedEntries,
					closeReason: Y.uspCategory.relationship.personOrganisationRelationshipClosureReason.category.codedEntries
				}				
			}
		}
	});
	
}, '0.0.1', {
	  requires: ['yui-base',
	             'form-util',
	             'handlebars',
	             'handlebars-helpers',
	             'handlebars-relationship-templates',
	             'usp-relationship-PersonOrganisationRelationship',
	     		 'categories-organisation-component-OrganisationType',	              
	     		 'categories-organisation-component-OrganisationSubType']
});