Y.use('usp-notifications', function() {
  var notificationSubscription = new Y.usp.notification.UserNotificationSubscription({
    url: window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '') + '/eclipse/rest/userNotificationSubscription?selfService'
  });

  notificationSubscription.load(Y.bind(function(err, r) {
    if (err === null) {
      var subscription = JSON.parse(r.responseText);
      if (subscription.activeChannels.indexOf('websocket') >= 0) {
        this.uspNotifications = new Y.usp.USPNotifications({
          urlPrefix: 'eclipse'
        });
        uspNotifications.connectAndSubscribe();
      }
    }
  }, this));

  Y.on('updatedNotificationSettings', Y.bind(function(e) {
    Y.fire('infoMessage:message', {
      message: e.successMessage,
      type: 'success'
    });

    if (e.subscription.activeChannels.indexOf('websocket') >= 0) {
      this.uspNotifications = new Y.usp.USPNotifications({
        urlPrefix: 'eclipse'
      });
      this.uspNotifications.connectAndSubscribe();
    } else {
      if (this.uspNotifications) {
        this.uspNotifications.disconnectAndUnsubscribe();
        this.uspNotifications.destroy();
        delete this.uspNotifications;
      }
    }
  }, this));
});
