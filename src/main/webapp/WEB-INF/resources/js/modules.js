var YUI_Configuration = YUI_Configuration || {};
YUI_Configuration.AppModules = YUI_Configuration.AppModules || {};
YUI_Configuration.AppModules.config = {
    'dialogs': {
        root: '/js/dialogs/',
        modules: {
            'address-dialog-views': {
                path: 'address.js'
            },
            'person-organisation-relationship-dialog-views': {
                path: 'person-organisation-relationship.js'
            },
            'base-addedit-relationship-view': {
                path: 'base-addedit-relationship-view.js'
            },
            'relationship-dialog-views': {
                path: 'relationships.js'
            },
            'reference-number-dialog-views': {
                path: 'reference-numbers.js'
            },
            'group-dialog-views': {
                path: 'group.js'
            },
            'subject-contact-views': {
                path: 'subject-contact.js'
            },
            'organisation-dialog-views': {
                path: 'organisation.js'
            },
            'person-dialog-views': {
                path: 'person.js'
            },
            'form-wizard-dialog-views': {
                path: 'formwizard.js'
            },
            'personwarning-dialog-views': {
                path: 'personwarnings.js'
            },
            'image-dialog-views': {
                path: 'image.js'
            },
            'settings-dialog-views': {
                path: 'settings.js'
            },
            'user-selfCredentialChange-dialog-views': {
                path: 'selfCredentialChange.js'
            },
            'groupofrights-dialog-views': {
                path: 'groupofrights.js'
            },
            'feedback-dialog-views': {
                path: 'feedback.js'
            },
            'event-dialog-views': {
                path: 'event.js'
            },
            'event2-dialog-views': {
                path: 'locations.js'
            },
            'service-dialog-views': {
                path: 'service.js'
            },
            'carerapproval-dialog-views': {
                path: 'carerapproval/carerapproval-dialog.js'
            },
            'childprotection-dialog-views': {
                path: 'childprotection/childprotection-dialog.js'
            },
            'duplicaterecords-dialog-views': {
                path: 'duplicaterecords/duplicaterecords-dialog.js'
            },
            'remove-dialogs': {
                path: 'remove-dialog.js'
            }
        }
    },
    'login-dialog': {
        root: '/js/dialogs/login/',
        modules: {
            'login-dialog-views': {
                path: 'login.js'
            }
        }
    },
    'authentication-views': {
        root: '/js/views/login/',
        modules: {
            'authentication-controller': {
                path: 'authentication-controller.js'
            }
        }
    },
    'person-dialogs': {
        root: '/js/dialogs/person/',
        modules: {
            'person-picture-dialog-views': {
                path: 'person-picture.js'
            },
            'person-alert-dialog': {
                path: 'person-alert-dialog.js',
                requires: ['app-dialog']
            },
            'person-output-document-dialog-views': {
                path: 'output/person-output-document-dialog-views.js'
            },
            'person-lock-dialog': {
                path: 'person-lock-dialog.js'
            }
        }
    },
    'attachment-dialogs': {
        root: '/js/dialogs/attachment/',
        modules: {
            'attachment-dialog': {
                path: 'dialog.js',
                requires: ['app-dialog', 'file-html5', 'event-custom', 'promise']
            }
        }
    },
    'user-dialogs': {
        root: '/js/dialogs/user/',
        modules: {
            'user-dialog-views': {
                path: 'user.js'
            }
        }
    },
    'team-dialogs': {
        root: '/js/dialogs/team/',
        modules: {
            'team-dialog': {
                path: 'dialog.js'
            }
        }
    },
    'classification-dialogs': {
        root: '/js/dialogs/classification/',
        modules: {
            'classification-dialog-views': {
                path: 'classification.js'
            },
            'classification-configuration-dialog-views': {
                path: 'admin/classification-configuration-dialog.js',
            }
        }
    },
    'person-summary-administration-dialogs': {
        root: '/js/dialogs/person-summary/administration/',
        modules: {
            'person-summary-administration-dialog-views': {
                path: 'person-summary-administration-dialog-views.js'
            }
        }
    },
    'autocomplete-view': {
        root: '/js/autocomplete/',
        modules: {
            'base-autocomplete-view': {
                path: 'base-autocomplete.js',
                requires: ['popup-autocomplete', 'view', 'template-micro']
            },
            'person-autocomplete-view': {
                path: 'person-autocomplete.js',
                requires: ['base-autocomplete-view']
            },
            'organisation-autocomplete-view': {
                path: 'organisation-autocomplete.js',
                requires: ['base-autocomplete-view']
            },
            'childlookedafter-autocomplete-view': {
                path: 'childlookedafter-autocomplete.js',
                requires: ['base-autocomplete-view']
            },
            'security-profile-autocomplete-view': {
                path: 'security-profile-autocomplete.js',
                requires: ['base-autocomplete-view']
            },
            'checklist-definition-autocomplete-view': {
                path: 'checklist-definition-autocomplete.js',
                requires: ['base-autocomplete-view']
            },
            'security-user-autocomplete-view': {
                path: 'security-user-autocomplete.js',
                requires: ['base-autocomplete-view']
            }
        }
    },
    'system-configuration': {
        root: '/js/dialogs/system-configuration/',
        modules: {
            'custommenu-dialog-views': {
                path: 'custommenu-dialog.js',
            }
        }
    },
    'recent-history': {
        root: '/js/views/recent-history/',
        modules: {
            'app-recent-records-dialog': {
                path: 'recent-records-dialog.js',
                requires: ['app-recent-people-view']
            },
            'app-recent-people-view': {
                path: 'recent-people-view.js',
                requires: ['app-recent-people-table-panel', 'person-search-navigation']
            },
            'app-recent-people-table-panel': {
                path: 'recent-people-table-panel.js'
            }
        }
    },
    'recent-subject': {
        root: '/js/subject/',
        modules: {
            'usp-recent-subject': {
                path: 'recentSubject.js'
            }
        }
    },
    'calendar-dialogs': {
        root: '/js/dialogs/calendar/',
        modules: {
            'calendar-dialog-views': {
                path: 'calendar-dialog-views.js'
            },
            'calendar-days-dialog-views': {
                path: 'calendar-days-dialog-views.js'
            },
            'calendar-nw-days-dialog-views': {
                path: 'calendar-nw-days-dialog-views.js'
            },
            'holiday-dialog-views': {
                path: 'holiday-dialog-views.js'
            },
            'holiday-event-dialog-views': {
                path: 'holiday-event-dialog-views.js'
            }
        }
    },
    'form-dialogs': {
        root: '/js/dialogs/form/',
        modules: {
            'form-definition-dialog-views': {
                path: 'form-definition-dialog-views.js'
            },
            'form-mapping-dialog-views': {
                path: 'form-mapping-dialog-views.js'
            }
        }
    },
    'checklist-dialogs': {
        root: '/js/dialogs/checklist/',
        modules: {
            'checklist-definition-dialog-views': {
                path: 'checklist-definition-dialog-views.js'
            },
            'checklist-instance-dialog-views': {
                path: 'checklist-instance-dialog-views.js',
                requires: ['checklist-ownership-util']
            },
            'checklist-instance-status-dialog-views': {
                path: 'checklist-instance-status-dialog-views.js',
                requires: ['checklist-instance-ownership-views']
            },
            'checklist-instance-priority-dialog-views': {
                path: 'checklist-instance-priority-dialog-views.js'
            },
            'checklist-instance-task-reassign-dialog-views': {
                path: 'checklist-instance-task-reassign-dialog-views.js'
            },
            'checklist-instance-ownership-views': {
                path: 'checklist-instance-ownership-views.js'
            },
            'checklist-ownership-util': {
                path: 'checklist-ownership-util.js'
            }
        }
    },
    'rule-dialogs': {
        root: '/js/dialogs/rule/',
        modules: {
            'rule-definition-dialog-views': {
                path: 'rule-definition-dialog-views.js'
            }
        }
    },
    'resolve-dialogs': {
        root: '/js/dialogs/resolve/',
        modules: {
            'cpraform-dialog-views': {
                path: 'cpraForm/cpraform-dialog-views.js'
            },
            'prform-dialog-views': {
                path: 'prForm/prform-dialog-views.js'
            },
            'intakeform-dialog-views': {
                path: 'intakeForm/intakeform-dialog-views.js'
            },
            'formstatus-dialog-views': {
                path: 'formstatus-dialog-views.js'
            }
        }
    },
    'models': {
        root: '/js/model/',
        modules: {
            'app-button-model': {
                path: 'button-model.js'
            },
            'person-relationship-model': {
                path: 'relationshipModel.js'
            },
            'app-relationship-suggestions-list': {
                path: 'relationship-suggestions.js'
            }
        }
    },
    'search': {
        root: '/js/search/navigation/',
        modules: {
            'person-search-navigation': {
                path: 'person-search-navigation.js'
            },
            'group-search-navigation': {
                path: 'group-search-navigation.js'
            },
            'organisation-search-navigation': {
                path: 'organisation-search-navigation.js'
            }
        }
    },
    'quick-search': {
        root: '/js/search/quick/',
        modules: {
            'quick-search': {
                path: 'search.js'
            },
            'person-quick-search': {
                path: 'person.js',
                requires: ['quick-search', 'person-search-navigation']
            },
            'group-quick-search': {
                path: 'group.js',
                requires: ['quick-search', 'group-search-navigation']
            },
            'organisation-quick-search': {
                path: 'organisation.js',
                requires: ['quick-search', 'organisation-search-navigation']
            }
        }
    },
    'app-search': {
        root: '/js/search/app/',
        modules: {
            'app-search': {
                path: 'search.js'
            },
            'app-advanced-search-base': {
                path: 'advancedSearchBase.js',
                requires: ['app-search', 'person-search-navigation']
            },
            'person-app-search': {
                path: 'person.js',
                requires: ['app-search', 'person-search-navigation', 'app-advanced-search-base']
            },
            'group-app-search': {
                path: 'group.js',
                requires: ['app-search', 'group-search-navigation']
            },
            'organisation-app-search': {
                path: 'organisation.js',
                requires: ['app-search', 'organisation-search-navigation', 'app-advanced-search-base']
            }
        }
    },
    'views': {
        root: '/js/views/',
        modules: {
            'content-context-button': {
                path: 'contentContextButton.js'
            },
            'app-accordion-toggle-all': {
                path: 'accordion-panel-toggle-all.js'
            }
        }
    },
    'header-views': {
        root: '/js/views/header/',
        modules: {
            'change-team-view': {
                path: 'changeTeam.js'
            }
        }
    },
    'usp-relationship': {
        root: '/js/relationship/',
        modules: {
            'usp-relationship-helper': {
                path: 'relationshipHelper.js'
            }
        }
    },
    'context-views': {
        root: '/js/views/context/',
        modules: {
            'subject-context': {
                path: 'subjectContext.js',
                requires: ['yui-base', 'content-context', 'model', 'event-custom-base']
            },
            'content-context': {
                path: 'context.js',
                requires: ['yui-base', 'view', 'model', 'event-custom-base']
            }
        }
    },
    'tabs': {
        root: '/js/tabs/',
        modules: {
            'filtered-tabs-view': {
                path: 'filtered-tabs-view.js'
            }
        }
    },
    'allocation-views' : {
    	root : '/js/views/allocation/',
    	modules: {
    		'allocation-controller-view' : {
    			path : 'controller.js'
    		},
    		'allocation-view' : {
    			path : 'view.js'
    		}
    	}
    },
    'home-views': {
        root: '/js/views/home/',
        modules: {
            'home-allocation': {
                path: 'viewAllocation.js'
            },
            'checklist-home-tabs': {
                path: 'checklistHomeTabs.js'
            },
            'checklist-home-tab': {
                path: 'checklistHomeTab.js'
            },
            'submitted-forms-controller': {
                path: 'authorisation/forms.js'
            },
            'authorisation-warnings-view': {
                path: 'authorisation/warnings.js'
            },
            'authorisation-controller-view': {
                path: 'authorisation/controller.js'
            },
            'dashboard-controller-view': {
                path: 'dashboard/controller.js'
            },
            'dashboard-view': {
                path: 'dashboard/view.js'
            }
        }
    },
    'app-base-views': {
        root: '/js/views/app-base/',
        modules: {
            'app-views-narrative': {
                path: 'narrative.js'
            },
            'app-views-autocomplete': {
                path: 'autocomplete.js'
            },
            'app-mixin-multi-view': {
                path: 'mixin-multi-view.js'
            },
            'app-multi-panel': {
                path: 'multi-panel.js'
            },
            'app-helpers': {
                path: 'helpers.js'
            },
            'app-calendar-popup': {
                path: 'calendar-popup.js'
            }
        }
    },
    'button-views': {
        root: '/js/views/buttons/',
        modules: {
            'app-button-trigger': {
                path: 'button-trigger.js'
            },
            'app-dialog-trigger': {
                path: 'dialog-trigger.js'
            }
        }
    },
    'person-views': {
        root: '/js/views/person/',
        modules: {
            'person-details-screen': {
                path: 'viewGeneral.js'
            },
            'person-lock-handler': {
                path: 'personLock.js'
            }
        }
    },
    'person-address-views': {
        root: '/js/views/person/address/',
        modules: {
            'person-address-controller-accordion': {
                path: 'controller.js'
            },
            'person-address-results': {
                path: 'results.js'
            }
        }
    },
    'person-belongings-views': {
        root: '/js/views/person/belongings/',
        modules: {
            'belongings-controller-accordion': {
                path: 'controller.js'
            },
            'belongings-results': {
                path: 'results.js'
            },
            'belongings-history-results': {
                path: 'history.js'
            }
        }
    },
    'person-belongings-dialogs': {
        root: '/js/dialogs/person/belongings/',
        modules: {
            'belongings-dialog': {
                path: 'dialog.js'
            }
        }
    },
    'person-education-views': {
        root: '/js/views/person/education/',
        modules: {
            'education-controller-accordion': {
                path: 'controller.js'
            },
        }
    },
    'person-education-dialogs': {
        root: '/js/dialogs/person/education/',
        modules: {
            'education-dialog': {
                path: 'dialog.js'
            }
        }
    },
    'person-subjectaccessrequest-views': {
        root: '/js/views/person/subjectaccessrequest/',
        modules: {
            'subjectaccessrequest-controller-accordion': {
                path: 'controller.js'
            },
            'subjectaccessrequest-results': {
                path: 'results.js'
            },
        }
    },
    'person-subjectaccessrequest-dialogs': {
        root: '/js/dialogs/person/subjectaccessrequest/',
        modules: {
            'subjectaccessrequest-dialog': {
                path: 'dialog.js'
            }
        }
    },
    'person-alert-views': {
        root: '/js/views/person/alert/',
        modules: {
            'person-alert-controller-view': {
                path: 'controller.js',
                requires: ['person-alert-dialog']
            }
        }
    },
    'delete-views': {
        root: '/js/views/deletion/',
        modules: {
            'remove-controller-view': {
                path: 'controller.js',
                requires: ['remove-dialogs']
            }
        }
    },
    'entry-dialogs': {
        root: '/js/dialogs/entry/',
        modules: {
            'entry-dialog': {
                path: 'dialog.js',
                requires: ['app-dialog', 'entry-dialog-views']
            },
            'entry-dialog-views': {
                path: 'entry-dialog-views.js'
            }
        }
    },
    'systemconfig-views': {
        root: '/js/views/systemConfig/',
        modules: {
            'admin-system-config-screen': {
                path: 'viewSystemConfigGeneral.js'
            },
            'app-custom-menu-form-edit': {
                path: 'custom-menu-edit-view.js'
            }
        }
    },
    'organisation-views': {
        root: '/js/views/organisation/',
        modules: {
            'app-organisation-accordions': {
                path: 'accordions-view.js'
            },
            'app-addresses-accordion': {
                path: 'accordion-addresses.js'
            },
            'app-contact-details-accordion': {
                path: 'accordion-contact-details.js'
            },
            'app-contact-details-view': {
                path: 'contact-details-view.js'
            },
            'app-organisation-identification-details-accordion': {
                path: 'accordion-identification-details.js'
            },
            'app-organisation-identification-details-view': {
                path: 'identification-details-view.js'
            },
            'app-reference-numbers-accordion': {
                path: 'accordion-reference-numbers.js'
            },
            'app-reference-numbers-view': {
                path: 'reference-numbers.js'
            }
        }
    },
    'group-views': {
        root: '/js/views/group/',
        modules: {
            'group-controller-view': {
                path: 'controller.js'
            },
            'group-results': {
                path: 'results.js'
            }
        }
    },
    'entry-views': {
        root: '/js/views/entry/',
        modules: {
            'entry-controller-view': {
                path: 'controller.js',
                requires: ['entry-results', 'entry-filter']
            },
            'entry-results': {
                path: 'results.js'
            },
            'entry-filter': {
                path: 'filter.js'
            }
        }
    },
    'entry-dialog-module': {
        root: '/js/dialogs/entry/',
        modules: {
            'entry-dialog': {
                path: 'dialog.js',
                requires: ['app-dialog', 'entry-dialog-views']
            },
            'entry-dialog-views': {
                path: 'entry-dialog-views.js'
            }
        }
    },
    'document-views': {
        root: '/js/views/document/',
        modules: {
            'document-controller-view': {
                path: 'controller.js'
            },
            'document-view': {
                path: 'view.js'
            },
            'outputs-view': {
                path: 'view.js'
            }
        }
    },
    'casenote-views': {
        root: '/js/views/casenotes/',
        modules: {
            'casenote-controller-view': {
                path: 'controller.js',
                requires: ['entry-controller-view', 'casenote-results', 'casenote-filter', 'casenote-results-print', 'casenote-files-results-view', 'casenote-attachment-view', 'casenote-dialog']
            },
            'casenote-results': {
                path: 'results.js'
            },
            'casenote-filter': {
                path: 'filter.js'
            },
            'casenote-results-print': {
                path: 'print.js'
            },
            'casenote-files-results-view': {
                path: 'attachment/casenote-files-results-view.js'
            },
            'casenote-attachment-view': {
                path: 'attachment/casenote-attachment-view.js'
            }
        }
    },
    'casenote-dialogs': {
        root: '/js/dialogs/casenote/',
        modules: {
            'casenote-dialog': {
                path: 'dialog.js',
                requires: ['entry-dialog', 'casenote-attachment-dialog', 'casenote-dialog-views']
            },
            'casenote-attachment-dialog': {
                path: 'attachment/casenote-attachment-dialog.js'
            },
            'casenote-dialog-views': {
                path: 'casenote-dialog-views.js'
            },
            'casenote-image-dialog': {
                path: 'image-dialog.js'
            },
            'casenote-output-document-dialog-views': {
                path: 'output/casenote-output-document-dialog-views.js'
            }
        }
    },
    'childprotection-views': {
        root: '/js/views/childprotection/',
        modules: {
            'childprotection-controller-view': {
                path: 'controller.js'
            },
            'childprotection-enquiry-accordion-views': {
                path: 'accordions/childprotection-enquiry.js'
            }
        }
    },
    'childlookedafter-views': {
        root: '/js/views/childlookedafter/',
        modules: {
            'childlookedafter-controller-view': {
                path: 'controller.js'
            },
            'childlookedafter-view': {
                path: 'view.js'
            }
        }
    },
    'childlookedafter-dialogs': {
        root: '/js/dialogs/childlookedafter/',
        modules: {
            'childlookedafter-mixin-classifications-optgroup': {
                path: 'mixin-classifications-optgroup.js'
            },
            'periodofcare-dialog': {
                path: 'periodofcare-dialog.js'
            },
            'episodeofcare-dialog': {
                path: 'episodeofcare-dialog.js'
            },
            'absencefromplacement-dialog': {
                path: 'absencefromplacement-dialog.js'
            },
            'temporaryplacement-dialog': {
                path: 'temporaryplacement-dialog.js'
            },
            'childlookedafter-subject-autocompleteview': {
                path: 'base-subject-autocompleteview.js'
            },
            'base-care-view': {
                path: 'base-care-view.js'
            },
            'base-addedit-episodeofcare-view': {
                path: 'base-addedit-episodeofcare-view.js'
            },
            'placement-address-add-view': {
                path: 'placement-address-add-view.js'
            }
        }
    },
    'careleaver-views': {
        root: '/js/views/careleaver/',
        modules: {
            'careleaver-controller-view': {
                path: 'controller.js'
            },
            'careleaver-view': {
                path: 'view.js'
            },
            'careleaver-results': {
                path: 'results.js'
            }
        }
    },
    'careleaver-dialogs': {
        root: '/js/dialogs/careleaver/',
        modules: {
            'careleaver-dialog': {
                path: 'careleaver-dialog.js'
            }
        }
    },
    'aftercare-views': {
        root: '/js/views/aftercare/',
        modules: {
            'aftercare-controller-view': {
                path: 'controller.js'
            },
            'aftercare-view': {
                path: 'view.js'
            }
        }
    },
    'aftercare-dialogs': {
        root: '/js/dialogs/aftercare/',
        modules: {
            'aftercare-dialog': {
                path: 'aftercare-dialog.js'
            }
        }
    },
    'adoptioninformation-views': {
        root: '/js/views/adoptioninformation/',
        modules: {
            'adoptinformation-controller-view': {
                path: 'controller.js'
            },
            'adoptinformation-view': {
                path: 'view.js'
            }
        }
    },
    'adoptioninformation-dialogs': {
        root: '/js/dialogs/adoptioninformation/',
        modules: {
            'adoptioninformation-dialog': {
                path: 'adoptioninformation-dialog.js'
            }
        }
    },
    'chronology-views': {
        root: '/js/views/chronology/',
        modules: {
            'chronology-controller-view': {
                path: 'controller.js',
                requires: ['entry-controller-view', 'chronology-results', 'chronology-filter', 'chronology-results-print', 'chronology-dialog']
            },
            'chronology-results': {
                path: 'results.js'
            },
            'chronology-results-print': {
                path: 'print.js'
            },
            'chronology-filter': {
                path: 'filter.js'
            }
        }
    },
    'chronology-dialogs': {
        root: '/js/dialogs/chronology/',
        modules: {
            'chronology-dialog': {
                path: 'dialog.js',
                requires: ['entry-dialog', 'chronology-dialog-views']
            },
            'chronology-dialog-views': {
                path: 'chronology-dialog-views.js'
            },
            'chronology-output-document-dialog-views': {
                path: 'output/chronology-output-document-dialog-views.js'
            }
        }
    },
    'classification-views': {
        root: '/js/views/classifications/',
        modules: {
            'classification-autocomplete': {
                path: 'autocomplete.js'
            },
            'classification-mixin': {
                path: 'mixin.js'
            },
            'classification-models': {
                path: 'models.js'
            },
            'classification-form-add': {
                path: 'form-add.js'
            },
            'classification-form-edit': {
                path: 'form-edit.js'
            },
            'classification-form-delete': {
                path: 'form-delete.js'
            }
        }
    },
    'classification-configuration-views': {
        root: '/js/views/classification-configuration/',
        modules: {
            'classification-configuration-models': {
                path: 'models.js'
            },
            'classification-config-form-add': {
                path: 'classification-config-form-add.js'
            },
            'classification-config-form-edit': {
                path: 'classification-config-form-edit.js'
            },
            'classification-config-form-delete': {
                path: 'classification-config-form-delete.js'
            },
            'classification-config-form-view': {
                path: 'classification-config-form-view.js'
            },
            'classification-group-config-form-add': {
                path: 'classification-group-config-form-add.js'
            },
            'classification-group-config-form-edit': {
                path: 'classification-group-config-form-edit.js'
            },
            'classification-group-config-form-delete': {
                path: 'classification-group-config-form-delete.js'
            },
            'classification-group-config-form-view': {
                path: 'classification-group-config-form-view.js'
            }
        }
    },
    'resolveform-views': {
        root: '/js/views/forms/resolve/',
        modules: {
            'tab-navigation': {
                path: 'tabNavigation.js'
            },
            'form-status-buttons': {
                path: 'statusButtons.js'
            },
            'form-add-buttons': {
                path: 'addButtons.js'
            },
            'prform-tabs': {
                path: 'prForm/prFormTabs.js'
            },
            'form-search-view': {
                path: 'search.js'
            },
            'form-view-context': {
                path: 'viewContext.js'
            },
            'form-filter-context': {
                path: 'filterContext.js'
            },
            'form-content-context': {
                path: 'formContext.js'
            },
            'form-results-filter': {
                path: 'resultsFilter.js'
            },
            'prform-background-tab': {
                path: 'prForm/viewBackground.js'
            },
            'prform-health-tab': {
                path: 'prForm/viewHealth.js'
            },
            'prform-educationemployment-tab': {
                path: 'prForm/viewEducationEmployment.js'
            },
            'prform-skills-tab': {
                path: 'prForm/viewSkills.js'
            },
            'prform-contacts-tab': {
                path: 'prForm/viewContacts.js'
            },
            'prform-summary-tab': {
                path: 'prForm/viewSummary.js'
            },
            'prform-rejection-tab': {
                path: 'prForm/viewRejection.js'
            },
            'prform-print': {
                path: 'prForm/prFormPrintView.js'
            },
            'cpraform-tabs': {
                path: 'cpraForm/cpraFormTabs.js'
            },
            'cpraform-background-tab': {
                path: 'cpraForm/viewBackground.js'
            },
            'cpraform-risks-view': {
                path: 'cpraForm/cpraRisks.js'
            },
            'cpraform-plan-and-risk-table': {
                path: 'cpraForm/planAndRiskTable.js'
            },
            'cpraform-plan-view': {
                path: 'cpraForm/cpraPlan.js'
            },
            'cpraform-plan-and-risks-tabs': {
                path: 'cpraForm/viewPlanAndRisks.js'
            },
            'cpraform-views-tab': {
                path: 'cpraForm/viewViews.js'
            },
            'cpraform-rejection-tab': {
                path: 'cpraForm/viewRejection.js'
            },
            'cpraform-progress-tab': {
                path: 'cpraForm/viewProgress.js'
            },
            'cpraform-print': {
                path: 'cpraForm/cpraFormPrintView.js'
            },
            'intakeform-tabs': {
                path: 'intakeForm/intakeFormTabs.js'
            },
            'intakeform-referenced-person-view': {
                path: 'intakeForm/intakeFormReferencedPerson.js'
            },
            'intakeform-client-tab': {
                path: 'intakeForm/viewClient.js'
            },
            'intakeform-placement-tab': {
                path: 'intakeForm/viewPlacement.js'
            },
            'intakeform-referral-tab': {
                path: 'intakeForm/viewReferral.js'
            },
            'intakeform-health-tab': {
                path: 'intakeForm/viewHealth.js'
            },
            'intakeform-mentalhealth-tab': {
                path: 'intakeForm/viewMentalHealth.js'
            },
            'intakeform-physio-tab': {
                path: 'intakeForm/viewPhysio.js'
            },
            'intakeform-speech-tab': {
                path: 'intakeForm/viewSpeech.js'
            },
            'intakeform-needs-tab': {
                path: 'intakeForm/viewNeeds.js'
            },
            'intakeform-app1comms-tab': {
                path: 'intakeForm/viewApp1Comms.js'
            },
            'intakeform-app2mentalhealth-tab': {
                path: 'intakeForm/viewApp2MentalHealth.js'
            },
            'intakeform-app3behaviour-tab': {
                path: 'intakeForm/viewApp3Behaviour.js'
            },
            'intakeform-rejection-tab': {
                path: 'intakeForm/viewRejection.js'
            },
            'intakeform-print': {
                path: 'intakeForm/intakeFormPrintView.js'
            }
        }
    },
    'form-instance-views': {
        root: '/js/views/forms/generic/',
        modules: {
            'form-controller': {
                path: 'controller.js',
                requires: ['app-base', 'form-instance-results-view', 'form-instance-add-view', 'checklist-link-from', 'dismissible-message']
            },
            'form-instance-results-view': {
                path: 'form-instance-results-view.js',
                requires: ['form-instance-search-controller']
            },
            'form-instance-add-view': {
                path: 'form-instance-add-view.js',
                requires: ['form-instance-add-controller']
            },
            'form-instance-search-controller': {
                path: 'search/controller.js',
                requires: ['form-instance-results', 'form-instance-dialog', 'form-attachment-dialog', 'form-output-document-dialog-views', 'output-document-dialog-views', 'handlebars-form-templates']
            },
            'form-instance-results': {
                path: 'search/results.js',
                requires: ['app-filtered-results', 'form-results-formatters', 'form-instance-filter']
            },
            'form-instance-filter': {
                path: 'search/filter.js'
            },
            'form-instance-add-controller': {
                path: 'add/controller.js',
                requires: ['add-form-results', 'model-errors-plugin', 'add-form-dialog']
            },
            'add-form-results': {
                path: 'add/results.js',
                requires: ['add-form-filter', 'app-filtered-results']
            },
            'add-form-filter': {
                path: 'add/filter.js'
            },
            'form-reviews-view': {
                path: 'searchFormReviews.js'
            },
            'form-launcher': {
                path: 'form-launcher.js'
            },
            'form-instance-context': {
                path: 'formContext.js'
            }
        }
    },
    'form-instance-dialogs': {
        root: '/js/dialogs/form/',
        modules: {
            'add-form-dialog': {
                path: 'instance/add-dialog.js'
            },
            'form-instance-dialog': {
                path: 'instance/dialog.js'
            },
            'form-dialog-errors-plugin': {
                path: 'instance/form-dialog-errors-plugin.js'
            },
            'form-output-document-dialog-views': {
                path: 'output/form-output-document-dialog-views.js'
            },
            'form-attachment-dialog': {
                path: 'instance/attachment-dialog.js',
                requires: ['attachment-dialog']
            }
        }
    },
    'address-views': {
        root: '/js/address/',
        modules: {
            'app-address-autocomplete': {
                path: 'autocomplete-widget.js'
            },
            'base-AddressLocation-formatter': {
                path: 'base-formatAddressLocation.js'
            },
            'app-address-form-add': {
                path: 'form-add.js'
            },
            'app-address-form-end': {
                path: 'form-end.js'
            },
            'app-address-form-update-start-end': {
                path: 'form-update-start-end.js'
            },
            'app-address-form-reopen': {
                path: 'form-reopen.js'
            },
            'app-address-form-remove': {
                path: 'form-remove.js'
            },
            'address-add-widget': {
                path: 'addressAdd-widget.js'
            },
            'app-address-switch-do-not-disclose': {
                path: 'doNotDisclose-toggle.js'
            }
        }
    },
    'reference-numbers-views': {
        root: '/js/views/reference-numbers/',
        modules: {
            'app-reference-number-add-form': {
                path: 'form-add.js'
            },
            'app-reference-number-edit-remove-form': {
                path: 'form-edit-remove.js'
            }
        }
    },
    'relationship-views': {
        root: '/js/views/relationship/',
        modules: {
            'relationship-controller-view': {
                path: 'controller.js'
            },
            'relationship-graph-controller-view': {
                path: 'graph/controller.js'
            },
            'relationship-person-person-results': {
                path: 'results-person.js'
            },
            'relationship-filter': {
                path: 'filter.js'
            },
            'relationship-results-formatters': {
                path: 'formatters.js'
            },
            'relationship-person-organisation-results': {
                path: 'results-organisation.js'
            },
            //the following need re-organisation
            'relationships-attributes-data': {
                path: 'attributes-data.js'
            },
            'relationships-attributes-panel': {
                path: 'attributes-panel.js'
            },
            'relationships-person-organisation-forms': {
                path: 'forms-organisation.js'
            }
        }
    },
    'organisation-relationship-views': {
        root: '/js/views/organisationrelationship/',
        modules: {
            'organisation-relationship-controller-view': {
                path: 'controller.js'
            },
            'relationship-organisation-person-results': {
                path: 'results.js'
            }
        }
    },
    'relationship-dialogs': {
        root: '/js/dialogs/relationship/',
        modules: {
            'relationship-download-dialog': {
                path: 'relationship-download-dialog.js'
            }
        }
    },
    'checklist-views': {
        root: '/js/views/checklist/',
        modules: {
            'checklist-instance-dialog-view': {
                path: 'baseInstance.js'
            },
            'checklist-controller': {
                path: 'controller.js',
                requires: ['app-base', 'checklist-instance-results-view', 'checklist-instance-add-view']
            },
            'checklist-instance-results-view': {
                path: 'checklist-instance-results-view.js',
                requires: ['checklist-instance-search-controller']
            },
            'checklist-instance-add-view': {
                path: 'checklist-instance-add-view.js',
                requires: ['checklist-instance-add-controller']
            },
            'checklist-instance-search-controller': {
                path: 'search/controller.js',
                requires: ['checklist-instance-results-filter', 'checklist-instance-filter-context', 'checklist-instance-dialog-view', 'handlebars-checklist-partials', 'handlebars-checklist-templates']
            },
            'checklist-instance-results-filter': {
                path: 'search/filter.js'
            },
            'checklist-instance-filter-context': {
                path: 'search/filterContext.js'
            },
            'checklist-instance-add-controller': {
                path: 'add/controller.js',
                requires: ['checklist-ownership-util', 'checklist-add-results', 'model-errors-plugin', 'checklist-instance-dialog-views']
            },
            'checklist-add-filter': {
                path: 'add/filter.js'
            },
            'checklist-add-results': {
                path: 'add/results.js',
                requires: ['checklist-add-filter']
            }
        }
    },
    'form-definition-views': {
        root: '/js/views/forms/admin/',
        modules: {
            'form-definition-controller-view': {
                path: 'definition/controller.js'
            },
            'form-definition-filter': {
                path: 'definition/filter.js'
            },
            'form-definition-results': {
                path: 'definition/results.js'
            },
            'form-mappings-search-view': {
                path: 'form-mappings-search-view.js'
            }
        }
    },
    'checklist-definition-views': {
        root: '/js/views/checklist/admin/',
        modules: {
            'checklist-definition-controller-view': {
                path: 'controller.js'
            },
            'checklist-definition-filter': {
                path: 'filter.js'
            },
            'checklist-definition-results': {
                path: 'results.js'
            }
        }
    },
    'rule-search-views': {
        root: '/js/views/rule/admin/',
        modules: {
            'rule-definition-search-view': {
                path: 'rule-definition-search-view.js'
            },
            'rule-definition-filter-context': {
                path: 'filterContext.js'
            },
            'rule-definition-results-filter': {
                path: 'resultsFilter.js'
            }
        }
    },
    'calendar-search-views': {
        root: '/js/views/calendar/admin/',
        modules: {
            'calendar-search-view': {
                path: 'calendar-search-view.js'
            },
            'calendar-days-search-view': {
                path: 'calendar-days-search-view.js'
            },
            'calendar-nw-days-search-view': {
                path: 'calendar-nw-days-search-view.js'
            },
            'holiday-results': {
                path: 'holiday-results.js'
            },
            'holiday-controller': {
                path: 'holiday-controller.js'
            },
            'holiday-event-results': {
                path: 'holiday-event-results.js'
            },
            'holiday-event-controller': {
                path: 'holiday-event-controller.js'
            }
        }
    },
    'duplicaterecord-admin-views': {
        root: '/js/views/duplicaterecords/',
        modules: {
            'duplicaterecords-controller': {
                path: 'controller.js'
            },
            'duplicaterecords-results-view': {
                path: 'view.js'
            },
            'duplicaterecords-filter': {
                path: 'filter.js'
            },
        }
    },
    'print-views': {
        root: '/js/views/print/',
        modules: {
            'print-header-view': {
                path: 'printHeader.js'
            }
        }
    },
    'settings-views': {
        root: '/js/views/settings/',
        modules: {
            'settings-coded-entry-view': {
                path: 'viewCodedEntry.js',
                requires: ['app-tabbed-page', 'app-coded-entry-admin-view']
            },
            'person-warnings-coded-entry-view': {
                path: 'viewPersonWarningsCodedEntry.js'
            },
            'warning-related-checklist-definition-accordion-view': {
                path: 'accordions/warning-related-checklist-definition.js'
            }
        }
    },
    'output-admin-views': {
        root: '/js/views/output/admin/',
        modules: {
            'output-tabs': {
                path: 'outputTabs.js'
            },
            'base-output-admin-tab': {
                path: 'baseOutputAdminTab.js'
            },
            'person-output-admin': {
                path: 'personOutputAdmin.js'
            },
            'person-letter-output-admin': {
                path: 'personLetterOutputAdmin.js'
            },
            'casenote-output-admin': {
                path: 'casenoteOutputAdmin.js'
            },
            'chronology-output-admin': {
                path: 'chronologyOutputAdmin.js'
            },
            'child-looked-after-output-admin': {
                path: 'childLookedAfterOutputAdmin.js'
            },
            'form-output-admin': {
                path: 'formOutputAdmin.js'
            },
            'output-template-results-filter': {
                path: 'resultsFilter.js'
            },
            'form-output-template-results-filter': {
                path: 'formOutputResultsFilter.js'
            },
            'output-template-results': {
                path: 'outputTemplateResults.js'
            }
        }
    },
    'output-dialogs': {
        root: '/js/dialogs/output/',
        modules: {
            'output-template-dialog-views': {
                path: 'output-template-dialog-views.js'
            },
            'casenote-output-template-dialog-views': {
                path: 'casenote-output-template-dialog-views.js'
            },
            'chronology-output-template-dialog-views': {
                path: 'chronology-output-template-dialog-views.js'
            },
            'person-output-template-dialog-views': {
                path: 'person-output-template-dialog-views.js'
            },
            'person-letter-output-template-dialog-views': {
                path: 'person-letter-output-template-dialog-views.js'
            },
            'form-output-template-dialog-views': {
                path: 'form-output-template-dialog-views.js'
            },
            'child-looked-after-output-template-dialog-views': {
                path: 'child-looked-after-output-template-dialog-views.js'
            },
            'output-document-dialog-views': {
                path: 'output-document-dialog-views.js'
            },
            'output-filters-extension': {
                path: 'output-filters-extension.js'
            }
        }
    },
    'team-views': {
        root: '/js/views/team/',
        modules: {
            'team-controller-view': {
                path: 'controller.js',
                requires: ['app-filter', 'app-filtered-results', 'app-results',
                    'app-tabbed-page', 'tabbed-page', 'app-toolbar', 'filtered-tabs-view',
                    'team-results', 'security-profile-results', 'security-domain-results',
                    'team-dialog', 'team-members-view', 'team-tabviewer-role',
                    'team-security-dialog'
                ]
            },
            'team-members-view': {
                path: 'team-members.js'
            },
            'team-tabviewer-role': {
                path: 'tabviewer-role.js'
            },
            'team-results': {
                path: 'team-results.js',
                requires: ['team-filter']
            },
            'team-filter': {
                path: 'team-filter.js',
                requires: ['results-filter', 'handlebars-helpers', 'handlebars-organisation-templates']
            },
            'security-profile-results': {
                path: 'security-profile-results.js',
                requires: ['security-profile-filter', 'security-profile-access-levels']
            },
            'security-profile-filter': {
                path: 'security-profile-filter.js',
                requires: ['results-filter', 'handlebars-helpers', 'handlebars-organisation-templates']
            },
            'security-domain-results': {
                path: 'security-domain-results.js',
                requires: ['security-domain-filter', 'security-domain-contents']
            },
            'security-domain-filter': {
                path: 'security-domain-filter.js',
                requires: ['results-filter', 'handlebars-helpers', 'handlebars-organisation-templates']
            },
            'security-profile-access-levels': {
                path: 'security-profile-access-levels.js',
                requires: ['access-level-values', 'react-security-app-view']
            },
            'access-level-values': {
                path: 'access-level-values.js',
            },
            'security-domain-contents': {
                path: 'security-domain-contents.js',
                requires: ['react-security-app-view']
            },
            'react-security-app-view': {
                path: 'react-security-app-view.js'
            }
        }
    },
    'team-security-dialogs': {
        root: '/js/dialogs/team-security/',
        modules: {
            'team-security-dialog': {
                path: 'dialog.js'
            }
        }
    },
    'location-dialog': {
        root: '/js/dialogs/location/',
        modules: {
            'location-dialog': {
                path: 'locationDialog.js'
            }
        }
    },
    'user-views': {
        root: '/js/views/user/',
        modules: {
            'user-search-view': {
                path: 'search.js'
            },
            'user-results-filter': {
                path: 'userFilter.js'
            },
            'user-results-filter-context': {
                path: 'userFilterContext.js'
            }
        }
    },
    'groupofrights-views': {
        root: '/js/views/groupofrights/',
        modules: {
            'groupofrights-search-view': {
                path: 'search.js'
            }
        }
    },
    'micro-templates': {
        root: '/js/micro-templates/',
        modules: {
            'template-all-toggle-button': {
                path: 'all-toggle-button.js'
            },
            'template-toggle-expand-collapse': {
                path: 'toggle-expand-collapse.js'
            },
            'template-output-button': {
                path: 'output-button.js'
            }
        }
    },
    'results': {
        root: '/js/results/',
        modules: {
            'caserecording-results-formatters': {
                path: 'formatters.js'
            },
            'attachment-results-formatters': {
                path: 'attachment-results-formatters.js'
            },
            'form-results-formatters': {
                path: 'form-results-formatters.js'
            },
            'app-view-mode-plugin': {
                path: 'view-mode-plugin.js'
            }
        }
    },
    'checklist-link': {
        root: '/js/checklist/',
        modules: {
            'checklist-link-from': {
                path: 'checklist-link-from.js'
            }
        }
    },
    'utils': {
        root: '/js/utils/',
        modules: {
            'app-utils-person': {
                path: 'app-utils-person.js'
            },
            'app-utils': {
                path: 'app-utils.js'
            }
        }
    },
    'graph': {
        root: '/js/graph/',
        modules: {
            'graph-template': {
                path: 'keyTemplate.js'
            },
            'nodeTemplate': {
                path: 'nodeTemplate.js'
            },
            'usp-graph': {
                path: 'graph.js',
                requires: ['usp-graph-css']
            }
        }
    },
    'genogram': {
        root: '/js/views/genogram/',
        modules: {
            'genogram-controller-view': {
                path: 'controller.js'
            },
            'genogram-model': {
                path: 'model.js'
            },
            'genogram-templates': {
                path: 'templates.js'
            }
        }
    },
    'genogramPrint': {
        root: '/js/dialogs/relationship/',
        modules: {
            'relationship-printgenogram-dialog': {
                path: 'relationship-printgenogram-dialog.js'
            }
        }
    },
    'navigation': {
        root: '/js/navigation/',
        modules: {
            'usp-navigation': {
                path: 'navigation.js'
            }
        }
    },
    'cropper': {
        root: '/js/cropper/',
        modules: {
            'usp-cropper': {
                path: 'cropper.js'
            }
        }
    },
    'person-summary-administration-views': {
        root: '/js/views/person-summary/admin/',
        modules: {
            'person-summary-administration-view': {
                path: 'person-summary-administration.js'
            }
        }
    },
    'approvals-views': {
        root: '/js/views/approvals/',
        modules: {
            'approvals-controller-view': {
                path: 'controller.js'
            },
            'approvals-view': {
                path: 'approvals.js'
            },
            'foster-approvals-accordion-views': {
                path: 'accordions/foster-approvals.js'
            },
            'adoption-approvals-accordion-views': {
                path: 'accordions/adoption-approvals.js'
            },
            'household-accordion-views': {
                path: 'accordions/household.js'
            },
            'training-accordion-views': {
                path: 'accordions/training.js'
            },
            'availability-accordion-views': {
                path: 'accordions/availability.js'
            },
            'approval-registrations-view': {
                path: 'accordions/registrations.js'
            },
            'adoptive-child': {
                path: 'adoptiveChild.js'
            }
        }
    },
    'audit-views': {
        root: '/js/views/audit/',
        modules: {
            'audit-controller-view': {
                path: 'controller.js'
            },
            'audit-filter': {
                path: 'filter.js'
            },
            'audit-personaccess-accordion-views': {
                path: 'accordions/personaccess.js'
            },
            'audit-eventlog-accordion-views': {
                path: 'accordions/eventlog.js'
            }
        }
    },
    'demo-views': {
        root: '/js/views/demo/',
        modules: {
            'demo-controller-view': {
                path: 'controller.js'
            },
            'demo-filter': {
                path: 'filter.js'
            },
            'demo-personaccess-accordion-views': {
                path: 'accordions/personaccess.js'
            }
        }
    },
    'matching-views': {
        root: '/js/views/matching/',
        modules: {
            'matching-controller-view': {
                path: 'matching-controller-view.js'
            },
            'matching-results': {
                path: 'matching-results.js'
            },
            'matching-filter': {
                path: 'matching-filter.js'
            }
        }
    },
    'source-views': {
        root: '/js/views/source/',
        modules: {
            'source-form': {
                path: 'source-form.js'
            }
        }
    },
    'attachment-filter-views': {
        root: '/js/views/attachment/',
        modules: {
            'attachment-filter': {
                path: 'attachment-filter.js'
            }
        }
    },
    'notification-views': {
        root: '/js/views/notification/',
        modules: {
            'notification-recipients': {
                path: 'recipients.js'
            }
        }
    },
    'application-css': {
        root: '/css/',
        modules: {
            'usp-graph-css': {
                type: 'css',
                path: 'graph.css'
            },
            'inline-input-css': {
                type: 'css',
                path: 'inline-input.css'
            }
        }
    },
    'templates': {
        root: '/handlebars/',
        modules: {
            'handlebars-person-templates': {
                path: 'person.jsp'
            },
            'handlebars-organisation-templates': {
                path: 'organisation.jsp'
            },
            'handlebars-entry-templates': {
                path: 'entry.jsp'
            },
            'handlebars-casenote-templates': {
                path: 'casenote.jsp'
            },
            'handlebars-chronology-templates': {
                path: 'chronology.jsp'
            },
            'handlebars-relationship-templates': {
                path: 'relationship.jsp'
            },
            'handlebars-classification-templates': {
                path: 'classification.jsp'
            },
            'handlebars-classification-configuration-templates': {
                path: 'classificationConfiguration.jsp'
            },
            'handlebars-group-templates': {
                path: 'group.jsp'
            },
            'handlebars-resolveform-templates': {
                path: 'resolveform.jsp'
            },
            'handlebars-form-templates': {
                path: 'form.jsp'
            },
            'handlebars-matching-templates': {
                path: 'matching.jsp'
            },
            'handlebars-calendar-templates': {
                path: 'calendar.jsp'
            },
            'handlebars-calendar-partials': {
                path: 'calendarPartials.jsp'
            },
            'handlebars-checklist-templates': {
                path: 'checklist.jsp'
            },
            'handlebars-checklist-partials': {
                path: 'checklistPartials.jsp'
            },
            'handlebars-rule-templates': {
                path: 'rule.jsp'
            },
            'handlebars-form-partials': {
                path: 'formPartials.jsp'
            },
            'handlebars-resolveform-partials': {
                path: 'resolveformPartials.jsp'
            },
            'handlebars-cpraform-templates': {
                path: 'cpraForm.jsp'
            },
            'handlebars-cpraform-print-templates': {
                path: 'cpraFormPrint.jsp'
            },
            'handlebars-prform-templates': {
                path: 'prForm.jsp'
            },
            'handlebars-prform-print-templates': {
                path: 'prFormPrint.jsp'
            },
            'handlebars-intakeform-templates': {
                path: 'intakeForm.jsp'
            },
            'handlebars-intakeform-print-templates': {
                path: 'intakeFormPrint.jsp'
            },
            'handlebars-user-templates': {
                path: 'user.jsp'
            },
            'handlebars-groupofrights-templates': {
                path: 'groupofrights.jsp'
            },
            'handlebars-personlock-templates': {
                path: 'personLock.jsp'
            },
            'handlebars-context-templates': {
                path: 'context.jsp'
            },
            'handlebars-advancedSearch-templates': {
                path: 'advancedSearch.jsp'
            },
            'handlebars-systemConfig-templates': {
                path: 'systemConfig.jsp'
            },
            'handlebars-selfCredentialChange-templates': {
                path: 'selfCredentialChange.jsp'
            },
            'handlebars-output-templates': {
                path: 'output.jsp'
            },
            'handlebars-person-summary-admin-templates': {
                path: 'personSummaryAdmin.jsp'
            },
            'handlebars-childlookedafter-templates': {
                path: 'childLookedAfter.jsp'
            },
            'handlebars-careleaver-templates': {
                path: 'careLeaver.jsp'
            },
            'handlebars-aftercare-templates': {
                path: 'aftercare.jsp'
            },
            'handlebars-carerApproval-templates': {
                path: 'carerApproval.jsp'
            },
            'handlebars-childProtection-templates': {
                path: 'childProtection.jsp'
            },
            'handlebars-belongings-templates': {
                path: 'belongings.jsp'
            },
            'handlebars-education-templates': {
                path: 'education.jsp'
            },
            'handlebars-subjectaccessrequest-templates': {
                path: 'subjectAccessRequest.jsp'
            },
            'handlebars-source-templates': {
                path: 'source.jsp'
            },
            'handlebars-notification-templates': {
                path: 'notification.jsp'
            },
            'handlebars-settings-templates': {
                path: 'settings.jsp'
            },
            'handlebars-dashboard-templates': {
                path: 'dashboard.jsp'
            },
            'handlebars-duplicaterecords-templates': {
                path: 'duplicaterecords.jsp'
            },
            'handlebars-audit-templates': {
                path: 'audit.jsp'
            },
            'handlebars-demo-templates': {
                path: 'demo.jsp'
            },
            'handlebars-location-templates': {
                path: 'location.jsp'
            },
            'handlebars-address-templates': {
                path: 'address.jsp'
            },
            'handlebars-header-templates': {
                path: 'header.jsp'
            },
            'handlebars-deletion-templates': {
                path: 'deletion.jsp'
            }
        }
    }
};
