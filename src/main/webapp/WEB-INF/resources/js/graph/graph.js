YUI.add("usp-graph", function(Y) {
	var N=Y.Node,
	A=Y.Array,
	E=Y.Escape,
	ORGANISATION_PREFIX="ORG_",
	PERSON_PREFIX="PER_",
	RELATIONSHIP_PREFIX="REL_",
	DEFAULT_LANE=3,
	DIAGRAM_DIV='<div class="diagram" style="position:relative;overflow:hidden;width:100%;height:800px"></div>',
	OUTLINE_DIV='<div class="outline"></div>',
	MENU_DIV='<div class="graph-menu"></div>',
	KEY_DIV='<div class="graph-key"></div>',
	TEMPLATE='<span class="box <%= this.isDeceased===true ? "deceased" : "" %>"><i class="fa fa-user"></i> <strong><%= this.name %></strong></span>',
	EDGE_TEMPLATE='<div id="<%= this.relationshipId %>-label" class="<%= this.automated===true ? "automatedLabel":"edgeLabel" %>"><span class="<%= this.automated===true ? "isAutomated":"" %>"><%= this.label %></span></div>',
	KEY_TEMPLATE='<div>Generation layout : <%= this.name %></div>',
	MENU_TEMPLATE='<div></div>',
	HUB_AND_SPOKE_LAYOUT='HUB_AND_SPOKE',
	SWIMLANE_LAYOUT="SWIMLANE",
	ORGANIC_LAYOUT="ORGANIC",
	HUB_AND_SPOKE_LAYOUT_NAME='Hub and spoke',
	SWIMLANE_LAYOUT_NAME="Generation",
	ORGANIC_LAYOUT_NAME="Organic",
	mxHubAndSpokeLayout=function(g){
		mxGraphLayout.call(this, g);
	};

	mxHubAndSpokeLayout.prototype = new mxGraphLayout;
	mxHubAndSpokeLayout.prototype.constructor = mxHubAndSpokeLayout;
	mxHubAndSpokeLayout.prototype.resetEdges = true;
	mxHubAndSpokeLayout.prototype.disableEdgeStyle = true;


	mxHubAndSpokeLayout.prototype.isVertexIgnored=function(vertex){
	    return !this.graph.getModel().isVertex(vertex) || !this.graph.isCellVisible(vertex) || this.graph.isSwimlane(vertex);
	};

	mxHubAndSpokeLayout.prototype.execute = function(parent, rootCell) {
	    var graph=this.graph,
	    model = graph.getModel(),
	    descendants=model.getDescendants(parent),
	    i,
	    cell,cells=[];


	    model.beginUpdate();
	    try {
    		for (i = 0; i < descendants.length; i++) {
	            cell = descendants[i];
	            if (this.isVertexIgnored(cell)){
	            	 this.isEdgeIgnored(cell) || (this.resetEdges && (this.graph.resetEdge(cell),this.setEdgePoints(cell, null)), (this.disableEdgeStyle && (this.setEdgeStyleEnabled(cell, false), this.setOrthogonalEdge(cell, false))));
	            }else {
	                cells.push(cell);
	            }
	        }

			var length = cells.length,spokeCount=/*(length>1?length-1:length)*/length,phi = 2 * Math.PI / spokeCount,
			r = spokeCount * 150 / Math.PI;
			cx = 0,// graph.container.scrollWidth / 2;
			cy = 0;// graph.container.scrollHeight / 2;

			//reset r if too small
			r = r<600?600:r;

			for (i = 0; i < length; i++)
			{
				cell=cells[i];
				if(cell===rootCell){
					//this.isVertexMovable(cell) && this.setVertexLocation(cell, cx,cy);
					//this.isVertexMovable will return false now movable cells have been disabled, which would break the Hub & spoke layout
					this.setVertexLocation(cell, cx,cy);
				}else{
					//this.isVertexMovable(cell) && this.setVertexLocation(cell, (r * Math.sin(i * phi))+cx, (r * Math.cos(i * phi))+cy);
					//this.isVertexMovable will return false now movable cells have been disabled, which would break the Hub & spoke layout
					this.setVertexLocation(cell, (r * Math.sin(i * phi))+cx, (r * Math.cos(i * phi))+cy);
				}
			}
	    }finally {
	    	model.endUpdate();
	    }
	};



	Y.namespace('usp').Graph=Y.Base.create('graph', Y.Widget,[],{
		initializer : function(cfg) {

			var instance=this,
			custTemplate = instance.get('template'),
			custKeyTemplate = instance.get('keyTemplate'),
			custMenuTemplate = instance.get('menuTemplate'),
			templateEngine=new Y.Template(Y.Template.Micro);

			//loading mask timer
			instance.loadingMaskTimer={};

			// Compile a template into a reusable function.
			instance.template = custTemplate||templateEngine.compile(TEMPLATE);
			instance.edgeTemplate = templateEngine.compile(EDGE_TEMPLATE);
			instance.keyTemplate = custKeyTemplate||templateEngine.compile(KEY_TEMPLATE);
			instance.menuTemplate = custMenuTemplate|| templateEngine.compile(MENU_TEMPLATE);

			//setup layout type
			instance.layout=SWIMLANE_LAYOUT;

			//setup ancestor model
			instance.ancestorModel=new mxGraphModel();

			//setup the swimlanes against the ancestor model
			instance._setupLanes(instance.ancestorModel);
	    },
	    bindUI: function(){
	    	var instance=this,data=instance.get('data');


			// bind to data event
			data.after('load', instance._handleAfterDataLoad, instance);
			data.after('error', instance._handleAfterDataLoadError, instance);


			/* Hide graph menu on click of page elements */
			Y.delegate('click', function () {
				this.graph.popupMenuHandler.hideMenu();
			}, '#sectionToolbar', 'a', instance);

			Y.on('relationship:filter', function(){
				this.graph.popupMenuHandler.hideMenu();
			}, instance);

			Y.one('div.page-header').on('click', function () {
				this.graph.popupMenuHandler.hideMenu();
			  }, instance);

			Y.one('header').on('click', function () {
				this.graph.popupMenuHandler.hideMenu();
			  }, instance);
	    },
	    renderUI: function(){
	    	var outline, hint,
	    	instance = this,
	    	contentBox = instance.get('contentBox'),
	    	boundingBox = instance.get('boundingBox'),
	    	showOutline = instance.get('showOutline'),
	    	keyEnabled = instance.get('keyEnabled');

	    	if(true===keyEnabled){
		    	//add node for key
	    		instance.keyContainer = N.create(KEY_DIV);
	    		instance.keyContainer.hide();
	    		instance.keyContainer.appendTo(contentBox);
	    	}

	    	//add node for diagram
	    	instance.diagram = N.create(DIAGRAM_DIV);
	    	instance.menuContainer = N.create(MENU_DIV);
	    	instance.menuContainer.appendTo(contentBox);
	    	instance.diagram.appendTo(contentBox);


	    	if(mxClient.IS_TOUCH){
	    		showOutline = true;
	    	}

	    	if(true===showOutline){
		    	//add node for outline
		    	outline=N.create(OUTLINE_DIV);
		    	outline.appendTo(contentBox);
	    	}

	    	//plug in loading mask
			boundingBox.plug(Y.Plugin.BusyOverlay,{css:'graph-disabled-mask'});

	    	switch(this.get('renderMode')){
		    	case 'fast':
		    		hint=mxConstants.DIALECT_MIXEDHTML;
		    		break;
		    	case 'faster':
		    		hint=mxConstants.DIALECT_PREFERHTML;
		    		break;
		    	case 'fastest':
		    		hint=mxConstants.DIALECT_STRICTHTML;
		    		break;
		    	case 'exact':
		    		hint=mxConstants.DIALECT_VML;
	    		break;
	    		default:
	    			hint=mxConstants.DIALECT_MIXEDHTML;
	    	};

	    	//setup graph
	    	instance.graph=new mxGraph(instance.diagram.getDOMNode(), instance.model, hint);

	    	// for Ipads - disabling double tap - single tap is now equal to click
	    	instance.graph.doubleTapEnabled = false;
	    	//instance.graph.tolerance = 40;

			//setup cell overlays
			instance.vertexCellOverlay=instance._createVertexCellOverlay();
			instance.edgeCellOverlay=instance._createEdgeCellOverlay();

	    	//configure graph settings
	    	instance._configureGraph();
	    	//configure stylesheet settings
	    	instance._configureStylesheet();

			if(outline){
				instance.outline=new mxOutline(instance.graph, outline.getDOMNode());
			}
	    },load:function(arg){
	    	var instance=this, data=instance.get('data'), timer=instance.loadingMaskTimer.timer;

	    	instance.clearSelection();

        	if (timer) {
        		timer.cancel();
            }

        	//timer to start mask
        	instance.loadingMaskTimer.timer=Y.later(300, instance, instance._setBusyOverlay, [true], false);

	    	data.load(arg);
	    },
	    _handleAfterDataLoad: function(e){
	    	var instance=this;

	    	//null out root cell
	    	instance.rootCellId=null;

	    	//load ancestor model
	    	instance._loadAncestorModel(e.currentTarget);

	    	//set graph visibility
	    	instance._setGraphVisibility();

    		//execute layout
	    	instance._executeLayout();

        	//hide any loading mask
        	instance._setBusyOverlay(false);
	    },
	    _handleAfterDataLoadError: function(e){

	    	var instance=this;
	    	instance._setBusyOverlay(false);

	    	Y.one('#relationshipValidationError').setHTML('<a href="#" class="close" title="Close">Close</a><h4 tabindex="0">'+instance.get('msgServerErrorLine1')+'</h4><p>'+instance.get('msgServerErrorLine2')+'</p>');
	    	Y.one('#relationshipValidationError').show();

	    },
	    _configureGraph: function(){
	    	var instance=this, graph=instance.graph,contentBox=instance.get('contentBox'),
	    	template=instance.template,edgeTemplate=instance.edgeTemplate;

	    	//TODO - allow these to be edited by the caller

	    	// Disables built-in context menu
			mxEvent.disableContextMenu(contentBox.getDOMNode());

			//turn off edit
			graph.setEnabled(false);

			//enable left click move
			graph.setPanning(true);
			graph.panningHandler.useLeftButtonForPanning = true;
			graph.setCellsSelectable(false);


		    // Shows hand cursor while panning
			graph.panningHandler.addListener(mxEvent.PAN_START, mxUtils.bind(graph, function()
			{
				this.container.style.cursor = 'move';
			}));


			graph.panningHandler.addListener(mxEvent.PAN_END, mxUtils.bind(graph, function()
			{
				this.container.style.cursor = 'default';
			}));

			//stop cells moving outside parent
			graph.graphHandler.removeCellsFromParent = false;

			//zoom to the center
			//graph.centerZoom = true;

			//set grid size
			graph.gridSize = 20;

			//enable tooltips
			graph.setTooltips(true);

			//Stop dangling edges
			graph.setAllowDanglingEdges(false);
			graph.setDisconnectOnMove(false);
			graph.setConnectable(false);
			graph.setCellsDisconnectable(false);
			graph.setCellsCloneable(false);
			graph.dropEnabled = false;
			graph.edgeLabelsMovable=false;
			graph.setAutoSizeCells(true);

			// no resize
			graph.isCellResizable = function(cell){
				return false;
			};

			//allow move
			graph.isCellMovable = function(cell){
				//allow moving of cells
				//return !graph.isSwimlane(cell);
				return false;
			};



			// Edges are not editable
			graph.isCellEditable = function(cell){
				return false;
			};

			graph.getLabel = function(cell){
				if (this.getModel().isVertex(cell) && !(this.isSwimlane(cell)===true)){
					return template(cell.value);
				}else if(this.isSwimlane(cell)){
					return cell.value;
				}else{
					return edgeTemplate(cell.value);
				}
			};

			graph.isHtmlLabel=function(cell){
				return !this.isSwimlane(cell);
			};

			// Installs a popupmenu handler
			graph.popupMenuHandler.init();
			graph.popupMenuHandler.factoryMethod = function(menu, cell, evt){
				return instance._createPopupMenu(menu, cell, evt);
			};

			//set z-index
			graph.popupMenuHandler.zIndex=9;
			graph.tooltipHandler.zIndex=8;

			// Ensure touch-only
			// There are some conditionals within the event such as Y.UA.gecko > 0
			// that will never trigger, but are left in just in case this event isn't touch-only.
			if(mxClient.IS_TOUCH) {
				graph.addListener(mxEvent.CLICK, function(sender, evt) {

					var cell = evt.getProperty('cell'); // cell may be null
					var xCoordinate;
	                var yCoordinate;

	                if(mxClient.IS_TOUCH && cell.vertex) {
	                	xCoordinate = Number(Y.one(evt.properties.event.currentTarget).getXY()[0]);
	                	yCoordinate = Number(Y.one(evt.properties.event.currentTarget).getXY()[1]) + 20;
	                } else if(mxClient.IS_TOUCH && cell.edge) {
	                	xCoordinate = Number(Y.one('#' + cell.value.relationshipId + '-label').getXY()[0]);
	                	yCoordinate = Number(Y.one('#' + cell.value.relationshipId + '-label').getXY()[1]);
	                } else if(Y.UA.gecko > 0) {
	                	xCoordinate = evt.getProperty("event").clientX;
	                	yCoordinate = evt.getProperty("event").clientY;
	                } else {
	                	xCoordinate = evt.getProperty("event").clientX+(5*graph.view.scale);
	                	yCoordinate = evt.getProperty("event").pageY+(5*graph.view.scale);
	                }

	                if(cell && cell.vertex)  {
	                    graph.popupMenuHandler.popup(xCoordinate, yCoordinate, cell, evt);
					} else if (cell && cell.edge) {
						graph.popupMenuHandler.popup(xCoordinate, yCoordinate, cell, evt);
					}
				});
			}


			// Installs a custom tooltip for cells
			graph.getTooltipForCell = function(cell) {

				var sourceNode, targetNode;

				if(cell.isEdge()) {

					sourceNode = cell.getTerminal(true);
					targetNode = cell.getTerminal();

					// TODO deal with person -> organisation relationship

					if(targetNode.value && targetNode.value._type === 'PERSON') {

						var str = ' is a ';
						var of = ' of';

						if (cell.value.label.toString().substring(0,1).toLowerCase()==='a'){
							str = ' is an ';
						}

						if(cell.value.descrimatorType==='ProfessionalRelationshipType') {
							of = ' ';
						}

						return E.html(targetNode.value.name + str + cell.value.label.toLowerCase().replace('gp','GP') + ' of ' + sourceNode.value.name);
					}

				} else if(cell.isVertex() && cell.value && cell.value.id){
					//check we haven't shown a YUI3 tooltip
					//if(alertTip.get('visible')===false){
						var alertStr="";
						if(cell.value.alerts){
							A.each(cell.value.alerts, function(a){
								alertStr+=" Alert: "+a.code;
							});
						}

						var ethnicity = "";
						if(cell.value.ethnicity){
							ethnicity='\n'+Y.uspCategory.person.ethnicity.category.codedEntries[cell.value.ethnicity].name;
						}

						var dob=Y.USPDate.formatDateValue(cell.value.dateOfBirth, cell.value.fuzzyDateMask);
						var dod=Y.USPDate.formatDateValue(cell.value.diedDate, cell.value.fuzzyDiedDateMask);


						var estimated = "";
						if(cell.value.dateOfBirthEstimated){
							estimated=" Estimated ";
						}

						var age = cell.value.age , ageLabel = '';


						if(age >= 0){
							if(age>1){
								ageLabel =" ("+cell.value.age+" years)";
							}else{
								ageLabel =" ("+cell.value.age+" year)";
							}
						}


						var warnings="";
						if (cell.value.numberOfWarnings){
							if (cell.value.numberOfWarnings===1){
								warnings='\n'+"This person has 1 warning:";
							}else{
								warnings='\n'+"This person has "+cell.value.numberOfWarnings+" warnings:";
							}
						}


						if(cell.value.personWarningVO && cell.value.personWarningVO.personWarningAssignmentVOs) {
							var warningsArray = cell.value.personWarningVO.personWarningAssignmentVOs;
							A.forEach(warningsArray, function(warning) {
								warnings+='\n'+"- "+ warning.codedEntryVO.name;
							});
						}

						var dobAgeEstimatedDobValue = '';
						if(dob && dob != '' && !dod){
							dobAgeEstimatedDobValue = 'Born: ' + dob+estimated+ageLabel;
						} else if(dob && dod) {
							dobAgeEstimatedDobValue = 'Born: ' + dob+estimated;
						}

						if(!dob) {
							dobAgeEstimatedDobValue = 'Born: Unknown';
						}

						var diedDate = '';
						if(dod) {
							diedDate = '\nDied: ' + dod + ageLabel;
						}

						var namePerIdDateValue = '';

						/*constructing the return string for unborn child*/
						var lifeState = cell.value.lifeState;
						var dueDate = Y.USPDate.formatDateValue(cell.value.dueDate, cell.value.fuzzyDateMask);
						var dueDateUnbornValue = dueDate+" Unborn";
						var notCarriedToTermValue = "Not carried";

						if(lifeState == "UNBORN"){
							namePerIdDateValue = cell.value.name+" ("+cell.value.personId+")"+'\n'+dueDateUnbornValue+ethnicity+warnings+alertStr;
						}else if(lifeState == "ALIVE" || lifeState == null){
							namePerIdDateValue = cell.value.name+" ("+cell.value.personId+")"+'\n'+dobAgeEstimatedDobValue+ethnicity+warnings+alertStr;
						}else if(lifeState == "NOT_CARRIED_TO_TERM"){
							namePerIdDateValue = cell.value.name+" ("+cell.value.personId+")"+'\n'+notCarriedToTermValue+ethnicity+warnings+alertStr;
						}else if(lifeState == "DECEASED") {
							namePerIdDateValue = cell.value.name+" ("+cell.value.personId+")"+'\n'+dobAgeEstimatedDobValue+diedDate+ethnicity+warnings+alertStr;
						}
						return E.html(namePerIdDateValue);
				}
				return "";
			};

		    // Changes the zoom on mouseWheel events
		    mxEvent.addMouseWheelListener(function (evt, up){
			    if (!mxEvent.isConsumed(evt)){
			    	//close the menu
			    	graph.popupMenuHandler.hideMenu();

			    	if (up)
					{
			    		graph.zoomIn();
					}else{
						graph.zoomOut();
					}

					mxEvent.consume(evt);
			    }
		    });

	    },_configureStylesheet: function(){
	    	//TODO - allow these to be edited by the caller
	    	var instance=this, graph=instance.graph,
	    	styles=graph.getStylesheet(),
	    	defaultStyle=styles.getDefaultVertexStyle(),
	    	swimlaneStyle=mxUtils.clone(defaultStyle),
	    	rootStyle, personStyle, femaleStyle, maleStyle,nogenderStyle,
	    	organisationStyle,
	    	edgeStyle,
	    	familialEdgeStyle,
	    	socialEdgeStyle,
	    	secondaryEdgeStyle,
	    	suggestionEdgeStyle,
	    	automatedEdgeStyle;

	    	//configure swimlane style
			swimlaneStyle[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_SWIMLANE;
			swimlaneStyle[mxConstants.STYLE_VERTICAL_ALIGN] = 'middle';
			swimlaneStyle[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = 'white';
			swimlaneStyle[mxConstants.STYLE_FONTSIZE] = 11;
			swimlaneStyle[mxConstants.STYLE_STARTSIZE] = 22;
			swimlaneStyle[mxConstants.STYLE_HORIZONTAL] = true;
			swimlaneStyle[mxConstants.STYLE_FONTCOLOR] = 'black';
			swimlaneStyle[mxConstants.STYLE_STROKECOLOR] = '#777';
			swimlaneStyle[mxConstants.STYLE_FOLDABLE] = 0;
			delete swimlaneStyle[mxConstants.STYLE_FILLCOLOR];
			styles.putCellStyle('swimlane', swimlaneStyle);

			//default styling
			defaultStyle[mxConstants.STYLE_ROUNDED] = true;
			defaultStyle[mxConstants.STYLE_ARCSIZE] = 6;
			defaultStyle[mxConstants.STYLE_FILLCOLOR] = '#F5f5f5';
			defaultStyle[mxConstants.STYLE_STROKECOLOR] = '#C5C5C5';
			defaultStyle[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_RECTANGLE;


	    	//configure person style
			personStyle=mxUtils.clone(defaultStyle);
			personStyle[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_RECTANGLE;
			personStyle[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
			personStyle[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
			personStyle[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP;
			personStyle[mxConstants.STYLE_STROKEWIDTH] = '2';
			personStyle[mxConstants.STYLE_FONTCOLOR] = '#2278d0';
			personStyle[mxConstants.STYLE_FONTFAMILY] = 'OpenSansRegular, Arial, Helvetica, sans-serif';
			personStyle[mxConstants.STYLE_OVERFLOW] = 'fill';
			personStyle[mxConstants.STYLE_FONTSIZE] = 9;
			personStyle[mxConstants.STYLE_RESIZABLE] = 0;
			personStyle[mxConstants.STYLE_AUTOSIZE] = 1;
			personStyle[mxConstants.STYLE_SPACING_LEFT ] = 10;
			personStyle[mxConstants.STYLE_SPACING_RIGHT ] = 45;
			styles.putCellStyle('person', personStyle);

			//configure nominated person
			rootStyle=mxUtils.clone(personStyle);
			rootStyle[mxConstants.STYLE_STROKEWIDTH] = '4';
			rootStyle[mxConstants.STYLE_STROKECOLOR] = '#34495e';
			rootStyle[mxConstants.STYLE_FILLCOLOR] = '#e1e1e1';
			styles.putCellStyle('root', rootStyle);

			femaleStyle=mxUtils.clone(personStyle);
			femaleStyle[mxConstants.STYLE_STROKECOLOR] = '#E29CCD';
			styles.putCellStyle('female', femaleStyle);

			maleStyle=mxUtils.clone(personStyle);
			maleStyle[mxConstants.STYLE_STROKECOLOR] = '#2278D0';
			styles.putCellStyle('male', maleStyle);

			indeterminateStyle=mxUtils.clone(personStyle);
			indeterminateStyle[mxConstants.STYLE_STROKECOLOR] = '#F29D06';
			styles.putCellStyle('indeterminate', indeterminateStyle);

			nogenderStyle=mxUtils.clone(personStyle);
			nogenderStyle[mxConstants.STYLE_STROKECOLOR] = '#000000';
			styles.putCellStyle('no-gender', nogenderStyle);

			//configure organisation style
			organisationStyle=mxUtils.clone(personStyle);
			organisationStyle[mxConstants.STYLE_STROKECOLOR] = 'black';
			organisationStyle[mxConstants.STYLE_FILLCOLOR] = 'orange';
			styles.putCellStyle('organisation', organisationStyle);


			edgeStyle = styles.getDefaultEdgeStyle();

			edgeStyle[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = '#fff';
			edgeStyle[mxConstants.STYLE_LABEL_PADDING] = '3';
			edgeStyle[mxConstants.STYLE_STROKEWIDTH] = '2';
			edgeStyle[mxConstants.STYLE_FONTCOLOR] = '#2278d0';
			edgeStyle[mxConstants.STYLE_ROUNDED] = true;

			familialEdgeStyle=mxUtils.clone(edgeStyle);
			familialEdgeStyle[mxConstants.STYLE_STROKECOLOR] = '#2278d0';
			styles.putCellStyle('FamilialRelationshipTypeVO', familialEdgeStyle);

			suggestionEdgeStyle=mxUtils.clone(edgeStyle);
			suggestionEdgeStyle[mxConstants.STYLE_DASHED] = '1';
			suggestionEdgeStyle[mxConstants.STYLE_FONTCOLOR] = '#ff6600';
			suggestionEdgeStyle[mxConstants.STYLE_STROKECOLOR] = '#FF6600';
			styles.putCellStyle('SUGGESTION', suggestionEdgeStyle);

			socialEdgeStyle=mxUtils.clone(edgeStyle);
			socialEdgeStyle[mxConstants.STYLE_STROKECOLOR] = '#48850e';
			styles.putCellStyle('SocialRelationshipTypeVO', socialEdgeStyle);

			professionalEdgeStyle=mxUtils.clone(edgeStyle);

			professionalEdgeStyle['whiteSpace'] = 'wrap';
			professionalEdgeStyle[mxConstants.STYLE_STROKECOLOR] = '#727272';
			styles.putCellStyle('ProfessionalRelationshipTypeVO', professionalEdgeStyle);

			secondaryEdgeStyle=mxUtils.clone(edgeStyle);
			secondaryEdgeStyle[mxConstants.STYLE_STROKECOLOR] = '#fae095';
			styles.putCellStyle('secondary', secondaryEdgeStyle);

			automatedEdgeStyle=mxUtils.clone(edgeStyle);
			automatedEdgeStyle[mxConstants.STYLE_STROKECOLOR] = '#D03722';
			styles.putCellStyle('automated', automatedEdgeStyle);

	    },
	    _executeLayout: function(fn){
	    	this.graph.popupMenuHandler.hideMenu();
	    	var instance=this,
	    	graph=instance.graph,
	    	model=graph.getModel(),
	    	layout, layoutName, key, menu,
	    	outline=instance.outline,
	    	keyEnabled = instance.get('keyEnabled');

	    	//turn off rendering
	    	graph.view.rendering = false;

	    	model.beginUpdate();

	    	var afterLoadCallback=instance.get('afterLoadCallbackFn');

            if(afterLoadCallback){
            	afterLoadCallback.apply(instance, []);
            }


	    	switch(instance.layout){
	    		case SWIMLANE_LAYOUT:
	    			layout=instance._getSwimLaneLayout();
	    			layoutName=SWIMLANE_LAYOUT_NAME;
	    		break;
	    		case HUB_AND_SPOKE_LAYOUT:
	    			layout=instance._getHubAndSpokeLayout();
	    			layoutName=HUB_AND_SPOKE_LAYOUT_NAME;
		    		break;
	    		case ORGANIC_LAYOUT:
	    			layout=instance._getOrganicLayout();
	    			layoutName=ORGANIC_LAYOUT_NAME;
		    		break;
	    		default:
	    			layout=instance._getHubAndSpokeLayout();
    			layoutName=HUB_AND_SPOKE_LAYOUT_NAME;
	    	}

			try{
				if (fn != null){
					fn.call(instance);
				}

				//clear layout
				//remove all cells edges and vertices
				graph.removeCells(model.getChildCells(model.getChildAt(model.getRoot(),0), true, true));

				//clear model
				model.clear();

				//merge in our swimlane cells to the graph
				model.mergeChildren(instance.ancestorModel.getRoot(), graph.getDefaultParent());

				if(layout instanceof mxSwimlaneLayout){
                    var removeCells=[];
                    A.each(model.getChildVertices(graph.getDefaultParent()),function(lane){
                    	A.each(model.getChildVertices(lane), function(cell){
                    		if(!cell.isVisible()){
                    			removeCells.push(cell);
                    		}else{
                    			this.updateCellSize(cell);
                    		}
                    	},this);
                    }, graph);
                    //remove cells
                    graph.removeCells(removeCells);
                    removeCells=[];
                    A.each(model.getChildVertices(graph.getDefaultParent()),function(lane){
                    	if (lane.getChildCount()===0) {
                    		removeCells.push(lane);
                    	}
                    });

                    //remove cells
                    graph.removeCells(removeCells);

                    //For each edge set appropriate geometry
                    A.each(model.getChildVertices(graph.getDefaultParent()),function(lane){
                    	//set appropriate geometry
                    	A.each(model.getChildCells(lane, false, true), function(edge){
                    		edge.geometry.x = 0;
                    		edge.geometry.y = 0;
                    	});
                    });

                    //finally execute the layout
                    layout.execute(graph.getDefaultParent(), model.getChildCells(graph.getDefaultParent(), true));
				}else{

					//remove cells from swimlanes and place in common parent
					var lanes=model.getChildCells(graph.getDefaultParent(), true);
					for(var i=0; i<lanes.length; i++){
						graph.removeCellsFromParent(model.getChildCells(lanes[i], true, true));

						model.remove(lanes[i]);
					};

					//set cell size for all vertices
					A.each(model.getChildVertices(graph.getDefaultParent()), function(cell){
						this.updateCellSize(cell);
					},graph);

					//For each edge set appropriate geometry
					// TODO:tidy up these labels.  The step stuff
					//		is there to prevent the labels overlapping
					//		but the result is a little scattershot
					var x=0.5;
					var step=0.05;
					A.each(model.getChildEdges(graph.getDefaultParent()),function(edge){
			    		edge.geometry.x = x;
			    		edge.geometry.y = 0;
			    		if (edge.style!=='secondary') {
			    			x=x-step;
				    		if (x<0.1) {
				    			step=-0.05;
				    		} else if(x>0.6){
				    			step=0.05;
				    		}
			    		}
					});
					layout.execute(graph.getDefaultParent(), instance._getRootCell());
				}


			}catch (e){
				throw e;
			}finally{
				graph.getModel().endUpdate();

				//fit the graph
				graph.fit();

				//turn back on rendering
				graph.view.rendering = true;

				//refresh the graph
				graph.refresh();


				if(outline){
					//update outline
					outline.update(true);
				}

	    	if(keyEnabled===true){
	    		//delete the graph key if already exists
	    		key = instance.keyContainer.one('div');
	    	  menu = instance.menuContainer.one('div');

          var rootCell = instance._getRootCell();

	    		if(key!=null) {
	    			key.remove(true);
	    		}

	    		if(!menu && rootCell) {
	    			menu = N.create(instance.menuTemplate({"layoutName": layoutName, "personName": rootCell.value.name, canAdd:instance.get('canAdd'), vanView:instance.get('canView')}));
			    	menu.appendTo(instance.menuContainer);
	    		}

		    	//add new node for graph key
          if(rootCell) {
            key=N.create(instance.keyTemplate({"layoutName": layoutName, "personName": rootCell.value.name}));
            key.appendTo(instance.keyContainer);
          }
	    	}
			}
	    },
	    _setGraphVisibility:function(){
	    	var instance=this,
	    	model=instance.ancestorModel,
	    	//get the root cell in the ancestor model
	    	rootCell=instance._getRootCell(model),
	    	lanes,
	    	edgesBetween,
	    	tweenVisibility,
	    	edgeVisibilityFn=instance.get('edgeVisibilityFn');

	    	//must have a root cell
	    	if(!rootCell){
				return;
			}

	    	//load the lanes and filter edges
	    	lanes=model.getChildCells(model.getRoot(), true);

	    	//must have lanes
	    	if(!lanes){
	    		return;
	    	}

	    	model.beginUpdate();
			try{
				//For each lane
				A.each(lanes,function(lane){
					//get the edges and execute visibility function
					A.each(model.getChildCells(lane, false, true), function(edge){
						//set visibility
						edge.setVisible(edgeVisibilityFn.apply(edge, [edge.value.classifier, rootCell]));
					});
				});


				//For each lane
				A.each(lanes,function(lane){
					//get the remaining cells and hide any that do not have a direct *visible* connection to the root cell
					A.each(model.getChildCells(lane, true, false), function(cell){
						if(cell===rootCell){
							cell.setVisible(true);
						}else{
							edgesBetween=model.getEdgesBetween(rootCell, cell, true);

							if(edgesBetween.length>0){
								//set visibility of target
								tweenVisibility=Y.some(edgesBetween, function(tweenEdge){
									return tweenEdge.isVisible();
								});
								cell.setVisible(tweenVisibility);
							}else{
								//hide as no direct relationship to root
								cell.setVisible(false);
							}
						}
					});
				});

			}finally{
				// Updates the display
				model.endUpdate();
			}
		}, _setBusyOverlay:function(show){
            var instance=this, boundingBox=instance.get('boundingBox'),timer=instance.loadingMaskTimer.timer;

            if(timer){
            	timer.cancel();
            }
            if(show){
                //disabled so apply mask and unbind click handlers
                boundingBox.busy.show();
            }else{
                //enabled - re-apply click handlers and remove mask
                boundingBox.busy.hide();
            }
        },_createPopupMenu:function( menu, cell, evt){
        	var instance=this, rootId=instance.rootCellId,
        	vertexMenuCallBack,edgeMenuCallBack, isRoot;

        	// Prevent right-click menu appearing
        	// a click on the drop-down arrow will have an name of 'click'.  Right-click's will not.

        	if (cell != null && evt && evt.name === 'click'){
				if(cell.isVertex() && cell.value && cell.value.id){

					vertexMenuCallBack=instance.get('vertexMenuCallbackFn');

					isRoot=(cell.value.id===rootId);



					if(vertexMenuCallBack){
						vertexMenuCallBack.apply(instance, [menu, cell.value, isRoot]);
					}
				}else if(cell.isEdge()){

					edgeMenuCallBack=instance.get('edgeMenuCallbackFn');

					if(edgeMenuCallBack){
						edgeMenuCallBack.apply(instance, [menu, cell.value, cell.source]);
					}
				}
			}
        },

        _createVertexCellOverlay:function(){
        	var instance=this,graph=instance.graph;

	        //create a new overlay for the vertex cells
        	vertexCellOverlay = new mxCellOverlay(new mxImage(mxClient.imageBasePath + '/dropdown.svg', 20, 20), 'Actions');
			vertexCellOverlay.cursor='hand';
			vertexCellOverlay.verticalAlign=mxConstants.ALIGN_TOP;
			vertexCellOverlay.offset=new mxPoint(-14, 12);

	        //install a handler for clicks on the overlay
			vertexCellOverlay.addListener('click', function(sender, evt){
				//the handler shows the popup menu
	            var cell = evt.getProperty('cell');
	            if (Y.UA.gecko > 0){
	            	graph.popupMenuHandler.popup(evt.getProperty("event").clientX, evt.getProperty("event").clientY, cell, evt);
	            }
	            else{
	            	if(mxClient.IS_TOUCH){
                        var xCoordinate = Number(Y.one(evt.properties.event.currentTarget).getXY()[0]);
                        var yCoordinate = Number(Y.one(evt.properties.event.currentTarget).getXY()[1]) + 20;
                        graph.popupMenuHandler.popup(xCoordinate, yCoordinate, cell, evt);
	            	}else{
	            		graph.popupMenuHandler.popup(evt.getProperty("event").pageX+(5*graph.view.scale), evt.getProperty("event").pageY+(5*graph.view.scale), cell, evt);
	            	}
	            }
	        });

			return vertexCellOverlay;
        },

      _createVertexIconOverlay: function(model, edgeCell) {

    	  var data, dob, icon, desc, ICON_PATH, EXT,rootData, lifeState, isNotCarriedToTerm,
	  		  vertexIconOverlay,
	  		  today = new Date(),
	  		  path = [],
	  		  root = this.get('data').get('people').getById(this.rootCellId),
	  		  isProfessional = false,
	  		  isAllocatedWorker = false,
	  		  isAdoptive = false;

    	  ICON_PATH = '/_gender/';
    	  EXT = '.svg';

    	  if(!model) { return; } 															//leave since we have nothing to work with


    	  data = model.toJSON();
    	  dob  = data.dateOfBirth;
    	  icon = (data.gender) ? data.gender.toLowerCase() : 'no-gender';
    	  desc = icon.charAt(0).toUpperCase() + icon.substr(1).toLowerCase();

    	  if(this.rootCellId === data.id && this.rootIsProfessional && !this.rootIsAllocatedWorker) {
    		isProfessional = true;
    		isAllocatedWorker = false;
    	  }

    	  // if vertex is the root && vertex has professional relationships....
    	  else if(this.rootCellId === data.id && this.rootIsProfessional && this.rootIsAllocatedWorker) {
    		isProfessional = true;
    		isAllocatedWorker = true;
    	  }

    	  else if(this.rootIsProfessional && this.rootCellId !== data.id && data.professionalRelationships.length > 0) {
    		rootData = root.toJSON();
    		if(rootData.professionalRelationships && rootData.professionalRelationships.length > 0) {
    			rootData.professionalRelationships.forEach(function(relationship) {
    				if(relationship.target===data.id) {
    					if(relationship.label === 'Allocated worker') {
    						isAllocatedWorker = true;
    						isProfessional = true;
    					}
    					else if(relationship.label === 'Psychologist' ||
    						relationship.label === 'GP' ||
    						relationship.label === 'Midwife' ||
							relationship.label === 'Psychiatrist' ||
    						relationship.label === 'Social worker' ||
    						relationship.label === 'District nurse' ||
    						relationship.label === 'Dentist' ||
    						relationship.label === 'Carer' ||
    						relationship.label === 'Foster carer' ||
    						relationship.label === 'Teacher' ||
    						relationship.label === 'Senco' ||
    						relationship.label === 'School Nurse' ||
    						relationship.label === 'Head teacher' ||
    						relationship.label === 'Health visitor' ||
    						relationship.label === 'Disability nurse' ||
    						relationship.label === 'Paediatrician')  {
    							isProfessional = true;
    					}
    				}
    			});
    		}
    	  }


    	  // if vertex isn't the root && vertex has professional relationships....
    	  else if(!this.rootIsProfessional && this.rootCellId !== data.id && data.professionalRelationships.length > 0) {
    		// ...determine they're prefessionally related to the root
    		rootData = root.toJSON();
    		if(rootData.professionalRelationships && rootData.professionalRelationships.length > 0) {
    			rootData.professionalRelationships.forEach(function(relationship) {
    				if(relationship.target===data.id) {
    					isProfessional=true;
    					if(relationship.label === 'Allocated worker') {
    						isAllocatedWorker = true;
    						isProfessional = true;
    					}
    				}
    			});
    		}
    	  }

    	// vertex has an adoptive relationship and they're the child of a parent-child relationship
    	  if(data.personalRelationships && data.personalRelationships.length > 0) {

			  data.personalRelationships.forEach(function(relationship) {
				 if(relationship.relationshipClassName==="Parent Child Relationship" && relationship.ancestorLevel===-1 && relationship.label.indexOf('Adoptive') !== -1) {
					 if(this.rootCellId === data.id || (edgeCell && edgeCell.style==="FamilialRelationshipTypeVO")) {
						 isProfessional = false;
						 isAllocatedWorker = false;
					 }

					 isAdoptive = true;
				 }
			 });

		 }

    	  lifeState = data.lifeState;
    	  isUnborn = lifeState == 'UNBORN'? true: false;
    	  isNotCarriedToTerm = lifeState == 'NOT_CARRIED_TO_TERM'? true: false;

    	  if(isProfessional && !isAllocatedWorker) {
    		desc = false;
    		ICON_PATH = '/_professional/';
    		icon = 'professional_worker';
    	  }

    	  else if(isProfessional && isAllocatedWorker) {
    		desc = false;
    		ICON_PATH = '/_professional/';
    		icon = 'allocated_worker';
    	  }

    	  else if(isAdoptive) {
    		  ICON_PATH = '/_family/';
    		  icon = 'adopted-' + data.gender.toLowerCase();
    	  }


    	  // if vertex is unborn and gender is determined through the value.
    	  else if(isUnborn){
    		  	ICON_PATH = '/_gender/';
         		icon = 'unborn-' +data.gender.toLowerCase();
         }

         // if vertex is unborn and not carried to term.
    	  else if(isNotCarriedToTerm){
    		  ICON_PATH = '/_gender/';
    		  icon = 'unborn-' + 'not-carried';
    	  }

    	  path.push(mxClient.imageBasePath);
    	  path.push(ICON_PATH);
    	  path.push(icon);
    	  path.push(EXT);

    	  //create a new overlay for the vertex icon
    	  vertexIconOverlay = new mxCellOverlay(new mxImage(path.join(''), 56, 54), desc);
    	  vertexIconOverlay.verticalAlign = mxConstants.ALIGN_MIDDLE;
    	  vertexIconOverlay.align = mxConstants.ALIGN_LEFT;
    	  vertexIconOverlay.offset = new mxPoint(35, 0);

    	  return vertexIconOverlay;

        },

        _createEdgeCellOverlay:function(){

        	var instance = this,
        		xBonus = 0,
        		graph = instance.graph;

	        //create a new overlay for the edge cells
			edgeCellOverlay = new mxCellOverlay(new mxImage(mxClient.imageBasePath + '/openedgemenu.svg', 20, 10), 'Actions');
			edgeCellOverlay.cursor='hand';
			edgeCellOverlay.defaultOverlap=0;
			edgeCellOverlay.verticalAlign=mxConstants.ALIGN_TOP;
			edgeCellOverlay.offset=new mxPoint(-14, 12);

	        edgeCellOverlay.getBounds = function(state) {
	        	var bounds = mxCellOverlay.prototype.getBounds.apply(this, arguments);

	        	if (state.view.graph.getModel().isEdge(state.cell) && state.text != undefined){

	        		// Automate relationships can have the dopdown icon close, because there is some additonal space.
	        		if(state.cell.value.automated) {
		        		xBonus = -5;
		        	} else {
		        		xBonus = 10;
		        	}
	        	    bounds.x = state.text.boundingBox.x + state.text.boundingBox.width - bounds.width + xBonus;
	    			bounds.y = state.text.boundingBox.y - 7;

	        	}
	        	return bounds;
	        };

	        //install a handler for clicks on the overlay
			edgeCellOverlay.addListener('click', function(sender, evt){
				//the handler shows the popup menu
	            var cell = evt.getProperty('cell');
	            if (Y.UA.gecko > 0){
	            	graph.popupMenuHandler.popup(evt.getProperty("event").clientX, evt.getProperty("event").clientY, cell, evt);
	            }else{
	            	if(mxClient.IS_TOUCH){
                        var xCoordinate = Number(Y.one(evt.properties.event.currentTarget).getXY()[0]);
                        var yCoordinate = Number(Y.one(evt.properties.event.currentTarget).getXY()[1]) + 20;
                        graph.popupMenuHandler.popup(xCoordinate, yCoordinate, cell, evt);
	            	} else {
	            		graph.popupMenuHandler.popup(evt.getProperty("event").clientX+(5*graph.view.scale), evt.getProperty("event").pageY+(5*graph.view.scale), cell, evt);
	            	}
	            }
	        });

			return edgeCellOverlay;
        },
        /**
         * Causes the graph to zoom in 1 step
         *
         * @method zoomIn
         *
         */
        zoomIn:function(){
        	var instance=this,graph=instance.graph;

	    	//close the menu
	    	graph.popupMenuHandler.hideMenu();

        	graph.zoomIn();
        },
        /**
         * Causes the graph to zoom out 1 step
         *
         * @method zoomOut
         *
         */
        zoomOut:function(){
        	var instance=this,graph=instance.graph;

	    	//close the menu
	    	graph.popupMenuHandler.hideMenu();

        	graph.zoomOut();
        },
        /**
         * Causes the graph key to display
         *
         * @method zoomIn
         *
         */
        showKey:function(){
        	var instance=this;
        	if (instance.get('keyEnabled')===true){
        		instance.keyContainer.show('fadeIn', {
                    easing: 'ease-in',
                    duration: 0.3
                });
        	}
        },
        /**
         * Causes the graph key to be hidden
         *
         * @method zoomIn
         *
         */
        hideKey:function(){
        	var instance=this;
        	if (instance.get('keyEnabled')===true){
        		instance.keyContainer.hide('fadeOut', {
                    easing: 'ease-out',
                    duration: 0.3
                });
        	}
        },
        /**
         * Causes the graph to redraw using the current layout
         *
         *  @method redraw
         */
        redraw:function(){
        	this.graph.popupMenuHandler.hideMenu();
	    	this._executeLayout();
        },
        /**
         * Causes the graph to redraw using the hub and spoke layout
         *
         *  @method redraw
         */
        redrawHubAndSpoke:function(){
        	if (this.layout!==HUB_AND_SPOKE_LAYOUT) {
        		this.layout=HUB_AND_SPOKE_LAYOUT;
        		this._loadAncestorModel(null);
            	//set graph visibility
    	    	this._setGraphVisibility();
        	}
        	this.redraw();
        },
        /**
         * Causes the graph to redraw using the swimlane layout
         *
         *  @method redraw
         */
        redrawSwimlane:function(){
        	if (this.layout!==SWIMLANE_LAYOUT) {
        		this.layout=SWIMLANE_LAYOUT;
        		this._loadAncestorModel(null);
            	//set graph visibility
    	    	this._setGraphVisibility();
        	}

        	this.redraw();
        },
        /**
         * Causes the graph to redraw using the organic layout
         *
         *  @method redraw
         */
        redrawOrganic:function(){
        	if (this.layout!==ORGANIC_LAYOUT) {
        		this.layout=ORGANIC_LAYOUT;
        		this._loadAncestorModel(null);
            	//set graph visibility
    	    	this._setGraphVisibility();
        	}

        	this.redraw();
        },
        /**
         * Refresh the graph, this can be used in conjunction with the
         * edgeVisibilityFn to apply filtering to the diagram
         *
         * @method filterGraph
         */
        filterGraph: function(){
        	var instance=this,graph=instance.graph;

        	//timer to start mask
        	if(instance.loadingMaskTimer.timer){
        		instance.loadingMaskTimer.timer.cancel();
        	}

        	//timer to start mask
        	instance.loadingMaskTimer.timer=Y.later(98, instance, instance._setBusyOverlay, [true], false);

	    	//turn off rendering
	    	graph.view.rendering = false;

        	//set visibility
        	instance._setGraphVisibility();

        	//load the new layout
			instance._executeLayout();

	    	//turn on rendering
	    	graph.view.rendering = true;

        	//hide any loading mask
        	instance._setBusyOverlay(false);
        },
        clearSelection:function(){
        	var instance=this, graph=instance.graph;

	    	//clear any selection
	    	graph.clearSelection();

        },
        print: function(){
        	var instance=this, graph=instance.graph,
        	preview = new mxPrintPreview(graph, 1);

        	preview.getCoverPages = function(w, h){
            	if (instance.get('keyEnabled')===true && instance.keyContainer.getStyle('display')!='none'){
    				return [this.renderPage(w, h, mxUtils.bind(this, function(div){
    					var printKeyHeader, printKeyContent, printkeyContainer=N.create('<div></div>');
    					printkeyContainer.setHTML(instance.keyContainer.getHTML());
    					printKeyHeader=printkeyContainer.one('.graph-key-header');
    					printKeyContent=printkeyContainer.one('.graph-key-content');

    					printkeyContainer.one('div').setStyle('border', 'solid 1px');
    					printkeyContainer.one('div').setStyle('margin', '2px');
    					printkeyContainer.one('div').setStyle('padding', '1px');

    					printKeyHeader.setStyle('background', '#000');
    					printKeyHeader.setStyle('color', '#fff');
    					printKeyHeader.setStyle('padding', '5px');

    					printKeyContent.setStyle('background', '#f2f2f2');
    					printKeyContent.setStyle('padding', '5px');
        				div.innerHTML = printkeyContainer.getHTML();
	        	    }))];
            	}
        	};

			preview.open();
        },_loadAncestorModel:function(data){

        	var instance = this,
    		model = instance.ancestorModel,
    		graph = instance.graph;

	    	if (!data) {
	    		data = instance.get('data');
	    	}

        	var rootId = data.get('rootId'),
				people = data.get('people'),
				organisations = data.get('organisations'),
				sourceCell,
				lanes,
				rootPerson,
				classifier,
				personalRelationshipsWithSuggestions,
				suggestions,
				key;


    		instance.rootIsAllocatedWorker = false;
        	instance.rootIsProfessional = false;
			model.beginUpdate();

			try
			{
				//clear model
				model.clear();

				//add the swim lanes
				instance._setupLanes(model);

				lanes=model.getChildCells(model.getRoot(), true);

				//find root person
				rootPerson=people.getById(rootId);

				//cache the rootCellId
				instance.rootCellId=rootId;

				//cache if the root cell is professional
				instance.rootIsProfessional = false;

				// determine if the root cell is a professional worker
				// if root has professional relationships...
				if(rootPerson.get('professionalRelationships').length > 0) {
					A.each(rootPerson.get('professionalRelationships'), function(relationship){
						if(relationship.label === 'Client of allocated worker') {
							instance.rootIsProfessional = true;
							instance.rootIsAllocatedWorker = true;
						}
						else if(relationship.label.indexOf('Patient of') !== -1 ||
								relationship.label.indexOf('Client of') !== -1 ||
								relationship.label.indexOf('Child of') !== -1 ||
								relationship.label.indexOf('Cared for') !== -1 ||
								relationship.label.indexOf('Pupil of') !== -1) {
							instance.rootIsAllocatedWorker = false;
							instance.rootIsProfessional = true;
						}
					});
				}

				//add root person to the model
				sourceCell=instance._createPerson(rootId, rootPerson, model);

				//add to lane
				model.add(lanes[DEFAULT_LANE], sourceCell);

	            //set the overlay for the sourceCell

				// add dropdown if user can add relationship
				if (this.get('canAdd')) {
					graph.addCellOverlay(sourceCell, instance.vertexCellOverlay);
				}
	            graph.addCellOverlay(sourceCell, instance._createVertexIconOverlay(people.getById(rootId)));

	            //check rootPersons suggestedRelationships and add to personalRelationships
	            personalRelationshipsWithSuggestions = rootPerson.get('personalRelationships');
	            suggestions = rootPerson.get('suggestedPersonalRelationships');

	            if(suggestions && suggestions.length > 0) {
	            	for(var i=0; i<suggestions.length;i++) {
	            		personalRelationshipsWithSuggestions.push(suggestions[i]);

	            	}
	            	rootPerson.set('suggestedPersonalRelationships', []);
	            }

	            //now for each relationship that the rootPerson has, add people and edges
				A.each(personalRelationshipsWithSuggestions, function(relationship){
					classifier=relationship.classifier;
					if (instance.layout==='HUB_AND_SPOKE') {
						instance._addPersonPrimaryRelationshipSingleNode(relationship, sourceCell, model, people, lanes);
					} else {
						instance._addPersonPrimaryRelationship(relationship, sourceCell, model, people, lanes, classifier);
					}
				});


				A.each(rootPerson.get('professionalRelationships'), function(relationship){
					classifier=relationship.classifier;

					if (instance.layout==='HUB_AND_SPOKE') {
						instance._addPersonPrimaryRelationshipSingleNode(relationship, sourceCell, model, people, lanes);
					} else {
						instance._addPersonPrimaryRelationship(relationship, sourceCell, model, people, lanes, classifier);
					}
				});

				A.each(rootPerson.get('organisationRelationships'), function(relationship){
					classifier=relationship.classifier;

					instance._addOrganisationRelationship(relationship, sourceCell, model, organisations, lanes, classifier);
				});

				//add secondary relationships
				people.each(function(person){
					//Already dealt with root
					if(person!==rootPerson){

						A.each(person.get('personalRelationships'), function(relationship){
							instance._addPersonSecondaryRelationship(relationship, person.get('id'), model, people, lanes);
						});
					}
				});

				// Check for suggested relationships
				people.each(function(person){
					//Already dealt with root
					if(person!==rootPerson){
						A.each(person.get('suggestedPersonalRelationships'), function(relationship){
							instance._addPersonSuggestedRelationship(relationship, person.get('id'), model, people, lanes);
						});
					}
				});
			}
			finally
			{
				// Updates the display
				model.endUpdate();
			}
        },

        _addPersonPrimaryRelationship:function(relationship, sourceCell, model, people, lanes, type){
            var instance = this,
	    		graph = instance.graph,
	    		targetId = relationship.target, 		//get the target id
	    		target = people.getById(targetId),		//locate in the model list
	    		ancestorLevel = Number(relationship.ancestorLevel||0), //get the ancestor level (differential)
	    		targetCell,
	    		edgeCell;

			//add a new Person vertex
			targetCell=instance._createPerson(target.get('id'), target, model);

			//add to model
			model.add(lanes[DEFAULT_LANE+ancestorLevel],targetCell);

			//create edges and add to model
			edgeCell = instance._createRelationship(relationship.relationshipId, relationship, sourceCell, targetCell, type);


			model.add(lanes[DEFAULT_LANE],edgeCell);

	        //set the overlay for the targetCell and edgeCell
	        graph.addCellOverlay(targetCell, instance.vertexCellOverlay);
	        graph.addCellOverlay(edgeCell, instance.edgeCellOverlay);
	        graph.addCellOverlay(targetCell, instance._createVertexIconOverlay(target, edgeCell));



        },_addPersonPrimaryRelationshipSingleNode:function(relationship, sourceCell, model, people, lanes){

        	var makeId=function(id){
        		return PERSON_PREFIX+id;
        	};

        	//get the target id
        	var instance=this, graph=instance.graph,
        	targetId=relationship.target,
			//locate in the model list
			target=people.getById(targetId),
			//get the ancestor level (differential)
			ancestorLevel=Number(relationship.ancestorLevel||0),
			targetCell,
			edgeCell;
			var edge=instance._getCellForPerson(targetId, model);


			if (edge != undefined){

        		// already a relationship drawn between
				// these two people
				// so add another
        		targetId=makeId(targetId);
        		targetCell=model.getCell(targetId);

        		edgeCell=instance._createRelationship(relationship.relationshipId, relationship, sourceCell, targetCell);
                model.add(sourceCell.getParent(),edgeCell);
    			graph.addCellOverlay(edgeCell, instance.edgeCellOverlay);
        	}
        	else{
        		// no relationship thus far, so need to create a person and add relationship
        		sourceId=sourceCell.id;
            	targetCell=instance._createPerson(targetId, target, model);
        		model.add(lanes[DEFAULT_LANE+ancestorLevel],targetCell);



        		edgeCell=instance._createRelationship(relationship.relationshipId, relationship, sourceCell, targetCell, relationship.classifier);

    			model.add(lanes[DEFAULT_LANE],edgeCell);
    			graph.addCellOverlay(targetCell, instance.vertexCellOverlay);
    	        graph.addCellOverlay(edgeCell, instance.edgeCellOverlay);
    	        graph.addCellOverlay(targetCell, instance._createVertexIconOverlay(people.getById(targetId)));


        	}

        },_addPersonSecondaryRelationship:function(relationship, sourceId, model, people, lanes){
        	//find the source of the relationship
        	var instance=this, graph=instance.graph,
        	sourceCell,edgeCell,
        	targetId=relationship.target,targetCell,
			makeId=function(id){
        		return PERSON_PREFIX+id;
        	};

        	//check whether this relationship is already represented by an edge on the diagram
        	var edge=instance._getEdgeForRelationship(relationship.relationshipId, model);

        	if (edge != undefined && edge.target){
        		//yes it is, so use it to determine the source/target ids for this secondary view of the relationship.
        		//doing it this way ensures that we use the correct person node in cases where the same person
        		//is represented more than once on the diagram
        		sourceId = edge.target.getId();
        		targetId = edge.source.getId();
        	} else{
            	//the source cannot be the root cell, but the target could be
            	sourceId=makeId(sourceId);
            	targetId=makeId(targetId);
        	}



    		sourceCell=model.getCell(sourceId);
        	targetCell=model.getCell(targetId);

        	if(!sourceCell || !targetCell) {
        		Y.log('relationshipId:', relationship.relationshipId,'  sourceId:', sourceId ,'  targetId:', targetId);
        	}
        	else {
	        	//create edges and add to model
	        	edgeCell=instance._createRelationship(relationship.relationshipId, relationship, sourceCell, targetCell, 'secondary');
	        	model.add(sourceCell.getParent(),edgeCell);

	        	//set the overlay for the targetCell and edgeCell
				graph.addCellOverlay(edgeCell, instance.edgeCellOverlay);
        	}

        },

        _addPersonSuggestedRelationship:function(relationship, sourceId, model, people, lanes){
        	//find the source of the relationship
        	var instance = this,
        		graph = instance.graph,
        		sourceCell,edgeCell,
        		targetId = relationship.target, targetCell,
				makeId = function(id){
	        		return PERSON_PREFIX+id;
	        	};

        	//check whether this relationship is already represented by an edge on the diagram

        	var edge = instance._getEdgeForRelationship(relationship.relationshipId, model);

        	if(!edge) {

        		sourceCell=model.getCell(makeId(sourceId));
            	targetCell=model.getCell(makeId(targetId));

            	edgeCell=instance._createRelationship(relationship.relationshipId, relationship, sourceCell, targetCell, 'SUGGESTION');

            	//edgeCell.setAttribute('stroke-dasharray', 5, 2);
            	model.add(sourceCell.getParent(), edgeCell);

                //set the overlay for the targetCell and edgeCell
    			graph.addCellOverlay(edgeCell, instance.edgeCellOverlay);
        	}


        },




        _addOrganisationRelationship:function(relationship, sourceCell, model, organisations, lanes, type){
        	//get the target id
        	var instance=this,
        	targetId=relationship.target,
			//locate in the model list
			target=organisations.getById(targetId),
			//get the ancestor level (differential)
			ancestorLevel=Number(relationship.ancestorLevel||0),
			targetCell;

			//add a new Person vertex
			targetCell=instance._createOrganisation(target.get('id')+'_'+relationship.relationshipId, target);

			//add to model
			model.add(lanes[DEFAULT_LANE+ancestorLevel],targetCell);

			//create edges and add to model
			model.add(lanes[DEFAULT_LANE],instance._createRelationship(relationship.relationshipId, relationship, sourceCell, targetCell, type));
        },_getSwimLaneLayout:function(){
        	var instance=this, graph=instance.graph,swimlaneLayout=new mxSwimlaneLayout(graph, mxConstants.DIRECTION_WEST, false);
			//configure the layout

			//resize parent element
			swimlaneLayout.resizeParent=true;

			//swimlaneLayout.swimlanes=lanes;
			swimlaneLayout.parallelEdgeSpacing=20;
			swimlaneLayout.tightenToSource=true;

			//Whether or not to drill into child cells and layout in reverse group order.
			//This also cause the layout to navigate edges whose terminal vertices
			//* have different parents but are in the same ancestry chain
			swimlaneLayout.traverseAncestors=true;

			return swimlaneLayout;
        },_getHubAndSpokeLayout:function(){
        	var instance=this, graph=instance.graph,
        	hubAndSpokeLayout = new mxHubAndSpokeLayout(graph),
        	parallelEdgeLayout=new mxParallelEdgeLayout(graph),
        	compositeLayout=new mxCompositeLayout(graph, [hubAndSpokeLayout, parallelEdgeLayout], hubAndSpokeLayout);

        	//configure the layout

        	//set parallel edge handling
        	parallelEdgeLayout.spacing=45;

        	return compositeLayout;
        },_getOrganicLayout:function(){
        	var instance=this, graph=instance.graph,
        	organicLayout = new mxFastOrganicLayout(graph),
        	parallelEdgeLayout=new mxParallelEdgeLayout(graph),
        	compositeLayout=new mxCompositeLayout(graph, [organicLayout, parallelEdgeLayout], organicLayout);

        	//configure the layout

        	organicLayout.radius=200;
        	organicLayout.minDistanceLimit=2;
        	organicLayout.forceConstant=600;

        	//set parallel edge handling
        	parallelEdgeLayout.spacing=45;

        	return compositeLayout;
        },_setupLanes:function(model){
        	var instance=this, lanes=[7],parent=model.getRoot();
        	model.beginUpdate();
			try
			{
				//setup lanes
				lanes[0] = instance._createVertex(null, 'Level -3', 110, 0, 'swimlane', false);
				lanes[1] = instance._createVertex(null, 'Level -2', 110, 0, 'swimlane', false);
				lanes[2] = instance._createVertex(null, 'Level -1', 110, 0, 'swimlane', false);
				lanes[3] = instance._createVertex(null, 'Level 0', 110, 0, 'swimlane', false);
				lanes[4] = instance._createVertex(null, 'Level 1', 110, 0, 'swimlane', false);
				lanes[5] = instance._createVertex(null, 'Level 2', 110, 0, 'swimlane', false);
				lanes[6] = instance._createVertex(null, 'Level 3', 110, 0, 'swimlane', false);

				//add these lanes to the model parent
				A.each(lanes, function(lane){
					model.add(parent,lane);
				});
			}finally{
				//finish updates
				model.endUpdate();
			}

			//return the newly added lanes
			return lanes;
        },_createCell:function(id, value, width, height, style){
        	var geo=new mxGeometry(0, 0, width, height);
            var cell = new mxCell(value, geo, style);
            cell.setId(id);
            return cell;
        },_createVertex:function(id, value, width, height, style,  connectable){
        	var vertex=this._createCell(id, value, width, height, style);
            vertex.setVertex(true);
        	vertex.setConnectable(connectable);

            return vertex;
        },

        _checkAutomated: function(autoData, id) {

        	var style = '';

        	for(var i=0; i<autoData.length; i++) {
    			if(id===RELATIONSHIP_PREFIX + autoData[i]) {
    				style='automated';
    			}
    		}

        	return style;
        },

        _createEdge:function(id, value, source, target, style){
        	var edge;
        	  	_instance = this,
        	  	autoData = _instance.get('automatedData');

        	// check for automated relationships and change their edge style
    	  	if(autoData && autoData.length > 0 && style!=='secondary') {
    	  		style = _instance._checkAutomated(autoData, id) || style;
        	}

        	edge = this._createCell(id, value, 0, 0, style);
            edge.setEdge(true);
            edge.geometry.relative=true;


            // Give professional relationships a width - this will wrap any long text.
            // We can only do it for professional relationships safely because there are no
            // secondary relationships to show (wrapped text might overlap each other in
            // certain instances when secondaries are showing.)
            if(style==='ProfessionalRelationshipTypeVO') {
            	edge.geometry.width = '70';
            }

            edge.source=source;
            edge.target=target;
            return edge;
        },

        _createPerson: function(id, person, model) {

        	var instance = this,
        		pId=PERSON_PREFIX+id,
        		n=0,
        		today = new Date(),
        		data = person.toJSON(),
        		dob = data.dateOfBirth,
        		rootId = instance.rootCellId,
        		style = (data.gender) ? data.gender.toLowerCase() : 'no-gender',
        		root = this.get('data').get('people').getById(this.rootCellId),
  	  		  	isProfessional = false,
  	  		  	isAllocatedWorker = false;



        	while(model.getCell(pId)){
        		pId=pId+"_"+n;
        		n++;
        	}
        	if(data.id===rootId) {
         		style='root';
        	} else if(style==='unknown' || dob && dob > today) {					// Use the default svg person style if gender is unknow or they are unborn
        		style='person';
        	}

        	// set the style to default for the person in professional role

        	if(style !== 'root'){

        		if(this.rootCellId === data.id && this.rootIsProfessional && !this.rootIsAllocatedWorker) {
            		isProfessional = true;
            		isAllocatedWorker = false;
            	  }
            	  // if vertex is the root && vertex has professional relationships....
            	  else if(this.rootCellId === data.id && this.rootIsProfessional && this.rootIsAllocatedWorker) {
            		isProfessional = true;
            		isAllocatedWorker = true;
            	  }
            	  else if(this.rootIsProfessional && this.rootCellId !== data.id && data.professionalRelationships.length > 0) {
            		rootData = root.toJSON();
            		if(rootData.professionalRelationships && rootData.professionalRelationships.length > 0) {
            			rootData.professionalRelationships.forEach(function(relationship) {
            				if(relationship.target===data.id) {
            					if(relationship.label === 'Allocated worker') {
            						isAllocatedWorker = true;
            						isProfessional = true;
            					}
            					else if(relationship.label === 'Psychologist' ||
            						relationship.label === 'GP' ||
            						relationship.label === 'Midwife' ||
        							relationship.label === 'Psychiatrist' ||
            						relationship.label === 'Social worker' ||
            						relationship.label === 'District nurse' ||
            						relationship.label === 'Dentist' ||
            						relationship.label === 'Carer' ||
            						relationship.label === 'Foster carer' ||
            						relationship.label === 'Teacher' ||
            						relationship.label === 'Senco' ||
            						relationship.label === 'School Nurse' ||
            						relationship.label === 'Head teacher' ||
            						relationship.label === 'Health visitor' ||
            						relationship.label === 'Disability nurse' ||
            						relationship.label === 'Paediatrician')  {
            							isProfessional = true;
            					}
            				}
            			});
            		}
            	  }
            	  // if vertex isn't the root && vertex has professional relationships....
            	  else if(!this.rootIsProfessional && this.rootCellId !== data.id && data.professionalRelationships.length > 0) {
            		// ...determine they're prefessionally related to the root
            		rootData = root.toJSON();
            		if(rootData.professionalRelationships && rootData.professionalRelationships.length > 0) {
            			rootData.professionalRelationships.forEach(function(relationship) {
            				if(relationship.target===data.id) {
            					isProfessional=true;
            					if(relationship.label === 'Allocated worker') {
            						isAllocatedWorker = true;
            						isProfessional = true;
            					}
            				}
            			});
            		}
            	  }

            	if(isProfessional){
    				style='person';
    			}

        	}

        	// end

	        return this._createVertex(pId, data, 300, 60, style, true);
        },
        _createOrganisation:function(id, organisation){
        	return this._createVertex(ORGANISATION_PREFIX+id, organisation.toJSON(), 250, 80, 'organisation', true);
        },
        _createRelationship:function(id, relationship, source, target, type){
        	return this._createEdge(RELATIONSHIP_PREFIX+id, relationship, source, target,  type);
        },
        _getEdgeForRelationship:function(relationshipId, model){
        	var instance = this,
        			model = model || instance.graph.getModel();
        	return model.getCell(RELATIONSHIP_PREFIX+relationshipId);
        },
        _getCellForPerson:function(personId, model){
        	var instance = this,
        			model = model || instance.graph.getModel();
        	return model.getCell(PERSON_PREFIX+personId);
        },
        _getRootCell:function(model){
        	var instance = this,
        			model = model||instance.graph.getModel(),
        			rootCell = null;
        	//assumption - person is always root
        	rootCell=model.getCell(PERSON_PREFIX+instance.rootCellId);

        	return rootCell;
        }
	},{
		CSS_PREFIX: "usp-graph",

		ATTRS: {
			/**
			* Configure the render mode, possible options are:
			* ['fast','faster','fastest','exact']
			*
			* @property renderMode
			* @type {String}
			* @default "fastest"
			*/
			renderMode:{
				value:'fastest'
			},
			/**
			* Allows graph edits (i.e. drag & drop)
			*
			* @property enabledEdit
			* @type {Boolean}
			* @default "true"
			*/
			enabledEdit:{
				value:true
			},
			data:{
				valueFn : function() {
					return new Y.usp.relationships.RelationshipsModel();
				}
			},
			/**
			* Enable graph animation
			*
			* ANIMATION IS DISABLED CURRENTLY TO SOLVE RE-SIZE PROBLEMS
			* @property animate
			* @type {Boolean}
			* @default "false"
			*/
			animate:{
				value:false
			},
			edgeVisibilityFn:{
				value:function(){
					//default to showing all edges
					return true;
				}
			},
			/**
			* Enable the outline graph preview window
			*
			* @property showOutline
			* @type {Boolean}
			* @default "true"
			*/
			showOutline:{
				value:false
			},
			/**
			* Enable the graph key panel
			*
			* @property keyEnabled
			* @type {Boolean}
			* @default "true"
			*/
			keyEnabled:{
				value:true
			},
			/**
			 * Template for rendering nodes in the graph
			 *
			 * This will be compiled using the MicroTemplate engine
			 *
			 * @property template
			 * @type {String}
			 * @default null
			 */
			template:{

			},
			/**
			 * Template for rendering edges in the graph
			 *
			 * This will be compiled using the MicroTemplate engine
			 *
			 * @property edgeTemplate
			 * @type {String}
			 * @default null
			 */
			edgeTemplate:{

			},
			/**
			 * Template for rendering the key to the graph
			 *
			 * This will be compiled using the MicroTemplate engine
			 *
			 * @property keyTemplate
			 * @type {String}
			 * @default null
			 */
			keyTemplate:{

			},
			/**
			 * Template for rendering the graph menu
			 * This will be compiled using the MicroTemplate engine
			 *
			 * @property menuTemplate
			 * @type {String}
			 * @default null
			 */
			menuTemplate:{

			},

			/**
			 * Array for holding relationships ID's that have been automated
			 *
			 * @property menuTemplate
			 * @type {array}
			 * @default null
			 */
			automatedData:{
			  value:null
			},

			/**
			 * Message for server error line 1
			 *
			 * @property msgServerErrorLine1
			 * @type {String}
			 * @default null
			 */
			msgServerErrorLine1:{
			  value:null
			},

			/**
			 * Message for server error line2
			 *
			 * @property msgServerErrorLine2
			 * @type {String}
			 * @default null
			 */
			msgServerErrorLine2:{
			  value:null
			},

			/**
			 * Vertex menu options
			 *
			 * This is a callback function to render Menu options
			 * for Cells visible on the graph
			 *
			 * The function has the following parameters
			 * 1. menu (The menu object, use menu.addItem to add menu item)
			 * 2. value (Cell.value)
			 * 3. isRoot (boolean)
			 *
			 * @property vertexMenuCallbackFn
			 * @type {function}
			 * @default null
			 */
			vertexMenuCallbackFn:{
				value:null
			},

			/**
			 * Edge menu options
			 *
			 * This is a callback function to render Menu options
			 * for Edges visible on the graph
			 *
			 * The function has the following parameters
			 * 1. menu (The menu object, use menu.addItem to add menu item)
			 * 2. value (Cell.value)
			 * 3. source (Edge source)
			 *
			 * @property edgeMenuCallbackFn
			 * @type {function}
			 * @default null
			 */
			edgeMenuCallbackFn:{
				value:null
			},
            afterLoadCallbackFn:{
                value:null
            },
            canView: {
            	value:false
            },
            canAdd: {
              value:false
            }
      }
	});
}, '0.0.1', {
    requires : [ 'yui-base',
                 'widget-base',
                 'template',
                 'template-micro',
                 'array-extras',
                 'person-relationship-model',
                 'gallery-busyoverlay',
                 'yui-later',
                 'usp-date',
                 'event-delegate',
                 'escape']
});
