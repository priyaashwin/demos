// Defines a YUI module which will hold the template source for our view.
YUI.add('nodeTemplate', function(Y) {
    Y.namespace('RelationshipRecording').nodeTemplate = Y.Template.Micro.compile(
        '<div class="node">\
               <div class="node-header">\
                 <div class="<%= this.isDeceased===true ? "deceased" : "" %>">\
                   <h3 tabindex="0"><i class="fa fa-user"></i> <%= this.name %> (<%= this.personId %>)</h3>\
                 </div>\
                 <div class="node-secondary">\
                   <span class="<%= this.dateOfBirthEstimated===true ? "estimated graph" : "" %>">\
                    <%=Y.USPDate.formatDateValue(this.dateOfBirth, this.fuzzyDateMask)%>\
                   </span>\
    			   <span><%=this.gender%> <%=this.age%> <%=this.age>1 ? " years " : " year " %> </span>\
                 </div>\
              </div>\
            </div>'
    );

    Y.namespace('RelationshipRecording').nodeTemplateLrg = Y.Template.Micro.compile(
        '<div class="pure-g node <%= this.gender==="MALE" ? "gender-male" : "" %> <%= this.gender==="FEMALE"? "gender-female" : "" %> <%= this.isProfessional===true ? "professional" : "" %>">\
            <div class="pure-u-1-4">&nbsp;</div>\
    			<div class="pure-u-3-4 node-header">\
    				<h3 tabindex="0"><%= this.name.length>30 ===true? this.name.slice(0,30)+"...":""   %><%= this.name.length<30 ===true? this.name:"" %> (<%= this.personId %>)</h3>\
                	<div class="node-secondary">\
    					<% if("UNBORN" === this.lifeState){ %>\
    						<span class="unborn graph">\
            				<%=Y.USPDate.formatDateValue(this.dueDate, this.fuzzyDateMask)%>\
            				</span>\
    						<span>(Unborn)</span><br/>\
    					<% }else if(("ALIVE" === this.lifeState  || this.lifeState == null) && this.dateOfBirth != null){ %>\
			    			<span class="<%= this.dateOfBirthEstimated===true ? "estimated graph" : "" %>">\
                        		<%=Y.USPDate.formatDateValue(this.dateOfBirth, this.fuzzyDateMask)%>\
    						</span>\
			    			<span> (<%=this.age%> <%=this.age>1 ? " years" : " year" %>)</span><br/>\
    						<span><%= this.ethnicity!=null ? Y.uspCategory.person.ethnicity.category.codedEntries[this.ethnicity].name : "" %></span>\
    		 			<% }else if("DECEASED" === this.lifeState) {%>\
    						<span class="<%= this.diedDateEstimated===true ? "deceased-estimated graph" : "deceased graph" %>">\
    							<%=Y.USPDate.formatDateValue(this.dateOfBirth, this.fuzzyDiedDateMask)%>\
    						</span>\
    						<%if(this.age >= 0){%>\
    							<span> (<%=this.age%> <%=this.age>1 ? " years" : " year" %>)</span><br/>\
    						<% }%>\
    					<% }else if("NOT_CARRIED_TO_TERM" === this.lifeState) {%>\
							<span>Not carried</span><br/>\
						<% }%>\
    					<span class="<%= this.numberOfWarnings>0? "show-warnings":"hide-warnings" %>">\
                        <span class="label label-small warning" aria-label="This person has <%= this.numberOfWarnings %> warnings"><i class="fa fa-exclamation-triangle"></i> <%= this.numberOfWarnings %></span>\
                    </span>\
                </div>\
             </div>\
          </div>'
    );
}, '0.0.1', {
    requires: ['template-micro', 'usp-date']
});