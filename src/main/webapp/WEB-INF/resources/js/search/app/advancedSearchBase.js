YUI.add('app-advanced-search-base', function (Y) {
    var L = Y.Lang;

    var hasCriteria = function (searchTerms) {
        return Object.values(searchTerms).some(function (v) {
            return !(v === undefined || v === null || v === '' || (Array.isArray(v) && v.length === 0));
        });
    };
    Y.namespace('app.search').CriteriaView = Y.Base.create('criteriaView', Y.app.AppSearchResults, [], {
        /**
         * The type of Filter Model to be created in the criteria view
         *
         * @property filterModelType
         * @type {Object}
         */
        filterModelType: null,
        /**
         * The type of Filter to be created in the criteria view
         *
         * @property filterType
         * @type {Object}
         */
        filterType: null,
        initializer: function (config) {
            var resultsConfig = config.resultsConfig || {};

            var filterModelConstructor = this.filterModelType;
            var filterConstructor = this.filterType;

            //construct filter model
            this.filterModel = new filterModelConstructor();

            this.advancedSearchFilter = new filterConstructor({
                filterModel: this.filterModel,
                filterConfig: resultsConfig.filterConfig,
                filterContextConfig: resultsConfig.filterContextConfig
            });

            this.after('searchTermsChange', this.handleSearchTermsChange, this);
        },
        handleSearchTermsChange: function (e) {
            var terms = e.newVal;

            //push those terms into our filter model
            this.filterModel.setAttrs(terms);

            //fire event to cause context to re-render
            this.filterModel.fire('filter:init');

            if (hasCriteria(terms)) {
                //hide filter as terms are present
                this.hideFilter();
                this.showResults();
            } else {
                //show filter as there are no search terms specified by the user
                this.showFilter();
                this.hideResults();
            }
        },
        destructor: function () {
            this.advancedSearchFilter.destroy();
            delete this.advancedSearchFilter;
        },
        render: function () {
            var container = this.get('container');

            Y.app.search.CriteriaView.superclass.render.call(this);

            //insert filter as the first child
            container.insertBefore(this.advancedSearchFilter.render().get('container'), container.one('div'));
            return this;
        },
        handleClear: function () {
            //just reset the filter model
            this.filterModel.reset();
            //re-render the filter context - via reset event
            this.filterModel.fire('filter:reset');

            //ensure the filter is shown (as now empty)
            this.showFilter();
            //hide the results table
            this.hideResults();
        },
        handleSearch: function () {
            var data = this.filterModel.toJSON();
            this.fire('performSearch', {
                filterData: data
            });
            //re-render the filter context - via search event
            this.filterModel.fire('filter:search');
        },
        hideResults: function () {
            this._tablePanel.get('container').hide();
            //empty table of results
            this.results.get('data').reset();
            //clear paginator model
            this.results.get('paginator').get('model').set('totalItems', 0);
            //force re-render of results count node
            this.results._handleAfterDataLoad();
        },
        showResults: function () {
            this._tablePanel.get('container').show();
        },
        showFilter: function () {
            this.advancedSearchFilter.set('showFilter', true);
        },
        hideFilter: function () {
            this.advancedSearchFilter.set('showFilter', false);
        }
    }, {
        ATTRS: {
            /**
             * Holds data to drive which fields are visible in the filter view
             */
            searchContext: {
                value: {}
            }
        }
    });

    Y.namespace('app.search').AdvancedSearchView = Y.Base.create('advancedSearchView', Y.app.AppSearchView, [], {
        viewConfig: {
            searchResults: {
                buttons: [{
                    name: 'clearButton',
                    labelHTML: 'Clear all criteria',
                    classNames: 'pull-left',
                    isText: true,
                    action: function () {
                        this.get('activeView').handleClear();
                    },
                    disabled: false
                }, {
                    name: 'searchButton',
                    type: 'submit',
                    labelHTML: '<i class="fa fa-search"></i> search',
                    action: function () {
                        this.get('activeView').handleSearch();
                    },
                    disabled: false
                }]
            }
        },
        initializer: function () {
            this.on('*:recordSelect', function (e) {
                this.handleSelect(e.record.get('id'));
            }, this);
            this.on('*:performSearch', function (e) {
                this.handleSearch(e.filterData);
            }, this);
        },
        handleSelect: function (e) {},
        handleSearch: function (filterData) {
            var data = filterData || {};
            var searchTerms = {};
            var searchView = this.searchResultsDialog.get('activeView');
            var searchConfig = this.get('searchConfig');

            //iterate over the whitelist
            this.whiteList.forEach(function (key) {
                if (data.hasOwnProperty(key)) {
                    searchTerms[key] = data[key];
                }
            });

            //find if there are any criteria
            var isSearchTerms = hasCriteria(searchTerms);
            if (isSearchTerms) {
                var url = L.sub(searchConfig.url, searchTerms);

                var results = searchView.results;

                //set the query string
                results.get('data').url = url;

                //set the state from saved data
                results.resultsTableSearchState.applyState();
                //reload the results - only reset if we are not returning from history

                results.reload({
                    reset: true
                });
            }

            //set the search terms into the view
            searchView.set('searchTerms', searchTerms);
        }
    }, {
        ATTRS: {
            searchConfig: {
                value: {}
            }
        }
    });

    Y.namespace('app.search').AdvancedSearchFilterContext = Y.Base.create('advancedSearchFilterContext', Y.View, [], {
        initializer: function () {
            var model = this.get('model');
            //render after visibility change
            //or model is reset of search executed
            this._eventHandlers = [
                this.after('showFilterChange', this.render, this),
                model.after(['filter:search', 'filter:reset', 'filter:init'], this.render, this)
            ];
        },
        destructor: function () {
            //unbind event handles
            this._eventHandlers.forEach(function (handle) {
                handle.detach();
            });

            delete this._eventHandlers;
        },
        render: function () {
            var container = this.get('container'),
                model = this.get('model'),
                data = model.toJSON(),
                html = '';

            //only display context if there are criteria - or the filter is hidden
            if (hasCriteria(data) || !this.get('showFilter')) {
                html = this.template({
                    activeFilter: this.getActiveFilter(data),
                    labels: this.get('labels'),
                    filterVisible: this.get('showFilter'),
                    modelData: data,
                    codedEntries: this.get('codedEntries'),
                    enumTypes: this.get('enumTypes'),
                });
            }
            //set the filter node into the container
            container.setHTML(html);
            return this;
        },
        getActiveFilter: function (data) {
            var active = {};

            Object.keys(data).forEach(function (k) {
                var v = data[k];
                if (!(v === null || v === undefined || v === '' || v === false || (Array.isArray(v) && v.length === 0))) {
                    active[k] = true;
                }
            });
            return active;
        }
    }, {
        ATTRS: {
            showFilter: {}
        }
    });

    
    Y.namespace('app.search').AdvancedSearchFilterView = Y.Base.create('advancedSearchFilterView', Y.usp.app.ResultsFilter, [], {
    	initializer: function (config) {
            //set the search context into the "otherData" attribute
            //so it is available in the template when rendering
            this.set('otherData', {
                searchContext: this.get('searchContext')
            });
        },
    }, {
        ATTRS: {
            /**
             * Filter is shown by default
             */
            visible: {
                value: true
            },
            searchTerms: {
                value: {}
            }
        }
    });

    Y.namespace('app.search').TabbedAdvancedSearchFilterView = Y.Base.create('tabbedAdvancedSearchFilterView', Y.usp.app.ResultsFilter, [], {
        initializer: function (config) {
            //set the search context into the "otherData" attribute
            //so it is available in the template when rendering
            this.set('otherData', {
                searchContext: this.get('searchContext')
            });
        },
        render: function () {
            var container = this.get('container');

            Y.app.search.TabbedAdvancedSearchFilterView.superclass.render.call(this);

            this.filterTabView = new Y.TabView({
                srcNode: container.one('#advancedSearchFilterTabsSrc'),
                render: container.one('#advancedSearchFilterTabs')
            });
            return this;
        },
        destructor: function () {
            //unbind event handles

            if (this.filterTabView) {
                this.filterTabView.destroy();

                delete this.filterTabView;
            }
        }
    }, {
        ATTRS: {
            /**
             * Filter is shown by default
             */
            visible: {
                value: true
            },
            searchTerms: {
                value: {}
            }
        }
    });

    Y.namespace('app.search').AdvancedSearchFilter = Y.Base.create('advancedSearchFilter', Y.usp.app.Filter, [], {
        events: {
            '.search-toggle': {
                click: 'handleToggleFilter'
            }
        },
        initializer: function () {
            this.get('filterContext').set('showFilter', this.get('showFilter'));

            this._advancedSearchEvts = [
                this.after('showFilterChange', this.handleShowFilterChange, this)
            ];
        },
        destructor: function () {
            //unbind event handles
            this._advancedSearchEvts.forEach(function (handle) {
                handle.detach();
            });

            delete this._advancedSearchEvts;
        },
        /**
         * A event handler for the showFilterChange event that keeps
         * the filterContext visibility attribute in sync with the 
         * UI to drive the criteria expand/collapse functionality
         * @method handleShowFilterChange 
         */
        handleShowFilterChange: function (e) {
            this.get('filterContext').set('showFilter', e.newVal);
        },
        handleToggleFilter: function (e) {
            e.preventDefault();

            this.set('showFilter', !this.get('showFilter'));
        }
    }, {
        ATTRS: {
            /**
             * Filter is shown by default
             */
            showFilter: {
                value: true
            },
            /**
             * There is no toolbar node for the advanced search
             */
            toolbarNode: {
                value: null
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-search',
        'usp-session-storage',
        'view',
        'tabview',
        'data-binding',
        'app-filter',
        'results-filter'
    ]
});