YUI.add('person-app-search', function (Y) {
    var padTo3Digits = function (v) {
        return (v || '').padEnd(3, '').substring(0, 3);
    };

    var padTo4Digits = function (v) {
        return (v || '').padEnd(3, '').substring(0, 4);
    };

    var L = Y.Lang;

    var TEMPLATE_REFERENCE_NUMBER = ' <span>{name}</span><br/><span>{value}</span>';

  // The below AOP exists because the results is coming from a lucene search
  // of which is forcing us to explicitly setup a locator for the dateOfBirth field
  Y.Do.after(function() {
    var schema = Y.Do.originalRetVal;
    schema.resultFields = schema.resultFields.filter(function(data){
        return data !== 'dateOfBirth';
    });
    schema.resultFields.push({
      key: 'dateOfBirth',
      locator: 'dateOfBirth.calculatedDate'
    });
    return new Y.Do.AlterReturn(null, schema);
  }, Y.usp.relationshipsrecording.PersonWithAddressWarningCountAndReferenceNumbers, 'getSchema', Y.usp.relationshipsrecording.PersonWithAddressWarningCountAndReferenceNumbers);

  Y.namespace('app.search').PersonAdvancedSearchFilterModel = Y.Base.create('personAdvancedSearchFilterModel', Y.usp.ResultsFilterModel, [], {}, {
        ATTRS: {
            nameOrId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            houseStreetTown: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            postCode: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            minAge: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setNumberValue,
                getter: Y.usp.ResultsFilterModel.getNumberValue,
                value: ''
            },
            maxAge: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setNumberValue,
                getter: Y.usp.ResultsFilterModel.getNumberValue,
                value: ''
            },
            gender: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            refNo: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            nhsFirstThreeDigits: {
                USPType: 'String',
                getter: padTo3Digits,
                value: ''
            },
            nhsSecondThreeDigits: {
                USPType: 'String',
                getter: padTo3Digits,
                value: ''
            },
            nhsLastFourDigits: {
                USPType: 'String',
                getter: padTo4Digits,
                value: ''
            },
            chiNumber: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            dayOfBirth: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            monthOfBirth: {
                USPType: 'String',
                setter: function (v) {
                    if (v === null || v === undefined || v === '') {
                        //clear day of birth
                        this.set('dayOfBirth', '');
                    }
                    return Y.usp.ResultsFilterModel.setListValue.call(this, v);
                },
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            yearOfBirth: {
                USPType: 'String',
                setter: function (v) {
                    if (v === null || v === undefined || v === '') {
                        //clear month of birth
                        this.set('monthOfBirth', '');
                    }
                    return Y.usp.ResultsFilterModel.setListValue.call(this, v);
                },
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            nhsNo: {
                USPType: 'String',
                setter: function (v) {
                    //split nhs number into individual fields
                    var nhsNumber = (v || '') + ''; //turn into a string
                    this.set('nhsFirstThreeDigits', nhsNumber.substring(0, 3));
                    this.set('nhsSecondThreeDigits', nhsNumber.substring(3, 6));
                    this.set('nhsLastFourDigits', nhsNumber.substring(6, 10));
                },
                getter: function () {
                    return [
                        this.get('nhsFirstThreeDigits'),
                        this.get('nhsSecondThreeDigits'),
                        this.get('nhsLastFourDigits')
                    ].join('').trim();
                }
            }
        }
    });

        /**
         * Provide a custom filter context view as this does not
         * act like a standard USP filter
         */
        Y.namespace('app.search').PersonAdvancedSearchFilterContext = Y.Base.create('personAdvancedSearchFilterContext', Y.app.search.AdvancedSearchFilterContext, [], {
            template: Y.Handlebars.templates.personAdvancedSearchActiveFilter,
        }, {
            ATTRS: {
                showFilter: {},
                codedEntries: {
                    value: {
                        gender: Y.uspCategory.person.gender.category.getActiveCodedEntries()
                    }
                },
                enumTypes: {
                    value: {
                        months: [{
                            enumName: '01',
                            name: 'Jan'
                        }, {
                            enumName: '02',
                            name: 'Feb'
                        }, {
                            enumName: '03',
                            name: 'Mar'
                        }, {
                            enumName: '04',
                            name: 'Apr'
                        }, {
                            enumName: '05',
                            name: 'May'
                        }, {
                            enumName: '06',
                            name: 'Jun'
                        }, {
                            enumName: '07',
                            name: 'Jul'
                        }, {
                            enumName: '08',
                            name: 'Aug'
                        }, {
                            enumName: '09',
                            name: 'Sep'
                        }, {
                            enumName: '10',
                            name: 'Oct'
                        }, {
                            enumName: '11',
                            name: 'Nov'
                        }, {
                            enumName: '12',
                            name: 'Dec'
                        }]
                    }
                }
            }
        });

    Y.namespace('app.search').PersonAdvancedSearchFilterView = Y.Base.create('personAdvancedSearchFilterView', Y.app.search.AdvancedSearchFilterView, [], {
        events: {
            'input[name="nhsFirstThreeDigits"]': {
                keyup: 'tabSecond'
            },
            'input[name="nhsSecondThreeDigits"]': {
                keyup: 'tabThird'
            }
        },
        template: Y.Handlebars.templates.personAdvancedSearch,
        initializer: function (config) {
            var fuzzyDateLabels = config.labels.fuzzyDateLabels;
            //setup widgets
            this.fuzzyDOBWidget = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyDOB',
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: new Date().getFullYear(),
                labels: fuzzyDateLabels
            });

            this._criteriaEvts = [
                //setup event handlers
                this.fuzzyDOBWidget.on('calendarPopup:dateSelection', function (e) {

                    var date = e.date;
                    if (date) {
                        this.get('model').setAttrs({
                            dayOfBirth: date.getDate(),
                            monthOfBirth: date.getMonth(),
                            yearOfBirth: date.getFullYear()
                        });
                    }
                }, this)
            ];
        },
        destructor: function () {
            //unbind event handles
            this._criteriaEvts.forEach(function (handle) {
                handle.detach();
            });

            delete this._criteriaEvts;

            this.fuzzyDOBWidget.destroy();
            delete this.fuzzyDOBWidget;
        },
        tabSecond: function (e) {
            var container = this.get('container'),
                nhsFirstThreeDigitsInput = container.one('input[name="nhsFirstThreeDigits"]'),
                nhsSecondThreeDigitsInput = container.one('input[name="nhsSecondThreeDigits"]'),
                inputValueLength = nhsFirstThreeDigitsInput.get('value').trim().length;
            // allow left/right arrow keys
            if (inputValueLength === 3 && e.keyCode !== 37 && e.keyCode !== 39) {
                nhsSecondThreeDigitsInput.focus().select();
            }
        },
        tabThird: function (e) {
            var container = this.get('container'),
                nhsSecondThreeDigitsInput = container.one('input[name="nhsSecondThreeDigits"]'),
                nhsLastFourDigitsInput = container.one('input[name="nhsLastFourDigits"]'),
                inputValueLength = nhsSecondThreeDigitsInput.get('value').trim().length;
            // allow left/right arrow keys
            if (inputValueLength === 3 && e.keyCode !== 37 && e.keyCode !== 39) {
                nhsLastFourDigitsInput.focus().select();
            }
        },
        render: function () {
            var container = this.get('container'),
                fuzzyDateContainer;

            Y.app.search.PersonAdvancedSearchFilterView.superclass.render.call(this);

            //setup the fuzzy date input node and render
            fuzzyDateContainer = container.one('#advancedSearchFilterForm_fuzzyBirthDate');

            this.fuzzyDOBWidget.set('contentNode', fuzzyDateContainer);
            this.fuzzyDOBWidget.render();
            //update input names to match filter form (not great - but no public access to inputs)
            this.fuzzyDOBWidget._yearInput.set('name', 'yearOfBirth');
            this.fuzzyDOBWidget._monthInput.set('name', 'monthOfBirth');
            this.fuzzyDOBWidget._dayInput.set('name', 'dayOfBirth');

            //initialize fuzzy date inputs
            this.fuzzyDOBWidget._disableInput('MONTH');
            this.fuzzyDOBWidget._disableInput('DAY');

            return this;
        }
    });

    Y.namespace('app.search').PersonAdvancedSearchFilter = Y.Base.create('personAdvancedSearchFilter', Y.app.search.AdvancedSearchFilter, [], {
        filterModelType: Y.app.search.PersonAdvancedSearchFilterModel,
        filterType: Y.app.search.PersonAdvancedSearchFilterView,
        filterContextType: Y.app.search.PersonAdvancedSearchFilterContext,
    });

    Y.namespace('app.search').PersonCriteriaView = Y.Base.create('personCriteriaView', Y.app.search.CriteriaView, [], {
        filterModelType: Y.app.search.PersonAdvancedSearchFilterModel,
        filterType: Y.app.search.PersonAdvancedSearchFilter,
        initializer: function() {
            this.on('searchTermsChange', this.handleOnSearchTermsChange, this);
        },
        handleOnSearchTermsChange: function (event) {
            var terms = event.newVal;
            if (terms.gender && !Array.isArray(terms.gender)) {
                //gender can be a comma separated lists
                terms.gender = terms.gender.split(',');
                event.newVal = terms;
            }
        }
    });

    //Overall View shown from subjectSearch, specifies PersonSearchPanelView as its view
    Y.namespace('app').PersonAppSearchView = Y.Base.create('personAppSearchView', Y.app.search.AdvancedSearchView, [Y.app.PersonSearchNavigation], {
        //This maps the field nameOrId to the keyword search
        whiteList: ['nameOrId', 'houseStreetTown', 'postCode', 'minAge', 'maxAge', 'gender', 'refNo', 'nhsNo', 'dayOfBirth', 'monthOfBirth', 'yearOfBirth', 'chiNumber'],
        initializer: function() {
            this.searchTerms = {};
            this.on('*:searchTermsChange', this.handleOnSearchTermsChange, this);
        },
        handleOnSearchTermsChange: function (event) {
            this.searchTerms = event.newVal;
        },
        formatterReferenceNumber: function(o) {
            var name = '',
              searchTermsRefNo = '', topRefNo = {},
              referenceNumbers = o.record.get('referenceNumbers');

            if(referenceNumbers.length > 0) {
              if (this.searchTerms.refNo && Array.isArray(this.searchTerms.refNo)) {
                searchTermsRefNo = this.searchTerms.refNo[0];
              }

              if(searchTermsRefNo !== ''){
                referenceNumbers = referenceNumbers.filter(function(ref){
                  return ref.referenceNumber.indexOf(searchTermsRefNo) !== -1;
                });

                if(referenceNumbers.length > 0){
                  topRefNo = referenceNumbers[0];
                  name = 'Reference';
                  if(Y.secured.uspCategory.person.referencetype.category.codedEntries.hasOwnProperty(topRefNo.type)){
                    name = Y.secured.uspCategory.person.referencetype.category.codedEntries[topRefNo.type].name;
                  }
                  o.column.allowHTML = true;
                  return L.sub(TEMPLATE_REFERENCE_NUMBER, {
                    name: name + ':',
                    value: topRefNo.referenceNumber
                  });
                }
              }
            }
            return '';
        },
        destructor: function () {
            delete this.searchTerms;
        },
        handleSelect: function (e) {
            if (e) {
                //we update the history so we know that a back can restore the search results
                this.history.replace({
                    'g_s': true
                });
                Y.uspSessionStorage.setStoreValue('optionSource', 'PERSON');
                //functionality provided by PersonSearchNavigation mixin
                this.navigateToPerson(e);
            }
        },
        getResultsConfig: function () {
            var searchConfig = this.get('searchConfig'),
                labels = searchConfig.labels,
                filterConfig = this.get('filterConfig'),
                filterContextConfig = this.get('filterContextConfig');

            return ({
                filterConfig: filterConfig,
                filterContextConfig: filterContextConfig,
                sortBy: [],
                plugins: [{
                    fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                    cfg: {
                        state: 'expanded',
                        summaryFormatter: Y.app.ColumnFormatters.summaryAddressFormat
                    }
                }],
                columns: [{
                    key: 'personIdentifier',
                    label: labels.personIdentifier,
                    sortable: true,
                    width: '15%',
                    formatter: Y.app.ColumnFormatters.linkable,
                    className: 'person-context',
                    headerTemplate: '<th id="{id}" colspan="1" rowspan="{_rowspan}" class="{className}" scope="col" {_id}{abbr}{title}>{content}</th>'
                }, {
                    key: 'organisationName',
                    label: 'Team Name',
                    sortable: true,
                    width: '20%',
                    className: 'person-context'
                }, {
                    key: 'name',
                    label: labels.personName,
                    allowHTML: true,
                    sortable: true,
                    width: '20%',
                    formatter: Y.app.ColumnFormatters.linkablePopOverWarnings,
                    className: 'person-context'
                }, {
                    key: 'dateOfBirth',
                    label: 'DoB',
                    sortable: true,
                    width: '15%',
                    maskField: 'dateOfBirth.fuzzyDateMask',
                    formatter: Y.usp.ColumnFormatters.fuzzyDate,
                    className: 'person-context'
                }, {
                    key: searchConfig.isScottishRegion ? 'chiNumber' : 'nhsNumber',
                    label: 'NHS / CHI number',
                    width: '15%',
                    formatter: Y.app.ColumnFormatters.formatterNHSCHINumber,
                    className: 'person-context'
                }, {
                    key: 'referenceNumbers',
                    label: 'Reference Number',
                    width: '15%',
                    formatter: this.formatterReferenceNumber.bind(this),
                    className: 'person-context'
                }, {
                    key: 'mobile-summary',
                    label: 'Summary',
                    className: 'mobile-summary pure-table-desktop-hidden',
                    width: '0%',
                    allowHTML: true,
                    formatter: Y.app.ColumnFormatters.summaryAddressFormat,
                    cellTemplate: '<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
                }],
                data: new Y.usp.relationshipsrecording.PaginatedPersonWithAddressWarningCountAndReferenceNumbersList({
                    url: searchConfig.url
                }),
                noDataMessage: searchConfig.noDataMessage
            });
        }
    }, {
        ATTRS: {
            searchConfig: {
                value: {}
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'handlebars',
        'handlebars-advancedSearch-templates',
        'app-advanced-search-base',
        'usp-relationshipsrecording-PersonWithAddressWarningCountAndReferenceNumbers',
        'caserecording-results-formatters',
        'categories-person-component-Gender',
        'categories-person-component-Ethnicity',
        'secured-categories-person-component-Referencetype',
        'person-search-navigation',
        'inline-input-css',
        'fuzzy-date',
        'results-table-summary-row-plugin',
    ]
});