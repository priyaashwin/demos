YUI.add('group-app-search', function (Y) {
    var USPTemplates = Y.usp.ColumnTemplates,
        USPformatters = Y.usp.ColumnFormatters;

    Y.namespace('app').GroupAppSearchView = Y.Base.create('groupAppSearchView', Y.app.AppSearchView, [Y.app.GroupSearchNavigation], {
        //This maps the field nameOrId to the keyword search
        whiteList: ['nameOrId'],
        initializer: function () {
            this.on('*:recordSelect', function (e) {
                this.handleSelect(e.record.get('id'));
            }, this);
        },
        handleSelect: function (groupId) {
            if (groupId) {
                //we update the history so we know that a back can restore the search results
                this.history.replace({
                    'g_s': true
                });
                Y.uspSessionStorage.setStoreValue('optionSource', 'GROUP');
                //functionality provided by GroupSearchNavigation mixin
                this.navigateToGroup(groupId);
            }
        },
        getResultsConfig: function () {
            var searchConfig = this.get('searchConfig'),
                labels = searchConfig.labels,
                codedEntries = this.get('codedEntries');

            return ({

                sortBy: [],
                plugins: [{
                    fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                    cfg: {
                        state: 'expanded',
                        summaryFormatter: Y.app.ColumnFormatters.formatMembers
                    }
                }],
                columns: [{
                    key: 'name',
                    label: labels.name,
                    sortable: true,
                    width: '25%',
                    formatter: Y.app.ColumnFormatters.formatNameAndIdentifier,
                    cellTemplate: USPTemplates.clickable(labels.showGroupLabel, 'switch-focus'),
                    url: searchConfig.groupUrl
                }, {
                    key: 'description',
                    label: labels.description,
                    width: '25%',
                }, {
                    key: 'mobile-summary',
                    label: 'Summary',
                    className: 'mobile-summary pure-table-desktop-hidden',
                    width: '0%',
                    allowHTML: true,
                    formatter: Y.app.ColumnFormatters.formatMembers,
                    cellTemplate: '<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
                }, {
                    key: 'groupType',
                    label: labels.groupType,
                    sortable: true,
                    allowHTML: true,
                    formatter: USPformatters.codedEntry,
                    codedEntries: codedEntries.groupType,
                    width: '15%'
                }, {
                    key: 'startDate',
                    label: labels.startDate,
                    sortable: true,
                    width: '10%',
                    formatter: Y.usp.ColumnFormatters.date
                }, {
                    key: 'closeDate',
                    label: labels.closeDate,
                    sortable: true,
                    width: '10%',
                    formatter: Y.usp.ColumnFormatters.date
                }],
                //configure the data set
                data: new Y.usp.relationshipsrecording.PaginatedGroupWithMemberDetailsAndCountList({
                    url: searchConfig.url
                }),
                noDataMessage: searchConfig.noDataMessage
            });
        }
    }, {
        ATTRS: {
            searchConfig: {
                value: {}
            },
            groupUrl: {
                value: '',
                writeOnce: 'initOnly'
            },
            codedEntries: {
                value: {
                    groupType: Y.uspCategory.group.groupType.category.codedEntries
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-search',
        'usp-session-storage',
        'usp-group-Group',
        'results-templates',
        'caserecording-results-formatters',
        'results-formatters',
        'group-search-navigation',
        'usp-relationshipsrecording-GroupWithMemberDetailsAndCount',
        'categories-group-component-GroupType',
        'results-table-summary-row-plugin'
    ]
});