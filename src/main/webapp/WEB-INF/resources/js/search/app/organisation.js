YUI.add('organisation-app-search', function (Y) {
    var O = Y.Object;

    Y.namespace('app.search').OrganisationAdvancedSearchFilterModel = Y.Base.create('organisationAdvancedSearchFilterModel', Y.usp.ResultsFilterModel, [], {}, {
        ATTRS: {
            nameOrId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            type: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            subType: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            externalReferenceNumber: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            postcode: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            houseStreetTown: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            }
        }
    });

    Y.namespace('app.search').OrganisationAdvancedSearchFilterContext = Y.Base.create('organisationAdvancedSearchFilterContext', Y.app.search.AdvancedSearchFilterContext, [], {
        template: Y.Handlebars.templates.organisationAdvancedSearchActiveFilter
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    organisationType: Y.uspCategory.organisation.organisationType.category.getActiveCodedEntries(),
                    organisationSubType: Y.uspCategory.organisation.organisationSubType.category.getActiveCodedEntries()
                }
            }
        }
    });

    Y.namespace('app.search').OrganisationAdvancedSearchFilterView = Y.Base.create('organisationAdvancedSearchFilterView', Y.app.search.TabbedAdvancedSearchFilterView, [], {
        template: Y.Handlebars.templates.organisationAdvancedSearch,
        render: function () {
            var container = this.get('container');

            Y.app.search.OrganisationAdvancedSearchFilterView.superclass.render.call(this);

            Y.FUtil.setSelectOptions(this.get('container').one('#organisationType'), O.values(Y.uspCategory.organisation.organisationType.category.getActiveCodedEntries()), null, null, true, true, 'code');
            Y.FUtil.setSelectOptions(this.get('container').one('#organisationSubType'), O.values(Y.uspCategory.organisation.organisationSubType.category.getActiveCodedEntries()), null, null, true, true, 'code');

            return this;
        }
    });

    Y.namespace('app.search').OrganisationAdvancedSearchFilter = Y.Base.create('organisationAdvancedSearchFilter', Y.app.search.AdvancedSearchFilter, [], {
        filterModelType: Y.app.search.OrganisationAdvancedSearchFilterModel,
        filterType: Y.app.search.OrganisationAdvancedSearchFilterView,
        filterContextType: Y.app.search.OrganisationAdvancedSearchFilterContext
    });

    Y.namespace('app.search').OrganisationCriteriaView = Y.Base.create('organisationCriteriaView', Y.app.search.CriteriaView, [], {
        filterModelType: Y.app.search.OrganisationAdvancedSearchFilterModel,
        filterType: Y.app.search.OrganisationAdvancedSearchFilter,
    });

    Y.namespace('app').OrganisationAppSearchView = Y.Base.create('organisationAppSearchView', Y.app.search.AdvancedSearchView, [Y.app.OrganisationSearchNavigation], {
        whiteList: ['nameOrId', 'type', 'subType', 'externalReferenceNumber', 'postcode', 'houseStreetTown'],
        handleSelect: function (organisationId) {
            if (organisationId) {
                //we update the history so we know that a back can restore the search results
                this.history.replace({
                    'g_s': true
                });
                Y.uspSessionStorage.setStoreValue('optionSource', 'ORGANISATION');
                //functionality provided by OrganisationSearchNavigation mixin
                this.navigateToOrganisation(organisationId);
            }
        },
        getResultsConfig: function () {
            var searchConfig = this.get('searchConfig'),
                labels = searchConfig.labels,
                filterConfig = this.get('filterConfig'),
                filterContextConfig = this.get('filterContextConfig');

            return ({
                filterConfig: filterConfig,
                filterContextConfig: filterContextConfig,
                sortBy: [],
                plugins: [{
                    fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                    cfg: {
                        state: 'expanded',
                        summaryFormatter: Y.app.ColumnFormatters.summaryAddressFormat
                    }
                }],
                columns: [{
                    key: 'organisationIdentifier',
                    label: labels.organisationIdentifier,
                    width: '15%',
                    formatter: Y.app.ColumnFormatters.linkable,
                    className: 'organisation-context',
                    headerTemplate: '<th id="{id}" colspan="1" rowspan="{_rowspan}" class="{className}" scope="col" {_id}{abbr}{title}>{content}</th>'
                }, {
                    key: 'name',
                    label: labels.organisationName,
                    allowHTML: true,
                    sortable: true,
                    width: '30%',
                    formatter: Y.app.ColumnFormatters.linkable,
                    className: 'organisation-context'
                }, {
                    key: 'type',
                    label: labels.organisationType,
                    allowHTML: true,
                    sortable: true,
                    width: '30%',
                    formatter: Y.usp.ColumnFormatters.codedEntry,
                    codedEntries: Y.uspCategory.organisation.organisationType.category.codedEntries,
                    className: 'organisation-context'
                }, {
                    key: 'subType',
                    label: labels.organisationSubtype,
                    allowHTML: true,
                    sortable: true,
                    width: '30%',
                    formatter: Y.usp.ColumnFormatters.codedEntry,
                    codedEntries: Y.uspCategory.organisation.organisationSubType.category.codedEntries,
                    className: 'organisation-context'
                }, {
                    key: 'mobile-summary',
                    label: 'Summary',
                    className: 'mobile-summary pure-table-desktop-hidden',
                    width: '0%',
                    allowHTML: true,
                    formatter: Y.app.ColumnFormatters.summaryAddressFormat,
                    cellTemplate: '<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
                }],
                data: new Y.usp.organisation.PaginatedOrganisationWithAddressAndPreviousNamesList({
                    url: searchConfig.url
                }),
                noDataMessage: searchConfig.labels.noDataMessage
            });
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'handlebars',
        'handlebars-advancedSearch-templates',
        'usp-organisation-OrganisationWithAddressAndPreviousNames',
        'categories-organisation-component-OrganisationType',
        'categories-organisation-component-OrganisationSubType',
        'app-advanced-search-base',
        'caserecording-results-formatters',
        'organisation-search-navigation',
        'inline-input-css',
        'results-table-summary-row-plugin'
    ]
});