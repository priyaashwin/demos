YUI.add('app-search', function (Y) {
    var L = Y.Lang,
        A = Y.Array,
        O = Y.Object;

    Y.namespace('app').AppSearchResults = Y.Base.create('appSearchResults', Y.View, [], {
        events: {
            '.pure-table-data tr a': {
                click: 'handleRowClick'
            }
        },
        initializer: function (config) {
            var resultsConfig = config.resultsConfig;

            //configure the form results table
            this.results = new Y.usp.PaginatedResultsTable(Y.merge(resultsConfig, {
                initialLoad: false,
                plugins: this.getPlugins(resultsConfig.plugins || [])
            }));

            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                tableId: this.name
            });

            // Add event targets
            this.results.addTarget(this);
        },

        /**
         * @methods getPlugins
         * returns base plugins concatenated with any found in resultsConfig
         * (output of getResultsConfig)
         */
        getPlugins: function (plugins) {
            return ([{
                fn: Y.Plugin.usp.ResultsTableMenuPlugin
            }, {
                fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
            }, {
                fn: Y.Plugin.usp.ResultsTableSearchStatePlugin,
                //turn off auto-rehydration of history as this is on every page and would interrupt the main results hydration 
                cfg: { noAutoLoad: true }
            }].concat(plugins));
        },
        handleRowClick: function (e) {
            var t = e.currentTarget,
                results = this.results,
                record = results.getRecord(t.get('id'));

            if (record && t.hasClass('switch-focus')) {
                e.preventDefault();

                this.fire('recordSelect', {
                    record: record
                })
            }
        },
        render: function () {
            var container = this.get('container'),
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment()),
                tableClass = this.get('tableClass');

            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));

            //render results
            this._tablePanel.renderResults(this.results);

            if (tableClass) {
                this.results.get('contentBox').one('table').addClass(tableClass);
            }

            //finally set the content into the container
            container.setHTML(contentNode);

            return this;
        },
        destructor: function () {
            if (this.results) {
                //clear the search state
                this.results.resultsTableSearchState.clearState();

                //destroy the results table
                this.results.destroy();

                delete this.results;
            }

            if (this._tablePanel) {
                this._tablePanel.destroy();

                delete this._tablePanel;
            }
        },
    });

    var AppSearchResultsDialog = Y.Base.create('appSearchResultsDialog', Y.usp.app.AppDialog, [], {
    }, {
        ATTRS: {
            width: {
                value: 950
            }        
        }
    });

    Y.namespace('app').AppSearchView = Y.Base.create('appSearchView', Y.View, [], {
        viewConfig:{
            searchResults:{
                type: Y.app.AppSearchResults
            }
        },
        initializer: function (config) {
            var viewConfig = config.viewConfig || {};

            this.searchResultsDialog = new AppSearchResultsDialog({
                views: {
                    searchResults: Y.merge(this.viewConfig.searchResults||{}, viewConfig)
                }
            });

            this.history = new Y.HistoryHash();

            // Add event targets
            this.searchResultsDialog.addTarget(this);

            this.searchResultsDialog.on('visibleChange', function (e) {
                if (e.newVal === false) {
                    //clear the history as the user has closed the dialog
                    this.history.replaceValue('g_s', undefined);
                    Y.uspSessionStorage.setStoreValue('optionSource', undefined);
                }
            }, this);
        },
        render: function () {
            //render the dialog
            this.searchResultsDialog.render();

            if (this.history.get('g_s')) {
                this.executeQuery(null, true);
            }

            //Rehydrate the search from saved state
            //searchState = this.results.resultsTableSearchState.applyState(); 
            //var searchState = sessionStorage.getItem('');
            return this;
        },
        getResultsConfig: function () {
            return ({});
        },
        /**
         * Passing a true for the history parameter will load
         * the results based on the search state captured in local session storage
         *  
         * @param searchTerm
         * @param history
         * @method executeQuery
         */
        executeQuery: function (searchTerms, history) {
            var searchConfig = this.get('searchConfig'),
                labels = searchConfig.labels,
                whiteList = this.whiteList || [],
                tableClass = this.get('tableClass');

            if (!O.isEmpty(searchTerms) || history) {
                this.searchResultsDialog.showView('searchResults', {
                        tableClass: tableClass,
                        labels: labels,
                        resultsConfig: this.getResultsConfig()
                    },
                    function (view) {
                        var results = view.results,
                            url,
                            searchState,
                            terms = {};

                        if (history) {
                            searchState = results.resultsTableSearchState.getState() || {};

                            //iterate over the whitelist
                            A.forEach(whiteList, function (key) {
                                if (O.hasKey(searchState, key)) {
                                    terms[key] = searchState[key];
                                }
                            });
                        } else {
                            terms = searchTerms;
                        }

                        if(Object.keys(terms).length>0){
                            //don't submit an empty search
                            url = L.sub(searchConfig.url, terms);

                            //set the query string
                            results.get('data').url = url;

                            //set the state from saved data
                            results.resultsTableSearchState.applyState();
                            //reload the results - only reset if we are not returning from history

                            results.reload({
                                reset: !history
                            });
                        }

                        //set the search terms into the view
                        view.set('searchTerms',terms);
                    });
            } else {
                //ensure hidden
                this.searchResultsDialog.hide();
            }
        },
        destructor: function () {
            if (this.searchResultsDialog) {
                this.searchResultsDialog.destroy();

                delete this.searchResultsDialog;
            }
        }
    }, {
        ATTRS: {
            tableClass: {
                value: {}
            },
            searchConfig: {
                value: {}
            }            
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'history',
        'usp-session-storage',
        'app-dialog',
        'accordion-panel',
        'paginated-results-table',
        'results-table-menu-plugin',
        'results-table-keyboard-nav-plugin',
        'results-table-search-state-plugin',
        'usp-relationshipsrecording-PersonWithAddressWarningCount'
    ]
});