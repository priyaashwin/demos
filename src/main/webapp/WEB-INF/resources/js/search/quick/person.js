YUI.add('person-quick-search', function(Y) {
    var L=Y.Lang,
        A=Y.Array,
        E=Y.Escape,
        ColumnFormatters=Y.app.ColumnFormatters;
    
    Y.namespace('app').PersonQuickSearchView = Y.Base.create('personQuickSearchView', Y.Base, [Y.app.PersonSearchNavigation], {
        initializer:function(config){
            //setup datasource
            var dataSource=new Y.DataSource.IO({
                source:config.url,
                ioConfig: {
                    headers:{'Accept': "application/vnd.olmgroup-usp.person.PersonWithAddressAndOtherNames+json"}
                } 
            });
            //plug in JSON schema
            dataSource.plug(Y.Plugin.DataSourceJSONSchema, {
                schema: {
                    resultListLocator: "results",
                    metaFields: {
                        pageSize: 'pagesize',
                        totalSize: 'totalSize'
                    }
                }
            });
            //mix in the config
            var cfg=Y.merge(config, {
                resultFormatter:this.resultFormatter,
                resultTextLocator: 'personIdentifier',
                source:dataSource,
                requestTemplate: '{query}&pageSize=' + this.get('maxResults')
            });

            this.quickSearchView=new Y.app.QuickSearchView(cfg);
            
            this.quickSearchView.on('*:select', this.handleSelect, this);
        },
        destructor:function(){
          //detach the event handler
            this.quickSearchView.detach('*:select', this.handleSelect);
            
            this.quickSearchView.destroy();            
        },
        render:function(){
            this.quickSearchView.render();
            
            return this;
        },
        handleSelect:function(e){
            var personId = e.details[0].result.raw.id;
            if(personId){
                //functionality provided by PersonSearchNavigation mixin
                this.navigateToPerson(personId);    
            }
        },
        resultFormatter: function (query, results) {           
            return A.map(results, function (result) {           
                var rawAddress = result.raw.address || {},
                    location = rawAddress.location || {},
                    locationDescription = location.location || '',
                    gender,
                    dob=result.raw.dateOfBirth,
                    dobEstimated=result.raw.dateOfBirthEstimated,
                    age = result.raw.age
                    address = [],
                    //Represent master records as 'primary' records in UI.
                    duplicateStatus = (result.raw.duplicated)? ((result.raw.duplicated === 'MASTER')? 'P' : 'D') : null;
                    
                    rawAddress.roomDescription ? address.push('Room '+ rawAddress.roomDescription) : '';
                    rawAddress.floorDescription ? address.push('Floor '+ rawAddress.floorDescription) : '';
                    locationDescription ? address.push(locationDescription) : '';
                    address.join(', ');
                var dobInfo = ColumnFormatters.formatDateBasedOnLifeState(result.raw.lifeState,result.raw.dueDate,dob,dobEstimated,age,true);

                gender=Y.uspCategory.person.gender.category.codedEntries[result.raw.gender];
                
                
                var sep='';
                var genderName='';
                if(gender){
                    genderName=gender.name;
                    if (gender.name.length && dobInfo.length){
                        sep=', ';
                    }
                }
                
                var rRow=    '<div title="'+E.html(result.raw.name)+'">'+ 
                             '<div class="identifier">' + ((duplicateStatus)? '<span class="duplicateStatus">' + duplicateStatus + '</span>' : '') +
                             '<span>'+E.html(result.raw.name)+' ('+E.html(result.raw.personIdentifier)+')'+'</span>'+ '</div>'+
                             '<div class="txt-color fl"> '+'<span> '+ genderName + sep + dobInfo.join(' ') + '</span>'+'</div>'+
                             ((address!==null)?('<div class="txt-color"> '+ E.html(address)+'</div>'):'')+
                             Y.app.search.person.RenderMatchingField(result.raw,query)+
                             '</div>';
                return rRow;
               
            });
      }
    },{
        ATTRS:{
            inputNode: {
                writeOnce: 'initOnly'
            },
            maxResults: {
                value: 20,
                writeOnce: 'initOnly',
                setter: function(val) {
                    return Number(val);
                },
                validator: function(val) {
                    return L.isNumber(val);
                }
            },
            url:{
                value: '',
                writeOnce: 'initOnly'
            }
        }
    })
}, '0.0.1', {
    requires: ['yui-base',
        'quick-search',
        'datasource-io', 
        'datasource-jsonschema',
        'person-name-matcher-highlighter',        
        'caserecording-results-formatters',
        'categories-person-component-Gender',
        'person-search-navigation'
    ]
});
