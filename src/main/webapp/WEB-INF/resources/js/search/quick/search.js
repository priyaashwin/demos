YUI.add('quick-search', function(Y) {
    Y.namespace('app').QuickSearchView = Y.Base.create('quickSearchView', Y.Base, [], {
        initializer: function(config) {
            var inputNode = Y.one(config.inputNode),
                //Mobile devices have input field hidden initially (Hence width is calculated as 0), therefore we fix 50% width for mobiles only
                inputWidth = inputNode.get('clientWidth') || '50%';
            
            this.autocomplete = new Y.AutoComplete({
                inputNode: config.inputNode,
                allowBrowserAutocomplete: false,
                enableCache: false,
                maxResults: config.maxResults,
                minQueryLength: 1,
                width: inputWidth,
                resultTextLocator: config.resultTextLocator,
                requestTemplate: config.requestTemplate,
                source: config.source,
                resultFormatter: config.resultFormatter,
                queryDelay: 250
            });
            
            //Add event target
            this.autocomplete.addTarget(this);
            
            this.showAll=config.viewConfig.header.showAll;
            
            config.source.after("response", this.searchHeaderReplaceIcon, this);
            
        },
                 
        searchHeaderReplaceIcon:function (results) {
            var size = results.response.meta.totalSize;
            var maxResults = this.get('maxResults');
            var content = (size > maxResults) ? this.showAll  : '<i class="fa fa-search"></i>'
            var searchButton = Y.one("#searchButton");
            size > maxResults ? searchButton.addClass('greenBackground') : searchButton.removeClass('greenBackground');
            searchButton.setContent(content);
        },
        
        render: function() {
            //render the AC - this view contains no renderable content
            this.autocomplete.render();
            // If there is any text ready to search on, kick off the request manually.
            if(this.autocomplete.get('query')){
        		this.autocomplete.sendRequest();
        	}       
            return this;
        },
        destructor: function() {
            if (this.autocomplete) {
                this.autocomplete.destroy();
                delete this.autocomplete;
            }
        }
    }, {
        ATTRS: {
            inputNode: {
                writeOnce: 'initOnly'
            },
            requestTemplate:{
                value: '',
                writeOnce: 'initOnly'
            },
            maxResults: {
                value: 20,
                writeOnce: 'initOnly'
            },
            source: {
                writeOnce: 'initOnly'
            },
            formatter: {
                writeOnce: 'initOnly'
            },
            resultTextLocator: {
                writeOnce: 'initOnly'
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'autocomplete',
               'node']
});