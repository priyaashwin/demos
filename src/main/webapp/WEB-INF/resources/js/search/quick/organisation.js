YUI.add('organisation-quick-search', function(Y) {
    var L=Y.Lang,
        A=Y.Array,
        E=Y.Escape,
        ColumnFormatters=Y.app.ColumnFormatters;
    
    Y.namespace('app').OrganisationQuickSearchView = Y.Base.create('organisationQuickSearchView', Y.Base, [Y.app.OrganisationSearchNavigation], {
        initializer:function(config){
            //setup datasource
            var dataSource=new Y.DataSource.IO({
                source:config.url,
                ioConfig: {
                    headers:{'Accept': "application/vnd.olmgroup-usp.organisation.OrganisationWithAddressAndPreviousNames+json"}
                }
            });
            //plug in JSON schema
            dataSource.plug(Y.Plugin.DataSourceJSONSchema, {
                schema: {
                    resultListLocator: "results",
                    metaFields: {
                        pageSize: 'pageSize',
                        totalSize: 'totalSize'
                    }
                }
            });
            //mix in the config
            var cfg=Y.merge(config, {
                resultFormatter:this.resultFormatter,
                resultTextLocator: 'organisationIdentifier',
                source:dataSource,
                requestTemplate: '{query}&pageSize=' + this.get('maxResults')
            });

            this.quickSearchView=new Y.app.QuickSearchView(cfg);
            
            this.quickSearchView.on('*:select', this.handleSelect, this);
        },
        destructor:function(){
          //detach the event handler
            this.quickSearchView.detach('*:select', this.handleSelect);
            
            this.quickSearchView.destroy();            
        },
        render:function(){
            this.quickSearchView.render();
            
            return this;
        },
        handleSelect:function(e){
            var organisationId = e.details[0].result.raw.id;
            if(organisationId){
                //functionality provided by OrganisationSearchNavigation mixin
                this.navigateToOrganisation(organisationId);
            }
        },
        resultFormatter: function (query, results) {           
            return A.map(results, function (result) { 
            	var rawAddress = result.raw.address || {},
                 location = rawAddress.location || {},
                 locationDescription = location.location || '',
                 address = [];
                 
                 rawAddress.roomDescription ? address.push('Room '+ rawAddress.roomDescription) : '';
                 rawAddress.floorDescription ? address.push('Floor '+ rawAddress.floorDescription) : '';
                 locationDescription ? address.push(locationDescription) : '';
                 address.join(', ');
                 
                var type = Y.uspCategory.organisation.organisationType.category.codedEntries[result.raw.type];
                var subType = Y.uspCategory.organisation.organisationSubType.category.codedEntries[result.raw.subType];
                var typeSubTypeField = type ? type.name + ((subType)?' - '+ subType.name:'') : '';
                
                var rRow=   '<div title="'+E.html(result.raw.name)+'">'+
                '<div class="identifier">' +'<span>'+E.html(result.raw.name)+' ('+E.html(result.raw.organisationIdentifier)+')'+'</span>'+'</div>' +
                ((address!==null)?('<div class="txt-color"> '+ E.html(address)+'</div>'):'')+
                ((type!==null)?('<div class="txt-color"> '+ E.html(typeSubTypeField)+'</div>'):'')+
                Y.app.search.organisation.RenderMatchingField(result.raw,query)+
                '</div>';
                return rRow;

            });
      }
    },{
        ATTRS:{
            inputNode: {
                writeOnce: 'initOnly'
            },
            maxResults: {
                value: 20,
                writeOnce: 'initOnly',
                setter: function(val) {
                    return Number(val);
                },
                validator: function(val) {
                    return L.isNumber(val);
                }
            },
            url:{
                value: '',
                writeOnce: 'initOnly'
            },
            organisationUrl:{
                value:'',
                writeOnce: 'initOnly'
            }
        }
    })
}, '0.0.1', {
    requires: ['yui-base',
               'escape',
        'quick-search',
        'datasource-io', 
        'datasource-jsonschema',
        'organisation-name-matcher-highlighter',   
        'caserecording-results-formatters',
        'categories-organisation-component-OrganisationType',                 
        'categories-organisation-component-OrganisationSubType',
        'organisation-search-navigation'
    ]
});
