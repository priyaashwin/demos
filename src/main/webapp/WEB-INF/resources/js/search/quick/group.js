YUI.add('group-quick-search', function(Y) {
    var L=Y.Lang,
        A=Y.Array,
        E=Y.Escape,
        ColumnFormatters=Y.app.ColumnFormatters;
    
    Y.namespace('app').GroupQuickSearchView = Y.Base.create('groupQuickSearchView', Y.Base, [Y.app.GroupSearchNavigation], {
        initializer:function(config){
            //setup datasource
            var dataSource=new Y.DataSource.IO({
                source:config.url,
                ioConfig: {
                    headers:{'Accept': "application/vnd.olmgroup-usp.relationshipsrecording.GroupWithMemberDetailsAndCount+json"}
                }
            });
            //plug in JSON schema
            dataSource.plug(Y.Plugin.DataSourceJSONSchema, {
                schema: {
                    resultListLocator: "results",
                    metaFields: {
                        pageSize: 'pageSize',
                        totalSize: 'totalSize'
                    }
                }
            });
            //mix in the config
            var cfg=Y.merge(config, {
                resultFormatter:this.resultFormatter,
                resultTextLocator: 'groupIdentifier',
                source:dataSource,
                requestTemplate : '{query}&temporalStatusFilter=ALL&s=&pageSize='+this.get('maxResults')
            });

            this.quickSearchView=new Y.app.QuickSearchView(cfg);
            
            this.quickSearchView.on('*:select', this.handleSelect, this);
        },
        destructor:function(){
          //detach the event handler
            this.quickSearchView.detach('*:select', this.handleSelect);
            
            this.quickSearchView.destroy();            
        },
        render:function(){
            this.quickSearchView.render();
            
            return this;
        },
        handleSelect:function(e){
            var groupId = e.details[0].result.raw.id;
            if(groupId){
                //functionality provided by GroupSearchNavigation mixin
                this.navigateToGroup(groupId);
            }
        },
        resultFormatter: function (query, results) {           
            return A.map(results, function (result) { 
            	var members = result.raw.members, membersCount = result.raw.membersCount, membersRow = '';
            	if(membersCount > 0 && members && members.length > 0 && !result.raw.isGroupSecured){
            		   var membersArray = [];
     				   A.each(members, function(member){
     				     var person=member.personSummary||member;
     					   membersArray.push(person.name + ' (' +person.personIdentifier+ ')');
     				   });
     				   
     				    membersRow = membersArray.join(', ');
     	            	if(membersRow.length>30){
     	            		membersRow = membersRow.slice(0,40)+'...';
     	            	}
            	}
            	if(membersCount >= 0  && result.raw.isGroupSecured){
            		membersRow = 'Group has restricted access';
            	}
            	
            	if(membersCount === 0  && !result.raw.isGroupSecured){
            		membersRow = 'There are no members to display';
            	}
            	
            	
            	var rRow ,title='', description = '';
            	if(E.html(result.raw.description)!=="null"){
            		title = E.html(result.raw.description);
            		description = E.html(result.raw.description);
            	}
                rRow = '<div title="'+title+'">'+
                                    '<div class="txt-ellipsis identifier" >' +
                                    	'<span class="hlit"> '+E.html(result.raw.name)+'</span>'+
                                    	'<span class="hlit"> (' +E.html(result.raw.groupIdentifier)+')</span>'+
                                        
                                    '</div>' +
                                    '<div class="txt-ellipsis groupDescription" > '+ description +'</div>'+
                                    '<div class="txt-ellipsis membersCount"> <span class="fa-stack"> <i class="fa fa-circle fa-stack-2x"> </i><i class="fa-inverse fa-stack-1x"><span class="membersCount-font">'+membersCount+ '</span></i></span><span> - ' + E.html(membersRow)+ ' </span> </div>' +
                         '</div>';
                    
                return rRow;
            });
      }
    },{
        ATTRS:{
            inputNode: {
                writeOnce: 'initOnly'
            },
            maxResults: {
                value: 20,
                writeOnce: 'initOnly',
                setter: function(val) {
                    return Number(val);
                },
                validator: function(val) {
                    return L.isNumber(val);
                }
            },
            url:{
                value: '',
                writeOnce: 'initOnly'
            },
            groupUrl:{
                value: '',
                writeOnce: 'initOnly'
            }
        }
    })
}, '0.0.1', {
    requires: ['yui-base',
        'quick-search',
        'datasource-io', 
        'datasource-jsonschema',   
        'caserecording-results-formatters',
        'group-search-navigation',
        'usp-relationshipsrecording-GroupWithMemberDetailsAndCount'
    ]
});
