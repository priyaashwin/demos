/**
  Provides the GroupSearchNavigation extension class. 
  This class adds a method to allow a common way of navigating to a given
  group.
  @module group-search-navigation
  @author Richard Gibson
  @since 1.0.0
 */
YUI.add('group-search-navigation', function(Y) {
   var L=Y.Lang;
   
    function GroupSearchNavigation(config) {            
    }
    
    GroupSearchNavigation.ATTRS = {
        groupUrl:{
            value:'',
            writeOnce: 'initOnly'
        }
    };
    
    GroupSearchNavigation.prototype = {
        navigateToGroup:function(groupId){
            if(groupId){                
                var path=window.location.pathname;
                var groupMatch=path.match(/\/[\w\/]+group?(\/add)?$/);
                if(groupMatch){
                    window.location=groupMatch[0]+L.sub('?id={groupId}&reset=true',{groupId:groupId});
                }else{
                    window.location=L.sub(this.get('groupUrl'),{groupId:groupId});
                }
            }
        }
    };
    // -- Namespace ---------------------------------------------------------------
    Y.namespace('app').GroupSearchNavigation = GroupSearchNavigation;
}, '0.0.1', {
    requires: ['yui-base']
});