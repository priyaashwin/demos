/**
  Provides the OrganisationSearchNavigation extension class. 
  This class adds a method to allow a common way of navigating to a given
  organisation.
  @module organisation-search-navigation
  @author Richard Gibson
  @since 1.0.0
 */
YUI.add('organisation-search-navigation', function(Y) {
   var L=Y.Lang;
   
    function OrganisationSearchNavigation(config) {            
    }
    
    OrganisationSearchNavigation.ATTRS = {
        organisationUrl:{
            value:'',
            writeOnce: 'initOnly'
        }
    };
    
    OrganisationSearchNavigation.prototype = {
        navigateToOrganisation:function(organisationId){
            if(organisationId){                
                var path=window.location.pathname;
                var organisationMatch=path.match(/\/[\w\/]+organisation(\/add)?$/);
                if(organisationMatch){
                    window.location=organisationMatch[0]+L.sub('?id={organisationId}&reset=true',{organisationId:organisationId});
                }else{
                    window.location=L.sub(this.get('organisationUrl'),{organisationId:organisationId});
                }
            }
        }
    };
    // -- Namespace ---------------------------------------------------------------
    Y.namespace('app').OrganisationSearchNavigation = OrganisationSearchNavigation;
}, '0.0.1', {
    requires: ['yui-base']
});