/**
  Provides the PersonSearchNavigation extension class. 
  This class adds a method to allow a common way of navigating to a given
  person.
  @module person-search-navigation
  @author Richard Gibson
  @since 1.0.0
 */
YUI.add('person-search-navigation', function(Y) {
   var L=Y.Lang;
   
    function PersonSearchNavigation(config) {            
    }
    
    PersonSearchNavigation.ATTRS = {
        personChecklistUrl:{
            value:'',
            writeOnce: 'initOnly'
        },
        personUrl:{
            value:'',
            writeOnce: 'initOnly'
        }
    };
    
    PersonSearchNavigation.prototype = {
        navigateToPerson:function(personId){
            if(personId){                
                var path=window.location.pathname;
                var personMatch=path.match(/\/[\w\/]+person(diagram)?(\/add)?$/);
                if(personMatch){
                    window.location=personMatch[0]+L.sub('?id={personId}&reset=true',{personId:personId});
                }else{
                  //Handle person ID specified in the path, rather than as a query parameter e.g. /approvals/person/123
                  var personIdPathMatch = path.match(/(\/[\w\/]+person\/)([-]?[\d]+)(\/[\w\/]*)?/);
                  if (personIdPathMatch) {
                    var action=personIdPathMatch[3]||'';
                    var query=window.location.search||'';
                    window.location=[personIdPathMatch[1],personId,action,query].join('');
                  }else{
                    window.location=L.sub(this.get('personUrl'),{personId:personId});
                  }
                }
            }
        }
    };
    // -- Namespace ---------------------------------------------------------------
    Y.namespace('app').PersonSearchNavigation = PersonSearchNavigation;
}, '0.0.1', {
    requires: ['yui-base']
});