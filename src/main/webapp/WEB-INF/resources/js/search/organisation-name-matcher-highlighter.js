YUI.add('organisation-name-matcher-highlighter', function (Y) {

	var E = Y.Escape;
	
	Y.namespace('app.search.organisation').GetMatchingFieldForQuery = function(object,query){
		var matchingField = {
				name : null,
				value: null
		};
		if(!object.name.match(new RegExp(query,'i'))){

			if(object.previousNames && object.previousNames.length>0){
				var previousNames = object.previousNames;
				for(var index =0;index<previousNames.length;index++){
					var name = previousNames[index].name;
					if(name.match(new RegExp(query,'i'))&&previousNames[index].type=='PREVIOUS'){
						matchingField.name = 'Previous name' ;
						matchingField.value = name ;
						break;
					}
				}
			}
		}
		return matchingField;
	};

	var renderMatchingField = function (object,query){
		var matchingField = Y.app.search.organisation.GetMatchingFieldForQuery(object,query);
		if(matchingField.name && matchingField.value)
			return '<div class="otherField">'+E.html(matchingField.name.toUpperCase())+' - '+ Y.Highlight.all(matchingField.value,query)+'</div>';
		else
			return '';
	};
	Y.namespace('app.search.organisation').RenderMatchingField = renderMatchingField;

}, '0.0.1', {
	requires: ['yui-base',
	           'highlight',
	           'form-util',
	           'escape']
});