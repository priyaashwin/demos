YUI.add('person-name-matcher-highlighter', function (Y) {
	
	var E = Y.Escape;	
	
	Y.namespace('app.search.person').GetMatchingFieldForQuery = function(object,query){
		var matchingField = {
				name : null,
				value: null
		},
		queryExpression=new RegExp(query,'i');
		
		if(!object.name.match(queryExpression)){
		
			if((object.forename+' '+object.surname).match(queryExpression)){
				matchingField.name = 'Name' ;
				matchingField.value = object.forename+' '+object.surname ;
			}
			else if(object.knownAs && object.knownAs.match(queryExpression)){
				matchingField.name = 'Known as' ;
				matchingField.value = object.knownAs ;
			}
			else if(object.middleNames && object.middleNames.match(queryExpression)){
				matchingField.name = 'Other forenames' ;
				matchingField.value = object.middleNames ;
			}
			else if(object.alternateNames && object.alternateNames.match(queryExpression)){
				matchingField.name = 'Other names' ;
				matchingField.value = object.alternateNames ;
			}
			else if(object.otherNames && object.otherNames.length>0){
					var otherNames = object.otherNames;
					for(var index =0;index<otherNames.length;index++){
						if(otherNames[index].middleNames){
							middleName = otherNames[index].middleNames;
						}else{
							middleName ='';
						}
						var fullName = otherNames[index].forename+' '+middleName+' '+otherNames[index].surname;
						if(fullName.match(queryExpression)&&otherNames[index].type=='PREVIOUS'){
							matchingField.name = 'Previous name' ;
        					matchingField.value = fullName ;
							break;
						}
					}
			}
		}
		return matchingField;
	};
	
	var renderMatchingField = function (object,query){
			var matchingField = Y.app.search.person.GetMatchingFieldForQuery(object,query);
			if(matchingField.name && matchingField.value)
				return '<div class="otherField">'+E.html(matchingField.name.toUpperCase()) + ' - ' +  Y.Highlight.all(matchingField.value,query)+'</div>';
			else
				return '';
	};
	Y.namespace('app.search.person').RenderMatchingField = renderMatchingField;

}, '0.0.1', {
    requires: ['yui-base', 'highlight','escape']
});