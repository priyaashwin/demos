//setup info-pop for entire page
Y.use('info-pop', function(Y){
    new Y.usp.InfoPop({
        selector:'.form-state, .form-type-state, .review-state',
        zIndex:9999999
    }).render();
});