YUI.add('app-address-form-reopen', function (Y) {
    var addressFormatters = Y.app.FormatAddressLocation;

    Y.namespace('app.address').ReopenAddressForm = Y.Base.create('reopenAddressForm', Y.usp.View, [], {

        events: {
            '#preSelectDateOptions li a': {
                click: 'preSelectFuzzyDate'
            }
        },

        template: Y.Handlebars.templates["reopenOrganisationAddress"],
        render: function () {
            var container = this.get('container'),
                template = this.template,
                description = '',
                summary = '',
                model = this.get('model'),
                labels = this.get('labels');

            container.setHTML(template({
                labels: labels,
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                modelData: model.toJSON(),
                targetOrganisationId: this.get('contextId'),
                targetOrganisationName: this.get('contextName'),
                codedEntries: this.get('codedEntries') || {}
            }));

            //set initial narrative
            description = this.get('contextName') + ' (ORG' + this.get('contextId') + ')';
            summary = labels.reopenAddressNarrative;
            this.getNarrative('summary').set('text', summary);
            this.getNarrative('description').set('text', description);

            return this;
        },

        getInput: function (id) {
            return this.get('container').one('#' + id);
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        }
    }, {

        ATTRS: {

            codedEntries: {
                value: {
                    addressEndReason: Y.uspCategory.address.endReason.category.codedEntries,
                    addressType: Y.uspCategory.address.type.category.codedEntries,
                    addressUsage: Y.uspCategory.address.usage.category.codedEntries,
                    unknownLocations: Y.uspCategory.address.unknownLocation.category.codedEntries
                }
            }
        }

    });

}, '0.0.1', {
    requires: ['yui-base',
        'event-custom',
        'form-util',
        'handlebars',
        'view',
        'handlebars-helpers',
        'handlebars-person-templates',
        'categories-address-component-EndReason',
        'categories-address-component-UnknownLocation',
        'categories-address-component-Type',
        'base-AddressLocation-formatter',
        'categories-address-component-Usage'
    ]
});