

YUI.add('base-AddressLocation-formatter', function(Y) {
	var Formatters = {
			getAddressLocationDescription : function(model,labels) {
	        	var addressLocationDescription = [],
	        	    roomLabel = labels ? labels.roomDescription + ' ' : 'Room ',
	        		floorLabel = labels ? labels.floorDescription+' ' :'Floor ',
	        		location = model.get('location') ? model.get('location').location : model.get('address');
	        				
	        		model.get('roomDescription') ? addressLocationDescription.push(roomLabel+ model.get('roomDescription')) : '';
	                model.get('floorDescription') ? addressLocationDescription.push (floorLabel+ model.get('floorDescription')) : '';
	                addressLocationDescription.push(location||'');
	                
	                return addressLocationDescription.join(', ');
	        }
	};
		
	Y.namespace('app').FormatAddressLocation = Formatters;
	
	
}, '0.0.1', {
	  requires: ['yui-base']
	});
	