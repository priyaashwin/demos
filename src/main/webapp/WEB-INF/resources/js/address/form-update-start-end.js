YUI.add('app-address-form-update-start-end', function (Y) {

    var addressFormatters = Y.app.FormatAddressLocation;

    Y.namespace('app.address').UpdateStartEndDateAddressForm = Y.Base.create('updateStartEndDateAddressForm', Y.usp.View, [], {

        events: {
            '#preSelectStartDateOptions li a': {
                click: 'preSelectStartFuzzyDate'
            },
            '#preSelectEndDateOptions li a': {
                click: 'preSelectEndFuzzyDate'
            }
        },

        template: Y.Handlebars.templates["updateOrganisationAddressDates"],
        render: function () {
            var container = this.get('container'),
                description = '',
                summary ='',
                template = this.template,
                model = this.get('model'),
                labels = this.get('labels'),
                endDate = this.get('model').get('endDate');

            container.setHTML(template({
                labels: this.get('labels'),
                targetOrganisationId: this.get('contextId'),
                targetOrganisationName: this.get('contextName'),
                codedEntries: this.get('codedEntries') || {},
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                modelData: model.toJSON()
            }));

            // This ensures the end date widget is not initialized when there is no existing end date on the address.
            if (endDate && endDate.calculatedDate) {
                // render the fuzzy date widget
                this.fuzzyEndDate = new Y.usp.SimpleFuzzyDate({
                    id: 'fuzzyEnd',
                    contentNode: this.getInput('fuzzyEndDate'),
                    yearInput: true,
                    monthInput: true,
                    dayInput: true,
                    showDateLabel: false,
                    calendarPopup: true,
                    cssPrefix: 'pure',
                    minYear: 1886,
                    maxYear: 9999,
                    labels: this.get('fuzzyDateLabels'),
                    dateValue: endDate
                });
                this.fuzzyEndDate.render();
            }

            this.fuzzyStartDate = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyStart',
                contentNode: this.getInput('fuzzyStartDate'),
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: 9999,
                labels: this.get('fuzzyDateLabels'),
                dateValue: this.get('model').get('startDate')
            });
            this.fuzzyStartDate.render();

            //set initial narrative
            description = this.get('contextName') + ' (ORG' + this.get('contextId') + ')';
                summary = labels.updateAddressDatesNarrative;
            this.getNarrative('summary').set('text', summary);
            this.getNarrative('description').set('text', description);

            return this;
        },

        getInput: function (id) {
            return this.get('container').one('#' + id);
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        },

        getEndDate: function () {
            if (this.fuzzyEndDate) {
                return this.fuzzyEndDate.getFuzzyDate();
            }
        },

        getStartDate: function () {
            return this.fuzzyStartDate.getFuzzyDate();
        },

        preSelectStartFuzzyDate: function (e) {
            e.preventDefault();
            var t = e.currentTarget,
                today = new Date();
            if (t.hasClass('currentDate')) {
                this.fuzzyStartDate.setDateAndMask(today, 'DAY');
            }
        },
        preSelectEndFuzzyDate: function (e) {
            e.preventDefault();
            var t = e.currentTarget,
                today = new Date();
            if (t.hasClass('currentDate')) {
                this.fuzzyEndDate.setDateAndMask(today, 'DAY');
            }
        },

        destructor: function () {
            if (this.fuzzyEndDate) {
                this.fuzzyEndDate.destroy();
                delete this.fuzzyEndDate;
            }

            this.fuzzyStartDate.destroy();
            delete this.fuzzyStartDate;
        }
    }, {

        ATTRS: {
            codedEntries: {
                value: {
                    addressEndReason: Y.uspCategory.address.endReason.category.codedEntries,
                    addressType: Y.uspCategory.address.type.category.codedEntries,
                    addressUsage: Y.uspCategory.address.usage.category.codedEntries,
                    unknownLocations: Y.uspCategory.address.unknownLocation.category.codedEntries
                }
            }
        }

    });

}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'event-custom',
        'form-util',
        'fuzzy-date',
        'calendar-popup',
        'handlebars',
        'handlebars-helpers',
        'handlebars-person-templates',
        'categories-address-component-EndReason',
        'categories-address-component-UnknownLocation',
        'categories-address-component-Type',
        'base-AddressLocation-formatter',
        'categories-address-component-Usage'
    ]
});