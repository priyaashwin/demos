

YUI.add('app-address-autocomplete', function(Y) {
	var postCodePattern = new RegExp("(ZZ99 3VZ|GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})"),    
    	outerPostCodePattern = new RegExp("(ZZ99|GIR|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW]))");	


	function AutoCompleteAddress() {	
		AutoCompleteAddress.superclass.constructor.apply(this, arguments);
	}

	AutoCompleteAddress.NAME = 'aclist';
	AutoCompleteAddress.NS = 'ac';
	AutoCompleteAddress.ATTRS = {};

	Y.extend(AutoCompleteAddress, Y.Plugin.usp.PopupAutoCompletePlugin, {
		
		sendRequest: function(query, requestTemplate) {    		
				
			var container = this.get('viewContainer');
			var houseNameNumber = container.one('#houseSearch-input').get('value');		
			var postcode = container.one('#postcodeSearch-input').get('value');    		
		
			if(postcode != null){
    		
				var normalisedPostcode = this.normalisePostCode(postcode);    		
				var formattedPostCode = postcode.toUpperCase().trim();
    		
				if(this.isValidPostCode(normalisedPostcode) ||    				
						(this.isValidOuterPostCode(formattedPostCode) && houseNameNumber != null && houseNameNumber.length > 0)) {
    			
					AutoCompleteAddress.superclass.sendRequest.call(this, query, requestTemplate); 
    		
				}else{
    			
					this._defClearFn();    			
					container.one('#address_search_note').show();
					container.one('#noSearchResults-wrapper').hide();
				}
    	
			}   		
	
		},
	
		// this code taken from PostcodeNormalizer.java (core facet)	
		normalisePostCode: function(postcode){
		
			var retString = null;
		
			if(postcode != null){							
				retString = postcode.toUpperCase().replace(/\s/g, '');			
				if(retString.length > 3){								
					retString = retString.substring(0, retString.length-3)+' '+retString.substring(retString.length-3);								
				}	
			}
		
			return retString;
	
		},
	
	
		isValidPostCode: function(postcode){		
			return postCodePattern.test(postcode);	
		},
	
	
		isValidOuterPostCode: function(postcode){		
			return outerPostCodePattern.test(postcode);	
		}

	}, {
		
		ATTRS: {
			
			viewContainer: {
				value: null
			}
		}
	});
	
	
	Y.namespace('Plugin.app').AutoCompleteAddress = AutoCompleteAddress;
	
	
}, '0.0.1', {
	  requires: ['yui-base',
	             'event-custom',
	             'popup-autocomplete-plugin'
	             ]
	});
	