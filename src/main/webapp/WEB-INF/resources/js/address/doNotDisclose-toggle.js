YUI.add('app-address-switch-do-not-disclose', function (Y) {
    var L = Y.Lang;

    Y.namespace('app.address').ToggleDoNotDiscloseSwitch = {
        handleDoNotDiscloseClick: function(e, urls) {
            var data = e.currentTarget.getData(),
                url = data.do_not_disclose === "true" ? urls.setAddressDiscloseURL : urls.setAddressDoNotDiscloseURL,
                id = data.id;

            if (data.disabled === 'false') {
                new Y.usp.Model({
                    url: L.sub(url, {
                        id: id
                    }),
                    id: id
                }).save(function(err, res) {
                    if (err === null) {
                        Y.fire('addressDoNotDisclose:addressChanged');
                    } else {
                        Y.fire('addressDoNotDisclose:error', err);
                    }
                });
            }
        }
    }
});
