YUI.add('app-address-form-add', function (Y) {

    var E = Y.Escape,

        L = Y.Lang,

        A = Y.Array,

        O = Y.Object,

        NewLocationModel = Y.Base.create("NewLocationModel", Y.usp.address.NewLocation, [Y.usp.ModelFormLink], {
            form: '#addAddressForm'
        }),

        newLocationModel = new NewLocationModel();

    Y.namespace('app.address').AddAddressForm = Y.Base.create('addAddressForm', Y.View, [], {
        template: Y.Handlebars.templates["addAddressDialog"],
        events: {
            '#locations li a': {
                click: 'changeLocation'
            },
            '#postcodeSearch-input': {
                keyup: 'chkEnableHouseSearch'
            },
            '#location_toggleAddLocation': {
                click: 'enableAddLocation'
            },
            '#confirmAddLocation': {
                click: 'submitAddLocation'
            },
            '#cancelAddLocation': {
                click: 'cancelAddLocation'
            },
            '.input-group.input-prepend #fuzzyStartYEAR, #fuzzyStartMONTH, #fuzzyStartDAY': {
                focus: 'changeErrorHighlightToBlue'
            },
            '.error #fuzzyStartYEAR, .error #fuzzyStartMONTH, .error #fuzzyStartDAY': {
                blur: 'changeErrorHighlightToRed'
            },
            '#preSelectDateOptions li a': {
                click: 'preSelectFuzzyDate'
            }
        },

        initializer: function () {
            this._evtHandlers = [];
        },

        destructor: function () {
            if (this.fuzzyStartDate) {
                this.fuzzyStartDate.destroy();
                delete this.fuzzyStartDate;
            }
        },

        render: function () {

            var container = this.get('container');

            container.setHTML(this.template({
                labels: this.get('labels'),
                otherData: this.get('otherData')
            }));

            this.fuzzyStartDate = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyStart',
                contentNode: this.getInput('fuzzyStartDate'),
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: 9999,
                labels: this.get('fuzzyDateLabels')
            });

            this.fuzzyStartDate.render();

            //setup autocompletes
            this.initPostcodeAutoComplete();
            this.initHouseAutoComplete();
            this.chkEnableHouseSearch();

            //get our coded entries		
            this.initUsages();
            this.initTypes();
            this.initAddLocation();

            this.setNarrative();

            return this;
        },

        setNarrative: function () {
            //set initial narrative						
            this.getNarrative('summary').set('text', "You're adding an address for");
            this.getNarrative('description').set('text', this.get('nameAndIdDisplay'));
        },

        getInput: function (id) {
            return this.get('container').one('#' + id + '-input');
        },

        getInputs: function (inputId) {
            var container = this.get('container');
            return container.one('#address_' + inputId);
        },

        getWrapper: function (id) {
            return this.get('container').one('#' + id + '-wrapper');
        },

        getLabel: function (id) {
            return this.get('container').one('#' + id + '-label');
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        },

        initPostcodeAutoComplete: function () {
            var acUrl = this.get('locationUrl');
            var acNode = this.getInput('postcodeSearch');
            var pageSize = 100;
            var self = this;

            acNode.plug(Y.Plugin.app.AutoCompleteAddress, {
                width: '48em',
                allowBrowserAutocomplete: false,
                maxResults: pageSize,
                enableCache: false,
                minQueryLength: 2,
                resultHighlighter: 'phraseMatch',
                resultListLocator: 'results',
                resultTextLocator: 'location',
                requestTemplate: function (query) {
                    var houseNameNumber = self.getInput('houseSearch').get('value');
                    var postCode = self.getInput('postcodeSearch').get('value');
                    var excludeNonShareable = true;
                    return '?nameOrNumber=' + houseNameNumber + '&excludeNonShareable=' + excludeNonShareable + '&postcode=' + postCode + '&pageSize=' + pageSize;
                },
                source: acUrl,
                viewContainer: this.get('container'),
                resultFormatter: function (query, results) {
                    return A.map(results, function (result) {
                        var html = '<div title="{title}"><div><span>' + E.html(result.raw.location) + '</span></div>';
                        return html;
                    });
                }
            });

            acNode.ancestor('div').addClass("yui3-skin-sam");

            acNode.ac.on('select', function (e) {
                this.afterAutoCompleteSelect(e);
            }, this);

            // we check returned results displaying an info message if none are returned 
            acNode.ac.on('results', function (e) {

                var selectedNode = this.get('container').one('#locations');

                if (e.data.results.length === 0) {
                    this.getWrapper('noSearchResults').show();
                    this.getWrapper('overMaxResults').hide();
                    this.get('container').one('#address_search_note').hide();
                    selectedNode.hide();
                } else {
                    //advise user to refine search if there are more results than our maximum visible results
                    if (e.data.totalSize > pageSize) {
                        this.getWrapper('overMaxResults').show();
                    } else {
                        this.getWrapper('overMaxResults').hide();
                    }
                    this.getWrapper('noSearchResults').hide();
                    this.get('container').one('#address_search_note').hide();
                    selectedNode.show();
                }
            }, this);

        },

        initHouseAutoComplete: function () {
            var postCodeSearch = this.getInput('postcodeSearch');

            this.getInput('houseSearch').on('keyup', function (e) {
                postCodeSearch.ac.sendRequest();
            });

        },

        afterAutoCompleteSelect: function (e) {
            var html = '<li class="data-item" id="location{locationId}"><span>{location}</span><a href="#none" class="remove small">Change</a></li>';

            this.setAttrs({
                location: e.result.raw,
                locationId: e.result.raw.id
            }, {
                silent: true
            });

            this.get('container').one('#locations ul').setHTML(L.sub(html, {
                location: E.html(this.get('location').location),
                locationId: e.result.raw.id
            }));

            //hide search fields & labels
            this.getWrapper('houseSearch').hide();
            this.getWrapper('postcodeSearch').hide();
            this.getWrapper('overMaxResults').hide();
            this.get('container').one('#location_toggleAddLocation').hide();
            this.get('container').one('#address_search_note').hide();
        },

        initUsages: function () {
            var usages;
            usages = Y.Object.values(Y.uspCategory.address.usage.category.getActiveCodedEntries());
            Y.FUtil.setSelectOptions(this.getInput('usage'), usages, null, null, true, true, 'code');
        },

        initTypes: function () {
            var types;
            types = Y.Object.values(Y.uspCategory.address.type.category.getActiveCodedEntries());
            Y.FUtil.setSelectOptions(this.getInput('type'), types, null, null, true, true, 'code');
        },

        initCounty: function () {
            var categories = Y.uspCategory.address.countyCode.category.getActiveCodedEntries();
            var input = this.getInput('county');
            Y.Object.each(categories, function (county, key) {
                county.code = key;
            });
            Y.FUtil.setSelectOptions(input, Y.Object.values(categories), null, null, true, true, 'code');

        },
        initCountry: function () {
            //Instead of getting active country codes, its been hardcoded to support Irish location to be added to ECM
            var values = [ {code:"GBR", name: "United Kingdom"},{code: "IRL", name: "Ireland"}];
            Y.FUtil.setSelectOptions(this.get('container').one('#country-input'), values, null, null, true, true, 'code');
        },

        initCountrySubdivision: function (country) {
            var countryCode = this.get('container').one('#country-input').get('value');

            if (countryCode) {
                values = O.values(Y.uspCategory.address.countrySubdivisionCode.category.getActiveCodedEntriesForParent(countryCode));
                Y.FUtil.setSelectOptions(this.get('container').one('#subdivision-input'), values, null, null, true, true, 'code');
            }
        },

        chkEnableHouseSearch: function (e) {

            if (this.getInput('postcodeSearch').get('value').length > 1) {
                this.getLabel('houseSearch').removeClass('label-disabled');
                this.getInput('houseSearch').set('disabled', false);
            } else {
                this.getLabel('houseSearch').addClass('label-disabled');
                this.getInput('houseSearch').set('disabled', true).set('value', '');
            }

        },

        changeErrorHighlightToBlue: function (e) {
            this.getWrapper('fuzzyStartDate').addClass('focus');

        },

        changeErrorHighlightToRed: function (e) {
            this.getWrapper('fuzzyStartDate').removeClass('focus');

        },

        getStartDate: function () {
            return this.fuzzyStartDate.getFuzzyDate();
        },

        preSelectFuzzyDate: function (e) {
            e.preventDefault();
            var t = e.currentTarget,
                today = new Date();
            if (t.hasClass('currentDate')) {
                this.fuzzyStartDate.setDateAndMask(today, 'DAY');
            }
        },

        changeLocation: function (e) {

            //rest, hide, unhide filed as appropriate
            this.get('container').one('#locations ul').setHTML('');
            this.getWrapper('houseSearch').show();
            this.getWrapper('postcodeSearch').show();
            this.getInput('houseSearch').set('value', '');
            this.getInput('postcodeSearch').set('value', '');
            this.get('container').one('#location_toggleAddLocation').show();
            this.get('container').one('#address_search_note').show();

            // reset our viewmodel location info
            this.setAttrs({
                location: '',
                locationId: ''
            }, {
                silent: true
            });
        },

        resetValidationErrors: function () {
            var validationNode = Y.one('.yui3-widget-bd .message-error');
            if (validationNode) {
                validationNode.remove();
            }

            this.get('container').all('input.error, select.error, fieldset.error, div.error').each(function (input) {
                input.removeClass('error');
            });
        },

        successMsg: function (options) {

            var wrapper = this.getWrapper('successMsg');
            var template = this.get('templateAddLocationMsg');
            var handleClick = function (e) {
                e.preventDefault();
                this.hide();
                this.hide(200);
            };

            options = options || {};

            wrapper.setHTML(L.sub(template, {
                msgAddLocationSuccess: E.html(options.msg),
                msgType: E.html(options.msgType)
            }));

            wrapper.setStyles({
                'opacity': 0,
                'display': 'none'
            });
            wrapper.delegate('click', handleClick, '.pure-alert .close', wrapper);
            wrapper.show(200);

            if (options.msgDuration) {
                setTimeout(function () {
                    wrapper.hide(200);
                }, options.msgDuration);
            }
        },

        initAddLocation: function () {
            this.initCounty();
            this.initCountry();
        },

        enableAddLocation: function (e) {
            var postcodeSearchData = '';
            var houseSearchData = '';
            var defaultCountry = 'GBR';
            e.preventDefault();

            Y.one('.addAddressForm .yui3-widget-ft').hide();
            this.resetValidationErrors();
            this.getWrapper('addLocation').show();
            this.getWrapper('location').hide();
            this.getWrapper('postcodeSearch').hide();
            this.getWrapper('houseSearch').hide();
            this.getWrapper('type').hide();
            this.getWrapper('usage').hide();
            this.getWrapper('roomDescription').hide();
            this.getWrapper('doNotDisclose').hide();
            this.getWrapper('floorDescription').hide();
            this.getWrapper('startDate').hide();
            this.getWrapper('fuzzyStartDate').hide();
            this.getWrapper('noSearchResults').hide();
            this.getWrapper('overMaxResults').hide();
            this.get('container').one('#address_search_note').hide();

            // get data from searchform
            postcodeSearchData = this.getInput('postcodeSearch').get('value');
            houseSearchData = this.getInput('houseSearch').get('value');

            this.get('container').one('#addAddressForm').reset();
            this.getNarrative('summary').set('text', "You're adding a new location and an address for");
            this.get('container').one('#address_postcode').setAttribute('value', postcodeSearchData);
            this.get('container').one('#address_primaryNameOrNumber').setAttribute('value', houseSearchData);

            // Force the country to UK for now since we only support UK postcodes
            this.getInput('country').set('value', defaultCountry);
            this.initCountrySubdivision(defaultCountry);
        },

        submitAddLocation: function (e) {

            var self = this;
            var dialog = this.get('panel');
            var selectionNode = Y.one('#locations');
            var locationHtml = '<li class="data-item" id="location{locationId}"><span>{location}</span><a href="#none" class="remove small">Change</a></li>';

            e.preventDefault();

            dialog.showDialogMask();
            newLocationModel.set('countryCode', 'GBR', {
                silent: true
            }); // Not picking it up from disabled dropdown
            newLocationModel.url = this.get('locationUrl');
            newLocationModel.save(function (err, res) {

                dialog.hideDialogMask();

                if (err) {
                    self.fire('locationSave:error', {
                        error: err,
                        response: res
                    });
                } else {

                    self.setAttrs({
                        location: newLocationModel.toJSON(),
                        locationId: newLocationModel.get('id')
                    }, {
                        silent: true
                    });

                    // Make another call back to the server to get the newly created location.
                    // We'll used this to update the narrative.	JSON parsing is best in try/catch logic
                    // since there is no easy way to detect if it's JSON.
                    //In chrome r.responseText is string of JSON object but in firefox it is string XML data 
                    //TODO: Need to set the Accept globally for all IO read operations
                    Y.io(res.getResponseHeader('Location'), {

                        headers: {
                            'Accept': 'application/json',
                        },
                        on: {
                            success: function (tx, r) {
                                var parsedResponse = {};
                                try {
                                    parsedResponse = Y.JSON.parse(r.responseText);
                                    self.get('container').one('#locations ul').setHTML(L.sub(locationHtml, {

                                        location: E.html(parsedResponse.location)

                                    }));
                                } catch (e) {

                                    return;
                                }
                            }
                        }
                    });

                    self.getWrapper('location').show();
                    self.getWrapper('type').show();
                    self.getWrapper('usage').show();
                    self.getWrapper('floorDescription').show();
                    self.getWrapper('roomDescription').show();
                    this.getWrapper('doNotDisclose').show();
                    self.getWrapper('startDate').show();
                    self.initCounty();
                    self.initCountry();
                    self.getWrapper('fuzzyStartDate').show();
                    self.getNarrative('summary').set('text', "You're adding an address for");
                    self.getWrapper('addLocation').hide();
                    Y.one('#location_toggleAddLocation').hide();
                    Y.one('.addAddressForm .yui3-widget-ft').show();
                    selectionNode.one('ul').all('li').some(function (o) {
                        o.remove(true);
                    });
                    self.resetValidationErrors();
                    self.successMsg({
                        msgDuration: 5000,
                        msg: self.get('labels').addPersonLocationSuccessMessage,
                        msgType: 'success'
                    });
                    self.getInputs('primaryNameOrNumber').set('value', '');
                    self.getInputs('street').set('value', '');
                    self.getInputs('locality').set('value', '');
                    self.getInputs('town').set('value', '');
                    self.getInputs('postcode').set('value', '');
                    selectionNode.show();

                    newLocationModel = new newLocationModel();
                }

            });

        },

        cancelAddLocation: function (e) {

            e.preventDefault();

            var footer = Y.one('.addAddressForm .yui3-widget-ft');

            if (footer) {
                footer.show();
            }

            this.getWrapper('addLocation').hide();
            this.getNarrative('summary').set('text', "You're adding an address for");
            this.getWrapper('location').show();
            this.getInput('postcodeSearch').set('value', '');
            this.getWrapper('postcodeSearch').show();
            this.getInput('houseSearch').set('value', '');
            this.getInput('houseSearch').set('disabled', true);
            this.getWrapper('houseSearch').show();
            this.getWrapper('type').show();
            this.getWrapper('usage').show();
            this.getWrapper('roomDescription').show();
            this.getWrapper('doNotDisclose').show();
            this.getWrapper('floorDescription').show();
            this.getWrapper('startDate').show();
            this.getWrapper('fuzzyStartDate').show();
            this.get('container').one('#address_search_note').show();
            this.resetValidationErrors();

        }

    }, {

        ATTRS: {

            codedEntries: {
                value: {
                    addressCountyCode: Y.uspCategory.address.countyCode.category.codedEntries,
                    addressEndReason: Y.uspCategory.address.endReason.category.codedEntries,
                    addressType: Y.uspCategory.address.type.category.codedEntries,
                    addressUsage: Y.uspCategory.address.usage.category.codedEntries
                }
            },

            contextId: {
                value: null
            },

            contextName: {
                value: null
            },

            contextType: {
                value: null
            },

            panel: {
                value: ''
            },

            // could not find a reference for this.  Is it needed?
            messageAddLocationSuccess: {
                value: ''
            },

            templateAddLocationMsg: {
                value: '<div class="pure-alert pure-alert-block pure-alert-{msgType}"><a href="#" class="close">Close</a><h4 tabindex="0">{msgAddLocationSuccess}</h4></div>'
            },

            /**
             * @attribute nameAndIdDisplay
             * @note this might be better as a handlebars helper..
             */
            nameAndIdDisplay: {
                getter: function () {

                    var NAME_DISPLAY = '{name} ({prefix}{id})',
                        prefix = '',
                        id = '',
                        name = '';

                    switch (this.get('contextType')) {
                    case 'organisation':
                        id = this.get('contextId');
                        name = this.get('contextName');
                        prefix = 'ORG';
                        break;
                    case 'person':
                        id = this.get('targetPersonName');
                        name = this.get('targetPersonId');
                        prefix = 'PER';
                        break;
                    }

                    return Y.Lang.sub(NAME_DISPLAY, {
                        id: id,
                        name: name,
                        prefix: prefix
                    });

                }

            }

        }

    });

}, '0.0.1', {
    requires: ['yui-base',
        'event-custom',
        'form-util',
        'fuzzy-date',
        'calendar-popup',
        'model-form-link',
        'handlebars',
        'handlebars-helpers',
        'handlebars-organisation-templates',
        'usp-address-Location',
        'usp-address-NewLocation',
        'categories-address-component-CountyCode',
        'categories-address-component-EndReason',
        'categories-address-component-Type',
        'categories-address-component-Usage',
        'categories-address-component-CountryCode',
        'categories-address-component-CountrySubdivisionCode',
        'usp-configuration-CodedEntry',
        'app-address-autocomplete'
    ]
});