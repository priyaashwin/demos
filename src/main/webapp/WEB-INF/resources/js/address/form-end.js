

YUI.add('app-address-form-end', function(Y) {

		
	var E = Y.Escape,
    	
		L = Y.Lang,
    	
    	A = Y.Array,
    	
    	addressFormatters = Y.app.FormatAddressLocation;
	
	Y.namespace('app.address').EndAddressForm = Y.Base.create('endAddressForm', Y.View, [], {
		
		events :{
			 '#preSelectDateOptions li a': {
				 click: 'preSelectFuzzyDate'
			}
		},
		
		template: Y.Handlebars.templates["closePersonAddress"],
		initializer: function () {
			var model = this.get('model');

			// Re-render this view when the model changes, and destroy this view when
			// the model is destroyed.
			model.after('change', this.render, this);
			model.after('destroy', this.destroy, this);
		},
		render: function() {
			var container = this.get('container');
			var template=this.template;
			var model=this.get('model'),
			    labels=this.get('labels');
			container.setHTML(template({
				labels: this.get('labels'),
				addressLocationDescription : addressFormatters.getAddressLocationDescription(model, labels),
				modelData:model.toJSON()
			}));
			
			//get our coded entries		
			this.initEndReasons(model.get('endReason'));
			
			//Y.app.address.EndAddressForm.superclass.render.call(this);
			
			// render the fuzzy date widget
			this.fuzzyEndDate = new Y.usp.SimpleFuzzyDate({
				id:'fuzzyEnd',
				contentNode:this.getInput('fuzzyEndDate'),
				yearInput: true,
				monthInput:true,
				dayInput:  true,
				showDateLabel:false,
				calendarPopup:true,
				cssPrefix:'pure',
				minYear: 1886,
				maxYear: 9999,
				labels: this.get('fuzzyDateLabels'),
				dateValue:this.get('model').get('endDate')
			});
			  	
			this.fuzzyEndDate.render();
			
			var usageDesc='';
			var usages = Y.Object.values(Y.uspCategory.address.usage.category.getActiveCodedEntries());
			Y.Object.each(usages, function(entry, key){
				if (entry.code===model.get('usage')) {
					usageDesc=entry.name;
				}
			});
			this.getInput('usage-dd').setHTML(usageDesc);
			var typeDesc='';
			var types = Y.Object.values(Y.uspCategory.address.type.category.getActiveCodedEntries());
			Y.Object.each(types, function(entry, key){
				if (entry.code===model.get('type')) {
					typeDesc=entry.name;
				}
			});
			this.getInput('type-dd').setHTML(typeDesc);

			//set initial narrative
			this.getNarrative('summary').set('text', "You're ending an address for");
			this.getNarrative('description').set('text', this.get('nameAndIdDisplay'));

			return this;
		},
		
		getInput: function(id) {			
			return this.get('container').one('#'+id);
		},
		
		getWrapper: function(id) {
			return this.get('container').one('#'+id+'-wrapper');
		},
		
		getNarrative: function(section) {
			var node = '';
			switch(section) {
				case 'summary':
					node = this.get('container').one('#narrative .narrative-summary');
					break;
				case 'description':
					node = this.get('container').one('#narrative .narrative-description');
					break;
			}
			return node;
		},

		initEndReasons: function(val) {
			var reasons,
			    input=this.getInput('endReason-input');
			
			reasons = Y.Object.values(Y.uspCategory.address.endReason.category.getActiveCodedEntries());
			Y.FUtil.setSelectOptions(input, reasons, null, null, true, true, 'code');
			
			input.set('value',val||'');
		},
		
		getEndDate: function(){
			return this.fuzzyEndDate.getFuzzyDate();
		},
		
		preSelectFuzzyDate:  function(e){
			e.preventDefault();			
			var t=e.currentTarget, today=new Date(); 			
			if(t.hasClass('currentDate')){
				this.fuzzyEndDate.setDateAndMask(today,'DAY');
        	}
		},
		
		destructor: function(){
			this.fuzzyEndDate.destroy();
			this.fuzzyEndDate=null; 
		}
		
		
    	
	},{
		
		ATTRS: {
			
			codedEntries:{					
				value: {
					addressCountyCode: Y.uspCategory.address.countyCode.category.codedEntries,
					addressEndReason: Y.uspCategory.address.endReason.category.codedEntries,
					addressType: Y.uspCategory.address.type.category.codedEntries,						
					addressUsage: Y.uspCategory.address.usage.category.codedEntries						
				}				
			},
			
			contextId: {
				value: null
			},
	    	
			contextName: {
	    		value: null
	    	},
	    	
	    	contextType: {
	    		value: null
	    	},
	    	
	    	panel: {
	    		value: ''
	    	},
	    	
	    	
	    	/**
	    	 * @attribute nameAndIdDisplay
	    	 * @note this might be better as a handlebars helper..
	    	 */
	    	nameAndIdDisplay: {
	    		getter: function() {

	    			var NAME_DISPLAY = '{name} ({prefix}{id})',	    			
	    				prefix = '', 
	    				id='', 
	    				name='';
	    			
	    			switch(this.get('contextType')) {
	    			case 'organisation':
	    				id = this.get('contextId');
	    				name = this.get('contextName');
	    				prefix = 'ORG';
	    				break;
	    			case 'person':
	    				id = this.get('targetPersonName');
	    				name = this.get('targetPersonId');
	    				prefix = 'PER';
	    				break;
	    			}
	    			
	    			return L.sub(NAME_DISPLAY, {
	    				id: id,
	    				name: name,
	    				prefix: prefix
	    			});

	    		}
	    	
	    	}
	    	
	    }
	
	});	
	
}, '0.0.1', {
	  requires: ['yui-base',
	             'event-custom',
	             'form-util',
	             'fuzzy-date',
	             'calendar-popup',
	             'handlebars',
	             'handlebars-helpers',
	             'handlebars-person-templates',
	             'categories-address-component-CountyCode',
	             'categories-address-component-EndReason',
	             'categories-address-component-Type',
	             'base-AddressLocation-formatter',
	             'categories-address-component-Usage',]
	});