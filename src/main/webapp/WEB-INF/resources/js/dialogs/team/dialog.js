YUI.add('team-dialog', function(Y) {
    var A = Y.Array,
        L = Y.Lang,
        O = Y.Object;

    var HTML_CANCEL_BTN = '<i class="fa fa-times"></i> Cancel',
        HTML_EDIT_BTN = '<i class="fa fa-pencil-square-o"></i> Edit',
        HTML_SAVE_BTN = '<i class="fa fa-check"></i> Save';

    var TEAM_LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div class="loading-main">Please wait....</div></div></div>';
    
    Y.namespace('app.admin.team').UpdateTeamAndProfessionalRelationships = Y.Base.create('updateTeamAndProfessionalRelationships', Y.usp.relationship.TeamAndActiveMemberships, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#teamForm',
        customValidation:function(attrs){
          var errors={},
            securityRecordTypeConfigurationId=attrs.securityRecordTypeConfigurationId||'',
            labels=this.labels||{};
        
      
          if(isNaN(securityRecordTypeConfigurationId)){
            errors['securityRecordTypeConfigurationId']=labels.securityRecordTypeConfigurationIdValidationErrorMessage;
          }
          
          return errors;
        }
    },{
      ATTRS:{
        securityRecordTypeConfigurationId:{
          value:null
        }
      }
    });

    var TeamRoleTabs = Y.Base.create('teamRoleTabs', Y.View, [], {
        template: Y.Handlebars.templates["teamManageMembers"],
        initializer: function(config) {

            this.after('professionalTeamRelationshipTypesChange', function() {
                this._setRoles();
            }, this);

            this._setRoles();
            this.roleViews = [];
        },
        _setRoles: function() {
            //set roles
            this.set('roles', (this.get('professionalTeamRelationshipTypes') || []).map(function(role) {
                return ({
                    name: role.roleAName.toLowerCase(),
                    label: role.roleAName,
                    id: role.id,
                    members: role.members || [],
                    hook: role.roleAName.toLowerCase() + '-content'
                });
            }));
        },
        render: function() {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment()),
                roles = this.get('roles'),
                labels = this.get('labels'),
                personAutocompleteURL = this.get('personAutocompleteURL');

            //set the markup into the container
            contentNode.setHTML(this.template({
                roles: roles
            }));

            //now for each role - render into the pre-defined element
            this.roleViews = (roles || []).map(function(role) {
                var roleView = new Y.app.team.TabviewerRole({
                    personAutocompleteURL: personAutocompleteURL,
                    labels: labels,
                    roleMembers: role.members || []
                });

                contentNode.one('#' + role.hook).insert(roleView.render().get('container'), 'replace');

                return roleView;
            }.bind(this));

            //pop into the container
            container.append(contentNode);

            //init tabs
            this.tabview = new Y.TabView({
                srcNode: container.one('#teams-tab-view-src')
            });
            this.tabview.render(container.one('#teams-tab-view'));

            return this;
        },
        getTeamMemberRelationships: function() {
            var relationships = [];

            A.forEach((this.get('roles') || []), function(role, i) {
                var members = this.roleViews[i].get('roleMembers') || [];
                A.forEach(members, function(member) {
                    relationships.push({
                        _type: "PersonIdAndProfessionalTeamRelationshipTypeId",
                        personId: member.id,
                        professionalTeamRelationshipTypeId: role.id
                    });
                }.bind(this));
            }.bind(this));

            return relationships;
        },
        destructor: function() {
            if (this.roleViews) {
                A.each(this.roleViews, function(roleView) {
                    roleView.destroy();
                });
                delete this.roleViews;
            }
            if (this.tabView) {
                this.tabView.destroy();

                delete this.tabView;
            }
        }
    }, {
        ATTRS: {
            professionalTeamRelationshipTypes: {
                value: []
            },
            roles: {
                value: []
            },
            personAutocompleteURL: {
                value: ''
            }
        }
    });

    var TeamAddEditIdentityPanel = Y.Base.create('teamAddEditIdentityPanel', Y.View, [], {
      template: Y.Handlebars.templates["teamIdentityPanel"],
      events: {
        '#sector-input': {
          change: '_onSectorChange'
        }
      },
      initializer:function(config){
          var labels=config.labels;
          
          this.securityProfileAutoCompleteView = new Y.app.admin.security.profile.SecurityProfileAutoCompleteResultView({
            autocompleteURL: this.get('securityProfileAutocompleteURL'),
            inputName:'securityRecordTypeConfigurationId',
            formName:config.formName,
            labels: {
                placeholder:labels.securityProfilePlaceholder 
            }
          });
          
          this.get('model').after('load', function(){
            var securityConfigurationId=this.get('model').get('securityConfigurationId'),
                securityConfigurationName=this.get('model').get('securityConfigurationName');
            
            if(!(securityConfigurationId===undefined||securityConfigurationId===null)){
               this.securityProfileAutoCompleteView.set('selectedSecurityProfile',{
                 id:securityConfigurationId,
                 name:securityConfigurationName
               });
            }
            
          }, this);
          this.securityProfileAutoCompleteView.addTarget(this);
        },
        render: function() {
          var container = this.get('container'),
              contentNode = Y.one(Y.config.doc.createDocumentFragment()),
              model=this.get('model');
          
          
          contentNode.setHTML(this.template({
                  modelData: model.toJSON(),
                  labels: this.get('labels'),
                  isAdd:model.isNew()
          }));
            
          if(this.securityProfileAutoCompleteView){
            //add our fragments to the content node
            contentNode.one('#securityProfileContent').append(this.securityProfileAutoCompleteView.render().get('container'));
          }
          
          //set the fragment into the view container
          container.setHTML(contentNode);

          this.initSectors();

          this.initRegion(model);
          
          return this;

        },
        initRegion : function(){
          var container = this.get('container'),
              regionInput = container.one('#region-input'),
              model = this.get('model');
          
          if (regionInput){
             var regionCategory = Y.uspCategory.organisation.teamRegion.category,
                 selectedRegion = model.get('region'),
                 region = this.lookupExistingEntry(regionCategory, selectedRegion);

             Y.FUtil.setSelectOptions(regionInput,  O.values(regionCategory.getActiveCodedEntries()), region.code, region.name, true, true, 'code');
             if (selectedRegion) {
                regionInput.set('value', selectedRegion);
             }
          }
        },
        lookupExistingEntry: function (codes, val) {
          var codedEntry, entry = {
            code: null,
            name: null
          };
          if (!(typeof val === 'undefined' || val === '' || val === null)) {
             codedEntry = codes[val];
             if (codedEntry) {
               entry.code = codedEntry.code;
               entry.name = codedEntry.name;
             }
          }
          return entry;
        },
        destructor: function() {
          if (this.securityProfileAutoCompleteView) {
              this.securityProfileAutoCompleteView.removeTarget(this);
              this.securityProfileAutoCompleteView.destroy();
              delete this.securityProfileAutoCompleteView;
          }
        },
        _onSectorChange: function (e) {
          var selectElement = e.currentTarget._node,
            selectedIndex = selectElement.selectedIndex;
          if (selectedIndex >= 0) {
            this._setSectorSubTypeOptions(selectElement.options[selectedIndex].value, null, 'write');
          }
        },
        initSectors: function () {
          var container = this.get('container'),
            model = this.get('model'),
            sector = this.lookupExistingEntry(Y.uspCategory.organisation.teamSector.category.codedEntries, model.get('sector')),
            sectorSubType = this.lookupExistingEntry(Y.uspCategory.organisation.teamSectorSubType.category.codedEntries, model.get('sectorSubType'));
          Y.FUtil.setSelectOptions(container.one('#sector-input'), O.values(Y.uspCategory.organisation.teamSector.category.getActiveCodedEntries(null, 'write')), sector.code, sector.name, true, true, 'code');

          var parentCode = model.get('sector');
          this._setSectorSubTypeOptions(parentCode, sectorSubType, 'write');

          container.one('#sector-input').set('value', parentCode);
          container.one('#sectorSubType-input').set('value', model.get('sectorSubType'));
        },
        _setSectorSubTypeOptions: function (parentCode, sectorSubType, accessLevel) {
          var container = this.get('container'),
            labels = this.get('labels'),
            sectorSubTypeCode = sectorSubType ? sectorSubType.code : null,
            sectorSubTypeName = sectorSubType ? sectorSubType.name : null;

          var currentCodedEntry = Y.uspCategory.organisation.teamSector.category.code,
            sectorSubTypeSelector = container.one('#sectorSubType-input'),
            sectorSubTypes = O.values(Y.uspCategory.organisation.teamSectorSubType.category.getActiveCodedEntriesForParent(parentCode, currentCodedEntry, accessLevel)),
            blankMessage = sectorSubTypes.length < 1 ? labels.noEntryAvailable : labels.noEntrySelected;

          Y.FUtil.setSelectOptions(sectorSubTypeSelector, sectorSubTypes, sectorSubTypeCode, sectorSubTypeName, blankMessage, true, 'code');
        }
    });

    var TeamManageMembersView = Y.Base.create('teamManageMembersView', Y.View, [], {
        initializer: function(config) {
            this.teamRoleTabs = new TeamRoleTabs(config);
            this.teamRoleTabs.addTarget(this);

            //push change in relationships down to tab view
            this.after('professionalTeamRelationshipTypesChange', function(e) {
                this.teamRoleTabs.set('professionalTeamRelationshipTypes', e.newVal);
            }, this);
        },
        destructor: function() {
            if (this.teamRoleTabs) {
                this.teamRoleTabs.removeTarget(this);
                this.teamRoleTabs.destroy();

                delete this.teamRoleTabs;
            }
        },
        render: function() {
            var container = this.get('container');

            container.setHTML(this.teamRoleTabs.render().get('container'));

            return this;
        },
        getTeamMemberRelationships: function() {
            return this.teamRoleTabs.getTeamMemberRelationships();
        }
    }, {
        ATTRS: {
            personAutocompleteURL: {}
        }
    });

    Y.namespace('app.admin.team').TeamAddEditView = Y.Base.create('teamAddEditView', Y.View, [], {
        template: Y.Handlebars.templates["addEditTeam"],
        initializer: function(config) {
            this.teamManageMembersView=new TeamManageMembersView(config);
          
            this.teamIdentityPanel = new TeamAddEditIdentityPanel(Y.merge(config,{
              formName:'teamForm'
            }));
            this.teamIdentityPanel.addTarget(this);
        
            this.get('model').after('*:load', function() {
                this.populateMembers();
                this.render();
            }, this);
        },
        populateMembers: function(e) {
            var model = this.get('model'),
                professionalTeamRelationshipTypes = this.get('professionalTeamRelationshipTypes');

            A.each(model.get('activeMemberships') || [], function(item) {
                var prt = A.find(professionalTeamRelationshipTypes, function(i) {
                    return item.personOrganisationRelationshipTypeVO.id === i.id
                });

                if (prt) {
                    prt.members = prt.members || [];
                    prt.members.push(item.personVO)
                } else {
                    Y.Log('unknown professional relationship');
                }
            }.bind(this));

            //update attribute - to force change handlers
            this.teamManageMembersView.set('professionalTeamRelationshipTypes', professionalTeamRelationshipTypes);
        },
        render: function() {
          var container = this.get('container'),
          contentNode = Y.one(Y.config.doc.createDocumentFragment());
      
      
          contentNode.setHTML(this.template({
                  modelData: this.get('model').toJSON(),
                  labels: this.get('labels')
          }));
      
          
          //add our fragments to the content node
          contentNode.one('#identityPanel').append(this.teamIdentityPanel.render().get('container'));
          contentNode.one('#rolesPanel').append(this.teamManageMembersView.render().get('container'));
          
          //set the fragment into the view container
          container.setHTML(contentNode);

          // the base-autocomplete.js code is forcing focus onto the ac search field at the bottom of the view..
          // the following code is needed to force it back up to the name input node.
          Y.later(105, this, function() {
            if(container.one('#name-input') !== null) container.one('#name-input').focus();
          }.bind(this));
          
          return this;
        },
        destructor:function(){
          if (this.teamIdentityPanel) {
            this.teamIdentityPanel.removeTarget(this);
            this.teamIdentityPanel.destroy();

            delete this.teamIdentityPanel;
          }
          
          if(this.teamManageMembersView){
            this.teamManageMembersView.removeTarget(this);
            this.teamManageMembersView.destroy();
            delete this.teamManageMembersView;
          }
        },
        getTeamMemberRelationships:function(){
          return this.teamManageMembersView.getTeamMemberRelationships();
        }
    },{
      ATTRS:{
        securityProfileAutocompleteURL:{
          value:''
        }
      }
    });

    Y.namespace('app.admin.team').TeamViewView = Y.Base.create('teamViewView', Y.View, [], {
        template:Y.Handlebars.templates["viewTeam"],
        initializer: function(config) {
            var model = this.get('model');
            teamId = model.get('id');

            this.teamMembersResults = new Y.app.admin.team.TeamMembersResults(Y.merge(config, {
                teamId: teamId
            }));

            this.teamMembersResults.addTarget(this);

            //re-align the dialog on a load event
            this.after('*:load', function(e) {
                //fire the align - so this outside of the current thread
                Y.later(10, this, function() {
                    this.fire('align')
                }.bind(this));
            }, this);
        },
        render: function() {
          var container = this.get('container'),
              contentNode = Y.one(Y.config.doc.createDocumentFragment());
          
          
            contentNode.setHTML(this.template({
                    modelData: this.get('model').toJSON(),
                    labels: this.get('labels'),
                    codedEntries: this.get('codedEntries')
            }));
            contentNode.append(this.teamMembersResults.render().get('container'));

            container.setHTML(contentNode);
            return this;
        },
        destructor: function() {
            if (this.teamMembersResults) {
                this.teamMembersResults.removeTarget(this);
                this.teamMembersResults.destroy();

                delete this.teamMembersResults;
            }
        }

    }, {
       ATTRS: {
           codedEntries: {
               value: {
                  region: Y.uspCategory.organisation.teamRegion.category.codedEntries,
                  sector: Y.uspCategory.organisation.teamSector.category.codedEntries,
                  sectorSubType: Y.uspCategory.organisation.teamSectorSubType.category.codedEntries
               }
           }
       }
    });

    var TeamProcessingView = Y.Base.create('teamProcessingView', Y.View, [], {
        render: function() {
            var container = this.get('container');
            container.setHTML(TEAM_LOADING_TEMPLATE);
            return this;
        }
    });
    Y.namespace('app.admin.team').TeamDialog = Y.Base.create('teamDialog', Y.usp.app.AppDialog, [], {
        views: {
            teamProcessing: {
                type: TeamProcessingView
            },
            editTeam: {
                type: Y.app.admin.team.TeamAddEditView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: HTML_SAVE_BTN,
                    action: function(e) {
                        var view = this.get('activeView'),
                            teamMemberRelationships = view.getTeamMemberRelationships();
                        this.handleSave(e, {
                            teamRelationships: teamMemberRelationships
                        }, {
                            transmogrify: Y.usp.relationship.UpdateTeamAndProfessionalRelationships
                        });
                    },
                    disabled: true
                }]
            },
            viewTeam: {
                type: Y.app.admin.team.TeamViewView,
                buttons: [{
                    action: function(e) {
                        var view = this.get('activeView');
                        e.preventDefault();

                        this.fire('edit', {
                            record: view.get('model')
                        });
                    },
                    name: 'editButton',
                    labelHTML: HTML_EDIT_BTN,
                    isActive: true,
                    disabled: true
                }]
            }
        },
        getTeamRelationshipTypesPromise: function() {
            return new Y.Promise(function(resolve, reject) {
                //the result is cached against this object
                if (!this.professionalTeamRelationshipTypes) {
                    var model = new Y.usp.relationship.ProfessionalTeamRelationshipType({
                        url: this.get('professionalTeamRelationshipTypeUrl')
                    });

                    var data = model.load(function(err) {
                        if (err !== null) {
                            //failed for some reason so reject the promise
                            reject(err);
                        }
                        this.professionalTeamRelationshipTypes = data.toJSON();
                        //success, so resolve the promise
                        resolve(this.professionalTeamRelationshipTypes);
                    });
                } else {
                    resolve(this.professionalTeamRelationshipTypes);
                }
            }.bind(this));
        }
    }, {
        ATTRS: {
            professionalTeamRelationshipTypeUrl: {

            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-dialog',
        'form-util',
        'handlebars',
        'handlebars-helpers',
        'handlebars-organisation-templates',
        'usp-organisation-Organisation',
        'usp-relationship-ProfessionalTeamRelationshipType',
        'usp-relationship-UpdateTeamAndProfessionalRelationships',
        'usp-relationship-TeamAndActiveMemberships',
        'promise',
        'team-tabviewer-role',
        'tabview',
        'team-members-view',
        'security-profile-autocomplete-view',
        'categories-organisation-component-TeamRegion',
        'categories-organisation-component-TeamSector',
        'categories-organisation-component-TeamSectorSubType'
    ]
});