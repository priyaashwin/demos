YUI.add('aftercare-dialog', function (Y) {

    'use-strict';

    var O = Y.Object,
        L = Y.Lang;

    Y.namespace('app.aftercare').NewAfterCareForm = Y.Base.create('newAfterCareForm', Y.usp.careleaver.NewAfterCare, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#aftercareAddForm'
    });

    Y.namespace('app.aftercare').EndAfterCareForm = Y.Base.create('endAfterCareForm', Y.usp.Model, [], {
        form: '#aftercareEndForm'
    });

    Y.namespace('app.aftercare').NewAfterCareContactForm = Y.Base.create('newAfterCareContactForm', Y.usp.careleaver.NewAfterCareContact, [Y.usp.ModelFormLink], {
        form: '#aftercareAddContactForm'
    });

    Y.namespace('app.aftercare').NewAfterCareProvisionForm = Y.Base.create('newAfterCareProvisionForm', Y.usp.careleaver.NewAfterCareProvision, [Y.usp.ModelFormLink], {
        form: '#aftercareProvisionAddForm'
    });

    Y.namespace('app.aftercare').UpdateAfterCareProvisionForm = Y.Base.create('updateAfterCareProvisionForm', Y.usp.careleaver.AfterCareProvision, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#aftercareProvisionEditForm'
    });

    Y.namespace('app.aftercare').NewAfterCareView = Y.Base.create('NewAfterCareView', Y.View, [], {
        template: Y.Handlebars.templates["afterCareAddDialog"],
        initializer: function () {
            this._evtHandlers = [
                this.get('container').delegate('change', this._handleReceivingAfterCareChange, '#receivingAfterCare', this)
            ]
        },
        render: function () {
            var container = this.get('container'),
                aftercareEligibility = this.get('subject').eligibility,
                labels = this.get('labels'),
                html = this.template({
                    labels: labels
                });

            container.setHTML(html);

            this.contactDateCalendar = new Y.usp.CalendarPopup({
                inputNode: this.get('container').one('#contactDate'),
                'maximumDate': new Date()
            });
            this.contactDateCalendar.render();

            this._initEligibility(aftercareEligibility, labels);

            this._initEconomicActivity();
            this._initAccommodation();
            this._initEndReasons();

            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            if (this.contactDateCalendar) {
                this.contactDateCalendar.destroy();
                delete this.contactDateCalendar;
            }
        },
        _initEligibility: function (eligibility, labels) {
            var aftercareEligibilityNode = this.get('container').one('#aftercareEligibility'),
                eligibilityLabel = (eligibility === 'COMPULSORY' ? labels.eligibilityDesc.compulsory : labels.eligibilityDesc.discretionary);

            if (aftercareEligibilityNode) {
                aftercareEligibilityNode.set('innerHTML', eligibilityLabel);
            }
        },
        _initEconomicActivity: function () {
            var economicActivityNode = this.get('container').one('#economicActivity'),
                options = Y.Object.values(Y.uspCategory.careleaver.aftercareEconomicActivity.category.getActiveCodedEntries());

            if (economicActivityNode && options) {
                Y.FUtil.setSelectOptions(economicActivityNode, options, null, null, true, true, 'code');
                economicActivityNode.removeClass('hidden-nav');
                economicActivityNode.removeAttribute('disabled');
            }
        },
        _initAccommodation: function () {
            var accommodationNode = this.get('container').one('#accommodation'),
                options = Y.Object.values(Y.uspCategory.careleaver.aftercareAccommodationType.category.getActiveCodedEntries());

            if (accommodationNode && options) {
                Y.FUtil.setSelectOptions(accommodationNode, options, null, null, true, true, 'code');
                accommodationNode.removeClass('hidden-nav');
                accommodationNode.removeAttribute('disabled');
            }
        },
        _initEndReasons: function () {
            var endReasonsNode = this.get('container').one('#servicesEndReason'),
                options = Y.Object.values(Y.uspCategory.careleaver.aftercareServiceEndReason.category.getActiveCodedEntries());

            if (endReasonsNode && options) {
                Y.FUtil.setSelectOptions(endReasonsNode, options, null, null, true, true, 'code');
            }
        },
        _handleReceivingAfterCareChange: function (e) {

            var value = e.target.get('value'),
                container = this.get('container'),
                endReasonNode = container.one('#endReason'),
                servicesNode = container.one('#servicesWrapper');

            if (value === 'true') {
                endReasonNode.addClass('hidden-nav');
                servicesNode.removeClass('hidden-nav');
                this.setAttrs({ receivingServices: true });
            } else {
                endReasonNode.removeClass('hidden-nav');
                servicesNode.addClass('hidden-nav');
                this.setAttrs({ receivingServices: false });
            }
        }

    }, {
            ATTRS: {
                receivingServices: {
                    value: true
                }
            }
        });

    Y.namespace('app.aftercare').EndAfterCareView = Y.Base.create('endAfterCareView', Y.usp.careleaver.AfterCareProvisionView, [], {
        template: Y.Handlebars.templates["afterCareEndDialog"],
        render: function () {

            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels')
                }),
                afterCare = this.get('otherData').afterCare;

            container.setHTML(html);

            this.endDateCalendar = new Y.usp.CalendarPopup({
                inputNode: this.get('container').one('#endDate'),
                'maximumDate': new Date(),
                'minimumDate': afterCare.contactDate
            });

            this.endDateCalendar.on('dateSelection', this._endDateChange, this);
            this.endDateCalendar.render();

            Y.FUtil.setSelectOptions(container.one('#endReason'), O.values(Y.uspCategory.careleaver.aftercareServiceEndReason.category.getActiveCodedEntries()), null, null, true, true, 'code');

            return this;
        },

        _endDateChange: function (e) {
            this.fire('dateSelected', e);
        },
        destructor: function () {
            if (this.endDateCalendar) {
                // destroy the calendar
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
        }
    }, {
            ATTRS: {}
        });

    var BaseCarerView = Y.Base.create('baseCarerView', Y.View, [], {
        template: Y.Template.Micro.compile('<div id="subjectSearchWrapper"></div>'),
        getAutoCompleteViewConstructor: function () {
            throw new TypeError('Please implement getAutoCompleteViewConstructor');
        },
        initializer: function (config) {
            this.autoCompleteView = new (this.getAutoCompleteViewConstructor())({
                containingNodeSelector: config.containingNodeSelector,
                autocompleteURL: config.autocompleteURL,
                inputName: 'carerSubjectSearch',
                formName: 'aftercareProvisionAddForm',
                rowTemplate: 'ROW_TEMPLATE_SIMPLE',
                labels: config.labels
            });

            this.autoCompleteView.addTarget(this);
        },
        render: function () {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //render the main template
            contentNode.setHTML(this.template());

            //add our fragments to the content node
            contentNode.one('#subjectSearchWrapper').append(this.autoCompleteView.render().get('container'));

            //set the fragment into the view container
            container.setHTML(contentNode);

            return this;
        },
        destructor: function () {
            this.autoCompleteView.removeTarget(this);
            this.autoCompleteView.destroy();
            delete this.autoCompleteView;
        }
    }, {
            ATTRS: {
                autocompleteURL: {}
            }
        });

    var PersonAutocompleteView = Y.Base.create('personAutocompleteView', BaseCarerView, [], {
        getAutoCompleteViewConstructor: function () {
            return Y.app.PersonAutoCompleteResultView;
        }
    });

    var OrganisationAutocompleteView = Y.Base.create('organisationAutocompleteView', BaseCarerView, [], {
        getAutoCompleteViewConstructor: function () {
            return Y.app.OrganisationAutoCompleteResultView;
        }
    });

    Y.namespace('app.aftercare').NewAfterCareProvisionView = Y.Base.create('newAfterCareProvisionView', Y.usp.View, [], {
        events: {
            '#provisionType': {
                'change': '_handleProvisionTypeChange'
            },
            'input[name="supportedLodgingsCarerSubjectType"]': {
                'change': '_handleSupportedLodgingsCarerSubjectTypeChange'
            }
        },
        template: Y.Handlebars.templates["afterCareProvisionAddDialog"],
        initializer: function () {
            this.startDateCalendar = new Y.usp.CalendarPopup({
                'maximumDate': new Date()
            }).addTarget(this);

            this.endDateCalendar = new Y.usp.CalendarPopup({
                minimumDateFieldDependency: '#aftercareProvisionAddForm input[name="startDate"]',
                'maximumDate': new Date()
            }).addTarget(this);
        },
        render: function () {
            var container = this.get('container');

            Y.app.aftercare.NewAfterCareProvisionView.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]')
            }).render();

            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]')
            }).render();

            this._initProvisionTypes();
            this._renderCarerView();

            return this;
        },
        destructor: function () {
            if (this.startDateCalendar) {
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }

            if (this.endDateCalendar) {
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }

            if (this.carerView) {
                this.carerView.removeTarget(this);
                this.carerView.destroy();
                delete this.carerView;
            }
        },
        _initProvisionTypes: function () {
            var node = this.get('container').one('#provisionType'),
                defaultValue = "CONTINUING_CARE";

            if (node) {
                Y.FUtil.setSelectOptions(node, O.values(Y.usp.careleaver.enum.AfterCareProvisionType.values), null, null, true, true, 'enumValue');
                node.set('value', defaultValue);
            }
        },
        _handleProvisionTypeChange: function (e) {
            var container = this.get('container');

            if (e.target.get('value') === 'CONTINUING_CARE') {
                container.one('#supportedLodgingsCarerSubjectTypeWrapper').setStyle('display', 'none');
            } else {
                container.one('#supportedLodgingsCarerSubjectTypeWrapper').setStyle('display', 'block');
            }

            this._renderCarerView();
        },
        _handleSupportedLodgingsCarerSubjectTypeChange: function (e) {
            this._renderCarerView();
        },
        _renderCarerView: function () {
            var container = this.get('container'),
                carerControlType = 'PERSON',
                provisionType = container.one('#provisionType').get('value'),
                supportLodgingsSubjectType = container.one('input[name="supportedLodgingsCarerSubjectType"]:checked').get('value');

            //destroy existing view
            if (this.carerView) {
                this.carerView.removeTarget(this);
                this.carerView.destroy();
            }

            if (provisionType === 'SUPPORTED_LODGINGS' && supportLodgingsSubjectType === 'ORGANISATION') {
                carerControlType = 'ORGANISATION';
            }

            this.carerView = this._getNewCarerView(carerControlType).addTarget(this);
            container.one('#carerSubjectSearchWrapper').setHTML(this.carerView.render().get('container'));
        },
        _getNewCarerView: function (controlType) {
            var carerView,
                labels = this.get('labels') || {};

            switch (controlType) {
                case 'PERSON':
                    carerView = new PersonAutocompleteView({
                        carerType: 'PERSON',
                        autocompleteURL: this.get('personCarerAutocompleteURL'),
                        labels: {
                            placeholder: 'Enter name or ID'
                        }
                    });
                    break;
                case 'ORGANISATION':
                    carerView = new OrganisationAutocompleteView({
                        carerType: 'ORGANISATION',
                        autocompleteURL: this.get('organisationCarerAutocompleteURL'),
                        labels: {
                            placeholder: 'Enter name or ID'
                        }
                    });
                    break;
            }

            return carerView;
        }
    });

    Y.namespace('app.aftercare').ViewAfterCareProvisionView = Y.Base.create('viewAfterCareProvisionView', Y.usp.View, [], {
        template: Y.Handlebars.templates["afterCareProvisionViewDialog"],
    }, {
            ATTRS: {
                enumTypes: {
                    value: {
                        afterCareProvisionType: Y.usp.careleaver.enum.AfterCareProvisionType.values
                    }
                }
            }
        });

    Y.namespace('app.aftercare').UpdateAfterCareProvisionView = Y.Base.create('updateAfterCareProvisionView', Y.usp.View, [], {
        template: Y.Handlebars.templates["afterCareProvisionEditDialog"],
        initializer: function () {
            this.endDateCalendar = new Y.usp.CalendarPopup(({
                maximumDate: new Date()
            })).addTarget(this);
        },
        render: function () {
            Y.app.aftercare.UpdateAfterCareProvisionView.superclass.render.call(this);
            var model = this.get('model');
            this.endDateCalendar.setAttrs({
                minimumDate: model.get('startDate'),
                inputNode: this.get('container').one('input[name="endDate"]')
            }).render();

            return this;
        },
        destructor: function () {
            if (this.endDateCalendar) {
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
        }
    }, {
            ATTRS: {
                enumTypes: {
                    value: {
                        afterCareProvisionType: Y.usp.careleaver.enum.AfterCareProvisionType.values
                    }
                }
            }
        });

    Y.namespace('app.aftercare').NewAfterCareContactView = Y.Base.create('newAfterCareContactView', Y.View, [], {
        template: Y.Handlebars.templates["afterCareAddContactDialog"],
        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels')
                });

            container.setHTML(html);

            this.contactDateCalendar = new Y.usp.CalendarPopup({
                inputNode: this.get('container').one('#contactDate'),
                'maximumDate': new Date()
            });
            this.contactDateCalendar.render();

            this._initEconomicActivity();
            this._initAccommodation();

            return this;
        },
        destructor: function () {
            if (this.contactDateCalendar) {
                this.contactDateCalendar.destroy();
                delete this.contactDateCalendar;
            }
        },
        _initEconomicActivity: function () {
            var economicActivityNode = this.get('container').one('#economicActivity'),
                options = Y.Object.values(Y.uspCategory.careleaver.aftercareEconomicActivity.category.getActiveCodedEntries()),
                currentEconomicActivity = this.get('otherData').currentEconomicActivity.economicActivity;

            if (economicActivityNode && options) {
                Y.FUtil.setSelectOptions(economicActivityNode, options, null, null, true, true, 'code');
                economicActivityNode.set('value', currentEconomicActivity);
            }
        },
        _initAccommodation: function () {
            var accommodationNode = this.get('container').one('#accommodationType'),
                options = Y.Object.values(Y.uspCategory.careleaver.aftercareAccommodationType.category.getActiveCodedEntries()),
                currentAccommodation = this.get('otherData').currentAccommodation.accommodationType;

            if (accommodationNode && options) {
                Y.FUtil.setSelectOptions(accommodationNode, options, null, null, true, true, 'code');
                accommodationNode.set('value', currentAccommodation);
            }
        }

    }, {
            ATTRS: {}
        });

    Y.namespace('app.aftercare').AfterCareDialog = Y.Base.create('aftercareDialog', Y.usp.app.AppDialog, [], {


        handleAddAfterCareSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                model = view.get('model'),
                labels = view.get('labels'),
                afterCareEligibility = view.get('otherData').subject.eligibility,
                isReceivingServices = view.get('receivingServices'),
                data = {
                    personId: view.get('otherData').subject.subjectId,
                    eligibility: afterCareEligibility
                },
                opts = {};

            if (isReceivingServices) {
                data.accommodation = container.one('#accommodation').get('value');
                data.economicActivity = container.one('#economicActivity').get('value');
                opts = {
                    transmogrify: Y.usp.careleaver.NewAfterCareReceivingServices
                };
            } else {
                data.endOfServicesReason = container.one('#servicesEndReason').get('value');
                opts = {
                    transmogrify: Y.usp.careleaver.NewAfterCareNotReceivingServices
                };
            }

            return Y.when(this.handleSave(e, data, opts));
        },
        handleEndAfterCareSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                endDate = container.one('#endDate').get('value'),
                parsedEndDate = Y.USPDate.parseDateTime(endDate).getTime(),
                endReason = container.one('#endReason').get('value'),
                aftercareId = view.get('otherData').aftercareId;

            return Y.when(this.handleSave(e, {
                id: aftercareId,
                endDate: parsedEndDate,
                endReason: endReason
            }));
        },
        handleAddAfterCareProvisionSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                subjectType = view.carerView.get('carerType'),
                selectedCarer = null,
                carerId = null;

            selectedCarer = (subjectType === 'PERSON') ? view.carerView.autoCompleteView.get('selectedPerson') : view.carerView.autoCompleteView.get('selectedOrganisation');
            carerId = (selectedCarer) ? selectedCarer.id : null;

            return Y.when(this.handleSave(e, {
                afterCareId: view.get('otherData').aftercareId,
                subjectType: subjectType,
                carerId: carerId
            }));
        },
        handleUpdateAfterCareProvisionSave: function (e) {
            var view = this.get('activeView');
            view.get('model').url = view.get('submitUrl');

            this.handleSave(e, {}, {
                transmogrify: Y.usp.careleaver.UpdateEndAfterCareProvision
            });
        },
        handleAddAfterCareContactSave: function (e) {
            var view = this.get('activeView'),
                model = view.get('model'),
                aftercareId = view.get('otherData').aftercareId;

            model.url = model.url.replace('{id}', aftercareId);
            return Y.when(this.handleSave(e));
        },
        views: {
            add: {
                type: Y.app.aftercare.NewAfterCareView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddAfterCareSave',
                    disabled: true
                }],
            },
            view: {
                type: Y.app.aftercare.ViewAfterCareView
            },
            end: {
                type: Y.app.aftercare.EndAfterCareView,
                buttons: [{
                    name: 'endButton',
                    labelHTML: '<i class="fa fa-check"></i> End Record',
                    action: 'handleEndAfterCareSave',
                    disabled: true
                }],
            },
            addProvision: {
                type: Y.app.aftercare.NewAfterCareProvisionView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddAfterCareProvisionSave',
                    disabled: true
                }]
            },
            viewProvision: {
                type: Y.app.aftercare.ViewAfterCareProvisionView,
                buttons: [{
                    name: 'editButton',
                    labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                    isActive: true,
                    action: function (e) {
                        this.fire('updateAfterCareProvision', {
                            aftercareProvisionId: this.get('activeView').get('model').get('id')
                        });
                    },
                    disabled: true
                }]
            },
            editProvision: {
                type: Y.app.aftercare.UpdateAfterCareProvisionView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleUpdateAfterCareProvisionSave',
                    disabled: true
                }]
            },
            addContact: {
                type: Y.app.aftercare.NewAfterCareContactView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddAfterCareContactSave',
                    disabled: true
                }]
            }
        }
    });

}, '0.0.1', {
        requires: [
            'yui-base',
            'view',
            'app-dialog',
            'model-form-link',
            'model-transmogrify',
            'entry-dialog-views',
            'handlebars',
            'handlebars-helpers',
            'handlebars-aftercare-templates',
            'calendar-popup',
            'person-autocomplete-view',
            'organisation-autocomplete-view',
            'usp-careleaver-NewAfterCare',
            'usp-careleaver-NewAfterCareReceivingServices',
            'usp-careleaver-NewAfterCareNotReceivingServices',
            'usp-careleaver-NewAfterCareProvision',
            'usp-careleaver-NewAfterCareContact',
            'usp-careleaver-AfterCare',
            'usp-careleaver-AfterCareProvision',
            'usp-careleaver-UpdateEndAfterCareProvision',
            'categories-careleaver-component-AftercareEconomicActivity',
            'categories-careleaver-component-AftercareAccommodationType',
            'categories-careleaver-component-AftercareServiceEndReason',
            'careleaver-component-enumerations',
        ]
    });