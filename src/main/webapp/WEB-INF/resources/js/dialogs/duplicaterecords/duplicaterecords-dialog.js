YUI.add('duplicaterecords-dialog-views', function (Y) {
    'use-strict';

    var O = Y.Object,
        L = Y.Lang;

    Y.namespace('app.admin.duplicaterecords').LinkDuplicateRecordsView = Y.Base.create('linkDuplicateRecordsView', Y.usp.person.UpdatePersonDuplicatesView, [], {
        template: Y.Handlebars.templates["duplicateRecordsLinkDialog"]
    });

    Y.namespace('app.admin.duplicaterecords').UnlinkDuplicateRecordForm = Y.Base.create('unlinkDuplicateRecordsForm',
        Y.usp.person.UpdatePersonDuplicates, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#unlinkDuplicateRecordsForm'
        });

    Y.namespace('app.admin.duplicaterecords').UnlinkDuplicateRecordsView = Y.Base.create('unlinkDuplicateRecordsView', Y.usp.person.UpdatePersonDuplicatesView, [], {
        template: Y.Handlebars.templates["duplicateRecordsUnlinkDialog"]
    });

    Y.namespace('app.admin.duplicaterecords').DuplicateRecordsDialog = Y.Base.create('duplicateRecordsDialog', Y.usp.app.AppDialog, [], {
        handleLinkDuplicateRecords: function (e) {
            var duplicateRecordList = this.get('activeView').get('otherData').duplicateRecordList;
            var primaryDuplicateRecordId = this.get('activeView').get('otherData').primaryDuplicateRecordId;

            return Y.when(this.handleSave(e, {
                id: primaryDuplicateRecordId,
                duplicatePersonIds: duplicateRecordList
            }));
        },
        views: {
            link: {
                type: Y.app.admin.duplicaterecords.LinkDuplicateRecordsView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    isPrimary: true,
                    action: 'handleLinkDuplicateRecords',
                    disabled: true
                }]
            },
            unlink: {
                type: Y.app.admin.duplicaterecords.UnlinkDuplicateRecordsView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    isPrimary: true,
                    action: 'handleRemove',
                    disabled: true
                }]
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'handlebars',
        'handlebars-duplicaterecords-templates',
        'usp-person-Person',
        'usp-person-UpdatePersonDuplicates'
    ]
});