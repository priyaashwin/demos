YUI.add('rule-definition-dialog-views', function(Y) {
    var L = Y.Lang,
        E = Y.Escape;

    //Status change rule (used for all status changes) - simple YUI form
    Y.namespace('app').RuleDefinitionStatusChangeForm = Y.Base.create('ruleDefinitionStatusChangeForm', Y.Model, [Y.ModelSync.REST], {});
        
    //Form for 'remove' rule definition
    Y.namespace('app').RuleDefinitionRemoveForm = Y.Base.create('ruleDefinitionRemoveForm', Y.Model, [Y.ModelSync.REST], {});
    
    Y.namespace('app').RuleDefinitionStatusChangeView = Y.Base.create('ruleDefinitionStatusChangeView', Y.View, [], {
        template: Y.Handlebars.templates["ruleDefinitionStatusChangeDialog"],
        initializer: function() {
            //publish events
            this.publish('saved', {
                preventable: true,
                broadcast: true
            });
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    modelData: this.get('model').toJSON(),
                    otherData: this.get('otherData'),
                    message: this.get('message')
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            return this;
        }
    }, {
        ATTRS: {
            model: {},
            /**
             * @attribute otherData Should contain the narrative attribute, along with any other data required by the template
             */
            otherData: {},
            message: {}
        }
    });
    
    Y.namespace('app').RuleDefinitionDialog = Y.Base.create('ruleDefinitionDialog', Y.usp.app.AppDialog, [], {
        views: {            
            publishRule: {
                type: Y.app.RuleDefinitionStatusChangeView,
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Publish',
                    classNames: 'pure-button-primary',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            deleteRule: {
                type: Y.app.RuleDefinitionStatusChangeView,
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Delete',
                    classNames: 'pure-button-primary',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            removeRule: {
                type: Y.app.RuleDefinitionStatusChangeView,
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'removeButton',
                    labelHTML: '<i class="fa fa-check"></i> Remove',
                    classNames: 'pure-button-primary',
                    action: 'handleRemove',
                    disabled: true
                }]
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base', 
               'view', 
               'app-dialog',
               'model-form-link', 
               'model-transmogrify', 
               'form-util', 
               'handlebars-helpers', 
               'usp-rule-NewRuleDefinition', 
               'usp-rule-RuleDefinition',
               'handlebars-rule-templates']
});