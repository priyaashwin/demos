YUI.add('checklist-instance-priority-dialog-views', function(Y) {

  Y.namespace('app').ChecklistInstancePriorityDialogView = Y.Base.create('checklistInstancePriorityDialogView', Y.usp.ReactView, [], {
      reactFactoryType: uspChecklistTaskAndPriority.UpdateChecklistPriorityDialog,
      renderComponent: function () {
          var labels = this.get('labels'),
              enums = this.get('enumTypes'),
              dialogConfig = this.get('dialogConfig'),
              urls = this.get('urls'),
              permissions = this.get('permissions');
              
          if (this.reactAppContainer) {
              var component;

              ReactDOM.render(this.componentFactory({
                  labels: labels,
                  enums: enums,
                  dialogConfig: dialogConfig,
                  urls: urls,
                  permissions: permissions,
                  handlePrioritiseSave: this.handlePrioritiseSave.bind(this)
              }), this.reactAppContainer.getDOMNode(), function () {
                  component = this;
              });

              this.component = component;
          }
      },
      showDialog: function(checklistId) {
        if (this.component) {
            this.component.showDialog(checklistId);
        }
      },
      handlePrioritiseSave: function () {
        if (this.component) {
            this.component.hideDialog();
            Y.fire('infoMessage:message', {
              message: this.get('dialogConfig').successMessage
            });
            this.fire('saved');
        }
      }
  }, {
      ATTRS: {
    	  dialogConfig: {},
          labels: {},
          enumTypes: {
            value: {
              ChecklistInstancePriority: Y.usp.checklist.enum.ChecklistInstancePriority
            }
          },
          urls: {},
      }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'usp-view',
    'usp-react-view',
    'checklist-priority-enumeration'
  ]
});