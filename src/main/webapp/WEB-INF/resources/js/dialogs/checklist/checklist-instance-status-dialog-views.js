YUI.add('checklist-instance-status-dialog-views', function(Y) {  
    var Micro = Y.Template.Micro,
        O = Y.Object,
        L = Y.Lang,
        A = Y.Array,
        CHECKLIST_OWNERSHIP_LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div class="loading-main">Please wait while we find possible owners for your checklist.</div></div></div>',
        ChecklistOwnershipLoadingView = Y.Base.create('checklistOwnershipLoadingView', Y.View, [], {
            render: function() {
                var container = this.get('container');
                container.setHTML(CHECKLIST_OWNERSHIP_LOADING_TEMPLATE);
                return this;
            }
        }),
        //Base View for status change
        BaseChecklistInstanceStatusChangeView = Y.Base.create('baseChecklistInstanceStatusChangeView', Y.View, [], {
            initializer: function() {
                this.get('model').after('load', this.render, this);
            },
            render: function() {
                var container = this.get('container');
                
                var html = this.template({
                        otherData: this.get('otherData'),
                        message: this.get('message'),
                        labels: this.get('labels')
                    });
                // Render this view's HTML into the container element.
                container.setHTML(html);
                return this;
            }
        }, {
            ATTRS: {
                model: {},
                /**
                 * @attribute otherData Should contain the narrative attribute, along with any other data required by the template
                 */
                otherData: {},
                message: {}
            }
        }),
        ReassignSuccessWithWarningsView=Y.Base.create('reassignSuccessWithWarningsView', Y.View, [], {
            template: Y.Handlebars.templates["reassignSuccessWithWarnings"],
            render:function(){
              var container = this.get('container');
              
              var html = this.template({
                modelData:this.get('model').toJSON(),
                message: this.get('message'),
                labels: this.get('labels')
              });
              // Render this view's HTML into the container element.
              container.setHTML(html);
              return this;
            }
        }),
        ChecklistInstanceStatusChangeView = Y.Base.create('checklistInstanceStatusChangeView', BaseChecklistInstanceStatusChangeView, [], {
            template: Y.Handlebars.templates["checklistInstanceStatusChangeDialog"]
        }),
        ChecklistInstancePauseView = Y.Base.create('checklistInstancePauseView', BaseChecklistInstanceStatusChangeView, [], {
            template: Y.Handlebars.templates["checklistInstancePauseDialog"],
            render: function() {
                this.constructor.superclass.render.call(this);
                //setup reasons list
                this.initPauseReasons();
                return this;
            },
            initPauseReasons: function() {
                var container = this.get('container'),
                    pauseReason = container.one('#checklistPauseForm_pauseReason');
                if (pauseReason) {
                    Y.FUtil.setSelectOptions(pauseReason, O.values(Y.uspCategory.checklist.pauseReason.category.getActiveCodedEntries()), null, null, true, true, 'code');
                    pauseReason.set('value', this.get('model').get('pauseReason')||null);
                }
            },
        }),
        ChecklistInstanceRemoveView = Y.Base.create('checklistInstanceRemoveView', BaseChecklistInstanceStatusChangeView, [], {
            template: Y.Handlebars.templates["checklistInstanceRemoveDialog"]
        }),
        ChecklistInstanceResetEndDateView = Y.Base.create('checklistInstanceResetEndDateView', Y.usp.checklist.UpdateChecklistInstanceEndDateView, [], {
            events: {
                'form': {
                    submit: '_preventSubmit'
                }
            },
            template: Y.Handlebars.templates["checklistInstanceResetEndDateDialog"],
            _preventSubmit: function(e) {
                e.preventDefault();
            },
            initializer: function() {
                this.endDateCalendar = new Y.usp.CalendarPopup({
                    iconClass: 'fa fa-calendar',
                    pickerStyle: 'DateTime',
                    'minimumDate': new Date()
                });
                //set as bubble targets
                this.endDateCalendar.addTarget(this);
            },
            render: function() {
                var container = this.get('container');
                //call super class
                ChecklistInstanceResetEndDateView.superclass.render.call(this);
                //set the input node for the calendar
                this.endDateCalendar.set('inputNode', container.one('#checklistInstanceResetEndDateForm_endDate'));
                //render the calendar
                this.endDateCalendar.render()
            },
            destructor: function() {
                if (this.endDateCalendar) {
                    this.endDateCalendar.destroy();
                    delete this.endDateCalendar;
                }
            }
        }),
        ChecklistInstanceErrorsView=new Y.Base.create('checklistInstanceResetEndDateView', Y.usp.checklist.ChecklistInstanceView, [], {
          template: Y.Handlebars.templates["checklistInstanceErrorsDialog"]
        },{
          ATTRS:{
            recoveryUrl:{
              value:''
            }
          }
        });
    
    //Status change form (used for all status changes except 'reject') - simple YUI form
    Y.namespace('app').ChecklistInstanceStatusChangeForm = Y.Base.create('checklistinstanceStatusChangeForm', Y.Model, [Y.ModelSync.REST, Y.usp.ModelFormLink], {
        form: '#statusChangeForm'
    });
    //Form for 'remove' checklist
    Y.namespace('app').ChecklistInstanceRemoveForm = Y.Base.create('checklistInstanceRemoveForm', Y.Model, [Y.ModelSync.REST], {});
    
    Y.namespace('app').ChecklistInstanceErrorModel = Y.Base.create('checklistInstanceErrorModel', Y.usp.checklist.ChecklistInstance, [Y.usp.ModelTransmogrify], {
    });
    
    Y.namespace('app').RecoverChecklistInstanceModel = Y.Base.create('recoverChecklistInstanceModel', Y.Model, [Y.ModelSync.REST], {
    },{
      ATTRS:{
        id:{
          
        }
      }
    });
    
    //Form for 'reassign' checklist
    Y.namespace('app').ChecklistInstanceReassignForm = Y.Base.create('checklistInstanceReassignForm', Y.usp.checklist.ChecklistInstance, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#checklistReassignForm',
        validate: function(attributes, callback) {
            var errs = {};
            
            if (null===attributes.ownerId|| L.trim(attributes.ownerId) === '') {
                errs['ownerId'] = 'Please select a valid owner for this work list instance.';
            }
            if (null===attributes.ownerSubjectType||L.trim(attributes.ownerSubjectType) === '') {
                errs['ownerSubjectType'] = 'Please select either a team or individual as owner.';
            }
            if (null===attributes.owningTeamId||L.trim(attributes.owningTeamId) === '') {
                errs['owningTeamId'] = 'The selected User has no team memberships, so cannot be assigned this worklist.';
            }
            
            if (O.keys(errs).length > 0) {
                return callback({
                    code: 400,
                    msg: {
                        validationErrors: errs
                    }
                });
            }
            //no errors
            return callback();
        }
    },{
        ATTRS: {
            ownerId: {
                value: null
            },
            ownerSubjectType:{
                value:null
            },
            owningTeamId: {
                value: null
            }
        }
    });
    //Form for reset end date of checklist
    Y.namespace('app').ChecklistInstanceResetEndDateForm = Y.Base.create('checklistInstanceResetEndDateForm', Y.usp.checklist.ChecklistInstance, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#checklistInstanceResetEndDateForm'
    }, {
        ATTRS: {
            endDate: {
                value: null,
                setter: function(val) {
                    var parsedDateTime = Y.USPDate.parseDateTime(val);
                    if (L.isDate(parsedDateTime)) {
                        return parsedDateTime.getTime();
                    }
                    return val;
                },
                getter: function(value) {
                    if (L.isNumber(value)) {
                        return value;
                    }
                    if (L.isDate(value)) {
                        return value.getTime();
                    }
                    if (L.trim(value) === '') {
                        return null;
                    }
                    //just return the value
                    return value;
                }
            }
        }
    });
    /**
     * Extend the UpdateChecklistInstanceEndDate to add additional client side validation
     */
    Y.namespace('app').UpdateChecklistInstanceEndDate = Y.Base.create('updateChecklistInstanceEndDate', Y.usp.checklist.UpdateChecklistInstanceEndDate, [], {
        validate: function(attributes, callback) {
            var errs = {},
                endDateTime;
            if (L.isNumber(attributes.endDate)) {
                endDateTime = new Date(attributes.endDate);
            } else {
                endDateTime = Y.USPDate.parseDateTime(attributes.endDate);
            }
            if (!L.isDate(endDateTime)) {
                errs['endDate'] = 'Please enter a valid end date and time for this work list.';
            } else {
                if (endDateTime.getTime() < new Date().getTime()) {
                    errs['endDate'] = 'End date may not be in the past.'
                }
            }
            if (O.keys(errs).length > 0) {
                return callback({
                    code: 400,
                    msg: {
                        validationErrors: errs
                    }
                });
            }
            // Now call any validate in the class we extended UpdateChecklistInstanceOwner
            return Y.app.ChecklistInstanceResetEndDateForm.superclass.validate.apply(this, arguments);
        }
    });
    Y.namespace('app').ChecklistInstancePauseForm = Y.Base.create('checklistInstancePauseForm', Y.usp.checklist.UpdateChecklistInstancePause, [Y.usp.ModelFormLink], {
        form: '#checklistPauseForm'
    });
    /**
     * Extend Y.usp.checklist.UpdateChecklistInstanceOwner to add a custom validation rule
     */
    Y.namespace('app').UpdateChecklistInstanceOwner = Y.Base.create('updateChecklistInstanceOwner', Y.usp.checklist.UpdateChecklistInstanceOwner, [], {
        validate: function(attributes, callback) {
            var errs = {};
            if (attributes.ownerId === undefined || attributes.ownerId === null || (L.isString(attributes.ownerId) ? attributes.ownerId.trim() === '' : false)) {
                errs['ownerId'] = 'Please select a valid owner to reassign this work list to.';
            }
            if (O.keys(errs).length > 0) {
                return callback({
                    code: 400,
                    msg: {
                        validationErrors: errs
                    }
                });
            }
            // Now call any validate in the class we extended UpdateChecklistInstanceOwner
            return Y.app.UpdateChecklistInstanceOwner.superclass.validate.apply(this, arguments);
        }
    });
    Y.namespace('app').ChecklistInstanceStatusFormDialog = Y.Base.create('checklistInstanceStatusFormDialog', Y.usp.app.AppDialog, [], {
        views: {
            loadingOwnership:{
              header:{
                test:'Reassign work list',
                icon:'fa fa-check-square-o'
              },
              type: ChecklistOwnershipLoadingView
            },
            deleteChecklist: {
                type: ChecklistInstanceStatusChangeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Delete',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            removeChecklist: {
                type: ChecklistInstanceRemoveView,
                buttons: [{
                    name: 'removeButton',
                    labelHTML: '<i class="fa fa-check"></i> Remove',
                    action: 'handleRemove',
                    disabled: true
                }]
            },
            reassignChecklist: {
                type: Y.app.ChecklistInstanceReassignView,
                buttons: [{
                    name: 'reassignButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                      var view=this.get('activeView');
                      var model=view.get('model');
                      if(view.get('reassignBatch')){
                        model.url=view.get('reassignBatchURL');
                      }else{
                        model.url=view.get('reassignURL');
                      }
                      this.handleSave(e, null, {
                        //use our extended version here - this has custom validation
                        transmogrify: Y.app.UpdateChecklistInstanceOwner
                      }, false, this.handleSuccessWithWarnings.bind(this));
                    
                    },
                    disabled: true
                }]
            },
            pauseChecklist: {
                type: ChecklistInstancePauseView,
                buttons: [{
                    name: 'pauseButton',
                    labelHTML: '<i class="fa fa-pause"></i> Pause',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            resumeChecklist: {
                type: ChecklistInstanceStatusChangeView,
                buttons: [{
                    name: 'resumeButton',
                    labelHTML: '<i class="fa fa-play"></i> Resume',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            resetEndDate: {
                type: ChecklistInstanceResetEndDateView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        this.handleSave(e, null, {
                            transmogrify: Y.app.UpdateChecklistInstanceEndDate
                        });
                    },
                    disabled: true
                }]
            },
            startChecklist: {
                type: ChecklistInstanceStatusChangeView,
                buttons: [{
                    name: 'startButton',
                    labelHTML: '<i class="fa fa-play"></i> Start',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            reassignSuccessWithWarnings:{
              type: ReassignSuccessWithWarningsView,
              buttons: [{
                labelHTML: '<i class="fa fa-check"></i> OK',
                action: 'handleCancel'
              }]
            },
            viewErrors:{
              type: ChecklistInstanceErrorsView,
              buttons: [{
                name: 'ignoreButton',
                labelHTML: '<i class="fa fa-eye-slash"></i> Ignore',
                action: function(e){
                  var model=this.get('activeView').get('model');
                  
                  model.url=L.sub(this.get('activeView').get('recoveryUrl'), {mode:'IGNORE'});
                  
                  this.handleSave(e, null, {
                    transmogrify: Y.app.RecoverChecklistInstanceModel
                  });
                },
                disabled: true
              },{
                name: 'retryButton',
                labelHTML: '<i class="fa fa-refresh"></i> Retry',
                action: function(e){
                  var model=this.get('activeView').get('model');
                  
                  model.url=L.sub(this.get('activeView').get('recoveryUrl'), {mode:'RETRY'});
                  
                  this.handleSave(e, null, {
                    transmogrify: Y.app.RecoverChecklistInstanceModel
                  });

                },
                disabled: true
            }]
          }
        },
        handleSuccessWithWarnings:function(result){
          var baseHandleSuccess=this._handleSuccess.bind(this);
          var warnings, responseObject;
          
          if(result.response && result.response.responseText){
            responseObject=JSON.parse(result.response.responseText);
          }
          
          warnings=responseObject.warnings;
          secondaryResourceURIs=responseObject.secondaryResourceURIs;
          
          var showReassignWarningsDialog=function(){
            if(warnings.length>0){
              //warnings generated
              this.showView('reassignSuccessWithWarnings', {
                model: new Y.Model({
                    warnings: warnings,
                    secondaryResourceURIs: secondaryResourceURIs
                })
              }, function(){
                //remove cancel button - we have an OK button instead
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
              });
            }
            //nothing to do
            return true;
          }.bind(this);
          
         
          
          //call into the standard success handler defined in app-dialog 
          return Y.when(baseHandleSuccess(result)).then(showReassignWarningsDialog);
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'app-dialog',
        'model-form-link',
        'model-transmogrify',
        'form-util',
        'handlebars-helpers',
        'handlebars-checklist-templates',
        'handlebars-checklist-partials',
        'usp-checklist-ChecklistInstance',
        'usp-checklist-UpdateChecklistInstanceOwner',
        'usp-checklist-UpdateChecklistInstancePause',
        'usp-checklist-UpdateChecklistInstanceEndDate',
        'categories-checklist-component-PauseReason',
        'checklist-instance-ownership-views',
        'querystring'
    ]
});