YUI.add('checklist-definition-dialog-views', function(Y) {
   'use strict';

    var CHECKLIST_CURRENT_VERSION = "1.0.0",
        L = Y.Lang,
        E = Y.Escape,
        A = Y.Array,
        O = Y.Object,
        DB = Y.usp.DataBindingUtil,
        Micro = Y.Template.Micro,
        AUTOCOMPLETE_ROW = Micro.compile('<div title="<%=this.team.name%>">\
                <div><span><%=this.team.organisationIdentifier%> - <%=this.team.name%></span></div>\
                 <%==this.matchingField%>\
                 </div>');
    
    //Status change checklist (used for all status changes) - simple YUI form
    Y.namespace('app.admin.checklist').ChecklistDefinitionStatusChangeForm = Y.Base.create('checklistDefinitionStatusChangeForm', Y.Model, [Y.ModelSync.REST], {});
    //Form for 'remove' checklist definition
    Y.namespace('app.admin.checklist').ChecklistDefinitionRemoveForm = Y.Base.create('checklistDefinitionRemoveForm', Y.Model, [Y.ModelSync.REST], {});
    //Form for edit checklist definition - model will be transmogrified into an UpdateChecklistDefinition
    Y.namespace('app.admin.checklist').ChecklistDefinitionEditForm = Y.Base.create('checklistDefinitionEditForm', Y.usp.checklist.ChecklistDefinition, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editChecklistDefinitionForm',
        customValidation:function(attrs){
          var errors={},
          securityDomain=attrs.securityDomain||'',
            labels=this.labels||{};
        
      
          if(securityDomain===null||securityDomain===''){
            errors.securityDomain=labels.securityDomainValidationErrorMessage;
          }
          
          return errors;
        }
    }, {
        ATTRS: {
            calculationMode: {
                //default value is FROM_START 
                value: 'FROM_START',
                setter:function(v){
                    if(v==='TO_END'){
                        //ensure valid settings
                        this.setAttrs({canPause:false,canBackdate:false});
                    }
                }
            },
            canPause: {
                //default value is FALSE
                value: false
            },
            canBackdate: {
                //default value is FALSE
                value: false
            },
            cardinality: {
                value: 'UNIQUE_PER_SUBJECT'
            },
            isCardinalityYes: {
                value: false,
                writeOnce: 'initOnly',
                getter: function() {
                    return this.get('cardinality') === 'UNCONSTRAINED';
                }
            }
        }
    });
    
    /**
     * Will be transmogrified into UpdateChecklistDefinitionTeamOwnershipConfiguration
     */
    Y.namespace('app.admin.checklist').ChecklistDefinitionOwnershipEditForm = Y.Base.create('checklistDefinitionOwnershipEditForm', Y.usp.checklist.ChecklistDefinitionWithConfiguration, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editChecklistDefinitionOwnershipForm'
    }, {
        ATTRS: {
            teamOwnershipConfigurations:{
                value:[]
            }
        }
    });
    
    Y.namespace('app.admin.checklist').UpdateChecklistDefinitionTeamOwnershipConfiguration = Y.Base.create('updateChecklistDefinitionTeamOwnershipConfiguration', Y.usp.checklist.UpdateChecklistDefinitionTeamOwnershipConfiguration, [], {
    },{
        ATTRS:{
            //We must turn the full VOs that we got from the getter into something that can be consumed by update REST endpoint
            teamOwnershipConfigurations:{
                setter:function(v){
                    return A.map(v, function(t){
                        return new Y.usp.checklist.NewChecklistTeamOwnershipConfiguration({
                            teamAddingId:t.teamAddingId,
                            teamOwningId:t.teamOwningId,
                            teamRelationshipTypeId:t.teamRelationshipTypeId
                        }); 
                    });
                }
            }
        }
    });
    
    //Form for view checklist definition 
    Y.namespace('app.admin.checklist').ChecklistDefinitionViewForm = Y.Base.create('checklistDefinitionViewForm', Y.usp.checklist.ChecklistDefinition, [], {}, {
        ATTRS: {
            /** Default values for - non - upgraded definition i.e. pre Eclipse 1.4.x */
            calculationMode: {
                //default value is FROM_START 
                value: 'FROM_START'
            },
            canPause: {
                //default value is FALSE
                value: false
            },
            canBackdate: {
                //default value is FALSE
                value: false
            },
            isCardinalityYes: {
                value: false,
                writeOnce: 'initOnly',
                getter: function() {
                    return this.get('cardinality') === 'UNCONSTRAINED';
                }
            }
        }
    });
    Y.namespace('app.admin.checklist').ChecklistDefinitionStatusChangeView = Y.Base.create('checklistDefinitionStatusChangeView', Y.usp.checklist.ChecklistDefinitionView, [], {
        template: Y.Handlebars.templates.checklistDefinitionStatusChangeDialog,
    });
    
    /**
     * A Base definition that DOES NOT re-render on model change
     */    
    Y.namespace('app.admin.checklist').BaseChecklistDefinitionView = Y.Base.create('baseChecklistDefinitionView', Y.View, [], {
       initializer: function() {
            var model = this.get('model');
            // Re-render this view when the model loads
            model.after('load', this.render, this);
            
        },      
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    modelData: this.get('model').toJSON(),
                    otherData: this.get('otherData'),
                    codedEntries: this.get('codedEntries'),
                    labels: this.get('labels')
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);

            return this;
        }
    }, {
        ATTRS: {
            codedEntries:{
              value:{}
            },
            model: {},
            otherData: {},
            labels: {}
        }
    });
    
    Y.namespace('app.admin.checklist').ChecklistDefinitionEditView = Y.Base.create('checklistDefinitionEditView', Y.app.admin.checklist.BaseChecklistDefinitionView, [], {
        template: Y.Handlebars.templates.checklistDefinitionEditDialog,
        events:{
                '#editChecklistDefinitionForm_calculationModeFromStart': {
                    'focus': 'updateCalculationMode',
                    'blur': 'updateCalculationMode'
                },
                '#editChecklistDefinitionForm_calculationModeToEnd': {
                    'focus': 'updateCalculationMode',
                    'blur': 'updateCalculationMode'
                },
                '#editChecklistDefinitionForm_cardinality_yes': {
                    change: 'displayMultipleInstancesWarning'
                },
                '#editChecklistDefinitionForm_cardinality_no': {
                    change: 'displayMultipleInstancesWarning'
                }
        },
        render:function(){
            var container=this.get('container'),
                model=this.get('model'),
                calculationMode=model.get('calculationMode'),
                securityDomains=this.get('securityDomains'),
                securityDomain={code:null, name:null},
                securityDomainCode=model.get('securityDomain');

            //call into superclass to render
            Y.app.admin.checklist.ChecklistDefinitionEditView.superclass.render.call(this);

            if(securityDomainCode){
              securityDomain = securityDomains.filter(function(f){return f.code===securityDomainCode;})[0]||securityDomain;
            }
            
            Y.FUtil.setSelectOptions(container.one('#editChecklistDefinitionForm_securityDomain'), securityDomains.filter(function(sd){return sd.active===true;}), securityDomainCode, securityDomain.name||securityDomainCode, true, true, 'code');
            container.one('#editChecklistDefinitionForm_securityDomain').set('value', securityDomainCode);

            //update radio options 
            this.setValidOptions(calculationMode);

            return this;
        },
        updateCalculationMode:function(e){
            this.setValidOptions(e.currentTarget.get('value'));
        },
        displayMultipleInstancesWarning:function(e) {
            var container=this.get('container'),
                isYesChecked=container.one('#editChecklistDefinitionForm_cardinality_yes').get('checked') === true,
                warningContainer = container.one('#multipleInstancesWarning');

            if (isYesChecked) {
                warningContainer.removeClass("hiddenWarning");
            } else {
                warningContainer.addClass("hiddenWarning");
            }
        },

        /**
         * Ensure the canPause and canBackdate attributes are valid with
         * regard to the selected calculationMode
         */
        setValidOptions:function(calculationMode){
            var container=this.get('container');
         
            var updateRadios=function(enable){
                container.all('.constrainedRadio').each(function(n){
                    n.all('input[type="radio"]').each(function(radio){
                        if(!enable){
                           if(radio.get('value')==='false'){
                               radio.set('checked',true);
                           }else{
                               radio.removeAttribute('checked');
                           }
                           radio.setAttribute('disabled',true);
                           radio.addClass('disabled');
                        }else{
                            radio.removeAttribute('disabled');
                            radio.removeClass('disabled');
                        }
                    });
                 });
            };
            
            if(calculationMode==='TO_END'){
                updateRadios(false);
            }else{
                updateRadios(true);
            }
        }
    },{
      ATTRS:{
        securityDomains:{
          value:[]
        }
      }
    });
    
    Y.namespace('app.admin.checklist').ChecklistDefinitionOwnershipEditView = Y.Base.create('checklistDefinitionOwnershipEditView', Y.app.admin.checklist.BaseChecklistDefinitionView, [], {
        template: Y.Handlebars.templates.checklistDefinitionOwnershipEditDialog,
        initializer: function(config) {
            var model = this.get('model');

            //views used for add and list
            this.teamOwnershipConfiguration=new TeamOwnershipConfiguration({
                model:model,
                labels:config.labels
            });
            this.teamOwnershipConfigurationAddRow=new TeamOwnershipConfigurationAddRow({
                model:config.model,
                autocompleteURL:config.autocompleteURL,
                labels:config.labels
            });
            
            this.relationshipTypes=new Y.usp.relationship.PaginatedProfessionalTeamRelationshipTypeList({
               url:this.get('professionalTeamRelationshipTypeURL') 
            });
            
            this.relationshipTypes.onceAfter('load', this.setupRelationshipTypes, this);
                        
            //load the relationship types
            this.relationshipTypes.load();
            
            //add event targets
            this.teamOwnershipConfiguration.addTarget(this);
            this.teamOwnershipConfigurationAddRow.addTarget(this);

        },
        render:function(){
            var container=this.get('container');
            
            //call into superclass to render
            Y.app.admin.checklist.ChecklistDefinitionOwnershipEditView.superclass.render.call(this);
            
            container.one('#ownershipAddRow').append(this.teamOwnershipConfigurationAddRow.render().get('container'));
            container.one('#ownershipConfiguration').append(this.teamOwnershipConfiguration.render().get('container'));
            
            return this;
        },
        setupRelationshipTypes:function(e){
            this.teamOwnershipConfigurationAddRow.set('relationshipTypes', this.relationshipTypes.map(function(type){
               return ({
                   id:type.get('id'),
                   name:type.get('roleAName')
               });
            }).sort(function(a,b){
                return (a.name||'').localeCompare(b.name||'');
            }));
        },
        destructor:function(){
            if(this.teamOwnershipConfiguration){
                this.teamOwnershipConfiguration.destroy();
                
                this.teamOwnershipConfiguration.removeTarget(this);
                
                
                delete this.teamOwnershipConfiguration;
            }
            if(this.teamOwnershipConfigurationAddRow){
                this.teamOwnershipConfigurationAddRow.destroy();
                
                this.teamOwnershipConfigurationAddRow.removeTarget(this);
                
                delete this.teamOwnershipConfigurationAddRow;
            }
        }
    },{
        ATTRS:{
            autocompleteURL:{
                value:null
            },
            professionalTeamRelationshipTypeURL:{
                value:null
            }
        }
    });    
    
    var TeamAutoComplete=Y.Base.create('teamAutoComplete', Y.View, [], {
        render:function(){
            var container = this.get('container');
            
            if (!this.autocomplete) {
                this.autocomplete=new Y.usp.PopupAutoComplete({
                    inputNode: this.get('inputNode'),
                    allowBrowserAutocomplete: false,
                    enableCache: false,
                    maxResults: this.get('maxResults'),
                    minQueryLength: 1,
                    source: this.get('autocompleteURL'),
                    scrollIntoView: true,
                    resultListLocator: function(response) {
                        return response.results || [];
                    },
                    resultTextLocator: function(result) {
                        return result.name;
                    },
                    resultFormatter: function(query, results) {
                        return A.map(results, function(result) {
                            return AUTOCOMPLETE_ROW({
                                team: result.raw,
                                matchingField: Y.app.search.organisation.RenderMatchingField(result.raw, query),
                            });
                        });
                    }
                }).render();
                
                //add as event target
                this.autocomplete.addTarget(this);
            }
            
            return this;
        },      
        destructor:function(){
            if(this.autocomplete){
                this.autocomplete.destroy();
                
                //remove target
                this.autocomplete.removeTarget(this);
                
                delete this.autocomplete;
            }
        }
    },{
        ATTRS:{
            inputNode:{
              value:null,
              getter:function(v){
                  return this.get('container').one(v);
              }
            },
            autocompleteURL:{
                value:null
            },
            maxResults:{
                value:25
            }
        }
    }),
    TeamOwnershipConfigurationAddRow=Y.Base.create('teamOwnershipConfigurationAddRow', Y.View, [], {
        template: Y.Handlebars.templates.checklistDefinitionOwnershipConfigurationAdd,
        events: {
            '.add': {
                click: 'handleClick'
            },
            'input': {
                'keyup': 'handleKeyUp'
            },        
            'select[name="teamRelationshipType"]':{
                change: '_onTeamRelationshipTypeChange'
            },
            'input[name="addingTeamType"]':{
                change:'_handleAddingTeamTypeChange'
            }             
        },
        handleKeyUp: function(e) {
            if (e.keyCode === 13) {
                //only prevent the default if we wish to consume this event
                e.preventDefault();
                this.addItem();
            }
        },
        handleClick: function(e) {
            e.preventDefault();
            this.addItem();
        },
        _handleAddingTeamTypeChange:function(e){
            var target=e.currentTarget;
                
            this.set('addingTeamType', DB.getElementValue(target._node));
          },        
        initializer:function(config){
            //auto complete modules
            this.owningTeamAutocomplete=new TeamAutoComplete({
                container:this.get('container'),
                inputNode:'#editChecklistDefinitionOwnershipForm_owningTeam',
                autocompleteURL:config.autocompleteURL
            });
            
            //setup select event for owning Team
            this.owningTeamAutocomplete.on('*:select', function(e){
                this.get('addRow').owningTeam=e.result.raw;
            },this);
            
            this.owningTeamAutocomplete.on('*:clear', function(e){
                this.get('addRow').owningTeam=null;
            },this);
            
            this.after('relationshipTypesChange', this.renderRelationshipTypes, this);
            this.after('addingTeamTypeChange', this._handleAfterAddingTeamTypeChange, this);
        },
        render:function(){
            var container = this.get('container'),
            model=this.get('model'),
            html = this.template({
                addingTeamType: this.get('addingTeamType'),
                labels: this.get('labels')
            });
        
            // Render this view's HTML into the container element.
            container.setHTML(html);
        
            this.owningTeamAutocomplete.render();
            
            //the presence of the autocomplete is controlled by the addingTeamType attribute
            this._setAddingTeamAutoComplete();
            
            this.renderRelationshipTypes();
            return this;
        },
        _handleAfterAddingTeamTypeChange:function(){
            this._setAddingTeamAutoComplete();
        },
        _setAddingTeamAutoComplete:function(){
            var addingTeamType=this.get('addingTeamType')||'ANY',
                container=this.get('container');
            
            if(addingTeamType==='SPECIFIC'){
                if(!this.addingTeamAutocomplete){
                    this.addingTeamAutocomplete=new TeamAutoComplete({
                        container:this.get('container'),
                        inputNode:'#editChecklistDefinitionOwnershipForm_addingTeam',
                        autocompleteURL:this.get('autocompleteURL')
                    }).render();
                    
                    //setup select event for adding Team
                    this.addingTeamAutocomplete.on('*:select', function(e){
                        this.get('addRow').addingTeam=e.result.raw;
                    }, this);
                    
                    this.addingTeamAutocomplete.on('*:clear', function(e){
                        this.get('addRow').addingTeam=null;
                    },this);
                }
                //show node
                container.one('#editChecklistDefinitionOwnershipForm_addingTeam').show();
                container.one('#editChecklistDefinitionOwnershipForm_anyAddingTeam').hide();
            }else if(addingTeamType==='ANY'){
                if(this.addingTeamAutocomplete){
                    this.addingTeamAutocomplete.destroy();
                    //clean up event
                    this.addingTeamAutocomplete.detach('*:select');
                    this.addingTeamAutocomplete.detach('*:clear');
                    
                    delete this.addingTeamAutocomplete;
                }
                
                //clear the adding row team
                this.get('addRow').addingTeam=null;
                
                //remove node
                container.one('#editChecklistDefinitionOwnershipForm_addingTeam').set('value', '').hide();
                container.one('#editChecklistDefinitionOwnershipForm_anyAddingTeam').show();
            }
        },
        renderRelationshipTypes:function(){
            var container=this.get('container'),
                relationshipTypesSelect=container.one('#editChecklistDefinitionOwnershipForm_teamRelationshipType');
            
            if(relationshipTypesSelect){
                Y.FUtil.setSelectOptions(relationshipTypesSelect, this.get('relationshipTypes'), null, null, "All");
            }
        },
        _onTeamRelationshipTypeChange:function(e){
            var value = Number(e.currentTarget.get('value')),
                type={};               
            
            //lookup type in array
            type=A.find(this.get('relationshipTypes'), function(t){
                return t.id===value;
            })||{};
            
            //if a YUI Model - convert to JSON Object
            this.get('addRow').relationshipType=type.toJSON?type.toJSON():type;
        }, 
        addItem:function(){
            var model=this.get('model'),
                addRow=this.get('addRow')||{},
                errs={};
            
            //clear existing errors
            this.fire('clearErrors');
            
            //add an entry into our model - if valid
            if(!addRow.addingTeam && this.get('addingTeamType')==='SPECIFIC'){
                errs.addingTeam = 'Please select the team adding.';
            }
            if(!addRow.owningTeam){
                errs.owningTeam = 'Please select the team owning.';
            }            
            
            if(O.keys(errs).length>0){
                //fire the error
                this.fire('error',{
                        error:{
                            code: 400,
                            msg: {
                                validationErrors:errs
                            }
                        }
                });
            }else{
                var ownershipConfig=model.get('teamOwnershipConfigurations')||[],
                    container=this.get('container');
                
                ownershipConfig.push({
                    id:Y.guid(),
                    teamAddingId:addRow.addingTeam?addRow.addingTeam.id:null,
                    teamAddingName:addRow.addingTeam?addRow.addingTeam.name:null,
                    teamOwningId:addRow.owningTeam?addRow.owningTeam.id:null,
                    teamOwningName:addRow.owningTeam?addRow.owningTeam.name:null,
                    teamRelationshipTypeId:addRow.relationshipType?addRow.relationshipType.id:null,
                    teamRelationshipTypeName:addRow.relationshipType?addRow.relationshipType.name:null
                });
                                
                //push the updated array back into the model
                model.set('teamOwnershipConfigurations', ownershipConfig);
                
                //clear down
                this.set('addRow', {});
                
                //clear the AC lists
                container.one('#editChecklistDefinitionOwnershipForm_addingTeam').set('value', '');
                container.one('#editChecklistDefinitionOwnershipForm_owningTeam').set('value', '');
                this.get('container').one('#editChecklistDefinitionOwnershipForm_teamRelationshipType').set('value', '');
            }
        },        
        destructor:function(){
            if(this.addingTeamAutocomplete){
                this.addingTeamAutocomplete.destroy();
                
                delete this.addingTeamAutocomplete;
            }            
            if(this.owningTeamAutocomplete){
                this.owningTeamAutocomplete.destroy();
                
                delete this.owningTeamAutocomplete;
            }
        }
    },{
        ATTRS:{
            autocompleteURL:{
                value:null
            },            
            labels:{
                value:{}
            },
            relationshipTypes:{
                value:[]
            },
            addingTeamType:{
                value:'ANY'
            },
            addRow:{value:{}}
        }
    }),
    TeamOwnershipConfiguration=Y.Base.create('teamOwnershipConfiguration', Y.View, [], {
        template: Y.Handlebars.templates.checklistDefinitionOwnershipConfiguration,
        events: {
            '.remove': {
              click: 'removeItem'
            }
        },
        initializer:function(){
            //as the ownership configuration changes - re-render
            this.get('model').after('teamOwnershipConfigurationsChange', this.render, this);
        },
        removeItem:function(e){
            var model=this.get('model'),
                ownershipConfig=model.get('teamOwnershipConfigurations')||[],
                currentTarget=e.currentTarget,
                id=currentTarget.getData('id');
                          
            e.preventDefault();
            
            ownershipConfig=ownershipConfig.filter(function(item){
                return item.id!=id;
            });
            
            //push the updated array back into the model
            model.set('teamOwnershipConfigurations', ownershipConfig);
          },        
        render:function(){
            var container = this.get('container'),
                model=this.get('model'),
            html = this.template({
                teamOwnershipConfigurations:model.get('teamOwnershipConfigurations').sort(function(a,b){
                    return (a.teamAddingName||'').localeCompare(b.teamAddingName||'') || (a.teamRelationshipTypeName||'').localeCompare(b.teamRelationshipTypeName||'');
                }),
                labels: this.get('labels')
            });
            
            // Render this view's HTML into the container element.
            container.setHTML(html);
            
            return this;
        }
    },{
        ATTRS:{
            labels:{
                value:{}
            }
        }
    });
    
    Y.namespace('app.admin.checklist').ChecklistDefinitionViewView = Y.Base.create('checklistDefinitionViewView', Y.usp.checklist.ChecklistDefinitionView, [], {
        template: Y.Handlebars.templates.checklistDefinitionViewDialog,
        initializer: function() {
          var model = this.get('model'),
              securityDomains={};
          
          this.get('securityDomains').forEach(function(sd){
            if(sd.code){
              securityDomains[sd.code]=sd;
            }
          });
          
          this.set('codedEntries', Y.merge(this.get('codedEntries'), {
            securityDomains:securityDomains
          }));
      }
    });
    
    Y.namespace('app.admin.checklist').ChecklistDefinitionDialog = Y.Base.create('checklistDefinitionDialog', Y.usp.app.AppDialog, [], {
        views: {
            view: {
              type: Y.app.admin.checklist.ChecklistDefinitionViewView
            },
            edit: {
              type: Y.app.admin.checklist.ChecklistDefinitionEditView,
              buttons: [{
                  name: 'saveButton',
                  labelHTML: '<i class="fa fa-check"></i> Save',
                  action: function(e) {
                      this.handleSave(e, null, {
                          transmogrify: Y.usp.checklist.UpdateChecklistDefinition
                      });
                  },
                  disabled: true
              }],
              //increase width for the edit to fit in controls
              width:700
            },
            publish: {
                type: Y.app.admin.checklist.ChecklistDefinitionStatusChangeView,
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Publish',
                    classNames: 'pure-button-primary',
                    action: function(e) {
                        this.handleSave(e, null, {
                            transmogrify: Y.usp.checklist.UpdateChecklistDefinition
                        });
                    },
                    disabled: true
                }]
            },
            editOwnership: {
                type: Y.app.admin.checklist.ChecklistDefinitionOwnershipEditView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        this.handleSave(e, null, {
                            transmogrify: Y.app.admin.checklist.UpdateChecklistDefinitionTeamOwnershipConfiguration
                        });
                    },
                    disabled: true
                }],
                //increase width for the edit ownership for table within dialog
                width:700
            },
            archive: {
                type: Y.app.admin.checklist.ChecklistDefinitionStatusChangeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Delete',
                    action: function(e) {
                        this.handleSave(e, null, {
                            transmogrify: Y.usp.checklist.UpdateChecklistDefinition
                        });
                    },
                    disabled: true
                }]
            },
            remove: {
                type: Y.app.admin.checklist.ChecklistDefinitionStatusChangeView,
                buttons: [{
                    name: 'removeButton',
                    labelHTML: '<i class="fa fa-check"></i> Remove',
                    action: 'handleRemove',
                    disabled: true
                }]
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'app-dialog',
               'model-form-link',
               'model-transmogrify',
               'form-util',
               'handlebars-helpers',
               'usp-checklist-NewChecklistDefinition',
               'usp-checklist-ChecklistDefinition',
               'usp-checklist-UpdateChecklistDefinition',
               'usp-checklist-UpdateChecklistDefinitionTeamOwnershipConfiguration',
               'usp-checklist-ChecklistDefinitionWithConfiguration',
               'usp-checklist-NewChecklistTeamOwnershipConfiguration',
               'usp-relationship-ProfessionalTeamRelationshipType',
               'handlebars-checklist-templates',
               'handlebars-checklist-partials',
               'organisation-name-matcher-highlighter',
               'popup-autocomplete-plugin',
               'data-binding']
});