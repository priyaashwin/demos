YUI.add('checklist-instance-dialog-views', function(Y) {
  var L = Y.Lang,
    O = Y.Object,
    ChecklistOwnershipUtil = Y.app.checklist.ChecklistOwnershipUtil,
    CHECKLIST_LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div class="loading-main">Please wait while we prepare your work list.</div></div></div>',
    BaseChecklistInstanceView = Y.Base.create('baseChecklistInstanceView', Y.View, [], {
      render: function() {
        var container = this.get('container'),
          html = this.template({
            modelData: this.get('model').toJSON(),
            labels: this.get('labels')
          });
        // Render this view's HTML into the container element.
        container.setHTML(html);
        return this;
      }
    }, {
      ATTRS: {
        /**
         * @attribute labels Labels to drive the template
         */
        labels: {}
      }
    }),
    BaseExistingChecklistInstanceView = Y.Base.create('baseExistingChecklistInstanceView', Y.View, [], {
      render: function() {
        var container = this.get('container'),
          html = this.template({
            modelData: this.get('model').toJSON(),
            otherData: this.get('otherData'),
            message: this.get('message'),
            labels: this.get('labels')
          });
        // Render this view's HTML into the container element.
        container.setHTML(html);
        return this;
      }
    }, {
      ATTRS: {
        /**
         * @attribute subject The subject (person or group) for this checklist instance
         */
        subject: {},
        /**
         * @attribute otherData Should contain the narrative attribute, along with any other data required by the template
         */
        otherData: {},
        /**
         * @attribute message Any message to display to the user
         */
        message: {},
        /**
         * @attribute labels Labels to drive the template
         */
        labels: {}
      }
    }),
    ShowExistingChecklistInstanceView = Y.Base.create('showExistingChecklistInstanceView', BaseExistingChecklistInstanceView, [], {
      template: Y.Handlebars.templates["checklistShowExistingInstanceDialog"]
    }),
    ShowExistingGroupChecklistInstancesView = Y.Base.create('showExistingGroupChecklistInstancesView', ShowExistingChecklistInstanceView, [], {
      template: Y.Handlebars.templates["checklistShowGroupExistingInstancesDialog"]
    }),
    AddChecklistInstanceView = Y.Base.create('addChecklistInstanceView', Y.usp.View, [], {
      template: Y.Handlebars.templates["checklistInstanceAddFormDialog"],
      events: {
        'form': {
          submit: '_preventSubmit'
        }
      },
      _preventSubmit: function(e) {
        e.preventDefault();
      },
      initializer: function(config) {
        var model = config.model,
          checklistInstanceBackdateViewConfig = config.checklistInstanceBackdateViewConfig || {},
          checklistInstanceFixedEndDateViewConfig = config.checklistInstanceFixedEndDateViewConfig || {},
          checklistInstanceOwnershipViewConfig = config.checklistInstanceOwnershipViewConfig || {};

        //determine which views we need to add to our model to support the configuration
        if (model.requireStartDate) {
          this.checklistInstanceBackdateView = new ChecklistInstanceBackdateView(Y.merge(checklistInstanceBackdateViewConfig, {
            model: model
          }));
          this.checklistInstanceBackdateView.addTarget(this);
        } else if (model.requireEndDate) {
          this.checklistInstanceFixedEndDateView = new ChecklistInstanceFixedEndDateView(Y.merge(checklistInstanceFixedEndDateViewConfig, {
            model: model
          }));
          this.checklistInstanceFixedEndDateView.addTarget(this);
        }
        if (model.requireOwnership) {
          this.checklistInstanceOwnershipView = new Y.app.ChecklistInstanceOwnershipView(Y.merge(checklistInstanceOwnershipViewConfig, {
            model: model,
            candidateOwnership: this.get('owners')
          }));
          this.checklistInstanceOwnershipView.addTarget(this);
        }
      },
      render: function() {
        var container = this.get('container'),
          contentNode = Y.one(Y.config.doc.createDocumentFragment());

        if (this.checklistInstanceBackdateView) {
          contentNode.append(this.checklistInstanceBackdateView.render().get('container'));
        } else if (this.checklistInstanceFixedEndDateView) {
          contentNode.append(this.checklistInstanceFixedEndDateView.render().get('container'));
        }
        if (this.checklistInstanceOwnershipView) {
          contentNode.append(this.checklistInstanceOwnershipView.render().get('container'));
        }
                
        //call super class
        AddChecklistInstanceView.superclass.render.call(this);

        //add our fragments to the content node
        container.one('#checklistAddContent').append(contentNode);

        return this;
      },
      destructor: function() {
        //clean up event targets and views
        if (this.checklistInstanceBackdateView) {
          this.checklistInstanceBackdateView.removeTarget(this);
          this.checklistInstanceBackdateView.destroy();
          delete this.checklistInstanceBackdateView;
        }
        if (this.checklistInstanceFixedEndDateView) {
          this.checklistInstanceFixedEndDateView.removeTarget(this);
          this.checklistInstanceFixedEndDateView.destroy();
          delete this.checklistInstanceFixedEndDateView;
        }
        if (this.checklistInstanceOwnershipView) {
          this.checklistInstanceOwnershipView.removeTarget(this);
          this.checklistInstanceOwnershipView.destroy();
          delete this.checklistInstanceOwnershipView;
        }
      }
    }, {
      ATTRS: {
        /**
         * @attribute subject The subject (person or group) for this checklist instance
         */
        subject: {},
        /**
         * @attribute model The NewChecklistInstanceModel with appropriate configuration
         */
        model: {},
        /**
         * @attribute checklistDefinition The checklist definition which should be an instance of ChecklistDefinitionWithConfiguration
         */
        checklistDefinition: {},
        /**
         * @attribute owners An array of potential owners (users and teams) for this checklist instance
         */
        owners: {},
        checklistInstanceBackdateViewConfig: {
          value: {
            labels: {}
          }
        },
        checklistInstanceFixedEndDateViewConfig: {
          value: {
            labels: {}
          }
        },
        checklistInstanceOwnershipViewConfig: {
          value: {
            labels: {}
          }
        }
      }
    }),
    ChecklistProcessingView = Y.Base.create('checklistProcessingView', Y.View, [], {
      render: function() {
        var container = this.get('container');
        container.setHTML(CHECKLIST_LOADING_TEMPLATE);
        return this;
      }
    }),
    ChecklistInstanceBackdateView = Y.Base.create('checklistInstanceBackdateView', BaseChecklistInstanceView, [], {
      template: Y.Handlebars.templates["checklistInstanceBackdateDialog"],
      initializer: function() {
        this.startDateCalendar = new Y.usp.CalendarPopup({
          iconClass: 'fa fa-calendar',
          pickerStyle: 'DateTime',
          'maximumDate': new Date()
        });
        //set as bubble targets
        this.startDateCalendar.addTarget(this);
      },
      render: function() {
        var container = this.get('container');
        //call super class
        ChecklistInstanceBackdateView.superclass.render.call(this);
        //set the input node for the calendar
        this.startDateCalendar.set('inputNode', container.one('#newChecklistInstance_startDate'));
        //render the calendar
        this.startDateCalendar.render();

        return this;
      },
      destructor: function() {
        if (this.startDateCalendar) {
          this.startDateCalendar.destroy();
          delete this.startDateCalendar;
        }
      }
    }),
    ChecklistInstanceFixedEndDateView = Y.Base.create('checklistInstanceFixedEndDateView', BaseChecklistInstanceView, [], {
      template: Y.Handlebars.templates["checklistInstanceFixedEndDateDialog"],
      initializer: function() {
        this.endDateCalendar = new Y.usp.CalendarPopup({
          iconClass: 'fa fa-calendar',
          pickerStyle: 'DateTime',
          'minimumDate': new Date()
        });
        //set as bubble targets
        this.endDateCalendar.addTarget(this);
        //set the default end date to that calculated within the checklist definition
        this.get('model')._set('endDate', this.get('model').get('endDate'));
      },
      render: function() {
        var container = this.get('container');
        //call super class
        ChecklistInstanceFixedEndDateView.superclass.render.call(this);
        //set the input node for the calendar
        this.endDateCalendar.set('inputNode', container.one('#newChecklistInstance_endDate'));
        //render the calendar
        this.endDateCalendar.render();

        return this;
      },
      destructor: function() {
        if (this.endDateCalendar) {
          this.endDateCalendar.destroy();
          delete this.endDateCalendar;
        }
      }
    });

  //Import display warnings form (used for displaying warnings following import) - simple YUI form
  Y.namespace('app').ExistingGroupChecklistInstancesModel = Y.Base.create('existingGroupChecklistInstancesModel', Y.Model, [Y.ModelSync.REST], {});
  //Model instance used in dialog - NOTE this is not designed to be persisted, rather to gather the user data and pass it back to the calling view,
  //We use the ModelFormLink to populate our form
  Y.namespace('app').NewChecklistInstanceModel = Y.Base.create('newChecklistInstanceModel', Y.Model, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
    form: '#newChecklistInstance',
    initializer: function(cfg) {
      this.requireStartDate = !!cfg.requireStartDate;
      this.requireEndDate = !!cfg.requireEndDate;
      this.requireOwnership = !!cfg.requireOwnership;
      this.isPotentialOwnerNotEmpty = !!cfg.isPotentialOwnerNotEmpty;
    },
    validate: function(attributes, callback) {
      var errs = {},
        startDateTime,
        endDateTime;
      //if start date is required - then validate that attribute
      if (this.requireStartDate) {
        if (L.isNumber(attributes.startDate)) {
          startDateTime = new Date(attributes.startDate);
        } else {
          startDateTime = Y.USPDate.parseDateTime(attributes.startDate);
        }
        if (!L.isDate(startDateTime)) {
          errs['startDate'] = 'Please enter a valid start date and time for this work list.';
        } else {
          if (startDateTime.getTime() > new Date().getTime()) {
            errs['startDate'] = 'Start date must be in a date in the past.';
          }
        }
      }
      if (this.requireEndDate) {
        if (L.isNumber(attributes.endDate)) {
          endDateTime = new Date(attributes.endDate);
        } else {
          endDateTime = Y.USPDate.parseDateTime(attributes.endDate);
        }
        if (!L.isDate(endDateTime)) {
          errs['endDate'] = 'Please enter a valid end date and time for this work list.';
        } else {
          if (endDateTime.getTime() < new Date().getTime()) {
            errs['endDate'] = 'End date may not be in the past.';
          }
        }
      }
      if (this.requireOwnership && this.isPotentialOwnerNotEmpty) {
        if (L.trim(attributes.ownership) === '') {
          errs['ownership'] = 'Please select a valid owner for this work list instance.';
        }
        if (null === attributes.ownerSubjectType || L.trim(attributes.ownerSubjectType) === '') {
          errs['ownerSubjectType'] = 'Please select either a team or individual as owner.';
        }
      }
      if (O.keys(errs).length > 0) {
        return callback({
          code: 400,
          msg: {
            validationErrors: errs
          }
        });
      }
      //no errors
      return callback();
    }
  }, {
    _NON_ATTRS_CFG: ['requireOwnership', 'requireStartDate', 'requireEndDate'],
    ATTRS: {
      'ownership': {
        value: null,
        setter: function(value) {
          if(value) {
            var ids = value.split(',');
            this.setAttrs({
              'ownerId': ids[0],
              //if owner is ORGANISATION then both ownerId and owningTeamId are same (ids[1] is undefined)
              'owningTeamId': ids[1] || ids[0]
            });
          }
        }
      },
      'ownerId': {
        value: null
      },
      'owningTeamId': {
        value: null
      },
      'checklistDefinitionId': {
        value: null
      },
      'ownerSubjectType': {
        value: null
      },
      'startDate': {
        value: null,
        setter: function(val) {
          var parsedDateTime = Y.USPDate.parseDateTime(val);
          if (L.isDate(parsedDateTime)) {
            return parsedDateTime.getTime();
          }
          return val;
        },
        getter: function(value) {
          if (L.isNumber(value)) {
            return value;
          }
          if (L.isDate(value)) {
            return value.getTime();
          }
          if (L.trim(value) === '') {
            return null;
          }
          //just return the value
          return value;
        }
      },
      'endDate': {
        value: null,
        setter: function(val) {
          var parsedDateTime = Y.USPDate.parseDateTime(val);
          if (L.isDate(parsedDateTime)) {
            return parsedDateTime.getTime();
          }
          return val;
        },
        getter: function(value) {
          if (L.isNumber(value)) {
            return value;
          }
          if (L.isDate(value)) {
            return value.getTime();
          }
          if (L.trim(value) === '') {
            return null;
          }
          //just return the value
          return value;
        }
      }
    }
  });
  var cancelButton = {
    name: 'cancelButton',
    labelHTML: '<i class="fa fa-times"></i> No',
    classNames: 'pure-button-secondary',
    action: 'handleCancel',
    section: Y.WidgetStdMod.FOOTER
  };

  Y.namespace('app').ChecklistInstanceDialog = Y.Base.create('checklistInstanceDialog', Y.usp.app.AppDialog, [], {
    views: {
      checklistProcessing: {
        type: ChecklistProcessingView
      },
      showExistingInstance: {
        type: ShowExistingChecklistInstanceView,
        buttons: [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Yes',
            classNames: 'pure-button-primary',
            action: function(e) {
              this._handleView(e);
            },
            disabled: true
          },
          cancelButton
        ]
      },
      showExistingGroupChecklistInstances: {
        type: ShowExistingGroupChecklistInstancesView,
        buttons: [Y.merge(cancelButton, {
          labelHTML: '<i class="fa fa-check"></i> OK',
          action: function(e) {
            this._handleAcknowledge(e);
          }
        })]
      },
      addChecklistInstance: {
        type: AddChecklistInstanceView,
        buttons: [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
              this._handlePopulateModel(e);
            },
            disabled: true
          },
          Y.merge(cancelButton, {
            labelHTML: '<i class="fa fa-times"></i> Cancel'
          })
        ]
      }
    },
    _handleCancel: function(e) {
      e.preventDefault();
      this.hide();
    },
    _handleAcknowledge: function(e) {
      var view = this.get('activeView'),
        checklistResourceURIs = view.get('model').get('checklistResourceURIs');
      e.preventDefault();
      //hide the dialog
      this.hide();
      //Only if >1 checklist was added do we show the success message
      if (checklistResourceURIs.length > 0) {
        //fire success message
        Y.fire('infoMessage:message', {
          message: view.get('successMessage')
        });
      }
      //fire event to return to checklist results view
      this.fire('viewChecklistResults');
    },
    _handleView: function(e) {
      e.preventDefault();
      // Get the view and the model
      var view = this.get('activeView'),
        model = view.get('model');
      //hide the dialog
      this.hide();
      // view the existing checklist by firing an event which will be picked up in the view            
      view.fire('view', {
        id: model.get('id')
      });
    },
    _handlePopulateModel: function(e) {
      var view = this.get('activeView'),
        model = view.get('model'),
        checklistDefiniton = view.get('checklistDefinition');
      //prevent default button action
      e.preventDefault();
      //perform a save - this is only saving into the model - not to the server (assuming configuration does not change)
      model.save({
          transmogrify: Y.usp.checklist.NewChecklistInstance
      }, Y.bind(function(err, response) {
        //slightly curious case here as there is no SYNC layer, then err is undefined on success
        if (err === null || err === undefined) {
          //fire an event to indicate success - NOTE no data is saved here, it is up to the caller to work out what to save in this instance
          view.fire('createChecklist', {
            subject: view.get('subject'),
            checklistData: model,
            checklistDefinition: checklistDefiniton
          });
        }
      }, this));
    }
  }, {
    ATTRS: {
      buttons: {
        value: ['closeButton']
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'usp-view',
    'app-dialog',
    'model-form-link',
    'model-transmogrify',
    'form-util',
    'handlebars-helpers',
    'handlebars-checklist-templates',
    'handlebars-checklist-partials',
    'calendar-popup',
    'usp-date',
    'checklist-ownership-util',
    'checklist-instance-ownership-views'
  ]
});