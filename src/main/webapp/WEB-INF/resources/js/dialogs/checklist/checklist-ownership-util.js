/**
 A collection of helper methods for checklist ownership
 @module checklist-ownership-util
 @author Richard Gibson
 @since 1.0.0
 */
YUI.add('checklist-ownership-util', function (Y) {
    var A=Y.Array, L=Y.Lang;
    
    //AOP to add additional schema values to PaginatedPersonOrganisationRelationshipList
    Y.Do.after(function() {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({
            key: 'organisationVO',
            locator: 'organisationVO'
        });
    }, Y.usp.relationship.PersonOrganisationRelationship, 'getSchema', Y.usp.relationship.PersonOrganisationRelationship);
    
    function ChecklistOwnershipUtil() {}

    ChecklistOwnershipUtil.getPotentialOwnersPromise=function(checklistDefinitionId, url) {
        return new Y.Promise(function(resolve, reject) {
            var model = new Y.usp.ModelList({
                url: L.sub(url, {
                    id: checklistDefinitionId
                })
            }).load(function(err) {
                if (err !== null) {
                    //failed to get the existing form instances for some reason so reject the promise
                    reject(err);
                }
                //success, so resolve the promise
                resolve(model);
            });
        });
    };
    
    ChecklistOwnershipUtil.getUserTeamsPromise=function(currentPersonId, url) {
        return new Y.Promise(function(resolve, reject) {
            var model = new Y.usp.relationship.PaginatedPersonOrganisationRelationshipList({
                url: L.sub(url, {
                    id: currentPersonId
                })
            }).load(function(err) {
                if (err !== null) {
                    //failed to get the existing form instances for some reason so reject the promise
                    reject(err);
                }
                //success, so resolve the promise
                resolve(model);
            });
        });
    };    
     
    Y.namespace('app.checklist').ChecklistOwnershipUtil = ChecklistOwnershipUtil;
}, '0.0.1', {
  requires: ['yui-base', 
             'promise',
             'usp-relationship-PersonOrganisationRelationship']
});