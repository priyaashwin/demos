YUI.add('checklist-instance-ownership-views', function (Y) {
    var A=Y.Array, L=Y.Lang,DB = Y.usp.DataBindingUtil;
        
    Y.namespace('app').ChecklistInstanceOwnershipView = Y.Base.create('checklistInstanceOwnershipView', Y.View, [], {
        template: Y.Handlebars.templates["checklistInstanceOwnershipDialog"],
        events: {
            'input[name="ownerSubjectType"]': {
                change: '_handleDataChange'
            }
        },
        _handleDataChange: function(e) {
            var target = e.currentTarget,
                name = target.get('name');
            this.get('model').set(name, DB.getElementValue(target._node));
        },
        initializer: function(config) {
            config.model.on('change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                model = this.get('model'),
                labels = this.get('labels'),
                candidateOwnership = this.get('candidateOwnership') || [],
                teamCandidates = [],
                individualCandidates = [],
                warningMessage = null,
                ownerTypeSwitch = false,
                deDupeTeams = function(a) {
                    var t = {};
                    return a.filter(function(c) {
                        return t[c.owningTeamDetails.id] === true ? false : (t[c.owningTeamDetails.id] = true);
                    });
                },
                reArrangeIndividual = function(obj) {
                    var results = {};
                    obj.forEach(function(entry) {
                        var currentTeamId = entry.owningTeamDetails.id
                        if (!results[currentTeamId]) {
                            var users = obj.filter(function(person) {
                                return currentTeamId === person.owningTeamDetails.id ? person.ownerDetails : false;
                            }).sort(function(a, b) {
                                return (a.ownerDetails.name || '').localeCompare(b.ownerDetails.name || '');
                            });
                            results[currentTeamId] = {
                                owningTeamDetails: entry.owningTeamDetails,
                                potentialOwners: users
                            };
                        }
                    });

                    //convert results object values to an array
                    var array = Object.keys(results).map(function(key) {
                        return results[key];
                    });

                    return array;
                };
            //determine if candidate ownership include team/person or both
            teamCandidates = deDupeTeams(candidateOwnership.filter(function(candidate) {
                return candidate.ownerDetails.id === candidate.owningTeamDetails.id;
            })).sort(function(a, b) {
                return (a.owningTeamDetails.name || '').localeCompare(b.owningTeamDetails.name || '');
            });
            individualCandidates = reArrangeIndividual(candidateOwnership.filter(function(candidate) {
                return candidate.ownerDetails.id !== candidate.owningTeamDetails.id;
            })).sort(function(a, b) {
                return (a.owningTeamDetails.name || '').localeCompare(b.owningTeamDetails.name || '');
            });
            if (teamCandidates.length > 0 && individualCandidates.length > 0) {
                //here we have a mixture of individual and team candidates
                //the user must pick which type they want
                ownerTypeSwitch = true;
                //leave the model ownerSubjectType at the default value
            } else if (teamCandidates.length > 0) {
                //set model type to ORGANISATION
                model.set('ownerSubjectType', 'ORGANISATION', {
                    silent: true
                });
            } else if (individualCandidates.length > 0) {
                //set model type to PERSON
                model.set('ownerSubjectType', 'PERSON', {
                    silent: true
                });
            } else {
                //goodness me - the user can't pick an owner
                warningMessage = labels.warningMessage;
            }
            //render the main template
            container.setHTML(this.template({
                modelData: model.toJSON(),
                ownerTypeSwitch: ownerTypeSwitch,
                warningMessage: warningMessage,
                teamCandidates: teamCandidates,
                individualCandidates: individualCandidates,
                labels: labels
            }));
            return this;
        }
    }, {
        ATTRS: {
            /**
             * @attribute labels Labels to drive the template
             */
            labels: {},
            /**
             * @attribute candidateOwnership Array of candidate ownership information
             */
            candidateOwnership: {
                value: []
            }
        }
    });
   
    var ChecklistBaseOwnerView = Y.Base.create('checklistBaseOwnerView', Y.View, [],{
        events:{
            'input[name="assignToSelect"]': {
                change: '_handleAssignToSelectChange'
            }
        },
        initializer:function(config){
            this.after('assignToSelectChange', this._handleAfterAssignToSelectChange, this);
            
            //setup view
            this._getNewSelectView(this.get('assignToSelect'));
        },
        _handleAssignToSelectChange: function(e) {
            var target = e.currentTarget,
                assignToSelect = DB.getElementValue(target._node);

            if (assignToSelect === 'false') {
                this.get('model').setAttrs({
                    'ownerId': this.get('personId'),
                    'owningTeamId': this.get('owningTeamId')
                });
            }
            //set value against this view attribute
            this.set('assignToSelect', assignToSelect ==='true');
        },
        _getNewSelectView:function(toSelect){
            //now create the new view
            var _view=(toSelect===true)?this._getNewSelectionView():null;
            if(_view){
                //add event bubble target
                _view.addTarget(this);
            }                   
            this.selectionView=_view;  
        },
        _handleAfterAssignToSelectChange:function(e){
            //destroy existing view
            if(this.selectionView){
                //unbind from event bubble
                this.selectionView.removeTarget(this);
                //destroy the view
                this.selectionView.destroy();
            }
            
            this._getNewSelectView(e.newVal);
            
            //re-render
            this.render();
        },
        /**
         * Override this to return appropriate selection view
         */
        _getNewSelectionView:function(){
           return null;
        },
        destructor:function(){
            //destroy ownership view
            if (this.selectionView) {
                this.selectionView.removeTarget(this);
                this.selectionView.destroy();
                delete this.selectionView;
            }
        }
    },{
        ATTRS:{
            assignToSelect:{
                value:false
            },
            labels:{
                value:{}
            }
        }
    }),
    ChecklistPersonOwnerView = Y.Base.create('checklistPersonOwnerView', ChecklistBaseOwnerView, [],{
        template: Y.Handlebars.templates["checklistInstanceReassignToPerson"],
        render:function(){
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment()),
                label=this.get('labels'),
                selectNode;

            //render the main template
            contentNode.setHTML(this.template({
                labels: this.get('labels'),
                assignToSelect: this.get('assignToSelect'),
                personId:this.get('personId'),
                teams: this.get('teams')
            }));

            selectNode = contentNode.one('#my-teams-list');
            if (selectNode) {
                //set selection to current team
                selectNode.set('value', this.get('model').get('owningTeamId'));
            }

            if(this.selectionView){
                //add our fragments to the content node
                contentNode.one('#personReassignContent').append(this.selectionView.render().get('container'));
            }
            //set the fragment into the view container
            this.get('container').setHTML(contentNode);
            
            return this;
        },
        _getNewSelectionView:function(){
            //show person autocomplete
            return new ChecklistPersonOwnerAutoCompleteView({
                autocompleteURL:this.get('autocompleteURL'),
                model:this.get('model'),
                labels:this.get('labels')||{}
            });
        }
    },{
        ATTRS:{            
            personId:{
                value:null
            },
            owningTeamId:{
                value:null
            },
            teams:{
                value:[]
            },
            autocompleteURL:{

            },
            assignToSelect:{
                getter:function(val){
                    //if the user is not in a team - don't allow selection
                    var teams=this.get('teams')||[];
                    if(teams.length===0){
                        return true;
                    }
                    return val;
                }
            }
        }
    }),
    ChecklistPersonOwnerAutoCompleteView = Y.Base.create('checklistPersonOwnerAutoCompleteView', Y.View, [],{
        initializer:function(config){
            this.personAutoCompleteView=new Y.app.ProfessionalRolePersonAutoCompleteResultView({
                autocompleteURL:config.autocompleteURL,
                inputName:'ownerId',
                formName:'checklistReassignForm',
                rowTemplate:'ROW_TEMPLATE_SIMPLE',
                preventSecuredSelection:false,
                labels:config.labels
            });
            
            this.personAutoCompleteView.addTarget(this);
        },
        destructor:function(){
            if(this.personAutoCompleteView){
                this.personAutoCompleteView.removeTarget(this);
                this.personAutoCompleteView.destroy();
                delete this.personAutoCompleteView;
            }
        },
        render:function(){
            var container=this.get('container');
            
            container.setHTML(this.personAutoCompleteView.render().get('container'));
            
            return this;
        }
    }),
    ChecklistOrganisationOwnerView = Y.Base.create('checklistOrganisationOwnerView', ChecklistBaseOwnerView, [],{
        template: Y.Handlebars.templates["checklistInstanceReassignToOrganisation"],
        render:function(){
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment()),
                label=this.get('labels'),
                selectNode;

            //render the main template
            contentNode.setHTML(this.template({
                labels: this.get('labels'),
                assignToSelect: this.get('assignToSelect'),
                teams:this.get('teams')
            }));

            selectNode = contentNode.one('#organisation-teams-list');
            if (selectNode) {
                //set selection to current team
                selectNode.set('value', this.get('model').get('owningTeamId'));
            }

            if(this.selectionView){
                //add our fragments to the content node
                contentNode.one('#organisationReassignContent').append(this.selectionView.render().get('container'));
            }
            //set the fragment into the view container
            this.get('container').setHTML(contentNode);
            
            return this;
        },
        _getNewSelectionView:function(){
            //show person autocomplete
            return new ChecklistOrganisationOwnerAutoCompleteView({
                autocompleteURL:this.get('autocompleteURL'),
                model:this.get('model'),
                labels:this.get('labels')||{}
            });
        }
    },{
        ATTRS:{
            personId:{
                value:null
            },
            owningTeamId:{
                value:null
            },
            teams:{
                value:[]
            },
            autocompleteURL:{

            },
            assignToSelect:{
                getter:function(val){
                    //if the user is not in a team - don't allow selection
                    var teams=this.get('teams')||[];
                    if(teams.length===0){
                        return true;
                    }
                    return val;
                }
            }
        }
    }),
    ChecklistOrganisationOwnerAutoCompleteView = Y.Base.create('checklistOrganisationOwnerAutoCompleteView', Y.View, [],{
        initializer:function(config){
            this.organisationAutoCompleteView=new Y.app.OrganisationAutoCompleteResultView({
                autocompleteURL:config.autocompleteURL,
                inputName:'ownerId',
                formName:'checklistReassignForm',
                rowTemplate:'ROW_TEMPLATE_SIMPLE',
                labels:config.labels
            });
            
            this.organisationAutoCompleteView.addTarget(this);
        },
        destructor:function(){
            if(this.organisationAutoCompleteView){
                this.organisationAutoCompleteView.removeTarget(this);
                this.organisationAutoCompleteView.destroy();
                delete this.organisationAutoCompleteView;
            }
        },
        render:function(){
            var container=this.get('container');
            
            container.setHTML(this.organisationAutoCompleteView.render().get('container'));
            
            return this;
        }
    }),
    CheckListPersonOwnerSelectView = Y.Base.create('checkListPersonOwnerSelectView', Y.View, [],{
        template: Y.Handlebars.templates["checklistInstanceReassignToPersonSelect"],
        render: function() {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.setHTML(this.template({
                selectedPersonTeams: this.get('selectedPersonTeams'),
                label: this.get('selectedPersonTeamsListLabel')
            }));

            container.setHTML(contentNode);

            return this;
        }
    },{
        ATTRS: {
          selectedPersonTeams:{
              value:[]
          },
          selectedPersonTeamsListLabel:{
              value:null
          }
        }
    });
    
    Y.namespace('app').ChecklistInstanceReassignView = Y.Base.create('checklistInstanceReassignView', Y.View, [], {
        events: {
            '#checklistReassignForm': {
                submit: '_preventSubmit'
            },
            'input[name="ownerSubjectType"]': {
                change: '_handleDataChange'
            },
            'input[name="reassignBatch"]': {
              change: '_handleReassignBatchChange'
            },
            'select[name="ownerId"]': {
                change: '_handleOrganisationTeamSelectChange'
            }
        },
        template: Y.Handlebars.templates["checklistInstanceReassignDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        initializer: function(config) {
            var model=this.get('model'),
                ownerSubjectType=model.get('ownerSubjectType'),
                _view,
                checklistModel=this.get('checklistModel');

            //setup initial view
            _view=this._getNewSelectionView(ownerSubjectType);
            if(_view){
                //add event bubble target
                _view.addTarget(this);
            }                   
            this.selectionView=_view;
            this._evtHandles=[
                //re-render when the owner subject type changes
                model.after('ownerSubjectTypeChange', this._afterOwnerSubjectTypeChange, this),
            
                //re-render only after load - we don't want to blow away the rendered dialog at any other time
                model.after('load', function(e) {
                    var model = this.get('model');

                    //set ownerId and owningTeamId to current session
                    model.setAttrs({
                        'ownerId': this.get('currentPersonId'),
                        'owningTeamId': this.get('owningTeamId')
                    });

                    //perform the initial render
                    this.render();
                }, this),
                this.after('*:selectedPersonChange', this._handlePersonAutocompleteChange, this),
                this.after('*:selectedOrganisationChange', this._handelOrganisationAutocompleteChange, this)
            ];
        },
        _handleDataChange: function(e) {
            var type = DB.getElementValue(e.currentTarget._node),
                valuesToUpdate = {
                    'owningTeamId': this.get('owningTeamId'),
                    'ownerSubjectType': type
                };

            if (type === 'PERSON') {
                valuesToUpdate.ownerId = this.get('currentPersonId');
            } else {
                valuesToUpdate.ownerId = this.get('owningTeamId');
            };

            this.get('model').setAttrs(valuesToUpdate, {silent: true});
        },
        _handleReassignBatchChange:function(e){
          var target = e.currentTarget;
          var reassignBatch=false;
          if(target.get('checked')){
            reassignBatch=true;
          }else{
            reassignBatch=false;
          }
          this.set('reassignBatch', reassignBatch);
        },
        _handleOrganisationTeamSelectChange: function(e) {
            var node = e.currentTarget._node;
            this.get('model').set('owningTeamId', node.value);
        },
        _handelOrganisationAutocompleteChange: function(e) {
            if (e.newVal) {
                this.get('model').set('owningTeamId', e.newVal.id);
            }
        },
        _handlePersonAutocompleteChange:function(e) {
            var selectedPerson = e.newVal;
            if(this.personSelectView) {
                this.personSelectView.get('container').setHTML('');
                //unbind from event bubble
                this.personSelectView.removeTarget(this);
                //destroy the view
                this.personSelectView.destroy();
            }
            if (selectedPerson) {
                var url = this.get('personTeamsURL'),
                    getTeamsURL = L.sub(url, {id: selectedPerson.id});

                new Y.usp.Model({url: getTeamsURL}).load(function(err, res) {
                    if(res.response) {
                        var results = JSON.parse(res.response).results,
                            selectedPersonTeamsListLabel = this.get('labels').person.teamSelect + ' ' + selectedPerson.name;

                        if (results.length === 0) {
                            this.get('model').set('owningTeamId', null);
                        } else {
                            var selectedPersonTeams = results.map(function(personOrganisationRelationship) {
                                return personOrganisationRelationship.organisationVO;
                            }).sort(function(a, b) {
                                return (a.name || '').localeCompare(b.name || '') || (a.organisationIdentifier || '').localeCompare(b.organisationIdentifier || '');
                            });

                            this.personSelectView = new CheckListPersonOwnerSelectView({
                                selectedPersonTeams: selectedPersonTeams,
                                selectedPersonTeamsListLabel: selectedPersonTeamsListLabel
                            }).addTarget(this);

                            Y.one('#person-teams-select').append(this.personSelectView.render().get('container'));
                        }
                    }
                }.bind(this));
            }
        },
        _afterOwnerSubjectTypeChange:function(e){
            var _view;
            //destroy existing view
            if(this.selectionView){
                //unbind from event bubble
                this.selectionView.removeTarget(this);
                //destroy the view
                this.selectionView.destroy();
            }
            //now create the new view
            _view=this._getNewSelectionView(e.newVal);
            if(_view){
                //add event bubble target
                _view.addTarget(this);
            }                   
            this.selectionView=_view;
            
            //re-render
            this.render();
        },        
        _getNewSelectionView:function(ownerSubjectType){
            var _view,
               labels=this.get('labels')||{},
               currentPersonId=this.get('currentPersonId'),
               owningTeamId = this.get('owningTeamId');

            switch(ownerSubjectType){
                case 'ORGANISATION':
                    //show organisation view
                    _view=new ChecklistOrganisationOwnerView({
                        autocompleteURL:this.get('organisationAutocompleteURL'),
                        model:this.get('model'),
                        labels:labels.organisation||{},
                        personId: owningTeamId,
                        owningTeamId: owningTeamId,
                        teams:this.get('currentPersonTeams')
                    });
                break;
                case 'PERSON':
                    //show person view
                    _view=new ChecklistPersonOwnerView({
                        autocompleteURL:this.get('personAutocompleteURL'),
                        model:this.get('model'),
                        labels:labels.person||{},
                        personId:currentPersonId,
                        owningTeamId: owningTeamId,
                        teams:this.get('currentPersonTeams')
                    });
                break;
            }
            return _view;
        },            
        render: function() {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment()),
                label=this.get('labels'),
                modelData=this.get('model').toJSON();

            //render the main template
            contentNode.setHTML(this.template({
                modelData: modelData,
                subject: modelData.subject||{},
                otherData: this.get('otherData'),
                checklist: this.get('checklistModel'),
                labels: this.get('labels'),
                reassignBatch: this.get('reassignBatch')
            }));
            
            if(this.selectionView){
                //add our fragments to the content node
                contentNode.one('#reassignContent').append(this.selectionView.render().get('container'));
            }
            //set the fragment into the view container
            this.get('container').setHTML(contentNode);
            
            return this;
        },
        destructor: function() {
            //destroy ownership view
            if (this.selectionView) {
                this.selectionView.removeTarget(this);
                this.selectionView.destroy();
                delete this.selectionView;
            }

            if(this.personSelectView) {
                this.personSelectView.removeTarget(this);
                this.personSelectView.destroy();
                delete this.personSelectView;
            }
        }
    }, {
        ATTRS: {
            labels:{
                value:{}
            },
            personAutocompleteURL:{
                
            },
            organisationAutocompleteURL:{
                
            },
            currentPersonId:{
                value:null
            },
            currentPersonTeams:{
                value:[]
            },
            checklistModel:{
              
            },
            reassignBatchURL:{
              
            },
            reassignURL:{
              
            },
            reassignBatch:{
              value:false
            }
        }
    });       
}, '0.0.1', {
  requires: ['yui-base', 
             'data-binding',
             'handlebars-helpers',
             'handlebars-checklist-templates',
             'handlebars-checklist-partials',
             'person-autocomplete-view',
             'organisation-autocomplete-view'
             ]
});