YUI.add('checklist-instance-task-reassign-dialog-views', function(Y) {

    Y.namespace('app').ChecklistInstanceTaskReassignDialogView = Y.Base.create('checklistInstanceTaskReassignDialogView', Y.usp.ReactView, [], {
        reactFactoryType: uspChecklistTaskAndPriority.UpdateChecklistTaskOwnershipDialog,
        renderComponent: function () {
            var labels = this.get('labels'),
                enums = this.get('enumTypes'),
                dialogConfig = this.get('dialogConfig'),     
                searchConfig = this.get('searchConfig'),
                urls = this.get('urls'),
                permissions = this.get('permissions');
                
            if (this.reactAppContainer) {
                var component;
  
                ReactDOM.render(this.componentFactory({
                    labels: labels,
                    enums: enums,
                    dialogConfig: dialogConfig,
                    searchConfig: searchConfig,
                    urls: urls,
                    permissions: permissions
                }), this.reactAppContainer.getDOMNode(), function () {
                    component = this;
                });
  
                this.component = component;
            }
        },
        showDialog: function(checklistId) {
          if (this.component) {
              this.component.showDialog(checklistId);
          }
        }
    }, {
        ATTRS: {
            dialogConfig: {},
            searchConfig: {},
            labels: {},
            enumTypes: {},
            urls: {},
        }
    });
  }, '0.0.1', {
    requires: ['yui-base',
      'view',
      'usp-view',
      'usp-react-view'
    ]
  });