YUI.add('user-selfCredentialChange-dialog-views', function(Y) {
    var A = Y.Array, E = Y.Escape, L = Y.Lang, O = Y.Object;
    
    Y.namespace('app').UpdateSelfPasswordChangeView = Y.Base.create('updateSelfPasswordChangeView', Y.usp.security.UpdateSecurityUserSelfPasswordWithExistingView, [], {
        // bind template
        template : Y.Handlebars.templates.updateSelfPasswordChangeDialog,
        events : {
            '#updateSelfPasswordChangeForm' : {
                submit : '_preventSubmit'
            }
        },
        _preventSubmit : function(e) {
            e.preventDefault();
        },
        render : function() {
            var container = this.get('container'), html = this.template({
                labels : this.get('labels'),
                modelData : this.get('model').toJSON(),
                targetName : this.get('targetName'),
                targetIdentifier : this.get('targetIdentifier')
            });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            
            return this;
        }
    });
    
    Y.namespace('app').UpdateSelfSecondaryPasswordChangeView = Y.Base.create('updateSelfSecondaryPasswordChangeView', Y.usp.security.UpdateSecurityUserSelfSecondaryPasswordWithExistingView, [], {
        // bind template
        template : Y.Handlebars.templates.updateSelfSecondaryPasswordChangeDialog,
        events : {
            '#updateSelfSecondaryPasswordChangeForm' : {
                submit : '_preventSubmit'
            }
        },
        _preventSubmit : function(e) {
            e.preventDefault();
        },
        render : function() {
            var container = this.get('container'), html = this.template({
                labels : this.get('labels'),
                modelData : this.get('model').toJSON(),
                targetName : this.get('targetName'),
                targetIdentifier : this.get('targetIdentifier')
            });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            
            return this;
        }
    });
    
    Y.namespace('app').UpdateNotificationSettingsView = Y.Base.create('updateNotificationSettingsView', Y.View, [], {
        template : Y.Handlebars.templates.updateNotificationSettingsDialog,
        events : {
            '#notificationSettingsForm' : {
                submit : '_preventSubmit'
            }
        },
        _preventSubmit : function(e) {
            e.preventDefault();
        },

        initializer: function() {
          this.subscriptionModel = new Y.usp.notification.UserNotificationSubscription({
            url: this.get('viewURL')
          });
          this.realmsModel = new Y.usp.notification.NotificationRealmList({
            url: this.get('realmsURL')
          });
          this.evtHandlers = [
            this.realmsModel.on('load', this.render, this)
          ];

          this.subscriptionModel.load(Y.bind(function(err, res) {
            var subscription = JSON.parse(res.responseText);

            if (err && err.code === 404) {
              this.model = new Y.app.NewUserNotificationSubscriptionForm({
                url: this.get('createURL'),
                objectVersion: 0
              });
            } else if (!err) {
              this.model = new Y.app.UpdateUserNotificationSubscriptionForm({
                url: L.sub(this.get('updateURL'), {
                  id: subscription.id
                }),
                id: subscription.id,
                objectVersion: subscription.objectVersion
              });
            }

            this.model.addTarget(this);
            this.set('model', this.model);
            this.render();
          }, this));
          this.realmsModel.load();
        },

        destructor: function() {
          if (this.evtHandlers) {
            this.evtHandlers.forEach(function(evt) {
              evt.detach();
            });
            this.evtHandlers = null;
          }

          if (this.model) {
            this.model.destroy();
            delete this.model;
          }

          if (this.subscriptionModel) {
            this.subscriptionModel.destroy();
            delete this.subscriptionModel;
          }

          if (this.realmsModel) {
            this.realmsModel.destroy();
            delete this.realmsModel;
          }
        },

        _setActiveChannels: function(activeChannels) {
          var availableChannels = ['sms', 'smtp', 'websocket'];
          availableChannels.forEach(function(channel) {
            Y.one('#activeChannel' + channel).set('checked', activeChannels.indexOf(channel) >= 0);
          });
        },

        _setAssignedRealms: function(availableRealms, assignedRealms) {
          availableRealms.forEach(function(realm) {
            Y.one('#assignedRealm' + realm.sanitisedName).set('checked', assignedRealms.indexOf(realm.name) >= 0);
          });
        },

        _setCaseEncompassment: function(encompassTeam) {
          var radioId = encompassTeam ? 'True' : 'False';
          Y.one('#encompassTeam' + radioId).set('checked', true);
        },

        _checkBrowserNotificationPermission: function() {
          if ('Notification' in window && Notification.permission === 'denied') {
            var blockedMessage = Y.one('#browser-notification-blocked');
            if (blockedMessage) {
              blockedMessage.show();
            }
          }
        },

        render: function() {
          var container = this.get('container'),
            realms = this.realmsModel.toJSON().map(function(realm) {
              realm.sanitisedName = realm.name.replace(/\s+/g, '');
              return realm;
            }),
            subscription = this.subscriptionModel.toJSON(),
            html = this.template({
              labels : this.get('labels'),
              realms: realms
            });

          container.setHTML(html);
          this._checkBrowserNotificationPermission();
          
          if (subscription.id) {
            this._setActiveChannels(subscription.activeChannels);
            this._setAssignedRealms(realms, subscription.assignedRealms);
            this._setCaseEncompassment(subscription.encompassTeam);
          }
          return this;
        }
    });

    var amendUserNotificationSubscriptionAttrs = function(attrs) {
      if (attrs.activeChannels === null) {
        attrs.activeChannels = [];
      }
      
      if (attrs.assignedRealms === null) {
        attrs.assignedRealms = [];
      }
      
      if (attrs.encompassTeam === null) {
        attrs.assignedRealms = false;
      }
      
      return attrs;
    };

    Y.namespace('app').NewUserNotificationSubscriptionForm = Y.Base.create('newUserNotificationSubscriptionForm', Y.usp.notification.NewUserNotificationSubscription, [
        Y.usp.ModelFormLink
    ], {
        form : '#notificationSettingsForm',
        amendAttrs: amendUserNotificationSubscriptionAttrs
    });

    Y.namespace('app').UpdateUserNotificationSubscriptionForm = Y.Base.create('updateUserNotificationSubscriptionForm', Y.usp.notification.UpdateUserNotificationSubscription, [
        Y.usp.ModelFormLink
    ], {
        form : '#notificationSettingsForm',
        amendAttrs: amendUserNotificationSubscriptionAttrs
    });
    
    Y.namespace('app').UpdateSelfPasswordChangeForm = Y.Base.create('updateSelfPasswordChangeForm', Y.usp.security.UpdateSecurityUserSelfPasswordWithExisting, [
        Y.usp.ModelFormLink
    ], {
        form : '#updateSelfPasswordChangeForm',
        idAttribute : 'securityUserId'
    });

    Y.namespace('app').UpdateSelfSecondaryPasswordChangeForm = Y.Base.create('updateSelfSecondaryPasswordChangeForm', Y.usp.security.UpdateSecurityUserSelfSecondaryPasswordWithExisting, [
        Y.usp.ModelFormLink
    ], {
        form : '#updateSelfSecondaryPasswordChangeForm',
        idAttribute : 'securityUserId'
    });
    
    // User Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').UserDialog = Y.Base.create('userDialog', Y.usp.MultiPanelPopUp, [], {
        
        initializer : function() {
            // plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        destructor : function() {
            // unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        // Views that this dialog supports
        views : {
            updateSelfPasswordChange : {
                type : Y.app.UpdateSelfPasswordChangeView,
                buttons : [
                    {
                        section : Y.WidgetStdMod.FOOTER,
                        name : 'saveButton',
                        labelHTML : '<i class="fa fa-check"></i> Save',
                        classNames : 'pure-button-primary',
                        action : function(e) {
                            e.preventDefault();
                            var self = this, container = self.get('boundingBox');
                            // show the mask
                            this.showDialogMask();
                            
                            // Get the view and the model
                            var view = this.get('activeView'), model = view.get('model'), labels = view.get('labels');
                            
                            // Do the save
                            model.save(Y.bind(function(err, response) {
                                // hide the mask
                                this.hideDialogMask();
                                
                                if (err === null) {
                                    this.hide();
                                    
                                    // fire saved event against the view
                                    view.fire('saved', {
                                        id : model.get('securityUserId'),
                                        successMessage : labels.updateSelfPasswordChangeSuccessMessage
                                    });
                                }
                                else {
                                	var errorResponse = Y.JSON.parse(response.responseText);
                                	//TODO Need to find better way to decide which error is being thrown from backend using codes
                                	//If the current password entered is not matched with the user record information need to clear all the fields inside the dialog
                                	if(errorResponse.validationErrors && errorResponse.validationErrors.SecurityUser){
                                		container.one('#currentPassword') ? container.one('#currentPassword').set('value', '') : '';
                                		container.one('#password') ? container.one('#password').set('value', '') : '';
                                		container.one('#confirmPassword') ? container.one('#confirmPassword').set('value', '') : '';
                                		
                                	}
                                	//If the password newly entered does not match with the confirm password then clear only those two fields
                                	else if(errorResponse.validationErrors && (errorResponse.validationErrors.password || errorResponse.validationErrors.confirmPassword)) {
                                		container.one('#password') ? container.one('#password').set('value', '') : '';
                                		container.one('#confirmPassword') ? container.one('#confirmPassword').set('value', '') : '';
                                	}
                                }
                            }, this));
                        },
                        disabled : true
                    }
                ]
            },
            updateSelfSecondaryPasswordChange : {
                type : Y.app.UpdateSelfSecondaryPasswordChangeView,
                buttons : [
                    {
                        section : Y.WidgetStdMod.FOOTER,
                        name : 'saveButton',
                        labelHTML : '<i class="fa fa-check"></i> Save',
                        classNames : 'pure-button-primary',
                        action : function(e) {
                            e.preventDefault();
                            // show the mask
                            this.showDialogMask();
                            var self = this, container = self.get('boundingBox');
                            // Get the view and the model
                            var view = this.get('activeView'), model = view.get('model'), labels = view.get('labels');
                            
                            // Do the save
                            model.save(Y.bind(function(err, response) {
                                // hide the mask
                                this.hideDialogMask();
                                
                                if (err === null) {
                                    this.hide();
                                    
                                    // fire saved event against the view
                                    view.fire('saved', {
                                        id : model.get('securityUserId'),
                                        successMessage : labels.updateSelfSecondaryPasswordChangeSuccessMessage
                                    });
                                }
                                else {
                                	var errorResponse = Y.JSON.parse(response.responseText);
                                	//TODO Need to find better way to decide which error is being thrown from backend using codes
                                	//If the current password entered is not matched with the user record information need to clear all the fields inside the dialog
                                	if(errorResponse.validationErrors && errorResponse.validationErrors.SecurityUser){
                                		container.one('#currentPassword') ? container.one('#currentPassword').set('value', '') : '';
                                		container.one('#secondaryPassword') ? container.one('#secondaryPassword').set('value', '') : '';
                                		container.one('#confirmSecondaryPassword') ? container.one('#confirmSecondaryPassword').set('value', '') : '';
                                		
                                	}
                                	//If the password newly entered does not match with the confirm password then clear only those two fields
                                	else if(errorResponse.validationErrors && (errorResponse.validationErrors.secondaryPassword || errorResponse.validationErrors.confirmSecondaryPassword)) {
                                		container.one('#secondaryPassword') ? container.one('#secondaryPassword').set('value', '') : '';
                                		container.one('#confirmSecondaryPassword') ? container.one('#confirmSecondaryPassword').set('value', '') : '';
                                	}
                                }
                            }, this));
                        },
                        disabled : true
                    }
                ]
            },
            updateNotificationSettings : {
                type : Y.app.UpdateNotificationSettingsView,
                buttons : [
                    {
                        section : Y.WidgetStdMod.FOOTER,
                        name : 'saveButton',
                        labelHTML : '<i class="fa fa-check"></i> Save',
                        classNames : 'pure-button-primary',
                        action : function(e) {
                          e.preventDefault();
                          this.showDialogMask();
                          var view = this.get('activeView'),
                            labels = view.get('labels'),
                            model = view.get('model');

                          model.save(Y.bind(function(err, response) {
                            this.hideDialogMask();
                            if (err === null) {
                              this.hide();
                              Y.fire('updatedNotificationSettings', {
                                successMessage : labels.successMessage,
                                subscription: model.toJSON()
                              });
                            }
                          }, this));
                        },
                        disabled: true
                    }
                ]
            }
        }
    
    }, {
        CSS_PREFIX : 'yui3-panel',
        ATTRS : {
            width : {
                value : 560
            },
            buttons : {
                value : [
                    {
                        name : 'cancelButton',
                        labelHTML : '<i class="fa fa-times"></i> Cancel',
                        classNames : 'pure-button-secondary',
                        action : function(e) {
                            e.preventDefault();
                            this.hide();
                        },
                        section : Y.WidgetStdMod.FOOTER
                    }, 'close'
                ]
            }
        }
    });
    
}, '0.0.1', {
    requires : [
        'yui-base',
        'json-parse',
        'multi-panel-popup',
        'multi-panel-model-errors-plugin',
        'model-form-link',
        'model-transmogrify',
        'form-util',
        'handlebars-helpers',
        'handlebars-user-templates',
        'usp-security-UpdateSecurityUserSelfPasswordWithExisting',
        'usp-security-UpdateSecurityUserSelfSecondaryPasswordWithExisting',
        'app-mixin-multi-view',
        'handlebars-selfCredentialChange-templates',
        'usp-notification-NewUserNotificationSubscription',
        'usp-notification-UpdateUserNotificationSubscription',
        'usp-notification-UserNotificationSubscription',
        'usp-notification-NotificationRealm'
    ]
});
