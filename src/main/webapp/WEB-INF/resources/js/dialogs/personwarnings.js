YUI.add('personwarning-dialog-views', function(Y) {
    var A = Y.Array,
        O = Y.Object,
        L = Y.Lang,
        appPersonUtil = Y.app.utils.person,
        isActive = function(endDate, todaysDate) {
            return !hasValue(endDate) || appPersonUtil.IsFirstGTSecondDate(endDate, todaysDate);
        },
        hasValue = function(data) {
            return data !== undefined && data !== null;
        },
        checkWarning = function(errors, warning, labels, today, index) {
            var warningData = warning.toJSON();
            if (!hasValue(warningData.codedEntryId)) {
                //add an error - category is mandatory
                errors['warningCategory' + index] = L.sub(labels.warningCategoryErrorMessage || 'Warning {number} category is mandatory', {
                    number: warning.index
                });
            }
            if (!hasValue(warningData.startDate)) {
                //add an error - startDate is mandatory
                errors['startDate' + index] = L.sub(labels.startDateRequiredErrorMessage || 'Warning {number} start date is mandatory', {
                    number: warning.index
                });
            }
            if (appPersonUtil.IsFirstGTSecondDate(warningData.startDate, today)) {
                //add an error - startDate needs to be equal or before the current date
                errors['startDate' + index] = L.sub(labels.startDateErrorMessage || 'Warning {number} start date should be equal or before todays date', {
                    number: warning.index
                });
            }
            if (hasValue(warningData.endDate) && appPersonUtil.IsFirstGTSecondDate(warningData.startDate, warningData.endDate)) {
                //add an error - endDate needs to be after the start date
                errors['endDate' + index] = L.sub(labels.endDateErrorMessage || 'Warning {number} end date should be equal or after the start date', {
                    number: warning.index
                });
            }
            return errors;
        },

        addEditWarningSave = function(e) {
            e.preventDefault();
            this.showDialogMask(); //show the mask
            // Get the view and the model
            var view = this.get('activeView'),
                container = view.get('container'),
                model = view.get('model'),
                todaysDate = (new Date()).getTime(),
                personWarningAssignments = model.get('personWarningAssignments');

            personWarningAssignments = personWarningAssignments.filter(function(personWarning) {
                return personWarning.isActive;
            });

            var contentContextChange = {
                noOfPersonWarnings: personWarningAssignments.length,
                warnings: personWarningAssignments.map(function(assignment) {
                    return assignment.codedEntryId;
                })
            };

            if (model.get('personWarnings').length === 0) {
                // add warning
                var newWarningForm = new Y.app.person.NewPersonWarning({
                    url: view.get('addURL')
                });
                newWarningForm.setAttrs({
                    warningAssignmentList: personWarningAssignments.map(function(warningAssignment) {
                        return createNewPersonWarningAssignment(warningAssignment, model);
                    }),
                    personId: model.get('personId')
                }, {
                    //don't trigger the onchange listener as we will re-render after save
                    silent: true
                });
                this.successMessage = getSuccessMessage(newWarningForm.get('warningAssignmentList'), view.get('labels'));

                newWarningForm.save(
                    Y.bind(function(err, response) {
                        //hide the mask
                        this.hideDialogMask();
                        if (err === null) {
                            this.hide();

                            //now fire the success message
                            Y.fire('infoMessage:message', {
                                message: this.successMessage
                            });

                            //ensure the context updates
                            Y.fire("contentContext:update", {
                                data: contentContextChange
                            });

                            //update the graph, if there is one (the fired event is also in the list view
                            //which doesn't need to refresh, hence the 'graphOnly' boolean.)
                            Y.fire('relationship:dataChanged', {
                                graphOnly: true,
                                newRelationshipId: model.get('id')
                            });
                        } else {
                            this.fire('error', {
                                error: err
                            });
                        }
                    }, this));
            } else {
                // edit warning
                var existingWarningId = model.get('personWarnings')[0].id;

                var updateWarningForm = new Y.app.person.UpdatePersonWarning({
                    id: existingWarningId,
                    url: view.get('updateURL')
                });

                updateWarningForm.setAttrs({
                    newWarningAssignments: personWarningAssignments.filter(function(warningAssignment) {
                        return !hasValue(warningAssignment.id)
                    }).map(function(warningAssignment) {
                        return createNewPersonWarningAssignment(warningAssignment, model);
                    }),
                    updateWarningAssignments: personWarningAssignments.filter(function(warningAssignment) {
                        return hasValue(warningAssignment.id)
                    }).map(function(warningAssignment) {
                        return new Y.app.person.UpdatePersonWarningAssignment(warningAssignment);
                    }),
                    objectVersion: model.get('personWarnings')[0].objectVersion
                }, {
                    //don't trigger the onchange listener as we will re-render after save
                    silent: true
                });

                this.successMessage = getSuccessMessage(updateWarningForm.get('newWarningAssignments'), view.get('labels'));

                updateWarningForm.save(
                    Y.bind(function(err, response) {
                        //hide the mask
                        this.hideDialogMask();
                        if (err === null) {
                            this.hide();

                            //now fire the success message
                            Y.fire('infoMessage:message', {
                                message: this.successMessage
                            });

                            //ensure the context updates
                            Y.fire("contentContext:update", {
                                data: contentContextChange
                            });

                            //update the graph, if there is one.
                            if (Y.one('#diagram')) {
                                Y.fire('relationship:dataChanged', {
                                    newRelationshipId: model.get('id')
                                });
                            }
                        } else {
                            this.fire('error', {
                                response: response,
                                error: err
                            });
                        }
                    }, this));


            }
        },

        createNewPersonWarningAssignment = function(warningAssignment, model) {
            var newPersonWarningAssignment = new Y.app.person.NewPersonWarningAssignment(Y.merge(warningAssignment, {
                personId: model.get('personId')
            }));
            newPersonWarningAssignment.setWarningCode();
            return newPersonWarningAssignment;
        },

        getSuccessMessage = function(personWarningAssignments, labels) {
            var successMessage = labels.successMessage;

            personWarningAssignments.forEach(function(wa) {
                if (wa.requiresAuthorisation()) {
                    successMessage = labels.successPendingAuthorisationMessage;
                }
            });

            return successMessage;
        },

        addEditWarningButtons = [{
            action: addEditWarningSave,
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> save',
            classNames: 'pure-button-primary',
            disabled: true
        }];

    Y.namespace('app.person').NewPersonWarningAssignment = Y.Base.create('newPersonWarningAssignment', Y.usp.person.NewPersonWarningAssignment, [], {
        initializer: function(config) {
            this.index = config.index;
        },
        setWarningCode: function() {
            //Convert the coded entry id into a code
            var matchingType = this.getWarningTypeForId(this.get('codedEntryId'));
            if (matchingType) {
                this.set('warningCode', matchingType.code);
            }
        },
        getWarningTypeForId: function(id) {
            //Convert the coded entry id into a code
            var warningTypes = Y.secured.uspCategory.person.warnings.category.codedEntries;
            var matchedType = Object.keys(warningTypes).find(function(warningType) {
                return warningTypes[warningType].id.toString() === id
            });
            var warningTypeMatchingId;
            if (matchedType) {
                warningTypeMatchingId = warningTypes[matchedType];
            }
            return warningTypeMatchingId;
        },
        requiresAuthorisation: function() {
            var matchingType = this.getWarningTypeForId(this.get('codedEntryId'));
            return (matchingType && matchingType.requiresAuthorisation);
        },
    }, {
        //in validation, the number is used to locate the row in error
        _NON_ATTRS_CFG: ['index', 'isActive']
    });

    Y.namespace('app.person').UpdatePersonWarningAssignment = Y.Base.create('updatePersonWarningAssignment', Y.usp.person.UpdatePersonWarningAssignment, [], {
        initializer: function(config) {
            this.index = config.index;
        }
    }, {
        //in validation, the number is used to locate the row in error
        _NON_ATTRS_CFG: ['index', 'isActive']
    });

    Y.namespace('app.person').NewPersonWarning = Y.Base.create('newPersonWarning', Y.usp.person.NewPersonWarning, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#personAddEditWarningForm',
        customValidation: function(attrs) {
            var errors = {},
                warnings = [],
                today = new Date(),
                today = today.getTime(),
                labels = this.labels || {};

            var newWarnings = attrs.warningAssignmentList;

            var checkWarnings = function(warning) {
                errors = checkWarning(errors, warning, labels, today, warnings.length);
                warnings.push(warning);
            };

            newWarnings.forEach(checkWarnings);

            return errors;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app.person').UpdatePersonWarning = Y.Base.create('updatePersonWarning', Y.usp.person.UpdatePersonWarning, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#personAddEditWarningForm',
        customValidation: function(attrs) {
            var errors = {},
                warnings = [],
                today = new Date(),
                today = today.getTime(),
                labels = this.labels || {};

            var updatedWarnings = attrs.updateWarningAssignments;
            var newWarnings = attrs.newWarningAssignments;

            var checkWarnings = function(warning) {
                errors = checkWarning(errors, warning, labels, today, warnings.length);
                warnings.push(warning);
            };

            newWarnings.forEach(checkWarnings);
            updatedWarnings.forEach(checkWarnings);

            return errors;
        }
    }, {
        ATTRS: {}
    });

    //Base functionality common to add/edit person
    function BasePersonWarning() {}
    BasePersonWarning.prototype = {
        destructor: function() {
            this.deconstructNoteTextAreas();
            this.deconstructCounters();
            this.deconstructCalendars();
        },
        deconstructNoteTextAreas: function() {
            if (this.noteTextAreas) {
                A.each(this.noteTextAreas, function(noteTextArea) {
                    noteTextArea.unplug();
                    noteTextArea.destroy();
                    delete noteTextArea;
                }, this);
            }
        },
        deconstructCounters: function() {
            if (this.charCounts) {
                A.each(this.charCounts, function(charCount) {
                    charCount.destroy();
                    delete charCount;
                });
            }
        },
        deconstructCalendars: function() {
            this.calendars.forEach(function(calendar) {
                this.deconstructCalendar(calendar.startDate);
                this.deconstructCalendar(calendar.endDate);
            }.bind(this));
        },
        deconstructCalendar: function(calendar) {
            calendar.destroy();
            delete calendar;
        },
        render: function() {
            //call superclass render
            Y.app.person.AddEditPersonWarning.superclass.render.call(this);

            //enable all form field functionality
            this.initFormFields();
        },
        initFormFields: function() {
            var container = this.get("container"),
                model = this.get('model'),
                personWarningAssignments = model.get('personWarningAssignments') || [];

            A.each(personWarningAssignments, function(item, index) {
                this.initNoteTextArea(index, container);
                this.initCategoryPicklist(index, container, item.isActive);
                if (item.isActive) {
                    this.initWarningDates(index, container);
                } else {
                    this.disableEndedWarnings(item.id, container);
                }
            }, this);
        },
        disableEndedWarnings: function(id, container) {
            var picklist = this.getElementById(container, id, 'select');
            var textarea = this.getElementById(container, id, 'textarea');
            var inputFields = this.getElementById(container, id, 'input');
            picklist.setAttribute('disabled', 'disabled');
            textarea.setAttribute('disabled', 'disabled');
            inputFields.setAttribute('disabled', 'disabled');
        },
        handleAddWarning: function(e) {
            var model = this.get('model');
            e.preventDefault();
            //update model with existing warnings - then add a new warning
            var personWarnings = model.get('personWarningAssignments') || [];
            personWarnings.unshift(this.get('blankWarning'));

            //model change will trigger re-render
            model.setAttrs({
                'personWarningAssignments': personWarnings
            });

            this.align();
        },
        handleRemoveWarning: function(e) {
            var target = e.currentTarget,
                model = this.get('model');

            //get the index from the target
            var index = Number(target.getAttribute('data-index'));

            //update the model
            var personWarnings = model.get('personWarningAssignments') || [];
            personWarnings.splice(index, 1);
            if (personWarnings.length === 0) {
                personWarnings.push({});
            }
            personWarnings = personWarnings.map(function(warning, index) {
                return (Y.merge(warning, {
                    index: personWarnings.length - index,
                }));
            });
            //model change will trigger re-render
            model.setAttrs({
                'personWarningAssignments': personWarnings
            });

            this.align();
        },
        handleWarningCategoryChange: function(e) {
            this.handleInputChange(e, 'codedEntryId');
        },
        handleWarningNoteChange: function(e) {
            this.handleInputChange(e, 'warningNote');
        },
        handleInputChange: function(e, field) {
            var change = {},
                target = e.currentTarget,
                index = this.getIndex(target);
            change[field] = target.get('value');
            this.updateWarning(index, change);
        },
        handleStartDateChange: function(e) {
            this.handleDateChange(e, this.handleStartDateSelection);
        },
        handleEndDateChange: function(e) {
            this.handleDateChange(e, this.handleEndDateSelection);
        },
        handleDateChange: function(e, handleDateSelection) {
            var index = e.target.getData('index');
            var newVal = e.target.get('value');
            e.date = Y.USPDate.parseDate(newVal);
            handleDateSelection.call(this, e, index);
        },
        handleStartDateSelection: function(e, index) {
            var date = e.date;
            if (hasValue(date)) {
                var parsedDate = Y.USPDate.parseDate(date);
                if(parsedDate!==null){
                    date = appPersonUtil.SetToMidnight(parsedDate.getTime());
                }
            }
            this.updateWarning(index, {
                startDate: date
            });
            this.updateEndDateValue(index, e.date);
        },
        handleEndDateSelection: function(e, index) {
            var date = e.date;
            if (hasValue(date)) {
                var parsedDate = Y.USPDate.parseDate(date);
                if(parsedDate!==null){
                    date = appPersonUtil.SetToMidnight(parsedDate.getTime());
                    this.notifyUserAfterEndDateHasChanged(index);
                }
            }
            this.updateWarning(index, {
                endDate: date
            });
        },
        notifyUserAfterEndDateHasChanged: function(index) {
            var messageNode = this.getMessageNode(index),
                labels = this.get('labels');

            if (!messageNode.get('children').size()) {
                var dismissibleMessage = new Y.usp.DismissibleMessage({
                    message: labels.endDateWarningMessage || 'Once a warning has been saved with an End Date in the past, it will be locked and can no longer be edited.',
                    type: 'warning'
                }).render(messageNode);

                Y.later(6000, this, function() {
                    dismissibleMessage.hide();
                });
            }
        },
        updateEndDateValue: function(index, startValue) {
            var endDate = this.getElement(this.get('container'), index, "input", "endDate");
            if (endDate.get('value') === "") return;
            var endValue = new Date(endDate.get('value'));
            if (startValue > endValue) {
                endDate.set('value', null);
                this.updateWarning(index, {
                    endDate: null
                });
            }
        },
        updateWarning: function(index, attrs) {
            var model = this.get('model');

            var personWarningAssignments = model.get('personWarningAssignments') || [];
            personWarningAssignments[index] = Y.merge(personWarningAssignments[index] || {}, attrs || {});

            this.set('personWarningAssignments', personWarningAssignments);
        },
        align: function() {
            //fire align event so dialog re-calculates position after render
            Y.later(50, this, function() {
                this.fire('align')
            });
        },
        initCategoryPicklist: function(index, container, isActive) {
            var keyValues = Y.secured.uspCategory.person.warnings.category,
                options = (isActive)? O.values(keyValues.getActiveCodedEntries(null, 'write')) : O.values(keyValues.getActiveCodedEntries(null, 'read_summary'));
                select = this.getElement(container, index, "select", "codedEntryId");

            if (select) {
                //set the select options for outputCategory
                Y.FUtil.setSelectOptions(select, options, null, null, true, true, 'id');

                //ensure the currently select value is set
                select.set('value', select.getAttribute('data-value'));

                this.disableWarningTypesPendingAuthorisation(select);
            }
        },
        disableWarningTypesPendingAuthorisation: function(select) {
            var model = this.get('model'),
                modelList = this.get('modelList'),
                labels = this.get('labels'),
                personWarningAssignments = modelList.toJSON() || [];

            var pendingWarningAssignments = personWarningAssignments.filter(function(pwa) {
                return pwa.authorisation && pwa.authorisation.authorisationState === 'PENDING'
            });
            if (pendingWarningAssignments) {
                var options = select.get('options');
                for (var i = 0; i < options.size(); i++) {
                    var option = options.item(i);
                    if (pendingWarningAssignments.find(function(pwa) {
                            return pwa.codedEntryVO.id.toString() === option.get('value')
                        })) {
                        option.set('disabled', 'true');
                        option.set('title', labels.disabledWarningTypeTitle);
                    }
                }
            }
        },
        initNoteTextArea: function(index, container) {
            var noteTextArea = this.getElement(container, index, "textarea", "warningNote");
            var charCount = this.getElement(container, index, "#charactersRemaining");
            var count = noteTextArea.get('value').length;

            //plug in counter
            noteTextArea.plug(Y.Plugin.Counter);

            //set up initial count
            charCount.set('text', Math.max(0, (1500 - count)));

            //event listener
            noteTextArea.counter.on('charCountChange', function(e) {
                var length = e.newVal;
                if (Y.UA.chrome || Y.UA.webkit) {
                    // chrome and ipad(safari) - text area is treating 'enter key'(carriage return) as 2 chars
                    //and disabling the user input after maxlength limit
                    // but the length of text area is calculating 1 char for 'enter key'(carriage return)
                    // so the remaning chars is showing some number but the text area is not allowing chars
                    length = noteTextArea.get('value').replace(/\r(?!\n)|\n(?!\r)/g, "\r\n").length;
                } else if (Y.UA.gecko) {
                    // for firefox e.newVal is different from text area length
                    // so using the length of the text area
                    length = noteTextArea.get('value').length;
                }
                charCount.set('text', Math.max(0, (1500 - length)));
            }, this);

            this.noteTextAreas.push(noteTextArea);
            this.charCounts.push(charCount);
        },
        initWarningDates: function(index, container) {
            this.initStartDate(index, container);
            this.initEndDate(index, container);
        },
        initStartDate: function(index, container) {
            this.initDate({
                inputNode: this.getElement(container, index, "input", "startDate"),
                maximumDate: new Date()
            }, index, 'startDate', this.handleStartDateSelection);
        },
        initEndDate: function(index, container) {
            this.initDate({
                inputNode: this.getElement(container, index, "input", "endDate"),
                minimumDateFieldDependency: 'input[name="startDate"][data-index="' + index + '"]'
            }, index, 'endDate', this.handleEndDateSelection);
        },
        initDate: function(setup, index, dateLabel, handler) {
            var calendar = new Y.usp.CalendarPopup(setup).render();
            calendar.after('dateSelection', Y.bind(function(e) {
                handler.call(this, e, index)
            }, this));
            this.calendars[index] = this.calendars[index] || {};
            this.calendars[index][dateLabel] = calendar;
        },
        getIndex: function(target) {
            //get the index from the target
            return Number(target.getAttribute('data-index'));
        },
        getElement: function(container, index, selector, identifier) {
            var name = identifier !== undefined ? '[name="' + identifier + '"]' : "";
            return container.one(selector + name + '[data-index="' + index + '"]');
        },
        getElementById: function(container, id, selector) {
            return container.all(selector + '[data-id="' + id + '"]');
        },
        getMessageNode: function(index) {
            var warningPanel = this.get('container').one('#warningPanel_' + index),
                messageNode = warningPanel.one(".endDateNotification"),
                contentNode;

            if (!messageNode) {
                messageNode = this.getNewMessageNode();
                contentNode = this.getNewContentNode(messageNode);
                warningPanel.append(contentNode);
            }

            return messageNode;
        },
        getNewMessageNode: function() {
            return Y.Node.create('<div class="endDateNotification"></div>');
        },
        getNewContentNode: function(messageNode) {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(messageNode);
            return contentNode;
        }
    };

    // Extend the USP PersonWarningView
    Y.namespace('app.person').ViewPersonWarning = Y.Base.create('ViewPersonWarning', Y.usp.person.PersonWarningView, [BasePersonWarning], {
        //bind template
        template: Y.Handlebars.templates["personWarningViewDialog"],
        initializer: function() {
            this.get('modelList').after('*:load', function() {
                var model = this.get('model'),
                    modelList = this.get('modelList'),
                    personWarningAssignments = modelList.toJSON() || [],
                    hasPersonWarningAssignments = personWarningAssignments.length !== 0;

                if (hasPersonWarningAssignments) {
                    personWarningAssignments = personWarningAssignments.filter(function(pwa) {
                        return (!pwa.authorisation || (pwa.authorisation.authorisationState !== 'PENDING' && pwa.authorisation.authorisationState !== 'REJECTED'))
                    });

                    personWarningAssignments = personWarningAssignments.map(function(warning, index) {
                        return ({
                            id: warning.id,
                            index: personWarningAssignments.length - index,
                            isActive: false,
                            warningNote: (hasValue(warning.warningNote)) ? warning.warningNote.content : warning.warningNote,
                            startDate: warning.startDate,
                            endDate: warning.endDate,
                            codedEntryId: warning.codedEntryVO.id
                        });
                    });
                }

                model.setAttrs({
                    'personWarningAssignments': personWarningAssignments,
                    'hasPersonWarningAssignments': hasPersonWarningAssignments
                });
            }, this);

            this.get('modelList').load();

            this.noteTextAreas = [];
            this.charCounts = [];
            this.calendars = [];
        }
    });

    // Extend the USP PersonWarningView
    Y.namespace('app.person').AddEditPersonWarning = Y.Base.create('AddEditPersonWarning', Y.usp.person.PersonWarningView, [BasePersonWarning], {
        //bind template personWarningAddEditDialog
        template: Y.Handlebars.templates["personWarningAddEditDialog"],
        events: {
            '.add': {
                click: 'handleAddWarning'
            },
            '.remove': {
                click: 'handleRemoveWarning'
            },
            'select[name=codedEntryId]': {
                change: 'handleWarningCategoryChange'
            },
            'textarea[name=warningNote]': {
                valuechange: 'handleWarningNoteChange'
            },
            'input[name="startDate"]': {
                valuechange: 'handleStartDateChange'
            },
            'input[name="endDate"]': {
                valuechange: 'handleEndDateChange'
            }
        },
        initializer: function() {
            this.get('modelList').after('*:load', function() {
                var model = this.get('model'),
                    modelList = this.get('modelList'),
                    personWarningAssignments = modelList.toJSON() || [];

                if (personWarningAssignments.length !== 0) {

                    //capture the warning from the model - this is used when we update to find
                    //what has been deleted
                    this.set('previousWarnings', personWarningAssignments.map(function(warning) {
                        return warning;
                    }));

                    var todaysDate = (new Date()).getTime();

                    personWarningAssignments = personWarningAssignments.filter(
                        function(pwa) {
                            return (!pwa.authorisation || (pwa.authorisation.authorisationState !== 'PENDING' && pwa.authorisation.authorisationState !== 'REJECTED'))
                        });
                    //turn warnings into something we can use by flattening out the warningNote
                    personWarningAssignments = personWarningAssignments.map(function(warning, index) {
                        return ({
                            id: warning.id,
                            index: personWarningAssignments.length - index,
                            isActive: isActive(warning.endDate, todaysDate) && warning._securityMetaData.write,
                            warningNote: (hasValue(warning.warningNote)) ? warning.warningNote.content : warning.warningNote,
                            startDate: warning.startDate,
                            endDate: warning.endDate,
                            codedEntryId: warning.codedEntryVO.id,
                            objectVersion: warning.objectVersion,
                        });
                    });
                }

                //if empty push in a new entry
                if (personWarningAssignments.length === 0) {
                    personWarningAssignments.push(this.get('blankWarning'));
                }

                model.setAttrs({
                    'personWarningAssignments': personWarningAssignments
                });
            }, this);

            this.get('modelList').load();

            this.noteTextAreas = [];
            this.charCounts = [];
            this.calendars = [];
        }
    }, {
        ATTRS: {
            blankWarning: {
                getter: function() {
                    var personWarningAssignments = this.get('model').get('personWarningAssignments') || [];
                    return {
                        index: personWarningAssignments.length + 1,
                        isActive: true
                    };
                }
            }
        }
    });


    //Person Warning Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').PersonWarningDialog = Y.Base.create('personWarningDialog', Y.usp.MultiPanelPopUp, [], {
        initializer: function() {
            //plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        views: {
            viewPersonWarning: {
                type: Y.app.person.ViewPersonWarning
            },
            addEditPersonWarning: {
                type: Y.app.person.AddEditPersonWarning,
                buttons: addEditWarningButtons
            }
        }
    }, {
        CSS_PREFIX: 'yui3-panel',
        ATTRS: {
            width: {
                value: 700
            },
            buttons: {
                value: [{
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    classNames: 'pure-button-secondary',
                    action: function(e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER
                }, 'close']
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'multi-panel-popup',
        'multi-panel-model-errors-plugin',
        'model-form-link',
        'gallery-textarea-counter',
        'usp-person-PersonWarning',
        'usp-person-PersonWithWarning',
        'usp-person-NewPersonWarning',
        'usp-person-UpdatePersonWarning',
        'usp-person-NewPersonWarningAssignment',
        'usp-person-UpdatePersonWarningAssignment',
        'secured-categories-person-component-Warnings',
        'handlebars-helpers',
        'handlebars-person-templates',
        'dismissible-message',
        'app-utils-person',
        'model-transmogrify'
    ]
});