YUI.add('childprotection-dialog-views', function(Y) {
    var O = Y.Object,
        L = Y.Lang,
        DB = Y.usp.DataBindingUtil;

    Y.namespace('app.childprotection').NewChildProtectionEnquiryForm = Y.Base.create('newChildProtectionEnquiryForm',
        Y.usp.childprotection.NewChildProtectionEnquiry, [Y.usp.ModelFormLink], {
            form: '#childProtectionEnquiryAddForm',
            customValidation: function(attrs) {
                var errors = {},
                    labels = this.labels || {};

                if (attrs.enquiryRequestDate === '') {
                    errors.enquiryRequestDate = labels.enquiryRequestDateMandatory;
                }

                if (attrs.enquirerSubjectType === null || L.trim(attrs.enquirerSubjectId) === '' || !L.isNumber(Number(attrs.enquirerSubjectId))) {
                    errors['enquirerSubjectType'] = labels.enquirerSubjectMandatory;
                }

                if (attrs.enquiryReason === "") {
                    errors.enquiryReason = labels.enquiryReasonMandatory;
                }

                return errors;
            }
        });

    var BaseReceivingSubjectView = Y.Base.create('baseReceivingSubjectView', Y.View, [], {
        template: Y.Handlebars.templates.childProtectionAutoCompleteView,
        getAutoCompleteViewConstructor: function() {
            throw new TypeError('Please implement getAutoCompleteViewConstructor');
        },
        initializer: function(config) {
            this.autoCompleteView = new(this.getAutoCompleteViewConstructor())({
                autocompleteURL: config.autocompleteURL,
                inputName: 'enquirerSubjectId',
                formName: 'childProtectionEnquiryAddForm',
                rowTemplate: 'ROW_TEMPLATE_SIMPLE',
                labels: config.labels,
                preventSecuredSelection: config.preventSecuredSelection
            });

            this.autoCompleteView.addTarget(this);
        },
        render: function() {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //render the main template
            contentNode.setHTML(this.template());

            //add our fragments to the content node
            contentNode.one('#subjectSearchWrapper').append(this.autoCompleteView.render().get('container'));

            //set the fragment into the view container
            container.setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this.autoCompleteView.removeTarget(this);
            this.autoCompleteView.destroy();
            delete this.autoCompleteView;
        }
    }, {
        ATTRS: {
            autocompleteURL: {},
            preventSecuredSelection: {}
        }
    });

    var PersonAutocompleteView = Y.Base.create('personAutocompleteView', BaseReceivingSubjectView, [], {
        getAutoCompleteViewConstructor: function() {
            return Y.app.ProfessionalRolePersonAutoCompleteResultView;
        }
    });
    var OrganisationAutocompleteView = Y.Base.create('organisationAutocompleteView', BaseReceivingSubjectView, [], {
        getAutoCompleteViewConstructor: function() {
            return Y.app.OrganisationAutoCompleteResultView;
        }
    });

    Y.namespace('app.childprotection').AddChildProtectionEnquiry = Y.Base.create('addChildProtectionEnquiry', Y.View, [], {
        events: {
            'input[name="enquirerSubjectType"]': {
                change: '_handleSubjectTypeChange'
            },
            'input[name="enquirerSubjectId"]': {
                change: '_handleSubjectIdChange'
            },
            'select[name="enquirerTeamId"]': {
                change: '_afterEnquirerTeamIdChange'
            }
        },
        template: Y.Handlebars.templates.childProtectionAddEnquiryDetails,
        initializer: function(config) {
            var model = this.get('model');

            this.enquiryRequestDateCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar'
            });

            this._evtHandlers = [
               this.after('*:selectedPersonChange', this._afterEnquirerSubjectTypeId, this),
                model.after('enquirerSubjectTypeChange', this._afterEnquirerSubjectTypeChange, this),
                //re-render only after load - we don't want to blow away the rendered dialog at any other time
                model.after('load', function(e) {
                    this.render();
                }, this)
            ];
        },

        _afterEnquirerTeamIdChange: function() {
            var container = this.get('container'),
                selectedAddress,
                addressDescription,
                location;
            this._getEnquirerTeamAddress().then(function(teamAddresses) {
                    if (teamAddresses) {
                        selectedAddress = teamAddresses.toJSON().results.find(function(teamAddress) {
                            return teamAddress.type === "WORK" && teamAddress.usage === "PERMANENT" && teamAddress.endDate === null;
                        });
                    }
                    addressDescription = (null != selectedAddress) ? selectedAddress.location : null;
                    location = (null != addressDescription) ? addressDescription.location : null;

                    container.one('#enquirerTeamAddress').set('text', location);
                }.bind(this))
                .then(null, function(err) {
                    this.fire('error', {
                        error: err
                    });
                }.bind(this));
        },

        _getEnquirerTeamAddress: function() {
            var teamId = this.get('container').one('#enquirerTeamId').get('value');
            if (teamId && teamId.length>0) {
                var model = new Y.usp.organisation.OrganisationAddress({
                    url: L.sub(this.get('enquirerTeamAddressURL'), {
                        id: teamId
                    })
                });

                return new Y.Promise(function(resolve, reject) {
                    var data = model.load(function(err) {
                        if (err !== null) {
                            //failed, reject promise
                            reject(err);
                        } else {
                            // success, resolve promise
                            resolve(data);
                        }
                    });

                }.bind(this));
            }
        },

        _afterEnquirerSubjectTypeChange: function(e) {
            var _view;
            //destroy existing view
            if (this.selectionView) {
                //unbind from event bubble
                this.selectionView.removeTarget(this);
                //destroy the view
                this.selectionView.destroy();
            }

            //now create the new view
            _view = this._getNewSelectionView(e.newVal);
            if (_view) {
                //add event bubble target
                _view.addTarget(this);
            }
            this.selectionView = _view;

            //render into specific location on container
            this.renderSelectionView();
        },

        _getEnquirerTeam: function(id) {
            if (id) {
                var model = new Y.usp.relationship.PersonOrganisationRelationship({
                    url: L.sub(this.get('enquirerTeamListURL'), {
                        id: id
                    })
                });

                return new Y.Promise(function(resolve, reject) {
                    var data = model.load(function(err) {
                        if (err !== null) {
                            //failed, reject promise
                            reject(err);
                        } else {
                            // success, resolve promise
                            resolve(data);
                        }
                    });
                });
            }
        },

        _clearEnquirerTeamDetails: function() {
            var container = this.get('container');
            container.one('#enquirerTeamId').setHTML('');
            container.one('#enquirerTeamAddress').setHTML('');
        },

        _hideEnquirerTeamDetails: function() {
            var container = this.get('container');
            container.one('#enquirerTeamWrapper').hide();
            container.one('#enquirerTeamAddressWrapper').hide();
        },

        _showEnquirerTeamDetails: function() {
            var container = this.get('container');
            container.one('#enquirerTeamWrapper').show();
            container.one('#enquirerTeamAddressWrapper').show();
        },
        _afterEnquirerSubjectTypeId: function(e) {
            var instance = this,
                container = this.get('container');

            this._clearEnquirerTeamDetails();

            if (!(null === e.newVal || undefined === e.newVal || '' === e.newVal)) {
                this._getEnquirerTeam(e.newVal.id).then(function(teamList) {
                    var select = container.one('#enquirerTeamId'),
                        enquirerAddress = (e.newVal.address !== null) ? e.newVal.address.location.location : null,
                        data = teamList.toJSON(),
                        teams;

                    if (data.totalSize > 0) {
                        teams = data.results.map(function(team) {
                            return ({
                                id: team['organisationVO'].id,
                                name: team['organisationVO'].name
                            });
                        });
                    }

                    Y.FUtil.setSelectOptions(select, teams, null, null, false, true);
                    
                    // if enquirer person has no team default to enquirer's address
                    if(!teams || teams.length===0) {
                        container.one('#enquirerTeamAddress').set('text', enquirerAddress);
                    }
                }.bind(this))
                .then(null, function(err) {
                    this.fire('error', {
                        error: err
                    });
                }.bind(this));
            }

        },

        _getNewSelectionView: function(enquirerSubjectType) {
            var _view,
                labels = this.get('labels') || {};

            switch (enquirerSubjectType) {
                case 'PERSON':
                    //show person view
                    _view = new PersonAutocompleteView({
                        autocompleteURL: this.get('personAutocompleteURL'),
                        labels: labels.person || {},
                        preventSecuredSelection:false,
                    });
                    this._showEnquirerTeamDetails();
                    
                    break;
                case 'ORGANISATION':
                    // clear person enquirer team details
                    this._clearEnquirerTeamDetails();
                    this._hideEnquirerTeamDetails();
                    //show organisation view
                    _view = new OrganisationAutocompleteView({
                        autocompleteURL: this.get('organisationAutocompleteURL'),
                        labels: labels.organisation || {}
                    });
                    break;
            }
            return _view;
        },
        render: function() {
            var container = this.get('container');

            //render the main template
            container.setHTML(this.template({
                modelData: this.get('model').toJSON() || {},
                otherData: this.get('otherData'),
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries') || {}
            }));

            this.enquiryRequestDateCalendar.set('inputNode', container.one('input[name="enquiryRequestDate"]'));
            this.enquiryRequestDateCalendar.render();

            Y.FUtil.setSelectOptions(container.one('select[name="enquiryReason"]'), O.values(Y.uspCategory.childprotection.enquiryReason.category.getActiveCodedEntries()), null, null, true, true, 'code');

            return this;
        },
        _handleSubjectTypeChange: function(e) {
            var target = e.currentTarget,
                name = target.get('name');
            this.get('model').set(name, DB.getElementValue(target._node));
        },
        _handleSubjectIdChange: function(e) {
            var target = e.currentTarget,
                name = target.get('name');
            this.get('model').set(name, DB.getElementValue(target._node));
        },
        renderSelectionView: function() {
            var container = this.get('container');
            if (this.selectionView) {
                //add our fragments to the content node
                container.one('#enquirerSubjectContent').setHTML(this.selectionView.render().get('container'));
            }
        },
        destructor: function() {
            this._evtHandlers.forEach(function(handle) {
                handle.detach();
            });

            delete this._evtHandlers;

            this.enquiryRequestDateCalendar.destroy();
            delete this.enquiryRequestDateCalendar;

            if (this.selectionView) {
                this.selectionView.removeTarget(this);
                this.selectionView.destroy();

                delete this.selectionView;
            }
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    enquiryReason: Y.uspCategory.childprotection.enquiryReason.category.codedEntries
                }
            },
            personAutocompleteURL: {
                value: ''
            },
            organisationAutocompleteURL: {
                value: ''
            }
        }
    });

    Y.namespace('app.childprotection').ViewChildProtectionEnquiry = Y.Base.create('viewChildProtectionEnquiry', Y.usp.childprotection.ChildProtectionEnquiryWithEnquirerSubjectView, [Y.usp.ModelFormLink], {
        template: Y.Handlebars.templates.childProtectionViewEnquiryDetails
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    enquiryReason: Y.uspCategory.childprotection.enquiryReason.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.childprotection').DeleteChildProtectionEnquiryView = Y.Base.create('deleteChildProtectionEnquiryView', Y.usp.View, [], {
        template: Y.Handlebars.templates.childProtectionDeleteEnquiryDialog
    });

    Y.namespace('app.childprotection').ChildProtectionEnquiryDialog = Y.Base.create('childProtectionEnquiryDialog', Y.usp.app.AppDialog, [], {
        views: {
            addChildProtectionEnquiry: {
                type: Y.app.childprotection.AddChildProtectionEnquiry,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            viewChildProtectionEnquiry: {
                type: Y.app.childprotection.ViewChildProtectionEnquiry
            },
            deleteChildProtectionEnquiry: {
                type: Y.app.childprotection.DeleteChildProtectionEnquiryView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            }
        }
    }, {
        ATTRS: {}
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'event-custom',
        'view',
        'usp-view',
        'form-util',
        'app-dialog',
        'app-multi-panel',
        'model-form-link',
        'usp-childprotection-NewChildProtectionEnquiry',
        'usp-organisation-OrganisationAddress',
        'usp-relationship-PersonOrganisationRelationship',
        'usp-childprotection-ChildProtectionEnquiryWithEnquirerSubject',
        'categories-childprotection-component-EnquiryReason',
        'handlebars',
        'handlebars-helpers',
        'handlebars-childProtection-templates',
        'calendar-popup',
        'organisation-autocomplete-view',
        'person-autocomplete-view',
        'promise',
        'template-micro'
    ]
});