YUI.add('location-dialog', function (Y) {
    var O = Y.Object,
        F = Y.FUtil;

    Y.namespace('app.location').NewLocationForm = Y.Base.create("newLocationForm", Y.usp.address.NewLocation, [Y.usp.ModelFormLink], {
        form: '#addLocationForm'
    });

    Y.namespace('app.location').NewLocationView = Y.Base.create('newLocationView', Y.usp.View, [], {
        events: {
            '#country-input': {
                change: '_handleSetSelectOptionsForCountrySubDivision'
            }
        },
        template: Y.Handlebars.templates.addLocationDialog,
        render: function () {
            Y.app.location.NewLocationView.superclass.render.call(this);
            var container = this.get('container');

            F.setSelectOptions(container.one('#country-input'), O.values(Y.uspCategory.address.countryCode.category.getActiveCodedEntries()), null, null, true, true, 'code');
            F.setSelectOptions(container.one('#county-input'), O.values(Y.uspCategory.address.countyCode.category.getActiveCodedEntries()), null, null, true, true, 'code');

            return this;
        },
        _handleSetSelectOptionsForCountrySubDivision: function () {
            var container = this.get('container'),
                countryCode = container.one('#country-input').get('value');

            if (countryCode) {
                F.setSelectOptions(container.one('#subdivision-input'), O.values(Y.uspCategory.address.countrySubdivisionCode.category.getActiveCodedEntriesForParent(countryCode)), null, null, true, true, 'code');
            }
        }
    });

    Y.namespace('app.location').LocationDialog = Y.Base.create('locationDialog', Y.usp.app.AppDialog, [], {
        cancelView: function (e) {
            e.preventDefault();
            
            // If you provided an eventRoot value return it so you can perform more actions.
            // e.g. Show dialog where the view was originally fired from and hidden.
            var eventRoot = this.get('activeView').get('otherData').eventRoot;
            return Y.when(this.handleCancel(e)).then(function () {
                this.fire('cancel:close:locationView', {
                    eventRoot: eventRoot
                });
            }.bind(this));
        },
        saveLocation: function (e) {
            e.preventDefault();

            // If you provided an eventRoot value return it so you can perform more actions. 
            // e.g. Show dialog where the view was originally fired from 
            // and use results to populate a field.
            var eventRoot = this.get('activeView').get('otherData').eventRoot;
            return Y.when(this.handleSave(e)).then(function (result) {
                this.fire('success:close:locationView', {
                    locationId: result.get('id'),
                    eventRoot: eventRoot
                });
            }.bind(this));
        },
        views: {
            add: {
                type: Y.app.location.NewLocationView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save and continue',
                    action: 'saveLocation',
                    disabled: false
                }, {
                    name: 'cancel',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    action: 'cancelView',
                    classNames: 'pure-button-secondary'
                }, {
                    name: 'close',
                    action: 'cancelView',
                    section: Y.WidgetStdMod.HEADER,
                    template: '<button type="button" />',
                    classNames: 'yui3-button yui3-button-close'
                }]
            }
        }
    });
}, '0.0.1', {
        requires: [
            'yui-base',
            'form-util',
            'usp-view',
            'model-form-link',
            'usp-address-NewLocation',
            'app-dialog',
            'handlebars',
            'handlebars-location-templates',
            'categories-address-component-CountyCode',
            'categories-address-component-CountryCode',
            'categories-address-component-EndReason',
            'categories-address-component-CountrySubdivisionCode',
            'usp-configuration-CodedEntry'
        ]
    });