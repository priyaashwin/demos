YUI.add('address-dialog-views', function (Y) {

    var addressFormatters = Y.app.FormatAddressLocation;

    var HEADER_HTML = '<h3 id="labelledby" aria-describedby="describedby" class="iconic-title">\
		<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-building-o fa-inverse fa-stack-1x"></i></span> {title}\
		</h3>';

    var EVENT_INFO_MESSAGE = 'infoMessage:message';
    var EVENT_REFRESH_RESULTS = 'address:refreshResults';

    var cancelDialog = function (e) {
        e.preventDefault();
        this.hide();
    };

    var _clearErrorBorderAroundStartDate = function ($errorBannerCloseButton, $fuzzyDateWidgetWrapper) {
        $errorBannerCloseButton.on('click', function (e) {
            $fuzzyDateWidgetWrapper.removeClass('error');
        });
    };

    var checkboxFormatter = function (e) {
        var html = '';
        if (e.record.get('relationshipName') === "No relation") {
            html = '<div><input type="checkbox" name="residentCheckbox"/></div>';
        } else {
            html = '<div><input type="checkbox" name="residentCheckbox" checked/></div>';
        }
        return html;
    };

    var addAddressSubmit = function (e) {

        var _instance = this;
        var view = this.get('activeView');
        var model = view.get('model');
        var labels = view.get('labels');
        var $startDateValidation, $errorBannerCloseButton;

        e.preventDefault();

        if (!view.fuzzyStartDate.isValid()) {

            var e = {};

            e.code = 400;
            e.msg = {
                validationErrors: {
                    startDate: labels.msgDateValidationError
                }
            };

            view.fire('error', { error: e });

            $startDateValidation = _instance.bodyNode.one('.message-error a[href="#startDate"]');
            $errorBannerCloseButton = _instance.bodyNode.one('.message-error a.close');

            if ($startDateValidation) {
                view.getWrapper('fuzzyStartDate').addClass('error');
                view.getWrapper('fuzzyStartDate').removeClass('focus');
                $startDateValidation.on('click', function (e) {
                    view.getWrapper('fuzzyStartDate').addClass('focus');
                    _instance.bodyNode.one('#fuzzyStartYEAR').focus();
                });
                _clearErrorBorderAroundStartDate($errorBannerCloseButton, view.getWrapper('fuzzyStartDate'));
            } else {
                view.getWrapper('fuzzyStartDate').removeClass('error');
            }

            return;

        }

        this.showDialogMask();

        model.setAttrs({
            organisationId: view.get('contextId'),
            locationId: view.get('locationId'),
            startDate: view.getStartDate()
        }, { silent: true });

        model.save(function (err, res) {

            var responseData = '',
                labels = view.get('labels') || {};

            _instance.hideDialogMask();

            if (err) {

                //manully add validation indictor
                responseData = Y.JSON.parse(res.responseText);
                if (responseData.validationErrors.locationId) {
                    view.getInput('postcodeSearch').addClass('error');
                    Y.one('a[href="#locationId"]').on('click', function (e) {
                        view.getInput('postcodeSearch').focus();
                    });
                }

                var $startDateValidation = _instance.bodyNode.one('.message-error a[href="#startDate"]');
                var $errorBannerCloseButton = _instance.bodyNode.one('.message-error a.close')
                if ($startDateValidation) {
                    view.getWrapper('fuzzyStartDate').addClass('error');
                    view.getWrapper('fuzzyStartDate').removeClass('focus');
                    $startDateValidation.on('click', function (e) {
                        view.getWrapper('fuzzyStartDate').addClass('focus');
                        _instance.bodyNode.one('#fuzzyStartYEAR').focus();
                    });
                    _clearErrorBorderAroundStartDate($errorBannerCloseButton, view.getWrapper('fuzzyStartDate'));
                } else {
                    view.getWrapper('fuzzyStartDate').removeClass('error');
                }

                if (err.code !== 400) {
                    Y.log(err.code + err.msg);
                }

            } else {
                _instance.hide();
                Y.fire(EVENT_REFRESH_RESULTS, {});
                Y.fire(EVENT_INFO_MESSAGE, {
                    message: labels.msgAddAddressOnSuccess
                });
            }
        });
    };

    var endAddressSubmit = function (e) {
        // Get the view and the model
        var view = this.get('activeView'),
            model = view.get('model'),
            labels = view.get('labels');
        var _instance = this,
            $errorBannerCloseButton;
        e.preventDefault();

        // checking if the fuzzy date is valid
        if (!view.fuzzyEndDate.isValid()) {
            var e = {};
            e.code = 400;
            e.msg = {
                validationErrors: { endDate: labels.dateValidationErrorMessage }
            };
            view.fire('error', { error: e });

            // add the link to the year field

            var $endDateValidation = _instance.bodyNode.one('.message-error a[href="#endDate"]');
            $errorBannerCloseButton = _instance.bodyNode.one('.message-error a.close');
            if ($endDateValidation) {
                view.getWrapper('fuzzyEndDate').addClass('error');
                view.getWrapper('fuzzyEndDate').removeClass('focus');
                $endDateValidation.on('click', function (e) {
                    view.getWrapper('fuzzyEndDate').addClass('focus');
                    _instance.bodyNode.one('#fuzzyEndYEAR').focus();
                });
                _clearErrorBorderAroundStartDate($errorBannerCloseButton, view.getWrapper('fuzzyEndDate'));
            } else {
                view.getWrapper('fuzzyEndDate').removeClass('error');
            }
            return;
        }

        // prevent duplicate save on double click 
        //show the mask
        this.showDialogMask();
        model.setAttrs({
            endDate: view.getEndDate()
        }, {
            //don't trigger the onchange listener as we will re-render after save
            silent: true
        });

        model.save({
            transmogrify: Y.usp.organisation.UpdateCloseOrganisationAddress
        }, function (err, response) {
            //hide the mask -	prevent duplicate save on double click 
            _instance.hideDialogMask();

            var $endDateValidation = _instance.bodyNode.one('.message-error a[href="#endDate"]');
            $errorBannerCloseButton = _instance.bodyNode.one('.message-error a.close');
            if ($endDateValidation) {
                view.getWrapper('fuzzyEndDate').addClass('error');
                view.getWrapper('fuzzyEndDate').removeClass('focus');
                $endDateValidation.on('click', function (e) {
                    view.getWrapper('fuzzyEndDate').addClass('focus');
                    _instance.bodyNode.one('#fuzzyEndYEAR').focus();
                });
                _clearErrorBorderAroundStartDate($errorBannerCloseButton, view.getWrapper('fuzzyEndDate'));
            } else {
                view.getWrapper('fuzzyEndDate').removeClass('error');
            }

            if (err === null) {
                //hide the dialog and tell the addresses table to update
                _instance.hide();
                Y.fire(EVENT_REFRESH_RESULTS, {});
                Y.fire(EVENT_INFO_MESSAGE, { message: labels.msgEndAddressOnSuccess });
            }
        });
    };

    var reopenAddressSubmit = function (e) {
        var view = this.get('activeView'),
            model = view.get('model'),
            labels = view.get('labels'),
            urls = view.get('urls'),
            _instance = this;

        e.preventDefault();
        this.showDialogMask();

        model.url = urls.reopenUrl;
        model.save({
            transmogrify: Y.usp.address.UpdateEndAddress
        }, function (err, response) {
            _instance.hideDialogMask();
            if (err === null) {
                _instance.hide();
                Y.fire(EVENT_REFRESH_RESULTS, {});
                Y.fire(EVENT_INFO_MESSAGE, { message: labels.reopenPersonAddressSuccessMessage });
            }
        });
    };

    var updateOrganisationAddressDatesSubmit = function (e) {
        var view = this.get('activeView'),
            model = view.get('model'),
            labels = view.get('labels'),
            urls = view.get('urls'),
            _instance = this;

        e.preventDefault();
        this.showDialogMask();

        model.setAttrs({
            startDate: view.getStartDate(),
            endDate: view.getEndDate()
        }, {
            silent: true
        });

        model.url = urls.updateStartEndUrl;
        model.save({
            transmogrify: Y.usp.organisation.UpdateOrganisationAddressStartEnd
        }, function (err, response) {
            _instance.hideDialogMask();
            if (err === null) {
                _instance.hide();
                Y.fire(EVENT_REFRESH_RESULTS, {});
                Y.fire(EVENT_INFO_MESSAGE, { message: labels.updateAddressDatesSuccessMessage });
            }
        });
    };

    var removeAddressSubmit = function (e) {
        var view = this.get('activeView'),
            model = view.get('model'),
            labels = view.get('labels');
        e.preventDefault();
        var _instance = this;
        model.destroy({ remove: true },
            function (err, response) {
                _instance.hideDialogMask();
                if (err === null) {
                    _instance.hide();
                    Y.fire(EVENT_REFRESH_RESULTS, {});
                    Y.fire(EVENT_INFO_MESSAGE, { message: labels.msgRemoveAddressOnSuccess });
                }
            });
    };

    Y.namespace('app.address').UpdateAddressView = Y.Base.create('updateAddressView', Y.usp.View, [], {
        template: Y.Handlebars.templates['updateAddressDialog'],
        events: {
            '#location_toggleAddLocation': {
                click: 'showLocationAddDialog'
            }
        },
        initializer: function () {
            var model = this.get('model'),
                locationConfig = this.get('locationConfig');

            this.addressAddView = new Y.app.address.AddressAddView({
                labels: this.get('labels'),
                locationUrl: this.get('urls').locationUrl,
                locationQueryParamsUrl: this.get('urls').locationQueryParamsUrl
            }).addTarget(this);

            this.locationDialog = new Y.app.location.LocationDialog(locationConfig).addTarget(this).render();

            this._evtHandlers = [
                this.after('*:close:locationView', this.handleAfterLocationAdd, this)
            ];

        },
        render: function () {
            var container = this.get('container'),
                template = this.template,
                model = this.get('model'),
                labels = this.get('labels'),
                description = this.get('targetSubjectName') + ' (' + this.get('targetSubjectPrefix') + this.get('targetSubjectId') + ')',
                summary = labels.updateAddressLocationNarrative;

            container.setHTML(template({
                labels: labels,
                otherData: this.get('otherData'),
                modelData: model.toJSON(),
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                targetSubjectId: this.get('targetSubjectId'),
                targetSubjectName: this.get('targetSubjectName'),
                codedEntries: this.get('codedEntries') || {}
            }));

            container.one('#addAddressWrapper').append(this.addressAddView.render().get('container'));

            //set initial narrative
            this.getNarrative('summary').set('text', summary);
            this.getNarrative('description').set('text', description);

            return this;
        },
        handleSetNewLocation: function (locationId) {
            this.addressAddView.handleSetNewLocation(locationId);
        },
        handleAfterLocationAdd: function (e) {
            var locationId = e.locationId,
                panel = e.eventRoot;

            // Show dialog where location dialog was launched
            panel.show();
            if (locationId) {
                panel.get('activeView').handleSetNewLocation(locationId);
            }
        },
        showLocationAddDialog: function (e) {
            e.preventDefault();
            var panel = this.get('panel');
            panel.hide();

            var url = this.get('urls').locationUrl;
            this.locationDialog.showView('add', {
                model: new Y.app.location.NewLocationForm({
                    url: url
                }),
                otherData: {
                    eventRoot: panel
                }
            }, function () {
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                this.removeButton('closeButton', Y.WidgetStdMod.HEADER);
            });
        },
        setupFieldVisibility: function () {
            //get the field visibility
            var fieldVisibility = this.getFieldVisibility();
            this.setAddressInputFieldsVisibility(fieldVisibility.addressFields);

            return fieldVisibility;
        },
        getFieldVisibility: function () {
            var fieldVisibility = {
                addressFields: false
            };
            return fieldVisibility;
        },
        setAddressInputFieldsVisibility: function (visible) {
            this.setFieldVisibility('#addAddressWrapper', null, visible);
        },
        setFieldVisibility: function (wrapper, field, visible) {
            var container = this.get('container');
            var wrapperNode = wrapper ? container.one(wrapper) : null;

            if (field) {
                var fieldVis = function (fld) {
                    var fieldNodes = fld ? container.all(fld) : null;
                    if (fieldNodes) {
                        if (visible) {
                            fieldNodes.removeAttribute('disabled');
                        } else {
                            fieldNodes.setAttribute('disabled', 'disabled');
                        }
                    }
                };
                if (Array.isArray(field)) {
                    field.forEach(fieldVis);
                } else {
                    fieldVis(field);
                }
            }

            if (wrapperNode) {
                if (visible) {
                    wrapperNode.setStyle('display', '');
                } else {
                    wrapperNode.setStyle('display', 'none');
                }
            }
        },
        getInput: function (id) {
            return this.get('container').one('#' + id);
        },
        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        }
    }, {

        ATTRS: {
            targetSubjectId: {},
            targetSubjectName: {},
            targetSubjectPrefix: {},
            codedEntries: {
                value: {
                    unknownLocations: Y.uspCategory.address.unknownLocation.category.codedEntries,
                    endReason: Y.uspCategory.address.endReason.category.codedEntries,
                    usage: Y.uspCategory.address.usage.category.codedEntries,
                    type: Y.uspCategory.address.type.category.codedEntries
                }
            },
            messageAddLocationSuccess: {},
            templateAddLocationMsg: {
                value: '<div class="pure-alert pure-alert-block pure-alert-{msgType}"><a href="#" class="close">Close</a><h4 tabindex="0">{msgAddLocationSuccess}</h4></div>'
            },
            panel: {
                value: ''
            }
        }
    });

    Y.namespace('app.address').ResidentPersonAddressResults = Y.Base.create('residentPersonAddressResults', Y.usp.app.Results, [], {
        resultsType: Y.usp.ResultsTable,
        events: {
            '.pure-table-data tr input[name="residentCheckbox"]': {
                click: '_rowClick'
            }
        },
        getResultsListModel: function () {
            var residentAddressesToUpdate = [];
            var residentAddresses = this.get('residentRecords') || [];
            var transformArr = residentAddresses.map(function (record) {
                var transform = {
                    'id': record.personId,
                    'objectVersion': record.objectVersion,
                    'personName': record.personName,
                    'relationshipName': record.relationshipName,
                    'addresses': record.addresses.map(function (address) {
                        return {
                            id: address.id,
                            objectVersion: address.objectVersion
                        }
                    })
                };
                return transform;
            }, this);

            transformArr.forEach(function (record) {
                if (record.relationshipName !== 'No relation') {
                    residentAddressesToUpdate.push(record);
                }
            }, this);

            this.set('residentAddressesToUpdate', residentAddressesToUpdate);

            return transformArr;
        },
        getColumnConfiguration: function () {
            var labels = this.get('labels');
            return ([{
                key: 'personName',
                label: labels.personName,
                width: '11%'
            }, {
                key: 'relationshipName',
                label: labels.relationshipName,
                width: '25%'
            }, {
                label: labels.update,
                className: 'pure-checkbox',
                allowHTML: true,
                width: '9%',
                formatter: checkboxFormatter
            }]);
        },
        destructor: function () {

        },
        _rowClick: function (e) {
            var target = e.currentTarget,
                results = this.get('results'),
                record = results.getRecord(target.get('id')),
                residentAddressesToUpdate = this.get('residentAddressesToUpdate');

            if (target.get('checked')) {
                residentAddress = {
                    'id': record.get('id'),
                    'addresses': record.get('addresses').map(function (address) {
                        return {
                            id: address.id,
                            objectVersion: address.objectVersion
                        }
                    })
                };
                residentAddressesToUpdate.push(residentAddress);
                this.set('residentAddressesToUpdate', residentAddressesToUpdate);
            } else {
                var removedItemArr = residentAddressesToUpdate.filter(function (address) {
                    return address.id !== record.get('id');
                });
                this.set('residentAddressesToUpdate', removedItemArr);
            }
        }
    }, {
        ATTRS: {
            initialLoad: {
                value: false
            },
            residentAddressesToUpdate: {
                value: []
            }
        }
    });

    Y.namespace('app.address').UpdateResidentAddressLocationView = Y.Base.create('updateResidentAddressLocationView', Y.View, [], {

        template: Y.Handlebars.templates["personAddressResidentsCloseAdd"],

        initializer: function (config) {
            this.residentPersonAddressResults = new Y.app.address.ResidentPersonAddressResults({
                residentRecords: config.residentRecords,
                labels: config.labels
            }).addTarget(this);

            this._evtHandlers = [
                this.residentPersonAddressResults.after('*:residentAddressesToUpdateChange', this._handleSaveButton, this)
            ]
        },
        destructor: function () {
            if (this.residentPersonAddressResults) {
                this.residentPersonAddressResults.destroy();
                delete this.residentPersonAddressResults;
            }
        },
        _handleSaveButton: function (e) {
            if (e.newVal && e.newVal.length > 0) {
                this.get('dialog').getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            } else {
                this.get('dialog').getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', true);
            }
        },
        render: function () {
            var labels = this.get('labels'),
                html = this.template({
                    narrativeSummaryResidents: labels.narrativeSummaryResidents
                });

            this.get('container').setHTML(html);
            this.get('container').append(this.residentPersonAddressResults.render().get('container'));
            return this;
        },
    }, {
        ATTRS: {

        }
    });

    Y.namespace('app.address').MultiPanelPopUp = Y.Base.create('addressMultiPanelPopUp', Y.usp.MultiPanelPopUp, [], {
        initializer: function () {
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        destructor: function () {
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        handleUpdateAddressLocation: function (e) {
            this.showDialogMask();
            e.preventDefault();

            var _instance = this,
                view = this.get('activeView'),
                model = view.get('model'),
                transmogrify = view.get('transmogrify'),
                residentModel = view.get('residentModel'),
                subjectId = view.get('targetSubjectId'),
                container = view.get('container'),
                roomDescription = container.one('#roomDescription-input').get('value'),
                floorDescription = container.one('#floorDescription-input').get('value'),
                newLocationId = view.addressAddView.get('selectedLocation'),
                addressId = view.get('addressId'),
                labels = view.get('labels');

            model.setAttrs({
                roomDescription: roomDescription,
                floorDescription: floorDescription,
                locationId: newLocationId
            }, {
                silent: true
            });

            model.url = view.get('urls').updateAddressUrl;
            model.save({
                transmogrify: transmogrify
            }, function (err) {
                _instance.hideDialogMask();
                if (!err) {
                    residentModel.load(function (err, res) {
                        if (!err) {
                            Y.fire('organisation:addressChanged');
                            Y.fire('infoMessage:message', { message: labels.msgUpdateAddressOnSuccess });
                            _instance.hide();

                            var responseText = Y.JSON.parse(res.responseText);
                            if (responseText.totalSize > 0) {
                                var records = responseText.results;
                                Y.fire('address:showDialog', {
                                    action: 'moveResidentPersonAddressLocation',
                                    newLocationId: newLocationId,
                                    addressId: addressId,
                                    residentRecords: records
                                });
                            }
                        }
                    });
                }
            });
        },
        handleUpdateResidentAddressLocation: function (e) {
            this.showDialogMask();
            e.preventDefault();
            var _instance = this,
                view = this.get('activeView'),
                model = view.get('model'),
                newLocationId = view.get('newLocationId'),
                residentRecords = view.residentPersonAddressResults.get('residentAddressesToUpdate');

            var personAddresses = [];
            residentRecords.forEach(function (record) {
                record.addresses.forEach(function (address) {
                    personAddresses.push(address);
                })
            });

            var updateAddressVOs = personAddresses.map(function (address) {
                return {
                    "id": address.id,
                    "objectVersion": address.objectVersion,
                    "locationId": newLocationId,
                    "_type": "UpdatePersonAddress"
                }
            })

            model.setAttrs({
                'id': view.get('addressId'),
                'updatePersonAddressList': updateAddressVOs
            }, {
                silent: true
            });

            model.save({}, function (err) {
                _instance.hideDialogMask();
                if (!err) {
                    _instance.hide();
                    Y.fire('infoMessage:message', { message: view.get('labels').residentsCloseAddSuccessMessage });
                }
            });
        },
        views: {
            addAddressForm: {
                type: Y.app.address.AddAddressForm,
                headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'Add address' }),
                buttons: [{
                    action: addAddressSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> save',
                    classNames: 'pure-button-primary'
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> cancel',
                    classNames: 'pure-button-secondary'
                }]
            },
            endAddressForm: {
                type: Y.app.address.EndAddressForm,
                headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'End address' }),
                buttons: [{
                    action: endAddressSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    classNames: 'pure-button-primary'
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> no',
                    classNames: 'pure-button-secondary'
                }]
            },
            removeAddressForm: {
                type: Y.app.address.RemoveAddressForm,
                headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'Remove address' }),
                buttons: [{
                    action: removeAddressSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    classNames: 'pure-button-primary'
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> no',
                    classNames: 'pure-button-secondary'
                }]
            },
            updateStartEndAddressDatesForm: {
                type: Y.app.address.UpdateStartEndDateAddressForm,
                headerTemplate: '<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span> Update address dates</h3>',
                buttons: [{
                    action: updateOrganisationAddressDatesSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    classNames: 'pure-button-primary',
                    disabled: false
                }, {
                    action: function (e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> no',
                    classNames: 'pure-button-secondary'
                }]
            },
            reopenAddressForm: {
                type: Y.app.address.ReopenAddressForm,
                headerTemplate: '<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span> Reopen address</h3>',
                buttons: [{
                    action: reopenAddressSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    classNames: 'pure-button-primary',
                    disabled: false
                }, {
                    action: function (e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> no',
                    classNames: 'pure-button-secondary'
                }]
            },
            moveAddressLocation: {
                type: Y.app.address.UpdateAddressView,
                headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'Update address' }),
                buttons: [{
                    action: 'handleUpdateAddressLocation',
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    classNames: 'pure-button-primary'
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> no',
                    classNames: 'pure-button-secondary'
                }]
            },
            moveResidentPersonAddressLocation: {
                type: Y.app.address.UpdateResidentAddressLocationView,
                headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'Move resident(s)' }),
                buttons: [{
                    action: 'handleUpdateResidentAddressLocation',
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    classNames: 'pure-button-primary'
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    classNames: 'pure-button-secondary'
                }]
            }
        }

    }, {

        CSS_PREFIX: 'yui3-panel',
        ATTRS: {

            width: {
                value: 700
            },

            buttons: {
                value: ['close']
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'form-util',
        'handlebars',
        'handlebars-helpers',
        'handlebars-organisation-templates',
        'event-custom',
        'app-address-form-add',
        'app-address-form-end',
        'app-address-form-update-start-end',
        'app-address-form-reopen',
        'app-address-form-remove',
        'usp-organisation-UpdateCloseOrganisationAddress',
        'usp-address-UpdateEndAddress',
        'usp-organisation-UpdateOrganisationAddressStartEnd',
        'location-dialog',
        'address-add-widget',
        'base-AddressLocation-formatter',
        'handlebars-address-templates',
        'handlebars-person-templates',
        'app-results',
        'usp-view'
    ]
});