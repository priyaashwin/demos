

YUI.add('person-organisation-relationship-dialog-views', function(Y) {
    var L = Y.Lang;	
    var cancelDialog = function(e) {
   		e.preventDefault();
   		this.hide();
	}; 
	
	var addClientPersonTeamRelationshipSubmit = function(e) {
		var _instance = this;
    	e.preventDefault();   
    	
    	 // Get the view and the model
        var view=this.get('activeView'),
        model=view.get('model');
        labels=view.get('labels');
        
        model.setAttrs(Y.merge(view.attributesPanel.getValues('Update'), {
			organisationId : view.get('relatedOrganisationId'),
			personId : view.get('targetPersonId'),
			personOrganisationRelationshipTypeId: view.get('clientTeamId'),
			selectedOrganisation: view.get('selectedOrganisation')
    	}),{
    		//don't trigger the onchange listener as we will re-render after save
    		silent:true
    	});
        
        model.save(function(err, res){
    		//hide the mask -	prevent duplicate save on double click 
    		_instance.hideDialogMask();	
    		var $startDateError;
    		var $orgIdError
    		if (err){
    		
    		$startDateError = _instance.bodyNode.one('a[href="#startDate"]');
    		$orgIdError = _instance.bodyNode.one('a[href="#organisationId"]');
    		$selectedOrganisation = _instance.bodyNode.one('a[href="#selectedOrganisation"]');
				if(err.code !== 400){
					Y.log(err.code + err.msg);
				}
				
				if($startDateError && view.getInput('startDate').get('value')) {
        			if($startDateError.get('text')!=='Invalid date') {
        			$startDateError.set('text', 'Invalid date');	
        			}
            	}
            	if($orgIdError){
	                view.getInput('relatedOrganisationSearch').addClass("error");
	                $orgIdError.on('click', function(e) {
	                view.getInput('relatedOrganisationSearch').focus();
	                });
	              }  
            	if($selectedOrganisation){
	                view.getInput('relatedOrganisationSearch').addClass("error");
	                $selectedOrganisation.on('click', function(e) {
	                view.getInput('relatedOrganisationSearch').focus();
	                });
	              }  
    		}else{
    			_instance.hide();
					
    					
    			Y.fire('infoMessage:message', {message:labels.addRelationshipSuccessMessage});				    
    			Y.fire('allocatedTeam:dataChanged', {});
          Y.fire('relationship:dataChanged', {
            tabIndex:1
          });
    		}
    
        });
        
     
        
	};

	var addPersonOrganisationRelationshipSubmit = function(e) {
    var _instance = this;
		e.preventDefault();

	 	// Get the view and the model
		var view = this.get('activeView'),
			model = view.get('model'),
			labels = view.get('labels'),
			attributePanelValues = view.attributesPanel.getValues('Add'),
			extraValues = {
				organisationId: view.get('relatedOrganisationId'),
				selectedOrganisation: view.get('selectedOrganisation')
			},
			personOrganisationRelationshipTypeId = view.get('container').one('input[name=personOrganisationRelationshipType]:checked').get('value');

		if(personOrganisationRelationshipTypeId){
			extraValues['personOrganisationRelationshipTypeId'] = personOrganisationRelationshipTypeId;
		} else if (attributePanelValues){
				if(attributePanelValues.responsibleLocalAuthority){
					extraValues['personOrganisationRelationshipTypeId'] = view.get('otherData.responsibleLocalAuthorityRelationshipTypeId');
				}
		}

    model.setAttrs(Y.merge(attributePanelValues, extraValues),{
      silent:true
    });

    model.save(function(err, res){
			//hide the mask -	prevent duplicate save on double click
			_instance.hideDialogMask();
			var $startDateError;
			var $orgIdError;
			if (err){

				$startDateError = _instance.bodyNode.one('a[href="#startDate"]');
				$orgIdError = _instance.bodyNode.one('a[href="#organisationId"]');
				$selectedOrganisation = _instance.bodyNode.one('a[href="#selectedOrganisation"]');
    		
				if(err.code !== 400){
					Y.log(err.code + err.msg);
				}

				if($startDateError && view.getInput('startDate').get('value')) {
        	if($startDateError.get('text')!=='Invalid date') {
        			$startDateError.set('text', 'Invalid date');
        	}
        }
        if($orgIdError){
	         view.getInput('relatedOrganisationSearch').addClass("error");
	         $orgIdError.on('click', function(e) {
	            view.getInput('relatedOrganisationSearch').focus();
	         });
	      }
        if($selectedOrganisation){
          view.getInput('relatedOrganisationSearch').addClass("error");
          $selectedOrganisation.on('click', function(e) {
 	          view.getInput('relatedOrganisationSearch').focus();
 	        });
        }
           	
    	}else{
    		_instance.hide();
    		Y.fire('infoMessage:message', {message:labels.addRelationshipSuccessMessage});
        Y.fire('relationship:dataChanged', {
            tabIndex:1
        });
    	}
    });
	};
	
	var deletePersonOrganisationRelationshipSubmit = function(e) {
	    // Get the view and the model
        var view=this.get('activeView'),
        	model=view.get('model'),     
        	_instance = this;
        e.preventDefault();
			//show the mask
        _instance.showDialogMask();
        
        // do the delete
        model.destroy({remove:true}, 
        function(err, response){
        	_instance.hideDialogMask();   
        	if (err===null){
        		        Y.fire('relationship:dataChanged', {
        		            action: 'delete',
        		            tabIndex:1
        		        });

            Y.fire('infoMessage:message', {message:'Relationship successfully removed'}); 
	        _instance.hide();
          }
        });
	};
	
	var editPersonOrganisationRelationshipSubmit = function(e)  {
		var _instance = this,
			view=this.get('activeView'),
			attrs = view.attributesPanel.getValues('Update'),
			model=view.get('model'),
			extraValues = {
        organisationId: view.get('relatedOrganisationId'),
        personId: view.get('targetPersonId')
      };

			e.preventDefault();

			_instance.showDialogMask();

			if(model.get('personOrganisationRelationshipTypeVO') && model.get('personOrganisationRelationshipTypeVO').relationshipClass==='EMPLOYEREMPLOYEE'){
				extraValues['personOrganisationRelationshipTypeId'] = view.get('otherData.employerEmployeeRelationshipTypeId');
			} else if (attrs){
				if(attrs.responsibleLocalAuthority){
					extraValues['personOrganisationRelationshipTypeId'] = view.get('otherData.responsibleLocalAuthorityRelationshipTypeId');
				}
			}

			model.setAttrs(Y.merge(attrs, extraValues),{
				silent:true
			});
        
			//To map organisationType explicitly for client team relationships
			var teamIdentifier =  model.get('organisationVO').organisationType;

			model.save({
					transmogrify:Y.usp.relationship.UpdatePersonOrganisationRelationship,
					organisationType : teamIdentifier
				}, function(err, res){
        	//hide the mask -	prevent duplicate save on double click 
    		_instance.hideDialogMask();	
    		var $startDateError;
    		if (err){
    		
    		$startDateError = _instance.bodyNode.one('a[href="#startDate"]');
				if(err.code !== 400){
					Y.log(err.code + err.msg);
				}
				
				if($startDateError && view.getInput('startDate').get('value')) {
        			if($startDateError.get('text')!=='Invalid date') {
        			$startDateError.set('text', 'Invalid date');	
        			}
            	}
            	 
           	
    		}else{
    			_instance.hide();
    			window.scrollTo(0,0); 
		        Y.fire('relationship:dataChanged', {
		            action: 'edit',
		            tabIndex:1
		        });
		        Y.fire('infoMessage:message', {message:'Relationship successfully updated'}); 
		        
		        if(teamIdentifier === 'TEAM'){
		        	Y.fire('allocatedTeam:dataChanged', {});
		        }
    			//Y.fire('organisationRelationship:refreshResults'); this needs an object to prevent error
    		}
            });
    };
    
    var endPersonOrganisationRelationshipSubmit = function(e)  {
        // Get the view and the model
        var view=this.get('activeView'),
        model=view.get('model');   
        var _instance = this;
			e.preventDefault();
			//show the mask
        _instance.showDialogMask();
        
        model.save({
    		transmogrify:Y.usp.relationship.UpdateClosePersonOrganisationRelationship
    	},
    	
    	function(err, res){
    	//hide the mask -	prevent duplicate save on double click 
		_instance.hideDialogMask();	
		var $startDateError;
		if (err){
		
		$endDateError = _instance.bodyNode.one('a[href="#closeDate"]');
			if(err.code !== 400){
				Y.log(err.code + err.msg);
			}
			
			if($endDateError && view.getInput('closeDate').get('value')) {
    			if($endDateError.get('text')!=='Invalid date') {
    				$endDateError.set('text', 'Invalid date');	
    			}
        	}
		}else{
			_instance.hide();
			window.scrollTo(0,0); 
	        Y.fire('relationship:dataChanged', {
	            action: 'edit',
	            tabIndex:1
	        });
	        Y.fire('infoMessage:message', {message:'Relationship successfully ended'});
			//Y.fire('organisationRelationship:refreshResults'); this needs an object to prevent error
		}
        });
    };
	
Y.namespace('app.relationship').MultiPanelPopUp = Y.Base.create('personOrganisationRelationMultiPanelPopUp', Y.usp.MultiPanelPopUp, [], {
		initializer: function() {
			this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
		},
		
		destructor: function() {
			this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
		},
		
		views: {
			addPersonTeamRelationship: {		
				type: Y.app.relationship.AddPersonOrganisationRelationshipView,
				headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Add relationship</h3>',
		        
				buttons: [{
	        	   	action: 	addClientPersonTeamRelationshipSubmit,
	        	   	section: 	Y.WidgetStdMod.FOOTER,
					name:      'saveButton',
					labelHTML: '<i class="fa fa-check"></i> save',
					classNames:'pure-button-primary'
				},{
					action: 	cancelDialog,
		            section:    Y.WidgetStdMod.FOOTER,
		            name:       'cancelButton', 
		            labelHTML:  '<i class="fa fa-times"></i> cancel',
		            classNames: 'pure-button-secondary'
				}]
			},
			addPersonOrganisationRelationshipView: {		
				type: Y.app.relationship.AddPersonOrganisationRelationshipView,
				headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Add relationship</h3>',
				buttons: [{
	        	   	action: 	addPersonOrganisationRelationshipSubmit,
	        	   	section: 	Y.WidgetStdMod.FOOTER,
					name:      'saveButton',
					labelHTML: '<i class="fa fa-check"></i> save',
					classNames:'pure-button-primary'
				},{
					action: 	cancelDialog,
		            section:    Y.WidgetStdMod.FOOTER,
		            name:       'cancelButton', 
		            labelHTML:  '<i class="fa fa-times"></i> cancel',
		            classNames: 'pure-button-secondary'
				}]
			},
			viewPersonOrganisationRelationshipView: {
				type: Y.app.relationship.ViewPersonOrganisationRelationshipView,
				headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> View relationship</h3>',
				buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'editButton',
                        labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                        classNames: 'pure-button-active',
                        action: function(e) {
                        	
                            e.preventDefault();
                            var model=this.get('activeView').get('model');
                            
                            Y.fire('personOrganisationRelationship:showDialog', {
            		            id: model.get('id'),
            		            action: 'editPersonOrganisationRelationship',
            		            organisationType : model.get('organisationVO').organisationType
            		        });	
                            
                            
                        },
                    }, 
                    {
                    	action:     cancelDialog,
                    	section:    Y.WidgetStdMod.FOOTER,
                    	name:       'cancelButton',
                    	labelHTML:  '<i class="fa fa-times"></i> Cancel',
                    	classNames: 'pure-button-secondary'
                    }]
			},
			deletePersonOrganisationRelationshipView: {
				type: Y.app.relationship.ViewPersonOrganisationRelationshipView,
				headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Remove relationship</h3>',
				buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
                        classNames: 'pure-button-primary',
                        action: deletePersonOrganisationRelationshipSubmit,
                    }, 
                    {
                    	action:     cancelDialog,
                    	section:    Y.WidgetStdMod.FOOTER,
                    	name:       'cancelButton',
                    	labelHTML:  '<i class="fa fa-times"></i> No',
                    	classNames: 'pure-button-secondary'
                    }]
			},
			endPersonOrganisationRelationshipView: {
				type: Y.app.relationship.EndPersonOrganisationRelationshipView,
				headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> End relationship</h3>',
				buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
                        classNames: 'pure-button-primary',
                        action: endPersonOrganisationRelationshipSubmit,
                    }, 
                    {
                    	action:     cancelDialog,
                    	section:    Y.WidgetStdMod.FOOTER,
                    	name:       'cancelButton',
                    	labelHTML:  '<i class="fa fa-times"></i> No',
                    	classNames: 'pure-button-secondary'
                    }]
			},
			editPersonOrganisationRelationshipView: {
            	//this view will be automatically created/destroyed as necessary as it has complex DOM elements
            	type:Y.app.relationship.EditPersonOrganisationRelationshipView,
        		headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Edit relationship</h3>',
        		buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Save',
                        classNames: 'pure-button-primary',
                        action: editPersonOrganisationRelationshipSubmit,
                        disabled: false
                    },
                    {
                    	action:     cancelDialog,
                    	section:    Y.WidgetStdMod.FOOTER,
                    	name:       'cancelButton',
                    	labelHTML:  '<i class="fa fa-times"></i> Cancel',
                    	classNames: 'pure-button-secondary'
                    }]
            }
		}
		
	},{
		
		CSS_PREFIX : 'yui3-panel',
		ATTRS: {
			width: {
				value: 560
			},
			
			buttons: {
				value : ['close' ]
			}
		}
	});

}, '0.0.1', {
	  requires: ['yui-base',
	             'form-util',
	             'event-custom',	             
	             'handlebars-relationship-templates',
	             'usp-relationship-UpdatePersonOrganisationRelationship',
	             'usp-relationship-UpdateClosePersonOrganisationRelationship',
	             'relationships-person-organisation-forms',
	             'usp-relationship-helper',
	             'usp-relationship-PersonOrganisationRelationshipType']
	});