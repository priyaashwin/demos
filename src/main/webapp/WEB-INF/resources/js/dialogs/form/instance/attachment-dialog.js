YUI.add('form-attachment-dialog', function(Y) {
  'use-strict';  
    
    var L=Y.Lang,
        USPFormatters = Y.usp.ColumnFormatters;
  
    Y.namespace('app.form').UploadAttachmentView = Y.Base.create('uploadAttachmentView', Y.usp.form.FormInstanceAttachmentView, [], {
      template:Y.Handlebars.templates["uploadAttachment"],
      render:function(){
        var container=this.get('container');
        
        //call superclass
        Y.app.form.UploadAttachmentView.superclass.render.call(this);
        
        this.sourceForm=new Y.app.source.SourceForm({
          model:this.get('model'),
          labels:this.get('labels')
        });
        
        this.sourceForm.addTarget(this);
        
        container.one('form').append(this.sourceForm.render().get('container'));
        
        return this;
      },
      destructor:function(){
        if(this.sourceForm){
          this.sourceForm.removeTarget(this);
          
          this.sourceForm.destroy();
          
          delete this.sourceForm;
        }
      }
    });
    
    Y.namespace('app.form').NewFormInstanceAttachment = Y.Base.create('uploadAttachmentView', Y.usp.form.NewFormInstanceAttachment, [Y.usp.ModelFormLink, Y.app.source.FormWithSourceMixin], {
      form: '#formUploadAttachment'
    });
    
    Y.namespace('app.form').FormAttachmentRemoveView = Y.Base.create('formAttachmentRemoveView', Y.usp.form.FormInstanceAttachmentView, [], {
      template: Y.Handlebars.templates["removeAttachment"]
    });

    

    Y.namespace('app.form').FormInstanceAttachmentResults = Y.Base.create('formInstanceAttachmentResults', Y.usp.app.Results, [], {
      /**
       * Override the base class rowClick so we can add the formInstance into the click event
       */
      rowClick: function(target, record, permissions) {
        //the default implementation is to fire an event
        var menuAction=target.getData('menu-action');
        if(menuAction){
          this.fire(menuAction, {
            record:record,
            formInstance:this.get('formInstance'),
            permissions:permissions
          });
        }
      },      
      getResultsListModel: function(config) {
        return new Y.usp.form.PaginatedFormInstanceAttachmentList({
            url: L.sub(config.url, {
                id: this.get('formInstance').get('id')
            })
        });
      },
      getColumnConfiguration: function(config) {
        var labels=config.labels,
            permissions=config.permissions,
            formInstance=this.get('formInstance'),
            status=formInstance.get('status');
        
        return ([{
            key: 'title',
            label: labels.title,
            width: '11%',
            sortable: true
          },{
            key: 'lastUploadedBy',
            label: labels.uploadedBy,
            width: '25%'
          },{
            key: 'lastUploaded',
            label: labels.uploadedOn,
            formatter: USPFormatters.date,
            width: '25%',
            sortable: true
          },{
            key: 'description',
            label: labels.description,
            width: '30%'
          },{
            label: labels.actions,
            className: 'pure-table-actions',
            width: '10%',
            formatter: USPFormatters.actions,
            items: [{
                clazz: 'view',
                title: labels.downloadTitle,
                label: labels.download,
                visible: function(){
                  return this.record.hasAccessLevel('READ_DETAIL');
                }, 
                enabled: permissions.canView
            },{
              clazz: 'remove',
              title: labels.removeTitle,
              label: labels.remove,
              visible: function(){
                return status==='DRAFT' && this.record.hasAccessLevel('WRITE');
              }, 
              enabled:status==='DRAFT' && permissions.canRemove
          }]
          }]);
      }
    },{
      ATTRS:{
        resultsListPlugins: {
          valueFn: function() {
            return [{
              fn: Y.Plugin.usp.ResultsTableMenuPlugin
                  }, {
              fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                  },{
                    fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                    cfg: {
                      state: 'expanded',
                      summaryFormatter:Y.app.source.SourceSummaryFormatter
                    }
                  }];
          }
        }
      }
    });
    
    Y.namespace('app.form').AttachmentsView = Y.Base.create('attachmentsView', Y.View, [], {
      initializer: function(config) {      
        var model = this.get('model'),
           searchConfig=config.searchConfig||{};
           
        this.after('*:load', this._align, this);
        this.attachmentPaginatedResults = new Y.app.form.FormInstanceAttachmentResults({
          formInstance:model,
          searchConfig:searchConfig
        }).addTarget(this);
    },
    render:function(){
      //add css class to hide results count
        this.get('container').addClass('results-count-hide');
        
        this.get('container').setHTML(this.attachmentPaginatedResults.render().get('container'));
        return this;
     },
     _align:function(){
       this.fire('align');
     }
    });

    
    Y.namespace('app.form').FormInstanceAttachmentDialog = Y.Base.create('formInstanceAttachmentDialog', Y.app.attachment.AttachmentDialog, [], {
        handleSubmitUploadFile:function(e){
          return Y.when(Y.app.form.FormInstanceAttachmentDialog.superclass.handleSubmitUploadFile.call(this, e).then(this._handleReturn.bind(this)));
        },
        handleCancel:function(e){
          return Y.when(Y.app.form.FormInstanceAttachmentDialog.superclass.handleCancel.call(this, e).then(this._handleReturn.bind(this)));
        },
        handleRemove:function(e){
          return Y.when(Y.app.form.FormInstanceAttachmentDialog.superclass.handleRemove.call(this, e).then(this._handleReturn.bind(this)));
        },        
        _handleReturn:function(e){
          //if the returnTo attribute is set and the formInstance is present
          //then we can fire the event to load the previous dialog view
          var view=this.get('activeView'),
              formInstance=view.get('formInstance'),
              returnTo=view.get('returnTo');
          
          if(returnTo && formInstance){
            this.fire(returnTo, {
              record:formInstance
            });
          }
        },
        handleUploadStart:function(e){
          var view=this.get('activeView'),
              model=view.get('model');
          
          e.preventDefault();
          this.fire('uploadAttachment', {
            record:model,
            returnTo:'manageFiles'
          });
        },
        views: {
          uploadFile:{
            type:Y.app.form.UploadAttachmentView
          },          
          manageFiles:{
            type:Y.app.form.AttachmentsView,
            buttons:[{
                  name: 'uploadFileButton',
                  labelHTML: '<i class="fa fa-upload"></i> Upload',
                  action:'handleUploadStart',
                  disabled: true
                }],
             width: 750
          },
          removeFile:{
            type:Y.app.form.FormAttachmentRemoveView,
            buttons: [{
                name: 'yesButton',
                labelHTML: '<i class="fa fa-check"></i> Yes',
                isPrimary: true,
                action: 'handleRemove',
                disabled: true
            }, {
                name: 'noButton',
                labelHTML: '<i class="fa fa-times"></i> No',
                isSecondary: true,
                action: 'handleCancel'
            }]
          }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'attachment-dialog',
               'view',
               'handlebars-helpers',
               'handlebars-form-templates',
               'usp-form-FormInstanceAttachment',
               'usp-form-NewFormInstanceAttachment',
               'model-form-link',
               'results-formatters',
               'results-table-keyboard-nav-plugin',
               'results-table-menu-plugin',
               'results-table-summary-row-plugin',
               'promise',
               'handlebars-helpers',
               'handlebars-form-templates',
               'source-form']
});