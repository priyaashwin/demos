YUI.add('form-mapping-dialog-views', function (Y) {
    var L = Y.Lang,
        A = Y.Array,
        BaseFormMappingView = Y.Base.create('baseFormMappingView', Y.View, [], {
            initializer:function(){
                var createFactory = function(type) {
                    return React.createElement.bind(null, type);
                };
                
                //setup the form definitions model - will contain the list of form definitions
                this.formDefinitionsModel=new Y.usp.form.FormDefinitionList({
                    url:this.get('definitionsURL')
                });
                
                //setup the form definition model - will contain the complete form definition
                this.sourceFormDefinitionFullDetailsModel=new Y.usp.form.FormDefinitionFullDetails({
                    url:this.get('definitionURL')
                });
    
                this.destinationFormDefinitionFullDetailsModel=new Y.usp.form.FormDefinitionFullDetails({
                    url:this.get('definitionURL')
                });
                            
                //ensure the events are passed to our view instance
                this.formDefinitionsModel.addTarget(this);
                this.sourceFormDefinitionFullDetailsModel.addTarget(this);
                this.destinationFormDefinitionFullDetailsModel.addTarget(this);
                
                
                //setup a React factory for the uspFormMapping
                this.formMappingFactory = createFactory(uspFormMapping.FormMappings);
            },
            render:function(){
                var container=this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());
                
                html = this.template({
                    modelData:this.get('model').toJSON(),
                    otherData:this.get('otherData'),
                    labels:this.get('labels')
                });
                            
                // Render this view's HTML into the container element.
                contentNode.setHTML(html);
                
                this.renderFormMappings(contentNode._node.querySelector('#formMappingComponent'));
                            
                //set the content node to the container
                container.setHTML(contentNode);
                
                return this;            
            },
            renderFormMappings:function(node){
                this.formMappings=ReactDOM.render(this.formMappingFactory({
                    formDefinitionsModel:this.formDefinitionsModel,
                    sourceFormDefinitionModel:this.sourceFormDefinitionFullDetailsModel,
                    destinationFormDefinitionModel:this.destinationFormDefinitionFullDetailsModel,
                    formDefinitionId:this.get('formDefinitionId'),
                    labels:this.get('labels'),
                    //initial form mappings (from the model - not required for an add operation)
                    initialMappings:this.getMappings(),
                    initialSelectedDefinition:this.get('initialSourceFormDefinitionId'),
                    viewOnly:this.get('viewOnly')
                }), node);
            },
            destructor:function(){
                //unbind event handles
                if (this._evtHandlers) {
                    A.each(this._evtHandlers, function(item) {
                        item.detach();
                    });
                    //null out
                    this._evtHandlers = null;
                }
                
                ReactDOM.unmountComponentAtNode(this.get('container').one('#formMappingComponent')._node);
            }           
        },{
            ATTRS:{
                /**
                 * @attribute loaded Tracks if the list of available form definitions is loaded
                 */
                loaded:{
                    value:false
                },
                /**
                 * @attribute definitionsURL The REST URL used to load available definitions 
                 */            
                definitionsURL:{
                    value:'',
                    writeOnce:'initOnly'
                },
                /**
                 * @attribute definitionURL The REST URL used to load the full definition details 
                 */            
                definitionURL:{
                    value:'',
                    writeOnce:'initOnly'
                },
                formDefinitionId:{
                    value:'',
                    writeOnce:'initOnly'                
                },
                otherData:{
                    
                },
                labels:{
                    
                },
                initialSourceFormDefinitionId:{
                    value:null
                },
                /**
                 * @attribute viewOnly Attribute to control the mode that the component renders
                 */
                viewOnly:{
                    value:false
                }
            }
        }),
        NewFormMappingView = Y.Base.create('newFormMappingView', BaseFormMappingView, [], {
            template:Y.Handlebars.templates["formMappingAddDialog"],
            getFieldMappings:function(){
                var mappings=[];
                //does the component exist
                if(this.formMappings){
                    mappings=this.formMappings.getMappings();
                }
                
                return  ({
                    newMappings:mappings
                });
            },
            /**
             * Returns the array of existing mappings (there are none for an add)
             */
            getMappings:function(){
                return [];
            }
        }),
        EditFormMappingView = Y.Base.create('editFormMappingView', BaseFormMappingView, [], {
            template:Y.Handlebars.templates["formMappingEditDialog"],            
            getFieldMappings:function(){
              var newMappings=[],
                  deletedMappings=[],
                  originalMappingsHash={},
                  updatedMappingsHash={};

                  if(this.formMappings){
                      //if the component exists - we access the updated form mappings from this.
                      
                      //create a hash so we can quickly compare keys
                      (this.formMappings.getMappings()||[]).forEach(function(mapping){
                          //update the has with the unique key to this mapping
                          updatedMappingsHash[mapping.destinationId+"~"+mapping.sourceFormId+"~"+mapping.sourceId]=mapping;
                      });
                      
                      //now do create a hash of original mappings
                      this.getMappings().forEach(function(mapping){
                          //update the has with the unique key to this mapping
                          originalMappingsHash[mapping.destination.id+"~"+mapping.sourceFormId+"~"+mapping.source.id]=mapping.id;                  
                      });
                      
                      //now we just build the set of new/deleted keys
                      Object.keys(originalMappingsHash).forEach(function(k){
                          if(updatedMappingsHash[k]){
                              //no change
                              updatedMappingsHash[k]=undefined;
                          }else{
                              //add to deleted
                              deletedMappings.push(originalMappingsHash[k]);
                          }
                      });
                      
                      //now perform the adds
                      Object.keys(updatedMappingsHash).forEach(function(k){
                         if(updatedMappingsHash[k]){
                             newMappings.push(updatedMappingsHash[k]);
                         } 
                      });                      
                  }
              return ({
                      newMappings:newMappings,
                      deletedMappings:deletedMappings
                  });
            },
            /**
             * Returns the array of existing mappings - out of the model
             */            
            getMappings:function(){
                //build the mappings list
                var data=this.get('model').toJSON()||{};
                
                return data.controlMappings||[];
            }
        }),
        ViewFormMappingView = Y.Base.create('viewFormMappingView', BaseFormMappingView, [], {
            template:Y.Handlebars.templates["formMappingViewDialog"],            
            /**
             * Returns the array of existing mappings - out of the model
             */            
            getMappings:function(){
                //build the mappings list
                var data=this.get('model').toJSON()||{};
                
                return data.controlMappings||[];
            },
            getSelectedSourceFormDefinitionId:function(){
                var sourceId;
                
                //we ask the REACT component
                if(this.formMappings){
                    sourceId=this.formMappings.getSelectedSourceDefinitionId();
                }
                
                return sourceId;
            }
        },{
            ATTRS:{
                //Set in readonly mode
                viewOnly:{
                    value:true
                }
            }
        }),        
        RemoveFormMappingView = Y.Base.create('removeFormMappingView', Y.View, [], {
            template:Y.Handlebars.templates["formMappingRemoveDialog"],
            render:function(){
                var container=this.get('container'),
                html = this.template({
                    modelData:this.get('model').toJSON(),
                    otherData:this.get('otherData'),
                    message:this.get('message')
                });
                            
                // Render this view's HTML into the container element.
                container.setHTML(html);
                return this;
            }
        }),
        PreferredFormMappingView=Y.Base.create('preferredFormMappingView', Y.View, [], {
            template:Y.Handlebars.templates["formMappingPreferredDialog"],
            render:function(){
                var container=this.get('container'),
                html = this.template({
                    modelData:this.get('model').toJSON(),
                    otherData:this.get('otherData'),
                    message:this.get('message')
                });
                            
                // Render this view's HTML into the container element.
                container.setHTML(html);
                return this;
            }
        });
    
    //form for creating New form mapping with control mappings instance
    Y.namespace('app').NewFormMappingForm= Y.Base.create('newFormMappingForm', Y.usp.form.NewFormMapping, [Y.usp.ModelFormLink], {
        form:'#newFormMappingForm'
    });
    
    //Form for 'edit' form mapping - model will be transmogrified into an UpdateFormMapping (we need to load the version with all the control mappings)
    Y.namespace('app').EditFormMappingForm= Y.Base.create('editFormMappingForm', Y.usp.form.FormMappingWithControlMappings, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form:'#editFormMappingForm'
    });   
    
    //Form for 'remove' form mapping - A simple YUI Model
    Y.namespace('app').RemoveFormMappingForm = Y.Base.create('removeFormMappingForm', Y.Model, [Y.ModelSync.REST], {});

    //Form for 'preferred' form mapping - A simple YUI Model
    Y.namespace('app').PreferredFormMappingForm = Y.Base.create('preferredFormMappingForm', Y.Model, [Y.ModelSync.REST], { });   
    
    Y.namespace('app').FormMappingDialog= Y.Base.create('formMappingDialog', Y.usp.app.AppDialog, [], {
        views:{
            addFormMapping:{
                type:NewFormMappingView,
                buttons:[{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    classNames: 'pure-button-primary',
                    action: function(e){
                        var view=this.get('activeView'),
                            //load the mappings from the view
                            mappings=view.getFieldMappings()||{};

                        //populate the model with any mappings from the view
                        this.handleSave(e, { 
                           'controlMappings':new Y.usp.form.NewFormControlMappingList({items:mappings.newMappings||[]})
                       });
                    },
                    disabled: true
                }]
            },
            editFormMapping:{
                type:EditFormMappingView,
                buttons:[{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    classNames: 'pure-button-primary',
                    action: function(e){
                        var view=this.get('activeView'),
                            //load the mappings from the view
                            mappings=view.getFieldMappings()||{};

                        this.handleSave(e, {
                            'newMappings':new Y.usp.form.NewFormControlMappingList({items:mappings.newMappings||[]}),
                            'deletedMappings': mappings.deletedMappings||[]
                        }, {
                            transmogrify: Y.usp.form.UpdateFormMapping
                        });
                    },
                    disabled: true
                }]
            },
            viewFormMapping:{
                type:ViewFormMappingView,
                buttons:[{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'editButton',
                    labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                    classNames: 'pure-button-active',
                
                    action: function(e) {
                        e.preventDefault();
                        var view = this.get('activeView'),
                        model=view.get('model');

                        //fire with data about the form type and the mapping id
                        view.fire('edit', {
                            id: model.get('id'),
                            name: model.get('name'),
                            formDefinitionId:model.get('destinationFormDefinitionId'),
                            formDefinitionName:model.get('destinationFormDefinitionName'),
                            //if the user has selected a definition - use this to seed the edit dialog
                            sourceFormDefinitionId:view.getSelectedSourceFormDefinitionId()
                        }); 
                    },
                    disabled: true
                }]
            },   
            removeFormMapping:{
                type:RemoveFormMappingView,
                buttons:[{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Delete',
                    classNames: 'pure-button-primary',
                    action: 'handleRemove',
                    disabled: true
                }]
            },
            preferredFormMapping:{
                type:PreferredFormMappingView,
                buttons:[{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    classNames: 'pure-button-primary',
                    action: 'handleSave',
                    disabled: true
                }]
            }
         }
    });
}, '0.0.1', {
    requires: [ 'yui-base',
                'view',
                'app-dialog',
                'model-form-link',
                'model-transmogrify',
                'handlebars-helpers',
                'handlebars-form-templates',
                'handlebars-form-partials',
                'usp-form-FormDefinition',
                'usp-form-FormDefinitionFullDetails',
                'usp-form-NewFormMapping',
                'usp-form-NewFormControlMapping',
                'usp-form-UpdateFormMapping',
                'usp-form-FormMappingWithControlMappings'
              ]
});