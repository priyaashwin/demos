YUI.add('add-form-dialog', function (Y) {
    var FU = Y.FUtil;

    var MicroSyntax = {
        code: /\{\{%([\s\S]+?)%\}\}/g,
        escapedOutput: /\{\{(?!%)([\s\S]+?)\}\}/g,
        rawOutput: /\{\{\{([\s\S]+?)\}\}\}/g
    };

    var LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div class="loading-main">Please wait....</div></div></div>';

    var ProcessingView = Y.Base.create('processingView', Y.View, [], {
        render: function () {
            var container = this.get('container');
            container.setHTML(LOADING_TEMPLATE);
            return this;
        }
    });

    var BaseAddFormInstanceView = Y.Base.create('baseAddFormInstanceView', Y.View, [], {
            initializer: function () {
                var model = this.get('model');

                // Re-render this view when the model changes
                model.after('change', this.render, this);
            },
            render: function () {
                var container = this.get('container'),
                    otherData = this.get('otherData') || {},
                    html;

                html = this.template({
                    modelData: this.get('model').toJSON(),
                    otherData: otherData,
                    message: this.get('message') || '',
                    infoMessage:this.get('infoMessage') || '',
                    warning: this.get('warning') || '',
                    labels: this.get('labels'),
                    MicroSyntax: MicroSyntax
                });

                // Render this view's HTML into the container element.
                container.setHTML(html);

                return this;
            }
        }, {
            ATTRS: {
                /**
                 * @attribute otherData Should contain the narrative attribute, along with any other data required by the template
                 */
                otherData: {},
                labels: {},
                message: {},
                warning: {},
                infoMessage:{},
                /**
                 * @attribute formType Holds the form type model instance
                 */
                formType: {}
            }
        }),
        AddMultipleFormInstanceView = Y.Base.create('addMultipleFormInstanceView', BaseAddFormInstanceView, [], {
            template: Y.Handlebars.templates.formAddMultipleDialog
        }),
        AddWhereInstanceExistsForGroupView = Y.Base.create('addWhereInstanceExistsForGroupView', BaseAddFormInstanceView, [], {
            template: Y.Handlebars.templates.formAddWhereInstanceExistsForGroupDialog
        }),
        ShowExistingInstanceView = Y.Base.create('showExistingInstanceView', BaseAddFormInstanceView, [], {
            template: Y.Handlebars.templates.formShowExistingInstanceDialog,
            initializer:function(config){
                if(this.get('isRestricted')===true){
                    //setup info message
                    this.set('infoMessage', config.restrictedMessage||'This form must be added via a worklist');
                }
            }
        },{
            ATTRS:{
                isRestricted:{
                    value:false
                }
            }
        }),
        AddFormSelectMappingView = Y.Base.create('addFormSelectMappingView', BaseAddFormInstanceView, [], {
            template: Y.Handlebars.templates.formAddSelectMappingDialog,
            render: function () {
                var container = this.get('container'),
                    select,
                    model = this.get('model'),
                    otherData = this.get('otherData'),
                    formMappingId = model.get('formMappingId'),
                    formMappings = otherData.formType.get('destinationFormMappings');

                //if there is no form mapping specified - attempt to set a default
                if (formMappingId === undefined || formMappingId === null || formMappingId === '') {
                    formMappings.some(function (m) {
                        if (m.preferred === true) {
                            formMappingId = m.id;

                            return true;
                        }
                    });
                }

                //call into superclass to render
                AddFormSelectMappingView.superclass.render.call(this);

                select = container.one('select[name="formMappingId"]');

                //now configure the select options
                FU.setSelectOptions(select, formMappings, null, null, true);

                //set the default value - ensure id is a String otherwise IE won't make the selection
                select.set('value', formMappingId + '');

                return this;
            }
        }),
        AddFormSelectSubTypeView = Y.Base.create('addFormSelectSubTypeView', BaseAddFormInstanceView, [], {
            template: Y.Handlebars.templates.formAddSelectSubTypeDialog,
            render: function () {
                var container = this.get('container'),
                    select,
                    subTypes = (this.get('otherData.formType').get('subTypes') || [])
                    .sort(function (a, b) { return a.name.localeCompare(b.name, 'en', { 'sensitivity': 'base' }) });

                //call into superclass to render
                AddFormSelectSubTypeView.superclass.render.call(this);

                select = container.one('select[name="subTypeId"]');

                FU.setSelectOptions(select, subTypes, null, null, true);

                return this;
            }
        });

    //Model instance used in dialog - NOTE this is not designed to be persisted, rather to gather the user data and pass it back to the calling view,
    //We use the ModelFormLink to populate our form
    Y.namespace('app').NewFormInstanceModel = Y.Base.create('newFormInstanceModel', Y.Model, [Y.usp.ModelFormLink], {
        form: '#newFormInstance'
    }, {
        ATTRS: {
            'formDefinitionId': {},
            'name': {},
            'subjectId': {},
            'formMappingId': {},
            'subTypeId': {}
        }
    });

    var yesButton = {
        name: 'saveButton',
        labelHTML: '<i class="fa fa-check"></i> Yes',
        disabled: true
    };

    var noButton = {
        name: 'cancelButton',
        labelHTML: '<i class="fa fa-times"></i> No',
        isSecondary: true,
        action: 'handleCancel'
    };

    Y.namespace('app.form').AddFormInstanceDialog = Y.Base.create('addFormInstanceDialog', Y.usp.app.AppDialog, [], {
        views: {
            addFormLoading: {
                type: ProcessingView
            },
            addMultipleInstance: {
                type: AddMultipleFormInstanceView,
                buttons: [Y.merge(yesButton, {
                    action: '_handleSuccess'
                }), noButton]
            },
            addWhereInstanceExistsForGroup: {
                type: AddWhereInstanceExistsForGroupView,
                buttons: [Y.merge(yesButton, {
                    action: '_handleSuccess'
                }), noButton]
            },
            showExistingInstance: {
                type: ShowExistingInstanceView,
                buttons: [Y.merge(yesButton, {
                    action: '_handleView'
                }), noButton]
            },
            addFormSelectMapping: {
                type: AddFormSelectMappingView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Accept',
                    disabled: true,
                    action: '_handleSuccess'
                }, 'cancelButton']
            },
            addFormSelectSubType: {
                type: AddFormSelectSubTypeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Accept',
                    disabled: true,
                    action: '_handleSuccess'
                }, {
                    name: 'skipButton',
                    isSecondary: true,
                    labelHTML: '<i class="fa fa-forward"></i> Skip',
                    disabled: false,
                    action: '_handleSkipSubType'
                }]
            }
        },
        _handleSkipSubType: function (e) {
            //prevent default button action
            e.preventDefault();

            this._saveIntoFormModel(true);
        },
        _handleSuccess: function (e) {
            //prevent default button action
            e.preventDefault();

            this._saveIntoFormModel();
        },
        _saveIntoFormModel: function (skipSubType) {
            var view = this.get('activeView'),
                labels = view.get('labels'),
                model = view.get('model'),
                formType = view.get('otherData').formType || view.get('formType'),
                subject = view.get('otherData').subject,
                linkTarget = view.get('otherData').linkTarget,
                destinationFormMappings = formType ? formType.get('destinationFormMappings') : [];

            //perform a save - this is only saving into the model - not to the server (assuming configuration does not change)
            model.save({}, Y.bind(function (err) {
                //slightly curious case here as there is no SYNC layer, then err is undefined on success
                if (err === null || err === undefined) {
                    //before we fire success - check if the user should have selected a mapping. This is true if there are any mappings against the view
                    //we can match on the view name to determine if we should apply this additional validation
                    if (view.name === 'addFormSelectMappingView') {
                        //remember - we can only do this as there is no SYNC layer in action here, so the result of our save
                        //is just the population of the model via the ModelFormLink.
                        //Please don't attempt to adopt this style of validation anywhere else. Validation in normal cases is done
                        //at the model level and is checked automatically as part of the model save AOP.
                        if (destinationFormMappings.length > 1 && (model.get('formMappingId') === null || model.get('formMappingId') === '')) {
                            //error condition
                            view.fire('error', {
                                error: {
                                    code: 400,
                                    msg: {
                                        validationErrors: {
                                            'formMappingId': labels.emptyFormMappingId
                                        }
                                    }
                                }
                            });
                            return;
                        }
                    } else if (view.name === 'addFormSelectSubTypeView') {
                        if (skipSubType) {
                            //clear any sub-type selection
                            model.set('subTypeId', null, { silent: true });
                        }
                        if (!skipSubType) {
                            if (model.get('subTypeId') === null || model.get('subTypeId') === '') {
                                //error condition
                                view.fire('error', {
                                    error: {
                                        code: 400,
                                        msg: {
                                            validationErrors: {
                                                'subTypeId': labels.emptySubTypeId
                                            }
                                        }
                                    }
                                });
                                return;
                            }
                        }
                    }
                    //fire an event to indicate success - NOTE no data is saved here, it is up to the caller to work out what to save in this instance
                    view.fire('success', {
                        model: model,
                        formType: formType,
                        subject: subject,
                        linkTarget: linkTarget
                    });
                }
            }, this));
        },
        _handleView: function (e) {
            e.preventDefault();
            // Get the view and the model
            var view = this.get('activeView');
            //hide the dialog
            this.hide();
            // view the existing form by firing an event which will be picked up in the view            
            view.fire('view', {
                id: view.get('otherData').existingFormInstances.item(0).get('id'),
                formName: view.get('otherData').formName
            });
        }
    }, {
        ATTRS: {
            //Don't want the standard set of buttons which includes a "Cancel" button
            buttons: {
                value: ['closeButton']
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'app-dialog',
        'model-form-link',
        'model-transmogrify',
        'form-util',
        'handlebars-helpers',
        'handlebars-form-templates'
    ]
});