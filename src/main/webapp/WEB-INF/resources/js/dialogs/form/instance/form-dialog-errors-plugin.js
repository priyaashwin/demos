YUI.add('form-dialog-errors-plugin', function(Y) {
    'use-strict';

    var FORM_CONTENT_ERROR_IDENTIFIER = 'error in object \'sectionContent\'';

    // An application-specific extension of the MultiPanelModelErrorsPlugin for use
    // by the FormInstanceDialog. The plugin has a custom method for click 
    // on error links, which launches the form editor
    function FormDialogErrorsPlugin(config) {
        FormDialogErrorsPlugin.superclass.constructor.apply(this, arguments);
    }

    FormDialogErrorsPlugin.NAME = 'formDialogErrors';
    FormDialogErrorsPlugin.NS = 'formDialogErrorsPlugin';
    FormDialogErrorsPlugin.ATTRS = {};

    Y.extend(FormDialogErrorsPlugin, Y.Plugin.usp.MultiPanelModelErrorsPlugin, {
        setErrorContent: function(content) {
            //call the parent object
            this.constructor.superclass.setErrorContent.call(this, content);

            //if this error is about form content
            if (this.formContentError) {
                // Disable the save button
                this._multiPanelPopup.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', true);
            }
        },
        _handleOnError: function(e) {
            var response = e.response || {},
                responseText = response.responseText;

            //We need to find out if this error is about Form Content, if so we will set a flag to indicate that
            //the errors need to be handled in a specific way
            //!!! Unfortunately this is the best we can do at the moment, which is to hunt in the responseText for an error about sectionContent
            if (responseText && responseText.indexOf(FORM_CONTENT_ERROR_IDENTIFIER) >= 0) {
                this.formContentError = true;
            } else {
                this.formContentError = false;
            }

            //call the parent object
            this.constructor.superclass._handleOnError.call(this, e);
        },
        _handleErrorClick: function(e) {
            var activeView = this._multiPanelPopup.get('activeView'),
                formEditorURL = activeView.get('formEditorURL');

            //if this error is about form content
            if (this.formContentError && activeView.get('formDefinition') && formEditorURL) {
                //handle the form content error
                this._handleFormContentError(e);
            } else {
                //call the parent object
                this.constructor.superclass._handleErrorClick.call(this, e);
            }
        },
        _handleFormContentError: function(e) {
            var activeView = this._multiPanelPopup.get('activeView'),
                hash = e.currentTarget.get('hash'),
                formDefinition = activeView.get('formDefinition'),
                formInstanceURL = activeView.get('formEditorURL'),
                id = activeView.get('model').get('id'),
                name = formDefinition.get('name');


            // Launch the form
            Y.app.FormLauncher.open(formInstanceURL, id, name, undefined, hash.substring(1));

            // Close the dialog
            this._multiPanelPopup.hide();
        }
    });
    Y.namespace('app.form.Plugin').FormDialogErrorsPlugin = FormDialogErrorsPlugin;
}, '0.0.1', {
    requires: ['yui-base', 'multi-panel-model-errors-plugin', 'form-launcher']
});