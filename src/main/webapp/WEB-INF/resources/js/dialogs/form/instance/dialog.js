YUI.add('form-instance-dialog', function(Y) {
  'use-strict';
  
  var FU = Y.FUtil;
  
  var LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div class="loading-main">Please wait....</div></div></div>';

  var ProcessingView = Y.Base.create('processingView', Y.View, [], {
      render: function() {
          var container = this.get('container');
          container.setHTML(LOADING_TEMPLATE);
          return this;
      }
  });
  
    //Base View for status change
    var BaseFormInstanceStatusChangeView = Y.Base.create('baseFormInstanceStatusChangeView', Y.View, [], {
            render: function() {
                var container = this.get('container'),
                    html;
                
                html = this.template({
                    modelData: this.get('model').toJSON(),
                    otherData: this.get('otherData'),
                    labels:this.get('labels'),
                    subTypes:this.get('subTypes')
                });

                // Render this view's HTML into the container element.
                container.setHTML(html);
                return this;
            }
        }, {
            ATTRS: {
                model: {},
                otherData: {},
                subTypes:{}
            }
        }),
        FormInstanceStatusChangeWithSubTypeView = Y.Base.create('formInstanceStatusChangeWithSubTypeView', BaseFormInstanceStatusChangeView, [], {
          template: Y.Handlebars.templates["statusChangeDialog"],
          initializer:function(){
            var formDefinition=this.get('formDefinition'),
                subTypes=formDefinition.get('subTypes')||[];
            
            //sort the subTypes
            this.set('subTypes', subTypes.sort(function(a,b){return a.name.localeCompare(b.name, 'en', {'sensitivity': 'base'})}),{silent:true});
          },
          render:function(){
            var container = this.get('container'),
                select,
                subTypes=this.get('subTypes'),
                model=this.get('model');
            
            //call into superclass to render
            FormInstanceStatusChangeWithSubTypeView.superclass.render.call(this);
            
            if(subTypes.length>0){
              select=container.one('select[name="subTypeId"]');
              
              FU.setSelectOptions(select, subTypes, null, null, true);
              
              select.set('value', model.get('subTypeId'));
            }
            return this;
          }
        },{
          ATTRS:{
            formDefinition:{
            }
          }
        }),
        FormInstanceStatusChangeView = Y.Base.create('formInstanceStatusChangeView', BaseFormInstanceStatusChangeView, [], {
            template: Y.Handlebars.templates["statusChangeDialog"]
        });

    //Status change form (used for all status changes except 'reject') - simple YUI form
    Y.namespace('app.form').FormInstanceStatusChangeForm = Y.Base.create('forminstanceStatusChangeForm', Y.Model, [Y.ModelSync.REST, Y.usp.ModelFormLink], {
        form: '#statusChangeForm'
    }, {
        ATTRS: {
            subTypeId: {
                value: ''
            }
        }
    });

    //Form for 'remove' form
    Y.namespace('app.form').FormInstanceRemoveForm = Y.Base.create('formInstanceRemoveForm', Y.Model, [Y.ModelSync.REST], {});

    //Form for comments capture as part of review process
    Y.namespace('app.form').FormReviewCommentsForm = Y.Base.create('formReviewCommentsForm', Y.usp.form.UpdateReviewFormInstance, [Y.usp.ModelFormLink],{
      form:'#reviewForm'
    });
      
    
    var FormInstanceReviewCommentsView = Y.Base.create('formInstanceReviewCommentsView', BaseFormInstanceStatusChangeView, [Y.usp.RichText], {
      template:Y.Handlebars.templates["formReviewCommentsDialog"]
    });
    
    Y.namespace('app.form').FormInstanceDialog = Y.Base.create('formInstanceDialog', Y.usp.app.AppDialog, [], {
        initializer: function() {
            //we want to use our own version of the errors plugin
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            //plug in the FormInstanceStatusDialogErrorsPlugin errors handling
            this.plug(Y.app.form.Plugin.FormDialogErrorsPlugin);
            this.plug(Y.usp.Plugin.RichTextFocusManager); 
        },
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.app.form.Plugin.FormDialogErrorsPlugin);
            this.unplug(Y.usp.Plugin.RichTextFocusManager);
        },
        views: {
            processing: {
              type: ProcessingView
            },          
            submitForm: {
                type: FormInstanceStatusChangeWithSubTypeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Submit',
                    action:'handleSave',
                    disabled: true
                },'cancelButton']
            },
            withdrawForm: {
                type: FormInstanceStatusChangeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Unsubmit',
                    action:'handleSave',
                    disabled: true
                },'cancelButton']
            },
            completeForm: {
                type: FormInstanceStatusChangeWithSubTypeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Complete',
                    action:'handleSave',
                    disabled: true
                },'cancelButton']
            },
            uncompleteForm: {
                type: FormInstanceStatusChangeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Uncomplete',
                    action:'handleSave',
                    disabled: true
                },'cancelButton']
            },
            deleteForm: {
                type: FormInstanceStatusChangeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Delete',
                    action:'handleSave',
                    disabled: true
                },'cancelButton']
            },
            removeForm: {
                type: FormInstanceStatusChangeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Remove',
                    action:'handleRemove',
                    disabled: true
                },'cancelButton']
            },
            rejectForm:{
              type:FormInstanceReviewCommentsView,
              buttons:[{
                     name: 'saveButton',
                     labelHTML: '<i class="fa fa-check"></i> Reject',
                     action: 'handleSave',
                     disabled: true
              },'cancelButton']
          },
          approveForm:{
              type:FormInstanceReviewCommentsView,
              buttons:[{
                   name: 'saveButton',
                   labelHTML: '<i class="fa fa-check"></i> Authorise',
                   action: 'handleSave',
                   disabled: true
              },'cancelButton']
          }
        }
    }, {
        ATTRS: {
            //Don't want the standard set of buttons which includes a "Cancel" button
            buttons: {
                value: ['closeButton']
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'app-dialog',
               'view',
               'form-util',
               'form-dialog-errors-plugin',
                'handlebars-helpers',
                'handlebars-form-templates',
                'handlebars-form-partials',
                'view-rich-text',
                'usp-form-UpdateReviewFormInstance',
                'model-form-link'
                ]
});