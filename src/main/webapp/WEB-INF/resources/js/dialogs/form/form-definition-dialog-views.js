var OUTPUT_LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div>Please wait, checking for output templates....</div></div></div>';

YUI.add('form-definition-dialog-views', function(Y) {
  var L = Y.Lang,
    Micro = Y.Template.Micro,
    /*jshint multistr:true */
    subTypeTemplate = Micro.compile('<div class="subType input-group input-append">\
            <div class="pure-u-1"><%=this.value%></div><span><a class="pure-button pure-button-secondary remove" title="remove" aria-label="Remove sub-type from list"><i class="fa fa-minus-circle"></i></a></span>\
                    </div>'),
    /*jshint multistr:false */
    SubTypesView = Y.Base.create('subTypesView', Y.View, [], {
      containerTemplate: '<ul>',
      events: {
        '.add': {
          click: 'addItem'
        },
        'input': {
          'keyup': 'handleKeyUp'
        }
      },
      initializer: function() {
        var model = this.get('model');
        // Re-render this view when the model changes
        model.after(['add', 'remove'], this.render, this);
      },
      handleKeyUp: function(e) {
        if (e.keyCode === 13) {
          //only prevent the default if we wish to consume this event
          e.preventDefault();
          this.addItem();
        }
      },
      handleClick: function(e) {
        e.preventDefault();
        this.addItem();
      },
      addItem: function() {
        var inputNode = this.inputNode,
          value;
        if (inputNode) {
          value = inputNode.get('value');
          if (value && value.trim().length > 0) {
            this.get('model').add({
              value: inputNode.get('value')
            });
            //clear the value ready for the next input
            inputNode.set('value', '');
            //re-focus on the field
            inputNode.focus();
          }
        }
      },
      render: function() {
        var container = this.get('container'),
          modelList = this.get('model'),
          contentNode = Y.one(Y.config.doc.createDocumentFragment()),
          inputNode = Y.Node.create('<input type="text" class="pure-input-1" name="subType" />');
        //Add the input node
        contentNode.appendChild('<li />').appendChild('<div class="input-group input-append " />').appendChild(inputNode);
        //insert the button
        inputNode.insert(Y.Node.create('<span><a class="pure-button pure-button-active add" title="Add" aria-label="Add sub-type to list"><i class="fa fa-plus-circle"></i></a></span>'), 'after');
        //store the inputNode against our instance for quick access
        this.inputNode = inputNode;

        //iterate known sub types and add to fragment
        modelList.each(function(model) {
          var view = new SubTypeView({
            model: model
          });
          //append to our fragment
          contentNode.append(view.render().get('container'));
        });
        //set the fragment into the view container
        container.setHTML(contentNode);
        return this;
      },
      destructor: function() {
        if (this.inputNode) {
          this.inputNode.destroy();
          delete this.inputNode;
        }
      }
    }),
    SubTypeView = Y.Base.create('subTypeView', Y.View, [], {
      containerTemplate: '<li class="subType">',
      events: {
        '.remove': {
          'click': 'removeItem'
        }
      },
      initializer: function() {
        var model = this.get('model');
        // Re-render this view when the model changes
        model.after('destroy', this.destroy, this);
      },
      removeItem: function(e) {
        var model = this.get('model');
        e.preventDefault();
        model.destroy();
      },
      template: subTypeTemplate,
      render: function() {
        var container = this.get('container'),
          model = this.get('model');
        container.setHTML(this.template(model.toJSON()));
        return this;
      }
    }),
    SubTypesModel = Y.Base.create('subTypesModel', Y.ModelList, [], {
      // Custom comparator to keep pies sorted by type.
      comparator: function(model) {
        var val = model.get('value') || '';
        return val.toLowerCase();
      }
    });

  //Status change form (used for all status changes) - simple YUI form
  Y.namespace('app.admin.form').FormDefinitionStatusChangeForm = Y.Base.create('formDefinitionStatusChangeForm', Y.Model, [Y.ModelSync.REST], {});

  //Form for edit form definition - model will be transmogrified into an UpdateFormDefinition
  Y.namespace('app.admin.form').FormDefinitionEditForm = Y.Base.create('formDefinitionEditForm', Y.usp.formchecklist.FormDefinitionWithRelatedChecklist, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
    form: '#editFormDefinitionForm',
    customValidation: function(attrs) {
      var errors = {},
        securityDomain = attrs.securityDomain || '',
        labels = this.labels || {};


      if (securityDomain === null || securityDomain === '') {
        errors['securityDomain'] = labels.securityDomainValidationErrorMessage;
      }

      return errors;
    }
  }, {
    ATTRS: {
      isCardinalityYes: {
        value: false,
        writeOnce: 'initOnly',
        getter: function() {
          return this.get('cardinality') === 'UNCONSTRAINED';
        }
      }
    }
  });
  //Form for view form definition 
  Y.namespace('app.admin.form').FormDefinitionViewForm = Y.Base.create('formDefinitionViewForm', Y.usp.formchecklist.FormDefinitionWithOutputTemplatesAndRelatedChecklist, [], {}, {
    ATTRS: {
      outputTemplates: {
        getter: function(v) {
          //filter to remove archived template
          return (v || []).filter(function(t) {
            return t.status !== 'ARCHIVED';
          });
        }
      },
      isCardinalityYes: {
        value: false,
        writeOnce: 'initOnly',
        getter: function() {
          return this.get('cardinality') === 'UNCONSTRAINED';
        }
      }
    }
  });

  //Form for 'remove' form definition
  Y.namespace('app.admin.form').FormDefinitionRemoveForm = Y.Base.create('formDefinitionRemoveForm', Y.Model, [Y.ModelSync.REST], {});


  Y.namespace('app.admin.form').FormDefinitionStatusChangeView = Y.Base.create('formDefinitionStatusChangeView', Y.View, [], {
    template: Y.Handlebars.templates["formTypeStatusChangeDialog"],
    initializer: function() {
      //publish events
      this.publish('saved', {
        preventable: true,
        broadcast: true
      });
    },
    render: function() {
      var container = this.get('container'),
        html = this.template({
          modelData: this.get('model').toJSON(),
          otherData: this.get('otherData'),
          labels: this.get('labels')
        });
      // Render this view's HTML into the container element.
      container.setHTML(html);
      return this;
    }
  }, {
    ATTRS: {
      model: {},
      /**
       * @attribute otherData Should contain the narrative attribute, along with any other data required by the template
       */
      otherData: {},
      labels: {}
    }
  });

  //view for publish event - extends the FormDefinitionStatusChangeView but provides a list of output template associated with the previous version
  //of the definition
  Y.namespace('app.admin.form').FormDefinitionPublishView = Y.Base.create('formDefinitionPublishView', Y.app.admin.form.FormDefinitionStatusChangeView, [], {
    template: Y.Handlebars.templates["formTypePublishDialog"],
    initializer: function() {
      //setup event handlers - re-render after last definition loaded
      this._changeEvts = [
        this.after('outputTemplatesChange', this.render, this)
      ];

      //get the form definition Id
      var formDefinitionId = this.get('formDefinitionId');

      //start a promise to load form definition versions
      this._getFormDefinitionVersions(formDefinitionId).then(function(definitions) {
        var definitionsArray = definitions ? definitions.toArray() : [],
          outputTemplates = [];
        if (definitionsArray.length > 1) {
          //look at the latest version
          if (definitionsArray[0].get('outputTemplates').length === 0) {
            //no templates attached to this version

            //get last published
            for (var i = 1, len = definitionsArray.length; i < len; i++) {
              var defn = definitionsArray[i];
              if (defn.get('status') === 'PUBLISHED' || defn.get('status') === 'ARCHIVED') {
                outputTemplates = defn.get('outputTemplates');
                break;
              }
            }
          }
        }

        //set the lastDefinition attribute - this will trigger the listener
        this.set('outputTemplates', outputTemplates);
      }.bind(this));
    },
    _getFormDefinitionVersions: function(formDefinitionId) {
      var model = new Y.usp.form.FormDefinitionWithOutputTemplatesList({
        url: L.sub(this.get('formDefinitionVersionsListURL'), {
          formDefinitionId: formDefinitionId
        })
      });
      return new Y.Promise(function(resolve, reject) {
        this.set('loading', false);
        var data = model.load(function(err) {
          if (err !== null) {
            //failed for some reason so reject the promise
            reject(err);
          }
          //success, so resolve the promise
          resolve(data);
        });
      }.bind(this));
    },
    render: function() {
      var container = this.get('container'),
        contentNode = Y.one(Y.config.doc.createDocumentFragment()),
        outputTemplates = this.get('outputTemplates');

      contentNode.append(this.template({
        modelData: this.get('model').toJSON(),
        otherData: this.get('otherData'),
        labels: this.get('labels'),
        outputTemplates: outputTemplates
      }));

      if (this.get('loading') === true) {
        contentNode.append(OUTPUT_LOADING_TEMPLATE);
      }
      //set the fragment into the view container
      container.setHTML(contentNode);
      return this;
    }
  }, {
    ATTRS: {
      outputTemplates: {
        value: []
      },
      formDefinitionId: {
        value: null
      },
      formDefinitionVersionsListURL: {
        value: ''
      },
      loading: {
        value: true
      }
    }
  });

  Y.namespace('app.admin.form').BaseFormDefinitionView = Y.Base.create('baseFormDefinitionView', Y.View, [], {
    render: function() {
      var container = this.get('container'),
        html = this.template({
          modelData: this.get('model').toJSON(),
          otherData: this.get('otherData'),
          codedEntries: this.get('codedEntries'),
          labels: this.get('labels')
        });
      // Render this view's HTML into the container element.
      container.setHTML(html);
      return this;
    }
  }, {
    ATTRS: {
      codedEntries: {
        value: {
          entryTypes: Y.uspCategory.casenote.entryType.category.codedEntries
        }
      },
      model: {},
      otherData: {},
      labels: {}
    }
  });

  Y.namespace('app.admin.form').FormDefinitionEditView = Y.Base.create('formDefinitionEditView', Y.app.admin.form.BaseFormDefinitionView, [], {
    template: Y.Handlebars.templates["formTypeEditDialog"],
    events: {
      '#editFormDefinitionForm_instancesRequireAuthorisation_Yes': {
        'focus': '_onInstancesRequireAuthorisationChange',
        'blur': '_onInstancesRequireAuthorisationChange'
      },
      '#editFormDefinitionForm_instancesRequireAuthorisation_No': {
        'focus': '_onInstancesRequireAuthorisationChange',
        'blur': '_onInstancesRequireAuthorisationChange'
      },
      '#editFormDefinitionForm_reviewGeneratesCaseNote_Yes': {
        'focus': '_onReviewGeneratesCaseNoteChange',
        'blur': '_onReviewGeneratesCaseNoteChange'
      },
      '#editFormDefinitionForm_reviewGeneratesCaseNote_No': {
        'focus': '_onReviewGeneratesCaseNoteChange',
        'blur': '_onReviewGeneratesCaseNoteChange'
      }
    },
    initializer: function(config) {
      var labels = config.labels;
      var model = this.get('model');
      this.subTypesModel = new SubTypesModel();
      // Re-render this view when the model loads
      model.after('load', this.handleAfterLoad, this);

      this.subTypesView = new SubTypesView({
        model: this.subTypesModel
      });

      this.checklistDefinitionAutocompleteView = new Y.app.checklist.ChecklistDefinitionAutoCompleteResultView({
        autocompleteURL: this.get('checklistDefinitionAutocompleteURL'),
        inputName: 'relatedChecklistDefinitionId',
        formName: 'editFormDefinitionForm',
        labels: {
          placeholder: labels.checklistDefinitionPlaceholder
        }
      });
    },
    handleAfterLoad: function() {
      var model = this.get('model');
      var relatedChecklistDefinition = null;
      if (null !== model.get('relatedChecklistDefinitionId')) {
        relatedChecklistDefinition = {
          definitionId: model.get('relatedChecklistDefinition.definitionId'),
          name: model.get('relatedChecklistDefinition.name')
        };
      }
      this.checklistDefinitionAutocompleteView.set('selectedChecklistDefinition', relatedChecklistDefinition);

      this.render();
    },
    render: function() {
      var container = this.get('container'),
        model = this.get('model'),
        securityDomains = this.get('securityDomains'),
        securityDomain = {
          code: null,
          name: null
        },
        securityDomainCode = model.get('securityDomain'),
        activityDateControlName = model.get('activityDateControlName'),
        activityDateControlId = model.get('activityDateControlLocalId'); 

      //Reset our sub-types model
      this.subTypesModel.reset(model.get('subTypes').map(function(st) {
        return ({
          value: st.name
        });
      }));

      //call into superclass to render
      Y.app.admin.form.FormDefinitionEditView.superclass.render.call(this);

      //render our subtypes control
      container.one('#subTypes').setHTML(this.subTypesView.render().get('container'));

      if (securityDomainCode) {
        securityDomain = securityDomains.filter(function(f) {
          return f.code === securityDomainCode;
        })[0] || securityDomain;
      }

      Y.FUtil.setSelectOptions(container.one('#editFormDefinitionForm_securityDomain'), securityDomains.filter(function(sd) {
        return sd.active === true;
      }), securityDomainCode, securityDomain.name || securityDomainCode, true, true, 'code');
      container.one('#editFormDefinitionForm_securityDomain').set('value', securityDomainCode);
      
      // Initialise activity date Inputs
      this.initActivityDateControlInputs(this.get('activityDateControlArray'), activityDateControlId, activityDateControlName);

      // Initialise form inputs relating to generate CaseNote entry on review 
      this.initReviewCaseNoteEntryInputs();

      if (this.checklistDefinitionAutocompleteView) {
        container.one('#checklistDefinitionIdAutocomplete').append(this.checklistDefinitionAutocompleteView.render().get('container'));
      }
    },
    destructor: function() {
      if (this.subTypesView) {
        this.subTypesView.destroy();
        delete this.subTypesView;
      }

      if (this.checklistDefinitionAutocompleteView) {
        this.checklistDefinitionAutocompleteView.removeTarget(this);
        this.checklistDefinitionAutocompleteView.destroy();
        delete this.checklistDefinitionAutocompleteView;
      }
    },
    getSubTypes: function() {
      //convert sub-types back to simple array
      var subTypes = [],
        modelList = this.subTypesModel;
      if (modelList) {
        subTypes = modelList.map(function(m) {
          return m.get('value');
        });
      }
      return subTypes;
    },
    initActivityDateControlInputs: function(activityDateControlArray, activityDateControlId, activityDateControlName) {   
    	var dateControls,
    	activityDateControlSelect = this.get('container').one('#activityDateControlLocalId');
    	
    	dateControls = activityDateControlArray.map(function(control){
            //convert to simple id,value object
            return({
                id: control.localId,
                name: control.name
            });
        });
    	// added null as an empty option in the select list so that the field can be set to null
    	dateControls.unshift({id:null, name: '' });
        
    	// added activityDateControl to the option list for those records where the control field selected does not exists for the FormDefinition
        Y.FUtil.setSelectOptions(activityDateControlSelect, dateControls, activityDateControlId, activityDateControlName, true, true);
        activityDateControlSelect.set('value', activityDateControlId);    	
    },
    
    initReviewCaseNoteEntryInputs: function() {
      var instancesRequireAuthorisation = this.get('model').get('instancesRequireAuthorisation'),
        reviewGeneratesCaseNote = this.get('model').get('reviewGeneratesCaseNote');

      // Initialise the ReviewCaseNoteEntryType input with coded entries
      this.initReviewCaseNoteEntryTypeInput(this.get('model').get('reviewCaseNoteEntryType'));

      // Enable/disable inputs as necessary
      if (instancesRequireAuthorisation) {
        this.toggleReviewGeneratesCaseNoteRadios(true);
        if (reviewGeneratesCaseNote) {
          this.toggleReviewCaseNoteEntryTypeInput(true);
        } else {
          this.toggleReviewCaseNoteEntryTypeInput(false);
        }
      } else {
        this.toggleReviewGeneratesCaseNoteRadios(false);
      }
    },
    initReviewCaseNoteEntryTypeInput: function(value) {
      var container = this.get('container'),
        reviewCaseNoteEntryType = container.one('#editFormDefinitionForm_reviewCaseNoteEntryType');

      Y.FUtil.setSelectOptions(reviewCaseNoteEntryType, Y.Object.values(Y.uspCategory.casenote.entryType.category.getActiveCodedEntries()), null, null, true, true, 'code');
      if (value) {
        reviewCaseNoteEntryType.set('value', value);
      }
    },
    toggleReviewCaseNoteEntryTypeInput: function(enable) {
      var container = this.get('container'),
        reviewCaseNoteEntryType = container.one('#editFormDefinitionForm_reviewCaseNoteEntryType'),
        mandatorySymbol = container.one('#editFormDefinitionForm_reviewCaseNoteEntryType_mandatory');

      if (enable) {
        reviewCaseNoteEntryType.removeAttribute('disabled');
        reviewCaseNoteEntryType.removeClass('disabled');
        mandatorySymbol.removeClass('hidden');
      } else {
        reviewCaseNoteEntryType.set('value', '');
        reviewCaseNoteEntryType.setAttribute('disabled', true);
        reviewCaseNoteEntryType.addClass('disabled');
        mandatorySymbol.addClass('hidden');
      }

    },
    toggleReviewGeneratesCaseNoteRadios: function(enable) {
      var container = this.get('container'),
        reviewCaseNoteEntryTypeYes = container.one('#editFormDefinitionForm_reviewGeneratesCaseNote_Yes'),
        reviewCaseNoteEntryTypeNo = container.one('#editFormDefinitionForm_reviewGeneratesCaseNote_No');

      if (enable) {
        //instances require authorisation, so enable review case note entry radios 
        reviewCaseNoteEntryTypeYes.removeAttribute('disabled');
        reviewCaseNoteEntryTypeYes.removeClass('disabled');
        reviewCaseNoteEntryTypeNo.removeAttribute('disabled');
        reviewCaseNoteEntryTypeNo.removeClass('disabled');
      } else {
        //instances do not require authorisation, so clear and disable all review case note entry inputs
        reviewCaseNoteEntryTypeYes.set("checked", false);
        reviewCaseNoteEntryTypeNo.set("checked", true);
        reviewCaseNoteEntryTypeYes.setAttribute('disabled', true);
        reviewCaseNoteEntryTypeYes.addClass('disabled');
        reviewCaseNoteEntryTypeNo.setAttribute('disabled', true);
        reviewCaseNoteEntryTypeNo.addClass('disabled');

        this.toggleReviewCaseNoteEntryTypeInput(false);
      }
    },
    _onReviewGeneratesCaseNoteChange: function(e) {
      var reviewGeneratesCaseNote = e.currentTarget.get('value');

      //if reviews should generate case note entries, enable case note entry type input
      //else clear and disable case note entry type input
      this.toggleReviewCaseNoteEntryTypeInput(reviewGeneratesCaseNote === 'true');
    },
    _onInstancesRequireAuthorisationChange: function(e) {
      var instancesRequireAuthorisation = e.currentTarget.get('value');

      //if instances require authorisation, enable review case note entry radios
      //else clear and disable all review case note entry inputs
      this.toggleReviewGeneratesCaseNoteRadios(instancesRequireAuthorisation === 'true');
    }
  }, {
    ATTRS: {
      securityDomains: {
        value: []
      }
    }
  });
  Y.namespace('app.admin.form').FormDefinitionViewView = Y.Base.create('formDefinitionViewView', Y.app.admin.form.BaseFormDefinitionView, [], {
    template: Y.Handlebars.templates["formTypeViewDialog"],
    initializer: function() {
      var model = this.get('model'),
        securityDomains = {};

      // Re-render this view when the model loads
      model.after('load', this.sortAndRender, this);

      this.get('securityDomains').forEach(function(sd) {
        if (sd.code) {
          securityDomains[sd.code] = sd;
        }
      });

      this.set('codedEntries', Y.merge(this.get('codedEntries'), {
        securityDomains: securityDomains
      }));
    },
    sortAndRender: function() {
      //sort the subType list
      var model = this.get('model'),
        subTypes = model.get('subTypes') || [];

      //sort the subTypes and re-set into the model
      model.set('subTypes', subTypes.sort(function(a, b) {
        return a.name.localeCompare(b.name, 'en', {
          'sensitivity': 'base'
        });
      }), {
        silent: true
      });

      //render the view
      this.render();
    }
  });

  Y.namespace('app.admin.form').FormDefinitionDialog = Y.Base.create('formDefinitionDialog', Y.usp.app.AppDialog, [], {
    views: {
      view: {
        type: Y.app.admin.form.FormDefinitionViewView
      },
      edit: {
        type: Y.app.admin.form.FormDefinitionEditView,
        buttons: [{
          section: Y.WidgetStdMod.FOOTER,
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Save',
          classNames: 'pure-button-primary',
          action: '_handleSave',
          disabled: true
        }]
      },
      publish: {
        type: Y.app.admin.form.FormDefinitionPublishView,
        buttons: [{
          section: Y.WidgetStdMod.FOOTER,
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Publish',
          classNames: 'pure-button-primary',
          action: '_handleSave',
          disabled: true
        }]
      },
      archive: {
        type: Y.app.admin.form.FormDefinitionStatusChangeView,
        buttons: [{
          section: Y.WidgetStdMod.FOOTER,
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Delete',
          classNames: 'pure-button-primary',
          action: '_handleSave',
          disabled: true
        }]
      },
      remove: {
        type: Y.app.admin.form.FormDefinitionStatusChangeView,
        buttons: [{
          section: Y.WidgetStdMod.FOOTER,
          name: 'removeButton',
          labelHTML: '<i class="fa fa-check"></i> Remove',
          classNames: 'pure-button-primary',
          action: 'handleRemove',
          disabled: true
        }]
      }
    },
    _handleSave: function(e) {
      // Get the view and the model
      var view = this.get('activeView'),
      container = view.get('container'),
        attrs = {};
      //If we have a Form Definition Edit View then lets extract the sub-types
      if (view.name === 'formDefinitionEditView') {
        attrs['subTypes'] = view.getSubTypes();
      }
      this.handleSave(e, attrs, {
        transmogrify: Y.usp.form.UpdateFormDefinition
      });
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'app-dialog',
    'model-form-link',
    'model-transmogrify',
    'form-util',
    'handlebars-helpers',
    'usp-form-NewFormDefinition',
    'usp-form-FormDefinition',
    'usp-form-UpdateFormDefinition',
    'usp-formchecklist-FormDefinitionWithRelatedChecklist',
    'usp-formchecklist-FormDefinitionWithOutputTemplatesAndRelatedChecklist',
    'usp-form-FormDefinitionWithOutputTemplates',
    'categories-casenote-component-EntryType',
    'handlebars-form-templates',
    'handlebars-form-partials',
    'template-micro',
    'promise',
    'checklist-definition-autocomplete-view'
  ]
});