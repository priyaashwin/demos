YUI.add('form-output-document-dialog-views', function (Y) {
    var L = Y.Lang;

    Y.namespace('app.form').FormOutputDocumentView = Y.Base.create('formOutputDocumentView', Y.app.output.BaseOutputDocumentView, [], {
        template: Y.Handlebars.templates["formProduceDocumentDialog"],
        //override BaseOutputDocumentView's loadTemplates to get templates directly
        //from the model
        loadTemplates: function () {
            this.get('model').after('load', this._setOutputTemplates, this);
        },
        _setOutputTemplates: function () {
            //get the list of output templates out of the model
            var outputTemplates = this.get('model').get('outputTemplates').filter(function (template) {
                return template.status == 'PUBLISHED';
            }).sort(function (a, b) {
                return a.outputName.localeCompare(b.outputName, 'en', {
                    'sensitivity': 'base'
                });
            });

            //set into the local attribute
            this.set('outputTemplates', outputTemplates);
        },
        getDocumentRequestModel: function () {
            return new Y.usp.form.NewFormDocumentRequest({
                url: this.get('generateURL'),
                documentType: this.get('formDocumentType'),
                formInstanceId: this.get('formInstanceId'),
                groupMemberId: this.get('isGroupForm') ? Number(this.get('subjectId')) : null,
                outputTemplateId: Number(this.get('selectedTemplate')),
                outputFormat: this.get('selectedFormat'),
                subjectId: Number(this.get('subjectId')),
                subjectType: this.get('subjectType').toUpperCase()
            });
        }
    }, {
        ATTRS: {
            formInstanceId: {
                value: ''
            },
            isGroupForm: {
                value: false
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'output-document-dialog-views',
        'handlebars-helpers',
        'handlebars-form-templates',
        'handlebars-form-partials',
        'usp-form-NewFormDocumentRequest'
    ]
});