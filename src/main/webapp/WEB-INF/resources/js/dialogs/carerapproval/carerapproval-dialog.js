YUI.add('carerapproval-dialog-views', function (Y) {
    var O = Y.Object,
        A = Y.Array,
        E = Y.Escape,
        Micro = Y.Template.Micro,
        MicroSyntax = {
            code: /\{\{%([\s\S]+?)%\}\}/g,
            escapedOutput: /\{\{(?!%)([\s\S]+?)\}\}/g,
            rawOutput: /\{\{\{([\s\S]+?)\}\}\}/g
        },
        isEmpty = function (v) { return v === null || v === '' };

    var BaseInvestigationView = Y.Base.create('baseInvestigationView', Y.usp.View, [], {
        getFosterChildrenList: function () {
            var url = this.get('fosterChildRelationshipsUrl');
            return new Y.Promise(function (resolve, reject) {
                var model = new Y.usp.relationship.PaginatedPersonPersonRelationshipDetailsList({
                    url: url
                });

                model.load(function (err, res) {
                    if (!err) {
                        resolve(model);
                    } else {
                        reject(err);
                    }
                }.bind(this));
            });
        },
        render: function () {
            //call superclass render
            BaseInvestigationView.superclass.render.call(this);

            //call here so we know 100% render has occured before we load
            this.getFosterChildrenList().then(function (list) {
                //set local attribute
                this.set('fosterChildrenList', list.toJSON());
            }.bind(this));

            this._initAllegedAbuse();
            return this;
        },

        _initAllegedAbuse: function () {
            var codes = Y.secured.uspCategory.childlookedafter.fosterCarerUnderInvestigationAllegedAbuse.category.getActiveCodedEntries(null, 'write');
            var value = this.get('model').get('allegedAbuse');
            var selectNode = this.get('container').one('select[name="allegedAbuse"]');
            if (selectNode) {
                Y.FUtil.setSelectOptions(selectNode, O.values(codes), null, null, true, true, 'code');
                if (value) {
                    selectNode.set('value', value);
                }
            }
        }
    }, {
        ATTRS: {
            fosterChildrenList: {
                value: []
            }
        }
    });

    var validateNumber = function (e) {
        var figure = e.currentTarget.get('value');
        //Remove leading zero
        figure = parseInt(figure, 10);
        if (isNaN(figure)) {
            figure = '';
        }
        // Regex returns true if number between 0-9 or an empty string and it is 1-3 digits
        function checkFigure(figure) {
            var pattern = /^[0-9]{1,3}$|^$/;
            return pattern.test(figure);
        }

        // Keep trimming the figure until a valid digits are entered or there is nothing left to slice, Ie Accept only 1-3 digit numbers
        while (!checkFigure(figure)) {
            figure = figure.slice(0, -1);
        }

        // The figure is either an valid figure or a blank string
        //set the clean value back into the input
        e.currentTarget.set('value', figure);
    };

    var validateCarerDetails = function (attrs) {
        var errors = {},
            labels = this.labels || {},
            maxRelatedPlacements = attrs.maxRelatedPlacements,
            maxUnrelatedPlacements = attrs.maxUnrelatedPlacements,
            numberOfRooms = attrs.numberOfRooms,
            upperAgePermitted = attrs.upperAgePermitted,
            lowerAgePermitted = attrs.lowerAgePermitted,
            disabilityPermitted = attrs.disabilityPermitted,
            typeOfCareId = attrs.typeOfCareId,
            organisationId = attrs.organisationId,
            primaryCarer = attrs.primaryCarer;

        if (maxRelatedPlacements === null) {
            errors['maxRelatedPlacements'] = labels.maxRelatedPlacements;
        }
        if (maxUnrelatedPlacements === null) {
            errors['maxUnrelatedPlacements'] = labels.maxUnrelatedPlacements;
        }
        if (numberOfRooms === null) {
            errors['numberOfRooms'] = labels.numberOfRooms;
        }
 
        // Validate these 3 attributes only for FosterCarerApproval and not for AdoptiveCarerApproval
        if (attrs._type === 'NewFosterCarerApproval') { 	
        	if (upperAgePermitted === null) {
                errors['upperAgePermitted'] = labels.upperAgePermitted;
            }
            if (lowerAgePermitted === null) {
                errors['lowerAgePermitted'] = labels.lowerAgePermitted;
            }
            if (disabilityPermitted === null) {
                errors['disabilityPermitted'] = labels.disabilitySupport;
            }
        }
        if (typeOfCareId === null || typeOfCareId === "") {
            errors['typeOfCareId'] = labels.typeOfCareId;

        }
        if (organisationId === null || organisationId === "") { // We have to check for empty string rather than null value
            errors['organisationId'] = labels.organisationId;
        }
        if (primaryCarer === null) {
            errors['primaryCarer'] = labels.primaryCarer;
        }

        return errors;
    };

    var validateCarerSuspension = function (attrs) {
        var errors = {},
            labels = this.labels || {};

        if (isEmpty(attrs.startDate)) {
            errors['startDate'] = labels.startDateMandatory;
        }
        if (isEmpty(attrs.reason)) {
            errors['reason'] = labels.reasonMandatory;
        }
        if (isEmpty(attrs.placesUnavailable)) {
            errors['placesUnavailable'] = labels.placesUnavailableMandatory;
        } else if (isNaN(attrs.placesUnavailable) || attrs.placesUnavailable < 0) {
            errors['placesUnavailable'] = labels.placesUnavailableIsNumeric;
        }

        return errors;
    };

    Y.namespace('app.carerApproval').NewFosterCarerDetailsForm = Y.Base.create('newFosterCarerDetailsForm',
        Y.usp.childlookedafter.NewFosterCarerApproval, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerDetailsAddForm',
            customValidation: validateCarerDetails
        });

    Y.namespace('app.carerApproval').NewAdoptiveCarerDetailsForm = Y.Base.create('newAdoptiveCarerDetailsForm',
        Y.usp.childlookedafter.NewAdoptiveCarerApproval, [Y.usp.ModelFormLink], {
            form: '#carerDetailsAddAdoptiveCarerForm',
            customValidation: validateCarerDetails
        });

    Y.namespace('app.carerApproval').NewCarerSuspensionForm = Y.Base.create('newCarerSuspensionForm',
        Y.usp.childlookedafter.NewFosterCarerSuspension, [Y.usp.ModelFormLink], {
            form: '#carerSuspensionAddForm',
            customValidation: validateCarerSuspension
        });

    Y.namespace('app.carerApproval').UpdateCarerSuspensionForm = Y.Base.create('updateCarerSuspensionForm',
        Y.usp.childlookedafter.ChildCarerSuspension, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerSuspensionEditForm'
        });

    Y.namespace('app.carerApproval').NewCarerExemptionForm = Y.Base.create('newCarerExemptionForm',
        Y.usp.childlookedafter.NewFosterCarerExemption, [Y.usp.ModelFormLink], {
            form: '#carerExemptionsAddForm'
        });

    Y.namespace('app.carerApproval').UpdateCarerExemptionForm = Y.Base.create('updateCarerExemptionForm',
        Y.usp.childlookedafter.ChildCarerExemption, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerExemptionsEditForm'
        });

    //custom model to provide isTemporaryApproval attribute as boolean - required for View
    Y.namespace('app.carerApproval').FosterApproval = Y.Base.create('fosterApproval',
        Y.usp.childlookedafter.ChildCarerApproval, [], {}, {
            ATTRS: {
                isTemporaryApproval: {
                    getter: function (value) {
                        if (!value) {
                            return false
                        }
                        return value;
                    }
                }
            }
        });

    var UpdateFosterCarer = Y.Base.create('UpdateFosterCarer', Y.usp.childlookedafter.FosterCarerApproval, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {}, {
        ATTRS: {
            synchronisedWithPartner: {
                /**
                 * The synchronisedWithPartner is set to true
                 * if the synchronisedPartner object contains a valid
                 * PersonVO
                 * @param {Object} value
                 */
                getter: function (value) {
                    var personVO = this.get('synchronisedPartner');

                    if (personVO && personVO.id) {
                        //if there is a valid personVO - return true
                        return true
                    }
                    return false;
                },
                value: false
            }
        }
    });

    Y.namespace('app.carerApproval').UpdateCarerDetailsForm = Y.Base.create('updateCarerDetailsForm',
        UpdateFosterCarer, [], {
            form: '#carerDetailsEditForm'
        }, {
            ATTRS: {
                isTemporaryApproval: {
                    getter: function (value) {
                        if (!value) {
                            return false
                        }
                        return value;
                    }
                }
            }
        });

    Y.namespace('app.carerApproval').UpdateFosterCarerApprovalDataForm = Y.Base.create('updateFosterCarerApprovalDataForm',
        UpdateFosterCarer, [], {
            form: '#fosterCarerApprovalDataForm',
            customValidation: function (attrs) {
                var errors = {},
                    labels = this.labels || {};

                if (isEmpty(attrs.lowerAgePermitted)) {
                    errors['lowerAgePermitted'] = labels.lowerAgePermittedMandatory;
                } else if (isNaN(attrs.lowerAgePermitted) || parseInt(attrs.lowerAgePermitted, 10) < 0 || parseInt(attrs.lowerAgePermitted, 10) > 18) {
                    errors['lowerAgePermitted'] = labels.lowerAgePermittedIsNumeric;
                }

                if (isEmpty(attrs.upperAgePermitted)) {
                    errors['upperAgePermitted'] = labels.upperAgePermittedMandatory;
                } else if (isNaN(attrs.upperAgePermitted) || parseInt(attrs.upperAgePermitted, 10) < 1 || parseInt(attrs.upperAgePermitted, 10) > 25) {
                    errors['upperAgePermitted'] = labels.upperAgePermittedIsNumeric;
                }

                if (isEmpty(attrs.maxRelatedPlacements)) {
                    errors['maxRelatedPlacements'] = labels.maxRelatedPlacementsMandatory;
                } else if (isNaN(attrs.maxRelatedPlacements) || parseInt(attrs.maxRelatedPlacements, 10) < 0 || parseInt(attrs.maxRelatedPlacements, 10) > 6) {
                    errors['maxRelatedPlacements'] = labels.maxRelatedPlacementsIsNumeric;
                }

                if (isEmpty(attrs.maxUnrelatedPlacements)) {
                    errors['maxUnrelatedPlacements'] = labels.maxUnrelatedPlacementsMandatory;
                } else if (isNaN(attrs.maxUnrelatedPlacements) || parseInt(attrs.maxUnrelatedPlacements, 10) < 1 || parseInt(attrs.maxUnrelatedPlacements, 10) > 3) {
                    errors['maxUnrelatedPlacements'] = labels.maxUnrelatedPlacementsIsNumeric;
                }

                return errors;
            }
        });

    Y.namespace('app.carerApproval').RemoveCarerApprovalForm = Y.Base.create('removeCarerApprovalForm',
        Y.usp.childlookedafter.FosterCarerApproval, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerApprovalRemoveForm'
        });

    Y.namespace('app.carerApproval').UpdateAdoptiveCarerDetailsForm = Y.Base.create('updateAdoptiveCarerDetailsForm',
        Y.usp.childlookedafter.AdoptiveCarerApproval, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerDetailsEditAdoptiveCarerForm'
        });

    Y.namespace('app.carerApproval').DeleteCarerExemptionForm = Y.Base.create('deleteCarerExemptionForm',
        Y.usp.childlookedafter.ChildCarerExemption, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerExemptionDeleteForm'
        });

    Y.namespace('app.carerApproval').DeleteChildCarerTrainingForm = Y.Base.create('deleteChildCarerTrainingForm',
        Y.usp.childlookedafter.ChildCarerTraining, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#childCarerTrainingDeleteForm'
        });

    var partnerTrainingDateSetter = function (value) {
        var parsed = null;

        if (typeof value === 'number') {
            return value;
        }

        try {
            parsed = Y.USPDate._parseDate(value);
            if (!Y.Lang.isNull(parsed)) {
                return parsed.getTime();
            }
        } catch (e) {
            Y.log("Unhandled Date Value set on partner training: " + value);
        }
        return parsed;
    };

    Y.namespace('app.carerApproval').NewCarerTrainingForm = Y.Base.create('newCarerTrainingForm',
        Y.usp.childlookedafter.NewFosterCarerTraining, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerTrainingAddForm'
        }, {
            ATTRS: {
                applyToPartner: {
                    value: 'false'
                },
                partnerTrainingStartDate: {
                    setter: partnerTrainingDateSetter,
                    getter: function (val) {
                        if (this.get('applyToPartner') === 'true') {
                            if (val === undefined || val === null || val === '') {
                                return this.get('startDate');
                            }
                        }
                        return val;
                    },
                    USPType: 'Date'
                },
                partnerTrainingEndDate: {
                    setter: partnerTrainingDateSetter,
                    getter: function (val) {
                        if (this.get('applyToPartner') === 'true') {
                            if (val === undefined || val === null || val === '') {
                                return this.get('endDate');
                            }
                        }
                        return val;
                    },
                    USPType: 'Date'
                }
            }
        });

    Y.namespace('app.carerApproval').NewFosterCarerTSDStandardsForm = Y.Base.create('newFosterCarerTSDStandardsForm',
        Y.usp.childlookedafter.NewFosterCarerTSDStandards, [Y.usp.ModelFormLink], {
            form: '#fosterCarerTSDAddForm'
        });

    Y.namespace('app.carerApproval').NewCarerUnderInvestigationForm = Y.Base.create('newCarerUnderInvestigationForm',
        Y.usp.childlookedafter.NewFosterCarerUnderInvestigation, [Y.usp.ModelFormLink], {
            form: '#carerUnderInvestigationAddForm'
        });

    Y.namespace('app.carerApproval').UpdateCarerUnderInvestigationForm = Y.Base.create('updateCarerUnderInvestigationForm',
        Y.usp.childlookedafter.FosterCarerUnderInvestigation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerUnderInvestigationEditForm'
        });

    Y.namespace('app.carerApproval').EndCarerUnderInvestigationForm = Y.Base.create('endCarerUnderInvestigationForm',
        Y.usp.childlookedafter.FosterCarerUnderInvestigation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#carerUnderInvestigationEndForm'
        });

    Y.namespace('app.carerApproval').NewSpecificChildApprovalForm = Y.Base.create('newSpecificChildApprovalForm',
        Y.usp.childlookedafter.NewSpecificChildApproval, [Y.usp.ModelFormLink], {
            form: '#specificChildApprovalAddform'
        });

    Y.namespace('app.carerApproval').UpdateEndFosterCarerRegistrationForm = Y.Base.create('updateEndFosterCarerRegistration',
        Y.usp.childlookedafter.UpdateEndFosterCarerRegistration, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#fosterCarerRegistrationEndForm'
        });

    Y.namespace('app.carerApproval').UpdateFosterCarerRegistrationForm = Y.Base.create('updateFosterCarerRegistration',
        Y.usp.childlookedafter.FosterCarerRegistration, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            customValidation: function (attrs) {
                var errors = {},
                    labels = this.labels || {};

                if (isEmpty(attrs.startDate)) {
                    errors['startDate'] = labels.startDateMandatory;
                }
                if (isEmpty(attrs.maxAvailablePlaces)) {
                    errors['maxAvailablePlaces'] = labels.maxAvailablePlacesMandatory;
                } else if (isNaN(attrs.maxAvailablePlaces) || parseInt(attrs.maxAvailablePlaces, 10) <= 0) {
                    errors['maxAvailablePlaces'] = labels.maxAvailablePlacesIsNumeric;
                }

                if (isEmpty(attrs.maxAvailableRooms)) {
                    errors['maxAvailableRooms'] = labels.maxAvailableRoomsMandatory;
                } else if (isNaN(attrs.maxAvailableRooms) || parseInt(attrs.maxAvailableRooms, 10) <= 0) {
                    errors['maxAvailableRooms'] = labels.maxAvailableRoomsIsNumeric;
                }
                if (isEmpty(attrs.primaryCarer)) {
                    errors['primaryCarer'] = labels.primaryCarer;
                }
                return errors;
            },
            form: '#fosterCarerRegistrationEditForm'
        });

    Y.namespace('app.carerApproval').AddFosterApproval = Y.Base.create('addFosterApproval', Y.usp.View, [], {
        events: {
            '#lowerAgePermitted': {
                keyup: validateNumber
            },
            '#upperAgePermitted': {
                keyup: validateNumber
            },
            '#maxRelatedPlacements': {
                keyup: validateNumber
            },
            '#maxUnrelatedPlacements': {
                keyup: validateNumber
            },
            '#numberOfRooms': {
                keyup: validateNumber
            },
            'input[name="disabilityPermitted"]': {
                change: '_handleDisabilityPermittedChange'
            },
            'select[name="typeOfCareId"]': {
                change: '_handleTypeOfCareIdInputChange'
            }
        },
        template: Y.Handlebars.templates.carerapprovalAddCarerDetails,

        initializer: function (config) {
            var autocompleteConfig = config.autocomplete || {};
            var autocompleteURL = autocompleteConfig.autocompleteURL;
            var autocompletePlaceholder = autocompleteConfig.labels.placeholder;

            var currentOrganisation = this.get('otherData.organisation');

            this._evtHandlers = [];

            //organisation auto-complete is only enabled if there is no organisation
            if (!currentOrganisation) {
                this.organisationAutoCompleteView = new Y.app.OrganisationAutoCompleteResultView({
                    autocompleteURL: autocompleteURL,
                    inputName: 'organisationId',
                    formName: 'carerDetailsAddForm',
                    labels: {
                        placeholder: autocompletePlaceholder
                    }
                }).addTarget(this);

                this._evtHandlers.push(
                    this.organisationAutoCompleteView.on('*:select', function (e) {
                        this.fire("setNarrative", {
                            action: "set",
                            selectedOrganisation: e.result.raw
                        });
                    }));
                this._evtHandlers.push(
                    this.after('*:selectedOrganisationChange', function (e) {
                        if (e.newVal === null) {
                            this.fire("setNarrative", {
                                action: "reset"
                            });
                        }
                    })
                );
            }

            this.startDateCalendar = new Y.usp.CalendarPopup({
                'maximumDate': new Date()
            });
        },
        render: function () {
            var container = this.get('container'),
                otherData = this.get('otherData');

            //call superclass render
            Y.app.carerApproval.AddFosterApproval.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]')
            }).render();

            Y.FUtil.setSelectOptions(container.one('select[name="typeOfCareId"]'), O.values(Y.secured.uspCategory.childlookedafter.typeOfCare.category.getActiveCodedEntries(null, 'write')), null, null, true, true, 'id');

            Y.FUtil.setSelectOptions(container.one('select[name="disabilityTypes"]'), O.values(Y.uspCategory.childlookedafter.typeOfDisability.category.getActiveCodedEntries()), null, null, false, true, 'code');

            if (this.organisationAutoCompleteView) {
                container.one('#organisationSubjectSearchWrapper').append(this.organisationAutoCompleteView.render().get('container'));
            }

            var canLinkPartner = this.validatePartner(otherData);

            this.hideIsTempApprovalCheckBox = function () {
                this.get('container').one('#isTemporaryApproval-wrapper')
                    .setAttribute("style", "display:none");
                this.get('container').one('input[name="isTemporaryApproval"]')
                    .setAttribute('disabled', 'disabled');
            };

            this.showIsTempApprovalCheckBox = function () {
                this.get('container').one('#isTemporaryApproval-wrapper')
                    .setAttribute("style", "display:block");
                this.get('container').one('input[name="isTemporaryApproval"]')
                    .removeAttribute('disabled');
            };
            
            var fosterCarerRegistrationPrimaryCarer = otherData.fosterCarerRegistrationPrimaryCarer;
            var fosterCarerRegistrationId =   otherData.currentFosterCarerRegistrationId;

            if(fosterCarerRegistrationId === null || fosterCarerRegistrationId == undefined){
            	container.one('#primaryCarerYes').setAttribute('checked', 'true');
             }
            
            if (!canLinkPartner && (fosterCarerRegistrationId === null || fosterCarerRegistrationId == undefined)) {
                container.one('#synchroniseWithPartner').setAttribute('disabled', 'disabled');
                container.one('#primaryCarerYes').setAttribute('checked', 'true');
                container.one('#primaryCarer').setAttribute('disabled', 'disabled');
            }
            return this;
        },
        validatePartner: function (otherData) {
            var clientOrganisationId = (otherData.organisation) ? otherData.organisation.id : null,
                partnerOrganisationId = (otherData.subject) ? otherData.subject.activePartnerOrganisationId : null;

            if (!otherData.subject.hasActivePartner) {
                return false;
            } else if ((null == partnerOrganisationId) || (null == clientOrganisationId)) {
                return true;
            } else if (partnerOrganisationId != clientOrganisationId) {
                return false;
            }
            return true;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handle) {
                handle.detach();
            });

            delete this._evtHandlers;

            if (this.organisationAutoCompleteView) {
                this.organisationAutoCompleteView.removeTarget();
                this.organisationAutoCompleteView.destroy();
                delete this.organisationAutoCompleteView;
            }

            this.startDateCalendar.destroy();
            delete this.startDateCalendar;
        },
        _handleDisabilityPermittedChange: function (e) {
            var typeOfDisabilityWrapper = this.get('container').one('#typeOfDisability-wrapper');

            if (e.target.get('value') === 'true') {
                typeOfDisabilityWrapper.setStyle('display', 'block');
            } else {
                typeOfDisabilityWrapper.setStyle('display', 'none');
                this.get('container').one('#typeOfDisability-input').set('value', '');
            }
        },
        _handleTypeOfCareIdInputChange: function (e) {
            var container = this.get('container'),
                fosterCarerApprovalContext = this.get('fosterCarerApprovalContext'),
                typeOfCareCodedEntry = Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries,
                selectedTypeOfCare = (container.one("#typeOfCareId-input option:checked")) ? true : false,
                selectedTypeOfCareId,
                selectedTypeOfCareCode;

            if (selectedTypeOfCare) {
                selectedTypeOfCareId = container.one("#typeOfCareId-input option:checked").get('value')
            };

            Object.keys(Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries).forEach(function (key) {
                var codedEntry = Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries[key];
                if ("" + codedEntry.id === "" + selectedTypeOfCareId) {
                    selectedTypeOfCareCode = codedEntry.code
                }
            })

            if (selectedTypeOfCare) {
                if ((selectedTypeOfCareCode === 'FAMILY_FRIEND' && fosterCarerApprovalContext === 'DEFAULT') ?
                    this.showIsTempApprovalCheckBox() : this.hideIsTempApprovalCheckBox());
            }
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    typesOfCare: Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries //com.olmgroup.usp.components.childlookedafter.category.typeOfCare
                }
            }
        }
    });

    Y.namespace('app.carerApproval').EndFosterCarerRegistrationErrorView = Y.Base.create('endFosterCarerRegistrationErrorView', Y.usp.View, [], {
        template: Y.Handlebars.templates.fosterCarerRegistrationEndErrorDialog,
        initializer: function (config) {
            var data = config.model.toJSON();
            var registerDependancies = ['approvalsCount', 'exemptionCount', 'underInvestigationCount', 'suspensionCount'];

            var errors = registerDependancies.filter(function (record) {
                return data[record] > 0;
            }).map(function (record) {
                var count = data[record];
                return {
                    error: Micro.render(config.labels[record] || '', {
                        singular: count === 1,
                        count: count
                    }, MicroSyntax)
                };
            });

            this.get('model').set('errors', errors);
        },
        render: function () {
            this.constructor.superclass.render.call(this);
            return this;
        }
    });

    Y.namespace('app.carerApproval').EndFosterCarerRegistrationFormView = Y.Base.create('endFosterCarerRegistrationFormView', Y.usp.View, [], {
        template: Y.Handlebars.templates.fosterCarerRegistrationEndDialog,
        initializer: function (config) {
            this.endDateCalendar = new Y.usp.CalendarPopup();
        },
        destructor: function () {
            this.endDateCalendar.destroy();
            delete this.endDateCalendar;
        },
        render: function () {
            var container = this.get('container');
            model = this.get('model');
            this.constructor.superclass.render.call(this);

            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]'),
                minimumDate: model.get('startDate')
            }).render();

            this._initReasons();
            return this;
        },
        _initReasons: function () {
            var codes = Y.uspCategory.childlookedafter.deregistrationReason.category.getActiveCodedEntries();
            var value = this.get('model').get('endReason');
            var selectNode = this.get('container').one('#endReason');
            if (selectNode) {
                Y.FUtil.setSelectOptions(selectNode, O.values(codes), null, null, true, true, 'code');
                if (value) {
                    selectNode.set('value', value);
                }
            }
        }
    });

    Y.namespace('app.carerApproval').EditFosterCarerRegistrationView = Y.Base.create('editFosterCarerRegistrationView', Y.usp.View, [], {
        template: Y.Handlebars.templates.fosterCarerRegistrationEditDialog,
        initializer: function (config) {
            this.startDateCalendar = new Y.usp.CalendarPopup();
        },
        destructor: function () {
            this.startDateCalendar.destroy();
            delete this.startDateCalendar;
        },
        render: function () {
            var container = this.get('container');

            this.constructor.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]'),
            }).render();

            return this;
        }
    });

    Y.namespace('app.carerApproval').AddAdoptiveApproval = Y.Base.create('addAdoptiveApproval', Y.usp.View, [], {
        events: {
            '#lowerAgePermitted': {
                keyup: validateNumber
            },
            '#upperAgePermitted': {
                keyup: validateNumber
            }
        },
        template: Y.Handlebars.templates.carerapprovalAddAdoptiveCarerDetails,
        initializer: function (config) {
            var model = this.get('model'),
                autocompleteChildConfig = config.autocompleteChild || {};
            var autocompleteConfig = config.autocomplete || {};
            var autocompleteURL = autocompleteConfig.autocompleteURL;
            var autocompletePlaceholder = autocompleteConfig.labels.placeholder;
            this.organisationAutoCompleteView = new Y.app.OrganisationAutoCompleteResultView({
                autocompleteURL: autocompleteURL,
                inputName: 'organisationId',
                formName: 'carerDetailsAddAdoptiveCarerForm',
                labels: {
                    placeholder: autocompletePlaceholder
                }
            }).addTarget(this);

            this.childListView = new Y.app.adoptive.ChildListView({
                labels: autocompleteChildConfig.labels,
                personAutocompleteURL: autocompleteChildConfig.autocompleteURL,
                adoptiveChildAssociations: model.get('adoptiveChildAssociations')
            }).addTarget(this);

            this.startDateCalendar = new Y.usp.CalendarPopup({
                'maximumDate': new Date()
            });

            this.addAdoptiveApprovalSubview = new Y.app.carerApproval.AddAdoptiveApprovalSubview(config).addTarget(this);

            this._evtHandlers = [
              this.childListView.after('*:adoptiveChildAssociationsChange', this.handleAdoptiveChildAssociationsChange, this)
            ];
        },
        render: function () {
            var container = this.get('container');

            //call superclass render
            Y.app.carerApproval.AddAdoptiveApproval.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]')
            }).render();

            container.one('#organisationSubjectSearchWrapper').append(this.organisationAutoCompleteView.render().get('container'));
            container.one('#adoptiveChildView').append(this.childListView.render().get('container'));

            if (this.addAdoptiveApprovalSubview) {
                var contentNode = Y.one(Y.config.doc.createDocumentFragment());
                contentNode.append(this.addAdoptiveApprovalSubview.render().get('container'));

                this.get('container').one('#contextualSubviewWrapper').setHTML(contentNode);
            }

            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function(handler) {
              handler.detach();
            });

            delete this._evtHandlers;

            if (this.addAdoptiveApprovalSubview) {
                this.addAdoptiveApprovalSubview.removeTarget(this);
                this.addAdoptiveApprovalSubview.destroy();
                delete this.addAdoptiveApprovalSubview;
            }

            this.startDateCalendar.destroy();
            delete this.startDateCalendar;

            this.organisationAutoCompleteView.removeTarget(this);
            this.organisationAutoCompleteView.destroy();
            delete this.organisationAutoCompleteView;

            this.childListView.removeTarget(this);
            this.childListView.destroy();
            delete this.childListView;
        },
        handleAdoptiveChildAssociationsChange: function(e) {
            var model = this.get('model'),
                adoptiveChild = [];

            if (e.newVal) {
                adoptiveChild = e.newVal.map(function(entry) {
                    return ({
                      personId: entry.id,
                      _type: "NewAdoptiveChildAssociation"
                    });
                });

                model.setAttrs({
                    adoptiveChildAssociations: adoptiveChild
                }, {silent: true});
            }
        }
    });

    Y.namespace('app.carerApproval').AddAdoptiveApprovalSubview = Y.Base.create('addAdoptiveApprovalSubview', Y.usp.View, [], {
        template: Y.Handlebars.templates.carerApprovalAddAdoptiveCarerDetailsSubview
    });

    Y.namespace('app.carerApproval').UpdateAdoptiveApproval = Y.Base.create('updateAdoptiveApproval',
        Y.usp.childlookedafter.ChildCarerApprovalView, [], {
            events: {
                'input[name="endDate"]': {
                    change: 'setEndDate',
                    dateChange: 'setEndDate'
                },
                '#endReason': {
                    change: 'handleEndReasonChange'
                }
            },
            template: Y.Handlebars.templates.carerapprovalUpdateAdoptiveCarerDetails,
            initializer: function (config) {
                var autocompleteChildConfig = config.autocompleteChild || {};

                this.childListView = new Y.app.adoptive.ChildListView({
                    labels: autocompleteChildConfig.labels,
                    personAutocompleteURL: autocompleteChildConfig.autocompleteURL
                }).addTarget(this);

                this.endDateCalendar = new Y.usp.CalendarPopup();

                if (config.adopterApprovalContext === 'RAA') {
                    this.raaUpdateAdoptiveApprovalSubview = new Y.app.carerApproval.RaaUpdateAdoptiveApprovalSubview(config).addTarget(this);
                } else {
                    this.carerUpdateAdoptiveApprovalSubview = new Y.app.carerApproval.CarerUpdateAdoptiveApprovalSubview(config).addTarget(this);
                }

                this._evtHandlers = [
                    this.get('model').after('load', this.handleModelLoad, this),
                    this.after('endDateChange', this.handleEndDateChange, this),
                    this.endDateCalendar.after('calendarPopup:dateSelection', this.setEndDate, this),
                    this.childListView.after('*:adoptiveChildAssociationsChange', this.handleAdoptiveChildAssociationsChange, this)
                ]
            },

            handleModelLoad: function () {
                this.set('endDate', this.get('model').get('endDate'));
                this.set('endReason', this.get('model').get('endReason'));
                this.childListView.set('adoptiveChildAssociations', this.get('model').get('adoptiveChild'));
            },

            render: function () {
                var container = this.get('container'),
                    model = this.get('model');

                //call superclass render
                Y.app.carerApproval.UpdateAdoptiveApproval.superclass.render.call(this);

                this.endDateCalendar.setAttrs({
                    'inputNode': container.one('input[name="endDate"]'),
                    'minimumDate': model.get('startDate')
                }).render();

                container.one('#adoptiveChildView').append(this.childListView.render().get('container'));

                if (model.get('endDate') === null) {
                    //Hide child placed radio buttons
                    container.one('.childBeenPlacedWrapper').hide();
                }

                Y.FUtil.setSelectOptions(container.one('select[name="endReason"]'), O.values(Y.uspCategory.childlookedafter.adopterApprovalEndReason.category.getActiveCodedEntries()), null, null, true, true, 'code');

                if (this.raaUpdateAdoptiveApprovalSubview) {
                    var contentNode = Y.one(Y.config.doc.createDocumentFragment());
                    //will render on model update
                    contentNode.append(this.raaUpdateAdoptiveApprovalSubview.get('container'));

                    this.get('container').one('#contextualSubviewWrapper').setHTML(contentNode);
                }

                if (this.carerUpdateAdoptiveApprovalSubview) {
                    var contentNode = Y.one(Y.config.doc.createDocumentFragment());
                    //will render on model update
                    contentNode.append(this.carerUpdateAdoptiveApprovalSubview.get('container'));
                    this.get('container').one('#contextualSubviewWrapper').setHTML(contentNode);
                }

                return this;
            },
            setEndDate: function (e) {
                this.set('endDate', e.currentTarget.get('value'));
            },
            handleEndReasonChange: function (e) {
                if (this.raaUpdateAdoptiveApprovalSubview) {
                    this.raaUpdateAdoptiveApprovalSubview.set('endReason', e.currentTarget.get('value'))
                }
                if (this.carerUpdateAdoptiveApprovalSubview) {
                    this.carerUpdateAdoptiveApprovalSubview.set('endReason', e.currentTarget.get('value'))
                }
            },
            handleEndDateChange: function (e) {
                var container = this.get('container');
                var node = container.one('.childBeenPlacedWrapper');

                if (this.get('endDate') === '') {
                    node.hide();
                } else {
                    node.show();
                }
            },
            handleAdoptiveChildAssociationsChange: function(e) {
                var model = this.get('model'),
                    adoptiveChild = [];

                if (e.newVal) {
                    adoptiveChild = e.newVal.map(function(entry) {
                        return ({
                          personId: entry.id,
                          _type: "NewAdoptiveChildAssociation"
                        });
                    });

                    model.setAttrs({
                        adoptiveChild: adoptiveChild
                    }, {silent: true});
                }
            },
            destructor: function () {
                this._evtHandlers.forEach(function (handler) {
                    handler.detach();
                });
                delete this._evtHandlers;

                if (this.carerUpdateAdoptiveApprovalSubview) {
                    this.carerUpdateAdoptiveApprovalSubview.removeTarget(this);
                    this.carerUpdateAdoptiveApprovalSubview.destroy();
                    delete this.carerUpdateAdoptiveApprovalSubview;
                }

                if (this.raaUpdateAdoptiveApprovalSubview) {
                    this.raaUpdateAdoptiveApprovalSubview.removeTarget(this);
                    this.raaUpdateAdoptiveApprovalSubview.destroy();
                    delete this.raaUpdateAdoptiveApprovalSubview;
                }

                this.endDateCalendar.destroy();
                delete this.endDateCalendar;

                this.childListView.removeTarget(this);
                this.childListView.destroy();
                delete this.childListView;
            }
        }, {
            ATTRS: {
                endDate: {
                    value: null
                },
                enumTypes: {
                    value: {
                        gender: Y.usp.childlookedafter.enum.GenderSelection.values
                    }
                }
            }
        });

    Y.namespace('app.carerApproval').RaaUpdateAdoptiveApprovalSubview = Y.Base.create('raaUpdateAdoptiveApprovalSubview', Y.usp.View, [], {
        events: {
            'input[name="adoptionType"]': {
                'change': 'setAdoptionType'
            }
        },
        template: Y.Handlebars.templates.raaCarerApprovalUpdateAdoptiveCarerDetailsSubview,
        initializer: function (config) {
            var autocompleteConfig = config.autocomplete || {};
            var autocompleteURL = autocompleteConfig.autocompleteURL;
            var autocompletePlaceholder = autocompleteConfig.labels.placeholder;
            this.organisationAutoCompleteView = new Y.app.OrganisationAutoCompleteResultView({
                autocompleteURL: autocompleteURL,
                inputName: 'childLocalAuthority',
                formName: 'carerDetailsUpdateAdoptiveCarerForm',
                labels: {
                    placeholder: autocompletePlaceholder
                }
            }).addTarget(this);

            this.dateMatchedCalendar = new Y.usp.CalendarPopup();
            this.dateChildPlacedCalendar = new Y.usp.CalendarPopup();
            this.dateOrderGrantedCalendar = new Y.usp.CalendarPopup();

            this._evtHandlers = [
                this.get('model').after('load', this.handleModelLoad, this),
                this.after('adoptionTypeChange', this.handleAdoptionTypeChange, this),
                this.after('endReasonChange', this.handleEndReasonChange, this),
            ]
        },

        handleModelLoad: function () {
            this.set('adoptionType', this.get('model').get('adoptionType'));
        },

        render: function () {
            var container = this.get('container');

            //call superclass render
            Y.app.carerApproval.RaaUpdateAdoptiveApprovalSubview.superclass.render.call(this);

            this.dateMatchedCalendar.setAttrs({
                inputNode: container.one('#dateMatched')
            }).render();

            this.dateChildPlacedCalendar.setAttrs({
                inputNode: container.one('#dateChildPlaced')
            }).render();

            this.dateOrderGrantedCalendar.setAttrs({
                inputNode: container.one('#dateOrderGranted')
            }).render();

            container.one('#childLocalAuthoritySearchWrapper').append(this.organisationAutoCompleteView.render().get('container'));

            this._setOriginallyFosteringChild();
            this._setFosteringForAdoption();
            this._setChildLocalAuthority();

            return this;
        },

        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            if (this.organisationAutoCompleteView) {
                this.organisationAutoCompleteView.removeTarget(this);
                this.organisationAutoCompleteView.destroy();
                delete this.organisationAutoCompleteView;
            }

            if (this.dateMatchedCalendar) {
                this.dateMatchedCalendar.destroy();
                delete this.dateMatchedCalendar;
            }

            if (this.dateChildPlacedCalendar) {
                this.dateChildPlacedCalendar.destroy();
                delete this.dateChildPlacedCalendar;
            }

            if (this.dateOrderGrantedCalendar) {
                this.dateOrderGrantedCalendar.destroy();
                delete this.dateOrderGrantedCalendar;
            }
        },

        handleAdoptionTypeChange: function (e) {
            this._setAdoptionTypeContent();
            this._resetHiddenFieldValues();
        },

        handleEndReasonChange: function (e) {
            this._setMandatoryMarkers();
        },

        setAdoptionType: function (e) {
            this.set('adoptionType', e.currentTarget.get('value'));
        },

        _setOriginallyFosteringChild: function () {
            var container = this.get('container');
            var originallyFosteringChildNode = container.one('input[name=originallyFosteringChild]');

            if (originallyFosteringChildNode) {
                if (this.get('model').get('originallyFosteringChild') === true) {
                    container.one('#originallyFosteringChildYes').set('checked', 'checked');
                } else if (this.get('model').get('originallyFosteringChild') === false) {
                    container.one('#originallyFosteringChildNo').set('checked', 'checked');
                }
            }
        },

        _setFosteringForAdoption: function () {
            var container = this.get('container');
            var fosteringForAdoptionNode = container.one('input[name=fosteringForAdoption]');

            if (fosteringForAdoptionNode) {
                if (this.get('model').get('fosteringForAdoption') === true) {
                    container.one('#fosteringForAdoptionYes').set('checked', 'checked');
                } else if (this.get('model').get('fosteringForAdoption') === false) {
                    container.one('#fosteringForAdoptionNo').set('checked', 'checked');
                }
            }
        },

        _setAdoptionTypeContent: function () {
            var container = this.get('container');
            if (this.get('adoptionType') === 'INTER') {
                container.one('#adoptionTypeInterCountry').set('checked', 'checked');
                container.one('#adoptionSubHeading').setHTML('<h4>' + this.get('labels').interCountryAdoptionSubheading + '</h4>');
                container.all('.domesticAdoptionFields').setStyle('display', 'none');
            } else if (this.get('adoptionType') === 'DOM') {
                container.one('#adoptionTypeDomestic').set('checked', 'checked');
                container.one('#adoptionSubHeading').setHTML('<h4>' + this.get('labels').domesticAdoptionSubheading + '</h4>');
                container.all('.domesticAdoptionFields').setStyle('display', 'block');
            }
        },

        _setChildLocalAuthority: function () {
            this.organisationAutoCompleteView.set('selectedOrganisation', this.get('model').get('childLocalAuthority'));
        },

        _setMandatoryMarkers: function () {
            var container = this.get('container');

            if (this.get('endReason') === 'ADOPTER_SUCCESS') {
                container.all('.adoptionTypeMandatoryField').setStyle('display', 'inline');
            } else {
                container.all('.adoptionTypeMandatoryField').setStyle('display', 'none');
            }
        },

        _resetHiddenFieldValues: function () {
            var container = this.get('container');

            //clear the values from the now hidden fields
            if (this.get('adoptionType') === 'INTER') {
                container.all('input[name="originallyFosteringChild"]').set('checked', '');
                container.all('input[name="fosteringForAdoption"]').set('checked', '');
                this.organisationAutoCompleteView.set('selectedOrganisation', null);
            }
        }
    }, {
        ATTRS: {
            endReason: {
                value: null
            }
        }
    });

    Y.namespace('app.carerApproval').CarerUpdateAdoptiveApprovalSubview = Y.Base.create('carerUpdateAdoptiveApprovalSubview', Y.usp.View, [], {
        events: {
            'input[name="adoptionType"]': {
                'change': 'setAdoptionType'
            }
        },
        template: Y.Handlebars.templates.carerApprovalUpdateAdoptiveCarerDetailsSubview,
        initializer: function (config) {
            this.dateMatchedCalendar = new Y.usp.CalendarPopup();
            this.dateChildPlacedCalendar = new Y.usp.CalendarPopup();
            this.dateOrderGrantedCalendar = new Y.usp.CalendarPopup();

            this._evtHandlers = [
                this.get('model').after('load', this.handleModelLoad, this),
                this.after('adoptionTypeChange', this.handleAdoptionTypeChange, this),
                this.after('endReasonChange', this.handleEndReasonChange, this),
            ]
        },

        handleModelLoad: function () {
            this.set('adoptionType', this.get('model').get('adoptionType'));
        },

        render: function () {
            var container = this.get('container');

            //call superclass render
            Y.app.carerApproval.CarerUpdateAdoptiveApprovalSubview.superclass.render.call(this);

            this.dateMatchedCalendar.setAttrs({
                inputNode: container.one('#dateMatched')
            }).render();

            this.dateChildPlacedCalendar.setAttrs({
                inputNode: container.one('#dateChildPlaced')
            }).render();

            this.dateOrderGrantedCalendar.setAttrs({
                inputNode: container.one('#dateOrderGranted')
            }).render();

            return this;
        },

        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            if (this.dateMatchedCalendar) {
                this.dateMatchedCalendar.destroy();
                delete this.dateMatchedCalendar;
            }

            if (this.dateChildPlacedCalendar) {
                this.dateChildPlacedCalendar.destroy();
                delete this.dateChildPlacedCalendar;
            }

            if (this.dateOrderGrantedCalendar) {
                this.dateOrderGrantedCalendar.destroy();
                delete this.dateOrderGrantedCalendar;
            }
        },

        handleAdoptionTypeChange: function (e) {
            this._setCarerApprovalAdoptionTypeContent();
        },

        handleEndReasonChange: function (e) {
            this._setMandatoryMarkers();
        },

        setAdoptionType: function (e) {
            this.set('adoptionType', e.currentTarget.get('value'));
        },

        _setCarerApprovalAdoptionTypeContent: function () {
            var container = this.get('container');
            if (this.get('adoptionType') === 'INTER') {
                container.one('#adoptionTypeInterCountry').set('checked', 'checked');
                container.one('#adoptionSubHeading').setHTML('<h4>' + this.get('labels').interCountryAdoptionSubheading + '</h4>');
                container.all('.interCountryAdoptionFields').setStyle('display', 'block');
            } else if (this.get('adoptionType') === 'DOM') {
                container.one('#adoptionTypeDomestic').set('checked', 'checked');
                container.all('.interCountryAdoptionFields').setStyle('display', 'none');
            }
        },

        _setMandatoryMarkers: function () {
            var container = this.get('container');
            if (this.get('endReason') === 'ADOPTER_SUCCESS') {
                container.all('.adoptionTypeMandatoryField').setStyle('display', 'inline');
            } else {
                container.all('.adoptionTypeMandatoryField').setStyle('display', 'none');
            }
        }

    }, {
        ATTRS: {
            endReason: {
                value: null
            }
        }
    });

    Y.namespace('app.carerApproval').ViewFosterApprovalView = Y.Base.create('viewFosterApprovalView', Y.usp.View, [], {
        template: Y.Handlebars.templates.carerDetailsViewDialog,
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    typesOfCare: Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries, //com.olmgroup.usp.components.childlookedafter.category.typeOfCare
                    typeOfDisability: Y.uspCategory.childlookedafter.typeOfDisability.category.codedEntries //com.olmgroup.usp.components.childlookedafter.category.typeOfDisability
                }
            },
            enumTypes: {
                value: {
                    gender: Y.usp.childlookedafter.enum.GenderSelection.values
                }
            }
        }
    });

    Y.namespace('app.carerApproval').ViewAdoptiveApproval = Y.Base.create('viewAdoptiveApproval', Y.usp.childlookedafter.ChildCarerApprovalView, [], {
        template: Y.Handlebars.templates.carerapprovalViewAdoptiveCarerDetails,
        initializer: function (config) {
            var labels = config.addChildLabels || {};

            this.childListView = new Y.app.adoptive.ChildListView({
                labels: labels,
            }).addTarget(this);

            if (config.adopterApprovalContext === 'RAA') {
                this.raaViewAdoptiveApprovalSubview = new Y.app.carerApproval.RaaViewAdoptiveApprovalSubview(config).addTarget(this);
            } else {
                this.carerViewAdoptiveApprovalSubview = new Y.app.carerApproval.CarerViewAdoptiveApprovalSubview(config).addTarget(this);
            }

            this._evtHandlers = [
                this.get('model').after('load', this.handleModelLoad, this)
            ];
        },
        handleModelLoad: function () {
            this.childListView.set('adoptiveChildAssociations', this.get('model').get('adoptiveChild'));
        },
        render: function () {
            Y.app.carerApproval.ViewAdoptiveApproval.superclass.render.call(this);

            this.get('container').one('#adoptiveChildView').append(this.childListView.render().get('container'));

            if (this.raaViewAdoptiveApprovalSubview) {
                var contentNode = Y.one(Y.config.doc.createDocumentFragment());
                contentNode.append(this.raaViewAdoptiveApprovalSubview.get('container'));

                this.get('container').one('#contextualSubviewWrapper').setHTML(contentNode);
            }

            if (this.carerViewAdoptiveApprovalSubview) {
                var contentNode = Y.one(Y.config.doc.createDocumentFragment());
                contentNode.append(this.carerViewAdoptiveApprovalSubview.get('container'));
                this.get('container').one('#contextualSubviewWrapper').setHTML(contentNode);
            }

        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            if (this.carerViewAdoptiveApprovalSubview) {
                this.carerViewAdoptiveApprovalSubview.removeTarget(this);
                this.carerViewAdoptiveApprovalSubview.destroy();
                delete this.carerViewAdoptiveApprovalSubview;
            }

            if (this.raaViewAdoptiveApprovalSubview) {
                this.raaViewAdoptiveApprovalSubview.removeTarget(this);
                this.raaViewAdoptiveApprovalSubview.destroy();
                delete this.raaViewAdoptiveApprovalSubview;
            }

            this.childListView.removeTarget(this);
            this.childListView.destroy();
            delete this.childListView;
        }
    }, {
        ATTRS: {
            enumTypes: {
                value: {
                    gender: Y.usp.childlookedafter.enum.GenderSelection.values
                }
            },
            codedEntries: {
                value: {
                    endReason: Y.uspCategory.childlookedafter.adopterApprovalEndReason.category.codedEntries
                }
            }
        }
    });
    Y.namespace('app.carerApproval').RaaViewAdoptiveApprovalSubview = Y.Base.create('raaViewAdoptiveApprovalSubview', Y.usp.childlookedafter.ChildCarerApprovalView, [], {
        template: Y.Handlebars.templates.raaCarerapprovalViewAdoptiveCarerDetailsSubview
    }, {
        ATTRS: {
            enumTypes: {
                value: {
                    adoptionType: Y.usp.childlookedafter.enum.AdoptionType.values
                }
            }
        }
    });

    Y.namespace('app.carerApproval').CarerViewAdoptiveApprovalSubview = Y.Base.create('carerViewAdoptiveApprovalSubview', Y.usp.childlookedafter.ChildCarerApprovalView, [], {
        template: Y.Handlebars.templates.carerapprovalViewAdoptiveCarerDetailsSubview
    }, {
        ATTRS: {
            enumTypes: {
                value: {
                    adoptionType: Y.usp.childlookedafter.enum.AdoptionType.values
                }
            }
        }
    });

    Y.namespace('app.carerApproval').UpdateFosterApprovalView = Y.Base.create('updateFosterApprovalView',
        Y.usp.childlookedafter.ChildCarerApprovalView, [], {
            initializer: function () {
                this.endDateCalendar = new Y.usp.CalendarPopup();
                if (this.get('otherData').canEditDates) {
                    this.startDateCalendar = new Y.usp.CalendarPopup();
                }
            },
            template: Y.Handlebars.templates.carerDetailsEditDialog,
            render: function () {
                var container = this.get('container'),
                    model = this.get('model'),
                    otherData = this.get('otherData');

                //call superclass render
                Y.app.carerApproval.UpdateFosterApprovalView.superclass.render.call(this);
                this.endDateCalendar.setAttrs({
                    'inputNode': container.one('input[name="endDate"]'),
                    'minimumDate': model.get('startDate')
                }).render();

                if (otherData.canEditDates) {
                    this.startDateCalendar.setAttrs({
                        'inputNode': container.one('input[name="startDate"]'),
                    }).render();
                }

                this.initEndReason();
                return this;
            },
            destructor: function () {
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;

                if (this.startDateCalendar) {
                    this.startDateCalendar.destroy();
                    delete this.startDateCalendar;
                }
            },
            initEndReason: function () {
                var codes = Y.uspCategory.childlookedafter.adopterApprovalEndReason.category.getActiveCodedEntries();
                var value = this.get('model').get('endReason');
                var selectNode = this.get('container').one('select[name="endReason"]');
                if (selectNode) {
                    Y.FUtil.setSelectOptions(selectNode, O.values(codes), null, null, true, true, 'code');
                    if (value) {
                        selectNode.set('value', value);
                    }
                }
            }
        }, {
            ATTRS: {
                enumTypes: {
                    value: {
                        gender: Y.usp.childlookedafter.enum.GenderSelection.values
                    }
                },
                codedEntries: {
                    value: {
                        endReason: Y.uspCategory.childlookedafter.adopterApprovalEndReason.category.codedEntries,
                        typeOfDisability: Y.uspCategory.childlookedafter.typeOfDisability.category.codedEntries //com.olmgroup.usp.components.childlookedafter.category.typeOfDisability
                    }
                }
            }
        });

    Y.namespace('app.carerApproval').UpdateFosterCarerApprovalDataView = Y.Base.create('updateFosterCarerApprovalDataView', Y.usp.View, [], {

      template: Y.Handlebars.templates.updateFosterCarerApprovalData,

      events: {
        'input[name="disabilityPermitted"]':{
          change:'_handleChangeDisabilityPermitted'
        }},

      initDisabilityTypes: function(isDisplayed){
        var model = this.get('model'),
            typeOfDisabilityWrap = this.get('container').one('#typeOfDisability-wrap'),
            disabilityTypesSelect = this.get('container').one('select[name="disabilityTypes"]'),
            disabilityTypesOptions = O.values(Y.uspCategory.childlookedafter.typeOfDisability.category.getActiveCodedEntries()),
            existingDisabilityTypes = (model.get('disabilityTypes') || []);
        Y.FUtil.setSelectOptions(disabilityTypesSelect, disabilityTypesOptions, null, null, false, true, 'code');
        disabilityTypesSelect.get('options').each(function () {
          if (existingDisabilityTypes.indexOf(this.get('value'))!=-1){
            this.set('selected',true);
          }
        });
        if (isDisplayed==="true"){
          typeOfDisabilityWrap.setStyle('display','block');
        }else{
          typeOfDisabilityWrap.setStyle('display','none');
          this.get('container').one('#typeOfDisability-input').setHTML('');
        }
      },

      _handleChangeDisabilityPermitted: function(e){
        this.initDisabilityTypes(e.target.get('value'));
      },

      render: function(){
        Y.app.carerApproval.UpdateFosterCarerApprovalDataView.superclass.render.call(this);
        this.initDisabilityTypes(this.get('model').get('disabilityPermitted').toString());
        return this;
      },
    });

    Y.namespace('app.carerApproval').RemoveLastFosterApprovalWarningView = Y.Base.create('removeLastFosterApprovalWarningView', Y.usp.View, [], {
        template: Y.Handlebars.templates.carerRemoveLastFosterApprovalWarningDialog
    });

    Y.namespace('app.carerApproval').AddCarerSuspensionView = Y.Base.create('addCarerSuspension', Y.usp.View, [], {
        template: Y.Handlebars.templates.carerapprovalAddSuspension,
        initializer: function () {
            this.startDateCalendar = new Y.usp.CalendarPopup();
            this.endDateCalendar = new Y.usp.CalendarPopup();
        },
        render: function () {
            var container = this.get('container');

            //call superclass render
            Y.app.carerApproval.AddCarerSuspensionView.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]')
            }).render();

            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]')
            }).render();

            Y.FUtil.setSelectOptions(container.one('select[name="reason"]'), O.values(Y.secured.uspCategory.childlookedafter.fosterCarerSuspensionReason.category.getActiveCodedEntries(null, 'write')), null, null, true, true, 'code');

            return this;
        },
        destructor: function () {
            this.startDateCalendar.destroy();
            delete this.startDateCalendar;

            this.endDateCalendar.destroy();
            delete this.endDateCalendar;
        }
    });

    Y.namespace('app.carerApproval').ViewCarerSuspensionView = Y.Base.create('viewCarerSuspensionView',
        Y.usp.View, [], {
            template: Y.Handlebars.templates.carerSuspensionViewDialog,
        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        suspensionReason: Y.uspCategory.childlookedafter.fosterCarerSuspensionReason.category.codedEntries
                    }
                }
            }
        });

    Y.namespace('app.carerApproval').UpdateCarerSuspensionView = Y.Base.create('updateCarerSuspensionView',
        Y.usp.childlookedafter.UpdateEndChildCarerSuspensionView, [], {
            template: Y.Handlebars.templates.carerSuspensionEditDialog,
            initializer: function () {
                this.endDateCalendar = new Y.usp.CalendarPopup();
            },
            render: function () {
                var container = this.get('container'),
                    model = this.get('model');

                //call superclass render
                Y.app.carerApproval.UpdateCarerSuspensionView.superclass.render.call(this);

                this.endDateCalendar.setAttrs({
                    inputNode: container.one('input[name="endDate"]'),
                    minimumDate: model.get('startDate')
                }).render();

                return this;
            },
            destructor: function () {
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        suspensionReason: Y.uspCategory.childlookedafter.fosterCarerSuspensionReason.category.codedEntries
                    }
                }
            }
        });

    Y.namespace('app.carerApproval').AddCarerExemption = Y.Base.create('addCarerExemption', Y.usp.View, [], {
        template: Y.Handlebars.templates.carerExemptionsAddDialog,
        initializer: function () {
            this.startDateCalendar = new Y.usp.CalendarPopup();
            this.endDateCalendar = new Y.usp.CalendarPopup();
        },
        render: function () {
            var container = this.get('container');

            //call superclass render
            Y.app.carerApproval.AddCarerExemption.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]')
            }).render();

            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]')
            }).render();

            return this;
        },
        destructor: function () {
            this.startDateCalendar.destroy();
            delete this.startDateCalendar;
            this.endDateCalendar.destroy();
            delete this.endDateCalendar;
        }
    });

    Y.namespace('app.carerApproval').UpdateCarerExemptionsView = Y.Base.create('updateCarerExemptionsView',
        Y.usp.childlookedafter.UpdateFosterCarerExemptionView, [], {
            template: Y.Handlebars.templates.carerExemptionsEditDialog,
            initializer: function (config) {
                this.endDateCalendar = new Y.usp.CalendarPopup();
            },
            render: function () {
                var container = this.get('container'),
                    model = this.get('model');

                //call superclass render
                Y.app.carerApproval.UpdateCarerExemptionsView.superclass.render.call(this);

                this.endDateCalendar.setAttrs({
                    minimumDate: model.get('startDate'),
                    inputNode: container.one('input[name="endDate"]'),
                }).render();

                return this;
            },
            destructor: function () {

                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
        }, {
            ATTRS: {
                enumTypes: {
                    value: {
                        exemptionType: Y.usp.childlookedafter.enum.ExemptionType.values
                    }
                }
            }
        });

    Y.namespace('app.carerApproval').DeleteCarerExemptionsView = Y.Base.create('deleteCarerExemptionsView',
        Y.usp.childlookedafter.ChildCarerExemptionView, [], {
            template: Y.Handlebars.templates.carerExemptionsDeleteDialog
        }, {
            ATTRS: {
                enumTypes: {
                    value: {
                        exemptionType: Y.usp.childlookedafter.enum.ExemptionType.values
                    }
                }
            }
        });

    Y.namespace('app.carerApproval').DeleteChildCarerTrainingView = Y.Base.create('deleteChildCarerTrainingView',
        Y.usp.childlookedafter.ChildCarerTrainingView, [], {
            template: Y.Handlebars.templates.carerTrainingDeleteDialog
        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        courseCode: Y.uspCategory.childlookedafter.training.category.codedEntries
                    }
                }
            }
        }
    );

    Y.namespace('app.carerApproval').AddCarerTrainingView = Y.Base.create('addCarerTrainingView', Y.usp.childlookedafter.FosterCarerTrainingView, [], {
        events: {
            'input[name="applyToPartner"]': {
                change: 'handleApplyToPartnerChange'
            }
        },
        template: Y.Handlebars.templates.carerTrainingAddDialog,
        initializer: function () {
            this.startDateCalendar = new Y.usp.CalendarPopup();
            this.endDateCalendar = new Y.usp.CalendarPopup();
            this.partnerStartDateCalendar = new Y.usp.CalendarPopup();
            this.partnerEndDateCalendar = new Y.usp.CalendarPopup();
        },
        render: function () {
            var container = this.get('container'),
                hasActivePartner = this.get('otherData').subject.hasActivePartner;
            //call superclass render
            Y.app.carerApproval.AddCarerTrainingView.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]')
            }).render();

            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]')
            }).render();

            this.partnerStartDateCalendar.setAttrs({
                inputNode: container.one('input[name="partnerTrainingStartDate"]')
            }).render();

            this.partnerEndDateCalendar.setAttrs({
                inputNode: container.one('input[name="partnerTrainingEndDate"]')
            }).render();

            Y.FUtil.setSelectOptions(container.one('select[name="courseCode"]'), O.values(Y.secured.uspCategory.childlookedafter.training.category.getActiveCodedEntries(null, 'write')), null, null, true, true, 'code');

            var applyToParner = false;
            if (hasActivePartner) {
                applyToParner = container.one('input[name="applyToPartner"]').get('checked') === true;
            }

            //show/hide apply to partner inputs
            this._setApplyToPartner(applyToParner);

            return this;
        },
        handleApplyToPartnerChange: function (e) {
            this._setApplyToPartner(e.currentTarget.get('checked') === true);
        },
        _setApplyToPartner: function (isApplied) {
            var container = this.get('container');

            if (isApplied) {
                container.one('#applyToPartnerOptions').show();
            } else {
                container.one('#applyToPartnerOptions').hide();
                //clear down existing values
                container.one('input[name="partnerTrainingStartDate"]').set('value', '');
                container.one('input[name="partnerTrainingEndDate"]').set('value', '');
            }
        },
        destructor: function () {
            this.startDateCalendar.destroy();
            delete this.startDateCalendar;

            this.endDateCalendar.destroy();
            delete this.endDateCalendar;

            this.partnerStartDateCalendar.destroy();
            delete this.partnerStartDateCalendar;

            this.partnerEndDateCalendar.destroy();
            delete this.partnerEndDateCalendar;
        }

    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    courseCode: Y.uspCategory.childlookedafter.training.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.carerApproval').AddFosterCarerTSDStandardsView = Y.Base.create('addFosterCarerTSDStandardsView', Y.usp.childlookedafter.FosterCarerTSDStandardsView, [], {
        template: Y.Handlebars.templates.fosterCarerTSDAddDialog,

        initializer: function () {
            this.statusDateCalendar = new Y.usp.CalendarPopup();
        },
        render: function () {
            var container = this.get('container');
            //call superclass render
            Y.app.carerApproval.AddFosterCarerTSDStandardsView.superclass.render.call(this);

            this.statusDateCalendar.setAttrs({
                inputNode: container.one('input[name="statusDate"]')
            }).render();

            Y.FUtil.setSelectOptions(container.one('select[name="statusCode"]'), O.values(Y.uspCategory.childlookedafter.fostercarertsdstandardsstatus.category.getActiveCodedEntries()), null, null, true, true, 'code');
            return this;
        },
        destructor: function () {
            this.statusDateCalendar.destroy();
            delete this.statusDateCalendar;
        }

    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    statusCode: Y.uspCategory.childlookedafter.fostercarertsdstandardsstatus.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.carerApproval').ViewFosterCarerTSDStandardsView = Y.Base.create('viewFosterCarerTSDStandardsView', Y.usp.View, [], {
        template: Y.Handlebars.templates.fosterCarerTSDViewDialog
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    statusCode: Y.uspCategory.childlookedafter.fostercarertsdstandardsstatus.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.carerApproval').ViewFosterCarerTSDStandardsHistoryView = Y.Base.create('viewFosterCarerTSDStandardsHistoryView', Y.usp.View, [], {
        template: Y.Handlebars.templates.fosterCarerTSDHistoryViewDialog,
        initializer: function (config) {
            var historyConfig = {
                searchConfig: config
            };

            //Fetch historic result set
            this.fosterCarerTSDStandardsResults = new Y.app.approvals.FosterCarerTSDStandardsHistoryResults(historyConfig).addTarget(this);
        },
        render: function () {
            var container = this.get('container');
            //call superclass
            Y.app.carerApproval.ViewFosterCarerTSDStandardsHistoryView.superclass.render.call(this);
            //Populate historic result set
            container.one(this.get('tableId')).setHTML(this.fosterCarerTSDStandardsResults.render().get('container'));
            return this;
        }
    }, {
        ATTRS: {
            tableId: {
                value: '#historyResultsView'
            },
            codedEntries: {
                value: {
                    statusCode: Y.uspCategory.childlookedafter.fostercarertsdstandardsstatus.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.carerApproval').AddCarerUnderInvestigationView = Y.Base.create('addCarerUnderInvestigationView', BaseInvestigationView, [], {
        events: {
            '#allegationMadeByType': {
                change: '_handleAllegationMadeByTypeChange'
            },
            '#outcome': {
                change: '_handleOutcomeChange'
            }
        },
        template: Y.Handlebars.templates.carerUnderInvestigationAddDialog,
        initializer: function () {

            this._addCarerUnderInvestigationEvts = [
                this.after('fosterChildrenListChange', this._setFosterChildList, this)
            ];

            this.startDateCalendar = new Y.usp.CalendarPopup();
            this.endDateCalendar = new Y.usp.CalendarPopup();
            this.monitoringStartDateCalendar = new Y.usp.CalendarPopup();
            this.monitoringEndDateCalendar = new Y.usp.CalendarPopup();
        },
        render: function () {
            var container = this.get('container');

            //call superclass render
            Y.app.carerApproval.AddCarerUnderInvestigationView.superclass.render.call(this);

            this.startDateCalendar.setAttrs({
                inputNode: container.one('input[name="startDate"]')
            }).render();
            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]')
            }).render();
            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]')
            }).render();
            this.monitoringStartDateCalendar.setAttrs({
                inputNode: container.one('input[name="monitoringStartDate"]')
            }).render();
            this.monitoringEndDateCalendar.setAttrs({
                inputNode: container.one('input[name="monitoringEndDate"]')
            }).render();

            this._setFosterChildList();
            this._initOutcomeOfInvestigation();

            return this;
        },
        destructor: function () {
            this._addCarerUnderInvestigationEvts.forEach(function (handle) {
                handle.detach();
            });

            delete this._addCarerUnderInvestigationEvts;

            this.startDateCalendar.destroy();
            delete this.startDateCalendar;

            this.endDateCalendar.destroy();
            delete this.endDateCalendar;

            this.monitoringStartDateCalendar.destroy();
            delete this.monitoringStartDateCalendar;

            this.monitoringEndDateCalendar.destroy();
            delete this.monitoringEndDateCalendar;
        },
        _setFosterChildList: function () {
            var container = this.get('container'),
                fosterChildNode = container.one('select[name="allegationMadeByAssociationsValue"]'),
                fosterChildrenList = this.get('fosterChildrenList') || [];

            if (fosterChildNode) {
                Y.FUtil.setSelectOptions(fosterChildNode, fosterChildrenList.map(function (relationship) {
                    return ({
                        id: relationship.roleBPersonVO.id,
                        name: relationship.roleBPersonVO.name + ' (PER' + relationship.roleBPersonVO.id + ')'
                    });
                }), null, null, false, false);
            }
        },
        _initOutcomeOfInvestigation: function () {
            var container = this.get('container');
            var selectNode = container.one('select[name="outcome"]');
            if (selectNode) {
                Y.FUtil.setSelectOptions(selectNode, O.values(Y.uspCategory.childlookedafter.fosterCarerUnderInvestigationOutcome.category.getActiveCodedEntries()), null, null, true, true, 'code');
            }
        },
        _handleAllegationMadeByTypeChange: function (e) {
            var value = e.target.get('value'),
                fosterChildNode = this.get('container').one('#fosterChildDetails'),
                otherNode = this.get('container').one('#otherDetails');

            if (value === 'FOSTER_CHILD') {
                fosterChildNode.show();
                otherNode.hide();
            } else if (value === 'OTHER') {
                fosterChildNode.hide();
                otherNode.show();
            }
        },
        _handleOutcomeChange: function (e) {
            var value = e.target.get('value'),
                monitoringFieldsNode = this.get('container').one('#monitoringFields');

            if (value === 'MONITORING') {
                monitoringFieldsNode.show();
            } else {
                monitoringFieldsNode.hide();
            }
        }
    });

    Y.namespace('app.carerApproval').UpdateCarerUnderInvestigationView = Y.Base.create('updateCarerUnderInvestigationView', BaseInvestigationView, [], {
        template: Y.Handlebars.templates.carerUnderInvestigationEditDialog,
        initializer: function () {
            this._updateCarerUnderInvestigationEvts = [
                this.after('fosterChildrenListChange', this._updatePicklist, this)
            ];
        },
        render: function () {
            var container = this.get('container'),
                model = this.get('model'),
                allegationMadeByPerson = model.get('allegationMadeByPerson');

            //call superclass render
            Y.app.carerApproval.UpdateCarerUnderInvestigationView.superclass.render.call(this);

            if (allegationMadeByPerson && allegationMadeByPerson.length > 0) {
                container.one('#peopleDetails').setStyle('display', 'block');
            } else {
                container.one('#otherDetails').setStyle('display', 'block');
            }
            return this;
        },
        _updatePicklist: function () {
            var container = this.get('container'),
                model = this.get('model'),
                allegationMadeByPerson = model.get('allegationMadeByPerson');

            if (!allegationMadeByPerson || allegationMadeByPerson.length === 0) {
                //no picklist if alligations not made by person
                return;
            }
            var subject = this.get('otherData.subject');
            var allegationsBy = this.get('model').get('allegationMadeByPerson');
            var fosterChildrenList = this.get('fosterChildrenList') || [];
            var selections = allegationsBy.map(function (by) {
                return ({
                    id: by.id,
                    name: E.html(by.name),
                    title: E.html(by.name)
                });
            });

            var assignedIds = allegationsBy.map(function (by) {
                return by.id;
            });

            var options = fosterChildrenList.map(function (relationship) {
                var person = relationship.roleAPersonId === Number(subject.subjectId) ? relationship.roleBPersonVO : relationship.roleAPersonVO;
                return ({
                    id: person.id,
                    name: E.html(person.name),
                    title: E.html(person.name)
                });
            });

            //preserve order
            var order = options.map(function (option) {
                return option.name;
            }).sort(function (a, b) {
                return (a || '').localeCompare(b || '');
            });

            //filter out already selected
            options = options.filter(function (option) {
                return assignedIds.indexOf(option.id) === -1;
            });

            this.picklist = new Y.usp.Picklist({
                optionsMap: {
                    text: 'name',
                    value: 'id',
                    title: 'title'
                },
                options: options,
                order: order,
                selections: selections,
                preserveOrder: true,
                stackMode: true,
                selectWidth: '100%',
                actionLabelOne: '>',
                actionLabelAll: '>>',
                actionLabelRmv: '<',
                actionLabelRmvAll: '<<'
            });
            this.picklist.render();
            container.one('#children-picklist').appendChild(this.picklist.get('srcNode'));
        },
        destructor: function () {
            this._updateCarerUnderInvestigationEvts.forEach(function (handle) {
                handle.detach();
            });
            delete this._updateCarerUnderInvestigationEvts;

            if (this.picklist) {
                this.picklist.destroy();
            }
        }
    });

    Y.namespace('app.carerApproval').EndCarerUnderInvestigationView = Y.Base.create('endDateCarerUnderInvestigationView', Y.usp.View, [], {
        events: {
            '#outcome': {
                change: '_handleOutcomeChange'
            }
        },
        template: Y.Handlebars.templates.carerUnderInvestigationEndDialog,
        initializer: function () {
            this.endDateCalendar = new Y.usp.CalendarPopup();
            this.monitoringStartDateCalendar = new Y.usp.CalendarPopup();
            this.monitoringEndDateCalendar = new Y.usp.CalendarPopup();
        },
        destructor: function () {
            this.endDateCalendar.destroy();
            delete this.endDateCalendar;
            this.monitoringStartDateCalendar.destroy();
            delete this.monitoringStartDateCalendar;
            this.monitoringEndDateCalendar.destroy();
            delete this.monitoringEndDateCalendar;
        },
        render: function () {
            var container = this.get('container');
            Y.app.carerApproval.EndCarerUnderInvestigationView.superclass.render.call(this);

            this.endDateCalendar.setAttrs({
                inputNode: container.one('input[name="endDate"]')
            }).render();

            this.monitoringStartDateCalendar.setAttrs({
                inputNode: container.one('input[name="monitoringStartDate"]')
            }).render();

            this.monitoringEndDateCalendar.setAttrs({
                inputNode: container.one('input[name="monitoringEndDate"]')
            }).render();

            this._initInvestigationOutcome();
            this._showHideMonitoringFlds(this.get('model').get('outcome'));

            return this;
        },
        _initInvestigationOutcome: function () {
            var codes = Y.uspCategory.childlookedafter.fosterCarerUnderInvestigationOutcome.category.getActiveCodedEntries();
            var value = this.get('model').get('outcome');
            var selectNode = this.get('container').one('#outcome');
            if (selectNode) {
                Y.FUtil.setSelectOptions(selectNode, O.values(codes), null, null, true, true, 'code');
                if (value) {
                    selectNode.set('value', value);
                }
            }
        },
        _showHideMonitoringFlds: function (value) {
            var monitoringFieldsNode = this.get('container').one('#monitoringFields');
            if (value === 'MONITORING') {
                monitoringFieldsNode.show();
            } else {
                monitoringFieldsNode.hide();
            }
        },
        _handleOutcomeChange: function (e) {
            this._showHideMonitoringFlds(e.target.get('value'));
        },
    });

    Y.namespace('app.carerApproval').ViewCarerUnderInvestigationView = Y.Base.create('viewCarerUnderInvestigationView', Y.usp.View, [], {
        template: Y.Handlebars.templates.carerUnderInvestigationViewDialog,
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    suspensionReason: Y.uspCategory.childlookedafter.fosterCarerSuspensionReason.category.codedEntries,
                    allegedAbuse: Y.uspCategory.childlookedafter.fosterCarerUnderInvestigationAllegedAbuse.category.codedEntries,
                    outcome: Y.uspCategory.childlookedafter.fosterCarerUnderInvestigationOutcome.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.carerApproval').AddSpecificChildApprovalView = Y.Base.create('addSpecificChildApprovalView', Y.usp.View, [], {
        template: Y.Handlebars.templates.specificChildApprovalAddDialog,
        initializer: function () {
            this.personAutoCompleteView = new Y.app.PersonAutoCompleteResultView({
                autocompleteURL: this.get('personAutocompleteURL'),
                inputName: 'childApprovedForId',
                labels: {
                    placeholder: this.get('labels').autoCompletePlaceholder
                }
            }).addTarget(this);
        },
        render: function () {
            var container = this.get('container');
            Y.app.carerApproval.AddSpecificChildApprovalView.superclass.render.call(this);

            container.one('#personSearchWrapper').append(this.personAutoCompleteView.render().get('container'));

            return this;
        }
    });

    Y.namespace('app.carerApproval').DeleteSpecificChildApprovalView = Y.Base.create('deleteSpecificChildApprovalView', Y.usp.View, [], {
        template: Y.Handlebars.templates.specificChildApprovalDeleteDialog
    });

    Y.namespace('app.carerApproval').ChildCarerApprovalDialog = Y.Base.create('childCarerApprovalDialog', Y.usp.app.AppDialog, [], {
        _addFosterCarerApprovalSubmit: function (e) {
            e.preventDefault();
            var view = this.get('activeView'),
                synchronisedWithPartnerNode = view.get('container').one('#synchroniseWithPartner'),
                synchronisedWithPartner = null;

            if (synchronisedWithPartnerNode) {
                synchronisedWithPartner = (synchronisedWithPartnerNode.get('disabled')) ? null : synchronisedWithPartnerNode.get('checked');
            }

            this.handleSave(e, {
                synchronisedWithPartner: synchronisedWithPartner
            });
        },
        initializer: function () {
            this.on("*:setNarrative", this.updateNarrativeDesc, this);
        },
        _endFosterCarerRegistrationSubmit: function (e) {
            e.preventDefault();
            var view = this.get('activeView');
            view.get('model').url = view.get('submitUrl');
            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateEndFosterCarerRegistration
            });
        },
        _editFosterCarerRegistrationSubmit: function (e) {
            e.preventDefault();
            var synchroniseWithPartner = true;
            this.handleSave(e, {synchroniseWithPartner: synchroniseWithPartner}, {
                transmogrify: Y.usp.childlookedafter.UpdateFosterCarerRegistration
            });
        },
        updateNarrativeDesc: function (e) {
            var view = this.get('activeView'),
                viewInfo = this._getViewInfo(view) || {};
            view.set('otherData.organisation', e.selectedOrganisation || null);
            this.updateNarrative(viewInfo.narrative);
        },
        _editFosterApprovalSubmit: function (e) {
            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateFosterCarerApproval
            });
        },
        _updateFosterCarerApprovalDataSubmit: function (e) {
            var view = this.get('activeView');
            view.get('model').url = view.get('submitUrl');

            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateDataFosterCarerApproval
            });
        },
        _editAdoptiveApprovalSubmit: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                childLocalAuthorityId = null;

            if (view.raaUpdateAdoptiveApprovalSubview) {
                var selectedOrganisation = view.raaUpdateAdoptiveApprovalSubview.organisationAutoCompleteView.get('selectedOrganisation');
                childLocalAuthorityId = (selectedOrganisation !== null && typeof selectedOrganisation !== "undefined") ? selectedOrganisation.id : null;
            }

            this.handleSave(e, {
                childLocalAuthority: childLocalAuthorityId,
                adoptiveChildAssociations: view.get('model').get('adoptiveChild')
            }, {
                transmogrify: Y.usp.childlookedafter.UpdateAdoptiveCarerApproval
            });
        },

        _editCarerSuspensionSubmit: function (e) {
            var view = this.get('activeView'),
                model = view.get('model');

            //ensure model uses update url as opposed to the url to get data
            model.url = view.get('submitUrl');

            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateEndChildCarerSuspension
            });
        },

        _addUnderInvestigationSubmit: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                allegationMadeByAssociations = null,
                allegationMadeByOther = null,
                typeSelected = (container.one('input[name=allegationMadeByType]:checked')) ? true : false;

            this.showDialogMask();

            if (typeSelected) {
                if (container.one('input[name=allegationMadeByType]:checked').get('value') === 'FOSTER_CHILD') {
                    var fosterChildIds = [];

                    container.one('#allegationMadeByAssociations').get("options").each(function () {
                        if (this.get('selected')) fosterChildIds.push(this.get('value'));
                    });

                    allegationMadeByAssociations = [];

                    A.forEach(fosterChildIds, function (fosterChildId) {
                        var newAllegationMadeByAssociation = {
                            _type: 'NewAllegationMadeByAssociation',
                            personId: fosterChildId
                        };

                        allegationMadeByAssociations.push(newAllegationMadeByAssociation);
                    });
                } else {
                    allegationMadeByOther = container.one('#allegationMadeByOther').get('value');
                }
            }

            this.handleSave(e, {
                personId: view.get('otherData').subject.subjectId,
                allegationMadeByAssociations: allegationMadeByAssociations,
                allegationMadeByOther: allegationMadeByOther
            });
        },

        _editUnderInvestigationSubmit: function (e) {
            var allegationMadeByAssociations = null;
            var allegationMadeByOther = null;
            var picklistValues = null;
            var view = this.get('activeView'),
                container = view.get('container'),
                model = view.get('model');

            if (view.picklist) {
                picklistValues = view.picklist.getSelectedValues();
                allegationMadeByAssociations = [];
                A.each(picklistValues, function (id) {
                    allegationMadeByAssociations.push({
                        _type: 'NewAllegationMadeByAssociation',
                        personId: id
                    });
                });
            } else {
                allegationMadeByOther = container.one('#allegationMadeByOther').get('value');
            }

            model.url = view.get('submitUrl');

            this.handleSave(e, {
                allegationMadeByAssociations: allegationMadeByAssociations,
                allegationMadeByOther: allegationMadeByOther
            }, {
                transmogrify: Y.usp.childlookedafter.UpdateFosterCarerUnderInvestigation
            });
        },

        _endUnderInvestigationSubmit: function (e) {
            var view = this.get('activeView');
            view.get('model').url = view.get('submitUrl');
            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateEndFosterCarerUnderInvestigation
            });
        },

        _editExemptionSubmit: function (e) {
            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateFosterCarerExemption
            });
        },

        _addCarerTrainingSubmit: function (e) {
            var view = this.get('activeView'),
                applyToPartner = (view.get('container').one('input[name=applyToPartner]:checked')) ? true : false;
            if (!applyToPartner) {
                this.handleSave(e, {}, {
                    //transmogrify to base type to remove additional attributes used to drive the view
                    //i.e. partnerTrainingStartDate & partnerTrainingEndDate
                    transmogrify: Y.usp.childlookedafter.NewFosterCarerTraining
                });
            } else {
                this.handleSave(e, {}, {
                    transmogrify: Y.usp.childlookedafter.NewFosterCarerTrainingWithPartner
                });
            }
        },

        _addSpecificChildApprovalSubmit: function (e) {
            var view = this.get('activeView'),
                personCarerSubjectFieldIsEmpty = view.personAutoCompleteView.get('selectedPerson') === null || typeof view.personAutoCompleteView.get('selectedPerson') === "undefined",
                childApprovedForId = (!personCarerSubjectFieldIsEmpty) ? view.personAutoCompleteView.get('selectedPerson').id : null;

            this.handleSave(e, {
                childApprovedForId: childApprovedForId
            });
        },

        views: {
            addFosterApproval: {
                type: Y.app.carerApproval.AddFosterApproval,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_addFosterCarerApprovalSubmit',
                    disabled: true
                }]
            },
            endFosterCarerRegistrationError: {
                type: Y.app.carerApproval.EndFosterCarerRegistrationErrorView
            },
            endFosterCarerRegistrationForm: {
                type: Y.app.carerApproval.EndFosterCarerRegistrationFormView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_endFosterCarerRegistrationSubmit',
                    disabled: true
                }]
            },
            editFosterCarerRegistration: {
                type: Y.app.carerApproval.EditFosterCarerRegistrationView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_editFosterCarerRegistrationSubmit',
                    disabled: true
                }]
            },
            addAdoptiveApproval: {
                type: Y.app.carerApproval.AddAdoptiveApproval,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            addCarerSuspension: {
                type: Y.app.carerApproval.AddCarerSuspensionView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            editCarerSuspension: {
                type: Y.app.carerApproval.UpdateCarerSuspensionView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_editCarerSuspensionSubmit',
                    disabled: true
                }]
            },
            viewCarerSuspension: {
                type: Y.app.carerApproval.ViewCarerSuspensionView,
                buttons: [{
                    name: 'editButton',
                    labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                    isActive: true,
                    action: function (e) {
                        var model = this.get('activeView').get('model');
                        this.fire('carerSuspension:showDialog', {
                            id: model.get('id'),
                            action: 'edit'
                        });
                    },
                    disabled: true
                }]
            },
            addCarerUnderInvestigation: {
                type: Y.app.carerApproval.AddCarerUnderInvestigationView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_addUnderInvestigationSubmit',
                    disabled: true
                }]
            },
            viewCarerInvestigation: {
                type: Y.app.carerApproval.ViewCarerUnderInvestigationView
            },
            editCarerInvestigation: {
                type: Y.app.carerApproval.UpdateCarerUnderInvestigationView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_editUnderInvestigationSubmit',
                    disabled: true
                }]
            },
            endCarerInvestigation: {
                type: Y.app.carerApproval.EndCarerUnderInvestigationView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_endUnderInvestigationSubmit',
                    disabled: true
                }]
            },
            addCarerTraining: {
                type: Y.app.carerApproval.AddCarerTrainingView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_addCarerTrainingSubmit',
                    disabled: true
                }]
            },
            deleteChildCarerTraining: {
                type: Y.app.carerApproval.DeleteChildCarerTrainingView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            addFosterCarerTSDStandards: {
                type: Y.app.carerApproval.AddFosterCarerTSDStandardsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            viewFosterCarerTSDStandards: {
                type: Y.app.carerApproval.ViewFosterCarerTSDStandardsView
            },
            viewFosterCarerTSDStandardsHistory: {
                type: Y.app.carerApproval.ViewFosterCarerTSDStandardsHistoryView
            },
            viewFosterApproval: {
                type: Y.app.carerApproval.ViewFosterApprovalView,
                buttons: [{
                    name: 'editButton',
                    labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                    isActive: true,
                    action: function (e) {
                        this.fire('updateCarerApproval', {
                            record: this.get('activeView').get('model')
                        });
                    },
                    disabled: true
                }]
            },
            editFosterApproval: {
                type: Y.app.carerApproval.UpdateFosterApprovalView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_editFosterApprovalSubmit',
                    disabled: true
                }]
            },
            updateFosterCarerApprovalData: {
                type: Y.app.carerApproval.UpdateFosterCarerApprovalDataView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_updateFosterCarerApprovalDataSubmit',
                    disabled: true
                }]
            },
            removeLastFosterApprovalWarning: {
                type: Y.app.carerApproval.RemoveLastFosterApprovalWarningView,
                buttons: [{
                    name: 'okButton',
                    labelHTML: '<i class="fa fa-check"></i> Ok',
                    action: 'handleCancel',
                    disabled: true
                }]
            }, 
            addCarerExemption: {
                type: Y.app.carerApproval.AddCarerExemption,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            editCarerExemption: {
                type: Y.app.carerApproval.UpdateCarerExemptionsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_editExemptionSubmit',
                    disabled: true
                }]
            },
            viewAdoptiveApproval: {
                type: Y.app.carerApproval.ViewAdoptiveApproval,
                buttons: [{
                    name: 'editButton',
                    labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                    isActive: true,
                    action: function (e) {
                        this.fire('updateAdoptiveCarerApproval', {
                            record: this.get('activeView').get('model')
                        });
                    },
                    disabled: true
                }]
            },
            editAdoptiveApproval: {
                type: Y.app.carerApproval.UpdateAdoptiveApproval,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_editAdoptiveApprovalSubmit',
                    disabled: true
                }]
            },
            deleteCarerExemption: {
                type: Y.app.carerApproval.DeleteCarerExemptionsView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            addSpecificChildApproval: {
                type: Y.app.carerApproval.AddSpecificChildApprovalView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_addSpecificChildApprovalSubmit',
                    disabled: true
                }]
            },
            deleteSpecificChildApproval: {
                type: Y.app.carerApproval.DeleteSpecificChildApprovalView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            }
        }
    }, {
        ATTRS: {}
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'event-custom',
        'view',
        'usp-view',
        'app-dialog',
        'model-form-link',
        'picklist',
        'usp-childlookedafter-AdoptiveCarerApproval',
        'usp-childlookedafter-NewChildCarerApproval',
        'usp-childlookedafter-NewFosterCarerApproval',
        'usp-childlookedafter-NewAdoptiveCarerApproval',
        'usp-childlookedafter-NewFosterCarerSuspension',
        'usp-childlookedafter-NewFosterCarerTraining',
        'usp-childlookedafter-NewFosterCarerTrainingWithPartner',
        'usp-childlookedafter-NewFosterCarerTSDStandards',
        'usp-childlookedafter-FosterCarerUnderInvestigation',
        'usp-childlookedafter-NewFosterCarerUnderInvestigation',
        'usp-childlookedafter-FosterCarerUnderInvestigation',
        'usp-childlookedafter-UpdateFosterCarerUnderInvestigation',
        'usp-childlookedafter-UpdateEndFosterCarerUnderInvestigation',
        'usp-childlookedafter-ChildCarerSuspension',
        'usp-childlookedafter-FosterCarerSuspension',
        'usp-childlookedafter-FosterCarerApproval',
        'usp-childlookedafter-ChildCarerApproval',
        'usp-childlookedafter-UpdateAdoptiveCarerApproval',
        'usp-childlookedafter-UpdateFosterCarerApproval',
        'usp-childlookedafter-UpdateDataFosterCarerApproval',
        'usp-childlookedafter-ChildCarerExemption',
        'usp-childlookedafter-FosterCarerExemption',
        'usp-childlookedafter-NewChildCarerExemption',
        'usp-childlookedafter-NewFosterCarerExemption',
        'usp-childlookedafter-UpdateChildCarerExemption',
        'usp-childlookedafter-UpdateFosterCarerExemption',
        'usp-childlookedafter-NewSpecificChildApproval',
        'usp-childlookedafter-UpdateEndChildCarerSuspension',
        'usp-childlookedafter-UpdateEndFosterCarerRegistration',
        'usp-childlookedafter-UpdateFosterCarerRegistration',
        'usp-relationship-PersonPersonRelationshipDetails',
        'categories-childlookedafter-component-TypeOfCare',
        'secured-categories-childlookedafter-component-TypeOfCare',
        'secured-categories-childlookedafter-component-Training',
        'secured-categories-childlookedafter-component-FosterCarerSuspensionReason',
        'secured-categories-childlookedafter-component-FosterCarerUnderInvestigationAllegedAbuse',
        'categories-childlookedafter-component-FosterCarerSuspensionReason',
        'categories-childlookedafter-component-FosterCarerUnderInvestigationAllegedAbuse',
        'categories-childlookedafter-component-FosterCarerUnderInvestigationOutcome',
        'categories-childlookedafter-component-DeregistrationReason',
        'categories-childlookedafter-component-AdopterApprovalEndReason',
        'categories-childlookedafter-component-TypeOfDisability',
        'categories-childlookedafter-component-Fostercarertsdstandardsstatus',
        'handlebars',
        'handlebars-helpers',
        'handlebars-carerApproval-templates',
        'calendar-popup',
        'model-transmogrify',
        'childlookedafter-component-enumerations',
        'organisation-autocomplete-view',
        'training-accordion-views',
        'person-autocomplete-view',
        'promise',
        'template-micro',
        'adoptive-child'
    ]
});
