YUI.add('adoptioninformation-dialog', function(Y) {

    'use-strict';

    var L = Y.Lang,
        O = Y.Object;

    Y.namespace('app.childlookedafter').BaseAdoptionInformationView = Y.Base.create('BaseAdoptionInformationView', Y.usp.View, [], {
        events: {
            '#dateShouldBePlaced': {
                change: '_handleDateChange',
                dateChange: '_handleDateChange'
            },
            '#dateMatched': {
                change: '_handleDateChange',
                dateChange: '_handleDateChange'
            },
            '#genderOfAdopters': {
                change: '_handleGenderOfAdoptersChange'
            },
            '#disruptionDate': {
                change: '_handleDisruptionDateChange',
                dateChange: '_handleDisruptionDateFromCalendarChange'
            },
            '#familyFindingActivity': {
                change: '_handleFamilyFindingActivityChange'
            }
        },

        initializer: function() {
            this.dateShouldBePlacedCalendar = new Y.usp.CalendarPopup().addTarget(this);
            this.dateMatchedCalendar = new Y.usp.CalendarPopup().addTarget(this);
            this.datePlacedCalendar = new Y.usp.CalendarPopup().addTarget(this);
            this.dateNoLongerPlannedCalendar = new Y.usp.CalendarPopup().addTarget(this);
            this.disruptionDateCalendar = new Y.usp.CalendarPopup().addTarget(this);
        },

        render: function() {
            Y.app.childlookedafter.BaseAdoptionInformationView.superclass.render.call(this);

            var container = this.get('container')

            this.dateShouldBePlacedCalendar.set('inputNode', container.one('#dateShouldBePlaced')).render();
            this.dateMatchedCalendar.set('inputNode', container.one('#dateMatched')).render();
            this.datePlacedCalendar.set('inputNode', container.one('#datePlaced')).render();
            this.dateNoLongerPlannedCalendar.set('inputNode', container.one('#dateNoLongerPlanned')).render();
            this.disruptionDateCalendar.set('inputNode', container.one('#disruptionDate')).render();

            this._initGenderOfAdopters(this.get('model').get('genderOfAdopters'));
            this._initReasonNoLongerPlanned(this.get('model').get('reasonNoLongerPlanned'));
            this._initFamilyFindingActivity(this.get('model').get('familyFindingActivity'));
            this._initLegalStatusOfAdopters(O.values(Y.uspCategory.childlookedafter.adopterLegalStatus.category.getActiveCodedEntries()), this.get('model').get('legalStatusOfAdopters'));

            return this;
        },

        destructor: function() {
            if (this.dateShouldBePlacedCalendar) {
                this.dateShouldBePlacedCalendar.removeTarget(this);
                this.dateShouldBePlacedCalendar.destroy();
                delete this.dateShouldBePlacedCalendar;
            }

            if (this.dateMatchedCalendar) {
                this.dateMatchedCalendar.removeTarget(this);
                this.dateMatchedCalendar.destroy();
                delete this.dateMatchedCalendar;
            }

            if (this.datePlacedCalendar) {
                this.datePlacedCalendar.removeTarget(this);
                this.datePlacedCalendar.destroy();
                delete this.datePlacedCalendar;
            }

            if (this.dateNoLongerPlannedCalendar) {
                this.dateNoLongerPlannedCalendar.removeTarget(this);
                this.dateNoLongerPlannedCalendar.destroy();
                delete this.dateNoLongerPlannedCalendar;
            }

            if (this.disruptionDateCalendar) {
                this.disruptionDateCalendar.removeTarget(this);
                this.disruptionDateCalendar.destroy();
                delete this.disruptionDateCalendar;
            }
        },

        _initGenderOfAdopters: function(value) {
            var adopterGenderValues = O.values(Y.uspCategory.childlookedafter.adopterGender.category.getActiveCodedEntries()),
                select = this.get('container').one('#genderOfAdopters');

            if (adopterGenderValues && select) {
                Y.FUtil.setSelectOptions(select, adopterGenderValues, null, null, true, true, 'code');

                if (value) {
                    select.set('value', value);
                }
            }
        },

        _initReasonNoLongerPlanned: function(value) {
            var reasonNoLongerPlannedValues = O.values(Y.uspCategory.childlookedafter.reasonAdoptionNoLongerPlanned.category.getActiveCodedEntries()),
                select = this.get('container').one('#reasonNoLongerPlanned');

            if (reasonNoLongerPlannedValues && select) {
                Y.FUtil.setSelectOptions(select, reasonNoLongerPlannedValues, null, null, true, true, 'code');

                if (value) {
                    select.set('value', value);
                }
            }
        },

        _initLegalStatusOfAdopters: function(adopterLegalStatusValues, value) {
            var select = this.get('container').one('#legalStatusOfAdopters');

            if (adopterLegalStatusValues && select) {
                Y.FUtil.setSelectOptions(select, adopterLegalStatusValues, null, null, true, true, 'code');

                if (value) {
                    select.set('value', value);
                }
            }
        },

        _initFamilyFindingActivity: function(value) {
            var familyFindingActivityValues = O.values(Y.uspCategory.childlookedafter.familyFindingActivity.category.getActiveCodedEntries()),
                select = this.get('container').one('#familyFindingActivity');

            if (familyFindingActivityValues && select) {
                Y.FUtil.setSelectOptions(select, familyFindingActivityValues, null, null, true, true, 'code');

                if (value) {
                    select.set('value', value);
                }
            }
        },

        _handleDateChange: function(e) {
            this._updateDateWarningMessage();
        },

        _handleGenderOfAdoptersChange: function(e) {
            this._updateLegalStatusOfAdopterOptions(e.target.get('value'));
        },

        _handleDisruptionDateChange: function(e) {
            this._updateMandatoryMarkers(e.currentTarget.get('value'));
        },

        _handleDisruptionDateFromCalendarChange: function(e) {
            this._updateMandatoryMarkers(e.date);
        },

        _handleFamilyFindingActivityChange: function(e) {
            this._updateOtherDisruptionReasonVisibility(e.currentTarget.get('value'));
        },

        _updateLegalStatusOfAdopterOptions: function(gender) {
            var values = O.values(Y.uspCategory.childlookedafter.adopterLegalStatus.category.getActiveCodedEntries()),
                currentLegalStatusOfAdopters = this.get('container').one('#legalStatusOfAdopters').get('value');

            switch (gender) {
                case 'M1':
                case 'F1':
                    values = values.filter(function(codedEntry) {
                        return codedEntry.code === 'L0';
                    });
                    break;
                case 'MF':
                    values = values.filter(function(codedEntry) {
                        return codedEntry.code === 'L3' || codedEntry.code === 'L11';
                    });
                    break;
                case 'MM':
                case 'FF':
                    values = values.filter(function(codedEntry) {
                        return codedEntry.code === 'L2' || codedEntry.code === 'L4' || codedEntry.code === 'L12';
                    });
                    break;
                default:
                    break;
            }

            //ensure we don't end up with a blank value when the filtered list does not contain the current legal status
            var currentStatusInFilteredList = (values.filter(function(codedEntry) {
                return codedEntry.code === currentLegalStatusOfAdopters
            }).length > 0);

            if (!currentStatusInFilteredList) {
                currentLegalStatusOfAdopters = "";
            }

            this._initLegalStatusOfAdopters(values, currentLegalStatusOfAdopters);
        },

        _updateDateWarningMessage: function() {
            var container = this.get('container'),
                dateShouldBePlaced = Y.USPDate.parseDate(container.one('#dateShouldBePlaced').get('value')),
                dateMatched = Y.USPDate.parseDate(container.one('#dateMatched').get('value'));

            if ((null != dateMatched) && (dateShouldBePlaced > dateMatched)) {
                this.get('container').one('#date-warning-wrapper').setStyle('display', 'block');
            } else {
                this.get('container').one('#date-warning-wrapper').setStyle('display', 'none');
            }
        },

        _updateMandatoryMarkers: function(value) {
            var mandatoryMarker = this.get('container').one('label[for="familyFindingActivity"] .mandatory');

            if (mandatoryMarker) {
                if (value) {
                    mandatoryMarker.setStyle('visibility', 'visible');
                } else {
                    mandatoryMarker.setStyle('visibility', 'hidden');
                }
            }
        },

        _updateOtherDisruptionReasonVisibility: function(value) {
            var otherDisruptionReasonWrapper = this.get('container').one('#otherDisruptionReasonWrapper');

            if (otherDisruptionReasonWrapper) {
                if (value === 'NO_FFOTHER') {
                    otherDisruptionReasonWrapper.setStyle('display', 'block');
                } else {
                    otherDisruptionReasonWrapper.one('#otherDisruptionReason').set('value', '');
                    otherDisruptionReasonWrapper.setStyle('display', 'none');
                }
            }
        }
    });

    Y.namespace('app.childlookedafter').NewAdoptionInformationForm = Y.Base.create('newAdoptionInformationForm',
        Y.usp.childlookedafter.NewAdoptionInformation, [Y.usp.ModelFormLink], {
            form: '#adoptionInformationAddForm'
        });

    Y.namespace('app.childlookedafter').NewAdoptionInformationView = Y.Base.create('NewAdoptionInformationView', Y.app.childlookedafter.BaseAdoptionInformationView, [], {
        template: Y.Handlebars.templates["adoptionInformationAddDialog"]
    });


    Y.namespace('app.childlookedafter').AdoptionInformationForm = Y.Base.create('adoptionInformationViewForm',
        Y.usp.childlookedafter.AdoptionInformation, [], {
            form: '#adoptionInformationViewForm'
        });


    Y.namespace('app.childlookedafter').AdoptionInformationView = Y.Base.create('AdoptionInformationView',
        Y.usp.childlookedafter.AdoptionInformationView, [], {
            template: Y.Handlebars.templates.adoptionInformationViewDialog
        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        gender: Y.uspCategory.childlookedafter.adopterGender.category.codedEntries,
                        legalStatus: Y.uspCategory.childlookedafter.adopterLegalStatus.category.codedEntries,
                        reasonNoLongerPlanned: Y.uspCategory.childlookedafter.reasonAdoptionNoLongerPlanned.category.codedEntries,
                        familyFindingActivity: Y.uspCategory.childlookedafter.familyFindingActivity.category.codedEntries
                    }
                }
            }
        });


    Y.namespace('app.childlookedafter').UpdateAdoptionInformationForm = Y.Base.create('updateAdoptionInformationForm',
        Y.usp.childlookedafter.AdoptionInformation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#adoptionInformationEditForm'
        });


    Y.namespace('app.childlookedafter').EditAdoptionInformationView = Y.Base.create('editAdoptionInformationView',
        Y.app.childlookedafter.BaseAdoptionInformationView, [], {
            template: Y.Handlebars.templates.adoptionInformationEditDialog,

            render: function() {
                Y.app.childlookedafter.EditAdoptionInformationView.superclass.render.call(this);

                this._updateDateWarningMessage();
                this._updateLegalStatusOfAdopterOptions(this.get('model').get('genderOfAdopters'));
                this._updateMandatoryMarkers(this.get('model').get('disruptionDate'));
                this._updateOtherDisruptionReasonVisibility(this.get('model').get('familyFindingActivity'));

                return this;
            }
        });

    Y.namespace('app.childlookedafter').AdoptionInformationDialog = Y.Base.create('adoptionInformationDialog', Y.usp.app.AppDialog, [], {
        handleAddAdoptionInformationSave: function(e) {
            this.handleSave(e, {
                personId: this.get('activeView').get('otherData.subject.subjectId')
            });
        },

        handleAddAdoptionInformationPeriodOfCareSave: function(e) {
            this.handleSave(e, {
                periodOfCareId: this.get('activeView').get('otherData.periodOfCareId')
            });
        },

        handleEditAdoptionInformationSave: function(e) {
            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateAdoptionInformation
            });
        },

        views: {
            add: {
                type: Y.app.childlookedafter.NewAdoptionInformationView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddAdoptionInformationSave',
                    disabled: true
                }]
            },
            addWithPeriodOfCare: {
                type: Y.app.childlookedafter.NewAdoptionInformationView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddAdoptionInformationPeriodOfCareSave',
                    disabled: true
                }]
            },
            view: {
                type: Y.app.childlookedafter.AdoptionInformationView,
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'editButton',
                    labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                    isActive: true,
                    action: function(e) {
                        var model = this.get('activeView').get('model');
                        this.fire('adoptionInformationDialog:showAdoptionInformationEditDialog', {
                            id: model.get('id'),
                            adoptionInformationId: model.get('id')
                        });
                    }
                }]
            },
            edit: {
                type: Y.app.childlookedafter.EditAdoptionInformationView,
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleEditAdoptionInformationSave',
                    disabled: true
                }]
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'event-custom',
        'calendar-popup',
        'handlebars',
        'handlebars-childlookedafter-templates',
        'model-form-link',
        'usp-childlookedafter-NewAdoptionInformation',
        'usp-childlookedafter-AdoptionInformation',
        'usp-childlookedafter-UpdateAdoptionInformation',
        'categories-childlookedafter-component-AdopterGender',
        'categories-childlookedafter-component-AdopterLegalStatus',
        'categories-childlookedafter-component-ReasonAdoptionNoLongerPlanned',
        'categories-childlookedafter-component-FamilyFindingActivity',
        'usp-date'
    ]
});