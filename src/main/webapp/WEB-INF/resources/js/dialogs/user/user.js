YUI.add('user-dialog-views', function(Y) {
    var A = Y.Array,
        E = Y.Escape,
        L = Y.Lang,
        O = Y.Object;
    var USER_LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div class="loading-main">Please wait....</div></div></div>';
    var USER_WARNINGS_TEMPLATE='<div class="pure-g-r"><div class="pure-u-1 model-errors"><div class="message contacts-warning-section user-contacts-warning-visibility message-warning"><span class="contacts-warning-summary"></span></div></div></div>';
    
    
    /*
     * Renders all the Roles choosers
     */
    var RolesView = Y.Base.create('RolesView', Y.View, [], {
        initializer: function() {
            this.after('teamsChange', this.render, this);
            this.after('personIdChange', this._personIdChange, this);
            this.teamsPicklists = [];
        },
        shown: function() {
            this.set('shown', true);
            var container = this.get('container');
            this.tabview = new Y.TabView({
                srcNode: container.one('#teams-tab-view')
            });
            this.tabview.render();
        },
        //The person has changed. Clear down the teams and await being given new teams
        _personIdChange: function(e) {
            this.set('teams', []);
        },
        _cleanUpUIComponents: function() {
            //remove any previous picklists
            A.each(this.teamsPicklists, function(picklist) {
                picklist.destroy();
            });
            this.teamsPicklists = [];
            //Handle re-renders
            if (this.tabview) {
                this.tabview.destroy();
                delete this.tabview;
            }
        },
        render: function() {
            var container = this.get('container'),
                teams = this.get('teams');
            this._cleanUpUIComponents();
            //render the initial HTML. It'll render the structure necessary to attach tabs to
            container.setHTML(Y.Handlebars.templates.userRoles({
                teams: teams
            }));
            //Initialise the No Teams picklist. This always appears
            this.noTeamsPicklist = this.initRolesPicklist(container.one('#noteam-content'), this.get('allRolesJSON'), this.get('roles'));
            //create a picklist for each team the person is a member of
            A.each(teams, function(team) {
                var rolesPicklist = this.initTeamRolesPicklist(container.one('#' + team.name + '-content'), this.get('allRolesJSON'), this.get('profileRoleIdsAndTeamIds'),
                    team.id);
                rolesPicklist.set('teamId', team.id);
                this.teamsPicklists.push(rolesPicklist);
            }, this);
            //TabViews can't be rendered until the container has been attached to the DOM, we'll wait for that. Bit of a hack.
            if (this.get('shown')) {
                this.tabview = new Y.TabView({
                    srcNode: container.one('#teams-tab-view')
                });
                this.tabview.render();
            }
            return this;
        },
        //convenience method to access the main roles data
        getNoTeamsRoles: function() {
            return this.noTeamsPicklist ? this.noTeamsPicklist.getSelectedValues() : [];
        },
        //scoop all the team/role assignments from the picklists and return them in a shape ready for sending to the server
        getTeamRoles: function() {
            var teamsPicklists = this.teamsPicklists || [],
                teamRoles = [];
            A.each(teamsPicklists, function(teamPicklist) {
                var teamId = teamPicklist.get('teamId');
                A.each(teamPicklist.getSelectedValues(), function(value) {
                    teamRoles.push({
                        roleId: value,
                        teamId: teamId
                    });
                });
            });
            return teamRoles;
        },
        destructor: function() {
            this._cleanUpUIComponents();
        },
        initRolesPicklist: function(pickListContainer, allRolesJSON, assignedRoles) {
            // set up the data for our roles picklist
            var assignedRoleIds = [],
                availableRoles = [],
                currentAssignedRoles = [],
                roleDisplay = '',
                roleTitle = '',
                roleClass = '';
            // set up the assignedRoleIds and currentAssignedRoles
            A.each(assignedRoles, function(roleData) {
                assignedRoleIds.push(roleData.id);
                roleDisplay = E.html(roleData.description);
                roleTitle = E.html(roleData.description);
                roleClass = '';
                if (!roleData.active) {
                    roleDisplay = '<span class="role-deactivated">' + roleDisplay + ' - deactivated</span>';
                    roleTitle = roleTitle + ' - deactivated';
                    roleClass = 'role-deactivated';
                }
                currentAssignedRoles.push({
                    id: roleData.id,
                    description: roleDisplay,
                    title: roleTitle,
                    className: roleClass
                });
            });
            // filter out the assigned and inactive roles to create the
            // availableRoles
            A.each(allRolesJSON, function(roleData) {
                if (A.indexOf(assignedRoleIds, roleData.id) === -1) {
                    roleDisplay = E.html(roleData.description);
                    roleTitle = E.html(roleData.description);
                    roleClass = '';
                    if (!roleData.active) {
                        roleDisplay = '<span class="role-deactivated">' + roleDisplay + ' - deactivated</span>';
                        roleTitle = roleTitle + ' - deactivated';
                        roleClass = 'role-deactivated';
                    }
                    availableRoles.push({
                        id: roleData.id,
                        description: roleDisplay,
                        title: roleTitle,
                        className: roleClass
                    });
                }
            });
            var rolesPicklist = new Y.usp.Picklist({
                optionsMap: {
                    text: 'description',
                    value: 'id',
                    title: 'title',
                    className: 'className'
                },
                options: availableRoles,
                stackMode: true,
                selectWidth: '100%',
                actionLabelOne: '>',
                actionLabelAll: '>>',
                actionLabelRmv: '<',
                actionLabelRmvAll: '<<'
            });
            // Setup the roles for picklist
            rolesPicklist.set('options', availableRoles);
            rolesPicklist.set('selections', currentAssignedRoles);
            rolesPicklist.set('assignedId', 'roleIds');
            rolesPicklist.render();
            // stick the picklist container into the form
            pickListContainer.appendChild(rolesPicklist.get('srcNode'));
            return rolesPicklist;
        },
        initTeamRolesPicklist: function(pickListContainer, allRolesJSON, teamRoles, teamId) {
            // set up the data for our roles picklist
            var assignedRoleIds = [],
                availableRoles = [],
                currentAssignedRoles = [],
                roleDisplay = '',
                roleTitle = '',
                roleClass = '',
                //Skim the list of teamroles to see if the role/team combo is present
                isRoleAssignedByTeam = function(teamRoles, role, team) {
                    return A.some(teamRoles, function(teamRole) {
                        return teamRole.teamId == team && teamRole.roleId == role;
                    });
                };
            // filter out the assigned and inactive roles to create the
            // availableRoles
            A.each(allRolesJSON, function(roleData) {
                if (A.indexOf(assignedRoleIds, roleData.id) === -1) {
                    roleDisplay = E.html(roleData.description);
                    roleTitle = E.html(roleData.description);
                    roleClass = '';
                    if (!roleData.active) {
                        roleDisplay = '<span class="role-deactivated">' + roleDisplay + ' - deactivated</span>';
                        roleTitle = roleTitle + ' - deactivated';
                        roleClass = 'role-deactivated';
                    }
                    if (isRoleAssignedByTeam(teamRoles, roleData.id, teamId)) {
                        currentAssignedRoles.push({
                            id: roleData.id,
                            description: roleDisplay,
                            title: roleTitle,
                            className: roleClass
                        });
                    } else {
                        availableRoles.push({
                            id: roleData.id,
                            description: roleDisplay,
                            title: roleTitle,
                            className: roleClass
                        });
                    }
                }
            });
            var rolesPicklist = new Y.usp.Picklist({
                optionsMap: {
                    text: 'description',
                    value: 'id',
                    title: 'title',
                    className: 'className'
                },
                options: availableRoles,
                stackMode: true,
                selectWidth: '100%',
                actionLabelOne: '>',
                actionLabelAll: '>>',
                actionLabelRmv: '<',
                actionLabelRmvAll: '<<'
            });
            // Setup the roles for picklist
            rolesPicklist.set('options', availableRoles);
            rolesPicklist.set('selections', currentAssignedRoles);
            rolesPicklist.set('assignedId', 'roleIds');
            rolesPicklist.render();
            // stick the picklist container into the form
            pickListContainer.appendChild(rolesPicklist.get('srcNode'));
            return rolesPicklist;
        }
    }, {
        ATTRS: {
            teams: {
                value: []
            },
            personId: {},
            shown: {
                value: false
            },
            roles: {
                value: []
            },
            profileRoleIdsAndTeamIds: {
                value: []
            }
        }
    });
    //AOP to make the PersonOrganisationRelationship aware of nested VO
    //TODO: Apply to PersonOrgRelsList instead?
    Y.Do.after(function() {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({
            key: 'organisationVO',
            locator: 'organisationVO'
        });
        return new Y.Do.AlterReturn(null, schema);
    }, Y.usp.relationship.PersonOrganisationRelationship, 'getSchema', Y.usp.relationship.PersonOrganisationRelationship);
    var PersonOrgRelsList = Y.Base.create('personOrgRelsList', Y.usp.relationship.PaginatedPersonOrganisationRelationshipList, [], {
        initializer: function(cfg) {
            //ModelLists don't seem to like substituting their attributes in URLs like Models do
            this.originalURL = cfg.url;
            this.after('personIdChange', function(e) {
                if (e.newVal) {
                    this.url = Y.Lang.sub(this.originalURL, this.getAttrs());
                    this.load();
                }
            }, this);
        }
    }, {
        ATTRS: {
            personId: {}
        }
    });
    
    function userDialogUtility(){}
  
    userDialogUtility.prototype={
        _showContactData : function(){
        	var container=this.get('container');
                if (this.get('workMobileContact') || this.get('workEmailContact')) {
                    if ((this.get('workMobileContact') === null ) || (this.get('workEmailContact') === null)) {
                        if (container.one('.contacts-warning-section')) {
                            container.one('.contacts-warning-section').removeClass('user-contacts-warning-visibility');
                            container.one('.contacts-warning-section .contacts-warning-summary').setHTML(this.get('labels').contactsWarningLabel);
                        }
                    } else {
                        if (container.one('.contacts-warning-section')) {
                            container.one('.contacts-warning-section').addClass('user-contacts-warning-visibility');
                        }
                    }
                    if (this.get('workMobileContact') !== null && this.get('workMobileContact') !== undefined) {
                        container.one('.workMobileContact .value').setHTML(this.get('workMobileContact').number);
                    }
                    if (this.get('workEmailContact') !== null && this.get('workEmailContact')  !== undefined) {
                        container.one('.workEmailContact .value').setHTML(this.get('workEmailContact').address);
                    }
                }
            }
    };
    
    // User Add View definition
    Y.namespace('app').UserAddView = Y.Base.create('userAddView', Y.usp.security.NewSecurityUserView, [userDialogUtility], {
        template: Y.Handlebars.templates.userAddDialog,
        initializer: function(config) {
                var autocompleteConfig=config.autocomplete||{};
            
            //bind change handler for autocomplete
            this.after('*:selectedPersonChange', this.afterSelectedPersonChange, this);
            this.after('selectedPersonIdChange', this._selectedPersonChange, this);
            
            this.personAutocompleteView = new Y.app.ProfessionalRolePersonAutoCompleteResultView({
                autocompleteURL: autocompleteConfig.autocompleteURL,
                preventSecuredSelection: false,
                labels: autocompleteConfig.labels
            });
            
            this.personAutocompleteView.addTarget(this);
          
            this.personTeams = new PersonOrgRelsList({
                url: this.get('personTeamURL')
            });
            this.teams = [];
            this.personTeams.on('load', this._teamsLoaded, this);
        },
        shown: function() {
            if (this.RolesView) {
                this.RolesView.shown();
            }
        },
        afterSelectedPersonChange:function(e){
          var person=e.newVal,
              personId=person?person.id:null;
          
          //set the selected person
          this.set('selectedPersonId', personId);
        },
        _selectedPersonChange: function(e) {
          var container=this.get('container'),
              personId=e.newVal;
          
          //clear down warnings
          container.one('#userWarnings').setHTML(USER_WARNINGS_TEMPLATE);
          container.one('.workMobileContact .value').setHTML('');
          container.one('.workEmailContact .value').setHTML('');
          //This triggers a load of the teams for a person
          this.personTeams.set('personId', personId);
            
            //tell the RolesView what the current person is
            this.RolesView.set('personId', personId);
            //Load only person is selected
            if (personId!==null && personId!==undefined) {                
                var mobileContacts=new Y.Promise(function(resolve, reject){
                    this.get('workMobileContactModel').url = L.sub(this.get('workMobileURL'), {
                      personId: personId,
                    });
                    this.get('workMobileContactModel').load(Y.bind(function(err, res) {
                      if (err) {
                          reject(err);
                      }
                      //This attribute needs to be used for displaying value and also showing warnings in case no mobile or multiple contacts found
                      this.set('workMobileContact', this.get('workMobileContactModel').toJSON());
                      resolve(this.get('workMobileContact'));
                  }, this));
                }.bind(this));
                
                var emailContacts=new Y.Promise(function(resolve, reject){
                  this.get('workEmailContactModel').url = L.sub(this.get('workEmailURL'), {
                    personId: personId
                  });
                  this.get('workEmailContactModel').load(Y.bind(function(err, res) {
                    if (err) {
                        reject(err);
                    }
                    //This attribute needs to be used for displaying value and also showing warnings in case no email address or multiple email address found
                    this.set('workEmailContact', this.get('workEmailContactModel').toJSON());
                    resolve(this.get('workEmailContact'));
                }, this));
                }.bind(this));
                
                Y.Promise.all([mobileContacts, emailContacts]).then(function(){
                  this._populateContacts();
                }.bind(this));
            }
        },
        _populateContacts: function() {
            var container = this.get('container');
            
            //If no mobile contact and 2 factor auth mode is enabled the , two factor option should be disabled
            if (this.get('workMobileContact') && this.get('systemConfigDetails').twoFactorSmsEnabled) {
                container.one('input[name="authMode"][value="PASSWORD_ONLY"]').set('checked', 'checked');
                container.one('input[name="authMode"][value="TWO_FACTOR_SMS"]').set('disabled', 'disabled');
            } else if (this.get('workMobileContact') && this.get('systemConfigDetails').twoFactorSmsEnabled) {
                container.one('input[name="authMode"][value="TWO_FACTOR_SMS"]').set('disabled', false);
            }
            this._showContactData();
        },
        _teamsLoaded: function() {
            var teams = [];
            this.personTeams.each(function(model) {
                if(!A.find(teams, function(team){
                    return team.id===model.get('organisationVO.id');
                })){
                    teams.push({
                        name: model.get('organisationVO.organisationIdentifier'),
                        label: model.get('organisationVO.name'),
                        id: model.get('organisationVO.id')
                    });
                }
            });
            this.teams = teams;
            this.RolesView.set('teams', teams);
        },
        getInput: function(inputId) {
            var container = this.get('container');
            return container.one('#addUser_' + inputId);
        },
        getWrapper: function(inputId) {
            var container = this.get('container');
            return container.one('#addUser_' + inputId + '_wrapper');
        },
        render: function() {
            var container = this.get('container'),
                modelData = this.get('model').toJSON(),
                today = new Date(),
                defaultPasswordExpireDate = new Date(),
                systemConfig = this.get('systemConfigDetails'),
                enableSecondaryPassword = systemConfig.secondaryPasswordEnabled,
                enableTwoFactor = this.get('enableTwoFactorAuthMode') ? this.get('enableTwoFactorAuthMode') : systemConfig.twoFactorSmsEnabled;
            var html = this.template({
                labels: this.get('labels'),
                modelData: modelData,
                enableSecondaryPassword: enableSecondaryPassword,
                enableTwoFactor: enableTwoFactor
            });
            container.setHTML(html);
            
            container.one('#addUser_wrapper').append(this.personAutocompleteView.render().get('container'));
            
            this.expiryDateCalendar = new Y.usp.CalendarPopup({
                // bind to expiryDate field
                inputNode: this.getInput('expiryDate')
            });
            this.expiryDateCalendar.render();
            this.RolesView = new RolesView({
                container: container.one('#user-tabs'),
                allRolesJSON: this.get('availableRolesJSON'),
                teams: this.teams
            }).render();
            // Set the password expiry date based on the default expiry duration
            defaultPasswordExpireDate.setDate(defaultPasswordExpireDate.getDate() + this.get('defaultPasswordExpiryDuration'));
            this.getInput('passwordExpiryDate').set('value', Y.USPDate.formatDateValue(defaultPasswordExpireDate));
            this.passwordExpiryDateCalendar = new Y.usp.CalendarPopup({
                // bind to passwordExpiryDate field
                inputNode: this.getInput('passwordExpiryDate'),
                // set minium date for the calender
                minimumDate: today
            });
            this.passwordExpiryDateCalendar.render();
            return this;
        },
        destructor: function() {
          if (this.personAutocompleteView) {
            this.personAutocompleteView.removeTarget(this);
            this.personAutocompleteView.destroy();
            delete this.personAutocompleteView;
          }          
          if (this.expiryDateCalendar) {
              // destroy the calendar
              this.expiryDateCalendar.destroy();
              delete this.expiryDateCalendar;
          }
          if (this.passwordExpiryDateCalendar) {
              // destroy the calendar
              this.passwordExpiryDateCalendar.destroy();
              delete this.passwordExpiryDateCalendar;
          }
          if (this.rolesPicklist) {
              // destroy the picklist
              this.rolesPicklist.destroy();
              delete this.rolesPicklist;
          }
        },
        teams: []
    }, {
        ATTRS: {
            systemConfigDetails: {
                value: {}
            },
            workMobileURL: {
                getter: function() {
                    return this.get('restUrlsForWorkContacts').workMobileContactsURL;
                }
            },
            workMobileContact: {},
            workMobileContactModel: {
                value: null,
                valueFn: function() {
                    return new Y.usp.contact.TelephoneContact({
                        url: this.get('restUrlsForWorkContacts').workMobileContactsURL
                    });
                }
            },
            workEmailURL: {
                getter: function() {
                    return this.get('restUrlsForWorkContacts').workEmailContactsURL;
                }
            },
            workEmailContact: {},
            workEmailContactModel: {
                value: null,
                valueFn: function() {
                    return new Y.usp.contact.EmailContact({
                        actualUrl: this.get('restUrlsForWorkContacts').workEmailContactsURL
                    });
                }
            },
            userDetailsEmailRequestModel: {
                value: null,
                valueFn: function() {
                    return new Y.usp.security.NewUserEmailRequest();
                }
            },
        }
    });
    Y.namespace('app').UserView = Y.Base.create('userView', Y.usp.person.SecurityUserWithRolesAndPersonView, [userDialogUtility], {
        // bind template
        template: Y.Handlebars.templates.userViewDialog,
        initializer: function(config) {
            this.after('contactsLoadedChange', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                model = this.get('model'),
                labels = this.get('labels'),
                roles = model.get('profileNames'),
                i;
            // call superclass
            Y.app.UserView.superclass.render.call(this);
            this.rolesContainer = container.one('#rolesContainer');
            if (!roles || roles.length === 0) {
                this.rolesContainer.append(labels.noRoles);
            } else {
                var roleOutputs = [];
                for (i = 0; i < roles.length; i++) {
                    var roleDisplay = roles[i];
                    roleOutputs.push(roleDisplay);
                }
                // output the roles as a comma separated list
                this.rolesContainer.append(roleOutputs.join(', '));
            }
            this._initAuthenticationModeValue();
            //Better way to do this but needs to be refactored later as part of all user dialogs
            this._loadContacts();
            return this;
        },
        _initAuthenticationModeValue: function() {
            var model = this.get('model'),
                labels = this.get('labels'),
                container = this.get('container'),
                authenticationModeValue = labels.authPasswordOnly;
            if (model.get('authMode') === 'SECONDARY_PASSWORD') {
                authenticationModeValue = labels.authOneAndAHalfFactor;
            }
            if (model.get('authMode') === 'TWO_FACTOR_SMS') {
                authenticationModeValue = labels.authTwoFactor;
            }
            container.one('.authenticationFactorValue .value').setHTML(authenticationModeValue);
        },
        _loadContacts: function() {
            var model = this.get('model'),
                person = model.get('person');
            if (person) {
            	var personId = person.id;
            	this.get('workMobileContactModel').url = L.sub(this.get('workMobileURL'), {
            		personId: personId,
            	});
                var mobileContactsLoaded = false,
                	emailContactsLoaded = false;
            	//Always present the contacts once both loaded even warnings also
            	this.get('workMobileContactModel').load(Y.bind(function(err, res) {
            		if (err) {
            			return console.log(err);
            		}
            		//This attribute needs to be used for displaying value and also showing warnings in case no mobile or multiple contacts found
            		this.set('workMobileContact', this.get('workMobileContactModel').toJSON());
            		mobileContactsLoaded = true;
            		this._populateContacts(mobileContactsLoaded, emailContactsLoaded);
            	}, this));
            	this.get('workEmailContactModel').url = L.sub(this.get('workEmailURL'), {
            		personId: personId,
            	});
            	this.get('workEmailContactModel').load(Y.bind(function(err, res) {
            		if (err) {
            			return console.log(err);
            		}
            		//This attribute needs to be used for displaying value and also showing warnings in case no email address or multiple email address found
            		this.set('workEmailContact', this.get('workEmailContactModel').toJSON());
            		emailContactsLoaded = true;
            		this._populateContacts(mobileContactsLoaded, emailContactsLoaded);
            	}, this));
            }
        },
        _populateContacts: function(mobileContactsLoaded, emailContactsLoaded) {
            if (mobileContactsLoaded || emailContactsLoaded) {
            	this._showContactData();
            }
        }
    }, {
        ATTRS: {
            systemConfigDetails: {
                value: {}
            },
            systemConfigDetailsURL: {
                value: ''
            },
            systemConfigDetailsModel: {
                value: {},
                valueFn: function() {
                    this.systemConfigDetailsModel = new Y.usp.security.SecurityConfiguration({
                        url: this.get('systemConfigDetailsURL')
                    });
                    this.systemConfigDetailsModel.load(Y.bind(function(err, res) {
                        if (err) {
                            return console.log(err);
                        }
                        this.set('systemConfigDetails', this.systemConfigDetailsModel.toJSON());
                    }, this));
                    return [];
                }
            },
            workMobileURL: {
                getter: function() {
                    return this.get('restUrlsForWorkContacts').workMobileContactsURL;
                }
            },
            workMobileContact: {},
            workMobileContactModel: {
                value: null,
                valueFn: function() {
                    return new Y.usp.contact.TelephoneContact({
                        url:this.get('restUrlsForWorkContacts').workMobileContactsURL
                    });
                }
            },
            workEmailURL: {
                getter: function() {
                    return this.get('restUrlsForWorkContacts').workEmailContactsURL;
                }
            },
            workEmailContact: {},
            workEmailContactModel: {
                value: null,
                valueFn: function() {
                    return new Y.usp.contact.EmailContact({
                        actualUrl: this.get('restUrlsForWorkContacts').workEmailContactsURL
                    });
                }
            },
        }
    });
    Y.namespace('app').UserUpdateView = Y.Base.create('userUpdateView', Y.usp.security.UpdateSecurityUserWithRolesAndTeamsView, [userDialogUtility], {
        // bind template
        template: Y.Handlebars.templates.userEditDialog,
        initializer: function() {
            this.personTeams = new PersonOrgRelsList({
                url: this.get('personTeamURL')
            });
            //When the User model has loaded and we have the person ID, request a list of that person's teams
            var model = this.get('model');
            model.on('load', function() {
                this.personTeams.set('personId', model.get('person.id')); //This will trigger a load
            }, this);
            //Once the list has loaded, handle the new team list data
            this.personTeams.on('load', this._teamsLoaded, this);
        },
        //The RolesView uses a TabView which can't be rendered until the dialog has been shown (inserted into the DOM)
        //shown() must be called in the showView() callback for it to work.
        shown: function() {
            if (this.RolesView) {
                this.RolesView.shown();
            }
        },
        _teamsLoaded: function() {
            var model = this.get('model');

            var teams = [];
            this.personTeams.each(function(model) {
                if(!A.find(teams, function(team){
                    return team.id===model.get('organisationVO.id');
                })){
                    teams.push({
                        name: model.get('organisationVO.organisationIdentifier'),
                        label: model.get('organisationVO.name'),
                        id: model.get('organisationVO.id')
                    });
                }
            });
                    
            this.RolesView.set('roles', model.get('roles'));
            this.RolesView.set('profileRoleIdsAndTeamIds', model.get('profileRoleIdsAndTeamIds'));
            this.RolesView.set('teams', teams);
        },
        getInput: function(inputId) {
            var container = this.get('container');
            return container.one('#editUser_' + inputId);
        },
        render: function() {
            var model = this.get('model'),
                container = this.get('container'),
                labels = this.get('labels'),
                systemConfig = this.get('systemConfigDetails'),
                enableSecondaryPassword = systemConfig.secondaryPasswordEnabled,
                enableTwoFactor = this.get('enableTwoFactorAuthMode') ? this.get('enableTwoFactorAuthMode') : systemConfig.twoFactorSmsEnabled;
            var html = this.template({
                labels: labels,
                modelData: model.toJSON(),
                targetIdentifier: this.get('targetIdentifier'),
                enableSecondaryPassword: enableSecondaryPassword,
                enableTwoFactor: enableTwoFactor,
                hasLdapUserName : model.get('ldapUserName')
            });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            this._loadContacts();
            this._initAuthenticationMode();
            // call render on expiryDate calendar
            this.expiryDateCalendar = new Y.usp.CalendarPopup({
                // bind to expiryDate field
                inputNode: this.getInput('expiryDate')
            });
            this.expiryDateCalendar.render();
            this.RolesView = new RolesView({
                container: container.one('#user-tabs'),
                allRolesJSON: this.get('availableRolesJSON'),
                teams: this.teams
            }).render();
            // call render on passwordExpiryDate calendar
            this.passwordExpiryDateCalendar = new Y.usp.CalendarPopup({
                // bind to passwordExpiryDate field
                inputNode: this.getInput('passwordExpiryDate')
            });
            this.passwordExpiryDateCalendar.render();
            return this;
        },
        _loadContacts: function() {
            if (this.get('model').get('person')) {
                var personId = this.get('model').get('person').id;
                this.get('workMobileContactModel').url = L.sub(this.get('workMobileURL'), {
                    personId: personId,
                });
                //Always present the contacts once both loaded even warnings also
                var mobileContactsLoaded = false,
                    emailContactsLoaded = false;
                this.get('workMobileContactModel').load(Y.bind(function(err, res) {
                    if (err) {
                        return console.log(err);
                    }
                    //This attribute needs to be used for displaying value and also showing warnings in case no mobile or multiple contacts found
                    this.set('workMobileContact', this.get('workMobileContactModel').toJSON());
                    mobileContactsLoaded = true;
                    this._populateContacts(mobileContactsLoaded, emailContactsLoaded);
                }, this));
                this.get('workEmailContactModel').url = L.sub(this.get('workEmailURL'), {
                    personId: personId,
                });
                this.get('workEmailContactModel').load(Y.bind(function(err, res) {
                    if (err) {
                        return console.log(err);
                    }
                    //This attribute needs to be used for displaying value and also showing warnings in case no email address or multiple email address found
                    this.set('workEmailContact', this.get('workEmailContactModel').toJSON());
                    emailContactsLoaded = true;
                    this._populateContacts(mobileContactsLoaded, emailContactsLoaded);
                }, this));
            }
        },
        _populateContacts: function(mobileContactsLoaded, emailContactsLoaded) {
            var container = this.get('container');
            if (mobileContactsLoaded || emailContactsLoaded) {
                //If  no mobile contact and 2 factor auth mode is enabled the , two factor option should be disabled
                if (!this.get('workMobileContact') || this.get('systemConfigDetails').twoFactorSmsEnabled) {
                    container.one('input[name="authenticationMode"][value="TWO_FACTOR_SMS"]').set('kd', 'disabled');
                } else if (this.get('workMobileContact') && this.get('systemConfigDetails').twoFactorSmsEnabled) {
                    container.one('input[name="authenticationMode"][value="TWO_FACTOR_SMS"]').set('disabled', false);
                }
                this._showContactData();
            }
        },
        _initAuthenticationMode: function() {
            var model = this.get('model'),
                container = this.get('container');
            if (model.get('authMode') && container.one('input[name="authenticationMode"][value=' + model.get('authMode') + ']')) {
                container.one('input[name="authenticationMode"][value=' + model.get('authMode') + ']').set('checked', 'checked');
            } else {
                container.one('input[name="authenticationMode"][value="PASSWORD_ONLY"]').set('checked', 'checked');
            }
        },
        destructor: function() {
            // destroy calendars
            if (this.expiryDateCalendar) {
                this.expiryDateCalendar.destroy();
                delete this.expiryDateCalendar;
            }
            if (this.passwordExpiryDateCalendar) {
                this.passwordExpiryDateCalendar.destroy();
                delete this.passwordExpiryDateCalendar;
            }
            if (this.rolesPicklist) {
                this.rolesPicklist.destroy();
                delete this.rolesPicklist;
            }
        }
    }, {
        ATTRS: {
            systemConfigDetails: {
                value: {}
            },
            workMobileURL: {
                getter: function() {
                    return this.get('restUrlsForWorkContacts').workMobileContactsURL;
                }
            },
            workMobileContact: {},
            workMobileContactModel: {
                value: null,
                valueFn: function() {
                    return new Y.usp.contact.TelephoneContact({
                        url: this.get('restUrlsForWorkContacts').workMobileContactsURL
                    });
                }
            },
            workEmailURL: {
                getter: function() {
                    return this.get('restUrlsForWorkContacts').workEmailContactsURL;
                }
            },
            workEmailContact: {},
            workEmailContactModel: {
                value: null,
                valueFn: function() {
                    return new Y.usp.contact.EmailContact({
                        actualUrl: this.get('restUrlsForWorkContacts').workEmailContactsURL
                    });
                }
            },
        }
    });
    Y.namespace('app').UserUpdatePasswordView = Y.Base.create('userUpdatePasswordView', Y.usp.security.UpdateSecurityUserAdminPasswordView, [], {
        // bind template
        template: Y.Handlebars.templates.changeUserPasswordDialog
    });
    Y.namespace('app').UserEnablementView = Y.Base.create('userEnablementView', Y.usp.person.SecurityUserWithRolesAndPersonView, [], {
        template: Y.Handlebars.templates.userEnablementDialog,
    });
    // Mix in the ModelFormLink to the NewUser model
    Y.namespace('app').NewUserForm = Y.Base.create('newUserForm', Y.usp.security.NewSecurityUser, [
        Y.usp.ModelFormLink
    ], {
        form: '#userAddForm',
        validate: function(attributes, callback) {
            var errs = {};
            var profileRoleAndTeamIds = attributes.profileRoleAndTeamIds || [],
                roleIds = attributes.roleIds || [];
            if (!profileRoleAndTeamIds.length && !roleIds.length) {
                errs['roleIds'] = 'Profiles are mandatory';
            }
            if (attributes.userSubjectId === undefined || attributes.userSubjectId === null || isNaN(attributes.userSubjectId)) {
                errs['addUser_personSearch'] = 'Person is mandatory';
            }
            if (O.keys(errs).length > 0) {
                return callback({
                    code: 400,
                    msg: {
                        validationErrors: errs
                    }
                });
            }
            //no errors
            return callback();
        }
    });
    // Model will be transmogrified into an UpdateUser
    Y.namespace('app').UpdateUserForm = Y.Base.create('updateUserForm', Y.usp.person.SecurityUserWithRolesAndPerson, [
        Y.usp.ModelFormLink, Y.usp.ModelTransmogrify
    ], {
        form: '#userEditForm',
        validate: function(attributes, callback) {
            var errs = {};
            var profileRoleAndTeamIds = attributes.profileRoleAndTeamIds || [],
                roleIds = attributes.roleIds || [];
            if (!profileRoleAndTeamIds.length && !roleIds.length) {
                errs['roleIds'] = 'Profiles are mandatory';
            }
            if (O.keys(errs).length > 0) {
                return callback({
                    code: 400,
                    msg: {
                        validationErrors: errs
                    }
                });
            }
            //no errors
            return callback();
        }
    });
    Y.namespace('app').UpdateUserPasswordForm = Y.Base.create('updateUserPasswordForm', Y.usp.security.UpdateSecurityUserAdminPassword, [
        Y.usp.ModelFormLink
    ], {
        form: '#changeUserPasswordForm'
    });
    var UserProcessingView = Y.Base.create('userProcessingView', Y.View, [], {
        render: function() {
            var container = this.get('container');
            container.setHTML(USER_LOADING_TEMPLATE);
            return this;
        }
    });
    // User Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').UserDialog = Y.Base.create('userDialog', Y.usp.app.AppDialog, [], {
        // Views that this dialog supports
        views: {
            userProcessing: {
                type: UserProcessingView
            },
            addUser: {
                type: Y.app.UserAddView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        // Get the view and the model
                        var view = this.get('activeView'),
                            labels = view.get('labels');
                        this.handleSave(e, {
                            // set the subject for this user
                            'userSubjectId': view.get('selectedPersonId'),
                            'subjectType': 'PERSON',
                            // set roles for no teams from RolesView                             
                            'roleIds': view.RolesView.getNoTeamsRoles(),
                            //for the freshers user they need to set the password on their own
                            'passwordReset': true,
                            // set roles for teams from RolesView
                            'profileRoleAndTeamIds': view.RolesView.getTeamRoles()
                        }).then(function(model) {
                            if (view.get('systemConfigDetails').emailLoginDetailsEnabled) {
                                // show the mask to process send login details                                    
                                var sendEmailModel = view.get('userDetailsEmailRequestModel');
                                sendEmailModel.setAttrs({
                                    userId: model.get('id'),
                                    password: model.get('password'),
                                    loginURL: view.get('appLoginURL'),
                                    //Trigger email sending part if only system is configured with it
                                });
                                sendEmailModel.url = L.sub(view.get('automaticEmailLoginDetailsURL'), {
                                    personId: model.get('id'),
                                });
                                sendEmailModel.save(Y.bind(function(err, response) {
                                    if (err === null) {
                                        Y.fire('infoMessage:message', {
                                            message: labels.addUserAndSendLoginCredentials
                                        });
                                    } else {
                                        Y.fire('infoMessage:message', {
                                            message: labels.addUserWarningForNoEmail,
                                            type: 'warning'
                                        });
                                    }
                                }, this));
                            }
                        }.bind(this));
                    },
                    disabled: true
                }, 'cancelButton']
            },
            viewUser: {
                type: Y.app.UserView,
                buttons: [{
                    action: function(e) {
                        e.preventDefault();
                        var model = this.get('activeView').get('model');
                        this.fire('changePassword', {
                            record: model
                        });
                    },
                    name: 'changePasswordButton',
                    labelHTML: 'Change password',
                    isPrimary: true,
                    classNames: 'pure-button-text pull-left',
                    disabled: true
                }, {
                    name: 'editButton',
                    labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
                    isActive: true,
                    action: function(e) {
                        e.preventDefault();
                        var model = this.get('activeView').get('model');
                        this.fire('edit', {
                            record: model
                        });
                    },
                    disabled: true
                }, 'cancelButton']
            },
            editUser: {
                type: Y.app.UserUpdateView,
                buttons: [{
                    action: function(e) {
                        e.preventDefault();
                        var model = this.get('activeView').get('model');
                        this.fire('changePassword', {
                            record: model
                        });
                    },
                    name: 'changePasswordButton',
                    labelHTML: 'Change password',
                    isText: true,
                    classNames: 'pull-left',
                    disabled: true
                }, {
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        var view = this.get('activeView'),
                            container = view.get('container');
                        //This is manual setting model with authmode is because disabled radio option though it is checked 
                        //not mapped with the value rather it is giving null, so 1.5/ 2 factor only will be disabled if the security config is not available
                        //on the system.
                        var authenticationMode = container.one('input[name="authenticationMode"]:checked').get('value');
                        // Manually set the 'resetSecondaryPassword' flag which is not present on our inbound VO
                        var resetSecondaryPassword = container.one('#editUser_resetSecPw').get('checked');
                        this.handleSave(e, {
                            'authMode': authenticationMode,
                            // set roles for no teams from RolesView
                            'roleIds': view.RolesView.getNoTeamsRoles(),
                            // set roles for teams from RolesView
                            'profileRoleAndTeamIds': view.RolesView.getTeamRoles(),
                            'resetSecondaryPassword': resetSecondaryPassword
                        }, {
                            transmogrify: Y.usp.security.UpdateSecurityUserWithRolesAndTeams
                        });
                    },
                    disabled: true
                }, 'cancelButton']
            },
            changeUserPassword: {
                type: Y.app.UserUpdatePasswordView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        this.handleSave(e, {
                            //Force the user navigate to renew password page when admin change the password
                            //manually
                            'passwordReset': true
                        });
                    },
                    disabled: true
                }, 'cancelButton']
            },
            disableUser: {
                type: Y.app.UserEnablementView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: 'handleSave',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            enableUser: {
                type: Y.app.UserEnablementView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: 'handleSave',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            }
        },
    }, {
        ATTRS: {
            //can't use default buttons because we don't always want a cancel button
            //specifically when presenting a 'yes/no' style dialog
            buttons: {
                value: ['closeButton']
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base', 
               'app-dialog', 
               'model-form-link', 
               'model-transmogrify', 
               'form-util', 
               'calendar-popup', 
               'handlebars-helpers', 
               'handlebars-user-templates', 
               'picklist',
               'usp-security-NewSecurityUser', 
               'usp-security-SecurityUserSummary', 
               'usp-person-SecurityUserWithRolesAndPerson', 
               'usp-security-UpdateSecurityUserWithRolesAndTeams',
               'usp-security-UpdateSecurityUserAdminPassword',
               'usp-security-SecurityRoleSummary',
               'usp-security-SecurityConfiguration',
               'popup-autocomplete-plugin',
               'categories-person-component-Gender',
               'usp-date',
               'usp-relationship-PersonOrganisationRelationship',
               'escape',
               'usp-contact-TelephoneContact',
               'usp-contact-EmailContact',
               'usp-security-NewUserEmailRequest',
               'tabview',
               'datasource-io',
               'dataschema-json',
               'person-autocomplete-view'
               ]
});