YUI.add('careleaver-dialog', function (Y) {

    'use-strict';

    var O = Y.Object,
        L = Y.Lang,
        BaseAddView,
        BaseView;

    BaseView = Y.Base.create('baseView', Y.usp.careleaver.CareLeaverView, [], {
        template: Y.Handlebars.templates["careLeaverViewDialog"]
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    careLeaverEligibility: Y.secured.uspCategory.careleaver.careLeaverEligibility.category.codedEntries,
                    reasonQualifying: Y.uspCategory.careleaver.reasonQualifying.category.codedEntries,
                    inTouch: Y.uspCategory.careleaver.inTouch.category.codedEntries,
                    mainActivity: Y.secured.uspCategory.careleaver.mainActivity.category.codedEntries,
                    accommodation: Y.secured.uspCategory.careleaver.accommodation.category.codedEntries,
                    accommodationSuitability: Y.uspCategory.careleaver.accommodationSuitability.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.careleaver').ViewCareLeaverView = Y.Base.create('viewCareLeaverView', BaseView, [], {
    });

    Y.namespace('app.careleaver').ViewWelshCareLeaverView = Y.Base.create('viewWelshCareLeaverView', BaseView, [], {
        render: function () {
            BaseView.superclass.render.call(this);
            var container = this.get('container'),
                reasonQualifyingNode = this.get('container').one('#reasonQualifyingWrapper'),
                multipleOccupancyNode = this.get('container').one('#multipleOccupancyWrapper');

            if (reasonQualifyingNode) {
                reasonQualifyingNode.setStyle('display', 'none');
            }
            if (multipleOccupancyNode) {
                multipleOccupancyNode.setStyle('display', 'none');
            }

            return this;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app.careleaver').NewCareLeaverForm = Y.Base.create('newCareLeaverForm', Y.usp.careleaver.NewCareLeaver, [Y.usp.ModelFormLink], {
        form: '#careLeaverAddForm'
    });

    BaseAddView = Y.Base.create('baseAddView', Y.View, [], {
        template: Y.Handlebars.templates["careLeaverAddDialog"],
        initializer: function () {
            this._evtHandlers = [
                this.get('container').delegate('change', this._handleInTouchChange, '#inTouch', this),
                this.get('container').delegate('change', this._handleEligibilityChange, '#careLeaverEligibility', this)
            ]
        },

        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels')
                });

            container.setHTML(html);

            this.contactDateCalendar = new Y.usp.CalendarPopup({
                inputNode: this.get('container').one('#contactDate'),
                'maximumDate': new Date()
            });
            this.contactDateCalendar.render();

            this._initCareLeaverEligibility();
            this._initInTouch();
            this._initMainActivity();
            this._initAccommodation();
            this._initAccommodationSuitability();

            return this;
        },

        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            if (this.contactDateCalendar) {
                this.contactDateCalendar.destroy();
                delete this.contactDateCalendar;
            }
        },

        _hideReasonQualifyingWrapper: function (value) {
            var reasonQualifyingWrapper = this.get('container').one('#reasonQualifyingWrapper');

            if (reasonQualifyingWrapper) {
                if (value === true) {
                    reasonQualifyingWrapper.setStyle('display', 'none');
                }
            }
        },

        _hideMultipleOccupancy: function (value) {
            var multipleOccupancyWrapper = this.get('container').one('#multipleOccupancyWrapper');

            if (multipleOccupancyWrapper) {
                if (value === true) {
                    multipleOccupancyWrapper.setStyle('display', 'none');
                }
            }
        },

        _initCareLeaverEligibility: function () {
            var careLeaverEligibilityNode = this.get('container').one('#careLeaverEligibility'),
                options = Y.Object.values(Y.secured.uspCategory.careleaver.careLeaverEligibility.category.getActiveCodedEntries());

            if (careLeaverEligibilityNode && options) {
                Y.FUtil.setSelectOptions(careLeaverEligibilityNode, options, null, null, true, true, 'code');
            }
        },
        _initReasonQualifying: function () {
            var reasonQualifyingNode = this.get('container').one('#reasonQualifying'),
                options = Y.Object.values(Y.uspCategory.careleaver.reasonQualifying.category.getActiveCodedEntries());

            if (reasonQualifyingNode && options) {
                Y.FUtil.setSelectOptions(reasonQualifyingNode, options, null, null, true, true, 'code');
            }
        },
        _initInTouch: function () {
            var inTouchNode = this.get('container').one('#inTouch'),
                options = Y.Object.values(Y.uspCategory.careleaver.inTouch.category.getActiveCodedEntries());

            if (inTouchNode && options) {
                Y.FUtil.setSelectOptions(inTouchNode, options, null, null, true, true, 'code');
            }
        },
        _initMainActivity: function () {
            var mainActivityNode = this.get('container').one('#mainActivity'),
                options = Y.Object.values(Y.secured.uspCategory.careleaver.mainActivity.category.getActiveCodedEntries(null, 'write'));

            if (mainActivityNode && options) {
                Y.FUtil.setSelectOptions(mainActivityNode, options, null, null, true, true, 'code');
            }
        },
        _initAccommodation: function () {
            var accommodationNode = this.get('container').one('#accommodation'),
                options = Y.Object.values(Y.secured.uspCategory.careleaver.accommodation.category.getActiveCodedEntries());

            if (accommodationNode && options) {
                Y.FUtil.setSelectOptions(accommodationNode, options, null, null, true, true, 'code');
            }
        },
        _initAccommodationSuitability: function () {
            var accommodationSuitabilityNode = this.get('container').one('#accommodationSuitability'),
                options = Y.Object.values(Y.uspCategory.careleaver.accommodationSuitability.category.getActiveCodedEntries());

            if (accommodationSuitabilityNode && options) {
                Y.FUtil.setSelectOptions(accommodationSuitabilityNode, options, null, null, true, true, 'code');
            }
        },
        _handleEligibilityChange: function (e) {
            var value = e.target.get('value');

            if (value === 'QUALIFYING') {
                this.get('container').one('#reasonQualifyingLabel').append('<span class="mandatory"> *</span>');
            } else {
                this.get('container').all('#reasonQualifyingLabel .mandatory').remove();
            }
        },
        _setSelectListToNull: function (node) {
            node.set('value', '');
        },
        _handleInTouchChange: function (e) {
            var value = e.target.get('value'),
                mainActivityNode = this.get('container').one('#mainActivity'),
                accommodationNode = this.get('container').one('#accommodation'),
                accommodationSuitabilityNode = this.get('container').one('#accommodationSuitability');

            if (value === 'YES') {
                this.get('container').all('.inTouchField').removeAttribute('disabled');
                this.get('container').all('.inTouchFieldLabel').append('<span class="mandatory"> *</span>');
            } else {
                if (mainActivityNode) {
                    this._setSelectListToNull(mainActivityNode);
                }
                if (accommodationNode) {
                    this._setSelectListToNull(accommodationNode);
                }
                if (accommodationSuitabilityNode) {
                    this._setSelectListToNull(accommodationSuitabilityNode);
                }
                this.get('container').all('.inTouchField').setAttribute('disabled', 'disabled');
                this.get('container').all('.inTouchFieldLabel .mandatory').remove();
            }
        }
    });

    Y.namespace('app.careleaver').NewCareLeaverView = Y.Base.create('NewCareLeaverView', BaseAddView, [], {
        render: function () {
            Y.app.careleaver.NewCareLeaverView.superclass.render.call(this);
            this._initReasonQualifying();

            return this;
        }
    });

    Y.namespace('app.careleaver').NewCareLeaverWelshView = Y.Base.create('newCareLeaverWelshView', BaseAddView, [], {
        render: function () {
            Y.app.careleaver.NewCareLeaverWelshView.superclass.render.call(this);
            this._hideMultipleOccupancy(true);
            this._hideReasonQualifyingWrapper(true);

            return this;
        }

    });

    Y.namespace('app.careleaver').CareLeaverDialog = Y.Base.create('careLeaverDialog', Y.usp.app.AppDialog, [], {
        handleAddCareLeaverSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                model = view.get('model'),
                labels = view.get('labels');

            return Y.when(this.handleSave(e, {
                personId: view.get('otherData').subject.subjectId
            }));
        },

        views: {
            add: {
                type: Y.app.careleaver.NewCareLeaverView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddCareLeaverSave',
                    disabled: true
                }]
            },
            view: {
                type: Y.app.careleaver.ViewCareLeaverView
            },
            addWelsh: {
                type: Y.app.careleaver.NewCareLeaverWelshView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddCareLeaverSave',
                    disabled: true
                }]
            },
            viewWelsh: {
                type: Y.app.careleaver.ViewWelshCareLeaverView
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'model-form-link',
        'entry-dialog-views',
        'handlebars',
        'handlebars-careleaver-templates',
        'calendar-popup',
        'usp-careleaver-NewCareLeaver',
        'usp-careleaver-CareLeaver',
        'secured-categories-careleaver-component-CareLeaverEligibility',
        'categories-careleaver-component-ReasonQualifying',
        'categories-careleaver-component-InTouch',
        'secured-categories-careleaver-component-MainActivity',
        'secured-categories-careleaver-component-Accommodation',
        'categories-careleaver-component-AccommodationSuitability',
        'handlebars-carerApproval-templates'
    ]
});