YUI.add('organisation-dialog-views', function (Y) {

    var L = Y.Lang,
        O = Y.Object;

    Y.namespace('app.organisation').EndOrganisationModel = Y.Base.create('endOrganisationModel', Y.usp.organisation.Organisation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#endOrganisationDialog'
    }, {
        ATTRS: {
            'endDate': {
                value: null,
                setter: function (val) {
                    var parsedDateTime = Y.USPDate.parseDateTime(val);
                    if (L.isDate(parsedDateTime)) {
                        return parsedDateTime.getTime();
                    }
                    return val;
                }
            }
        }
    });

    Y.namespace('app.organisation').EndOrganisationView = Y.Base.create('endOrganisationView', Y.usp.organisation.UpdateEndOrganisationView, [], {
        template: Y.Handlebars.templates['endOrganisationDialog'],
        initializer: function (config) {
            this.endDateCalendar = new Y.usp.CalendarPopup({
                minimumDate: new Date()
            }).addTarget(this);
        },
        render: function () {
            var model = this.get('model');
            var html = this.template({
                labels: this.get('labels'),
                isTeam: model.get('organisationType')
            });
            this.get('container').setHTML(html);
            this.endDateCalendar.set('inputNode', this.get('container').one('#endDate')).render();
            return this;
        }
    });

    Y.namespace('app.organisation').EndOrganisationDialog = Y.Base.create('endOrganisationDialog', Y.usp.app.AppDialog, [], {
        endOrganisationSubmit: function (e) {
            var view = this.get('activeView');
            var model = view.get('model');
            var transmogrify = view.get('transmogrify');
            model.url = Y.Lang.sub(view.get('organisationUrl'), { id: model.get('id') });

            this.handleSave(e, {}, { transmogrify: transmogrify }).then(function (model) {
                Y.fire('organisationDetailsChanged');
                Y.fire('contentContext:update', {
                    data: {
                        id: model.get('id'),
                        name: model.get('name'),
                        personId: model.get('organisationIdentifier'),
                    }
                });
            }.bind(this));
        },
        views: {
            endOrganisation: {
                type: Y.app.organisation.EndOrganisationView,
                buttons: [{
                    action: 'endOrganisationSubmit',
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> end',
                    isPrimary: true
                }]
            },
            endTeam: {
                type: Y.app.organisation.EndOrganisationView,
                buttons: [{
                    action: 'endOrganisationSubmit',
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> end',
                    isPrimary: true
                }]
            }
        }
    });
    var AddOrganisationOwnershipForm = Y.Base.create('addOrganisationOwnershipForm', Y.usp.organisation.NewOrganisationOwnership, [Y.usp.ModelFormLink], {
        form: '#addOrganisationOwnershipForm',
    });

    var AddOrganisationOwnershipView = Y.Base.create('addOrganisationOwnershipView', Y.usp.organisation.NewOrganisationOwnershipView, [], {
        template: Y.Handlebars.templates['addOrganisationOwnershipForm'],
        initializer: function (config) {
            this.startDateCalendar = new Y.usp.CalendarPopup().addTarget(this);
            this.organisationAutoCompleteResultView = new Y.app.OrganisationAutoCompleteResultView({
                autocompleteURL: this.get('organisationAutocompleteURL'),
                inputName: 'parentOrganisationId',
                formName: 'addOrganisationOwnershipForm',
                labels: {
                    placeholder: this.get('labels').organisationSearchPlaceholder
                }
            }).addTarget(this);
        },
        render: function () {
            var container = this.get('container');
            AddOrganisationOwnershipView.superclass.render.call(this);
            this.startDateCalendar.set('inputNode', container.one('#startDate')).render();
            container.one('#parentOrganisationIdWrapper').append(this.organisationAutoCompleteResultView.render().get('container'));
            return this;
        },
        destructor: function () {
            if (this.startDateCalendar) {
                this.startDateCalendar.removeTarget(this);
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }
            if (this.organisationAutoCompleteResultView) {
                this.organisationAutoCompleteResultView.removeTarget(this);
                this.organisationAutoCompleteResultView.destroy();
                delete this.organisationAutoCompleteResultView;
            }
        }

    }, {
        ATTRS: {
            organisationAutocompleteURL: {
                value: ''
            }
        }
    });

    var EditOrganisationForm = Y.Base.create('editOrganisationForm', Y.usp.organisation.Organisation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editOrganisationForm',
    });

    var EditOrganisationView = Y.Base.create('editOrganisationView', Y.usp.organisation.UpdateOrganisationView, [], {

        events: {
            '#mainTypeOrganisation-input': { change: '_onTypeChange' }
        },

        template: Y.Handlebars.templates['editOrganisationForm'],

        initializer: function (config) {
            this._evtHandlers = [];
            this._evtHandlers.push(this.after('organisationTypeChange', this._afterTypeChange, this));
        },

        render: function () {

            var html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                teamView: this.get('model').toJSON().organisationType === 'TEAM'
            });
            this.get('container').setHTML(html);

            this._initCodedEntries();
            this.set('organisationType', this.get('model').get('organisationType'), { silent: true });
            if (this.get('container').one('#subTypeOrganisation-input'))
                this.get('container').one('#subTypeOrganisation-input').removeAttribute('disabled');

        },

        _initCodedEntries: function () {

            var codedEntries = this.get('codedEntries');
            var container = this.get('container');
            var model = this.get('model');

            O.each(this.get('inputs'), function (value, selector) {
                var options = [];
                if (value.parentName) {
                    options = O.values(codedEntries[value.category].getActiveCodedEntriesForParent(model.get(value.parentName)));
                } else {
                    options = O.values(codedEntries[value.category].getActiveCodedEntries());
                }

                this._initSelectInput(container.one(selector), options, model.get(value.name));
            }, this);
        },

        _initSelectInput: function (node, options, selectedValue) {
            if (!node) {
                Y.log('cannot find <select></select> with given id', 'debug');
                return;
            }

            Y.FUtil.setSelectOptions(node, options, null, null, false, true, 'code');
            //ensure the current value is selected
            node.set('value', selectedValue || '');
        },

        /**
         * @method _onTypeChange
         * @description Handles user change of 'organistionType' and set the views
         * 	'organistionType' attribute
         *  
         */
        _onTypeChange: function (e) {
            this.set('organisationType', e.currentTarget.get('value'));
        },

        _afterTypeChange: function (e) {
            var container = this.get('container'),
                codedEntries = this.get('codedEntries'),
                subType = container.one('#subTypeOrganisation-input'),
                options = [];

            if (e.silent) { return; }

            if (e.newVal) {
                options = O.values(codedEntries.organisationSubType.getActiveCodedEntriesForParent(e.newVal));
                this._initSelectInput(subType, options);
                subType.removeAttribute('disabled');
            } else {
                this.get('model').set('type', '');
                this.get('model').set('subType', '');
                subType.set('value', '');
                subType.setAttribute('disabled', 'disabled');
            }

        }

    }, {

        ATTRS: {

            codedEntries: {
                value: {
                    organisationType: Y.uspCategory.organisation.organisationType.category,
                    organisationSubType: Y.uspCategory.organisation.organisationSubType.category
                }
            },

            inputs: {
                value: {
                    '#mainTypeOrganisation-input': {
                        category: 'organisationType',
                        name: 'type',
                        _type: 'select'
                    },
                    '#subTypeOrganisation-input': {
                        category: 'organisationSubType',
                        parentName: 'type',
                        name: 'subType',
                        _type: 'select'
                    }

                }

            },

            /**
             * @attribute organisationType
             * @description
             * 	'_onTypeChange' - saves every user change to this attribute.
             * 	'_afterTypeChange' - enables/disables the subType input depending on changes.
             *  
             */
            organisationType: {
                value: ''
            }
        }
    });

    var ChangeOrganisationNameView = Y.Base.create('changeOrganisationNameView', Y.usp.organisation.UpdateOrganisationView, [], {
        template: Y.Handlebars.templates['changeOrganisationNameForm'],
    });

    // Add contact View
    var NewContactView = Y.Base.create('NewContactView', Y.app.contact.NewSubjectContactView, [], {

        template: Y.Handlebars.templates['personContactAddDialog'],

        render: function () {
            var container = this.get('container'),
                //template=Y.Handlebars.compile(Y.one('#personContactAddDialog').getHTML()),

                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    targetPersonId: this.get('contextId'),
                    targetPersonName: this.get('contextName')
                });

            // Render this view's HTML into the container element.
            container.setHTML(html);
            this.initlists('contactType');
            this.initlists('contactUsage');
            this.initlists('socialMediaType');
            this.initlists('telephoneType');
            this.initContactVerifiedDate(container);
            return this;
        },

        aftercontactFixedType: function (e) {
            var input = this.getInput('contactFixedType');
            var value = input.get('value');

            //apply the background highlight to the context sensitive inputs
            Y.one('#contextSensitive-wrapper.inner-panel').removeClass('pseudo');

            // set flag to enable/disable the save button
            if (value) {
                this.set('saveEnabled', true);
            } else {
                this.set('saveEnabled', false);
            }

            this.setAttrs({
                fixedContactType: value
            }, { silent: true });

            switch (value) {
            case 'SOCIALMEDIA':
                this.getWrapper('socialMediaType').show();
                this.getWrapper('identifier').show();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').hide();
                this.getWrapper('contactUsage').show();
                break;
            case 'TELEPHONE':
                this.getWrapper('telephoneType').show();
                this.getWrapper('number').show();
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('address').hide();
                this.getWrapper('contactUsage').show();
                break;
            case 'EMAIL':
                this.getLabel('address').set('text', 'Email address');
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').show();
                this.getWrapper('contactUsage').show();
                break;
            case 'WEB':
                this.getLabel('address').set('text', 'Web address');
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').show();
                this.getWrapper('contactUsage').show();
                break;
            default:
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').show();
                break;
            }
        },
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            contextId: {},
            contextName: {},

            // flag to remember the state of the dialog save button.
            saveEnabled: {
                value: false
            },

            // holds the users contact type selection.
            // set in aftercontactFixedType wired to a change listener.
            fixedContactType: {
                value: null
            }
        }
    });

    var EditRemoveContactView = Y.Base.create('EditRemoveContactView', Y.app.contact.EditSubjectContactView, [], {
        initializer: function () {

            this.contactVerifiedDateCalendars = [];
            this._evtHandlers = [];
            this._evtHandlers.push(this.get('model').after('load', this.render, this));
        },

        // Specify attributes and static properties for your View here.
    }, {
        ATTRS: {

            model: {
                value: null
            },

            /**
             * @attribute contextId
             */
            contextId: {
                value: null
            },

            codedEntries: {
                value: {
                    contactType: Y.uspCategory.contact.contactType.category.codedEntries,
                    contactUsage: Y.uspCategory.contact.contactUsage.category.codedEntries,
                }
            }
        }
    });

    Y.namespace('app.organisation').EditOrganisationView = EditOrganisationView;
    Y.namespace('app.organisation').AddOrganisationOwnershipView = AddOrganisationOwnershipView;
    Y.namespace('app.organisation').ChangeOrganisationNameView = ChangeOrganisationNameView;
    Y.namespace('app.organisation').NewContactView = NewContactView;
    Y.namespace('app.organisation').EditRemoveContactView = EditRemoveContactView;
    Y.namespace('app.organisation').EditOrganisationForm = EditOrganisationForm;
    Y.namespace('app.organisation').AddOrganisationOwnershipForm = AddOrganisationOwnershipForm;

}, '0.0.1', {
    requires: ['yui-base',
        'form-util',
        'handlebars',
        'handlebars-helpers',
        'handlebars-organisation-templates',
        'handlebars-person-templates',
        'event-custom',
        'info-message',
        'calendar-popup',
        'usp-organisation-UpdateOrganisation',
        'usp-organisation-Organisation',
        'usp-organisation-OrganisationOwnership',
        'usp-organisation-NewOrganisationOwnership',
        'app-contact-details-view',
        'usp-contact-Contact',
        'usp-contact-NewContact',
        'usp-contact-NewEmailContact',
        'usp-contact-NewWebContact',
        'usp-contact-NewSocialMediaContact',
        'usp-contact-NewTelephoneContact',
        'categories-contact-component-SocialMediaType',
        'categories-contact-component-TelephoneType',
        'categories-organisation-component-OrganisationType',
        'categories-organisation-component-OrganisationSubType',
        'organisation-autocomplete-view',
        'usp-organisation-UpdateEndOrganisation',
        'usp-organisation-UpdateEndTeam',
        'usp-date',
        'subject-contact-views'
    ]
});