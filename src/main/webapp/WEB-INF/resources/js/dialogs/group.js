YUI.add('group-dialog-views', function (Y) {

	var E=Y.Escape,L=Y.Lang,A=Y.Array,O=Y.Object;

	//AOP to add additional schema values to PersonGroupMembershipDetailsWithAddress
    Y.Do.after(function(){
           var schema=Y.Do.originalRetVal;
           schema.resultFields.push({
                 key:'address',
                 locator:'address'
           });
           return new Y.Do.AlterReturn(null, schema);
    }, Y.usp.relationshipsrecording.PersonGroupMembershipDetailsWithAddressWarningCount, 'getSchema', Y.usp.relationshipsrecording.PersonGroupMembershipDetailsWithAddressWarningCount);

	// New Group View
	//var NewGroupTodayView=Y.Base.create('NewGroupTodayView', Y.usp.group.NewGroupTodayView,[],{
      var NewGroupWithDateView=Y.Base.create('NewGroupWithDateView', Y.usp.group.NewGroupWithDateView,[],{
    			//bind template
					template: Y.Handlebars.templates["groupAddDialog"],
		 			initializer: function (config) {
	            this.startDateCalendar = new Y.usp.CalendarPopup({
	            	maximumDate: new Date()
	            }).addTarget(this);
	        },

		render: function() {

            // superclass call
		    this.constructor.superclass.render.call(this);

		    //init groups
			this.initGroupTypes();
			this.startDateCalendar.set('inputNode', this.get('container').one('#startDate-input')).render();
			return this;
		},
		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#'+inputId+'-input');
		},
		initGroupTypes: function(){
			Y.FUtil.setSelectOptions(this.getInput('groupType'), O.values(Y.uspCategory.group.groupType.category.getActiveCodedEntries()), null, null, true, true, 'code');
		}
	});

	// Extend the USP GroupView
	var ViewGroupView=Y.Base.create('ViewGroupView', Y.usp.group.GroupView,[],{
		//bind template
		template:Y.Handlebars.templates["groupViewDialog"],
		initializer: function() {
			this.groupMembers = new Y.usp.relationshipsrecording.PaginatedPersonGroupMembershipDetailsWithAddressWarningCountList({
    			url : L.sub(this.get('groupMembersURL'), {
					id: this.get('groupId')
						})
    	});
			this.groupMembers.after('*:load', this.loadMembers, this);
			this.groupMembers.load();
		},
		render: function () {
			var container = this.get('container'),

			html = this.template({
				labels: this.get('labels'),
				modelData: this.get('model').toJSON(),
				otherData: this.get('otherData'),
				codedEntries: this.get('codedEntries'),
				groupMembersData : this.groupMembers.toJSON()
			});

			// Render this view's HTML into the container element.
			container.setHTML(html);
			return this;
		},
		loadMembers : function(e) {
			this.render();
			Y.fire('groupDialog:render',{});
		}
	},{
        ATTRS:{
            codedEntries:{
                value:{
                    groupTypes:Y.uspCategory.group.groupType.category.codedEntries,
                    closeReasons:Y.uspCategory.group.closeReason.category.codedEntries,
                    ethnicity:Y.uspCategory.person.ethnicity.category.codedEntries,
                    gender:Y.uspCategory.person.gender.category.codedEntries
                }
            },

            groupId : {
            	value : ''
            },

            groupMembers : {
            	valueFn: function(){
	            	return  new Y.usp.relationshipsrecording.PaginatedPersonGroupMembershipDetailsWithAddressWarningCountList({
	        			url : L.sub(this.get('groupMembersURL'), {
							id: this.get('groupId')
						})
	        	    });
            	}
						},
            groupMembersURL : {
							value : ''
						}
        }
	});

	// Extend the USP NewGroupTodayView
	var UpdateGroupView=Y.Base.create('UpdateGroupView', Y.usp.group.UpdateGroupView,[],{
		//bind template
		template:Y.Handlebars.templates["groupEditDialog"],

		render: function() {
            // superclass call
            this.constructor.superclass.render.call(this);

			this.initGroupTypes();

			return this;
		},

		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#'+inputId+'-input');
		},

		initGroupTypes: function(){
			var model=this.get('model'),
			    groupTypeInput=this.getInput('groupType'),
		         //is there an existing group type picked?
			    existingGroupType=this._lookupExistingEntry(Y.uspCategory.group.groupType.category.codedEntries, model.get('groupType'));

			Y.FUtil.setSelectOptions(groupTypeInput, O.values(Y.uspCategory.group.groupType.category.getActiveCodedEntries()), existingGroupType.code, existingGroupType.name, true, true, 'code');

		    //select value
			groupTypeInput.set('value', model.get('groupType'));
		},
		_lookupExistingEntry: function(codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        }
	});

	var OpenGroupView=Y.Base.create('openGroupView', Y.usp.group.GroupView,[],{
		template:Y.Handlebars.templates["groupReopenDialog"]
	},{	ATTRS:{
      	codedEntries:{
                value:{
                  groupTypes:Y.uspCategory.group.groupType.category.codedEntries,
                  closeReasons:Y.uspCategory.group.closeReason.category.codedEntries
                }}}});

	// Extend the USP UpdateCloseGroupTodayView
	var UpdateCloseGroupView=Y.Base.create('UpdateCloseGroupView', Y.usp.group.UpdateCloseGroupTodayView,[],{
		// bind the template
		template:Y.Handlebars.templates["groupCloseDialog"],

		render: function() {
		    /* TODO - As this is setting HTML don't call the super class - RG - discuss
			UpdateCloseGroupView.superclass.render.call(this);
			*/

		    /* TODO - RG Actually you do really want to call the superclass,
		        so find another way of passing your data
		        through to the template - the contents of initMembersSize is not good. */
			this.initMembersSize();
			this.initCloseGroupReasons();
		},

		initMembersSize: function(){

			var activeMemberSize = this.get('activeMembers').toJSON().length;
			var pendingMemberSize= this.get('pendingMembers').toJSON().length;
			var onlyPendingMembers = false;
			if(pendingMemberSize>0 && activeMemberSize===0){
				onlyPendingMembers=true;
			}

			var container = this.get('container'),
			html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                otherData: this.get('otherData'),
                codedEntries: this.get('codedEntries'),


				membershipsLink: this.get('membershipsLink'),
				onlyPendingMembers:onlyPendingMembers,
				activeMemberSize: activeMemberSize,
				pendingMemberSize:pendingMemberSize

			});
			// Render this view's HTML into the container element.
			container.setHTML(html);
		},

		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#'+inputId+'-input');
		},
		initCloseGroupReasons: function(){
		    Y.FUtil.setSelectOptions(this.getInput('closeReason'), O.values(Y.uspCategory.group.closeReason.category.getActiveCodedEntries()), null, null, true, true, 'code');
		}
	},{
	    ATTRS:{
            codedEntries:{
                value:{
                    groupTypes:Y.uspCategory.group.groupType.category.codedEntries
                }
            }
        }
	});

	// Extend the USP GroupView, for Group Deletion
	var DeleteGroupView=Y.Base.create('DeleteGroupView', Y.usp.group.GroupView,[],{
		// bind the template
		template:Y.Handlebars.templates["groupDeleteDialog"],

		render: function() {
		    /* TODO - As you are setting the container HTML in initMembersSize
		      There is no point calling the superclass.
		      But ideally you should allow the superclass to render,
		      Why can't this activeMembers/pendingMembers be calculated
		      on the model with a getter?
		     */
		    /*
			DeleteGroupView.superclass.render.call(this);
			*/
			this.initMembersSize();
		},

		initMembersSize: function(){

			var activeMemberSize = this.get('activeMembers').toJSON().length;
			var pendingMemberSize= this.get('pendingMembers').toJSON().length;
			var onlyPendingMembers = false;
			if(pendingMemberSize>0 && activeMemberSize===0){
				onlyPendingMembers=true;
			}

			var container = this.get('container'),
			html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                otherData: this.get('otherData'),

				membershipsLink: this.get('membershipsLink'),
				onlyPendingMembers:onlyPendingMembers,
				activeMemberSize: activeMemberSize,
				pendingMemberSize:pendingMemberSize
			});
			// Render this view's HTML into the container element.
			container.setHTML(html);
		}
	},{
        ATTRS:{
            codedEntries:{
                value:{
                    groupTypes:Y.uspCategory.group.groupType.category.codedEntries,
                    closeReasons:Y.uspCategory.group.closeReason.category.codedEntries
                }
            }
        }
    });

	// New PersonGroupMembership View
	var NewPersonGroupMembershipView=Y.Base.create('NewPersonGroupMembershipView', Y.View,[],{
		events: {
			'#group-member-joinDate': {change: '_setJoinDateFrom', keyup:'_ieSetDateOnEnter'},
			'form':{
			    submit: '_preventSubmit'
			}
		},
        _preventSubmit: function(e) {
            e.preventDefault();
        },
		//bind template
		template: Y.Handlebars.templates["groupAddMembershipDialog"],
		initializer:function(config){
		    var labels=config.labels||{};

		    this.personAutoCompleteResultView=new Y.app.PersonAutoCompleteResultView({
		        labels:{
		            placeholder:labels.personSearchPlaceholder,
		            noResults:'No results found - please check your criteria',
		            change:'change'
		        },
		        autocompleteURL:config.autocompleteURL,
                inputName:'ownerId',
                formName:'groupAddMemberForm',
                preventSecuredSelection: true
		    });

		    this.personAutoCompleteResultView.addTarget(this);

		    this.personAutoCompleteResultView.after('*:selectedPersonChange', this._handleSelectedPersonChange, this);
		},
		_handleSelectedPersonChange:function(e){
		    var selectedPerson=e.newVal,
		        container=this.get('container');

		    if(!selectedPerson){
	          // Reset autocomplete if change button is clicked
                container.one('.hiddenNarrative').setStyle('display', 'none');
                container.one('.groupId').setStyle('display', 'inline-block');
		    }else{
		        var personSelectedMessage = E.html(selectedPerson.name +' ('+selectedPerson.personIdentifier+') '+this.get('labels').membershipLabels.selectMembershipMessage+' '+this.get('targetGroupName')+' ('+this.get('targetGroupIdentifier')+')');
                container.one('.memberDetails').setHTML(personSelectedMessage);
                container.one('.hiddenNarrative').setStyle('display', 'block');
                container.one('.groupId').setStyle('display', 'none');
		    }
		},
		render: function() {
			var container = this.get('container'),

			html = this.template({
				labels:this.get('labels'),
				modelData:this.get('model').toJSON(),
				currentDate:new Date(),
				targetGroupId:this.get('targetGroupId'),
				targetGroupName:this.get('targetGroupName'),
				targetGroupIdentifier:this.get('targetGroupIdentifier'),
				targetGroupStartDate:this.get('targetGroupStartDate')
			});
			// Render this view's HTML into the container element.
			container.setHTML(html);

			container.one('#personSearch').append(this.personAutoCompleteResultView.render().get('container'));

			this.joinDateCalendar=new Y.usp.CalendarPopup({
				inputNode:this.getInput('member-joinDate')
			});

			this.joinDateCalendar.render();

			this._calEvtHandlers=[
			                      this.joinDateCalendar.on('dateSelection', Y.bind(this._setDateOnSelection, this))
			                      ];

			return this;
		},

		destructor: function(){
			var calNode=this.getInput('member-joinDate');

			if(this.joinDateCalendar){
				//destroy calendar
			    this.joinDateCalendar.destroy();
			    delete this.joinDateCalendar;
			}

			if(calNode){
				//destroy node
				calNode.destroy();
				calNode=null;
			}

			if(this.personAutoCompleteResultView){
			    this.personAutoCompleteResultView.removeTarget(this);
			    this.personAutoCompleteResultView.destroy();
			}

            //unbind event handlers
            if (this._calEvtHandlers) {
                A.each(this._calEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._calEvtHandlers = null;
            }
		},

		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#group-'+inputId);
		},

		// if carriage return been clicked
        _ieSetDateOnEnter: function(e) {
            if (e.keyCode == 13) {
            	this._setJoinDateFrom(e);
            }
        },

        // if the value in the join date field changed.
        _setJoinDateFrom:function(e){
        	//move focus out of field
            e.currentTarget.blur();
            var dateString = e.currentTarget.get("value");
            //move focus back in
            e.currentTarget.focus();
            if (dateString.length) {
                var parsedDate = Y.USPDate.parseDate(dateString);
                this._displayMembershipJoinDateBeforeGroupStartDateMessage(parsedDate);
            }
        },

        // if calendar selection made
        _setDateOnSelection:function(e){
        	var parsedDate = Y.USPDate.parseDate(e.date);
        	this._displayMembershipJoinDateBeforeGroupStartDateMessage(parsedDate);
        },

        _displayMembershipJoinDateBeforeGroupStartDateMessage:function(joinDate){
            if(joinDate!=null){
            	var container = this.get('container');
            	var formattedGroupStartDate = this.get('targetGroupStartDate');
            	joinDate.setHours(0,0,0);
            	if(joinDate.getTime()<formattedGroupStartDate){
            		container.one('.hiddenWarning').setStyle('display', 'block');
            	}else{
            		container.one('.hiddenWarning').setStyle('display', 'none');
            	}
            }
        }
	},{
	        // Specify attributes and static properties for your View here.
	        ATTRS: {
	        	targetGroupId:{},
	        	targetGroupName:{},
	        	targetGroupStartDate:{},
	        	targetGroupIdentifier:{},
	        	autocompleteURL:{},
	        	rootURL:{}
	        }
	});


	var UpdateCloseMembershipGroupView=Y.Base.create('UpdateCloseMembershipGroupView', Y.usp.person.PersonGroupMembershipDetailsView,[],{
		//bind template
		template:Y.Handlebars.templates["groupCloseMembershipDialog"],
		initializer:function(){
		    //create leave date calendar
		  this.leaveDateCalendar=new Y.usp.CalendarPopup();
		},
		render: function(){
			var container = this.get('container'),
			model=this.get('model'),
			minDate = model.get('joinDate'),
			maxDate=model.get('groupVO').closeDate;

			// call superclass
			this.constructor.superclass.render.call(this);

			this.leaveDateInput=container.one('#group-membership-leaveDate');

			this.initLeaveGroupReasons();

            //configure the date calendar input node
            this.leaveDateCalendar.set('inputNode', this.leaveDateInput);
            //supply a Date for minimum range
            this.leaveDateCalendar.set('minimumDate', minDate);
            //supply a Date for maximum range
            this.leaveDateCalendar.set('maximumDate', maxDate);
            //render the calendar
            this.leaveDateCalendar.render();
		},
		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#'+inputId+'-input');
		},
		initLeaveGroupReasons : function(){
			Y.FUtil.setSelectOptions(this.getInput('leaveReason'), O.values(Y.uspCategory.group.leaveReason.category.getActiveCodedEntries()), null, null, true, true, 'code');
		},
		destructor:function(){
			if(this.leaveDateCalendar){
				//destroy calendar
			    this.leaveDateCalendar.destroy();
				delete this.leaveDateCalendar;
			}

			if(this.leaveDateInput){
			    this.leaveDateInput.destroy();
			    delete this.leaveDateInput;
			}
		}
	});

	// Extend the USP PersonGroupMembershipDetailsView, for Membership Deletion
	var DeleteMembershipGroupView=Y.Base.create('DeleteMembershipGroupView', Y.usp.person.PersonGroupMembershipDetailsView,[],{
		// bind the template
		template:Y.Handlebars.templates["groupDeleteMembershipDialog"]
	},{
		ATTRS:{
		    codedEntries:{
                value:{
                    leaveReasons:Y.uspCategory.group.leaveReason.category.codedEntries
                }
            }
		}
	});

	//Y.namespace('app.group').NewGroupTodayView=NewGroupTodayView;
	Y.namespace('app.group').NewGroupWithDateView=NewGroupWithDateView;
	Y.namespace('app.group').ViewGroupView=ViewGroupView;
	Y.namespace('app.group').UpdateGroupView=UpdateGroupView;
	Y.namespace('app.group').OpenGroupView=OpenGroupView;
	Y.namespace('app.group').UpdateCloseGroupView=UpdateCloseGroupView;
	Y.namespace('app.group').NewPersonGroupMembershipView=NewPersonGroupMembershipView;
	Y.namespace('app.group').DeleteGroupView=DeleteGroupView;
	Y.namespace('app.group').UpdateCloseMembershipGroupView=UpdateCloseMembershipGroupView;
	Y.namespace('app.group').DeleteMembershipGroupView=DeleteMembershipGroupView;
}, '0.0.1', {
	requires: ['yui-base',
	           'event-custom',
	           'handlebars-helpers',
	           'handlebars-group-templates',
	           'usp-group-NewGroupWithDate',
	           'usp-group-Group',
	           'usp-group-UpdateGroup',
	           'usp-group-UpdateCloseGroupToday',
	           'usp-person-NewPersonGroupMembership',
	           'form-util',
	           'calendar-popup',
	           'usp-person-PersonGroupMembershipDetails',
	           'categories-group-component-LeaveReason',
	           'usp-date',
	           'categories-person-component-Gender',
	           'categories-group-component-CloseReason',
	           'categories-group-component-GroupType',
	           'person-autocomplete-view',
	           'usp-relationshipsrecording-PersonGroupMembershipDetailsWithAddressWarningCount',
	           'categories-person-component-Ethnicity',
	   			'categories-person-component-Gender',
	   			'escape'
	           ]
});
