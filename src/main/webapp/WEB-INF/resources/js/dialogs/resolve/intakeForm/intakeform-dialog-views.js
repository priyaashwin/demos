YUI.add('intakeform-dialog-views', function(Y) {
    var FU = Y.FUtil,
        A = Y.Array,
        setEnumVal = function(v) {
            if ('' === v) {
                return null;
            }
            return v;
        },
        L = Y.Lang,
        O = Y.Object,
        editFormButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model'),
                    transmogrify = view.get('transmogrify'),
                    pathSuffix = view.get('pathSuffix');

                if (pathSuffix !== undefined && model.url.indexOf(pathSuffix, model.url.length - pathSuffix.length) === -1) {
                    model.url = model.url + '/' + pathSuffix;
                }

                // Do the save
                model.save({
                    transmogrify: transmogrify
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();

                    if (err === null) {
                        this.hide();

                        //fire saved event against the view
                        view.fire('saved', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editFormAppendixButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model'),
                    transmogrify = view.get('transmogrify'),
                    appendixType = view.get('appendixType');

                // Do the save
                model.url = view.get('saveUrl');
                model.save({
                    transmogrify: transmogrify
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();

                    if (err === null) {
                        this.hide();

                        //fire saved event against the view
                        view.fire('savedAppendix', {
                            appendixType: appendixType
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editFormRejectionButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.url = view.get('saveUrl');
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateFormRejection
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();

                    if (err === null) {
                        this.hide();

                        //fire saved event against the view
                        view.fire('saved', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editSocialButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model'),
                    updateModel,
                    pathSuffix = view.get('pathSuffix'),
                    transmogrifiedData = {},
                    form = Y.one(model.form),
                    socialEvents = model.get('socialEvents'),
                    elem, name;

                // Handle the social events timetable
                form.get('elements').each(function(formElem, index) {
                    elem = formElem.getDOMNode();
                    name = elem.name;
                    if (name === 'event' + index) {
                        // special processing of socialEvents form inputs');
                        var updateEvent=new  Y.usp.resolveassessment.UpdateIntakeTimetableEvent({
                            event:elem.value,
                            objectVersion:socialEvents[index].objectVersion,                            
                            id:socialEvents[index].id
                        })

                        socialEvents[index]=updateEvent.toJSON();
                    }
                });
                transmogrifiedData['socialEvents'] = socialEvents;

                // Include the two rich text inputs
                transmogrifiedData['socialCurrentArrangementsIssues'] = view.getEditor(Y.one('#intakeEditForm_socialCurrentArrangementsIssues')).getData();
                transmogrifiedData['socialSupportRequired'] = view.getEditor(Y.one('#intakeEditForm_socialSupportRequired')).getData();

                // Set the id and version
                transmogrifiedData.id = model.get('id');
                transmogrifiedData.objectVersion = model.get('objectVersion');

                updateModel = new Y.usp.resolveassessment.UpdateIntakeSocial(transmogrifiedData);
                updateModel.url = model.url + '/' + pathSuffix;

                //bubble events from the update model to this
                updateModel.addTarget(this);
                
                // Do the save
                updateModel.save({
                    view: view
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved social event against the view
                        //not using just 'saved' here as we want to trigger 
                        //a reload of the timetable events
                        view.fire('savedSocial');
                    }
                    
                    //remove this as event target
                    updateModel.removeTarget(this);
                }, this));
            },
            disabled: true
        }],
        addPreviousPlacementButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved previous placement event against the view
                        //not using just 'saved' here as don't want to trigger 
                        //an entire intakeForm reload
                        view.fire('savedPreviousPlacement');
                    }
                }, this));
            },
            disabled: true
        }],
        editPreviousPlacementButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIntakePreviousPlacement
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedPreviousPlacement event against the view
                        view.fire('savedPreviousPlacement', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removePreviousPlacementButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removedPreviousPlacement event against the view
                        view.fire('removedPreviousPlacement', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        addIdentifiedRiskButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved previous placement event against the view
                        //not using just 'saved' here as don't want to trigger 
                        //an entire intakeForm reload
                        view.fire('savedIdentifiedRisk');
                    }
                }, this));
            },
            disabled: true
        }],
        editIdentifiedRiskButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeRiskAssessment
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedIdentifiedRisk event against the view
                        view.fire('savedIdentifiedRisk', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removeIdentifiedRiskButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removedIdentifiedRisk event against the view
                        view.fire('removedIdentifiedRisk', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        addSupportPlanButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved previous placement event against the view
                        //not using just 'saved' here as don't want to trigger 
                        //an entire intakeForm reload
                        view.fire('savedSupportPlan');
                    }
                }, this));
            },
            disabled: true
        }],
        editSupportPlanButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeSupportPlan
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedSupportPlan event against the view
                        view.fire('savedSupportPlan', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removeSupportPlanButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removedSupportPlan event against the view
                        view.fire('removedSupportPlan', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        addMedicationButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved previous placement event against the view
                        //not using just 'saved' here as don't want to trigger 
                        //an entire intakeForm reload
                        view.fire('savedMedication');
                    }
                }, this));
            },
            disabled: true
        }],
        editMedicationButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeMedications
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedMedication event against the view
                        view.fire('savedMedication', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removeMedicationButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removedMedication event against the view
                        view.fire('removedMedication', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        addPersonalCareNeedButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved previous placement event against the view
                        //not using just 'saved' here as don't want to trigger 
                        //an entire intakeForm reload
                        view.fire('savedPersonalCareNeed');
                    }
                }, this));
            },
            disabled: true
        }],
        editPersonalCareNeedButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIntakePersonalCareNeed
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedPersonalCareNeed event against the view
                        view.fire('savedPersonalCareNeed', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removePersonalCareNeedButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removedPersonalCareNeed event against the view
                        view.fire('removedPersonalCareNeed', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        addSocialSupportButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved social support event rather than saved as we don't want to force a form reload
                        view.fire('savedSocialSupport');
                    }
                }, this));
            },
            disabled: true
        }],
        editSocialSupportButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeSupport
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved social support event rather than saved as we don't want to force a form reload
                        view.fire('savedSocialSupport', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removeSocialSupportButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removed social support event rather than saved as we don't want to force a form reload
                        view.fire('removedSocialSupport', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        addReferencedPersonButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved referenced person with ID and Involvement
                        view.fire('savedReferencedPerson', {
                            id: model.get('id'),
                            involvement: model.get('involvement')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editReferencedPersonButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeReferencedPerson
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved referenced person with ID and Involvement
                        view.fire('savedReferencedPerson', {
                            id: model.get('id'),
                            involvement: model.get('involvement')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removeReferencedPersonButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removed referenced person with ID and Involvement
                        view.fire('removedReferencedPerson', {
                            id: model.get('id'),
                            involvement: model.get('involvement')
                        });
                    }
                }, this));
            },
            disabled: true
        }];

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseEditView() {}
    BaseEditView.prototype = {
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        events: {
            '#intakeEditForm': {
                submit: '_preventSubmit'
            }
        }
    };

    //Base remove functionality - specific model classes will be augmented with this code
    function BaseRemoveForm() {}

    BaseRemoveForm.prototype = {
        destroy: function(options, callback) {
            var self = this,
                facade = {
                    options: options
                },
                finish = Y.bind(function(err) {
                    if (!err) {
                        //call the superclass
                        this.constructor.superclass.destroy.call(this);
                    } else {
                        facade.error = err;
                        facade.src = 'remove';
                        facade.response = arguments[1] || '500';
                        this.fire('error', facade);
                    }
                    if (callback) {
                        callback(err || null, arguments[1]);
                    }
                }, self);

            if (typeof options === 'function') {
                callback = options;
                options = null;
            }

            if (options && (options.remove || options['delete'])) {
                self.sync('delete', options, finish);
            } else {
                finish();
            }
            return self;
        }
    };


    //Model will be transmogrified into an UpdateIntakeConsent
    Y.namespace('app').UpdateIntakeConsentForm = Y.Base.create('updateIntakeConsentForm', Y.usp.resolveassessment.IntakeConsent, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeHistory
    Y.namespace('app').UpdateIntakeBriefHistoryForm = Y.Base.create('updateIntakeBriefHistoryForm', Y.usp.resolveassessment.IntakeHistory, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeCurrentPlacement
    Y.namespace('app').UpdateIntakeCurrentPlacementForm = Y.Base.create('updateIntakeCurrentPlacementForm', Y.usp.resolveassessment.IntakeCurrentPlacement, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeLifestyle
    Y.namespace('app').UpdateIntakeLifestyleForm = Y.Base.create('updateIntakeLifestyleForm', Y.usp.resolveassessment.IntakeLifestyle, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeMedication
    Y.namespace('app').UpdateIntakeHealthActionPlanForm = Y.Base.create('updateIntakeHealthActionPlanForm', Y.usp.resolveassessment.IntakeMedication, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeReferral
    Y.namespace('app').UpdateIntakeReferralForm = Y.Base.create('updateIntakeReferralForm', Y.usp.resolveassessment.IntakeReferral, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeFuture
    Y.namespace('app').UpdateIntakeFutureForm = Y.Base.create('updateIntakeFutureForm', Y.usp.resolveassessment.IntakeFuture, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeSpeech
    Y.namespace('app').UpdateIntakeEatingForm = Y.Base.create('updateIntakeEatingForm', Y.usp.resolveassessment.IntakeSpeech, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be manually transmogrified into an UpdateIntakeSocial
    Y.namespace('app').UpdateIntakeSocialForm = Y.Base.create('updateIntakeSocialForm', Y.usp.resolveassessment.IntakeSocial, [], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeCommunicationIssues
    Y.namespace('app').UpdateIntakeCommunicationIssuesForm = Y.Base.create('updateIntakeCommunicationIssuesForm', Y.usp.resolveassessment.IntakeSpeech, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeSpiritualNeeds
    Y.namespace('app').UpdateIntakeSpiritualNeedsForm = Y.Base.create('updateIntakeSpiritualNeedsForm', Y.usp.resolveassessment.IntakeSpiritual, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeMobility
    Y.namespace('app').UpdateIntakeMobilityForm = Y.Base.create('updateIntakeMobilityForm', Y.usp.resolveassessment.IntakePhysiotherapy, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeHealth
    Y.namespace('app').UpdateIntakeHealthForm = Y.Base.create('updateIntakeHealthForm', Y.usp.resolveassessment.IntakeHealth, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeMentalHealthForm
    Y.namespace('app').UpdateIntakeMentalHealthForm = Y.Base.create('updateIntakeMentalHealthForm', Y.usp.resolveassessment.IntakeMentalHealth, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakePsychologyForm
    Y.namespace('app').UpdateIntakePsychologyForm = Y.Base.create('updateIntakePsychologyForm', Y.usp.resolveassessment.IntakePsychology, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeOccupationalIssues
    Y.namespace('app').UpdateIntakeOccupationalIssuesForm = Y.Base.create('updateIntakeOccupationalIssuesForm', Y.usp.resolveassessment.IntakeOccupationalTherapy, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixOneCommunication
    Y.namespace('app').UpdateIntakeAppendixOneCommunicationForm = Y.Base.create('updateIntakeAppendixOneCommunication', Y.usp.resolveassessment.IntakeAppendixOneCommunication, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixOneFamilyHistory
    Y.namespace('app').UpdateIntakeAppendixOneFamilyHistoryForm = Y.Base.create('updateIntakeAppendixOneFamilyHistory', Y.usp.resolveassessment.IntakeAppendixOneFamilyHistory, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixOneReview
    Y.namespace('app').UpdateIntakeAppendixOneReviewForm = Y.Base.create('updateIntakeAppendixOneReview', Y.usp.resolveassessment.IntakeAppendixOneReview, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
 
    //Model will be transmogrified into an UpdateIntakeAppendixOneProblems
    Y.namespace('app').UpdateIntakeAppendixOneProblemsForm = Y.Base.create('updateIntakeAppendixOneProblems', Y.usp.resolveassessment.IntakeAppendixOneProblems, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
    
    //Model will be transmogrified into an UpdateIntakeAppendixOneFootCare
    Y.namespace('app').UpdateIntakeAppendixOneFootCareForm = Y.Base.create('updateIntakeAppendixOneFootCare', Y.usp.resolveassessment.IntakeAppendixOneFootCare, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
    
    //Model will be transmogrified into an UpdateIntakeAppendixOneContinence
    Y.namespace('app').UpdateIntakeAppendixOneContinenceForm = Y.Base.create('updateIntakeAppendixOneContinence', Y.usp.resolveassessment.IntakeAppendixOneContinence, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
    
    //Model will be transmogrified into an UpdateIntakeAppendixOneEpilepsy
    Y.namespace('app').UpdateIntakeAppendixOneEpilepsyForm = Y.Base.create('updateIntakeAppendixOneEpilepsy', Y.usp.resolveassessment.IntakeAppendixOneEpilepsy, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
    
    //Model will be transmogrified into an UpdateIntakeAppendixOneSkin
    Y.namespace('app').UpdateIntakeAppendixOneSkinForm = Y.Base.create('updateIntakeAppendixOneSkin', Y.usp.resolveassessment.IntakeAppendixOneSkin, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
    
    //Model will be transmogrified into an UpdateIntakeAppendixOneBreathingCirculation
    Y.namespace('app').UpdateIntakeAppendixOneBreathingCirculationForm = Y.Base.create('updateIntakeAppendixOneBreathingCirculation', Y.usp.resolveassessment.IntakeAppendixOneBreathingCirculation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
    
    //Model will be transmogrified into an UpdateIntakeAppendixOneMensHealth
    Y.namespace('app').UpdateIntakeAppendixOneMensHealthForm = Y.Base.create('updateIntakeAppendixOneMensHealth', Y.usp.resolveassessment.IntakeAppendixOneMensHealth, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixTwoContext
    Y.namespace('app').UpdateIntakeAppendixTwoContextForm = Y.Base.create('UpdateIntakeAppendixTwoContextForm', Y.usp.resolveassessment.IntakeAppendixTwoContext, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });
    
    //This is a custom UpdateIntakeAppendixTwoContext model that we will use until the client cartridge knows how to deal with DateTime fields
    Y.namespace('app').UpdateIntakeAppendixTwoContext = Y.Base.create('updateIntakeAppendixTwoContext', Y.usp.resolveassessment.UpdateIntakeAppendixTwoContext, [], {        
        _VOValidate: function (attributes, errors) {
            var thisAttrs = Y.app.UpdateIntakeAppendixTwoContext.ATTRS;
            
            if (!errors) errors = {};                
                //loop over the attributes of JUST THIS CLASS, in the context of this object instance
                O.each(thisAttrs, function (attrMetadata, attrName) { 
                    var attrValue = attributes[attrName],
                    attrChanged, currentValue;
                    if (O.hasKey(attributes, attrName)) { //if the attrubute to validate exists in this class, then consider validation
                        attrChanged = O.hasKey(this.changed, attrName);
                        currentValue = this.get(attrName);
                        if (
                            thisAttrs[attrName].USPType === 'DateTime' && // only validate Dates
                            !thisAttrs[attrName].USPStereotypes.Fuzzy && // skip fuzzy Dates
                            !L.isNull(attrValue) && // do not validate if null
                            !(currentValue === attrValue || attrChanged)
                        ) { 
                            try {
                                Y.USPDate._parseDateTime(attrValue);
                            } catch (e) {
                                errors[attrName] = e.message;
                            }
                        }
                    }
                }, this);
                
            //call superclass
            Y.app.UpdateIntakeAppendixTwoContext.superclass._VOValidate.call(this, attributes, errors);
            return errors;
        }
    },{
        ATTRS:{
            dateAndTimeOfAssessment : {
                setter: function (value) {                   
                    var parsed = null,
                    numberValue = null;
                    
                    // handle values which are numeric in nature, either as Strings or actual number types (the regex passes both)
                    if (/^-?\d+$/.test(value)) {
                        numberValue = Number(value); // cast to actual number 

                        //if the number does not fall within a range which could be considered a year, then treat as epoch 
                        if (!(numberValue > 0 && numberValue < 9999)) {
                            return numberValue; 
                        }
                        //otherwise, fall into date string parsing
                    }
                    try {
                        parsed = Y.USPDate._parseDateTime(value);
                        if (!Y.Lang.isNull(parsed)) {
                            return parsed.getTime();
                        }
                    }catch (e) {
                        Y.log("Unhandled Date Value set on currentPlacementEndDate: " + value); 
                    }
                    return parsed;
                },                
                USPType: 'DateTime',
                USPStereotypes: {
                },
                //type: DateTime - dateAndTimeOfAssessment - datatype

                validation: {
                }
                
            }
        }
    });    
    
    //End of custom UpdateIntakeAppendixTwoContext model
        
    //Model will be transmogrified into an UpdateIntakeAppendixTwoMentalState
    Y.namespace('app').UpdateIntakeAppendixTwoMentalStateForm = Y.Base.create('updateIntakeAppendixTwoMentalState', Y.usp.resolveassessment.IntakeAppendixTwoMentalState, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixTwoOrientation
    Y.namespace('app').UpdateIntakeAppendixTwoOrientationForm = Y.Base.create('updateIntakeAppendixTwoOrientation', Y.usp.resolveassessment.IntakeAppendixTwoOrientation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixTwoOrientation
    Y.namespace('app').UpdateIntakeAppendixTwoTimeOrientationForm = Y.Base.create('updateIntakeAppendixTwoTimeOrientationForm', Y.app.UpdateIntakeAppendixTwoOrientationForm, [], {}, {
        ATTRS: {
            //prevent enum values from setting blank
            timePartOfDay: {
                setter: setEnumVal
            }
        }
    });

    //Model will be transmogrified into an UpdateIntakeAppendixTwoWellbeing
    Y.namespace('app').UpdateIntakeAppendixTwoWellbeingForm = Y.Base.create('updateIntakeAppendixTwoWellbeing', Y.usp.resolveassessment.IntakeAppendixTwoWellbeing, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixTwoWellbeing
    Y.namespace('app').UpdateIntakeAppendixTwoAppetiteForm = Y.Base.create('updateIntakeAppendixTwoAppetiteForm', Y.usp.resolveassessment.IntakeAppendixTwoWellbeing, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixTwoWellbeing
    Y.namespace('app').UpdateIntakeAppendixTwoOtherForm = Y.Base.create('updateIntakeAppendixTwoOtherForm', Y.usp.resolveassessment.IntakeAppendixTwoWellbeing, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixTwoOutcome
    Y.namespace('app').UpdateIntakeAppendixTwoOutcomeForm = Y.Base.create('updateIntakeAppendixTwoOutcome', Y.usp.resolveassessment.IntakeAppendixTwoOutcome, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateIntakeAppendixThreeContext
    Y.namespace('app').UpdateIntakeAppendixThreeContextForm = Y.Base.create('UpdateIntakeAppendixThreeContextForm', Y.usp.resolveassessment.IntakeAppendixThreeContext, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    
    //This is a custom UpdateIntakeAppendixThreeContext model that we will use until the client cartridge knows how to deal with DateTime fields
    Y.namespace('app').UpdateIntakeAppendixThreeContext = Y.Base.create('updateIntakeAppendixThreeContext', Y.usp.resolveassessment.UpdateIntakeAppendixThreeContext, [], {        
        _VOValidate: function (attributes, errors) {
            var thisAttrs = Y.app.UpdateIntakeAppendixThreeContext.ATTRS;
            
            if (!errors) errors = {};                
                //loop over the attributes of JUST THIS CLASS, in the context of this object instance
                O.each(thisAttrs, function (attrMetadata, attrName) { 
                    var attrValue = attributes[attrName],
                    attrChanged, currentValue;
                    if (O.hasKey(attributes, attrName)) { //if the attrubute to validate exists in this class, then consider validation
                        attrChanged = O.hasKey(this.changed, attrName);
                        currentValue = this.get(attrName);
                        if (
                            thisAttrs[attrName].USPType === 'DateTime' && // only validate Dates
                            !thisAttrs[attrName].USPStereotypes.Fuzzy && // skip fuzzy Dates
                            !L.isNull(attrValue) && // do not validate if null
                            !(currentValue === attrValue || attrChanged)
                        ) { 
                            try {
                                Y.USPDate._parseDateTime(attrValue);
                            } catch (e) {
                                errors[attrName] = e.message;
                            }
                        }
                    }
                }, this);
                
            //call superclass
            Y.app.UpdateIntakeAppendixThreeContext.superclass._VOValidate.call(this, attributes, errors);
            return errors;
        }
    },{
        ATTRS:{
            dateAndTimeOfAssessment : {
                setter: function (value) {                   
                    var parsed = null,
                    numberValue = null;
                    
                    // handle values which are numeric in nature, either as Strings or actual number types (the regex passes both)
                    if (/^-?\d+$/.test(value)) {
                        numberValue = Number(value); // cast to actual number 

                        //if the number does not fall within a range which could be considered a year, then treat as epoch 
                        if (!(numberValue > 0 && numberValue < 9999)) {
                            return numberValue; 
                        }
                        //otherwise, fall into date string parsing
                    }
                    try {
                        parsed = Y.USPDate._parseDateTime(value);
                        if (!Y.Lang.isNull(parsed)) {
                            return parsed.getTime();
                        }
                    }catch (e) {
                        Y.log("Unhandled Date Value set on currentPlacementEndDate: " + value); 
                    }
                    return parsed;
                },                
                USPType: 'DateTime',
                USPStereotypes: {
                },
                //type: DateTime - dateAndTimeOfAssessment - datatype

                validation: {
                }
                
            }
        }
    });    
    
    //End of custom UpdateIntakeAppendixThreeContext model
    
    
    //Model will be transmogrified into an UpdateIntakeAppendixThreeBehaviour
    Y.namespace('app').UpdateIntakeAppendixThreeBehaviourForm = Y.Base.create('updateIntakeAppendixThreeBehaviour', Y.usp.resolveassessment.IntakeAppendixThreeBehaviour, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#intakeEditForm'
    });

    //Model will be transmogrified into an UpdateRejectForm
    Y.namespace('app').UpdateIntakeFormRejectionForm = Y.Base.create('updateIntakeFormRejectionForm', Y.usp.resolveassessment.FormRejection, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#rejectFormForm'
    }, {
        ATTRS: {
            subject: {
                writeOnce: 'initOnly'
            }
        }
    });

    //New previous placement form
    Y.namespace('app').AddIntakePreviousPlacementForm = Y.Base.create('addIntakePreviousPlacementForm', Y.usp.resolveassessment.NewIntakePreviousPlacement, [Y.usp.ModelFormLink], {
        form: '#addEditPreviousPlacementForm'
    });

    //Model will be transmogrified into an UpdateIntakePreviousPlacement
    Y.namespace('app').UpdateIntakePreviousPlacementForm = Y.Base.create('updateIntakePreviousPlacementForm', Y.usp.resolveassessment.IntakePreviousPlacement, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#addEditPreviousPlacementForm'
    });

    //Remove IntakePreviousPlacement - simple YUI form
    Y.namespace('app').RemoveIntakePreviousPlacementForm = Y.Base.create('removeIntakePreviousPlacementForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});

    //New identified risk form
    Y.namespace('app').AddIntakeIdentifiedRiskForm = Y.Base.create('addIntakeIdentifiedRiskForm', Y.usp.resolveassessment.NewIntakeRiskAssessment, [Y.usp.ModelFormLink], {
        form: '#addEditIdentifiedRiskForm'
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            likelihood: {
                setter: setEnumVal
            },
            impact: {
                setter: setEnumVal
            },
            revisedLikelihood: {
                setter: setEnumVal
            },
            revisedImpact: {
                setter: setEnumVal
            }
        }
    });

    //Update identified risk form : Model will be transmogrified into an UpdateIntakeIntakeRiskAssessment
    Y.namespace('app').UpdateIntakeIdentifiedRiskForm = Y.Base.create('updateIntakeIdentifiedRiskForm', Y.usp.resolveassessment.IntakeRiskAssessment, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#addEditIdentifiedRiskForm'
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            likelihood: {
                setter: setEnumVal
            },
            impact: {
                setter: setEnumVal
            },
            revisedLikelihood: {
                setter: setEnumVal
            },
            revisedImpact: {
                setter: setEnumVal
            }
        }
    });

    //Remove identified risk - simple YUI form
    Y.namespace('app').RemoveIntakeIdentifiedRiskForm = Y.Base.create('removeIntakeIdentifiedRiskForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});

    //New support plan form
    Y.namespace('app').AddIntakeSupportPlanForm = Y.Base.create('addIntakeSupportPlanForm', Y.usp.resolveassessment.NewIntakeSupportPlan, [Y.usp.ModelFormLink], {
        form: '#addEditSupportPlanForm'
    });

    //Update support plan form : Model will be transmogrified into an UpdateIntakeSupportPlan
    Y.namespace('app').UpdateIntakeSupportPlanForm = Y.Base.create('updateIntakeSupportPlanForm', Y.usp.resolveassessment.IntakeSupportPlan, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#addEditSupportPlanForm'
    });

    //Remove support plan  - simple YUI form
    Y.namespace('app').RemoveIntakeSupportPlanForm = Y.Base.create('removeIntakeSupportPlanForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});

    //New medication form
    Y.namespace('app').AddIntakeMedicationForm = Y.Base.create('addIntakeMedicationForm', Y.usp.resolveassessment.NewIntakeMedications, [Y.usp.ModelFormLink], {
        form: '#addEditMedicationForm'
    });

    //Update medication form : Model will be transmogrified into an UpdateIntakeMedications
    Y.namespace('app').UpdateIntakeMedicationForm = Y.Base.create('updateIntakeMedicationForm', Y.usp.resolveassessment.IntakeMedications, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#addEditMedicationForm'
    });

    //Remove medication  - simple YUI form
    Y.namespace('app').RemoveIntakeMedicationForm = Y.Base.create('removeIntakeMedicationForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});

    //New personal care need form
    Y.namespace('app').AddIntakePersonalCareNeedForm = Y.Base.create('addIntakePersonalCareNeedForm', Y.usp.resolveassessment.NewIntakePersonalCareNeed, [Y.usp.ModelFormLink], {
        form: '#addEditPersonalCareNeedForm'
    });

    //Update personal care need form : Model will be transmogrified into an UpdateIntakePersonalCareNeed
    Y.namespace('app').UpdateIntakePersonalCareNeedForm = Y.Base.create('updateIntakePersonalCareNeedForm', Y.usp.resolveassessment.IntakePersonalCareNeed, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#addEditPersonalCareNeedForm'
    });

    //Remove personal care need  - simple YUI form
    Y.namespace('app').RemoveIntakePersonalCareNeedForm = Y.Base.create('removeIntakePersonalCareNeedForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});


    //New support form
    Y.namespace('app').AddIntakeSupportForm = Y.Base.create('addIntakeSupportForm', Y.usp.resolveassessment.NewIntakeSupport, [Y.usp.ModelFormLink], {
        form: '#addSupportForm'
    });

    //Update support form : Model will be transmogrified into an UpdateIntakeSupport
    Y.namespace('app').UpdateIntakeSupportForm = Y.Base.create('updateIntakeSupportForm', Y.usp.resolveassessment.IntakeSupport, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editSupportForm'
    });

    //Remove support form  - simple YUI form
    Y.namespace('app').RemoveIntakeSupportForm = Y.Base.create('removeIntakeSupportForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});

    //Add referenced person form
    Y.namespace('app').AddIntakeReferencedPersonForm = Y.Base.create('addIntakeReferencedPersonForm', Y.usp.resolveassessment.NewIntakeReferencedPerson, [Y.usp.ModelFormLink], {
        form: '#addReferencedPersonForm',
        initializer: function() {
            Y.Do.after(this._toJSON, this, 'toJSON', this);
        },
        _toJSON: function() {
            //get the attributes from the original wrapped call
            var attrs = Y.Do.originalRetVal;

            //remove relationship type
            delete attrs.relationshipType;

            //return the attributes
            return new Y.Do.AlterReturn("Remove relationship type", attrs);
        },
        validate: function(attrs, callback) {
            var errs = {};

            //We only want to validate against the populated model. So only validate if the relationshipType attribute 
            //is NOT present (we remove it in the _toJSON when we populate the actual model)
            if ((!attrs.relationshipType) && (attrs.relationshipRole === '' || attrs.relationshipRole === null)) {
                errs['relationshipTypeId'] = 'You must select the relationship.';
            }

            if (O.keys(errs).length > 0) {
                // owning provider id must be set
                return callback({
                    code: 400,
                    msg: {
                        validationErrors: errs
                    }
                });
            }

            //call super class
            return Y.app.AddIntakeReferencedPersonForm.superclass.validate.apply(this, arguments);
        }
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            relationshipRole: {
                setter: setEnumVal
            },
            relationshipType: {
                setter: function(val) {
                    //sets the relationship type id and role based on value
                    var sp, vals = {
                        relationshipRole: null,
                        relationshipTypeId: null
                    };
                    if (val) {
                        sp = val.split('_');
                        vals['relationshipTypeId'] = sp[0];
                        if (sp[1]) {
                            vals['relationshipRole'] = (sp[1] === 'A') ? 'ROLE_A' : 'ROLE_B';
                        }
                    }

                    //set attributes
                    this.setAttrs(vals, {
                        silent: true
                    });
                }
            }
        }
    });

    //Update referenced person form : Model will be transmogrified into an UpdateIntakeReferencedPerson    
    Y.namespace('app').UpdateIntakeReferencedPersonForm = Y.Base.create('updateIntakeReferencedPerson', Y.usp.resolveassessment.IntakeReferencedPerson, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editReferencedPersonForm'
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            relationshipRole: {
                setter: setEnumVal
            },
            relationshipType: {
                setter: function(val) {
                    //sets the relationship type id and role based on value
                    var sp, vals = {
                        relationshipRole: null,
                        relationshipTypeId: null
                    };
                    if (val) {
                        sp = val.split('_');
                        vals['relationshipTypeId'] = sp[0];
                        if (sp[1]) {
                            vals['relationshipRole'] = (sp[1] === 'A') ? 'ROLE_A' : 'ROLE_B';
                        }
                    }

                    //set attributes
                    this.setAttrs(vals, {
                        silent: true
                    });
                }
            }
        }
    });

    //Remove referenced person - simple YUI form
    Y.namespace('app').RemoveIntakeReferencedPersonForm = Y.Base.create('removeIntakeReferencedPersonForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});

    Y.namespace('app').IntakeFormEditConsentView = Y.Base.create('intakeFormEditConsentView', Y.usp.resolveassessment.UpdateIntakeConsentView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditConsentDialog"]
    });

    Y.namespace('app').IntakeFormEditBriefHistoryView = Y.Base.create('intakeFormEditBriefHistoryView', Y.usp.resolveassessment.UpdateIntakeHistoryView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditPlacementBriefHistoryDialog"]
    });

    Y.namespace('app').IntakeFormEditCurrentPlacementView = Y.Base.create('intakeFormEditCurrentPlacementView', Y.usp.resolveassessment.UpdateIntakeCurrentPlacementView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditCurrentPlacementDialog"],
        initializer: function() {
            this.startDateCalendar = new Y.usp.CalendarPopup();
            this.endDateCalendar = new Y.usp.CalendarPopup();
        },
        destructor: function() {
            //destroy the calendars
            this.startDateCalendar.destroy();
            delete this.startDateCalendar;
            if (this.startDateInput) {
                this.startDateInput.destroy();
                delete this.startDateInput;
            }
            this.endDateCalendar.destroy();
            delete this.endDateCalendar;
            if (this.endDateInput) {
                this.endDateInput.destroy();
                delete this.endDateInput;
            }
        },
        render: function() {
            //call superclass
            Y.app.IntakeFormEditCurrentPlacementView.superclass.render.call(this);
            var container = this.get('container'),
                startDateInput = container.one('#intakeEditForm_currentPlacementStartDate'),
                endDateInput = container.one('#intakeEditForm_currentPlacementEndDate');

            //configure the calendar input nodes
            this.startDateCalendar.set('inputNode', startDateInput);
            this.endDateCalendar.set('inputNode', endDateInput);

            //render the calendars
            this.startDateCalendar.render();
            this.endDateCalendar.render();

            //cache the date inputs to destroy later
            this.startDateInput = startDateInput;
            this.endDateInput = endDateInput;
        }
    });

    Y.namespace('app').IntakeFormEditLifestyleView = Y.Base.create('intakeFormEditLifestyleView', Y.usp.resolveassessment.UpdateIntakeLifestyleView, [Y.usp.RichText], {
        events: {
            '#intakeEditForm': {
                submit: '_preventSubmit'
            },
            '#intakeEditForm input[name="lifestyleAlcoholConsumption"]': {
                change: '_toggleAlcoholDetails'
            },
            '#intakeEditForm input[name="lifestyleSmoking"]': {
                change: '_toggleSmokingDetails'
            },
            '#intakeEditForm input[name="lifestyleDrugs"]': {
                change: '_toggleDrugsDetails'
            }
        },
        template: Y.Handlebars.templates["intakeFormEditLifestyleDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        destructor: function() {
            if (this.lifestyleAlcoholDetails) {
                this.lifestyleAlcoholDetails.destroy();
                delete this.lifestyleAlcoholDetails;
            }

            if (this.lifestyleSmokingDetails) {
                this.lifestyleSmokingDetails.destroy();
                delete this.lifestyleSmokingDetails;
            }

            if (this.lifestyleDrugsDetails) {
                this.lifestyleDrugsDetails.destroy();
                delete this.lifestyleDrugsDetails;
            }
        },
        render: function() {
            var container = this.get('container'),
                model = this.get('model');

            //call superclass
            Y.app.IntakeFormEditLifestyleView.superclass.render.call(this);

            //get a reference to our content editable areas
            this.lifestyleAlcoholDetails = container.one('#intakeEditForm_lifestyleAlcoholDetails');
            this.lifestyleSmokingDetails = container.one('#intakeEditForm_lifestyleSmokingDetails');
            this.lifestyleDrugsDetails = container.one('#intakeEditForm_lifestyleDrugsDetails');


            //setup rich text areas (if supported)
            this._setupRichTextArea(this.lifestyleAlcoholDetails, model.get('lifestyleAlcoholConsumption'));
            this._setupRichTextArea(this.lifestyleSmokingDetails, model.get('lifestyleSmoking'));
            this._setupRichTextArea(this.lifestyleDrugsDetails, model.get('lifestyleDrugs'));
        },
        _toggleAlcoholDetails: function(e) {
            this._setupRichTextArea(this.lifestyleAlcoholDetails, e.currentTarget.get('value')==='true');
        },
        _toggleSmokingDetails: function(e) {
            this._setupRichTextArea(this.lifestyleSmokingDetails, e.currentTarget.get('value')==='true');
        },
        _toggleDrugsDetails: function(e) {
            this._setupRichTextArea(this.lifestyleDrugsDetails, e.currentTarget.get('value')==='true');
        },
        _setupRichTextArea: function(node, enabled) {
            if (enabled) {
                //enable the editor
                this.enableEditor(node);
            } else {
                //disable the editor - clearing any existing data
                this.disableEditor(node, true);
            }
        }
    });

    Y.namespace('app').IntakeFormEditReferrerView = Y.Base.create('intakeFormEditReferrerView', Y.usp.resolveassessment.UpdateIntakeReferralView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditReferralDialog"],
        initializer: function() {
            this.referralDateCalendar = new Y.usp.CalendarPopup();
        },
        destructor: function() {
            //destroy the calendars
            this.referralDateCalendar.destroy();
            delete this.referralDateCalendar;
            if (this.referralDateInput) {
                this.referralDateInput.destroy();
                delete this.referralDateInput;
            }
        },
        render: function() {
            //call superclass
            Y.app.IntakeFormEditReferrerView.superclass.render.call(this);
            var container = this.get('container'),
                referralDateInput = container.one('#intakeEditForm_referralDate');


            //configure the calendar input node
            this.referralDateCalendar.set('inputNode', referralDateInput);

            //render the calendar
            this.referralDateCalendar.render();

            //cache the date input to destroy later
            this.referralDateInput = referralDateInput;
        }
    });


    Y.namespace('app').IntakeFormEditFutureView = Y.Base.create('intakeFormEditFutureView', Y.usp.resolveassessment.UpdateIntakeFutureView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditFutureDialog"]
    });

    Y.namespace('app').IntakeFormEditEatingView = Y.Base.create('intakeFormEditEatingView', Y.usp.resolveassessment.UpdateIntakeSpeechView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditEatingDialog"]
    });

    Y.namespace('app').IntakeFormEditHealthActionPlanView = Y.Base.create('intakeFormEditHealthActionPlanView', Y.usp.resolveassessment.UpdateIntakeMedicationView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditHealthActionPlanDialog"]
    });

    Y.namespace('app').IntakeFormEditSocialView = Y.Base.create('intakeFormEditSocialView', Y.usp.resolveassessment.UpdateIntakeSocialView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditSocialDialog"]
    });

    Y.namespace('app').IntakeFormEditCommunicationIssuesView = Y.Base.create('intakeFormEditCommunicationIssuesView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditCommunicationIssuesDialog"]
    });

    Y.namespace('app').IntakeFormEditSpiritualNeedsView = Y.Base.create('intakeFormEditSpiritualNeedsView', Y.usp.resolveassessment.UpdateIntakeSpiritualView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditSpiritualNeedsDialog"]
    });

    Y.namespace('app').IntakeFormEditAppendixView = Y.Base.create('intakeFormEditAppendixView', Y.View, [Y.usp.RichText, BaseEditView], {
        initializer: function() {
            var model = this.get('model'), pickerStyle;

            // Re-render this view when the model changes, and destroy this view when
            // the model is destroyed.
            model.after('change', this.render, this);
            model.after('destroy', this.destroy, this);

            //init any required calendars
            this.calendars = [];
            this.dateInputs = [];

            A.each(this.get('popupCalendarsConfig'), function(popupCalendarConfig) {
                pickerStyle = popupCalendarConfig.style===undefined?'Date':popupCalendarConfig.style;
                this.calendars.push(
                    new Y.usp.CalendarPopup({
                        pickerStyle: pickerStyle
                    })
                );
            }, this);
        },
        destructor: function() {
            //destroy any calendars
            A.each(this.calendars, function(calendar) {
                calendar.destroy();
            });
            //destroy any cached date inputs
            A.each(this.dateInputs, function(dateInput) {
                dateInput.destroy();
            });

            //null out arrays
            this.calendars = null;
            this.dateInputs = null;
        },
        render: function() {
            var container = this.get('container'),
                calendar, dateInput,
                html = Y.Handlebars.templates[this.template]({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    otherData: this.get('otherData'),
                    codedEntries: this.get('codedEntries'),
                    enumTypes: this.get('enumTypes')
                });

            Y.log("render " + this.name, "debug");

            // Render this view's HTML into the container element.
            container.setHTML(html);

            // Setup any required calendars
            A.each(this.get('popupCalendarsConfig'), function(popupCalendarConfig, index) {
                dateInput = container.one(popupCalendarConfig.selector);
                calendar = this.calendars[index];

                //configure the calendar input node
                calendar.set('inputNode', dateInput);

                //render the calendar
                calendar.render();

                //cache the date input to destroy later
                this.dateInputs.push(dateInput);
            }, this);

            return this;
        }
    });

    Y.namespace('app').IntakeFormEditAppendixTimeOrientationView = Y.Base.create('intakeFormEditAppendixTimeOrientationView', Y.app.IntakeFormEditAppendixView, [], {
        destructor: function() {
            if (this.dayPartInput) {
                this.dayPartInput.destroy();
                delete this.dayPartInput;
            }
        },
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.IntakeFormEditAppendixTimeOrientationView.superclass.render.call(this);

            var container = this.get('container'),
                dayPartInput = container.one('#intakeEditForm_timePartOfDay'),
                dayPartOptions = this.get('dayPart');

            //setup dayPart options
            FU.setSelectOptions(dayPartInput, dayPartOptions, null, null, true, true, 'enumName');

            //perform the selections
            dayPartInput.set('value', this.get('model').get('timePartOfDay'));

            //cache the dayPart div
            this.dayPartInput = dayPartInput;
        }
    }, {
        ATTRS: {
            dayPart: {
                value: Y.usp.resolveassessment.enum.DayPart.values
            }
        }
    });

    Y.namespace('app').IntakeFormEditAppendixAppetiteView = Y.Base.create('intakeFormEditAppendixAppetiteView', Y.app.IntakeFormEditAppendixView, [], {
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.IntakeFormEditAppendixAppetiteView.superclass.render.call(this);

            var container = this.get('container'),
                breakfastInput = container.one('#intakeEditForm_BreakfastInput'),
                dinnerInput = container.one('#intakeEditForm_DinnerInput'),
                teaInput = container.one('#intakeEditForm_TeaInput'),
                supperInput = container.one('#intakeEditForm_SupperInput'),
                appetiteOptions = this.get('enumTypes').appetite,
                appetitesSelected = this.get('model').get('appetite');
            
            //perform the checkbox selections
            if (appetitesSelected !== undefined){
                A.each(appetitesSelected, function(appetiteSelected) {
                    if (appetiteSelected === appetiteOptions[0].enumName){
                        breakfastInput.setAttribute('checked', 'checked');
                    }
                    else if (appetiteSelected === appetiteOptions[1].enumName){
                        dinnerInput.setAttribute('checked', 'checked');
                    }
                    else if (appetiteSelected === appetiteOptions[2].enumName){
                        teaInput.setAttribute('checked', 'checked');
                    }
                    else if (appetiteSelected === appetiteOptions[3].enumName){
                        supperInput.setAttribute('checked', 'checked');
                    }
                }, this);
            }

            //destroy the references to the inputs
            breakfastInput.destroy();
            delete this.breakfastInput;
            dinnerInput.destroy();
            delete this.dinnerInput;
            teaInput.destroy();
            delete this.teaInput;
            supperInput.destroy();
            delete this.supperInput;
        }
    }, {
        ATTRS: {
            enumTypes: {
                value: {
                    appetite: Y.usp.resolveassessment.enum.Appetite.values
                }
            }
        }
    });

    Y.namespace('app').IntakeFormEditAppendixOtherWellbeingView = Y.Base.create('intakeFormEditAppendixOtherWellbeingView', Y.app.IntakeFormEditAppendixView, [], {
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.IntakeFormEditAppendixOtherWellbeingView.superclass.render.call(this);

            var container = this.get('container'),
                heartPoundingInput = container.one('#intakeEditForm_anxiety0Input'),
                dryMouthInput = container.one('#intakeEditForm_anxiety1Input'),
                sweatsOrFlushesInput = container.one('#intakeEditForm_anxiety2Input'),
                tremblingInput = container.one('#intakeEditForm_anxiety3Input'),
                sweatingInput = container.one('#intakeEditForm_anxiety4Input'),
                chestPainInput = container.one('#intakeEditForm_anxiety5Input'),
                breathInput = container.one('#intakeEditForm_anxiety6Input'),
                nauseaInput = container.one('#intakeEditForm_anxiety7Input'),
                dizzinessInput = container.one('#intakeEditForm_anxiety8Input'),
                anxietyOptions = this.get('enumTypes').anxiety,
                anxietiesSelected = this.get('model').get('anxiety');
            
            //perform the checkbox selections
            if (anxietiesSelected !== undefined){
                A.each(anxietiesSelected, function(anxietySelected) {
                    switch(anxietySelected){
                        case anxietyOptions[0].enumName:
                            heartPoundingInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[1].enumName:
                            dryMouthInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[2].enumName:
                            sweatsOrFlushesInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[3].enumName:
                            tremblingInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[4].enumName:
                            sweatingInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[5].enumName:
                            chestPainInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[6].enumName:
                            breathInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[7].enumName:
                            nauseaInput.setAttribute('checked', 'checked');
                        break;         
                        case anxietyOptions[8].enumName:
                            dizzinessInput.setAttribute('checked', 'checked');
                        break;         
                    }
                }, this);
            }

            //destroy the references to the inputs
            heartPoundingInput.destroy();
            delete this.heartPoundingInput;
            dryMouthInput.destroy();
            delete this.dryMouthInput;
            sweatsOrFlushesInput.destroy();
            delete this.sweatsOrFlushesInput;
            tremblingInput.destroy();
            delete this.sweatingInput;
            chestPainInput.destroy();
            delete this.chestPainInput;
            breathInput.destroy();
            delete this.breathInput;
            nauseaInput.destroy();
            delete this.nauseaInput;
            dizzinessInput.destroy();
            delete this.dizzinessInput;
        }
    }, {
        ATTRS: {
            enumTypes: {
                value: {
                    anxiety: Y.usp.resolveassessment.enum.Anxiety.values
                }
            }
        }
    });

    Y.namespace('app').IntakeFormEditMobilityView = Y.Base.create('intakeFormEditMobilityView', Y.usp.resolveassessment.UpdateIntakePhysiotherapyView, [Y.usp.RichText], {
        events: {
            '#intakeEditForm': {
                submit: '_preventSubmit'
            },
            '#intakeEditForm input[name="mobilityRecentTripsOrFalls"]': {
                change: '_toggleMobilityTripsMoreRegular'
            }
        },
        template: Y.Handlebars.templates["intakeFormEditMobilityDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            var container = this.get('container'),
                model = this.get('model');

            //call superclass
            Y.app.IntakeFormEditMobilityView.superclass.render.call(this);

            //get a reference to our content editable areas
            this.mobilityTripsMoreRegular = container.one('#intakeEditForm_mobilityTripsMoreRegular');

            //setup rich text areas (if supported)
            this._setupRichTextArea(this.mobilityTripsMoreRegular, model.get('mobilityRecentTripsOrFalls'));
        },
        _toggleMobilityTripsMoreRegular: function(e) {
            this._setupRichTextArea(this.mobilityTripsMoreRegular, e.currentTarget.get('value')==='true');
        },
        _setupRichTextArea: function(node, enabled) {
            if (enabled) {
                //enable the editor
                this.enableEditor(node);
            } else {
                //disable the editor - clearing any existing data
                this.disableEditor(node, true);
            }
        }
    });


    Y.namespace('app').IntakeFormEditOccupationalIssuesView = Y.Base.create('intakeFormEditOccupationalIssuesView', Y.usp.resolveassessment.UpdateIntakeMobilityView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditOccupationalIssuesDialog"]
    });

    Y.namespace('app').IntakeFormEditHealthView = Y.Base.create('intakeFormEditHealthView', Y.usp.resolveassessment.UpdateIntakeHealthView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditHealthDialog"],
        initializer: function() {
            this.lastAnnualHealthCheckCalendar = new Y.usp.CalendarPopup();
        },
        destructor: function() {
            //destroy the calendars
            this.lastAnnualHealthCheckCalendar.destroy();
            delete this.lastAnnualHealthCheckCalendar;
            if (this.lastAnnualHealthCheckInput) {
                this.lastAnnualHealthCheckInput.destroy();
                delete this.lastAnnualHealthCheckInput;
            }
        },
        render: function() {
            //call superclass
            Y.app.IntakeFormEditHealthView.superclass.render.call(this);
            var container = this.get('container'),
                lastAnnualHealthCheckInput = container.one('#intakeEditForm_lastAnnualHealthCheck');

            //configure the calendar input nodes
            this.lastAnnualHealthCheckCalendar.set('inputNode', lastAnnualHealthCheckInput);

            //render the calendars
            this.lastAnnualHealthCheckCalendar.render();

            //cache the date inputs to destroy later
            this.lastAnnualHealthCheckInput = lastAnnualHealthCheckInput;
        }
    });

    Y.namespace('app').IntakeFormEditMentalHealthView = Y.Base.create('intakeFormEditMentalHealthView', Y.usp.resolveassessment.UpdateIntakeMentalHealthView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditMentalHealthDialog"]
    });

    Y.namespace('app').IntakeFormEditPsychologyView = Y.Base.create('intakeFormEditPsychologyView', Y.usp.resolveassessment.UpdateIntakePsychologyView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["intakeFormEditPsychologyDialog"]
    });

    Y.namespace('app').IntakeFormEditRejectionView = Y.Base.create('intakeFormEditRejectionView', Y.usp.resolveassessment.UpdateFormRejectionView, [Y.usp.RichText], {
        events: {
            '#rejectFormForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["formEditRejectionDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        }
    });

    //Add previous placement view
    Y.namespace('app').AddEditIntakePreviousPlacementView = Y.Base.create('addEditIntakePreviousPlacementView', Y.usp.resolveassessment.IntakePreviousPlacementView, [], {
        events: {
            '#addEditPreviousPlacementForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditPreviousPlacementDialog"],
        initializer: function() {
            this.startDateCalendar = new Y.usp.CalendarPopup();
            this.endDateCalendar = new Y.usp.CalendarPopup();
        },
        render: function() {
            //call superclass
            Y.app.AddEditIntakePreviousPlacementView.superclass.render.call(this);

            var container = this.get('container'),
                startDateInput = container.one('#addEditPreviousPlacementForm_startDate'),
                endDateInput = container.one('#addEditPreviousPlacementForm_endDate');

            //configure the date calendar input node
            this.startDateCalendar.set('inputNode', startDateInput);
            //render the calendar
            this.startDateCalendar.render();

            this.endDateCalendar.set('inputNode', endDateInput);
            //render the calendar
            this.endDateCalendar.render();
        },
        destructor: function() {
            this.endDateCalendar.destroy();
            delete this.endDateCalendar;

            this.startDateCalendar.destroy();
            delete this.startDateCalendar;
        },
        _preventSubmit: function(e) {
            e.preventDefault();
        }
    });

    //Remove previous placement view
    Y.namespace('app').RemoveIntakePreviousPlacementView = Y.Base.create('removeIntakePreviousPlacementView', Y.usp.resolveassessment.IntakePreviousPlacementView, [], {
        template: Y.Handlebars.templates["intakeFormRemovePreviousPlacementDialog"]
    });

    //Base identified risk view
    Y.namespace('app').BaseIntakeIdentifiedRiskView = Y.Base.create('baseIntakeIdentifiedRiskView', Y.usp.resolveassessment.IntakeRiskAssessmentView, [], {
        events: {
            '#addEditIdentifiedRiskForm': {
                submit: '_preventSubmit'
            },
            //change events for risk and revised risk
            '#addEditIdentifiedRiskForm_likelihood': {
                change: '_calculateRiskTotal'
            },
            '#addEditIdentifiedRiskForm_impact': {
                change: '_calculateRiskTotal'
            },
            '#addEditIdentifiedRiskForm_revisedLikelihood': {
                change: '_calculateRevisedRiskTotal'
            },
            '#addEditIdentifiedRiskForm_revisedImpact': {
                change: '_calculateRevisedRiskTotal'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditIdentifiedRiskDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        _initializer: function() {
            this.riskDateCalendar = new Y.usp.CalendarPopup();
        },
        _destructor: function() {
            //destroy the calendar
            this.riskDateCalendar.destroy();
            delete this.riskDateCalendar;
            if (this.riskDateInput) {
                this.riskDateInput.destroy();
                delete this.riskDateInput;
            }
            if (this.riskTotal) {
                this.riskTotal.destroy();
                delete this.riskTotal;
            }
            if (this.likelihoodInput) {
                this.likelihoodInput.destroy();
                delete this.likelihoodInput;
            }
            if (this.impactInput) {
                this.impactInput.destroy();
                delete this.impactInput;
            }
            if (this.revisedRiskTotal) {
                this.revisedRiskTotal.destroy();
                delete this.revisedRiskTotal;
            }
            if (this.revisedLikelihoodInput) {
                this.revisedLikelihoodInput.destroy();
                delete this.revisedLikelihoodInput;
            }
            if (this.revisedImpactInput) {
                this.revisedImpactInput.destroy();
                delete this.revisedImpactInput;
            }
        },
        _render: function(prefix, model) {
            var container = this.get('container'),
                riskDateInput = container.one('#' + prefix + '_riskDate'),
                //lookup divs for total calculation
                totalDiv = container.one('#' + prefix + '_total'),
                likelihoodInput = container.one('#' + prefix + '_likelihood'),
                impactInput = container.one('#' + prefix + '_impact'),
                revisedTotalDiv = container.one('#' + prefix + '_revisedTotal'),
                revisedLikelihoodInput = container.one('#' + prefix + '_revisedLikelihood'),
                revisedImpactInput = container.one('#' + prefix + '_revisedImpact'),
                impactOptions = this.get('impact'),
                likelihoodOptions = this.get('likelihood');

            //setup impact options
            FU.setSelectOptions(impactInput, impactOptions, null, null, true, true, 'enumName');
            //set likelihood options
            FU.setSelectOptions(likelihoodInput, likelihoodOptions, null, null, true, true, 'enumName');
            //set revised impact options
            FU.setSelectOptions(revisedImpactInput, impactOptions, null, null, true, true, 'enumName');
            //set revised likelihood options
            FU.setSelectOptions(revisedLikelihoodInput, likelihoodOptions, null, null, true, true, 'enumName');

            if (model) {
                //perform the selections
                impactInput.set('value', model.get('impact'));
                likelihoodInput.set('value', model.get('likelihood'));
                revisedImpactInput.set('value', model.get('revisedImpact'));
                revisedLikelihoodInput.set('value', model.get('revisedLikelihood'));
            }

            //configure the date calendar input node
            this.riskDateCalendar.set('inputNode', riskDateInput);
            //render the calendar
            this.riskDateCalendar.render();
            //cache the date input to destroy later
            this.riskDateInput = riskDateInput;
            //cache those risk total divs
            this.riskTotal = totalDiv;
            this.revisedRiskTotal = revisedTotalDiv;
            this.likelihoodInput = likelihoodInput;
            this.impactInput = impactInput;
            this.revisedLikelihoodInput = revisedLikelihoodInput;
            this.revisedImpactInput = revisedImpactInput;
            //ensure we have correct values in totals
            this._calculateRiskTotal();
            this._calculateRevisedRiskTotal();
        },
        _calculateTotal: function(impactInput, likelihoodInput, outputDiv) {
            //impact*likelihood
            var impact = impactInput.get('value'),
                likelihood = likelihoodInput.get('value'),
                total = '',
                iScore, lScore;
            if (impact && likelihood && impact !== '' && likelihood !== '') {
                //convert from enum to ordinal value
                iScore = this._findImpactScore(impact);
                lScore = this._findLikelihoodScore(likelihood);
                total = iScore * lScore;
            }
            //set the total value
            outputDiv.set('text', total);
        },
        _calculateRiskTotal: function() {
            this._calculateTotal(this.impactInput, this.likelihoodInput, this.riskTotal);
        },
        _calculateRevisedRiskTotal: function() {
            this._calculateTotal(this.revisedImpactInput, this.revisedLikelihoodInput, this.revisedRiskTotal);
        },
        _findImpactScore: function(enumName) {
            var score = 0,
                impact = this.get('impact'),
                enumObj = A.find(impact, function(o) {
                    return o.enumName === enumName;
                });
            if (enumObj) {
                score = enumObj.enumValue;
            }
            return score;
        },
        _findLikelihoodScore: function(enumName) {
            var score = 0,
                likelihood = this.get('likelihood'),
                enumObj = A.find(likelihood, function(o) {
                    return o.enumName === enumName;
                });
            if (enumObj) {
                score = enumObj.enumValue;
            }
            return score;
        }
    });

    //Add identified risk view
    Y.namespace('app').AddIntakeIdentifiedRiskView = Y.Base.create('addIntakeIdentifiedRiskView', Y.app.BaseIntakeIdentifiedRiskView, [Y.usp.RichText], {
        initializer: function() {
            //call common init code
            this._initializer();
        },
        destructor: function() {
            this._destructor();
        },
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.AddIntakeIdentifiedRiskView.superclass.render.call(this);

            //call common render code
            this._render('addEditIdentifiedRiskForm');
        }
    }, {
        ATTRS: {
            impact: {
                value: Y.usp.resolveassessment.enum.Impact.values
            },
            likelihood: {
                value: Y.usp.resolveassessment.enum.Likelihood.values
            }
        }
    });

    //Edit identified risk view
    Y.namespace('app').EditIntakeIdentifiedRiskView = Y.Base.create('editIntakeIdentifiedRiskView', Y.app.BaseIntakeIdentifiedRiskView, [Y.usp.RichText], {
        initializer: function() {
            //call common init code
            this._initializer();
        },
        destructor: function() {
            this._destructor();
        },
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.EditIntakeIdentifiedRiskView.superclass.render.call(this);

            //call common render code
            this._render('addEditIdentifiedRiskForm', this.get('model'));
        }
    }, {
        ATTRS: {
            impact: {
                value: Y.usp.resolveassessment.enum.Impact.values
            },
            likelihood: {
                value: Y.usp.resolveassessment.enum.Likelihood.values
            }
        }
    });

    //Remove identified risk view
    Y.namespace('app').RemoveIntakeIdentifiedRiskView = Y.Base.create('removeIntakeIdentifiedRiskView', Y.usp.resolveassessment.IntakeRiskAssessmentView, [], {
        template: Y.Handlebars.templates["intakeFormRemoveIdentifiedRiskDialog"]
    });

    //Add support plan view
    Y.namespace('app').AddIntakeSupportPlanView = Y.Base.create('addIntakeSupportPlanView', Y.usp.resolveassessment.IntakeSupportPlanView, [Y.usp.RichText], {
        events: {
            '#addEditSupportPlanForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditSupportPlanDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        }
    });

    //Edit support plan view
    Y.namespace('app').EditIntakeSupportPlanView = Y.Base.create('editIntakeSupportPlanView', Y.usp.resolveassessment.IntakeSupportPlanView, [Y.usp.RichText], {
        events: {
            '#addEditSupportPlanForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditSupportPlanDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        }
    });

    //Remove support plan view
    Y.namespace('app').RemoveIntakeSupportPlanView = Y.Base.create('removeIntakeSupportPlanView', Y.usp.resolveassessment.IntakeSupportPlanView, [], {
        template: Y.Handlebars.templates["intakeFormRemoveSupportPlanDialog"]
    });

    //Add medication view
    // TODO: use IntakeFormFullDetails until AB delivers IntakeMedications
    Y.namespace('app').AddIntakeMedicationView = Y.Base.create('addIntakeMedicationView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [Y.usp.RichText], {
        events: {
            '#addEditMedicationForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditMedicationDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.AddIntakeMedicationView.superclass.render.call(this);

            var container = this.get('container'),
                medicationFormCategory = Y.uspCategory.resolveassessment.medicationForm.category,
                medicationFormInput = container.one('#addEditMedicationForm_form');

            //setup medication form options
            FU.setSelectOptions(medicationFormInput, O.values(medicationFormCategory.getActiveCodedEntries()), null, null, true, true, 'code');

            //clean up nodes we no longer require
            medicationFormInput.destroy();
        }
    });

    //Edit medication view
    // TODO: use IntakeFormFullDetails until AB delivers IntakeMedications
    Y.namespace('app').EditIntakeMedicationView = Y.Base.create('editIntakeMedicationView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [Y.usp.RichText], {
        events: {
            '#addEditMedicationForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditMedicationDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.AddIntakeMedicationView.superclass.render.call(this);

            var container = this.get('container'),
                model = this.get('model'),
                medicationFormCategory = Y.uspCategory.resolveassessment.medicationForm.category,
                medicationFormInput = container.one('#addEditMedicationForm_form'),
                //setup medication form options
                medicationForm = this._lookupExistingEntry(medicationFormCategory.codedEntries, model.get('form'));

            FU.setSelectOptions(medicationFormInput, O.values(medicationFormCategory.getActiveCodedEntries()), medicationForm.code, medicationForm.name, true, true, 'code');

            //set selected option for medication form
            medicationFormInput.set('value', model.get('form'));

            //clean up nodes we no longer require
            medicationFormInput.destroy();
        },
        _lookupExistingEntry: function(codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        }
    });

    //Remove medication view
    Y.namespace('app').RemoveIntakeMedicationView = Y.Base.create('removeIntakeMedicationView', Y.usp.resolveassessment.IntakeMedicationsView, [], {
        template: Y.Handlebars.templates["intakeFormRemoveMedicationDialog"]
    });

    //Add personal care need view
    Y.namespace('app').AddIntakePersonalCareNeedView = Y.Base.create('addIntakePersonalCareNeedView', Y.usp.resolveassessment.IntakePersonalCareNeedView, [Y.usp.RichText], {
        events: {
            '#addEditPersonalCareNeedForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditPersonalCareNeedDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.AddIntakePersonalCareNeedView.superclass.render.call(this);

            var container = this.get('container'),
                needCategory = Y.uspCategory.resolveassessment.need.category,
                needInput = container.one('#addEditPersonalCareNeedForm_need');

            //setup need options
            FU.setSelectOptions(needInput, O.values(needCategory.getActiveCodedEntries()), null, null, true, true, 'code');

            //clean up nodes we no longer require
            needInput.destroy();
        }
    });

    //Edit personal care need view
    Y.namespace('app').EditIntakePersonalCareNeedView = Y.Base.create('editIntakePersonalCareNeedView', Y.usp.resolveassessment.IntakePersonalCareNeedView, [Y.usp.RichText], {
        events: {
            '#addEditPersonalCareNeedForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddEditPersonalCareNeedDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.EditIntakePersonalCareNeedView.superclass.render.call(this);
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    needTypes: Y.uspCategory.resolveassessment.need.category.codedEntries
                }
            }
        }
    });

    //Remove personal care need view
    Y.namespace('app').RemoveIntakePersonalCareNeedView = Y.Base.create('removeIntakePersonalCareNeedView', Y.usp.resolveassessment.IntakePersonalCareNeedView, [], {
        template: Y.Handlebars.templates["intakeFormRemovePersonalCareNeedDialog"]
    });

    //Add support view
    Y.namespace('app').AddIntakeSupportView = Y.Base.create('addIntakeSupportView', Y.usp.resolveassessment.IntakeSupportView, [Y.usp.RichText], {
        events: {
            '#addSupportForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormAddSupportDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            var container = this.get('container'),
                supportInput;

            //call superclass
            Y.app.AddIntakeSupportView.superclass.render.call(this);

            supportInput = container.one('#addSupportForm_support');

            Y.FUtil.setSelectOptions(supportInput, O.values(Y.uspCategory.resolveassessment.support.category.getActiveCodedEntries()), null, null, true, true, 'code');

            //clean up nodes we no longer require
            supportInput.destroy();
        }
    });

    //Edit support view
    Y.namespace('app').EditIntakeSupportView = Y.Base.create('editIntakeSupportView', Y.usp.resolveassessment.IntakeSupportView, [Y.usp.RichText], {
        events: {
            '#editSupportForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["intakeFormEditSupportDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    supportTypes: Y.uspCategory.resolveassessment.support.category.codedEntries
                }
            }
        }
    });

    //Remove support view
    Y.namespace('app').RemoveIntakeSupportView = Y.Base.create('removeIntakeSupportView', Y.usp.resolveassessment.IntakeSupportView, [], {
        template: Y.Handlebars.templates["intakeFormRemoveSupportDialog"]
    });

    Y.namespace('app').EditIntakeReferencedPersonView = Y.Base.create('editIntakeReferencedPersonView', Y.usp.resolveassessment.IntakeReferencedPersonView, [], {
        template: Y.Handlebars.templates["intakeFormEditReferencedPersonDialog"],
        events: {
            '#editReferencedPersonForm input[name="active"]': {
                change: '_setActive'
            }
        },
        _setActive: function(e) {
            //update the model based on the current checked/unchecked state of the active tick box
            //this will trigget the render phase
            var val = e.currentTarget.get('value')==='true';

            //clear down values on active change
            this.get('model').setAttrs({
                active: val
            }, {
                name: null
            }, {
                address: null
            }, {
                contact: null
            });
        }
    });

    /**
     *Define Base referenced person WITH ROLE class with common functionality
     */
    function BaseIntakeReferencedPersonWithRoleView() {}

    BaseIntakeReferencedPersonWithRoleView.prototype = {
        _init: function() {
            //setup model listener for change event
            var relationshipTypesModel = this.get('relationshipTypes');

            relationshipTypesModel.on('load', this._buildRelationshipTypesList, this);
        },
        _destructor: function() {
            if (this.relationshipTypeSelect) {
                this.relationshipTypeSelect.destroy();

                delete this.relationshipTypeSelect;
            }
        },
        _buildRelationshipTypesList: function() {
            var relationshipTypesModel = this.get('relationshipTypes'),
                types = [];
            if (relationshipTypesModel) {
                //wizz round the model list
                relationshipTypesModel.each(function(relationshipType) {
                    if (relationshipType.get('active') === true) {
                        //setup role A
                        types.push({
                            id: relationshipType.get('id') + '_A',
                            name: relationshipType.get('roleAName')
                        });

                        if (relationshipType.get('roleAName') !== relationshipType.get('roleBName')) {
                            //only set up role B for asymetric role type
                            types.push({
                                id: relationshipType.get('id') + '_B',
                                name: relationshipType.get('roleBName')
                            });
                        }
                    }
                });
            }

            return types;
        }
    };

    Y.namespace('app').AddIntakeReferencedPersonWithRoleView = Y.Base.create('addIntakeReferencedPersonWithRoleView', Y.usp.resolveassessment.NewIntakeReferencedPersonView, [BaseIntakeReferencedPersonWithRoleView], {
        template: Y.Handlebars.templates["intakeFormAddReferencedPersonWithRoleDialog"],
        initializer: function() {
            //call common init
            this._init();
        },
        destructor: function() {
            //call common destructor
            this._destructor();
        },
        render: function() {
            var container = this.get('container'),
                types;

            //super class call
            Y.app.AddIntakeReferencedPersonWithRoleView.superclass.render.call(this);

            this.relationshipTypeSelect = container.one('#addReferencedPersonForm_relationshipTypeId');

            //setup lists
            types = this._buildRelationshipTypesList();

            FU.setSelectOptions(this.relationshipTypeSelect, types, null, null, true, true);
        }
    }, {
        ATTRS: {
            relationshipTypes: {}
        }
    });

    Y.namespace('app').EditIntakeReferencedPersonWithRoleView = Y.Base.create('editIntakeReferencedPersonWithRoleView', Y.usp.resolveassessment.IntakeReferencedPersonView, [BaseIntakeReferencedPersonWithRoleView], {
        template: Y.Handlebars.templates["intakeFormEditReferencedPersonWithRoleDialog"],
        initializer: function() {
            //call common init
            this._init();
        },
        destructor: function() {
            //call common destructor
            this._destructor();
        },
        render: function() {
            var container = this.get('container'),
                model = this.get('model'),
                currentRelationshipTypeId = model.get('relationshipTypeId'),
                mandatoryInputs = this.get('otherData').mandatoryInputs,
                types;

            //super class call
            Y.app.AddIntakeReferencedPersonWithRoleView.superclass.render.call(this);

            this.relationshipTypeSelect = container.one('#editReferencedPersonForm_relationshipTypeId');

            //setup lists
            types = this._buildRelationshipTypesList();

            //set current selection
            if (currentRelationshipTypeId !== null) {
                currentRelationshipTypeId = currentRelationshipTypeId + '_' + (model.get('relationshipRole') === 'ROLE_A' ? 'A' : 'B');
            }


            FU.setSelectOptions(this.relationshipTypeSelect, types, currentRelationshipTypeId, model.get('relationship'), mandatoryInputs?false:true, true);
            if (currentRelationshipTypeId !== null) {
                this.relationshipTypeSelect.set('value', currentRelationshipTypeId);
            }
        }
    }, {
        ATTRS: {
            relationshipTypes: {}
        }
    });

    //Remove Referenced Person
    Y.namespace('app').RemoveIntakeReferencedPersonView = Y.Base.create('removeIntakeReferencedPersonView', Y.usp.resolveassessment.IntakeReferencedPersonView, [], {
        template: Y.Handlebars.templates["intakeFormRemoveReferencedPersonDialog"]
    });

    //ResolveFormDialog Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').IntakeFormDialog = Y.Base.create('intakeFormDialog', Y.usp.MultiPanelPopUp, [], {
        initializer: function() {
            //plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            //handle that focus
            this.plug(Y.usp.Plugin.RichTextFocusManager);
        },
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            this.unplug(Y.usp.Plugin.RichTextFocusManager);
        },
        showAppendixView: function(viewName, config, model) {
            this.showView(viewName, {
                labels: config.labels,
                model: model,
                otherData: {
                    narrative: L.sub(config.narrative, {
                        formName: config.formName
                    }),
                    formModel: config.formModel
                },
                appendixType: config.appendixType,
                saveUrl: config.saveUrl,
                template: config.template,
                popupCalendarsConfig: config.popupCalendarsConfig,
                transmogrify: config.transmogrify
            }, {
                modelLoad: true,
                payload: {
                    id: config.formModel.id
                }
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        views: {
            editIntakeFormConsent: {
                type: Y.app.IntakeFormEditConsentView,
                buttons: editFormButtons
            },
            editIntakeFormBriefHistory: {
                type: Y.app.IntakeFormEditBriefHistoryView,
                buttons: editFormButtons
            },
            editIntakeFormCurrentPlacement: {
                type: Y.app.IntakeFormEditCurrentPlacementView,
                buttons: editFormButtons
            },
            editIntakeFormLifestyle: {
                type: Y.app.IntakeFormEditLifestyleView,
                buttons: editFormButtons
            },
            editIntakeFormReferrer: {
                type: Y.app.IntakeFormEditReferrerView,
                buttons: editFormButtons
            },
            editIntakeFormFuture: {
                type: Y.app.IntakeFormEditFutureView,
                buttons: editFormButtons
            },
            editIntakeFormEating: {
                type: Y.app.IntakeFormEditEatingView,
                buttons: editFormButtons
            },
            editIntakeFormSocial: {
                type: Y.app.IntakeFormEditSocialView,
                buttons: editSocialButtons
            },
            editIntakeFormCommunicationIssues: {
                type: Y.app.IntakeFormEditCommunicationIssuesView,
                buttons: editFormButtons
            },
            editIntakeFormSpiritualNeeds: {
                type: Y.app.IntakeFormEditSpiritualNeedsView,
                buttons: editFormButtons
            },
            editIntakeFormMobility: {
                type: Y.app.IntakeFormEditMobilityView,
                buttons: editFormButtons
            },
            editIntakeFormOccupationalIssues: {
                type: Y.app.IntakeFormEditOccupationalIssuesView,
                buttons: editFormButtons
            },
            editIntakeFormAppendix: {
                type: Y.app.IntakeFormEditAppendixView,
                buttons: editFormAppendixButtons
            },
            editIntakeFormAppendixTimeOrientation: {
                type: Y.app.IntakeFormEditAppendixTimeOrientationView,
                buttons: editFormAppendixButtons
            },
            editIntakeFormAppendixAppetite: {
                type: Y.app.IntakeFormEditAppendixAppetiteView,
                buttons: editFormAppendixButtons
            },
            editIntakeFormAppendixOtherWellbeing: {
                type: Y.app.IntakeFormEditAppendixOtherWellbeingView,
                buttons: editFormAppendixButtons
            },
            editIntakeFormHealth: {
                type: Y.app.IntakeFormEditHealthView,
                buttons: editFormButtons
            },
            editIntakeFormMentalHealth: {
                type: Y.app.IntakeFormEditMentalHealthView,
                buttons: editFormButtons
            },
            editIntakeFormHealthActionPlan: {
                type: Y.app.IntakeFormEditHealthActionPlanView,
                buttons: editFormButtons
            },
            editIntakeFormPsychology: {
                type: Y.app.IntakeFormEditPsychologyView,
                buttons: editFormButtons
            },
            addPreviousPlacement: {
                type: Y.app.AddEditIntakePreviousPlacementView,
                buttons: addPreviousPlacementButtons
            },
            editPreviousPlacement: {
                type: Y.app.AddEditIntakePreviousPlacementView,
                buttons: editPreviousPlacementButtons
            },
            removePreviousPlacement: {
                type: Y.app.RemoveIntakePreviousPlacementView,
                buttons: removePreviousPlacementButtons
            },
            addIdentifiedRisk: {
                type: Y.app.AddIntakeIdentifiedRiskView,
                buttons: addIdentifiedRiskButtons
            },
            editIdentifiedRisk: {
                type: Y.app.EditIntakeIdentifiedRiskView,
                buttons: editIdentifiedRiskButtons
            },
            removeIdentifiedRisk: {
                type: Y.app.RemoveIntakeIdentifiedRiskView,
                buttons: removeIdentifiedRiskButtons
            },
            addSupportPlan: {
                type: Y.app.AddIntakeSupportPlanView,
                buttons: addSupportPlanButtons
            },
            editSupportPlan: {
                type: Y.app.EditIntakeSupportPlanView,
                buttons: editSupportPlanButtons
            },
            removeSupportPlan: {
                type: Y.app.RemoveIntakeSupportPlanView,
                buttons: removeSupportPlanButtons
            },
            addMedication: {
                type: Y.app.AddIntakeMedicationView,
                buttons: addMedicationButtons
            },
            editMedication: {
                type: Y.app.EditIntakeMedicationView,
                buttons: editMedicationButtons
            },
            removeMedication: {
                type: Y.app.RemoveIntakeMedicationView,
                buttons: removeMedicationButtons
            },
            addPersonalCareNeed: {
                type: Y.app.AddIntakePersonalCareNeedView,
                buttons: addPersonalCareNeedButtons
            },
            editPersonalCareNeed: {
                type: Y.app.EditIntakePersonalCareNeedView,
                buttons: editPersonalCareNeedButtons
            },
            removePersonalCareNeed: {
                type: Y.app.RemoveIntakePersonalCareNeedView,
                buttons: removePersonalCareNeedButtons
            },
            addSocialSupport: {
                type: Y.app.AddIntakeSupportView,
                buttons: addSocialSupportButtons
            },
            editSocialSupport: {
                type: Y.app.EditIntakeSupportView,
                buttons: editSocialSupportButtons
            },
            removeSocialSupport: {
                type: Y.app.RemoveIntakeSupportView,
                buttons: removeSocialSupportButtons
            },
            editIntakeReferencedPerson: {
                type: Y.app.EditIntakeReferencedPersonView,
                buttons: editReferencedPersonButtons
            },
            editIntakeReferencedPersonWithRole: {
                type: Y.app.EditIntakeReferencedPersonWithRoleView,
                buttons: editReferencedPersonButtons
            },
            addIntakeReferencedPersonWithRole: {
                type: Y.app.AddIntakeReferencedPersonWithRoleView,
                buttons: addReferencedPersonButtons
            },
            removeIntakeReferencedPerson: {
                type: Y.app.RemoveIntakeReferencedPersonView,
                buttons: removeReferencedPersonButtons
            },
            editIntakeFormRejection: {
                type: Y.app.IntakeFormEditRejectionView,
                buttons: editFormRejectionButtons
            }
        }
    }, {
        CSS_PREFIX: 'yui3-panel',
        ATTRS: {
            width: {
                value: 760
            },
            buttons: {
                value: [{
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    classNames: 'pure-button-secondary',
                    action: function(e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER
                }, 'close']
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'multi-panel-popup',
               'multi-panel-model-errors-plugin',
               'model-form-link',
               'model-transmogrify',
               'form-util',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'handlebars-resolveform-templates',
               'handlebars-resolveform-partials',
               'view-rich-text',
               'calendar-popup',
               'resolveassessment-component-enumerations',
               'usp-resolveassessment-IntakeFormFullDetails',
               'usp-resolveassessment-FormRejection',
               'usp-resolveassessment-UpdateFormRejection',
               'usp-resolveassessment-IntakeConsent',
               'usp-resolveassessment-UpdateIntakeConsent',
               'usp-resolveassessment-IntakeHistory',
               'usp-resolveassessment-UpdateIntakeHistory',
               'usp-resolveassessment-IntakeCurrentPlacement',
               'usp-resolveassessment-UpdateIntakeCurrentPlacement',
               'usp-resolveassessment-IntakeHealth',
               'usp-resolveassessment-UpdateIntakeHealth',
               'usp-resolveassessment-IntakeMentalHealth',
               'usp-resolveassessment-UpdateIntakeMentalHealth',
               'usp-resolveassessment-IntakePsychology',
               'usp-resolveassessment-UpdateIntakePsychology',
               'usp-resolveassessment-IntakeOccupationalTherapy',
               'usp-resolveassessment-UpdateIntakeMobility',
               'usp-resolveassessment-IntakeLifestyle',
               'usp-resolveassessment-UpdateIntakeLifestyle',
               'usp-resolveassessment-IntakeMedication',
               'usp-resolveassessment-UpdateIntakeMedication',
               'usp-resolveassessment-IntakeReferral',
               'usp-resolveassessment-UpdateIntakeReferral',
               'usp-resolveassessment-IntakeFuture',
               'usp-resolveassessment-UpdateIntakeFuture',
               'usp-resolveassessment-IntakeSpeech',
               'usp-resolveassessment-UpdateIntakeSpeech',
               'usp-resolveassessment-IntakeSocial',
               'usp-resolveassessment-UpdateIntakeSocial',
               'usp-resolveassessment-NewIntakeSupport',
               'usp-resolveassessment-UpdateIntakeSupport',
               'usp-resolveassessment-IntakeSupport',
               'usp-resolveassessment-IntakeSpiritual',
               'usp-resolveassessment-UpdateIntakeSpiritual',
               'usp-resolveassessment-IntakePhysiotherapy',
               'usp-resolveassessment-UpdateIntakePhysiotherapy',
               'usp-resolveassessment-IntakePreviousPlacement',
               'usp-resolveassessment-NewIntakePreviousPlacement',
               'usp-resolveassessment-UpdateIntakePreviousPlacement',
               'usp-resolveassessment-IntakeRiskAssessment',
               'usp-resolveassessment-NewIntakeRiskAssessment',
               'usp-resolveassessment-UpdateIntakeRiskAssessment',
               'usp-resolveassessment-IntakeSupportPlan',
               'usp-resolveassessment-NewIntakeSupportPlan',
               'usp-resolveassessment-UpdateIntakeSupportPlan',
               'usp-resolveassessment-IntakeMedications',
               'usp-resolveassessment-NewIntakeMedications',
               'usp-resolveassessment-UpdateIntakeMedications',
               'usp-resolveassessment-IntakePersonalCareNeed',
               'usp-resolveassessment-NewIntakePersonalCareNeed',
               'usp-resolveassessment-UpdateIntakePersonalCareNeed',
               'usp-resolveassessment-IntakeReferencedPerson',
               'usp-resolveassessment-NewIntakeReferencedPerson',
               'usp-resolveassessment-UpdateIntakeReferencedPerson',
               'usp-resolveassessment-IntakeAppendixThreeContext',
               'usp-resolveassessment-UpdateIntakeAppendixThreeContext',
               'usp-resolveassessment-IntakeAppendixThreeBehaviour',
               'usp-resolveassessment-UpdateIntakeAppendixThreeBehaviour',
               'usp-resolveassessment-IntakeAppendixTwoContext',
               'usp-resolveassessment-UpdateIntakeAppendixTwoContext',
               'usp-resolveassessment-IntakeAppendixTwoMentalState',
               'usp-resolveassessment-UpdateIntakeAppendixTwoMentalState',
               'usp-resolveassessment-IntakeAppendixTwoOrientation',
               'usp-resolveassessment-UpdateIntakeAppendixTwoOrientation',
               'usp-resolveassessment-IntakeAppendixTwoWellbeing',
               'usp-resolveassessment-UpdateIntakeAppendixTwoWellbeing',
               'usp-resolveassessment-IntakeAppendixTwoOutcome',
               'usp-resolveassessment-UpdateIntakeAppendixTwoOutcome',
               'usp-resolveassessment-IntakeAppendixOneCommunication',
               'usp-resolveassessment-UpdateIntakeAppendixOneCommunication',
               'usp-resolveassessment-IntakeAppendixOneFamilyHistory',
               'usp-resolveassessment-UpdateIntakeAppendixOneFamilyHistory',
               'usp-resolveassessment-IntakeAppendixOneReview',
               'usp-resolveassessment-UpdateIntakeAppendixOneReview',
               'usp-resolveassessment-IntakeAppendixOneProblems',
               'usp-resolveassessment-UpdateIntakeAppendixOneProblems',
               'usp-resolveassessment-IntakeAppendixOneFootCare',
               'usp-resolveassessment-UpdateIntakeAppendixOneFootCare',
               'usp-resolveassessment-IntakeAppendixOneContinence',
               'usp-resolveassessment-UpdateIntakeAppendixOneContinence',
               'usp-resolveassessment-IntakeAppendixOneEpilepsy',
               'usp-resolveassessment-UpdateIntakeAppendixOneEpilepsy',
               'usp-resolveassessment-IntakeAppendixOneSkin',
               'usp-resolveassessment-UpdateIntakeAppendixOneSkin',
               'usp-resolveassessment-IntakeAppendixOneBreathingCirculation',
               'usp-resolveassessment-UpdateIntakeAppendixOneBreathingCirculation',
               'usp-resolveassessment-IntakeAppendixOneMensHealth',
               'usp-resolveassessment-UpdateIntakeAppendixOneMensHealth',
               'usp-resolveassessment-UpdateIntakeTimetableEvent',
               'categories-resolveassessment-component-Need',
               'categories-resolveassessment-component-MedicationForm',
               'categories-resolveassessment-component-Support'
              ]
});