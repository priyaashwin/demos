YUI.add('formstatus-dialog-views', function (Y) {	
	//Status change form (used for all status changes except 'reject') - simple YUI form
	Y.namespace('app').StatusChangeForm = Y.Base.create('statusChangeForm', Y.Model, [Y.ModelSync.REST],{});
	
	//Form for 'reject' status change - model will be transmogrified into an UpdateRejectForm
	Y.namespace('app').RejectStatusForm = Y.Base.create('rejectStatusForm', Y.usp.resolveassessment.UpdateRejectForm, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
		form:'#rejectFormForm'
	});

    Y.namespace('app').FormStatusChangeView = Y.Base.create('formStatusChangeView', Y.View, [], {
        template:Y.Handlebars.templates["statusChangeDialog"],
    	initializer:function(){
			//publish events
			this.publish('saved', {preventable: true, broadcast: true});
    	},
        render:function(){
            var container=this.get('container'),
            formData=this.get('formModel'),
            html = this.template({
                formData:(formData.toJSON)?formData.toJSON():formData,
                modelData:this.get('model').toJSON(),
                otherData:this.get('otherData'),
                message:this.get('message')
            });
                        
            // Render this view's HTML into the container element.
            container.setHTML(html);
            return this;
            
        }
    },{
        ATTRS:{
            model:{},
            /**
             * @attribute otherData Should contain the narrative attribute, along with any other data required by the template
             */
            otherData:{},
            /**
             * @attribute formModel Holds the form model or data about the form used in the narrative
             */
            formModel:{},
            message:{},
            successMessage:{}
        }
    });
	
    Y.namespace('app').RejectFormView = Y.Base.create('rejectFormView', Y.View, [Y.usp.RichText], {
    	events:{
   		 '#rejectFormForm': {submit: '_preventSubmit'}
    	},
    	template:Y.Handlebars.templates["formAddRejectionDialog"],
    	
        render:function(){
            var container=this.get('container'),
            formData=this.get('formModel'),
            html = this.template({
                formData:(formData.toJSON)?formData.toJSON():formData,
                modelData:this.get('model').toJSON(),
                otherData:this.get('otherData'),
                labels:this.get('labels'),
                message:this.get('message')
            });
                        
            // Render this view's HTML into the container element.
            container.setHTML(html);
            return this;
        },

    	_preventSubmit:function(e){
			e.preventDefault();
    	}
    },{
        ATTRS:{
            model:{},
            /**
             * @attribute otherData Should contain the narrative attribute, along with any other data required by the template
             */
            otherData:{},
            /**
             * @attribute formModel Holds the form model or data about the form used in the narrative
             */
            formModel:{},
            message:{},
            labels:{},
            successMessage:{}
        }
    });

    //ResolveStatusFormDialog Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').ResolveStatusFormDialog= Y.Base.create('resolveStatusFormDialog', Y.usp.MultiPanelPopUp, [], {
    	initializer:function(){
    		//plug in the MultiPanel errors handling
    		this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            //handle that focus
            this.plug(Y.usp.Plugin.RichTextFocusManager);    		
    	},
    	destructor:function(){
    		//unplug everything we know about
    		this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            this.unplug(Y.usp.Plugin.RichTextFocusManager);
    	},
        views:{
        	 completeForm:{
        	     type:Y.app.FormStatusChangeView,
        	     buttons:[{
        	              section: Y.WidgetStdMod.FOOTER,
                          name: 'saveButton',
                          labelHTML: '<i class="fa fa-check"></i> Complete',
                          classNames: 'pure-button-primary',
                          action: function(e) {
                        	  this._handleSave(e);
                          },
                          disabled: true
        	     }]
        	 },
        	 reopenForm:{
        	     type:Y.app.FormStatusChangeView,
        	     buttons:[{
        	              section: Y.WidgetStdMod.FOOTER,
                          name: 'saveButton',
                          labelHTML: '<i class="fa fa-check"></i> Reopen',
                          classNames: 'pure-button-primary',
                          action: function(e) {
                        	  this._handleSave(e);
                          },
                          disabled: true
        	     }]
        	 },
        	 rejectForm:{
        	     type:Y.app.RejectFormView,
        	     buttons:[{
        	              section: Y.WidgetStdMod.FOOTER,
                          name: 'saveButton',
                          labelHTML: '<i class="fa fa-check"></i> Reject',
                          classNames: 'pure-button-primary',
                          action: function(e) {
                        	  this._handleSave(e);
                          },
                          disabled: true
        	     }]
        	 },
        	 approveForm:{
        	     type:Y.app.FormStatusChangeView,
        	     buttons:[{
        	              section: Y.WidgetStdMod.FOOTER,
                          name: 'saveButton',
                          labelHTML: '<i class="fa fa-check"></i> Authorise',
                          classNames: 'pure-button-primary',
                          action: function(e) {
                        	  this._handleSave(e);
                          },
                          disabled: true
        	     }]
        	 },
        	 removeForm:{
        	     type:Y.app.FormStatusChangeView,
        	     buttons:[{
        	              section: Y.WidgetStdMod.FOOTER,
                          name: 'saveButton',
                          labelHTML: '<i class="fa fa-check"></i> Delete',
                          classNames: 'pure-button-primary',
                          action: function(e) {
                        	  this._handleSave(e);
                          },
                          disabled: true
        	     }]
        	 }
         },
    	 _handleSave:function(e) {
            e.preventDefault();
            //show the mask
            this.showDialogMask();

            // Get the view and the model
            var view=this.get('activeView'),
            model=view.get('model');

            // Do the save
            model.save(Y.bind(function(err, response){
               //hide the mask
               this.hideDialogMask();

                   if (err===null){
                       this.hide();

                       //fire success message
                       Y.fire('infoMessage:message', {message:view.get('successMessage')});
                       
                       //fire saved event against the view
                       view.fire('saved', {id:model.get('id')});
                   }
               },this));
        }
    },{
    	CSS_PREFIX: 'yui3-panel',
    	ATTRS:{
        	width:{
        		value:560
        	},
        	buttons:{
        		value:[{
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    classNames: 'pure-button-secondary',
                    action: function(e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER
        		},
        		'close']
        	}
    	}
    });
}, '0.0.1', {
    requires: [ 'yui-base',
                'view',
                'multi-panel-popup',
                'multi-panel-model-errors-plugin',
                'model-form-link',
                'model-transmogrify',
                'form-util',
    			'handlebars-helpers',
    			'handlebars-resolveform-templates',
    			'handlebars-resolveform-partials',
                'view-rich-text',
    			'usp-resolveassessment-UpdateRejectForm']
});