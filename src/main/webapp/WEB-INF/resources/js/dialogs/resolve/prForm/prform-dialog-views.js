YUI.add('prform-dialog-views', function(Y) {
    var editFormButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdatePlacementReport
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();

                    if (err === null) {
                        this.hide();

                        //fire saved event against the view
                        view.fire('saved', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editFormRejectionButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.url = view.get('saveUrl');
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateFormRejection
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();

                    if (err === null) {
                        this.hide();

                        //fire saved event against the view
                        view.fire('saved', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }];

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseEditView() {}
    BaseEditView.prototype = {
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        events: {
            '#prEditForm': {
                submit: '_preventSubmit'
            }
        }
    };

    //Model will be transmogrified into an UpdatePlacementReport
    Y.namespace('app').UpdatePlacementReportForm = Y.Base.create('updatePlacementReportForm', Y.usp.resolveassessment.PlacementReport, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#prEditForm'
    });

    //Model will be transmogrified into an UpdateRejectForm
    Y.namespace('app').UpdatePlacementFormRejectionForm = Y.Base.create('updatePlacementFormRejectionForm', Y.usp.resolveassessment.FormRejection, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#rejectFormForm'
    }, {
        ATTRS: {
            subject: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app').PRFormEditBackgroundView = Y.Base.create('prFormEditBackgroundView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditBackgroundDialog"]
    });

    Y.namespace('app').PRFormEditHealthView = Y.Base.create('prFormEditHealthView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditHealthDialog"]
    });

    Y.namespace('app').PRFormEditMedicationView = Y.Base.create('prFormEditMedicationView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditMedicationDialog"]
    });

    Y.namespace('app').PRFormEditGPView = Y.Base.create('prFormEditGPView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditGPDialog"]
    });

    Y.namespace('app').PRFormEditEducationEmploymentView = Y.Base.create('prFormEditEducationEmploymentView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditEducationEmploymentDialog"]
    });

    Y.namespace('app').PRFormEditSocialView = Y.Base.create('prFormEditSocialView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditSocialDialog"]
    });

    Y.namespace('app').PRFormEditSkillsView = Y.Base.create('prFormEditSkillsView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditSkillsDialog"]
    });

    Y.namespace('app').PRFormEditIdentityView = Y.Base.create('prFormEditIdentityView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditIdentityDialog"]
    });

    Y.namespace('app').PRFormEditLeisureView = Y.Base.create('prFormEditLeisureView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditLeisureDialog"]
    });

    Y.namespace('app').PRFormEditContactsView = Y.Base.create('prFormEditContactsView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditContactsDialog"]
    });

    Y.namespace('app').PRFormEditSummaryView = Y.Base.create('prFormEditSummaryView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["prFormEditSummaryDialog"]
    });

    Y.namespace('app').PRFormEditRejectionView = Y.Base.create('prFormEditRejectionView', Y.usp.resolveassessment.UpdatePlacementReportView, [Y.usp.RichText], {
        events: {
            '#rejectFormForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["formEditRejectionDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        }
    });

    //ResolveFormDialog Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').PRFormDialog = Y.Base.create('prFormDialog', Y.usp.MultiPanelPopUp, [], {
        initializer: function() {
            //plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            //handle that focus
            this.plug(Y.usp.Plugin.RichTextFocusManager);
        },
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            this.unplug(Y.usp.Plugin.RichTextFocusManager);
        },
        views: {
            editPRFormBackground: {
                type: Y.app.PRFormEditBackgroundView,
                buttons: editFormButtons
            },
            editPRFormHealth: {
                type: Y.app.PRFormEditHealthView,
                buttons: editFormButtons
            },
            editPRFormMedication: {
                type: Y.app.PRFormEditMedicationView,
                buttons: editFormButtons
            },
            editPRFormGP: {
                type: Y.app.PRFormEditGPView,
                buttons: editFormButtons
            },
            editPRFormEducationEmployment: {
                type: Y.app.PRFormEditEducationEmploymentView,
                buttons: editFormButtons
            },
            editPRFormSocial: {
                type: Y.app.PRFormEditSocialView,
                buttons: editFormButtons
            },
            editPRFormSkills: {
                type: Y.app.PRFormEditSkillsView,
                buttons: editFormButtons
            },
            editPRFormIdentity: {
                type: Y.app.PRFormEditIdentityView,
                buttons: editFormButtons
            },
            editPRFormLeisure: {
                type: Y.app.PRFormEditLeisureView,
                buttons: editFormButtons
            },
            editPRFormContacts: {
                type: Y.app.PRFormEditContactsView,
                buttons: editFormButtons
            },
            editPRFormSummary: {
                type: Y.app.PRFormEditSummaryView,
                buttons: editFormButtons
            },
            editPRFormRejection: {
                type: Y.app.PRFormEditRejectionView,
                buttons: editFormRejectionButtons
            }
        }
    }, {
        CSS_PREFIX: 'yui3-panel',
        ATTRS: {
            width: {
                value: 760
            },
            buttons: {
                value: [{
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    classNames: 'pure-button-secondary',
                    action: function(e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER
                }, 'close']
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'multi-panel-popup',
               'multi-panel-model-errors-plugin',
               'model-form-link',
               'model-transmogrify',
               'form-util',
               'handlebars-helpers',
               'handlebars-prform-templates',
               'handlebars-resolveform-templates',
               'handlebars-resolveform-partials',
               'view-rich-text',
               'usp-resolveassessment-PlacementReport',
               'usp-resolveassessment-FormRejection',
               'usp-resolveassessment-UpdatePlacementReport',
               'usp-resolveassessment-UpdateFormRejection'
              ]
});