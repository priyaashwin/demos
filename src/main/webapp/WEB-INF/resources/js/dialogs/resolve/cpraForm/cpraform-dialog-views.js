YUI.add('cpraform-dialog-views', function(Y) {
    var FU = Y.FUtil,
        A = Y.Array,
        setEnumVal = function(v) {
            if ('' === v) {
                return null;
            }
            return v;
        },
        editFormButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');
                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateCarePlanRiskAssessment
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire saved event against the view
                        view.fire('saved', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editFormRejectionButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();

                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');

                // Do the save
                model.url = view.get('saveUrl');
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateFormRejection
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();

                    if (err === null) {
                        this.hide();

                        //fire saved event against the view
                        view.fire('saved', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        addRiskButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model'),
                    riskType = view.get('riskType');
                // Do the save
                model.save(Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedRisk event against the view
                        view.fire('savedRisk', {
                            riskType: riskType
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editRiskButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model'),
                    riskType = view.get('riskType');
                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateIdentifiedRisk
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedRisk event against the view
                        view.fire('savedRisk', {
                            id: model.get('id'),
                            riskType: riskType
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        removeRiskButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'removeButton',
            labelHTML: '<i class="fa fa-check"></i> Remove',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model'),
                    riskType = view.get('riskType');
                // Do the delete
                model.destroy({
                    remove: true
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire removedRisk event against the view
                        view.fire('removedRisk', {
                            id: model.get('id'),
                            riskType: riskType
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editKeyAreaButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');
                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateKeyArea
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedRisk event against the view
                        view.fire('savedKeyArea', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }],
        editProgressReportButtons = [{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
                e.preventDefault();
                //show the mask
                this.showDialogMask();
                // Get the view and the model
                var view = this.get('activeView'),
                    model = view.get('model');
                // Do the save
                model.save({
                    transmogrify: Y.usp.resolveassessment.UpdateProgressReport
                }, Y.bind(function(err, response) {
                    //hide the mask
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        //fire savedRisk event against the view
                        view.fire('savedProgressReport', {
                            id: model.get('id')
                        });
                    }
                }, this));
            },
            disabled: true
        }];


    //Base remove functionality - specific model classes will be augmented with this code
    function BaseRemoveForm() {}

    BaseRemoveForm.prototype = {
        destroy: function(options, callback) {
            var self = this,
                facade = {
                    options: options
                },
                finish = Y.bind(function(err) {
                    if (!err) {
                        //call the superclass
                        this.constructor.superclass.destroy.call(this);
                    } else {
                        facade.error = err;
                        facade.src = 'remove';
                        facade.response = arguments[1] || '500';
                        this.fire('error', facade);
                    }
                    if (callback) {
                        callback(err || null, arguments[1]);
                    }
                }, self);

            if (typeof options === 'function') {
                callback = options;
                options = null;
            }

            if (options && (options.remove || options['delete'])) {
                self.sync('delete', options, finish);
            } else {
                finish();
            }
            return self;
        }
    };

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseEditView() {}
    BaseEditView.prototype = {
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        events: {
            '#cpraEditForm': {
                submit: '_preventSubmit'
            }
        }
    };

    //Model will be transmogrified into an UpdateRejectForm
    Y.namespace('app').UpdateCarePlanRiskAssessmentFormRejectionForm = Y.Base.create('updateCarePlanRiskAssessmentFormRejectionForm', Y.usp.resolveassessment.FormRejection, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#rejectFormForm'
    }, {
        ATTRS: {
            subject: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app').AddIdentifiedRiskForm = Y.Base.create('addIdentifiedRiskForm', Y.usp.resolveassessment.NewIdentifiedRisk, [Y.usp.ModelFormLink], {
        form: '#addRiskForm'
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            likelihood: {
                setter: setEnumVal
            },
            impact: {
                setter: setEnumVal
            },
            revisedLikelihood: {
                setter: setEnumVal
            },
            revisedImpact: {
                setter: setEnumVal
            }
        }
    });

    //Model will be transmogrified into an UpdateIdentifiedRisk
    Y.namespace('app').UpdateIdentifiedRiskForm = Y.Base.create('updateIdentifiedRiskForm', Y.usp.resolveassessment.IdentifiedRisk, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editRiskForm'
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            likelihood: {
                setter: setEnumVal
            },
            impact: {
                setter: setEnumVal
            },
            revisedLikelihood: {
                setter: setEnumVal
            },
            revisedImpact: {
                setter: setEnumVal
            }
        }
    });

    //Remove IdentifiedRisk - simple YUI form
    Y.namespace('app').RemoveIdentifiedRiskForm = Y.Base.create('removeIdentifiedRiskForm', Y.Model, [Y.ModelSync.REST, BaseRemoveForm], {});

    //Model will be transmogrified into an UpdateCarePlanRiskAssessment
    Y.namespace('app').UpdateCarePlanRiskAssessmentForm = Y.Base.create('updateCarePlanRiskAssessmentForm', Y.usp.resolveassessment.CarePlanRiskAssessment, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#cpraEditForm'
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            historicalRiskLevel: {
                setter: setEnumVal
            }
        }
    });

    //Model will be transmogrified into an UpdateKeyArea
    Y.namespace('app').UpdateKeyAreaForm = Y.Base.create('updateKeyAreaForm', Y.usp.resolveassessment.KeyArea, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editKeyAreaForm'
    });

    //Model will be transmogrified into an UpdateProgressReport
    Y.namespace('app').UpdateProgressReportForm = Y.Base.create('updateProgressReportForm', Y.usp.resolveassessment.ProgressReport, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#editProgressReportForm'
    });

    function BaseIdentifiedRiskView() {}
        /**
         * Common function that will be augmented against Add and Edit identified risk view
         */
    BaseIdentifiedRiskView.prototype = {
        _calculateTotal: function(impactInput, likelihoodInput, outputDiv) {
            //impact*likelihood
            var impact = impactInput.get('value'),
                likelihood = likelihoodInput.get('value'),
                total = '',
                iScore, lScore;
            if (impact && likelihood && impact !== '' && likelihood !== '') {
                //convert from enum to ordinal value
                iScore = this._findImpactScore(impact);
                lScore = this.findLikelihoodScore(likelihood);
                total = iScore * lScore;
            }
            //set the total value
            outputDiv.set('text', total);
        },
        _calculateRiskTotal: function() {
            this._calculateTotal(this.impactInput, this.likelihoodInput, this.riskTotal);
        },
        _calculateRevisedRiskTotal: function() {
            this._calculateTotal(this.revisedImpactInput, this.revisedLikelihoodInput, this.revisedRiskTotal);
        },
        _findImpactScore: function(enumName) {
            var score = 0,
                impact = this.get('impact'),
                enumObj = A.find(impact, function(o) {
                    return o.enumName === enumName;
                });
            if (enumObj) {
                score = enumObj.enumValue;
            }
            return score;
        },
        findLikelihoodScore: function(enumName) {
            var score = 0,
                likelihood = this.get('likelihood'),
                enumObj = A.find(likelihood, function(o) {
                    return o.enumName === enumName;
                });
            if (enumObj) {
                score = enumObj.enumValue;
            }
            return score;
        },
        _initializer: function() {
            this.riskDateCalendar = new Y.usp.CalendarPopup();
        },
        _destructor: function() {
            //destroy the calendar
            this.riskDateCalendar.destroy();
            delete this.riskDateCalendar;
            if (this.riskDateInput) {
                this.riskDateInput.destroy();
                delete this.riskDateInput;
            }
            if (this.riskTotal) {
                this.riskTotal.destroy();
                delete this.riskTotal;
            }
            if (this.likelihoodInput) {
                this.likelihoodInput.destroy();
                delete this.likelihoodInput;
            }
            if (this.impactInput) {
                this.impactInput.destroy();
                delete this.impactInput;
            }
            if (this.revisedRiskTotal) {
                this.revisedRiskTotal.destroy();
                delete this.revisedRiskTotal;
            }
            if (this.revisedLikelihoodInput) {
                this.revisedLikelihoodInput.destroy();
                delete this.revisedLikelihoodInput;
            }
            if (this.revisedImpactInput) {
                this.revisedImpactInput.destroy();
                delete this.revisedImpactInput;
            }
        },
        _render: function(prefix, model) {
            var container = this.get('container'),
                riskDateInput = container.one('#' + prefix + '_riskDate'),
                //lookup divs for total calculation
                totalDiv = container.one('#' + prefix + '_total'),
                likelihoodInput = container.one('#' + prefix + '_likelihood'),
                impactInput = container.one('#' + prefix + '_impact'),
                revisedTotalDiv = container.one('#' + prefix + '_revisedTotal'),
                revisedLikelihoodInput = container.one('#' + prefix + '_revisedLikelihood'),
                revisedImpactInput = container.one('#' + prefix + '_revisedImpact'),
                impactOptions = this.get('impact'),
                likelihoodOptions = this.get('likelihood');

            //setup impact options
            FU.setSelectOptions(impactInput, impactOptions, null, null, true, true, 'enumName');
            //set likelihood options
            FU.setSelectOptions(likelihoodInput, likelihoodOptions, null, null, true, true, 'enumName');
            //set revised impact options
            FU.setSelectOptions(revisedImpactInput, impactOptions, null, null, true, true, 'enumName');
            //set revised likelihood options
            FU.setSelectOptions(revisedLikelihoodInput, likelihoodOptions, null, null, true, true, 'enumName');

            if (model) {
                //perform the selections
                impactInput.set('value', model.get('impact'));
                likelihoodInput.set('value', model.get('likelihood'));
                revisedImpactInput.set('value', model.get('revisedImpact'));
                revisedLikelihoodInput.set('value', model.get('revisedLikelihood'));
            }

            //configure the date calendar input node
            this.riskDateCalendar.set('inputNode', riskDateInput);
            //render the calendar
            this.riskDateCalendar.render();
            //cache the date input to destroy later
            this.riskDateInput = riskDateInput;
            //cache those risk total divs
            this.riskTotal = totalDiv;
            this.revisedRiskTotal = revisedTotalDiv;
            this.likelihoodInput = likelihoodInput;
            this.impactInput = impactInput;
            this.revisedLikelihoodInput = revisedLikelihoodInput;
            this.revisedImpactInput = revisedImpactInput;
            //ensure we have correct values in totals
            this._calculateRiskTotal();
            this._calculateRevisedRiskTotal();
        }
    };

    Y.namespace('app').AddIdentifiedRiskView = Y.Base.create('addIdentifiedRiskView', Y.usp.resolveassessment.NewIdentifiedRiskView, [Y.usp.RichText, BaseIdentifiedRiskView], {
        events: {
            '#addRiskForm': {
                submit: '_preventSubmit'
            },
            //change events for risk and revised risk
            '#addRiskForm_likelihood': {
                change: '_calculateRiskTotal'
            },
            '#addRiskForm_impact': {
                change: '_calculateRiskTotal'
            },
            '#addRiskForm_revisedLikelihood': {
                change: '_calculateRevisedRiskTotal'
            },
            '#addRiskForm_revisedImpact': {
                change: '_calculateRevisedRiskTotal'
            }
        },
        template: Y.Handlebars.templates["cpraFormAddRiskDialog"],
        initializer: function() {
            //call common init code
            this._initializer();
        },
        destructor: function() {
            this._destructor();
        },
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.AddIdentifiedRiskView.superclass.render.call(this);

            //call common render code
            this._render('addRiskForm');
        }
    }, {
        ATTRS: {
            impact: {
                value: Y.usp.resolveassessment.enum.Impact.values
            },
            likelihood: {
                value: Y.usp.resolveassessment.enum.Likelihood.values
            },
            riskType: {}
        }
    });

    Y.namespace('app').EditIdentifiedRiskView = Y.Base.create('editIdentifiedRiskView', Y.usp.resolveassessment.UpdateIdentifiedRiskView, [Y.usp.RichText, BaseIdentifiedRiskView], {
        events: {
            '#editRiskForm': {
                submit: '_preventSubmit'
            },
            //change events for risk and revised risk
            '#editRiskForm_likelihood': {
                change: '_calculateRiskTotal'
            },
            '#editRiskForm_impact': {
                change: '_calculateRiskTotal'
            },
            '#editRiskForm_revisedLikelihood': {
                change: '_calculateRevisedRiskTotal'
            },
            '#editRiskForm_revisedImpact': {
                change: '_calculateRevisedRiskTotal'
            }
        },
        template: Y.Handlebars.templates["cpraFormEditRiskDialog"],
        initializer: function() {
            //call common init code
            this._initializer();
        },
        destructor: function() {
            this._destructor();
        },
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        render: function() {
            //call superclass
            Y.app.EditIdentifiedRiskView.superclass.render.call(this);

            //call common render code
            this._render('editRiskForm', this.get('model'));
        }
    }, {
        ATTRS: {
            impact: {
                value: Y.usp.resolveassessment.enum.Impact.values
            },
            likelihood: {
                value: Y.usp.resolveassessment.enum.Likelihood.values
            },
            riskType: {}
        }
    });



    //Remove Identified Risk
    Y.namespace('app').RemoveIdentifiedRiskView = Y.Base.create('removeIdentifiedRiskView', Y.usp.resolveassessment.IdentifiedRiskView, [], {
        template: Y.Handlebars.templates["cpraFormRemoveRiskDialog"]
    }, {
        ATTRS: {
            riskType: {}
        }
    });

    Y.namespace('app').EditKeyAreaView = Y.Base.create('editKeyAreaView', Y.usp.resolveassessment.KeyAreaView, [Y.usp.RichText], {
        events: {
            '#editKeyAreaForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["cpraFormEditKeyAreaDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        }
    });

    Y.namespace('app').EditProgressReportView = Y.Base.create('editProgressReportView', Y.usp.resolveassessment.ProgressReportView, [Y.usp.RichText], {
        events: {
            '#editProgressReportForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["cpraFormEditProgressReportDialog"],
        _preventSubmit: function(e) {
            e.preventDefault();
        },
        initializer: function() {
            this.reviewDateCalendar = new Y.usp.CalendarPopup();
        },
        destructor: function() {
            //destroy the calendar
            this.reviewDateCalendar.destroy();
            delete this.reviewDateCalendar;
            if (this.reviewDateInput) {
                this.reviewDateInput.destroy();
                delete this.reviewDateInput;
            }
        },
        render: function() {
            //call superclass
            Y.app.EditProgressReportView.superclass.render.call(this);
            var container = this.get('container'),
                reviewDateInput = container.one('#editProgressReportForm_reviewDate');

            //configure the date calendar input node
            this.reviewDateCalendar.set('inputNode', reviewDateInput);
            //render the calendar
            this.reviewDateCalendar.render();
            //cache the date input to destroy later
            this.reviewDateInput = reviewDateInput;
        }
    }, {
        ATTRS: {
            //pass as coded entries - but actually enum
            codedEntries: {
                value: {
                    planType: Y.usp.resolveassessment.enum.CarePlanType.values
                }
            }
        }
    });

    Y.namespace('app').CPRAFormEditCommsAndRisksView = Y.Base.create('cpraFormEditCommsAndRisksView', Y.usp.resolveassessment.UpdateCarePlanRiskAssessmentView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["cpraFormEditCommsAndRisksDialog"],

        render: function() {
            //call superclass
            Y.app.CPRAFormEditCommsAndRisksView.superclass.render.call(this);

            var container = this.get('container'),
                model = this.get('model'),
                historicalRiskLevelInput = container.one('#cpraEditForm_historicalRiskLevel'),
                historicalRiskLevelOptions = this.get('historicalRiskLevel');

            //setup impact options
            FU.setSelectOptions(historicalRiskLevelInput, historicalRiskLevelOptions, null, null, true, true, 'enumName');

            if (model) {
                //perform the selections
                historicalRiskLevelInput.set('value', model.get('historicalRiskLevel'));
            }
            this.historicalRiskLevelInput = historicalRiskLevelInput;
        },
        destructor: function() {
            if (this.historicalRiskLevelInput) {
                this.historicalRiskLevelInput.destroy();
                delete this.historicalRiskLevelInput;
            }
        }
    }, {
        ATTRS: {
            //prevent enum values from setting blank
            historicalRiskLevel: {
                value: Y.usp.resolveassessment.enum.HistoricalRiskLevel.values
            }
        }
    });

    Y.namespace('app').CPRAFormEditServiceUserView = Y.Base.create('cpraFormEditServiceUserView', Y.usp.resolveassessment.UpdateCarePlanRiskAssessmentView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["cpraFormEditServiceUserDialog"]
    });

    Y.namespace('app').CPRAFormEditRelativeView = Y.Base.create('cpraFormEditRelativeView', Y.usp.resolveassessment.UpdateCarePlanRiskAssessmentView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["cpraFormEditRelativeDialog"]
    });

    Y.namespace('app').CPRAFormEditCarersPerspectiveView = Y.Base.create('cpraFormEditCarersPerspectiveView', Y.usp.resolveassessment.UpdateCarePlanRiskAssessmentView, [Y.usp.RichText, BaseEditView], {
        template: Y.Handlebars.templates["cpraFormEditCarersPerspectiveDialog"]
    });

    Y.namespace('app').CPRAFormEditRejectionView = Y.Base.create('cpraFormEditRejectionView', Y.usp.resolveassessment.UpdateCarePlanRiskAssessmentView, [Y.usp.RichText], {
        events: {
            '#rejectFormForm': {
                submit: '_preventSubmit'
            }
        },
        template: Y.Handlebars.templates["formEditRejectionDialog"],
        _preventSubmit: function(e) {
                e.preventDefault();
        }
    });

    //ResolveFormDialog Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').CPRAFormDialog = Y.Base.create('cpraFormDialog', Y.usp.MultiPanelPopUp, [], {
        initializer: function() {
            //plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            //handle that focus
            this.plug(Y.usp.Plugin.RichTextFocusManager);
        },
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
            this.unplug(Y.usp.Plugin.RichTextFocusManager);
        },
        views: {
            addIdentifiedRisk: {
                type: Y.app.AddIdentifiedRiskView,
                buttons: addRiskButtons
            },
            editIdentifiedRisk: {
                type: Y.app.EditIdentifiedRiskView,
                buttons: editRiskButtons
            },
            removeIdentifiedRisk: {
                type: Y.app.RemoveIdentifiedRiskView,
                buttons: removeRiskButtons
            },
            editKeyArea: {
                type: Y.app.EditKeyAreaView,
                buttons: editKeyAreaButtons
            },
            editCPRAFormCommsAndRisks: {
                type: Y.app.CPRAFormEditCommsAndRisksView,
                buttons: editFormButtons
            },
            editCPRAFormServiceUser: {
                type: Y.app.CPRAFormEditServiceUserView,
                buttons: editFormButtons
            },
            editCPRAFormRelative: {
                type: Y.app.CPRAFormEditRelativeView,
                buttons: editFormButtons
            },
            editCPRAFormCarersPerspective: {
                type: Y.app.CPRAFormEditCarersPerspectiveView,
                buttons: editFormButtons
            },
            editProgressReport: {
                type: Y.app.EditProgressReportView,
                buttons: editProgressReportButtons
            },
            editCPRAFormRejection: {
                type: Y.app.CPRAFormEditRejectionView,
                buttons: editFormRejectionButtons
            }
        }
    }, {
        CSS_PREFIX: 'yui3-panel',
        ATTRS: {
            width: {
                value: 760
            },
            buttons: {
                value: [{
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    classNames: 'pure-button-secondary',
                    action: function(e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER
                }, 'close']
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'model',
               'multi-panel-popup',
               'multi-panel-model-errors-plugin',
               'model-form-link',
               'model-transmogrify',
               'form-util',
               'handlebars-helpers',
               'handlebars-cpraform-templates',
               'handlebars-resolveform-templates',
               'handlebars-resolveform-partials',
               'view-rich-text',
               'calendar-popup',
               'resolveassessment-component-enumerations',
               'usp-resolveassessment-CarePlanRiskAssessment',
               'usp-resolveassessment-FormRejection',
               'usp-resolveassessment-UpdateCarePlanRiskAssessment',
               'usp-resolveassessment-UpdateFormRejection',
               'usp-resolveassessment-IdentifiedRisk',
               'usp-resolveassessment-NewIdentifiedRisk',
               'usp-resolveassessment-UpdateIdentifiedRisk',
               'usp-resolveassessment-KeyArea',
               'usp-resolveassessment-UpdateKeyArea',
               'usp-resolveassessment-ProgressReport',
               'usp-resolveassessment-UpdateProgressReport'
              ]
});