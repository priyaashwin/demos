YUI.add('entry-dialog-views', function (Y) {

    var O = Y.Object;

    Y.namespace('app.entry').BaseEntryView = Y.Base.create('baseEntryView', Y.View, [], {
        events: {
            'input[name="visibleToAll"]': {
                change: 'handleVisibleToAllChange'
            },
            'input[type=checkbox]#select-all-members-input': {
                change: 'toggleSelectAllMembers'
            },
            '.groupMembers input[type=checkbox]': {
                change: 'updateSelectAllToggle'
            }
        },
        initializer: function (config) {
            //merge our events with the base-class
            this.events = Y.merge(this.events, Y.app.entry.BaseEntryView.superclass.events);

            this._baseEntryViewEvtHandles = [
                //event handler for tab change
                this.after(['*:selectionChange', '*:load'], this._align, this)
            ];

            if (this.get('isGroup')) {
                this.setMemberList();
            }

        },
        destructor: function () {
            this._baseEntryViewEvtHandles.forEach(function (handler) {
                handler.detach();
            });
            delete this._baseEntryViewEvtHandles;

            if (this.tabView) {
                this.tabView.removeTarget(this);

                this.tabView.destroy();

                delete this.tabView;
            }
        },
        toggleSelectAllMembers: function (e) {
            var container = this.get('container'),
                groupMembersList = container.all('.groupMembers input[type=checkbox]');
            groupMembersList.each(function (groupMemberNode) {
                groupMemberNode.set('checked', e.currentTarget.get('checked'));
            });
            this.updateSelectAllToggle();
        },
        updateSelectAllToggle: function (e) {
            var container = this.get('container'),
                groupMembersList = container.all('.groupMembers input[type=checkbox]'),
                selectAllNode = container.one('input[type=checkbox]#select-all-members-input');

            var checked = groupMembersList.filter('input[type=checkbox]:checked');

            if (checked.size() === groupMembersList.size()) {
                selectAllNode.set('checked', true);
            } else {
                selectAllNode.set('checked', false);
            }
        },

        handleVisibleToAllChange: function (e) {
            var currentTarget = e.currentTarget,
                checked = currentTarget.get('checked');

            this._setVisibleToAll(checked);
            this.updateSelectAllToggle();
        },
        _setEntrySubTypeOptions: function (category, parentCode, entrySubType) {
            var entrySubTypeCode = entrySubType ? entrySubType.code : null,
                entrySubTypeName = entrySubType ? entrySubType.name : null;

            var currentCodedEntry = Y.uspCategory[category].entryType.category.code,
                entrySubTypes = O.values(Y.uspCategory[category].entrySubType.category.getActiveCodedEntriesForParent(parentCode, currentCodedEntry)),
                blankMessage = entrySubTypes.length < 1 ? 'No sub types available' : 'No sub type selected';

            if (entrySubTypes.length > 0) {
                entrySubTypes.unshift({
                    code: '',
                    name: 'No sub type selected'
                });
            }

            Y.FUtil.setSelectOptions(this.getNode('entrySubTypeSelect'), entrySubTypes, entrySubTypeCode, entrySubTypeName, blankMessage, true, 'code');
        },
        _setSecuredEntrySubTypeOptions: function (category, parentCode, entrySubType, accessLevel) {
            var entrySubTypeCode = entrySubType ? entrySubType.code : null,
                entrySubTypeName = entrySubType ? entrySubType.name : null;

            var currentCodedEntry = Y.secured.uspCategory[category].entryType.category.code,
                entrySubTypes = O.values(Y.secured.uspCategory[category].entrySubType.category.getActiveCodedEntriesForParent(parentCode, currentCodedEntry, accessLevel)),
                blankMessage = entrySubTypes.length < 1 ? 'No sub types available' : 'No sub type selected';

            if (entrySubTypes.length > 0) {
                entrySubTypes.unshift({
                    code: '',
                    name: 'No sub type selected'
                });
            }

            Y.FUtil.setSelectOptions(this.getNode('entrySubTypeSelect'), entrySubTypes, entrySubTypeCode, entrySubTypeName, blankMessage, true, 'code');
        },
        _setVisibleToAll: function (visibleToAll) {
            var container = this.get('container'),
                selectAllInput = container.one('#select-all-members-input'),
                groupMembersList = container.all('input[name="personVisibility"]'),
                memberContainer = container.one('.groupMembers'),
                personVisibility = this.get('model').get('personVisibility') || [];

            //clear all selected members
            if (visibleToAll) {
                memberContainer.addClass('disabledBackground');

                groupMembersList.each(function (groupMemberNode) {
                    groupMemberNode.set('checked', true).setAttribute('disabled', 'disabled');
                });
                selectAllInput.set('checked', false).setAttribute('disabled', 'disabled');
            } else {
                memberContainer.removeClass('disabledBackground');
                //enable all select boxes
                groupMembersList.removeAttribute('disabled');

                if (personVisibility.length === 0) {
                    //select all
                    groupMembersList.set('checked', true);
                } else {
                    Y.Array.each(personVisibility, function (id) {
                        groupMembersList.filter('input[value="' + id + '"]').set('checked', true);
                    }, this);
                }

                //if zero pre-selected or all are selected ensure the selectAll is set
                if (personVisibility.length === 0 || personVisibility.length === groupMembersList.size()) {
                    selectAllInput.set('checked', true).removeAttribute('disabled');
                } else {
                    selectAllInput.removeAttribute('disabled');
                }
            }
        },
        _initVisibilityMembers: function () {
            var model = this.get('model');

            //ensure tick boxes are set
            this._setVisibleToAll(model.get('visibleToAll'));

            this.updateSelectAllToggle();
        },
        lookupExistingEntry: function (codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        },
        getNode: function (nodeId) {
            var container = this.get('container');
            return container.one(this.get('formId') + '_' + nodeId);
        },
        setMemberList: function () {
            var otherData = this.get('otherData'),
                modelData = this.get('model').toJSON(),
                activeGroupMembers = otherData.activeGroupMembersList,
                visibleMembers = this.get('model').toJSON().personVisibility,
                displayVisibleMembers = [];

            Y.Array.each(activeGroupMembers, function (activeGroupMember) {
                if (modelData.visibleToAll) {
                    displayVisibleMembers.push({
                        memberName: activeGroupMember['personVO!name'],
                        memberId: activeGroupMember['personVO!id'],
                        memberIdentifier: activeGroupMember['personVO!personIdentifier']
                    });
                } else {
                    Y.Array.each(visibleMembers, function (visibleMember) {
                        if (activeGroupMember['personVO!id'] === visibleMember) {
                            displayVisibleMembers.push({
                                memberName: activeGroupMember['personVO!name'],
                                memberId: activeGroupMember['personVO!id'],
                                memberIdentifier: activeGroupMember['personVO!personIdentifier']
                            });
                            return;
                        }
                    });
                }
            });
            otherData.visibleMembersList = displayVisibleMembers;
            this.set('otherData', otherData);
        },

        render: function () {
            var container = this.get('container'),
                model = this.get('model'),
                otherData = this.get('otherData');

            var html = this.template({
                labels: this.get('labels'),
                modelData: model.toJSON(),
                otherData: otherData,
                codedEntries: this.get('codedEntries'),
                isGroup: this.get('isGroup'),
                notificationEnabled: this.get('notificationEnabled'),
                impactEnabled: this.get('impactEnabled')
            });

            container.setHTML(html);

            this.renderTabs();

            //call superclass
            Y.app.entry.BaseEntryView.superclass.render.call(this);

            if (this.get('isGroup') && model.get('status') === 'DRAFT') {
                this._initVisibilityMembers();
            }

            return this;
        },
        renderTabs: function () {
            var container = this.get('container'),
                otherData = this.get('otherData'),
                defaultTab = otherData.defaultTab;

            this.tabView = new Y.TabView({
                srcNode: container.one('#tabSource')
            }).addTarget(this);

            this.tabView.render(this.get('tabSourceNode'));


            //if default tab selected - go to it
            if (defaultTab) {
                this._selectChildTab(defaultTab);
            }
        },
        _selectChildTab: function (index) {
            if (this.tabView) {
                this.tabView.selectChild(index);
            }
        },
        _getTab: function (index) {
            return this.tabView ? this.tabView._items[index] : undefined;
        },
        _align: function () {
            //just drop out of this thread to fire the align
            Y.later(5, this, function () {
                this.fire('align');
            });
        }
    }, {
        ATTRS: {
            formId: {
                value: ''
            },
            isGroup: {
                value: false
            },
            impactEnabled: {
                value: false
            },
            activeMembersUrl: '',
            /**
             * @attribute activeGroupMembersList
             */
            activeGroupMembersList: {
                value: [],
            },
            /**
             * @attribute tabSourceNode
             * @type string
             * @value '#casenoteTabs'
             * @description Holds DOM element id for rendering tabs
             */
            tabSourceNode: {
                value: '#tabsSource',
                getter: function (v) {
                    return this.get('container').one(v);
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view'
    ]
});
