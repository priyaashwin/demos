YUI.add('entry-dialog', function(Y) {
    var O = Y.Object,
        L = Y.Lang;


    var LOADING_TEMPLATE = '<div class="pure-g-r readonly"><div class="pure-u-1 l-box"><div class="loading-main">Please wait....</div></div></div>';

    var ProcessingView = Y.Base.create('processingView', Y.View, [], {
        render: function() {
            var container = this.get('container');
            container.setHTML(LOADING_TEMPLATE);
            return this;
        }
    });

    var ConfirmView = Y.Base.create('confirmView', Y.View, [], {
        render: function() {
            var labels = this.get("labels");
            this.get("container").setHTML('<div class="pure-g-r"><div class="pure-u-1 l-box"><div class="pure-alert pure-alert-block pure-alert-warning"><h4>' + labels.completeConfirm + '</h4></div></div></div>');
        }
    }, {
        ATTRS: {
            labels: {}
        }
    });

    Y.namespace('app.entry').BaseEntryDialog = Y.Base.create('baseEntryDialog', Y.usp.app.AppDialog, [], {
        /**
         * Complete these properties in the extending class to specify model types
         */
        entryCreateModelClass: undefined,
        entryCreateWithVisbilityModelClass: undefined,
        entryUpdateDraftModelClass: undefined,
        entryUpdateCompleteModelClass: undefined,
        entryUpdateWithVisbilityModelClass: undefined,
        _getConstructor: function(type) {
            return L.isString(type) ? O.getValue(Y, type.split('.')) : type;
        },
        initializer: function(config) {
            var views = {};
            this.plug(Y.usp.Plugin.RichTextFocusManager);
            
            var saveComplete = this.saveComplete.bind(this);

            this.confirmDialog = new Y.usp.app.AppDialog({
                zIndex: 1000,
                buttons: [{
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel',
                }, 'closeButton'],
                views: {
                    confirmComplete: {
                        type: ConfirmView,
                        header:config.confirmComplete.header,
                        buttons: [{
                            name: 'saveButton',
                            labelHTML: '<i class="fa fa-check"></i> Yes',
                            action: function(e) {
                                //this closes the dialog
                                this.handleCancel(e);

                                //this will invoke the complete in the parent
                                saveComplete(e);
                            }
                        }]
                    }
                }
            }).render();

            // Before showing confirm dialog, disable any content editable areas
            // to prevent controls from showing through in IE10.  On hide, 
            // re-enable it.
            this.confirmDialog.on('visibleChange', function(e) {
                var view = this.get('activeView'),
                    container = view.get('container');

                container.all('div[contenteditable]').each(function(node) {
                    node.setAttribute('contenteditable', !e.newVal);
                });
            }, this);
        },
        destructor: function() {
            this.unplug(Y.usp.Plugin.RichTextFocusManager);

            this.confirmDialog.destroy();

            delete this.confirmDialog;
        },
        saveDraft: function(e) {
            var view = this.get('activeView'),
                attributes;
            
            var notificationRecipients=view.get('notificationRecipientsView');
            if(notificationRecipients){
              
              attributes={
                  notificationTargets:notificationRecipients.getNotificationTargetsVO()
              };
            }
            this._saveEntry(e, attributes);
        },
        saveComplete: function(e) {
            var view = this.get('activeView'),
                model = view.get('model');

            if (model.get('id') !== undefined && model.get('id') !== null) {
                model.url = view.get('completeURL');
                this.updateEntry(e);
            } else {
                this._saveEntry(e, {
                    status: 'COMPLETE'
                });
            }
        },
        _saveEntry: function(e, attributes, options) {
            var view = this.get('activeView'),
                model = view.get('model');

            //additional attributes to set into the model before save
            var _attributes = {
                //practitioner is present only for display in the model
                //the actual practitioner is defined by the practitionerId value
                practitioner: undefined,
                eventDate: view.getFuzzyEventDate(),
                temporaryAttachmentIds: view.attachmentPaginatedResults ? view.attachmentPaginatedResults.get('temporaryAttachmentIds') : null
            };

            var _options = {};

            if (model.get('id') === undefined || model.get('id') === null) {
                if (view.get('isGroup')) {
                    //group entry include visibility so use a different model
                    _options.transmogrify = this._getConstructor(this.entryCreateWithVisbilityModelClass);
                } else {
                    //turn it back into a create model without all the additional attributes for visibility
                    _options.transmogrify = this._getConstructor(this.entryCreateModelClass);
                }
            }

            this.handleSave(e, Y.merge(attributes || {}, _attributes), Y.merge(options || {}, _options));
        },
        updateEntry: function(e) {
            var options = {},
                view = this.get('activeView');
            if (view.get('model').get('status') === 'COMPLETE') {
                options.transmogrify = this._getConstructor(this.entryUpdateCompleteModelClass);
            } else {
                if (view.get('isGroup')) {
                    options.transmogrify = this._getConstructor(this.entryUpdateWithVisbilityModelClass);
                } else {
                    options.transmogrify = this._getConstructor(this.entryUpdateDraftModelClass);
                }
            }
            this._saveEntry(e, {}, options);
        },
        completeConfirm: function(e) {
            // Get the view and the model
            var view = this.get('activeView'),
                model = view.get('model');

            //validate - use model form link to set values as we need to not show confirm dialog if errors
            model.validate(Y.merge(model._setModelFromForm(), {
                eventDate: view.getFuzzyEventDate(),
            }), function(err) {
                if (err) {
                    //fire an error
                    view.fire('error', {
                        error: err
                    });
                } else {
                    //show confirm view
                    this.confirmDialog.showView('confirmComplete', {
                        labels: view.get('labels')
                    });
                }
            }.bind(this));
        },
        views: {
            processing: {
                type: ProcessingView,
                buttons:[]
            },
            add: {
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    disabled: true,
                    action: 'saveDraft'
                }, {
                    name: 'completeButton',
                    labelHTML: '<i class="fa fa-check-square"></i> Complete',
                    disabled: true,
                    isActive: true,
                    action: 'completeConfirm'
                }]
            },
            view: {},
            edit: {
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    isPrimary: true,
                    disabled: true,
                    action: 'updateEntry'
                }, {
                    name: 'completeButton',
                    labelHTML: '<i class="fa fa-check-square"></i> Complete',
                    classNames: 'pure-button-active',
                    action: 'completeConfirm'
                }]
            },
            editComplete: {
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    isPrimary: true,
                    disabled: true,
                    action: 'updateEntry'
                }]
            },
            hardDelete: {
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    isPrimary: true,
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            softDelete: {
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    isPrimary: true,
                    action: 'handleSave',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            complete: {
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    isPrimary: true,
                    action: function(e) {
                        this.handleSave(e, {}, {
                            transmogrify: this._getConstructor(this.entryUpdateDraftModelClass)
                        });
                    },
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            uncomplete: {
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    isPrimary: true,
                    action: function(e) {
                        this.handleSave(e, {}, {
                            transmogrify: this._getConstructor(this.entryUpdateDraftModelClass)
                        });
                    },
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            }
        }
    }, {
        ATTRS: {
            width: {
                value: 750
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-dialog',
        'view-rich-text',
        'view'
    ]
});