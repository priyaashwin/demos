YUI.add('login-dialog-views', function (Y) {
    Y.namespace('app.login').LoginModel = new Y.Base.create('loginModel', Y.usp.Model, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#loginForm'
    }, {
        ATTRS: {
            userName: {},
            profileId: {},
            profileName: {},
            credentials: {}
        }
    });

    /**
     * Model used for the actual authentication - LoginModel will be transmogrified into this
     */
    Y.namespace('app.login').SecurityAuthenticationModel = new Y.Base.create('securityAuthenticationModel', Y.usp.Model, [], {}, {
        ATTRS: {
            userName: {},
            profileId: {},
            credentials: {},
            _type: {
                value: 'SecurityAuthentication'
            }
        }
    });

    /**
     * A function that returns a new Secondary Password Model with the appropriate 
     * attributes for the secondary password challenge
     */
    Y.namespace('app.login').getSecondaryPasswordModel = function (requiredIndices) {
        var indices = requiredIndices || [];
        var attrCfg = {
            challengeEntry: {
                readOnly: true,
                getter: function () {
                    var data = {};
                    indices.forEach(function (index) {
                        data[index] = this.get('challengeEntry[' + index + ']');
                    }.bind(this));
                    return data;
                }
            },
            profileId: {}
        };
        indices.forEach(function (index) {
            attrCfg['challengeEntry[' + index + ']'] = { value: '' };
        });

        return new Y.Base.create('secondaryPasswordModel', Y.usp.Model, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#loginForm'
        }, {
            ATTRS: attrCfg
        });
    };

    /**
     * Model used for the security challenge (secondary password)
     * secondaryPasswordModel will be transmogrified into this
     */
    Y.namespace('app.login').SecurityChallengeVerificationModel = Y.Base.create('securityChallengeVerificationModel', Y.usp.Model, [], {}, {
        ATTRS: {
            challengeEntry: {
                value: {}
            },
            profileId: {
                value: {}
            },
            _type: {
                value: 'SecurityChallengeVerification'
            }
        }
    });

    Y.namespace('app.login').LoginView = Y.Base.create('loginView', Y.usp.View, [], {
        template: Y.Handlebars.templates['sessionLogin'],
        events: {
            'input[name="credentials"]': {
                keyup: '_keyup'
            }
        },
        focusPassword: function () {
            this.get('container').one('input[name="credentials"]').focus();
        },
        _keyup: function (e) {
            if (e.charCode === 13) {
                this.fire('submit');
            }
        }
    });

    Y.namespace('app.login').SessionSwitchView = Y.Base.create('sessionSwitchView', Y.usp.View, [], {
        render: function () {
            var template;
            if (this.get('from').username !== this.get('to').username) {
                template = Y.Handlebars.templates['sessionUserSwitch'];
            } else {
                template = Y.Handlebars.templates['sessionProfileSwitch'];
            }

            this.get('container').setHTML(
                template({
                    from: this.get('from'),
                    to: this.get('to')
                })
            );
        },
    }, {
        ATTRS: {
            from: {},
            to: {}
        }
    });

    Y.namespace('app.login').IncompleteAuthView = Y.Base.create('incompleteAuthView', Y.usp.View, [], {
        template: Y.Handlebars.templates['sessionIncompleteAuth'],
        events: {
            'a': {
                'click': '_handleRedirect'
            }
        },
        _handleRedirect: function (e) {
            e.preventDefault();

            window.location = this.get('loginURL');
        }
        
    }, {
        ATTRS: {
            loginURL: {
                value: '/'
            }
        }
    });

    Y.namespace('app.login').SecondaryPasswordView = Y.Base.create('secondaryPasswordView', Y.usp.View, [], {
        events: {
          'input': {
              keyup: '_keyup'
          }
        },
        template: Y.Handlebars.templates['secondaryPassword'],
        _keyup: function (e) {
          if (e.charCode === 13) {
              this.fire('submit');
          }
        },
        focusSecondaryPassword: function () {
          this.get('container').one('input').focus();
        }
    });

    Y.namespace('app.login').AccountExpiredView = Y.Base.create('accountExpiredView', Y.usp.View, [], {
        template: Y.Handlebars.templates['accountExpired']
    });

    // subclass Y.usp.MultiPanelPopUp so we can override _setupEscHandler and prevent escaping of the SessionDialog
    Y.namespace('app.login').SessionDialog = Y.Base.create('sessionDialog', Y.usp.app.AppDialog, [], {
        initializer: function () {
            this._sessionDialogEvents = [
                this.on('secondaryPasswordView:submit', this.submitSecondardPassword, this),
                this.on('loginView:submit', this.submitAuthentication, this),
                this.on(['loginModel:error', 'secondaryPasswordModel:error'], this._handleModelError, this),
                this.on('visibleChange', this._setBackgroundOpaque, this)
            ];
        },
        destructor: function () {
            this._sessionDialogEvents.forEach(function (handler) {
                handler.detach();
            });
            delete this._evt_sessionDialogEventss;
        },
        _handleModelError: function (e) {
            var target = e.target,
                labels = this.get('labels'),
                error, suppressError, detail;
            var response = e.response;

            try {
                detail = Y.JSON.parse(response.response || response.responseText);
            } catch (ignore) {
                detail = {};
            }

            var view = this.get('activeView');
            var viewInfo = view.get('container').one('.pure-alert-info');
            if (viewInfo) {
                //remove info panel which clutters display if error shown
                viewInfo.remove(true);
            }

            //specific error handling for LoginFormModel
            if (target.name === 'loginModel') {
                switch (detail.type) {
                case 'BadCredentialsException':
                    // falls through
                case 'LockedException':
                    {
                        suppressError = true;
                        error = {
                            code: 400,
                            msg: {
                                validationErrors: {
                                    credentials: labels.lockedException
                                }
                            }
                        };
                        break;
                    }
                case 'SecondaryPasswordRequiredException':
                    {
                        suppressError = true;
                        this.fire('secondaryPasswordRequired', {
                            requiredIndices: detail.requiredIndices,
                            profileId: detail.profileId
                        });
                        break;
                    }
                case 'IncompleteAuthenticationException':
                    {
                        suppressError = true;
                        this.fire('incompleteAuthentication');
                        break;
                    }

                case 'AccountExpiredException':
                    {
                        suppressError = true;
                        this.fire('accountExpired');
                        break;
                    }
                }

            } else if (target.name === 'secondaryPasswordModel') {
                //errors from the secondary password model       
                switch (detail.type) {
                case 'IncompleteAuthenticationException':
                    {
                        suppressError = true;
                        this.fire('incompleteAuthentication');
                        break;
                    }
                }
            }

            if (suppressError) {
                e.halt(true);
                if (error) {
                    //fire a new error with the updated details
                    this.fire('error', { error: error });
                }
            }
        },
        _setBackgroundOpaque: function(e) {
            if (e.newVal) {
                Y.one('body').addClass('opaqueBackground');
            } else {
                Y.one('body').removeClass('opaqueBackground');
            }
        },
        views: {
            loginForm: {
                type: Y.app.login.LoginView,
                buttons: [{
                    name: 'loginButton',
                    labelHTML: '<i class="fa fa-check"></i> Login',
                    disabled: true,
                    action: 'submitAuthentication'
                }]
            },
            sessionSwitchMessage: {
                type: Y.app.login.SessionSwitchView
            },
            incompleteAuthMessage: {
                type: Y.app.login.IncompleteAuthView,
            },
            secondaryPassword: {
                type: Y.app.login.SecondaryPasswordView,
                buttons: [{
                    name: 'submitButton',
                    labelHTML: '<i class="fa fa-check"></i> Submit',
                    disabled: true,
                    action: 'submitSecondardPassword'
                }]
            },
            accountExpired: {
                type: Y.app.login.AccountExpiredView,
            }
        },
        _setupEscHandler: function () {
            return;
        },
        submitAuthentication: function (e) {
            //call save on the model to use the ModelFormLink to populate the user inputs
            this.handleSave(e, {}, { transmogrify: Y.app.login.SecurityAuthenticationModel });
        },
        submitSecondardPassword: function (e) {
            this.handleSave(e, {}, { transmogrify: Y.app.login.SecurityChallengeVerificationModel });
        }
    }, {
        ATTRS: {
            buttons: {
                value: [] //no additional buttons required
            },
            width: {
                value: 450
            },
            zIndex: {
                //The Session dialog needs to show above everything else. 
                //Current value promotes it above the Bootstrap navbar in Forms/Checklists, which uses 1000
                value: 1050
            },
            labels: {

            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'usp-model',
        'usp-view',
        'event-custom',
        'model-form-link',
        'model-transmogrify',
        'webresources-handlebars-session-templates',
        'app-dialog',
        'json-parse'
    ]
});