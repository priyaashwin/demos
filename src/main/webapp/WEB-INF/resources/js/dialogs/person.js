YUI.add('person-dialog-views', function (Y) {

    var E = Y.Escape,
        L = Y.Lang,
        A = Y.Array,
        O = Y.Object,
        UP = Y.app.utils.person,
        ADDRESS_TYPE_HOME = "HOME",
        ADDRESS_USAGE_TEMPORARY = "TEMPORARY",
        LOCATION_TYPE_HOMELESS = "HOMELESS",
        USPFormatters = Y.usp.ColumnFormatters,
        addressFormatters = Y.app.FormatAddressLocation,
        // this patterns are taken from PostcodeNormalizer.java (core facet)
        postCodePattern = new RegExp("(ZZ99 3VZ|GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})"),
        outerPostCodePattern = new RegExp("(ZZ99|GIR|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW]))"),

        checkboxFormatter = function (e) {
            var html = '';
            if (e.record.get('relationshipName') === "No relation") {
                html = '<div><input type="checkbox" name="residentCheckbox"/></div>';
            } else {
                html = '<div><input type="checkbox" name="residentCheckbox" checked/></div>';
            }
            return html;
        };

    function AutoCompleteAddress() {
        AutoCompleteAddress.superclass.constructor.apply(this, arguments);
    }
    AutoCompleteAddress.NAME = 'aclist';
    AutoCompleteAddress.NS = 'ac';
    AutoCompleteAddress.ATTRS = {};
    Y.extend(AutoCompleteAddress, Y.Plugin.usp.PopupAutoCompletePlugin, {

        sendRequest: function (query, requestTemplate) {
            var houseNameNumber = Y.one('.AddPersonAddressView #houseSearch-input').get('value');
            var postcode = Y.one('.AddPersonAddressView #postcodeSearch-input').get('value');
            if (postcode != null) {
                var normalisedPostcode = this.normalisePostCode(postcode);
                var formattedPostCode = postcode.toUpperCase().trim();
                if (this.isValidPostCode(normalisedPostcode) ||
                    (this.isValidOuterPostCode(formattedPostCode) && houseNameNumber != null && houseNameNumber.length > 0)) {
                    AutoCompleteAddress.superclass.sendRequest.call(this, query, requestTemplate);
                } else {
                    this._defClearFn();
                    Y.one('.AddPersonAddressView #address_search_note').show();
                    Y.one('.AddPersonAddressView #noSearchResults-wrapper').hide();
                }
            }
        },
        // this code taken from PostcodeNormalizer.java (core facet)
        normalisePostCode: function (postcode) {
            var retString = null;
            if (postcode != null) {
                retString = postcode.toUpperCase().replace(/\s/g, '');
                if (retString.length > 3) {
                    retString = retString.substring(0, retString.length - 3) + ' ' + retString.substring(retString.length - 3);
                }
            }
            return retString;
        },

        isValidPostCode: function (postcode) {
            return postCodePattern.test(postcode);
        },

        isValidOuterPostCode: function (postcode) {
            return outerPostCodePattern.test(postcode);
        }
    });

    var NewLocationForm = Y.Base.create("NewLocationForm", Y.usp.address.NewLocation, [Y.usp.ModelFormLink], {
        form: '#addPersonAddressForm'
    });

    var newLocationModel = new NewLocationForm();

    var citizenshipResultsTable = Y.Base.create('citizenshipResultsTable', Y.usp.app.Results, [], {
        getResultsListModel: function (config) {
            return new Y.ModelList({
                model: this.get('defaultModel'),
                items: this.get('citizenships')
            });
        },
        resultsType: Y.usp.ResultsTable,
        getColumnConfiguration: function (config) {
            var labels = config.labels || {};
            return ([{
                    key: 'citizenship',
                    label: labels.citizenship,
                    width: '48%',
                    formatter: '<select type="text" class="pure-input-1" name="citizenship" value="" disabled="disabled"><select/>'
                },
                {
                    key: 'startDate',
                    label: labels.startDate,
                    width: '25%',
                    formatter: '<input type="text" class="pure-input-1" name="startDate" disabled="disabled"/>'
                },
                {
                    key: 'endDate',
                    label: labels.endDate,
                    width: '25%',
                    formatter: '<input type="text" class="pure-input-1" name="endDate" disabled="disabled"/>'
                },
                {
                    key: 'addDelete',
                    label: '<button class="yui3-button pure-button-active add-button" title="Add citizenship" disabled="disabled"><i class="fa fa-plus" aria-hidden="true"></i></button>',
                    width: '2%',
                    formatter: '<button class="yui3-button pure-button-error delete-button" title="Remove citizenship" disabled="disabled"><i class="fa fa-times" aria-hidden="true"></i></button>'
                }
            ]);
        }
    });

    //Base functionality common to add/edit person
    function BaseAddUpdatePerson() {}
    BaseAddUpdatePerson.prototype = {
        events: {
            '#nhsFirstThreeDigits-input': {
                keyup: 'tabSecond'
            },
            '#nhsSecondThreeDigits-input': {
                keyup: 'tabThird'
            },
            '#fuzzyDOBDAY': {
                change: 'onFuzzyDayChange'
            },
            '#fuzzyDOBMONTH': {
                change: 'onFuzzyMonthChange'
            },
            '#fuzzyDOBYEAR': {
                valuechange: 'onFuzzyYearChange'
            },
            '#dateOfBirthEstimated-input': {
                change: 'onDateOfBirthEstimatedChange'
            },
            '#chiNumber-input': {
                change: 'onChiNumberChange'
            },
            '#age-input': {
                keyup: 'onAgeChange'
            },
            '#fuzzyDeceasedDOBYEAR, #fuzzyDiedDateYEAR': {
                valuechange: 'onFuzzyDecasedYearChange'
            },
            '#fuzzyDeceasedDOBMONTH, #fuzzyDiedDateMONTH': {
                change: 'onFuzzyDecasedMonthChange'
            },
            '#fuzzyDeceasedDOBDAY, #fuzzyDiedDateDAY': {
                change: 'onFuzzyDecasedDayChange'
            },
            '.error #fuzzyDOBYEAR,.error #fuzzyDOBMONTH,.error #fuzzyDOBDAY,.error #age-input': {
                blur: 'changeErrorHighlightToRed'
            },
            '.error #fuzzyDeceasedDOBYEAR,.error #fuzzyDiedDateYEAR,.error #fuzzyDeceasedDOBMONTH,.error #fuzzyDiedDateMONTH': {
                blur: 'changeErrorHighlightToRed'
            },
            '.error #fuzzyDeceasedDOBDAY,.error #fuzzyDiedDateDAY,.error #age-at-death': {
                blur: 'changeErrorHighlightToRed'
            },
            '.input-group.input-prepend #fuzzyDOBYEAR,#fuzzyDOBMONTH,#fuzzyDOBDAY,#age-input': {
                focus: 'changeErrorHighlightToBlue'
            },
            '.input-group.input-prepend #fuzzyDeceasedDOBYEAR,#fuzzyDiedDateYEAR,#fuzzyDeceasedDOBMONTH,,#fuzzyDiedDateDAY,#fuzzyDeceasedDOBDAY,#age-input,': {
                focus: 'changeErrorHighlightToBlue'
            },
            '#title-input': {
                change: 'onTitleChanged'
            },
            '#gender input': {
                change: 'onGenderChanged'
            },
            '#genderIdentity-input': {
                change: 'onGenderIdentityChanged'
            },
            '#filename-input': {
                change: 'onFilenameChange'
            },
            '#citizenshipResultsTableResults button.add-button': {
                click: 'handleNewCitizenship'
            },
            '#citizenshipResultsTableResults button.delete-button': {
                click: 'handleDeleteCitizenship'
            },
            '#citizenshipResultsTableResults select': {
                change: 'onCitizenshipChange'
            },
            '#citizenshipResultsTableResults input[name=startDate], #citizenshipResultsTableResults input[name=endDate]': {
                change: 'onCitizenshipDateChange'
            }
        },
        getRow: function (node) {
            while (node.get('tagName') !== 'TR') {
                node = node.ancestor();
            }
            return node;
        },
        getCitizenshipsList: function () {
            return this.citizenshipResults.get('results').data;
        },
        getModelForNode: function (node) {
            return this.getCitizenshipsList().getByClientId(this.getRecordId(node));
        },
        getRecordId: function (node) {
            return this.getRow(node).getData('yui3-record');
        },
        updateCitizenshipModelType: function (model) {
            if (model.get('_type') === 'Citizenship') {
                model.set('_type', 'UpdateCitizenship', {
                    silent: true
                });
                model.removeAttr('_secured');
            }
        },
        onCitizenshipChange: function (e) {
            var model = this.getModelForNode(e.currentTarget),
                node = e.currentTarget;

            model.set(node.get('name'), node.get('value'), {
                silent: true
            });
            this.updateCitizenshipModelType(model);
        },
        onCitizenshipDateChange: function (e) {
            var node = e.node ? e.node : e.currentTarget,
                model = this.getModelForNode(node),
                date = e.date ? e.date : Y.USPDate.parse(node.get('value')),
                timeSinceEpoch = !!date ? date.getTime() : null;

            model.set(node.get('name'), date, {
                silent: true
            });
            this.updateCitizenshipModelType(model);
        },
        initCitizenships: function () {
            var citizenshipsTable = this.get('container').one('#citizenships-table'),
                citizenships = this.get('model').get('citizenships') || [];
            citizenships.forEach(Y.bind(this._setupCitizenships, this));
            if (this.permissions.canAddCitizenship) {
                citizenshipsTable.one('button.add-button').removeAttribute('disabled');
            }
        },
        _addCalendarToList: function (dateSelector, node) {
            var recordId = this.getRecordId(node);
            var nodeCalendarList = this.citizenshipCalendarsList[recordId];
            if (!!nodeCalendarList && nodeCalendarList.length >= 0) {
                this.citizenshipCalendarsList[recordId].push(dateSelector);
            } else {
                this.citizenshipCalendarsList[recordId] = [dateSelector];
            }
        },
        _initCitizenshipRow: function (node, citizenship) {
            var permissions = this.permissions;
            if (node.get('tagName') === 'SELECT') {
                Y.FUtil.setSelectOptions(node, O.values(Y.uspCategory.address.countryCode.category.getActiveCodedEntries()), null, null, true, true, 'code');
                node.set('value', citizenship ? citizenship.citizenship : '');
            } else if (node.get('tagName') === 'INPUT') {
                var name = node.get('name');
                var dateSelector = new Y.usp.CalendarPopup({
                    inputNode: node,
                    maximumDate: new Date()
                }).render();
                this._addCalendarToList(dateSelector, node);
                dateSelector.after('dateSelection', this.onCitizenshipDateChange, this);
                node.set('value', citizenship ? Y.USPDate.formatDateValue(citizenship[name]) : '');
            }

            if (permissions.canUpdateCitizenship) {
                node.removeAttribute('disabled');
            }

            if ((node.get('tagName') === 'BUTTON' && permissions.canRemoveCitizenship) || !citizenship) {
                node.removeAttribute('disabled');
            }
        },
        handleNewCitizenship: function () {
            this.getCitizenshipsList().add(new Y.usp.person.NewCitizenship());
            var citizenshipRows = this.get('container').all('#citizenshipResultsTableResults tbody.pure-table-data tr');
            citizenshipRows.item(citizenshipRows.size() - 1).all('select, input, button').each(Y.bind(this._initCitizenshipRow, this));
        },
        handleDeleteCitizenship: function (e) {
            var recordId = this.getRecordId(e.currentTarget);
            this.getRow(e.currentTarget).all('select, input, button').each(function (node) {
                node.setAttribute('disabled', 'disabled');
            });
            this.getModelForNode(e.currentTarget).set('_type', 'DeleteCitizenship', {
                silent: true
            });
            this.citizenshipCalendarsList[recordId].forEach(function (dateSelector) {
                dateSelector.destroy();
            });
            delete this.citizenshipCalendarsList[recordId];
        },
        onFilenameChange: function (e) {
            this.fire('clearErrors');
            var container = this.get('container'),
                file = container.one('#filename-input')._node.files[0],
                fReader = new FileReader(),
                imageType = /^image\//,
                displayImage = function () {
                    Y.one('.pure-button-primary').set('disabled', false);
                    container.one('.person-image-container-no-image').setStyle('display', 'none');
                    container.one('.person-image-container').setStyle('display', 'block')

                    var uploader = new Y.app.utils.cropper.Uploader({
                        input: container.one('#filename-input'),
                        types: ['gif', 'jpg', 'jpeg', 'png']
                    });

                    var editor = new Y.app.utils.cropper.Cropper({
                        size: {
                            width: 225,
                            height: 225
                        },
                        canvas: container.one('#personImageCanvas'),
                        preview: container.one('#croppingResultCanvas'),
                        limit: 500
                    });

                    // Make sure both were initialised correctly
                    if (uploader && editor) {
                        // Start the uploader, which will launch the editor
                        uploader.listen(editor.setImageSource.bind(editor), function (error) {
                            Y.log(error, 'debug');
                        });
                    }
                },
                displayDefaultImage = function () {
                    container.one('.person-image-container').setStyle('display', 'none');
                    container.one('.person-image-container-no-image').setStyle('display', 'block');
                    Y.one('.pure-button-primary').set('disabled', 'disabled');
                }

            if (file) {
                if (!imageType.test(file.type)) {
                    Y.log('this is not an image', 'debug');
                    file = null;

                    this.fire('error', {
                        error: {
                            code: 400,
                            msg: {
                                validationErrors: {
                                    'picture': 'Picture must be a type of image.'
                                }
                            }
                        }
                    });

                    displayDefaultImage();
                } else {
                    fReader.readAsDataURL(file);
                }
            } else {
                displayDefaultImage();
            }

            fReader.onloadend = function (event) {
                displayImage();
                return;
            }
        },
        validateChiNumberAndGender: function () {
            var chiNumberValue = this.chiNumberInput.get('value');

            // get the 9th digit and validate gender
            if (chiNumberValue.length === 10) {
                var genderDigit = chiNumberValue.substring(8, 9),
                    message = this.get('labels').genderDigitMismatchWithChiNumber;
                if (this.maleInput.get('checked')) {
                    // 9th digit should be an odd number for male
                    if (genderDigit % 2 === 0) {
                        this.notifyUserOfEffectOnChiNumber(message);
                    }
                } else if (this.femaleInput.get('checked')) {
                    //9th digit should be an even number for female
                    if (genderDigit % 2 !== 0) {
                        this.notifyUserOfEffectOnChiNumber(message);
                    }
                } else if (this.indeterminateInput.get('checked') || this.unknownInput.get('checked')) {
                    this.notifyUserOfEffectOnChiNumber(message);
                }
            }
        },
        onGenderChanged: function (e) {
            // User has manually provided a gender value
            this.set('manualGenderSelection', true);

            if (e.target.get('checked')) {
                //validate against chi number
                var chiNumberValue = this.chiNumberInput.get('value');
                if (L.isNull(chiNumberValue) || L.isUndefined(chiNumberValue) || chiNumberValue === "") {
                    return;
                }
                this.validateChiNumberAndGender();
            }
        },
        onGenderIdentityChanged: function (e) {
            var container = this.get('container');
            var genderIdentity = container.one('#genderIdentity-input').get('value');
            var genderIdentitySelfDefinedContainer = container.one('#genderIdentitySelfDefined-container');
            var genderIdentitySelfDefined = container.one('#genderIdentitySelfDefined-input');
            if (genderIdentity === 'SELF_DEFINED') {
                genderIdentitySelfDefinedContainer.show();
            } else {
                genderIdentitySelfDefined.set('value', '');
                genderIdentitySelfDefinedContainer.hide();
            }
        },
        onTitleChanged: function (e) {
            var container = this.get('container');
            var genderFieldset = container.one('#gender');
            var currentTitle = container.one('#title-input').get('value');
            var manualGenderSelection = this.get('manualGenderSelection');

            // Early return if gender fieldset doesn't exist on this dialog, or the user has manually set a gender, or currentTitle cannot be parsed
            if (null === genderFieldset || manualGenderSelection || 'string' !== typeof currentTitle) {
                return;
            }

            var titleGenderMap = this.get('titleGenderMap');
            var currentTitleUpper = currentTitle.toUpperCase();

            // Test whether the current title has a gender mapping
            if (O.hasKey(titleGenderMap, currentTitleUpper)) {
                var targetGenderLower = O.getValue(titleGenderMap, [currentTitleUpper]).toLowerCase();

                // Set the mapped radio input to checked=true
                container.one('#' + targetGenderLower + '-input').set('checked', true);
            }
        },
        tabSecond: function (e) {

            var inputValueLength = this.nhsFirstThreeDigitsInput.get('value').length;

            // allow left/right arrow keys
            if (inputValueLength === 3 && e.keyCode !== 37 && e.keyCode !== 39) {
                this.nhsSecondThreeDigitsInput.focus().select();
            }
        },
        tabThird: function (e) {

            var inputValueLength = this.nhsSecondThreeDigitsInput.get('value').length;

            // allow left/right arrow keys
            if (inputValueLength === 3 && e.keyCode !== 37 && e.keyCode !== 39) {
                this.nhsLastFourDigitsInput.focus().select();
            }
        },
        changeErrorHighlightToBlue: function (e) {
            if (this.dateOfBirthWrapper) {
                this.dateOfBirthWrapper.addClass('focus');
            }
            if (this.deceasedDateOfBirthWrapper) {
                this.deceasedDateOfBirthWrapper.addClass('focus');
            }
            if (this.diedDateWrapper) {
                this.diedDateWrapper.addClass('focus');
            }

        },
        changeErrorHighlightToRed: function (e) {
            if (this.dateOfBirthWrapper) {
                this.dateOfBirthWrapper.removeClass('focus');
            }
            if (this.deceasedDateOfBirthWrapper) {
                this.deceasedDateOfBirthWrapper.removeClass('focus');
            }
            if (this.diedDateWrapper) {
                this.diedDateWrapper.removeClass('focus');
            }
        },
        initPersonPictureCanvas: function () {
            var container = this.get('container');

            var uploader = new Y.app.utils.cropper.Uploader({
                input: container.one('#filename-input'),
                types: ['gif', 'jpg', 'jpeg', 'png']
            });

            var editor = new Y.app.utils.cropper.Cropper({
                size: {
                    width: 225,
                    height: 225
                },
                canvas: container.one('#personImageCanvas'),
                preview: container.one('#croppingResultCanvas'),
                limit: 500
            });

            // Make sure both were initialised correctly
            if (uploader && editor) {
                // Start the uploader, which will launch the editor
                uploader.listen(editor.setImageSource.bind(editor), function (error) {
                    Y.log(error, 'debug');
                });
            }
        },
        initializer: function (config) {
            this.permissions = this.get('permissions') || {
                canViewCitizenship: false,
                canAddCitizenship: false,
                canUpdateCitizenship: false,
                canRemoveCitizenship: false
            };
            this.citizenshipCalendarsList = {};
            // setup widgets
            this.fuzzyDOBWidget = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyDOB', // I don't usually do this, rather let the widget generate a GUID, but there is CSS attached to this id.
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: new Date().getFullYear(),
                labels: config.fuzzyDateLabels
            });

            this.fuzzyDeceasedDOBWidget = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyDeceasedDOB', // This id will be used for fields in it for their ids
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: new Date().getFullYear(),
                labels: config.fuzzyDateLabels
            });

            this.fuzzydiedDateWidget = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyDiedDate', // //This id will be used for fields in
                // it for their ids
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: new Date().getFullYear(),
                labels: config.fuzzyDateLabels
            });

            // Event handlers for common events
            this._evtCommon = [
                // setup event handlers
                this.fuzzyDOBWidget.on('calendarPopup:dateSelection', function (e) {
                    this.toggleEstimatedCheckbox(false);
                    this.checkDate(e.date);
                }, this),
                this.fuzzyDeceasedDOBWidget.on('calendarPopup:dateSelection', function (e) {
                    this.toggleDeceasedEstimatedCheckbox(false, 'BORN');
                    this.bornDateValue = e.date;
                    this.calculateAgeOnDateSelection(e);
                }, this),
                this.fuzzydiedDateWidget.on('calendarPopup:dateSelection', function (e) {
                    this.toggleDeceasedEstimatedCheckbox(false, 'DIED');
                    this.diedDateValue = e.date;
                    this.calculateAgeOnDateSelection(e);
                }, this)
            ];
        },
        _destroyCommon: function () {
            // unbind event handles
            if (this._evtCommon) {
                A.each(this._evtCommon, function (item) {
                    item.detach();
                });
                // null out
                this._evtCommon = null;
            }

            // common clean up to be called from destructor
            if (this.nhsFirstThreeDigitsInput) {
                this.nhsFirstThreeDigitsInput.destroy();
                delete this.nhsFirstThreeDigitsInput;
            }

            if (this.nhsSecondThreeDigitsInput) {
                this.nhsSecondThreeDigitsInput.destroy();
                delete this.nhsSecondThreeDigitsInput;
            }

            if (this.nhsLastFourDigitsInput) {
                this.nhsLastFourDigitsInput.destroy();
                delete this.nhsLastFourDigitsInput;
            }
            if (this.chiNumberInput) {
                this.chiNumberInput.destroy();
                delete this.chiNumberInput;
            }
            if (this.dateOfBirthWrapper) {
                this.dateOfBirthWrapper.destroy();
                delete this.dateOfBirthWrapper;
            }
            if (this.deceasedDateOfBirthWrapper) {
                this.deceasedDateOfBirthWrapper.destroy();
                delete this.deceasedDateOfBirthWrapper;
            }
            if (this.diedDateWrapper) {
                this.diedDateWrapper.destroy();
                delete this.diedDateWrapper;
            }
            if (this.dateOfBirthInput) {
                this.dateOfBirthInput.destroy();
                delete this.dateOfBirthInput;
            }
            if (this.deceasedDateOfBirthInput) {
                this.deceasedDateOfBirthInput.destroy();
                delete this.deceasedDateOfBirthInput;
            }

            if (this.ageInput) {
                this.ageInput.destroy();
                delete this.ageInput;
            }

            if (this.fuzzyDOBYEARTextBox) {
                this.fuzzyDOBYEARTextBox.destroy();
                delete this.fuzzyDOBYEARTextBox;
            }
            if (this.fuzzyDOBMONTHSelect) {
                this.fuzzyDOBMONTHSelect.destroy();
                delete this.fuzzyDOBMONTHSelect;
            }
            if (this.fuzzyDOBDAYSelect) {
                this.fuzzyDOBDAYSelect.destroy();
                delete this.fuzzyDOBDAYSelect;
            }
            if (this.fuzzyDeceasedDOBYEARTextBox) {
                this.fuzzyDeceasedDOBYEARTextBox.destroy();
                delete this.fuzzyDeceasedDOBYEARTextBox;
            }
            if (this.fuzzyDeceasedDOBMONTHSelect) {
                this.fuzzyDeceasedDOBMONTHSelect.destroy();
                delete this.fuzzyDeceasedDOBMONTHSelect;
            }
            if (this.fuzzyDeceasedDOBDAYSelect) {
                this.fuzzyDeceasedDOBDAYSelect.destroy();
                delete this.fuzzyDeceasedDOBDAYSelect;
            }
            if (this.fuzzyDiedDateYEARTextBox) {
                this.fuzzyDiedDateYEARTextBox.destroy();
                delete this.fuzzyDiedDateYEARTextBox;
            }
            if (this.fuzzyDiedDateMONTHSelect) {
                this.fuzzyDiedDateMONTHSelect.destroy();
                delete this.fuzzyDiedDateMONTHSelect;
            }
            if (this.fuzzyDiedDateDAYSelect) {
                this.fuzzyDiedDateDAYSelect.destroy();
                delete this.fuzzyDiedDateDAYSelect;
            }
            if (this.estimatedInfoArea) {
                this.estimatedInfoArea.destroy();
                delete this.estimatedInfoArea;
            }
            if (this.estimatedCheckbox) {
                this.estimatedCheckbox.destroy();
                delete this.estimatedCheckbox;
            }
            if (this.deceasedDobEstimatedCheckbox) {
                this.deceasedDobEstimatedCheckbox.destroy();
                delete this.deceasedDobEstimatedCheckbox;
            }
            if (this.deceasedDiedEstimatedCheckbox) {
                this.deceasedDiedEstimatedCheckbox.destroy();
                delete this.deceasedDiedEstimatedCheckbox;
            }
            if (this.estimatedCheckboxArea) {
                this.estimatedCheckboxArea.destroy();
                delete this.estimatedCheckboxArea;
            }
            if (this.deceasedDobEstimatedCheckboxArea) {
                this.deceasedDobEstimatedCheckboxArea.destroy();
                delete this.deceasedDobEstimatedCheckboxArea;
            }
            if (this.deceasedDiedEstimatedCheckboxArea) {
                this.deceasedDiedEstimatedCheckboxArea.destroy();
                delete this.deceasedDiedEstimatedCheckboxArea;
            }
            if (this.titleInput) {
                this.titleInput.destroy();
                delete this.titleInput;
            }
            if (this.professionalTitleInput) {
                this.professionalTitleInput.destroy();
                delete this.professionalTitleInput;
            }
            if (this.genderIdentityInput) {
                this.genderIdentityInput.destroy();
                delete this.genderIdentityInput;
            }
            if (this.sexualOrientationInput) {
                this.sexualOrientationInput.destroy();
                delete this.sexualOrientationInput;
            }
            if (this.genderIdentitySelfDefinedInput) {
                this.genderIdentitySelfDefinedInput.destroy();
                delete this.genderIdentitySelfDefinedInput;
            }
            if (this.ethnicityInput) {
                this.ethnicityInput.destroy();
                delete this.ethnicityInput;
            }
            if (this.religionInput) {
                this.religionInput.destroy();
                delete this.religionInput;
            }
            if (this.countryOfBirthInput) {
                this.countryOfBirthInput.destroy();
                delete this.countryOfBirthInput;
            }
            if (this.forenameInput) {
                this.forenameInput.destroy();
                delete this.forenameInput;
            }
            if (this.surnameInput) {
                this.surnameInput.destroy();
                delete this.surnameInput;
            }
            if (this.dueDateInput) {
                this.dueDateInput.destroy();
                delete this.dueDateInput;
            }
            if (this.diedDateInput) {
                this.diedDateInput.destroy();
                delete this.diedDateInput;
            }
            if (this.ageAtDeath) {
                this.ageAtDeath.destroy();
                delete this.ageAtDeath;
            }
            if (this.citizenshipResults) {
                this.citizenshipResults.destroy();
                delete this.citizenshipResults;
            }
            if (this.citizenshipCalendarsList) {
                for (recordId in this.citizenshipCalendarsList) {
                    this.citizenshipCalendarsList[recordId].forEach(function (dateSelector) {
                        dateSelector.destroy();
                    });
                }
                delete this.citizenshipCalendarsList;
            }
            if (this.maleInput) {
                this.maleInput.destroy();
                delete this.maleInput;
            }
            if (this.femaleInput) {
                this.femaleInput.destroy();
                delete this.femaleInput;
            }
            if (this.indeterminateInput) {
                this.indeterminateInput.destroy();
                delete this.indeterminateInput;
            }
            if (this.unknownInput) {
                this.unknownInput.destroy();
                delete this.unknownInput;
            }

            if (this.notCarriedToTermDateInput) {
                this.notCarriedToTermDateInput.destroy();
                delete this.notCarriedToTermDateInput;
                this.exactNotCarriedToTermDateWidget.destroy();
                delete this.exactNotCarriedToTermDateWidget;
            }
            if (this.terminationInput) {
                this.terminationInput.destroy();
                delete this.terminationInput;
            }
            if (this.miscarriageInput) {
                this.miscarriageInput.destroy();
                delete this.miscarriageInput;
            }
            if (this.stillbirthInput) {
                this.stillbirthInput.destroy();
                delete this.stillbirthInput;
            }
        },
        _renderCommon: function () {
            var container = this.get('container');

            // Will have been rendered at this point.

            // cache common used inputs to avoid repeated lookups. This also allows us to destroy them in our destructor code
            this.dateOfBirthInput = container.one('#dateOfBirth-input');
            this.deceasedDateOfBirthInput = container.one('#deceased-dateOfBirth-input');
            this.dueDateInput = container.one('#dueDate-input');
            this.notCarriedToTermDateInput = container.one('#notCarriedToTermDate-input');
            this.diedDateInput = container.one('#diedDate-input');
            this.ageAtDeath = container.one('#age-at-death');
            this.ageInput = container.one('#age-input');
            this.titleInput = container.one('#title-input');
            this.professionalTitleInput = container.one('#professionalTitle-input');
            this.organisationInput = container.one('#organisationName-input');
            this.sexualOrientationInput = container.one('#sexualOrientation-input');
            this.genderIdentityInput = container.one('#genderIdentity-input');
            this.genderIdentitySelfDefinedInput = container.one('#genderIdentitySelfDefined-input');
            this.ethnicityInput = container.one('#ethnicity-input');
            this.religionInput = container.one('#religion-input');
            this.countryOfBirthInput = container.one('#countryOfBirth-input');
            this.nhsFirstThreeDigitsInput = container.one('#nhsFirstThreeDigits-input');
            this.nhsSecondThreeDigitsInput = container.one('#nhsSecondThreeDigits-input');
            this.nhsLastFourDigitsInput = container.one('#nhsLastFourDigits-input');
            this.chiNumberInput = container.one('#chiNumber-input');
            this.pureInfoWrapper = container.one('#pureInfoWrapper');
            this.dateOfBirthWrapper = container.one('#dateOfBirth-wrapper');
            this.deceasedDateOfBirthWrapper = container.one('#deceased-dateOfBirth-wrapper');
            this.dueDateWrapper = container.one('#dueDate-wrapper');
            this.diedDateWrapper = container.one('#diedDate-wrapper');
            this.estimatedInfoArea = container.one('p.estimatedInfo');
            this.estimatedDiedInfoArea = container.one('p.deceasedEstimatedInfo');
            this.estimatedCheckbox = container.one('#dateOfBirthEstimated-input');
            this.estimatedCheckboxArea = container.one('#dateOfBirthEstimated');
            this.deceasedDobEstimatedCheckbox = container.one('#deceasedDobEstimated-input');
            this.deceasedDiedEstimatedCheckbox = container.one('#deceasedDiedEstimated-input');
            this.deceasedDobEstimatedCheckboxArea = container.one('#deceasedDobEstimated');
            this.deceasedDiedEstimatedCheckboxArea = container.one('#deceasedDiedEstimated');
            this.forenameInput = container.one('#forename-input');
            this.surnameInput = container.one('#surname-input');

            // setup gender
            this.maleInput = container.one('#male-input');
            this.femaleInput = container.one('#female-input');
            this.indeterminateInput = container.one('#indeterminate-input');
            this.unknownInput = container.one('#unknown-input');

            // setup notCarriedToTermType
            this.terminationInput = container.one('#termination-input');
            this.miscarriageInput = container.one('#miscarriage-input');
            this.stillbirthInput = container.one('#stillbirth-input');

            // setup the fuzzy date input node and render

            this.fuzzyDOBWidget.set('contentNode', this.dateOfBirthInput);
            if (this.dateOfBirthInput) {
                this.fuzzyDOBWidget.render();
            }
            // for date of birth for deceased person
            this.fuzzyDeceasedDOBWidget.set('contentNode', this.deceasedDateOfBirthInput);
            if (this.deceasedDateOfBirthInput) {
                this.fuzzyDeceasedDOBWidget.render();
            }

            // for died date calendar widget
            this.fuzzydiedDateWidget.set('contentNode', this.diedDateInput);
            if (this.diedDateInput) {
                this.fuzzydiedDateWidget.render();
            }

            if (this.permissions.canViewCitizenship) {
                this.citizenshipResults = new citizenshipResultsTable({
                    searchConfig: {
                        initialLoad: false,
                        labels: this.get('labels')
                    },
                    defaultModel: Y.usp.person.Citizenship,
                    citizenships: this.get('model').toJSON().citizenships
                });

                var citizenshipsTable = this.get('container').one('#citizenships-table');
                if (citizenshipsTable != null) {
                    citizenshipsTable.setHTML(this.citizenshipResults.render().get('container'));
                }
            }
            // some more inputs which are cached, but only available after the fuzzyDate is rendered
            this.fuzzyDOBYEARTextBox = container.one('#fuzzyDOBYEAR');
            this.fuzzyDOBMONTHSelect = container.one('#fuzzyDOBMONTH');
            this.fuzzyDOBDAYSelect = container.one('#fuzzyDOBDAY');
            this.fuzzyDeceasedDOBYEARTextBox = container.one('#fuzzyDeceasedDOBYEAR');
            this.fuzzyDeceasedDOBMONTHSelect = container.one('#fuzzyDeceasedDOBMONTH');
            this.fuzzyDeceasedDOBDAYSelect = container.one('#fuzzyDeceasedDOBDAY');
            this.fuzzyDiedDateYEARTextBox = container.one('#fuzzyDiedDateYEAR');
            this.fuzzyDiedDateMONTHSelect = container.one('#fuzzyDiedDateMONTH');
            this.fuzzyDiedDateDAYSelect = container.one('#fuzzyDiedDateDAY');
        },
        yearFormatChecker: function (updateAge) {
            var re4digit = /^\d{4}$/;
            var fuzzyYear = this.fuzzyDOBYEARTextBox.get('value');
            var fuzzyMonth = this.fuzzyDOBMONTHSelect.get('value'),
                resolvedMonth;
            // Only going to use the values if they are 4 digits
            var correctYearFormat = fuzzyYear.match(re4digit);

            // For Jan month value will be 0 , Number(0) will return false so fuzzyMonth === '0'
            resolvedMonth = this.resolveMonth(fuzzyMonth);

            if (correctYearFormat != null) {
                if (updateAge === true) {
                    var enteredDOBAsYear = new Date();
                    enteredDOBAsYear.setFullYear(fuzzyYear, resolvedMonth, 31);
                    enteredDOBAsYear.setHours(12, 0, 0, 0);

                    this.checkDate(enteredDOBAsYear);
                } else {
                    return fuzzyYear;
                }
            } else {
                // invalid year format so reset age field
                this.ageInput.set('value', "");
            }
        },
        resolveMonth: function (inputMonth) {
            var defaultMonth = 11;
            if (Number(inputMonth) || inputMonth === '0') {
                return inputMonth;
            }
            return defaultMonth;
        },
        checkDiedAndBornYear: function (updateAge) {
            var re4digit = /^\d{4}$/;
            var fuzzyDOBYear = this.fuzzyDeceasedDOBYEARTextBox.get('value'),
                fuzzyDiedYear = this.fuzzyDiedDateYEARTextBox.get('value'),
                fuzzyDOBMonth = this.fuzzyDeceasedDOBMONTHSelect.get('value'),
                fuzzyDiedMonth = this.fuzzyDiedDateMONTHSelect.get('value'),
                fuzzyDOBDay = this.fuzzyDeceasedDOBDAYSelect.get('value'),
                fuzzyDiedDay = this.fuzzyDiedDateDAYSelect.get('value');
            // Only going to use the values if they are 4 digits
            var correctfuzzyDOBYear = fuzzyDOBYear.match(re4digit),
                correctfuzzyDiedYear = fuzzyDiedYear.match(re4digit),
                resolvedDobMonth, resolvedDiedMonth;
            if (correctfuzzyDOBYear != null && correctfuzzyDiedYear != null) {
                if (updateAge === true) {
                    var enteredDOBAsYear = new Date();
                    resolvedDobMonth = this.resolveMonth(fuzzyDOBMonth);
                    var resolvedDobDay = this.getTotalDaysInMonth(fuzzyDOBMonth, fuzzyDOBYear);
                    if (Number(fuzzyDOBDay)) {
                        resolvedDobDay = fuzzyDOBDay;
                    }
                    enteredDOBAsYear.setFullYear(Number(correctfuzzyDOBYear), Number(resolvedDobMonth), Number(resolvedDobDay));
                    enteredDOBAsYear.setHours(12, 0, 0, 0);

                    var enteredDiedAsYear = new Date();
                    resolvedDiedMonth = this.resolveMonth(fuzzyDiedMonth);
                    var resolvedDiedDay = this.getTotalDaysInMonth(fuzzyDiedMonth, fuzzyDiedYear);
                    if (Number(fuzzyDiedDay)) {
                        resolvedDiedDay = fuzzyDiedDay;
                    }
                    enteredDiedAsYear.setFullYear(Number(correctfuzzyDiedYear), Number(resolvedDiedMonth), Number(resolvedDiedDay));
                    enteredDiedAsYear.setHours(12, 0, 0, 0);

                    this.calculateAgeAtDeath(enteredDOBAsYear, enteredDiedAsYear);
                } else {
                    return {
                        'bornYear': fuzzyDOBYear,
                        'diedYear': fuzzyDiedYear
                    };
                }
            } else {
                // invalid year format so reset age field
                this.ageAtDeath.set('value', "");
            }
        },
        onFuzzyYearChange: function (e) {
            // Any change to month will clear day field and therefore an estimated DOB
            this.toggleEstimatedCheckbox(true);
            this.fuzzyDOBDAYSelect.set('value', "");
            this.yearFormatChecker(true);
        },
        onFuzzyDayChange: function (e) {
            var day = this.fuzzyDOBDAYSelect.get('value'),
                month = this.fuzzyDOBMONTHSelect.get('value'),
                year = this.fuzzyDOBYEARTextBox.get('value'),
                enteredDOB = new Date(year, month, day, 12, 0, 0, 0);

            if (Number(day)) {
                this.checkDate(enteredDOB);
                this.toggleEstimatedCheckbox(false);
            } else {
                this.toggleEstimatedCheckbox(true);
                this.checkDate(this.monthOnlyDOBCalculation(month, year));
            }
        },
        onFuzzyMonthChange: function (e) {
            var month = this.fuzzyDOBMONTHSelect.get('value'),
                year = this.fuzzyDOBYEARTextBox.get('value');
            this.fuzzyDOBDAYSelect.set('value', "");
            this.toggleEstimatedCheckbox(true);
            this.checkDate(this.monthOnlyDOBCalculation(month, year));
        },
        onAgeChange: function (e) {
            var currentDate = new Date(),
                currentDay = currentDate.getDate(),
                currentMonth = currentDate.getMonth(),
                currentYear = currentDate.getFullYear(),
                // get the age from the event target
                age = e.currentTarget.get('value'),
                fuzzyDOBYEAR = '';

            // reset fuzzy inputs
            this.fuzzyDOBDAYSelect.set('value', "").setAttribute("disabled", "disabled");
            // set the estimate check box
            this.toggleEstimatedCheckbox(true);

            // Regex returns true if number between 0-9 or an empty string
            function checkAge(age) {
                var pattern = /^[0-9]{1,3}$|^$/;
                return pattern.test(age);
            }

            // Keep trimming the age until a valid digits are entered or there is nothing left to slice
            while (!checkAge(age)) {
                age = age.slice(0, -1);
            }

            // The age is either an valid age or a blank string
            if (age) {
                // set the clean value back into the input
                this.ageInput.set("value", age);
                // update the fuzzy month
                this.fuzzyDOBMONTHSelect.removeAttribute("disabled");

                // If a year entered, then the age is calculated based on the 31-12-YYYY
                // Therefore, if age is entered, it's +1 unless the date is the 31-12-YYYY
                if (currentDay == 31 && currentMonth == 11) {
                    fuzzyDOBYEAR = (currentYear - (Number(age)));
                } else {
                    fuzzyDOBYEAR = (currentYear - (Number(age) + 1));
                }
                // reset the month control
                this.fuzzyDOBMONTHSelect.set("value", null);
                this.fuzzyDOBYEARTextBox.set("value", fuzzyDOBYEAR);
            } else {
                this.ageInput.set("value", age);
                this.fuzzyDOBMONTHSelect.set('value', "").setAttribute("disabled", "disabled");
                this.fuzzyDOBYEARTextBox.set('value', "");
            }
        },
        onFuzzyDecasedYearChange: function (e) {
            if (e.target.get('id') === 'fuzzyDeceasedDOBYEAR') {
                this.toggleDeceasedEstimatedCheckbox(true, 'BORN');
                this.fuzzyDeceasedDOBDAYSelect.set('value', "");
            } else if (e.target.get('id') === 'fuzzyDiedDateYEAR') {
                this.toggleDeceasedEstimatedCheckbox(true, 'DIED');
                this.fuzzyDiedDateDAYSelect.set('value', "");
            }

            this.checkDiedAndBornYear(true);
        },
        onFuzzyDecasedMonthChange: function (e) {
            if (e.target.get('id') === 'fuzzyDeceasedDOBMONTH') {
                this.toggleDeceasedEstimatedCheckbox(true, 'BORN');
                this.fuzzyDeceasedDOBDAYSelect.set('value', "");
            } else if (e.target.get('id') === 'fuzzyDiedDateMONTH') {
                this.toggleDeceasedEstimatedCheckbox(true, 'DIED');
                this.fuzzyDiedDateDAYSelect.set('value', "");
            }
            var dOBMonth = this.fuzzyDeceasedDOBMONTHSelect.get('value'),
                dOBYear = this.fuzzyDeceasedDOBYEARTextBox.get('value'),
                diedMonth = this.fuzzyDiedDateMONTHSelect.get('value'),
                diedYear = this.fuzzyDiedDateYEARTextBox.get('value');
            this.calculateAgeAtDeath((this.monthOnlyDOBCalculation(dOBMonth, dOBYear)),
                (this.monthOnlyDOBCalculation(diedMonth, diedYear)));
        },
        onFuzzyDecasedDayChange: function (e) {

            var dobDay = this.fuzzyDeceasedDOBDAYSelect.get('value'),
                dobMonth = this.fuzzyDeceasedDOBMONTHSelect.get('value'),
                dobYear = this.fuzzyDeceasedDOBYEARTextBox.get('value'),
                enteredDOB = new Date(dobYear, dobMonth, dobDay, 12, 0, 0, 0);
            var diedDay = this.fuzzyDiedDateDAYSelect.get('value'),
                diedMonth = this.fuzzyDiedDateMONTHSelect.get('value'),
                diedYear = this.fuzzyDiedDateYEARTextBox.get('value'),
                enteredDiedDate = new Date(diedYear, diedMonth, diedDay, 12, 0, 0, 0);

            if (e.target.get('id') === 'fuzzyDeceasedDOBDAY') {
                Number(dobDay) ? this.toggleDeceasedEstimatedCheckbox(false, 'BORN') : this.toggleDeceasedEstimatedCheckbox(true, 'BORN');
            } else if (e.target.get('id') === 'fuzzyDiedDateDAY') {
                Number(diedDay) ? this.toggleDeceasedEstimatedCheckbox(false, 'DIED') : this.toggleDeceasedEstimatedCheckbox(true, 'DIED');
            }

            Number(dobDay) && Number(diedDay) ? this.calculateAgeAtDeath(enteredDOB, enteredDiedDate) : this.calculateAgeAtDeath((this.monthOnlyDOBCalculation(dobMonth, dobYear)),
                (this.monthOnlyDOBCalculation(diedMonth, diedYear)));
        },
        onChiNumberChange: function (e) {
            if (e.target.get('id') === 'chiNumber-input') {
                var chiNumberValue = e.target.get('value'),
                    message = this.get('labels').dobEstimateMessageWithChiNumber;

                if (L.isNull(chiNumberValue) || L.isUndefined(chiNumberValue) || chiNumberValue === "") {
                    return;
                }
                // validate against estimatedDateOfBirth checkbox
                if (this.estimatedCheckbox.get('checked')) {
                    this.notifyUserOfEffectOnChiNumber(message);
                }
                this.validateChiNumberAndGender();
            }
        },
        onDateOfBirthEstimatedChange: function (e) {
            if ((e.target.get('id') === 'dateOfBirthEstimated-input') && (e.target.get('checked'))) {
                var chiNumberValue = this.chiNumberInput.get('value')
                message = this.get('labels').dobEstimateMessageWithChiNumber;

                if (L.isNull(chiNumberValue) || L.isUndefined(chiNumberValue) || chiNumberValue === "") {
                    return;
                } else {
                    this.notifyUserOfEffectOnChiNumber(message);
                }
            }
        },
        getMessageNode: function () {
            var warningPanel = this.get('container').one('#pureInfoWrapper'),
                messageNode = warningPanel.one(".chiNumberWarningNotification"),
                contentNode;

            if (!messageNode) {
                messageNode = this.getNewMessageNode();
                contentNode = this.getNewContentNode(messageNode);
                warningPanel.append(contentNode);
            }

            return messageNode;
        },
        getNewMessageNode: function () {
            return Y.Node.create('<div class="chiNumberWarningNotification"></div>');
        },
        getNewContentNode: function (messageNode) {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(messageNode);
            return contentNode;
        },
        notifyUserOfEffectOnChiNumber: function (diaplayMessage) {
            var messageNode = this.getMessageNode(),
                labels = this.get('labels');

            if (!messageNode.get('children').size()) {
                var dismissibleMessage = new Y.usp.DismissibleMessage({
                    message: diaplayMessage,
                    type: 'warning'
                }).render(messageNode);

                Y.later(6000, this, function () {
                    dismissibleMessage.hide();
                });
            }
        },
        toggleEstimatedCheckbox: function (hide) {
            if (hide) {
                this.estimatedCheckbox.set('checked', false);
                this.ageInput.removeAttribute('disabled');
                this.pureInfoWrapper.removeClass("simple");
                this.estimatedInfoArea.removeClass("estimatedCheckbox");
                this.estimatedCheckboxArea.addClass("estimatedCheckbox");
            } else {
                this.estimatedInfoArea.addClass("estimatedCheckbox");
                this.pureInfoWrapper.addClass("simple");
                this.estimatedCheckboxArea.removeClass("estimatedCheckbox");
                this.ageInput.setAttribute("disabled", "disabled");
            }
        },
        toggleDeceasedEstimatedCheckbox: function (hide, widgetType) {
            if (hide) {
                if (widgetType === 'BORN') {
                    this.deceasedDobEstimatedCheckbox.set('checked', false);
                    this.deceasedDobEstimatedCheckboxArea.addClass('estimatedCheckbox');
                    this.deceasedDobEstimatedCheckboxArea.ancestor().addClass('estimatedCheckbox');
                } else if (widgetType === 'DIED') {
                    this.deceasedDiedEstimatedCheckbox.set('checked', false);
                    this.deceasedDiedEstimatedCheckboxArea.addClass('estimatedCheckbox');
                    this.deceasedDiedEstimatedCheckboxArea.ancestor().addClass('estimatedCheckbox');
                }
                if (this.deceasedDiedEstimatedCheckboxArea.ancestor().hasClass('estimatedCheckbox') &&
                    this.deceasedDobEstimatedCheckboxArea.ancestor().hasClass('estimatedCheckbox')) {
                    this.pureInfoWrapper.removeClass('estimatedCheckbox');
                }
                this.pureInfoWrapper.removeClass('simple');
                this.estimatedDiedInfoArea.removeClass('estimatedCheckbox');
            } else {
                if (widgetType === 'BORN') {
                    this.deceasedDobEstimatedCheckboxArea.ancestor().removeClass('estimatedCheckbox');
                    this.deceasedDobEstimatedCheckboxArea.removeClass('estimatedCheckbox');
                    this.deceasedDobEstimatedCheckboxArea.ancestor().addClass('simple');
                } else if (widgetType === 'DIED') {
                    this.deceasedDiedEstimatedCheckboxArea.ancestor().removeClass('estimatedCheckbox');
                    this.deceasedDiedEstimatedCheckboxArea.removeClass('estimatedCheckbox');
                    this.deceasedDiedEstimatedCheckboxArea.ancestor().addClass('simple');
                }
                this.estimatedDiedInfoArea.addClass('estimatedCheckbox');
                this.pureInfoWrapper.addClass('estimatedCheckbox');
            }
        },
        isLeapYear: function (year) {
            var date = new Date(year, 1, 28);
            date.setDate(date.getDate() + 1);
            // If it is a leap year, then the month will remain 1 (0=Jan,1=Feb) else the increment above will push it into March
            return date.getMonth() == 1;
        },
        checkDate: function (date) {
            var today = new Date(),
                // prevent update of date passed in from event handler
                enteredDOB = new Y.USPDate.parseDate(date);

            // Calculate today date and time
            today.setHours(12, 0, 0, 0);
            // calculate the number of whole years up to last birthday
            var years = today.getFullYear() - enteredDOB.getFullYear();
            enteredDOB.setFullYear(enteredDOB.getFullYear() + years);
            // check if DOB has come this year or not
            if (enteredDOB > today) {
                // no birthday yet, so reduce the age
                years--;
                // set the Full year back by one to calculate the number of days till age increments
                enteredDOB.setFullYear(enteredDOB.getFullYear() - 1);
            }
            // Get the number of days between today and last birthday
            var days = (today.getTime() - enteredDOB.getTime()) / (3600 * 24 * 1000);
            // divide days by 365 or 366 if it's leap year. Round down and add to years.
            var age = years + Math.floor(days / (this.isLeapYear(today.getFullYear()) ? 366 : 365));
            if (age < 0) {
                age = "0";
            }
            // update age input value
            this.ageInput.set('value', age);
        },
        getResolvedDate: function (year, month, day) {
            var resolvedDate = null;
            if (Number(year)) {
                resolvedDate = new Date();

                var resolvedMonth = this.resolveMonth(month);
                var resolvedDay = this.getTotalDaysInMonth(month, year);
                if (Number(day)) {
                    resolvedDay = day;
                }
                resolvedDate.setFullYear(year, Number(resolvedMonth), Number(resolvedDay));
                resolvedDate.setHours(12, 0, 0, 0);
            }
            return resolvedDate;
        },
        calculateAgeOnDateSelection: function (e) {

            if (this.bornDateValue === undefined) {

                var fuzzyDOBYear = this.fuzzyDeceasedDOBYEARTextBox.get('value'),
                    fuzzyDOBMonth = this.fuzzyDeceasedDOBMONTHSelect.get('value'),
                    fuzzyDOBDay = this.fuzzyDeceasedDOBDAYSelect.get('value');
                this.bornDateValue = this.getResolvedDate(fuzzyDOBYear, fuzzyDOBMonth, fuzzyDOBDay);
            }

            if (this.diedDateValue === undefined) {
                var fuzzyDiedYear = this.fuzzyDiedDateYEARTextBox.get('value'),
                    fuzzyDiedMonth = this.fuzzyDiedDateMONTHSelect.get('value'),
                    fuzzyDiedDay = this.fuzzyDiedDateDAYSelect.get('value');

                this.diedDateValue = this.getResolvedDate(fuzzyDiedYear, fuzzyDiedMonth, fuzzyDiedDay);
            }
            if (L.isDate(this.bornDateValue) && L.isDate(this.diedDateValue)) {
                this.calculateAgeAtDeath(this.bornDateValue, this.diedDateValue);
            }
        },
        calculateAgeAtDeath: function (bornDate, diedDate) {
            // prevent update of date passed in from event handler
            enteredDOB = new Y.USPDate.parseDate(bornDate);
            enteredDiedDate = new Y.USPDate.parseDate(diedDate);
            if (L.isDate(enteredDOB) && L.isDate(enteredDiedDate)) {
                // calculate the number of whole years up to last birthday
                var years = enteredDiedDate.getFullYear() - enteredDOB.getFullYear();
                enteredDOB.setFullYear(enteredDOB.getFullYear() + years);
                // check if DOB has come this year or not
                if (enteredDOB > enteredDiedDate) {
                    // no birthday yet, so reduce the age
                    years--;
                    // set the Full year back by one to calculate the number of days till age increments
                    enteredDOB.setFullYear(enteredDOB.getFullYear() - 1);
                }
                // Get the number of days between today and last birthday
                var days = (enteredDiedDate.getTime() - enteredDOB.getTime()) / (3600 * 24 * 1000);
                // divide days by 365 or 366 if it's leap year. Round down and add to years.
                var age = years + Math.floor(days / (this.isLeapYear(enteredDiedDate.getFullYear()) ? 366 : 365));
                if (age < 0) {
                    age = "0";
                }
                // update age input value
                this.ageAtDeath.set('value', age);
            } else {
                this.ageAtDeath.set('value', '');
            }
        },
        getTotalDaysInMonth: function (month, fuzzyYear) {

            var totalDays = 31; // default to 31 days as this is most likely February leap year
            if (month === "1" && this.isLeapYear(fuzzyYear)) {
                totalDays = 29;
            }

            // February
            else if (month === "1") {
                totalDays = 28;
            }

            // April, June, September, November
            else if (month === "3" || month === "5" || month === "8" || month === "10") {
                totalDays = 30;
            }
            return totalDays;
        },
        monthOnlyDOBCalculation: function (month, fuzzyYear) {

            var monthOnlyDOBCalculationDOB = new Date();

            monthOnlyDOBCalculationDOB.setHours(12, 0, 0, 0);

            var totalDays = this.getTotalDaysInMonth(month, fuzzyYear);

            monthOnlyDOBCalculationDOB.setFullYear(fuzzyYear, Number(month), totalDays);

            return monthOnlyDOBCalculationDOB;

        },
        getCitizenships: function (type) {
            return this.getCitizenshipsList().map(function (rec) {
                if (rec.get('_type') === type) {
                    return type === 'DeleteCitizenship' ? rec.get('id') : rec;
                }
            }).filter(Boolean);

        },
        getNhsNumber: function () {
            var nhsNum = [];
            if (this.nhsFirstThreeDigitsInput || this.nhsSecondThreeDigitsInput || this.nhsLastFourDigitsInput) {
                nhsNum.push(this.nhsFirstThreeDigitsInput.get('value') || '');
                nhsNum.push(this.nhsSecondThreeDigitsInput.get('value') || '');
                nhsNum.push(this.nhsLastFourDigitsInput.get('value') || '');
            }
            return nhsNum.join('');
        },
        getFuzzyDOB: function () {
            return this.fuzzyDOBWidget.getFuzzyDate();
        },
        getFuzzyDeceasedDOB: function () {
            return this.fuzzyDeceasedDOBWidget.getFuzzyDate();
        },
        getFuzzyDeceasedDiedDate: function () {
            return this.fuzzydiedDateWidget.getFuzzyDate();
        },
        // These 2 methods have been re-instated, as they are used within the dialog save code.
        // This is not a good way of doing this. Nothing outside the view should be looking up nodes
        getWrapper: function (id) {
            return this.get('container').one('#' + id + '-wrapper');
        },
        getInput: function (id) {
            return this.get('container').one('#' + id + '-input');
        },
        getMinDueDate: function () {
            return new Date();
        },
        getMaxNotCarriedToTermDate: function () {
            return new Date();
        },
        getMaxDueDate: function (MAX_PREGNANCY_DURATION) {
            return Y.Date.addDays(this.getMinDueDate(), MAX_PREGNANCY_DURATION);
        }

    };
    var AddPersonRolesView = Y.Base.create('AddPersonRolesView', Y.usp.person.PersonView, [], {

        template: Y.Handlebars.templates["addPersonRolesDialog"],
        render: function () {
            var html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                codedEntries: {
                    personType: Y.uspCategory.person.personType.category.codedEntries
                },
                addPersonRoles: this.get('action') === 'addPersonRoles'
            });

            this.get('container').setHTML(html);

            this.initRoles();

            return this;
        },
        events: {
            '#person_roles input': {
                change: 'addSelectedRoles'
            }
        },
        initRoles: function () {
            if (this.get('personRoles')) {
                if (this.get('personRoles').indexOf('CLIENT') > -1) {
                    this.get('container').one('#person_roleClient-input').setAttribute('checked', 'checked');
                    this.get('container').one('#person_roleClient-input').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                }
                if (this.get('personRoles').indexOf('PROFESSIONAL') > -1) {
                    this.get('container').one('#person_roleProfessional-input').setAttribute('checked', 'checked');
                    this.get('container').one('#person_roleProfessional-input').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                }
                if (this.get('personRoles').indexOf('OTHER') > -1) {
                    this.get('container').one('#person_roleOther-input').setAttribute('checked', 'checked');
                    this.get('container').one('#person_roleOther-input').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                }
                if (this.get('personRoles').indexOf('FOSTER_CARER') > -1) {
                    this.get('container').one('#person_roleFosterCarer-input').setAttribute('checked', 'checked');
                    this.get('container').one('#person_roleFosterCarer-input').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                }
                if (this.get('personRoles').indexOf('ADOPTER') > -1) {
                    this.get('container').one('#person_roleAdopter-input').setAttribute('checked', 'checked');
                    this.get('container').one('#person_roleAdopter-input').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                }
            }
        },
        addSelectedRoles: function (e) {
            var roleType = [],
                lifeState = null;
            switch (e.target.get('id')) {
            case 'person_roleClient-input':
                roleType = 'CLIENT';
                lifeState = 'ALIVE';
                break;
            case 'person_roleOther-input':
                roleType = 'OTHER';
                lifeState = 'ALIVE';
                break;
            case 'person_roleProfessional-input':
                roleType = 'PROFESSIONAL';
                lifeState = 'ALIVE';
                break;
            case 'person_roleFosterCarer-input':
                roleType = 'FOSTER_CARER';
                lifeState = 'ALIVE';
                break;
            case 'person_roleAdopter-input':
                roleType = 'ADOPTER';
                lifeState = 'ALIVE';
                break;
            }

            var roles = this.get('personRoles');
            if (typeof roles === 'undefined') {
                roles = [];
            }
            var pos = roles.indexOf(roleType);

            if (e.target.get('checked')) {
                if (!pos > -1) {
                    roles.push(roleType);
                }
            } else {
                if (pos > -1) {
                    roles.splice(pos, 1);
                }
            }

            this.setAttrs({
                'personRoles': roles,
                'lifeState': lifeState
            }, {
                silent: true
            });

        }
    }, {

        ATTRS: {

        }
    });

    var PersonRoleChangeView = Y.Base.create('PersonRoleChangeView', Y.usp.person.PersonView, [], {

        template: Y.Handlebars.templates["personRoleChangeDialog"],
        render: function () {
            var html = this.template({
                labels: this.get('labels'),
                codedEntries: {
                    personType: Y.uspCategory.person.personType.category.codedEntries
                },
                modelData: this.get('model').toJSON()
            });
            this.setAttrs({
                'personRoles': ["CLIENT"]
            }, {
                silent: true
            });

            this.get('container').setHTML(html);

            return this;
        },
        events: {
            '#person_roles input': {
                change: 'updateSelectedRoles'
            }
        },

        updateSelectedRoles: function (e) {
            this.setAttrs({
                'personRoles': [e.currentTarget.get('value')]
            }, {
                silent: true
            });
        }
    }, {

        ATTRS: {

        }
    });

    // Extend the USP PersonView
    var ViewPersonView = Y.Base.create('ViewPersonView', Y.usp.person.PersonView, [], {
        // bind template
        template: Y.Handlebars.templates["personViewDialog"]
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    ethnicity: Y.uspCategory.person.ethnicity.category.codedEntries,
                    title: Y.uspCategory.person.title.category.codedEntries,
                    gender: Y.uspCategory.person.gender.category.codedEntries
                }
            }
        }
    });

    var UpdatePersonView = Y.Base.create('UpdatePersonView', Y.usp.person.UpdatePersonView, [BaseAddUpdatePerson], {
        events: {
            'input[name="outcome"]': {
                click: '_handleRadioChange'
            }
        },
        initializer: function (config) {
            this.events = Y.merge(this.events, BaseAddUpdatePerson.prototype.events);
        },
        // bind template
        template: Y.Handlebars.templates['personEditDialog'],

        render: function () {
            var container = this.get('container'),
                isProfessional = this.get('personRoles').indexOf('PROFESSIONAL') > -1,
                isClient = this.get('personRoles').indexOf('CLIENT') > -1,
                isOther = this.get('personRoles').indexOf('OTHER') > -1,
                isFosterCarer = this.get('personRoles').indexOf('FOSTER_CARER') > -1,
                isAdopter = this.get('personRoles').indexOf('ADOPTER') > -1,
                lifeState = UP.GetImpliedLifeState(this.get('lifeState')),
                isOtherOrFosterOrAdopter = isOther || isFosterCarer || isAdopter,
                professionalOnly = isProfessional && !isOtherOrFosterOrAdopter && !isClient;

            var html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                otherRole: isOtherOrFosterOrAdopter,
                clientRole: isClient,
                professionalRole: isProfessional,
                professionalOrUnborn: (lifeState === 'UNBORN') || (isProfessional && !isClient && !isOther),
                professionalOrUnbornOrNotCarriedOrDeceased: lifeState === 'UNBORN' || lifeState === 'NOT_CARRIED_TO_TERM' || lifeState === 'DECEASED' || professionalOnly,
                deceased: lifeState === 'DECEASED',
                unbornClient: lifeState === 'UNBORN' && isClient,
                notCarriedToTermClient: lifeState === 'NOT_CARRIED_TO_TERM' && isClient,
                unbornOrClient: lifeState === 'UNBORN' || isClient,
                aliveOrDeceasedClientOrCarer: (isClient || isFosterCarer || isAdopter) && (lifeState === 'ALIVE' || lifeState === 'DECEASED'),
                pictureUrl: this.get('pictureUrl')
            });

            // this.constructor.superclass.render.call(this);
            this.get('container').setHTML(html);

            // call common render code
            this._renderCommon();

            // cache radio inputs
            this.maleInput = container.one('#male-input');
            this.femaleInput = container.one('#female-input');
            this.indeterminateInput = container.one('#indeterminate-input');
            this.unknownInput = container.one('#unknown-input');
            // Set up due date for unborn client
            this.exactDueDateWidget = new Y.usp.CalendarPopup({
                inputNode: this.dueDateInput,
                // supply a Date for minimum range
                minimumDate: this.getMinDueDate(),
                // supply a Date for maximum range
                maximumDate: this.getMaxDueDate(280)
            });
            if (this.dueDateInput && this.getLifeState() === 'UNBORN') {
                this.exactDueDateWidget.render();
            }
            this.initDueDate();

            // Setup Not carried to term reason
            this.initNotCarriedToTermType();

            // Set up not carried date for 'NotCarriedToTerm' client
            if (this.notCarriedToTermDateInput && this.getLifeState() === 'NOT_CARRIED_TO_TERM') {
                this.exactNotCarriedToTermDateWidget = new Y.usp.CalendarPopup({
                    inputNode: this.notCarriedToTermDateInput,
                    // Supply maximum date
                    maximumDate: this.getMaxNotCarriedToTermDate()
                });
                this.exactNotCarriedToTermDateWidget.render();
                this.initNotCarriedToTermDate();
            }

            // Setup ethnicity select box
            this.initEthnicity();
            this.initReligion();
            this.initCountryOfBirth();

            if (this.permissions.canViewCitizenship) {
                this.initCitizenships();
            }

            // Setup title select box
            this.initTitle();

            // setup NHS Number
            this.initNHSNumber();
            // setup Gender
            this.initGender();
            this.initGenderIdentity();
            this.initSexualOrientation();

            // setup DOB
            this.initDOB();
            this.initDiedDate();
            this.initProfessionalTitle();

            this.initOrganisation();

            return this;
        },
        _handleRadioChange: function (e) {
            this.set('outcome', e.currentTarget.get('value'));
        },
        initTitle: function () {
            var model = this.get('model'),
                // is there an existing title picked?
                existingTitle = this._lookupExistingEntry(Y.uspCategory.person.title.category.codedEntries, model.get('title'));

            Y.FUtil.setSelectOptions(this.titleInput, O.values(Y.uspCategory.person.title.category.getActiveCodedEntries()), existingTitle.code, existingTitle.name, true, true, 'code');

            // select value
            this.titleInput.set('value', model.get('title'));
        },
        initProfessionalTitle: function () {

            var model = this.get('model'),
                existingProfTitle = this._lookupExistingEntry(Y.uspCategory.person.professionalTitle.category.codedEntries, model.get('professionalTitle'));
            if (this.professionalTitleInput) {
                Y.FUtil.setSelectOptions(this.professionalTitleInput, O.values(Y.uspCategory.person.professionalTitle.category.getActiveCodedEntries()), existingProfTitle.code, existingProfTitle.name, true, true, 'code');
                this.professionalTitleInput.set('value', model.get('professionalTitle'));
            }

        },
        initOrganisation: function () {
            if (this.organisationInput) {
                var model = this.get('model');
                this.organisationInput.set('value', model.get('organisationName'));
            }
        },
        initDueDate: function () {
            var model = this.get('model');
            if (this.dueDateInput && this.getLifeState() === 'UNBORN') {
                this.dueDateInput.set('value', Y.USPDate.formatDateValue(model.get('dueDate'), null));
            }
        },
        initNotCarriedToTermType: function () {
            if (this.notCarriedToTermDateInput) {
                switch (this.get('model').get('notCarriedToTermType')) {
                case 'TERMINATION':
                    this.terminationInput.set('checked', 'checked');
                    break;
                case 'MISCARRIAGE':
                    this.miscarriageInput.set('checked', 'checked');
                    break;
                case 'STILLBIRTH':
                    this.stillbirthInput.set('checked', 'checked');
                    break;
                }
            }
        },
        initNotCarriedToTermDate: function () {
            var model = this.get('model');
            this.notCarriedToTermDateInput.set('value', Y.USPDate.formatDateValue(model.get('notCarriedToTermDate'), null));

        },
        initEthnicity: function () {
            var model = this.get('model'),
                // is there an existing ethnicity picked?
                existingEthnicity = this._lookupExistingEntry(Y.uspCategory.person.ethnicity.category.codedEntries, model.get('ethnicity'));
            if (this.ethnicityInput) {
                Y.FUtil.setSelectOptions(this.ethnicityInput, O.values(Y.uspCategory.person.ethnicity.category.getActiveCodedEntries()), existingEthnicity.code, existingEthnicity.name, true, true, 'code');
                // select value
                this.ethnicityInput.set('value', model.get('ethnicity'));
            }
        },
        initReligion: function () {
            if (this.religionInput) {
                Y.FUtil.setSelectOptions(this.religionInput, O.values(Y.uspCategory.person.religion.category.getActiveCodedEntries()), null, null, true, true, 'code');
                this.religionInput.set('value', this.get('model').get('religion'));
            }
        },
        initCountryOfBirth: function () {
            if (this.countryOfBirthInput) {
                Y.FUtil.setSelectOptions(this.countryOfBirthInput, O.values(Y.uspCategory.address.countryCode.category.getActiveCodedEntries()), null, null, true, true, 'code');
                this.countryOfBirthInput.set('value', this.get('model').get('countryOfBirth'));
            }
        },
        _setupCitizenships: function (citizenship, index) {
            var permissions = this.permissions;
            var updateCitizenshipRows = this.get('container').all('#citizenshipResultsTableResults tbody.pure-table-data tr');
            updateCitizenshipRows.item(index).all('select, input, button').each(Y.bind(function (node) {
                this._initCitizenshipRow(node, citizenship);
            }, this));
        },
        initDOB: function () {
            var model = this.get('model'),
                dob = model.get('dateOfBirth'),
                age = model.get('age');
            if (this.dateOfBirthInput && dob) {
                this.fuzzyDOBWidget.setDateAndMask(new Date(dob.calculatedDate), dob.fuzzyDateMask);
                if (dob.fuzzyDateMask !== 'DAY') {
                    this.toggleEstimatedCheckbox(true);
                } else if (dob.fuzzyDateMask === 'DAY') {
                    // set the checkbox state based on model data
                    this.estimatedCheckbox.set('checked', model.get('dateOfBirthEstimated'));
                    this.toggleEstimatedCheckbox(false);
                }
            }
            if (this.deceasedDateOfBirthInput && dob) {
                this.fuzzyDeceasedDOBWidget.setDateAndMask(new Date(dob.calculatedDate), dob.fuzzyDateMask);
                if (dob.fuzzyDateMask !== 'DAY') {
                    this.toggleDeceasedEstimatedCheckbox(true, 'BORN');
                } else if (dob.fuzzyDateMask === 'DAY') {
                    // set the checkbox state based on model data
                    this.deceasedDobEstimatedCheckbox.set('checked', model.get('dateOfBirthEstimated'));
                    this.toggleDeceasedEstimatedCheckbox(false, 'BORN');
                }
            }
            if (this.ageInput && dob && age >= 0) {
                this.ageInput.set('value', age);
            }
            if (this.ageAtDeath && dob && age >= 0) {
                this.ageAtDeath.set('value', age);
            }
        },
        initDiedDate: function () {
            var model = this.get('model'),
                diedDate = model.get('diedDate');
            if (this.diedDateInput) {
                if (diedDate) {
                    this.fuzzydiedDateWidget.setDateAndMask(new Date(diedDate.calculatedDate), diedDate.fuzzyDateMask);
                }

                // Prevent the died date UI from being altered
                this.fuzzyDiedDateYEARTextBox.setAttribute('disabled', 'disabled');
                this.fuzzyDiedDateMONTHSelect.setAttribute('disabled', 'disabled');
                this.fuzzyDiedDateDAYSelect.setAttribute('disabled', 'disabled');
                var diedDateCalendarIcon = this.diedDateInput.one('a');
                diedDateCalendarIcon.setAttribute("disabled", "disabled");
                if (diedDateCalendarIcon._node) {
                    Y.one(diedDateCalendarIcon._node.parentNode).setStyle('pointer-events', 'none');
                }
            }

        },
        getLifeState: function () {
            return this.get('lifeState');
        },
        initNHSNumber: function () {
            var nhsNumber = this.get('model').get('nhsNumber');

            if (nhsNumber !== null && nhsNumber !== "") {
                this.nhsFirstThreeDigitsInput.set('value', nhsNumber.substr(0, 3));
                this.nhsSecondThreeDigitsInput.set('value', nhsNumber.substr(3, 3));
                this.nhsLastFourDigitsInput.set('value', nhsNumber.substr(6, 4));
            }
        },
        initGender: function () {
            if (this.maleInput) {
                switch (this.get('model').get('gender')) {
                case 'MALE':
                    this.maleInput.set('checked', 'checked');
                    break;
                case 'FEMALE':
                    this.femaleInput.set('checked', 'checked');
                    break;
                case 'INDETERMINATE':
                    this.indeterminateInput.set('checked', 'checked');
                    break;
                case 'UNKNOWN':
                    this.unknownInput.set('checked', 'checked');
                    break;
                }
            }
        },
        initGenderIdentity: function () {
            var model = this.get('model'),
                existingGenderIdentity = this._lookupExistingEntry(Y.uspCategory.person.ethnicity.category.codedEntries, model.get('genderIdentity'));
            if (this.genderIdentityInput) {
                Y.FUtil.setSelectOptions(this.genderIdentityInput, O.values(Y.uspCategory.person.genderIdentity.category.getActiveCodedEntries()), existingGenderIdentity.code, existingGenderIdentity.name, true, true, 'code');
                this.genderIdentityInput.set('value', model.get('genderIdentity'));
                if (model.get('genderIdentity') === 'SELF_DEFINED') {
                    this.get('container').one('#genderIdentitySelfDefined-container').show();
                }
            }
        },
        initSexualOrientation: function () {
            var model = this.get('model'),
                existingSexualOrientation = this._lookupExistingEntry(Y.uspCategory.person.ethnicity.category.codedEntries, model.get('sexualOrientation'));
            if (this.sexualOrientationInput) {
                Y.FUtil.setSelectOptions(this.sexualOrientationInput, O.values(Y.uspCategory.person.sexualOrientation.category.getActiveCodedEntries()), existingSexualOrientation.code, existingSexualOrientation.name, true, true, 'code');
                this.sexualOrientationInput.set('value', model.get('sexualOrientation'));
            }
        },
        _lookupExistingEntry: function (codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        },
        destructor: function () {
            if (this.maleInput) {
                this.maleInput.destroy();
                delete this.maleInput;
            }
            if (this.femaleInput) {
                this.femaleInput.destroy();
                delete this.femaleInput;
            }
            if (this.indeterminateInput) {
                this.indeterminateInput.destroy();
                delete this.indeterminateInput;
            }
            if (this.unknownInput) {
                this.unknownInput.destroy();
                delete this.unknownInput;
            }

            // call common destructor code
            this._destroyCommon();
        }
    });

    var RemovePreviousNameView = Y.Base.create('removePreviousNameView', Y.usp.person.PersonView, [], {
        template: Y.Handlebars.templates["personPreviousNameRemoveDialog"]
    });

    var ChangePersonNameView = Y.Base.create('ChangePersonNameView', Y.usp.person.UpdatePersonView, [], {
        // bind template
        template: Y.Handlebars.templates["changePersonName"],
        render: function () {
            // superclass call
            this.constructor.superclass.render.call(this);

            this.initTitle();

            return this;
        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#' + inputId + '-input');
        },
        initTitle: function () {
            var model = this.get('model'),
                existingTitle = this._lookupExistingEntry(Y.uspCategory.person.title.category.codedEntries, model.get('title'));
            Y.FUtil.setSelectOptions(this.getInput('title'), O.values(Y.uspCategory.person.title.category.getActiveCodedEntries()), existingTitle.code, existingTitle.name, true, true, 'code');
            this.getInput('title').set('value', model.get('title'));
        },
        _lookupExistingEntry: function (codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        }
    });

    var ChangeUnbornView = Y.Base.create('changeUnbornView', Y.usp.person.UpdatePersonBornView, [BaseAddUpdatePerson], {
        events: {
            'input[name="outcome"]': {
                click: '_handleRadioChange'
            },
            '#dateOfBirthEstimated-input': {
                change: 'onDateOfBirthEstimatedChange'
            },
            '#chiNumber-input': {
                change: 'onChiNumberChange'
            },
            '#gender input': {
                change: 'onGenderChanged'
            }
        },
        template: Y.Handlebars.templates["changeUnbornDialog"],

        initializer: function () {

            this._evtHandler = [];
            this._evtHandler.push(this.after('outcomeChange', this._handleOutcomeChange, this));
        },

        destructor: function () {
            if (this._evtHandler) {
                A.each(this._evtHandler, function (event) {
                    if (event) {
                        event.detach();
                    }
                });
            }
            this._evtHandler = null;
        },

        render: function () {
            // superclass call
            var container = this.get('container');

            html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON()
            });

            this.setAttrs({
                'personRoles': ["CLIENT"]
            }, {
                silent: true
            });

            // Render this view's HTML into the container element.
            container.setHTML(html);

            this.maleInput = container.one('#male-input');
            this.femaleInput = container.one('#female-input');
            this.indeterminateInput = container.one('#indeterminate-input');
            this.unknownInput = container.one('#unknown-input');
            this._renderCommon();

            // Set up not carried date for unborn client
            if (this.notCarriedToTermDateInput) {
                this.exactNotCarriedToTermDateWidget = new Y.usp.CalendarPopup({
                    inputNode: this.notCarriedToTermDateInput,
                    // Supply maximum date
                    maximumDate: this.getMaxNotCarriedToTermDate()
                });
                this.exactNotCarriedToTermDateWidget.render();
            }

            this.initEthnicity();
            this.initGender();
            this.initNotCarriedToTermDate();
            this.get('container').one('.notCarriedToTermDate').hide();

            return this;
        },

        _handleRadioChange: function (e) {
            this.set('outcome', e.currentTarget.get('value'));
        },

        _handleOutcomeChange: function (e) {
            this.get('container').one('.notCarriedToTermDate').show();
            this.get('container').one('.details').hide();
            if (e.newVal === 'BORN') {
                this.get('container').one('.notCarriedToTermDate').hide();
                this.get('container').one('#notCarriedToTermDate-input').set('value', '');
                this.get('container').one('.details').show();
            }
        },

        initEthnicity: function () {
            var model = this.get('model'),
                // is there an existing ethnicity picked?
                existingEthnicity = this._lookupExistingEntry(Y.uspCategory.person.ethnicity.category.codedEntries, model.get('ethnicity'));
            if (this.ethnicityInput) {
                Y.FUtil.setSelectOptions(this.ethnicityInput, O.values(Y.uspCategory.person.ethnicity.category.getActiveCodedEntries()), existingEthnicity.code, existingEthnicity.name, true, true, 'code');
                // select value
                this.ethnicityInput.set('value', model.get('ethnicity'));
            }
        },
        initGender: function () {

            switch (this.get('model').get('gender')) {
            case 'MALE':
                this.maleInput.set('checked', 'checked');
                break;
            case 'FEMALE':
                this.femaleInput.set('checked', 'checked');
                break;
            case 'INDETERMINATE':
                this.indeterminateInput.set('checked', 'checked');
                break;
            case 'UNKNOWN':
                this.unknownInput.set('checked', 'checked');
                break;
            }
        },
        initNotCarriedToTermDate: function () {
            var model = this.get('model');
            if (this.notCarriedToTermDateInput) {
                this.notCarriedToTermDateInput.set('value', Y.USPDate.formatDateValue(model.get('notCarriedToTermDate'), null));
            }
        },
        _lookupExistingEntry: function (codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        }

    }, {

        ATTRS: {
            outcome: {
                value: 'BORN'
            }
        }
    });

    var ChangeAliveView = Y.Base.create('ChangeAliveView', Y.usp.person.UpdatePersonDeceasedView, [BaseAddUpdatePerson], {
        template: Y.Handlebars.templates["changeAliveDialog"],

        render: function () {
            var container = this.get('container'),
                lifeState = UP.GetImpliedLifeState(this.get('lifeState'));

            var html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON()
            });

            // this.constructor.superclass.render.call(this);
            this.get('container').setHTML(html);

            // call common render code
            this._renderCommon();
            // setup DOB
            this.initDOB();
            return this;
        },
        initDOB: function () {
            var model = this.get('model'),
                dob = model.get('dateOfBirth'),
                age = model.get('age');
            if (this.dateOfBirthInput && dob) {
                this.fuzzyDOBWidget.setDateAndMask(new Date(dob.calculatedDate), dob.fuzzyDateMask);
                if (dob.fuzzyDateMask !== 'DAY') {
                    this.toggleEstimatedCheckbox(true);
                } else if (dob.fuzzyDateMask === 'DAY') {
                    // set the checkbox state based on model data
                    this.estimatedCheckbox.set('checked', model.get('dateOfBirthEstimated'));
                    this.toggleEstimatedCheckbox(false);
                }
            }
            if (this.deceasedDateOfBirthInput && dob) {
                this.fuzzyDeceasedDOBWidget.setDateAndMask(new Date(dob.calculatedDate), dob.fuzzyDateMask);
                if (dob.fuzzyDateMask !== 'DAY') {
                    this.toggleDeceasedEstimatedCheckbox(true, 'BORN');
                } else if (dob.fuzzyDateMask === 'DAY') {
                    // set the checkbox state based on model data
                    this.deceasedDobEstimatedCheckbox.set('checked', model.get('dateOfBirthEstimated'));
                    this.toggleDeceasedEstimatedCheckbox(false, 'BORN');
                }
            }
            if (this.ageAtDeath) {
                this.ageAtDeath.set('value', '');
            }
        },
        getLifeState: function () {
            return this.get('lifeState');
        },
        destructor: function () {
            // call common destructor code
            this._destroyCommon();
        },
    });
    var ChangeDeceasedToAliveView = Y.Base.create('ChangeDeceasedToAliveView', Y.usp.person.UpdatePersonDeceasedToAliveView, [], {
        template: Y.Handlebars.templates["removeDeceasedConfirmDialog"],
        render: function () {
            // superclass call
            var container = this.get('container');
            html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON()
            });

            // Render this view's HTML into the container element.
            container.setHTML(html);
        }
    });
    var ChangeNotCarriedView = Y.Base.create('ChangeNotCarriedView', Y.usp.person.UpdateNotCarriedPersonBornView, [BaseAddUpdatePerson], {
        // Worth understanding that these events will overwrite those coming from the 'BaseAddUpdatePerson' that's being mixed in.^
        // It doesn't look to me as though any of those are being used, so I'm not going to change this, but it's the sort of thing that could catch
        // folks out.
        events: {
            'input[name="outcome"]': {
                click: '_handleRadioChange'
            },
            '#dateOfBirthEstimated-input': {
                change: 'onDateOfBirthEstimatedChange'
            },
            '#chiNumber-input': {
                change: 'onChiNumberChange'
            },
            '#gender input': {
                change: 'onGenderChanged'
            }
        },
        template: Y.Handlebars.templates["changeNotCarriedDialog"],

        initializer: function () {

            this._evtHandler = [];
            this._evtHandler.push(this.after('outcomeChange', this._handleOutcomeChange, this));
            // this._evtHandler.push(this.get('container').delegate('click',
            // this._handleRadioChange, 'input[name="pregnancyState"]', this));
        },

        destructor: function () {
            if (this._evtHandler) {
                A.each(this._evtHandler, function (event) {
                    if (event) {
                        event.detach();
                    }
                });
            }
            this._evtHandler = null;
        },

        render: function () {
            // superclass call
            var container = this.get('container');
            html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON()
            });

            this.setAttrs({
                'personRoles': ["CLIENT"]
            }, {
                silent: true
            });

            // Render this view's HTML into the container element.
            container.setHTML(html);

            this.maleInput = container.one('#male-input');
            this.femaleInput = container.one('#female-input');
            this.indeterminateInput = container.one('#indeterminate-input');
            this.unknownInput = container.one('#unknown-input');
            this._renderCommon();
            // Set up due date for unborn client
            this.exactDueDateWidget = new Y.usp.CalendarPopup({
                inputNode: this.dueDateInput,
                // supply a Date for minimum range
                minimumDate: this.getMinDueDate(),
                // supply a Date for maximum range
                maximumDate: this.getMaxDueDate(280)
            });
            if (this.dueDateInput) {
                this.exactDueDateWidget.render();
            }
            this.initDueDate();
            this.initEthnicity();
            this.initGender();

            return this;
        },

        _handleRadioChange: function (e) {
            this.set('outcome', e.currentTarget.get('value'));
        },

        _handleOutcomeChange: function (e) {
            this.get('container').one('.born-details').hide();
            if (e.newVal === 'BORN') {
                this.get('container').one('.born-details').show();
                this.get('container').one('.unborn-details').hide();
            } else if (e.newVal === 'UNBORN') {
                this.get('container').one('.unborn-details').show();
                this.get('container').one('.born-details').hide();
            }
        },
        getLifeState: function () {
            return this.get('lifeState');
        },
        initEthnicity: function () {
            var model = this.get('model'),
                // is there an existing ethnicity picked?
                existingEthnicity = this._lookupExistingEntry(Y.uspCategory.person.ethnicity.category.codedEntries, model.get('ethnicity'));
            if (this.ethnicityInput) {
                Y.FUtil.setSelectOptions(this.ethnicityInput, O.values(Y.uspCategory.person.ethnicity.category.getActiveCodedEntries()), existingEthnicity.code, existingEthnicity.name, true, true, 'code');
                // select value
                this.ethnicityInput.set('value', model.get('ethnicity'));
            }
        },
        initGender: function () {

            switch (this.get('model').get('gender')) {
            case 'MALE':
                this.maleInput.set('checked', 'checked');
                break;
            case 'FEMALE':
                this.femaleInput.set('checked', 'checked');
                break;
            case 'INDETERMINATE':
                this.indeterminateInput.set('checked', 'checked');
                break;
            case 'UNKNOWN':
                this.unknownInput.set('checked', 'checked');
                break;
            }
        },
        initDueDate: function () {
            var model = this.get('model');
            if (this.dueDateInput && this.getLifeState() === 'UNBORN') {
                this.dueDateInput.set('value', Y.USPDate.formatDateValue(model.get('dueDate'), null));
            }
        },
        _lookupExistingEntry: function (codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        }

    }, {

        ATTRS: {
            outcome: {
                value: 'BORN'
            },

        }
    });

    var AddPersonAddressView = Y.Base.create('AddPersonAddressView', Y.app.address.AddAddressForm, [], {
        template: Y.Handlebars.templates["personAddAddressDialog"],
        events: {
            '#locations li a': {
                click: 'changeLocation'
            },
            '#postcodeSearch-input': {
                keyup: 'chkEnableHouseSearch'
            },
            '#location_toggleAddLocation': {
                click: 'enableAddLocation'
            },
            '#confirmAddLocation': {
                click: 'submitAddLocation'
            },
            '#cancelAddLocation': {
                click: 'cancelAddLocation'
            },
            '#country-input': {
                change: 'handleCountryChanged'
            },
            '.input-group.input-prepend #fuzzyStartYEAR, #fuzzyStartMONTH, #fuzzyStartDAY': {
                focus: 'changeErrorHighlightToBlue'
            },
            '.error #fuzzyStartYEAR, .error #fuzzyStartMONTH, .error #fuzzyStartDAY': {
                blur: 'changeErrorHighlightToRed'
            },
            '#preSelectDateOptions li a': {
                click: 'preSelectFuzzyDate'
            },
            '#unknownLocationType-input': {
                change: 'hideLocationSelector'
            }
        },

        render: function () {
            Y.app.person.AddPersonAddressView.superclass.render.call(this);

            this.initUnknownLocation();

            return this;
        },

        setNarrative: function () {
            description = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')';
            this.getNarrative('summary').set('text', "You're adding an address for");
            this.getNarrative('description').set('text', description);
        },

        enableAddLocation: function (e) {

            var footer = Y.one('.AddPersonAddressView .yui3-widget-ft');
            var postcodeSearchData = '';
            var houseSearchData = '';
            // Force the country to UK for now since we only support UK postcodes
            var defaultCountry = 'GBR';

            e.preventDefault();
            footer.hide();

            this.resetValidationErrors();
            this.getWrapper('addLocation').show();
            this.getWrapper('location').hide();
            this.getWrapper('postcodeSearch').hide();
            this.getWrapper('houseSearch').hide();
            this.getWrapper('type').hide();
            this.getWrapper('usage').hide();
            this.getWrapper('startDate').hide();
            this.getWrapper('floorDescription').hide();
            this.getWrapper('roomDescription').hide();
            this.getWrapper('fuzzyStartDate').hide();
            this.getWrapper('noSearchResults').hide();
            this.getWrapper('unknownLocation').hide();
            this.getWrapper('doNotDisclose').hide();
            this.get('container').one('#address_search_note').hide();

            // get data from searchform
            postcodeSearchData = this.getInput('postcodeSearch').get('value');
            houseSearchData = this.getInput('houseSearch').get('value');

            this.get('container').one('#addPersonAddressForm').reset();
            this.getNarrative('summary').set('text', "You're adding a new location and an address for");
            this.get('container').one('#address_postcode').setAttribute('value', postcodeSearchData);
            this.get('container').one('#address_primaryNameOrNumber').setAttribute('value', houseSearchData);

            this.get('container').one('#country-input').set('value', defaultCountry);
            this.initCountrySubdivision(defaultCountry);
        },

        submitAddLocation: function (e) {

            var self = this;
            var dialog = this.get('panel');
            var selectionNode = Y.one('#locations');
            var locationHtml = '<li class="data-item" id="location{locationId}"><span>{location}</span><a href="#none" class="remove small">Change</a></li>';
            var footer = Y.one('.AddPersonAddressView .yui3-widget-ft');
            e.preventDefault();
            // show the mask
            dialog.showDialogMask();
            newLocationModel.set('countryCode', 'GBR', {
                silent: true
            }); // Not picking it up from disabled dropdown
            newLocationModel.url = this.get('locationUrl');
            newLocationModel.save(function (err, res) {

                dialog.hideDialogMask();

                if (err) {
                    self.fire('locationSave:error', {
                        error: err,
                        response: res
                    });
                } else {

                    self.setAttrs({
                        location: newLocationModel.toJSON(),
                        locationId: newLocationModel.get('id')
                    }, {
                        silent: true
                    });

                    // Make another call back to the server to get the newly created location. We'll used this to update the narrative. JSON parsing is
                    // best in try/catch logic since there is no easy way to detect if it's JSON. In chrome r.responseText is string of JSON object but in
                    // firefox it is string XML data
                    // TODO: Need to set the Accept globally for all IO read operations
                    Y.io(res.getResponseHeader('Location'), {

                        headers: {
                            'Accept': 'application/json',
                        },
                        on: {
                            success: function (tx, r) {
                                var parsedResponse = {};
                                try {
                                    parsedResponse = Y.JSON.parse(r.responseText);
                                    self.get('container').one('#locations ul').setHTML(L.sub(locationHtml, {

                                        location: E.html(parsedResponse.location)

                                    }));
                                } catch (e) {

                                    return;
                                }
                            }
                        }
                    });

                    self.getWrapper('location').show();
                    self.getWrapper('type').show();
                    self.getWrapper('usage').show();
                    self.getWrapper('startDate').show();
                    self.getWrapper('floorDescription').show();
                    self.getWrapper('roomDescription').show();
                    self.getWrapper('doNotDisclose').show();
                    self.initCounty();
                    self.initCountry();
                    self.getWrapper('fuzzyStartDate').show();
                    self.getNarrative('summary').set('text', "You're adding an address for");
                    self.getWrapper('addLocation').hide();
                    Y.one('#location_toggleAddLocation').hide();
                    Y.one('#unknownLocationType-input').set('value', '');
                    // Y.one('#unknownLocationType-input').set('disabled',
                    // true);
                    footer.show();
                    selectionNode.one('ul').all('li').some(function (o) {
                        o.remove(true);
                    });
                    self.resetValidationErrors();
                    self.successMsg({
                        msgDuration: 5000,
                        msg: self.get('labels').addPersonLocationSuccessMessage,
                        msgType: 'success'
                    });
                    self.getInputs('primaryNameOrNumber').set('value', '');
                    self.getInputs('street').set('value', '');
                    self.getInputs('locality').set('value', '');
                    self.getInputs('town').set('value', '');
                    self.getInputs('postcode').set('value', '');
                    selectionNode.show();

                    newLocationModel = new NewLocationForm();
                }

            });

        },

        cancelAddLocation: function (e) {
            e.preventDefault();
            var footer = Y.one('.AddPersonAddressView .yui3-widget-ft');
            footer.show();
            this.getWrapper('addLocation').hide();
            this.getNarrative('summary').set('text', "You're adding an address for");

            this.getWrapper('location').show();
            this.getInput('postcodeSearch').set('value', '');
            this.getWrapper('postcodeSearch').show();
            this.getInput('houseSearch').set('value', '');
            this.getInput('houseSearch').set('disabled', true);
            this.getWrapper('houseSearch').show();
            this.getWrapper('type').show();
            this.getWrapper('usage').show();
            this.getWrapper('floorDescription').show();
            this.getWrapper('roomDescription').show();
            this.getWrapper('startDate').show();
            this.getWrapper('fuzzyStartDate').show();
            this.getWrapper('unknownLocation').show();
            this.getWrapper('doNotDisclose').show();
            this.get('container').one('#address_search_note').show();
            this.resetValidationErrors();
        },

        initUnknownLocation: function () {
            const unknownLocations = Y.Object.values(Y.uspCategory.address.unknownLocation.category.getActiveCodedEntries());
            Y.FUtil.setSelectOptions(this.getInput('unknownLocationType'), unknownLocations, null, null, true, true, 'code');
        },
        _setAddressType: function (type) {
            Y.one('#type-input').set('value', type);
        },
        _setAddressUsage: function (usage) {
            Y.one('#usage-input').set('value', usage);
        },
        hideLocationSelector: function (e) {
            this.get('container').one('#address_search_note').hide();
            this.getWrapper('addLocation').hide();
            this.getWrapper('location').hide();
            this.getWrapper('postcodeSearch').hide();
            this.getWrapper('houseSearch').hide();
            if (e.currentTarget.get('value') === LOCATION_TYPE_HOMELESS) {
                this._setAddressType(ADDRESS_TYPE_HOME);
                this._setAddressUsage(ADDRESS_USAGE_TEMPORARY);
            }
            this.setAttrs({
                location: null,
                locationId: null
            }, {
                silent: true
            });
        }
    }, {

        ATTRS: {
            messageAddLocationSuccess: {},
            templateAddLocationMsg: {
                value: '<div class="pure-alert pure-alert-block pure-alert-{msgType}"><a href="#" class="close">Close</a><h4 tabindex="0">{msgAddLocationSuccess}</h4></div>'
            },
            endPersonAddressDetails: {
                addressType: {},
                addressUsage: {},
                endReason: {},
                endDate: {},
                endDateSFD: {},
                addressId: {},
                objectVersion: {}
            },
        },
        panel: {
            value: ''
        }

    });

    // This model is subtly different to the EndPersonAddressView as it will not support update of the end date unless the address is already ended.
    var UpdateStartEndPersonAddressView = Y.Base.create('UpdateStartEndPersonAddressView', Y.View, [], {

        template: Y.Handlebars.templates["updatePersonAddressDates"],
        initializer: function () {
            var model = this.get('model');
            model.after('change', this.render, this);
            model.after('destroy', this.destroy, this);
        },
        events: {
            '#preSelectEndDateOptions li a': {
                click: 'preSelectEndFuzzyDate'
            },
            '#preSelectStartDateOptions li a': {
                click: 'preSelectStartFuzzyDate'
            }
        },
        render: function () {
            var container = this.get('container'),
                description = '',
                template = this.template,
                model = this.get('model'),
                labels = this.get('labels'),
                endDate = this.get('model').get('endDate');

            container.setHTML(template({
                labels: labels,
                otherData: this.get('otherData'),
                modelData: model.toJSON(),
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                targetPersonId: this.get('targetPersonId'),
                targetPersonName: this.get('targetPersonName'),
                codedEntries: this.get('codedEntries') || {}

            }));

            UpdateStartEndPersonAddressView.superclass.render.call(this);

            // This ensures the end date widget is not initialized when there is no existing end date on the address.
            if (endDate && endDate.calculatedDate) {
                // render the fuzzy date widget
                this.fuzzyEndDate = new Y.usp.SimpleFuzzyDate({
                    id: 'fuzzyEnd',
                    contentNode: this.getInput('fuzzyEndDate'),
                    yearInput: true,
                    monthInput: true,
                    dayInput: true,
                    showDateLabel: false,
                    calendarPopup: true,
                    cssPrefix: 'pure',
                    minYear: 1886,
                    maxYear: 9999,
                    labels: this.get('fuzzyDateLabels'),
                    dateValue: endDate
                });

                this.fuzzyEndDate.render();
            }

            this.fuzzyStartDate = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyStart',
                contentNode: this.getInput('fuzzyStartDate'),
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: 9999,
                labels: this.get('fuzzyDateLabels'),
                dateValue: this.get('model').get('startDate')
            });

            this.fuzzyStartDate.render();

            // set initial narrative
            description = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')',
                summary = labels.updatePersonAddressDatesNarrative;
            this.getNarrative('summary').set('text', summary);
            this.getNarrative('description').set('text', description);

            return this;
        },

        getInput: function (id) {
            return this.get('container').one('#' + id);
        },

        getWrapper: function (id) {
            return this.get('container').one('#' + id + '-wrapper');
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        },

        getEndDate: function () {
            if (this.fuzzyEndDate) {
                return this.fuzzyEndDate.getFuzzyDate();
            }
        },

        getStartDate: function () {
            return this.fuzzyStartDate.getFuzzyDate();
        },

        preSelectStartFuzzyDate: function (e) {
            e.preventDefault();
            var t = e.currentTarget,
                today = new Date();
            if (t.hasClass('currentDate')) {
                this.fuzzyStartDate.setDateAndMask(today, 'DAY');
            }
        },

        preSelectEndFuzzyDate: function (e) {
            e.preventDefault();
            var t = e.currentTarget,
                today = new Date();
            if (t.hasClass('currentDate')) {
                this.fuzzyEndDate.setDateAndMask(today, 'DAY');
            }
        },

        destructor: function () {
            if (this.fuzzyEndDate) {
                this.fuzzyEndDate.destroy();
                this.fuzzyEndDate = null;
            }
            this.fuzzyStartDate.destroy();
            this.fuzzyStartDate = null;
        }

    }, {

        ATTRS: {
            targetPersonId: {},
            targetPersonName: {},
            sourceViewName: {},
            codedEntries: {
                value: {
                    unknownLocations: Y.uspCategory.address.unknownLocation.category.codedEntries,
                    endReason: Y.uspCategory.address.endReason.category.codedEntries,
                    usage: Y.uspCategory.address.usage.category.codedEntries,
                    type: Y.uspCategory.address.type.category.codedEntries
                }
            }
        }
    });

    var EndPersonAddressView = Y.Base.create('EndPersonAddressView', Y.View, [], {

        events: {
            // TODO need to put it in common place for all fuzzy dates
            '#preSelectDateOptions li a': {
                click: 'preSelectFuzzyDate'
            }
        },

        template: Y.Handlebars.templates["closePersonAddress"],
        initializer: function () {
            var model = this.get('model');

            // Re-render this view when the model changes, and destroy this view when the model is destroyed.
            model.after('change', this.render, this);
            model.after('destroy', this.destroy, this);
        },
        render: function () {
            var container = this.get('container');
            var description = '';
            // var template =
            // Y.Handlebars.compile(Y.one('#endPersonAddressView').getHTML());
            var template = this.template;
            var model = this.get('model'),
                labels = this.get('labels');
            container.setHTML(template({
                labels: labels,
                otherData: this.get('otherData'),
                modelData: model.toJSON(),
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                targetPersonId: this.get('targetPersonId'),
                targetPersonName: this.get('targetPersonName'),
                codedEntries: this.get('codedEntries') || {}

            }));

            // get our coded entries
            this.initEndReasons(model.get('endReason'));

            EndPersonAddressView.superclass.render.call(this);

            // render the fuzzy date widget
            this.fuzzyEndDate = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyEnd',
                contentNode: this.getInput('fuzzyEndDate'),
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: 9999,
                labels: this.get('fuzzyDateLabels'),
                dateValue: this.get('model').get('endDate')
            });

            this.fuzzyEndDate.render();

            var usageDesc = '';
            var usages = Y.Object.values(Y.uspCategory.address.usage.category.getActiveCodedEntries());
            Y.Object.each(usages, function (entry, key) {
                if (entry.code === model.get('usage')) {
                    usageDesc = entry.name;
                }
            });
            this.getInput('usage-dd').setHTML(usageDesc);
            var typeDesc = '';
            var types = Y.Object.values(Y.uspCategory.address.type.category.getActiveCodedEntries());
            Y.Object.each(types, function (entry, key) {
                if (entry.code === model.get('type')) {
                    typeDesc = entry.name;
                }
            });
            this.getInput('type-dd').setHTML(typeDesc);

            // set initial narrative
            description = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')';
            this.getNarrative('summary').set('text', "You're ending an address for");
            this.getNarrative('description').set('text', description);

            return this;
        },

        getInput: function (id) {
            return this.get('container').one('#' + id);
        },

        getWrapper: function (id) {
            return this.get('container').one('#' + id + '-wrapper');
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        },

        initEndReasons: function (val) {
            var reasons,
                input = this.getInput('endReason-input');

            reasons = Y.Object.values(Y.uspCategory.address.endReason.category.getActiveCodedEntries());
            Y.FUtil.setSelectOptions(input, reasons, null, null, true, true, 'code');

            // perform selection of the existing value
            input.set('value', val || '');
        },

        getEndDate: function () {
            return this.fuzzyEndDate.getFuzzyDate();
        },

        preSelectFuzzyDate: function (e) {
            e.preventDefault();
            var t = e.currentTarget,
                today = new Date();
            if (t.hasClass('currentDate')) {
                this.fuzzyEndDate.setDateAndMask(today, 'DAY');
            }
        },

        destructor: function () {
            this.fuzzyEndDate.destroy();
            this.fuzzyEndDate = null;
        }

    }, {

        ATTRS: {
            targetPersonId: {},
            targetPersonName: {},
            sourceViewName: {},
            codedEntries: {
                value: {
                    unknownLocations: Y.uspCategory.address.unknownLocation.category.codedEntries
                }
            }
        }
    });

    var UpdatePersonLocationView = Y.Base.create('UpdatePersonLocationView', Y.View, [], {
        template: Y.Handlebars.templates["personUpdateLocationDialog"],

        events: {
            '#location_toggleAddLocation': {
                click: 'showLocationAddDialog'
            }
        },

        initializer: function () {
            var model = this.get('model'),
                locationConfig = this.get('locationConfig');

            model.after('change', this.render, this);
            model.after('destroy', this.destroy, this);

            this.placementAddressView = new Y.app.childlookedafter.PlacementAddressAddView({
                labels: this.get('labels'),
                locationUrl: this.get('urls').locationUrl,
                locationQueryParamsUrl: this.get('urls').locationQueryParamsUrl
            }).addTarget(this);

            this.locationDialog = new Y.app.location.LocationDialog(locationConfig).addTarget(this).render();

            this._evtHandlers = [
                this.after('*:close:locationView', this.handleAfterLocationAdd, this)
            ];

        },
        render: function () {

            var container = this.get('container'),
                description = '',
                template = this.template,
                model = this.get('model'),
                labels = this.get('labels');

            container.setHTML(template({
                labels: labels,
                otherData: this.get('otherData'),
                modelData: model.toJSON(),
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                targetPersonId: this.get('targetPersonId'),
                targetPersonName: this.get('targetPersonName'),
                codedEntries: this.get('codedEntries') || {}

            }));

            container.one('#addAddressWrapper').append(this.placementAddressView.render().get('container'));

            // set initial narrative
            description = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')',
                summary = labels.updatePersonAddressLocationNarrative;
            this.getNarrative('summary').set('text', summary);
            this.getNarrative('description').set('text', description);

            return this;
        },

        handleSetNewLocation: function (locationId) {
            this.placementAddressView.handleSetNewLocation(locationId);
        },

        handleAfterLocationAdd: function (e) {
            var eventRoot = e.eventRoot,
                locationId = e.locationId,
                panel = this.get('panel');

            // Show dialog where location dialog was launched
            panel.show();
            if (locationId) {
                panel.get('activeView').handleSetNewLocation(locationId);
            }
        },

        showLocationAddDialog: function (e) {
            e.preventDefault();
            var panel = this.get('panel');
            panel.hide();

            var url = this.get('urls').locationUrl;
            this.locationDialog.showView('add', {
                model: new Y.app.location.NewLocationForm({
                    url: url
                }),
                otherData: {
                    eventRoot: panel
                }
            }, function () {
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                this.removeButton('closeButton', Y.WidgetStdMod.HEADER);
            });
        },

        setupFieldVisibility: function () {
            // get the field visibility
            var fieldVisibility = this.getFieldVisibility();

            this.setPlacementAddressInputFieldsVisibility(fieldVisibility.placementAddressFields);

            // return the visibility object as may be useful to calling code
            return fieldVisibility;
        },

        getFieldVisibility: function () {

            var fieldVisibility = {
                placementAddressFields: false
            };
            return fieldVisibility;
        },

        setPlacementAddressInputFieldsVisibility: function (visible) {
            this.setFieldVisibility('#addAddressWrapper', null, visible);
        },

        setFieldVisibility: function (wrapper, field, visible) {
            var container = this.get('container');
            var wrapperNode = wrapper ? container.one(wrapper) : null;

            if (field) {
                var fieldVis = function (fld) {
                    var fieldNodes = fld ? container.all(fld) : null;
                    if (fieldNodes) {
                        if (visible) {
                            fieldNodes.removeAttribute('disabled');
                        } else {
                            fieldNodes.setAttribute('disabled', 'disabled');
                        }
                    }
                };
                if (Array.isArray(field)) {
                    field.forEach(fieldVis);
                } else {
                    fieldVis(field);
                }
            }

            if (wrapperNode) {
                if (visible) {
                    wrapperNode.setStyle('display', '');
                } else {
                    wrapperNode.setStyle('display', 'none');
                }
            }
        },

        getInput: function (id) {
            return this.get('container').one('#' + id);
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        }
    }, {

        ATTRS: {
            targetPersonId: {},
            targetPersonName: {},
            sourceViewName: {},
            codedEntries: {
                value: {
                    unknownLocations: Y.uspCategory.address.unknownLocation.category.codedEntries,
                    endReason: Y.uspCategory.address.endReason.category.codedEntries,
                    usage: Y.uspCategory.address.usage.category.codedEntries,
                    type: Y.uspCategory.address.type.category.codedEntries
                }
            },
            messageAddLocationSuccess: {},
            templateAddLocationMsg: {
                value: '<div class="pure-alert pure-alert-block pure-alert-{msgType}"><a href="#" class="close">Close</a><h4 tabindex="0">{msgAddLocationSuccess}</h4></div>'
            },
            endPersonAddressDetails: {
                addressType: {},
                addressUsage: {},
                endReason: {},
                endDate: {},
                endDateSFD: {},
                addressId: {},
                objectVersion: {}
            },
            panel: {
                value: ''
            }
        }
    });

    var ReopenPersonAddressView = Y.Base.create('ReopenPersonAddressView', Y.View, [], {

        template: Y.Handlebars.templates["reopenPersonAddress"],
        initializer: function () {
            var model = this.get('model');

            model.after('change', this.render, this);
            model.after('destroy', this.destroy, this);
        },
        render: function () {
            var container = this.get('container'),
                description = '',
                template = this.template,
                model = this.get('model'),
                labels = this.get('labels');

            container.setHTML(template({
                labels: labels,
                otherData: this.get('otherData'),
                modelData: model.toJSON(),
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                targetPersonId: this.get('targetPersonId'),
                targetPersonName: this.get('targetPersonName'),
                codedEntries: this.get('codedEntries') || {}

            }));

            ReopenPersonAddressView.superclass.render.call(this);

            // set initial narrative
            description = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')',
                summary = labels.reopenPersonAddressNarrative;
            this.getNarrative('summary').set('text', summary);
            this.getNarrative('description').set('text', description);

            return this;
        },

        getInput: function (id) {
            return this.get('container').one('#' + id);
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        }
    }, {

        ATTRS: {
            targetPersonId: {},
            targetPersonName: {},
            sourceViewName: {},
            codedEntries: {
                value: {
                    unknownLocations: Y.uspCategory.address.unknownLocation.category.codedEntries,
                    endReason: Y.uspCategory.address.endReason.category.codedEntries,
                    usage: Y.uspCategory.address.usage.category.codedEntries,
                    type: Y.uspCategory.address.type.category.codedEntries
                }
            }
        }
    });

    var RemovePersonAddressView = Y.Base.create('RemovePersonAddressView', Y.View, [], {

        template: Y.Handlebars.templates["removePersonAddress"],
        initializer: function () {
            var model = this.get('model');

            // Re-render this view when the model changes, and destroy this view
            // when
            // the model is destroyed.
            model.after('change', this.render, this);
            model.after('destroy', this.destroy, this);
        },
        render: function () {
            var container = this.get('container');
            var description = '';
            var template = this.template;
            var model = this.get('model'),
                labels = this.get('labels');
            container.setHTML(template({
                labels: labels,
                otherData: this.get('otherData'),
                addressLocationDescription: addressFormatters.getAddressLocationDescription(model, labels),
                modelData: model.toJSON(),
                targetPersonId: this.get('targetPersonId'),
                targetPersonName: this.get('targetPersonName')

            }));

            var usageDesc = '';
            var usages = Y.Object.values(Y.uspCategory.address.usage.category.getActiveCodedEntries());
            Y.Object.each(usages, function (entry, key) {
                if (entry.code === model.get('usage')) {
                    usageDesc = entry.name;
                }
            });
            this.getInput('usage-dd').setHTML(usageDesc);

            var typeDesc = '';
            var types = Y.Object.values(Y.uspCategory.address.type.category.getActiveCodedEntries());
            Y.Object.each(types, function (entry, key) {
                if (entry.code === model.get('type')) {
                    typeDesc = entry.name;
                }
            });
            this.getInput('type-dd').setHTML(typeDesc);

            if (model.get('endDate')) {
                var endReasonDesc = '';
                var reasons = Y.Object.values(Y.uspCategory.address.endReason.category.getActiveCodedEntries());
                Y.Object.each(reasons, function (entry, key) {
                    if (entry.code === model.get('endReason')) {
                        endReasonDesc = entry.name;
                    }
                });
                this.getInput('endReason-dd').setHTML(endReasonDesc);
            }

            // set initial narrative
            description = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')';
            this.getNarrative('summary').set('text', "You're removing an address for");
            this.getNarrative('description').set('text', description);

            return this;
        },

        getInput: function (id) {
            return this.get('container').one('#' + id);
        },

        getNarrative: function (section) {
            var node = '';
            switch (section) {
            case 'summary':
                node = this.get('container').one('#narrative .narrative-summary');
                break;
            case 'description':
                node = this.get('container').one('#narrative .narrative-description');
                break;
            }
            return node;
        }

    }, {

        ATTRS: {
            targetPersonId: {},
            targetPersonName: {}

        }

    });

    // Add PersonReferenceNumber View
    var NewPersonReferenceNumberView = Y.Base.create('NewPersonReferenceNumberView', Y.usp.person.NewPersonReferenceNumberView, [], {
        // bind template
        events: {
            '#type-input': {
                change: 'changeMaxlength'
            }
        },
        template: Y.Handlebars.templates["personReferenceNumberAddDialog"],

        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    targetPersonId: this.get('targetPersonId'),
                    targetPersonName: this.get('targetPersonName')
                });

            // Render this view's HTML into the container element.
            container.setHTML(html);
            this.initReferenceTypes();
            return this;
        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#' + inputId + '-input');
        },

        initReferenceTypes: function () {
            var activeReferenceType = Y.secured.uspCategory.person.referencetype.category.getActiveCodedEntries(null, 'write'),
                i, j;
            // Add the code as a property in the coded entry, used to set the option value
            Y.Object.each(activeReferenceType, function (referenceNumberType, key) {
                referenceNumberType.code = key;
            });
            // Array of coded entries to pass to Y.FUtil.setSelectOptions
            var activeReferenceTypeArray = Y.Object.values(activeReferenceType);

            // Match associated reference numbers with active reference types and filter types array.
            for (j = 0; j < this.get('activeReferenceNumbers').length; j++) {
                for (i = 0; i < activeReferenceTypeArray.length; i++) {
                    if (this.get('activeReferenceNumbers')[j] === activeReferenceTypeArray[i].code) {
                        activeReferenceTypeArray.splice(i, 1);
                    }
                }
            }

            Y.FUtil.setSelectOptions(this.getInput('type'), activeReferenceTypeArray, null, null, true, true, 'code');
        },
        changeMaxlength: function (e) {
            // Change max length attribute of input field '#referenceNumber-input' depending on the selection made UPN=14,
            // TUPN=13 and the rest is 30 characters
            var container = this.get('container');
            var refValue = container.one('#type-input').get('value');
            if (refValue === 'UPN') {
                container.one('#referenceNumber-input').set('maxLength', '14')
            }
            if (refValue === 'TUPN') {
                container.one('#referenceNumber-input').set('maxLength', '13')
            }
            if (refValue !== 'TUPN' && refValue !== 'UPN') {
                container.one('#referenceNumber-input').set('maxLength', '30')
            }
        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            targetPersonId: {},
            targetPersonName: {},
            activeReferenceNumbers: {}
        }
    });

    // Update languages view
    var UpdatePersonLanguageView = Y.Base.create('UpdatePersonLanguageView', Y.usp.person.UpdatePersonLanguageView, [], {
        events: {
            '#firstLanguageList li a': {
                click: 'changeFirstLanguage'
            },
            '#otherLanguageList li a': {
                click: 'removeOtherLanguage'
            }
        },
        // bind template
        template: Y.Handlebars.templates["personLanguageDialog"],

        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    targetPersonId: this.get('targetPersonId'),
                    targetPersonName: this.get('targetPersonName'),
                    codedEntries: this.get('codedEntries')
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            this.initProficiency();
            this.initLanguageAutoComplete();
            this.initInterpreter();
            this.initFirstLanguage();
            this.initSecondLanguages();
            this.initNonVerbalCommunication();
        },

        initFirstLanguage: function () {
            var vos = this.get('model').get('personLanguageAssignmentVOs');
            if (vos.length) {
                for (var i = 0, len = vos.length; i < len; i++) {
                    if (vos[i].primaryLanguage) {
                        this.setFirstLanguage(vos[i].codedEntryVO.id, this._lookupExistingEntryById(Y.uspCategory.person.languages.category.codedEntries, vos[i].codedEntryVO.id).name);
                        break;
                    };
                }

            }
        },
        initSecondLanguages: function () {
            var vos = this.get('model').get('personLanguageAssignmentVOs');
            if (vos.length) {
                for (var i = 0, len = vos.length; i < len; i++) {
                    if (!vos[i].primaryLanguage) {
                        this.setOtherLanguage(vos[i].codedEntryVO.id, this._lookupExistingEntryById(Y.uspCategory.person.languages.category.codedEntries, vos[i].codedEntryVO.id).name);
                    };
                }

            }
        },
        initNonVerbalCommunication: function () {
            var existingNonVerbalLanguages = (this.get('model').get('personNonVerbalLanguageAssignmentVOs') || []).map(function (language) {
                    return (language.codedEntryVO.id);
                }),
                nonVerbalLanguagesSelect = this.get('container').one('#nonVerbalLanguages-input'),
                options = Y.Object.values(Y.uspCategory.person.nonVerbalLanguages.category.getActiveCodedEntries());
            Y.FUtil.setSelectOptions(nonVerbalLanguagesSelect, options, null, null, true, true, 'id');

            // Set the option values to selected if already present for a person
            nonVerbalLanguagesSelect.get('options').each(function () {
                if (existingNonVerbalLanguages.indexOf(parseInt(this.get('value'))) != -1) {
                    this.set('selected', true);
                }
            });
        },
        initInterpreter: function () {
            if (this.get('model').get('interpreterRequired')) {
                this.get('container').one('#interpreterRequired-input').setAttribute('checked', 'checked');
            }
        },
        initProficiency: function () {
            var proficiency = this.get('model').get('proficiencyInOfficialLanguage');
            var proficiencyEntry = this._lookupExistingEntry(Y.uspCategory.person.proficiencyInOfficialLanguage.category.codedEntries, proficiency);
            Y.FUtil.setSelectOptions(this.get('container').one('#proficiencyInOfficialLanguage-input'), O.values(Y.uspCategory.person.proficiencyInOfficialLanguage.category.getActiveCodedEntries()), proficiencyEntry.code, proficiencyEntry.name, true, true, 'code');
            if (proficiency) {
                this.get('container').one('#proficiencyInOfficialLanguage-input').set('value', proficiency);
            }
        },
        initLanguageAutoComplete: function () {
            var langCodes = Y.uspCategory.person.languages.category.getActiveCodedEntries();
            var langArray = Object.keys(langCodes).map(function (k) {
                return langCodes[k].name;
            });

            function languageFilter(query, results) {
                query = query.toLowerCase();
                return Y.Array.filter(results, function (result) {
                    // Allow for search beyond comma, eg 'Bengali, Bangla'
                    // or bracket, eg Persian (Farsi)
                    var arr = result.text.toLowerCase().split(',');
                    if (arr.length < 2) {
                        arr = result.text.toLowerCase().split('(');
                    }
                    if (arr.length > 1) {
                        return arr[0].trim().substr(0, query.length) == query || arr[1].trim().substr(0, query.length) == query;
                    } else {
                        return result.text.toLowerCase().substr(0, query.length) == query;
                    }
                });
            }
            Y.one(this.get('container').one('#firstLanguage-input')).plug(Y.Plugin.usp.PopupAutoCompletePlugin, {
                width: '321px', // This is set again in personDialog.jsp
                allowBrowserAutocomplete: false,
                maxResults: 10,
                minQueryLength: 1,
                resultHighlighter: 'phraseMatch',
                resultFilters: languageFilter,
                source: langArray
            });
            var firstLanguageSearch = this.getInput('firstLanguage');
            var otherLanguageSearch = this.getInput('otherLanguage');
            // add css
            firstLanguageSearch.ancestor('div').addClass("yui3-skin-sam");
            otherLanguageSearch.ancestor('div').addClass("yui3-skin-sam");

            Y.one(this.get('container').one('#otherLanguage-input')).plug(Y.Plugin.usp.PopupAutoCompletePlugin, {
                width: '321px', // This is set again in personDialog.jsp
                allowBrowserAutocomplete: false,
                maxResults: 10,
                minQueryLength: 1,
                resultHighlighter: 'phraseMatch',
                resultFilters: languageFilter,
                source: langArray
            });

            firstLanguageSearch.ac.on('select', function (e) {
                var langCode = getLangCode(e.result.text);
                this.setFirstLanguage(langCode, e.result.text);
            }, this);

            otherLanguageSearch.ac.on('select', function (e) {
                var langCode = getLangCode(e.result.text);
                this.setOtherLanguage(langCode, e.result.text);
            }, this);
            this.getInput('otherLanguage').ac.after('select', function (e) {
                otherLanguageSearch.set('value', '');
            }, this);

            function getLangCode(text) {
                for (var prop in langCodes) {
                    if (langCodes.hasOwnProperty(prop)) {
                        if (langCodes[prop].name === text)
                            return langCodes[prop].id;
                    }
                }
            }
        },
        setFirstLanguage: function (langCode, text) {
            var firstLanguageSearch = this.getInput('firstLanguage');
            var firstLanguageItemTemplate = '<li class="data-item" data-text="' + langCode + '"><span>' + text + '</span><a href="#none" class="remove small">Change</a></li>';
            var firstLanguageList = this.get('container').one('#firstLanguageList');
            firstLanguageSearch.hide();
            firstLanguageList.one('ul').append(firstLanguageItemTemplate);
            this.updateLangArray();
            this.manageLanguageFields(langCode);
        },
        setOtherLanguage: function (langCode, text) {
            var otherLanguageItemTemplate = '<li class="data-item usp-fx-all" data-text="' + langCode + '"><span>' + text + '</span><a href="#none" class="remove small">Remove</a></li>';
            var otherLanguageList = this.get('container').one('#otherLanguageList');
            otherLanguageList.one('ul').append(otherLanguageItemTemplate);
            this.updateLangArray();
            this.toggleOtherLanguage();
        },
        toggleOtherLanguage: function () {
            if (this.getSelectedOtherLanguageNames().length > 4) {
                var infoTemplate = '<p class="estimatedInfo">' + this.get('labels').languageMaxMessage + '</p>';
                this.get('container').one('#languageLimitInfo').addClass('pure-info');
                this.get('container').one('#languageLimitInfo').append(infoTemplate);
                this.getInput('otherLanguage').set('disabled', 'disabled');
                this.getInput('otherLanguage').setAttribute('readonly', 'readonly');
            } else {
                this.getInput('otherLanguage').set('disabled', '');
                this.getInput('otherLanguage').removeAttribute('readonly');
                this.get('container').one('#languageLimitInfo').removeClass('pure-info');
                this.get('container').one('#languageLimitInfo').empty();
            }
        },
        manageLanguageFields: function (code) {
            if (code === this.get('localLanguageCode')) {
                if (this.get('container').one('#language_proficiencyInOfficialLanguage_label span')) {
                    this.get('container').one('#language_proficiencyInOfficialLanguage_label span').remove();
                }
                this.getInput('interpreterRequired').set('checked', false);
                this.getInput('interpreterRequired').set('disabled', 'disabled');
            } else {
                if (!this.get('container').one('#language_proficiencyInOfficialLanguage_label span')) {
                    this.get('container').one('#language_proficiencyInOfficialLanguage_label').append('<span class="mandatory">*</span>');
                }
                this.getInput('interpreterRequired').set('disabled', '');
            }
        },
        // Check which languages have already been selected and remove them from the search results
        updateLangArray: function () {
            var selectedLangArray = [];
            selectedLangArray = this.getSelectedLanguageNames();
            var langCodes = Y.uspCategory.person.languages.category.getActiveCodedEntries();
            var langArray = Object.keys(langCodes).map(function (k) {
                return langCodes[k].name;
            });
            for (var i = langArray.length - 1; i > -1; i--) {
                var pos = selectedLangArray.indexOf(langArray[i]);
                if (pos > -1) {
                    langArray.splice(i, 1);
                }
            }
            this.getInput('firstLanguage').ac.set('source', langArray);
            this.getInput('otherLanguage').ac.set('source', langArray);
        },
        // Return an array of all selected language names
        getSelectedLanguageNames: function () {
            return this.getSelectedFirstLanguageName().concat(this.getSelectedOtherLanguageNames());
        },
        // Return selected first language name
        getSelectedFirstLanguageName: function () {
            if (this.get('container').one('#firstLanguageList li span')) {
                return this.get('container').one('#firstLanguageList li span').get('text');
            } else {
                return [];
            }
        },
        // Return selected first language code
        getSelectedFirstLanguageCode: function () {
            if (this.get('container').one('#firstLanguageList li')) {
                return this.get('container').one('#firstLanguageList li').getAttribute('data-text');
            } else {
                return [];
            }
        },
        // return array of selected other language names
        getSelectedOtherLanguageNames: function () {
            if (this.get('container').all('#otherLanguageList li span')) {
                return this.get('container').all('#otherLanguageList li span').get('text');
            } else {
                return [];
            }
        },
        // Return array of selected other language codes
        getSelectedOtherLanguageCodes: function () {
            if (this.get('container').one('#otherLanguageList li')) {
                return this.get('container').all('#otherLanguageList li').getAttribute('data-text');
            } else {
                return [];
            }
        },
        getSelectedNonVerbalLanguageIds: function () {
            var result = [];
            this.get('container').one('#nonVerbalLanguages-input').get('options').each(function () {
                if (this.get('selected') && this.get('value')) {
                    result.push(this.get('value'));
                }
            });
            return result;
        },
        changeFirstLanguage: function (e) {
            e.preventDefault();
            e.currentTarget.ancestor('li').remove(true);
            this.getInput('firstLanguage').set('value', '');
            this.getInput('firstLanguage').show();
            this.updateLangArray();
            this.manageLanguageFields();
        },

        removeOtherLanguage: function (e) {
            e.preventDefault();
            e.currentTarget.ancestor('li').remove(true);
            this.updateLangArray();
            this.toggleOtherLanguage();
        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#' + inputId + '-input');
        },
        getLabel: function (inputId) {
            var container = this.get('container');
            return container.one('#language_' + inputId + '_label');
        },
        _lookupExistingEntry: function (codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        },
        _lookupExistingEntryById: function (codes, id) {
            var entry = {
                code: null,
                name: null
            };
            if (!(typeof id === 'undefined' || id === '' || id === null)) {
                for (var key in codes) {
                    if (codes[key].id === id) {
                        entry.code = codes[key].code;
                        entry.name = codes[key].name;
                        break;
                    };
                }
            }
            return entry;
        }

    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            targetPersonId: {},
            targetPersonName: {},
            localLanguageCode: {},
            codedEntries: {
                value: {
                    nonVerbalLanguages: Y.uspCategory.person.nonVerbalLanguages.category.codedEntries
                }
            }
        }
    });

    // Edit/Remove PersonReferenceNumber View
    var EditRemovePersonReferenceNumberView = Y.Base.create('EditRemovePersonReferenceNumberView', Y.usp.person.PersonReferenceNumberView, [], {
        events: {
            'a.removeLink': {
                click: '_removeReferenceNumber'
            },
            'input': {
                focus: 'changeMaxlengthOnReferenceEdit'
            }
        },

        // bind template
        template: Y.Handlebars.templates["personReferenceNumberEditRemoveDialog"],
        render: function () {
            // EditRemovePersonReferenceNumberView.superclass.render.call(this);
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    targetPersonId: this.get('targetPersonId'),
                    targetPersonName: this.get('targetPersonName'),
                    codedEntries: this.get('codedEntries'),
                    canRemoveReferenceNumber: this.get('canRemoveReferenceNumber') ? '' : 'none;'
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            return this;

        },

        _removeReferenceNumber: function (e) {
            e.preventDefault();
            e.target.setStyle("visibility", "hidden");
            this.get('container').one('#referenceNumberLabel_' + e.target.getAttribute('data-remove-id')).addClass('disabled');
            var referenceNumberInput = this.get('container').one('#referenceNumber_' + e.target.getAttribute('data-remove-id'));
            referenceNumberInput.setAttribute("data-remove-num", true);
            referenceNumberInput.set("value", "");
            referenceNumberInput.set("disabled", true);
        },

        changeMaxlengthOnReferenceEdit: function (e) {
            // Change max length attribute of input field with class 'type-TUPN' or 'type-UPN'

            var cList = e.target.get('classList');
            lastClass = Y.Object.values(cList)[0][Y.Object.values(cList)[0].length - 1];
            if (lastClass === 'type-UPN') {
                e.target.set('maxLength', '14')
            };
            if (lastClass === 'type-TUPN') {
                e.target.set('maxLength', '13')
            };

        },

        // Specify attributes and static properties for your View here.
        ATTRS: {}
    });

    // Edit/Remove PersonContact View
    var EditRemoveContactView = Y.Base.create('EditRemoveContactView', Y.app.contact.EditSubjectContactView, [], {

        initializer: function () {
            this.contactVerifiedDateCalendars = [];
            this.on('*:load', this.render(), this);
        },
    });

    var CombinedEndAddPersonAddressResults = Y.Base.create('CombinedEndAddPersonAddressResults', Y.usp.app.Results, [], {

        updateCloseAddPersonAddressArray: [],

        // calls the _handleRowClick of superclass
        events: {
            '.pure-table-data tr input[name="residentCheckbox"]': {
                click: '_rowClick'
            }
        },

        getResultsListModel: function (config) {
            if (this.updateCloseAddPersonAddressArray && this.updateCloseAddPersonAddressArray.length > 0) {
                this.updateCloseAddPersonAddressArray = [];
            }
            if (config.sourceView === "moveResidentPersonAddressLocation") {
                var updateCloseAddPersonAddress,
                    preSelectedArray = [];

                config.preSelectedRecords.forEach(function (record) {
                    updateCloseAddPersonAddress = {
                        "id": record.id,
                        "objectVersion": record.objectVersion,
                        "startDateSDF": record.startDate,
                        "personName": record.personName,
                        "startDate": record.startDate.calculatedDate,
                        "location": record.location
                    };
                    preSelectedArray.push(updateCloseAddPersonAddress);

                });
                this.updateCloseAddPersonAddressArray = preSelectedArray;
                this.set('residentDetails', this.updateCloseAddPersonAddressArray);
                return new Y.usp.relationship.PaginatedPersonAddressRelationshipNameList({
                    url: config.residentListUrl
                });
            }
            return new Y.usp.relationship.PersonAddressRelationshipNameList({
                url: config.residentListUrl
            });
        },
        getColumnConfiguration: function (config) {
            var labels = config.labels;
            if (config.sourceView === "moveResidentPersonAddressLocation") {
                return ([{
                        key: 'personName',
                        label: labels.personName,
                        width: '11%',
                        sortable: false
                    }, {
                        key: 'relationshipName',
                        label: labels.relationshipName,
                        width: '25%'
                    },
                    {
                        label: labels.update,
                        className: 'pure-checkbox',
                        allowHTML: true,
                        width: '9%',
                        formatter: checkboxFormatter
                    }
                ]);

            }

            return ([{
                    key: 'personName',
                    label: labels.personName,
                    width: '11%',
                    sortable: false
                }, {
                    key: 'relationshipName',
                    label: labels.relationshipName,
                    width: '25%'
                },
                {
                    label: labels.update,
                    className: 'pure-checkbox',
                    allowHTML: true,
                    width: '9%',
                    formatter: function () {
                        var html = '<div><input type="checkbox" name="residentCheckbox"/></div>';
                        return html;
                    }
                }
            ]);
        },

        destructor: function () {
            if (this.get('residentDetails')) {
                this.get('residentDetails').destroy();
                delete this.get('residentDetails');
            }
        },

        _rowClick: function (e) {

            var target = e.currentTarget,
                results = this.get('results'),
                record = results.getRecord(target.get("id")),
                searchConfig = this.get('searchConfig') || {},
                permissions = searchConfig.permissions;

            var updateCloseAddPersonAddress;
            // get person name

            if (target.get('checked')) {
                updateCloseAddPersonAddress = {
                    "id": record.get('id'),
                    "objectVersion": record.get('objectVersion'),
                    "startDateSDF": record.get('startDate'),
                    "personName": record.get('personName'),
                    "startDate": record.get('startDate').calculatedDate,
                    "location": record.get('location')
                };
                this.updateCloseAddPersonAddressArray.push(updateCloseAddPersonAddress);
                target.getElementsByTagName('residentCheckbox').set("id", "resident_" + record.get('id'));
            } else {
                if (this.updateCloseAddPersonAddressArray.length > 0) {
                    var index;
                    for (var i = 0; i < this.updateCloseAddPersonAddressArray.length; i++) {

                        if (this.updateCloseAddPersonAddressArray[i].id == record.get('id')) { // record
                            // found
                            index = i;
                            this.updateCloseAddPersonAddressArray.splice(index, 1); // remove
                            // record
                            break;
                        }
                    }
                }

            }
            this.set('residentDetails', this.updateCloseAddPersonAddressArray);
        }
    }, {
        ATTRS: {
            residentDetails: []
        }
    });

    var UpdatePersonAddressLocationView = Y.Base.create('updatePersonAddressLocationView',
        Y.View, [], {

            template: Y.Handlebars.templates["personAddressResidentsCloseAdd"],

            initializer: function (config) {
                this.combinedEndAddPersonAddressResults = new CombinedEndAddPersonAddressResults({
                    searchConfig: config
                });
            },
            destructor: function () {},

            render: function () {
                var labels = this.get('labels'),
                    html = this.template({
                        narrativeSummaryResidents: labels.narrativeSummaryResidents
                    });
                this.get('container').setHTML(html);
                this.get('container').append(this.combinedEndAddPersonAddressResults.render().get('container'));
                return this;
            },
        }, {
            ATTRS: {
                searchConfig: {
                    value: {
                        url: {},
                        noDataMessage: {},
                        permissions: {},
                        resultsCountNode: {}
                    }
                },
                locationId: {},
                startDate: {},
                startDateSFD: {},
                endPersonAddressDetails: {},
                residentsToProcess: {}
            }
        });

    var CombinedEndAddPersonAddressView = Y.Base.create('combinedEndAddPersonAddressView',
        Y.View, [], {

            template: Y.Handlebars.templates["personAddressResidentsCloseAdd"],

            initializer: function (config) {
                this.combinedEndAddPersonAddressResults = new CombinedEndAddPersonAddressResults({
                    searchConfig: config
                });
            },
            destructor: function () {},

            render: function () {
                var labels = this.get('labels');

                html = this.template({
                    narrativeSummaryResidents: labels.narrativeSummaryResidents,
                    startDate: this.get('startDate'),
                    endDate: this.get('endPersonAddressDetails').endDate,
                    endReason: this.get('endPersonAddressDetails').endReason,
                    narrativeDescriptionReason: labels.narrativeDescriptionReason,
                    narrativeDescriptionStart: labels.narrativeDescriptionStart,
                    narrativeDescriptionEnd: labels.narrativeDescriptionEnd,
                    codedEntries: {
                        endReason: Y.uspCategory.address.endReason.category.getActiveCodedEntries()
                    },
                });
                this.get('container').setHTML(html);
                this.get('container').append(this.combinedEndAddPersonAddressResults.render().get('container'));
                return this;
            },
        }, {
            ATTRS: {
                searchConfig: {
                    value: {
                        url: {},
                        noDataMessage: {},
                        permissions: {},
                        resultsCountNode: {}
                    }
                },
                locationId: {},
                startDate: {},
                startDateSFD: {},
                endPersonAddressDetails: {},
                residentsToProcess: {}
            }
        });

    // Add PersonContact View
    var NewPersonContactView = Y.Base.create('NewPersonContactView', Y.app.contact.NewSubjectContactView, [], {
        // bind template
        template: Y.Handlebars.templates["personContactAddDialog"],

        render: function () {
            var container = this.get('container'),
                // template=Y.Handlebars.compile(Y.one('#personContactAddDialog').getHTML()),

                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    targetPersonId: this.get('targetPersonId'),
                    targetPersonName: this.get('targetPersonName')
                });

            // Render this view's HTML into the container element.
            container.setHTML(html);
            this.initlists('contactType');
            this.initlists('contactUsage');
            this.initlists('socialMediaType');
            this.initlists('telephoneType');
            this.initContactVerifiedDate(container);
            return this;
        },

        aftercontactFixedType: function (e) {
            var input = this.getInput('contactFixedType');
            var value = input.get('value');

            // apply the background highlight to the context sensitive inputs
            Y.one('#contextSensitive-wrapper.inner-panel').removeClass('pseudo');

            // set flag to enable/disable the save button
            if (value) {
                this.set('saveEnabled', true);
            } else {
                this.set('saveEnabled', false);
            }

            // To set the type of contact default as HOME
            this.getInput('contactType').set('value', "HOME");

            this.setAttrs({
                fixedContactType: value
            }, {
                silent: true
            });

            switch (value) {
            case 'SOCIALMEDIA':
                this.getWrapper('socialMediaType').show();
                this.getWrapper('identifier').show();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').hide();
                this.getWrapper('contactUsage').show();
                break;
            case 'TELEPHONE':
                this.getWrapper('telephoneType').show();
                this.getWrapper('number').show();
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('address').hide();
                this.getWrapper('contactUsage').show();
                // TODO only for professional persons
                if (this.get('targetPersonTypes').indexOf('PROFESSIONAL') > -1) {
                    this.getInput('contactType').set('value', 'WORK');
                    this.getInput('telephoneType').set('value', 'MOBILE');
                }

                break;
            case 'EMAIL':
                this.getLabel('address').setContent('Email address <span class="mandatory">*</span>');
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').show();
                this.getWrapper('contactUsage').show();
                break;
            case 'WEB':
                this.getLabel('address').setContent('Web address <span class="mandatory">*</span>');
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').show();
                this.getWrapper('contactUsage').show();
                break;
            default:
                this.getWrapper('socialMediaType').hide();
                this.getWrapper('identifier').hide();
                this.getWrapper('telephoneType').hide();
                this.getWrapper('number').hide();
                this.getWrapper('address').show();
                break;
            }
        },
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            targetPersonId: {},
            targetPersonName: {},

            targetPersonTypes: [],

            // flag to remember the state of the dialog save button.
            saveEnabled: {
                value: false
            },

            // holds the users contact type selection. set in aftercontactFixedType wired to a change listener.
            fixedContactType: {
                value: null
            }
        }
    });

    var UpdatePersonPictureView = Y.Base.create('UpdatePersonPictureView', Y.usp.person.NewPersonPictureView, [BaseAddUpdatePerson], {
        // bind template

        template: Y.Handlebars.templates['editPersonPicture'],
        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    personDetailsModelData: this.get('personDetailsModel').toJSON(),
                    defaultImageUrl: this.get('defaultImageUrl')
                });

            container.setHTML(html);

            this.initPersonPictureCanvas();

            return this;
        }
    }, {
        ATTRS: {}
    });

    var RemovePersonPictureView = Y.Base.create('RemovePersonPictureView', Y.usp.person.PersonPictureView, [BaseAddUpdatePerson], {
        // bind template
        template: Y.Handlebars.templates['removePersonPicture'],
        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    personDetailsModelData: this.get('personDetailsModel').toJSON()
                });

            container.setHTML(html);

            return this;
        }
    }, {
        ATTRS: {
            width: {
                value: 100
            }
        }
    });

    Y.namespace('app.person').AddPersonRolesView = AddPersonRolesView;
    Y.namespace('app.person').PersonRoleChangeView = PersonRoleChangeView;
    Y.namespace('app.person').ViewPersonView = ViewPersonView;
    Y.namespace('app.person').UpdatePersonView = UpdatePersonView;
    Y.namespace('app.person').ChangePersonNameView = ChangePersonNameView;
    Y.namespace('app.person').ChangeUnbornView = ChangeUnbornView;
    Y.namespace('app.person').ChangeNotCarriedView = ChangeNotCarriedView;
    Y.namespace('app.person').ChangeAliveView = ChangeAliveView;
    Y.namespace('app.person').ChangeDeceasedToAliveView = ChangeDeceasedToAliveView;
    Y.namespace('app.person').AddPersonAddressView = AddPersonAddressView;
    Y.namespace('app.person').UpdatePersonLocationView = UpdatePersonLocationView;
    Y.namespace('app.person').EndPersonAddressView = EndPersonAddressView;
    Y.namespace('app.person').ReopenPersonAddressView = ReopenPersonAddressView;
    Y.namespace('app.person').UpdateStartEndPersonAddressView = UpdateStartEndPersonAddressView;
    Y.namespace('app.person').RemovePersonAddressView = RemovePersonAddressView;
    Y.namespace('app.person').NewPersonReferenceNumberView = NewPersonReferenceNumberView;
    Y.namespace('app.person').EditRemovePersonReferenceNumberView = EditRemovePersonReferenceNumberView;
    Y.namespace('app.person').EditRemoveContactView = EditRemoveContactView;
    Y.namespace('app.person').NewPersonContactView = NewPersonContactView;
    Y.namespace('app.person').UpdatePersonLanguageView = UpdatePersonLanguageView;
    Y.namespace('app.person').UpdatePersonPictureView = UpdatePersonPictureView;
    Y.namespace('app.person').RemovePersonPictureView = RemovePersonPictureView;
    Y.namespace('app.person').CombinedEndAddPersonAddressView = CombinedEndAddPersonAddressView;
    Y.namespace('app.person').UpdatePersonAddressLocationView = UpdatePersonAddressLocationView;
    Y.namespace('app.person').CombinedEndAddPersonAddressResults = CombinedEndAddPersonAddressResults;
    Y.namespace('app.person').RemovePreviousNameView = RemovePreviousNameView;

}, '0.0.1', {
    requires: ['yui-base',
        'form-util',
        'fuzzy-date',
        'handlebars',
        'handlebars-helpers',
        'handlebars-person-templates',
        'calendar-popup',
        'event-custom',
        'tabview',
        'usp-person-NewPerson',
        'usp-person-Person',
        'usp-contact-Contact',
        'usp-contact-NewContact',
        'usp-contact-NewEmailContact',
        'usp-contact-NewWebContact',
        'usp-contact-NewSocialMediaContact',
        'usp-contact-NewTelephoneContact',
        'usp-person-NewPersonReferenceNumber',
        'usp-person-UpdatePersonLanguage',
        'usp-person-PersonReferenceNumber',
        'usp-person-UpdatePerson',
        'usp-person-PersonAddress',
        'usp-relationship-PersonAddressRelationshipName',
        'categories-person-component-Languages',
        'categories-person-component-NonVerbalLanguages',
        'categories-person-component-ProficiencyInOfficialLanguage',
        'secured-categories-person-component-Referencetype',
        'categories-person-component-Ethnicity',
        'categories-person-component-Religion',
        'categories-person-component-Title',
        'categories-person-component-ProfessionalTitle',
        'categories-person-component-Gender',
        'categories-person-component-GenderIdentity',
        'categories-person-component-SexualOrientation',
        'categories-address-component-EndReason',
        'categories-address-component-CountyCode',
        'categories-address-component-CountryCode',
        'categories-address-component-CountrySubdivisionCode',
        'usp-configuration-CodedEntry',
        'usp-configuration-CategoryContext',
        'categories-address-component-UnknownLocation',
        'categories-person-component-PersonType',
        'usp-address-Location',
        'usp-address-NewLocation',
        'model-form-link',
        'popup-autocomplete-plugin',
        'datatype-date-math',
        'usp-person-UpdatePersonBorn',
        'usp-person-UpdateNotCarriedPersonBorn',
        'usp-person-UpdateNotCarriedPersonUnborn',
        'usp-person-UpdatePersonDeceased',
        'app-utils-person',
        'usp-person-UpdatePersonDeceasedToAlive',
        'usp-person-PersonPicture',
        'usp-person-NewPersonPicture',
        'usp-cropper',
        'results-formatters',
        'app-results',
        'app-filtered-results',
        'view',
        'base-AddressLocation-formatter',
        'app-address-form-add',
        'dismissible-message',
        'promise',
        'multi-panel-popup',
        'multi-panel-model-errors-plugin',
        'base-care-view',
        'placement-address-add-view',
        'usp-date',
        'subject-contact-views'

    ]
});