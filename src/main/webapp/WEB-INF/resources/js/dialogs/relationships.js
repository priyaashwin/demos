YUI.add('relationship-dialog-views', function (Y) {
    var E = Y.Escape,
        L = Y.Lang,
        A = Y.Array,
        O = Y.Object;

    //person types constants
    var PROFESSIONAL_PERSONTYPE = 'PROFESSIONAL',
        CLIENT_PERSONTYPE = 'CLIENT',
        OTHER_PERSONTYPE = 'OTHER',
        FOSTER_CARER_PERSONTYPE = 'FOSTER_CARER',
        ADOPTER_PERSONTYPE = 'ADOPTER';

    /**
     * TODO
     * StateToggleCalendarPopup needs to use the available module
     */

    /**
     * StateToggleCalendarPopup
     * ========================
     * Extends Y.usp.CalendarPopup
     *
     * We need the start date in Add Relationship to have an enabled/disabled state. The available disable and enable
     * methods in CalendarPopup only effect the input node, not the date-picker button which remains click-able even
     * if input has been disabled.
     *
     * _handleButtonClick 	- method overrides Y.usp.CalendarPopup method to not do anything if 'buttonDisabled' is true
     * enableButton 		- removes CSS disabled class and sets a 'buttonDisabled' state to true
     * disableButton 		- adds CSS disabled class and sets a 'buttonDisabled' state to false
     * getButton			- helper to return the button node
     *
     */

    var StateToggleCalendarPopup = Y.Base.create('calendarPopup', Y.usp.CalendarPopup, [], {
        _handleButtonClick: function (e) {
            e.preventDefault();
            var instance = this;

            if (instance.get('buttonDisabled')) {
                return;
            }

            if (instance.get('visible') === true) {
                instance.hide();
            } else {
                instance.show();
            }
        },
        getButton: function () {
            return this._calendarButton.one(this.get('SELECTOR_CAL_BUTTON'));
        },
        enableButton: function () {
            this.set('buttonDisabled', false);
            this.getButton().removeClass(this.get('CSS_BUTTON_DISABLED'));
        },
        disableButton: function () {
            this.set('buttonDisabled', true);
            this.getButton().addClass(this.get('CSS_BUTTON_DISABLED'));
        }
    }, {
        ATTRS: {
            SELECTOR_CAL_BUTTON: {
                value: '.pure-button-cal'
            },
            CSS_BUTTON_DISABLED: {
                value: 'pure-button-disabled'
            },
            buttonDisabled: {
                value: false
            }
        }
    });

    // New Relationship View - Extend the view BaseAddEditRelationshipView
    var NewRelationshipView = Y.Base.create('NewRelationshipView',Y.app.relationship.BaseAddEditRelationshipView, [Y.app.mixin.MultiView], {
            //bind template
            template: Y.Handlebars.templates["relationshipAddDialog"],

            events: {
                '#relatedPersonList li a': {
                    click: 'changeRelatedPerson'
                },
                '#relationship_relationshipTypeId': {
                    change: 'initRoleSelect'
                },
                '#relationship_relationshipRole': {
                    change: 'afterRelationshipRole'
                },
                '#roleQualifiers input': {
                    click: 'updateRoleDisplay'
                },
                '#unionRoles input': {
                    click: 'updateUnionDisplay'
                },
                'input[name="startDate"]': {
                    dateChange: 'initProfessionalTeamSelectOnDateChange',
                    change: 'initProfessionalTeamSelectOnDateChange'
                }
            },

            /**
             * @method initializer
             * @description
             */
            initializer: function (config) {

                this.set('targetPerson', {
                    id: config.targetPersonId,
                    personTypes: config.personRoles,
                    name: config.targetPersonName,
                    identifier: 'PER' + config.targetPersonId,
                    gender: config.otherData.gender,
                    age: config.otherData.age,
                    entityType: 'person'
                });

                this.hideNamedPersonRelationship = config.hideNamedPersonRelationship;
                this.allowedAttributes = config.allowedAttributes;
                this.startDateCalendar = new StateToggleCalendarPopup({
                    minimumDate: new Date(1900, 1, 1)
                }).addTarget(this);
            },

            changeErrorHighlightToBlue: function (e) {
                this.getWrapper('dateOfBirth').addClass('focus');
            },

            changeErrorHighlightToRed: function (e) {
                this.getWrapper('dateOfBirth').removeClass('focus');
            },

            getViews: function (cfg) {

                var related = Y.merge({}, this.get('relatedPerson'), {
                    entityType: 'person'
                });

                return {
                    attributesPanel: {
                        hook: '#attributes-panel-hook',
                        type: Y.app.relationship.AttributesPanel,
                        relationshipType: this.get('relationshipType'),
                        relatedEntity: related,
                        targetEntity: this.get('targetPerson'),
                        familialRelationshipRoles: this.get('familialRelationshipRoles'),
                        socialRelationshipRoles: this.get('socialRelationshipRoles'),
                        professionalRelationshipRoles: this.get('professionalRelationshipRoles'),
                        isEdit: false,
                        hideNamedPersonRelationship: this.hideNamedPersonRelationship,
                        allowedAttributes: this.allowedAttributes
                    }
                };
            },

            render: function () {

                var container = this.get('container'),
                    relatedPerson = this.get('relatedPerson'),
                    relationshipTypes = this.get('relationshipTypes'),
                    relationshipType = this.get('relationshipType'),
                    narrativeSummary,
                    html;

                if (relatedPerson) {
                    this.setAttrs({
                        relatedPerson: relatedPerson,
                        relatedPersonName: relatedPerson.name,
                        relatedPersonId: relatedPerson.personId || relatedPerson.relatedPersonId,
                        relatedPersonDoB: relatedPerson.relatedPersonDoB || (typeof relatedPerson.dateOfBirth === "undefined" || relatedPerson.dateOfBirth === null) ? null : relatedPerson.dateOfBirth.calculatedDate,
                        relatedPersonDoBEstimated: relatedPerson.dateOfBirthEstimated || relatedPerson.relatedPersonDoBEstimated,
                        relatedPersonDueDate: (relatedPerson.dueDate) ? Y.Date.format(Y.Date.parse(relatedPerson.dueDate), {
                            format: "%F"
                        }) : null,
                        relatedPersonLifeState: relatedPerson.lifeState
                    }, {
                        silent: true
                    });
                }

                narrativeSummary = "You're adding a " + relationshipTypes[relationshipType] + " relationship for ";
                narrativeDescription = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')';

                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    otherData: this.get('otherData'),
                    relatedPerson: this.get('relatedPerson'),
                    relationshipTypeName: this.get('relationshipType'),
                    relationshipTypes: this.get('relationshipTypes'),
                    narrativeSummary: narrativeSummary,
                    narrativeDescription: narrativeDescription

                });

                // Render this view's HTML into the container element.
                container.setHTML(html);

                this.initPersonAutoComplete();
                this.initRoleSelect();

                //if we have a related person already setup the relationship roles
                if (relatedPerson) {
                    this.getInput('relationshipRole').removeAttribute('disabled');
                    this.getLabel('relationshipRole').removeClass('label-disabled');
                    this.setAttrs({
                        genderDisplay: (relatedPerson.gender) ? relatedPerson.gender.toUpperCase() : null
                    }, {
                        silent: true
                    });
                    this.initRoleSelect();
                }

                //call render on our calendar
                this.startDateCalendar.set('inputNode', this.getInput('startDate'));
                //render the calendar
                this.startDateCalendar.render();
                this.startDateCalendar.disableButton();

                if (relationshipType !== 'FamilialRelationshipType') {
                    container.one('#roleQualifiers').setStyle('display', 'none');
                }

                if (relationshipType !== 'ProfessionalRelationshipType') {
                    container.one('#relatedProfessionalTeamSelect').setStyle('display', 'none');
                }

                return this;
            },

            destructor: function () {
                //destroy autocomplete
                var relatedPersonSearch = this.getInput('relatedPersonSearch'),
                    calNode = this.getInput('startDate');

                if (relatedPersonSearch) {
                    //unplug everything
                    relatedPersonSearch.unplug();
                    //destroy node
                    relatedPersonSearch.destroy();
                }
                if (this.startDateCalendar) {
                    //destroy calendar
                    this.startDateCalendar.removeTarget(this);
                    this.startDateCalendar.destroy();
                    delete this.startDateCalendar;
                }
                if (calNode) {
                    //destroy node
                    calNode.destroy();
                    calNode = null;
                }
            },

            getInput: function (inputId) {
                var container = this.get('container');
                return container.one('#relationship_' + inputId);
            },

            getLabel: function (inputId) {
                var container = this.get('container');
                return container.one('#relationship_' + inputId + '_label');
            },

            getWrapper: function (inputId) {
                var container = this.get('container');
                return container.one('#relationship_' + inputId + '_wrapper');
            },

            afterRelationshipRole: function (e) {

                var relationshipTypeName = this.getAttrs().relationshipType;
                e.preventDefault();
                this.startDateCalendar.enableButton();
                this.getInput('startDate').removeAttribute('disabled');
                this.getInput('startDateEstimated').removeAttribute('disabled');
                this.getLabel('startDate').removeClass('label-disabled');
                this.getLabel('roleQualifiers').removeClass('label-disabled');
                this.getLabel('unionQualifiers').removeClass('label-disabled');

                this.initRoleQualifier(e);

                var role = this.getAttrs().relationshipRoleDisplay,
                    self = this;

                if (relationshipTypeName === "FamilialRelationshipType") {
                    var roleFound = false;
                    A.each(this.get('familialRelationshipRoles'), function (familialRelationshipRole) {
                        if (familialRelationshipRole.name === role) {
                            roleFound = true;
                            if (familialRelationshipRole.startDateGuidance === "YOUNGEST_PERSON_DATE_OF_BIRTH") {
                                self.setupSuggestedStartDate(relationshipTypeName);
                            } else {
                                self.getInput('startDate').set('value', '');
                                self.getInput('startDateEstimated').set('checked', false);
                            }
                            return false;
                        }
                    });
                    if (!roleFound) {
                        self.getInput('startDate').set('value', '');
                        self.getInput('startDateEstimated').set('checked', false);
                    }
                }

                this.attributesPanel.set('relationshipRole', e.currentTarget.get('value'));
            },

            updateRoleDisplay: function () {
                var isAdoptive = this.getInput('adoptive').get('checked'),
                    isHalf = this.getInput('half').get('checked'),
                    isStep = this.getInput('step').get('checked'),
                    isInLaw = this.getInput('inLaw').get('checked'),
                    isMultipleBirth = this.getInput('multipleBirth').get('checked'),
                    roleDisplay = this.get('relationshipRoleDisplay'),
                    relationshipTypeName = this.getAttrs().relationshipType,
                    self = this,
                    roleQualifiers = [roleDisplay]; // will help build our final role qualifier display

                if (isStep) {
                    roleQualifiers.unshift('step');
                    self.getInput('startDate').set('value', '');
                    self.getInput('startDateEstimated').set('checked', false);
                }

                if (isHalf) {
                    roleQualifiers.unshift('half');
                }

                if (isAdoptive) {
                    roleQualifiers.unshift('adoptive');
                    self.getInput('startDate').set('value', '');
                    self.getInput('startDateEstimated').set('checked', false);
                }

                if (isMultipleBirth) {
                    roleQualifiers.unshift('multipleBirth');
                }

                if (isInLaw) {
                    roleQualifiers.push('in-law');
                    self.getInput('startDate').set('value', '');
                    self.getInput('startDateEstimated').set('checked', false);
                }
                if (!isStep && !isAdoptive && !isInLaw) {
                    self.setupSuggestedStartDate(relationshipTypeName);
                }

                this.set('roleQualifierDisplay', roleQualifiers.join(' '));
                this.updateNarrative(3);

            },

            updateUnionDisplay: function (e) {

                var gender = this.get('genderDisplay'),
                    relationshipRoleDisplay,
                    SPACE = ' ';

                // set relationshipRoleDisplay as default first radio, or user clicked radio
                if (!e) {
                    relationshipRoleDisplay = Y.one('#unionRoles').one('input').next().get('text');
                } else {
                    relationshipRoleDisplay = Y.one(e.target).next().get('text');
                }
                var nominatedPersonGender;
                if (gender) {
                    gender = gender.toLowerCase(); // checking for gender ensures no error using toLowerCase if it's missing
                    nominatedPersonGender = this.get('otherData').gender;

                    nominatedPersonGender = nominatedPersonGender.toLowerCase();
                }
                if ((!gender || !nominatedPersonGender || gender === nominatedPersonGender) ||
                    nominatedPersonGender === 'unknown' ||
                    nominatedPersonGender === 'indeterminate' ||
                    gender === 'unknown' ||
                    gender === 'indeterminate') {
                    relationshipRoleDisplay = relationshipRoleDisplay.trim(); // ensure no leading/trailing whitespace IE9+ capable

                    if (relationshipRoleDisplay === 'Married' || relationshipRoleDisplay === 'Divorced' || relationshipRoleDisplay === 'Separated' || relationshipRoleDisplay === 'Engaged') {

                        relationshipRoleDisplay = relationshipRoleDisplay + SPACE + 'partner';

                    }

                }

                this.set('relationshipRoleDisplay', relationshipRoleDisplay);
                this.updateNarrative(2);
            },

            updateNarrative: function (step) {

                var summary = '',
                    description = '',
                    relationshipRoleDisplay = this.get('relationshipRoleDisplay'),
                    roleQualifierDisplay = this.get('roleQualifierDisplay'),
                    relationshipType = this.get('relationshipType'),
                    relationshipTypes = this.get('relationshipTypes'),
                    targetPersonName = this.get('targetPersonName'),
                    targetPersonNameAndId = targetPersonName + ' (PER' + this.get('targetPersonId') + ')',
                    relatedPersonName = '',
                    relatedPersonNameAndId = '',
                    of = ' ';

                if (relationshipRoleDisplay) {
                    if (!(relationshipRoleDisplay.indexOf(" of ") > -1) && !(relationshipRoleDisplay.indexOf(" for by") > -1)) {
                        of = ' of ';
                    }
                }

                switch (step) {
                case 1:
                    this.set('genderDisplay', '');
                    this.set('relatedPersonId', '');
                    this.set('relatedPersonName', '');
                    this.set('relationshipRoleDisplay', '');
                    relationshipRoleDisplay = '';

                    summary = "You're adding a " + relationshipTypes[relationshipType] + " relationship for " + targetPersonNameAndId;
                    description = "";
                    break;

                case 2:
                    relatedPersonName = this.get('relatedPersonName');
                    relatedPersonNameAndId = relatedPersonName + ' (PER' + this.get('relatedPersonId') + ')';
                    summary = "You're adding a " + relationshipTypes[relationshipType] + " relationship for " + targetPersonName;
                    description = relatedPersonNameAndId + " is " + relationshipRoleDisplay + of +targetPersonNameAndId;
                    break;

                case 3:
                    relatedPersonName = this.get('relatedPersonName');
                    relatedPersonNameAndId = relatedPersonName + ' (PER' + this.get('relatedPersonId') + ')';
                    summary = "You're adding a " + relationshipTypes[relationshipType] + " relationship for " + targetPersonName;
                    description = relatedPersonNameAndId + " is " + roleQualifierDisplay + of +targetPersonNameAndId;
                    break;
                }

                //TODO This is liable to break if Partner change back to union, would be better to do this on an id.
                if (relationshipRoleDisplay && relationshipRoleDisplay.toLowerCase() === 'partner') {
                    this.updateUnionDisplay();
                    return;
                }

                if (relationshipRoleDisplay && relationshipRoleDisplay.toLowerCase() === 'not recorded') {
                    description = 'Relationship type not recorded';
                }

                if (relationshipRoleDisplay && relationshipRoleDisplay.toLowerCase() === 'unknown') {
                    description = 'Relationship type unknown';
                }

                //TODO correct - we shouldn't be using Y.one in a view
                Y.one('#relationship_summary').set('text', summary);
                Y.one('#relationship_description').set('text', description);

            },

            initPersonAutoComplete: function () {
                var addressHTML = '<strong>Address</strong> <span class="address">{address}</span>';

                var relatedPerson = this.get("relatedPerson");
                var acUrl = this.get('autocompleteURL');

                if (relatedPerson) {
                    return;
                }

                var PersonName = this.get('targetPersonName');

                /* Work out which person types should be selectable based on:
                 * a) Relationship type
                 * b) Nominated persons roles

                  Rel type  Left person roles     Selectable persons
                  --------  -----------------     ------------------
                  prof      prof                  Persons whose roles include one of (other, client, foster carer, adopter)
                  prof      oth/cli/carer         Persons whose roles include professional
                  prof      prof+oth/cli/carer    All persons
                  fam/soc   oth/cli/carer         Persons whose roles include one of (other, client, foster carer, adopter)
                  fam/soc   prof+oth/cli/carer    Persons whose roles include one of (other, client, foster carer, adopter)

                  prof=professional role only
                  oth/cli/carer=Roles only include one or more of (other, client, foster carer, adopter).
                  prof+oth/cli =professional role plus one or more of (other, client, foster carer, adopter) roles
                  Left person=nominated person
                 */
                var relationshipType = this.get('relationshipType');
                var selectablePersonRoles = [];
                var targetPersonRoles = this.get('personRoles');

                // If the relationship type is familial or social
                if (relationshipType === 'FamilialRelationshipType' || relationshipType === 'SocialRelationshipType') {
                    selectablePersonRoles = ['OTHER', 'CLIENT', 'FOSTER_CARER', 'ADOPTER'];
                    // Otherwise the type is assumed to be professional
                } else {
                    // If the target person has a professional role
                    if (targetPersonRoles.includes('PROFESSIONAL')) {
                        selectablePersonRoles = ['OTHER', 'CLIENT', 'FOSTER_CARER', 'ADOPTER'];
                        // If the target person also has any one of other, client, foster carer or adopter roles
                        for (var i = 0; i < selectablePersonRoles.length; i++) {
                            if (targetPersonRoles.includes(selectablePersonRoles[i])) {
                                selectablePersonRoles.push('PROFESSIONAL');
                                break;
                            }
                        }
                        // If the target person does not have a professional role
                    } else {
                        acUrl = this.get('autocompleteProfessionalURL');
                        selectablePersonRoles = ['PROFESSIONAL'];
                    }
                }

                var relatedPersonSearch = this.getInput('relatedPersonSearch');
                var ds = new Y.DataSource.IO({
                    source: acUrl,
                    ioConfig: {
                        headers: {
                            'Accept': "application/vnd.olmgroup-usp.person.PersonWithAddressAndSecurityAccessStatus+json"
                        }
                    }
                });

                var isPersonSelectable = function (person) {
                    var accessRestricted = typeof (person.accessAllowed) === 'boolean' && !person.accessAllowed;
                    var personTypes = person.personTypes || [];

                    if (relationshipType === 'ProfessionalRelationshipType') {
                        if (personTypes.includes('PROFESSIONAL')) {
                            // The selectable person is a professional and this is a professional relationship
                            // I cannot tell which is the professional role at this point, so the API may fail
                            // if the person is not in the professional role and access is not allowed.
                        } else {
                            // The selectable person is not a professional, so need to check access
                            if (accessRestricted) {
                                return false;
                            }
                        }
                    } else {
                        // Not selectable if the subject is access restricted
                        if (accessRestricted) {
                            return false;
                        }
                    }

                    // Determine whether this person's role is valid for the
                    // relationship type
                    for (var i = 0; i < selectablePersonRoles.length; i++) {
                        if (personTypes.indexOf(selectablePersonRoles[i]) !== -1) {
                            return true;
                        }
                    }

                    return false;
                };

                formatter = function (query, results) {
                    return A.map(results, function (result) {

                        var location = (result.raw.address) ? result.raw.address.location || {} : {},
                            address = location.location || '',
                            postcode = location.postcode || '',
                            addressStr = (address !== '' || postcode !== '') ? L.sub(addressHTML, {
                                address: E.html(address)
                            }) : null;

                        var dob = result.raw.dateOfBirth;
                        var dobEstimated = result.raw.dateOfBirthEstimated;
                        var dobInfo, gender, genderName, age = result.raw.age;

                        var personTypes = result.raw.personTypes || [],
                            rRow;

                        dobInfo = Y.app.ColumnFormatters.formatDateBasedOnLifeState(result.raw.lifeState, result.raw.dueDate, dob, dobEstimated, age, true);
                        gender = Y.uspCategory.person.gender.category.codedEntries[result.raw.gender];
                        genderName = '';

                        if (gender) {
                            genderName = gender.name;
                        }
                        var selectable = isPersonSelectable(result.raw);

                        var sep = '';
                        if (genderName.length && dobInfo.length) {
                            sep = ', ';
                        }

                        // TO DO: unselectable isn't a very good class name
                        if (personTypes.includes('PROFESSIONAL')) {
                            rRow = '<div ' + (selectable ? '' : 'class="unselectable"') + ' title="' + E.html(result.raw.name) + '">' + '<div ' + (selectable ? '' : 'class="unselectable" disabled') + '>' + '<span>' + E.html(result.raw.personIdentifier) + ' - ' + E.html(result.raw.name) + '</span>' + '</div>' + '<div ' + (selectable ? 'class="txt-color fl" ' : 'class="unselectable fl"') + '> ' + '</div>' + Y.app.search.person.RenderMatchingField(result.raw, query) + '</div>';
                        } else {
                            rRow = '<div ' + (selectable ? '' : 'class="unselectable"') + ' title="' + E.html(result.raw.name) + '">' + '<div ' + (selectable ? '' : 'class="unselectable" disabled') + '>' + '<span>' + E.html(result.raw.personIdentifier) + ' - ' + E.html(result.raw.name) + '</span>' + '</div>' + '<div ' + (selectable ? 'class="txt-color fl" ' : 'class="unselectable fl"') + '> ' + '<span> ' + genderName + sep + dobInfo + '</span>' + '</div>' + (addressStr ? ('<div ' + (selectable ? 'class="txt-color"' : 'class="unselectable"') + '> ' + addressStr + '</div>') : '') + Y.app.search.person.RenderMatchingField(result.raw, query) + '</div>';
                        }
                        return rRow;
                    });

                };

                ds.plug(Y.Plugin.DataSourceJSONSchema, {
                    schema: {
                        resultListLocator: "results"
                    }
                });
                relatedPersonSearch.plug(Y.Plugin.usp.PopupAutoCompletePlugin, {
                    width: '35em',
                    allowBrowserAutocomplete: false,
                    maxResults: 10,
                    minQueryLength: 1,
                    resultHighlighter: 'phraseMatch',
                    scrollIntoView: true,
                    resultListLocator: function (response) {
                        return response || [];
                    },
                    resultTextLocator: 'personIdentifier',
                    source: ds,
                    resultFormatter: formatter
                });
                //add the skin css to the container
                relatedPersonSearch.ancestor('div').addClass("yui3-skin-sam");

                // Update selected related person list
                relatedPersonSearch.ac.on('select', function (e) {
                    if (!isPersonSelectable(e.result.raw)) {
                        e.preventDefault();
                        return;
                    }
                    var relatedPersonSearchNode = this.getInput('relatedPersonSearch'),
                        relatedPersonItemTemplate = '<li class="data-item" id="relatedPerson{personId}"><span>{name} ({personId})</span><a href="#none" class="remove small">Change</a></li>',
                        relatedPersonItemNarrativeTemplate = '<span class="narrative_txt" style="display:block;">What is <span>{selectedName}&#39;s </span> relationship to {PersonName}?</span>',
                        result = e.result,
                        rawResult = result.raw,
                        relatedPersonList = Y.one('#relatedPersonList');
                    narrative = Y.one('#relationship_relationshipRole_label');

                    e.preventDefault();
                    // TODO: 	Uncomment when add person has been fixed to include
                    //			person types
                    //this.getInput('toggleAddPerson').hide();
                    this.getInput('relationshipRole').removeAttribute('disabled');
                    this.getLabel('relationshipRole').removeClass('label-disabled');

                    // Make a note of the gender type for any gendered roles to display
                    this.setAttrs({
                        genderDisplay: e.result.raw.gender,
                        relatedPersonId: rawResult.id,
                        relatedPersonName: result.raw.name,
                        relatedPersonDoB: (result.raw.dateOfBirth) ? result.raw.dateOfBirth.calculatedDate : null,
                        relatedPersonDoBEstimated: result.raw.dateOfBirthEstimated,
                        relatedPersonRoles: rawResult.personTypes,
                        relatedPersonDueDate: (result.raw.dueDate) ? Y.Date.format(Y.Date.parse(result.raw.dueDate), {
                            format: "%F"
                        }) : null,
                        relatedPersonLifeState: result.raw.lifeState
                    }, {
                        silent: true
                    });

                    // we can initialise the role options now as we have store the gender of the related person
                    this.initRoleSelect();

                    // update AttributesPanel with related person
                    this.attributesPanel && this.attributesPanel.set('relatedEntity', {
                        id: rawResult.id,
                        identifier: rawResult.personIdentifier,
                        name: result.raw.name,
                        gender: rawResult.gender,
                        age: result.raw.age,
                        personTypes: rawResult.personTypes,
                    });

                    //clear value and hide
                    relatedPersonSearchNode.set('value', '');
                    relatedPersonSearchNode.select();
                    relatedPersonSearchNode.ac.hide();
                    relatedPersonSearch.hide();

                    // for now we are only going to allow add one relationship at a time,
                    // so if a person has already been selected, then remove them
                    relatedPersonList.one('ul').all('li').some(function (o) {
                        o.remove(true);
                    });

                    relatedPersonList.one('ul').append(L.sub(relatedPersonItemTemplate, {
                        personId: E.html(rawResult.personIdentifier),
                        name: E.html(result.raw.name)
                    }));

                    // Hide any existing narratives
                    if (narrative.one('.default_txt')) narrative.one('.default_txt').hide();
                    if (narrative.one('.narrative_txt')) narrative.all('.narrative_txt').hide();
                    //Dispaly narrative message for relationship add dialogue
                    narrative.append(L.sub(relatedPersonItemNarrativeTemplate, {
                        PersonName: E.html(PersonName),
                        selectedName: E.html(result.raw.name)
                    }));

                }, this);

                // we check returned results dispalying an info message if none are returned
                relatedPersonSearch.ac.on('results', function (e) {

                    var selectedNode = this.get('container').one('#relatedPersonList');

                    if (e.results.length === 0) {
                        this.getWrapper('noSearchResults').show();
                        selectedNode.hide();
                    } else {
                        this.getWrapper('noSearchResults').hide();
                        selectedNode.show();
                    }
                }, this);

                relatedPersonSearch.ac.after('results', function (e) {
                    // Add unselectable class to list items that contain unselectable divs
                    this.get('container').all('.unselectable').each(function (node) {
                        node.ancestor('li').addClass('unselectable');
                    });
                }, this);

            },

            changeRelatedPerson: function (e) {
                e.preventDefault();
                //Y.one('#relationship_relatedPersonSearch').show();
                //remove the node
                e.currentTarget.ancestor('li').remove(true);

                // ENABLED/DISABLE
                this.initRoleSelect();
                this.resetRoleQualifiers();
                this.getInput('relatedPersonSearch').show();
                this.startDateCalendar.disableButton();
                this.getInput('relationshipRole').setAttribute('disabled', 'disabled');
                this.getInput('startDate').set('value', '').setAttribute('disabled', 'disabled');
                this.getInput('startDateEstimated').set('checked', '').setAttribute('disabled', 'disabled');
                this.getLabel('relationshipRole').addClass('label-disabled');
                this.getLabel('roleQualifiers').addClass('label-disabled');
                this.getLabel('startDate').addClass('label-disabled');
                this.getLabel('relatedProfessionalTeamName').addClass('label-disabled');

                Y.one('#roleQualifiers').show();
                Y.one('#unionQualifiers').hide();
                Y.one('#unionRoles').setHTML('');
                Y.one('#professional-teams-select').hide();
                Y.one('#professional-teams-select').setHTML('');

                this.updateNarrative(1);
                this.attributesPanel.clrValues();
            },

            initRoleQualifier: function (e) {

                var roleId = e.target.get("options").item(e.target.get('selectedIndex')).get('value'),
                    roleDisplay = e.target.get("options").item(e.target.get('selectedIndex')).get('text'),
                    relationshipTypeName = this.getAttrs().relationshipType;

                e.preventDefault();

                this.set('relationshipRoleDisplay', roleDisplay);
                this.setupRoleQualifiers(roleId, relationshipTypeName);
                this.updateNarrative(2);
            },

            getArrayFromString: function (string) {
                return string.replace("[", "").replace("]", "").split(",");
            },

            initRoleSelect: function (e) {

                var relationshipRoleInput = this.getInput('relationshipRole'),
                    relationshipType = this.get('relationshipType'),
                    allRoles,
                    maleGenderRoles = [],
                    femaleGenderRoles = [],
                    unknownGenderRoles = [],
                    socialRoles = [];

                switch (relationshipType) {
                case 'FamilialRelationshipType':
                    var attrs = this.getAttrs(),
                        genderDisplay = attrs.genderDisplay;
                    allRoles = this.get('familialRelationshipRoles');
                    maleGenderRoles = [];
                    femaleGenderRoles = [];
                    unknownGenderRoles = [];

                    allRoles.forEach(function (role) {
                        if (role.description !== 'UNION') {
                            if (role.gender === 'MALE') {
                                maleGenderRoles.push(role);
                            } else if (role.gender === 'FEMALE') {
                                femaleGenderRoles.push(role);
                            } else if (role.gender === 'NOGENDER') {
                                unknownGenderRoles.push(role);
                            } else {
                                maleGenderRoles.push(role);
                                femaleGenderRoles.push(role);
                                unknownGenderRoles.push(role);
                            }
                        }
                    });

                    if (genderDisplay === 'MALE') {
                        Y.FUtil.setSelectOptions(relationshipRoleInput, maleGenderRoles, null, null, true, true);
                    } else if (genderDisplay === 'FEMALE') {
                        Y.FUtil.setSelectOptions(relationshipRoleInput, femaleGenderRoles, null, null, true, true);
                    } else {
                        Y.FUtil.setSelectOptions(relationshipRoleInput, unknownGenderRoles, null, null, true, true);
                    }
                    break;
                case 'SocialRelationshipType':
                    //removeUnions(femaleGenderRoles);
                    allRoles = this.get('socialRelationshipRoles');
                    socialRoles = [];
                    allRoles.forEach(function (role) {
                        if (role.description !== 'UNION') {
                            socialRoles.push(role);
                        }
                    });
                    Y.FUtil.setSelectOptions(relationshipRoleInput, socialRoles, null, null, true, true);
                    break;
                case 'ProfessionalRelationshipType':
                    /* For professional relationships, we need to work out from the roles of the persons whether to display
                     * professional-centric relationships (eg GP), subject-centric relationships (eg Client of GP), or both
                     *  
                     *  Rules are as follows:
                     *  Nominated person roles		Selected person roles		Relationships to display
                    	-----------------			------------------			----------------------------
                    	pro							oth/cli						Subject-centric
                    	pro							pro+oth/cli					Subject-centric
                    	oth/cli						pro							Professional-centric
                    	oth/cli						pro+oth/cli					Professional-centric
                    	pro+oth/cli					oth/cli						Subject-centric
                    	pro+oth/cli					pro							Professional-centric
                    	pro+oth/cli					pro+oth/cli					Professional-centric and subject-centric
                     *
                     *
                     */

                    // Nominated person is professional only
                    var relTypes;
                    var personRolesArray = this.getArrayFromString(this.get('personRoles'));

                    if (personRolesArray.length === 1 && personRolesArray[0] === 'PROFESSIONAL') {
                        relTypes = 'SUB';
                        // Nominated person is other and/or client
                    } else if (personRolesArray.includes('PROFESSIONAL')) {
                        relTypes = 'PRO';
                        // Nominated person is professional and other and/or client
                    } else {
                        // Selected person has professional role
                        if (this.get('relatedPersonRoles')) {
                            var relatedPersonRoles = this.get('relatedPersonRoles');
                            if (relatedPersonRoles.includes('PROFESSIONAL')) {
                                relTypes = 'PRO';
                                // Selected person has professional and other and/or client role
                                if (relatedPersonRoles.includes('CLIENT') || relatedPersonRoles.includes('OTHER')) {
                                    relTypes = 'BOTH';
                                }
                                // Selected person has only other and/or client role
                            } else {
                                relTypes = 'SUB';
                            }
                        }

                    }

                    var allRoles = this.get('professionalRelationshipRoles');
                    var rolesToDisplay = [];
                    if (relTypes !== 'BOTH') {
                        allRoles.forEach(function (role) {
                            if (role.isAncestorRole === (relTypes === 'PRO')) {
                                rolesToDisplay.push(role);
                            }
                        });
                    } else {
                        rolesToDisplay = this.get('professionalRelationshipRoles');
                    }

                    Y.FUtil.setSelectOptions(relationshipRoleInput, rolesToDisplay, null, null, true, true);
                    break;
                }
            },

            resetRoleQualifiers: function () {
                var inLawInput = this.getInput('inLaw'),
                    adoptiveInput = this.getInput('adoptive'),
                    halfInput = this.getInput('half'),
                    stepInput = this.getInput('step'),
                    multipleBirthInput = this.getInput('multipleBirth');

                inLawInput.set('checked', '').setAttribute('disabled', 'disabled');
                adoptiveInput.set('checked', '').setAttribute('disabled', 'disabled');
                halfInput.set('checked', '').setAttribute('disabled', 'disabled');
                stepInput.set('checked', '').setAttribute('disabled', 'disabled');
                multipleBirthInput.set('checked', '').setAttribute('disabled', 'disabled');
            },

            setupRoleQualifiers: function (roleId, relationshipType) {
                var inLawInput = this.getInput('inLaw'),
                    adoptiveInput = this.getInput('adoptive'),
                    halfInput = this.getInput('half'),
                    stepInput = this.getInput('step'),
                    multipleBirthInput = this.getInput('multipleBirth');

                var selectedRelationshipTypeIsUnion = false;

                //TODO This is liable to break if Partner change back to union, would be better to do this on an id.
                A.each(this.get('socialRelationshipRoles'), function (socialRelationshipRole) {
                    if (socialRelationshipRole.id === roleId && socialRelationshipRole.relationshipClassName === 'Partner') {
                        selectedRelationshipTypeIsUnion = true;
                    }
                });

                //TODO This is liable to break if Partner change back to union, would be better to do this on an id.
                A.each(this.get('familialRelationshipRoles'), function (familialRelationshipRole) {
                    if (familialRelationshipRole.id === roleId && familialRelationshipRole.relationshipClassName === 'Partner') {
                        selectedRelationshipTypeIsUnion = true;
                    }
                });

                if (selectedRelationshipTypeIsUnion) {

                    Y.one('#roleQualifiers').hide();

                    var attrs = this.getAttrs(),
                        genderDisplay = attrs.genderDisplay,
                        relationshipNames = [],
                        relationshipClassNames = [],
                        checked,
                        nominatedPersonGender,
                        radioTemplateHtml = '';

                    Y.one('#unionRoles').setHTML(null);

                    if (relationshipType === 'FamilialRelationshipType') {
                        checked = ' checked ';
                        nominatedPersonGender = this.get('otherData').gender;
                        A.each(this.get('familialRelationshipRoles'), function (familialRelationshipRole) {

                            if (familialRelationshipRole.description === 'UNION' &&
                                (genderDisplay) &&
                                (nominatedPersonGender) &&
                                (genderDisplay !== '') &&
                                (nominatedPersonGender !== '') &&
                                (genderDisplay !== nominatedPersonGender) &&
                                (nominatedPersonGender !== 'UNKNOWN') &&
                                (nominatedPersonGender !== 'INDETERMINATE') &&
                                (genderDisplay !== 'UNKNOWN') &&
                                (genderDisplay !== 'INDETERMINATE') &&
                                (genderDisplay === familialRelationshipRole.gender || familialRelationshipRole.gender === null)) {

                                if (A.indexOf(relationshipNames, familialRelationshipRole.name) === -1) {
                                    //!! This should be moved outside of the iteration block - and converted into a proper template
                                    radioTemplateHtml = '<div class="pure-u-1-3 l-box"><label class="pure-radio">' + '<input type="radio" id="' + familialRelationshipRole.id + '" name="unionType"' + checked + ' value="' + familialRelationshipRole.id + '"/>' + '<span class="mandatory">' + familialRelationshipRole.name + '</span></label></div>';

                                    /*
	                			 Y.Node.create(radioTemplateHtml).appendTo(Y.one('#unionRoles')).on('click', function(selectedRole){
	                     			Y.log(' union selected ');
	                     		  });
	                     		  */

                                    Y.Node.create(radioTemplateHtml).appendTo(Y.one('#unionRoles'));

                                    relationshipNames.push(familialRelationshipRole.name);
                                    checked = '';
                                }
                            } else if (familialRelationshipRole.description === 'UNION' &&
                                ((!genderDisplay || genderDisplay === '' || !nominatedPersonGender || nominatedPersonGender === '' || genderDisplay === nominatedPersonGender) || (nominatedPersonGender === 'UNKNOWN') ||
                                    (nominatedPersonGender === 'INDETERMINATE') || (genderDisplay === 'UNKNOWN') ||
                                    (genderDisplay === 'INDETERMINATE'))) {

                                if (A.indexOf(relationshipClassNames, familialRelationshipRole.relationshipClass) === -1) {

                                    var relClass = familialRelationshipRole.relationshipClassName.toLowerCase();
                                    var displayClass = relClass.charAt(0).toUpperCase() + relClass.slice(1);
                                    if (displayClass.indexOf('relationship') > -1) {
                                        displayClass = displayClass.substring(0, displayClass.indexOf('relationship'));
                                    }
                                    //!! This should be moved outside of the iteration block - and converted into a proper template
                                    radioTemplateHtml = '<div class="pure-u-1-3 l-box"><label class="pure-radio">' + '<input type="radio" id="' + familialRelationshipRole.id + '" name="unionType"' + checked + ' value="' + familialRelationshipRole.id + '"/>' + '<span class="mandatory">' + E.html(displayClass) + '</span></label></div>';
                                    /*
	                 			 Y.Node.create(radioTemplateHtml).appendTo(Y.one('#unionRoles')).on('click', function(selectedRole){
	                      			Y.log(' union selected ');
	                      		  });
	                      		  */
                                    Y.Node.create(radioTemplateHtml).appendTo(Y.one('#unionRoles'));

                                    relationshipClassNames.push(familialRelationshipRole.relationshipClass);
                                    checked = '';
                                }
                            }
                        });

                    } else if (relationshipType === 'SocialRelationshipType') {
                        checked = ' checked ';
                        A.each(this.get('socialRelationshipRoles'), function (socialRelationshipRole) {
                            if (socialRelationshipRole.description === 'UNION') {
                                //!! This should be moved outside of the iteration block - and converted into a proper template
                                radioTemplateHtml = '<div class="pure-u-1-1 l-box"><label class="pure-radio">' + '<input type="radio" id="' + socialRelationshipRole.id + '" name="unionType" ' + checked + ' value="' + socialRelationshipRole.id + '"/>' + '<span class="mandatory">' + socialRelationshipRole.name + '</span></label></div>';
                                /*
                			 Y.Node.create(radioTemplateHtml).appendTo(Y.one('#unionRoles')).on('click', function(selectedRole){
                     			Y.log(' union selected ');
                     		  });
                     		  */

                                Y.Node.create(radioTemplateHtml).appendTo(Y.one('#unionRoles'));

                                checked = '';
                            }
                        });

                    }

                    Y.one('#unionQualifiers').show();
                    Y.one('#unionRoles').show();

                } else {

                    Y.one('#unionQualifiers').hide();
                    Y.one('#unionRoles').hide();
                    Y.one('#roleQualifiers').show();

                    if (relationshipType !== 'FamilialRelationshipType' || roleId === '') {
                        inLawInput.set('checked', '').setAttribute('disabled', 'disabled');
                        adoptiveInput.set('checked', '').setAttribute('disabled', 'disabled');
                        halfInput.set('checked', '').setAttribute('disabled', 'disabled');
                        stepInput.set('checked', '').setAttribute('disabled', 'disabled');
                        multipleBirthInput.set('checked', '').setAttribute('disabled', 'disabled');
                    } else {
                        A.each(this.get('familialRelationshipRoles'), function (familialRelationshipRole) {
                            if (familialRelationshipRole.id === roleId) {
                                if (familialRelationshipRole.isInLawApplicable === true) {
                                    inLawInput.set('checked', '');
                                    inLawInput.removeAttribute('disabled');
                                } else {
                                    inLawInput.set('checked', '').setAttribute('disabled', 'disabled');
                                }
                                if (familialRelationshipRole.isAdoptiveApplicable === true) {
                                    adoptiveInput.set('checked', '');
                                    adoptiveInput.removeAttribute('disabled');
                                } else {
                                    adoptiveInput.set('checked', '').setAttribute('disabled', 'disabled');
                                }
                                if (familialRelationshipRole.isHalfFullApplicable === true) {
                                    halfInput.set('checked', '');
                                    halfInput.removeAttribute('disabled');
                                } else {
                                    halfInput.set('checked', '').setAttribute('disabled', 'disabled');
                                }
                                if (familialRelationshipRole.isStepApplicable === true) {
                                    stepInput.set('checked', '');
                                    stepInput.removeAttribute('disabled');
                                } else {
                                    stepInput.set('checked', '').setAttribute('disabled', 'disabled');
                                }
                                if (familialRelationshipRole.isMultipleBirthApplicable === true) {
                                    multipleBirthInput.set('checked', '');
                                    multipleBirthInput.removeAttribute('disabled');
                                } else {
                                    multipleBirthInput.set('checked', '').setAttribute('disabled', 'disabled');
                                }
                            }
                        });
                    }
                }
            },

            setupSuggestedStartDate: function () {

                var roleAPersonDOB = this.get('targetPersonDOB'),
                    roleBPersonDOB = this.get('relatedPersonDoB');
                var targetPersonDobEstimated = String(this.get('targetPersonDoBEstimated')),
                    relatedPersonDobEstimated = String(this.get('relatedPersonDoBEstimated'));
                var startDateAPersonDOB = Y.USPDate.formatDateValue(roleAPersonDOB),
                    startDateBPersonDOB = Y.USPDate.formatDateValue(roleBPersonDOB);
                var self = this;

                if (roleAPersonDOB > roleBPersonDOB) {
                    this.getInput('startDate').set('value', startDateAPersonDOB);
                    if (targetPersonDobEstimated === String(true)) {
                        self.getInput('startDateEstimated').set('checked', true);
                    }
                } else if (roleAPersonDOB < roleBPersonDOB) {
                    this.getInput('startDate').set('value', startDateBPersonDOB);
                    if (relatedPersonDobEstimated === String(true)) {
                        self.getInput('startDateEstimated').set('checked', true);
                    }
                } else if (roleAPersonDOB === roleBPersonDOB) {
                    this.getInput('startDate').set('value', startDateAPersonDOB);
                    if (relatedPersonDobEstimated === String(true) || targetPersonDobEstimated === String(true)) {
                        self.getInput('startDateEstimated').set('checked', true);
                    }
                }

            }

        }, {
            // Specify attributes and static properties for your View here.
            ATTRS: {
                targetPersonId: {},
                targetPersonDOB: {},
                targetPersonDoBEstimated: {},
                relatedPersonId: {},
                relationshipTypes: {},
                familialRelationshipRoles: {},
                socialRelationshipRoles: {},
                professionalRelationshipRoles: {},
                autocompleteURL: {},
                relatedPerson: {},
                professionalTeamId: {},
                relatedPersonLifeState: {
                    value: ''
                },
                relatedPersonDueDate: {
                    value: ''
                },
                targetRole: {
                    value: '',
                },
                relatedRole: {
                    value: '',
                },
                currentNewPerson: {},
                panel: {
                    value: ''
                }
            }
        });

    RelatedProfessionalTeamSelectView = Y.Base.create('RelatedProfessionalTeamSelectView', Y.View, [], {
        template: Y.Handlebars.templates["relatedProfessionalTeamSelect"],
        render: function () {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.setHTML(this.template({
                selectedProfessionalTeams: this.get('selectedProfessionalTeams')
            }));

            container.setHTML(contentNode);

            return this;
        }
    }, {
        ATTRS: {
            selectedProfessionalTeams: {
                value: []
            }
        }
    });

    var NewRelationshipTypeView = Y.Base.create('NewRelationshipTypeView', Y.usp.relationship.NewPersonPersonRelationshipView, [], {

        template: Y.Handlebars.templates["relationshipAddRelationshipTypeDialog"],
        events: {
            '#relationship_types input': {
                click: 'updateRelationshipType'
            }
        },
        render: function () {
            NewRelationshipTypeView.superclass.render.call(this);

            // Ensure our html template knows whether there is a 'relatedPerson' or not
            var container = this.get('container');
            var html = this.template({
                relatedPerson: this.get('relatedPerson'),
                modelData: this.get('model').toJSON()
            });
            container.setHTML(html);

            this.initTypes();
        },
        initTypes: function () {
            var targetPersonRoles = this.get('targetPersonTypes'),
                relatedPersonRoles = this.get('relatedPersonRoles');

            if (this.get('model').get('relationshipType')) {
                this.setAttrs({
                    'relationshipType': this.get('model').get('relationshipType')
                }, {
                    silent: true
                });
            }

            // Not client, other or foster carer/adopter
            if (targetPersonRoles.indexOf(CLIENT_PERSONTYPE) < 0 && targetPersonRoles.indexOf(OTHER_PERSONTYPE) < 0 &&
                targetPersonRoles.indexOf(FOSTER_CARER_PERSONTYPE) < 0 && targetPersonRoles.indexOf(ADOPTER_PERSONTYPE) < 0) {
                this.get('container').one('#relationship_typeProfessional').set('checked', 'checked');
                this.get('container').one('#relationship_typeFamilial').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                this.get('container').one('#relationship_typeSocial').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                this.setAttrs({
                    'relationshipType': 'ProfessionalRelationshipType'
                }, {
                    silent: true
                });
            }
            //if any of the related or target person has role of Professional then
            //enable professional radio option and make sure right piklist is enabled based on
            //the person type on both ends
            if (relatedPersonRoles && targetPersonRoles.indexOf(PROFESSIONAL_PERSONTYPE) < 0 && relatedPersonRoles.indexOf(PROFESSIONAL_PERSONTYPE) < 0) {
                this.get('container').one('#relationship_typeProfessional').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
            }
            if (targetPersonRoles.indexOf(CLIENT_PERSONTYPE) < 0 && targetPersonRoles.indexOf(FOSTER_CARER_PERSONTYPE) < 0 && targetPersonRoles.indexOf(ADOPTER_PERSONTYPE) < 0) {
                if (this.get('container').one('#relationship_typeTeam')) {
                    this.get('container').one('#relationship_typeTeam').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                }
            }
        },
        updateRelationshipType: function (e) {
            this.setAttrs({
                'relationshipType': e.currentTarget.get('value')
            }, {
                silent: true
            });
        }
    }, {

        ATTRS: {

            targetPerson: {
                value: {}
            },

            targetPersonId: {},
            targetPersonName: {},
            targetPersonDOB: {},
            targetPersonDoBEstimated: {},
            relatedPersonId: {},
            relationshipType: {}
        }
    });

    // Extend the USP UpdateClosePersonPersonRelationshipView
    var CloseRelationshipView = Y.Base.create('CloseRelationshipView', Y.usp.relationship.UpdateClosePersonPersonRelationshipView, [], {
        //bind template
        template: Y.Handlebars.templates["relationshipCloseDialog"],

        render: function () {
            var model = this.get('model');
            // update the display info for different relationships
            var RelationshipRole = model.get('relationship');
            if (RelationshipRole.toLowerCase() === 'unknown') {
                model.set('relationshipLiteralText', 'Relationship type unknown');
            } else if (RelationshipRole.toLowerCase() === 'not recorded') {
                model.set('relationshipLiteralText', 'Relationship type not recorded');
            } else {
                if (!(RelationshipRole.indexOf(" of ") > -1) && !(RelationshipRole.indexOf(" for by") > -1)) {
                    RelationshipRole = RelationshipRole + ' of';
                    model.set('relationshipRole', RelationshipRole);
                } else {
                    model.set('relationshipRole', RelationshipRole);
                }
            }
            //call superclass
            CloseRelationshipView.superclass.render.call(this);

            this.initClosureReasons();

            //call render on our calendar
            this.closeDateCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('closeDate'),
                //can't close before the start date
                minimumDate: this.get('model').get('startDate')
            });

            this.closeDateCalendar.render();

        },

        destructor: function () {
            var calNode = this.getInput('closeDate');
            if (this.closeDateCalendar) {
                //destroy calendar
                this.closeDateCalendar.destroy();
                delete this.closeDateCalendar;
            }

            if (calNode) {
                //destroy node
                calNode.destroy();
                calNode = null;
            }
        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#relationship_' + inputId);
        },

        initClosureReasons: function (closureReasons) {
            Y.FUtil.setSelectOptions(this.getInput('closeReason'), O.values(Y.uspCategory.relationship.personPersonRelationshipClosureReason.category.getActiveCodedEntries()), null, null, true, true, 'code');
        }
    });

    // Extend the USP DeleteRelationshipDetailView
    var DeleteRelationshipDetailView = Y.Base.create('DeleteRelationshipDetailView', Y.usp.relationshipsrecording.AsymmetricPersonPersonRelationshipDetailView, [], {
        render: function () {
            var model = this.get('model');
            // update the display info for different relationships
            var RelationshipRole = model.get('relationship');
            if (RelationshipRole.toLowerCase() === 'unknown') {
                model.set('relationshipLiteralText', 'Relationship type unknown');
            } else if (RelationshipRole.toLowerCase() === 'not recorded') {
                model.set('relationshipLiteralText', 'Relationship type not recorded');
            } else {
                if (!(RelationshipRole.indexOf(" of ") > -1) && !(RelationshipRole.indexOf(" for by") > -1)) {
                    RelationshipRole = RelationshipRole + ' of';
                    model.set('relationshipRole', RelationshipRole);
                } else {
                    model.set('relationshipRole', RelationshipRole);
                }
            }
            DeleteRelationshipDetailView.superclass.render.call(this);
        }
    });

    // Extend the USP ViewRelationshipDetailView
    var ViewRelationshipDetailView = Y.Base.create('ViewRelationshipDetailView', Y.usp.relationshipsrecording.AsymmetricPersonPersonRelationshipDetailView, [], {
        render: function () {
            var model = this.get('model');
            // update the display info for different relationships
            var RelationshipRole = model.get('relationship');
            if (RelationshipRole.toLowerCase() === 'unknown') {
                model.set('relationshipLiteralText', 'Relationship type unknown');
            } else if (RelationshipRole.toLowerCase() === 'not recorded') {
                model.set('relationshipLiteralText', 'Relationship type not recorded');
            } else {
                if (!(RelationshipRole.indexOf(" of ") > -1) && !(RelationshipRole.indexOf(" for by") > -1)) {
                    RelationshipRole = RelationshipRole + ' of';
                    model.set('relationshipRole', RelationshipRole);
                } else {
                    model.set('relationshipRole', RelationshipRole);
                }
            }
            ViewRelationshipDetailView.superclass.render.call(this);
        }
    });

    // Update Relationship View - Extend the view BaseAddEditRelationshipView
    var UpdateRelationshipView = Y.Base.create('UpdateRelationshipView', Y.app.relationship.BaseAddEditRelationshipView, [Y.app.mixin.MultiView], {

        template: Y.Handlebars.templates["relationshipEditDialog"],

        events: {
            'input[name="startDate"]': {
                dateChange: 'initProfessionalTeamSelectOnDateChange',
                change: 'initProfessionalTeamSelectOnDateChange'
            }
        },
        
        initializer: function (config) {
            this.get('model').after('load', this._syncPanel, this);
            this.hideNamedPersonRelationship = config.hideNamedPersonRelationship;
            this.allowedAttributes = config.allowedAttributes;
        },

        /**
         * @method getViews
         * @description Mixed in.
         *
         * We don't want attibutesPanel to render, so don't pass relationshipType
         */
        getViews: function (config) {
            return {

                attributesPanel: {
                    hook: '#attributes-panel-hook.person',
                    type: Y.app.relationship.AttributesPanel,
                    targetEntity: this.get('subject'),
                    familialRelationshipRoles: this.get('familialRelationshipRoles'),
                    socialRelationshipRoles: this.get('socialRelationshipRoles'),
                    professionalRelationshipRoles: this.get('professionalRelationshipRoles'),
                    isEdit: true,
                    hideNamedPersonRelationship: this.hideNamedPersonRelationship,
                    allowedAttributes: this.allowedAttributes
                }

            };
        },
        
        getLabel: function(inputId) {
            var container = this.get('container');
            return container.one('#relationship_' + inputId + '_label');
        },
        
        render: function() {
            var model = this.get('model');
            // update the display info for different relationships
            var RelationshipRole = model.get('relationship');

            if (RelationshipRole.toLowerCase() === 'unknown') {
                model.set('relationshipLiteralText', 'Relationship type unknown');
            } else if (RelationshipRole.toLowerCase() === 'not recorded') {
                model.set('relationshipLiteralText', 'Relationship type not recorded');
            } else {
                if (!(RelationshipRole.indexOf(" of ") > -1) && !(RelationshipRole.indexOf(" for by") > -1)) {
                    RelationshipRole = RelationshipRole + ' of';
                    model.set('relationshipRole', RelationshipRole);
                } else {
                    model.set('relationshipRole', RelationshipRole);
                }
            }

            this.get('container').setHTML(this.template({
                codedEntries: this.get('codedEntries'),
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                otherData: this.get('otherData'),
                relationshipTypeProfessional: this.get('relationshipType') === 'ProfessionalRelationshipType'
            }));

            if (this.get('container').one('#relatedProfessionalTeamSelect')) {
               this.initializeProfessionalTeamsOnStartDate(new Date(model.get('startDate')));
            }

            var minDate = new Date(1900, 1, 1);
            //call render on our calendar
            this.startDateCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('startDate'),
                minimumDate: minDate
            });

            this.startDateCalendar.render();

            return this;

        },

        destructor: function () {
            var calNode = this.getInput('startDate');
            if (this.startDateCalendar) {
                //destroy calendar
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }

            if (calNode) {
                //destroy node
                calNode.destroy();
                calNode = null;
            }
        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#relationship_' + inputId);
        },

        /**
         * @method _syncPanel
         * @description
         *
         * 	This view inherits from Y.View because we want more control of the render.
         *
         * 	After the render, but before we updatePanel, any views
         * 	in getViews will be attached to the view.
         *
         * 	This means the attributesPanel is attached displaying nothing because
         * 	relationshipType determines which attributes should display.
         */
        _syncPanel: function () {

            var attrs = this.get('model').toJSON();

            this.set('targetEntity', Y.merge(this.get('targetEntity'), {
                //TODO: Need to map from the VO Since we dont have person id in the model like nominatedPersonName , so just
                //slicing 3 characters  so PERXXXXX will give XXXXX as id
                id: attrs.nominatedPersonIdentifier.slice(3),
                name: attrs.nominatedPersonName,
                identifier: attrs.nominatedPersonIdentifier,
                age: this.get('nominatedPersonAge'),
                entityType: 'person'
            }));

            this.set('relatedEntity', {
                id: attrs.personVO.id,
                name: attrs.personVO.name,
                age: attrs.personVO.age,
                identifier: attrs.personVO.personIdentifier,
                personTypes: attrs.personVO.personTypes,
                entityType: 'person'
            });

            this.render();

            this.attributesPanel.updatePanel(Y.merge(attrs, {
                relationshipType: this.get('relationshipType'),
                relationshipRole: attrs.relationship,
                targetEntity: this.get('targetEntity'),
                relatedEntity: this.get('relatedEntity')
            }));

        }
    }, {
        ATTRS: {
            familialRelationshipRoles: {},
            socialRelationshipRoles: {},
            professionalRelationshipRoles: {},
            nominatedPersonAge: {}
        }
    });

    var SaveSuggestedRelationshipView = Y.Base.create('SaveSuggestedRelationshipView', Y.usp.relationship.NewPersonPersonRelationshipView, [], {
        //bind template
        template: Y.Handlebars.templates["suggestionSaveDialog"],

        render: function () {
            //call superclass
            SaveSuggestedRelationshipView.superclass.render.call(this);

            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    roleAPersonName: this.get('roleAPersonName'),
                    roleBPersonName: this.get('roleBPersonName'),
                    roleAPersonIdentifier: this.get('roleAPersonIdentifier'),
                    roleBPersonIdentifier: this.get('roleBPersonIdentifier'),
                    relationshipLabel: this.get('relationshipLabel'),
                    relationshipStartDate: this.get('relationshipStartDate'),
                    startDateEstimated: this.get('startDateEstimated'),
                    professionalTeamId: this.get('professionalTeamId'),
                    professionalTeamName: this.get('professionalTeamName')
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);

            //call render on our calendar
            this.startDateCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('startDate')
            });

            this.startDateCalendar.render();

            return this;
        },
        destructor: function () {
            var calNode = this.getInput('startDate');
            if (this.startDateCalendar) {
                //destroy calendar
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }

            if (calNode) {
                //destroy node
                calNode.destroy();
                calNode = null;
            }
        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#relationship_' + inputId);
        }
    });

    // Extend the USP UpdateClosePersonPersonRelationshipView
    var RelationshipSuggestionsView = Y.Base.create('RelationshipSuggestionsView', Y.usp.relationship.SuggestedPersonPersonRelationshipsView, [], {

        template: Y.Handlebars.templates["relationshipSuggestionsDialog"],

        events: {
            '.pure-button.suggestions': {
                click: 'saveRelationship'
            },
            '.suggestion-hd': {
                click: 'toggleSelection'
            },
            '.roleQualifiers input': {
                click: 'updateRelationshipStartDate'
            }
        },

        render: function () {
            var container = this.get('container'),
                suggestions = this.get("model").get("suggestions"),
                suggestionsClientIds = this.get("model").get("suggestionsClientIds"),
                calenders = [];

            //call superclass
            //Removed as you are overwriting the HTML anyway!
            //RelationshipSuggestionsView.superclass.render.call(this);

            // Render this view's HTML into the container element.
            container.setHTML(this.template({
                labels: this.get('labels'),
                suggestions: suggestions
            }));

            suggestionsClientIds.forEach(function (clientId) {
                var myCalendar = new Y.usp.CalendarPopup({
                    inputNode: container.one('.date_' + clientId + ' #relationship_startDate')
                }).render();

                calenders.push(myCalendar);
            });

            this.allCalenders = calenders;

            return this;
        },

        destructor: function () {

            var container = this.get('container');
            var suggestionsClientIds = this.get("model").get("suggestionsClientIds");
            suggestionsClientIds.forEach(function (clientId) {
                var startDateNode = container.one('.date_' + clientId + ' #relationship_startDate');
                if (startDateNode) {
                    //destroy node
                    startDateNode.destroy();
                    startDateNode = null;
                }
            });
            var calenders = this.allCalenders;
            calenders.forEach(function (calender) {
                if (calender) {
                    //destroy node
                    calender.destroy();
                    calender = null;
                }
            });
        },

        updateRelationshipStartDate: function (e) {

            var t = e.currentTarget;
            var qualifierId = t.getAttribute('id');

            var suggestion = e.currentTarget.ancestor('.suggestions');
            var formId = suggestion.getData('clientId');

            var isHalf = false;
            var isStep = false;
            var isInLaw = false;
            var isAdoptive = false;

            var half = suggestion.one('#half');
            var step = suggestion.one('#step');
            var inLaw = suggestion.one('#inLaw');
            var adoptive = suggestion.one('#adoptive');

            if (half !== null) {
                isHalf = half.get('checked');
            }
            if (step !== null) {
                isStep = step.get('checked');
            }
            if (inLaw !== null) {
                isInLaw = inLaw.get('checked');
            }
            if (adoptive !== null) {
                isAdoptive = adoptive.get('checked');
            }

            if (isStep || isAdoptive || isInLaw) {
                suggestion.one('#relationship_startDate').set('value', '');
                suggestion.one('#relationship_startDateEstimated').set('checked', false);
            }

            if (!isStep && !isAdoptive && !isInLaw) {
                this.get("model").get("suggestions").forEach(function (personDetails) {
                    var personSuggestions = personDetails.personSuggestions;
                    personSuggestions.forEach(function (suggestiondata) {
                        var clientId = suggestiondata.clientId;
                        if (clientId === formId) {
                            if (suggestiondata.relationshipStartDate) {
                                suggestion.one('#relationship_startDate').set('value', suggestiondata.relationshipStartDate);
                            }
                            if (suggestiondata.startDateEstimated) {
                                suggestion.one('#relationship_startDateEstimated').set('checked', true);
                            }
                        }
                    });
                });
            }

        },

        toggleSelection: function (e) {
            e.preventDefault();
            var t = e.currentTarget;
            var id = t.getAttribute('id');
            var parentNode = Y.one("#" + id).ancestor();
            Y.all('.suggestion-bd').removeClass('selected');
            var module = Y.one('#' + parentNode.get('id'));
            module.one('.suggestion-bd').addClass('selected');
            //@TODO since css3 is not supported by IE9 - the below code can be used to fix the slideUp/sildeDown in IE9
            /*var content = module.one('.suggestion-bd').plug(Y.Plugin.NodeFX, {
		    	from: { height: 0 },
		        to: {
		        	height: 75
		        },
		        easing: Y.Easing.easeOut,
		        duration: 0.5
		    });
		    content.fx.set('reverse', !content.fx.get('reverse')); // toggle reverse
	        content.fx.run();*/
        },

        saveRelationship: function (e) {

            e.preventDefault();
            var t = e.currentTarget;
            var buttonId = t.getAttribute('id');

            //Mix in ModelFormLink to model where necessary
            //URGH - why is this embeded inside the event handler? Shouldn't this be set up as a static definition?
            var NewPersonPersonRelationForm = Y.Base.create("NewPersonPersonRelationForm", Y.usp.relationship.NewPersonPersonRelationship, [Y.usp.ModelFormLink], {
                form: '#SuggestedPersonPersonRelationshipForm_28'
            });

            var container = this.get('container');
            var panelPopup = this.get("panelPopup");
            var model = this.get("model");
            var nominatedPersonId = model.get("id");
            var nominatedPersonName = this.get("personName");
            var addSuggestRelationshipUrl = this.get('addSuggestRelationshipUrl');
            panelPopup.showDialogMask();
            
            this.get("model").get("suggestions").forEach(function (personDetails) {

                var personSuggestions = personDetails.personSuggestions;
                personSuggestions.forEach(function (suggestion) {

                    // This code must be revisited/changed when working on Parent/Child Relations
                    // The Current code is for Sibling and Union(Engaged,Married..etc) Relations
                    var clientId = suggestion.clientId;
                    if (clientId === buttonId) {

                        var newRelationshipForm = new NewPersonPersonRelationForm();

                        //set the url
                        newRelationshipForm.url = addSuggestRelationshipUrl;
                        if (suggestion.firstPersonRole === null || suggestion.firstPersonRole === 'ROLE_A') {
                            newRelationshipForm.set('roleAPersonId', suggestion.roleAPersonId);
                            newRelationshipForm.set('roleBPersonId', suggestion.roleBPersonId);
                        } else {
                            newRelationshipForm.set('roleBPersonId', suggestion.roleAPersonId);
                            newRelationshipForm.set('roleAPersonId', suggestion.roleBPersonId);
                        }
                        if (typeof suggestion.personPersonRelationshipTypeId !== "undefined") {
                            newRelationshipForm.set('personPersonRelationshipTypeId', suggestion.personPersonRelationshipTypeId);
                        } else {
                            newRelationshipForm.set('personPersonRelationshipTypeId', container.one('#' + clientId + ' input[name=unionRelationshipType]:checked').get("value"));
                            //Y.one("#SuggestedPersonPersonRelationshipForm_27  input[name=unionRelationshipType]:checked").get("value");
                        }

                        var startDate = container.one('#' + clientId + ' #relationship_startDate');
                        if (startDate !== null) {
                            newRelationshipForm.set('startDate', startDate.get('value'));
                        }

                        var startDateEstimated = container.one('#' + clientId + ' #relationship_startDateEstimated');
                        if (startDateEstimated !== null) {
                            newRelationshipForm.set('startDateEstimated', startDateEstimated.get('checked'));
                        }

                        var qualifiers = '';

                        var half = container.one('#' + clientId + ' #half');
                        if (half !== null) {
                            newRelationshipForm.set('half', half.get('checked'));
                            if (half.get('checked')) {
                                qualifiers = qualifiers + 'half-';
                            }
                        }
                        var step = container.one('#' + clientId + ' #step');
                        if (step !== null) {
                            newRelationshipForm.set('step', step.get('checked'));
                            if (step.get('checked')) {
                                qualifiers = qualifiers + 'step-';
                            }
                        }
                        var inLaw = container.one('#' + clientId + ' #inLaw');
                        if (inLaw !== null) {
                            newRelationshipForm.set('inLaw', inLaw.get('checked'));
                            if (inLaw.get('checked')) {
                                qualifiers = qualifiers + 'inLaw-';
                            }
                        }
                        var adoptive = container.one('#' + clientId + ' #adoptive');
                        if (adoptive !== null) {
                            newRelationshipForm.set('adoptive', adoptive.get('checked'));
                            if (adoptive.get('checked')) {
                                qualifiers = qualifiers + 'adoptive-';
                            }
                        }
                        if (qualifiers.length > 0) {
                            qualifiers = qualifiers.substring(0, qualifiers.length - 1);
                        }

                        newRelationshipForm.set('professionalTeamId', suggestion.professionalTeamId);
                        
                        newRelationshipForm.set('saveAutomaticRelationships', true);

                        // added relationshipType & selectedPerson only for the purpose of the form validation error messages
                        // id user do not select any person and relationship type and submits the form - these values are checked
                        // in constraints-relationshipsrecording.xml
                        //not saved in the database
                        if (typeof suggestion.personPersonRelationshipTypeId !== "undefined") {
                            newRelationshipForm.set('relationshipType', suggestion.personPersonRelationshipTypeId);
                        } else {
                            newRelationshipForm.set('relationshipType', container.one('#' + clientId + ' input[name=unionRelationshipType]:checked').get("value"));
                            //Y.one("#SuggestedPersonPersonRelationshipForm_27  input[name=unionRelationshipType]:checked").get("value");
                        }

                        newRelationshipForm.set('selectedPerson', suggestion.roleBPersonId);

                        newRelationshipForm.form = "#" + clientId;
                        var relationshipText = '';

                        if (suggestion.hasAnyQualifiers) {
                          relationshipText = suggestion.roleAForename + ' ' + suggestion.roleASurname + ' is ' + qualifiers + ' ' + suggestion.relationshipRole + ' ' + suggestion.roleBForename + ' ' + suggestion.roleBSurname;
                        } else {
                            if (suggestion.relationship === 'union with') {
                              var unionType = container.one('#' + clientId + ' input[name=unionRelationshipType]:checked').get('parentElement').get('outerText');
                              relationshipText = suggestion.roleAForename + ' ' + suggestion.roleASurname + ' is ' + unionType + ' of ' + suggestion.roleBForename + ' ' + suggestion.roleBSurname;
                            } else {
                              relationshipText = suggestion.roleAForename + ' ' + suggestion.roleASurname + ' is ' + suggestion.relationship + ' ' + suggestion.roleBForename + ' ' + suggestion.roleBSurname;
                            }
                        }
                        
                        if (suggestion.professionalTeamName) {
                          relationshipText = relationshipText + ' ' + ' associated with ' + suggestion.professionalTeamName;	
                        }
                        
                        newRelationshipForm.save(function (err, response) {

                            panelPopup.hideDialogMask();

                            var suggestionsNode = Y.one('#suggestionsOverlayAlertMessage');
                            if (suggestionsNode) {
                                suggestionsNode.setHTML('<p><div class="message message-block message-error"></div></p>');
                            }

                            var validationNode = Y.one('.yui3-widget-bd .message-error');
                            if (validationNode) {
                                validationNode.remove();
                            }

                            Y.log("suggestion.save response: " + response);
                            if (err === null) {

                                container.one('#success_relationship_' + clientId).setHTML(relationshipText);
                                container.one('#li_' + clientId).hide();
                                container.one('#success_' + clientId).show('fadeIn', {
                                    easing: 'ease-in',
                                    duration: 0.3
                                });

                                Y.fire('infoMessage:message', {
                                    message: 'Relationship successfully saved'
                                });

                                var responseData = Y.JSON.parse(response.responseText);

                                var AUTO_GENERATED_ID, // Constant holding an ID used by session storage
                                    automaticRelationships = [], // an array of new automatic relationships
                                    automaticRelationshipsList = [], // an array of existing automatic relationships
                                    Storage; // Session storage object

                                AUTO_GENERATED_ID = 'automaticRelationshipsList';
                                automaticRelationships = responseData.secondaryResourceURIs; // Our automatic relationship id's begin life as url's
                                Storage = Y.uspSessionStorage;

                                if (nominatedPersonId && (nominatedPersonId == suggestion.roleAPersonId || nominatedPersonId == suggestion.roleBPersonId)) {
                                    // If there are automatic relationships....
                                    if (automaticRelationships.length > 0) {

                                        // ..and existing automatic relationships to show....
                                        if (Storage.hasStoreValue(AUTO_GENERATED_ID)) {
                                            automaticRelationshipsList = Y.JSON.parse(Storage.getStoreValue(AUTO_GENERATED_ID));
                                        }

                                        automaticRelationships.forEach(function (url) { // extract id from url
                                            var id = url.split('/').slice(-1).toString();
                                            automaticRelationshipsList.push(id);
                                        });

                                        // ..store automatic relationships
                                        Storage.setStoreValue(AUTO_GENERATED_ID, Y.JSON.stringify(automaticRelationshipsList));
                                    }

                                    Y.fire('relationship:dataChanged', {
                                        action: 'add'
                                    });
                                }

                                //show filter message
                                // show infom message if the saved suggestion may not be visible because of filter options & secondary button(off)

                                var relationshipType = suggestion.relationshipType;
                                var relationshipTypeUnion = suggestion.relationship;
                                if (relationshipTypeUnion === 'union with') {
                                    var unionSuggestions = suggestion.unionRelationshipRoles;
                                    unionSuggestions.forEach(function (unionSuggestion) {
                                        if (unionSuggestion.personPersonRelationshipTypeId === newRelationshipForm.get('personPersonRelationshipTypeId')) {
                                            relationshipType = unionSuggestion.relationshipType;
                                        }
                                    });
                                }
                                var relationshipStartDate = Y.USPDate.parseDate(startDate.get('value'));

                                var filterMessage = Y.uspRelationshipHelper.getFilterMessage(nominatedPersonId, suggestion.roleAPersonId, suggestion.roleBPersonId, relationshipStartDate, relationshipType);

                                if (filterMessage) {
                                    filterMessage = filterMessage + ' to view the saved suggestion.</br>';
                                }
                                // show message if any automatic relations are created

                                //var personId =  suggestion.roleAPersonId;

                                var infomessage = null;
                                if (automaticRelationships.length > 0) {

                                    var automaticRelationshipIdsArray = [];
                                    automaticRelationships.forEach(function (automaticRelationship) {
                                        var lastSlash = automaticRelationship.lastIndexOf("/");
                                        automaticRelationshipIdsArray.push(automaticRelationship.substring(lastSlash + 1));
                                    });

                                    var queryString = '';
                                    automaticRelationshipIdsArray.forEach(function (automaticRelationshipId) {
                                        queryString = queryString + '&relationshipIds=' + automaticRelationshipId;
                                    });

                                    var url = this.get('addPersonUrl');

                                    var firstAmp = queryString.indexOf("&");

                                    url = url + nominatedPersonId + '/personRelationship?' + queryString.substring(firstAmp + 1);

                                    var totalNumberOfNewRelationships = automaticRelationships.length;
                                    var relationshipsInNetwork = 0;

                                    Y.io(url, {
                                        sync: true,
                                        on: {
                                            success: function (id, e) {
                                                relationshipsInNetwork = Y.JSON.parse(e.responseText);
                                            }
                                        }
                                    });

                                    //call ajax/rest cal to get no. new relationships in the nominated person's network

                                    var relationshipsOutsideNetwork = totalNumberOfNewRelationships - relationshipsInNetwork;

                                    infomessage = 'The application also created ' + totalNumberOfNewRelationships + ' new relationship(s)';
                                    if (relationshipsInNetwork > 0 && relationshipsOutsideNetwork > 0) {
                                        infomessage = infomessage + ' - ' + relationshipsInNetwork + ' within and ' + relationshipsOutsideNetwork + ' outside ' + nominatedPersonName + "'s network.";
                                    } else if (relationshipsInNetwork > 0) {
                                        infomessage = infomessage + ' within ' + nominatedPersonName + "'s network.";
                                    } else if (relationshipsOutsideNetwork > 0) {
                                        infomessage = infomessage + ' outside ' + nominatedPersonName + "'s network.";
                                    }

                                }

                                if (filterMessage || infomessage) {
                                    if (filterMessage == null) {
                                        filterMessage = '';
                                    }
                                    if (infomessage == null) {
                                        infomessage = '';
                                    }
                                    Y.one('#suggestionsOverlayAlertMessage').setHTML('<p><div class="message message-block message-info message-closable" role="alert"><a href="#" class="close" title="Close">Close</a><h4>' + filterMessage + ' ' + infomessage + '</h4></div></p>').show('fadeIn');
                                }

                            } else {
                                Y.log("suggestion.save error: " + err);
                                e.error = err;
                                e.response = response;
                                var alertType = 'error';
                                panelPopup.fire(alertType, e);
                            }

                            Y.fire('relationship:dataChanged', {
                                action: 'add',
                                preventClear: true
                            });

                            if (Y.one('.usp-graph-content')) {
                                window.scrollTo(0, 0);
                            }

                        });

                    }
                });
            });
        }

    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {

        }
    });

    Y.namespace('app.relationships').NewRelationshipView = NewRelationshipView;
    Y.namespace('app.relationships').NewRelationshipTypeView = NewRelationshipTypeView;
    Y.namespace('app.relationships').CloseRelationshipView = CloseRelationshipView;
    Y.namespace('app.relationships').UpdateRelationshipView = UpdateRelationshipView;
    Y.namespace('app.relationships').ViewRelationshipDetailView = ViewRelationshipDetailView;
    Y.namespace('app.relationships').DeleteRelationshipDetailView = DeleteRelationshipDetailView;
    Y.namespace('app.relationships').RelationshipSuggestionsView = RelationshipSuggestionsView;
    Y.namespace('app.relationships').SaveSuggestedRelationshipView = SaveSuggestedRelationshipView;
    Y.namespace('app.relationships').RelatedProfessionalTeamSelectView = RelatedProfessionalTeamSelectView;

}, '0.0.1', {
    requires: ['yui-base',
        'escape',
        'event-custom',
        'handlebars-helpers',
        'handlebars-relationship-templates',
        'popup-autocomplete-plugin',
        'person-name-matcher-highlighter',
        'usp-relationship-NewPersonPersonRelationship',
        'usp-relationship-UpdatePersonPersonRelationship',
        'usp-relationship-UpdateClosePersonPersonRelationship',
        'usp-relationship-SuggestedPersonPersonRelationships',
        'usp-relationshipsrecording-AsymmetricPersonPersonRelationshipDetail',
        'usp-person-Person',
        'usp-person-PersonWithAddressAndOtherNames',
        'form-util',
        'calendar-popup',
        'model-form-link',
        'datasource-io',
        'datasource-jsonschema',
        'usp-date',
        'caserecording-results-formatters',
        'relationships-attributes-panel',
        'app-mixin-multi-view',
        'app-views-narrative',
        'person-autocomplete-view',
        'categories-relationship-component-PersonPersonRelationshipClosureReason',
        'base-addedit-relationship-view'
    ]
});