YUI.add('groupofrights-dialog-views', function(Y) {
    var A = Y.Array, E=Y.Escape;
    
    function BaseGroupOfRightsAddEditView() {
    }
    
    BaseGroupOfRightsAddEditView.prototype = {
        initRightsPicklist : function(pickListContainer, allRightsJSON, assignedRights) {
            // set up the data for our rights picklist
            var assignedRightsIds = [], availableRights = [], currentAssignedRights = [];
            // set up the assignedRights and currentAssignedRights
            A.each(assignedRights, function(rightsData) {
                assignedRightsIds.push(rightsData.id);
                currentAssignedRights.push({
                    id : rightsData.id,
                    name : E.html(rightsData.name),
                    title : E.html(rightsData.name)
                });
            });
            
            // filter out the assigned to create the availableRights
            A.each(allRightsJSON, function(rightsData) {
                if (A.indexOf(assignedRightsIds, rightsData.id) === -1) {
                    availableRights.push({
                        id : rightsData.id,
                        name : E.html(rightsData.name),
                        title : E.html(rightsData.name)
                    });
                }
            });
            
            var rightsPicklist = new Y.usp.Picklist({
                optionsMap : {
                    text : 'name',
                    value : 'id',
                    title : 'title'
                },
                options : availableRights,
                preserveOrder : true,
                stackMode : true,
                selectWidth : '100%',
                actionLabelOne : '>',
                actionLabelAll : '>>',
                actionLabelRmv : '<',
                actionLabelRmvAll : '<<'
            });
            
            // Setup the rights for picklist
            rightsPicklist.set('options', availableRights);
            
            var order = [];
            
            Y.Array.each(allRightsJSON, function(item) {
                order.push(item.name);
            });
            
            rightsPicklist.set('order', order);
            rightsPicklist.set('selections', currentAssignedRights);
            rightsPicklist.set('assignedId', 'rightIds');
            rightsPicklist.render();
            
            // stick the picklist container into the form
            pickListContainer.appendChild(rightsPicklist.get('srcNode'));
            
            return rightsPicklist;
        },
        _preventSubmit : function(e) {
            e.preventDefault();
        }
    };
    
    // Group of rights Add View definition
    Y.namespace('app').GroupOfRightsAddView = Y.Base.create('groupOfRightsAddView', Y.usp.security.NewSecurityGroupOfRightsView, [
        BaseGroupOfRightsAddEditView
    ], {
        template : Y.Handlebars.templates.groupofrightsAddDialog,
        events : {
            '#groupOfRightsAddForm' : {
                submit : '_preventSubmit'
            }
        },
        
        render : function() {
            var container = this.get('container');
            
            // call superclass
            Y.app.GroupOfRightsAddView.superclass.render.call(this);
            
            // set up the rights picklist
            this.rightsPicklist = this.initRightsPicklist(container.one('#rightsPicklist'), this.get('availableRightsJSON'), []);
            
            return this;
        },
        destructor : function() {
            if (this.rightsPicklist) {
                // destroy the picklist
                this.rightsPicklist.destroy();
                delete this.rightsPicklist;
            }
        }
    
    });
    
    // Mix in the ModelFormLink to the NewRight model
    Y.namespace('app').NewGroupOfRightsForm = Y.Base.create('newGroupOfRightsForm', Y.usp.security.NewSecurityGroupOfRights, [
        Y.usp.ModelFormLink
    ], {
        form : '#groupOfRightsAddForm'
    });
    
    // Group of rights update view definition
    Y.namespace('app').GroupOfRightsUpdateView = Y.Base.create('groupOfRightsUpdateView', Y.usp.security.UpdateSecurityGroupOfRightsWithRightsView, [BaseGroupOfRightsAddEditView], {
        // bind template
        template : Y.Handlebars.templates.groupofrightsEditDialog,
        events : {
            '#groupOfRightsEditForm' : {
                submit : '_preventSubmit'
            }
        },
        getInput : function(inputId) {
            var container = this.get('container');
            return container.one('#editGroupOfRights_' + inputId);
        },
        render : function() {
            var container = this.get('container'), labels = this.get('labels'), html = this.template({
                labels : labels,
                modelData : this.get('model').toJSON(),
                targetIdentifier : this.get('targetIdentifier')
            });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            
            // set up the groupOfRights picklist
            this.rightsPicklist = this.initRightsPicklist(container.one('#rightsPicklist'), this.get('availableRightsJSON'), this.get('model').get('rights'));
            
            return this;
        },
        destructor : function() {
            
            if (this.rightsPicklist) {
                this.rightsPicklist.destroy();
                delete this.rightsPicklist;
            }
        }
    });
    
    // Model will be transmogrified into an UpdateGroupOfRights
    Y.namespace('app').UpdateGroupOfRightsForm = Y.Base.create('updateGroupOfRightsForm', Y.usp.security.SecurityGroupOfRightsWithRights, [
        Y.usp.ModelFormLink, Y.usp.ModelTransmogrify
    ], {
        form : '#groupOfRightsEditForm'
    });
    
    // group of rights Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').GroupOfRightsDialog = Y.Base.create('groupOfRightsDialog', Y.usp.MultiPanelPopUp, [], {
        
        initializer : function() {
            // plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        destructor : function() {
            // unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        // Views that this dialog supports
        views : {
            addGroupOfRights : {
                type : Y.app.GroupOfRightsAddView,
                buttons : [
                    {
                        section : Y.WidgetStdMod.FOOTER,
                        name : 'saveButton',
                        labelHTML : '<i class="fa fa-check"></i> Save',
                        classNames : 'pure-button-primary',
                        action : function(e) {
                            e.preventDefault();
                            // show the mask
                            this.showDialogMask();
                            
                            // Get the view and the model
                            var view = this.get('activeView'), model = view.get('model'), labels = view.get('labels');
                            
                            // set group of rights from rightListPicklist
                            var rightIds = view.rightsPicklist.getSelectedValues();
                            model.set('rightIds', rightIds, {
                                silent : true
                            });
                            model.set('active', true, {
                                silent : true
                            });
                            // Do the save
                            model.save(Y.bind(function(err, response) {
                                // hide the mask
                                this.hideDialogMask();
                                
                                if (err === null) {
                                    this.hide();
                                    
                                    // fire saved event against the view
                                    view.fire('saved', {
                                        successMessage : labels.addGroupOfRightsSuccessMessage
                                    });
                                }
                            }, this));
                            
                            // hide the mask
                            this.hideDialogMask();
                        },
                        disabled : true
                    }
                ]
            },
            editGroupOfRights : {
                type : Y.app.GroupOfRightsUpdateView,
                buttons : [
                    {
                        section : Y.WidgetStdMod.FOOTER,
                        name : 'saveButton',
                        labelHTML : '<i class="fa fa-check"></i> Save',
                        classNames : 'pure-button-primary',
                        action : function(e) {
                            e.preventDefault();
                            // show the mask
                            this.showDialogMask();
                            // Get the view and the model
                            var view = this.get('activeView'), model = view.get('model'), labels = view.get('labels');
                            
                            // set the GroupOfRights assigned Rights from rightsListPicklist
                            var rightIds = view.rightsPicklist.getSelectedValues() || [];
                            model.set('rightIds', rightIds, {
                                silent : true
                            });
                            
                            // set the url before saving the model
                            model.url = view.get('updateUrl');
                            
                            // Do the save
                            model.save({
                                transmogrify : Y.usp.security.UpdateSecurityGroupOfRightsWithRights
                            }, Y.bind(function(err, response) {
                                // hide the mask
                                this.hideDialogMask();
                                if (err === null) {
                                    this.hide();
                                    // fire saved event against the view
                                    view.fire('saved', {
                                        id : model.get('id'),
                                        successMessage : labels.editGroupOfRightsSuccessMessage
                                    });
                                }
                            }, this));
                        },
                        disabled : true
                    }
                ]
            }
        }
    
    }, {
        CSS_PREFIX : 'yui3-panel',
        ATTRS : {
            width : {
                value : 560
            },
            buttons : {
                value : [
                    {
                        name : 'cancelButton',
                        labelHTML : '<i class="fa fa-times"></i> Cancel',
                        classNames : 'pure-button-secondary',
                        action : function(e) {
                            e.preventDefault();
                            this.hide();
                        },
                        section : Y.WidgetStdMod.FOOTER
                    }, 'close'
                ]
            }
        }
    });
    
}, '0.0.1', {
    requires : [
        'yui-base', 
        'multi-panel-popup', 
        'multi-panel-model-errors-plugin', 
        'model-form-link', 
        'model-transmogrify', 
        'form-util', 
        'handlebars-helpers', 
        'handlebars-groupofrights-templates', 
        'picklist', 
        'usp-security-SecurityGroupOfRights', 
        'usp-security-NewSecurityGroupOfRights', 
        'usp-security-SecurityGroupOfRightsWithRights', 'usp-security-SecurityRight',
        'usp-security-UpdateSecurityGroupOfRightsWithRights',
        'escape'
    ]
});