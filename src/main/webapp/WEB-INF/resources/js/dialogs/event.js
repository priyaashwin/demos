YUI.add('event-dialog-views', function(Y) {
    var A = Y.Array,
        O = Y.Object,
        L=Y.Lang;
            
    Y.namespace('app.event').Calendar = Y.Base.create('Calendar', Y.View, [], {
        events: {
            '#searchButton':{click: 'searchClicked'},
            '#createButton':{click: 'createClicked'}
        },
    	template:Y.Handlebars.compile(Y.one('#addEventDialog').getHTML()),	
        render:function(){
        	Y.log('Calendar renderedy');
        	 this.get('container').setHTML(this.template());
        	
        //Y.View.constructor.superclass.render.call(this);
      
        },
        searchClicked: function(){
            this.fire('searchClicked');
        },
        createClicked: function(){
            this.fire('createClicked');
        }
    });

    Y.namespace('app.event').eventSearch = Y.Base.create('EventSearch', Y.View, [], {
        events: {
           '#testButton':{click: 'testClicked'}
        },
        template:Y.Handlebars.compile(Y.one('#searchEventDialog').getHTML()),  
        render:function(){
            Y.log('Event Search renderedy');
             this.get('container').setHTML(this.template());

             this.startDateCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('fromDate'),
                minimumDate: new Date(1900,1,1)
            });

            this.endDateCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('toDate'),
                minimumDate: new Date(1900,1,1)
            });

             this.startDateCalendar.render();
             this.endDateCalendar.render();
            
        //Y.View.constructor.superclass.render.call(this);
      
        },
        getInput: function(id) {
            var container = this.get('container');
            return container.one('#' + id + '-input');
        }, 
        testClicked: function(e){e.preventDefault(); 
            this.fire('testClicked',{name:'test variables'}); 

        }
    });

    Y.namespace('app.event').eventCreate = Y.Base.create('EventCreate', Y.View, [], {
        events: {
            // '#searchButton':{click: 'searchClicked'}
             '#testButton2':{click: 'testClicked'},
             '#newEvent':{click: 'newEventClicked'}
        },
        template:Y.Handlebars.compile(Y.one('#createEventDialog').getHTML()),  
        render:function(date){
            Y.log('Event Create rendered');
            // alert(this.get('date'));
             this.get('container').setHTML(this.template());

             this.startDateCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('from'),
                minimumDate: new Date(1900,1,1)
            });
            this.startDateCalendar.render();
        //Y.View.constructor.superclass.render.call(this);
      
        },
        getInput: function(id) {
            var container = this.get('container');
            return container.one('#' + id + '-input');
        }, 
        searchClicked: function(){
            // this.fire('searchClicked');
        },
        testClicked: function(e){e.preventDefault(); 
            this.fire('wizardClicked',{name:'test variables'}); 
            

        },
        newEventClicked: function(e){e.preventDefault(); 
            this.fire('wizardClicked',{name:'test variables'}); 
            

        }


    });

      Y.namespace('app.event').eventWizardOne = Y.Base.create('eventWizardOne', Y.View, [], {
        events: {
           '#testButton2':{click: 'testClicked'}
        },
        template:Y.Handlebars.compile(Y.one('#eventWizardOneDialog').getHTML()),  
        render:function(){
            Y.log('Event Search renderedy');
             this.get('container').setHTML(this.template());

            this.startRangeCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('fromDate'),
                minimumDate: new Date(1900,1,1)
            });
            
            this.endRangeCalendar = new Y.usp.CalendarPopup({
                //bind to start date field
                inputNode: this.getInput('startRange'),
                minimumDate: new Date(1900,1,1)
            });

            this.endRangeCalendar.render();
            this.startRangeCalendar.render();


             
            
        //Y.View.constructor.superclass.render.call(this);
      
        },
        getInput: function(id) {
            var container = this.get('container');
            return container.one('#' + id + '-input');
        },
        testClicked: function(e){e.preventDefault(); 
            //this.fire('wizardClicked',{name:'test variables'});       

        }
    });


     

   Y.namespace('app.event').newAppointment = Y.Base.create('newAppointment', Y.View, [], {
        events: {
           '#testButton2':{click: 'testClicked'}
        },
        template:Y.Handlebars.compile(Y.one('#eventAppointmentDialog').getHTML()),  
        render:function(){
            Y.log('Event Search renderedy');
             this.get('container').setHTML(this.template());

             
            
        //Y.View.constructor.superclass.render.call(this);
      
        },
  
        testClicked: function(e){e.preventDefault(); 
            //this.fire('wizardClicked',{name:'test variables'}); 

           

        }
    });

   Y.namespace('app.event').eventWizardTwo = Y.Base.create('eventWizardTwo', Y.View, [], {
        events: {
           '#testButton2':{click: 'testClicked'}
        },
        template:Y.Handlebars.compile(Y.one('#eventWizardTwoDialog').getHTML()),  
        render:function(){
            Y.log('Event Wizard2 renderedy');
             this.get('container').setHTML(this.template());

             
            
        //Y.View.constructor.superclass.render.call(this);
      
        },
  
        testClicked: function(e){e.preventDefault(); 
            //this.fire('wizardClicked',{name:'test variables'}); 
          

        }
    });

   Y.namespace('app.event').insertAppointment = Y.Base.create('insertAppointment', Y.View, [], {
        events: {
           '#insert-appointment':{click: 'insertClicked'}
        },
        template:Y.Handlebars.compile(Y.one('#insertAppTemplateDialog').getHTML()),  
        render:function(){
            Y.log('Event insert rendered');
             this.get('container').setHTML(this.template());

             
            
        //Y.View.constructor.superclass.render.call(this);
      
        },
  
        
    });

   Y.namespace('app.event').eventTemplate = Y.Base.create('eventTemplate', Y.View, [], {
        events: {
          '#insert-appointment':{click: 'insertClicked'}
        },
        template:Y.Handlebars.compile(Y.one('#eventTemplateDialog').getHTML()),  
        render:function(){
            Y.log('Event template renderedy');
            this.get('container').setHTML(this.template());
            var tabview = new Y.TabView({srcNode:this.get('container').one('#demo')});
            //setTimeout(function(){ tabview.render();}, 3000);

            //tabview.render();

        //Y.View.constructor.superclass.render.call(this);
      
        },
  
        insertClicked: function(e){
             e.preventDefault();
             
            this.fire('insertAppointmentClicked',{name:'test variables'}); 
           
        }
    });

    
}, '0.0.1', {
    requires: ['yui-base','view','handlebars','calendar-popup','tabview']
});