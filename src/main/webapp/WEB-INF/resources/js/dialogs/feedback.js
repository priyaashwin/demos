YUI.add('feedback-dialog-views', function (Y) {
	
	var IFrameTemplate='<iframe src="{url}" width="99%" height="450"></src>';
	
	var FeedbackView=Y.Base.create('FeedbackView', Y.View,[],{
		//bind template	
		//template: Y.Handlebars.templates["imageUploadView"],
		render: function() {
			var url = this.get('url');
			this.get('container').setHTML(
					Y.Lang.sub(IFrameTemplate,{
						url:url
					}));
		},
		destructor: function(){
			window.onbeforeunload=null;
		}
		
	},{
        // Specify attributes and static properties for your View here.
        ATTRS: {
        	params:{}
        }
	});
	

	Y.namespace('usp').FeedbackView=FeedbackView;
	
}, '0.0.1', {
    requires: ['yui-base','event-custom']
});