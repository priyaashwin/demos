YUI.add('casenote-dialog', function(Y) {

    Y.namespace('app.casenote').NewCaseNoteEntryForm = Y.Base.create("newCaseNoteEntryForm", Y.usp.casenote.NewCaseNoteEntry, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify, Y.app.source.FormWithSourceMixin], {
        form: '#caseNoteEntryAddForm',
        customValidation: function(attrs) {
            var errors = {},
                eventDate = attrs.eventDate || {},
                labels = this.labels || {};

            //No event date is valid, but if there is one it must be in valid range
            if (eventDate && (eventDate.year < 1886 || eventDate.year > (new Date().getFullYear()))) {
                errors['fuzzyEventDateYEAR'] = this.labels.eventDateValidationErrorMessage;
            }

            if ((attrs.event || '').trim() === '') {
                errors['event'] = labels.entryValidationErrorMessage;
            }
            if ((attrs.entryType || '') === '') {
                errors['entryType'] = labels.entryTypeValidationErrorMessage;
            }

            return errors;
        }
    }, {
        ATTRS: {
            visibleToAll: {
                //default value
                value: true
            },
            impact: {
                value: 'UNKNOWN'
            },
            personVisibility: {
                value: []
            },
            peopleSeen: {
                value: []
            },
            peopleSeenAlone: {
                value: []
            }
        }
    });

    Y.namespace('app.casenote').UpdateCaseNoteEntryForm = Y.Base.create("updateCaseNoteEntryForm", Y.usp.casenote.CaseNoteEntryWithVisibility, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify, Y.app.source.FormWithSourceMixin], {
        form: '#caseNoteEntryEditForm'
    });
    
    Y.namespace('app.casenote').UpdateNotifyCaseNoteEntryForm = Y.Base.create("updateNotifyCaseNoteEntryForm", Y.usp.casenote.UpdateNotifyCaseNoteEntry,[Y.usp.ModelFormLink],{
    	form: '#caseNoteEntryNotificationForm'
    });


    Y.namespace('app.casenote').CasenoteDialog = Y.Base.create('casenoteDialog', Y.app.entry.BaseEntryDialog, [], {
        entryCreateModelClass: Y.usp.casenote.NewCaseNoteEntry,
        entryCreateWithVisbilityModelClass: Y.usp.casenote.NewCaseNoteEntryWithVisibility,
        entryUpdateDraftModelClass: Y.usp.casenote.UpdateDraftCaseNoteEntry,
        entryUpdateCompleteModelClass: Y.usp.casenote.UpdateCompleteCaseNoteEntry,
        entryUpdateWithVisbilityModelClass: Y.usp.casenote.UpdateCaseNoteEntryWithVisibility,
        views: {
            add: {
                type: Y.app.casenote.NewCaseNoteEntryView,
                //override buttons
                buttons: [{
                    name: 'uploadFileButton',
                    labelHTML: '<i class="fa fa-upload"></i> Upload',
                    isActive: true,
                    action: function(e) {
                        e.preventDefault();
                        this.fire('addAttachment', {
                            temporaryAttachment: true,
                            action: 'uploadAttachment'
                        });
                    }
        }].concat(Y.app.entry.BaseEntryDialog.prototype.views.add.buttons)
      },
      view:{
        type: Y.app.casenote.ViewCaseNoteEntryView
      },
      viewPeopleSeen: {
        type: Y.app.casenote.ViewCaseNoteEntryPeopleSeenView
      },
      edit:{
        type: Y.app.casenote.EditCaseNoteEntryView,
        buttons:[
        {
          name: 'uploadFileButton',
          labelHTML: '<i class="fa fa-upload"></i> Upload',
          isActive:true,
          action:function(e){
            e.preventDefault();         
        
            this.fire('addAttachment', {
              id:this.get('activeView').get('model').get('id'),
              temporaryAttachment: false,
              action: 'uploadAttachment'
            });
          }
        }].concat(Y.app.entry.BaseEntryDialog.prototype.views.edit.buttons)
      },
      editComplete:{
        type: Y.app.casenote.EditCompleteCaseNoteEntryView,
        buttons:[
                 {
                   name: 'uploadFileButton',
                   labelHTML: '<i class="fa fa-upload"></i> Upload',
                   isActive:true,
                   disabled:true,
                   action:function(e){
                     e.preventDefault();         
                 
                     this.fire('addAttachment', {
                       id:this.get('activeView').get('model').get('id'),
                       temporaryAttachment: false,
                       action: 'uploadAttachment'
                     });
                   }
                 }].concat(Y.app.entry.BaseEntryDialog.prototype.views.editComplete.buttons)
        
      },
      hardDelete:{
        type: Y.app.casenote.RemoveCaseNoteEntryView
      },
      softDelete:{
        type: Y.app.casenote.HideCaseNoteEntryView
      },      
      complete:{
        type: Y.app.casenote.CompleteCaseNoteEntryView
      },      
      uncomplete:{
        type: Y.app.casenote.UncompleteCaseNoteEntryView
      },
      sendNotification:{
    	  type: Y.app.casenote.SendNotificationView,
    	  buttons:[{
			  name:'sendNotificationButton',
			  labelHTML: '<i class="fa fa-check"></i> Send',
			  isActive:true,
			  disabled: true,				  
              action: function(e) {
            	  e.preventDefault();
            	  var view = this.get('activeView'),
            	  	  attributes;
            	  var notificationRecipients = view.get('notificationRecipientsView');
            	  if(notificationRecipients){
            		  attributes={
            				  notificationTargets:notificationRecipients.getNotificationTargetsVO()
            		  }
            	  }
        		  this.handleSave(e,attributes);
              },
		  }]
      },
    }
  });
}, '0.0.1', {
    requires: ['yui-base',
        'entry-dialog',
        'casenote-dialog-views',
        'model-form-link',
        'model-transmogrify',
        'usp-casenote-NewCaseNoteEntry',
        'usp-casenote-NewCaseNoteEntryWithVisibility',
        'usp-casenote-CaseNoteEntryWithVisibility',
        'usp-casenote-UpdateDraftCaseNoteEntry',
        'usp-casenote-UpdateCaseNoteEntryWithVisibility',
        'usp-casenote-UpdateCompleteCaseNoteEntry',
        'notification-recipients',
        'source-form'
    ]
});