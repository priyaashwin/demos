YUI.add('casenote-output-document-dialog-views', function (Y) {
    var L = Y.Lang;
    // A list of filter parameters supported by the back end 
    var supportedEntries = [
        'startDate', 'endDate', 'impactList', 'sourceList', 'sourceOrgList', 'pracList',
        'pracOrgList', 'entryTypeList', 'entrySubTypeList', 'excludeGroupEntries', 'startRecordedDate',
        'endRecordedDate', 'startEditedDate', 'endEditedDate', 'statusList', 'groupIdList', 'hasAttachments', 
        'attachmentSource', 'attachmentOtherSource', 'attachmentSourceOrganisation', 'attachmentOtherSourceOrganisation',
        'attachmentTitle', 'otherSource', 'otherSourceOrg', 'subjectSeenIndicatorList'
    ];

    Y.namespace('app').CaseNoteOutputDocumentView = Y.Base.create('caseNoteOutputDocumentView', Y.app.output.BaseOutputDocumentView, [Y.app.output.FiltersExtension], {
        getDocumentRequestModel: function () {
            var model = this.get('model');
            var filterParameters = this.get('filterModel').toJSON();
            var cleanedFilterParameters = this.cleanFilterParameters(filterParameters, supportedEntries);

            return new Y.usp.casenote.NewCaseNoteDocumentRequest(Y.merge({
                url: this.get('generateURL'),
                documentType: this.get('documentType'),
                subjectId: Number(model.get('subjectId')),
                subjectType: model.get('subjectType').toUpperCase(),
                outputTemplateId: Number(this.get('selectedTemplate')),
                outputFormat: this.get('selectedFormat')
            }, cleanedFilterParameters));
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'output-document-dialog-views',
        'output-filters-extension',
        'usp-casenote-NewCaseNoteDocumentRequest'
    ]
});