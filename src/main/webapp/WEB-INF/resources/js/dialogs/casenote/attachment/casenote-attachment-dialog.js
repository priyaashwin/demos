YUI.add('casenote-attachment-dialog', function(Y) {

    var L=Y.Lang;

    
    Y.namespace('app.casenote.attachment').UploadFileCaseNoteEntryForm = Y.Base.create("uploadFileCaseNoteEntryForm", Y.usp.casenote.NewCaseNoteEntryAttachment, [Y.usp.ModelFormLink, Y.app.source.FormWithSourceMixin], {
        form: '#uploadForm'
    });
    
    Y.namespace('app.casenote.attachment').UploadSubjectTemporaryFileCaseNoteEntryForm = Y.Base.create("uploadSubjectTemporaryFileCaseNoteEntryForm", Y.usp.casenote.NewSubjectTemporaryAttachment, [Y.usp.ModelFormLink, Y.app.source.FormWithSourceMixin], {
        form: '#uploadForm'
    });

    var downloadFile = function(e) {
        var view = this.get('activeView'),
            downloadUrl = view.get('downloadUrl'),
            model = view.get('model');
        e.preventDefault();
        window.location = Y.Lang.sub(downloadUrl, {
            id: model.get("id")
        });
    };

    Y.namespace('app.casenote.attachment').AttachmentMultiPanelPopUp = Y.Base.create('attachmentMultiPanelPopUp', Y.app.attachment.AttachmentDialog, [], {
        handleSubmitUploadFile: function(e) {
            var view = this.get('activeView'),
                isTemporaryAttachment = view.get('temporaryAttachment');

            Y.app.casenote.attachment.AttachmentMultiPanelPopUp.superclass.handleSubmitUploadFile.call(this, e).then(function(model) {
                if (isTemporaryAttachment) {
                    Y.fire('attachments:temporaryAttachmentUploaded', {
                        temporaryAttachmentId: model.get('id')
                    });
                } else {
                    Y.fire('attachments:dataChanged', {});
                }

                Y.fire('attachments:showUploadedFilesTab', {});
            });
        },
        views: {
            viewAttachment: {
                type: Y.app.casenote.attachment.CaseNoteAttachmentView,
                buttons: [{
                    section: Y.WidgetStdMod.HEADER,
                    name: 'download',
                    labelHTML: '<i class="fa fa-download" title="Download file"></i>',
                    classNames: 'attachment-download-button',
                    isText: true,
                    action: downloadFile,
                    disabled: false
                }]
            },
            deleteAttachment: {
                type: Y.app.casenote.attachment.DeleteAttachmentConfirmView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: function(e) {
                        this.handleRemove(e).then(function() {
                            Y.fire('attachments:dataChanged', {});
                        });
                    },
                    disabled: false
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            uploadFile: {
                type: Y.app.casenote.UploadFileCaseNoteEntryView
            }
        }

    });
}, '0.0.1', {
    requires: ['yui-base',
        'attachment-dialog',
        'view',
        'casenote-attachment-view',
        'casenote-dialog-views',
        'model-form-link',
        'usp-casenote-NewSubjectTemporaryAttachment',
        'usp-casenote-NewCaseNoteEntryAttachment',
        'source-form'
    ]
});