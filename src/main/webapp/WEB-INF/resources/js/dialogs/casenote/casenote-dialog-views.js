YUI.add('casenote-dialog-views', function (Y) {
        var O = Y.Object,
            L = Y.Lang;

        var EVENT_SHOW_UPLOADED_FILES_TAB = 'attachments:showUploadedFilesTab';

        var BaseCaseNoteEntryView = Y.Base.create('baseCaseNoteEntryView', Y.app.entry.BaseEntryView, [], {
            events: {
                'input[name="visibleToAll"]': {
                    change: 'handleVisibleToAllChange'
                },
                'input[type=checkbox]#select-all-members-input': {
                    change: 'toggleSelectAllMembers'
                },
                '.groupMembers input[type=checkbox]': {
                    change: 'updateSelectAllToggle'
                }
            },
            initializer: function (config) {
                this._baseCaseNoteEntryEvtHandles = [
                    Y.on(EVENT_SHOW_UPLOADED_FILES_TAB, this._selectUploadTab, this)
                ];

                if (this.get('showAttachments')) {
                    this._initializeAttachmentResults(config.attachmentsConfig);
                }
            },
            destructor: function () {
                this._baseCaseNoteEntryEvtHandles.forEach(function (handler) {
                    handler.detach();
                });
                delete this._baseCaseNoteEntryEvtHandles;
                if (this.attachmentPaginatedResults) {
                    this.attachmentPaginatedResults.removeTarget(this);
                    this.attachmentPaginatedResults.destroy();
                    delete this.attachmentPaginatedResults;
                }
            },
            toggleSelectAllMembers: function (e) {
                var container = this.get('container'),
                    groupMembersList = container.all('.groupMembers input[type=checkbox]');
                groupMembersList.each(function (groupMemberNode) {
                    groupMemberNode.set('checked', e.currentTarget.get('checked'));
                });
                this.updateSelectAllToggle();
            },
            updateSelectAllToggle: function (e) {
                var container = this.get('container'),
                    groupMembersList = container.all('.groupMembers input[type=checkbox]'),
                    selectAllNode = container.one('input[type=checkbox]#select-all-members-input');

                var checked = groupMembersList.filter('input[type=checkbox]:checked');

                if (checked.size() === groupMembersList.size()) {
                    selectAllNode.set('checked', true);
                } else {
                    selectAllNode.set('checked', false);
                }

                this.peopleSeenView.render();
            },

            _setEntrySubTypeOptions: function (category, parentCode, entrySubType) {
                var entrySubTypeCode = entrySubType ? entrySubType.code : null,
                    entrySubTypeName = entrySubType ? entrySubType.name : null;

                var currentCodedEntry = Y.uspCategory[category].entryType.category.code,
                    entrySubTypes = O.values(Y.uspCategory[category].entrySubType.category.getActiveCodedEntriesForParent(parentCode, currentCodedEntry)),
                    blankMessage = entrySubTypes.length < 1 ? 'No sub types available' : 'No sub type selected';

                if (entrySubTypes.length > 0) {
                    entrySubTypes.unshift({
                        code: '',
                        name: 'No sub type selected'
                    });
                }

                Y.FUtil.setSelectOptions(this.getNode('entrySubTypeSelect'), entrySubTypes, entrySubTypeCode, entrySubTypeName, blankMessage, true, 'code');
            },
            _initializeAttachmentResults: function (config) {
                var model = this.get('model'),
                    //substitute ID into url
                    attachmentsConfig = Y.merge(config, {
                        searchConfig: {
                            sortBy: [{
                                attachmentVersion: 'desc',
                                title: 'asc'
                            }],
                            labels: config.labels,
                            permissions: config.permissions
                        },
                        attachmentListUrl: L.sub(config.attachmentListUrl, {
                            id: model.get('id')
                        }),
                        showDeleteButton: model.get('status') === 'DRAFT'
                    });

                this.attachmentPaginatedResults = new Y.app.casenote.AttachmentResults(attachmentsConfig).addTarget(this);
                //add event handler
                this._baseCaseNoteEntryEvtHandles.push(this.attachmentPaginatedResults.after('*:load', this._updateAttachmentCountInTab, this));
            },

            renderTabs: function () {
                var container = this.get('container');
                var model = this.get('model');
                //call superclass
                BaseCaseNoteEntryView.superclass.renderTabs.call(this);

                if (this.attachmentPaginatedResults) {
                    container.one(this.get('tableId')).setHTML(this.attachmentPaginatedResults.render().get('container'));
                }

                this.peopleSeenView = new Y.app.casenote.PeopleSeenView({
                    peopleSeenContainer: container.one('#groupPeopleSeen'),
                    container: container,
                    model: this.get('model'),
                    viewType: this.name,
                    otherData: this.get('otherData'),
                    isGroup: this.get('isGroup'),
                    isEditCompleteCaseNoteEntry: this.name === 'editCompleteCaseNoteEntryView'
                });
                this.peopleSeenView.addTarget(this).render();
            },
            _selectUploadTab: function () {
                //how do you know the upload tab is tab 1??
                this._selectChildTab(1);
            },
            _updateAttachmentCountInTab: function () {
                if (this._getTab(1)) {
                    this._getTab(1).set('label', this.get("labels").attachmentTabName + ' (' + this.attachmentPaginatedResults.getTotalAttachmentsCount() + ')');
                }
            }
        }, {
            ATTRS: {
                tableId: {
                    value: '#attachmentResultsView'
                },
                showAttachments: {
                    value: false
                },
                /**
                 * @attribute tabSourceNode
                 * @type string
                 * @value '#casenoteTabs'
                 * @description Holds DOM element id for rendering tabs
                 */
                tabSourceNode: {
                    value: '#casenoteTabs',
                    getter: function (v) {
                        return this.get('container').one(v);
                    }
                }
            }
        });

        var ViewCaseNoteEntryView = Y.Base.create('viewCaseNoteEntryView', BaseCaseNoteEntryView, [], {
            template: Y.Handlebars.templates["caseNoteEntryDialog"],
            initializer: function () {
                this.sourceView = new Y.app.source.SourceView({
                    model: this.get('model'),
                    labels: this.get('labels')
                });
            },
            render: function () {
                var container = this.get('container');
                //call superclass
                ViewCaseNoteEntryView.superclass.render.call(this);

                //render the source view
                container.one('#caseNote_source').insert(this.sourceView.render().get('container'), "replace");
            },
            destructor: function () {
                this.sourceView.destroy();
                delete this.sourceView;
            }
        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        entryTypes: Y.secured.uspCategory.casenote.entryType.category.codedEntries,
                        entrySubTypes: Y.secured.uspCategory.casenote.entrySubType.category.codedEntries
                    }
                },
                formId: {
                    value: '#caseNoteEntryViewForm'
                },
                showAttachments: {
                    value: true
                }
            }
        });

        var PeopleSeenView = Y.Base.create('peopleSeenView', Y.View, [], {
            template: Y.Handlebars.templates["caseNoteEntryPeopleSeenDialog"],
            events: {
                '#peopleSeenInputs input[type=radio]': {
                    change: 'updatePeopleSeen'
                }
            },
            initializer: function () {
                this.get('model').after('load', function () {
                    this.initPeopleSeen();
                }, this);
            },
            destructor: function () {},
            render: function () {
                this.getPeople();
                var model = this.get('model');

                if (!(this.get('isGroup') || this.isPersonSeen(model))) return;

                var html = this.template({
                    peopleSeenList: this.get('peopleSeenList'),
                    isGroup: this.get('isGroup'),
                    isPersonWithPersonSeenRecord: !this.get('isGroup') && this.isPersonSeen(model),
                    isEditCompleteCaseNoteEntry: this.get('isEditCompleteCaseNoteEntry'),
                    isDeleted: model.get('status') == 'DELETED'
                });

                var peopleSeenContainer = this.get('peopleSeenContainer');

                peopleSeenContainer.setHTML(html);

                return this;
            },
            isPersonSeen: function (model) {
                var peopleSeen = model.get('peopleSeen'),
                    peopleSeenAlone = model.get('peopleSeenAlone');
                return (peopleSeen && peopleSeen.length > 0) || (peopleSeenAlone && peopleSeenAlone.length > 0);
            },
            getPeople: function (e) {
                var container = this.get('container'),
                    groupMembersList = container.all('.groupMembers input[type=checkbox]'),
                    model = this.get('model'),
                    selectAllNode = container.one('input[name="visibleToAll]:checked');

                var checked = groupMembersList.filter('input[type=checkbox]:checked');

                //if isGroup and all the visible people are checked
                if (this.get('isGroup') && checked.size() === groupMembersList.size() && selectAllNode != null) {
                    this._setAllPeopleSeen();
                    //if is a group but no checkboxes exist
                } else if (this.get('isGroup') && checked.size() == 0 && groupMembersList.size() == 0) {
                    var peopleIds = {
                        _nodes: []
                    };

                    peopleIds._nodes = this.get('otherData').visibleMembersList.map(function (member) {
                        return member.memberId
                    });

                    this._setSelectedPeopleSeen(peopleIds);
                    //if not a group but has people seen entries
                } else if (!this.get('isGroup') && this.isPersonSeen(model)) {
                    this._setPersonSeen();
                } else {
                    this._setSelectedPeopleSeen(checked);
                }

            },
            initPeopleSeen: function () {
                var model = this.get('model');
                var peopleSeen = model.get('peopleSeen');
                var peopleSeenAlone = model.get('peopleSeenAlone');
                this.set('peopleSeen', peopleSeen);
                this.set('peopleSeenAlone', peopleSeenAlone);
            },
            _setSelectedPeopleSeen: function (selectedPeople) {
                var peopleSeenNode = this.get('peopleSeenContainer');
                var otherData = this.get('otherData');
                var peopleSeenList = [];
                //clear people seen records
                if (peopleSeenNode) peopleSeenNode.empty();
                this.set('peopleSeenList', []);

                //loop through members of the group
                for (var personIterator = 0; personIterator < otherData.activeGroupMembersList.length; personIterator++) {
                    //loop through checked members of the group
                    for (var selectedPeopleItrator = 0; selectedPeopleItrator < selectedPeople._nodes.length; selectedPeopleItrator++) {
                        //if a user has been checked
                        if (otherData.activeGroupMembersList[personIterator].personId == selectedPeople._nodes[selectedPeopleItrator].value || otherData.activeGroupMembersList[personIterator].personId == selectedPeople._nodes[selectedPeopleItrator]) {
                            var person = otherData.activeGroupMembersList[personIterator];
                            peopleSeenList = this._setPeopleSeenData(person, peopleSeenList);
                        }
                    }
                }
                this.set('peopleSeenList', peopleSeenList);
            },
            _setPersonSeen: function () {
                var model = this.get('model');
                var peopleSeenList = [];
                var person = model.get('subjectIdName');
                peopleSeenList = this._setPeopleSeenData(person, peopleSeenList);
                this.set('peopleSeenList', peopleSeenList);
            },
            _setAllPeopleSeen: function () {
                var peopleSeenNode = this.get('peopleSeenContainer');
                if (peopleSeenNode) peopleSeenNode.empty();
                var peopleSeenList = [];
                //clear people seen records
                this.set('peopleSeenList', []);
                var otherData = this.get('otherData');

                for (var personIterator = 0; personIterator < otherData.activeGroupMembersList.length; personIterator++) {
                    var person = otherData.activeGroupMembersList[personIterator];
                    peopleSeenList = this._setPeopleSeenData(person, peopleSeenList);
                }
                this.set('peopleSeenList', peopleSeenList);
            },

            _setPeopleSeenData: function (person, peopleSeenList) {
                var model = this.get('model');
                var peopleSeen = model.get('peopleSeen');
                var peopleSeenAlone = model.get('peopleSeenAlone');

                //loop through people seen values
                for (var i = 0; i < peopleSeen.length; i++) {
                    if (person.personId == peopleSeen[i] || person.id == peopleSeen[i]) {
                        person.personSeen = true;
                    }
                }
                //loop through people seen alone values
                for (var i = 0; i < peopleSeenAlone.length; i++) {
                    if (person.personId == peopleSeenAlone[i] || person.id == peopleSeen[i]) {
                        person.personSeenAlone = true;
                    }
                }
                peopleSeenList.push(person);
                return peopleSeenList;

            },
            updatePeopleSeen: function (e) {
                var peopleSeenContainer = this.get('peopleSeenContainer'),
                    model = this.get('model'),
                    groupMembersList = peopleSeenContainer.all('#peopleSeenInputs input[type=radio]');

                var checked = groupMembersList.filter('input[type=radio]:checked');
                var peopleSeen = [];
                var peopleSeenAlone = [];

                for (var i = 0; i < checked._nodes.length; i++) {
                    var radioValue = checked._nodes[i].value;
                    if (radioValue === "seen") {
                        peopleSeen.push(checked._nodes[i].name);
                    } else if (radioValue === "seenAlone") {
                        peopleSeenAlone.push(checked._nodes[i].name);
                    }
                }

                model.set('peopleSeen', peopleSeen);
                model.set('peopleSeenAlone', peopleSeenAlone);
            },
        }, {
            ATTRS: {
                peopleSeen: {
                    value: []
                },
                peopleSeenAlone: {
                    value: []
                },
                peopleSeenList: {
                    value: []
                }
            }
        });

        var ViewCaseNoteEntryPeopleSeenView = Y.Base.create('viewCaseNoteEntryPeopleSeenView', ViewCaseNoteEntryView, [], {
            render: function () {
                ViewCaseNoteEntryPeopleSeenView.superclass.render.call(this);
            }
        });

        var BaseUpdateCaseNoteEntryView = Y.Base.create('baseUpdateCaseNoteEntryView', BaseCaseNoteEntryView, [Y.usp.RichText], {
            events: {
                '#preSelectDateOptions li a': {
                    click: 'preSelectFuzzyDate'
                },
                '#caseNoteEntryForm_entryType select': {
                    change: '_onEntryTypeChange'
                }
            },
            initializer: function (config) {
                this.events = Y.merge(this.events, BaseUpdateCaseNoteEntryView.superclass.events);
            },
            render: function () {
                var container = this.get('container'),
                    dateValue = this.get("model").get("eventDate"),
                    fuzzyDateLabels = this.get('fuzzyDateLabels'),
                    subjectType = this.get('otherData').subject.subjectType;
                //call superclass
                BaseUpdateCaseNoteEntryView.superclass.render.call(this);

                this.initEntryTypes(subjectType);
                if('organisation'===subjectType){
                    fuzzyDateLabels.date = fuzzyDateLabels.date + ' <span class="mandatory">*</span>';
                }

                // render the fuzzy event date widget
                this.fuzzyEventDateWidget = new Y.usp.FuzzyDate({
                    id: 'fuzzyEventDate',
                    contentNode: this.getNode('fuzzyEventDate'),
                    dateValue: dateValue,
                    yearInput: true,
                    monthInput: true,
                    dayInput: true,
                    hourInput: true,
                    minuteInput: true,
                    secondInput: false,
                    showDateLabel: false,
                    calendarPopup: true,
                    cssPrefix: 'pure',
                    minYear: 1886,
                    maxYear: new Date().getFullYear(),
                    labels: fuzzyDateLabels
                });
                this.fuzzyEventDateWidget.render();

                if (!dateValue) {
                    //Day & month should default to being disabled.
                    container.one('#fuzzyEventDateMONTH').set('disabled', true);
                    container.one('#fuzzyEventDateDAY').set('disabled', true);
                }

                this.initSource();

                return this;
            },
            destructor: function () {
                // destroy the fuzzy event date widget
                if (this.fuzzyEventDateWidget) {
                    this.fuzzyEventDateWidget.destroy();
                    delete this.fuzzyEventDateWidget;
                }
                if (this.sourceForm) {
                    this.sourceForm.removeTarget(this);

                    this.sourceForm.destroy();

                    delete this.sourceForm;
                }
            },
            initSource: function () {
                var container = this.get('container');
                this.sourceForm = new Y.app.source.SourceForm({
                    model: this.get('model'),
                    labels: this.get('labels'),
                    //source is NOT mandatory for case notes
                    mandatory: false
                });

                this.sourceForm.addTarget(this);

                container.one('#caseNote_source').setHTML(this.sourceForm.render().get('container'));
            },
            _onEntryTypeChange: function (e) {
                var selectElement = e.currentTarget._node,
                    selectedIndex = selectElement.selectedIndex;
                if (selectedIndex >= 0) {
                    this._setSecuredEntrySubTypeOptions('casenote', selectElement.options[selectedIndex].value, null, 'write');
                }
            },
            initEntryTypes: function (subjectType) {
                // get the right coded entry id
                var contextualisingContext = 'com.olmgroup.usp.components.casenote.categoryContext.subjectTypeTagging',
                    contextualisingCodedEntry = subjectType.toUpperCase(),
                    currentCodedEntry = null,
                    accessLevel = 'write',
                    contextualisedCodedEntries = Y.secured.uspCategory.casenote.entryType.category.findActiveContextualisedCodedEntries(currentCodedEntry, accessLevel, contextualisingContext, contextualisingCodedEntry);

                if ('ORGANISATION' !== contextualisingCodedEntry) {
                    const uncontextualisedCodedEntries = Y.secured.uspCategory.casenote.entryType.category.findActiveUncontextualisedCodedEntries(currentCodedEntry, accessLevel, contextualisingContext)
                    contextualisedCodedEntries = contextualisedCodedEntries.concat(uncontextualisedCodedEntries);
                }

                // Get the category context id

                Y.FUtil.setSelectOptions(this.getNode('entryTypeSelect'), O.values(contextualisedCodedEntries), null, null, true, true, 'code');
                Y.FUtil.setSelectOptions(this.getNode('entrySubTypeSelect'), [], null, null, 'Please select a type', true, 'code');
            },
            preSelectFuzzyDate: function (e) {
                e.preventDefault();
                var t = e.currentTarget,
                    today = new Date();

                if (t.hasClass('currentDate')) {
                    this.fuzzyEventDateWidget.setDateAndMask(today, 'DAY');

                } else if (t.hasClass('currentDateTime')) {
                    this.fuzzyEventDateWidget.setDateAndMask(today, 'TIME');
                }
            },

            getFuzzyEventDate: function () {
                return this.fuzzyEventDateWidget.getFuzzyDate();
            },

            getEntry: function () {
                return this.getNode('event');
            },

            getEntryType: function () {
                return this.getNode('entryType');
            },
            resolveCKConfig: function (node, defaultConfig) {
                defaultConfig.extraPlugins = 'imagedialog';
                defaultConfig.imageUploadURL = L.sub(this.get("imageUploadURL"), {
                    id: this.get('model').get('id')
                });
                defaultConfig.allowedContent = true;
                defaultConfig.toolbar.push({
                    name: 'image',
                    groups: ['imageupload'],
                    items: ['Up']
                });

                return defaultConfig;
            }
        }, {
            ATTRS: {
                showAttachments: {
                    value: true
                },
                formId: {
                    value: '#caseNoteEntryAddForm'
                }
            }
        });

        var NewCaseNoteEntryView = Y.Base.create('newCaseNoteEntryView', BaseUpdateCaseNoteEntryView, [], {
            template: Y.Handlebars.templates["caseNoteEntryAddDialog"],
            initializer: function (config) {
                var model = this.get('model'),
                    notificationsConfig = config.notificationsConfig || {};

                this.events = Y.merge(this.events, NewCaseNoteEntryView.superclass.events);

                this.notificationRecipientsView = new Y.app.notification.RecipientsView(Y.merge(notificationsConfig, {
                    model: model,
                    subject: this.get('otherData').subject
                })).addTarget(this);
            },
            render: function () {
                var container = this.get('container');
                //call superclass
                NewCaseNoteEntryView.superclass.render.call(this);

                if (this.get('notificationEnabled')) {
                    container.one('#recipientSelection').append(this.notificationRecipientsView.render().get('container'));
                }

                return this;
            },
            destructor: function () {
                this.notificationRecipientsView.removeTarget(this);
                this.notificationRecipientsView.destroy();

                delete this.notificationRecipientsView;
            }
        }, {
            ATTRS: {
                notificationEnabled: {
                    value: false
                },
                notificationsConfig: {
                    value: {
                        labels: {},
                        personAutocompleteURL: null,
                        organisationAutocompleteURL: null

                    }
                },
                /**
                 * Getter for notifications view
                 */
                notificationRecipientsView: {
                    readOnly: true,
                    getter: function () {
                        return this.notificationRecipientsView;
                    }
                }
            }
        });

        var EditCaseNoteEntryView = Y.Base.create('editCaseNoteEntryView', BaseUpdateCaseNoteEntryView, [], {
            template: Y.Handlebars.templates["caseNoteEntryEditDraftDialog"],
            initializer: function () {
                this.events = Y.merge(this.events, EditCaseNoteEntryView.superclass.events);
            },
            initEntryTypes: function (subjectType) {

                var model = this.get('model'),
                    entryType = this.lookupExistingEntry(Y.secured.uspCategory.casenote.entryType.category.codedEntries, model.get('entryType')),
                    entrySubType = this.lookupExistingEntry(Y.secured.uspCategory.casenote.entrySubType.category.codedEntries, model.get('entrySubType')),
                    contextualisingContext = 'com.olmgroup.usp.components.casenote.categoryContext.subjectTypeTagging',
                    contextualisingCodedEntry = subjectType.toUpperCase(),
                    accessLevel = 'write',
                    contextualisedCodedEntries = Y.secured.uspCategory.casenote.entryType.category.findActiveContextualisedCodedEntries(entryType, accessLevel, contextualisingContext, contextualisingCodedEntry);

                if ('ORGANISATION' !== contextualisingCodedEntry) {
                    const uncontextualisedCodedEntries = Y.secured.uspCategory.casenote.entryType.category.findActiveUncontextualisedCodedEntries(entryType, accessLevel, contextualisingContext);
                    contextualisedCodedEntries = contextualisedCodedEntries.concat(uncontextualisedCodedEntries);
                }

                Y.FUtil.setSelectOptions(this.getNode('entryTypeSelect'), O.values(contextualisedCodedEntries), entryType.code, entryType.name, true, true, 'code');

                var parentCode = model.get('entryType');
                this._setSecuredEntrySubTypeOptions('casenote', parentCode, entrySubType, 'write');

                this.getNode('entryTypeSelect').set('value', parentCode);
                this.getNode('entrySubTypeSelect').set('value', model.get('entrySubType'));
            }
        }, {
            ATTRS: {
                formId: {
                    value: '#caseNoteEntryEditForm'
                }
            }
        });

        var EditCompleteCaseNoteEntryView = Y.Base.create('editCompleteCaseNoteEntryView', EditCaseNoteEntryView, [], {
            template: Y.Handlebars.templates["caseNoteEntryEditCompleteDialog"],
            events: {
                '#fuzzyEventDate_fuzzyDate select': {
                    change: 'fireDateChange',
                },
                '#fuzzyEventDateYEAR': {
                    change: 'fireDateChange',
                    dateChange: 'fireDateChange'
                }
            },
            initializer: function () {
                this.previousDateValue = null;
                this.events = Y.merge(this.events, EditCompleteCaseNoteEntryView.superclass.events);
                this.sourceView = new Y.app.source.SourceView({
                    model: this.get('model'),
                    labels: this.get('labels')
                });
            },
            initEntryTypes: function () {
                //Don't allow entry types to be changed
            },
            initSource: function () {
                //render the source view
                this.get('container').one('#caseNote_source').insert(this.sourceView.render().get('container'), "replace");
            },
            destructor: function () {
                this.sourceView.destroy();
                delete this.sourceView;
            },
            fireDateChange: function (e) {
                if ((e.type === 'change') || (this.previousDateValue && (e.date.valueOf() !== this.previousDateValue))) {
                    this.fire('casenote:dateChange', {});
                    this.previousDateValue = null;
                } else {
                    this.previousDateValue = e.date.valueOf();
                }
            }
        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        entryTypes: Y.secured.uspCategory.casenote.entryType.category.codedEntries,
                        entrySubTypes: Y.secured.uspCategory.casenote.entrySubType.category.codedEntries
                    }
                }
            }
        });

        /**
         * @class UploadFileCaseNoteEntryView
         * @inherits Y.View
         */

        var UploadFileCaseNoteEntryView = Y.Base.create('uploadFileCaseNoteEntryView', Y.usp.casenote.NewCaseNoteEntryAttachmentView, [], {
            //bind template
            template: Y.Handlebars.templates["uploadFileCaseNoteEntryDialog"],
            render: function () {
                var container = this.get('container');

                //call superclass
                UploadFileCaseNoteEntryView.superclass.render.call(this);

                this.sourceForm = new Y.app.source.SourceForm({
                    model: this.get('model'),
                    labels: this.get('labels')
                });

                this.sourceForm.addTarget(this);

                container.one('form').append(this.sourceForm.render().get('container'));

                return this;
            },
            destructor: function () {
                if (this.sourceForm) {
                    this.sourceForm.removeTarget(this);

                    this.sourceForm.destroy();

                    delete this.sourceForm;
                }
            }
        }, {
            ATTRS: {
                temporaryAttachment: {
                    value: false
                }
            }
        });

        // HARD DELETE
        var RemoveCaseNoteEntryView = Y.Base.create('removeCaseNoteEntryView', Y.usp.casenote.ExtendedCaseNoteEntryView, [], {
            //bind template
            template: Y.Handlebars.templates["caseNoteEntryRemoveDialog"]

        });
        // SOFT DELETE
        var HideCaseNoteEntryView = Y.Base.create('hideCaseNoteEntryView', Y.usp.casenote.ExtendedCaseNoteEntryView, [], {
            //bind template
            template: Y.Handlebars.templates["caseNoteEntryDeleteDialog"]

        });
        // COMPLETE CASENOTE
        var CompleteCaseNoteEntryView = Y.Base.create('completeCaseNoteEntryView', Y.usp.casenote.ExtendedCaseNoteEntryView, [], {
            //bind template
            // this is when Complete is called from result sets action menu
            template: Y.Handlebars.templates["caseNoteEntryCompleteDialog"]
        });
        // UNCOMPLETE Case Note Entry
        var UncompleteCaseNoteEntryView = Y.Base.create('uncompleteCaseNoteEntryView', Y.usp.casenote.ExtendedCaseNoteEntryView, [], {
            //bind template
            template: Y.Handlebars.templates["caseNoteEntryUncompleteDialog"]
        });
        //SEND NOTIFICATION
        var SendNotificationView = Y.Base.create('SendNotificationView', Y.usp.casenote.UpdateNotifyCaseNoteEntryView, [], {
            template: Y.Handlebars.templates["caseNoteNotificationDialog"],
            initializer: function (config) {
                var model = this.get('model'),
                    notificationsConfig = config.notificationsConfig || {};

                this.events = Y.merge(this.events, SendNotificationView.superclass.events);
                this.notificationRecipientsView = new Y.app.notification.RecipientsView(Y.merge(notificationsConfig, {
                    model: model,
                    subject: this.get('otherData').subject
                })).addTarget(this);
            },
            render: function () {
                var container = this.get('container');
                //call superclass
                SendNotificationView.superclass.render.call(this);
                if (this.get('notificationEnabled')) {
                    container.one('#recipientSelection').append(this.notificationRecipientsView.render().get('container'));
                }
                return this
            },
            destructor: function () {
                this.notificationRecipientsView.removeTarget(this);
                this.notificationRecipientsView.destroy();
                delete this.notificationRecipientsView;
            }
        }, {
            ATTRS: {
                notificationEnabled: {
                    value: false
                },
                notificationsConfig: {
                    value: {
                        labels: {},
                        personAutoCompleteURL: null,
                        orgainisationAutoCompleteURL: null
                    }
                },
                /******
                 * Getter for notificationViews
                 */
                notificationRecipientsView: {
                    readOnly: true,
                    getter: function () {
                        return this.notificationRecipientsView;
                    }
                }
            }
        });

        Y.namespace('app.casenote').ViewCaseNoteEntryView = ViewCaseNoteEntryView;
        Y.namespace('app.casenote').PeopleSeenView = PeopleSeenView;
        Y.namespace('app.casenote').NewCaseNoteEntryView = NewCaseNoteEntryView;
        Y.namespace('app.casenote').EditCaseNoteEntryView = EditCaseNoteEntryView;
        Y.namespace('app.casenote').EditCompleteCaseNoteEntryView = EditCompleteCaseNoteEntryView;
        Y.namespace('app.casenote').HideCaseNoteEntryView = HideCaseNoteEntryView;
        Y.namespace('app.casenote').RemoveCaseNoteEntryView = RemoveCaseNoteEntryView;
        Y.namespace('app.casenote').CompleteCaseNoteEntryView = CompleteCaseNoteEntryView;
        Y.namespace('app.casenote').UncompleteCaseNoteEntryView = UncompleteCaseNoteEntryView;
        Y.namespace('app.casenote').UploadFileCaseNoteEntryView = UploadFileCaseNoteEntryView;
        Y.namespace('app.casenote').SendNotificationView = SendNotificationView;

    },
    '0.0.1', {
        requires: ['yui-base',
            'handlebars-helpers',
            'handlebars-casenote-templates',
            'usp-casenote-NewCaseNoteEntry',
            'usp-casenote-UpdateDraftCaseNoteEntry',
            'usp-casenote-UpdateCompleteCaseNoteEntry',
            'usp-casenote-UpdateNotifyCaseNoteEntry',
            'usp-casenote-NewCaseNoteEntryAttachment',
            'categories-casenote-component-EntryTypeSubjectTagging',
            'secured-categories-casenote-component-EntryType',
            'secured-categories-casenote-component-EntrySubType',
            'usp-configuration-CategoryContextWithContextualisingCodes',
            'usp-configuration-CategoryWithOwnerAndContextualisingDetails',
            'form-util',
            'fuzzy-date',
            'view-rich-text',
            'casenote-files-results-view',
            'entry-dialog-views',
            'source-form',
            'notification-recipients',
            'usp-configuration-CategoryContext'
        ]
    });