YUI.add('casenote-image-dialog', function(Y) {

    
    Y.namespace('app.casenote.image').UploadSubjectImageForm = Y.Base.create("uploadSubjectImageForm", Y.usp.casenote.NewSubjectTemporaryAttachment, [Y.usp.ModelFormLink], {
        form: '#uploadForm'
    });
    
    Y.namespace('app.casenote.image').UploadImageView = Y.Base.create('uploadImageView', Y.usp.View, [], {
        template:Y.Handlebars.templates["uploadImageDialog"],
      });

    Y.namespace('app.casenote.image').ImageMultiPanelPopUp = Y.Base.create('imageMultiPanelPopUp', Y.app.attachment.AttachmentDialog, [], {
        views: {
            uploadFile: {
                type: Y.app.casenote.image.UploadImageView
            }
        }

    });
}, '0.0.1', {
    requires: ['yui-base',
        'attachment-dialog',
        'usp-view',
        'handlebars-casenote-templates',
        'model-form-link',
        'usp-casenote-NewSubjectTemporaryAttachment'
    ]
});