YUI.add('episodeofcare-dialog', function (Y) {
    'use-strict';
    /* globals moment, Promise*/
    var O = Y.Object,
        L = Y.Lang,
        MixinOptgroup = Y.app.childlookedafter.MixinClassificationsOptgroup,
        Micro = Y.Template.Micro,
        /*jshint multistr:true */
        legalStatusTemplate = Micro.compile('<div class="legalStatus input-group input-append">\
                <div class="pure-u-1 legalStatusItem" name="legalStatusIds" data-value="<%=this.value%>"> <%=this.name%> </div><span><a class="pure-button pure-button-secondary remove" title="remove" aria-label="Remove legal status from list"><i class="fa fa-minus-circle"></i></a></span>\
                        </div>'),
        isEmpty = function (v) {
            return v === null || v === ''
        };

    Y.namespace('app.childlookedafter').NewEpisodeOfCareForm = Y.Base.create('newEpisodeOfCareForm', Y.usp.childlookedafter.NewEpisodeOfCare, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#episodeOfCareAddForm'
    });

    Y.namespace('app.childlookedafter').NewAdmittanceEpisodeOfCareForm = Y.Base.create('newAdmittanceEpisodeOfCareForm', Y.usp.childlookedafter.NewAdmittanceEpisodeOfCare, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#episodeOfCareAddAdmittanceForm'
    });

    Y.namespace('app.childlookedafter').NewEpisodeOfCareWithPlacementAddressForm = Y.Base.create('newEpisodeOfCareWithPlacementAddressForm', Y.usp.childlookedafter.NewEpisodeOfCareWithPlacementAddress, [Y.usp.ModelTransmogrify], {
        customValidation: function (attrs) {
            var errors = {};

            if (isEmpty(attrs.locationId)) {
                errors.locationId = 'Location is mandatory.';
            }

            return errors;
        }
    });

    var EpisodeWarningsView = Y.Base.create('EpisodeWarnings', Y.View, [], {
        template: Y.Handlebars.templates.episodeOfCareWarnings,
        initializer: function () {
            this._evtHandlers = [
                this.after('warningsChange', this.render, this)
            ];
        },
        render: function () {
            var container = this.get('container');
            var warnings = this.get('warnings');

            container.setHTML(this.template({
                warnings: warnings,
                labels: {
                    title: 'Warnings'
                }
            }));

            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;
        }
    }, {
        ATTRS: {
            warnings: {
                value: []
            }
        }
    });

    var legalStatusesView = Y.Base.create('legalStatusView', Y.View, [], {
        initializer: function () {
            var legalStatusModel = this.get('legalStatusModel');
            this.legalStatusViews = [];
            this._evtHandlers = [];
            if (this.get('supportsMultipleEntries')) {
                this._evtHandlers.push(
                    legalStatusModel.after(['add', 'remove'], this.render, this)
                );
            }
        },
        render: function () {
            var legalStatusModel = this.get('legalStatusModel'),
                container = this.get('container'),
                legalStatusViews = this.legalStatusViews,
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            this.clearLegalStatusViews();

            legalStatusModel.each(function (model) {
                var view = new legalStatusView({
                    model: model
                });

                contentNode.append(view.render().get('container'));

                legalStatusViews.push(view);
            });

            container.setHTML(contentNode);
            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.clearLegalStatusViews();

            if (this.legalStatusViews) {
                delete this.legalStatusViews;
            }
        },
        clearLegalStatusViews: function () {

            if (this.legalStatusViews && this.legalStatusViews.length > 0) {
                this.legalStatusViews.forEach(function (legalStatusView) {
                    legalStatusView.destroy();
                });
            }
        }
    }, {
        ATTRS: {
            supportsMultipleEntries: {
                value: true
            }
        }
    });

    var legalStatusView = Y.Base.create('legalStatusView', Y.View, [], {
        events: {
            '.remove': {
                'click': 'removeItem'
            }
        },
        removeItem: function (e) {
            var node = e.currentTarget.getDOMNode();
            if (!node.getAttribute('disabled')) {
                var model = this.get('model');
                e.preventDefault();
                model.destroy();
            }
        },
        template: legalStatusTemplate,
        render: function () {
            var container = this.get('container'),
                model = this.get('model');
            container.setHTML(this.template(model.toJSON()));
            return this;
        }
    });

    var legalStatusModel = Y.Base.create('legalStatusModel', Y.ModelList, [], {});

    /**
     * @class app.childlookedafter.NewEpisodeOfCareView
     * @description Mixes in Y.app.childlookedafter.MixinClassificationsOptgroup, which provides the method setOptgroupSelect
     */

    Y.namespace('app.childlookedafter').NewEpisodeOfCareView = Y.Base.create('newEpisodeOfCareView', Y.app.childlookedafter.BaseAddEditEpisodeOfCareView, [MixinOptgroup], {
        template: Y.Handlebars.templates.episodeOfCareAddDialog,
        events: {
            'input[name="startDate"]': {
                change: '_startDateChange',
                dateChange: '_startDateChange'
            },
            'input[name="carerSubjectType"]': {
                'change': '_handleCarerSubjectTypeInputChange'
            },
            'select[name="placementTypeId"]': {
                'change': '_handlePlacementTypeSelectChange'
            },
            'select[name="startReason"]': {
                'change': '_handleStartReasonSelectChange'
            },
            '#legalStatusIds': {
                'change': '_addLegalStatus'
            },
            '#location_toggleAddLocation': {
                click: '_showLocationAddDialog'
            }
        },
        initializer: function () {
            this.legalStatusModel = new legalStatusModel();

            this.legalStatusesView = new legalStatusesView({
                legalStatusModel: this.legalStatusModel,
                supportsMultipleEntries: this.get('supportsMultipleLegalStatusEntries') || false
            });

            this.warningsView = new EpisodeWarningsView();

            this.placementAddressView = new Y.app.childlookedafter.PlacementAddressAddView({
                labels: this.get('labels'),
                locationUrl: this.get('locationUrl'),
                locationQueryParamsUrl: this.get('locationQueryParamsUrl')
            }).addTarget(this);

            //load latestEpisode as a promise to use as default values
            var latestEpisodePromise = new Y.Promise(function (resolve, reject) {
                this.get('latestEpisode').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load legalStatusClassifications as a promise
            var legalStatusPromise = new Y.Promise(function (resolve, reject) {
                this.get('legalStatusClassifications').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load placementTypeClassifications as a promise
            var placementTypePromise = new Y.Promise(function (resolve, reject) {
                this.get('placementTypeClassifications').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load parental relationships as a promise
            var parentalRelationshipPromise = new Y.Promise(function (resolve, reject) {
                this.get('parentalRelationshipList').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            var personPlacementAddressPromise = new Y.Promise(function (resolve, reject) {
                this.get('personPlacementAddress').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //resolves our promises
            Y.Promise.all([latestEpisodePromise, legalStatusPromise, placementTypePromise, parentalRelationshipPromise, personPlacementAddressPromise]).then(function (data) {
                var container = this.get('container'),
                    latestEpisode = this.get('latestEpisode'),
                    latestPlacement = this._getLatestEpisodeMainPlacement(),
                    placementProvision = latestPlacement.placementProvision,
                    legalStatuses = latestEpisode.get('legalStatuses'),
                    firstLegalStatus = legalStatuses[0],
                    legalStatusValue = {};

                //OLM-17685 V4. Do not show IND_EP on new Episode of Care dialog
                this.legalStatusCodes = data[1].filter(function (e) {
                    return e.code !== 'IND_EP'
                });

                if (!this.legalStatusesView.get('supportsMultipleEntries') &&
                    firstLegalStatus.hasOwnProperty('id')) {
                    legalStatusValue = {
                        value: firstLegalStatus.id
                    };
                }

                this.setOptgroupSelect(container.one('#legalStatusIds'), this.legalStatusCodes, legalStatusValue);

                this.setOptgroupSelect(container.one('#placementType'), data[2], {
                    value: latestPlacement.placementType.id
                });

                this.set('placementType', latestPlacement.placementType.code);

                this._handleSetCarer();
                this._setDefaultTypesOfCare();

                if (placementProvision) {
                    this._setPlacementProvisionValue(placementProvision);
                }

                this._setParentOptions(data[3].results);
                this.set('parentList', data[3].results);

                //Set to the context of 'this' so base care view has a reference
                this.personPlacementAddress = (data[4].length) ? data[4][0] : null;
                if (this.personPlacementAddress) {
                    var location = {
                        location: this.personPlacementAddress.location.location,
                        id: this.personPlacementAddress.location.id
                    }
                    this.placementAddressView.handleSetLocationsValue(location);
                    this._setPersonPlacementAddress(this.personPlacementAddress);
                }

                if (latestEpisode) {
                    //set the placement type attribute value
                    this.set('placementType', latestPlacement.placementType.code);

                    this._resetLegalStatuses(legalStatuses, this.legalStatusModel);
                }
            }.bind(this));

            this._evtHandlers = [
                this.after('startReasonChange', this._handleStartReasonChange, this),
                this.after('carerSubjectTypeChange', this._handleCarerSubjectTypeChange),
                this.legalStatusModel.after(['add', 'remove'], this.updateWarnings, this),
                this.after('*:selectedPersonChange', this._handleSelectedSubjectChange, this),
                this.after('*:newCarerViewCreated', this._handleNewCarerViewCreated, this),
                this.after('placementTypeChange', this.handlePlacementTypeChange, this),
                this.after('startDateChange', this._updateTypeOfCareForSameCarer, this),
                this.after('parentListChange', this.setupFieldVisibility, this)
            ];
        },
        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels')
                });

            container.setHTML(html);

            this._initStartReason(this.get('container').one('#startReason'));
            this._initPlacementChangeReason(this.get('container').one('#placementChangeReason'));

            this.startDateCalendar = new Y.usp.CalendarPopup({
                inputNode: this.get('container').one('#startDate')
            });
            this.startDateCalendar.addTarget(this);
            this.startDateCalendar.render();

            container.one('#episodeWarningsWrapper').append(this.warningsView.render().get('container'));

            if (this.legalStatusesView.get('supportsMultipleEntries')) {
                container.one('#legalStatuses').append(this.legalStatusesView.render().get('container'));
            }

            container.one('#addAddressWrapper').append(this.placementAddressView.render().get('container'));

            /* OLM-23371 if there are more requirements to display/hide more fields in the future we should consider creating a new view entirely */
            if (this.get('otherData.showScottishView') || this.get('otherData.showWelshView')) {
                container.one('#placementProvisionContainer').hide();
            } else {
                // only populate list if it's displayed 
                Y.FUtil.setSelectOptions(container.one('#placementProvision'), O.values(Y.uspCategory.childlookedafter.placementProvision.category.getActiveCodedEntries()), null, null, true, true, 'code');
            }

            //setup the initial field visibility
            this.setupFieldVisibility();
            return this;
        },
        _handleSetCarer: function () {
            var latestPlacement = this._getLatestEpisodeMainPlacement();
            if (latestPlacement.carerSubject) {
                if (latestPlacement.carerSubject.type === 'PERSON') {
                    latestPlacement.carerSubject.personIdentifier = latestPlacement.carerSubject.identifier;
                }
                if (latestPlacement.carerSubject.type === 'ORGANISATION') {
                    latestPlacement.carerSubject.organisationIdentifier = latestPlacement.carerSubject.identifier;
                }

                // Set latest main carer if they haven't been set.
                // This avoids unnecessary 'carerSubjectTypeChange' events firing
                if (this.get('carerSubject') !== latestPlacement.carerSubject) {
                    //set view attributes which will drive the carer view containing the autcomplete controls
                    this.set('carerSubjectType', latestPlacement.carerSubject.type);
                    this.set('carerSubject', latestPlacement.carerSubject);
                }
            }
        },
        _showLocationAddDialog: function () {
            this.fire('episodeOfCare:open:locationView');
        },
        _resetLegalStatuses: function (newLegalStatuses, legalStatusModel) {
            var legalStatusArray = legalStatusModel.toArray();

            legalStatusArray.forEach(function (model) {
                legalStatusModel.remove(model);
            });

            (newLegalStatuses || []).forEach(function (newLegalStatus) {
                legalStatusModel.add({
                    name: newLegalStatus.name,
                    value: newLegalStatus.id,
                    code: newLegalStatus.code
                });
            });
        },
        _addLegalStatus: function (e) {
            var supportsMultipleLegalStatusEntries = this.get('supportsMultipleLegalStatusEntries') || false;

            var selectNode = e.currentTarget.getDOMNode(),
                optionIndex = selectNode.selectedIndex,
                optionNode = selectNode[optionIndex],
                value,
                name,
                code;

            if (optionNode) {
                value = optionNode.value;
                name = optionNode.text.trim();
                code = optionNode.getAttribute('code');

                var legalStatus = {
                    name: name,
                    value: value,
                    code: code
                };

                if (supportsMultipleLegalStatusEntries) {
                    var existingValues = this.legalStatusModel.filter(function (legalStatus) {
                        return legalStatus.get('value') == value;
                    });

                    if (existingValues.length == 0) {
                        this.legalStatusModel.add(legalStatus);
                    }

                    //clear selection
                    selectNode.selectedIndex = -1;
                } else {
                    //only a single entry allowed - so reset model to new entry
                    this.legalStatusModel.reset();

                    this.legalStatusModel.add(legalStatus);
                }
            }

        },
        _getLatestEpisodeMainPlacement: function () {
            return this.get('latestEpisode').get('placements')[0];
        },
        getLegalStatuses: function () {
            var container = this.get('container'),
                legalStatuses = [],
                supportsMultipleLegalStatusEntries = this.get('supportsMultipleLegalStatusEntries') || false;
            if (supportsMultipleLegalStatusEntries) {
                var modelList = this.legalStatusModel;
                if (modelList) {
                    legalStatuses = modelList.map(function (m) {
                        return m.get('value').toString();
                    });
                }
            } else {
                legalStatuses.push(container.one('#legalStatusIds').get('value'));
            }
            return legalStatuses;
        },
        clearLegalStatuses: function () {
            this.legalStatusModel.each(function (model) {
                model.destroy();
            });
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            if (this.legalStatusesView) {
                this.legalStatusesView.destroy();
                delete this.legalStatusesView;
            }

            if (this.selectionView) {
                this.selectionView.removeTarget(this);
                this.selectionView.destroy();
                delete this.selectionView;
            }

            if (this.startDateCalendar) {
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }

            if (this.warningsView) {
                this.warningsView.destroy();
                delete this.warningsView;
            }
        },

        // See OLM-17685 for rules on warnings
        updateWarnings: function () {
            var latestEpisode = this.get('latestEpisode'),
                container = this.get('container'),
                labels = this.get('labels'),
                warnings = [];

            //placement type from attribute value on view
            var placementTypeCode = this.get('placementType'),
                previousPlacementTypeCode = this._getLatestEpisodeMainPlacement().placementType.code;

            var previousLegalStatusTypes = latestEpisode.get('legalStatuses'),
                previousLegalStatusCodes = previousLegalStatusTypes.map(function (legalStatus) {
                    return legalStatus.code;
                });

            var previousEpisodeStartDate = moment(latestEpisode.get('startDate'));

            var dateOfPlacement = Y.USPDate.parse(container.one('#startDate').get('value')),
                dateOfBirth = Y.USPDate.parse(this.get('subject').dateOfBirth);

            var adoptionCurrentCarerCodes = ['CONS_ADOPT_FP', 'ORD_ADOPT_FP'];
            var allowedPreviousPlacementWithCurrentCarerCodes = [
                'CONS_ADOPT_FP', 'CONS_ADOPT_NOTFP', 'ORD_ADOPT_FP', 'ORD_ADOPT_NOTFP', //Adoption Codes
                'REL', 'REL_ADOPT', 'REL_NOT_LT_ADOPT', 'LT', 'APP_ADOPT', 'NOT_LT_ADOPT' //Fostering Codes
            ];

            if (adoptionCurrentCarerCodes.indexOf(placementTypeCode) != -1 && allowedPreviousPlacementWithCurrentCarerCodes.indexOf(previousPlacementTypeCode) == -1) {
                warnings.push(labels.warnings.incorrectPreviousPlacement);
            }

            var disallowedLegalStatusUnderTenCodes = ['REMAND', 'LA_ACCOM_PACE', 'SO_RES'],
                legalStatusModelArray = this.legalStatusModel.toArray();

            var disallowedLegalStatusUnderTen = legalStatusModelArray.find(function (model) {
                return disallowedLegalStatusUnderTenCodes.indexOf(model.get('code')) !== -1
            });

            if (dateOfBirth && dateOfPlacement) {
                var ageAtPlacement = dateOfPlacement.diff(dateOfBirth, 'years'),
                    lengthOfProposedEpisode = dateOfPlacement.diff(previousEpisodeStartDate, 'days');

                // W2 - Warnings on age of Child at new Episode start date
                if (ageAtPlacement >= 18 && (placementTypeCode !== 'SEC_UNIT' && legalStatusModelArray.indexOf('SEC20') == -1)) {
                    warnings.push(labels.warnings.overageHome);
                }

                if (ageAtPlacement < 10 && disallowedLegalStatusUnderTen) {
                    warnings.push(labels.warnings.underageForYouthJustice);
                }

                if (ageAtPlacement < 15 && placementTypeCode == 'IND_LIV') {
                    warnings.push(labels.warnings.underageForIndependentLiving);
                }

                if (ageAtPlacement < 14 && placementTypeCode == 'CHLD_HOME') {
                    warnings.push(labels.warnings.underageForHostels);
                }

                // W10
                if (ageAtPlacement < 10 && placementTypeCode == 'YOI') {
                    warnings.push(labels.warnings.underageForYouthPrison);
                }

                if (ageAtPlacement < 4 && placementTypeCode == 'RES_SCH') {
                    warnings.push(labels.warnings.underageForResidentialSchool);
                }

                // Warnings on duration of Episode being closed
                if (lengthOfProposedEpisode > 3 && previousLegalStatusCodes.indexOf('POL_PROT') > -1) {
                    warnings.push(labels.warnings.policeProtectionDuration);
                }

                if (lengthOfProposedEpisode > 21 && previousLegalStatusCodes.indexOf('EMERG_PROT') > -1) {
                    warnings.push(labels.warnings.emergencyProtectionDuration);
                }

                if (lengthOfProposedEpisode > 7 && previousLegalStatusCodes.indexOf('CHLD_ASSESS') > -1) {
                    warnings.push(labels.warnings.assessmentOrderDuration);
                }
            }

            this.warningsView.set('warnings', warnings);
        },

        _initStartReason: function (select) {
            var startReasons = Y.Object.values(Y.secured.uspCategory.childlookedafter.episodeStartReason.category.getActiveCodedEntries(null, 'write')).filter(function (codedEntry) {
                return codedEntry.code !== 'S';
            });
            if (startReasons && select) {
                Y.FUtil.setSelectOptions(select, startReasons, null, null, true, true, 'code');
            }
        },
        _initPlacementChangeReason: function (select) {
            var placementChangeReasons = Y.Object.values(Y.secured.uspCategory.childlookedafter.placementChangeReason.category.getActiveCodedEntries(null, 'write'));
            if (placementChangeReasons && select) {
                Y.FUtil.setSelectOptions(select, placementChangeReasons, null, null, true, true, 'code');
            }
        },

        _setTypeOfCareValue: function (value) {
            this.get('container').one('select[name="typeOfCare"]').set('value', value);
        },
        _setPlacementProvisionValue: function (value) {
            this.get('container').one('select[name="placementProvision"]').set('value', value);
        },
        _handleStartReasonSelectChange: function (e) {
            var startReason = e.target.get('options')._nodes[e.target.get('selectedIndex')].value;

            this.set('startReason', startReason);

            if (startReason === 'L') {
                this._resetPlacementType();
            }
        },
        _handleStartReasonChange: function () {
            this.setupFieldVisibility();
            this._updateTypeOfCareForSameCarer();
        },
        _updateTypeOfCareForSameCarer: function () {
            var startReason = this.get('startReason');
            // If same carer allow user to select another type of care if applicable
            if (startReason === 'U' || startReason === 'T') {
                // Ensure latest placement carer is in context
                this._handleSetCarer();
                var params = {
                    subjectId: this.get('carerSubject').id,
                    date: this.get('startDate')
                }
                // Get types of care applicable to carer and default to the current type of care
                this._getTypesOfCareForSubjectByDate(params).then(function (results) {
                    this._setTypesOfCare(results);
                }.bind(this));
            }
        },
        handlePlacementTypeChange: function () {
            //setup field visibility
            this.setupFieldVisibility();
            //and update warnings
            this.updateWarnings();
        },
        _handlePlacementTypeSelectChange: function (e) {
            var placementType = e.target.get('options')._nodes[e.target.get('selectedIndex')].getAttribute('code');

            //update the placement type attribute - this is what will drive the autocomplete select logic
            this.set('placementType', placementType);
        },
        _handleNewCarerViewCreated: function (e) {
            this._handleSelectedSubjectChange(e);
            this._updateTypeOfCareForSameCarer();
        },
        _handleSelectedSubjectChange: function (e) {
            this._handleTypeOfCareSetup(e);
            this._handleSelectedSubjectAddressChange(e);
        },
        _handleTypeOfCareSetup: function (e) {
            //do request for types of care
            if (e.newVal) {
                this._getTypesOfCareForSubject(e.newVal).then(function (results) {
                    this._setTypesOfCare(results);
                }.bind(this));
            } else {
                //clear types of care values if subject is null
                this._setDefaultTypesOfCare();
            }
        },
        _setDefaultTypesOfCare: function () {
            //initialise type of care field, if value from previous record is 'UNKNOWN', retrieve type of care values.
            if (this.typeOfCareNotUnknown()) {
                this._setTypesOfCare();
            } else {
                this._getTypesOfCareForSubject(this._getLatestEpisodeMainPlacement().carerSubject).then(function (results) {
                    this._setTypesOfCare(results);
                }.bind(this));
            }
        },
        _setParentOptions: function (values) {
            var selectNode = this.get('container').one('#parent');

            var options = (values || []).map(function (value) {
                var name = (value.personAddressVO) ? (value.personVO.name + ' - ' + value.personAddressVO.location.location) : value.personVO.name;
                return {
                    id: value.personVO.id,
                    name: name
                }
            });
            Y.FUtil.setSelectOptions(selectNode, options, null, null, true, true, 'id');
        },
        _getTypesOfCareForSubject: function (subject) {
            var typeOfCareURL = this.get('typeOfCareURL').replace('{subjectId}', subject.id);
            return new Promise(function (resolve, reject) {
                Y.io(typeOfCareURL, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },

                    on: {
                        success: function (trId, response) {
                            try {
                                var results = JSON.parse(response.response).results;
                                resolve(results);
                            } catch (e) {
                                Y.log('Failed to get types of care', 'debug');
                                reject(e);
                            }
                        }
                    }
                });
            });
        },
        _setTypesOfCare: function (results) {
            var typesOfCare = {},
                typeOfCareInput = this.get('container').one('#typeOfCare');

            Y.Array.forEach(results, function (result) {
                var codedEntry = result.typeOfCareAssignmentVO.codedEntryVO;
                typesOfCare[codedEntry.code] = codedEntry;
            });

            //Add the inactive NOT_KNOWN type of care so that we can still display it (disabled)
            //so that the user knows why they have to select a new type of care
            if ('NOT_KNOWN' === this.get('latestEpisode').get('typeOfCare')) {
                typesOfCare['NOT_KNOWN'] = {
                    active: false,
                    code: 'NOT_KNOWN',
                    name: 'Previously Unknown - select a new value',
                };
            }

            Y.FUtil.setSelectOptions(typeOfCareInput, Y.Object.values(typesOfCare), null, null, true, true, 'code');

            this._disableOptionWithValue(typeOfCareInput, 'NOT_KNOWN');

        },
        _disableOptionWithValue: function (node, optionValue) {
            var optionForValue = Array.apply(null, node.getDOMNode().options).find(
                function (option) {
                    return option.value === optionValue
                });

            if (optionForValue) {
                optionForValue.disabled = true;
            }
        },
        _startDateChange: function (e) {
            var startDate = null;
            if (e.target.get('value')) {
                var parsedStartDate = Y.USPDate.parseDateTime(e.target.get('value'));
                if (null !== parsedStartDate) {
                    startDate = parsedStartDate.toDate().setHours(0, 0, 0, 0);
                }
            } else if (e.date) {
                startDate = e.date.setHours(0, 0, 0, 0);
            }

            if (startDate) {
                this.set('startDateValid', true);

                var currentStartDate = this.get('startDate', startDate),
                    dateChanged = currentStartDate !== startDate;

                if (dateChanged) {
                    this.set('startDate', startDate);
                }
            } else {
                this.set('startDateValid', false);
            }

            //clear down auto complete for person selection - as may no longer be valid where
            //the start date is used in the criteria
            if (this.selectionView && this.selectionView.name === 'carerAutocompleteView') {
                this.selectionView.set('selectedPerson', undefined);
            }

            //ensure the warnings update
            this.updateWarnings();
        },
        /**
         * Resets placement type to previous episodes placementType.
         */
        _resetPlacementType: function () {
            var latestPlacement = this._getLatestEpisodeMainPlacement();

            this.get('container').one('#placementType').set('value', latestPlacement.placementType.id);
            this.set('placementType', latestPlacement.placementType.code);
        },
        /**
         * Return a set of flags to indicate
         * field visibility
         */
        getFieldVisibility: function () {
            //get the default field visibility from the parent class
            var fieldVisibility = Y.app.childlookedafter.NewEpisodeOfCareView.superclass.getFieldVisibility.call(this);

            fieldVisibility.plannedChange = false;

            var startReason = this.get('startReason'),
                placementType = this.get('placementType'),
                scottishView = this.get('otherData.showScottishView');

            if (!scottishView) {
                switch (startReason) {
                case 'P': {
                    fieldVisibility.plannedChange = true;
                    break;
                }

                case 'B': {
                    fieldVisibility.plannedChange = true;
                    break;
                }
                }

            }

            switch (placementType) {
            case 'IND_LIV': {
                if (startReason == 'L') {
                    fieldVisibility.personPlacementAddressField = true;
                    fieldVisibility.placementAddressFields = false;
                } else {
                    fieldVisibility.personPlacementAddressField = false;
                    fieldVisibility.placementAddressFields = true;
                }

                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'RES_EMP': {
                fieldVisibility.subjectAddress = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'SCT_PARENT':
                //falls through
            case 'PARENT': {
                fieldVisibility.parent = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'SCT_FRIEND_REL': {
                fieldVisibility.typeOfCare = false;
                fieldVisibility.subjectAddress = true;
                break;
            }
            case 'SCT_PROSPECT_ADOPT': {
                //hide type of care fields
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'ORD_ADOPT_FP': //falls through
            case 'ORD_ADOPT_NOTFP': //falls through
            case 'CONS_ADOPT_FP': //falls through
            case 'CONS_ADOPT_NOTFP':
            case 'WALES_A8': {
                fieldVisibility.typeOfCare = false;
                break;
            }
            }

            return fieldVisibility;
        },
        /**
         * Returns an object to indicate the enabled
         * state of the optional fields - NOTE
         * just because a field is enabled does not
         * mean it is visible and editable
         */
        getFieldEnablement: function () {
            var enabledFields = Y.app.childlookedafter.NewEpisodeOfCareView.superclass.getFieldEnablement.call(this),
                startReason = this.get('startReason'),
                placementType = this.get('placementType');

            switch (placementType) {
            case 'IND_LIV': {
                enabledFields.carerSubjectType = false;
                enabledFields.typeOfCare = false;
                break;
            }
            case 'RES_EMP':
            case 'PARENT':
            case 'SCT_PARENT': {
                enabledFields.carerSubjectType = false;
                enabledFields.typeOfCare = false;
                break;
            }
            case 'SCT_PROSPECT_ADOPT': //falls through
            case 'ORD_ADOPT_FP': //falls through
            case 'ORD_ADOPT_NOTFP': //falls through
            case 'CONS_ADOPT_FP': //falls through
            case 'CONS_ADOPT_NOTFP': //falls through
            case 'WALES_A8': {
                enabledFields.typeOfCare = false;
                break;
            }
            }
            switch (startReason) {
            case 'L': {
                enabledFields.placementChangeReason = false;
                enabledFields.carerSubjectType = false;
                if (this.typeOfCareNotUnknown()) {
                    enabledFields.typeOfCare = false;
                }
                enabledFields.placementType = false;
                enabledFields.parent = false;
                enabledFields.placementProvision = false;
                break;
            }
            case 'P': {
                enabledFields.legalStatus = false;
                break;
            }
            case 'T': {
                enabledFields.carerSubjectType = false;
                enabledFields.legalStatus = false;
                break;
            }
            case 'B': {
                break;
            }
            case 'U': {
                enabledFields.carerSubjectType = false;
                break;
            }
            }

            return enabledFields;
        },

        typeOfCareNotUnknown: function () {
            return 'NOT_KNOWN' !== this.get('latestEpisode').get('typeOfCare');
        },

        /**
         * Performs the actual show/hide of the fields
         */
        setupFieldVisibility: function () {
            var placementType = this.get('placementType'),
                lockCarerSubjectType = false;

            //control setting the subject type based on the placement Type
            switch (placementType) {
            case 'SCT_OTHER_COMM':
                //falls through
            case 'SCT_LA_HOME':
                //falls through
            case 'SCT_VOL_HOME':
                //falls through
            case 'SCT_RES_SCHOOL':
                //falls through
            case 'SCT_CRISIS':
                //falls through
            case 'SCT_SECURE_ACCOM':
                //falls through
            case 'SCT_OTHER_RES':
                //set the carer subject type and disable
                this.set('carerSubjectType', 'ORGANISATION');
                lockCarerSubjectType = true;
                break;
            case 'SCT_PROSPECT_ADOPT':
                //falls through
            case 'SCT_FRIEND_REL':
                //set the carer subject type and disable
                this.set('carerSubjectType', 'PERSON');
                lockCarerSubjectType = true;
                break;
            }

            //call into the superclass to set the field visibility - also returns the visible state
            var fieldVisibility = Y.app.childlookedafter.NewEpisodeOfCareView.superclass.setupFieldVisibility.call(this);

            var fieldEnablement = this.setupFieldEnablement();

            //update planned change visibility
            this.setPlannedChangeVisibility(fieldVisibility.plannedChange);

            //if the carerSubectType field is visible
            if (fieldVisibility.carerSubjectType && fieldEnablement.carerSubjectType) {
                //update the lock status of the field
                this.lockCarerSubjectType(lockCarerSubjectType);
            }
        },
        setPlannedChangeVisibility: function (visible) {
            this.setFieldVisibility('.plannedEndOfPlacementWrapper', 'radio[name="plannedEndOfPlacement"]', visible);
        },
        setupFieldEnablement: function () {
            Y.app.childlookedafter.NewEpisodeOfCareView.superclass.setupFieldEnablement.call(this);
            var fieldEnablement = this.getFieldEnablement();
            this.setLegalStatusEnablement(fieldEnablement.legalStatus);
            return fieldEnablement;
        },
        setLegalStatusEnablement: function (enabled) {
            var latestEpisode = this.get('latestEpisode'),
                legalStatuses = latestEpisode.get('legalStatuses'),
                container = this.get('container');

            this.setLabelForEnablement('legalStatus', enabled);
            if (enabled) {
                container.all('.legalStatus .remove').removeAttribute('disabled');
                container.one('#legalStatusIds').removeAttribute('disabled');
            } else {
                this._resetLegalStatuses(legalStatuses, this.legalStatusModel);
                container.all('.legalStatus .remove').setAttribute('disabled', 'disabled');
                container.one('#legalStatusIds').setAttribute('disabled', 'disabled');
            }
        },
        getNewSelectionView: function () {
            var _view, controlType = this.get('carerSubjectType');
            var placementType = this.get('placementType');

            var acAttrs = {
                containingNodeSelector: '#episodeOfCareAddForm',
                inputName: 'carerSubjectId',
                formName: 'episodeOfCareAddForm',
                labels: {
                    placeholder: 'Enter name or ID'
                }
            };

            switch (controlType) {
            case 'ORGANISATION':
                _view = new Y.app.childlookedafter.OrganisationAutocompleteView(Y.merge(acAttrs, {
                    autocompleteURL: this.get('organisationAutocompleteURL')
                }));
                break;
            case 'PERSON': {
                switch (placementType) {
                case 'SCT_FRIEND_REL':
                    //default person autocomplete - NOT RESTRICTED TO CARER
                    _view = new Y.app.childlookedafter.PersonAutocompleteView(
                        Y.merge(acAttrs, {
                            autocompleteURL: this.get('personAutocompleteURL')
                        })
                    );
                    break;
                case 'SCT_PROSPECT_ADOPT':
                    //prospective foster carer AC
                    _view = new Y.app.childlookedafter.CarerAutocompleteView(
                        Y.merge(acAttrs, {
                            autocompleteURL: L.sub(this.get('adopterAutocompleteURL'), {
                                childId: this.get('otherData').subject.subjectId
                            })
                        })
                    );
                    break;
                case 'ORD_ADOPT_FP':
                case 'ORD_ADOPT_NOTFP':
                case 'CONS_ADOPT_FP':
                case 'CONS_ADOPT_NOTFP':
                case 'WALES_A8':
                    //prospective adopter(s)
                    _view = new Y.app.childlookedafter.CarerAutocompleteView(Y.merge(acAttrs, {
                        autocompleteURL: L.sub(this.get('adopterChildAutocompleteURL'), {
                            childId: this.get('otherData').subject.subjectId
                        })
                    }));
                    break;
                default:
                    _view = new Y.app.childlookedafter.CarerAutocompleteView(
                        Y.merge(acAttrs, {
                            autocompleteURL: this.get('fosterCarerAutocompleteURL'),
                            //only enable if the start date is currently valid
                            disabled: !this.get('startDateValid')
                        })
                    );
                }

                break;
            }
            }
            return _view;
        },
        handleSetNewLocation: function (locationId) {
            this.placementAddressView.handleSetNewLocation(locationId);
        },
        _setPersonPlacementAddress: function (address) {
            this.get('container').one('#personPlacementAddress').set('innerText', address.location.location);
        },
        _getTypesOfCareForSubjectByDate: function (params) {
            var typeOfCareURL = L.sub(this.get('typeOfCareByDateURL'), {
                'subjectId': params.subjectId,
                'date': params.date
            });
            return new Promise(function (resolve, reject) {
                Y.io(typeOfCareURL, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },

                    on: {
                        success: function (trId, response) {
                            try {
                                var results = JSON.parse(response.response).results;
                                resolve(results);
                            } catch (e) {
                                Y.log('Failed to get types of care', 'debug');
                                reject(e);
                            }
                        }
                    }
                });
            });
        },
    }, {
        ATTRS: {
            latestEpisode: {
                valueFn: function () {
                    return new Y.usp.childlookedafter.EpisodeOfCareWithPlacements({
                        url: L.sub(this.get('latestEpisodeOfCareURL'), {
                            periodOfCareId: this.get('otherData.periodOfCareId')
                        })
                    });
                }
            },
            legalStatusClassifications: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationWithCodePathList({
                        url: this.get('legalStatusesURL')
                    });
                }
            },
            placementTypeClassifications: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationWithCodePathList({
                        url: this.get('placementTypesURL')
                    });
                }
            },
            parentalRelationshipList: {
                valueFn: function () {
                    return new Y.usp.relationship.RelationshipWithPersonAndHomeAddress({
                        url: this.get('parentalRelationshipURL')
                    });
                }
            },
            personAddress: {
                valueFn: function () {
                    return new Y.usp.relationship.RelationshipWithPersonAndHomeAddress({
                        url: this.get('personAddressURL')
                    });
                }
            },
            personPlacementAddress: {
                valueFn: function () {
                    return new Y.usp.person.PersonAddress({
                        url: this.get('personPlacementAddressUrl')
                    });
                }
            },
            /**
             * Keeps track of the start date validity to enable/disable
             * the person auto complete view
             * @attribute startDateValid
             */
            startDateValid: {
                value: false
            },
            /**
             * Track the currently selected placement type
             */
            placementType: {
                value: ''
            },
            /**
             * Track current carer subject type
             */
            carerSubject: {
                value: null
            },
            /**
             * @attribute supportsMultipleLegalStatusEntries
             *
             * Does the dialog for episode entry support multiple
             * legal status entries.
             */
            supportsMultipleLegalStatusEntries: {
                value: false
            }
        }
    });

    Y.namespace('app.childlookedafter').NewAdmittanceEpisodeOfCareView = Y.Base.create('newAdmittanceEpisodeOfCareView', Y.app.childlookedafter.NewPeriodOfCareView, [], {
        template: Y.Handlebars.templates.episodeOfCareAddAdmittanceDialog,
        events: {
            '#legalStatusIds': {
                'change': '_addLegalStatus'
            }
        },

        initializer: function (config) {
            this.events = Y.merge(this.events, Y.app.childlookedafter.NewAdmittanceEpisodeOfCareView.superclass.events);
        },

        getNewSelectionView: function () {
            var _view, controlType = this.get('carerSubjectType');

            switch (controlType) {
            case 'ORGANISATION':
                _view = new Y.app.childlookedafter.OrganisationAutocompleteView({
                    containingNodeSelector: '#episodeOfCareAddAdmittanceForm',
                    inputName: 'carerSubjectId',
                    formName: 'episodeOfCareAddAdmittanceForm',
                    autocompleteURL: this.get('organisationAutocompleteURL'),
                    labels: {
                        placeholder: 'Enter name or ID'
                    }
                });
                break;
            case 'PERSON':
                _view = new Y.app.childlookedafter.CarerAutocompleteView({
                    containingNodeSelector: '#episodeOfCareAddAdmittanceForm',
                    inputName: 'carerSubjectId',
                    formName: 'episodeOfCareAddAdmittanceForm',
                    autocompleteURL: this.get('fosterCarerAutocompleteURL'),
                    labels: {
                        placeholder: 'Enter name or ID'
                    },
                    //only enable if the start date is currently valid
                    disabled: !this.get('startDateValid')
                });
                break;
            }
            return _view;

        },

        getLegalStatuses: function () {
            var container = this.get('container'),
                legalStatuses = [],
                supportsMultipleLegalStatusEntries = this.get('supportsMultipleLegalStatusEntries') || false;
            if (supportsMultipleLegalStatusEntries) {
                var modelList = this.legalStatusModel;
                if (modelList) {
                    legalStatuses = modelList.map(function (m) {
                        return m.get('value').toString();
                    });
                }
            } else {
                legalStatuses.push(container.one('#legalStatus').get('value'));
            }
            return legalStatuses;
        },

        _initStartReason: function (select) {
            var startReasons = Y.Object.values(Y.secured.uspCategory.childlookedafter.episodeStartReason.category.getActiveCodedEntries(null, 'write')).filter(function (codedEntry) {
                return codedEntry.code !== 'S';
            });
            if (startReasons && select) {
                Y.FUtil.setSelectOptions(select, startReasons, null, null, true, true, 'code');
            }
        }

    });

    Y.namespace('app.childlookedafter').EpisodeOfCareForm = Y.Base.create('episodeOfCareForm', Y.usp.childlookedafter.EpisodeOfCareWithPlacementsAndPeriodOfCare, [Y.usp.ModelFormLink], {
        form: '#episodeOfCareViewForm'
    });

    Y.namespace('app.childlookedafter').UpdateEpisodeOfCareForm = Y.Base.create('updateEpisodeOfCareForm',
        Y.usp.childlookedafter.EpisodeOfCareWithPlacementsAndPeriodOfCare,
        [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#episodeOfCareEditForm'
        });

    /**
     * @class app.childlookedafter.EpisodeOfCareView
     * @description Simple view
     */
    Y.namespace('app.childlookedafter').EpisodeOfCareView = Y.Base.create('EpisodeOfCareView', Y.usp.childlookedafter.EpisodeOfCareView, [Y.usp.ModelFormLink], {
        startTemplate: Y.Handlebars.templates.episodeOfCareViewStartDialog,
        changeTemplate: Y.Handlebars.templates.episodeOfCareViewDialog,
        render: function () {
            var container = this.get('container'),
                modelData = this.get('model').toJSON(),
                html,
                mainPlacement = modelData.placements.find(function (placement) {
                    return (placement.mainPlacement || placement.placementCategory === 'MAIN');
                }),
                template = this.changeTemplate;
            if (modelData.startReason === 'S') {
                template = this.startTemplate;
            }

            html = template({
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries'),
                modelData: modelData,
                mainPlacement: mainPlacement,
                displayProperties: this.get('otherData').displayProperties
            });

            container.setHTML(html);
            return this;
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    startReason: Y.secured.uspCategory.childlookedafter.episodeStartReason.category.codedEntries,
                    placementChangeReason: Y.secured.uspCategory.childlookedafter.placementChangeReason.category.codedEntries,
                    placementProvision: Y.uspCategory.childlookedafter.placementProvision.category.codedEntries,
                    endReason: Y.secured.uspCategory.childlookedafter.episodeEndReason.category.codedEntries,
                    careAdmissionReason: Y.uspCategory.childlookedafter.careAdmissionReason.category.codedEntries,
                    typeOfCare: Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries

                }
            }
        }
    });

    /**
     * @class app.childlookedafter.EditEpisodeOfCareView
     * @description Simple view
     */
    Y.namespace('app.childlookedafter').EditEpisodeOfCareView = Y.Base.create('editEpisodeOfCareView',
        Y.app.childlookedafter.BaseAddEditEpisodeOfCareView, [MixinOptgroup], {
            template: Y.Handlebars.templates.episodeOfCareEditDialog,
            events: {
                'input[name="carerSubjectType"]': {
                    'change': '_handleCarerSubjectTypeInputChange'
                }
            },
            initializer: function () {
                this._evtHandlers = [
                    this.after('*:selectedPersonChange', this._handleSelectedPersonChange, this),
                    this.after('carerSubjectTypeChange', this._handleCarerSubjectTypeChange, this),
                    this.on('*:saved', this.refreshPersonHeaderAddress, this)
                ];

            },
            mainPlacement: function (placements) {
                return placements.find(function (placement) {
                    return (placement.mainPlacement || placement.placementCategory === 'MAIN');
                });
            },
            render: function () {
                var container = this.get('container'),
                    modelData = this.get('model').toJSON(),
                    mainPlacement = this.mainPlacement(modelData.placements),
                    html = this.template({
                        labels: this.get('labels'),
                        codedEntries: this.get('codedEntries'),
                        modelData: modelData,
                        mainPlacement: mainPlacement,
                        displayProperties: this.get('otherData').displayProperties
                    });

                container.setHTML(html);

                //setup the initial field visibility
                this.setupFieldVisibility();

                if (mainPlacement.carerSubject) {
                    if (mainPlacement.carerSubject.type === 'PERSON') {
                        mainPlacement.carerSubject.personIdentifier = mainPlacement.carerSubject.identifier;
                        this._setTypesOfCare(mainPlacement.carerSubject.id);
                    }
                    if (mainPlacement.carerSubject.type === 'ORGANISATION') {
                        mainPlacement.carerSubject.organisationIdentifier = mainPlacement.carerSubject.identifier;
                    }
                }
                return this;
            },
            destructor: function () {
                this._evtHandlers.forEach(function (handler) {
                    handler.detach();
                });
                delete this._evtHandlers;

                if (this.selectionView) {
                    this.selectionView.removeTarget(this);
                    this.selectionView.destroy();
                    delete this.selectionView;
                }
            },

            getNewSelectionView: function () {
                var _view, controlType = this.get('carerSubjectType'),
                    placements = this.get('model').toJSON().placements,
                    mainPlacement = this.mainPlacement(placements),
                    mainPlacementTypeCode = mainPlacement.placementType.code;

                switch (controlType) {
                case 'ORGANISATION':
                    _view = new Y.app.childlookedafter.OrganisationAutocompleteView({
                        containingNodeSelector: '#episodeOfCareEditForm',
                        inputName: 'carerSubjectId',
                        formName: 'episodeOfCareEditForm',
                        autocompleteURL: this.get('organisationAutocompleteURL'),
                        labels: {
                            placeholder: 'Enter name or ID'
                        }
                    });
                    break;
                case 'PERSON':
                    switch (mainPlacementTypeCode) {
                    case 'WALES_A8':
                        //prospective adopter(s)
                        _view = new Y.app.childlookedafter.CarerAutocompleteView({
                            containingNodeSelector: '#episodeOfCareEditForm',
                            inputName: 'carerSubjectId',
                            formName: 'episodeOfCareEditForm',
                            autocompleteURL: L.sub(this.get('adopterAutocompleteURL'), {
                                childId: this.get('subject').subjectId
                            }),
                            labels: {
                                placeholder: 'Enter name or ID'
                            },
                        });
                        break;
                    default:
                        _view = new Y.app.childlookedafter.CarerAutocompleteView({
                            containingNodeSelector: '#episodeOfCareEditForm',
                            inputName: 'carerSubjectId',
                            formName: 'episodeOfCareEditForm',
                            autocompleteURL: this.get('fosterCarerAutocompleteURL'),
                            labels: {
                                placeholder: 'Enter name or ID'
                            },
                        });
                        break;
                    }
                }
                return _view;
            },

            /**
             * Performs the actual show/hide of the fields
             */
            setupFieldVisibility: function () {
                //call into the superclass to set the field visibility
                Y.app.childlookedafter.EditEpisodeOfCareView.superclass.setupFieldVisibility.call(this);
                this.setupFieldEnablement();
            },

            getFieldVisibility: function () {
                //get the default field visibility from the parent class
                var fieldVisibility = Y.app.childlookedafter.EditEpisodeOfCareView.superclass.getFieldVisibility.call(this);
                var mainPlacements = this.get('model').toJSON().placements,
                    mainPlacement = this.mainPlacement(mainPlacements),
                    placementTypeCode = mainPlacement.placementType.code;

                switch (placementTypeCode) {
                case 'WALES_A8':
                    fieldVisibility.typeOfCare = false;
                    break;
                default:
                }
                return fieldVisibility;
            },

            _handleSelectedPersonChange: function (e) {
                this._setUpTypesOfCareForSubject(e);
                this._handleSelectedSubjectAddressChange(e);
            },

            refreshPersonHeaderAddress: function () {
                Y.fire('person:addressChanged');
            },
        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        startReason: Y.secured.uspCategory.childlookedafter.episodeStartReason.category.codedEntries,
                        placementChangeReason: Y.secured.uspCategory.childlookedafter.placementChangeReason.category.codedEntries,
                        placementProvision: Y.uspCategory.childlookedafter.placementProvision.category.codedEntries,
                        endReason: Y.secured.uspCategory.childlookedafter.episodeEndReason.category.codedEntries,
                        careAdmissionReason: Y.uspCategory.childlookedafter.careAdmissionReason.category.codedEntries,
                        typeOfCare: Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries

                    }
                },
                fosterCarerAutocompleteURL: {},
                organisationAutoCompleteURL: {},
                carerSubject: {
                    value: null
                },
            }
        });

    Y.namespace('app.childlookedafter').MigClaFirstEpisodeOfCareView = Y.Base.create('migClaFirstEpisodeOfCareView', Y.usp.View, [], {
        template: Y.Handlebars.templates.migClaFirstEpisodeOfCareViewDialog
    });

    Y.namespace('app.childlookedafter').MigClaOtherEpisodeOfCareView = Y.Base.create('migClaOtherEpisodeOfCareView', Y.usp.View, [], {
        template: Y.Handlebars.templates.migClaOtherEpisodeOfCareViewDialog
    });

    Y.namespace('app.childlookedafter').DeleteEpisodeOfCareView = Y.Base.create('deleteEpisodeOfCareView', Y.usp.View, [], {
        template: Y.Handlebars.templates.episodeOfCareDeleteDialog
    });

    Y.namespace('app.childlookedafter').DeleteLastEpisodeOfCareWarningView = Y.Base.create('deleteLastEpisodeOfCareWarningView', Y.usp.View, [], {
        template: Y.Handlebars.templates.episodeOfCareDeleteLastEpisodeWarningDialog
    });

    Y.namespace('app.childlookedafter').DeleteInProgressView = Y.Base.create('deleteInProgressView', Y.usp.View, [], {
        template: Y.Handlebars.templates.episodeOfCareDeleteInProgress
    });

    Y.namespace('app.childlookedafter').EpisodeOfCareDialog = Y.Base.create('episodeOfCareDialog', Y.usp.app.AppDialog, [], {
        saveEpisodeOfCare: function (e, data, transmogrify) {
            this.handleSave(e, Y.merge({
                //potential hidden fields are reset to undefined here
                //they will be re-populated by model-form-link where they
                //are still valid. This fixes the issue of stale data if
                //the user changes the options after a save attempt had
                //failed with validation errors
                typeOfCare: undefined,
                carerSubjectType: undefined,
                carerSubjectId: undefined,
                placementTypeId: undefined,
                placementProvision: undefined,
                placementChangeReason: undefined,
                plannedEndOfPlacement: undefined
            }, data), transmogrify);
        },
        handleAddEpisodeOfCareSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                carerSubjectType = undefined,
                carerSubjectId = undefined,
                startReason = view.get('startReason'),
                sameCarerReason = (startReason === 'U' || startReason === 'L' || startReason === 'T'),
                legalStatusIds = view.getLegalStatuses(),
                selectedPlacementTypeCode = container.one('#placementType').get('options')._nodes[container.one('#placementType').get('selectedIndex')].attributes.code,
                independentLiving = (selectedPlacementTypeCode && (selectedPlacementTypeCode.value === 'RES_EMP' || selectedPlacementTypeCode.value === 'IND_LIV' || selectedPlacementTypeCode.value === 'SCT_FRIEND_REL')) ? true : false,
                livingWithParent = (selectedPlacementTypeCode && (selectedPlacementTypeCode.value === 'PARENT' || selectedPlacementTypeCode.value === 'SCT_PARENT')) ? true : false;
            if (independentLiving | sameCarerReason) {
                carerSubjectType = null;
                carerSubjectId = null;
            } else if (livingWithParent && !sameCarerReason) {
                carerSubjectType = 'PERSON';
                carerSubjectId = container.one('#parent').get('value');
            }

            var values = {
                periodOfCareId: view.get('otherData').periodOfCareId,
                carerSubjectType: carerSubjectType,
                carerSubjectId: carerSubjectId,
                legalStatusIds: legalStatusIds
            };

            var transmogrify;
            if (startReason !== 'L' && selectedPlacementTypeCode.value === 'IND_LIV') {
                var roomDescription = container.one('#roomDescription-input').get('value'),
                    floorDescription = container.one('#floorDescription-input').get('value'),
                    locationId = view.placementAddressView.get('selectedLocation');

                var addressValues = {
                    locationId: locationId,
                    addressType: 'PLACEMENT',
                    addressUsage: 'PERMANENT',
                    roomDescription: roomDescription,
                    floorDescription: floorDescription
                };

                values = Y.merge(values, addressValues);
                transmogrify = Y.app.childlookedafter.NewEpisodeOfCareWithPlacementAddressForm;
            }

            this.saveEpisodeOfCare(e, values, {
                transmogrify: transmogrify
            });
        },

        handleEditEpisodeOfCareViewSave: function (e) {
            var view = this.get('activeView'),
                model = view.get('model');
            model.url = view.get('placementUrl'),
                placements = model.get('placements'),
                carerSubjectType = view.get('carerSubjectType'),
                mainPlacement = null,
                carerSubjectId = null,
                personSubjectFieldIsEmpty = true,
                organisationCarerSubjectFieldIsEmpty = true;

            if (view.selectionView !== null && typeof view.selectionView !== 'undefined') {
                personSubjectFieldIsEmpty = view.selectionView.autoCompleteView.get('selectedPerson') === null || typeof view.selectionView.autoCompleteView.get('selectedPerson') === 'undefined',
                    organisationCarerSubjectFieldIsEmpty = view.selectionView.autoCompleteView.get('selectedOrganisation') === null || typeof view.selectionView.autoCompleteView.get('selectedOrganisation') === 'undefined';
            }

            if (!personSubjectFieldIsEmpty) {
                carerSubjectId = view.selectionView.autoCompleteView.get('selectedPerson').id;
            }

            if (!organisationCarerSubjectFieldIsEmpty) {
                carerSubjectTypeValue = 'ORGANISATION';
                carerSubjectId = view.selectionView.autoCompleteView.get('selectedOrganisation').id;
                model.set('typeOfCare', undefined);
            }

            mainPlacement = placements.find(function (placement) {
                return (placement.mainPlacement || placement.placementCategory === 'MAIN');
            });

            this.handleSave(e, {
                id: mainPlacement.id,
                carerSubjectType: carerSubjectType,
                carerSubjectId: carerSubjectId
            }, {
                transmogrify: Y.usp.childlookedafter.UpdateCarerPlacement
            });
        },

        handleAddEpisodeOfCareAdmittanceSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                carerSubjectType = undefined,
                carerSubjectId = undefined,
                startReason = 'S',
                sameCarerReason = (startReason === 'U' || startReason === 'L' || startReason === 'T'),
                legalStatusIds = view.getLegalStatuses(),
                selectedPlacementTypeCode = container.one('#placementType').get('options')._nodes[container.one('#placementType').get('selectedIndex')].attributes.code,
                independentLiving = (selectedPlacementTypeCode && (selectedPlacementTypeCode.value === 'RES_EMP' || selectedPlacementTypeCode.value === 'IND_LIV' || selectedPlacementTypeCode.value === 'SCT_FRIEND_REL')) ? true : false,
                livingWithParent = (selectedPlacementTypeCode && (selectedPlacementTypeCode.value === 'PARENT' || selectedPlacementTypeCode.value === 'SCT_PARENT')) ? true : false;
            if (independentLiving | sameCarerReason) {
                carerSubjectType = null;
                carerSubjectId = null;
            } else if (livingWithParent && !sameCarerReason) {
                carerSubjectType = 'PERSON';
                carerSubjectId = container.one('#parent').get('value');
            }

            var values = {
                periodOfCareId: view.get('otherData').periodOfCareId,
                carerSubjectType: carerSubjectType,
                carerSubjectId: carerSubjectId,
                legalStatusIds: legalStatusIds,
                startReason: startReason
            };

            var transmogrify;
            if (startReason !== 'L' && selectedPlacementTypeCode.value === 'IND_LIV') {
                var roomDescription = container.one('#roomDescription-input').get('value'),
                    floorDescription = container.one('#floorDescription-input').get('value'),
                    locationId = view.placementAddressView.get('selectedLocation');

                var addressValues = {
                    locationId: locationId,
                    addressType: 'PLACEMENT',
                    addressUsage: 'PERMANENT',
                    roomDescription: roomDescription,
                    floorDescription: floorDescription
                };

                values = Y.merge(values, addressValues);
                transmogrify = Y.app.childlookedafter.NewEpisodeOfCareWithPlacementAddressForm;
            }

            this.saveEpisodeOfCare(e, values, {
                transmogrify: transmogrify
            });
        },

        findDeletionRequest: function (url, episodeOfCareId) {
            var deletionRequestId;

            this.getEpisodeDeletionRequest(url, episodeOfCareId).then(function (data) {
                var deletionRequest = data.find(function (r) {
                    return r.deletionState === 'READY_TO_DELETE';
                });

                if (this.timerId) {
                    clearTimeout(this.timerId);
                }

                if (deletionRequest) {
                    deletionRequestId = deletionRequest.id;
                    this.get('activeView').fire('episodeOfCare:deleteEpisode', {
                        episodeOfCareId: episodeOfCareId,
                        deletionRequestId: deletionRequestId
                    });
                } else {
                    //TODO - what if it failed?
                    //should there be a maximum poll count

                    //to get out of this situation - the user can hit the cancel button

                    //poll
                    this.timerId = setTimeout(this.findDeletionRequst.bind(this), 10000, url, episodeOfCareId);
                }
            }.bind(this));
        },

        handlePrepareDeleteEpisodeOfCare: function () {
            var view = this.get('activeView'),
                urls = view.get('otherData').urlConfig,
                model = view.get('model'),
                episodeOfCareId = view.get('otherData.episodeOfCareId'),
                getUrl = L.sub(urls.url, {
                    episodeOfCareId: episodeOfCareId
                });

            //bind an error handler on the  model
            model.on('error', function (e) {
                    var error = e.error || {};
                    //timeout error code -start to poll for deletion request
                    if (error.code === 504 || error.code === 0) {
                        //stop error bubble
                        e.halt(true);

                        this.findDeletionRequest(getUrl, episodeOfCareId);
                    }
                    //just let the event bubble as normal
                }, this),

                this.showDialogMask();

            model.save({}, Y.bind(function (err) {
                if (err === null) {
                    this.hideDialogMask();
                    //model has id of newly created deletion request
                    view.fire('episodeOfCare:deleteEpisode', {
                        episodeOfCareId: episodeOfCareId,
                        deleteRequestId: model.get('id')
                    });
                }
            }, this));
        },

        views: {
            add: {
                type: Y.app.childlookedafter.NewEpisodeOfCareView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddEpisodeOfCareSave',
                    disabled: true
                }]
            },
            addAdmittance: {
                type: Y.app.childlookedafter.NewAdmittanceEpisodeOfCareView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddEpisodeOfCareAdmittanceSave',
                    disabled: true
                }]
            },
            view: {
                type: Y.app.childlookedafter.EpisodeOfCareView,
                buttons: []
            },
            edit: {
                type: Y.app.childlookedafter.EditEpisodeOfCareView,
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleEditEpisodeOfCareViewSave',
                    disabled: true
                }]
            },
            prepareDeleteEpisode: {
                type: Y.app.childlookedafter.DeleteEpisodeOfCareView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    action: 'handlePrepareDeleteEpisodeOfCare',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            deleteLastEpisodeWarning: {
                type: Y.app.childlookedafter.DeleteLastEpisodeOfCareWarningView,
                buttons: [{
                    name: 'okButton',
                    labelHTML: '<i class="fa fa-check"></i> Ok',
                    action: 'handleCancel',
                    disabled: true
                }]
            },
            deleteEpisode: {
                type: Y.app.childlookedafter.DeleteEpisodeOfCareView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            deleteInProgress: {
                type: Y.app.childlookedafter.DeleteInProgressView,
            },
            viewFirstMigClaEpisode: {
                type: Y.app.childlookedafter.MigClaFirstEpisodeOfCareView
            },
            viewOtherMigClaEpisode: {
                type: Y.app.childlookedafter.MigClaOtherEpisodeOfCareView
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'usp-date',
        'app-dialog',
        'model-form-link',
        'childlookedafter-mixin-classifications-optgroup',
        'childlookedafter-autocomplete-view',
        'childlookedafter-component-enumerations',
        'usp-childlookedafter-EpisodeOfCareWithPlacements',
        'usp-childlookedafter-NewEpisodeOfCare',
        'usp-childlookedafter-NewEpisodeOfCareWithPlacementAddress',
        'usp-childlookedafter-NewPlacement',
        'usp-childlookedafter-EpisodeOfCareWithPlacementsAndPeriodOfCare',
        'usp-childlookedafter-UpdateCarerPlacement',
        'usp-person-PersonAddress',
        'usp-deletion-DeletionRequest',
        'usp-classification-ClassificationWithCodePath',
        'secured-categories-childlookedafter-component-PlacementChangeReason',
        'secured-categories-childlookedafter-component-EpisodeStartReason',
        'secured-categories-childlookedafter-component-EpisodeEndReason',
        'categories-childlookedafter-component-PlacementProvision',
        'categories-childlookedafter-component-TypeOfCare',
        'categories-childlookedafter-component-CareAdmissionReason',
        'usp-relationship-RelationshipWithPersonAndHomeAddress',
        'entry-dialog-views',
        'handlebars',
        'handlebars-helpers',
        'handlebars-childlookedafter-templates',
        'calendar-popup',
        'person-autocomplete-view',
        'organisation-autocomplete-view',
        'childlookedafter-subject-autocompleteview',
        'base-care-view',
        'base-addedit-episodeofcare-view',
        'placement-address-add-view',
        'model-transmogrify',
        'usp-childlookedafter-NewAdmittanceEpisodeOfCare',
        'periodofcare-dialog'
    ]
});