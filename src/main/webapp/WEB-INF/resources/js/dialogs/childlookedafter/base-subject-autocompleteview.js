YUI.add('childlookedafter-subject-autocompleteview', function (Y) {
    'use-strict';

    /**
     * Provides a base class for autocomplete views when the type of
     * autocomplete changes based on a user selectable field.
     * 
     * This view allows the previous autocomplete to be correctly
     * destroyed and the new autocomplete to be created
     */
    Y.namespace('app.childlookedafter').BaseSubjectAutoCompleteView = Y.Base.create('BaseSubjectAutoCompleteView', Y.View, [], {
        template: Y.Handlebars.templates.childLookedAfterAutoCompleteView,
        getAutoCompleteViewConstructor: function () {
            throw new TypeError('Please implement getAutoCompleteViewConstructor');
        },
        initializer: function (config) {
            this.autoCompleteView = new(this.getAutoCompleteViewConstructor())({
                containingNodeSelector: config.containingNodeSelector,
                autocompleteURL: config.autocompleteURL,
                inputName: config.inputName,
                formName: config.formName,
                rowTemplate: 'ROW_TEMPLATE_SIMPLE',
                labels: config.labels,
                disabled:config.disabled
            });

            this.autoCompleteView.addTarget(this);
            this._evtHandlers=[
                this.after('disabledChange', this.handleAfterDisabledChange, this)
            ];
        },
        render: function () {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //render the main template
            contentNode.setHTML(this.template());

            //add our fragments to the content node
            contentNode.one('#subjectSearchWrapper').append(this.autoCompleteView.render().get('container'));

            //set the fragment into the view container
            container.setHTML(contentNode);

            return this;
        },
        handleAfterDisabledChange:function(e){
            //pass on the property
            this.autoCompleteView.set('disabled', e.newVal);
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.autoCompleteView.removeTarget(this);
            this.autoCompleteView.destroy();
            delete this.autoCompleteView;
        }
    }, {
        ATTRS: {
            autocompleteURL: {},
            disabled:{}
        }
    });

    Y.namespace('app.childlookedafter').PersonAutocompleteView = Y.Base.create('personAutocompleteView', Y.app.childlookedafter.BaseSubjectAutoCompleteView, [], {
        getAutoCompleteViewConstructor: function () {
            return Y.app.PersonAutoCompleteResultView;
        }
    });

    Y.namespace('app.childlookedafter').OrganisationAutocompleteView = Y.Base.create('organisationAutocompleteView', Y.app.childlookedafter.BaseSubjectAutoCompleteView, [], {
        getAutoCompleteViewConstructor: function () {
            return Y.app.OrganisationAutoCompleteResultView;
        }
    }, {
        ATTRS: {
            disabled:{}
        }
    });

    Y.namespace('app.childlookedafter').CarerAutocompleteView = Y.Base.create('carerAutocompleteView', Y.app.childlookedafter.BaseSubjectAutoCompleteView, [], {
        getAutoCompleteViewConstructor: function () {
            return Y.app.childlookedafter.CarerAutoCompleteResultView
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'handlebars',
        'handlebars-childlookedafter-templates',
        'person-autocomplete-view',
        'organisation-autocomplete-view',
        'childlookedafter-autocomplete-view'
    ]
});