YUI.add('placement-address-add-view', function (Y) {

    var L = Y.Lang,
        A = Y.Array,
        E = Y.Escape,
        fields = {
            noSearchResultsNode: false,
            overMaxResultsNode: false,
            addressNoteNode: true,
            locationsNode: true,
            houseSearchNode: true,
            postcodeSearchNode: true,
            addLocationNode: true
        };

    Y.namespace('app.childlookedafter').PlacementAddressAddView = Y.Base.create('placementAddressAddView', Y.usp.View, [], {
        template: Y.Handlebars.templates.placementAddressAddView,
        events: {
            '#locations li a': {
                click: 'changeLocation'
            }
        },
        initializer: function () {
            Y.app.childlookedafter.PlacementAddressAddView.superclass.initializer.call(this);

            this.fieldVisibility = new Y.Model(fields);

            this._evtHandlers = [
                this.fieldVisibility.on('change', this._handleFieldVisibilty, this)
            ];
        },
        render: function () {
            Y.app.childlookedafter.PlacementAddressAddView.superclass.render.call(this);

            this._handleFieldVisibilty();
            this._initLocationSearch();

            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;
        },
        _initLocationSearch: function () {
            var container = this.get('container'),
                acUrl = this.get('locationUrl'),
                acQueryParamsUrl = this.get('locationQueryParamsUrl'),
                acNode = container.one('#postcodeSearch-input'),
                pageSize = 50;

            acNode.plug(Y.Plugin.app.AutoCompleteAddress, {
                width: '48em',
                allowBrowserAutocomplete: false,
                maxResults: pageSize,
                enableCache: false,
                minQueryLength: 2,
                resultHighlighter: 'phraseMatch',
                resultListLocator: 'results',
                resultTextLocator: 'location',
                requestTemplate: function () {
                    var nameOrNumber = container.one('#houseSearch-input').get('value'),
                        postcode = container.one('#postcodeSearch-input').get('value');

                    return L.sub(acQueryParamsUrl, {
                        nameOrNumber: nameOrNumber,
                        postcode: postcode,
                        pageSize: pageSize
                    });
                },
                source: acUrl,
                viewContainer: container,
                resultFormatter: function (query, results) {
                    return A.map(results, function (result) {
                        var html = '<div title="{title}"><div><span>' + E.html(result.raw.location) + '</span></div>';
                        return html;
                    });
                }
            });

            acNode.ancestor('div').addClass('yui3-skin-sam');

            acNode.ac.on('select', function (e) {
                this.afterAutoCompleteSelect(e);
            }, this);

            // we check returned results displaying an info message if none are returned 
            acNode.ac.on('results', function (e) {

                if (e.data.results.length === 0) {

                    this.fieldVisibility.setAttrs({
                        noSearchResultsNode: true,
                        overMaxResultsNode: false,
                        addressNoteNode: false,
                        locationsNode: false
                    });

                } else {

                    //advise user to refine search if there are more results than our maximum visible results
                    if (e.data.totalSize > pageSize) {
                        this.fieldVisibility.set('overMaxResultsNode', true);
                    } else {
                        this.fieldVisibility.set('overMaxResultsNode', false);
                    }

                    this.fieldVisibility.setAttrs({
                        noSearchResultsNode: false,
                        addressNoteNode: false,
                        locationsNode: true
                    });
                }
            }, this);

            container.one('#houseSearch-input').on('keyup', function () {
                acNode.ac.sendRequest();
            });
        },
        afterAutoCompleteSelect: function (e) {
            var location = e.result.raw.location,
                locationId = e.result.raw.id;

            this.handleSetLocationsValue({
                location: location,
                id: locationId
            });
        },
        changeLocation: function () {
            this.handleSetLocationsValue();
            this._handleSetHouseSearchValue('');
            this._handleSetPostcodeSearchValue('');

            this.fieldVisibility.setAttrs({
                addressNoteNode: true,
                houseSearchNode: true,
                postcodeSearchNode: true,
                addLocationNode: true
            });
        },
        handleSetNewLocation: function (locationId) {
            var locationModel = new Y.usp.address.Location({
                url: this.get('locationUrl') + '/' + locationId
            });

            var locationPromise = new Y.Promise(function (resolve, reject) {
                locationModel.load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            locationPromise.then(function (data) {
                this.handleSetLocationsValue({
                    location: data.location,
                    id: locationId
                });
            }.bind(this));
        },
        _handleFieldVisibilty: function () {
            this._handleNoSearchResultsNodeVisibility(this.fieldVisibility.get('noSearchResultsNode'));
            this._handleOverMaxResultsNodeVisibility(this.fieldVisibility.get('overMaxResultsNode'));
            this._handleAddressNoteNodeVisibility(this.fieldVisibility.get('addressNoteNode'));
            this._handleLocationsNodeVisibility(this.fieldVisibility.get('locationsNode'));
            this._handleHouseSearchNodeVisibility(this.fieldVisibility.get('houseSearchNode'));
            this.handleAddLocationNodeVisibility(this.fieldVisibility.get('addLocationNode'));
            this._handlePostcodeSearchNodeVisibility(this.fieldVisibility.get('postcodeSearchNode'));
        },
        _handleNoSearchResultsNodeVisibility: function (visible) {
            this._handleNodeVisibility(visible, '#noSearchResults-wrapper');
        },
        _handleOverMaxResultsNodeVisibility: function (visible) {
            this._handleNodeVisibility(visible, '#overMaxResults-wrapper');
        },
        _handleAddressNoteNodeVisibility: function (visible) {
            this._handleNodeVisibility(visible, '#address_search_note');
        },
        _handleLocationsNodeVisibility: function (visible) {
            this._handleNodeVisibility(visible, '#locations');
        },
        _handleHouseSearchNodeVisibility: function (visible) {
            this._handleNodeVisibility(visible, '#houseSearch-wrapper');
        },
        _handlePostcodeSearchNodeVisibility: function (visible) {
            this._handleNodeVisibility(visible, '#postcodeSearch-wrapper');
        },
        handleAddLocationNodeVisibility: function (visible) {
            this._handleNodeVisibility(visible, '#location_toggleAddLocation');
        },
        _handleSetHouseSearchValue: function (value) {
            this.get('container').one('#houseSearch-input').set('value', value);
        },
        _handleSetPostcodeSearchValue: function (value) {
            this.get('container').one('#postcodeSearch-input').set('value', value);
        },
        handleSetLocationsValue: function (locationAttrs) {
            var container = this.get('container'),
                html = '<li class="data-item" id="locationId"><span>{location}</span><a href="#none" class="remove small">Change</a></li>';

            if (locationAttrs) {

                this.set('selectedLocation', locationAttrs.id);

                container.one('#locations ul').setHTML(L.sub(html, {
                    location: E.html(locationAttrs.location)
                }));

                this.fieldVisibility.setAttrs({
                    houseSearchNode: false,
                    postcodeSearchNode: false,
                    overMaxResultsNode: false,
                    addLocationNode: false,
                    addressNoteNode: false
                });
            } else {
                this.set('selectedLocation', '');
                container.one('#locations ul').setHTML('');
            }
        },
        _handleNodeVisibility: function (visible, nodeIdentifier) {
            var node = this.get('container').one(nodeIdentifier);
            if (visible) {
                node.show();
            } else {
                node.hide();
            }
        }
    }, {
        ATTRS: {
            selectedLocation: {
                value: ''
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'usp-view',
        'usp-address-Location',
        'app-address-autocomplete',
        'location-dialog',
        'handlebars-location-templates'
    ]
});