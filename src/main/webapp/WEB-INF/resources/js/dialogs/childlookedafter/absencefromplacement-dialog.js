YUI.add('absencefromplacement-dialog', function(Y) {

    'use-strict';

    var L = Y.Lang,
        O = Y.Object,
        BaseView;


    /**
     * @class BaseView
     * @description BaseView for all AbsenceFromPlacement views
     */
    BaseView = Y.Base.create('baseView', Y.usp.childlookedafter.NewAbsenceFromPlacementView, [], {
    	
        render: function() {
            var container = this.get('container')

            container.setHTML(this.template({
                codedEntries: this.get('codedEntries'),
                labels: this.get('labels'),
                modelData: this.get('model').toJSON()
            }));

            if (container.one('#startDateValue')) {
                this.startDateCalendar = new Y.usp.CalendarPopup({
                    inputNode: container.one('#startDateValue'),
                    pickerStyle: 'DateTime'
                }).render();
            }

            if (container.one('#endDateValue')) {
                this.endDateCalendar = new Y.usp.CalendarPopup({
                    inputNode: container.one('#endDateValue'),
                    pickerStyle: 'DateTime'
                }).render();
            }

            return this;
        },

        destructor: function() {
            if (this.startDateCalendar) {
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }
            if (this.endDateCalendar) {
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
        },

        _initAbsenceTypes: function(val) {
            var absenceTypes = this.get('absenceTypes'),
                select = this.get('container').one('#absenceType');

            if(val===undefined||val===null||val==='') val='';  //ensure a falsey string for IE11

            if (absenceTypes && select) {
                Y.FUtil.setSelectOptions(select, absenceTypes, null, null, true, true);
                select.set('value', val.toString());  //ensure value is a string for IE11
            }
        },

        _initReasonMissing: function(val) {
            var reasonsMissing = O.values(Y.secured.uspCategory.childlookedafter.reasonMissing.category.getActiveCodedEntries(null, 'write')),
                select = this.get('container').one('#reasonMissing');

            if(val===undefined||val===null||val==='') val=''; //ensure a falsey string for IE11

            if (reasonsMissing && select) {
                Y.FUtil.setSelectOptions(select, reasonsMissing, null, null, true, true, 'code');
                select.set('value', val.toString()); //ensure value is a string for IE11
            }
        }
    }, {
        ATTRS: {
            absenceTypes: {
                value: null
            },
            classifications: {
                valueFn: function() {
                    return new Y.usp.classification.ClassificationList({
                        url: this.get('absenceTypesURL')
                    })
                }
            },
            codedEntries: {
                value: {
                    reasonMissing: Y.uspCategory.childlookedafter.reasonMissing.category.codedEntries
                }
            }
        }
    });


    /**
     * @class Y.app.childlookedafter.NewAbsenceFromPlacementForm
     * @description Model-form-linked model for AbsenceFromPlacementAdd
     */
    Y.namespace('app.childlookedafter').NewAbsenceFromPlacementForm = Y.Base.create('newAbsenceFromPlacementForm',
        Y.usp.childlookedafter.NewAbsenceFromPlacement, [Y.usp.ModelFormLink], {
            form: '#absenceFromPlacementAddForm',
            customValidation: function(attrs) {
                var errors = {},
                startDateTime,
                endDateTime;
                
                if(L.isNumber(attrs.startDate)) {
              	  startDateTime = new Date(attrs.startDate);
                } else {
                  startDateTime = Y.USPDate.parseDateTime(attrs.startDate);
                }
                if(attrs.startDate){
                	if(!L.isDate(startDateTime)){
                    	errors.startDate = this.labels.startDateFormat;
                    }
                } else {
                	errors.startDate = this.labels.startDateMandatory;
                }
                
                
                if(L.isNumber(attrs.endDate)) {
              	  endDateTime = new Date(attrs.endDate);
                } else {
                  endDateTime = Y.USPDate.parseDateTime(attrs.endDate);
                }
                
                if(attrs.endDate) {
                	if(!L.isDate(endDateTime)) {
                		errors.endDate = this.labels.endDateFormat;
                	}
                }
                
                if(attrs.absenceType==='') {
                    errors.absenceType = this.labels.absenceTypeMandatory;
                }

                return errors;
            }
        }, {
          ATTRS: {
            // dateTime attribute
            startDate: {
              value: null,
              setter: function(value) {
            	  var parsedDateTime = Y.USPDate.parseDateTime(value);
                  if (L.isDate(parsedDateTime)) {
                      return parsedDateTime.getTime();
                  }
                  
                  return value;
              }, 
              getter: function(value) {
            	  if (L.isNumber(value)) {
                      return value;
                  }
                  if (L.isDate(value)) {
                      return value.getTime();
                  }
                  if (L.trim(value) === '') {
                      return null;
                  }
                  
                  return value;
              }
            },
            // dateTime attribute
            endDate: {
            	value: null,
                setter: function (value) {
              	  var parsedDateTime = Y.USPDate.parseDateTime(value);
                    if (L.isDate(parsedDateTime)) {
                        return parsedDateTime.getTime();
                    }
                    
                    return value;
                }, 
                getter: function(value) {
              	  	if (L.isNumber(value)) {
                        return value;
                    }
                    if (L.isDate(value)) {
                        return value.getTime();
                    }
                    if (L.trim(value) === '') {
                        return null;
                    }
                    
                    return value;
                }
            }
          }
        });


    /**
     * @class Y.app.childlookedafter.AbsenceFromPlacementAdd
     * @description Add view
     */
    Y.namespace('app.childlookedafter').AbsenceFromPlacementAdd = Y.Base.create('NewAbsenceFromPlacementView', BaseView, [], {
        events: {
          'input[name="childOfferedReturnInterview"]':{
            'change': '_handleChildOfferedReturnInterviewChange'
          }
        },
        template: Y.Handlebars.templates.absenceFromPlacementAddDialog,
        initializer: function() {

            var classificationsLoad = new Y.Promise(function(resolve, reject) {
                this.get('classifications').load(function(err, res) {
                    err ? reject(err) : resolve(res);
                });
            }.bind(this));

            Y.Promise.all([classificationsLoad]).then(function(data) {
                this.set('absenceTypes', JSON.parse(data[0].response));
                this._initAbsenceTypes();
                this._initReasonMissing();
            }.bind(this));
        },
        render: function() {
            Y.app.childlookedafter.AbsenceFromPlacementAdd.superclass.render.call(this);
            
            return this;
        },
        _handleChildOfferedReturnInterviewChange: function(e) {
          this._setChildAcceptedReturnInterviewFields(e.target.get('value'));
        },
        _setChildAcceptedReturnInterviewFields: function(value) {
          var container = this.get('container');
          
          if(value === 'true') {
            container.one('#childAcceptedReturnInterviewWrapper').setStyle('display', 'block');
            container.all('#childAcceptedReturnInterviewWrapper input[type="radio"]').removeAttribute('disabled');
          } else {
            container.one('#childAcceptedReturnInterviewWrapper').setStyle('display', 'none');
            container.all('#childAcceptedReturnInterviewWrapper input[type="radio"]').setAttribute('disabled', 'disabled');
          }
        }
    });



    /**
     * @class Y.app.childlookedafter.UpdateAbsenceFromPlacementForm
     * @description Model-form-linked model for AbsenceFromPlacementEdit
     */
    Y.namespace('app.childlookedafter').UpdateAbsenceFromPlacementForm = Y.Base.create('updateAbsenceFromPlacementForm',
        Y.usp.childlookedafter.AbsenceFromPlacement, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#absenceFromPlacementEditForm',
            customValidation: function(attrs) {
              var errors = {},
              endDateTime;
                
                if(L.isNumber(attrs.endDate)) {
              	  endDateTime = new Date(attrs.endDate);
                } else {
                  endDateTime = Y.USPDate.parseDateTime(attrs.endDate);
                }
                
                if(attrs.endDate) {
                	if(!L.isDate(endDateTime)) {
                		errors.endDate = this.labels.endDateFormat;
                	}
                }

              if(attrs.absenceType==='') {
                errors.absenceType = this.labels.absenceTypeMandatory;
              }

              return errors;
            }
        }, {
          ATTRS: {
              // dateTime attribute
              endDate: {
              	value: null,
                  setter: function(value) {
                	  var parsedDateTime = Y.USPDate.parseDateTime(value);
                      if (L.isDate(parsedDateTime)) {
                          return parsedDateTime.getTime();
                      }
                      
                      return value;
                  }, 
                  getter: function(value) {
                	  if (L.isNumber(value)) {
                          return value;
                      }
                      if (L.isDate(value)) {
                          return value.getTime();
                      }
                      if (L.trim(value) === '') {
                          return null;
                      }
                      
                      return value;
                  }
              },
              notes: {
               setter: function(value) {
                return value || 'No notes.';
               }
            }
          }
        });



    /**
     * @class Y.app.childlookedafter.AbsenceFromPlacementEdit
     * @description Edit view needs to load absences types and model data
     */
    Y.namespace('app.childlookedafter').AbsenceFromPlacementEdit = Y.Base.create('absenceFromPlacementEdit', BaseView, [], {
        events: {
          'input[name="childOfferedReturnInterview"]':{
            'change': '_handleChildOfferedReturnInterviewChange'
          }
        },
        template: Y.Handlebars.templates.absenceFromPlacementEditDialog,
        initializer: function() {

            var modelLoad = new Y.Promise(function(resolve, reject) {
                this.get('model').load(function(err, res) {
                    err ? reject(err) : resolve(res);
                });
            }.bind(this));

            var classificationsLoad = new Y.Promise(function(resolve, reject) {
                this.get('classifications').load(function(err, res) {
                    err ? reject(err) : resolve(res);
                });
            }.bind(this));

            Y.Promise.all([classificationsLoad, modelLoad]).then(function(data) {
                this.set('absenceTypes', JSON.parse(data[0].response));
                this._initAbsenceTypes(JSON.parse(data[1].response).absenceType.id);
                this._initReasonMissing(JSON.parse(data[1].response).reasonMissing);
            }.bind(this));

        },
        render: function() {
            Y.app.childlookedafter.AbsenceFromPlacementEdit.superclass.render.call(this);

            this._setChildAcceptedReturnInterviewFields(this.get('model').get('childOfferedReturnInterview'));
            this._initChildOfferedReturnInterview();
            this._initChildAcceptedReturnInterview();
            
            return this;
        },
        _initChildOfferedReturnInterview: function() {
          var node = this.get('container').one('input[name=childOfferedReturnInterview]');
          
          if(node) {
            if(typeof this.get('model').get('childOfferedReturnInterview') !== "undefined"){
              if(this.get('model').get('childOfferedReturnInterview') === true) {
                this.get('container').one('#childOfferedReturnInterviewYes').set('checked', 'checked');
              } else if(this.get('model').get('childOfferedReturnInterview') === false) {
                this.get('container').one('#childOfferedReturnInterviewNo').set('checked', 'checked');
              }
            }
          }
        },
        _initChildAcceptedReturnInterview: function() {
          var node = this.get('container').one('input[name=childAcceptedReturnInterview]');
        
          if(node) {
            if(typeof this.get('model').get('childAcceptedReturnInterview') !== "undefined"){
              if(this.get('model').get('childAcceptedReturnInterview') === true) {
                this.get('container').one('#childAcceptedReturnInterviewYes').set('checked', 'checked');
              } else if(this.get('model').get('childAcceptedReturnInterview') === false) {
                this.get('container').one('#childAcceptedReturnInterviewNo').set('checked', 'checked');
              }
            }
          }
        },
        _handleChildOfferedReturnInterviewChange: function(e) {
          this._setChildAcceptedReturnInterviewFields(e.target.get('value'));
        },
        _setChildAcceptedReturnInterviewFields: function(value) {
          var container = this.get('container');
          
          if(value === 'true' || value === true) {
            container.one('#childAcceptedReturnInterviewWrapper').setStyle('display', 'block');
            container.all('#childAcceptedReturnInterviewWrapper input[type="radio"]').removeAttribute('disabled');
          } else {
            container.one('#childAcceptedReturnInterviewWrapper').setStyle('display', 'none');
            container.all('#childAcceptedReturnInterviewWrapper input[type="radio"]').setAttribute('disabled', 'disabled');
          }
        }
    });



    /**
     * @class Y.app.childlookedafter.AbsenceFromPlacementView
     * @description Simple View
     */
    Y.namespace('app.childlookedafter').AbsenceFromPlacementView = Y.Base.create('absenceFromPlacementView', BaseView, [], {
        template: Y.Handlebars.templates.absenceFromPlacementView,
        render: function() {
            Y.app.childlookedafter.AbsenceFromPlacementView.superclass.render.call(this);
            return this;
        }
    });

    // SOFT DELETE
    Y.namespace('app.childlookedafter').HideAbsenceFromPlacementView = Y.Base.create('hideAbsenceFromPlacementView', Y.View, [], {
        //bind template 
        template: Y.Handlebars.templates["absenceFromPlacementArchive"],
    render: function() {
    	 this.get('container').setHTML(this.template({
             labels: this.get('labels'),
             otherData: this.get('otherData')
                      }));
    	return this;
    }

    });



    /**
     * @class Y.app.childlookedafter.AbsenceFromPlacementDialog
     * @description Main dialog
     */
    Y.namespace('app.childlookedafter').AbsenceFromPlacementDialog = Y.Base.create('absenceFromPlacementDialog', Y.usp.app.AppDialog, [], {
        handleAddAbsenceFromPlacementSave: function(e) {
          return Y.when(this.handleSave(e, {
            periodOfCareId: this.get('activeView').get('otherData.periodOfCareId')
          }));
        },
        handleEditAbsenceFromPlacementSave: function(e) {
          var container = this.get('activeView').get('container'),
          childAcceptedReturnInterviewNode = container.one('input[name=childAcceptedReturnInterview]'),
          childAcceptedReturnInterview = null;
          
          /*
           * value is a Boolean object, therefore, true, false and null are all acceptable values.
           * if disabled, or there is no checked value, set the model attribute as null to ensure it
           * gets updated with null, as opposed to not being updated at all.
           */
          if(!childAcceptedReturnInterviewNode.get('disabled')) {
            if(container.one('input[name=carerSubjectType]:checked')){
              childAcceptedReturnInterview = container.one('input[name=carerSubjectType]:checked').get('value');
            }
          }
          
          return Y.when(this.handleSave(e, {
        	  childAcceptedReturnInterview: childAcceptedReturnInterview
          }, {
            transmogrify: Y.usp.childlookedafter.UpdateAbsenceFromPlacement
          }));
        },
        handleArchiveAbsenceFromPlacementSave: function(e) {
        	var otherData = this.get('activeView').get('otherData'),
        	archiveAbsenceURL = this.get('activeView').get('url').replace('{id}', otherData.id);
        	return Y.when(this.handleSave(e, {}, {
        		periodOfCareId: this.get('activeView').get('otherData.periodOfCareId')
        	}));
          },
        views: {
            add: {
                type: Y.app.childlookedafter.AbsenceFromPlacementAdd,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddAbsenceFromPlacementSave',
                    disabled: true
                }]
            },
            edit: {
                type: Y.app.childlookedafter.AbsenceFromPlacementEdit,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleEditAbsenceFromPlacementSave',
                    disabled: true
                }]
            },
            view: {
                type: Y.app.childlookedafter.AbsenceFromPlacementView
            },
            archive: {
            	type: Y.app.childlookedafter.HideAbsenceFromPlacementView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    isPrimary: true,
                    action: 'handleArchiveAbsenceFromPlacementSave',
                    disabled: false
                }]
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'calendar-popup',
        'handlebars',
        'handlebars-childlookedafter-templates',
        'secured-categories-childlookedafter-component-ReasonMissing',
        'categories-childlookedafter-component-ReasonMissing',
        'model-form-link',
        'usp-childlookedafter-AbsenceFromPlacement',
        'usp-childlookedafter-NewAbsenceFromPlacement',
        'usp-childlookedafter-UpdateAbsenceFromPlacement',
        'usp-classification-Classification'
    ]
});
