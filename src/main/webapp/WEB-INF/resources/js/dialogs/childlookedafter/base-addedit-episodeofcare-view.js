YUI.add('base-addedit-episodeofcare-view', function(Y) {
    Y.namespace('app.childlookedafter').BaseAddEditEpisodeOfCareView = Y.Base.create('baseAddEditEpisodeOfCareView', Y.app.childlookedafter.BaseAddCareView, [], {

        _handleCarerSubjectTypeInputChange: function() {
            var container = this.get('container'),
                subjectType = container.one('input[name="carerSubjectType"]:checked').get('value');

            //sync the view attribute to the input selection
            this.set('carerSubjectType', subjectType);
        },
        _handleCarerSubjectTypeChange: function(e) {
            var container = this.get('container');
            //ensure new value is set
            var radioOption = container.one('input[name="carerSubjectType"][value="' + e.newVal + '"]');
            if (radioOption) {
                radioOption.set('checked', true);
            }
            this.setupFieldVisibility();
        },
        _setUpTypesOfCareForSubject: function(e) {
            //do request for types of care
            if (e.newVal) {
                var typeOfCareURL = this.get('typeOfCareURL').replace('{subjectId}', e.newVal.id);
                Y.io(typeOfCareURL, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    },

                    on: {
                        success: function(trId, response) {
                            try {
                                var data = JSON.parse(response.response);
                                this._setTypesOfCare(data.results);
                            } catch (e) {
                                Y.log('Failed to get types of care', 'debug');
                                return;
                            }
                        }.bind(this)
                    }
                });
            }
        },
        _setTypesOfCare: function(results) {
            var typesOfCare = {};

            Y.Array.forEach(results, function(result) {
                var codedEntry = result.typeOfCareAssignmentVO.codedEntryVO;
                typesOfCare[codedEntry.code] = codedEntry;
            });

            Y.FUtil.setSelectOptions(this.get('container').one('#typeOfCare'), Y.Object.values(typesOfCare), null, null, true, true, 'code');
            var typeOfCare = this.get('model').get('typeOfCare');
            if (typeOfCare) {
                this.get('container').one('#typeOfCare').set('value', typeOfCare);
            }
        },
        _handleSelectedSubjectAddressChange: function(e) {
            var showNoActiveAddressMsg = false;
            if (e.newVal) {
                if (e.newVal.address === null ||
                    (typeof e.newVal.address === 'object' &&
                        e.newVal.address.hasOwnProperty('location') &&
                        e.newVal.address.hasOwnProperty('type') &&
                        e.newVal.address.type !== 'HOME')) {

                    showNoActiveAddressMsg = true;
                }
            }
            this.setFieldVisibility.call(this, '.addressWrapper .pure-info', null, showNoActiveAddressMsg);
        },

        /**
         * Returns an object to indicate the enabled
         * state of the optional fields - NOTE
         * just because a field is enabled does not
         * mean it is visible and editable
         */
        getFieldEnablement: function() {
            var placementType = this.get('placementType'),
                carerSubjectType = this.get('carerSubjectType');

            var enabledFields = {
                carerSubjectType: true,
                typeOfCare: true,
                legalStatus: true,
                placementChangeReason: true,
                placementType: true,
                parent: true,
                placementProvision: true
            };

            switch (carerSubjectType) {
                case 'ORGANISATION': {
                    enabledFields.typeOfCare = false;
                    break;
                }
                case 'PERSON': {
                    enabledFields.typeOfCare = true;
                    break;
                }
            }
            return enabledFields;
        },
        setupFieldEnablement: function() {
            var fieldEnablement = this.getFieldEnablement();
            this.setTypeOfCareEnablement(fieldEnablement.typeOfCare);
            this.setCarerSubjectTypeEnablement(fieldEnablement.carerSubjectType);
            this.setPlacementChangeReasonEnablement(fieldEnablement.placementChangeReason);
            this.setPlacementTypeEnablement(fieldEnablement.placementType);
            this.setParentEnablement(fieldEnablement.parent);
            this.setPlacementProvisionEnablement(fieldEnablement.placementProvision);
            return fieldEnablement;
        },
        setPlacementProvisionEnablement: function(enabled) {
            this.setFieldForEnablement('placementProvision', enabled);
        },
        setParentEnablement: function(enabled) {
            this.setFieldForEnablement('parent', enabled);
        },
        setPlacementTypeEnablement: function(enabled) {
            this.setFieldForEnablement('placementTypeId', enabled);
        },
        setPlacementChangeReasonEnablement: function(enabled) {
            this.setFieldForEnablement('placementChangeReason', enabled);
        },
        setCarerSubjectTypeEnablement: function(enabled) {
            this.setFieldForEnablement('carerSubjectType', enabled);
            if (this.selectionView) {
                this.selectionView.set('disabled', !enabled);
            }
        },
        setTypeOfCareEnablement: function(enabled) {
            this.setFieldForEnablement('typeOfCare', enabled);
        },
        setFieldForEnablement: function(name, enabled) {
            this.setLabelForEnablement(name, enabled);
            this.setInputForEnablement(name, enabled);
        },
        setLabelForEnablement: function(name, enabled) {
            var container = this.get('container');
            var label = container.one('label[for="' + name + '"] .mandatory, legend[for="' + name + '"] .mandatory');
            if (label) {
                label.setStyle('visibility', enabled ? 'visible' : 'hidden');
            }
        },
        setInputForEnablement: function(name, enabled) {
            var container = this.get('container');
            var inputs = container.all('input[name="' + name + '"],select[name="' + name + '"]');
            inputs.each(function(input) {
                if (enabled) {
                    input.removeAttribute('disabled');
                } else {
                    input.setAttribute('disabled', 'disabled');
                }
            });
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'usp-view',
        'base-care-view'
    ]
});