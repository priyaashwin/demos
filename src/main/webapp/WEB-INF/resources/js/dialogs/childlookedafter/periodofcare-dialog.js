YUI.add('periodofcare-dialog', function (Y) {
    'use-strict';
    var O = Y.Object,
        L = Y.Lang,
        MixinOptgroup = Y.app.childlookedafter.MixinClassificationsOptgroup,
        WARNING_TEMPLATE = '<div id="warning-message" class="model-errors"><div class="message message-block" role="alert"><i class="pure-info"></i> Admission date is the same as the previous period end date.</div></div>',
        isEmpty = function (v) { return v === null || v === '' };

    var invalidCategoryOfNeed = ['NOTCIN', 'NOTSTAT'];

    /**
     * A base form for adding periods of care - includes validation for
     * all fields that are common to both the scottish and default 
     * period of care add
     */
    var NewBasePeriodOfCareForm = Y.Base.create('newBasePeriodOfCareForm', Y.usp.childlookedafter.NewPeriodOfCareWithEpisodeOfCare, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#periodOfCareAddForm',
        customValidation: function (attrs) {
            var errors = {},
                labels = this.labels || {};

            if (isEmpty(attrs.startDate)) {
                errors.startDate = labels.startDateMandatory;
            }

            if (isEmpty(attrs.placementTypeId)) {
                errors.placementTypeId = labels.placementTypeMandatory;
            }

            if (isEmpty(attrs.legalStatusId)) {
                errors.legalStatusId = labels.legalStatusMandatory;
            }

            return errors;
        }
    });

    /**
     * A form for adding the standard (non-scottish) period of care.
     * This includes validation additional validation on top of the base set
     * of validation
     */
    Y.namespace('app.childlookedafter').NewPeriodOfCareForm = Y.Base.create('newPeriodOfCareForm', NewBasePeriodOfCareForm, [Y.usp.ModelTransmogrify], {
        customValidation: function (attrs) {
            var errors = {},
                labels = this.labels || {};

            if (isEmpty(attrs.categoryOfNeedId)) {
                errors.categoryOfNeedId = labels.categoryOfNeedMandatory;
            } else if (invalidCategoryOfNeed.includes(this.categoryOfNeedCode)) {
                errors.categoryOfNeedId = labels.categoryOfNeedInvalid;
            }

            if (attrs.previouslyLookedAfterValue === 'true') {
                if (isEmpty(attrs.previousPermanenceOption)) {
                    errors.previousPermanenceOption = labels.previousPermanenceOptionMandatory;
                }

                if (isEmpty(attrs.previousPermanenceLocalAuthorityId) && isEmpty(attrs.previousPermanenceRegion)) {
                    errors.previousPermanenceLocalAuthorityId = labels.previousPermanenceLAOrRegionMandatory;
                    //assign empty string to ensure highlighting of both fields
                    errors.previousPermanenceRegion = '';
                }
            }

            if (this.livingWithParent) {
                if (isEmpty(attrs.carerSubjectId)) {
                    errors.carerSubject = labels.carerSubjectMandatory;
                }
            }

            if (isEmpty(attrs.placementProvision)) {
                errors.placementProvision = labels.placementProvisionMandatory;
            }

            return errors;
        }
    });
    /**
     * A form for adding the scottish period of care.
     * This includes validation additional validation on top of the base set
     * of validation
     */
    Y.namespace('app.childlookedafter').NewScottishPeriodOfCareForm = Y.Base.create('newScottishPeriodOfCareForm', NewBasePeriodOfCareForm, [], {
        customValidation: function (attrs) {
            var errors = {},
                labels = this.labels || {};

            if (isEmpty(attrs.admissionReason)) {
                errors.admissionReason = labels.admissionReasonMandatory;
            }

            return errors;
        }
    });

    /**
     * A form for adding the welsh period of care.
     * This includes validation additional validation on top of the base set
     * of validation
     */
    Y.namespace('app.childlookedafter').NewWelshPeriodOfCareForm = Y.Base.create('newWelshPeriodOfCareForm', NewBasePeriodOfCareForm, [], {
        customValidation: function (attrs) {
            var errors = {},
                labels = this.labels || {};

            if (isEmpty(attrs.categoryOfNeedId)) {
              errors.categoryOfNeedId = labels.categoryOfNeedMandatory;
            } else if (invalidCategoryOfNeed.includes(this.categoryOfNeedCode)) {
              errors.categoryOfNeedId = labels.categoryOfNeedInvalid;
            }

            return errors;
        }
    });
    
    Y.namespace('app.childlookedafter').NewPeriodOfCareWithPlacementAddressForm = Y.Base.create('newPeriodOfCareWithPlacementAddressForm', Y.usp.childlookedafter.NewPeriodOfCareWithEpisodeOfCareAndPlacementAddress, [Y.usp.ModelTransmogrify], {
        customValidation: function (attrs) {
            var errors = {};

            if (isEmpty(attrs.locationId)) {
                errors.locationId = 'Location is mandatory.';
            }

            return errors;
        }
    });

    /**
     * @class app.childlookedafter.BaseNewPeriodOfCareView
     * @description Mixes in Y.app.childlookedafter.MixinClassificationsOptgroup, which provides the method setOptgroupSelect
     */
    Y.namespace('app.childlookedafter').BaseNewPeriodOfCareView = Y.Base.create('NewPeriodOfCareView', Y.app.childlookedafter.BaseAddCareView, [MixinOptgroup], {
        events: {
            'input[name="startDate"]': {
                change: '_startDateChange',
                dateChange: '_startDateChange'
            },
            'input[name="carerSubjectType"]': {
                'change': '_handleCarerSubjectTypeInputChange'
            },
            'select[name="placementTypeId"]': {
                'change': '_handlePlacementTypeSelectChange'
            }
        },
        initializer: function () {
            //load activeCategoryOfNeed as a promise to use as default values
            var activeCategoryOfNeedPromise = new Y.Promise(function (resolve, reject) {
                this.get('activeCategoryOfNeed').load(function (err, res) {
                    (err && err.code !== 404) ? reject(err): resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load categoryOfNeedClassifications as a promise
            var categoryOfNeedPromise = new Y.Promise(function (resolve, reject) {
                this.get('categoryOfNeedClassifications').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load legalStatusClassifications as a promise
            var legalStatusPromise = new Y.Promise(function (resolve, reject) {
                this.get('legalStatusClassifications').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load placementTypeClassifications as a promise
            var placementTypePromise = new Y.Promise(function (resolve, reject) {
                this.get('placementTypeClassifications').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load parental relationships as a promise
            var parentalRelationshipPromise = new Y.Promise(function (resolve, reject) {
                this.get('parentalRelationshipList').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            //load latestPeriodOfCare as a promise
            var latestPeriodOfCarePromise = new Y.Promise(function (resolve, reject) {
                this.get('latestPeriodOfCare').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            this.latestPeriod = null;

            latestPeriodOfCarePromise.then(function (data) {
                this.latestPeriod = data;
            }.bind(this));

           //resolves our promises
            Y.Promise.all([categoryOfNeedPromise, legalStatusPromise, placementTypePromise, activeCategoryOfNeedPromise, parentalRelationshipPromise]).then(function (data) {
                var container = this.get('container'),
                    activeCategoryOfNeed = this.get('activeCategoryOfNeed'),
                    previousCIN = {};

                if (!!activeCategoryOfNeed && !!activeCategoryOfNeed.get('totalSize')) {
                    var activeCIN = this.findActiveCIN(activeCategoryOfNeed, data[0]);
                    if (activeCIN !== 0) {
                        previousCIN = {
                            value: activeCIN
                        };
                    }
                }

                var categoryOfNeedList = [];
                data[0].forEach(function (item) {
                    if (!invalidCategoryOfNeed.includes(item.code)) {
                        categoryOfNeedList.push(item);
                    }
                });

                this.setOptgroupSelect(container.one('#categoryOfNeedId'), categoryOfNeedList, previousCIN);
                this.setOptgroupSelect(container.one('#legalStatus'), data[1]);
                this.setOptgroupSelect(container.one('#placementType'), data[2]);

                
                this._setParentOptions(data[4].results);
                this.set('parentList', data[4].results);

            }.bind(this));

            this._evtHandlers = [
                this.after('*:selectedPersonChange', this._handleSelectedSubjectChange, this),
                this.after('startDateValidChange', this._handleStartDateValidChange, this),
                this.after('carerSubjectTypeChange', this._handleCarerSubjectTypeChange, this),
                this.after('placementTypeChange', this.setupFieldVisibility, this),
                this.after('parentListChange', this.setupFieldVisibility, this)
            ];
        },
        render: function () {
            var container = this.get('container');

            container.setHTML(this.template({
                labels: this.get('labels'),
                showWelshView: this.get('otherData.showWelshView')
            }));

            this.startDateCalendar = new Y.usp.CalendarPopup({
                inputNode: container.one('#startDate')
            }).render();

            this._setTypesOfCare([]);
            
            //setup the initial field visibility
            this.setupFieldVisibility();
            return this;
        },
        getNewSelectionView: function () {
            var _view, controlType = this.get('carerSubjectType'),
                placementType = this.get('placementType');

            var acAttrs = {
                containingNodeSelector: '#periodOfCareAddForm',
                inputName: 'carerSubjectId',
                formName: 'periodOfCareAddForm',
                labels: {
                    placeholder: 'Enter name or ID'
                }
            };

            switch (controlType) {
            case 'ORGANISATION':
                _view = new Y.app.childlookedafter.OrganisationAutocompleteView(Y.merge(acAttrs, {
                    autocompleteURL: this.get('organisationAutocompleteURL'),
                }));
                break;
            case 'PERSON':
                switch (placementType) {
                    case 'ORD_ADOPT_FP':
                    case 'ORD_ADOPT_NOTFP':
                    case 'CONS_ADOPT_FP':
                    case 'CONS_ADOPT_NOTFP':
                    case 'WALES_A8':
                        //prospective adopter(s)
                        _view = new Y.app.childlookedafter.CarerAutocompleteView(Y.merge(acAttrs, {
                            autocompleteURL: L.sub(this.get('adopterChildAutocompleteURL'), {
                                childId: this.get('otherData').subject.subjectId
                            })
                        }));
                        break;
                    default:
                        _view = new Y.app.childlookedafter.CarerAutocompleteView(Y.merge(acAttrs, {
                            autocompleteURL: this.get('fosterCarerAutocompleteURL'),
                            //only enable if the start date is currently valid
                            disabled: !this.get('startDateValid')
                        }));
                }
            }
            return _view;

        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;
            if (this.startDateCalendar) {
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }
        },
        _handleCarerSubjectTypeChange: function (e) {
            var container = this.get('container');
            //ensure new value is set
            var radioOption = container.one('input[name="carerSubjectType"][value="' + e.newVal + '"]');
            if (radioOption) {
                radioOption.set('checked', true);
            }
            this.setupFieldVisibility();
        },
        _handleCarerSubjectTypeInputChange: function () {
            var container = this.get('container'),
                subjectType = container.one('input[name="carerSubjectType"]:checked').get('value');

            //sync the view attribute to the input selection
            this.set('carerSubjectType', subjectType);
        },
        _handleSelectedSubjectChange: function (e) {
            this._setUpTypesOfCare(e);
            this._handleSelectedSubjectAddressChange(e);
        },
        _handleSelectedSubjectAddressChange: function (e) {
            var showNoActiveAddressMsg = false;
            if (e.newVal) {
                if (e.newVal.address === null ||
                    (typeof e.newVal.address === 'object' &&
                        e.newVal.address.hasOwnProperty('location') &&
                        e.newVal.address.hasOwnProperty('type') &&
                        e.newVal.address.type !== 'HOME')) {

                    showNoActiveAddressMsg = true;
                }
            }
            Y.app.childlookedafter.NewPeriodOfCareView.superclass.setFieldVisibility.call(this, '.addressWrapper .pure-info', null, showNoActiveAddressMsg);
        },
        _setUpTypesOfCare: function (e) {
            //do request for types of care
            if (e.newVal) {
                var typeOfCareURL = this.get('typeOfCareURL').replace('{subjectId}', e.newVal.id);
                Y.io(typeOfCareURL, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },

                    on: {
                        success: function (trId, response) {
                            try {
                                var data = JSON.parse(response.response);
                                this._setTypesOfCare(data.results);
                            } catch (e) {
                                Y.log('Failed to get types of care', 'debug');
                                return;
                            }
                        }.bind(this)
                    }
                });
            }
        },
        _handleSelectedLocalAuthorityChange: function (e) {
            if (!e.target.get('selectedOrganisation')) {
                this.get('container').one('#previousPermanenceRegion').removeAttribute('disabled');
            }
        },
        _setParentOptions: function (values) {
            var selectNode = this.get('container').one('#parent');

            var options = Y.Array.map(values, function (value) {
                var name = (value.personAddressVO) ? (value.personVO.name + ' - ' + value.personAddressVO.location.location) : value.personVO.name;
                return {
                    id: value.personVO.id,
                    name: name
                }
            });
            Y.FUtil.setSelectOptions(selectNode, options, null, null, true, true, 'id');
        },
        _setTypesOfCare: function (results) {
            var typesOfCare = {};

            Y.Array.forEach(results, function (result) {
                var codedEntry = result.typeOfCareAssignmentVO.codedEntryVO;
                typesOfCare[codedEntry.code] = codedEntry;
            });

            Y.FUtil.setSelectOptions(this.get('container').one('#typeOfCare'), Y.Object.values(typesOfCare), null, null, true, true, 'code');
        },
        findActiveCIN: function (activeClassifications, categoryOfNeedList) {
            var activeClassificationList = activeClassifications.get('results');
            for (var cin in categoryOfNeedList) {
                for (var classification in activeClassificationList) {
                    if (categoryOfNeedList[cin].id === activeClassificationList[classification].classificationVO.id) {
                        return categoryOfNeedList[cin].id;
                    }
                }
            }
            return 0;
        },
        _startDateChange: function (e) {
            if (Y.one('#warning-message')) {
                Y.one('#warning-message').remove();
            }

            var startDate = null
            if (e.target.get('value')) {
                var parsedStartDate = Y.USPDate.parseDateTime(e.target.get('value'));
                if (null != parsedStartDate) {
                    startDate = parsedStartDate.toDate().setHours(0, 0, 0, 0);
                }
            } else if (e.date) {
                startDate = e.date.setHours(0, 0, 0, 0);
            }

            if (startDate) {
                this.set('startDateValid', true);
            } else {
                this.set('startDateValid', false);
            }

            //clear down auto complete for person selection - as may no longer be valid where
            //the start date is used in the criteria
            if (this.selectionView && this.selectionView.name === 'carerAutocompleteView') {
                this.selectionView.set('selectedPerson', undefined);
            }

            if (this.latestPeriod) {
                var latestEndDate = new Date(this.latestPeriod.endDate).setHours(0, 0, 0, 0);

                if (startDate === latestEndDate) {
                    //Prepend warning
                    Y.one('#periodOfCareAddForm').prepend(WARNING_TEMPLATE);
                }
            }
        },
        _handleStartDateValidChange: function (e) {
            //only constrain the autocomplete if the selection is carerAutocompleteView
            if (this.selectionView && this.selectionView.name === 'carerAutocompleteView') {
                if (e.newVal) {
                    //enable autocomplete
                    this.selectionView.set('disabled', false);
                } else {
                    this.selectionView.set('disabled', true);
                }
            }
        },
        _handlePlacementTypeSelectChange: function (e) {
            var placementType = e.target.get('options')._nodes[e.target.get('selectedIndex')].getAttribute('code');

            //update the placement type attribute - this is what will drive the autocomplete select logic
            this.set('placementType', placementType);
        }
    }, {
        ATTRS: {
            activeCategoryOfNeed: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationAssignmentWithClassificationDetails({
                        url: this.get('activeCategoryOfNeedURL')
                    });
                }
            },
            categoryOfNeedClassifications: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationWithCodePathList({
                        url: this.get('categoryOfNeedsURL')
                    });
                }
            },
            legalStatusClassifications: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationWithCodePathList({
                        url: this.get('legalStatusesURL')
                    });
                }
            },
            placementTypeClassifications: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationWithCodePathList({
                        url: this.get('placementTypesURL')
                    });
                }
            },
            latestPeriodOfCare: {
                valueFn: function () {
                    return new Y.usp.childlookedafter.PeriodOfCareWithStartAndEnd({
                        url: this.get('latestPeriodOfCareURL')
                    });
                }
            },
            parentalRelationshipList: {
                valueFn: function () {
                    return new Y.usp.relationship.RelationshipWithPersonAndHomeAddress({
                        url: this.get('parentalRelationshipURL')
                    });
                }
            },
            /**
             * Keeps track of the start date validity to enable/disable
             * the person auto complete view
             * @attribute startDateValid
             */
            startDateValid: {
                value: false
            },
            /**
             * Track the currently selected placement type
             */
            placementType: {
                value: ''
            },
            
            fosterCarerAutocompleteURL: {},
            organisationAutoCompleteURL: {}
        }
    });

    Y.namespace('app.childlookedafter').NewPeriodOfCareView = Y.Base.create('NewPeriodOfCareView', Y.app.childlookedafter.BaseNewPeriodOfCareView, [], {
        events: {
            'input[name="previouslyLookedAfter"]': {
                'change': '_handlePreviouslyLookedAfterChange'
            },
            '#periodOfCareAddForm_previousPermanenceLocalAuthoritySearch': {
                'change': '_handlePreviousPermanenceValueChange'
            },
            '#previousPermanenceRegion': {
                'change': '_handlePreviousPermanenceValueChange'
            },
            '#location_toggleAddLocation': {
                click: '_showLocationAddDialog'
            }
        },
        template: Y.Handlebars.templates.periodOfCareAddDialog,
        initializer: function () {
            this.events = Y.merge(this.events, Y.app.childlookedafter.NewPeriodOfCareView.superclass.events);

            this.placementAddressView = new Y.app.childlookedafter.PlacementAddressAddView({
                labels: this.get('labels'),
                locationUrl: this.get('locationUrl'),
                locationQueryParamsUrl: this.get('locationQueryParamsUrl')
            }).addTarget(this);

            var autoCompletePlaceholder = 'Enter name or ID';

            this.fuzzyPermDateWidget = new Y.usp.SimpleFuzzyDate({
                id: 'fuzzyPermDate',
                yearInput: true,
                monthInput: true,
                dayInput: true,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: new Date(),
                labels: this.get('fuzzyDateLabels')
            });

            this.localAuthorityAutoCompleteView = new Y.app.OrganisationAutoCompleteResultView({
                autocompleteURL: this.get('localAuthorityAutocompleteURL'),
                inputName: 'previousPermanenceLocalAuthorityId',
                formName: 'periodOfCareAddForm',
                labels: {
                    placeholder: autoCompletePlaceholder
                }
            }).addTarget(this);

            this._newPeriodOfCareEvtHandlers = [
                this.localAuthorityAutoCompleteView.after('selectedOrganisationChange', this._handleSelectedLocalAuthorityChange, this)
            ];
        },
        render: function () {
            Y.app.childlookedafter.NewPeriodOfCareView.superclass.render.call(this);

            this._initPermanencePlacementDate();
            this._setPreviousPermanenceOptions();
            this._setPreviousPermanenceRegionOptions();
            this._setPlacementProvisionOptions();

            var container = this.get('container');
            container.one('#previousPermanenceLocalAuthorityIdWrapper').append(this.localAuthorityAutoCompleteView.render().get('container'));
            container.one('#addAddressWrapper').append(this.placementAddressView.render().get('container'));
        },
        _showLocationAddDialog: function () {
            this.fire('periodOfCare:open:locationView');
        },
        getFieldVisibility: function () {
            //get the default field visibility from the parent class
            var fieldVisibility = Y.app.childlookedafter.NewPeriodOfCareView.superclass.getFieldVisibility.call(this);

            var placementType = this.get('placementType');

            switch (placementType) {
            case 'IND_LIV': {
                fieldVisibility.placementAddressFields = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'RES_EMP': {
                fieldVisibility.subjectAddress = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'PARENT': {
                fieldVisibility.parent = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'ORD_ADOPT_FP':   //falls through
            case 'ORD_ADOPT_NOTFP':   //falls through
            case 'CONS_ADOPT_FP':   //falls through
            case 'CONS_ADOPT_NOTFP': 
            case 'WALES_A8': {
                fieldVisibility.typeOfCare = false;
                break;
            }
            }
            return fieldVisibility;

        },
        destructor: function () {
            this._newPeriodOfCareEvtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._newPeriodOfCareEvtHandlers;

            if (this.fuzzyEventDateWidget) {
                this.fuzzyEventDateWidget.destroy();
                delete this.fuzzyEventDateWidget;
            }

            if (this.localAuthorityAutoCompleteView) {
                this.localAuthorityAutoCompleteView.removeTarget(this);
                this.localAuthorityAutoCompleteView.destroy();
                delete this.localAuthorityAutoCompleteView;
            }
        },
        _initPermanencePlacementDate: function () {
            this.fuzzyPermDateWidget.set('contentNode', this.get('container').one('#permanencePlacementDate-input'));
            this.fuzzyPermDateWidget.render();
        },
        _setPreviousPermanenceOptions: function () {
            var selectNode = this.get('container').one('#previousPermanenceOption'),
                options = O.values(Y.uspCategory.childlookedafter.previousPermanenceOption.category.getActiveCodedEntries());

            Y.FUtil.setSelectOptions(selectNode, options, null, null, true, true, 'code');
        },
        _setPreviousPermanenceRegionOptions: function () {
            var selectNode = this.get('container').one('#previousPermanenceRegion'),
                options = O.values(Y.uspCategory.childlookedafter.previousPermanenceRegion.category.getActiveCodedEntries());

            Y.FUtil.setSelectOptions(selectNode, options, null, null, true, true, 'code');
        },
        _setPlacementProvisionOptions: function () {
            var selectNode = this.get('container').one('#placementProvision'),
                options = O.values(Y.uspCategory.childlookedafter.placementProvision.category.getActiveCodedEntries());

            Y.FUtil.setSelectOptions(selectNode, options, null, null, true, true, 'code');
        },
        _resetPermanenceFields: function () {
            this.localAuthorityAutoCompleteView.set('selectedOrganisation', null);
            this.get('container').one('#previousPermanenceOption').set('value', '');
            this.get('container').one('#periodOfCareAddForm_previousPermanenceLocalAuthorityId').removeAttribute('disabled');
            this.get('container').one('#previousPermanenceRegion').set('value', '');
            this.get('container').one('#previousPermanenceRegion').removeAttribute('disabled');
            this.fuzzyPermDateWidget.reset();
            this.get('container').one('#fuzzyPermDateYEAR').set('value', '');
            this.get('container').one('#fuzzyPermDateMONTH').set('value', '');
            this.get('container').one('#fuzzyPermDateDAY').set('value', '');
        },
        _handlePreviouslyLookedAfterChange: function (e) {
            var permanenceOrderFields = this.get('container').one('#permanenceOrderFields');

            if (e.target.get('value') === 'true') {
                permanenceOrderFields.setStyle('display', 'block');
            } else {
                //reset and hide permanence fields
                this._resetPermanenceFields();
                permanenceOrderFields.setStyle('display', 'none');
            }
        },
        _handlePreviousPermanenceValueChange: function (e) {
            var node = e.target.get('id');

            if (node === 'periodOfCareAddForm_previousPermanenceLocalAuthorityId') {
                this.get('container').one('#previousPermanenceRegion').setAttribute('disabled', 'disabled');
            }

            if (node === 'previousPermanenceRegion') {
                this.get('container').one('#periodOfCareAddForm_previousPermanenceLocalAuthorityId').setAttribute('disabled', 'disabled');
            }
        },
        handleSetNewLocation: function (locationId) {
            this.placementAddressView.handleSetNewLocation(locationId);
        }
    });

    Y.namespace('app.childlookedafter').NewScottishNewPeriodOfCareView = Y.Base.create('newScottishNewPeriodOfCareView', Y.app.childlookedafter.BaseNewPeriodOfCareView, [MixinOptgroup], {
        template: Y.Handlebars.templates.scottishPeriodOfCareAddDialog,
        /**
         * Override the base class implementation as the type of selection view is based
         * on placementType and carerSubjectType
         * @param {string} controlType type of selection Person or Organisation
         */
        getNewSelectionView: function () {
            var _view,
                placementType = this.get('placementType'),
                subjectType = this.get('carerSubjectType');

            if ('ORGANISATION' === subjectType) {
                //great - this is the same as base class
                return Y.app.childlookedafter.NewScottishNewPeriodOfCareView.superclass.getNewSelectionView.call(this);
            } else if ('PERSON' === subjectType) {
                var acAttrs = {
                    containingNodeSelector: '#periodOfCareAddForm',
                    inputName: 'carerSubjectId',
                    formName: 'periodOfCareAddForm',
                    labels: {
                        placeholder: 'Enter name or ID'
                    }
                };

                switch (placementType) {
                case 'SCT_FRIEND_REL':
                    //default person autocomplete - NOT RESTRICTED TO CARER
                    _view = new Y.app.childlookedafter.PersonAutocompleteView(
                        Y.merge(acAttrs, {
                            autocompleteURL: this.get('personAutocompleteURL')
                        })
                    );
                    break;
                case 'SCT_PROSPECT_ADOPT':
                    //prospective foster carer AC
                    _view = new Y.app.childlookedafter.CarerAutocompleteView(
                        Y.merge(acAttrs, {
                        	autocompleteURL: L.sub(this.get('adopterAutocompleteURL'), {
                        		childId: this.get('otherData').subject.subjectId
                        	})
                        })
                    );
                    break;
                default:
                    //just call into the base class
                    return Y.app.childlookedafter.NewScottishNewPeriodOfCareView.superclass.getNewSelectionView.call(this);
                }
            }
            return _view;
        },
        getFieldVisibility: function () {
            //get the default field visibility from the parent class
            var fieldVisibility = Y.app.childlookedafter.NewPeriodOfCareView.superclass.getFieldVisibility.call(this);

            var placementType = this.get('placementType');

            switch (placementType) {
            case 'SCT_FRIEND_REL': {
                fieldVisibility.typeOfCare = false;
                fieldVisibility.subjectAddress = true;
                break;
            }
            case 'SCT_PARENT': {
                fieldVisibility.typeOfCare = false;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.parent = true;

                break;
            }
            case 'SCT_PROSPECT_ADOPT': {
                //hide type of care fields
                fieldVisibility.typeOfCare = false;
                break;
            }
            }

            return fieldVisibility;
        },

        setupFieldVisibility: function () {
            var placementType = this.get('placementType'),
                lockCarerSubjectType = false;

            //control setting the subject type based on the placement Type
            switch (placementType) {
            case 'SCT_OTHER_COMM':
                //falls through
            case 'SCT_LA_HOME':
                //falls through
            case 'SCT_VOL_HOME':
                //falls through
            case 'SCT_RES_SCHOOL':
                //falls through
            case 'SCT_CRISIS':
                //falls through
            case 'SCT_SECURE_ACCOM':
                //falls through
            case 'SCT_OTHER_RES':
                //set the carer subject type and disable
                this.set('carerSubjectType', 'ORGANISATION');
                lockCarerSubjectType = true;
                break;
            case 'SCT_PROSPECT_ADOPT':
                //falls through
            case 'SCT_FRIEND_REL':
                //set the carer subject type and disable
                this.set('carerSubjectType', 'PERSON');
                lockCarerSubjectType = true;
                break;
            }

            //setup field visibility
            var fieldVisibility = Y.app.childlookedafter.NewPeriodOfCareView.superclass.setupFieldVisibility.call(this);
            //if the carerSubectType field is visible
            if (fieldVisibility.carerSubjectType) {
                //update the lock status of the field
                this.lockCarerSubjectType(lockCarerSubjectType);
            }
        },
        render: function () {
            Y.app.childlookedafter.NewScottishNewPeriodOfCareView.superclass.render.call(this);
            this._setAdmissionReasonOptions();
        },
        _setAdmissionReasonOptions: function () {
            var selectNode = this.get('container').one('#admissionReason'),
                options = O.values(Y.uspCategory.childlookedafter.careAdmissionReason.category.getActiveCodedEntries());

            Y.FUtil.setSelectOptions(selectNode, options, null, null, true, true, 'code');
        }
    }, {
        ATTRS: {
            adopterAutocompleteURL: {},
            personAutocompleteURL: {}
        }
    });

    Y.namespace('app.childlookedafter').NewWelshPeriodOfCareView = Y.Base.create('newWelshPeriodOfCareView', Y.app.childlookedafter.BaseNewPeriodOfCareView, [MixinOptgroup], {
    	 events: {
             '#location_toggleAddLocation': {
                 click: '_showLocationAddDialog'
             }
         },    	
        template: Y.Handlebars.templates.periodOfCareAddDialog,
        
        initializer: function () {
            this.events = Y.merge(this.events, Y.app.childlookedafter.NewWelshPeriodOfCareView.superclass.events);

            this.placementAddressView = new Y.app.childlookedafter.PlacementAddressAddView({
                labels: this.get('labels'),
                locationUrl: this.get('locationUrl'),
                locationQueryParamsUrl: this.get('locationQueryParamsUrl')
            }).addTarget(this);
        },
        
        getFieldVisibility: function () {
            //get the default field visibility from the parent class
            var fieldVisibility = Y.app.childlookedafter.NewWelshPeriodOfCareView.superclass.getFieldVisibility.call(this);

            fieldVisibility = {
            		carerSubjectType: true,
                    typeOfCare: true,
                    parent: false,
                    subjectAddress: false,
                    placementAddressFields: false,
                    personPlacementAddressField: false,
                    permanenceOrderFields: false,
                    previouslyLookedAfter:false,
                    providerTypeWrapper:false,
                    siblingGroupWrapper:false,
                    siblingsAssessedGroupWrapper:false                    
             };
            
            var subjectType = this.get('carerSubjectType'),
                placementType = this.get('placementType');
        
            if ('ORGANISATION' === subjectType) {
             fieldVisibility.typeOfCare = false;
            }

            switch (placementType) {
            case 'IND_LIV': {
                fieldVisibility.placementAddressFields = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'RES_EMP': {
                fieldVisibility.subjectAddress = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'PARENT': {
                fieldVisibility.parent = true;
                fieldVisibility.carerSubjectType = false;
                fieldVisibility.typeOfCare = false;
                break;
            }
            case 'WALES_A8': {
                fieldVisibility.typeOfCare = false;
                break;
            }
            }
            return fieldVisibility;
        },

        setupFieldVisibility: function () {
            //setup field visibility
            var fieldVisibility = Y.app.childlookedafter.NewWelshPeriodOfCareView.superclass.setupFieldVisibility.call(this);            
            this.setFieldVisibility('#previouslyLookedAfter', null, fieldVisibility.previouslyLookedAfter);
            this.setFieldVisibility('#permanenceOrderFields', null, fieldVisibility.permanenceOrderFields);
            this.setFieldVisibility('#providerTypeWrapper', null, fieldVisibility.placementProvision);
            this.setFieldVisibility('#siblingGroupWrapper', null, fieldVisibility.siblingGroup);
            this.setFieldVisibility('#siblingsAssessedGroupWrapper', null, fieldVisibility.siblingsAssessedGroup);
        },
        _showLocationAddDialog: function () {
            this.fire('periodOfCare:open:locationView');
        },
        render: function () {
            Y.app.childlookedafter.NewWelshPeriodOfCareView.superclass.render.call(this);
            var container = this.get('container');
            container.one('#addAddressWrapper').append(this.placementAddressView.render().get('container'));            
        }
    });
    
    Y.namespace('app.childlookedafter').EndPeriodOfCareForm = Y.Base.create('endPeriodOfCareForm', Y.usp.childlookedafter.UpdateEndPeriodOfCare, [Y.usp.ModelFormLink], {
        form: '#periodOfCareEndForm'
    });

    Y.namespace('app.childlookedafter').EndPeriodOfCareView = Y.Base.create('EndPeriodOfCareView', Y.usp.childlookedafter.UpdateEndPeriodOfCareView, [], {
        template: Y.Handlebars.templates.periodOfCareEndDialog,
        initializer: function () {

            /*
            !!! THIS IS A RACE CONDITION - there is code in here that expects
            this to be loaded, but no safe guards are in place. This loading
            needs to occur in the controller before this view is rendered then
            it is guaranteed the data will be present

            TODO - RG TO FIX
            */

            //load periodOfCare as a promise
            this.get('latestPlacement').load();
            //load person details
            this.get('personDetails').load();
        },
        render: function () {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels')
                });

            container.setHTML(html);

            this.endDateCalendar = new Y.usp.CalendarPopup({
                inputNode: this.get('container').one('#endDate')
            });
            this.endDateCalendar.render();

            Y.FUtil.setSelectOptions(container.one('#endReason'), O.values(Y.secured.uspCategory.childlookedafter.episodeEndReason.category.getActiveCodedEntries(null, 'write')), null, null, true, true, 'code');

            this._evtHandlers = [
                this.get('container').delegate('change', this._handleEndReasonChange, '#endReason', this),
                this.get('container').delegate('change', this._handleEndDateChange, '#endDate', this)
            ];

            return this;
        },
        _validateWarnings: function (endReason, endDate, dob) {

            var container = this.get('container'),
                placementType = this.get('latestPlacement').get('placementType').code;

            if (endReason === 'E11' || endReason === 'E12') {

                if (placementType !== 'CONS_ADOPT_FP' && placementType !== 'CONS_ADOPT_NOTFP' && placementType !== 'ORD_ADOPT_FP' && placementType !== 'ORD_ADOPT_NOTFP') {
                    container.one('#adoptionWarning').show();
                } else {
                    container.one('#adoptionWarning').hide();
                }

                if (endDate) {
                    var endDateDate = new Date(endDate);
                    var _MS_PER_YEAR = 1000 * 60 * 60 * 24 * 365;
                    var utc1 = Date.UTC(dob.getFullYear(), dob.getMonth(), dob.getDate());
                    var utc2 = Date.UTC(endDateDate.getFullYear(), endDateDate.getMonth(), endDateDate.getDate());
                    var age = Math.floor((utc2 - utc1) / _MS_PER_YEAR);

                    if (age >= 18) {
                        container.one('#ageWarning').show();
                    } else {
                        container.one('#ageWarning').hide();
                    }
                } else {
                    container.one('#ageWarning').hide();
                }

            } else {
                container.one('#adoptionWarning').hide();
                container.one('#ageWarning').hide();
            }
        },
        _handleEndReasonChange: function (e) {
            var container = this.get('container'),
                dob = new Date(this.get('personDetails').get('dateOfBirth').calculatedDate),
                endDate = container.one('#endDate').get('value'),
                endReason = e.target.get('value');

            this._validateWarnings(endReason, endDate, dob);

        },
        /*
         * !!! This event handler won't fire for calendar events
         * 
         * TODO - RG TO FIX
         */
        _handleEndDateChange: function () {
            var container = this.get('container'),
                dob = new Date(this.get('personDetails').get('dateOfBirth').calculatedDate),
                endDate = container.one('#endDate').get('value'),
                endReason = container.one('#endReason').get('value');

            this._validateWarnings(endReason, endDate, dob);
        },
        destructor: function () {

            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            if (this.endDateCalendar) {
                // destroy the calendar
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
        }
    }, {
        ATTRS: {
            latestPlacement: {
                valueFn: function () {
                    return new Y.usp.childlookedafter.Placement({
                        url: L.sub(this.get('latestPlacementURL'), {
                            id: this.get('otherData').periodOfCareId
                        })
                    });
                }
            },
            personDetails: {
                valueFn: function () {
                    return new Y.usp.person.Person({
                        url: this.get('getPersonURL')
                    });
                }
            }

        }
    });

    Y.namespace('app.childlookedafter').DeletePeriodOfCareView = Y.Base.create('deletePeriodOfCareView', Y.usp.View, [], {
        template: Y.Handlebars.templates.periodOfCareDeleteDialog
    });

    Y.namespace('app.childlookedafter').DeleteInProgressView = Y.Base.create('deleteInProgressView', Y.usp.View, [], {
        template: Y.Handlebars.templates.periodOfCareDeleteInProgress
    });

    Y.namespace('app.childlookedafter').PeriodOfCareDialog = Y.Base.create('periodOfCareDialog', Y.usp.app.AppDialog, [Y.usp.ModelTransmogrify], {
        savePeriodOfCare: function (e, data, transmogrify) {
            this.handleSave(e, Y.merge({
                //potential hidden fields are reset to undefined here
                //they will be re-populated by model-form-link where they
                //are still valid. This fixes the issue of stale data if
                //the user changes the options after a save attempt had
                //failed with validation errors
                typeOfCare: undefined,
                carerSubjectType: undefined,
                carerSubjectId: undefined
            }, data), {
                transmogrify: transmogrify
            });
        },
        handleAddPeriodOfCareSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                permanencePlacementDate = view.fuzzyPermDateWidget.getFuzzyDate(),
                carerSubjectType = undefined,
                carerSubjectId = undefined,
                selectedPlacementTypeCode = container.one('#placementType').get('options')._nodes[container.one('#placementType').get('selectedIndex')].attributes.code,
                selectedCategoryOfNeedCode = container.one('#categoryOfNeedId').get('options')._nodes[container.one('#categoryOfNeedId').get('selectedIndex')].attributes.code,
                categoryOfNeedCode,
                independentLiving = (selectedPlacementTypeCode && (selectedPlacementTypeCode.value === 'RES_EMP' || selectedPlacementTypeCode.value === 'IND_LIV')) ? true : false,
                livingWithParent = (selectedPlacementTypeCode && selectedPlacementTypeCode.value === 'PARENT') ? true : false;              
            
            if (selectedCategoryOfNeedCode) {
                categoryOfNeedCode = selectedCategoryOfNeedCode.value
            }

            if (independentLiving) {
                carerSubjectType = null;
                carerSubjectId = null;
            } else if (livingWithParent) {
                carerSubjectType = 'PERSON';
                carerSubjectId = container.one('#parent').get('value');
            }

            var values = {
                personId: view.get('otherData.subject.subjectId'),
                carerSubjectType: carerSubjectType,
                carerSubjectId: carerSubjectId,
                permanencePlacementDate: permanencePlacementDate                
            };

            //Set non model attributes as a property of the model to be used in customValidation
            view.get('model').livingWithParent = livingWithParent;
            view.get('model').categoryOfNeedCode = categoryOfNeedCode;

            var transmogrify;
            if (!L.isUndefined(selectedPlacementTypeCode) && selectedPlacementTypeCode.value === 'IND_LIV') {
                var roomDescription = container.one('#roomDescription-input').get('value'),
                    floorDescription = container.one('#floorDescription-input').get('value'),
                    locationId = view.placementAddressView.get('selectedLocation');

                var addressValues = {
                    locationId: locationId,
                    addressType: 'PLACEMENT',
                    addressUsage: 'PERMANENT',
                    roomDescription: roomDescription,
                    floorDescription: floorDescription
                };

                values = Y.merge(values, addressValues);
                transmogrify = Y.app.childlookedafter.NewPeriodOfCareWithPlacementAddressForm;
            }

            this.savePeriodOfCare(e, values, transmogrify);
        },
        handleScottishAddPeriodOfCareSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                carerSubjectType = undefined,
                carerSubjectId = undefined,
                selectedPlacementTypeCode = container.one('#placementType').get('options')._nodes[container.one('#placementType').get('selectedIndex')].attributes.code,
                independentLiving = (selectedPlacementTypeCode && (selectedPlacementTypeCode.value === 'SCT_FRIEND_REL' || selectedPlacementTypeCode.value === 'IND_LIV')) ? true : false,
                livingWithParent = (selectedPlacementTypeCode && selectedPlacementTypeCode.value === 'SCT_PARENT') ? true : false;

            if (independentLiving) {
                carerSubjectType = null;
                carerSubjectId = null;
            } else if (livingWithParent) {
                carerSubjectType = 'PERSON';
                carerSubjectId = container.one('#parent').get('value');
            }

            //Set non model attributes as a property of the model to be used in customValidation
            view.get('model').livingWithParent = livingWithParent;

            this.savePeriodOfCare(e, {
                personId: view.get('otherData.subject.subjectId'),
                carerSubjectType: carerSubjectType,
                carerSubjectId: carerSubjectId
            });
        },
        handleWelshAddPeriodOfCareSave: function (e) {
            var view = this.get('activeView'),
                container = view.get('container'),
                carerSubjectType = undefined,
                carerSubjectId = undefined,
                selectedPlacementTypeCode = container.one('#placementType').get('options')._nodes[container.one('#placementType').get('selectedIndex')].attributes.code,
                selectedCategoryOfNeedCode = container.one('#categoryOfNeedId').get('options')._nodes[container.one('#categoryOfNeedId').get('selectedIndex')].attributes.code,
                categoryOfNeedCode,
                independentLiving = (selectedPlacementTypeCode && (selectedPlacementTypeCode.value === 'RES_EMP' || selectedPlacementTypeCode.value === 'IND_LIV')) ? true : false,
                livingWithParent = (selectedPlacementTypeCode && selectedPlacementTypeCode.value === 'PARENT') ? true : false;
                showWelshView = view.get('otherData.showWelshView');
            
            if (selectedCategoryOfNeedCode) {
                categoryOfNeedCode = selectedCategoryOfNeedCode.value
            }

            if (independentLiving) {
                carerSubjectType = null;
                carerSubjectId = null;
            } else if (livingWithParent) {
                carerSubjectType = 'PERSON';
                carerSubjectId = container.one('#parent').get('value');
            }

            var values = {
                personId: view.get('otherData.subject.subjectId'),
                carerSubjectType: carerSubjectType,
                carerSubjectId: carerSubjectId,                            
            };

            //Set non model attributes as a property of the model to be used in customValidation
            view.get('model').livingWithParent = livingWithParent;
            view.get('model').categoryOfNeedCode = categoryOfNeedCode;
            view.get('model').showWelshView = showWelshView;

            var transmogrify;
            if (!L.isUndefined(selectedPlacementTypeCode) && selectedPlacementTypeCode.value === 'IND_LIV') {
                var roomDescription = container.one('#roomDescription-input').get('value'),
                    floorDescription = container.one('#floorDescription-input').get('value'),
                    locationId = view.placementAddressView.get('selectedLocation');

                var addressValues = {
                    locationId: locationId,
                    addressType: 'PLACEMENT',
                    addressUsage: 'PERMANENT',
                    roomDescription: roomDescription,
                    floorDescription: floorDescription
                };

                values = Y.merge(values, addressValues);
                transmogrify = Y.app.childlookedafter.NewPeriodOfCareWithPlacementAddressForm;
            }

            this.savePeriodOfCare(e, values, transmogrify);
        },        
        handleEndPeriodOfCareSave: function (e) {
            var view = this.get('activeView');
            return Y.when(this.handleSave(e, {
                id: view.get('otherData').periodOfCareId
            }));
        },

        getPeriodDeletionRequest: function (url, periodOfCareId) {
            return new Y.Promise(function (resolve, reject) {
                var model = new Y.usp.deletion.DeletionRequestList({
                    url: Y.Lang.sub(url, {
                        periodOfCareId: periodOfCareId
                    })
                });

                model.load(function (err, res) {
                    (err) ? reject(err): resolve(JSON.parse(res.response));
                }.bind(this));
            });
        },

        findDeletionRequest: function (url, periodOfCareId) {
            var deletionRequestId;

            this.getPeriodDeletionRequest(url, periodOfCareId).then(function (data) {
                var deletionRequest = data.find(function (r) {
                    return r.deletionState === 'READY_TO_DELETE';
                });

                if(this.timerId){
                    clearTimeout(this.timerId);
                }

                if (deletionRequest) {
                    deletionRequestId = deletionRequest.id;
                    this.get('activeView').fire('periodOfCare:deletePeriod', {
                        periodOfCareId: periodOfCareId,
                        deletionRequestId: deletionRequestId
                    });
                } else {
                    //TODO - what if it failed?
                    //should there be a maximum poll count

                    //to get out of this situation - the user can hit the cancel button

                    //poll
                    this.timerId = setTimeout(this.findDeletionRequest.bind(this), 10000, url, periodOfCareId);
                }
            }.bind(this));
        },

        handlePrepareDeletePeriodOfCare: function () {
            var view = this.get('activeView'),
                urls = view.get('otherData').urlConfig,
                model = view.get('model'),
                periodOfCareId = view.get('otherData.periodOfCareId'),
                getUrl = L.sub(urls.url, {
                    periodOfCareId: periodOfCareId
                });

            //bind an error handler on the model
            model.on('error', function (e) {
                var error = e.error || {};
                //timeout error code - start to poll for deletion request
                if (error.code === 504 || error.code === 0) {
                    //stop error bubble
                    e.halt(true);

                    this.findDeletionRequest(getUrl, periodOfCareId);
                }
                //just let the event bubble as normal
            }, this),

            this.showDialogMask();

            model.save({}, Y.bind(function (err) {
                if (err === null) {
                    this.hideDialogMask();
                    //model has id of newly created deletion request
                    view.fire('periodOfCare:deletePeriod', {
                        periodOfCareId: periodOfCareId,
                        deletionRequestId: model.get('id')
                    });
                }
            }, this));
        },        
        destructor: function () {
            if (this.timerId) {
                //stop polling timer
                clearTimeout(this.timerId);
                delete this.timerId;
            }
        },
        views: {
            add: {
                type: Y.app.childlookedafter.NewPeriodOfCareView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddPeriodOfCareSave',
                    disabled: true
                }]
            },
            addWelsh: {
                type: Y.app.childlookedafter.NewWelshPeriodOfCareView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleWelshAddPeriodOfCareSave',
                    disabled: true
                }]
            },
            addScottish: {
                type: Y.app.childlookedafter.NewScottishNewPeriodOfCareView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleScottishAddPeriodOfCareSave',
                    disabled: true
                }]
            },
            end: {
                type: Y.app.childlookedafter.EndPeriodOfCareView,
                buttons: [{
                    name: 'endButton',
                    labelHTML: '<i class="fa fa-check"></i> End Period',
                    action: 'handleEndPeriodOfCareSave',
                    disabled: true
                }]
            },
            prepareDeletePeriod: {
                type: Y.app.childlookedafter.DeletePeriodOfCareView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    action: 'handlePrepareDeletePeriodOfCare',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            deletePeriod: {
                type: Y.app.childlookedafter.DeletePeriodOfCareView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            deleteInProgress: {
                type: Y.app.childlookedafter.DeleteInProgressView                
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'model-form-link',
        'childlookedafter-mixin-classifications-optgroup',
        'childlookedafter-autocomplete-view',
        'usp-childlookedafter-NewPeriodOfCareWithEpisodeOfCare',
        'usp-childlookedafter-NewPeriodOfCareWithEpisodeOfCareAndPlacementAddress',
        'usp-childlookedafter-NewEpisodeOfCare',
        'usp-childlookedafter-NewPlacement',
        'usp-childlookedafter-Placement',
        'usp-childlookedafter-PeriodOfCare',
        'usp-childlookedafter-UpdateEndPeriodOfCare',
        'usp-childlookedafter-PeriodOfCareWithStartAndEnd',
        'usp-classification-ClassificationWithCodePath',
        'usp-classification-ClassificationAssignmentWithClassificationDetails',
        'usp-classification-ClassificationAssignment',
        'usp-relationship-RelationshipWithPersonAndHomeAddress',
        'usp-deletion-DeletionRequest',
        'usp-person-Person',
        'secured-categories-childlookedafter-component-EpisodeEndReason',
        'categories-childlookedafter-component-EpisodeEndReason',
        'categories-childlookedafter-component-PlacementProvision',
        'categories-childlookedafter-component-PreviousPermanenceOption',
        'categories-childlookedafter-component-PreviousPermanenceRegion',
        'categories-childlookedafter-component-CareAdmissionReason',
        'entry-dialog-views',
        'handlebars',
        'handlebars-childlookedafter-templates',
        'calendar-popup',
        'person-autocomplete-view',
        'organisation-autocomplete-view',
        'childlookedafter-subject-autocompleteview',
        'base-care-view',
        'placement-address-add-view',
        'model-transmogrify',
        'simple-fuzzy-date'
    ]
});