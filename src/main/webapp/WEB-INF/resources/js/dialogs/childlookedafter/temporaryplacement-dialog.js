YUI.add('temporaryplacement-dialog', function (Y) {
    'use-strict';

    var isEmpty = function (v) { return v === null || v === '' },
        L = Y.Lang,
        MixinOptgroup = Y.app.childlookedafter.MixinClassificationsOptgroup,
        DB = Y.usp.DataBindingUtil;

    Y.namespace('app.childlookedafter').NewTemporaryPlacementForm = Y.Base.create('newTemporaryPlacementForm', Y.usp.childlookedafter.NewPlacement, [Y.usp.ModelFormLink], {
        form: '#temporaryPlacementAddForm',
        customValidation: function (attrs) {
            var errors = {},
                labels = this.labels || {},
                validPlacementTypeCodesForMandatoryCarer = ['TMP_ACC_OTH', 'TMP_FP_HOL', 'TMP_HOSP', 'TMP_HOL'];

            if (isEmpty(attrs.startDate)) {
                errors.startDate = labels.startDateMandatory;
            }

            if (isEmpty(attrs.placementTypeId)) {
                errors.placementTypeId = labels.placementTypeMandatory;
            }

            if (isEmpty(attrs.placementProvision)) {
                errors.placementProvision = labels.placementProvisionMandatory;
            }

            if (validPlacementTypeCodesForMandatoryCarer.includes(this.placementTypeCode) && isEmpty(attrs.carerSubjectId)) {
                errors.CarerSubjectSearch = labels.carerMandatory;
            }

            return errors;
        }
    });

    Y.namespace('app.childlookedafter').UpdateTemporaryPlacementForm = Y.Base.create('updateTemporaryPlacementForm', Y.usp.childlookedafter.Placement, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#temporaryPlacementEditForm'
    });

    // TO DO: this is the same as the view in episodeofcare-dialog.js and should probably be moved to a common file
    var TempPlacementWarningsView = Y.Base.create('TempPlacementWarnings', Y.View, [], {
        template: Y.Handlebars.templates.tempPlacementWarnings,

        initializer: function () {
            this.after('warningsChange', this.render, this);
        },

        render: function () {
            var container = this.get('container');
            var warnings = this.get('warnings');

            container.setHTML(this.template({
                warnings: warnings,
                labels: {
                    title: 'Warnings'
                }
            }));

            return this;
        }
    }, {
        ATTRS: {
            warnings: { value: [] }
        }
    });

    var BaseTemporaryPlacementView = Y.Base.create('NewTemporaryPlacementView', Y.View, [], {
        _handleDateChange: function () {
            this._checkHolidayWarnings();
        },
        _checkHolidayWarnings: function () {
            var warnings = [];
            var labels = this.get('labels');
            var startDate = this._getParsedStartDate();
            var endDate = this._getParsedEndDate();
            var placementType = this._getPlacementType();
            // if TMP_FP_HOL selected
            if (placementType == 'TMP_FP_HOL') {
                // if both dates are valid
                if (null != startDate && null != endDate) {
                    Y.io(this.get('fosterCarerHolidaysURL'), {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                        },

                        on: {
                            success: function (trId, response) {
                                try {
                                    response = JSON.parse(response.response);
                                    // Check if placements exceed 2 or days exceed 21 in any of the VOs
                                    for (var i = 0; i < response.fosterCarerHolidays.length; ++i) {
                                        if (response.fosterCarerHolidays[i].totalDays > 21 || response.fosterCarerHolidays[i].totalPlacements > 2) {
                                            var placementText = response.fosterCarerHolidays[i].totalPlacements == 1 ? ' placement of this type totalling ' : ' placements of this type totalling ';
                                            var dayText = response.fosterCarerHolidays[i].totalDays == 1 ? ' day for reporting period ' : ' days for reporting period ';
                                            var warningInfo = 'Adding this placement will result in ' + response.fosterCarerHolidays[i].totalPlacements + placementText + response.fosterCarerHolidays[i].totalDays + dayText + Y.USPDate.format(response.fosterCarerHolidays[i].yearStart) + '-' + Y.USPDate.format(response.fosterCarerHolidays[i].yearEnd);
                                            warnings.push(warningInfo);
                                        }
                                    }
                                    this.warningsView.set('warnings', warnings);

                                } catch (e) {
                                    Y.log('Failed to get foster carer holidays', 'debug');
                                    return;
                                }

                            }.bind(this),
                            failure: function (trId, response) {
                                this._setOverlappingWarning(labels, warnings, response);
                                this.warningsView.set('warnings', warnings);
                            }.bind(this)
                        }
                    });
                }
            } else if (placementType == 'TMP_HOSP' || placementType == 'TMP_ACC_OTH') {
                if (null != startDate && null != endDate) {
                    var diffDays = Y.USPDate.parse(endDate).get('value').diff(Y.USPDate.parse(startDate).get('value'), 'days');
                    if (placementType == 'TMP_HOSP' && diffDays > 42) {
                        warnings.push(labels.hospitalDuration);
                    }
                    if (placementType == 'TMP_ACC_OTH' && diffDays > 7) {
                        warnings.push(labels.otherDuration);
                    }

                }
            }
            this.warningsView.set('warnings', warnings);

        },
        _setOverlappingWarning: function (labels, warnings, response) {
            if (Y.JSON.parse(response.responseText).type == 'TemporaryPlacementOverlappingException') {
                // Display a warning in the case of overlap
                // NB: will only display for TMP_FP_HOL placement
                warnings.push(labels.overlapping)
            }
            return warnings;
        },
        _getFosterCarerHolidaysURL: function (val) {
            return Y.Lang.sub(val, {
                placementStartDate: this._getStartDateEpoch(),
                placementEndDate: this._getEndDateEpoch(),
                episodeOfCareId: this._getEpisodeOfCareId()
            });
        },
        _getParsedStartDate: function () {
            return this.startDateCalendar ? this.startDateCalendar.parseInputDate() : null;
        },
        _getParsedEndDate: function () {
            return this.endDateCalendar ? this.endDateCalendar.parseInputDate() : null;
        },
        _getStartDateEpoch: function () {
            var parsedStartDate = this._getParsedStartDate();
            return parsedStartDate ? Y.USPDate.parse(parsedStartDate).get('value').getTime() : null;
        },
        _getEndDateEpoch: function () {
            var parsedEndDate = this._getParsedEndDate();
            return parsedEndDate ? Y.USPDate.parse(parsedEndDate).get('value').getTime() : null;
        },
        _getEpisodeOfCareId: function () {
            return this.get('model').get('episodeOfCareId');
        },
        _getPlacementType: function () {
            var container = this.get('container'),
                placementType = container.one('#placementType');
            if (!placementType) return null;
            var selectedType = placementType.get('options').item(placementType.get('selectedIndex'));
            return selectedType ? selectedType.getAttribute('code') : null;
        }
    }, {
        ATTRS: {
            temporaryPlacementTypeClassifications: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationWithCodePathList({
                        url: this.get('temporaryPlacementTypesURL')
                    });
                }
            },
            latestEpisodeOfCare: {
                valueFn: function () {
                    return new Y.usp.childlookedafter.EpisodeOfCare({
                        url: L.sub(this.get('latestEpisodeOfCareURL'), {
                            periodOfCareId: this.get('otherData.periodOfCareId')
                        })
                    });
                }
            },
            fosterCarerHolidaysURL: {
                value: '',
                getter: '_getFosterCarerHolidaysURL'
            },
            personAutocompleteURL: {
                value: ''
            },
            organisationAutocompleteURL: {
                value: ''
            },
            carerAutocompleteURL: {
                value: ''
            }
        }
    });

    /**
     * @class app.childlookedafter.NewTemporaryPlacementView
     * @description Mixes in Y.app.childlookedafter.MixinClassificationsOptgroup, which provides the method setOptgroupSelect
     */
    Y.namespace('app.childlookedafter').NewTemporaryPlacementView = Y.Base.create('NewTemporaryPlacementView', BaseTemporaryPlacementView, [MixinOptgroup], {
        events: {
            'select[name="placementTypeId"]': {
                change: '_handlePlacementTypeChange'
            },
            '#startDate': {
                valuechange: '_handleDateChange'
            },
            '#endDate': {
                valuechange: '_handleDateChange'
            },
            '#carerTypeRadio': {
                click: '_handleCarerTypeChange'
            }
        },
        template: Y.Handlebars.templates.temporaryPlacementAddDialog,
        initializer: function () {
            var model = this.get('model');

            this.startDateCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar'
            });

            this.endDateCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar'
            });

            var temporaryPlacementTypePromise = new Y.Promise(function (resolve, reject) {
                this.get('temporaryPlacementTypeClassifications').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            temporaryPlacementTypePromise.then(function (data) {
                var container = this.get('container');
                this.setOptgroupSelect(container.one('#placementType'), data);
            }.bind(this));

            var latestEpisodeOfCarePromise = new Y.Promise(function (resolve, reject) {
                this.get('latestEpisodeOfCare').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            latestEpisodeOfCarePromise.then(function (data) {
                this.get('model').set('episodeOfCareId', data.id, {
                    silent: true
                });
            }.bind(this));

            this._handleSetPlacementProvider();

            this._evtHandlers = [
                this.startDateCalendar.after('dateSelection', this._handleDateChange, this),
                this.endDateCalendar.after('dateSelection', this._handleDateChange, this),
                this.after('*:selectedPersonChange', this._handlePersonOrOrganisationChange, this),
                this.after('*:selectedOrganisationChange', this._handlePersonOrOrganisationChange, this),
                //re-render only after load - we don't want to blow away the rendered dialog at any other time
                model.after('placementTypeIdChange', this._renderCarerView, this),
                model.after('load', function () {
                    this.render();
                }, this)
            ];
            this.warningsView = new TempPlacementWarningsView();
        },
        render: function () {
            var container = this.get('container');

            container.setHTML(this.template({
                modelData: this.get('model').toJSON() || {},
                otherData: this.get('otherData'),
                displayProperties: this.get('displayProperties'),
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries') || {}
            }));

            this.startDateCalendar.set('inputNode', container.one('#startDate'));
            this.startDateCalendar.render();

            this.endDateCalendar.set('inputNode', container.one('#endDate'));
            this.endDateCalendar.render();

            container.one('#carerSubject').hide();
            container.one('#carerControls').hide();
            container.one('#tempPlacementWarningsWrapper').append(this.warningsView.render().get('container'));
            return this;
        },
        renderSelectionView: function () {
            var container = this.get('container');
            if (this.selectionView) {
                //add our fragments to the content node
                container.one('#personSubjectSearchWrapper').setHTML(this.selectionView.render().get('container'));
            }
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;
            if (this.startDateCalendar) {
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }

            if (this.endDateCalendar) {
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
            if (this.warningsView) {
                this.warningsView.destroy();
                delete this.warningsView;
            }

            if (this.selectionView) {
                this.selectionView.removeTarget(this);
                this.selectionView.destroy();

                delete this.selectionView;
            }
        },
        _handleCarerTypeChange: function (e) {
            var labels = this.get('labels'),
                container = this.get('container');
            if (e.target.get('value') == 'PERSON') {
                container.one('#carerSubjectLabel').set('innerHTML', labels.carer + '<span class="mandatory"> *</span>');
            } else if (e.target.get('value') == 'ORGANISATION') {
                container.one('#carerSubjectLabel').set('innerHTML', labels.careOrganisation + '<span class="mandatory"> *</span>');
            }
            this._renderCarerView();
        },
        _handlePlacementTypeChange: function (e) {
            var labels = this.get('labels'),
                container = this.get('container'),
                selectedType = e.currentTarget.get('options').item(e.currentTarget.get('selectedIndex')),
                placementType = selectedType ? selectedType.getAttribute('code') : null;

            var target = e.currentTarget,
                name = target.get('name');
            this.get('model').set(name, DB.getElementValue(target._node));

            container.one('#carerControls').show();
            container.one('#carerSubject').show();
            if (placementType === 'TMP_FP_HOL') {
                container.one('#carerTypeRadio').hide();
                container.one('#carerSubjectLabel').set('innerHTML', labels.carer + '<span class="mandatory"> *</span>');

            } else if (placementType === 'TMP_HOSP') {
                container.one('#carerTypeRadio').hide();
                container.one('#carerSubjectLabel').set('innerHTML', labels.careOrganisation + '<span class="mandatory"> *</span>');

            } else {
                container.one('#carerTypeRadio').show();
                container.all('#carerType').set('disabled', false);
                var selectedPerOrg = this.get('container').one('input[name=carerType]:checked').get('value');
                if (selectedPerOrg === 'PERSON') {
                    container.one('#carerSubjectLabel').set('innerHTML', labels.carer + '<span class="mandatory"> *</span>');
                } else if (selectedPerOrg === 'ORGANISATION') {
                    container.one('#carerSubjectLabel').set('innerHTML', labels.careOrganisation + '<span class="mandatory"> *</span>');
                }
            }
            this._checkHolidayWarnings();
        },
        _handlePersonOrOrganisationChange: function (e) {
            if (e.newVal) {
                this.get('container').all('#carerType').set('disabled', true);
            } else {
                this.get('container').all('#carerType').set('disabled', false);
            }
        },
        _renderCarerView: function () {
            var _view;
            //destroy existing view
            if (this.selectionView) {
                //unbind from event bubble
                this.selectionView.removeTarget(this);
                //destroy the view
                this.selectionView.destroy();
            }
            // Work out which view we want to show - Carer, Person or Organisation
            var selCode = this.get('container').one('#placementType option:checked')._node.getAttribute('code');
            var selType;
            var controlType;
            if (selCode == 'TMP_ACC_OTH' || selCode == 'TMP_HOL') {
                selType = this.get('container').one('#carerType:checked').get('value');
            }
            if (selCode == 'TMP_HOSP' || selType == 'ORGANISATION') {
                controlType = 'ORGANISATION';
            } else if (selCode == 'TMP_FP_HOL') {
                controlType = 'CARER';
            } else if (selType == 'PERSON') {
                controlType = 'PERSON';
            }
            _view = this._getNewSelectionView(controlType);
            if (_view) {
                //add event bubble target
                _view.addTarget(this);
            }
            this.selectionView = _view;

            //render into specific location on container
            this.renderSelectionView();
        },
        _getNewSelectionView: function (controlType) {
            var _view;

            switch (controlType) {
            case 'CARER':
                //show carer view
                _view = new Y.app.childlookedafter.CarerAutocompleteView({
                    containingNodeSelector: '#temporaryPlacementAddForm',
                    autocompleteURL: this.get('carerAutocompleteURL'),
                    inputName: 'CarerSubjectSearch',
                    formName: 'temporaryPlacementAddForm',
                    labels: {
                        placeholder: 'Enter name or ID'
                    }
                });
                break;
            case 'ORGANISATION':
                _view = new Y.app.childlookedafter.OrganisationAutocompleteView({
                    containingNodeSelector: '#temporaryPlacementAddForm',
                    inputName: 'CarerSubjectSearch',
                    formName: 'temporaryPlacementAddForm',
                    autocompleteURL: this.get('organisationAutocompleteURL'),
                    labels: {
                        placeholder: 'Enter name or ID'
                    }
                });
                break;
            case 'PERSON':
                _view = new Y.app.childlookedafter.PersonAutocompleteView({
                    containingNodeSelector: '#temporaryPlacementAddForm',
                    inputName: 'CarerSubjectSearch',
                    formName: 'temporaryPlacementAddForm',
                    autocompleteURL: this.get('personAutocompleteURL'),
                    labels: {
                        placeholder: 'Enter name or ID'
                    }
                });
                break;
            }
            return _view;
        },
        _handleSetPlacementProvider: function () {
            var showProviderTypeField = this.get('displayProperties.showProviderTypeField');
            if (showProviderTypeField) {
                var placementProvision = Y.uspCategory.childlookedafter.placementProvision.category.getActiveCodedEntries().NA;
                this.get('model').setAttrs({
                    placementProvision: placementProvision.code
                }, {
                    silent: true
                });
            }
        }
    }, {
        ATTRS: {
            temporaryPlacementTypeClassifications: {
                valueFn: function () {
                    return new Y.usp.classification.ClassificationWithCodePathList({
                        url: this.get('temporaryPlacementTypesURL')
                    });
                }
            },
            latestEpisodeOfCare: {
                valueFn: function () {
                    return new Y.usp.childlookedafter.EpisodeOfCare({
                        url: L.sub(this.get('latestEpisodeOfCareURL'), {
                            periodOfCareId: this.get('otherData.periodOfCareId')
                        })
                    });
                }
            },
            fosterCarerHolidaysURL: {
                value: '',
                getter: '_getFosterCarerHolidaysURL'
            },
            personAutocompleteURL: {
                value: ''
            },
            organisationAutocompleteURL: {
                value: ''
            },
            carerAutocompleteURL: {
                value: ''
            },
            codedEntries: {
                value: {
                    placementProvisions: Y.uspCategory.childlookedafter.placementProvision.category.codedEntries
                }
            }
        }
    });

    /**
     * @class app.childlookedafter.UpdateTemporaryPlacementView
     * @description Mixes in Y.app.childlookedafter.MixinClassificationsOptgroup, which provides the method setOptgroupSelect
     */
    Y.namespace('app.childlookedafter').UpdateTemporaryPlacementView = Y.Base.create('updateTemporaryPlacementView', BaseTemporaryPlacementView, [MixinOptgroup], {
        events: {
            '#endDate': {
                valuechange: '_handleDateChange'
            }
        },
        template: Y.Handlebars.templates.temporaryPlacementEditDialog,
        initializer: function () {

            var model = this.get('model');

            this.endDateCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar'
            });

            this._evtHandlers = [
                this.endDateCalendar.after('dateSelection', this._handleDateChange, this),
                model.after('load', function () {
                    this.render();
                }, this)
            ];
            this.warningsView = new TempPlacementWarningsView().addTarget(this);
        },
        render: function () {
            var container = this.get('container');

            container.setHTML(this.template({
                modelData: this.get('model').toJSON() || {},
                otherData: this.get('otherData'),
                displayProperties: this.get('displayProperties'),
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries') || {}
            }));

            this.endDateCalendar.set('inputNode', container.one('#endDate'));
            this.endDateCalendar.render();

            container.one('#tempPlacementWarningsWrapper').append(this.warningsView.render().get('container'));
            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;
            if (this.endDateCalendar) {
                this.endDateCalendar.destroy();
                delete this.endDateCalendar;
            }
        },
        _getParsedStartDate: function () {
            return Y.USPDate.parseDate(this.get('otherData').placement.startDate);
        },
        _setOverlappingWarning: function () {
            // NB: will only display overwrite warning via the add temporary placement dialog
            // So this will just overwrite the base method and do nothing
        },
        _getEpisodeOfCareId: function () {
            return this.get('otherData').episodeOfCareId;
        },
        _getPlacementType: function () {
            return this.get('otherData').placement.placementType.code;
        }
    });

    Y.namespace('app.childlookedafter').ViewTemporaryPlacementView = Y.Base.create('viewTemporaryPlacementView', Y.usp.View, [], {
        template: Y.Handlebars.templates.temporaryPlacementViewDialog,

        render: function () {
            var container = this.get('container');

            container.setHTML(this.template({
                modelData: this.get('model').toJSON() || {},
                otherData: this.get('otherData'),
                displayProperties: this.get('displayProperties'),
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries') || {}
            }));

            return this;
        }

    });
    
    Y.namespace('app.childlookedafter').DeleteTemporaryPlacementView = Y.Base.create('deleteTemporaryPlacementView', Y.usp.View, [], {
    	template: Y.Handlebars.templates.temporaryPlacementDeleteDialog
    });
    
    Y.namespace('app.childlookedafter').TemporaryPlacementDialog = Y.Base.create('temporaryPlacementDialog', Y.usp.app.AppDialog, [], {
        handleAddTemporaryPlacement: function (e) {
            var view = this.get('activeView'),
                model = view.get('model'),
                container = view.get('container'),
                carerSubjectTypeValue = 'PERSON',
                carerSubjectId = null,
                personSubjectFieldIsEmpty = true,
                organisationCarerSubjectFieldIsEmpty = true,
                placementTypeSelectedOption = container.one('#placementType').get('options')._nodes[container.one('#placementType').get('selectedIndex')];

            if (view.selectionView !== null && typeof view.selectionView !== 'undefined') {
                personSubjectFieldIsEmpty = view.selectionView.autoCompleteView.get('selectedPerson') === null || typeof view.selectionView.autoCompleteView.get('selectedPerson') === 'undefined',
                    organisationCarerSubjectFieldIsEmpty = view.selectionView.autoCompleteView.get('selectedOrganisation') === null || typeof view.selectionView.autoCompleteView.get('selectedOrganisation') === 'undefined';
            }

            if (!personSubjectFieldIsEmpty) {
                carerSubjectId = view.selectionView.autoCompleteView.get('selectedPerson').id;
            }

            if (!organisationCarerSubjectFieldIsEmpty) {
                carerSubjectTypeValue = 'ORGANISATION';
                carerSubjectId = view.selectionView.autoCompleteView.get('selectedOrganisation').id;
            }

            //Set non model attributes here to be used for custom validation
            if (placementTypeSelectedOption) {
                var placementType = placementTypeSelectedOption.attributes.code;
                if(placementType) {
                    model.placementTypeCode = placementType.value;
                }
            }
            
            return Y.when(this.handleSave(e, {
                carerSubjectType: carerSubjectTypeValue,
                carerSubjectId: carerSubjectId,
                placementCategory: 'TEMPORARY'
            }));
        },
        handleUpdateTemporaryPlacement: function (e) {
            var view = this.get('activeView'),
                model = view.get('model');

            model.url = view.get('updateUrl');
            this.handleSave(e, {}, {
                transmogrify: Y.usp.childlookedafter.UpdateClosePlacement
            });
        },
        views: {
            add: {
                type: Y.app.childlookedafter.NewTemporaryPlacementView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleAddTemporaryPlacement',
                    disabled: true
                }]
            },
            edit: {
                type: Y.app.childlookedafter.UpdateTemporaryPlacementView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleUpdateTemporaryPlacement',
                    disabled: true
                }]
            },
            view: {
                type: Y.app.childlookedafter.ViewTemporaryPlacementView
            },
            deleteTempPlacement: {
                type: Y.app.childlookedafter.DeleteTemporaryPlacementView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> yes',
                    action: 'handleRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'model-form-link',
        'usp-childlookedafter-UpdateClosePlacement',
        'usp-childlookedafter-NewPlacement',
        'usp-childlookedafter-EpisodeOfCare',
        'childlookedafter-mixin-classifications-optgroup',
        'categories-childlookedafter-component-PlacementProvision',
        'handlebars',
        'handlebars-childlookedafter-templates',
        'calendar-popup',
        'entry-dialog-views',
        'usp-date',
        'template-micro',
        'childlookedafter-subject-autocompleteview'
    ]
});