YUI.add('childlookedafter-mixin-classifications-optgroup', function(Y) {

    'use-strict';

    var O = Y.Object,
    
	L = Y.Lang,
    
    DEFAULT_OPTIONS = {
        msg: 'Please choose',
        optgroupAttr: 'parentGroupName',
        displayAttr: 'name',
        valueAttr: 'id',
        value: '', // or something off modelData
    };


    function Mixin(config) {}


    Mixin.prototype = {
        /**
         * @method setOptgroupSelect
         * @description 
         */
    	setOptgroupSelect: function(node, data, options) {

                var html, modelList;
                
                if(!node) return;
                
                options = Y.merge({}, DEFAULT_OPTIONS, options || {});
                
                html = Y.Lang.sub('<option value="" class="blank">{msg}</option>', {
                    msg: options.msg
                });
                
                modelList = new Y.ModelList({
                    items: data
                });

                Y.Array.forEach(Y.Array.unique(modelList.get(options.optgroupAttr)), function(optgroup) {

                    html += Y.Lang.sub('<optgroup label="{optgroup}"></optgroup>', {
                    	optgroup: optgroup
                    });

                    Y.Array.forEach(modelList.toJSON().filter(function(item) {
                        return item[options.optgroupAttr] === optgroup;
                    }), function(option) {
                        html += Y.Lang.sub('<option code="{code}" value="{value}">{display}</option>', {
                            value: option[options.valueAttr],
                            display: '\xa0\xa0' + option[options.displayAttr],
                        	code: option.code
                        });
                    });
                });

                node.setHTML(html);
                node.set('value', (options.value)? options.value.toString() : '');
                
    	}
    };


    Y.namespace('app.childlookedafter').MixinClassificationsOptgroup = Mixin;


}, '0.0.1', {
    requires: [
        'yui-base',
        'event-custom-base'
    ]
});