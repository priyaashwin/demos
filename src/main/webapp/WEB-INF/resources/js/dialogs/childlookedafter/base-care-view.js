YUI.add('base-care-view', function (Y) {
    Y.namespace('app.childlookedafter').BaseAddCareView = Y.Base.create('baseAddCareView', Y.usp.View, [], {
        destructor: function () {
            if (this.selectionView) {
                this.selectionView.removeTarget(this);
                this.selectionView.destroy();
                delete this.selectionView;
            }
        },
        /**
         * Return a set of flags to indicate
         * field visibility
         */
        getFieldVisibility: function () {
            var carerSubjectType = this.get('carerSubjectType');

            var typeOfCareVisible = true;
            if (carerSubjectType === 'PERSON') {
                typeOfCareVisible = true;
            } else if (carerSubjectType === 'ORGANISATION') {
                typeOfCareVisible = false;
            }
            var fieldVisibility = {
                carerSubjectType: true,
                typeOfCare: typeOfCareVisible,
                parent: false,
                subjectAddress: false,
                placementAddressFields: false,
                personPlacementAddressField: false
            };
            return fieldVisibility;
        },
        /**
         * Performs the actual show/hide of the fields
         */
        setupFieldVisibility: function () {
            //get the field visibility
            var fieldVisibility = this.getFieldVisibility();

            //now set the fields based on the visibility object
            //these are the fields that are common over all views
            this.setCarerSubjectTypeVisibility(fieldVisibility.carerSubjectType);
            this.setTypeOfCareVisibility(fieldVisibility.typeOfCare);
            this.setParentFieldVisibility(fieldVisibility.parent);
            this.setSubjectAddressFieldVisibility(fieldVisibility.subjectAddress);
            this.setPlacementAddressInputFieldsVisibility(fieldVisibility.placementAddressFields);
            this.setPersonPlacementAddressField(fieldVisibility.personPlacementAddressField);

            //render the autocomplete views for carerSubjectType
            this._renderCarerView();

            //return the visibility object as may be useful to calling code
            return fieldVisibility;
        },
        _renderCarerView: function () {
            var _view;
            //destroy existing view
            if (this.selectionView) {
                //unbind from event bubble
                this.selectionView.removeTarget(this);
                //destroy the view
                this.selectionView.destroy();
            }

            //get appropriate auto-complete view
            _view = this.getNewSelectionView();
            if (_view) {
                //add event bubble target
                _view.addTarget(this);
            }
            this.selectionView = _view;

            //render into specific location on container
            this.renderSelectionView();

            //allow view / container views to handle new carer view.
            this.fire('newCarerViewCreated', {});
        },
        /**
         * Return a selection view of the appropriate type
         * Used to render the selection view
         * 
         * @method getNewSelectionView
         */
        getNewSelectionView: function () {
            return null;
        },
        renderSelectionView: function () {
            var container = this.get('container');
            if (this.selectionView) {
                //add our fragments to the content node
                container.one('#subjectSearchWrapper').setHTML(this.selectionView.render().get('container'));
            } else {
                //clear out the old selection view
                container.one('#subjectSearchWrapper').setHTML('');
            }
        },
        /**
         * 
         * @param {string} wrapper A selector to the field wrapper visibility
         * will be set on this
         * @param {string|array} field (or fields) selector to the field(s), this will be disabled
         * @param {boolean} visible A boolean value to indicate if the field should
         * be shown or hidden
         */
        setFieldVisibility: function (wrapper, field, visible) {
            var container = this.get('container');
            var wrapperNode = wrapper ? container.one(wrapper) : null;

            if (field) {
                var fieldVis = function (fld) {
                    var fieldNodes = fld ? container.all(fld) : null;
                    if (fieldNodes) {
                        if (visible) {
                            fieldNodes.removeAttribute('disabled');
                        } else {
                            fieldNodes.setAttribute('disabled', 'disabled');
                        }
                    }
                };
                if (Array.isArray(field)) {
                    field.forEach(fieldVis);
                } else {
                    fieldVis(field);
                }
            }

            if (wrapperNode) {
                if (visible) {
                    wrapperNode.setStyle('display', '');
                } else {
                    wrapperNode.setStyle('display', 'none');
                }
            }
        },
        /**
         * Locks or unlocks the carer subject type
         * depending on the boolean flag supplied.
         * If locked - only the currently selected value
         * is enabled.
         * 
         * @param {boolean} lock 
         */
        lockCarerSubjectType: function (lock) {
            var container = this.get('container');
            if (lock) {
                container.all('input[name="carerSubjectType"]').each(function (node) {
                    if (node.get('checked')) {
                        node.removeAttribute('disabled');
                    } else {
                        node.setAttribute('disabled', 'disabled');
                    }
                });
            }
        },
        setTypeOfCareVisibility: function (visible) {
            this.setFieldVisibility('.typeOfCareWrapper', 'select[name="typeOfCare"]', visible);
            this.setFieldVisibility('.currentTypeOfCareWrapper', 'input[name="currentTypeOfCare"]', visible);
        },
        setCarerSubjectTypeVisibility: function (visible) {
            this.setFieldVisibility('.carerSubjectTypeWrapper', 'input[name="carerSubjectType"]', visible);
            if (visible) {
                if (!this.get('carerSubjectType')) {
                    this.set('carerSubjectType', 'PERSON');
                }
            } else {
                //clear carer subject type - as fields will be hidden
                //this will clear the selection view also
                this.set('carerSubjectType', null);
            }
        },
        setParentFieldVisibility: function (visible) {
            // check that returned personVO is assigned parental responsibility
            const isParentalResponsibility = function (relationshipData) {
                const roleAId = relationshipData.roleAPersonId;
                const roleBId = relationshipData.roleBPersonId;

                const parentalResponsibilityId = relationshipData.parentalResponsibility === 'ROLE_A' ? roleAId : roleBId;

                return relationshipData.personVO.id === parentalResponsibilityId;
            }

            var container = this.get('container');
            this.setFieldVisibility('.parentWrapper', 'select[name="parent"]', visible);

            //always set pure-info it non-visible
            this.setFieldVisibility('.parentWrapper .pure-info', null, false);
            var parentList = this.get('parentList') || [];

            // Remove relationships that have a personVO that does not 
            // correspond to the person with parental responsibility
            const filteredParentList = parentList.filter(isParentalResponsibility);

            if (visible) {
                // No parental relationships returned for this person
                if (filteredParentList.length === 0) {
                    this.setFieldVisibility('.parentAddress', 'select[name="parent"]', false);
                    this.setFieldVisibility('.parentWrapper .pure-info', null, true);
                } else if (filteredParentList.length > 0) {
                    this.setFieldVisibility('.parentAddress', 'select[name="parent"]', true);
                    if (filteredParentList.length === 1) {
                        container.one('select[name="parent"]').set('value', filteredParentList[0].personVO.id);
                    }
                }
            }
        },
        setSubjectAddressFieldVisibility: function (visible) {
            this.setFieldVisibility('.addressWrapper', null, visible);
            //always set pure-info it non-visible
            this.setFieldVisibility('.addressWrapper .pure-info', null, false);
        },
        setPlacementAddressInputFieldsVisibility: function (visible) {
            this.setFieldVisibility('#addAddressWrapper', null, visible);
        },
        setPersonPlacementAddressField: function (visible) {
            this.setFieldVisibility('.personPlacementAddress', null, visible);
            //always set pure-info it non-visible
            this.setFieldVisibility('.personPlacementAddressWrapper .pure-info', null, false);

            if (visible) {
                if (this.personPlacementAddress) {
                    this.setFieldVisibility('.personPlacementAddress', null, true);
                } else {
                    this.setFieldVisibility('.personPlacementAddress', null, false);
                    this.setFieldVisibility('.personPlacementAddressWrapper .pure-info', null, true);
                }
            }
        },
    }, {
        ATTRS: {
            /**
             * Track the currently selected carer subject type
             */
            carerSubjectType: {
                value: 'PERSON'
            },
            parentList: {
                value: []
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'usp-view'
    ]
});