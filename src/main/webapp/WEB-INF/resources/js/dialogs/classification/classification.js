

YUI.add('classification-dialog-views', function (Y) {
	
	var HTML_HEADER = Y.app.AppStdDialog.DIALOG_HEADER;
		
	EVENT_INFO_MSG = 'infoMessage:message';
	EVENT_RELOAD_DATA = 'classification:dataChanged';
	
	
	var cancelDialog = function(e) {		   
		e.preventDefault();		   
		this.hide();	  
	};  

	 
	var addFormSubmit = function(e) {
    	
	  	var view = this.get('activeView'),   		
	  		model = view.get('model'),
	  		startDateToParse = view.get('container').one('#classification-start-date');

	    
	  	e.preventDefault(); 
	  	this.showDialogMask();
	  	
	  	model.setAttrs({
	  		startDate: Y.USPDate.parseDate(startDateToParse.get("value")),
	  		classificationId: view.get('selectedClassification')	  		
	  	}, { silent:true });
	  	
        model.save(Y.bind(function(err, res){

        	if(err){        
           		this.hideDialogMask();		
        	} else {
        		this.hide();
        		Y.fire(EVENT_RELOAD_DATA);
        		Y.fire(EVENT_INFO_MSG, { 
        			message: this.get('onSuccessMessages.addClassification')
        		});
        	}
        
        }, this));
     
	};			
	
	
	var editFormSubmit = function(e) {
		var _instance = this;
		 e.preventDefault();	  
		  	//show the mask
	   	 this.showDialogMask();	  	
	     // Get the view and the model
	     var view=this.get('activeView');
	     var model=view.get('model');
	     //modified the url because the REST url for loading the Classification Assignment
	     // is different from the endClassification url
	     model.url= model.endUrl;
	     model.save({
	    	 transmogrify:Y.usp.person.UpdateEndManagedClassificationAssignment
	     	},
				function(err, res){
	     		//hide the mask - note, this happens regardless of whether an error occurs
		          	_instance.hideDialogMask();
		     	// reverting the url back to how it was
		     	model.url= model.originalUrl;
	     		if (err){
						if(err.code !== 400){
							Y.log(err.code + err.msg);
						}
	     		}
	     		else {
	     			 _instance.hide();
	   		         Y.fire('classification:dataChanged');
			         Y.fire(EVENT_INFO_MSG, {message: 'Classification successfully ended' }); // TODO reinistatte MProp commonLabels.successMessageLabels.endClassificationSuccessMessage
	             }
	         });
		 
	};
	
	var	deleteFormSubmit = function(e) {
		 var _instance = this;
	     var view=this.get('activeView');
	     var model=view.get('model');
	     
		 e.preventDefault();
	   	 this.showDialogMask();	  	
	     
	     
		//modified the url because the REST url for loading the Classification Assignment
    // is different from the endClassification url
	   	 
	   	 model.url=model.deletedUrl;
		
	     model.save({
	    	 transmogrify:Y.usp.classification.ClassificationAssignment
	     	},
				function(err, res){
	     		
	     			//hide the mask - 
		         	_instance.hideDialogMask();
		         
		         	// reverting the url back to how it was
		         	model.url=model.originalUrl;
			     
			     
		         	if(err){								     		
		         		if(err.code !== 400){									     			
		         			Y.log(err.code + err.msg);								     		
		         		}
		     		
		         	} else {
		         		_instance.hide();
		   		         Y.fire(EVENT_RELOAD_DATA);
				         Y.fire(EVENT_INFO_MSG, {message: 'Classification successfully deleted' }); // TODO reinistatte MProp commonLabels.successMessageLabels.deleteClassificationSuccessMessage
				         //commonLabels.successMessageLabels.deleteClassificationSuccessMessage});
		         	}

	     	  });
		 
		};

	var MultiPanelPopUp = Y.Base.create('classificationPopUp', Y.app.BaseMultiPanel, [Y.app.classification.Mixin], {

		views: {

			addClassification: {
				type: Y.app.classification.AddForm, 
				headerTemplate: HTML_HEADER,
				buttons: [
		           {
		             action:    addFormSubmit,
		             section:   Y.WidgetStdMod.FOOTER,
		             name:      'saveButton',
		             labelHTML: Y.app.AppStdDialog.BTN_LABEL_SAVE,
		             classNames:'pure-button-primary',
		             disabled:  true
		           },
		           {
		             action:     cancelDialog,
		             section:    Y.WidgetStdMod.FOOTER,
		             name:      'noButton', 
		             labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL, 
		             classNames:'pure-button-secondary'
		           }
	           ]
		
			},
				   	
			endClassification: {	   						
				type: Y.app.classification.EditForm,						
				headerTemplate: HTML_HEADER,
				buttons:[
					{
						action:    editFormSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'yesButton',
						labelHTML: Y.app.AppStdDialog.BTN_LABEL_YES, 
						classNames:'pure-button-primary',
						disabled:  true
					},
			        {			          
						action:   cancelDialog,			          
						section:    Y.WidgetStdMod.FOOTER,			          
						name:      'noButton', 			          
						labelHTML:  Y.app.AppStdDialog.BTN_LABEL_NO, 		          
						classNames:'pure-button-secondary'
			        }
				]
			},
			
			deleteClassification: {	   						
				type: Y.app.classification.DeleteForm,	
				headerTemplate: HTML_HEADER,
				buttons:[
					{
						action:    deleteFormSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'yesButton',
						labelHTML: Y.app.AppStdDialog.BTN_LABEL_YES, 
						classNames:'pure-button-primary',
						disabled:  true
					},
			        {			          
						action:   cancelDialog,			          
						section:    Y.WidgetStdMod.FOOTER,			          
						name:      'noButton', 			          
						labelHTML:  Y.app.AppStdDialog.BTN_LABEL_NO, 		          
						classNames:'pure-button-secondary'
			        }
				]
			}
		}
		
	},{
		CSS_PREFIX: 'yui3-panel'
	});

	
	Y.namespace('app.classification').MultiPanelPopUp = MultiPanelPopUp;

}, '0.0.1', {
	
	requires: [
	   'yui-base',
	   'event-custom',	         
	   'app-multi-panel',
	   'classification-form-add',
	   'classification-form-delete',
	   'classification-form-edit',
	   'classification-models',
	   'classification-mixin',
	   'usp-person-UpdateEndManagedClassificationAssignment',
	   'usp-date'
	]
});