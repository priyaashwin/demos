YUI.add('classification-configuration-dialog-views', function(Y) {
    var L = Y.Lang;
    var HTML_HEADER = Y.app.AppStdDialog.DIALOG_HEADER,
        CLASSIFICATION_MULTIPANEL_DIALOG_ACTION = 'classificationConfig:showDialog',

        EVENT_INFO_MSG = 'infoMessage:message',
        EVENT_RELOAD_DATA = 'classificationsTree:dataChanged';


    var cancelDialog = function(e) {
        e.preventDefault();
        this.hide();
    };

    var proceedClassifcationSubmit = function(e) {
        var view = this.get('activeView'),
            container = view.get('container'),
            otherData = view.get('otherData'),
            action = 'NONE';
        if (container.one('#classfication_type')._node.checked) {
            action = 'addClassification';
        } else if (container.one('#classfication_group_type')._node.checked) {
            action = 'addClassificationGroup';
        }
        e.preventDefault();
        if (!container.one('#classfication_type').checked && container.one('#classfication_group_type').checked) {
            var e = {};
            e.code = 400;
            e.msg = {
                validationErrors: {
                    type: 'Please select at least one option'
                }
            }
            view.fire('error', {
                error: e
            });
            return;
        }
        Y.fire(CLASSIFICATION_MULTIPANEL_DIALOG_ACTION, {
            action: action,
            parentGroupId: otherData.parentGroupId,
            codePath: otherData.codePath
        });
    }

    var addClassificationGroupSubmit = function(e) {

        var _instance = this,
            view = this.get('activeView'),
            model = view.get('model'),
            container = view.get('container'),
            labels = view.get('labels'),
            otherData = view.get('otherData');


        e.preventDefault();
        this.showDialogMask();

        model.url = L.sub(model.url, {
            id: otherData.parentGroupId
        });

        model.setAttrs({
            mutuallyExclusive: container.one('input[name="mutuallyExclusive"]').get('checked'),
            parentGroupId: otherData.parentGroupId
        }, {
            silent: true
        });
        //Need to do model save
        model.save(Y.bind(function(err, res) {

            if (err) {
                this.hideDialogMask();
            } else {
                this.hide();
                Y.fire(EVENT_RELOAD_DATA);
                Y.fire(EVENT_INFO_MSG, {
                    message: _instance.get('onSuccessMessages.addClassificationGroup')
                });
            }

        }, this));

    };


    var editClassificationGroupSubmit = function(e) {
        var _instance = this;
        e.preventDefault();
        //show the mask
        _instance.showDialogMask();
        // Get the view and the model
        var view = _instance.get('activeView'),
            container = view.get('container'),
            model = view.get('model');

        model.setAttrs({
            mutuallyExclusive: container.one('input[name="mutuallyExclusive"]').get('checked'),
        }, {
            silent: true
        });

        model.save({
                transmogrify: Y.usp.classification.UpdateClassificationGroup
            },
            function(err, response) {
                //hide the mask
                _instance.hideDialogMask();
                if (err === null) {
                    _instance.hide();
                    Y.fire(EVENT_RELOAD_DATA);
                    Y.fire(EVENT_INFO_MSG, {
                        message: _instance.get('onSuccessMessages.editClassificationGroup')
                    });
                } else {
                    _instance.hideDialogMask();
                }
            });

    };

    var deleteClassificationGroupSubmit = function(e) {
        var _instance = this;
        var view = this.get('activeView'),
            model = view.get('model');

        e.preventDefault();
        this.showDialogMask();

        model.url = model.archivedUrl;

        model.save({
                transmogrify: Y.usp.classification.ClassificationGroup
            },
            function(err, res) {
                //hide the mask - 
                _instance.hideDialogMask();
                // reverting the url back to how it was
                model.url = model.originalUrl;
                if (err) {
                    if (err.code !== 400) {
                        Y.log(err.code + err.msg);
                    }
                } else {
                    _instance.hide();
                    Y.fire(EVENT_RELOAD_DATA);
                    Y.fire(EVENT_INFO_MSG, {
                        message: _instance.get('onSuccessMessages.deleteClassificationGroup')
                    });
                }

            });

    };

    var addClassificationSubmit = function(e) {

        var _instance = this,
            view = _instance.get('activeView'),
            model = view.get('model'),
            container = view.get('container'),
            otherData = view.get('otherData');


        e.preventDefault();
        this.showDialogMask();
        model.url = L.sub(model.url, {
            id: otherData.parentGroupId
        });
        model.setAttrs({
            significantWhenCurrent: container.one('input[name="significantWhenCurrent"]').get('checked'),
            significantWhenHistoric: container.one('input[name="significantWhenHistoric"]').get('checked'),
            keyInformation: container.one('input[name="keyInformation"]').get('checked'),
            notManuallyAssignable: container.one('input[name="notManuallyAssignable"]').get('checked'),
            parentGroupId: otherData.parentGroupId
        }, {
            silent: true
        });
        //Need to do model save
        model.save(Y.bind(function(err, res) {

            if (err) {
                this.hideDialogMask();
            } else {
                this.hide();
                Y.fire(EVENT_RELOAD_DATA);
                Y.fire(EVENT_INFO_MSG, {
                    message: _instance.get('onSuccessMessages.addClassification')
                });
            }

        }, this));

    };


    var editClassificationSubmit = function(e) {
        var _instance = this,
            view = _instance.get('activeView'),
            model = view.get('model'),
            container = view.get('container');
        e.preventDefault();
        //show the mask
        _instance.showDialogMask();
        // Get the view and the model
        var view = _instance.get('activeView'),
            model = view.get('model');

        model.setAttrs({
            significantWhenCurrent: container.one('input[name="significantWhenCurrent"]').get('checked'),
            significantWhenHistoric: container.one('input[name="significantWhenHistoric"]').get('checked'),
            keyInformation: container.one('input[name="keyInformation"]').get('checked'),
            notManuallyAssignable: container.one('input[name="notManuallyAssignable"]').get('checked'),
        }, {
            silent: true
        });

        model.save({
                transmogrify: Y.usp.classification.UpdateClassification
            },
            function(err, response) {
                //hide the mask
                _instance.hideDialogMask();
                if (err === null) {
                    _instance.hide();
                    Y.fire(EVENT_RELOAD_DATA);
                    Y.fire(EVENT_INFO_MSG, {
                        message: _instance.get('onSuccessMessages.editClassification')
                    });
                } else {
                    _instance.hideDialogMask();
                }
            });

    };

    var deleteClassificationSubmit = function(e) {
        var _instance = this;
        var view = this.get('activeView'),
            model = view.get('model');

        e.preventDefault();
        this.showDialogMask();

        model.url = model.archivedUrl;

        model.save({
                transmogrify: Y.usp.classification.Classification
            },
            function(err, res) {
                //hide the mask - 
                _instance.hideDialogMask();
                // reverting the url back to how it was
                model.url = model.originalUrl;
                if (err) {
                    if (err.code !== 400) {
                        Y.log(err.code + err.msg);
                    }
                } else {
                    _instance.hide();
                    Y.fire(EVENT_RELOAD_DATA);
                    Y.fire(EVENT_INFO_MSG, {
                        message: _instance.get('onSuccessMessages.deleteClassification')
                    });
                }

            });

    };

    var MultiPanelPopUp = Y.Base.create('classificationConfigurationPopUp', Y.app.BaseMultiPanel, [Y.app.classification.Mixin], {

        views: {
            addClassificationProceed: {
                type: Y.app.classification.configuration.AddClassificationProceedForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: proceedClassifcationSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'proceedButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_PROCEED,
                    classNames: 'pure-button-primary',
                    disabled: true
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL,
                    classNames: 'pure-button-secondary'
                }]

            },
            addClassificationGroup: {
                type: Y.app.classification.configuration.AddClassificationGroupForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: addClassificationGroupSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_SAVE,
                    classNames: 'pure-button-primary',
                    disabled: true
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL,
                    classNames: 'pure-button-secondary'
                }]

            },

            addClassification: {
                type: Y.app.classification.configuration.AddClassificationForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: addClassificationSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_SAVE,
                    classNames: 'pure-button-primary',
                    disabled: true
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL,
                    classNames: 'pure-button-secondary'
                }]

            },

            editClassificationGroup: {
                type: Y.app.classification.configuration.EditClassificationGroupForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: editClassificationGroupSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_SAVE,
                    classNames: 'pure-button-primary',
                    disabled: true
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL,
                    classNames: 'pure-button-secondary'
                }]
            },

            editClassification: {
                type: Y.app.classification.configuration.EditClassificationForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: editClassificationSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'saveButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_SAVE,
                    classNames: 'pure-button-primary',
                    disabled: true
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL,
                    classNames: 'pure-button-secondary'
                }]
            },
            deleteClassificationGroup: {
                type: Y.app.classification.configuration.DeleteClassificationGroupForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: deleteClassificationGroupSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'yesButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_YES,
                    classNames: 'pure-button-primary',
                    disabled: true
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_NO,
                    classNames: 'pure-button-secondary'
                }]
            },
            deleteClassification: {
                type: Y.app.classification.configuration.DeleteClassificationForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: deleteClassificationSubmit,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'yesButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_YES,
                    classNames: 'pure-button-primary',
                    disabled: true
                }, {
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_NO,
                    classNames: 'pure-button-secondary'
                }]
            },
            viewClassification: {
                type: Y.app.classification.configuration.ViewClassificationForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL,
                    classNames: 'pure-button-secondary'
                }]
            },
            viewClassificationGroup: {
                type: Y.app.classification.configuration.ViewClassificationGroupForm,
                headerTemplate: HTML_HEADER,
                buttons: [{
                    action: cancelDialog,
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: Y.app.AppStdDialog.BTN_LABEL_CANCEL,
                    classNames: 'pure-button-secondary'
                }]
            }
        }
    }, {
        CSS_PREFIX: 'yui3-panel'
    });


    Y.namespace('app.classification.configuration').MultiPanelPopUp = MultiPanelPopUp;

}, '0.0.1', {

    requires: [
        'yui-base',
        'event-custom',
        'app-multi-panel',
        'classification-mixin',
        'usp-date',
        'usp-classification-UpdateClassificationGroup',
        'usp-classification-UpdateClassification',
        'classification-config-form-add',
        'classification-config-form-edit',
        'classification-config-form-delete',
        'classification-config-form-view',
        'classification-group-config-form-add',
        'classification-group-config-form-edit',
        'classification-group-config-form-delete',
        'classification-group-config-form-view'
    ]
});