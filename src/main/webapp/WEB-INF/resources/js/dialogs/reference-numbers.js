

YUI.add('reference-number-dialog-views', function(Y) {

	var O=Y.Object, L=Y.Lang;
	var HEADER_HTML = '<h3 id="labelledby" aria-describedby="describedby" class="iconic-title">\
		<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-building-o fa-inverse fa-stack-1x"></i></span> {title}\
		</h3>';
		
	var EVENT_INFO_MESSAGE = 'infoMessage:message';
	var EVENT_REFRESH_RESULTS = 'address:refreshResults';
	
	var cancelDialog = function(e) {
   		e.preventDefault();
   		this.hide();
	}; 
	
	var UpdateRemoveReferenceNumberModel=Y.Base.create("UpdateRemoveReferenceNumberModel", Y.usp.relationshipsrecording.UpdateDeleteOrganisationReferenceNumbers,[],{
		validate: function(attributes, callback){
			var errs={};
			attributes.updateReferenceNumberVOs.forEach(function(referenceNumber){
				if(referenceNumber.referenceNumber===""){
					// No access to reference number type in the model hence the Y.one lookup to get the name of the reference number in error
					errs['referenceNumber_'+referenceNumber.id]=Y.one('#referenceNumberLabel_'+referenceNumber.id).getHTML()+' cannot be empty';
				}
			});

			if(O.keys(errs).length>0){
				return callback({
					code:400,
					msg:{validationErrors:errs}
				});
			}
			
			// Now call any validate in the class we extended UpdateDeletePersonReferenceNumbers
			return UpdateRemoveReferenceNumberModel.superclass.validate.apply(this, arguments);
		}
	});
	
	var addReferenceNumberSubmit = function(e) {
		
		// prevent duplicate save on double click
	  	var _instance = this;
	    e.preventDefault(); 
		//show the mask
		this.showDialogMask();
		
        // Get the view and the model
        var view=this.get('activeView'),
        model=view.get('model'),
        labels = view.get('labels');
            
        model.setAttrs({
        	
    	},{
    		//don't trigger the onchange listener as we will re-render after save
    		silent:true
    	});		        

     	// Do the save   
        model.save(function(err, res){
        		//hide the mask -	prevent duplicate save on double click 
        		_instance.hideDialogMask();	
        		if (err){
					if(err.code !== 400){
						Y.log(err.code + err.msg);
					}
        		}else{
        			_instance.hide();
  					Y.fire('referenceNumberChanged');
  					Y.fire('infoMessage:message', {message:labels.referenceNumberAddSuccessMessage});
        	}
        });
		  		
	};
	
	var editReferenceNumberSubmit = function(e){
		referenceNumberEdited=0, 
		referenceNumberDeleted=0;
		// prevent duplicate save on double click
	  	var _instance = this;
		e.preventDefault();
		this.showDialogMask();		//show the mask
		
		var deleteRefNums = [], updateRefNums = [], model, view,referenceNumInput,removeReferenceNum;
		
		model = this.get('activeView').get('model').toJSON();
		view = this.get('activeView').get('container');
		labels = this.get('activeView').get('labels');
					
		model.forEach(function(referenceNumber){
			referenceNumInput = view.getById('referenceNumber_'+referenceNumber.id);
			removeReferenceNum = referenceNumInput.getAttribute('data-remove-num');
			if(removeReferenceNum){
				// add reference numbers to delete to the array
				deleteRefNums.push(referenceNumber.id);
				referenceNumberDeleted = referenceNumberDeleted+1;
			}
			else{
				currentValue = referenceNumInput.get('value');
				if(currentValue!==referenceNumber.referenceNumber){
					referenceNumberEdited = referenceNumberEdited+1;
				}
					updateReferenceNumber = {"referenceNumber":currentValue,"id":referenceNumber.id,"startDate":referenceNumber.startDate,"objectVersion":referenceNumber.objectVersion,"setId":true ,"_type":"UpdateOrganisationReferenceNumber" };
					updateRefNums.push(updateReferenceNumber);
			}
		});
		
		updateRemoveReferenceNumberModel = new UpdateRemoveReferenceNumberModel(
			{
				"_type":"UpdateDeleteOrganisationReferenceNumbers",
				"objectVersion":0,
				"updateReferenceNumberVOs":updateRefNums,
				"deleteOrganisationReferenceNumberIds":deleteRefNums
			}
		);
		updateRemoveReferenceNumberModel.root = this.get('activeView').get('url');
		// Adding this model to the organisationDialog targets allows us to use the multipanel
		// error message
		updateRemoveReferenceNumberModel.addTarget(_instance);
		
		updateRemoveReferenceNumberModel.save(function(err, res){
			//hide the mask -	prevent duplicate save on double click 
        	_instance.hideDialogMask();
			if (err){
				if(err.code !== 400){
					Y.log(err.code + err.msg);
				}
    		}else{
    			_instance.hide();
					Y.fire('referenceNumberChanged');
    			//Show success message, even if no changes have been made, consistent with rest of app
    			if(referenceNumberEdited<2 && referenceNumberDeleted===0){
    				Y.fire('infoMessage:message', {message:labels.referenceNumberSingleEditSuccessMessage});	
				}
    			//Multiple edits only
    			else if(referenceNumberEdited>1 && referenceNumberDeleted===0){
    				Y.fire('infoMessage:message', {message:labels.referenceNumberMultipleEditSuccessMessage});		
    			}
    			//Single remove only
    			else if(referenceNumberEdited===0 && referenceNumberDeleted===1){
    				Y.fire('infoMessage:message', {message:labels.referenceNumberSingleRemoveSuccessMessage});	
    			}
    			//Multiple remove only
    			else if(referenceNumberEdited===0 && referenceNumberDeleted>1){
    				Y.fire('infoMessage:message', {message:labels.referenceNumberMultipleRemoveSuccessMessage});	
    			}
    			// Edit(s) and remove(s)
    			else if(referenceNumberEdited>0 && referenceNumberDeleted>0){
    				Y.fire('infoMessage:message', {message:labels.referenceNumberMultipleEditRemoveSuccessMessage});	
				}
    	}
    });
	};
	
	Y.namespace('app.referenceNumber').NewPersonReferenceNumberForm=Y.Base.create("NewPersonReferenceNumberForm", Y.usp.person.NewPersonReferenceNumber, [Y.usp.ModelFormLink],{    
			
		form: '#personReferenceNumberAddForm',
		customValidation: function(attrs){
			var errors = {};
			
			var validationLabels = this.labels.validationLabels || {},
			type = attrs.type,
			referenceNumber = attrs.referenceNumber;
			
			if (this.isAttrNull(type)){
				errors['type'] = validationLabels.typeMandatory;
			}
			
			if (this.isAttrNull(referenceNumber)){
				errors['referenceNumber'] = validationLabels.referenceNumberMandatory;
			}
			
			
			
			return errors;
		},
		
		isAttrNull: function(attr) {
			if(L.isNull(attr) || L.isUndefined(attr) || attr === ""){
				return true;
			}
			
			return false;
		}
	});
	

	Y.namespace('app.referenceNumber').MultiPanelPopUp = Y.Base.create('referenceNumberMultiPanelPopUp', Y.usp.MultiPanelPopUp, [], {
		
		initializer: function() {
			this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
		},
		
		destructor: function() {
			this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
		},
		
		views: {
			addReferenceNumber: {		
				type: Y.app.referenceNumber.AddForm,
				headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'Add reference number' }),
				buttons: [{
	        	   	action: 	addReferenceNumberSubmit,
	        	   	section: 	Y.WidgetStdMod.FOOTER,
					name:      'saveButton',
					labelHTML: '<i class="fa fa-check"></i> save',
					classNames:'pure-button-primary'
				},{
					action: 	cancelDialog,
		            section:    Y.WidgetStdMod.FOOTER,
		            name:       'cancelButton', 
		            labelHTML:  '<i class="fa fa-times"></i> cancel',
		            classNames: 'pure-button-secondary'
				}]
			},
			editRemoveReferenceNumber: {
				type: Y.app.referenceNumber.EditForm,
				headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'Edit reference numbers' }),
				buttons: [{
	        	   	action: 	editReferenceNumberSubmit,
	        	   	section: 	Y.WidgetStdMod.FOOTER,
					name:      'saveButton',
					labelHTML: '<i class="fa fa-check"></i> save',
					classNames:'pure-button-primary'
				},{
					action: 	cancelDialog,
		            section:    Y.WidgetStdMod.FOOTER,
		            name:       'cancelButton', 
		            labelHTML:  '<i class="fa fa-times"></i> cancel',
		            classNames: 'pure-button-secondary'
				}]
			}
		}
		
	},{
		
		CSS_PREFIX : 'yui3-panel',
		ATTRS: {
			
			width: {
				value: 700
			},
			
			buttons: {
				value : ['close' ]
			}
		}
	});

	

}, '0.0.1', {
	  requires: ['yui-base',	             
	             'handlebars',
	             'handlebars-helpers',
	             'handlebars-organisation-templates', 
	             'event-custom',
	             'usp-organisation-NewOrganisation',
	             'usp-relationshipsrecording-UpdateDeleteOrganisationReferenceNumbers',
	             'app-reference-number-add-form',
	             'app-reference-number-edit-remove-form',
	             'usp-person-NewPersonReferenceNumber']
	});