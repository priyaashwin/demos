YUI.add('relationships-forms-person-team', function (Y) {
	
	var E = Y.Escape,
    L = Y.Lang,
    A = Y.Array,
    O = Y.Object;
	

/** 
	 * StateToggleCalendarPopup
	 * ========================
	 * Extends Y.usp.CalendarPopup
	 * 
	 * We need the start date in Add Relationship to have an enabled/disabled state. The available disable and enable
	 * methods in CalendarPopup only effect the input node, not the date-picker button which remains click-able even 
	 * if input has been disabled.
	 *  
	 * _handleButtonClick 	- method overrides Y.usp.CalendarPopup method to not do anything if 'buttonDisabled' is true
	 * enableButton 		- removes CSS disabled class and sets a 'buttonDisabled' state to true
	 * disableButton 		- adds CSS disabled class and sets a 'buttonDisabled' state to false
	 * getButton			- helper to return the button node
	 * 
	 */
	
	var StateToggleCalendarPopup = Y.Base.create('calendarPopup', Y.usp.CalendarPopup, [], { 
		_handleButtonClick: function (e) {
            e.preventDefault();
            var instance = this;
            
            if(instance.get('buttonDisabled')) {
               return;
            }
            
            if (instance.get('visible') === true) {
            	instance.hide();
            } else {
            	instance.show();
            }           
        },
        getButton: function() {
        	return this._calendarButton.one(this.get('SELECTOR_CAL_BUTTON'));
        },
        enableButton: function() {
        	this.set('buttonDisabled', false);
        	this.getButton().removeClass(this.get('CSS_BUTTON_DISABLED'));
        },
        disableButton: function() {
        	this.set('buttonDisabled', true);
        	this.getButton().addClass(this.get('CSS_BUTTON_DISABLED'));
        }
	},{
        ATTRS: {
        	SELECTOR_CAL_BUTTON: {
        		value: '.pure-button-cal'
        	},
        	CSS_BUTTON_DISABLED: {
        		value: 'pure-button-disabled'
        	},
        	buttonDisabled: {
        		value: false
        	}
        }
	});
	
	Y.namespace('app.relationship').AddPersonTeamRelationshipView =Y.Base.create('AddPersonTeamRelationshipView', 
			Y.usp.relationship.NewPersonOrganisationRelationshipView,[Y.app.mixin.MultiView],{
		
		events: {			
			'#relatedOrganisationList li a': { click: 'changeRelatedOrganisation' },
		},
		
		initializer: function(config) {
			
		},
		
		template:Y.Handlebars.templates["relationshipAddPersonOrganisationDialog"],
		
		getViews: function() {
	    	
			var target = {
            	entityType: 'person',
            	id: this.get('targetPersonId'),
            	name: this.get('targetPersonName')            	
            };
			
			return {
	    		attributesPanel: {
	    			hook: '#attributes-panel-hook',
	    			type: Y.app.relationship.AttributesPanel,
	    			relationshipType: this.get('relationshipType'),
	    			targetEntity: target,
	    			isEdit:false
	    		}
	    	};
	    },
	    
	    initAttributesPanel: function(e) {
	    	
	    	this.attributesPanel.set('relationshipType', this.get('relationshipType'));
	    
            
            
	    },

		render: function(){
			var container = this.get('container');
			narrativeSummary = "You're adding a team relationship for ";
			narrativeDescription = this.get('targetPersonName') + ' (PER' + this.get('targetPersonId') + ')'; 
			teamRelation = true;

			html = this.template({
				labels: this.get('labels'),
				narrativeSummary: narrativeSummary,
				narrativeDescription: narrativeDescription,
				teamRelation: teamRelation
			});
			container.setHTML(html);
			this.initAutoComplete();
			
			//call render on our calendar
			this.startDateCalendar = new StateToggleCalendarPopup({
				//bind to start date field
				inputNode:this.getInput('startDate'),
				minimumDate: new Date(1900,1,1)
			});
			
			this.startDateCalendar.render();
			this.startDateCalendar.disableButton();
		},
		
		destructor: function(){
			//destroy autocomplete
			var relatedOrganisationSearch=this.getInput('relatedOrganisationSearch'),calNode=this.getInput('startDate');

			if(relatedOrganisationSearch){
				//unplug everything
				relatedOrganisationSearch.unplug();
				//destroy node
				relatedOrganisationSearch.destroy();
			}
			if(this.startDateCalendar){
				//destroy calendar
			    this.startDateCalendar.destroy();				
				delete this.startDateCalendar;
			}
			if(calNode){
				//destroy node
				calNode.destroy();
				calNode=null;				
			}			
		},
		
	onFuzzyDayChange: function(e) {
			
			e.preventDefault();
			
			var container = this.get('container');
			var enteredDOB = new Date();
			var day, month, year;
			
			day = Number(container.one('#fuzzyDOBDAY').get('value'));
			month = container.one('#fuzzyDOBMONTH').get('value');
			year = container.one('#fuzzyDOBYEAR').get('value');
			
			enteredDOB.setFullYear(year, month, day);
			enteredDOB.setHours(12, 0, 0, 0);
			
			if(Number(day)){
				this.checkDate(enteredDOB);
				this.toggleEstimatedCheckbox(container, false);
			} else {
				this.toggleEstimatedCheckbox(container, true);
				this.checkDate(this.monthOnlyDOBCalculation(month, year));
			}
			
		},
		
		 onFuzzyMonthChange: function(e) {			  
			  var container = this.get('container'),
			      month,year;
			  
              e.preventDefault();
              
			  month = container.one('#fuzzyDOBMONTH').get('value');
			  year = container.one('#fuzzyDOBYEAR').get('value');
			  
			  this.toggleEstimatedCheckbox(container, true);
			  this.checkDate(this.monthOnlyDOBCalculation(month, year));					
		  }, 
		  checkDate:function(date){
				var today, years, days, age,
				//prevent update of date passed in from event handler
		        enteredDOB=new Y.USPDate.parseDate(date);
				
				// Calculate today date and time
				today = new Date();
				today.setHours(12, 0, 0, 0);
				
				// calculate the number of whole years up to last birthday
				years = today.getFullYear() - enteredDOB.getFullYear();
				enteredDOB.setFullYear(enteredDOB.getFullYear() + years);
				
				// check if DOB has come this year or not
				if(enteredDOB > today){
					// no birthday yet, so reduce the age
					years--;
					// set the Full year back by one to calculate the number of days till age increments
					enteredDOB.setFullYear(enteredDOB.getFullYear() -1);
				}	
				// Get the number of days between today and last birthday
				days = (today.getTime() - enteredDOB.getTime()) / (3600 * 24 * 1000);
				
				// divide days by 365 or 366 if it's leap year. Round down and add to years.
				age = years + Math.floor(days / (this.isLeapYear(today.getFullYear()) ? 366 : 365));
				if(age < 0){
					age="0";
				}
			   	this.getInput('age').set('value', age);
			   	
			},
				
			
			isLeapYear:function(year){
				 var date = new Date(year, 1, 28);
				 date.setDate(date.getDate() + 1);
				 // If it is a leap year, then the month will remain 1 (0=Jan,1=Feb)
				 // else the increment above will push it into March
				 return date.getMonth() === 1;
			},
			
		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#relationship_'+inputId);
		},
		getLabel: function(inputId) {
			var container = this.get('container');
			return container.one('#relationship_'+inputId+ '_label');
		},
		
		getWrapper: function(inputId) {
			var container = this.get('container');
			return container.one('#relationship_'+inputId+ '_wrapper');
		}, 
		initAutoComplete: function(){
			var relatedOrganisationSearch = this.getInput('relatedOrganisationSearch');
			
			// Adjust auto complete drop down according to the width of the input box 
			var inputWidth = '35em';
/*
			// Mobile devices have input field hidden initially (Hence width is calculated as 0), therefore we fix 50% width for mobiles only
			if (inputWidth==0) {inputWidth="50%";} */

			var applicationMaxResults = 10;
			var ds=new Y.DataSource.IO({
				source:L.sub('/eclipse/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&byOrganisationType=true&organisationTypes=TEAM&nameOrOrgIdOrPrevName='),
				//source:'/eclipse/rest/organisation?useSoundex=false&escapeSpecialCharacters=true&appendWildcard=true&nameOrId=', //smi&useSoundex=false&escapeSpecialCharacters=true&appendWildcard=true
				ioConfig: {headers:{'Accept': "application/vnd.olmgroup-usp.organisation.OrganisationWithAddressAndPreviousNames+json"}}
			});

			ds.plug(Y.Plugin.DataSourceJSONSchema, {
				schema: {
					resultListLocator: "results"
				}
			});


			var input = relatedOrganisationSearch.plug(Y.Plugin.usp.PopupAutoCompletePlugin,{
				allowBrowserAutocomplete:false,
				enableCache:false,
				maxResults:applicationMaxResults,
				minQueryLength:1,
				width : inputWidth,
				resultListLocator: function(response){
					return response||[];
				},
				resultTextLocator: 'organisationIdentifier',
				requestTemplate : "{query}"+'&escapeSpecialCharacters=true&appendWildcard=true&pageSize='+applicationMaxResults,

				// Placing static values in the source
				//source :'/relationshipsrecording/rest/organisation?useSoundex=false&escapeSpecialCharacters=true&appendWildcard=true&nameOrId=',

				source:ds,

				resultFormatter: function (query, results) {

					return A.map(results, function (result) {           
						var location =(result.raw.address)?result.raw.address.location||{}:{},
								address=location.location||null;
						var type = Y.uspCategory.organisation.organisationType.category.codedEntries[result.raw.type];
						var subType = Y.uspCategory.organisation.organisationSubType.category.codedEntries[result.raw.subType];
						var typeSubTypeField = type ? type.name + ((subType)?' - '+ subType.name:'') : '';
						


						var rRow=	'<div title="'+E.html(result.raw.name)+'">'+
						'<div>' +'<span>'+E.html(result.raw.organisationIdentifier)+' - '+E.html(result.raw.name)+'</span>'+'</div>' +
						((address!==null)?('<div class="txt-color fl"><span> '+ E.html(address)+'</span></div>'):'')+
						((type!==null)?('<div class="txt-color fl"><span>'+ E.html(typeSubTypeField)+'</span></div>'):'')+
						Y.app.search.organisation.RenderMatchingField(result.raw,query)+
						'</div>';
						return rRow;

					});
				}
			});

			//add the skin css to the container
			input.ancestor('div').addClass("yui3-skin-sam");    

			input.ac.on('select', function(e){
	        	// Hmm not sure if this is the best way to check for unselectable class 
	   
	        	var relatedOrganisationSearchNode = this.getInput('relatedOrganisationSearch'),
	        		relatedOrganisationItemTemplate = '<li class="data-item" id="relatedOrganisation{organisationId}"><span>{name} ({organisationId})</span><a href="#none" class="remove small">Change</a></li>',
	        		result = e.result,
	        		rawResult = result.raw,
	        		relatedOrganisationList = Y.one('#relatedOrganisationList');
	        	
	        	e.preventDefault();
	        	
	            
	            // Make a note of the gender type for any gendered roles to display
	            this.setAttrs({ 
	            	relatedOrganisationId: rawResult.id,
	            	relatedOrganisationName: result.raw.name,
	            	selectedOrganisation: rawResult.id,
            	},{ silent: true});
	            
	            
	            //clear value and hide
	            relatedOrganisationSearchNode.set('value','');
	            relatedOrganisationSearchNode.select();
	            relatedOrganisationSearchNode.ac.hide();
	            relatedOrganisationSearch.hide();
	            
	            // for now we are only going to allow add one relationship at a time, 
	            // so if a organisation has already been selected, then remove them
	            relatedOrganisationList.one('ul').all('li').some(function(o){
	        	   o.remove(true);
	        	});

	            relatedOrganisationList.one('ul').append(L.sub(relatedOrganisationItemTemplate,{
	            	organisationId: E.html(rawResult.organisationIdentifier), 
	            	name: E.html(result.raw.name)
	            }));	            
	            
	            this.getInput('startDate').removeAttribute('disabled');
				this.getInput('startDateEstimated').removeAttribute('disabled');
				this.getLabel('startDate').removeClass('label-disabled');
				this.startDateCalendar.enableButton();
								
				// update AttributesPanel with related organisation
	            this.attributesPanel && this.attributesPanel.set('relatedEntity', {
	            	entityType: 'organisation',
	            	id: rawResult.id,
	            	name: result.raw.name	            	
	            });
	            
	            this.attributesPanel.enablePanel();
				
				
	        }, this); 
			
			   // we check returned results dispalying an info message if none are returned 
	        relatedOrganisationSearch.ac.on('results', function (e) {

	        	var selectedNode = this.get('container').one('#relatedOrganisationList');
	        	
	        	if(e.results.length === 0) {
	        		this.getWrapper('noSearchResults').show();
	        		selectedNode.hide();
	        	} else {
	        		this.getWrapper('noSearchResults').hide();
	        		selectedNode.show();
	        	}
	        }, this);
			
		},
		changeRelatedOrganisation: function(e){
		  	
			e.preventDefault(); 	
		  	e.currentTarget.ancestor('li').remove(true);
		  	 //clear value and hide
		  	var relatedOrganisationSearchNode = this.getInput('relatedOrganisationSearch')
            relatedOrganisationSearchNode.set('value','');
		  	relatedOrganisationSearchNode.select();
		  	relatedOrganisationSearchNode.ac.hide();
		  	this.getInput('relatedOrganisationSearch').set('value','');
		  	this.getInput('relatedOrganisationSearch').show();
		  	this.startDateCalendar.disableButton();
		  	this.getInput('startDate').set('value','').setAttribute('disabled','disabled');
		  	this.getInput('startDateEstimated').set('checked', '').setAttribute('disabled', 'disabled');
		  	this.getLabel('startDate').addClass('label-disabled');
		  	this.attributesPanel && this.attributesPanel.set('relationshipRole', '');
		  	this.attributesPanel.disablePanel();
		  	
		}
	
	});
	
}, '0.0.1', {
	  requires: ['yui-base',
	             'popup-autocomplete-plugin',
	             'datasource-io', 
	     		 'datasource-jsonschema',
	             'form-util',
	             'handlebars',
	             'handlebars-helpers',
	             'handlebars-relationship-templates',	             
	             'usp-relationship-PersonOrganisationRelationship',
	             'usp-relationship-NewPersonOrganisationRelationship',
	             'usp-relationship-UpdateClosePersonOrganisationRelationship',
	             'usp-relationship-UpdatePersonOrganisationRelationship',	             
	     		 'categories-organisation-component-OrganisationType',	              
	     		 'categories-organisation-component-OrganisationSubType',
	     		 'categories-relationship-component-PersonOrganisationRelationshipClosureReason',
	     		 'organisation-name-matcher-highlighter',
	     		 'calendar-popup',
	     		 'app-mixin-multi-view',
	     		 'relationships-attributes-panel'
	     		 ]
	});