YUI.add('remove-dialogs', function(Y) {
    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.remove').RemoveView = Y.Base.create('removeView', Y.usp.View, [], {
        template: Y.Handlebars.templates.removeDialog
    });

    Y.namespace('app.remove').RemoveInProgressView = Y.Base.create('removeInProgressView', Y.usp.View, [], {
        template: Y.Handlebars.templates.removeInProgressDialog
    });

    Y.namespace('app.remove').RemoveDialog = Y.Base.create('removeDialog', Y.usp.app.AppDialog, [], {
        handlePrepareRemove: function () {
            var view = this.get('activeView'),
                model = view.get('model');

            //bind an error handler on the  model
            model.on('error', function(e) {
                var error = e.error || {};
                if (error.code === 409) {
                    //stop error bubble
                    e.halt(true)
                    view.fire('removeDialog:removeInProgress');
                }
                //just let the event bubble as normal
                this.hideDialogMask();
            }, this);

            this.showDialogMask();

            model.save({}, Y.bind(function (err) {
                if (err === null) {
                	var views = this.views;
                	// If this is remove careLeaver and has associated warnings attribute populated then amend deleteConfirmation message.
                	if (view.get('otherData').type === 'careLeaver' && model.get('warnings').length > 0) {
                		views.remove.labels.deleteConfirmation = views.remove.labels.deleteConfirmationWithWarnings;
                	}	
                    this.hideDialogMask();
                    //model has id of newly created deletion request
                    view.fire('removeDialog:removeRecord', {
                        deletionRequestId: model.get('id')
                    });
                }
            }, this));
        },
        handleRemoveCancel: function (e) {
            var view = this.get('activeView'),
                model = view.get('model'),
                resetUrl = view.get('resetUrl');

            new Y.usp.Model({
              url: L.sub(resetUrl, { deletionRequestId: model.get('id') }),
              id: model.get('id')
            }).save(function (err, res) {
                this.handleCancel(e);
            }.bind(this));
        },
        handleRemoveOK: function (e) {
            this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', true);
            this.handleRemove(e);
        },
        views: {
            prepareRemove: {
                type: Y.app.remove.RemoveView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: 'handlePrepareRemove',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleCancel'
                }]
            },
            remove: {
                type: Y.app.remove.RemoveView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes, remove',
                    action: 'handleRemoveOK',
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary: true,
                    action: 'handleRemoveCancel'
                }]
            },
            removeInProgress: {
                type: Y.app.remove.RemoveInProgressView
            }
        }
    });

}, '0.0.1', {
    requires: [ 'yui-base',
        'app-dialog',
        'view',
        'handlebars',
        'handlebars-helpers',
        'handlebars-deletion-templates',
        'usp-deletion-DeletionRequest',
        'usp-deletion-NewDeletionRequest'
    ]
});
