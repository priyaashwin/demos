YUI.add('output-document-dialog-views', function (Y) {
    var L = Y.Lang,
        O = Y.Object,
        A = Y.Array;

    var NO_OUTPUT_TEMPLATES = '\
      <div class="pure-alert pure-alert-block pure-alert-warning">\
        <span>There are no output templates available for this form type.</span>\
      </div>';

    Y.namespace('app.output').BaseOutputDocumentView = Y.Base.create('baseOutputDocumentView', Y.View, [], {
        template: Y.Handlebars.templates['produceDocumentDialog'],
        events: {
            'select[name="availableTemplates"]': {
                change: '_selectedTemplateChange'
            },
            'select[name="category"]': {
                change: '_selectedCategoryChange'
            },
            '#outputFormat': {
                change: '_outputFormatChange'
            },
        },
        initializer: function () {
            this.after('outputTemplatesChange', this.render, this);
            this.after('selectedCategoryChange', this.handleSelectedCategoryChange, this);
            this.after('selectedTemplateChange', this.handleSelectedTemplateChange, this);
            this.after('selectedFormatChange', this.handleSelectedFormatChange, this);

            // Method for loading document templates that can be optionally overridden
            this.loadTemplates();
        },
        render: function () {
            var container = this.get('container'),
                html = this.template({
                    modelData: this.get('model') ? this.get('model').toJSON() : {},
                    otherData: this.get('otherData'),
                    labels: this.get('labels'),
                    message: this.get('message'),
                }),
                outputTemplates = this.get('outputTemplates'),
                wrapper;

            // Render this view's HTML into the container element.
            container.setHTML(html);

            //lookup select by ID
            wrapper = container.one('#templateWrapper');
            if (wrapper) {
                if (outputTemplates.length < 1) {
                    //No output template available
                    //remove select - replacing with message
                    wrapper.setHTML(NO_OUTPUT_TEMPLATES);
                } else {
                    this._initOutputCategories();
                    this._initOutputTemplates();
                }
            }

            this._align();

            return this;
        },
        loadTemplates: function () {
            //load available templates via promise
            this._getOutputTemplates().then(function (outputTemplates) {

                //get the list of output templates out of the model
                var templates = outputTemplates.toJSON().filter(function (template) {
                    return template.status == 'PUBLISHED';
                });

                //set the outputTemplates attribute - this will trigger the listener
                this.set('outputTemplates', templates);
            }.bind(this), function () {
                //on error - just set to blank
                this.set('outputTemplates', []);
            });
        },
        _getOutputTemplates: function () {
            //output templates model
            var model = new Y.usp.output.OutputTemplateList({
                url: this.get('templateListURL')
            });
            return new Y.Promise(function (resolve, reject) {
                var data = model.load(function (err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    }
                    //success, so resolve the promise
                    resolve(data);
                });
            });
        },
        _selectedTemplateChange: function (e) {
            //set the selected template
            this.set('selectedTemplate', e.currentTarget.get('value'));
        },
        _outputFormatChange: function (e) {
            //set the selected format
            this.set('selectedFormat', e.currentTarget.get('value'));
        },
        _selectedCategoryChange: function (e) {
            this.set('selectedCategory', e.currentTarget.get('value'));
        },
        _align: function () {
            //just drop out of this thread to fire the align
            Y.later(5, this, function () {
                this.fire('align');
            });
        },
        handleSelectedCategoryChange: function (e) {
            //set the available output templates
            this._initOutputTemplates();

            //clear the current selection
            this.set('selectedTemplate', null);
        },
        handleSelectedTemplateChange: function (e) {
            var templateId = e.newVal,
                tNum,
                template,
                formats = [],
                currentSelectedFormat = this.get('selectedFormat'),
                select,
                container = this.get('container');

            if (templateId) {
                //convert to numeric value
                tNum = Number(templateId);

                //lookup the formats select version
                select = container.one('#outputFormat');

                //get the selected template
                template = A.find((this.get('outputTemplates') || []), function (t) {
                    //check for matching id
                    return t.id === tNum;
                });

                if (template) {
                    //load the formats from the template
                    formats = template.supportedFormats || [];
                }

                Y.FUtil.setSelectOptions(select, formats.map(function (f) {
                    return ({
                        name: f,
                        id: f
                    });
                }), null, null, true, true);

                //if the currently selected supported format is not in the list - reset
                if (!A.find(formats, function (v) {
                        return v === currentSelectedFormat;
                    })) {
                    this.set('selectedFormat', null);
                }

                //ensure the selection is made
                select.set('value', this.get('selectedFormat'));
            }

            this._setButtonState();
        },
        _initOutputCategories: function () {
            var container = this.get('container'),
                labels = this.get('labels'),
                outputTemplates = this.get('outputTemplates') || [],
                select = container.one('select[name="category"]');

            if (!select) {
                return;
            }

            var outputCategory = Y.uspCategory.output.outputCategory.category;

            var categoryCodes = {};

            //include coded entry for each output template
            outputTemplates.forEach(function (template) {
                if (template.category && outputCategory.codedEntries[template.category]) {
                    categoryCodes[template.category] = outputCategory.codedEntries[template.category];
                }
            }.bind(this));

            var usedCategoryCodes = O.values(categoryCodes).sort(function (a, b) {
                //name sort
                return a.name.localeCompare(b.name, 'en', {
                    'sensitivity': 'base'
                });
            });
            //if no categories - don't show the select
            if (usedCategoryCodes.length === 0) {
                var parent = select.ancestor('div', false, 'form');
                if (parent) {
                    parent.hide();
                }
            } else {
                Y.FUtil.setSelectOptions(select, usedCategoryCodes, null, null, labels.outputCategoryPrompt, true, 'code');
            }
        },
        _initOutputTemplates: function () {
            var container = this.get('container'),
                select = container.one('select[name="availableTemplates"]'),
                selectedCategory = this.get('selectedCategory') || '',
                outputTemplates = this.get('outputTemplates') || [],
                formatSelect;

            if (select) {
                //filter by output template
                outputTemplates = outputTemplates.filter(function (template) {
                    return !selectedCategory || template.category === selectedCategory;
                }).map(function (template) {
                    return ({
                        name: template.outputName,
                        id: template.id
                    });
                }).sort(function (a, b) {
                    return a.name.localeCompare(b.name, 'en', {
                        'sensitivity': 'base'
                    });
                });
                Y.FUtil.setSelectOptions(select, outputTemplates, null, null, true, true);

                formatSelect = container.one('#outputFormat');
                if (formatSelect) {
                    //just ensure the selected format select is clear
                    Y.FUtil.setSelectOptions(container.one('#outputFormat'), [], null, null, true, true);
                }
            }
        },
        handleSelectedFormatChange: function () {
            this._setButtonState();
        },
        _setButtonState: function () {
            var dialog = this.get('dialog'),
                generateButton,
                currentSelectedTemplate = this.get('selectedTemplate'),
                currentSelectedFormat = this.get('selectedFormat');

            //enable/disable dialog buttons
            if (dialog) {
                //set generate button state based on selected template validity
                generateButton = dialog.getButton('generateButton', Y.WidgetStdMod.FOOTER);
                if (generateButton) {
                    if (currentSelectedTemplate && currentSelectedFormat) {
                        //enable
                        generateButton.set('disabled', false);
                    } else {
                        //disable
                        generateButton.set('disabled', true);
                    }
                }
            }
        },
        getDocumentRequestModel: function () {
            var model = this.get('model');

            return new Y.usp.output.NewDocumentRequest({
                url: this.get('generateURL'),
                documentType: this.get('documentType'),
                subjectId: Number(model.get('subjectId')),
                subjectType: model.get('subjectType').toUpperCase(),
                outputTemplateId: Number(this.get('selectedTemplate')),
                outputFormat: this.get('selectedFormat')
            });

        },
        /**
         * Does the work of generating the document request
         */
        generate: function (dialog) {
            var generateButton, model;
            //disable the generate button
            generateButton = dialog.getButton('generateButton', Y.WidgetStdMod.FOOTER);

            if (generateButton) {
                //disable
                generateButton.set('disabled', true);
            }

            //create a new instance of NewDocumentRequest
            model = this.getDocumentRequestModel();

            model.addTarget(this); //Add as event target

            //call the save on the model
            model.save(Y.bind(function (err, response) {
                var message, messageType;
                //remove event target
                model.removeTarget(this);

                message = err ? this.get('failureMessage') : this.get('successMessage');
                messageType = err ? 'error' : 'success';

                Y.fire('infoMessage:message', {
                    message: message,
                    type: messageType
                });
            }, this));
        },
        /**
         * Override this to provide the specific Model necessary for document request generation
         */
        getGenerateModel: function () {
            return new Y.Model();
        }
    }, {
        ATTRS: {
            outputTemplates: {
                value: []
            },
            selectedCategoryChange: {
                value: null
            },
            selectedTemplate: {
                value: null
            },
            selectedFormat: {
                value: null
            },
            dialog: {
                value: null
            },
            generateURL: {
                value: ''
            },
            documentType: {
                value: null
            },
            successMessage: {
                value: ''
            },
            failureMessage: {
                value: ''
            }
        }
    });
    //OutputTemplateDialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app.output').OutputDocumentDialog = Y.Base.create('outputDocumentDialog', Y.usp.app.AppDialog, [], {
        views: {
            produceDocument: {
                //Mix in the appropriate view type here -this should be an extension of baseOutputDocumentView
                //type:
                buttons: [{
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'generateButton',
                    labelHTML: '<i class="fa fa-check"></i> Generate',
                    classNames: 'pure-button-primary',
                    action: function (e) {
                        //stop the default click event
                        e.preventDefault();
                        //now delegate to the active view to create the document request
                        this.get('activeView').generate(this);
                        //close dialog
                        this.hide();
                    },
                    disabled: true
                }]
            }
        },
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'info-message',
        'form-util',
        'gallery-busyoverlay',
        'yui-later',
        'handlebars-helpers',
        'handlebars-output-templates',
        'categories-output-component-OutputCategory',
        'usp-output-OutputTemplate',
        'usp-output-NewDocumentRequest'
    ]
});