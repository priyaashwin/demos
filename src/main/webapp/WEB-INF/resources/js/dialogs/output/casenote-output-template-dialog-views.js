YUI.add('casenote-output-template-dialog-views', function(Y) {
	//Mix in the ModelFormLink to the NewOutputTemplateWithVersion model
    Y.namespace('app.admin.output').NewCaseNoteOutputTemplateForm = Y.Base.create('newCaseNoteOutputTemplateForm',  Y.usp.output.NewOutputTemplateWithVersion,  [Y.usp.ModelFormLink],{
		form:'#outputTemplateAddForm',
	},{
		ATTRS:{
			outputTemplateStatus:{
				value:'DRAFT'
			},
			//set the context for this template
			context:{
				value:'CASENOTES'
			},
			//A default value for content type to prevent validation errors when the user makes no file selection
			contentType:{
				value:'application/octet-stream'
			}
		}
	});
	
	//Mix in the ModelFormLink to the OutputTemplate model and transmogrify
    Y.namespace('app.admin.output').CaseNoteOutputTemplateForm = Y.Base.create('caseNoteOutputTemplateForm',  Y.usp.output.OutputTemplate,  [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
		form:'#outputTemplateUpdateForm',
		transmogrify: Y.usp.output.UpdateOutputTemplate
	},{
		ATTRS:{			
			//set the context for this template
			context:{
				value:'CASENOTES'
			},
            supportedFormats:{
            	getter:function(val) {
            		return val||[];
                }
            }
		}
	});
    
    Y.namespace('app.admin.output').CaseNoteOutputTemplateAddView = Y.Base.create('caseNoteOutputTemplateAddView', Y.app.admin.output.BaseOutputTemplateAddView, [], {
    	template: Y.Handlebars.templates["standardOutputTemplateAddDialog"],    	
    });
    
    Y.namespace('app.admin.output').CaseNoteOutputTemplateViewView = Y.Base.create('caseNoteOutputTemplateViewView', Y.app.admin.output.BaseOutputTemplateViewView, [], {
    	template: Y.Handlebars.templates["standardOutputTemplateViewDialog"]    	    	  
    });    
    
    Y.namespace('app.admin.output').CaseNoteOutputTemplateUpdateView = Y.Base.create('caseNoteOutputTemplateUpdateView', Y.app.admin.output.BaseOutputTemplateUpdateView, [], {
    	template: Y.Handlebars.templates["standardOutputTemplateUpdateDialog"]    	    	  
    });     
  
    Y.namespace('app.admin.output').CaseNoteOutputTemplateGenerateView = Y.Base.create('caseNoteOutputTemplateGenerateView', Y.app.admin.output.BaseOutputTemplateGenerateView, [], {
    	generate:function(dialog){
    		var url=this.get('url')
    		if(url){
    			//hide dialog
    			dialog.hide();
    			
    			window.location=url;	
    		}    		
    	}
    },{
    	ATTRS:{
    		url:{
    			value:''
    		}
    	}
    });    
    
}, '0.0.1', {
    requires: [
               'model-form-link',
               'output-template-dialog-views',
               'handlebars-helpers',
               'handlebars-output-templates',
               'usp-output-NewOutputTemplateWithVersion',
               'usp-output-OutputTemplate',
               'usp-output-UpdateOutputTemplate'
               ]
});