YUI.add('chronology-output-template-dialog-views', function(Y) {
	//Mix in the ModelFormLink to the NewOutputTemplateWithVersion model
    Y.namespace('app.admin.output').NewChronologyOutputTemplateForm = Y.Base.create('newChronologyOutputTemplateForm',  Y.usp.output.NewOutputTemplateWithVersion,  [Y.usp.ModelFormLink],{
		form:'#outputTemplateAddForm',
	},{
		ATTRS:{
			outputTemplateStatus:{
				value:'DRAFT'
			},
			//set the context for this template
			context:{
				value:'CHRONOLOGY'
			},
			//A default value for content type to prevent validation errors when the user makes no file selection
			contentType:{
				value:'application/octet-stream'
			}
		}
	});
	
	//Mix in the ModelFormLink to the OutputTemplate model and transmogrify
    Y.namespace('app.admin.output').ChronologyOutputTemplateForm = Y.Base.create('chronologyOutputTemplateForm',  Y.usp.output.OutputTemplate,  [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
		form:'#outputTemplateUpdateForm',
		transmogrify: Y.usp.output.UpdateOutputTemplate
	},{
		ATTRS:{			
			//set the context for this template
			context:{
				value:'CHRONOLOGY'
			},
            supportedFormats:{
            	getter:function(val) {
            		return val||[];
                }
            }
		}
	});
    
    Y.namespace('app.admin.output').ChronologyOutputTemplateAddView = Y.Base.create('chronologyOutputTemplateAddView', Y.app.admin.output.BaseOutputTemplateAddView, [], {
    	template: Y.Handlebars.templates["standardOutputTemplateAddDialog"],    	
    });
    
    Y.namespace('app.admin.output').ChronologyOutputTemplateViewView = Y.Base.create('chronologyOutputTemplateViewView', Y.app.admin.output.BaseOutputTemplateViewView, [], {
    	template: Y.Handlebars.templates["standardOutputTemplateViewDialog"]    	    	  
    });    
    
    Y.namespace('app.admin.output').ChronologyOutputTemplateUpdateView = Y.Base.create('chronologyOutputTemplateUpdateView', Y.app.admin.output.BaseOutputTemplateUpdateView, [], {
    	template: Y.Handlebars.templates["standardOutputTemplateUpdateDialog"]    	    	  
    });     
  
    Y.namespace('app.admin.output').ChronologyOutputTemplateGenerateView = Y.Base.create('chronologyOutputTemplateGenerateView', Y.app.admin.output.BaseOutputTemplateGenerateView, [], {
    	generate:function(dialog){
    		var url=this.get('url')
    		if(url){
    			//hide dialog
    			dialog.hide();
    			
    			window.location=url;	
    		}    		
    	}
    },{
    	ATTRS:{
    		url:{
    			value:''
    		}
    	}
    });    
    
}, '0.0.1', {
    requires: [
               'model-form-link',
               'output-template-dialog-views',
               'handlebars-helpers',
               'handlebars-output-templates',
               'usp-output-NewOutputTemplateWithVersion',
               'usp-output-OutputTemplate',
               'usp-output-UpdateOutputTemplate'
               ]
});