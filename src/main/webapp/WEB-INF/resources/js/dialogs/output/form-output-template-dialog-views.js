YUI.add('form-output-template-dialog-views', function(Y) {
    var O = Y.Object,
        A = Y.Array,
        L = Y.Lang;
    //Mix in the ModelFormLink to the NewFormOutputTemplateWithVersion model
    Y.namespace('app.admin.output').NewFormOutputTemplateForm = Y.Base.create('newFormOutputTemplateForm', Y.usp.form.NewFormOutputTemplateWithVersion, [Y.usp.ModelFormLink], {
        form: '#outputTemplateAddForm',
    }, {
        ATTRS: {
            outputTemplateStatus: {
                value: 'DRAFT'
            },
            //set the context for this template
            context: {
                value: 'FORMS'
            },
            //A default value for content type to prevent validation errors when the user makes no file selection
            contentType: {
                value: 'application/octet-stream'
            }
        }
    });
    //Mix in the ModelFormLink to the OutputTemplate model and transmogrify
    Y.namespace('app.admin.output').FormOutputTemplateForm = Y.Base.create('formOutputTemplateForm', Y.usp.form.FormOutputTemplate, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#outputTemplateUpdateForm',
        transmogrify: Y.usp.form.UpdateFormOutputTemplate
    }, {
        ATTRS: {
            //set the context for this template
            context: {
                value: 'FORMS'
            },
            formDefinitionIds:{
            	value:[]
            },
            supportedFormats:{
            	getter:function(val) {
            		return val||[];
                }
            }
        }
    });
    Y.namespace('app.admin.output').FormOutputTemplateAddView = Y.Base.create('formOutputTemplateAddView', Y.app.admin.output.BaseOutputTemplateAddView, [], {
        template: Y.Handlebars.templates["formOutputTemplateAddDialog"],
        events: {
            '#formDefinitions-input': {
                change: '_onFormDefinitionSelectionChange'
            }
        },
        initializer: function() {
            //merge events defined on this class with those on the superclass
            this.events = Y.merge(this.events, Y.app.admin.output.FormOutputTemplateAddView.superclass.events);
            //setup event handlers
            this._changeEvts = [
                        this.after('formDefinitionMapChange', this._setFormDefinitionSelect, this)
                    ];
            //start a promise to set the form definitions
            this._getFormDefinitions().then(function(types) {
                //construct a map by formDefinitionId (GUID)
                var definitionMap = {};
                types.toArray().forEach(function(definition) {
                    var definitionId = definition.get('formDefinitionId');
                    //push into our map
                    definitionMap[definitionId] = definitionMap[definitionId] ? definitionMap[definitionId].concat(definition) : [definition];
                });
                
                //sort the definition map - reverse by version
                O.keys(definitionMap).forEach(function(definitionId){
                	 definitionMap[definitionId].sort(function(a,b){
                		 return (b.get('version')||0)-(a.get('version')||0);
                	 });
                });
                
                //set the form definition map attribute - this will trigger the listener
                this.set('formDefinitionMap', definitionMap);
            }.bind(this));
        },
        destructor: function() {
            //unbind event handles
            if (this._changeEvts) {
                A.each(this._changeEvts, function(item) {
                    item.detach();
                });
                //null out
                this._changeEvts = null;
            }
        },
        render: function() {
            //call into superclass to render
            Y.app.admin.output.FormOutputTemplateAddView.superclass.render.call(this);
            this._setFormDefinitionSelect();
            return this;
        },
        _setFormDefinitionSelect: function() {
            var container = this.get('container'),
                formDefinitionMap = this.get('formDefinitionMap');
            //may not be rendered or may not have the map yet
            if (formDefinitionMap && container) {
                //lookup the form definition select from the container
                var formDefinitionsSelect = container.one('#formDefinitions-input');
                if (formDefinitionsSelect) {
                    //convert types to key : value                
                    var formDefinitions = O.keys(formDefinitionMap).map(function(key) {
                        var defn = formDefinitionMap[key][0];
                        return ({
                            version: defn.get('version'),
                            name: defn.get('name'),
                            id: defn.get('formDefinitionId')
                        });
                    }).sort(function(a, b) {
                        return a.name.localeCompare(b.name, 'en', {
                            'sensitivity': 'base'
                        });
                    });
                    Y.FUtil.setSelectOptions(formDefinitionsSelect, formDefinitions, null, null, true, true);
                }
            }
        },
        _getFormDefinitions: function() {
            var model = new Y.usp.form.FormDefinitionList({
                url: this.get('formDefinitionListURL')
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    }
                    //success, so resolve the promise
                    resolve(data);
                });
            });
        },
        _onFormDefinitionSelectionChange: function(e) {
            var value = e.currentTarget.get('value'),
                formDefinitionMap = this.get('formDefinitionMap') || {},
                versions = [],
                container = this.get('container');
            //clear selected form definition ids in the model
            this.get('model').set('formDefinitionIds', []);
            //lookup the form definition select from the container
            var formDefinitionIdsSelect = container.one('#formDefinitionIds-input');
            if (formDefinitionIdsSelect) {
                var versions = (formDefinitionMap[value] || []).map(function(defn) {
                    return ({
                        version: defn.get('version'),
                        name: 'Version: ' + defn.get('version'),
                        id: defn.get('id')
                    });
                }).sort(function(a, b) {
                    //version sort
                    return (b.version || 0) - (a.version || 0);
                });
                Y.FUtil.setSelectOptions(formDefinitionIdsSelect, versions, null, null, false, true);
            }
        }
    }, {
        ATTRS: {
            /**
             * @attribute formDefinitionListURL
             * @description URL to get possible form definitions to populate list
             * @default null
             * @type string
             */
            formDefinitionListURL: {
                value: null
            },
            /**
             * @attribute formDefinitionMap
             * @description A map of formDefinitionId to formDefinition instances
             * @default null
             * @type object
             */
            formDefinitionMap: {
                value: null
            }
        }
    });
    Y.namespace('app.admin.output').FormOutputTemplateViewView = Y.Base.create('formOutputTemplateViewView', Y.app.admin.output.BaseOutputTemplateViewView, [], {
        template: Y.Handlebars.templates["formOutputTemplateViewDialog"],
        initializer:function(){
        	 var model = this.get('model');
             //after the model is loaded - sort the definitions
             model.after('load', function() {
             	//sort the definitions in the model
             	model.set('formDefinitions', model.get('formDefinitions').sort(function(a, b) {
                     //version sort
                     return (b.version || 0) - (a.version || 0);
                 }));
             }, this);
        }
    });
    Y.namespace('app.admin.output').FormOutputTemplateUpdateView = Y.Base.create('formOutputTemplateUpdateView', Y.app.admin.output.BaseOutputTemplateUpdateView, [], {
        template: Y.Handlebars.templates["formOutputTemplateUpdateDialog"],
        initializer: function() {
            var model = this.get('model');
            //setup event handlers
            this._changeEvts = [
              this.after('formDefinitionVersionsListChange', this._setFormDefinitionVersionsSelect, this)
            ];
            //after the model is loaded - get the version list
            model.after('load', function() {
            	var definitions,                
                	formDefinitionId = null;
                
            	//sort the definitions in the model
            	model.set('formDefinitions', model.get('formDefinitions').sort(function(a, b) {
                    //version sort
                    return (b.version || 0) - (a.version || 0);
                }));
            	
                //lookup the formDefinitionId from the array of definitions - this will be sorted
                definitions = model.get('formDefinitions');                               
                if (definitions.length > 0) {               
                    formDefinitionId = definitions[0].formDefinitionId;
                }
                
                //start a promise to load form definition versions
                this._getFormDefinitionVersions(formDefinitionId).then(function(definitions) {
                    //set the formDefinitionVersionsList attribute - this will trigger the listener
                    this.set('formDefinitionVersionsList', definitions.toJSON().sort(function(a, b) {
	                        //version sort
	                        return (b.version || 0) - (a.version || 0);
	                    })
                    );
                }.bind(this));
            }.bind(this), this);
        },
        destructor: function() {
            //unbind event handles
            if (this._changeEvts) {
                A.each(this._changeEvts, function(item) {
                    item.detach();
                });
                //null out
                this._changeEvts = null;
            }
        },
        _getFormDefinitionVersions: function(formDefinitionId) {
            var model = new Y.usp.form.FormDefinitionList({
                url: L.sub(this.get('formDefinitionVersionsListURL'), {
                    formDefinitionId: formDefinitionId
                })
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    }
                    //success, so resolve the promise
                    resolve(data);
                });
            });
        },
        _setFormDefinitionVersionsSelect: function(e) {
            var container = this.get('container'),
                //lookup the form definition select from the container
                formDefinitionIdsSelect = container.one('#formDefinitionIds-input');
            if (formDefinitionIdsSelect) {
                var formDefinitionVersionsList = this.get('formDefinitionVersionsList'),
                    versions = [];
                if (formDefinitionVersionsList) {
                    versions = formDefinitionVersionsList.map(function(defn) {
                        return ({
                            version: defn.version,
                            name: 'Version: ' + defn.version,
                            id: defn.id
                        });
                    });
                }
                Y.FUtil.setSelectOptions(formDefinitionIdsSelect, versions, null, null, false, true);
                var existingValues=(this.get('model').get('formDefinitions')||[]).map(function(dfn){return dfn.id});
                             
                var select=formDefinitionIdsSelect._node;
                //set default values on select
                for(var i = 0, len = select.options.length; i < len; i++ )
                {
                  if(existingValues.indexOf(Number(select.options[i].value))!==-1){
                	  select.options[i].selected=true;
                  }
                }                
            }
        }
    }, {
        ATTRS: {
            /**
             * @attribute formDefinitionListURL
             * @description URL to get possible form definitions to populate list
             * @default null
             * @type string
             */
            formDefinitionVersionsListURL: {
                value: null
            },
            /**
             * @attribute formDefinitionVersionsList
             * @description A list of form definitions ordered by version
             * @default null
             * @type object
             */
            formDefinitionVersionsList: {
                value: null
            }
        }
    });
    
    Y.namespace('app.admin.output').FormOutputTemplateGenerateView = Y.Base.create('formOutputTemplateGenerateView', Y.app.admin.output.BaseOutputTemplateGenerateView, [], {
    	template: Y.Handlebars.templates["formOutputTemplateGenerateDialog"],
        events: {
            'form': {
                submit: '_preventSubmit'
            }
        },
        _preventSubmit: function(e) {
            //we don't want the default form action for submit
            e.preventDefault();
        },    	
        initializer: function() {
            var model = new Y.usp.form.FormDefinitionList({
                url: this.get('formDefinitionListURL')
            });
            
            //setup event handlers
            this._changeEvts = [
              this.after('formDefinitionsChange', this._setFormDefinitionsSelect, this)
            ];
            
            //start a promise to set the form definitions
            this._getFormDefinitions().then(function(definitions) {                
                //set the form definitions attribute - this will trigger the listener
            	//map to object compatible with FUtil and sort by name and version
                var groupedDefinitions={};
                
                definitions.toArray().forEach(function(definition){
                	var definitionId=definition.get('formDefinitionId'),
	                	item=({
	                		name:definition.get('name')+' (version:'+definition.get('version')+')',
	                		version:definition.get('version'),
	                		definitionName:definition.get('name'),
	                		id:definition.get('id')
	                	});
                	
                	groupedDefinitions[definitionId]=groupedDefinitions[definitionId]?groupedDefinitions[definitionId].concat(item):[item];                	
                });
                
                //sort by version within groups
                O.keys(groupedDefinitions).forEach(function(k){
                	groupedDefinitions[k].sort(function(a,b){
                		return (b.version - a.version);
                    });
                });
                
                var sortedGroupedFormDefinitions=O.keys(groupedDefinitions).map(function(k){
                	return groupedDefinitions[k];
                }).sort(function(a,b){
                	//name sort
                	return a[0].definitionName.localeCompare(b[0].definitionName, 'en', {
                        'sensitivity': 'base'
                    });   
                });
                
                this.set('formDefinitions', sortedGroupedFormDefinitions.reduce(function(a, b) {
                	  return a.concat(b);
                })); 
            }.bind(this));            
        },
        destructor: function() {
            //unbind event handles
            if (this._changeEvts) {
                A.each(this._changeEvts, function(item) {
                    item.detach();
                });
                //null out
                this._changeEvts = null;
            }
        },        
        _getFormDefinitions: function() {
            var model = new Y.usp.form.FormDefinitionList({
                url: this.get('formDefinitionListURL')
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    }
                    //success, so resolve the promise
                    resolve(data);
                });
            });
        },    
        _setFormDefinitionsSelect: function() {
            var container = this.get('container'),
            	formDefinitions = this.get('formDefinitions');
            //may not be rendered or may not have the map yet
            if (formDefinitions && container) {
                //lookup the form definition select from the container
                var formDefinitionsSelect = container.one('#formDefinitionId-input');
                if (formDefinitionsSelect) {
                    Y.FUtil.setSelectOptions(formDefinitionsSelect, formDefinitions, null, null, true, true);
                }
            }
        },        
        render:function(){
            //call into superclass to render
            Y.app.admin.output.FormOutputTemplateGenerateView.superclass.render.call(this);
            this._setFormDefinitionsSelect();
            return this;
        },
        generate:function(dialog){
        	var container = this.get('container'),
        		formDefinitionsSelect = container.one('#formDefinitionId-input');
        	//check we have a selection
        	if(formDefinitionsSelect){
        		var definitionId=formDefinitionsSelect.get('value');
        		
        		if((definitionId!==null||definitionId!==undefined)&&definitionId.trim().length>0){
        			var url=this.get('url')
            		if(url){
            			//hide dialog
            			dialog.hide();
            			
            			window.location=L.sub(url,{
            				formDefinitionId:definitionId
            			});	
            		}    	
        		}else{
        			//fire an error that can be handled by the dialog
        			//error condition
                    this.fire('error',{
                        error:{
                            code: 400,
                            msg: {
                                validationErrors:{'formDefinitionId':'You must select the form type and version before pressing the generate button.'}
                            }
                        }
                    });        			
        		}
        	}
        }
    },{
    	ATTRS:{
            /**
             * @attribute formDefinitionListURL
             * @description URL to get possible form definitions to populate list
             * @default null
             * @type string
             */    		
    		formDefinitionListURL:{
    			value:null
    		},
            /**
             * @attribute formDefinitions
             * @description List of possible form definitions to generate a template for
             * @default []
             * @type array
             */    		    		
    		formDefinitions:{
    			value:[]
    		}
    	}
    });     
}, '0.0.1', {
    requires: [
               'model-form-link',
               'model-transmogrify',
               'output-template-dialog-views',
               'form-util',
               'handlebars-helpers',
               'handlebars-output-templates',
               'usp-form-NewFormOutputTemplateWithVersion',
               'usp-form-UpdateFormOutputTemplate',
               'usp-form-FormOutputTemplate',
               'usp-form-FormDefinition',
               'promise'
               ]
});