YUI.add('output-filters-extension', function (Y) {
    var L = Y.Lang;

    var singletonArrays = [
        'attachmentOtherSource', 'attachmentOtherSourceOrganisation', 'attachmentTitle',
        'otherSource', 'otherSourceOrg', 'hasAttachments'
    ];

    _getSingletonArrayValue = function (key, firstValue) {
        // The filter model represents hasAttachments with three 
        // states using an array that is empty (representing unset) 
        // or contains the String 'true' or 'false'
        if (key === 'hasAttachments') {
            return firstValue === 'true' ? true : false;
        }

        // The remaining arrays contain only a single user-provided 
        // String stored in an array. The back end expects a String, 
        // not an array
        return firstValue;   
    }

    _processArray = function (outputObject, key, array) {
        var filteredArray = array.filter(function (v) { return !!v; });

        if (filteredArray.length > 0) {
            var firstValue = filteredArray[0];

            if (singletonArrays.indexOf(key) !== -1) {
                outputObject[key] = _getSingletonArrayValue(key, firstValue);
            } else {
                outputObject[key] = filteredArray;
            }
        }
        return outputObject;
    }

    // This extension is used to clean empty, null and blank values 
    // retrieved from the filterModel JSON used in the Case Notes 
    // and Chronology output dialogue views 
    function FiltersExtension() {}

    FiltersExtension.prototype = {
        cleanFilterParameters: function (filterParameters, supportedEntries) {
            // The back end expects this field to be named 'groupIdList'
            filterParameters['groupIdList'] = filterParameters['groupIds'];

            // Remove empty, null and blank values from filter object
            return Object.keys(filterParameters).reduce(function (outputObject, key) {
                var value = filterParameters[key];
                if (!!value && supportedEntries.indexOf(key) !== -1) {
                    if (!Array.isArray(value)) {
                        outputObject[key] = value;
                    } else {
                        outputObject = _processArray(outputObject, key, value);
                    }
                }
                return outputObject;
            }, {});
        }
    }

    Y.namespace("app.output").FiltersExtension = FiltersExtension;
}, "0.0.1");