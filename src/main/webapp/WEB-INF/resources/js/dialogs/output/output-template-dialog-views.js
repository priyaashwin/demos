YUI.add('output-template-dialog-views', function(Y) {
    var L = Y.Lang,
        O = Y.Object;
    
    /**
     * Base class for views associated with output templates
     */
    var BaseOutputTemplateView = Y.Base.create('baseOutputTemplateView', Y.View, [], {
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    modelData: this.get('model').toJSON(),
                    otherData: Y.merge(this.get('otherData'), {
                        outputName: this.get('model').get('outputName')
                    }),
                    message: this.get('message'),
                    labels: this.get('labels'),
                    codedEntries: this.get('codedEntries')
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            return this;
        }
    }, {
        ATTRS: {
            model: {},
            otherData: {},
            codedEntries:{
                value:{
                    outputCategory:Y.uspCategory.output.outputCategory.category.codedEntries
                }
            },
            labels: {},
            message: {},
            successMessage: {}
        }
    });
    /**
     * Base class for views which include an update form for output templates
     */
    var BaseOutputTemplateWithUpdateView = Y.Base.create('baseOutputTemplateWithUpdateView', BaseOutputTemplateView, [], {
        events: {
            'form': {
                submit: '_preventSubmit'
            }
        },
        _preventSubmit: function(e) {
            //we don't want the default form action for submit
            e.preventDefault();
        },
        initializer:function(){
            this._baseOutputEvents=[
                Y.Do.after(this.initOutputCategories, this, 'render', this)
            ];
        },
        destructor:function(){
            this._baseOutputEvents.forEach(function(handler) {
                handler.detach();
          });
          delete this._baseOutputEvents;
        },
        initOutputCategories:function(){
            var model = this.get('model'),
                container=this.get('container'),
                outputCategoryCategory=Y.uspCategory.output.outputCategory.category,
                outputCategory = this.lookupExistingEntry(outputCategoryCategory.codedEntries, model.get('category'))||{};
            
            var select=container.one('select[name="category"]');
            
            if(select){
                //set the select options for outputCategory
                Y.FUtil.setSelectOptions(select, O.values(outputCategoryCategory.getActiveCodedEntries()), outputCategory.code, outputCategory.name, true, true, 'code');
                
                //ensure the currenty select value is set
                select.set('value', model.get('category')||'');
            }
        },
        lookupExistingEntry: function(codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        }
    });
    Y.namespace('app.admin.output').BaseOutputTemplateAddView = Y.Base.create('baseOutputTemplateAddView', BaseOutputTemplateWithUpdateView, [], {
    });
    
    Y.namespace('app.admin.output').BaseOutputTemplateUpdateView = Y.Base.create('baseOutputTemplateUpdateView', BaseOutputTemplateWithUpdateView, [], {
        initializer: function() {
            //render on change
            this.get('model').after('change', this.render, this);
        }
    });
    Y.namespace('app.admin.output').BaseOutputTemplateGenerateView = Y.Base.create('baseOutputTemplateGenerateView', Y.View, [], {
        template: Y.Handlebars.templates["outputTemplateGenerateDialog"],
        /**
         * Performs the generation - override this in your context specific generate view
         */
        generate: function() {
            //do nothing
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    modelData: {},
                    otherData: this.get('otherData'),
                    message: this.get('message'),
                    labels: this.get('labels')
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);
            return this;
        }
    });
    Y.namespace('app.admin.output').OutputTemplateStatusChangeView = Y.Base.create('outputTemplateStatusChangeView', BaseOutputTemplateWithUpdateView, [], {
        template: Y.Handlebars.templates["outputTemplateMessageDialog"]
    });
    Y.namespace('app.admin.output').BaseOutputTemplateViewView = Y.Base.create('baseOutputTemplateViewView', BaseOutputTemplateView, [], {
        initializer: function() {
            //render on change
            this.get('model').after('change', this.render, this);
        }
    });
    Y.namespace('app.admin.output').OutputTemplateExportView = Y.Base.create('outputTemplateExportView', BaseOutputTemplateView, [], {
        template: Y.Handlebars.templates["outputTemplateMessageDialog"],
        exportTemplate: function(dialog) {
            var url = this.get('url'),
            	model=this.get('model');
            
            if (url) {
                //hide dialog
                dialog.hide();
                window.location = L.sub(url, {id:model.get('id')});
            }
        }
    });
    
    /**
     * A helper function to get a common "save" style button
     */
    var _getSaveButton = function(label) {
        return ({
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> ' + label,
            action: function(e){
                var activeView=this.get('activeView'),
                    model=activeView.get('model'||{});
                
                var opts={};
                
                //if the url is specified against the view - push it into the model
                if (activeView.get('url')) {
                    //ensure the url is set against the model
                    model.url = activeView.get('url');
                }
                
                if(model && model.transmogrify){
                    opts.transmogrify=model.transmogrify
                }
                return Y.when(this.handleSave(e, {}, opts));
            },
            disabled: true
        });
    };
    
    Y.namespace('app.admin.output').OutputTemplateDialog = Y.Base.create('outputTemplateDialog', Y.usp.app.AppDialog, [], {
        handleSaveWithAttachment:function(e){
            var activeView=this.get('activeView'),
                model=activeView.get('model');

            //mask of the dialog to prevent further changes
            this.showDialogMask();
            
            //we bypass the standard save here as we need to include an attachment
            //override the sync method provided by ModelSyncREST to handle the file upload process
            //Note - we can only handle a SINGLE file upload here            
            model.sync = function(action, options, callback) {
                options || (options = {});
                var templateFile;
                var url = this.getURL(action, options);
                //construct new Blob containing the model data
                var modelDataBlob = new Blob([this.serialize(action)], {
                    type: 'application/json'
                });
                //entity = this.serialize(action);
                templateFile = new Y.FileHTML5({
                    file: activeView.get('container').one(model.form)._node.filename.files[0]
                });
                templateFile.on('uploaderror', function(e) {
                    var originEvent = e.originEvent || {};
                    if (callback) {
                        callback({
                            code: e.status,
                            msg: e.statusText
                        }, originEvent.target);
                    }
                });
                templateFile.on('uploadcomplete', function(e) {
                    var originEvent = e.originEvent || {};
                    if (callback) {
                        callback(null, originEvent.target);
                    }
                });
                var data = {};
                data[this.get('_type')] = modelDataBlob;
                //start the file upload passing the file and our blob
                templateFile.startUpload(url, data, 'file');
            };
            
            return Y.when(this.handleSave(e, {}));
        },
        views: {
            addTemplate: {
                //Mix in the appropriate view type here -this should be an extension of baseOutputTemplateAddView
                //type:
                buttons: [Y.merge(_getSaveButton('Save'), {action:'handleSaveWithAttachment'})]
            },
            viewTemplate: {
                //Mix in the appropriate view type here -this should be an extension of baseOutputTemplateViewView
                //type:
            },
            updateTemplate: {
                //Mix in the appropriate view type here -this should be an extension of baseOutputTemplateUpdateView
                //type:
                buttons: [_getSaveButton('Save')]
            },
            publishTemplate: {
                type: Y.app.admin.output.OutputTemplateStatusChangeView,
                buttons: [_getSaveButton('Publish')]
            },
            archiveTemplate: {
                type: Y.app.admin.output.OutputTemplateStatusChangeView,
                buttons: [_getSaveButton('Delete')]
            },
            removeTemplate: {
                type: Y.app.admin.output.OutputTemplateStatusChangeView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Remove',
                    action: function(e){
                        var activeView=this.get('activeView'),
                        model=activeView.get('model'||{});
                                        
                        //if the url is specified against the view - push it into the model
                        if (activeView.get('url')) {
                            //ensure the url is set against the model
                            model.url = activeView.get('url');
                        }
                    
                        return Y.when(this.handleRemove(e));
                    },
                    disabled: true
                }]
            },
            generateTemplate: {
                type: Y.app.admin.output.OutputTemplateExportView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Generate',
                    action: function(e) {
                        //stop the default click event
                        e.preventDefault();
                        //now delegate to the active view to perform the generate
                        this.get('activeView').generate(this);
                    },
                    disabled: true
                }]
            },
            exportTemplate: {
                //Mix in the appropriate view type here -this should be an extension of baseOutputTemplateExportView
                //type:
                type: Y.app.admin.output.OutputTemplateExportView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Export',
                    action: function(e) {
                        //stop the default click event
                        e.preventDefault();
                        //now delegate to the active view to perform the export
                        this.get('activeView').exportTemplate(this);
                    },
                    disabled: true
                }]
            }
        }
    }, {
        ATTRS: {
            width: {
                value: 600
            }
        }
    });
}, '0.0.1', {
    requires: [
               'yui-base',
               'view',
               'app-dialog',
               'file-html5',
               'json-parse',
               'handlebars-helpers',
               'handlebars-output-templates',
               'categories-output-component-OutputCategory',
               'form-util'
               ]
});