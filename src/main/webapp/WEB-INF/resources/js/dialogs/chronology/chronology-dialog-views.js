YUI.add('chronology-dialog-views', function(Y) {
    var O = Y.Object;

    var BaseChronologyEntryView = Y.Base.create('baseChronologyEntryView', Y.app.entry.BaseEntryView, [], {}, {
        ATTRS: {
            /**
             * @attribute tabSourceNode
             * @type string
             * @value '#chronologyTabs'
             * @description Holds DOM element id for rendering tabs
             */
            tabSourceNode: {
                value: '#chronologyTabs',
                getter: function(v) {
                    return this.get('container').one(v);
                }
            }
        }
    });

    var ViewChronologyEntryView = Y.Base.create('viewChronologyEntryView', BaseChronologyEntryView, [], {
        template: Y.Handlebars.templates["chronologyEntryDialog"],
        initializer:function(){
          this.sourceView=new Y.app.source.SourceView({
            model: this.get('model'),
            labels: this.get('labels')
          });
        },
        render:function(){
          var container=this.get('container');
          //call superclass
          ViewChronologyEntryView.superclass.render.call(this);
          
          //render the source view
          container.one('#chronologyEntry_source').insert(this.sourceView.render().get('container'), "replace");
        },
        destructor:function(){
          this.sourceView.destroy();
          delete this.sourceView;
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    entryTypes: Y.secured.uspCategory.chronology.entryType.category.codedEntries,
                    entrySubTypes: Y.secured.uspCategory.chronology.entrySubType.category.codedEntries
                }
            },
            formId: {
                value: '#chronologyEntryViewForm'
            }
        }
    });

    var NewChronologyEntryView = Y.Base.create('newChronologyEntryView', BaseChronologyEntryView, [], {
        template: Y.Handlebars.templates["chronologyEntryAddDialog"],
        events: {
            '#preSelectDateOptions li a': {
                click: 'preSelectFuzzyDate'
            },
            '#chronologyEntryForm_entryType select': {
                change: '_onEntryTypeChange'
            }
        },
        initializer: function() {
            this.events = Y.merge(this.events, NewChronologyEntryView.superclass.events);
        },
        render: function() {
            var container = this.get('container'),
                dateValue = this.get("model").get("eventDate");
            //call superclass
            NewChronologyEntryView.superclass.render.call(this);

            this.initEntryTypes();

            // render the fuzzy event date widget
            this.fuzzyEventDateWidget = new Y.usp.FuzzyDate({
                id: 'fuzzyEventDate',
                contentNode: this.getNode('fuzzyEventDate'),
                dateValue: dateValue,
                yearInput: true,
                monthInput: true,
                dayInput: true,
                hourInput: true,
                minuteInput: true,
                secondInput: false,
                showDateLabel: false,
                calendarPopup: true,
                cssPrefix: 'pure',
                minYear: 1886,
                maxYear: new Date().getFullYear(),
                labels: this.get('fuzzyDateLabels')
            });
            this.fuzzyEventDateWidget.render();

            if (!dateValue) {
                //Day & month should default to being disabled.
                container.one('#fuzzyEventDateMONTH').set('disabled', true);
                container.one('#fuzzyEventDateDAY').set('disabled', true);
            }
            
            this.initSource();
            return this;
        },
        initSource:function(){
          var container=this.get('container');
          this.sourceForm = new Y.app.source.SourceForm({
            model: this.get('model'),
            labels: this.get('labels'),
            mandatory:true
          });
          
          this.sourceForm.addTarget(this);
          
          container.one('#chronologyEntry_source').setHTML(this.sourceForm.render().get('container'));

        },
        destructor: function() {
            if (this.sourceForm) {
                this.sourceForm.removeTarget(this);

                this.sourceForm.destroy();

                delete this.sourceForm;
            }

            // destroy the fuzzy event date widget
            if (this.fuzzyEventDateWidget) {
                this.fuzzyEventDateWidget.destroy();
                delete this.fuzzyEventDateWidget;
            }
        },
        _onEntryTypeChange: function(e) {
            var selectElement = e.currentTarget._node,
                selectedIndex = selectElement.selectedIndex;
            if (selectedIndex >= 0) {
                this._setSecuredEntrySubTypeOptions('chronology', selectElement.options[selectedIndex].value, null, 'write');
            }
        },
        initEntryTypes: function() {
            Y.FUtil.setSelectOptions(this.getNode('entryTypeSelect'), O.values(Y.secured.uspCategory.chronology.entryType.category.getActiveCodedEntries(null, 'write')), null, null, true, true, 'code');
            Y.FUtil.setSelectOptions(this.getNode('entrySubTypeSelect'), [], null, null, 'Please select a type', true, 'code');
        },
        preSelectFuzzyDate: function(e) {
            e.preventDefault();
            var t = e.currentTarget,
                today = new Date();

            if (t.hasClass('currentDate')) {
                this.fuzzyEventDateWidget.setDateAndMask(today, 'DAY');

            } else if (t.hasClass('currentDateTime')) {
                this.fuzzyEventDateWidget.setDateAndMask(today, 'TIME');
            }
        },

        getFuzzyEventDate: function() {
            return this.fuzzyEventDateWidget.getFuzzyDate();
        },

        getEntry: function() {
            return this.getNode('event');
        },

        getEntryType: function() {
            return this.getNode('entryType');
        }
    }, {
        ATTRS: {
            formId: {
                value: '#chronologyEntryAddForm'
            }
        }
    });


    var EditChronologyEntryView = Y.Base.create('editChronologyEntryView', NewChronologyEntryView, [], {
        template: Y.Handlebars.templates["chronologyEntryEditDraftDialog"],
        initializer: function() {
            this.events = Y.merge(this.events, EditChronologyEntryView.superclass.events);
        },
        initEntryTypes: function() {
            var model = this.get('model'),
                entryType = this.lookupExistingEntry(Y.secured.uspCategory.chronology.entryType.category.codedEntries, model.get('entryType')),
                entrySubType = this.lookupExistingEntry(Y.secured.uspCategory.chronology.entrySubType.category.codedEntries, model.get('entrySubType'));
            Y.FUtil.setSelectOptions(this.getNode('entryTypeSelect'), O.values(Y.secured.uspCategory.chronology.entryType.category.getActiveCodedEntries(null, 'write')), entryType.code, entryType.name, true, true, 'code');

            var parentCode = model.get('entryType');
            this._setSecuredEntrySubTypeOptions('chronology', parentCode, entrySubType, 'write');

            this.getNode('entryTypeSelect').set('value', parentCode);
            this.getNode('entrySubTypeSelect').set('value', model.get('entrySubType'));
        }
    }, {
        ATTRS: {
            formId: {
                value: '#chronologyEntryEditForm'
            }
        }
    });

    var EditCompleteChronologyEntryView = Y.Base.create('editCompleteChronologyEntryView', EditChronologyEntryView, [], {
        template: Y.Handlebars.templates["chronologyEntryEditCompleteDialog"],
        initializer:function(){
          this.sourceView=new Y.app.source.SourceView({
            model: this.get('model'),
            labels: this.get('labels')
          });
        },
        initEntryTypes: function() {
            //Don't allow entry types to be changed
        },
        initSource:function(){
          //render the source view
          this.get('container').one('#chronologyEntry_source').insert(this.sourceView.render().get('container'), "replace");
        },
        destructor:function(){
          this.sourceView.destroy();
          delete this.sourceView;
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    entryTypes: Y.secured.uspCategory.chronology.entryType.category.codedEntries,
                    entrySubTypes: Y.secured.uspCategory.chronology.entrySubType.category.codedEntries
                }
            }
        }
    });


    // HARD DELETE
    var RemoveChronologyEntryView = Y.Base.create('removeChronologyEntryView', Y.usp.chronology.ExtendedChronologyEntryView, [], {
        //bind template 
        template: Y.Handlebars.templates["chronologyEntryRemoveDialog"]

    });
    // SOFT DELETE
    var HideChronologyEntryView = Y.Base.create('hideChronologyEntryView', Y.usp.chronology.ExtendedChronologyEntryView, [], {
        //bind template 
        template: Y.Handlebars.templates["chronologyEntryDeleteDialog"]

    });
    // COMPLETE Chronology Entry
    var CompleteChronologyEntryView = Y.Base.create('completeChronologyEntryView', Y.usp.chronology.ExtendedChronologyEntryView, [], {
        //bind template 
        // this is when Complete is called from result sets action menu
        template: Y.Handlebars.templates["chronologyEntryCompleteDialog"]
    });
    // UNCOMPLETE Chronology Entry
    var UncompleteChronologyEntryView = Y.Base.create('uncompleteChronologyEntryView', Y.usp.chronology.ExtendedChronologyEntryView, [], {
        //bind template 
        template: Y.Handlebars.templates["chronologyEntryUncompleteDialog"]
    });

    Y.namespace('app.chronology').ViewChronologyEntryView = ViewChronologyEntryView;
    Y.namespace('app.chronology').NewChronologyEntryView = NewChronologyEntryView;
    Y.namespace('app.chronology').EditChronologyEntryView = EditChronologyEntryView;
    Y.namespace('app.chronology').EditCompleteChronologyEntryView = EditCompleteChronologyEntryView;
    Y.namespace('app.chronology').HideChronologyEntryView = HideChronologyEntryView;
    Y.namespace('app.chronology').RemoveChronologyEntryView = RemoveChronologyEntryView;
    Y.namespace('app.chronology').CompleteChronologyEntryView = CompleteChronologyEntryView;
    Y.namespace('app.chronology').UncompleteChronologyEntryView = UncompleteChronologyEntryView;

}, '0.0.1', {
    requires: ['yui-base',
        'handlebars-helpers',
        'handlebars-chronology-templates',
        'usp-chronology-NewChronologyEntry',
        'usp-chronology-UpdateChronologyEntry',
        'form-util',
        'fuzzy-date',
        'secured-categories-chronology-component-EntryType',
        'secured-categories-chronology-component-EntrySubType',
        'entry-dialog-views',
        'source-form'
    ]
});