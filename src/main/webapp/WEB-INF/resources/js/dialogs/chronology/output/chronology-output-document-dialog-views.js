YUI.add('chronology-output-document-dialog-views', function (Y) {
    var L = Y.Lang;
    // A list of filter parameters supported by the back end 
    var supportedEntries = [
        'startDate', 'endDate', 'impactList', 'sourceList', 'sourceOrgList', 'pracList',
        'pracOrgList', 'entryTypeList', 'entrySubTypeList', 'excludeGroupEntries', 'startRecordedDate',
        'endRecordedDate', 'startEditedDate', 'endEditedDate', 'statusList', 'groupIdList', 'otherSource',
        'otherSourceOrg'
    ];

    Y.namespace('app').ChronologyOutputDocumentView = Y.Base.create('chronologyOutputDocumentView', Y.app.output.BaseOutputDocumentView, [Y.app.output.FiltersExtension], {
        getDocumentRequestModel: function () {
            var model = this.get('model');
            var filterParameters = this.get('filterModel').toJSON();
            var cleanedFilterParameters = this.cleanFilterParameters(filterParameters, supportedEntries);

            return new Y.usp.chronology.NewChronologyDocumentRequest(Y.merge({
                url: this.get('generateURL'),
                documentType: this.get('documentType'),
                subjectId: Number(model.get('subjectId')),
                subjectType: model.get('subjectType').toUpperCase(),
                outputTemplateId: Number(this.get('selectedTemplate')),
                outputFormat: this.get('selectedFormat')
            }, cleanedFilterParameters));
        }
    });
    
}, '0.0.1', {
    requires: [
        'yui-base',
        'output-document-dialog-views',
        'output-filters-extension',
        'usp-chronology-NewChronologyDocumentRequest'
    ]
});