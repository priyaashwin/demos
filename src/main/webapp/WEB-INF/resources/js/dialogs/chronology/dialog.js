YUI.add('chronology-dialog', function(Y) {
  
  Y.namespace('app.chronology').NewChronologyEntryForm = Y.Base.create("newChronologyEntryForm", Y.usp.chronology.NewChronologyEntry, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify, Y.app.source.FormWithSourceMixin], {
    form: '#chronologyEntryAddForm',
    customValidation:function(attrs){
      var errors={},
          eventDate=attrs.eventDate||{},
          labels=this.labels||{};
      
      //No event date is valid, but if there is one it must be in valid range
      if(eventDate&&(eventDate.year<1886||eventDate.year>(new Date().getFullYear()))){
        errors['fuzzyEventDateYEAR']=this.labels.eventDateValidationErrorMessage;
      }
    
      if ((attrs.event||'').trim()==='') {
        errors['event']=labels.entryValidationErrorMessage;
      }
      if ((attrs.entryType||'')==='') {
        errors['entryType']=labels.entryTypeValidationErrorMessage;
      }
      
      return errors;
    }
  },{
    ATTRS:{
      visibleToAll:{
        //default value
        value:true
      },
      impact:{
        value:'UNKNOWN'
      },
      personVisibility:{
        value:[]
      }
    }
  });

  Y.namespace('app.chronology').UpdateChronologyEntryForm = Y.Base.create("updateChronologyEntryForm", Y.usp.chronology.ChronologyEntryWithVisibility, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify, Y.app.source.FormWithSourceMixin], {
    form: '#chronologyEntryEditForm'
  });

  
  Y.namespace('app.chronology').ChronologyDialog = Y.Base.create('chronologyDialog', Y.app.entry.BaseEntryDialog, [], {
    entryCreateModelClass:Y.usp.chronology.NewChronologyEntry,
    entryCreateWithVisbilityModelClass:Y.usp.chronology.NewChronologyEntryWithVisibility,
    entryUpdateDraftModelClass:Y.usp.chronology.UpdateDraftChronologyEntry,
    entryUpdateCompleteModelClass:Y.usp.chronology.UpdateCompleteChronologyEntry,
    entryUpdateWithVisbilityModelClass:Y.usp.chronology.UpdateChronologyEntryWithVisibility,
    views:{
      add:{
        type: Y.app.chronology.NewChronologyEntryView
      },
      view:{
        type: Y.app.chronology.ViewChronologyEntryView
      },
      edit:{
        type: Y.app.chronology.EditChronologyEntryView
      },
      editComplete:{
        type: Y.app.chronology.EditCompleteChronologyEntryView
      },
      hardDelete:{
        type: Y.app.chronology.RemoveChronologyEntryView
      },
      softDelete:{
        type: Y.app.chronology.HideChronologyEntryView
      },      
      complete:{
        type: Y.app.chronology.CompleteChronologyEntryView
      },      
      uncomplete:{
        type: Y.app.chronology.UncompleteChronologyEntryView
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base', 
             'entry-dialog',
             'chronology-dialog-views',
             'model-form-link',
             'model-transmogrify',
             'usp-chronology-NewChronologyEntry',
             'usp-chronology-NewChronologyEntryWithVisibility',
             'usp-chronology-ChronologyEntryWithVisibility',
             'usp-chronology-NewChronologyEntry',
             'usp-chronology-UpdateDraftChronologyEntry',
             'usp-chronology-UpdateChronologyEntryWithVisibility',
             'usp-chronology-UpdateChronologyEntry',
             'usp-chronology-UpdateCompleteChronologyEntry',
             'source-form'
             ]
});