YUI.add('person-summary-administration-dialog-views', function (Y) {
    var A = Y.Array,
        L = Y.Lang,
        O = Y.Object;

    Y.namespace('app').AddFormConfigurableTypeReferenceForm = Y.Base.create(
        'addFormConfigurableTypeReferenceForm', Y.usp.form.NewFormConfigurableTypeReference, [Y.usp.ModelFormLink], {
            form: '#addFormConfigurableTypeReferenceForm'
        });

    Y.namespace('app').UpdateFormConfigurableTypeReferenceForm = Y.Base.create(
        'updateFormConfigurableTypeReferenceForm', Y.usp.form.UpdateFormConfigurableTypeReference, [Y.usp.ModelFormLink], {
            form: '#updateFormConfigurableTypeReferenceForm'
        });

    Y.namespace('app').FormConfigurableTypeReferenceBaseView = Y.Base.create('FormConfigurableTypeReferenceBaseView', Y.usp.View, [], {
        initializer: function () {
            this._evtHandlers = [
                this.after('formDefinitionListChange', this.handleFormDefinitionListChange, this)
            ];

            var formDefinitionListPromise = new Y.Promise(function (resolve, reject) {
                new Y.usp.form.FormDefinitionList({
                    url: L.sub(this.get('formDefinitionUrl'), {
                        sortBy: encodeURI(JSON.stringify([{
                            name: 'asc'
                        }]))
                    })
                }).load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            Y.Promise.all([formDefinitionListPromise]).then(function (formDefinitions) {
                this.set('formDefinitionList', formDefinitions[0]);
            }.bind(this));
        },
        handleFormDefinitionListChange: function (e) {
            var formDefinitionSelector = this.get('container').one('#formDefinitionId'),
                formattedFormDefinitionList = e.newVal,
                recordFormDefinitionUid = this.get('formDefinitionUid') || '',
                recordFormDefinitionId = '';

            var formDefinitions = formattedFormDefinitionList.map(function (formDefinition) {
                if (formDefinition.formDefinitionId === recordFormDefinitionUid) {
                    recordFormDefinitionId = formDefinition.id;
                }

                return {
                    'name': formDefinition.name,
                    'id': formDefinition.id
                }
            });

            Y.FUtil.setSelectOptions(formDefinitionSelector, formDefinitions, null, 'name', true, false, 'id');
            formDefinitionSelector.set('value', recordFormDefinitionId || '');
        },

        _initTeamPickList: function (teams) {
            var container = this.get('container');
            var model = this.get('model');
            var alreadySelectedTeams = model.get('associatedTeams') || [];

            teams = teams.filter(function (team) {
                var teamAlreadySelected = false;
                A.each(alreadySelectedTeams, function (alreadySelectedTeam) {
                    if (team.name == alreadySelectedTeam.name) {
                        teamAlreadySelected = true;
                        return;
                    }
                });
                return !teamAlreadySelected;
            });

            var order = teams.map(function (team) {
                return team.name;
            }).sort(function (a, b) {
                return (a || '').localeCompare(b || '');
            });

            var teamPicklist = new Y.usp.Picklist({
                optionsMap: {
                    text: 'name',
                    value: 'id',
                    title: 'title'
                },
                options: teams,
                order: order,
                selections: alreadySelectedTeams || {},
                preserveOrder: true,
                stackMode: true,
                selectWidth: '100%',
                actionLabelOne: '>',
                actionLabelAll: '>>',
                actionLabelRmv: '<',
                actionLabelRmvAll: '<<'
            });

            teamPicklist.render();
            container.one('#associatedTeams').appendChild(teamPicklist.get('srcNode'));

            return teamPicklist;
        },

        destructor: function () {
            //destroy event handlers
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }
        }
    }, {
        ATTRS: {
            formDefinitionList: {}
        }
    });

    Y.namespace('app').AddFormConfigurableTypeReferenceView = Y.Base.create('addFormConfigurableTypeReferenceView',
        Y.app.FormConfigurableTypeReferenceBaseView, [], {
            template: Y.Handlebars.templates.addFormConfigurableTypeReferenceViewDialog,
            render: function () {
                Y.app.AddFormConfigurableTypeReferenceView.superclass.render.call(this);

                this.teamPicklist = this._initTeamPickList(this.get('teamList').results);

                return this;
            }
        });

    Y.namespace('app').UpdateFormConfigurableTypeReferenceView = Y.Base.create('updateFormConfigurableTypeReferenceView',
        Y.app.FormConfigurableTypeReferenceBaseView, [], {
            template: Y.Handlebars.templates.updateFormConfigurableTypeReferenceViewDialog,
            render: function () {
                Y.app.UpdateFormConfigurableTypeReferenceView.superclass.render.call(this);

                this.teamPicklist = this._initTeamPickList(this.get('teamList').results);

                return this;
            }
        });

    Y.namespace('app').DeleteFormConfigurableTypeReferenceView = Y.Base.create('deleteFormConfigurableTypeReferenceView',
        Y.View, [], {});

    Y.namespace('app').UpdateCaseNoteConfigurableTypeReferenceForm = Y.Base.create(
        'updateCaseNoteConfigurableTypeReferenceForm', Y.usp.casenote.UpdateCaseNoteConfigurableTypeReference, [Y.usp.ModelFormLink], {
            form: '#updateCaseNoteConfigurableTypeReferenceForm'
        });

    Y.namespace('app').UpdateCaseNoteConfigurableTypeReferenceView = Y.Base.create('updateCaseNoteConfigurableTypeReferenceView',
        Y.usp.casenote.UpdateCaseNoteConfigurableTypeReferenceView, [], {
            template: Y.Handlebars.templates.updateCaseNoteConfigurableTypeReferenceViewDialog,
            events: {
                '#updateCaseNoteConfigurableTypeReferenceForm': {
                    submit: '_preventSubmit'
                }
            },
            render: function () {
                Y.app.UpdateCaseNoteConfigurableTypeReferenceView.superclass.render.call(this);

                Y.FUtil.setSelectOptions(this.get('container').one('#caseNoteEntryType'), O.values(Y.uspCategory.casenote.entryType.category.getActiveCodedEntries()), null, null, true, true, 'code');

                return this;
            }
        });

    Y.namespace('app').PersonSummaryAdministrationDialog = Y.Base.create('personSummaryAdministrationDialog',
        Y.usp.app.AppDialog, [], {
            views: {
                addFormConfigurableTypeReferenceView: {
                    type: Y.app.AddFormConfigurableTypeReferenceView,
                    buttons: [{
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Save',
                        action: function (e) {
                            this._addFormConfiguarbleTypeReference(e);
                        },
                        disabled: true
                    }]
                },

                updateFormConfigurableTypeReferenceView: {
                    type: Y.app.UpdateFormConfigurableTypeReferenceView,
                    buttons: [{
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Save',
                        action: function (e) {
                            this._updateFormConfiguarbleTypeReference(e);
                        },
                        disabled: true
                    }]
                },

                deleteFormConfigurableTypeReferenceView: {
                    type: Y.app.DeleteFormConfigurableTypeReferenceView,
                    buttons: [{
                        name: 'removeButton',
                        labelHTML: '<i class="fa fa-check"></i> Remove',
                        action: function (e) {
                            this._deleteFormConfiguarbleTypeReference(e);
                        },
                        disabled: true
                    }]
                },

                updateCaseNoteConfigurableTypeReferenceView: {
                    type: Y.app.UpdateCaseNoteConfigurableTypeReferenceView,
                    headerTemplate: this.headerTemplate,
                    buttons: [{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Save',
                        classNames: 'pure-button-primary',
                        action: function (e) {
                            this._updateCaseNoteConfiguarbleTypeReference(e);
                        },
                        disabled: true
                    }]
                }
            },

            _addFormConfiguarbleTypeReference: function (e) {
                var view = this.get('activeView');

                this.handleSave(e, {
                    associatedTeams: view.teamPicklist.getSelectedValues()
                });
            },

            _updateFormConfiguarbleTypeReference: function (e) {
                var view = this.get('activeView');

                this.handleSave(e, {
                    associatedTeams: view.teamPicklist.getSelectedValues()
                });
            },

            _deleteFormConfiguarbleTypeReference: function (e) {
                this.handleRemove(e);
            },

            _updateCaseNoteConfiguarbleTypeReference: function (e) {
                var view = this.get('activeView'),
                    model = view.get('model');

                model.save(Y.bind(function (err) {
                    this.hideDialogMask();
                    if (err === null) {
                        this.hide();
                        var successMessage = this.get('activeView').get('labels').successMessage;
                        Y.fire('caseNoteConfigurableTypeReference:saved', {
                            successMessage: successMessage
                        });
                    }
                }, this));
            }
        });
}, '0.0.1', {
    requires: [
        'picklist',
        'form-util',
        'usp-form-UpdateFormConfigurableTypeReference',
        'usp-form-NewFormConfigurableTypeReference',
        'usp-casenote-UpdateCaseNoteConfigurableTypeReference',
        'handlebars-helpers',
        'handlebars-person-summary-admin-templates',
        'usp-form-FormDefinition',
        'app-dialog',
        'categories-casenote-component-EntryType'
    ]
});