var version = '0.0.1',
  requiredModules = [
    'yui-base',
    'app-dialog',
    'model-form-link',
    'model-transmogrify',
    'form-util',
    'handlebars-calendar-templates',
    'handlebars-calendar-partials',
    'calendar-component-enumerations',
    'usp-date',
    'usp-calendar-NewWorkingDay',
    'usp-calendar-UpdateWorkingDay',
    'usp-calendar-WorkingDay'
  ],
  options = {
    requires: requiredModules
  };

YUI.add('calendar-days-dialog-views', function (Y) {
  var F = Y.FUtil,
    D = Y.USPDate,
    O = Y.Object;
  
  var calendarDaysAddView = Y.Base.create(
    'calendarDaysAddView', 
    Y.usp.calendar.NewWorkingDayView,
    [],
    {
      template: Y.Handlebars.templates["calendarDaysAddDialog"],
      events: {
        '#calendarDaysAddForm': {submit: '_preventSubmit'}
      },
      _preventSubmit: function(e) {
        e.preventDefault();
      },
      _compareTime: function(a, b) {
		if (a.name < b.name) {
		  return -1;
		}
		if (a.name > b.name) {
		  return 1;
		}
		return 0;
      },
      _getMillisecondTime: function(modelTime, startOrEnd) {
    	  var today = new Date(),
    	    hourMin = null;
    	  if (modelTime != null) {
			  hourMin = D.formatDateValue(modelTime, "%H:%M").split(":");
    	  } else if (startOrEnd != null) {
    		  hourMin = startOrEnd == "start" ? [0, 0] : [23, 45];
    	  }
    	  
    	  return hourMin != null ? new Date(today.getFullYear(), today.getMonth(), today.getDate(), hourMin[0], hourMin[1], 0).getTime() : null;
      },
      initializer: function() {
    	var times = [],
    	  ms15minutes = 900000,
    	  t = this._getMillisecondTime(null, "start");
    	
    	while (t <= this._getMillisecondTime(null, "end")) {
    		times.push({ time: t, name: D.formatDateValue(t, "%H:%M") })
    		t = t + ms15minutes;
    	}
    	
		this.times = times.sort(this._compareTime);
      },
      render: function(){
        var container = this.get('container');
        Y.app.CalendarDaysAddView.superclass.render.call(this);
        this.dayInput = container.one('#addCalendarWorkingDay_day');
        this.startTimeInput = container.one('#addCalendarWorkingDay_startTime');
        this.endTimeInput = container.one('#addCalendarWorkingDay_endTime');
        
        F.setSelectOptions(this.dayInput, O.values(Y.usp.calendar.enum.DayOfWeek.values), null, null, false, true, 'enumValue');
        F.setSelectOptions(this.startTimeInput, this.times, null, null, false, true, 'time');
        F.setSelectOptions(this.endTimeInput, this.times, null, null, false, true, 'time');
        return this;
      },
      destructor: function() {
		if (this.times) {
			delete this.times;
    	}
		
		if (this.dayInput) {
			this.dayInput.destroy();
			delete this.dayInput;
    	}
		
		if (this.startTimeInput) {
			this.startTimeInput.destroy();
			delete this.startTimeInput;
    	}
		
		if (this.endTimeInput) {
			this.endTimeInput.destroy();
			delete this.endTimeInput;
    	}
      }
  },{
    ATTRS:{
    }
  });
  
  var calendarDaysUpdateView = Y.Base.create(
    'calendarDaysUpdateView', 
    Y.usp.calendar.UpdateWorkingDayView,
    [],
    {
      template: Y.Handlebars.templates["calendarDaysEditDialog"],
      events: {
        '#calendarDaysEditForm': {submit: '_preventSubmit'}
      },
      _preventSubmit: function(e) {
        e.preventDefault();
      },
      _compareTime: function(a, b) {
		if (a.name < b.name) {
		  return -1;
		}
		if (a.name > b.name) {
		  return 1;
		}
		return 0;
      },
      _getMillisecondTime: function(modelTime, startOrEnd) {
    	  var today = new Date(),
    	    hourMin = null;
    	  if (modelTime != null) {
			  hourMin = D.formatDateValue(modelTime, "%H:%M").split(":");
    	  } else if (startOrEnd != null) {
    		  hourMin = startOrEnd == "start" ? [0, 0] : [23, 45];
    	  }
    	  
    	  return hourMin != null ? new Date(today.getFullYear(), today.getMonth(), today.getDate(), hourMin[0], hourMin[1], 0).getTime() : null;
      },
      initializer: function() {
    	var times = [],
    	  ms15minutes = 900000,
    	  t = this._getMillisecondTime(null, "start");
    	
    	while (t <= this._getMillisecondTime(null, "end")) {
    		times.push({ time: t, name: D.formatDateValue(t, "%H:%M") })
    		t = t + ms15minutes;
    	}
    	
		this.times = times.sort(this._compareTime);
      },
      render: function(){
        var container = this.get('container'),
          model = this.get('model');
        
        Y.app.CalendarDaysUpdateView.superclass.render.call(this);
        this.dayInput = container.one('#editCalendarWorkingDay_day');
        this.startTimeInput = container.one('#editCalendarWorkingDay_startTime');
        this.endTimeInput = container.one('#editCalendarWorkingDay_endTime');
        
        F.setSelectOptions(this.dayInput, O.values(Y.usp.calendar.enum.DayOfWeek.values), null, null, false, true, 'enumValue');
        F.setSelectOptions(this.startTimeInput, this.times, null, null, false, true, 'time');
        F.setSelectOptions(this.endTimeInput, this.times, null, null, false, true, 'time');
        
        this.dayInput.set('value', model.get("day"));
        this.startTimeInput.set('value', this._getMillisecondTime(model.get("startTime")));
        this.endTimeInput.set('value', this._getMillisecondTime(model.get("endTime")));
        return this;
      },
      destructor: function() {
		if (this.times) {
			delete this.times;
    	}
		
		if (this.dayInput) {
			this.dayInput.destroy();
			delete this.dayInput;
    	}
		
		if (this.startTimeInput) {
			this.startTimeInput.destroy();
			delete this.startTimeInput;
    	}
		
		if (this.endTimeInput) {
			this.endTimeInput.destroy();
			delete this.endTimeInput;
    	}
      }
  },{
    ATTRS:{
    }
  });
  
  var calendarDaysDeleteView = Y.Base.create(
    'calendarDaysDeleteView',
    Y.View,
    [],
    {
      template: Y.Handlebars.templates['calendarDaysDeleteDialog'],
      render: function() {
    	  var container = this.get('container'),
    	    html = this.template({
    	    	labels: this.get('labels')
    	    });
    	  container.setHTML(html);
    	  return this;
      }
    },
    {
      ATTRS: {}
    }
  );
  
  Y.namespace('app').CalendarDaysAddView = calendarDaysAddView;
  Y.namespace('app').CalendarDaysUpdateView = calendarDaysUpdateView;
  Y.namespace('app').CalendarDaysDeleteView = calendarDaysDeleteView;
  
  var newCalendarDaysForm = Y.Base.create(
    'newCalendarDaysForm',
    Y.usp.calendar.NewWorkingDay,
    [Y.usp.ModelFormLink],
    { form:'#calendarDaysAddForm' }
  );
  
  var updateCalendarDaysForm = Y.Base.create(
    'updateCalendarDaysForm',
    Y.usp.calendar.WorkingDay,
    [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],
    { form:'#calendarDaysEditForm' }
  );
  
  var deleteCalendarDaysForm = Y.Base.create(
    'deleteCalendarDaysForm',
    Y.Model,
    [Y.ModelSync.REST],
    {}
  );
  
  Y.namespace('app').NewCalendarDaysForm = newCalendarDaysForm;
  Y.namespace('app').UpdateCalendarDaysForm = updateCalendarDaysForm;
  Y.namespace('app').DeleteCalendarDaysForm = deleteCalendarDaysForm;
  
  var calendarDaysDialog = Y.Base.create(
    'calendarDaysDialog',
    Y.usp.app.AppDialog,
    [],
    {
      views: {
        addCalendarWorkingDay: {
          type:Y.app.CalendarDaysAddView,
          buttons:[{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: 'handleSave',
            disabled: true
          }]
        },
        editCalendarWorkingDay: {
        	type: Y.app.CalendarDaysUpdateView,
        	buttons: [{
				section: Y.WidgetStdMod.FOOTER,
				name: 'saveButton',
				labelHTML: '<i class="fa fa-check"></i> Save',
				classNames: 'pure-button-primary',
				action: function(e) {
					this.handleSave(e, {
						calendarId: this.get('activeView').get('otherData').calendar.id
					}, {
						transmogrify: Y.usp.calendar.UpdateWorkingDay
					});
				},
				disabled: true
        	}]
        },
        deleteCalendarWorkingDay: {
        	type: Y.app.CalendarDaysDeleteView,
        	buttons: [{
				section: Y.WidgetStdMod.FOOTER,
				name: 'saveButton',
				labelHTML: '<i class="fa fa-check"></i> Delete',
				classNames: 'pure-button-primary',
				action: 'handleRemove',
				disabled: true
        	}]
        }
      }
});

  Y.namespace('app').CalendarDaysDialog = calendarDaysDialog;
}, version, options);