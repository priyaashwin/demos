var version = '0.0.1',
  requiredModules = [
    'yui-base',
    'promise',
    'app-dialog',
    'model-form-link',
    'model-transmogrify',
    'form-util',
    'handlebars-calendar-templates',
    'handlebars-calendar-partials',
    'usp-calendar-CalendarEventBundle',
    'usp-calendar-NewCalendarCalendarEventBundle'
  ],
  options = {
    requires: requiredModules
  };

YUI.add('calendar-nw-days-dialog-views', function (Y) {
  var calendarNWDaysAddView = Y.Base.create(
    'calendarNWDaysAddView', 
    Y.usp.calendar.NewCalendarCalendarEventBundleView,
    [],
    {
      template: Y.Handlebars.templates["calendarNWDaysAddDialog"],
      events: {
        '#calendarNWDaysAddForm': {submit: '_preventSubmit'}
      },
      _preventSubmit: function(e) {
        e.preventDefault();
      },
      _getCalendarEventBundles: function() {
          var cebModel = new Y.usp.calendar.PaginatedCalendarEventBundleList({
            url: this.get('calendarEventBundleListURL')
          });
          var ccebModel = new Y.usp.calendar.PaginatedCalendarCalendarEventBundleWithCalendarEventBundleList({
            url: Y.Lang.sub(this.get('calendarCalendarEventBundleListURL'), {calendarId: this.get('calendar').id})
          });
          return new Y.Promise(function(resolve, reject) {
              var cebData = cebModel.load(function(err) {
                  if (err !== null) {
                      reject(err);
                  }
                  var ccebData = ccebModel.load(function(err) {
					  if (err !== null) {
						  reject(err);
					  }
                	  var ccebIds = Y.Array.map(ccebData.toJSON(), function(cceb) {
                		  return cceb['calendarEventBundle!id']
                	  });
                	  resolve(Y.Array.reject(cebData.toJSON(), function(ceb) {
                		  return ccebIds.indexOf(ceb.id) >= 0 || ceb.status == "IN_ACTIVE";
                	  }));
                  });
              });
          });
      },
      render: function(){
        var container = this.get('container');
        Y.app.CalendarNWDaysAddView.superclass.render.call(this);
		this._getCalendarEventBundles().then(function (data) {
			var bundles = data.length > 0 ? data : [{name: "No holidays available"}];
			var nWDaysInput = container.one('#addCalendarNonWorkingDay_eventBundle');
			Y.FUtil.setSelectOptions(nWDaysInput, bundles, null, null, false, true);
		});
        return this;
      }
  },{
    ATTRS:{
    }
  });
  
  var calendarNWDaysDeleteView = Y.Base.create(
    'calendarNWDaysDeleteView',
    Y.View,
    [],
    {
      template: Y.Handlebars.templates['calendarNWDaysDeleteDialog'],
      render: function() {
    	  var container = this.get('container'),
    	    html = this.template({
    	    	labels: this.get('labels')
    	    });
    	  container.setHTML(html);
    	  return this;
      }
    },
    {
      ATTRS: {}
    }
  );
  
  Y.namespace('app').CalendarNWDaysAddView = calendarNWDaysAddView;
  Y.namespace('app').CalendarNWDaysDeleteView = calendarNWDaysDeleteView;
  
  var newCalendarNWDaysForm = Y.Base.create(
    'newCalendarNWDaysForm',
    Y.usp.calendar.NewCalendarCalendarEventBundle,
    [Y.usp.ModelFormLink],
    { form:'#calendarNWDaysAddForm' }
  );
  
  var deleteCalendarNWDaysForm = Y.Base.create(
    'deleteCalendarNWDaysForm',
    Y.Model,
    [Y.ModelSync.REST],
    {}
  );
  
  Y.namespace('app').NewCalendarNWDaysForm = newCalendarNWDaysForm;
  Y.namespace('app').DeleteCalendarNWDaysForm = deleteCalendarNWDaysForm;
  
  var calendarNWDaysDialog = Y.Base.create(
    'calendarNWDaysDialog',
    Y.usp.app.AppDialog,
    [],
    {      
      views: {
        addCalendarNonWorkingDay: {
          type:Y.app.CalendarNWDaysAddView,
          buttons:[{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
            	this.handleSave(e, {calendarId: this.get('activeView').get('calendar').id});
            },
            disabled: true
          }]
        },
        deleteCalendarNonWorkingDay: {
          type:Y.app.CalendarNWDaysDeleteView,
          buttons:[{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Delete',
            classNames: 'pure-button-primary',
            action: 'handleRemove',
            disabled: true
          }]
        }
      }
  });

  Y.namespace('app').CalendarNWDaysDialog = calendarNWDaysDialog;
}, version, options);
