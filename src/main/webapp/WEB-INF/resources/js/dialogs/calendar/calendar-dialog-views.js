var version = '0.0.1',
  requiredModules = [
    'yui-base',
    'app-dialog',
    'model-form-link',
    'model-transmogrify',
    'form-util',
    'handlebars-calendar-templates',
    'handlebars-calendar-partials',
    'calendar-component-enumerations',
    'usp-calendar-NewCalendarAndWorkingDays',
    'usp-calendar-UpdateCalendar',
    'usp-calendar-Calendar'
  ],
  options = {
    requires: requiredModules
  };

YUI.add('calendar-dialog-views', function (Y) {
  var O = Y.Object,
      F = Y.FUtil;

  var calendarAddView = Y.Base.create(
    'calendarAddView', 
    Y.usp.calendar.NewCalendarAndWorkingDaysView,
    [],
    {
      template: Y.Handlebars.templates["calendarAddDialog"],
      events: {
        '#calendarAddForm': {submit: '_preventSubmit'}
      },
      _preventSubmit: function(e) {
        e.preventDefault();
      },
      render: function(){
        var container = this.get('container');
        Y.app.CalendarAddView.superclass.render.call(this);
        this.firstDayOfWeekInput = container.one('#addCalendar_firstDayOfWeek');
        F.setSelectOptions(this.firstDayOfWeekInput, O.values(Y.usp.calendar.enum.DayOfWeek.values), null, null, false, true, 'enumValue');
        return this;
      },
      destructor: function() {
		if (this.firstDayOfWeekInput) {
			this.firstDayOfWeekInput.destroy();
			delete this.firstDayOfWeekInput;
    	}
      }
  },{
    ATTRS:{
    }
  });
  
  var calendarUpdateView = Y.Base.create(
    'calendarUpdateView', 
    Y.usp.calendar.UpdateCalendarView,
    [],
    {
      template: Y.Handlebars.templates["calendarEditDialog"],
      events: {
        '#calendarEditForm': {submit: '_preventSubmit'}
      },
      _preventSubmit: function(e) {
        e.preventDefault();
      },
      render: function(){
        var container = this.get('container'),
          model = this.get('model');
        
        Y.app.CalendarUpdateView.superclass.render.call(this);
        this.firstDayOfWeekInput = container.one('#editCalendar_firstDayOfWeek');
        F.setSelectOptions(this.firstDayOfWeekInput, O.values(Y.usp.calendar.enum.DayOfWeek.values), null, null, false, true, 'enumValue');
        this.firstDayOfWeekInput.set('value', model.get("firstDayOfWeek"))
        return this;
      },
      destructor: function() {
		if (this.firstDayOfWeekInput) {
			this.firstDayOfWeekInput.destroy();
			delete this.firstDayOfWeekInput;
    	}
      }
  },{
    ATTRS:{
    }
  });
  
  var calendarStatusChangeView = Y.Base.create(
    'calendarStatusChangeView', 
    Y.View,
    [],
    {
      template: Y.Handlebars.templates["calendarStatusChangeDialog"],
	  render: function() {
		  var container = this.get('container'),
			  html = this.template({
				  labels: this.get('labels')
			  });
		  container.setHTML(html);
		  return this;
	  }
  },{
    ATTRS:{
    }
  });
  
  var calendarSetDefaultView = Y.Base.create(
    'calendarSetDefaultView', 
    Y.View,
    [],
    {
      template: Y.Handlebars.templates["calendarSetDefaultDialog"],
	  render: function() {
		  var container = this.get('container'),
			  html = this.template({
				  labels: this.get('labels')
			  });
		  container.setHTML(html);
		  return this;
	  }
  },{
    ATTRS:{
    }
  });

  Y.namespace('app').CalendarAddView = calendarAddView;
  Y.namespace('app').CalendarUpdateView = calendarUpdateView;
  Y.namespace('app').CalendarStatusChangeView = calendarStatusChangeView;
  Y.namespace('app').CalendarSetDefaultView = calendarSetDefaultView;

  var newCalendarForm = Y.Base.create(
    'newCalendarForm',
    Y.usp.calendar.NewCalendarAndWorkingDays,
    [Y.usp.ModelFormLink],
    { form:'#calendarAddForm' }
  );
  
  var updateCalendarForm = Y.Base.create(
    'updateCalendarForm',
    Y.usp.calendar.Calendar,
    [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],
    { form:'#calendarEditForm' }
  );
  
  var calendarStatusChangeForm = Y.Base.create(
	'calendarStatusChangeForm',
	Y.Model,
	[Y.ModelSync.REST],
	{}
  );
  
  var calendarSetDefaultForm = Y.Base.create(
	'calendarSetDefaultForm',
	Y.Model,
	[Y.ModelSync.REST],
	{}
  );

  Y.namespace('app').NewCalendarForm = newCalendarForm;
  Y.namespace('app').UpdateCalendarForm = updateCalendarForm;
  Y.namespace('app').CalendarStatusChangeForm = calendarStatusChangeForm;
  Y.namespace('app').CalendarSetDefaultForm = calendarSetDefaultForm;

  var calendarDialog = Y.Base.create(
    'calendarDialog',
    Y.usp.app.AppDialog,
    [],
    {
      views:{
        addCalendar: {
          type:Y.app.CalendarAddView,
          buttons:[{
            section: Y.WidgetStdMod.FOOTER,
            name: 'saveButton',
            labelHTML: '<i class="fa fa-check"></i> Save',
            classNames: 'pure-button-primary',
            action: function(e) {
            	this.handleSave(e, {
					status: 'IN_ACTIVE',
					timeZone: 'GMT'
            	});
            },
            disabled: true
          }]
        },
        editCalendar: {
        	type: Y.app.CalendarUpdateView,
        	buttons: [{
				section: Y.WidgetStdMod.FOOTER,
				name: 'saveButton',
				labelHTML: '<i class="fa fa-check"></i> Save',
				classNames: 'pure-button-primary',
				action: function(e) {
					this.handleSave(e, null, {
						transmogrify: Y.usp.calendar.UpdateCalendar
					});
				},
				disabled: true
        	}]
        },
        activateCalendar: {
			type: Y.app.CalendarStatusChangeView,
			buttons: [{
				section: Y.WidgetStdMod.FOOTER,
				name: 'saveButton',
				labelHTML: '<i class="fa fa-check"></i> Activate',
				classNames: 'pure-button-primary',
				action: function(e) {
					this.handleSave(e, null, {
						transmogrify: Y.usp.calendar.UpdateCalendar
					});
				},
				disabled: true
			}]
        },
        deactivateCalendar: {
        	type: Y.app.CalendarStatusChangeView,
        	buttons: [{
				section: Y.WidgetStdMod.FOOTER,
				name: 'saveButton',
				labelHTML: '<i class="fa fa-check"></i> Deactivate',
				classNames: 'pure-button-primary',
				action: function(e) {
					this.handleSave(e, null, {
						transmogrify: Y.usp.calendar.UpdateCalendar
					});
				},
				disabled: true
        	}]
        },
        setDefaultCalendar: {
			type: Y.app.CalendarSetDefaultView,
			buttons: [{
				section: Y.WidgetStdMod.FOOTER,
				name: 'saveButton',
				labelHTML: '<i class="fa fa-check"></i> Set As Default',
				classNames: 'pure-button-primary',
				action: function(e) {
					this.handleSave(e, null, {
						transmogrify: Y.usp.calendar.UpdateCalendar
					});
				},
				disabled: true
			}]
        }
      }
  });

  Y.namespace('app').CalendarDialog = calendarDialog;
}, version, options);
