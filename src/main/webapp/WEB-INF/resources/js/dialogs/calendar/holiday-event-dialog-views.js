YUI.add('holiday-event-dialog-views', function(Y) {
  var L = Y.Lang,
    O = Y.Object;

  var BaseHolidayEventAddEditView = Y.Base.create('baseHolidayEventAddEditView', Y.usp.View, [], {
    initializer: function() {
      this.holidayCalendar = new Y.usp.CalendarPopup({
        iconClass: 'fa fa-calendar'
      });
      //set as bubble targets
      this.holidayCalendar.addTarget(this);
    },
    render: function() {
      var container = this.get('container');

      BaseHolidayEventAddEditView.superclass.render.call(this);

      //set the input node for the calendar
      this.holidayCalendar.set('inputNode', container.one('input[name="date"]'));
      //render the calendar
      this.holidayCalendar.render();

      return this;
    },
    destructor: function() {
      this.holidayCalendar.removeTarget(this);
      this.holidayCalendar.destroy();
      delete this.holidayCalendar;
    }
  });

  var HolidayEventAddView = Y.Base.create('holidayEventAddView', BaseHolidayEventAddEditView, [], {
    template: Y.Handlebars.templates["holidayEventAddDialog"]
  });

  var HolidayEventUpdateView = Y.Base.create('holidayEventUpdateView', BaseHolidayEventAddEditView, [], {
    template: Y.Handlebars.templates["holidayEventEditDialog"]
  });

  var HolidayEventDeleteView = Y.Base.create('HolidayEventDeleteView', Y.usp.View, [], {
    template: Y.Handlebars.templates["holidayEventMessageDialog"]
  });

  var _dateSetter=function(val){
    var date =  L.isNumber(val)?Y.USPDate.parseDate(new Date(val)):Y.USPDate.parseDate(val);
    if (L.isDate(date)) {
      this._setAttrs({
        date: date.getTime()
      }, {
        silent: true
      });
    }
  };

  var BaseHolidayEventForm = Y.Base.create('baseHolidayEventForm', Y.usp.calendar.CalendarEvent, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
    customValidation: function(attrs) {
      var errors = {},
        labels = this.labels || {},
        parseDate=null;
      if (O.owns(attrs, 'name')) {
        if (!attrs.name) {
          errors['name'] = labels.name || 'Name is mandatory';
        }
      }

      if (O.owns(attrs, 'date')) {
        parseDate=(L.isNumber(attrs.date))?Y.USPDate.parse(new Date(attrs.date)):Y.USPDate.parse(attrs.date);
        if (!L.isDate(parseDate)) {
          errors.date = this.labels.date || 'Date is mandatory';
        }
      }
      return errors;
    }
  }, {
    ATTRS: {
      startDate:{
        setter: _dateSetter,
        getter: function(val){
          //actual value is stored in date field
          var date=this.get('date'),
          startDate=L.isNumber(date)?Y.USPDate.parseDate(new Date(date)):Y.USPDate.parseDate(date);

          if (L.isDate(startDate)) {
            startDate.setHours(0, 0, 0, 0);
          }

          return startDate?startDate.getTime():null;
        }
      },
      endDate:{
        setter: _dateSetter,
        getter: function(val){
          //actual value is stored in date field
          var date=this.get('date'),
          endDate=L.isNumber(date)?Y.USPDate.parseDate(new Date(date)):Y.USPDate.parseDate(date);

          if (L.isDate(endDate)) {
            endDate.setHours(23, 59, 59, 0);
          }

          return endDate?endDate.getTime():null;
        }
      },
      date: {
        value:null
      },
      allDay: {
        value: true
      }
    }
  });


  Y.namespace('app.admin.calendar').NewHolidayEventForm = Y.Base.create('newHolidayEventForm', BaseHolidayEventForm, [], {
    form: '#holidayEventAddForm'
  });

  Y.namespace('app.admin.calendar').UpdateHolidayEventForm = Y.Base.create('updateHolidayEventForm', BaseHolidayEventForm, [], {
    form: '#holidayEventEditForm'
  });


  Y.namespace('app.admin.calendar').HolidayEventDialog = Y.Base.create('holidayEventDialog', Y.usp.app.AppDialog, [], {
    saveNewHoliday: function(e) {
      this.handleSave(e, {
        typeOfEvent: "BHOL"
      }, {
        transmogrify: Y.usp.calendar.NewCalendarEvent
      });
    },
    saveUpdateHoliday: function(e) {
      this.handleSave(e, null, {
        transmogrify: Y.usp.calendar.UpdateCalendarEvent
      });
    },
    views: {
      addHolidayEvent: {
        type: HolidayEventAddView,
        buttons: [{
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Save',
          action: 'saveNewHoliday',
          disabled: true
        }]
      },
      updateHolidayEvent: {
        type: HolidayEventUpdateView,
        buttons: [{
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Save',
          action: 'saveUpdateHoliday',
          disabled: true
        }]
      },
      deleteHolidayEvent: {
        type: HolidayEventDeleteView,
        buttons: [{
          name: 'removeButton',
          labelHTML: '<i class="fa fa-check"></i> Delete',
          action: 'handleRemove',
          disabled: true
        }]
      }
    }
  });
}, '0.0.1', {
  requires: [
    'yui-base',
    'app-dialog',
    'model-form-link',
    'model-transmogrify',
    'form-util',
    'calendar-popup',
    'usp-date',
    'handlebars-calendar-templates',
    'usp-calendar-NewCalendarEvent',
    'usp-calendar-UpdateCalendarEvent',
    'usp-calendar-CalendarEvent',
    'usp-view'
  ]
});