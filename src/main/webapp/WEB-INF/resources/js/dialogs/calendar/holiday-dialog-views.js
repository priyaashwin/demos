YUI.add('holiday-dialog-views', function(Y) {

  var HolidayAddView = Y.Base.create('holidayAddView', Y.usp.View, [], {
    template: Y.Handlebars.templates["holidayAddDialog"]
  });

  var HolidayUpdateView = Y.Base.create('holidayUpdateView', Y.usp.View, [], {
    template: Y.Handlebars.templates["holidayEditDialog"]
  });

  var HolidayMessageView = Y.Base.create('holidayMessageView', Y.usp.View, [], {
    template: Y.Handlebars.templates["holidayMessageDialog"]
  });

  var BaseHolidayForm = Y.Base.create('baseHolidayForm', Y.usp.calendar.CalendarEventBundle, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {});

  Y.namespace('app.admin.calendar').NewHolidayForm = Y.Base.create('newHolidayForm', BaseHolidayForm, [], {
    form: '#holidayAddForm'
  }, {
    ATTRS: {
      status: {
        value: "IN_ACTIVE"
      }
    }
  });

  Y.namespace('app.admin.calendar').UpdateHolidayForm = Y.Base.create('updateHolidayForm', BaseHolidayForm, [], {
    form: '#holidayEditForm'
  });

  Y.namespace('app.admin.calendar').HolidayDialog = Y.Base.create('holidayDialog', Y.usp.app.AppDialog, [], {
    _saveWithUpdateUrl: function(e, attrs, options) {
      var view = this.get('activeView'),
          model = view.get('model'),
          url;

      url=view.get('updateUrl');
      if(url){
        //update url differs from view url - so update here
        model.url = view.get('updateUrl');
      }
      this.handleSave(e, attrs, options);
    },    
    saveNewHoliday: function(e) {
      this.handleSave(e, null, {
        transmogrify: Y.usp.calendar.NewCalendarEventBundle
      });
    },    
    saveUpdateHoliday: function(e) {
      this._saveWithUpdateUrl(e, null, {
        transmogrify: Y.usp.calendar.UpdateCalendarEventBundle
      });
    },
    views: {
      addHoliday: {
        type: HolidayAddView,
        buttons: [{
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Save',
          action: 'saveNewHoliday',
          disabled: true
        }]
      },
      updateHoliday: {
        type: HolidayUpdateView,
        buttons: [{
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Save',
          action: 'saveUpdateHoliday',
          disabled: true
        }]
      },
      activateHoliday: {
        type: HolidayMessageView,
        buttons: [{
          name: 'saveButton',
          labelHTML: '<i class="fa fa-check"></i> Activate',
          action: 'saveUpdateHoliday',
          disabled: true
        }]
      },
      deleteHoliday: {
        type: HolidayMessageView,
        buttons: [{
          name: 'removeButton',
          labelHTML: '<i class="fa fa-check"></i> Delete',
          action: 'handleRemove',
          disabled: true
        }]
      }
    }
  });

}, '0.0.1', {
  requires: [
    'yui-base',
    'app-dialog',
    'model-form-link',
    'model-transmogrify',
    'form-util',
    'handlebars-calendar-templates',
    'usp-calendar-NewCalendarEventBundle',
    'usp-calendar-UpdateCalendarEventBundle',
    'usp-calendar-CalendarEventBundle',
    'usp-view'
  ]
});