YUI.add('attachment-dialog', function(Y) {
    'use-strict';
    
    Y.namespace('app.attachment').AttachmentDialog = Y.Base.create('attachmentDialog', Y.usp.app.AppDialog, [], {
      initializer:function(){
          this._attachmentDialogEvts = [
            this.get('contentBox').delegate('change', this._handleFileChange, 'input[type="file"]', this)
          ];
      },
      handleSubmitUploadFile:function(e) {
        var view = this.get('activeView'),
            container = view.get('container'),
            formInput = container.one('input[type="file"]'),
            model=view.get('model');

        //prevent further updates
        this.showDialogMask();

        model.sync = function(action, options, callback) {
            options || (options = {});
            var file, modelDataBlob,
                data = {};

            //construct new Blob containing the model data
            modelDataBlob = new Blob([this.serialize(action)], {
                type: 'application/json'
            });

            file = new Y.FileHTML5({
                file: formInput.getDOMNode().files[0]
            });

            file.on('uploaderror', function(e) {
                var originEvent = e.originEvent || {};
                if (callback) {
                    callback({
                        code: e.status,
                        msg: e.data
                    }, originEvent.target);
                }
            });
            file.on('uploadcomplete', function(e) {
                var originEvent = e.originEvent || {};
                if (callback) {
                    callback(null, originEvent.target);
                }
            });

            data[this.get('_type')] = modelDataBlob;

            //start the file upload passing the file and our blob
            file.startUpload(model.getURL(action), data, 'file');
        };

        return Y.when(this.handleSave(e, {}));
      },      
      _handleFileChange:function(e){
        var activeView=this.get('activeView'),
            enabled=false;
        
        
        if(e.currentTarget.get('value')){
          enabled=true;
        }
        
        //target next input
        var nextInput=activeView.get('container').one('input[type="text"]');
        if(nextInput){
        	nextInput.focus();
        }        
        
        this.getButton('uploadButton', Y.WidgetStdMod.FOOTER).set('disabled', !enabled);
        this.getButton('resetButton', Y.WidgetStdMod.FOOTER).set('disabled', !enabled);
      },
      _resetFile:function(e){
        //clear the form input       
        var view = this.get('activeView'),
            container = view.get('container'),
            formInput = container.one('input[type="file"]'),
            model=view.get('model');
        
        formInput.set('value', '');
        
        //disable the upload button
        this.getButton('uploadButton', Y.WidgetStdMod.FOOTER).set('disabled', true);
        this.getButton('resetButton', Y.WidgetStdMod.FOOTER).set('disabled', true);
      },     
      views: {
          uploadFile: {
              buttons: [{
                  name: 'resetButton',
                  labelHTML: 'remove chosen file',
                  isText:true,
                  classNames: 'pull-left',
                  disabled: true,
                  action: '_resetFile'
                },{
                  name: 'uploadButton',
                  labelHTML: '<i class="icon-ok"></i> Upload',
                  disabled: true,
                  action: 'handleSubmitUploadFile'
              }]
          }
      }
    });



}, '0.0.1', {
    requires: ['yui-base',
              'app-dialog',
              'file-html5',
              'event-custom',
              'promise']
});