YUI.add('relationship-printgenogram-dialog', function (Y) {
  'use-strict';
  var L = Y.Lang;

  Y.namespace('app.relationship').PrintGenogramView = Y.Base.create('printGenogramView', Y.View, [], {
    template: Y.Handlebars.templates["relationshipPrintGenogramDialog"],
    render: function () {
      var container = this.get('container');
      // Render this view's HTML into the container element.
      container.setHTML(this.template());

      return this;
    }
  });

  Y.namespace('app.relationship').PrintGenogramDialog = Y.Base.create('printGenogramDialog', Y.usp.app.AppDialog, [], {
    views: {
      print: {
        type: Y.app.relationship.PrintGenogramView,
        buttons: [{
          name: 'printButton',
          isPrimary: true,
          labelHTML: '<i class="fa fa-check"></i> Print',
          action: function (e) {
            //stop the default click event
            e.preventDefault();
            var view = this.get('activeView');
            var container = view.get('container');
            var selectedFormat = container.one('input[type=radio][name=print]:checked');
            view.fire('doPrint', {
              format: selectedFormat.get('value'),
            })
            this.hide();
          },
          disabled: false,

        }]
      }
    }
  });


}, '0.0.1', {
    requires: ['yui-base',
      'view',
      'app-dialog',
      'handlebars-helpers',
      'handlebars-relationship-templates']
  });