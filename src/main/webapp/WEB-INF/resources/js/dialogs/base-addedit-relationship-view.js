YUI.add('base-addedit-relationship-view', function(Y) {

    Y.namespace('app.relationship').BaseAddEditRelationshipView = Y.Base.create('baseAddEditRelationshipView', Y.View, [], {

        destroyProfessionalSelectView: function() {
            if (this.professionalSelectView) {
                this.professionalSelectView.get('container').setHTML('');
                //unbind from event bubble
                this.professionalSelectView.removeTarget(this);
                //destroy the view
                this.professionalSelectView.destroy();
                //delete the view
                delete this.professionalSelectView;
            }
        },

        initProfessionalTeamSelectOnDateChange: function(e) {
            this.initializeProfessionalTeamsOnStartDate(e.date ? e.date : e.currentTarget.get("value"));
        },

        initializeProfessionalTeamsOnStartDate: function(relationshipStartDate) {
            var relatedPersonId = this.get('relatedPersonId') ? this.get('relatedPersonId') : this.get('model').get('personVO').id,
                currentUserPersonId = this.get('currentUserPersonId');

            if (this.get('relationshipType') === 'ProfessionalRelationshipType') {
                this.destroyProfessionalSelectView();

                if (relatedPersonId.toString() !== currentUserPersonId) {
                    var professionalTeamPromise = new Y.Promise(function(resolve, reject) {
                        var model = new Y.usp.ModelList({
                            url: Y.Lang.sub(this.get('personTeamsURL'), {
                                personId: relatedPersonId
                            })
                        }).load(function(err, res) {
                            err ? reject(err) : resolve(JSON.parse(res.response));
                        });
                    }.bind(this));

                    professionalTeamPromise.then(function(data) {
                        if (data) {
                            var results = data.results;
                            if (results.length === 0) {
                                this.getLabel('relatedProfessionalTeamName').hide();
                                this.get('container').one('#professional-teams-select').hide();
                            } else {
                                var selectedProfessionalTeams = results.filter(function(result) {
                                    return ((relationshipStartDate.getTime() >= result.startDate) &&
                                        (result.closeDate === null || (relationshipStartDate.getTime() <= result.closeDate)));
                                }).map(function(a) {
                                    return a.organisationVO;
                                }).sort(function(a, b) {
                                    return (a.name || '').localeCompare(b.name || '') || (a.organisationIdentifier || '').localeCompare(b.organisationIdentifier || '');
                                });

                                // Show the drop down if selectedProfessionalTeams (filtered data according to start date) is not empty 
                                if (Array.isArray(selectedProfessionalTeams) && selectedProfessionalTeams.length === 0) {
                                    this.getLabel('relatedProfessionalTeamName').hide();
                                    this.get('container').one('#professional-teams-select').hide();
                                } else {
                                    //Remove the duplicate team from the list
                                    var uniqueTeamList = function() {
                                        let uniq = {};
                                        return selectedProfessionalTeams.filter(function(obj) {
                                            return !uniq[obj.id] && (uniq[obj.id] = true);
                                        });
                                    };

                                    this.professionalSelectView = new RelatedProfessionalTeamSelectView({
                                        selectedProfessionalTeams: uniqueTeamList
                                    }).addTarget(this);

                                    this.get('container').one('#professional-teams-select').append(this.professionalSelectView.render().get('container'));
                                    this.getLabel('relatedProfessionalTeamName').show();
                                    this.getLabel('relatedProfessionalTeamName').removeClass('pure-hide');
                                    this.get('container').one('#professional-teams-select').show();

                                    // If there is a previously recorded team and is present in selectedProfessionalTeams, set it on the drop down                            
                                    var recordedTeam = this.get('model').get('professionalTeamId');
                                    // IE11 does not support Arrow function, so commented out below line
                                    //if (recordedTeam && selectedProfessionalTeams.some(someTeam => someTeam.id === recordedTeam)) {
                                    if (selectedProfessionalTeams.some(function(x) { return x.id === recordedTeam; })) {
                                        this.get('container').one('#professional-teams-list').set('value', recordedTeam.toString());
                                    }
                                }
                            }
                        }
                    }.bind(this));
                } else {
                    this.getLabel('relatedProfessionalTeamName').show();
                    this.getLabel('relatedProfessionalTeamName').removeClass('pure-hide');
                    this.get('container').one('#professional-teams-select').set('innerText', this.get('professionalTeamName'));
                    this.get('model').set('professionalTeamId', this.get('professionalTeamId'), {
                        silent: true
                    });
                }
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view'
    ]
});