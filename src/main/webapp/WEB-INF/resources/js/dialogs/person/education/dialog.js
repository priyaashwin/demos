YUI.add('education-dialog', function (Y) {

    Y.namespace('app.person.education').PersonEducationModel = Y.Base.create('personEducationModel',
        Y.usp.person.PersonEducation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#educationEditForm'
        });

    Y.namespace('app.person.education').UpdateEducationView = Y.Base.create('updateEducationView', Y.usp.person.UpdatePersonEducationView, [], {
        template: Y.Handlebars.templates["educationEditDialog"],
        render: function () {
            Y.app.person.education.UpdateEducationView.superclass.render.call(this);

            this.initCurriculumOffset(this.get('model').get('nationalCurriculumOffset'));
            return this;
        },
        initCurriculumOffset: function (value) {
            var select = this.get('container').one('#nationalCurriculumOffset-input')
            if (select) {
                if (value) {
                    select.set('value', value);
                }
            }
        },
    });

    Y.namespace('app.person.education').EducationDialog = Y.Base.create('educationDialog', Y.usp.app.AppDialog, [], {
        handleEditEducationSave: function (e) {
            var view = this.get('activeView'),
                model = view.get('model'),
                //flag to tell which VO to assign to the className variable which will tell whether
                // we have to create a new Education or edit an existing one
                defaulted = view.get('educationModel').get('defaulted'),
                className,
                personId = view.get('educationModel').get('personId'),
                personEducationId = view.get('educationModel').get('defaulted') ? undefined : view.get('educationModel').get('id');
            if (defaulted) {
                className = Y.usp.person.NewPersonEducation;
                model.url = view.get('createUrl');
            } else {
                personEducationId = view.get('educationModel').get('id')
                className = Y.usp.person.UpdatePersonEducation;
                model.url = view.get('updateUrl')
            }
            this.handleSave(e, {
                personId: personId,
                id: personEducationId
            }, {
                transmogrify: className
            });
        },
        views: {
            updateEducation: {
                type: Y.app.person.education.UpdateEducationView,
                buttons: [{
                    nationalCurriculumOffset: -3,
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleEditEducationSave',

                    disabled: true
                }]
            },
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'app-dialog',
        'handlebars',
        'handlebars-helpers',
        'handlebars-education-templates',
        'model-transmogrify',
        'usp-person-NewPersonEducation',
        'usp-person-UpdatePersonEducation',
        'usp-person-PersonEducation',
        'model-form-link'
    ]
});