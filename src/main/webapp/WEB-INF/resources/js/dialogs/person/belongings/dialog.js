YUI.add('belongings-dialog', function(Y) {
    var O = Y.Object,
        L = Y.Lang,
        DB = Y.usp.DataBindingUtil;

    Y.namespace('app.person.belongings').NewBelongingsForm = Y.Base.create('newBelongingsForm', Y.usp.belongings.NewBelongings, [
        Y.usp.ModelFormLink
    ], {
        form: '#belongingsForm'
    }, {
        ATTRS: {
            ownerSubjectType: {
                value: 'PERSON'
            }
        }
    });

    Y.namespace('app.person.belongings').UpdateMoveBelongings = Y.Base.create('updateMoveBelongings', Y.usp.belongings.Belongings, [
        Y.usp.ModelFormLink, Y.usp.ModelTransmogrify
    ], {
        form: '#updateBelongingsForm'
    });

    Y.namespace('app.person.belongings').UpdateBelongings = Y.Base.create('updateBelongings', Y.usp.belongings.Belongings, [
      Y.usp.ModelFormLink, Y.usp.ModelTransmogrify
  ], {
      form: '#updateBelongingsForm'
  });

    Y.namespace('app.person.belongings').UpdateDestroyBelongings = Y.Base.create('updateDestroyBelongings', Y.usp.belongings.Belongings, [
        Y.usp.ModelFormLink, Y.usp.ModelTransmogrify
    ], {
        form: '#updateBelongingsForm'
    },{
      ATTRS:{
        destructionDate:{
          value:null
        }
      }
    });

    Y.namespace('app.person.belongings').UpdateReturnBelongings = Y.Base.create('updateReturnBelongings', Y.usp.belongings.Belongings, [
        Y.usp.ModelFormLink, Y.usp.ModelTransmogrify
    ], {
        form: '#updateBelongingsForm',
        customValidation: function(attrs) {
            var errors = {},
                labels = this.labels || {};

            if (attrs.receivingSubjectType === null) {
                errors['receivingSubjectType'] = labels.receivingSubjectTypeErrorMessage;
            } else {
                if (L.trim(attrs.receivingSubjectId) === '' || !L.isNumber(Number(attrs.receivingSubjectId))) {
                    errors['receivingSubjectType'] = (attrs.receivingSubjectType === 'PERSON') ? labels.receivingSubjectIdPersonErrorMessage : labels.receivingSubjectIdOrganisationErrorMessage;
                }
            }


            return errors;
        }
    }, {
        ATTRS: {
            dateReturned:{
              value:null
            },
            receivingSubjectId: {
                value: null
            },
            receivingSubjectType: {
                value: null
            }
        }
    });

    Y.namespace('app.person.belongings').AddBelongingsView = Y.Base.create('addBelongingsView', Y.usp.belongings.NewBelongingsView, [], {
        template: Y.Handlebars.templates["belongingsAddDialog"],
        initializer: function() {
            this.dateReceived = new Y.usp.CalendarPopup()

            this.dateReceivedCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar',
                //can't receive in the future
                maximumDate: new Date()
            });
            //set as bubble targets
            this.dateReceivedCalendar.addTarget(this);
        },
        render: function() {
            var container = this.get('container');

            Y.app.person.belongings.AddBelongingsView.superclass.render.call(this);

            this.dateReceivedCalendar.set('inputNode', container.one('input[name="dateReceived"]'));
            this.dateReceivedCalendar.render();

            this.initOptions();

            return this;
        },
        destructor: function() {
            this.dateReceivedCalendar.destroy();
            delete this.dateReceivedCalendar;
        },
        initOptions: function() {
            var container = this.get('container');
            Y.FUtil.setSelectOptions(container.one('select[name="location"]'), O.values(Y.uspCategory.belongings.location.category.getActiveCodedEntries()), null, null, true, true, 'code');
            Y.FUtil.setSelectOptions(container.one('select[name="type"]'), O.values(Y.uspCategory.belongings.type.category.getActiveCodedEntries()), null, null, true, true, 'code');
        }
    });

    Y.namespace('app.person.belongings').UpdateBelongingsView = Y.Base.create('updateBelongingsView', Y.usp.belongings.BelongingsView, [], {
        template: Y.Handlebars.templates["belongingsEditDialog"]
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    types: Y.uspCategory.belongings.type.category.codedEntries,
                    locations: Y.uspCategory.belongings.location.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.person.belongings').ViewBelongingsView = Y.Base.create('viewBelongingsView', Y.usp.belongings.BelongingsView, [], {
        template: Y.Handlebars.templates["belongingsViewDialog"]
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    types: Y.uspCategory.belongings.type.category.codedEntries,
                    locations: Y.uspCategory.belongings.location.category.codedEntries,
                    statusTypes: Y.usp.belongings.enum.BelongingsStatus.values
                }
            }
        }
    });

    Y.namespace('app.person.belongings').HistoryBelongingsView = Y.Base.create('historyBelongingsView', Y.usp.belongings.BelongingsView, [], {
        template: Y.Handlebars.templates["belongingsHistoryDialog"],
        initializer: function(config) {
            var historyConfig = {
                searchConfig: config
            };

            this.belongingsResults = new Y.app.person.belongings.BelongingHistoryResults(historyConfig).addTarget(this);
        },
        render: function() {
            var container = this.get('container');
            //call superclass
            this.constructor.superclass.render.call(this);

            container.one(this.get('tableId')).setHTML(this.belongingsResults.render().get('container'));
        }
    }, {
        ATTRS: {
            tableId: {
                value: '#historyResultsView'
            },
            codedEntries: {
               value: {
                types: Y.uspCategory.belongings.type.category.codedEntries
            }
        }
    }
  });

    Y.namespace('app.person.belongings').MoveBelongingsView = Y.Base.create('moveBelongingsView', Y.usp.belongings.UpdateMoveBelongingsView, [], {
        template: Y.Handlebars.templates["belongingsMoveDialog"],
        initializer: function() {
            var model = this.get('model');

            this.dateMovedCalendar = new Y.usp.CalendarPopup()

            this.dateMovedCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar',
                //can't move after today
                maximumDate: new Date()
            });

            this._evtHandles = [
                //update the minimum calendar date when the model loads
                model.after('load', function(e) {
                    //can't move before previous move date
                    if (model.get('dateMoved')) {
                        this.dateMovedCalendar.set('minimumDate', model.get('dateMoved'));
                    } else {
                        //can't move before received date
                        this.dateMovedCalendar.set('minimumDate', model.get('dateReceived'));
                    }
                }, this)
            ];

            //set as bubble targets
            this.dateMovedCalendar.addTarget(this);

        },
        render: function() {
            var container = this.get('container');

            Y.app.person.belongings.MoveBelongingsView.superclass.render.call(this);

            this.dateMovedCalendar.set('inputNode', container.one('input[name="dateMoved"]'));
            this.dateMovedCalendar.render();

            //initialise location options
            this._initOptions();

            return this;
        },
        _initOptions: function() {
            var select = this.get('container').one('select[name="location"]');
            Y.FUtil.setSelectOptions(select, O.values(Y.uspCategory.belongings.location.category.getActiveCodedEntries()), null, null, true, true, 'code');
        },
        destructor: function() {
            this._evtHandles.forEach(function(handler) {
                handler.detach();
            });

            this.dateMovedCalendar.destroy();
            delete this.dateMovedCalendar;
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    types: Y.uspCategory.belongings.type.category.codedEntries,
                    locations: Y.uspCategory.belongings.location.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app.person.belongings').DestroyBelongingsView = Y.Base.create('destroyBelongingsView', Y.usp.belongings.UpdateDestroyBelongingsView, [], {
        template: Y.Handlebars.templates["belongingsDestroyDialog"],
        initializer: function() {
            var model = this.get('model');

            this.destructionDateCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar'
            });
            this._evtHandles = [
                //update the minimum calendar date when the model loads
                model.after('load', function(e) {
                    //can't destroy before move date
                    if (model.get('dateMoved')) {
                        this.destructionDateCalendar.set('minimumDate', model.get('dateMoved'));
                    } else {
                        //can't dfestroy before received date
                        this.destructionDateCalendar.set('minimumDate', model.get('dateReceived'));
                    }
                }, this)
            ];
        },
        render: function() {
            var container = this.get('container');

            Y.app.person.belongings.DestroyBelongingsView.superclass.render.call(this);

            this.destructionDateCalendar.set('inputNode', container.one('input[name="destructionDate"]'));
            this.destructionDateCalendar.render();

            return this;
        },
        destructor: function() {
            this._evtHandles.forEach(function(handler) {
                handler.detach();
            });

            this.destructionDateCalendar.destroy();
            delete this.destructionDateCalendar;
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    types: Y.uspCategory.belongings.type.category.codedEntries,
                    locations: Y.uspCategory.belongings.location.category.codedEntries
                }
            }
        }
    });

    var BaseReceivingSubjectView = Y.Base.create('baseReceivingSubjectView', Y.View, [], {
        template: Y.Handlebars.templates["belongingsReturnTo"],
        getAutoCompleteViewConstructor: function() {
            throw new TypeError('Please implement getAutoCompleteViewConstructor');
        },
        initializer: function(config) {
            this.autoCompleteView = new(this.getAutoCompleteViewConstructor())({
                autocompleteURL: config.autocompleteURL,
                inputName: 'receivingSubjectId',
                formName: 'updateBelongingsForm',
                rowTemplate: 'ROW_TEMPLATE_SIMPLE',
                labels: config.labels
            });

            this.autoCompleteView.addTarget(this);
        },
        render: function() {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //render the main template
            contentNode.setHTML(this.template({
                labels: this.get('labels')
            }));

            //add our fragments to the content node
            contentNode.one('#updateBelongingsForm_receivingSubject').append(this.autoCompleteView.render().get('container'));

            //set the fragment into the view container
            container.setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this.autoCompleteView.removeTarget(this);
            this.autoCompleteView.destroy();
            delete this.autoCompleteView;
        }
    }, {
        ATTRS: {
            autocompleteURL: {}
        }
    });

    var ReturnToOrganisationView = Y.Base.create('returnToOrganisationView', BaseReceivingSubjectView, [], {
        getAutoCompleteViewConstructor: function() {
            return Y.app.OrganisationAutoCompleteResultView;
        }
    });
    var ReturnToPersonView = Y.Base.create('returnToPersonView', BaseReceivingSubjectView, [], {
        getAutoCompleteViewConstructor: function() {
            return Y.app.PersonAutoCompleteResultView;
        }
    });

    Y.namespace('app.person.belongings').ReturnBelongingsView = Y.Base.create('returnBelongingsView', Y.View, [], {
        events: {
            'input[name="receivingSubjectType"]': {
                change: '_handleDataChange'
            }
        },
        template: Y.Handlebars.templates["belongingsReturnDialog"],
        initializer: function() {
            var model = this.get('model');

            this.dateReturnedCalendar = new Y.usp.CalendarPopup({
                iconClass: 'fa fa-calendar'
            });


            this._evtHandles = [
                //re-render when the owner subject type changes
                model.after('receivingSubjectTypeChange', this._afterReceivingSubjectTypeChange, this),

                //re-render only after load - we don't want to blow away the rendered dialog at any other time
                model.after('load', function(e) {
                    var model = this.get('model');

                    if (model.get('dateMoved')) {
                        //can'g return before moved date
                        this.dateReturnedCalendar.set('minimumDate', model.get('dateMoved'));
                    } else {
                        //can't return before received date
                        this.dateReturnedCalendar.set('minimumDate', model.get('dateReceived'));
                    }
                    //perform the initial render
                    this.render();
                }, this)
            ];

        },
        _handleDataChange: function(e) {
            var target = e.currentTarget,
                name = target.get('name');
            this.get('model').set(name, DB.getElementValue(target._node));
        },
        _afterReceivingSubjectTypeChange: function(e) {
            var _view;
            //destroy existing view
            if (this.selectionView) {
                //unbind from event bubble
                this.selectionView.removeTarget(this);
                //destroy the view
                this.selectionView.destroy();
            }

            //now create the new view
            _view = this._getNewSelectionView(e.newVal);
            if (_view) {
                //add event bubble target
                _view.addTarget(this);
            }
            this.selectionView = _view;

            //render into specific location on container
            this.renderSelectionView();
        },
        _getNewSelectionView: function(receivingSubjectType) {
            var _view,
                labels = this.get('labels') || {};

            switch (receivingSubjectType) {
                case 'ORGANISATION':
                    //show organisation view
                    _view = new ReturnToOrganisationView({
                        autocompleteURL: this.get('organisationAutocompleteUrl'),
                        labels: labels.organisation || {}
                    });
                    break;
                case 'PERSON':
                    //show person view
                    _view = new ReturnToPersonView({
                        autocompleteURL: this.get('personAutocompleteUrl'),
                        labels: labels.person || {}
                    });
                    break;
            }
            return _view;
        },
        render: function() {
            var container = this.get('container');

            //render the main template
            container.setHTML(this.template({
                modelData: this.get('model').toJSON() || {},
                otherData: this.get('otherData'),
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries') || {}
            }));

            this.dateReturnedCalendar.set('inputNode', container.one('input[name="dateReturned"]'));
            this.dateReturnedCalendar.render();

            return this;
        },
        renderSelectionView: function() {
            var container = this.get('container');
            if (this.selectionView) {
                //add our fragments to the content node
                container.one('#receivingSubjectContent').setHTML(this.selectionView.render().get('container'));
            }
        },
        destructor: function() {
            this._evtHandles.forEach(function(handler) {
                handler.detach();
            });

            this.dateReturnedCalendar.destroy();
            delete this.dateReturnedCalendar;

            if (this.selectionView) {
                this.selectionView.removeTarget(this);
                this.selectionView.destroy();

                delete this.selectionView;
            }
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    types: Y.uspCategory.belongings.type.category.codedEntries,
                    locations: Y.uspCategory.belongings.location.category.codedEntries
                }
            },
            personAutocompleteUrl: {
                value: ''
            },
            organisationAutocompleteUrl: {
                value: ''
            }
        }
    });

    Y.namespace('app.person.belongings').BelongingsDialog = Y.Base.create('belongingsDialog', Y.usp.app.AppDialog, [], {
        _saveWithUpdateUrl: function(e, attrs, options) {
            var view = this.get('activeView'),
                model = view.get('model');

            //update url differs from view url - so update here
            model.url = view.get('updateUrl');
            this.handleSave(e, attrs, options);
        },
        _saveMoveBelongings: function(e) {
            this._saveWithUpdateUrl(e, {}, {
                transmogrify: Y.usp.belongings.UpdateMoveBelongings
            });
        },
        _saveDestroyBelongings: function(e) {
            this._saveWithUpdateUrl(e, {}, {
                transmogrify: Y.usp.belongings.UpdateDestroyBelongings
            });
        },
        _saveReturnBelongings: function(e) {
            this._saveWithUpdateUrl(e, {}, {
                transmogrify: Y.usp.belongings.UpdateReturnBelongings
            });
        },
        views: {
            addBelongings: {
                type: Y.app.person.belongings.AddBelongingsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleSave',
                    disabled: true
                }]
            },
            updateBelongings: {
                type: Y.app.person.belongings.UpdateBelongingsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        this.handleSave(e, {}, {
                            transmogrify: Y.usp.belongings.UpdateBelongings
                        });
                    },
                    disabled: true
                }]
            },
            moveBelongings: {
                type: Y.app.person.belongings.MoveBelongingsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_saveMoveBelongings',
                    disabled: true
                }]
            },
            destroyBelongings: {
                type: Y.app.person.belongings.DestroyBelongingsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_saveDestroyBelongings',
                    disabled: true
                }]
            },
            historyBelongings: {
                type: Y.app.person.belongings.HistoryBelongingsView
            },
            returnBelongings: {
                type: Y.app.person.belongings.ReturnBelongingsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: '_saveReturnBelongings',
                    disabled: true
                }]
            },
            viewBelongings: {
                type: Y.app.person.belongings.ViewBelongingsView
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'app-dialog',
        'handlebars',
        'handlebars-helpers',
        'handlebars-belongings-templates',
        'model-transmogrify',
        'model-form-link',
        'usp-belongings-UpdateBelongings',
        'usp-belongings-UpdateMoveBelongings',
        'usp-belongings-UpdateDestroyBelongings',
        'usp-belongings-UpdateReturnBelongings',
        'usp-belongings-NewBelongings',
        'usp-belongings-Belongings',
        'categories-belongings-component-Location',
        'categories-belongings-component-Type',
        'belongings-component-enumerations',
        'belongings-history-results',
        'form-util',
        'calendar-popup',
        'usp-date',
        'data-binding',
        'person-autocomplete-view',
        'organisation-autocomplete-view'
    ]
});
