YUI.add('subjectaccessrequest-dialog', function(Y) {

    Y.namespace('app.person.subjectaccessrequest').NewSubjectAccessRequestForm = Y.Base.create('newSubjectAccessRequestForm', Y.usp.subjectaccessrequest.NewSubjectAccessRequest, [
        Y.usp.ModelFormLink
    ], {
        form: '#subjectaccessrequestForm'
    }, {
        ATTRS: {
            status: {
                value: 'PENDING'
            }
        }
    });


    
   
    Y.namespace('app.person.subjectaccessrequest').SubjectAccessRequestDialog = Y.Base.create('subjectaccessrequestDialog', Y.usp.app.AppDialog, [], {
        views: {
            addSubjectAccessRequest: {
                type: Y.app.person.subjectaccessrequest.AddSubjectAccessRequestView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'handleSave',
                    disabled: true
                }]
            },
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'app-dialog',
        'handlebars',
        'handlebars-helpers',
        'handlebars-subjectaccessrequest-templates',
        'model-form-link',
        'usp-subjectaccessrequest-NewSubjectAccessRequest',
        'usp-subjectaccessrequest-SubjectAccessRequest',
        'subjectaccessrequest-component-enumerations',
    ]
});
