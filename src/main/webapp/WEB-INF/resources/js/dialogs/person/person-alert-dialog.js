YUI.add('person-alert-dialog', function(Y) {
    'use-strict';

    var O=Y.Object,
        L=Y.Lang,
        hasValue = function(data) {
            return data !== undefined && data !== null;
        };

    Y.namespace('app.person.alert').PersonAlerts = Y.Base.create('personAlerts', Y.usp.person.PersonWithAlerts, [Y.usp.ModelTransmogrify], {
        setToMidnight: function(value) {
            var dateTime = new Date(value);
            dateTime.setHours(0, 0, 0, 0);
            return dateTime.getTime();
        },
        dayDiff:function(first, second){
            return this.setToMidnight(first) > this.setToMidnight(second);
        },
        customValidation:function(attrs){
            var errors={},
                labels=this.labels||{},
                today=new Date(),
                startDates={};

            today = today.getTime();

            var checkAlerts=function(alertModel){
                var alert=alertModel.toJSON();

                if((alert.alertCategory||'').trim().length===0){
                    //add an error - alert category is mandatory
                    errors['alertCategory'+alertModel.number]=L.sub(labels.alertCategoryErrorMessage||'Alert category is mandatory', {number:alertModel.number+1});
                }
                if((alert.alertNote||'').trim().length===0){
                    //add an error - note is mandatory
                    errors['alertNote'+alertModel.number]=L.sub(labels.alertNoteErrorMessage||'Alert note is mandatory', {number:alertModel.number+1});
                }
                if(alert.startDate){
                    startDates[alert.id] = alert.startDate;
                    if(this.dayDiff(alert.startDate, today)){
                        //add an error - startDate needs to be equal or before the current date
                        errors['alertStartDate'+alertModel.number]=L.sub(labels.alertStartDateErrorMessage||'Alert start date should be equal or before the current date', {number:alertModel.number+1});
                    }
                }
                if(alert.endDate){
                    checkEndDate(alertModel, alert, alert.startDate);
                }
            }.bind(this);

            var checkDeletedAlerts=function(alertModel){
                var alert=alertModel.toJSON();

                if(alert.endDate){
                    checkEndDate(alertModel, alert, startDates[alert.id]);
                }
            }.bind(this);

            var checkEndDate = function(alertModel, alert, startDate){
                var startDate = startDate || today;
                if(this.dayDiff(startDate, alert.endDate)){
                    //add an error - endDate needs to be after the start date
                    errors['alertEndDate'+alertModel.number]=L.sub(labels.alertEndDateErrorMessage||'Alert end date should be equal or after the start date', {number:alertModel.number+1});
                }
            }.bind(this);

            var newAlerts=attrs.newAlerts||[];
            var updatedAlerts=attrs.updatedAlerts||[];
            var deletedAlerts=attrs.deletedAlerts||[];

            newAlerts.forEach(checkAlerts);
            updatedAlerts.forEach(checkAlerts);
            deletedAlerts.forEach(checkDeletedAlerts);

            return errors;
          }
    },{
        ATTRS:{
            personAlerts:{
                value:[]
            },
            newAlerts:{
                value:[]
            },
            deletedAlerts:{
                value:[]
            },
            updatedAlerts:{
                value:[]
            }
        }
    });

    //extends NewPersonAlert to set number as a non attrib property
    Y.namespace('app.person.alert').NewPersonAlert = Y.Base.create('newPersonAlert', Y.usp.person.NewPersonAlert, [], {
        initializer:function(config){
            this.number=config.number;
        }
    },{
        //in validation, the number is used to locate the row in error
        _NON_ATTRS_CFG: ['number']
    });

    //extends UpdatePersonAlert to set number as a non attrib property
    Y.namespace('app.person.alert').UpdatePersonAlert = Y.Base.create('updatePersonAlert', Y.usp.person.UpdatePersonAlert, [], {
        initializer:function(config){
            this.number=config.number;
        }
    },{
        //in validation, the number is used to locate the row in error
        _NON_ATTRS_CFG: ['number']
    });

    //extends UpdatePersonAlert to set number as a non attrib property
    Y.namespace('app.person.alert').UpdateClosePersonAlert = Y.Base.create('updateClosePersonAlert', Y.usp.person.UpdateClosePersonAlert, [], {
      initializer:function(config){
        this.number=config.number;
      }
    },{
      //in validation, the number is used to locate the row in error
      _NON_ATTRS_CFG: ['number']
    });

    Y.namespace('app.person.alert').ViewPersonAlertsView = Y.Base.create('viewPersonAlertsView', Y.usp.alert.AlertView, [], {
        template:Y.Handlebars.templates["personAlertsViewDialog"]
    },{
        ATTRS:{
            codedEntries:{
                alertsCategory:Y.uspCategory.person.alerts.category.codedEntries
            }
        }
    });

    Y.namespace('app.person.alert').ManagePersonAlertsView = Y.Base.create('managePersonAlertsView', Y.usp.alert.AlertView, [], {
        template:Y.Handlebars.templates["personAlertsManageDialog"],
        events: {
            '.add': {
                click: 'handleAddAlert'
            },
            '.remove': {
                click: 'handleRemoveAlert'
            },
            'input[name="alertNote"]':{
                change: 'handleNoteChange'
            },
            'select[name="alertCategory"]':{
                change: 'handleAlertCategoryChange'
            },
            'input[name="alertStartDate"]': {
                change: 'handleStartDateChange'
            },
            'input[name="alertEndDate"]': {
                change: 'handleEndDateChange'
            }
        },
        initializer:function(){
            /*
             * Snap shot the alerts after the model has loaded
             */
          this.get('model').after('*:load', function(){
              var model=this.get('model'),
                  personAlerts=model.get('personAlerts')||[];

              //capture the alert ids from the model - this is used when we update to find
              //what has been deleted
              this.set('previousAlerts', personAlerts.map(function(alert){
                  return alert;
              }));

              //turn alerts into something we can use by flattening out the alertNote
              personAlerts=personAlerts.map(function(alert){
                  return ({
                      id:alert.id,
                      alertNote:alert.notes.map(function(note){
                          return note.content;
                      }).join(' '),
                      objectVersion:alert.objectVersion,
                      alertCategory:alert.alertCategory,
                      personId:alert.personId,
                      startDate:alert.startDate,
                      endDate:alert.endDate
                  });
              });

              //if empty push in a new entry
              if(personAlerts.length===0){
                  personAlerts.push({});
              }

              //update the model attribute - which will re-render
              model.setAttrs({
                  'personAlerts':personAlerts
             });
          }, this);

          this.calendars = [];
        },
        render:function(){
            Y.app.person.alert.ManagePersonAlertsView.superclass.render.call(this);

            //setup alert categories
            this._initPersonAlert();

            return this;
        },
        destructor: function() {
          this._destroyDates();
        },
        _destroyDates: function() {
            this.calendars.forEach(function(calendar) {
              this._destroyDate(calendar.startDate);
              this._destroyDate(calendar.endDate);
            }.bind(this));
        },
        _destroyDate: function(calendar) {
          calendar.destroy();
          delete calendar;
        },
        _initPersonAlert:function(){
            var model=this.get('model'),
              container=this.get('container'),
              personAlerts=model.get('personAlerts')||[];

            this._destroyDates();

            personAlerts.forEach(function(alert, i){
                this.initAlertCategory(i, container, alert);
                this.initAlertDate(i, container);
            }.bind(this));
        },
        handleAddAlert:function(e){
            var model=this.get('model');

            e.preventDefault();

            //update model with existing alerts - then add a new alert
            var personAlerts=model.get('personAlerts')||[];
            personAlerts.push({});

            //model change will trigger re-render
            model.setAttrs({'personAlerts':personAlerts});

            this.align();
        },
        handleRemoveAlert:function(e){
            var target=e.currentTarget,
              model=this.get('model');

            //get the index from the target
            var index=Number(target.getAttribute('data-index'));

            //update the model
            var personAlerts=model.get('personAlerts')||[];
            personAlerts.splice(index, 1);
            if(personAlerts.length===0){
                personAlerts.push({});
            }
            //model change will trigger re-render
            model.setAttrs({'personAlerts':personAlerts});

            this.align();
        },
        align:function(){
            //fire align event so dialog re-calculates position after render
            Y.later(50, this, function(){
                this.fire('align')
            });
        },
        handleNoteChange:function(e){
            var target=e.currentTarget;
            //get the index from the target
            var index=this.getIndex(target);

            this.updateAlert(index, {
                alertNote:target.get('value')
            });
        },
        handleAlertCategoryChange:function(e){
            var target=e.currentTarget;
            //get the index from the target
            var index=this.getIndex(target);

            this.updateAlert(index, {
                alertCategory: target.get('value')
            });
        },
        handleStartDateSelection:function(e, index){
            var date = e.date;
            if (hasValue(date)) {
                var parsedDate = Y.USPDate.parseDate(date);
                if(parsedDate!==null){
                    date = parsedDate.getTime();
                }
            }
            this.updateAlert(index, {
                startDate: date
            });
            this.updateEndDateValue(index, e.date);
        },
        handleEndDateSelection:function(e, index){
            var date = e.date;
            if (hasValue(date)) {
                var parsedDate = Y.USPDate.parseDate(date);
                if(parsedDate!==null){
                    date = parsedDate.getTime();
                }
            }
            this.updateAlert(index, {
                endDate: date
            });
        },
        handleStartDateChange: function(e) {
            this.handleDateChange(e, this.handleStartDateSelection);
        },
        handleEndDateChange: function(e) {
            this.handleDateChange(e, this.handleEndDateSelection);
        },
        handleDateChange: function(e, handleDateSelection) {
            var index = e.target.getData('index');
            var newVal = e.target.get('value');
            e.date = Y.USPDate.parseDate(newVal);
            handleDateSelection.call(this, e, index);
        },
        getIndex:function(target){
            //get the index from the target
            return Number(target.getAttribute('data-index'));
        },
        updateEndDateValue:function(index, startValue){
            var endDate = this.get('container').one('#alertEndDate'+index);
            if(endDate.get('value') === "") return;
            var endValue = new Date(endDate.get('value'));
            if(startValue > endValue){
                endDate.set('value', null);
                this.updateAlert(index, {
                    endDate: null
                });
            }
        },
        updateAlert:function(index, attrs){
            var model=this.get('model');

            var personAlerts=model.get('personAlerts')||[];
            personAlerts[index]=Y.merge(personAlerts[index]||{}, attrs||{});

            this.set('personAlerts', personAlerts);
        },
        initAlertCategory:function(index, container, alert){
            var alertsCategory=Y.uspCategory.person.alerts.category,
                alertCategory = this.lookupExistingEntry(alertsCategory.codedEntries, alert.alertCategory)||{};

            var select=container.one('select[name="alertCategory"][data-index="'+index+'"]');

            if(select){
                //set the select options for outputCategory
                Y.FUtil.setSelectOptions(select, O.values(alertsCategory.getActiveCodedEntries()), alertCategory.code, alertCategory.name, true, true, 'code');

                //ensure the currently select value is set
                select.set('value', alert.alertCategory||'');
            }
        },
        initAlertDate:function(index, container){
            this.initStartDate(index, container);
            this.initEndDate(index, container);
        },
        initStartDate:function(index, container){
            this.initDate({
                inputNode: container.one('#alertStartDate'+index),
                maximumDate: new Date()
            }, index, 'startDate', this.handleStartDateSelection);
        },
        initEndDate:function(index, container){
            this.initDate({
                inputNode: container.one('#alertEndDate'+index),
                minimumDateFieldDependency: '#alertStartDate'+index
            }, index, 'endDate', this.handleEndDateSelection);
        },
        initDate:function(setup, index, dateLabel, handler){
            var calendar = new Y.usp.CalendarPopup(setup).render();
            calendar.after('dateSelection', Y.bind(function(e){handler.call(this, e, index)}, this));
            this.calendars[index] = this.calendars[index]||{};
            this.calendars[index][dateLabel] = calendar;
        },
        lookupExistingEntry: function(codes, val) {
            var codedEntry, entry = {
                code: null,
                name: null
            };
            if (!(typeof val === 'undefined' || val === '' || val === null)) {
                codedEntry = codes[val];

                if (codedEntry) {
                    entry.code = codedEntry.code;
                    entry.name = codedEntry.name;
                }
            }
            return entry;
        }
    },{
        ATTRS:{
            /**
             * Track the changes to alerts by maintaining a local copy of the alert ids from the model
             * this can then be compared against those in the model on a save to find the changes
             */
            previousAlerts:{
                value:[]
            },
            codedEntries:{
                alertsCategory:Y.uspCategory.person.alerts.category.codedEntries
              }
        }
    });

    Y.namespace('app.person.alert').PersonAlertDialog = Y.Base.create('personAlertDialog', Y.usp.app.AppDialog, [], {
        views: {
            manageAlerts: {
                type: Y.app.person.alert.ManagePersonAlertsView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e){
                        var view=this.get('activeView'),
                            model=view.get('model'),
                            //load cached alerts from the view
                            previousAlerts=view.get('previousAlerts')||[],
                            alerts=model.get('personAlerts')||[],
                            otherData=view.get('otherData')||{},
                            subject=otherData.subject||{};

                        //add/update a number - used to sync validation with input row for each alert
                        //this is done before any pruning and shuffling of data
                        alerts.forEach(function(alert, i){
                            alert.number=i;
                        });

                        //find new alerts
                        var newAlerts=alerts.filter(function(alert){
                           return alert.id===undefined;
                        }).filter(function(alert){
                            //alerts with no alert note text and no category are not to be saved
                            return (alert.alertNote||'').trim().length>0||(alert.alertCategory||'').trim().length>0;
                        });

                        var removedAlerts=previousAlerts.filter(function(previous){
                            return !alerts.find(function(a){return a.id===previous.id;});
                        }).map(function(alert){
                            var parsedDate = Y.USPDate.parseDate(new Date());
                            alert.endDate = parsedDate.getTime();
                            return alert;
                        });

                        //also remove alerts where the alert note & category has been cleared
                        removedAlerts=removedAlerts.concat(alerts.filter(function(alert){
                            return alert.id!==undefined && (
                              ( alert.endDate!==null && alert.endDate!==undefined ) ||
                              ( (alert.alertNote||'').trim().length===0 && (alert.alertCategory||'').trim().length===0 )
                            );
                        }));

                        var modifiedAlerts=alerts.filter(function(alert){
                            return alert.id!==undefined&&((alert.alertNote||'').trim().length>0||(alert.alertCategory||'').trim().length>0);
                        });

                        //update url differs from view url - so update here
                        model.url=view.get('updateUrl');

                        this.handleSave(e, {
                            'personAlerts':alerts,
                            'newAlerts':newAlerts.map(function(alert){
                              var ext = {};
                              if(alert.startDate !== undefined){
                                ext = { startDate: alert.startDate };
                              }
                              if(alert.endDate !== undefined){
                                ext = Y.merge(ext, { endDate: alert.endDate });
                              }
                              return new Y.app.person.alert.NewPersonAlert(Y.merge(ext, {
                               objectVersion:alert.objectVersion,
                               alertNote:alert.alertNote,
                               alertCategory: alert.alertCategory,
                               personId:subject.subjectId,
                               number:alert.number
                             }));
                            }),
                            'deletedAlerts': removedAlerts.map(function(alert){
                              return new Y.app.person.alert.UpdateClosePersonAlert({
                               objectVersion:alert.objectVersion,
                               endDate:alert.endDate,
                               number:alert.number,
                               id:alert.id
                              });
                            }),
                            'updatedAlerts': modifiedAlerts.map(function(alert){
                              return new Y.app.person.alert.UpdatePersonAlert({
                                alertCategory: alert.alertCategory,
                                objectVersion:alert.objectVersion,
                                alertNote: alert.alertNote,
                                id: alert.id,
                                number: alert.number,
                                personId: alert.personId,
                                startDate: alert.startDate
                              });
                            })
                        }, {
                            //pass the subject id so it can be substituted into the url
                            subjectId: subject.subjectId,
                            transmogrify: Y.usp.person.UpdatePersonAlerts
                        });

                    },
                    disabled: true
                }]
            },
            viewAlerts: {
                type: Y.app.person.alert.ViewPersonAlertsView
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'app-dialog',
        'view',
        'handlebars',
        'handlebars-helpers',
        'handlebars-person-templates',
        'usp-alert-Alert',
        'usp-person-NewPersonAlert',
        'usp-person-UpdatePersonAlert',
        'usp-person-UpdatePersonAlerts',
        'usp-person-UpdateClosePersonAlert',
        'usp-person-PersonWithAlerts',
        'model-transmogrify',
        'yui-later',
        'calendar-popup',
        'usp-date',
        'categories-person-component-Alerts'
    ]
});
