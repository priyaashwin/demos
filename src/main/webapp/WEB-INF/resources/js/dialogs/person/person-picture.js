YUI.add('person-picture-dialog-views', function(Y) {
    var A = Y.Array,
        L = Y.Lang,
        O = Y.Object;

    Y.namespace('app').PersonPictureDialog = Y.Base.create('personPictureDialog',
        Y.usp.app.AppDialog, [], {
            initializer: function() {
                this.on('*:clearErrors', function() {
                    this.multiPanelModelErrors.clearErrors();
                }, this);
            },
            views: {
                editPersonPicture: {
                    // This is in person.js (there is a pile of refactoring needed there)
                    type: Y.app.person.UpdatePersonPictureView,
                    buttons: [{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Save',
                        classNames: 'pure-button-primary',
                        action: function(e) {
                            var view = this.get('activeView'),
                                personDetailsModel = view.get('personDetailsModel'),
                                existingPictureId = personDetailsModel.get('pictureId'),
                                retrievePictureInfoUrl = view.get('pictureInfoUrl');
                            model = view.get('model');

                            e.preventDefault();
                            this.showDialogMask();

                            if (view.get('container').one('#filename-input').get('value')) {
                                //we bypass the standard save here as we need to include an attachment
                                //override the sync method provided by ModelSyncREST to handle the file upload process
                                if (existingPictureId) {
                                    model.set('existingPictureId', existingPictureId, {
                                        silent: true
                                    });
                                }
                                model.set('personId', personDetailsModel.get('id'), {
                                    silent: true
                                });
                                model.sync = function(action, options, callback) {
                                    options || (options = {});
                                    var templateFile,
                                        url = this.url;

                                    //construct new Blob containing the model data
                                    var modelDataBlob = new Blob([this.serialize(action)], {
                                        type: 'application/json'
                                    });

            			            var dataURI = Y.one(model.form).one('#croppingResultCanvas')._node.toDataURL();
            			            var byteString;
            							    if (dataURI.split(',')[0].indexOf('base64') >= 0)
            							        byteString = atob(dataURI.split(',')[1]);
            							    else
            							        byteString = unescape(dataURI.split(',')[1]);
            					
            							    // separate out the mime component
            							    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            					
            							    // write the bytes of the string to a typed array
            							    var ia = new Uint8Array(byteString.length);
            							    for (var i = 0; i < byteString.length; i++) {
            							        ia[i] = byteString.charCodeAt(i);
            							    }
            					
            							    var blob = new Blob([ia], {type:mimeString});
            		    
            			            templateFile = new Y.FileHTML5({
            			                file: blob
            			            });

                                    templateFile.on('uploaderror', function(e) {
                                        var originEvent = e.originEvent || {};
                                        if (callback) {
                                            callback({
                                                code: e.status,
                                                msg: e.statusText
                                            }, originEvent.target);
                                        }
                                    });
                                    templateFile.on('uploadcomplete', function(e) {
                                        var originEvent = e.originEvent || {};
                                        if (callback) {
                                            callback(null, originEvent.target);
                                        }
                                    });
                                    var data = {};
                                    data[this.get('_type')] = modelDataBlob;
                                    //start the file upload passing the file and our blob
                                    templateFile.startUpload(url, data, 'file');
                                }
                            }

                            model.save(Y.bind(function(err, res) {
                                if (!err) {
                                    Y.fire('person:personDetailChanged');
                                    Y.fire('infoMessage:message', {
                                        message: view.get('labels.editPersonPictureSuccess')
                                    });
                                    this.hide();
                                } else {
                                    this.hideDialogMask();
                                }
                            }, this));
                        },
                        disabled: true
                    }]
                },
                removePersonPicture: {
                    type: Y.app.person.RemovePersonPictureView,
                    buttons: [{
                            section: Y.WidgetStdMod.FOOTER,
                            name: 'yesButton',
                            labelHTML: '<i class="fa fa-check"></i> yes',
                            classNames: 'pure-button-primary',
                            action: function(e) {
                                var view = this.get('activeView'),
                                    personDetailsModel = view.get('personDetailsModel'),
                                    model = view.get('model');

                                e.preventDefault();
                                this.showDialogMask();
                                model.url = Y.Lang.sub(model.removeUrl, {
                                    pictureId: personDetailsModel.get('pictureId')
                                });

                                model.destroy({
                                    remove: true
                                }, Y.bind(function(err, res) {
                                    if (!err) {
                                        Y.fire('person:personDetailChanged');
                                        Y.fire('infoMessage:message', {
                                            message: view.get('labels.removePersonPictureSuccess')
                                        });
                                        this.hide();
                                    } else {
                                        this.hide();
                                    }
                                }, this));
                            },
                            disabled: false
                        },
                        {
                            name: 'noButton',
                            labelHTML: '<i class="fa fa-times"></i> No',
                            classNames: 'pure-button-secondary',
                            action: 'handleCancel',
                            section: Y.WidgetStdMod.FOOTER
                        }
                    ]
                }
            }
        });
}, '0.0.1', {
    requires: [
        'app-dialog',
    ]
});