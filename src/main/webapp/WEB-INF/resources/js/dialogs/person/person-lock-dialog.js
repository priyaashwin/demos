YUI.add('person-lock-dialog', function(Y) {
    var A = Y.Array;

    Y.namespace('app.person.lock').SecurityAccessAllowedRecord = Y.Base.create('securityAccessAllowedRecord', Y.usp.security.SecurityAccessAllowedRecord, [Y.usp.ModelTransmogrify], {        
    });
    
    Y.namespace('app.person.lock').SecurityAccessDeniedRecord = Y.Base.create('securityAccessDeniedRecord', Y.usp.security.SecurityAccessDeniedRecord, [Y.usp.ModelTransmogrify], {        
    });
    
    Y.namespace('app.person.lock').SecurityMessageView = Y.Base.create('securityMessageView', Y.usp.View, [], {
        template: Y.Handlebars.templates.messageDialog,
        render:function(){
            var container = this.get('container'),
            html;

            html = this.template({
                message: this.get('message') || ''
            });
    
            // Render this view's HTML into the container element.
            container.setHTML(html);
    
            return this;
        }
    },{
        ATTRS:{
            message:{
                value:''
            }
        }
    });

    Y.namespace('app.person.lock').PersonLockAddView = Y.Base.create('personLockAddView', Y.View, [], {
        // bind template
        template: Y.Handlebars.templates.personLockAddDialog,
        events: {
            // enable the proceed button once the user selects one of the radio
            // buttons
            'input[name=personLockType]': {
                click: '_enableProceedButton'
            }
        },
        _enableProceedButton: function() {
            // inform the dialog to enable the proceed button
            this.fire('enableProceedButton');
        },
        render: function() {
            var container = this.get('container'),
                permissions = this.get('permissions'),
                html = this.template({
                    labels: this.get('labels'),
                    permissions:permissions
                });
            // Render this view's HTML into the container element.
            container.setHTML(html);

            return this;
        }
    },{
        ATTRS:{
            permissions:{
                value:{
                    canAddSecurityAccessAllowedRecord:false,
                    canAddSecurityAccessDeniedRecord:false
                }
            }
        }
    });

    Y.namespace('app.person.lock').SecurityBodyPickListView = Y.Base.create('securityBodyPickListView', Y.View, [], {
        render:function(){
            var container=this.get('container'),
                picklistNode=Y.Node.create('<div />');
                availableOptions=this.get('availableOptions').sort(function(a,b){
                    return a.name.localeCompare(b.name, 'en', {sensitivity: 'base'});
                }),
                assignedOptions=this.get('assignedOptions'),
                labels=this.get('labels');
            
            this.picklist = new Y.usp.Picklist({
                optionsMap: {
                    text: 'displayName',
                    value: 'id',
                    title: 'name'
                },
                options: availableOptions.filter(function(option){
                    return A.find(assignedOptions,function(o){
                       return o.id===option.id; 
                    })===null;
                }),
                selections: assignedOptions,
                order: availableOptions.map(function(option){
                    return option.displayName;
                }),
                stackMode: true,
                preserveOrder: true,
                selectWidth: '100%',
                actionLabelOne: '>',
                actionLabelAll: '>>',
                actionLabelRmv: '<',
                actionLabelRmvAll: '<<',
                availableLabelText: labels.available,
                assignedLabelText: labels.assigned + '<span class="mandatory"> *</span>',
                assignedId:this.get('assignedId')
            });

            container.appendChild(picklistNode);
            
            this.picklist.render(picklistNode);
            
            return this;
        },
        destructor:function(){
            if(this.picklist){
                this.picklist.destroy();
                
                delete this.picklist;
            }
        },
        getSelections:function(){
            var selections=[];
            
            if(this.picklist){
                selections=this.picklist.getSelections();
            }
            
            return selections;
        }
    },{
        ATTRS:{
            availableOptions:{
                value:[]
            },
            assignedOptions:{
                value:[]
            },
            labels:{
                value:{}
            },
            assignedId:{
                value:'securityBodyIdTypeVOList'
            }
        }
    });
    
    Y.namespace('app.person.lock').SecurityAccessRecordView = Y.Base.create('securityAccessRecordView', Y.usp.View, [], {
        template: Y.Handlebars.templates.securityAccessDialog,
        initializer:function(){
            this.after('loadedChange', this.handleLoadedChange, this);
            
            var userSecurityBodies=new Y.Promise(function(resolve, reject){
                var model=new Y.usp.security.SecurityBodyIdNameList({
                    url: this.get('userSecurityBodiesUrl')
                }).load(function(err, response){
                  if(!err){
                      resolve(model);
                  }else{
                   reject(err);
                  }
                })
            }.bind(this));
            

            var convertToOption=function(teamSecurityBody){
                return ({
                   id: teamSecurityBody.get('id'),
                   name: teamSecurityBody.get('name'),
                   type: teamSecurityBody.get('type'),
                   identifier: teamSecurityBody.get('identifier'),
                   displayName: [teamSecurityBody.get('identifier'), teamSecurityBody.get('name')].join(' - ') 
                });
            };
            
            var teamSecurityBodies=new Y.Promise(function(resolve, reject){
                var model=new Y.usp.security.SecurityBodyIdNameList({
                    url: this.get('teamSecurityBodiesUrl')
                }).load(function(err, response){
                  if(!err){
                      resolve(model);
                  }else{
                   reject(err);
                  }
                })
            }.bind(this));

            Y.Promise.all([userSecurityBodies, teamSecurityBodies]).then(function(data){
                this.set('availableUsers', data[0].map(convertToOption));
                this.set('availableTeams', data[1].map(convertToOption))
                
                //signal that loading is over - time to show the tabs
                this.set('loaded', true);
          }.bind(this)).catch(function(e){
              this.fire('error',{
                  error:e
              });
          }.bind(this))
        },
        render:function(){
            var container = this.get('container');
            
            var html = this.template({
                labels: this.get('labels')
            });
    
            container.setHTML(html);
            
            if(this.get('loaded')===true){
                this.renderTabs();
            }
        },
        handleLoadedChange:function(e){
            if(e.newVal===true){
                this.renderTabs();
            }
        },
        renderTabs: function() {
            var container = this.get('container'),
                labels=this.get('labels'),
                tabNode=container.one('#securityAccessTabs');
            
            if(!tabNode){
                //render not yet complete
                return;
            }
            
            var model=this.get('model');
            var securityAccessEntryVOList=model.get('securityAccessEntryVOList')||[];
            
            var usersPicklistContainer=Y.Node.create('<div />');
            var teamsPicklistContainer=Y.Node.create('<div />');
            
            this.tabView = new Y.TabView({
                children:[{
                   label:labels.usersTab,
                   content:usersPicklistContainer
                },{
                   label: labels.teamsTab,
                   content:teamsPicklistContainer
                }]
            }).addTarget(this);

            //clear the tabNode (remove the loading indicator)
            tabNode.setHTML('');
            
            this.tabView.render(tabNode);
            
            var securityAccessEntryVOToOption=function(entry){
                return ({
                    id: entry.securityBodyIdNameVO.id,
                    name: entry.securityBodyIdNameVO.name,
                    type: entry.securityBodyIdNameVO.type,
                    identifier: entry.securityBodyIdNameVO.identifier,
                    displayName: [entry.securityBodyIdNameVO.identifier, entry.securityBodyIdNameVO.name].join(' - ') 
                 });
            };
            
            //setup the 2 picklists
            this.usersPickList=new Y.app.person.lock.SecurityBodyPickListView({
                labels:{
                    available:labels.availableUsers,
                    assigned:labels.assignedUsers  
                },
                availableOptions:this.get('availableUsers'),
                assignedOptions:securityAccessEntryVOList.filter(function(entry){
                    return entry.securityBodyIdNameVO.type==='USER';
                }).map(securityAccessEntryVOToOption)
            }).render();
            
            usersPicklistContainer.appendChild(this.usersPickList.get('container'));
            
            this.teamsPickList=new Y.app.person.lock.SecurityBodyPickListView({
                labels:{
                    available:labels.availableTeams,
                    assigned:labels.assignedTeams
                },
                availableOptions:this.get('availableTeams'),
                assignedOptions:securityAccessEntryVOList.filter(function(entry){
                    return entry.securityBodyIdNameVO.type==='TEAM';
                }).map(securityAccessEntryVOToOption)
            }).render(usersPicklistContainer);
            
            teamsPicklistContainer.appendChild(this.teamsPickList.get('container'));
            
            //fire align event so dialog re-calculates position after render of tabs
            Y.later(50, this, function(){
                this.fire('align')
            });
        },
        destructor:function(){
            if(this.tabView){
                this.tabView.destroy();
                
                delete this.tabView;
            }
            
            if(this.usersPickList){
                this.usersPickList.destroy();
                
                delete this.usersPickList;
            }
            
            if(this.teamsPickList){
                this.teamsPickList.destroy();
                
                delete this.teamsPickList;
            }
        },
        /**
         * Returns the combined selections of team/user security bodies
         */
        getSelectedSecurityBodies:function(){
            var selections=[];
            if(this.usersPickList){
                selections=selections.concat(this.usersPickList.getSelections().map(function(option){
                    return ({
                        id:option.value,
                        type:'USER'
                    });
                }));
            }
            if(this.teamsPickList){
                selections=selections.concat(this.teamsPickList.getSelections().map(function(option){
                    return ({
                        id:option.value,
                        type:'TEAM'
                    })
                }));
            }
            return selections;
        }
    },{
        ATTRS:{
            labels:{
                value:{
                    usersTab:'Users',
                    teamsTab: 'Teams'
                }
            },
            loaded:{
                value:false
            },
            availableUsers:{
                value:[]
            },
            availableTeams:{
                value:[]
            },
            userSecurityBodiesUrl:{
                value:''
            },
            teamSecurityBodiesUrl:{
                values:''
            }
        }
    });
     
    Y.namespace('app.person.lock').PersonLockDialog = Y.Base.create('personLockDialog', Y.usp.app.AppDialog, [], {
        initializer: function() {
            this._personLockEvtHandlers = [
                this.on('*:enableProceedButton', this._enableProceedButton, this)
            ];
        },
        _enableProceedButton: function() {
            this.getButton('proceedButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        },
        destructor: function() {
            if (this._personLockEvtHandlers) {
                A.each(this._personLockEvtHandlers, function(item) {
                    item.detach();
                });
                // null out
                this._personLockEvtHandlers = null;
            }
        },
        saveSecurityRecord:function(e, options){
            // Get the view and the model
            var activeView = this.get('activeView'),
                model = activeView.get('model');
            
            //don't allow the save until the lists have loaded
            if(!activeView.get('loaded')==true){
                return;
            }
            
            this.handleSave(e,{
                subjectId: activeView.get('personId'),
                subjectType: 'PERSON',
                securityBodyIdTypeVOList: activeView.getSelectedSecurityBodies()
            },
            options).then(function(){
              Y.fire('contentContext:update', {});
            });
        },
        updateSecurityRecord:function(e, options){
            // Get the view and the model
            var activeView = this.get('activeView'),
                model = activeView.get('model');
            
            //set the url
            model.url = activeView.get('updateUrl');
            
            this.saveSecurityRecord(e, options);
        },
        // Views that this dialog supports
        views: {
            addPersonLock: {
                type: Y.app.person.lock.PersonLockAddView,
                buttons: [{
                    name: 'proceedButton',
                    labelHTML: '<i class="fa fa-check"></i> Proceed',
                    action: function(e) {
                        e.preventDefault();
                        // show the mask
                        this.showDialogMask();

                        // Get the view
                        var activeView = this.get('activeView'),
                            selectedLockType = activeView.get('container').one('input[name=personLockType]:checked');

                        // fire saved event against the view
                        this.fire('selectedLockType', {
                            selectedLockType: selectedLockType.get('value'),
                            personId: activeView.get('personId')
                        });
                    },
                    disabled: true
                }]
            },
            addAccessAllowedList: {
                type: Y.app.person.lock.SecurityAccessRecordView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'saveSecurityRecord',
                    disabled: true
                }]
            },
            addAccessDeniedList: {
                type: Y.app.person.lock.SecurityAccessRecordView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: 'saveSecurityRecord',
                    disabled: true
                }]
            },
            editAccessAllowedList: {
                type: Y.app.person.lock.SecurityAccessRecordView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        this.updateSecurityRecord(e, {
                            transmogrify: Y.usp.security.UpdateSecurityAccessAllowedRecord
                        });
                    },
                    disabled: true
                }]
            },
            editAccessDeniedList: {
                type: Y.app.person.lock.SecurityAccessRecordView,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Save',
                    action: function(e) {
                        this.updateSecurityRecord(e, {
                            transmogrify: Y.usp.security.UpdateSecurityAccessDeniedRecord
                        });
                    },
                    disabled: true
                }]
            },
            deleteAccessAllowedList: {
                type: Y.app.person.lock.SecurityMessageView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: function(e) {
                        // Get the view and the model
                        var view = this.get('activeView'),
                            model = view.get('model'),
                            labels = view.get('labels');

                        // set the url before saving the model
                        model.url = view.get('deleteUrl');
                        
                        this.handleSave(e,{},{
                            transmogrify: Y.usp.security.SecurityAccessAllowedRecord
                        }).then(function(){
                          Y.fire('contentContext:update', {});
                        });
                    },
                    disabled: true
                }, {
                    section: Y.WidgetStdMod.FOOTER,
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary:true,
                    action: 'handleCancel',
                    disabled: true
                }]
            },
            removeAccessDeniedList: {
                type: Y.app.person.lock.SecurityMessageView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Yes',
                    action: function(e) {
                        // Get the view and the model
                        var view = this.get('activeView'),
                            model = view.get('model'),
                            labels = view.get('labels');

                        // set the url before issuing the delete
                        model.url = view.get('deleteUrl');

                        this.handleRemove(e).then(function(){
                          Y.fire('contentContext:update', {});
                        });
                    },
                    disabled: true
                }, {
                    name: 'noButton',
                    labelHTML: '<i class="fa fa-times"></i> No',
                    isSecondary:true,
                    action: 'handleCancel',
                    disabled: true
                }]
            },
            recordLockOverride: {
                type: Y.app.person.lock.SecurityMessageView,
                buttons: [{
                    name: 'yesButton',
                    labelHTML: '<i class="fa fa-check"></i> Continue',
                    action: function(e,subject, url) {
                        e.preventDefault();
                        // show the mask
                        this.showDialogMask();

                        var type = this.get('activeView').get('type'),
                            id = this.get('activeView').get('id'),
                            url = this.get('activeView').get('url'),
                            view = this.get('activeView');

                        Y.io(Y.Lang.sub(url, {
                            id: parseInt(id),
                            type: type}), {
                            method : 'PUT',
                            on: {
                                success: function(id, e) {
                                    view.fire('lockRemoved', {});
                                },
                                failure: function(error) {
                                    this.hideDialogMask();
                                    view.fire('error', {error: 'could not override the lock for this record.'});
                                }.bind(this)
                            }
                        });
                    },
                    disabled: false
                }]
            },
        }

    }, {
        ATTRS: {
            width: {
                value: 700
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base', 
        'yui-later',
        'view',
        'usp-view',
        'app-dialog',
        'tabview',
        'model-transmogrify', 
        'handlebars-helpers',
        'handlebars-personlock-templates',
        'picklist',
        'usp-security-SecurityBodyIdName',
        'usp-security-UpdateSecurityAccessAllowedRecord', 
        'usp-security-UpdateSecurityAccessDeniedRecord',
        'usp-security-SecurityAccessAllowedRecord',
        'usp-security-SecurityAccessDeniedRecord',
        'promise'
        
    ]
});