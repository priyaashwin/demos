YUI.add('person-output-document-dialog-views', function (Y) {
    var L = Y.Lang, Micro = Y.Template.Micro;
    var OPTION_TEMPLATE=Micro.compile('<option value="<%=this.id%>" data-type="<%=this.type%>"><%=this.name%></option>');
    Y.namespace('app.person').PersonOutputDocumentView = Y.Base.create('personOutputDocumentView', Y.app.output.BaseOutputDocumentView, [], {
        initializer: function () {
            this._evtHandlers = [
                this.get('container').delegate('change', this._setButtonState, '#outputPersonList', this)
            ];

        },
        render: function () {
            //call into superclass to render
            Y.app.person.PersonOutputDocumentView.superclass.render.call(this);

            var outputPersonList = this.get('container').one('#outputPersonList');
            if (!!outputPersonList) {
                Y.Promise.all([this._getPersonPersonRelationships(), this._getPersonOrganisationRelationships()]).then(function (data) {
                    this._setupRelationshipsList(data);
                }.bind(this));
            }
            return this;
        },
        _getSelectedPersons: function (outputPersonList) {
            var outputIdList = [];
            var options = outputPersonList.get('options')._nodes;
            for (var option in options) {
                if (options[option].selected && !!options[option].value) {
                    var value = options[option].value;
                    var type = options[option].dataset.type;
                    outputIdList.push({
                        // No, I don't like this either. However because we're mixing Person and Organisation records there is a chance that the id will be the same.
                        // This means we have to use the identifier (ORG / PER) or have the chance of values clashing.
                        // Because we're sending the actual id here we need to "undo" that...
                        id: Number(value.slice(3)),
                        type: type
                    });
                }
            }

            return outputIdList;
        },
        getDocumentRequestModel: function () {
            var model = this.get('model'),
                outputPersonList = this.get('container').one('#outputPersonList');

            // The parent document request requires the id field to be 'subjectId'
            model.set('subjectId', Number(model.get('id')));
            model.set('subjectType', 'PERSON');

            // If we have the person list, generate multiple documents
            if (this.get('relationshipsListActive')) {
                this.set('documentType', 'PERSON_LETTER');
                var outputIdList = this._getSelectedPersons(outputPersonList);
                return new Y.usp.output.NewPersonLetterSubjectDocumentRequest({
                    url: this.get('generatePersonLetterURL'),
                    documentType: this.get('documentType'),
                    subjectId: Number(model.get('subjectId')),
                    subjectType: model.get('subjectType').toUpperCase(),
                    outputTemplateId: Number(this.get('selectedTemplate')),
                    outputFormat: this.get('selectedFormat'),
                    letterAddressees: outputIdList
                });

            }
            this.set('documentType', 'PERSON');
            // If only one person, call superclass method to generate the document request
            return Y.app.person.PersonOutputDocumentView.superclass.getDocumentRequestModel.call(this);
        },
        _getPersonPersonRelationships: function () {
            return new Y.Promise(function (resolve, reject) {
                var url = this.get('getRelationshipsListURL');
                var relationshipsList = new Y.usp.relationship.PaginatedPersonPersonRelationshipDetailsList({
                    url: url
                });

                relationshipsList.load(function (err, res) {
                    if (!err) {
                        resolve(relationshipsList);
                    } else {
                        reject(err);
                    }
                });
            }.bind(this));
        },
        _getPersonOrganisationRelationships: function () {
            return new Y.Promise(function (resolve, reject) {
                var url = this.get('getOrganisationsListURL');
                var organisationsList = new Y.usp.relationshipsrecording.PaginatedOrganisationPersonOrganisationRelationshipsResultList({
                    url: url
                });
                organisationsList.load(function (err, res) {
                    if (!err) {
                        resolve(organisationsList);
                    } else {
                        reject(err);
                    }
                });
            }.bind(this));
        },
        _setupRelationshipsList: function (data) {
            var relationshipsList = data[0].toJSON().concat(data[1].toJSON()),
                outputPersonList = this.get('container').one('#outputPersonList'),
                subjectName = this.get('model').get('name'),
                subjectId = this.get('model').get('id'),
                newList = [{
                    name: subjectName,
                    id: this.get('model').get('personIdentifier'),
                    type: "PERSON"
                }];
            Y.Array.each(relationshipsList, function (item) {
                if (item._type === 'OrganisationPersonOrganisationRelationshipsResult') {
                    newList.push({
                        name: item.organisationVO.name,
                        id: item.organisationVO.organisationIdentifier,
                        type: "ORGANISATION"
                    });
                } else if (item.roleAPersonId !== subjectId) {
                    newList.push({
                        name: item.roleAPersonVO.name + ' - ' + item.personPersonRelationshipTypeVO.roleAName,
                        id: item.roleAPersonVO.personIdentifier,
                        type: "PERSON"
                    });
                } else {
                    newList.push({
                        name: item.roleBPersonVO.name + ' - ' + item.personPersonRelationshipTypeVO.roleBName,
                        id: item.roleBPersonVO.personIdentifier,
                        type: "PERSON"
                    });
                }
            });
            outputPersonList.appendChild('<option value="" class="blank">Please choose</option>');
            newList.forEach(function (relationship) {
                outputPersonList.appendChild(OPTION_TEMPLATE(relationship));
            });
        },
        _setButtonState: function () {
            Y.app.person.PersonOutputDocumentView.superclass._setButtonState.call(this);

            var dialog = this.get('dialog'),
                outputPersonList = this.get('container').one('#outputPersonList'),
                idList = null;

            this._checkTemplateType(outputPersonList); // Switch the list on/off as appropriate

            if (!this.get('relationshipsListActive')) {
                return;
            }

            idList = this._getSelectedPersons(outputPersonList);
            if (idList.length < 1) {
                var generateButton = dialog.getButton('generateButton', Y.WidgetStdMod.FOOTER);
                if (generateButton) {
                    generateButton.set('disabled', true);
                }
            }
        },
        destructor: function () {
            if (this._evtHandlers) {
                Y.Array.each(this._evtHandlers, function (item) {
                    item.detach();
                });

                this._evtHandlers = null;
            }
        },
        _checkTemplateType: function (outputPersonList) {
            var templateId = this.get('selectedTemplate'),
                templateList = this.get('outputTemplates'),
                outputPersonListContainer = this.get('container').one('#outputPersonListContainer'),
                enable = false;

            if (!outputPersonList || !templateList) {
                return;
            }

            if (!!templateId) {
                // Is the the right context?
                templateId = parseInt(templateId);
                for (var template in templateList) {
                    if (templateList[template].id === templateId && (templateList[template].context === "PERSON_LETTER")) {
                        enable = true;
                        break;
                    }
                }
            }

            this.set('relationshipsListActive', enable);
            if (enable) {
                outputPersonListContainer.show();
            } else {
                outputPersonListContainer.hide();
            }
        }

    }, {
        ATTRS: {
            templateListURL: {
                value: ''
            },     
            relationshipsListActive: {
                value: false
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'output-document-dialog-views',
        'handlebars-helpers',
        'handlebars-person-templates',
        'usp-output-OutputTemplate',
        'promise',
        'usp-output-NewDocumentRequest',
        'usp-output-NewPersonLetterSubjectDocumentRequest',
        'usp-relationship-PersonPersonRelationshipDetails',
        'usp-relationshipsrecording-PersonOrganisationRelationshipsResult',
        'relationship-organisation-person-results',
        'usp-relationship-PersonOrganisationRelationship',
        'template-micro'
    ]
});