YUI.add('team-security-dialog', function(Y) {
  var A=Y.Array,
      Micro = Y.Template.Micro,
      O=Y.Object;
    
    var VALUES_KEY=Y.app.admin.security.profile.AccessLevelValues;
  
    Y.namespace('app.admin.team').AddUpdateSecurityDomainView = Y.Base.create('addUpdateSecurityDomainView', Y.usp.securityextension.SecurityDomainView, [], {
      template: Y.Handlebars.templates.securityDomainDialog
    });

   Y.namespace('app.admin.team').NewSecurityDomainForm = Y.Base.create("newSecurityDomainForm", Y.usp.securityextension.NewSecurityDomain, [Y.usp.ModelFormLink], {
        form: '#securityDomainForm'
    },{
      ATTRS:{
        active:{
          value:true
        }
      }
    });

     Y.namespace('app.admin.team').RemoveSecurityDomainForm = Y.Base.create("removeSecurityDomainForm", Y.usp.securityextension.SecurityDomain, [], {});
   
    Y.namespace('app.admin.team').UpdateSecurityDomainForm = Y.Base.create("updateSecurityDomainForm", Y.usp.securityextension.SecurityDomain, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
      form: '#securityDomainForm'
    });
    
    Y.namespace('app.admin.team').NewSecurityRecordTypeConfigurationForm = Y.Base.create("newSecurityRecordTypeConfigurationForm",  Y.usp.securityextension.NewSecurityRecordTypeConfiguration, [Y.usp.ModelFormLink], {
      form: '#securityRecordTypeConfiguration'
    });
    
    Y.namespace('app.admin.team').UpdateSecurityRecordTypeConfigurationForm = Y.Base.create("updateSecurityRecordTypeConfigurationForm", Y.usp.securityextension.SecurityRecordTypeConfiguration, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
      form: '#securityRecordTypeConfiguration'
    });
    
    Y.namespace('app.admin.team').RemoveSecurityRecordTypeConfigurationForm = Y.Base.create("removeSecurityRecordTypeConfigurationForm", Y.usp.securityextension.SecurityRecordTypeConfiguration, [], {});
    
    Y.namespace('app.admin.team').DuplicateSecurityRecordTypeConfigurationForm = Y.Base.create("duplicateSecurityRecordTypeConfigurationForm", Y.usp.securityextension.SecurityRecordTypeConfiguration, [Y.usp.ModelTransmogrify], {
      
    },{
      ATTRS:{
        copyFromId:{
          value:null
        }
      }
    });
    
    Y.namespace('app.admin.team').SecurityRecordTypeConfigurationMessageView = Y.Base.create('securityRecordTypeConfigurationMessageView', Y.usp.securityextension.SecurityRecordTypeConfigurationView, [], {
      template: Y.Handlebars.templates.securityMessageDialog
    });

    Y.namespace('app.admin.team').SecurityDomainMessageView = Y.Base.create('securityDomainMessageView', Y.usp.securityextension.SecurityDomainView, [], {
      template: Y.Handlebars.templates.securityMessageDialog
    });

    function RecordAccessLevelSliderValueClass(config) {
      //set of possible values are in the RecordAccessLevel enumeration
      this.values=Y.usp.security.enum.RecordAccessLevel.values;
      
      this.set('min', 0);
      this.set('max', this.values.length-1);
      this.set('minorStep', 1);
      this.set('majorStep', 1);
      
      this.setValue(config.value||0);
    }

    Y.augment(RecordAccessLevelSliderValueClass, Y.SliderValueRange);
    
    RecordAccessLevelSliderValueClass.ATTRS={
        value:{
          setter: '_convertValue'
        }
    };
        
    
    RecordAccessLevelSliderValueClass.prototype._convertValue=function(val){
      var v=val||0;
      if(typeof v !== 'number'){
        A.forEach(this.values, function(av, i){
          if(val===av.enumValue){
            v=i;
          }
        });
      }
      
      return this._setNewValue(v);
    }
    
    RecordAccessLevelSliderValueClass.prototype.getValue=function(){
      var v=this.get('value')||0;
      return this.values[v];
    };
    RecordAccessLevelSliderValueClass.prototype.setValue=function(val){  
      var v=val||0;
      if(typeof v !== 'number'){
        A.forEach(this.values, function(av, i){
          if(val===av.enumValue){
            v=i;
          }
        });
      }
      this.set('value', v);
    };
    
    var THUMB_TEMPLATE=Micro.compile('<span class="<%=this.thumbClass%>" aria-labelledby="<%=this.thumbAriaLabelId%>" aria-valuetext="" aria-valuemax="" aria-valuemin="" aria-valuenow="" role="slider" tabindex="0"><span class="thumb-grip"></span></span>');
    var RANGE_TEMPLATE=Micro.compile('<span class="{<%=this.railClass}"><span class="{<%=this.railMinCapClass}"></span><span class="{<%=this.railMaxCapClass}"></span></span>');
    var RANGE_VALUE=Micro.compile('<div style="top:<%=this.top%>px;" class="slider-tick"><span style="background-color:<%=this.colour%>;color:<%=this.fontColour%>" class="slider-tick-value pop-up-data" data-align="top" data-content="<%=this.label%>"><%=this.value%></span> <span class="slider-tick-label"><%=this.label%></span></div>');
    
    var RecordAccessLevelSlider = Y.Base.create('recordAccessLevelSlider', Y.SliderBase, [
        RecordAccessLevelSliderValueClass,
        Y.ClickableRail
    ],{
      initializer:function(){
        this.__RecordAccessSliderEvt=[
                                      this.after('slideEnd', this._syncThumbPosition, this),
                                      this.after('click', this._syncThumbPosition, this),
                                      this.after('thumbMove', this._syncAccessColour, this),
                                      ];
        
        this._accessColour=Y.Node.create('<div class="access-colour"></div>');
      },
      _syncAccessColour:function(){
        var value=this.get('value');
        var vEnum=this.getValue();
        var vKey=VALUES_KEY[vEnum.enumValue];
        var thumb = this.thumb;
        
        var thumbSize = thumb.getStyle( this._key.dim );
            thumbSize = parseFloat( thumbSize ) || 16;
            
        var pos = this._valueToOffset(value)+ (thumbSize / 2);
        this._accessColour.setStyle('height', pos+'px');
        
        var values=this.values;
        
        var railChildren=this.rail.all('.slider-tick'),
            railValueNode, railValue;

        railChildren.each(function(node, i){
          railValue=VALUES_KEY[values[i].enumValue];
          if(i<=value && (value===0||i>0)){
            node.addClass('selected');
          }else{
            node.removeClass('selected');
          }
        });
      },
      syncUI :function(){
        RecordAccessLevelSlider.superclass.syncUI.call(this);
        
        this.addAccessLevels();
        
        //add the access colour node
        this.rail.append(this._accessColour);
        //sync the color
        this._syncAccessColour();
      },
      addAccessLevels: function() {
        var values = this.values;
        var rail = this.rail;
        var thumb = this.thumb;

        var thumbSize = thumb.getStyle( this._key.dim );
            thumbSize = parseFloat( thumbSize ) || 16;

        var pos, valObj;
        
        for(var i = 0; i<values.length; i++) {
            pos = this._valueToOffset(i) + (thumbSize / 2);
            valObj=VALUES_KEY[values[i].enumValue]||{};
            rail.append(Y.Node.create(RANGE_VALUE({
              value:valObj.value,
              label:valObj.label,
              colour:valObj.colour,
              fontColour:valObj.fontColour,
              top:pos
            })));
        }
      },
      renderRange:function(){
        var minCapClass = this.getClassName( 'rail', 'cap', this._key.minEdge ),
            maxCapClass = this.getClassName( 'rail', 'cap', this._key.maxEdge );

        return Y.Node.create(RANGE_TEMPLATE({
          railClass: this.getClassName('rail'),
          railMinCapClass: minCapClass,
          railMaxCapClass: maxCapClass
       }));
      },
      renderThumb:function(){
        return Y.Node.create(THUMB_TEMPLATE({
           thumbClass: this.getClassName( 'thumb' ),
           thumbAriaLabelId: this.getClassName( 'label', Y.guid())
        }));
      },
      destructor:function(){
        if(this._accessColour){
          this._accessColour.destroy();
          
          delete this._accessColour;
        }
        this.__RecordAccessSliderEvt.forEach(function(handler) {
          handler.detach();
        });
        delete this.__RecordAccessSliderEvt;
      }
    },{
      CSS_PREFIX: 'yui3-slider',
      ATTRS:{
        length:{
          value:'200px'
        },
        axis:{
          value:'y'
        }
      }
    });
    
    Y.namespace('app.admin.security').RecordAccessLevelSliderView = Y.Base.create('recordAccessLevelSliderView', Y.View, [], {
      template:Micro.compile('<input type="hidden" value="<%=this.value%>" name="<%=this.name%>"/>'),
      initializer:function(){        
        this.slider=new RecordAccessLevelSlider({
          //set to default value (no access)
          value:this.get('value')
        });
        
        this.__sliderEvts=[
          this.slider.after('focusedChange', function(e){
            if(e.newVal){
              this.get('container').addClass('focused');
            }else{
              this.get('container').removeClass('focused');
            }
          }, this),          
          this.after('valueChange', function(e){
            this.slider.setValue(e.newVal);
          }, this),
          this.slider.after('valueChange', this._syncValue, this)
        ];
      },
      _syncValue:function(e){
        if(this._hiddenInput){
          this._hiddenInput.set('value', this.slider.getValue().enumValue);
        }
      },
      render:function(){
        var container=this.get('container');
        
        container.addClass('record-access-level-slider');
        
        this.slider.render(container);
        
        //render the template
        this._hiddenInput=Y.Node.create(this.template({
          value:this.slider.getValue().enumValue,
          name:this.get('name')
        }));
        
        container.append(this._hiddenInput);
        return this;
      },
      destructor:function(){
        this.__sliderEvts.forEach(function(handler) {
          handler.detach();
        });
        delete this.__sliderEvts;
        
        this.slider.destroy();
        delete this.slider;
        
        if(this._hiddenInput){
          this._hiddenInput.destroy();
          delete this._hiddenInput;
        }
      }
    },{
      ATTRS:{
        value:{
          value:'NONE'
        }
      }
    });
    
    Y.namespace('app.admin.team').AddUpdateSecurityRecordTypeConfigurationView = Y.Base.create('addUpdateSecurityRecordTypeConfigurationView', Y.usp.securityextension.SecurityRecordTypeConfigurationView, [], {
      template: Y.Handlebars.templates.securityRecordTypeConfigurationDialog,
      initializer:function(){
        var model=this.get('model');
        
        this.recordAccessLevelSlider=new Y.app.admin.security.RecordAccessLevelSliderView({
          name:'defaultAccessLevel',
          value:model.get('defaultAccessLevel')
        });
       
        
        model.after('load', function(){
          this.recordAccessLevelSlider.set('value', model.get('defaultAccessLevel'));
        }, this);
      },
      render:function(){
        //call superclass
        Y.app.admin.team.AddUpdateSecurityRecordTypeConfigurationView.superclass.render.call(this);
        
        //render the RecordAccessLevelSlider
        this.get('container').one('#securityRecordTypeConfiguration_defaultAccessLevel_slider').setHTML(this.recordAccessLevelSlider.render().get('container'));
               
        return this;
      },
      destructor:function(){
        this.recordAccessLevelSlider.destroy();
        delete this.recordAccessLevelSlider; 
      }
    
    });
    
    Y.namespace('app.admin.team').TeamSecurityDialog = Y.Base.create('teamSecurityDialog', Y.usp.app.AppDialog, [], {
        views: {
          addSecurityDomain: {
            type: Y.app.admin.team.AddUpdateSecurityDomainView,
            buttons: [{
                name: 'saveButton',
                labelHTML: '<i class="fa fa-check"></i> Save',
                action: 'handleSave',
                disabled: true
            }]
          },
          updateSecurityDomain: {
            type: Y.app.admin.team.AddUpdateSecurityDomainView,
            buttons: [{
                name: 'saveButton',
                labelHTML: '<i class="fa fa-check"></i> Save',
                action: function(e) {
                  this.handleSave(e, {}, {
                      transmogrify:  Y.usp.securityextension.UpdateSecurityDomain
                  });
                },
                disabled: true
            }]
          },
          removeSecurityDomain: {
            type: Y.app.admin.team.SecurityDomainMessageView,
            buttons: [{
              name: 'removeButton',
              labelHTML: '<i class="fa fa-check"></i> Remove',
              isPrimary:true,
              action: 'handleRemove',
              disabled: true
            }]
          },          
          addSecurityProfile: {
            type: Y.app.admin.team.AddUpdateSecurityRecordTypeConfigurationView,
            buttons: [{
                name: 'saveButton',
                labelHTML: '<i class="fa fa-check"></i> Save',
                action: 'handleSave',
                disabled: true
            }]
          },
          updateSecurityProfile: {
            type: Y.app.admin.team.AddUpdateSecurityRecordTypeConfigurationView,
            buttons: [{
                name: 'saveButton',
                labelHTML: '<i class="fa fa-check"></i> Save',
                action: 'handleSave',
                action: function(e) {
                  this.handleSave(e, {}, {
                      transmogrify:  Y.usp.securityextension.UpdateSecurityRecordTypeConfiguration
                  });
                },
                disabled: true
            }]
          },
          removeSecurityProfile: {
            type: Y.app.admin.team.SecurityRecordTypeConfigurationMessageView,
            buttons: [{
              name: 'removeButton',
              labelHTML: '<i class="fa fa-check"></i> Remove',
              isPrimary:true,
              action: 'handleRemove',
              disabled: true
            }]
          },
          duplicateSecurityProfile: {
            type: Y.app.admin.team.SecurityRecordTypeConfigurationMessageView,
            buttons: [{
              name: 'saveButton',
              labelHTML: '<i class="fa fa-check"></i> Duplicate',
              action: function(e) {
                this.handleSave(e, {}, {
                    transmogrify:  Y.usp.securityextension.NewSecurityRecordTypeConfigurationCopy
                });
              },
              disabled: true
            }]
          }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'app-dialog',
               'view',
               'model-form-link',
               'model-transmogrify',
               'handlebars-helpers',
               'handlebars-organisation-templates',
               'usp-securityextension-NewSecurityDomain',
               'usp-securityextension-SecurityDomain',
               'usp-securityextension-UpdateSecurityDomain',
               'usp-securityextension-NewSecurityRecordTypeConfiguration',
               'usp-securityextension-SecurityRecordTypeConfiguration',
               'usp-securityextension-UpdateSecurityRecordTypeConfiguration',
               'usp-securityextension-NewSecurityRecordTypeConfigurationCopy',
               'slider-base',
               'slider-value-range',
               'usp-security-record-access-level-enumeration',
               'template-micro',
               'clickable-rail',
               'access-level-values']
});