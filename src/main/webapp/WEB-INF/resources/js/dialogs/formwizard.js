YUI.add('form-wizard-dialog-views', function(Y) {
    var A = Y.Array,
        O = Y.Object,
        L=Y.Lang,
        
        extractWarnings=function(modelData){
            var personWarningsDisplay = [],
            personWarningsAssigned = null;
            
            if (modelData.personWarnings !== null && modelData.personWarnings.length > 0) {
                personWarningsAssigned = modelData.personWarnings[0].personWarningAssignmentVOs;
            }

            A.each(O.values(Y.secured.uspCategory.person.warnings.category.getActiveCodedEntries(null, 'write')), function(activeWarning) {
                var warningDisplay = {
                    code: activeWarning.code,
                    name: activeWarning.name,
                    id: activeWarning.id
                };
    
                if (personWarningsAssigned !== null) {
                    A.each(personWarningsAssigned, function(assignedWarning) {
                        if (activeWarning.code === assignedWarning.codedEntryVO.code) {
                            warningDisplay.checked = true;
                        }
                    });
                }
    
                personWarningsDisplay.push(warningDisplay);
    
            });
            
            return personWarningsDisplay;
        },
        
        addEditWarningSave = function(e){
        	e.preventDefault();
        	this.showDialogMask();		//show the mask
            // Get the view and the model
            var view=this.get('activeView'),
                container=view.get('container'),
                model=view.get('model'),
                warningAssignmentIds=model.get('warningAssignmentIds');
            
            if(model.get('personWarnings').length == 0){ 
                // add warning
                var newWarningForm = new Y.usp.person.NewPersonWarning({
                    url:view.get('addURL')
                });
                  newWarningForm.setAttrs({
                    warningAssignmentIds: warningAssignmentIds,
                    note: L.trim(container.one('#note').get('value'))
                },{
                    //don't trigger the onchange listener as we will re-render after save
                    silent:true
                }); 
                  
                newWarningForm.save(
                        Y.bind(function(err, response) {
                            //hide the mask
                            this.hideDialogMask();
                            if (err === null) {
                            	this.hide();

                                //now fire the success message
                                Y.fire('infoMessage:message', {message:'Warnings successfully updated'});
                                
                                //ensure the context updates
                                Y.fire("contentContext:update",{data:{noOfPersonWarnings:noOfWarningSelected}});
                                
                                //update the graph, if there is one (the fired event is also in the list view
                                //which doesn't need to refresh, hence the 'graphOnly' boolean.)
                                Y.fire('relationship:dataChanged', {
	                          		graphOnly: true,
	                          		newRelationshipId: model.get('id')
	                          	});  
                            }
                        }, this));
            }else{                  
                // edit warning
                var existingWarningId = model.get('personWarnings')[0].id;
                
                var updateWarningForm = new Y.usp.person.UpdatePersonWarning({
                    id:existingWarningId,
                    url: view.get('updateURL')
                });
                
                updateWarningForm.setAttrs({
                    warningAssignmentIds: warningAssignmentIds,
                    note: L.trim(container.one('#note').get('value')),
                    objectVersion: model.get('personWarnings')[0].objectVersion
                },{
                    //don't trigger the onchange listener as we will re-render after save
                    silent:true
                });
                updateWarningForm.save(
                        Y.bind(function(err, response) {   
                            //hide the mask
                            this.hideDialogMask();
                            if (err === null) {       
                                this.hide();
                          
                              //now fire the success message
                              Y.fire('infoMessage:message', {message:'Warnings successfully updated'});
                              
                              //update context
                              Y.fire("contentContext:update",{data:{noOfPersonWarnings:noOfWarningSelected}});
                              
                              //update the graph, if there is one.
                              if(Y.one('#diagram')) {
                            	  Y.fire('relationship:dataChanged', {
                            		  newRelationshipId: model.get('id')
                            	  });  
                              }
                            }
                        }, this));
            }
        },        
        
        addEditWarningButtons=[{
                action: function(e){
                    e.preventDefault();            
                    //clear warnings
                    this.get('activeView').clearWarnings();
                    //ensure save button is enabled
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                    
                },
                section: Y.WidgetStdMod.FOOTER,
                name:      'clearButton',
                labelHTML: 'Clear all warnings',
                classNames:'pure-button-text pull-left', 
                disabled: true
            },{
                action: addEditWarningSave,
                section: Y.WidgetStdMod.FOOTER,
                name:      'saveButton',
                labelHTML: '<i class="fa fa-check"></i> save',
                classNames:'pure-button-primary', 
                disabled: true
            }];
 
    // Extend the USP PersonWarningView        
    Y.namespace('app.person').ViewPersonWarning = Y.Base.create('ViewPersonWarning', Y.usp.person.PersonWarningView, [], {
        //bind template 
        template: Y.Handlebars.templates["personWarningViewDialog"],
        configureWarnings:function(){
            var model=this.get('model');
            
            //update warnings attribute
            model.setAttrs({
                warnings: extractWarnings(model.toJSON())
            },{silent:true});
        },
        render:function(){
            //ensure model is up-to-date
            this.configureWarnings();
            
            //call superclass render
            Y.app.person.ViewPersonWarning.superclass.render.call(this);
        }
    });

    // Extend the USP PersonWarningView
    Y.namespace('app.person').AddEditPersonWarning = Y.Base.create('AddEditPersonWarning', Y.usp.person.PersonWarningView, [], {

    	template: Y.Handlebars.templates["formWizardDialog"],
        
    	events:{
                     
        },
        
        initializer:function(){                        
           
        },
        
        destructor:function(){
            
        },
        
        initPicklist: function() {
        	
        	var container = this.get("container");
        	
        	// Define some data ... note, not required to be in { text:, value: } format
        	var jsData = [
				{ cname:'White',  	chex:'#FFFFFF' },	
				{ cname:'Silver', 	chex:'#C0C0C0' },	
				{ cname:'Navy', 	chex:'#000080' },	
				{ cname:'Fuchsia',      chex:'#FF00FF' },	
				{ cname:'Purple', 	chex:'#800080' }
		   ];
        	
        	// Create a tooltip for the Options ...
 		   for(var i=0; i<jsData.length; i++)
 			jsData[i].ctitle = 'Color ' + jsData[i].cname + ' (Hex ' + jsData[i].chex + ')';
 		 
 		 
	 		//
	 		// Create the PickList ... using cssButtons 
	 		//
 		   var myPL = new Y.PickList( {
 			srcNode: 	'#picklist',
 			template:	'#picklist-ui-template',
 			buttonType : 	'cssbutton',
 			optionsMap : 	{ text:'cname', value:'chex', title:'ctitle' },
 			options : 	jsData
 		   });
 		   
 		   
 		 
 		   myPL.render(container.one('#picklist')); 
        },
        
        render: function() {
            
        	
            var template = Y.Handlebars.compile(Y.one('#formWizardDialog').getHTML());
            
            
            this.template = template;

            //call superclass render
            Y.app.person.AddEditPersonWarning.superclass.render.call(this);
            
            this.initPicklist();

    		 
    		
        }
    },{
        ATTRS:{
            warningAssignmentIds:{
                valueFn:function(){
                    return [];
                },
                setter:function(val){
                    //set into model
                    this.get('model').setAttrs({
                        'warningAssignmentIds':val
                    }, {
                        silent:true
                    });                    
                }
            },
            saveEnabled:{
                value:true
            }
        }
    });
    
    
    
    
    
    //Person Warning Dialog - with MultiPanelModelErrorsPlugin plugin
    Y.namespace('app').PersonWarningDialog= Y.Base.create('personWarningDialog', Y.usp.MultiPanelPopUp, [], {
        initializer:function(){
            //plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        destructor:function(){
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        views:{
            viewPersonWarning:{
                 type:Y.app.person.ViewPersonWarning
             },
             addEditPersonWarning:{
                 type:Y.app.person.AddEditPersonWarning,
                 buttons:addEditWarningButtons
             }
         }
    },{
        CSS_PREFIX: 'yui3-panel',
        ATTRS:{
            width:{
                value:700
            },
            buttons:{
                value:[{
                    name: 'cancelButton',
                    labelHTML: '<i class="fa fa-times"></i> Cancel',
                    classNames: 'pure-button-secondary',
                    action: function(e) {
                        e.preventDefault();
                        this.hide();
                    },
                    section: Y.WidgetStdMod.FOOTER
                },'close']
            }
        }
    });
    
}, '0.0.1', {
    requires: ['yui-base', 
               'multi-panel-popup',
               'multi-panel-model-errors-plugin',
               'model-form-link', 
               'gallery-textarea-counter',
               'gallery-picklist',
               'usp-person-PersonWarning', 
               'usp-person-PersonWithWarning',
               'usp-person-NewPersonWarning',
               'usp-person-UpdatePersonWarning',
               'secured-categories-person-component-Warnings',
               'handlebars-helpers', 
               'handlebars-person-templates']
});