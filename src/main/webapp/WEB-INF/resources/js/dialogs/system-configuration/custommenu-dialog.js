

YUI.add('custommenu-dialog-views', function(Y) {

	var A = Y.Array;

	var HEADER_HTML = '<h3 id="labelledby" aria-describedby="describedby" class="iconic-title">\
		<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> {title}\
		</h3>';


	var EVENT_REFRESH_RESULTS = 'customMenuItems:refreshResults';


	var updateCustomMenuItemSubmit = function(e){
		// Get the view and the model
        var view=this.get('activeView'),
        model=view.get('model'), labels = view.get('labels'), SYSTEM_CONFIG_URL = view.get('SYSTEM_CONFIG_URL');

        var container = this.get('boundingBox');
        e.preventDefault();

        // prevent duplicate save on double click
        //show the mask
        this.showDialogMask();

        var menuItemEnabledFlag = container.one('#menuActive-input').get('checked');

        this.handleSave(e, {
          active:menuItemEnabledFlag
        }, {
            transmogrify: Y.usp.custommenu.UpdateCustomMenuItem
        }).then(function(){
            //Y.fire(EVENT_REFRESH_RESULTS, {}); - no point the window is going to re-load
            window.location = SYSTEM_CONFIG_URL;
        });
};



Y.namespace('app.custommenu').MultiPanelPopUp = Y.Base.create('customMenuMultiPanelPopUp', Y.usp.app.AppDialog, [], {
		views: {
			editCustomMenuItemForm: {
				type: Y.app.custommenu.EditCustomMenuItemView,
				headerTemplate: Y.Lang.sub(HEADER_HTML, { title: 'Edit custom menu' }),
				buttons: [{
	        	   	action: 	updateCustomMenuItemSubmit,
	        	   	section: 	Y.WidgetStdMod.FOOTER,
					name:      'saveButton',
					labelHTML: '<i class="fa fa-check"></i> save',
					classNames:'pure-button-primary'
				}]
			}
		}

	},{
		ATTRS: {
			width: {
				value: 700
			}
		}
	});



}, '0.0.1', {
	  requires: ['yui-base',
	             'app-dialog',
	             'form-util',
	             'event-custom',
	             'app-custom-menu-form-edit',
	             'usp-custommenu-UpdateCustomMenuItem']
	});
