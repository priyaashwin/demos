YUI.add('image-dialog-views', function (Y) {
	
	
	var ImageUploadView=Y.Base.create('ImageUploadView', Y.View,[],{
		//bind template	
		//template: Y.Handlebars.templates["imageUploadView"],
		events: {
            '#upload_image': {change: 'fileChanged'}
		},	
		render: function() {
			//TODO: template should be supplied elsewhere really
			var template=Y.Handlebars.compile(Y.one('#imageUploadView').getHTML());			
			this.get('container').setHTML(template({
				labels:this.get('labels'),
				otherData:this.get('otherData')
			}));
		},
		
		fileChanged: function(e){
			this.fire('fileChange',{available:(e.target.get('value')?true:false)});
		},
		
		destroy: function(){
			
		},
		getNode: function(nodeId) {
			var container=this.get('container');
			return container.one('#upload_'+nodeId);
		}
		
		
	},{
        // Specify attributes and static properties for your View here.
        ATTRS: {           		
        }
	});
	

	Y.namespace('app.image').ImageUploadView=ImageUploadView;
	
}, '0.0.1', {
    requires: ['yui-base','event-custom','handlebars-helpers','view-rich-text']
});