YUI.add('subject-contact-views', function (Y) {

    var A = Y.Array,

        ACTION_ADD_CONTACT = 'addContact',

        ACTION_EDIT_REMOVE_CONTACT = 'editRemoveContact',

        EVENT_SHOW_ORG_DIALOG = 'organisation:showDialog',

        CONTEXT_UPDATE_EVENT = 'contentContext:update',

        EVENT_SHOW_PER_DIALOG = 'person:showDialog';

    var NewSubjectContactView = Y.Base.create('newSubjectContactView', Y.usp.contact.NewContactView, [], {

        events: {
            '#contactFixedType-input': {
                change: 'aftercontactFixedType'
            },
            'input[name="contactVerified"]': {
                change: 'updateVerified'
            }
        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#' + inputId + '-input');
        },

        getLabel: function (id) {
            return this.get('container').one('#' + id + '-label');
        },

        getWrapper: function (inputId) {
            var container = this.get('container');
            return container.one('#' + inputId + '-wrapper');
        },

        initlists: function (field) {
            var activeCodedEntries;
            if (field === 'contactType') {
                activeCodedEntries = Y.uspCategory.contact.contactType.category.getActiveCodedEntries();
            } else if (field === 'contactUsage') {
                activeCodedEntries = Y.uspCategory.contact.contactUsage.category.getActiveCodedEntries();
            } else if (field === 'socialMediaType') {
                activeCodedEntries = Y.uspCategory.contact.socialMediaType.category.getActiveCodedEntries();
            } else if (field === 'telephoneType') {
                activeCodedEntries = Y.uspCategory.contact.telephoneType.category.getActiveCodedEntries();
            }

            // Add the code as a property in the coded entry, used to set the
            // option value
            Y.Object.each(activeCodedEntries, function (fld, key) {
                fld.code = key;
            });
            // Array of coded entries to pass to Y.FUtil.setSelectOptions
            var activeCodedEntriesArray = Y.Object.values(activeCodedEntries);

            Y.FUtil.setSelectOptions(this.getInput(field), activeCodedEntriesArray, null, null, true, true, 'code');

        },

        initContactVerifiedDate: function (container) {

            this.updateVerified(null);

            var node = container.one('#contactVerifiedDate-input');

            if (node) {
                this.contactVerifiedDateCalendar = new Y.usp.CalendarPopup({
                    inputNode: node,
                    pickerStyle: 'Date'
                }).render();
            }
        },

        updateVerified: function (e) {

            var checked = this.get('container').one('input[name="contactVerified"]').get('checked');
            var verifiedDate = this.get('container').one('#verifiedDate');
            if (!checked) {
                verifiedDate.setAttribute("style", "display:none");
                verifiedDate.setAttribute('disabled', 'disabled');
            } else {
                verifiedDate.removeAttribute('disabled');
                verifiedDate.setStyle('display', 'block');
            }
        }
    });

    var EditSubjectContactView = Y.Base.create('editSubjectContactView', Y.usp.contact.ContactView, [], {

        events: {
            'a.removeLink': {
                click: '_removeContact'
            },
            'input[name="contactVerified"]': {
                change: 'updateVerified'
            }
        },

        // bind template
        template: Y.Handlebars.templates["personContactEditRemoveDialog"],

        destructor: function () {
            this._destroyDates();
        },

        _destroyDates: function () {
            for (var i = 0; i < this.contactVerifiedDateCalendars.length; i++) {
                this._destroyDate(this.contactVerifiedDateCalendars[i]);
            }
        },

        _destroyDate: function (calendar) {
            calendar.destroy();
            delete calendar;
        },

        render: function () {
            var container = this.get('container'),
                html = this.template(Y.merge(this.get('templateBaseData'), {
                    labels: this.get('labels'),
                    targetPersonId: this.get('contextId'),
                    targetPersonName: this.get('contextName'),
                    canRemoveContact: this.get('canRemoveContact') ? '' : 'none;'
                }));

            // Render this view's HTML into the container element.
            container.setHTML(html);
            var telephoneContacts = this.get('telephoneContacts');
            if (telephoneContacts != null) {
                this.renderVerificationDates(container, telephoneContacts);
            }
            var mobileContacts = this.get('mobileContacts');
            if (mobileContacts != null) {
                this.renderVerificationDates(container, mobileContacts);
            }
            var faxContacts = this.get('faxContacts');
            if (faxContacts != null) {
                this.renderVerificationDates(container, faxContacts);
            }
            var emailContacts = this.get('emailContacts');
            if (emailContacts != null) {
                this.renderVerificationDates(container, emailContacts);
            }
            var socialMediaContacts = this.get('socialMediaContacts');
            if (socialMediaContacts != null) {
                this.renderVerificationDates(container, socialMediaContacts);
            }
            var webContacts = this.get('webContacts');
            if (webContacts != null) {
                this.renderVerificationDates(container, webContacts);
            }
            return this;
        },

        updateVerified: function (e) {
            e.preventDefault();
            var verifiedNode = e.target._node;
            var id = verifiedNode.dataset.id;
            var container = this.get('container');
            var verifiedCheckBoxNodeId = '#contactVerified-input_'.concat(id);
            var verifiedCheckBoxNode = container.one(verifiedCheckBoxNodeId);

            var verifiedDateNodeId = '#contactVerifiedDate-input_'.concat(id);
            var verifiedDateNode = container.one(verifiedDateNodeId);

            var verifiedDate = container.one('#verifiedDate_'.concat(id));

            if (verifiedDateNode) {
                var dateAsNumber = Y.USPDate.parseDate(verifiedDateNode.get('value'));

                if (dateAsNumber == null) {
                    verifiedDateNode.set('value', null);
                }

                this.syncVisibilityOfVerifiedDate(verifiedCheckBoxNode._node, verifiedDate, verifiedDateNode);
            }
        },

        renderVerificationDates: function (container, contacts) {
            if (contacts && contacts.length) {
                var contact, verifiedCheckBoxNodeId, verifiedCheckBoxNode, verifiedDateNodeId, verifiedDateNode,
                    verifiedDateLabelId, verifiedDateLabelNode;

                for (var i = 0; i < contacts.length; i++) {
                    verifiedCheckBoxNodeId = '#contactVerified-input_'.concat(contacts[i].id);
                    verifiedCheckBoxNode = container.one(verifiedCheckBoxNodeId);
                    verifiedDateNodeId = '#contactVerifiedDate-input_'.concat(contacts[i].id);
                    verifiedDateNode = container.one(verifiedDateNodeId);

                    var verifiedDate = this.get('container').one('#verifiedDate_'.concat(contacts[i].id));

                    if (verifiedDate) {
                        this.syncVisibilityOfVerifiedDate(verifiedCheckBoxNode._node, verifiedDate, verifiedDateNode);
                        this.contactVerifiedDateCalendars.push(new Y.usp.CalendarPopup({
                            inputNode: verifiedDateNode,
                            pickerStyle: 'Date'
                        }).render());
                    }
                }
            }
        },

        _removeContact: function (e) {
            e.preventDefault();
            e.target.setStyle("visibility", "hidden");
            this.get('container').one('#contactLabel_' + e.target.getAttribute('data-remove-id')).addClass('disabled');
            var contactInput = this.get('container').one('#contact_' + e.target.getAttribute('data-remove-id'));
            contactInput.setAttribute("data-remove-num", true);
            contactInput.set("value", "");
            contactInput.set("disabled", true);
            this.get('container').one('#contactVerifiedDate-label_' + e.target.getAttribute('data-remove-id')).addClass('disabled');
            var verifiedDateInput = this.get('container').one('#contactVerifiedDate-input_' + e.target.getAttribute('data-remove-id'));
            verifiedDateInput.setAttribute("data-remove-num", true);
            verifiedDateInput.set("value", "");
            verifiedDateInput.set("disabled", true);
            var verifiedInput = this.get('container').one('#contactVerified-input_' + e.target.getAttribute('data-remove-id'));
            verifiedInput.setAttribute("data-remove-num", true);
            verifiedInput.set("value", "");
            verifiedInput.set("disabled", true);
        },

        syncVisibilityOfVerifiedDate: function (verifiedCheckbox, verifiedDate, verifiedDateInput) {

            var checked = verifiedCheckbox ? verifiedCheckbox.checked : false;

            if (!checked) {
                verifiedDate.setAttribute("style", "display:none");
                verifiedDate.setAttribute('disabled', 'disabled');
                verifiedDateInput.nodeValue = null;
            } else {
                verifiedDate.removeAttribute('disabled');
                verifiedDate.setStyle('display', 'block');
            }
        },
        /**
         * @method _getContactTypeAndSubtype
         * @param 
         * @description
         * 	example for Mobile:
         * 		type = 'TelephoneContacts'
         * 		subTypes = 'Mobile'
         * 		subTypeKey = 'telephoneType'
         * 
         * 	example for Telephone Contacts (these exclude mobile)
         */
        _getContactTypeAndSubtypes: function (type, subTypes, subTypeKey) {

            var contacts = [];

            if (subTypes && Y.Lang.isString(subTypes)) {
                subTypes = [subTypes];
            }

            A.each(this.get('model').toJSON(), function (contact) {
                if (contact._type === type) {
                    A.each(subTypes, function (subType) {
                        if (subType === contact[subTypeKey]) {
                            contacts.push(contact);
                        }
                    });
                }
            });
            return contacts;
        },

        _getContactByType: function (type) {

            var contacts = [];

            A.each(this.get('model').toJSON(), function (contact) {
                if (contact._type === type) {
                    contacts.push(contact);
                }
            });

            return this._groupContacts(contacts);
        },

        _groupContacts: function (contacts) {
            return contacts;
        },

        // Specify attributes and static properties for your View here.
    }, {
        ATTRS: {
            /**
             * @attribute emailContacts
             */
            emailContacts: {
                getter: function (value) {
                    return this._getContactByType('EmailContact');
                }
            },

            /**
             * @attribute webContacts
             */
            webContacts: {
                getter: function (value) {
                    return this._getContactByType('WebContact');
                }
            },

            /**
             * @attribute
             */
            faxContacts: {
                getter: function (value) {
                    return this._getContactTypeAndSubtypes('TelephoneContact', 'FAX', 'telephoneType');
                }
            },

            /**
             * @attribute
             */
            mobileContacts: {
                getter: function (value) {
                    return this._getContactTypeAndSubtypes('TelephoneContact', 'MOBILE', 'telephoneType');
                }
            },

            /**
             * @attribute
             */
            socialMediaContacts: {
                getter: function (value) {
                    return this._getContactByType('SocialMediaContact');
                }
            },

            /**
             * @attribute
             */
            telephoneContacts: {
                getter: function (value) {
                    return this._getContactTypeAndSubtypes('TelephoneContact', ['LANDLINE', 'TEXTPHONE', null], 'telephoneType');
                }
            },

            templateBaseData: {
                getter: function (value) {
                    return {
                        modelData: this.get('model').toJSON(),
                        emailContacts: this.get('emailContacts'),
                        webContacts: this.get('webContacts'),
                        faxContacts: this.get('faxContacts'),
                        mobileContacts: this.get('mobileContacts'),
                        socialMediaContacts: this.get('socialMediaContacts'),
                        telephoneContacts: this.get('telephoneContacts'),
                        webContacts: this.get('webContacts'),
                        codedEntries: this.get('codedEntries')
                    }

                }

            }

        }
    });

    Y.namespace('app.contact').NewSubjectContactView = NewSubjectContactView;
    Y.namespace('app.contact').EditSubjectContactView = EditSubjectContactView;

}, '0.0.1', {
    requires: ['yui-base',
        'calendar-popup',
        'usp-contact-Contact',
        'usp-contact-NewContact',
        'usp-contact-NewEmailContact',
        'usp-contact-NewWebContact',
        'usp-contact-NewSocialMediaContact',
        'usp-contact-NewTelephoneContact'
    ]
});