YUI.add('casenote-filter', function(Y) {
    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.casenote').CasenoteFilterModel = Y.Base.create('casenoteFilterModel', Y.app.entry.BaseEntryFilterModel, [Y.app.attachment.EntityWithAttachmentFilterModelMixin], {},{
        ATTRS: {
            subjectSeenIndicatorVisible: {
                value: true,
                getter: function() {
                    var showGroupFilter = this.get('showGroupFilter');
                    if(!showGroupFilter) return false;
                    return true;
                }
            },
            subjectSeenIndicatorList : {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: [],
            },
        }
    });

    Y.namespace('app.casenote').CasenoteFilterView = Y.Base.create('casenoteFilterView', Y.app.entry.BaseEntryFilterView, [Y.app.attachment.EntityWithAttachmentFilterViewMixin], {
        getFilterItemsModel: function(id) {
        	var subject = this.get('subject'), 
        		url = L.sub(this.get('filterItemsUrl'), {
        			id: id
        		});
        	
        	if(subject.subjectType === 'person'){
        		url = url + '?personId=' + subject.subjectId;
        	}
        	
            return new Y.usp.relationshipsrecording.CaseNoteFilterItems({
                url: url
            });
        },
        getRecordModel: function(subjectId, subjectType) {
            return new Y.usp.casenote.CaseNoteRecord({
                url: L.sub(this.get('caseNoteRecordUrl'), {
                    subjectType: subjectType
                }),
                id: subjectId
            });
        }
    }, {
        ATTRS: {
            entryTypes: {
                value: Y.secured.uspCategory.casenote.entryType.category.codedEntries
            },
            entrySubTypes: {
                value: Y.secured.uspCategory.casenote.entrySubType.category.codedEntries
            },
            caseNoteRecordUrl: {}
        }
    });


    Y.namespace('app.casenote').CasenoteFilterContext = Y.Base.create('casenoteFilterContext', Y.app.entry.BaseEntryFilterContext, [], {});

    Y.namespace('app.casenote').CasenoteFilter = Y.Base.create('casenoteFilter', Y.app.entry.BaseEntryFilter, [], {
        filterModelType: Y.app.casenote.CasenoteFilterModel,
        filterType: Y.app.casenote.CasenoteFilterView,
        filterContextType: Y.app.casenote.CasenoteFilterContext
    });
}, '0.0.1', {
    requires: ['yui-base',
        'entry-filter',
        'secured-categories-casenote-component-EntryType',
        'secured-categories-casenote-component-EntrySubType',
        'usp-relationshipsrecording-CaseNoteFilterItems',
        'usp-casenote-CaseNoteRecord',
        'attachment-filter'
    ]
});