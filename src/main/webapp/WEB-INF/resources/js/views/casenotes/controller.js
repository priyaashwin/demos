YUI.add('casenote-controller-view', function (Y) {
    'use-strict';

    var L = Y.Lang,
        O = Y.Object,
        E = Y.Escape;

    Y.namespace('app.casenote').CasenoteControllerView = Y.Base.create('casenoteControllerView', Y.app.entry.BaseEntryControllerView, [], {
        resultsListClass: Y.app.casenote.CasenoteFilteredResults,
        dialogClass: Y.app.casenote.CasenoteDialog,
        linkFromType: 'CASENOTE',
        dialogAddModelClass: Y.app.casenote.NewCaseNoteEntryForm,
        dialogViewModelClass: Y.app.casenote.UpdateCaseNoteEntryForm,
        dialogUpdateModelClass: Y.app.casenote.UpdateCaseNoteEntryForm,
        dialogNotifyModelClass: Y.app.casenote.UpdateNotifyCaseNoteEntryForm,
        initializer: function (config) {
            this.attachmentPopupDialog = new Y.app.casenote.attachment.AttachmentMultiPanelPopUp(Y.merge(config.attachmentDialogConfig, {
                zIndex: 1000
            })).addTarget(this).render();

            this.imagePopupDialog = new Y.app.casenote.image.ImageMultiPanelPopUp(Y.merge(config.imageDialogConfig, {
                zIndex: 1000
            })).addTarget(this).render();

            this._casenoteEvtHandlers = [
                this.on('*:uploadFile', this.showUploadFileDialog, this),
                this.on('*:addAttachment', this.showAddAttachmentDialog, this),
                this.on('*:viewAttachment', this.showViewAttachmentDialog, this),
                this.on('*:deleteAttachment', this.showDeleteAttachmentDialog, this),
                this.on('*:manageFiles', this.showManageFilesDialog, this),
                //not happy about this - but the way the dialogs are constructed - they are throwing global
                //events and there is no time to re-work it
                Y.on('attachments:dataChanged', this.results.reload, this.results),
                Y.on('upload:showDialog', this.handleShowImageDialog, this),
                this.on('uploadImageView:saved', this.handleImageSaved, this)
            ];
        },
        
        destructor: function () {
            this._casenoteEvtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._casenoteEvtHandlers;

            this.attachmentPopupDialog.removeTarget(this);
            this.attachmentPopupDialog.destroy();
            delete this.attachmentPopupDialog;

            if (this.currentEditor) {
                delete this.currentEditor;
            }

            this.imagePopupDialog.removeTarget(this);
            this.imagePopupDialog.destroy();
            delete this.imagePopupDialog;

        },
        showManageFilesDialog: function (e) {
            var permissions = e.permissions,
                record = e.record;
            var canReadDetail = record.hasAccessLevel('READ_DETAIL');
            if (permissions.canView && permissions.canViewAttachment && canReadDetail) {
                this._showViewDialog(record, 1);
            }
        },
        showUploadFileDialog: function (e) {
            var record = e.record,
                permissions = e.permissions;
            var canWrite = record.hasAccessLevel('WRITE');
            if (permissions.canUploadAttachment && canWrite) {
                this._showAddAttachmentDialog(record.get('id'));
            }
        },
        handleShowImageDialog: function (e) {
            var subject = this.get('subject'),
                subjectType = subject.subjectType,
                imageUploadModel, url,
                config = this.get('imageDialogConfig').views.uploadFile.config;

            //get the ckeditor from the event and store it locally 
            this.currentEditor = e.editor;

            url = L.sub(config.url, {
                subjectId: subject.subjectId,
                subjectType: subjectType
            });

            imageUploadModel = new Y.app.casenote.image.UploadSubjectImageForm({
                url: url,
                inline: true,
                subjectType: subjectType.toUpperCase()
            });

            this.imagePopupDialog.showView('uploadFile', {
                model: imageUploadModel
            });
        },
        handleImageSaved: function (e) {
            var config = this.get('imageDialogConfig').views.uploadFile.config;
            if (!this.currentEditor) {
                return;
            }
            var img = this.currentEditor.document.createElement('img');
            // Just as a point of interest all of the attributes have 
            // to be lower case (and YUI lower cases them if they're not) 
            img.setAttribute('src', L.sub(config.imageUrl, { id: e.id }));
            //hard coded to use temporary attachment
            img.setAttribute('data-class', 'TemporaryAttachmentVO');
            img.setAttribute('data-entity_id', e.id);
            this.currentEditor.insertElement(img);
        },
        showAddAttachmentDialog: function (e) {
            var isTemporaryAttachment = e.temporaryAttachment;

            this._showAddAttachmentDialog(e.id, isTemporaryAttachment);
        },
        _showAddAttachmentDialog: function (id, isTemporaryAttachment) {
            var attachmentModel,
                subject = this.get('subject'),
                subjectType = subject.subjectType,
                config = this.get('attachmentDialogConfig').views.uploadFile.config;

            if (!isTemporaryAttachment) {
                attachmentModel = new Y.app.casenote.attachment.UploadFileCaseNoteEntryForm({
                    url: L.sub(config.url, {
                        id: id
                    }),
                    labels: config.validationLabels,
                    inline: false
                });
            } else {
                attachmentModel = new Y.app.casenote.attachment.UploadSubjectTemporaryFileCaseNoteEntryForm({
                    url: L.sub(config.temporarySubjectAttachmentUrl, {
                        id: subject.subjectId,
                        subjectType: subjectType
                    }),
                    labels: config.validationLabels,
                    inline: false,
                    subjectType: subjectType.toUpperCase()
                });
            }
            this.attachmentPopupDialog.showView('uploadFile', {
                temporaryAttachment: isTemporaryAttachment,
                model: attachmentModel,
                otherData: {
                    subject: subject
                }
            });
        },
        showViewAttachmentDialog: function (e) {
            var attachmentModel,
                subject = this.get('subject'),
                permissions = this.get('permissions'),
                config = this.get('attachmentDialogConfig').views.viewAttachment.config,
                record = e.record;
            var canReadDetail = record.hasAccessLevel('READ_DETAIL');
            if (permissions.canViewAttachment) {
                attachmentModel = new Y.usp.casenote.CaseNoteEntryAttachment({
                    url: L.sub(config.url, {
                        id: e.id
                    })
                });

                this.attachmentPopupDialog.showView('viewAttachment', {
                    model: attachmentModel,
                    otherData: {
                        subject: subject
                    }
                }, {
                        modelLoad: true
                    });
            }
        },
        showDeleteAttachmentDialog: function (e) {
            var subject = this.get('subject'),
                permissions = this.get('permissions'),
                config = this.get('attachmentDialogConfig').views.deleteAttachment.config,
                record = e.record;
            // FIXME The reference to e.record is not always being set.
            var canWrite = record.hasAccessLevel('WRITE');
            if (permissions.canDeleteAttachment) {
                this.attachmentPopupDialog.showView('deleteAttachment', {
                    model: new Y.usp.casenote.CaseNoteEntryAttachment({
                        id: e.id,
                        url: config.url
                    }),
                    otherData: {
                        subject: subject
                    }
                }, {
                        modelLoad: true
                    }, function (view) {
                        //remove the cancel button
                        this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                    });
            }
        },
        buildChecklistMessageParameters: function (parameters) {
            return {
                entryState: E.html((parameters['CASENOTE_ENTRY_STATE'] || {}).displayValue),
                entryType: E.html((parameters['CASENOTE_ENTRY_TYPE'] || {}).displayValue),
            };
        },
        afterShowViewDialog: function (record, permissions) {
            var canWrite = record.hasAccessLevel('WRITE'),
                status = record.get('status'),
                isUpload = permissions.canUpload && status == 'DRAFT' && canWrite;
            // Unless we have special permissions
            if (permissions.canUploadAttachmentOnCompleteInstance && status == 'COMPLETE' && canWrite) {
                isUpload = true;
            }

            if (isUpload) {
                this.dialog.getButton('uploadFileButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            }
        },

    }, {
            ATTRS: {
                impactEnabled: {
                    value: false
                },
                notificationEnabled: {
                    value: false
                }
            }
        });
}, '0.0.1', {
        requires: ['yui-base',
            'entry-controller-view',
            'casenote-results',
            'casenote-dialog',
            'casenote-attachment-dialog',
            'casenote-output-document-dialog-views',
            'escape',
            'casenote-image-dialog'
        ]
    });