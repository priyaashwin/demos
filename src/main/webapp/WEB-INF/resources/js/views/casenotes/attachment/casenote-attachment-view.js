YUI.add('casenote-attachment-view', function(Y) {
  'use-strict';
  
  var A = Y.Array,
    L = Y.Lang,
    E = Y.Escape,
    USPFormatters = Y.usp.ColumnFormatters,
    USPTemplates = Y.usp.ColumnTemplates,
    APPFormatters = Y.app.ColumnFormatters,
    EVENT_REFRESH_RESULTS = 'attachments:dataChanged',
    DEFAULT_HTML_TEMPLATE = '<div class="casenote-file-view"></div>',
    IMAGE_HTML_TEMPLATE = '<div class="casenote-file-view">\
          <img src="{downloadUrl}" alt="{fileName}" height="75%"> \
      </div>',
    VIDEO_HTML_TEMPLATE = '<div class="casenote-file-view">\
          <video height="100%" controls>\
            <source src="{downloadUrl}" type="{contentType}">\
                   Your browser does not support the video tag.\
            </video>\
        </div>',
    AUDIO_HTML_TEMPLATE = '<div class="casenote-file-view">\
          <audio controls>\
            <source src="{downloadUrl}" type="{contentType}">\
                   Your browser does not support the audio tag.\
            </audio>\
        </div>';
  
  /**
   * @namespace Y.app.casenote.attachment.views.CaseNoteAttachmentView
   * @description 
   */
  var CaseNoteAttachmentView = Y.Base.create('caseNoteAttachmentView', Y.usp.casenote.CaseNoteEntryAttachmentView, [], {
    _align: function() {
      this.fire('align');
    },
    /**
     * @method initializer
     */
    initializer: function(config) {},
    /**
     * @method detructor
     */
    destructor: function() {
      if (this._loadHander) {
        this._loadHander.detach();
        delete this._loadHandler;
      }
    },
    /**
     * @method render
     */
    render: function() {
      var container = this.get('container'),
        model = this.get('model').toJSON(),
        downloadUrl = this.get('downloadUrl'),
        resolvedURL = Y.Lang.sub(downloadUrl, {
          id: model.id
        }),
        html = DEFAULT_HTML_TEMPLATE;
      
      switch (Y.app.utils.ResolveFileContentType(model.contentType).contentType) {
        case 'VIDEO':
          html = Y.Lang.sub(VIDEO_HTML_TEMPLATE, {
            downloadUrl: resolvedURL,
            contentType: model.contentType
          });
          break;
        case 'AUDIO':
          html = Y.Lang.sub(AUDIO_HTML_TEMPLATE, {
            downloadUrl: resolvedURL,
            contentType: model.contentType
          });
          break;
        case 'IMAGE':
          html = Y.Lang.sub(IMAGE_HTML_TEMPLATE, {
            downloadUrl: resolvedURL,
            fileName: model.filename
          });
          break;
        default:
          html = DEFAULT_HTML_TEMPLATE;
          break;
      }
      
      container.setHTML(html);
      
      var item = container.one('.casenote-file-view img, .casenote-file-view source');
      if (item) {
        //align when loaded
        this._loadHander = item.on('load', this._align, this);
      }
      
      this._align();
      return this;
    }
  }, {
    ATTRS: {}
  });
  
  var DeleteAttachmentConfirmView = Y.Base.create('deleteAttachmentConfirmView', Y.usp.casenote.CaseNoteEntryAttachmentView, [], {
    render: function() {
      var labels = this.get("labels");
      this.get("container").setHTML('<div class="pure-g-r"><div class="pure-u-1 l-box"><div class="pure-alert pure-alert-block pure-alert-warning"><h4>' + labels.deleteConfirm + '</h4></div></div></div>');
    }
  });
  
  Y.namespace('app.casenote.attachment').CaseNoteAttachmentView = CaseNoteAttachmentView;
  Y.namespace('app.casenote.attachment').DeleteAttachmentConfirmView = DeleteAttachmentConfirmView;
}, '0.0.1', {
  requires: ['yui-base',
               'view',
               'handlebars-base',
               'handlebars-helpers',
               'event-custom',
               'usp-date',
               'escape',
               'app-utils',
               'results-formatters',
               'usp-casenote-CaseNoteEntryAttachment'
               ]
});