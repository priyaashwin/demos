YUI.add('casenote-results', function(Y) {
    'use-strict';

    var L = Y.Lang,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates,
        AttachmentFormatters = Y.app.attachment.ColumnFormatters,
    	APPFormatters = Y.app.ColumnFormatters;
    
    Y.Do.after(function() {
            var schema = Y.Do.originalRetVal;
            schema.resultFields.push({
                key: 'eventDate!calculatedDate',
                locator: 'eventDate.calculatedDate'
            }, {
                key: 'createdStamp!auditDate',
                locator: 'createdStamp.auditDate'
            }, {
                key: 'lastEditedStamp!auditDate',
                locator: 'lastEditedStamp.auditDate'
            }, {
                key: 'eventDate!fuzzyDateMask',
                locator: 'eventDate.fuzzyDateMask'
            }, {
                key: 'subjectIdName!id',
                locator: 'subjectIdName.id'
            }, {
                key: 'subjectIdName!name',
                locator: 'subjectIdName.name'
            }, {
                key: 'subjectIdName!identifier',
                locator: 'subjectIdName.identifier'
            }, {
                key: 'subjectIdName!type',
                locator: 'subjectIdName.type'
            });

            return new Y.Do.AlterReturn(null, schema);
        },
        Y.usp.casenote.CaseNoteEntryWithVisibility, 'getSchema',
        Y.usp.casenote.CaseNoteEntryWithVisibility);


    Y.namespace('app.casenote').PaginatedFilteredCaseNoteEntryWithVisibilityList = Y.Base.create("paginatedFilteredCaseNoteEntryWithVisibilityList",
        Y.usp.casenote.PaginatedCaseNoteEntryWithVisibilityList, [Y.usp.ResultsFilterURLMixin], {
            whitelist: ['startDate', 'endDate', 'startRecordedDate', 'endRecordedDate',
                'startEditedDate', 'endEditedDate', 'impactList', 'sourceList', 'otherSource',
                'sourceOrgList', 'otherSourceOrg', 'pracList', 'pracOrgList',
                'groupIds', 'entryTypeList', 'statusList',
                'excludeGroupEntries', 'entrySubTypeList',
                'hasAttachments', 'attachmentSource', 'attachmentOtherSource',
                'attachmentSourceOrganisation', 'attachmentOtherSourceOrganisation',
                'attachmentTitle', 'subjectSeenIndicatorList'
            ],
            staticData: {
                useSoundex: false,
                appendWildcard: true
            }
        }, {
            ATTRS: {
                filterModel: {
                    writeOnce: 'initOnly'
                }
            }
        });

    Y.namespace('app.casenote').CasenoteResults = Y.Base.create('casenoteResults', Y.app.entry.BaseEntryResults, [], {
        getResultsListModel: function(config) {
            var subject = config.subject;

            return new Y.app.casenote.PaginatedFilteredCaseNoteEntryWithVisibilityList({
                url: L.sub(config.url, {
                    id: subject.subjectId,
                    subjectType: subject.subjectType
                }),
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions,
                subject = config.subject || {},
                subjectType = subject.subjectType,
                impactEnabled = config.impactEnabled,
                notificationEnabled = config.notificationEnabled;

            var columns = ([{
                key: "createdStamp!auditDate",
                label: labels.recordedDate,
                startHidden: true,
                width: '10%',
                readingView: '10%',
                formatter: USPFormatters.dateTime,
                className: "yui3-hide",
                cellTemplate: USPTemplates.clickable(labels.viewTitle, 'view', permissions.canView)
            }, {
                key: "lastEditedStamp!auditDate",
                label: labels.editedDate,
                startHidden: true,
                width: '10%',
                readingView: '10%',
                formatter: USPFormatters.dateTime,
                className: "yui3-hide",
                cellTemplate: USPTemplates.clickable(labels.viewTitle, 'view', permissions.canView)
            }, {
                key: "eventDate!calculatedDate",
                label: labels.calculatedDate,
                width: '10%',
                readingView: '15%',
                maskField: 'eventDate!fuzzyDateMask',
                formatter: APPFormatters.fuzzyDateLinkForReadSummaryFormatter,
                title:labels.viewTitle,
                url: config.urlForView,
                permissions: permissions
            }, {
                key: "event",
                label: labels.event,
                className: 'truncatedRichText',
                width: '15%',
                readingView: '50%',
                allowHTML: true,
                expandedFormatter: this.richTextWithTypeFullFormatter,
                collapsedFormatter: this.richTextWithTypeTruncateFormatter,
                type: 'Case note',
                subjectType: subjectType
            }, {
                key: "entryType",
                label: labels.entryType,
                width: impactEnabled ? '8%' : '13%',
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.secured.uspCategory.casenote.entryType.category.codedEntries,
                //coded entry formatter escapes content
                allowHTML: true
            }, {
                key: "source",
                label: labels.source,
                width: '5%',
                formatter: Y.app.source.SourceWithSourceOrganisationFormatter
            }, {
                key: "practitioner",
                label: labels.practitioner,
                width: impactEnabled ? '8%' : '13%',
                formatter: this.appendPractitionerOrganisationFormatter
            }, {
                key: "status",
                label: labels.status,
                width: '5%',
                allowHTML: true,
                formatter: this.statusFormatter,
                type: 'Entry'
            }, {
                key: "peopleSeen",
                label: labels.peopleSeen,
                readingView: '5%',
                width: '5%',
                allowHTML: true,
                formatter: function(o) {
                    if ((o.data.peopleSeen && o.data.peopleSeen.length > 0) || (o.data.peopleSeenAlone && o.data.peopleSeenAlone.length > 0)) {
                        return '<i class="fa fa-check fa-icon-large"></i>';
                    }
                },
                startHidden: subjectType === 'organisation',
                className: subjectType === 'organisation'?'yui3-hide':'',
            }, {
                key: "attachmentsCount",
                label: labels.attachmentCount,
                width: '5%',
                readingView: '5%',
                allowHTML: true,
                formatter: AttachmentFormatters.attachment
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '5%',
                readingView: '5%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'view',
                    label: labels.view,
                    title: labels.viewTitle,
                    visible: function() {
                        var canReadDetail = this.record.hasAccessLevel('READ_DETAIL');
                        return permissions.canView && canReadDetail;
                    }
                }, {
                    clazz: 'complete',
                    label: labels.complete,
                    title: labels.completeTitle,
                    visible: function() {
                        var canWrite = this.record.hasAccessLevel('WRITE');
                        //Status must be DRAFT
                        return permissions.canComplete && this.data.status == "DRAFT" && canWrite;
                    }
                }, {
                    clazz: 'uncomplete',
                    label: labels.uncomplete,
                    title: labels.uncompleteTitle,
                    visible: function() {
                        var canWrite = this.record.hasAccessLevel('WRITE');
                        //Status must be COMPLETE
                        return permissions.canUncomplete && this.data.status == "COMPLETE" && canWrite;
                    }
                }, {
                    clazz: 'softDelete',
                    label: labels.deleteEntry,
                    title: labels.deleteEntryTitle,
                    visible: function() {
                        var canWrite = this.record.hasAccessLevel('WRITE');
                        //Status must NOT be DELETED
                        return permissions.canDelete && this.data.status !== 'DELETED' && canWrite;
                    }
                }, {
                    clazz: 'hardDelete',
                    label: labels.remove,
                    title: labels.removeTitle,
                    visible: function() {
                        var canWrite = this.record.hasAccessLevel('WRITE');
                        return permissions.canRemove && canWrite;
                    }
                }, {
                    clazz: 'uploadFile',
                    label: labels.uploadFile,
                    title: labels.uploadFileTitle,
                    visible: function() {
                        var canWrite = this.record.hasAccessLevel('WRITE');
                        // Status must be DRAFT
                        var visible = permissions.canUpload && this.data.status == "DRAFT" && canWrite;
                        // Unless we have special permissions
                        if(permissions.canUploadAttachmentOnCompleteInstance && this.data.status == "COMPLETE" && canWrite) {
                        	visible=true;
                        }
                        return visible;
                    }
                }, {
                	clazz: 'sendNotification',
                	label: labels.sendNotification,
                	title: labels.sendNotificationTitle,
                	visible: function() {
                		var visibility = notificationEnabled && permissions.canSendNotification && this.record.hasAccessLevel('WRITE')===true;
                		return visibility;
                	}
                }]
            }]);

            if (impactEnabled) {
                //insert the impact column if impact is enabled
                columns.splice(6, 0, {
                    key: "impact",
                    label: labels.impact,
                    width: '5%',
                    allowHTML: true,
                    formatter: this.impactFormatter,
                    type: 'Case note'
                });
            }
            return columns;
        }
    });

    Y.namespace('app.casenote').CasenoteFilteredResults = Y.Base.create('casenoteFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.casenote.CasenoteFilter,
        resultsType: Y.app.casenote.CasenoteResults,
        updateResults: function() {
            //force a refresh of the filter options
            this.get('filter').updateFilterListOptions();
            //reload list
            this.reload();
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-filtered-results',
        'results-filter',
        'results-formatters',
        'results-templates',
        'usp-casenote-CaseNoteEntryWithVisibility',
        'attachment-results-formatters',
        'secured-categories-casenote-component-EntryType',
        'casenote-filter',
        'entry-results',
        'source-form',
        'caserecording-results-formatters'
    ]
});