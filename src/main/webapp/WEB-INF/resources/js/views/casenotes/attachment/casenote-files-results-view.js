YUI.add('casenote-files-results-view', function(Y) {

  'use-strict';
  var L = Y.Lang,
    USPFormatters = Y.usp.ColumnFormatters,
    APPFormatters = Y.app.ColumnFormatters,
    EVENT_REFRESH_RESULTS = 'attachments:dataChanged';

  Y.namespace('app.casenote').AttachmentResults = Y.Base.create('attachmentResults', Y.usp.app.Results, [], {
    initializer: function(config) {
      this._evtHandlers = [
        this.on('*:downloadFile', this.downloadFile, this),
        this.on('*:removeFile', this.removeFile, this),
        this.on('*:viewFile', this.viewFile, this),
        this.on('*:removeTemporaryAttachment', this.removeTemporaryAttachment, this),
        Y.on(EVENT_REFRESH_RESULTS, this.reload, this),
        Y.on('attachments:temporaryAttachmentUploaded', this.addTemporararyAttachment, this),
        this.after('temporaryAttachmentIdsChange', this.handleTemporaryAttachmentIdsChange, this)
      ];
    },
    destructor:function() {
      this._evtHandlers.forEach(function(handle) {
        handle.detach();
      });

      this._evtHandlers = null;
    },
    viewFile: function(e) {
      var record = e.record;
      this.fire('viewAttachment', {
        id: record.get("id"),
        record: record
      });
    },
    removeTemporaryAttachment: function(e) {
      var record = e.record;
      var permissions = e.permissions || {};

      if (this.get('showDeleteButton') || this.get('isTemporaryAttachments') && permissions.canDeleteAttachment) {
        this._removeTemporaryAttachment(record.get('id'));
      }
    },
    removeFile: function(e) {
      var record = e.record;
      var permissions = e.permissions || {};

      if (this.get('showDeleteButton') || this.get('isTemporaryAttachments') && permissions.canDeleteAttachment) {
        this.fire('deleteAttachment', {
          id: record.get("id"),
          record: record
        });
      }
    },
    downloadFile: function(e) {
      var record = e.record;

      window.location = Y.Lang.sub(this.get('downloadUrl'), {
        id: record.get("id")
      });

    },
    getTotalAttachmentsCount: function() {
      var results=this.get('results');
      //Do not refer to data which return page wise total items, need to find total attachments as below
      if (results.pagModel) {
        return results.pagModel.toJSON().totalItems;
      }
      return 0;
    },
    getResultsListModel: function() {
      var isTemporaryAttachments=this.get('isTemporaryAttachments'),
          url=this.get('attachmentListUrl'),
          casenoteId=this.get('casenoteId');
      
      if (isTemporaryAttachments) {
        return new Y.usp.casenote.PaginatedTemporaryAttachmentList({
          url: L.sub(url, {
            temporaryAttachmentIds: this.get('temporaryAttachmentIds').join(',')
          })
        });

      } else {
        return new Y.usp.casenote.PaginatedCaseNoteEntryAttachmentList({
          url: L.sub(url, {
            casenoteEntryId: casenoteId
          })
        });
      }
    },
    addTemporararyAttachment: function(e) {
      var temporaryAttachmentIds = this.get('temporaryAttachmentIds') || [];

      var tempAttachmentId = Y.Number.parse(e.temporaryAttachmentId);
      if (temporaryAttachmentIds.indexOf(tempAttachmentId) < 0) {
        temporaryAttachmentIds.push(tempAttachmentId);
      }
      this.set('temporaryAttachmentIds', temporaryAttachmentIds);
    },
    _removeTemporaryAttachment: function(tempAttachmentId) {
      var temporaryAttachmentIds = this.get('temporaryAttachmentIds');
      var index = temporaryAttachmentIds.indexOf(tempAttachmentId);
      if (index > -1) {
        temporaryAttachmentIds.splice(index, 1);
      }

      this.set('temporaryAttachmentIds', temporaryAttachmentIds);
    },
    handleTemporaryAttachmentIdsChange: function(e) {
      //update temporary attachment url
      this.get('results').get('data').url = L.sub(this.get('attachmentListUrl'), {
        temporaryAttachmentIds: (e.newVal || []).join(',')
      });

      this.reload();
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;

      var isTemporaryAttachments=this.get('isTemporaryAttachments');
      var deleteAttachment = ((this.get('showDeleteButton') || isTemporaryAttachments) && permissions.canDeleteAttachment) || false;
      var removeClazz = isTemporaryAttachments ? 'removeTemporaryAttachment' : 'removeFile';
      var downloadUrl=this.get('downloadUrl');
      
      return ([{
        key: 'title',
        allowHTML: true,
        label: labels.title,
        width: '11%',
        formatter: APPFormatters.linkable,
        clazz:'downloadFile',
        title: 'download',
        url: downloadUrl
        }, {
        key: "lastUploadedBy",
        label: labels.uploadedBy,
        width: '25%'
        }, {
        key: "lastUploaded",
        label: labels.uploadedOn,
        formatter: USPFormatters.date,
        width: '25%'
        }, {
        key: "description",
        label: labels.description,
        allowHTML: true,
        width: '30%'
        }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '8%',
        formatter: USPFormatters.actions,
        items: [{
            clazz: 'downloadFile',
            title: labels.fileDownload,
            label: labels.fileDownload,
            visible: function() {
              return this.record.hasAccessLevel('READ_DETAIL');
            }
            }, {
            clazz: 'viewFile',
            title: labels.viewFile,
            label: labels.viewFile,
            visible: function() {
              return !isTemporaryAttachments && Y.app.utils.ResolveFileContentType(this.data.contentType).view && this.record.hasAccessLevel('READ_DETAIL');
            }
            },
          {
            clazz: removeClazz,
            title: labels.fileRemove,
            label: labels.fileRemove,
            visible: function() {
              return deleteAttachment && this.record.hasAccessLevel('WRITE');
            }
            }]
        }]);
    }
  }, {
    ATTRS: {
      temporaryAttachmentIds: {
        value: []
      },
      labels: {
        value: {}
      },
      url: {
        value: {}
      },
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
                }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                },{
                  fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                  cfg: {
                    state: 'expanded',
                    summaryFormatter:Y.app.source.SourceSummaryFormatter
                  }
                }];
        }
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
               'view',
               'app-results',
               'event-custom',
               'app-utils',
               'results-formatters',
               'results-templates',
               'results-table-keyboard-nav-plugin',
               'results-table-menu-plugin',
               'results-table-summary-row-plugin',
               'usp-casenote-CaseNoteEntryAttachment',
               'usp-casenote-TemporaryAttachment',
               'source-form'
               ]
});