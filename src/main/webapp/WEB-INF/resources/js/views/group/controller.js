YUI.add('group-controller-view', function(Y) {
    'use-strict';

   var L=Y.Lang;

    Y.namespace('app.group').GroupControllerView = Y.Base.create('groupControllerView', Y.View, [], {
        initializer: function(config) {
            if(config.subject.subjectType==='group'){
              var resultsConfig={
                searchConfig:Y.merge(config.membersSearchConfig, {
                //mix the subject into the search config so it can be used in URL generation
                  subject: config.subject
                }),
                permissions: config.permissions
              };
              this.groupResults = new Y.app.group.GroupMembershipResults(resultsConfig).addTarget(this);
            }else{
              var resultsConfig={
                searchConfig:Y.merge(config.groupSearchConfig, {
                  //mix the subject into the search config so it can be used in URL generation
                  subject: config.subject
                }),
                permissions: config.permissions
              };
              this.groupResults = new Y.app.group.GroupResults(resultsConfig).addTarget(this);
            }

            this._groupEvtHandlers = [
                 this.on('*:viewMembers', this.handleViewMembers, this),
                 this.on('*:viewGroup', this.handleViewGroup, this),
                 this.on('*:editGroup', this.handleEditGroup, this),
                 this.on('*:closeGroup', this.handleCloseGroup, this),
                 this.on('*:deleteGroup', this.handleDeleteGroup, this),
                 this.on('*:viewPersonGroups', this.handleViewPersonGroups, this),
                 this.on('*:closePersonGroupMembership', this.handleEndMembership, this),
                 this.on('*:removePersonGroupMembership', this.handleRemoveMembership, this),
                 this.on('*:openGroup',this.handleOpenGroup, this),
                 Y.on('group:dataChanged', this.groupResults.reload, this.groupResults)
             ];
        },
        destructor: function() {
            this._groupEvtHandlers.forEach(function(handler) {
              handler.detach();
            });
            delete this._groupEvtHandlers;

            this.groupResults.removeTarget(this);
            this.groupResults.destroy();
            delete this.groupResults;
        },
        render: function() {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.groupResults.render().get('container'));

            this.get('container').setHTML(contentNode);

            return this;
        },
        handleAddGroup:function(){
            Y.fire('group:showDialog', {
              action: 'add'
          });
        },
        handleViewMembers:function(e){
          var groupsUrl=this.get('groupsUrl'),
              record=e.record;

          window.location=L.sub(groupsUrl,{groupId:record.get('groupId')});
        },
        handleViewGroup:function(e){
          var record=e.record, permissions=e.permissions,
              groupMembersUrl=this.get('groupMembersUrl');

          if(permissions.canViewGroup){
            Y.fire('group:showDialog', {
              id: record.get('groupId'),
                action: 'view',
                groupMembersURL : groupMembersUrl
            });
          }
        },
        handleViewPersonGroups:function(e){
          var personGroupsUrl=this.get('personGroupsUrl');
          record=e.record;

          window.location=L.sub(personGroupsUrl,{personId:record.get('personId')});
        },
        handleEditGroup:function(e){
          var record=e.record, permissions=e.permissions;
          if(permissions.canViewMembers){
            Y.fire('group:showDialog', {
              id: record.get('groupId'),
                action: 'edit'
            });
          }
        },
        handleOpenGroup:function(e){
          var record=e.record, permissions=e.permissions;
          if(permissions.canOpenGroup){
            Y.fire('group:showDialog', {
              id: record.get('groupId'),
                action: 'open'
            });
          }
        },
        handleCloseGroup:function(e){
          var record=e.record, permissions=e.permissions;
          if(permissions.canCloseGroup){
            Y.fire('group:showDialog', {
              id: record.get('groupId'),
                action: 'close'
            });
          }
        },
        handleDeleteGroup:function(e){
          var record=e.record, permissions=e.permissions;
          if(permissions.canDeleteGroup){
            Y.fire('group:showDialog', {
              id: record.get('groupId'),
                action: 'delete'
            });
          }
        },
        handleEndMembership:function(e){
          var record=e.record, permissions=e.permissions;
          if(permissions.canClosePersonGroupMembership){
            Y.fire('group:showDialog', {
              id: record.get('id'),
              action: 'endMembership'
            });
          }
        },
        handleRemoveMembership:function(e){
          var record=e.record, permissions=e.permissions;
          if(permissions.canRemovePersonGroupMembership){
            Y.fire('group:showDialog', {
              id: record.get('id'),
              action: 'deleteMembership'
            });
          }
        }
    }, {
        ATTRS: {
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'group-results'
    ]
});
