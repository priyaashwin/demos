YUI.add('group-results', function(Y) {
  'use strict';

  var A = Y.Array,
    L = Y.Lang,
    E = Y.Escape,
    USPFormatters = Y.usp.ColumnFormatters,
    USPTemplates = Y.usp.ColumnTemplates,
    APPFormatters = Y.app.ColumnFormatters,
    address_template_DND = '<div class="pop-up-data" data-content="Do not disclose" data-align="bottom"><span class="do-not-disclose"> <i class="fa fa-ban" aria-hidden="true"></i> </span></div>',
    addressFormatters = Y.app.FormatAddressLocation;

  //AOP to add additional schema values to PersonGroupMembershipDetailsVO
  Y.Do.after(function() {
    var schema = Y.Do.originalRetVal;
    schema.resultFields.push({
      key: 'groupVO!isInactiveHistoric',
      locator: 'groupVO.isInactiveHistoric'
    });
    schema.resultFields.push({
      key: 'groupVO!groupIdentifier',
      locator: 'groupVO.groupIdentifier'
    });
    schema.resultFields.push({
      key: 'groupVO!name',
      locator: 'groupVO.name'
    });
    schema.resultFields.push({
      key: 'groupVO!description',
      locator: 'groupVO.description'
    });
    schema.resultFields.push({
      key: 'groupVO!groupType',
      locator: 'groupVO.groupType'
    });
    schema.resultFields.push({
      key: 'groupVO!startDate',
      locator: 'groupVO.startDate'
    });
    schema.resultFields.push({
      key: 'groupVO!closeDate',
      locator: 'groupVO.closeDate'
    });
    return new Y.Do.AlterReturn(null, schema);
  }, Y.usp.relationshipsrecording.PersonGroupMembershipDetailsWithMembersAndCount, 'getSchema', Y.usp.relationshipsrecording.PersonGroupMembershipDetailsWithMembersAndCount);

  //AOP to add additional schema values to PersonGroupMembershipDetailsWithAddress
  Y.Do.after(function() {
    var schema = Y.Do.originalRetVal;
    schema.resultFields.push({
      key: 'address',
      locator: 'address'
    });

    return new Y.Do.AlterReturn(null, schema);
  }, Y.usp.relationshipsrecording.PersonGroupMembershipDetailsWithAddressWarningCount, 'getSchema', Y.usp.relationshipsrecording.PersonGroupMembershipDetailsWithAddressWarningCount);

  Y.namespace('app.group').GroupResults = Y.Base.create('groupResults', Y.usp.app.Results, [], {

    getResultsListModel: function(config) {
      var subject = config.subject;

      return new Y.usp.relationshipsrecording.PaginatedPersonGroupMembershipDetailsWithMembersAndCountList({
        url: L.sub(config.url, {
          id: subject.subjectId,
          subjectType: subject.subjectType
        })
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;
      return ([{
        key: 'groupVO!name',
        label: labels.name,
        sortable: true,
        width: '20%',
        clazz: 'viewMembers',
        cellTemplate: USPTemplates.clickable(labels.membersView, 'viewMembers'),
        formatter: APPFormatters.formatNameAndIdentifier
      }, {
        key: 'groupVO!description',
        label: labels.description,
        width: '22%'
      }, {
        key: 'groupVO!groupType',
        label: labels.type,
        width: '10%',
        allowHTML: true,
        codedEntries: Y.uspCategory.group.groupType.category.codedEntries,
        formatter: USPFormatters.codedEntry
      }, {
        key: 'groupVO!startDate',
        label: labels.startDate,
        sortable: true,
        width: '10%',
        formatter: USPFormatters.date
      }, {
        key: 'groupVO!closeDate',
        label: labels.closeDate,
        sortable: true,
        width: '10%',
        formatter: USPFormatters.date
      }, {
        key: 'joinDate',
        label: labels.joinDate,
        sortable: true,
        width: '10%',
        formatter: USPFormatters.date
      }, {
        key: 'leaveDate',
        label: labels.leaveDate,
        sortable: true,
        width: '10%',
        formatter: USPFormatters.date
       }, {
        key: 'mobile-summary',
        label: 'Summary',
        className: 'mobile-summary pure-table-desktop-hidden',
        width: '0%',
        allowHTML: true,
        formatter: APPFormatters.formatMembers,
        cellTemplate: '<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '8%',
        formatter: USPFormatters.actions,
        items: [{
          clazz: 'viewGroup',
          label: labels.view,
          title: labels.viewTitle,
          visible: permissions.viewGroup
        }, {
          clazz: 'editGroup',
          label: labels.edit,
          title: labels.editTitle,
          visible: function() {
            return !this.record.get('groupVO!isInactiveHistoric') && permissions.canEditGroup
          }
        }, {
          clazz: 'closeGroup',
          label: labels.close,
          title: labels.closeTitle,
          visible: function() {
            return !this.record.get('groupVO!isInactiveHistoric') && permissions.canCloseGroup
          }
        },{
          clazz: 'deleteGroup',
          label: labels.deleteGroup,
          title: labels.deleteGroupTitle,
          visible: permissions.canDeleteGroup
        },{
         clazz: 'openGroup',
         label: labels.open,
         title: labels.openTitle,
         visible: function() {
          return (this.record.get('groupVO!closeDate') || this.record.get('closeDate')) && permissions.canOpenGroup;
}
        },{
          clazz: 'closePersonGroupMembership',
          label: labels.endMembership,
          title: labels.endMembershipTitle,
          visible: function() {
            return !(this.record.get('groupVO!isInactiveHistoric') || this.record.get('isInactiveHistoric')) && permissions.canClosePersonGroupMembership;
          }
        }, {
          clazz: 'removePersonGroupMembership',
          label: labels.removeMembership,
          title: labels.removeMembershipTitle,
          visible: permissions.canRemovePersonGroupMembership
        }]
      }]);
    }
  }, {
    ATTRS: {
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
            cfg: {
              state: 'expanded',
              summaryFormatter:APPFormatters.formatMembers
            }
          }];
        }
      }
    }
  });

  //TODO - move to a common formatter
  var summaryFormat = function(o) {
    var record=o.record,
        column=o.column||{},
        labels=column.labels,
        iconDND = '';

    if (record.get('doNotDisclose')) {
      iconDND = address_template_DND;
    }

    var addressHTML = '<strong>Address</strong> <div class="addressLocation-wrapper">{iconDND}<span class="address">{address}</span> <span class="postcode">{postcode}</span></div>',
      row, dobInfo = [],
      genderInfo=[],
      gender,
      dob = record.get('personVO!dateOfBirth'),
      dobEstimated = record.get('personVO!dateOfBirthEstimated'),
      age = record.get('personVO!age'),
      address = addressFormatters.getAddressLocationDescription(record, labels),
      postcode = record.get('postcode'),
      lifeState = record.get('personVO!lifeState'),
      dueDate = record.get('personVO!dueDate');

    address = address.split(',').slice(0, -1).toString();
    address = address || 'None';
    address = L.sub(addressHTML, {
      address: E.html(address),
      postcode: E.html(postcode),
      iconDND: iconDND
    });

    dobInfo = APPFormatters.formatDateBasedOnLifeState(lifeState, dueDate, dob, dobEstimated, age, false);

    var ethnicity = Y.uspCategory.person.ethnicity.category.codedEntries[record.get('personVO!ethnicity')];
    var ethnicityInfo = '';
    if (ethnicity) {
      ethnicityInfo = '<strong> Ethnicity</strong> ' + E.html(ethnicity.name);
    }

    gender = Y.uspCategory.person.gender.category.codedEntries[record.get('personVO!gender')];
    genderInfo = '';
    if (gender) {
      genderInfo = '<strong> Gender</strong> ' + E.html(gender.name);
    }

    return ('<div>' + address + genderInfo + dobInfo.join(' ') + ethnicityInfo + '</div>');
  };

  Y.namespace('app.group').GroupMembershipResults = Y.Base.create('groupMembershipResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      var subject = config.subject;

      return new Y.usp.relationshipsrecording.PaginatedPersonGroupMembershipDetailsWithAddressWarningCountList({
        url: L.sub(config.url, {
          id: subject.subjectId,
          subjectType: subject.subjectType
        })
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;
      return ([{
        key: 'personVO!personIdentifier',
        label: labels.id,
        sortable: true,
        width: '12%',
        clazz: 'viewPersonGroups',
        cellTemplate: USPTemplates.clickable(labels.viewPersonGroups, 'viewPersonGroups'),
      }, {
        key: 'personVO!name',
        label: labels.name,
        allowHTML: true,
        sortable: true,
        width: '50%',
        clazz: 'viewPersonGroups',
        title: labels.viewPersonGroups,
        formatter: APPFormatters.linkablePopOverWarnings,
        url: '#none'
      }, {
        key: 'joinDate',
        label: labels.joinDate,
        sortable: true,
        width: '15%',
        formatter: USPFormatters.date
      }, {
        key: 'leaveDate',
        label: labels.leaveDate,
        sortable: true,
        width: '15%',
        formatter: USPFormatters.date
      }, {
        key: 'mobile-summary',
        label: 'Summary',
        className: 'mobile-summary pure-table-desktop-hidden',
        width: '0%',
        allowHTML: true,
        formatter: summaryFormat,
        labels: labels,
        cellTemplate: '<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '8%',
        formatter: USPFormatters.actions,
        items: [{
          clazz: 'closePersonGroupMembership',
          label: labels.endMembership,
          title: labels.endMembershipTitle,
          visible: function() {
            return !(this.record.get('groupVO!isInactiveHistoric') || this.record.get('isInactiveHistoric')) && permissions.canClosePersonGroupMembership;
          }
        }, {
          clazz: 'removePersonGroupMembership',
          label: labels.removeMembership,
          title: labels.removeMembershipTitle,
          visible: permissions.canRemovePersonGroupMembership,
          enabled: function() {
            return !this.record.get('groupVO!isInactiveHistoric')
          }
        }]
      }]);
    }
  }, {
    ATTRS: {
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
            cfg: {
              state: 'expanded',
              summaryFormatter:summaryFormat
            }
          }];
        }
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'usp-relationshipsrecording-PersonGroupMembershipDetailsWithMembersAndCount',
    'usp-relationshipsrecording-PersonGroupMembershipDetailsWithAddressWarningCount',
    'caserecording-results-formatters',
    'results-formatters',
    'results-templates',
    'results-table-summary-row-plugin',
    'categories-group-component-GroupType',
    'categories-person-component-Ethnicity',
    'categories-person-component-Gender',
    'base-AddressLocation-formatter'
  ]
});
