YUI.add('checklist-add-results', function(Y) {
  'use-strict';

  var L = Y.Lang,
    CaseRecordingFormatters = Y.app.ColumnFormatters;

  var PaginatedFilteredChecklistDefinitionSearchResultList = Y.Base.create("paginatedFilteredChecklistDefinitionSearchResultList", Y.usp.checklist.PaginatedChecklistDefinitionWithCalculatedDueDateList, [Y.usp.ResultsFilterURLMixin], {
    whiteList: ['name'],
    staticData: {
      useSoundex: false,
      appendWildcard: true
    }
  }, {
    ATTRS: {
      filterModel: {
        writeOnce: 'initOnly'
      }
    }
  });

  Y.namespace('app.checklist').AddChecklistInstanceResults = Y.Base.create('addChecklistInstanceResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      var subject = config.subject,
      permissions = config.permissions;

      return new PaginatedFilteredChecklistDefinitionSearchResultList({
        url: L.sub(config.url, {
          //if the user is unable to view restricted checklist definition - we must exclude them
          excludePreferAutomatedAdd: (permissions.canViewAutomatedAdd !== true),
          subjectId: subject.subjectId,
          subjectType: (subject.subjectType || '').toUpperCase()
        }),
        //Required access level is WRITE as user needs to be able to write the checklist
        requestedAccess: 'WRITE',
        //plug in the filter model
        filterModel: config.filterModel
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;

      return ([{
        key: 'name',
        label: labels.name,
        width: '80%',
        sortable: true
      }, {
        key: 'version',
        label: labels.version,
        width: '10%',
        sortable: true
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '10%',
        formatter: CaseRecordingFormatters.button,
        button: {
          clazz: 'add',
          iconClazz: 'fa fa-plus-circle',
          title: labels.addChecklistTitle,
          label: labels.addChecklist,
          enabled: function() {
            if (permissions.canAdd === true && this.record.hasAccessLevel('WRITE') === true) {
              return true;
            }
            return false;
          }
        }
      }]);
    }
  }, {
    ATTRS: {
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
          }];
        }
      },
    }
  });

  Y.namespace('app.checklist').AddChecklistFilteredResults = Y.Base.create('addChecklistFilteredResults', Y.usp.app.FilteredResults, [], {
    filterType: Y.app.checklist.AddChecklistInstanceFilterView,
    resultsType: Y.app.checklist.AddChecklistInstanceResults
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'app-filtered-results',
    'app-filter',
    'results-table-menu-plugin',
    'results-table-keyboard-nav-plugin',
    'results-table-search-state-plugin',
    'caserecording-results-formatters',
    'checklist-add-filter',
    'usp-checklist-ChecklistDefinitionWithCalculatedDueDate'
  ]
});