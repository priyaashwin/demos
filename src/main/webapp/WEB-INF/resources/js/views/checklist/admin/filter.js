YUI.add('checklist-definition-filter', function(Y) {
  "use strict";

  var DB = Y.usp.DataBindingUtil;

  Y.namespace('app.admin.checklist').ChecklistDefinitionFilterModel = Y.Base.create('checklistDefinitionFilterModel', Y.usp.ResultsFilterModel, [], {}, {
    ATTRS: {
      status: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        //default status list defined here
        value: ['DRAFT', 'PUBLISHED']
      },
      securityDomain: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      name: {
        USPType: 'String',
        value: ''
      }
    }
  });


  Y.namespace('app.admin.checklist').ChecklistDefinitionResultsFilter = Y.Base.create('checklistDefinitionResultsFilter', Y.usp.app.ResultsFilter, [], {
    template: Y.Handlebars.templates['checklistDefinitionResultsFilter'],
    initializer: function() {
      this.after('securityDomainsChange', this.renderSecurityDomains, this);
    },
    render: function() {
      //get the container 
      var instance = this,
        container = instance.get('container');

      //call into superclass to render
      Y.app.admin.checklist.ChecklistDefinitionResultsFilter.superclass.render.call(this);
      //flag to track if rendered
      this.rendered = true;

      this.renderSecurityDomains();

      return this;
    },
    renderSecurityDomains: function() {
      var select, securityDomains, container = this.get('container');
      if (this.rendered) {
        select = container.one('#checklistFilter_securityDomain');
        securityDomains = this.get('securityDomains') || [];

        Y.FUtil.setSelectOptions(select, securityDomains, null, null, false, true, 'code');

        //now we have out select box populated - we need to ensure that any default selection is made
        DB.setElementValue(select._node, this.get('model').get('securityDomain'));
      }
    }
  }, {
    // Specify attributes and static properties for your View here.
    ATTRS: {
      otherData: {
        value: {
          statusList: [{
            code: "DRAFT",
            name: "Draft"
          }, {
            code: "PUBLISHED",
            name: "Published"
          }, {
            code: "ARCHIVED",
            name: "Deleted"
          }]
        }
      },
      securityDomains: {
        value: []
      }
    }
  });

  Y.namespace('app.admin.checklist').ChecklistDefinitionFilter = Y.Base.create('checklistDefinitionFilter', Y.usp.app.Filter, [], {
    filterModelType: Y.app.admin.checklist.ChecklistDefinitionFilterModel,
    filterType: Y.app.admin.checklist.ChecklistDefinitionResultsFilter,
    filterContextTemplate: Y.Handlebars.templates["checklistDefinitionActiveFilter"]
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-filter',
    'results-filter',
    'handlebars-helpers',
    'handlebars-checklist-templates',
    'data-binding',
    'form-util'
  ]
});