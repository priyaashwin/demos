YUI.add('checklist-instance-dialog-view', function(Y) {
  var L = Y.Lang;

  Y.namespace('app').ChecklistInstanceStatusDialogView = Y.Base.create('checklistInstanceStatusDialogView', Y.View, [], {
    initializer: function() {
      var statusDialogConfig = this.get('dialogConfig');

      //create checklist status dialog and render
      this.checklistStatusDialog = new Y.app.ChecklistInstanceStatusFormDialog(statusDialogConfig).render().addTarget(this);
    },
    destructor: function() {
      if (this.checklistStatusDialog) {
        this.checklistStatusDialog.removeTarget(this);
        this.checklistStatusDialog.destroy();
        delete this.checklistStatusDialog;
      }
    },
    showChecklistErrorsDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        checklistType = checklist.checklistDefinitionName;
      this.checklistStatusDialog.showView('viewErrors', {
        model: new Y.app.ChecklistInstanceErrorModel({
          url: statusDialogConfig.views.viewErrors.config.url,
          id: checklist.id
        }),
        otherData: {
          checklistType: checklistType,
          subject: subject
        }
      }, {
        modelLoad: true
      }, function() {
        var model = this.get('activeView').get('model');

        var errors = model.get('errors');
        if (errors && errors.length > 0) {
          var error = errors[0];
          if (error.canRetry) {
            this.getButton('retryButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          }
          if (error.canIgnore) {
            this.getButton('ignoreButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          }
        }
      });
    },
    showDeleteChecklistDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        checklistType = checklist.checklistDefinitionName;
      this.checklistStatusDialog.showView('deleteChecklist', {
        model: new Y.app.ChecklistInstanceStatusChangeForm({
          url: (L.sub(statusDialogConfig.checklistStatusURL, {
            action: 'archived'
          })),
          id: checklist.id
        }),
        checklistModel: {
          status: checklist.status
        },
        otherData: {
          checklistType: checklistType,
          subject: subject
        }
      }, function() {
        //Get the button by name out of the footer and enable it.
        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showRemoveChecklistDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        checklistType = checklist.checklistDefinitionName;

      this.checklistStatusDialog.showView('removeChecklist', {
        model: new Y.app.ChecklistInstanceRemoveForm({
          url: statusDialogConfig.checklistRemoveURL,
          id: checklist.id
        }),
        checklistModel: {
          status: checklist.status
        },
        otherData: {
          checklistType: checklistType,
          subject: subject
        }
      }, function() {
        //Get the button by name out of the footer and enable it.
        this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showReassignChecklistDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        reassignConfig = (statusDialogConfig.views.reassignChecklist || {}).config || {},
        checklistType = checklist.checklistDefinitionName,
        currentPersonId = this.get('currentPersonId');
      //show dialog to indicate loading 
      this.checklistStatusDialog.showView('loadingOwnership');

      //load the current users teams
      Y.app.checklist.ChecklistOwnershipUtil.getUserTeamsPromise(currentPersonId, reassignConfig.personTeamsURL).then(function(teams) {
        this.checklistStatusDialog.showView('reassignChecklist', {
          model: new Y.app.ChecklistInstanceReassignForm({
            url: statusDialogConfig.checklistInstanceURL,
            id: checklist.id
          }),
          currentPersonId: currentPersonId,
          currentPersonTeams: teams.map(function(personOrganisationRelationship) {
            return personOrganisationRelationship.get('organisationVO');
          }).sort(function(a, b) {
            return (a.name || '').localeCompare(b.name || '') || (a.organisationIdentifier || '').localeCompare(b.organisationIdentifier || '');
          }),
          otherData: {
            checklistType: checklistType,
            subject: subject
          }
        }, {
          modelLoad: true
        }, function() {
          //Get the button by name out of the footer and enable it.
          this.getButton('reassignButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }.bind(this)).then(undefined, function(error) {
        this.checklistStatusDialog.get('activeView').fire('error', {
          error: error
        });
      }.bind(this));
    },
    showPauseChecklistDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        checklistType = checklist.checklistDefinitionName;
      this.checklistStatusDialog.showView('pauseChecklist', {
        model: new Y.app.ChecklistInstancePauseForm({
          url: (L.sub(statusDialogConfig.checklistStatusURL, {
            action: 'paused'
          })),
          id: checklist.id
        }),
        otherData: {
          checklistType: checklistType,
          subject: subject
        }
      }, function() {
        //Get the button by name out of the footer and enable it.
        this.getButton('pauseButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showResumeChecklistDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        checklistType = checklist.checklistDefinitionName;
      this.checklistStatusDialog.showView('resumeChecklist', {
        model: new Y.app.ChecklistInstanceStatusChangeForm({
          url: (L.sub(statusDialogConfig.checklistStatusURL, {
            action: 'resumed'
          })),
          id: checklist.id
        }),
        otherData: {
          checklistType: checklistType,
          subject: subject
        }
      }, function() {
        //Get the button by name out of the footer and enable it.
        this.getButton('resumeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showResetChecklistEndDateDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        checklistType = checklist.checklistDefinitionName;
      this.checklistStatusDialog.showView('resetEndDate', {
        model: new Y.app.ChecklistInstanceResetEndDateForm({
          url: statusDialogConfig.checklistInstanceURL,
          id: checklist.id
        }),
        otherData: {
          checklistType: checklistType,
          subject: subject
        }
      }, {
        modelLoad: true
      }, function() {
        //update the URL to the correct save URL - this is different from the URL used in the get
        this.get('activeView').get('model').url = L.sub(statusDialogConfig.checklistActionURL, {
          action: 'resetEndDate'
        });
        //Get the button by name out of the footer and enable it.
        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showChecklistStartDateDialog: function(checklist, subject) {
      var statusDialogConfig = this.get('dialogConfig'),
        checklistType = checklist.checklistDefinitionName;
      this.checklistStatusDialog.showView('startChecklist', {
        model: new Y.app.ChecklistInstanceStatusChangeForm({
          url: (L.sub(statusDialogConfig.checklistStatusURL, {
            action: 'started'
          })),
          id: checklist.id
        }),
        otherData: {
          checklistType: checklistType,
          subject: subject
        }
      }, function() {
        //Get the button by name out of the footer and enable it.
        this.getButton('startButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    }
  }, {
    ATTRS: {
      subject: {},
      dialogConfig: {},
      currentPersonId: {
        value: null
      }
    }
  });
  
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'checklist-instance-status-dialog-views',
    'usp-checklist-ChecklistInstance',
    'handlebars-helpers',
    'handlebars-checklist-partials',
    'handlebars-checklist-templates',
    'checklist-ownership-util'
  ]
});