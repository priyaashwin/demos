YUI.add('checklist-definition-results', function(Y) {
  "use strict";

  var USPTemplates = Y.usp.ColumnTemplates,
    USPFormatters = Y.usp.ColumnFormatters,
    USPColumnFormatters = Y.app.ColumnFormatters;

  Y.namespace('app.admin.checklist').PaginatedFilteredChecklistDefinitionResultList = Y.Base.create("paginatedFilteredChecklistDefinitionResultList", Y.usp.checklist.PaginatedChecklistDefinitionList, [
    Y.usp.ResultsFilterURLMixin
  ], {
    whiteList: ["name", "status", "securityDomain"],
    staticData: {
      useSoundex: false,
      appendWildcard: true
    }
  }, {
    ATTRS: {
      filterModel: {
        writeOnce: 'initOnly'
      }
    }
  });


  Y.namespace('app.admin.checklist').ChecklistDefinitionResults = Y.Base.create('checklistDefinitionResults', Y.usp.app.Results, [], {
    securityDomainFormatter: function(o) {
      var val = '';
      var securityDomains = this.get('securityDomains');
      var ce = securityDomains[o.value];
      if (ce) {
        val = ce.name;
      }
      return val;
    },
    getResultsListModel: function(config) {
      return new Y.app.admin.checklist.PaginatedFilteredChecklistDefinitionResultList({
        url: config.url,
        //plug in the filter model
        filterModel: config.filterModel
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;

      return ([{
        key: 'name',
        label: labels.name,
        sortable: true,
        width: '32%',
        cellTemplate: USPTemplates.clickable(labels.viewChecklistDefinition, 'view', permissions.canView)
      }, {
        key: 'securityDomain',
        label: labels.securityDomain,
        //NOTE - we can't use the standard CodedEntry formatter
        //here as the security domains are loaded async and
        //will not be available until the results list renders
        formatter: this.securityDomainFormatter.bind(this),
        width: '24%'
      }, {
        key: 'version',
        label: labels.version,
        width: '10%',
        sortable: true
      }, {
        key: 'status',
        label: labels.state,
        width: '10%',
        sortable: true,
        allowHTML: true,
        formatter: USPColumnFormatters.checklistState
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '10%',
        formatter: USPFormatters.actions,
        items: [{
          clazz: 'view',
          title: labels.viewChecklistTitle,
          label: labels.viewChecklist,
          enabled: true
        }, {
          clazz: 'edit',
          title: labels.editChecklistTitle,
          label: labels.editChecklist,
          enabled: function() {
            if (permissions.canEdit === true) {
              return (this.record.get('status') === 'DRAFT');
            }
            return false;
          }
        }, {
          clazz: 'editOwnership',
          title: labels.editOwnershipTitle,
          label: labels.editOwnership,
          enabled: function() {
            if (permissions.canEdit === true) {
              //can update ownership when either DRAFT or PUBLISHED
              return (this.record.get('status') === 'DRAFT' || this.record.get('status') === 'PUBLISHED');
            }
            return false;
          }
        }, {
          clazz: 'editDesign',
          title: labels.editChecklistDefinitonTitle,
          label: labels.editChecklistDefinition,
          enabled: permissions.canEdit
        }, {
          clazz: 'publish',
          title: labels.publishChecklistTitle,
          label: labels.publishChecklist,
          enabled: function() {
            if (permissions.canPublish === true) {
              return (this.record.get('status') === 'DRAFT');
            }
            return false;
          }
        }, {
          clazz: 'archive',
          title: labels.deleteChecklistTitle,
          label: labels.deleteChecklist,
          enabled: function() {
            if (permissions.canArchive === true) {
              return (this.record.get('status') === 'PUBLISHED');
            }
            return false;
          }
        }, {
          clazz: 'remove',
          title: labels.removeChecklistTitle,
          label: labels.removeChecklist,
          enabled: function() {
            if (permissions.canRemove === true) {
              return (this.record.get('status') === 'DRAFT');
            }
            return false;
          }
        }]
      }]);
    }
  }, {
    ATTRS: {
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
          }];
        }
      },
      securityDomains: {
        setter: function(val) {
          var securityDomains = {};

          (val || []).forEach(function(sd) {
            if (sd.code) {
              securityDomains[sd.code] = sd;
            }
          });

          return securityDomains;
        }
      }
    }
  });

  Y.namespace('app.admin.checklist').ChecklistDefinitionFilteredResults = Y.Base.create('checklistDefinitionFilteredResults', Y.usp.app.FilteredResults, [], {
    filterType: Y.app.admin.checklist.ChecklistDefinitionFilter,
    resultsType: Y.app.admin.checklist.ChecklistDefinitionResults
  });

}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'results-formatters',
    'results-templates',
    'checklist-definition-filter',
    'app-filtered-results',
    'app-filter',
    'app-results',
    'results-table-search-state-plugin',
    'usp-checklist-ChecklistDefinition',
    'caserecording-results-formatters'
  ]
});