YUI.add('checklist-instance-results-view', function(Y) {
  'use-strict';

  /**
   * A view for displaying checklist instance results and the associated filter
   * 
   * A thin wrapper around the appropriate search view
   */
  Y.namespace('app.checklist').ChecklistInstanceResultsView = Y.Base.create('checklistInstanceResultsView', Y.View, [], {
    initializer: function(config) {
      var subject = config.subject,
        subjectType = subject.subjectType,
        resultsConfig = config.resultsConfig || {};

      //setup config object for the view
      var viewConfig = Y.mix({
        subject: subject
      }, resultsConfig);

      var viewConstructor;
      if ('group' === subjectType) {
        viewConstructor = Y.app.checklist.GroupChecklistInstanceSearchControllerView;
      } else {
        viewConstructor = Y.app.checklist.PersonChecklistInstanceSearchControllerView;
      }

      //construct an instance of the results view
      this.checklistInstanceResults = new viewConstructor(viewConfig).addTarget(this);
    },
    render: function() {
      var container = this.get('container');

      container.setHTML(this.checklistInstanceResults.render().get('container'));
      return this;
    },
    destructor: function() {
      if (this.checklistInstanceResults) {
        this.checklistInstanceResults.removeTarget(this);
        this.checklistInstanceResults.destroy();
        delete this.checklistInstanceResults;
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'checklist-instance-search-controller'
  ]
});