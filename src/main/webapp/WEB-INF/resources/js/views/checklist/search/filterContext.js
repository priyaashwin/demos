YUI.add('checklist-instance-filter-context', function(Y) {
  Y.namespace('app').ChecklistInstanceFilterContext = Y.Base.create('checklistInstanceFilterContext', Y.usp.app.FilterContext, [], {
    template: Y.Handlebars.templates["checklistActiveFilter"],
    initializer: function() {
      //update blacklist property
      this.blacklist = Y.app.ChecklistInstanceFilterContext.superclass.blacklist.concat([
        'includeOwnedByIndividual',
        'includeOwnedByTeam',
        'includeOwnedByTeamMembers',
        'teamIds',
        'owningTeamsForPersonOwners',
        'includeTeamMembers',
        'includeTeamOwners',
        'includeNoOwningTeam'
      ]);
    }
  });
}, '0.0.1', {
  requires: ['app-filter', 'handlebars-checklist-templates']
});