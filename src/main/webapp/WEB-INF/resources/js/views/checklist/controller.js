YUI.add('checklist-controller', function(Y) {
  'use-strict';

  /**
   * A Controller view that extends Y.App and manages 2 view instances
   * These are `results` the standard results view
   * and `add` the add checklist view
   */
  Y.namespace('app.checklist').ChecklistControllerView = Y.Base.create('checklistControllerView', Y.App, [], {
    initializer: function(config) {
      var permissions = config.permissions || {};

      //setup a new toolbar instance - use this to look for specific events
      //i.e. don't use the bubbled events from toolbars created in the view
      //instances
      this.toolbar = new Y.usp.app.AppToolbar({
        permissions: permissions
      }).addTarget(this);

      this._controllerEvtHandlers = [
        this.toolbar.on('appToolbar:add', this.showAddView, this),
        this.on('*:viewChecklistResults', this.showResultsView, this),
        this.after('navigate', this._resetScroll, this)
      ];
    },
    destructor: function() {
      this.toolbar.destroy();
      delete this.toolbar;

      this._controllerEvtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._controllerEvtHandlers;
    },
    _resetScroll:function(){
      Y.one('html').removeClass('noScroll');
    },
    views: {
      results: {
        type: Y.app.checklist.ChecklistInstanceResultsView
      },
      add: {
        type: Y.app.checklist.ChecklistInstanceAddView
      }
    },
    showAddView: function() {
      this.navigate(this.get('addRoute'));
    },
    showResultsView: function() {
      this.navigate(this.get('viewRoute'));
    },
    handleRouteResults: function() {
      var activeView = this.get('activeView');
      if (!activeView || activeView.name !== 'checklistInstanceResultsView') {
        var subject = this.get('subject'),
          resultsConfig = this.get('resultsConfig');
      
        //set title
        Y.one('h3 .context-title').set('text', resultsConfig.title || '');
  
        //set style on first button
        this.toolbar.get('toolbarNode').one('li').removeClass('last');
  
        //setup buttons - show add button
        this.toolbar.showButton('add');
  
        this.showView('results', {
          resultsConfig: resultsConfig,
          subject: subject
        });
      }
    },
    handleRouteAdd: function() {
      var activeView = this.get('activeView');
      if (!activeView || activeView.name !== 'checklistInstanceAddView') {
  
        var subject = this.get('subject'),
          addConfig = this.get('addConfig');
        //set title
        Y.one('h3 .context-title').set('text', addConfig.title || '');
        //set style on first button
        this.toolbar.get('toolbarNode').one('li').addClass('last');
  
        //setup buttons - hide add button
        this.toolbar.hideButton('add');
  
        this.showView('add', {
          addConfig: addConfig,
          subject: subject
        });
      }
    
    },
    /**
     * Because the router fires for all changes to the HTML5 history object
     * we only route if the history state is not replace.
     * 
     * `replace` is triggered by the ResultsSearchStatePlugin so fires
     * false positive routing
     */
    _afterHistoryChange: function (e) {
      if(e.src==='replace'){
        return;
      }
      
      Y.app.checklist.ChecklistControllerView.superclass._afterHistoryChange.apply(this, arguments);
    }
  }, {
    serverRouting: true,
    ATTRS: {
      routes: {
        value: [{
            path: '/view',
            callbacks: 'handleRouteResults'
          },
          {
            path: '/add',
            callbacks: 'handleRouteAdd'
          }
        ]
      },
      subject: {
        value: {}
      },
      resultsConfig: {
        value: {}
      },
      addConfig: {
        value: {}
      },
      addRoute: {
        value: '#none'
      },
      viewRoute: {
        value: '#none'
      },
      permissions: {
        value: {}
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-base',
    'checklist-instance-results-view',
    'checklist-instance-add-view'
  ]
});