YUI.add('checklist-definition-controller-view', function(Y) {
    'use-strict';

    var L = Y.Lang,
        Q = Y.QueryString;
    
    Y.namespace('app.admin.checklist').ChecklistDefinitionControllerView = Y.Base.create('checklistDefinitionControllerView', Y.View, [], {
        initializer: function(config) {
            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            var resultsConfig = {
                searchConfig: config.searchConfig,
                filterConfig:config.filterConfig,
                filterContextConfig: config.filterContextConfig,
                permissions: config.permissions
            };
            
            this.checklistDefinitionResults = new Y.app.admin.checklist.ChecklistDefinitionFilteredResults(resultsConfig).addTarget(this);
            
            this.checklistDefinitionDialog = new Y.app.admin.checklist.ChecklistDefinitionDialog(config.dialogConfig).addTarget(this).render();
            
            this._checklistDefinitionEvtHandlers = [
                this.on('*:saved', this.checklistDefinitionResults.reload, this.checklistDefinitionResults),
                this.on('*:editDesign', this.launchChecklistDesigner, this),
                this.on('*:edit', this.showEditChecklistDefinitionDialog, this),
                this.on('*:view', this.showViewChecklistDefinitionDialog, this),
                this.on('*:publish', this.showPublishChecklistDefinitionDialog, this),
                this.on('*:archive', this.showArchiveChecklistDefinitionDialog, this),
                this.on('*:remove', this.showRemoveChecklistDefinitionDialog, this),
                this.on('*:editOwnership', this.showEditOwnershipChecklistDefinitionDialog, this),
                this.on('*:addChecklistDesign', this.launchChecklistDesigner, this),
                this.after('securityDomainsChange', this.setupSecurityDomains, this),
                Y.Global.on('refreshList', this.checklistDefinitionResults.reload, this.checklistDefinitionResults),
                this.after('loadedChange', this._renderSecurityDomainList, this),
                this.on('*:clearErrors', function() {
                  //Clear any errors in the dialog
                  if (this.checklistDefinitionDialog) {
                      this.checklistDefinitionDialog.multiPanelModelErrors.clearErrors();
                  }
                }, this)
            ];
            
            this._getSecurityDomains().then(function (results) {
              this.set('securityDomains', results.toJSON()||[]);
            }.bind(this));
        },
        setupSecurityDomains:function(e){
          //push security domains into filter
          this.checklistDefinitionResults.get('filter').get('resultsFilter').set('securityDomains', e.newVal);
          //push into results
          this.checklistDefinitionResults.get('results').set('securityDomains', e.newVal);
          
          this.set('loaded', true);
        },
        destructor: function() {
            this._checklistDefinitionEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._checklistDefinitionEvtHandlers;
            
            this.checklistDefinitionResults.removeTarget(this);
            this.checklistDefinitionResults.destroy();
            delete this.checklistDefinitionResults;
            
            this.checklistDefinitionDialog.removeTarget(this);
            this.checklistDefinitionDialog.destroy();
            delete this.checklistDefinitionDialog;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;
        },
        render: function() {
            //render occurs once loaded attribute is set
            return this;
        },
        _renderSecurityDomainList:function(){
          if(this.get('loaded')){
            this.get('container').setHTML(this.checklistDefinitionResults.render().get('container'));
          }
        },
        _getSecurityDomains: function() {
          var model = new Y.usp.securityextension.PaginatedSecurityDomainList({
              url:this.get('securityDomainUrl') 
          });
          return new Y.Promise(function(resolve, reject) {
              var data = model.load(function(err) {
                  if (err !== null) {
                      //failed for some reason so reject the promise
                      reject(err);
                  }else{
                    //success, so resolve the promise
                    resolve(data);
                  }
              });
          });
        },
        showViewChecklistDefinitionDialog:function(e){
          var record=e.record,
              permissions=e.permissions;
          
          var config = this.get('dialogConfig').views.view.config;
          
          if(permissions.canView){
            this.checklistDefinitionDialog.showView('view', {
              model: new Y.app.admin.checklist.ChecklistDefinitionViewForm({
                  id:record.get('id'),
                  url: config.url
              }),
              securityDomains:this.get('securityDomains')
            },{
              modelLoad:true
            });
          }
        },
        showEditChecklistDefinitionDialog:function(e){
          var record=e.record,
              permissions=e.permissions;

          var config = this.get('dialogConfig').views.edit.config;

          if(permissions.canEdit){
            this.checklistDefinitionDialog.showView('edit', {
              model: new Y.app.admin.checklist.ChecklistDefinitionEditForm({
                  id:record.get('id'),
                  url: config.url,
                  labels:config.validationLabels
              }),
              securityDomains:this.get('securityDomains')
            },{
              modelLoad:true
            },function(view) {
              this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          });
          }
        },
        showEditOwnershipChecklistDefinitionDialog:function(e){
          var record=e.record,
              permissions=e.permissions;

          var config = this.get('dialogConfig').views.editOwnership.config;

          if(permissions.canEdit){
            this.checklistDefinitionDialog.showView('editOwnership', {
              model: new Y.app.admin.checklist.ChecklistDefinitionOwnershipEditForm({
                  id:record.get('id'),
                  url: config.url
              }),
              securityDomains:this.get('securityDomains')
            },{
              modelLoad:true
            },function(view) {
              this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          });
          }
        },
        showPublishChecklistDefinitionDialog:function(e){
          var record=e.record,
              permissions=e.permissions;
          
          var config = this.get('dialogConfig').views.publish.config;
          
          if(permissions.canPublish){
            this.checklistDefinitionDialog.showView('publish', {
              model:  new Y.app.admin.checklist.ChecklistDefinitionStatusChangeForm(
                      Y.merge(record.toJSON(), {url:L.sub(config.url, {action:config.action})}))
            },function(view) {
              this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          });
          }
        },
        showArchiveChecklistDefinitionDialog:function(e){
          var record=e.record,
              permissions=e.permissions;
          
          var config = this.get('dialogConfig').views.archive.config;
          
          if(permissions.canArchive){
            this.checklistDefinitionDialog.showView('archive', {
              model: new Y.app.admin.checklist.ChecklistDefinitionStatusChangeForm(
                      Y.merge(record.toJSON(), {url:L.sub(config.url, {action:config.action})}))
            },function(view) {
              this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          });
          }
        },
        showRemoveChecklistDefinitionDialog:function(e){
          var record=e.record,
              permissions=e.permissions;
          
          var config = this.get('dialogConfig').views.remove.config;
          
          if(permissions.canRemove){
            this.checklistDefinitionDialog.showView('remove', {
              model: new Y.app.admin.checklist.ChecklistDefinitionRemoveForm(
                      Y.merge(record.toJSON(), {url:config.url}))
              },function(view) {
              this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          });
          }
        },
        launchChecklistDesigner:function(e){
          var record=e.record,
            id;
          
          if(record){
            id=record.get('id');
          }
          
          var designerConfig = this.get('designerConfig'),
              designerURL=designerConfig.designerURL,
              url;
          
          if(!(id===undefined || id===null)){
              url=designerURL+'?'+Q.stringify({id:id});
          }else{
              url=designerURL;
          }
          
          var ref=window.open( 
                  url,  
                  'Work List Designer',
                  'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1024,height=768,left=100,top=100'
              );  
          if(ref){
              //attempt to move to front
              ref.focus();
          }
          
          return;
      }
    },{
      ATTRS: {
        searchConfig: {
            value:{
                url: {},
                noDataMessage: {},
                permissions: {},
                resultsCountNode:{}
            }
        },
        filterContextConfig: {
            value:{
                labels: {}
            }
        },
        filterConfig: {
            value:{
                labels: {}
            }
        },
        dialogConfig: {
            value:{
                labels: {}
            }
        },
        securityDomainUrl:{
          value:''
        },
        securityDomains:{
          value:[]
        },
        loaded:{
          value:false
        }
      }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'app-toolbar',
               'view',
               'checklist-definition-results',
               'checklist-definition-dialog-views',
               'usp-securityextension-SecurityDomain',
               'querystring-stringify'
               ]
});
