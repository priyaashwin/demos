YUI.add('checklist-instance-add-view', function(Y) {
  'use-strict';

  /**
   * A view for displaying the checklist instance add screen and the associated filter
   */
  Y.namespace('app.checklist').ChecklistInstanceAddView = Y.Base.create('checklistInstanceAddView', Y.View, [], {
    initializer: function(config) {
      var subject = config.subject,
          addConfig=config.addConfig||{};

      //setup config object for the view
      var viewConfig = Y.mix({
        subject: subject
      }, addConfig);

      //construct an instance of the results view
      this.checklistInstanceAddResults = new Y.app.checklist.ChecklistInstanceAddControllerView(viewConfig).addTarget(this);
    },
    render: function() {
      var container = this.get('container');

      container.setHTML(this.checklistInstanceAddResults.render().get('container'));
      return this;
    },
    destructor: function() {
      this.checklistInstanceAddResults.removeTarget(this);
      this.checklistInstanceAddResults.destroy();
      delete this.checklistInstanceAddResults;
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'checklist-instance-add-controller'
  ]
});