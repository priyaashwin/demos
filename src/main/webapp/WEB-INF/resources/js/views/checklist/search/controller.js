YUI.add('checklist-instance-search-controller', function (Y) {
    var L = Y.Lang,
        E = Y.Escape,
        USPFormatters = Y.usp.ColumnFormatters,
        O = Y.Object;

    var _hasError = function (record, errorType) {
        var ownershipErrors = record.get('errors').filter(function (error) {
            return error.exceptionClass === errorType;
        });
        return ownershipErrors.length > 0;
    },
        hasWriteAccess = function (record) {
            return record.hasAccessLevel('WRITE');
        },
        hasReadDetailAccess = function (record) {
            return record.hasAccessLevel('READ_DETAIL');
        };

    var statusFormatter = function (o) {
        var val = o.value,
            html = ['<a href="#" class="tool-tip checklist-state" data-title="{stateTitle}" data-content="{stateContent}" data-usemarkup="{useMarkup}" aria-label="{stateTitle}">',
                '',
                '</a>'
            ],
            linkIndex = 0,
            textIndex = 1,
            errors;
        switch (val) {
            case 'INITIALIZING':
            /* falls through */
            case 'NOT_STARTED':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'Not started',
                    stateContent: 'This work list has not yet started',
                    useMarkup: false
                });
                html[textIndex] = '<i class="checklist-status-indicator not-started"></i>';
                o.rowClass += 'not-started';
                break;
            case 'ON_TIME':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'On time',
                    stateContent: 'This work list is currently on time',
                    useMarkup: false
                });
                html[textIndex] = '<i class="checklist-status-indicator on-time"></i>';
                o.rowClass += 'on-time';
                break;
            case 'LAST_REMINDER':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'Reminder',
                    stateContent: 'This work list is at the last reminder',
                    useMarkup: false
                });
                html[textIndex] = '<i class="checklist-status-indicator at-risk"></i>';
                o.rowClass += 'at-risk';
                break;
            case 'OVERDUE':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'Overdue',
                    stateContent: 'This work list is overdue',
                    useMarkup: false
                });
                html[textIndex] = '<i class="checklist-status-indicator late"></i>';
                o.rowClass += 'late';
                break;
            case 'COMPLETE':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'Complete',
                    stateContent: 'This work list is complete',
                    useMarkup: false
                });
                html[textIndex] = '<i class="fa fa-check-square checklist-indicator checklist-complete"></i>';
                o.rowClass += 'checklist-complete';
                break;
            case 'ARCHIVED':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'Archived',
                    stateContent: 'This work list has been deleted',
                    useMarkup: false
                });
                html[textIndex] = '<i class="fa fa-trash checklist-indicator checklist-deleted"></i>';
                o.rowClass += 'checklist-deleted';
                break;
            case 'PAUSED':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'Paused',
                    stateContent: 'This work list is paused',
                    useMarkup: false
                });
                html[textIndex] = '<i class="fa fa-pause checklist-indicator"></i>';
                o.rowClass += 'checklist-paused';
                break;
            case 'IN_ERROR':
                html[linkIndex] = L.sub(html[linkIndex], {
                    stateTitle: 'This work list is unable to continue due to an error',
                    stateContent: '',
                    useMarkup: true
                });
                //populate errors list
                errors = (o.record.get('errors') || []).map(function (err) {
                    return (['<li>', E.html(err.error), '</li>'].join(''));
                }).join('');
                html[textIndex] = '<i class="fa fa-stop-circle checklist-indicator checklist-error"></i><div class="content-markup"><div class="errors"><ul class="simple-list">' + errors + '</ul></div></div>';
                o.rowClass += 'checklist-error';
                break;
        }
        return html.join('');
    };

    var BaseChecklistInstanceSearchControllerView = Y.Base.create('baseChecklistInstanceSearchControllerView', Y.View, [], {
        resultsListClass: undefined,
        _getConstructor: function (type) {
            return L.isString(type) ? O.getValue(Y, type.split('.')) : type;
        },
        initializer: function (config) {
            var searchConfig = config.searchConfig || {},
                subject = config.subject || {},
                permissions = config.permissions || {},
                dialogConfig = Y.mix({
                    subject: subject
                }, config.dialogConfig || {});

            var resultsConfig = {
                searchConfig: Y.merge(searchConfig, {
                    //mix the subject into the search config so it is available when getting the URL
                    subject: subject
                }),
                filterConfig: Y.merge(config.filterConfig, {
                    permissions: permissions,
                    subject: subject
                }),
                filterContextConfig: config.filterContextConfig,
                quickFiltersConfig: config.quickFiltersConfig,
                permissions: permissions
            };

            var ResultsConstructor = this._getConstructor(this.resultsListClass);
            this.results = new ResultsConstructor(resultsConfig).addTarget(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions
            }).addTarget(this);

            //create checklist instance dialog view
            this.statusDialog = new Y.app.ChecklistInstanceStatusDialogView({
                dialogConfig: dialogConfig,
                subject: subject,
                currentPersonId: searchConfig.currentPersonId
            }).render().addTarget(this);

            //create checklist instance dialog view
            this.priorityDialog = new Y.app.ChecklistInstancePriorityDialogView(dialogConfig.views.prioritiseChecklist).render().addTarget(this);
                
            //create checklist task reassign dialog view
            this.taskReassignDialog = new Y.app.ChecklistInstanceTaskReassignDialogView(dialogConfig.views.reassignTask).render().addTarget(this);

            //event handlers
            this._checklistEvtHandlers = [
                //subscribe to events where we need to update our results list
                this.on([
                    'checklistInstanceStatusChangeView:saved',
                    'checklistInstancePauseView:saved',
                    'checklistInstanceResumeView:saved',
                    'checklistInstanceRemoveView:saved',
                    'checklistInstanceReassignView:saved',
                    'checklistInstanceResetEndDateView:saved',
                    'checklistInstanceStartView:saved',
                    'checklistInstancePriorityDialogView:saved',
                    'checklistInstanceTaskReassignDialogView:saved'
                ], this.results.reload, this.results),
                this.on('viewChecklist', function (e) {
                    var checklist = e.checklist;
                    if (checklist && checklist.hasAccessLevel('READ_SUMMARY') === true) {
                        this.results.get('results').get('results').showMask();
                        //go to the checklist instance page
                        window.location = L.sub(searchConfig.viewURL, {
                            id: checklist.get('id')
                        });
                    }
                }, this),
                this.on('*:view', this.handleView, this),
                this.on('*:delete', this.handleDelete, this),
                this.on('*:remove', this.handleRemove, this),
                this.on('*:reassign', this.handleReassign, this),
                this.on('*:reassignTaskOwner', this.handleReassignTaskOwner, this),
                this.on('*:prioritise', this.handlePrioritise, this),
                this.on('*:pause', this.handlePause, this),
                this.on('*:resume', this.handleResume, this),
                this.on('*:resetEndDate', this.handleResetEndDate, this),
                this.on('*:startEarly', this.handleStartEarly, this),
                this.on('*:viewErrors', this.handleViewErrors, this)
            ];
        },
        destructor: function () {
            this._checklistEvtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._checklistEvtHandlers;

            this.statusDialog.removeTarget(this);
            this.statusDialog.destroy();
            delete this.statusDialog;

            this.priorityDialog.removeTarget(this);
            this.priorityDialog.destroy();
            delete this.priorityDialog;

            this.taskReassignDialog.removeTarget(this);
            this.taskReassignDialog.destroy();
            delete this.taskReassignDialog;

            this.results.removeTarget(this);
            this.results.destroy();
            delete this.results;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;

        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //append the results
            contentNode.append(this.results.render().get('container'));

            //set fragment into container
            this.get('container').setHTML(contentNode);

            return this;
        },
        getSubject: function (record) {
            return ({
                subjectType: record.get('subject!subjectType'),
                subjectName: record.get('subject!name'),
                subjectIdentifier: record.get('subject!identifier')
            });
        },
        handleView: function (e) {
            var permissions = this.get('permissions'),
                record = e.record;

            if (permissions.canView === true) {
                //fire event
                this.fire('viewChecklist', {
                    checklist: record
                });
            }
        },
        handleDelete: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasWriteAccess(record) && permissions.canDelete) {
                this.statusDialog.showDeleteChecklistDialog(record.toJSON(), subject);
            }
        },
        handleRemove: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasWriteAccess(record) && permissions.canRemove) {
                this.statusDialog.showRemoveChecklistDialog(record.toJSON(), subject);
            }
        },
        handleReassign: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasWriteAccess(record) && permissions.canReassign) {
                this.statusDialog.showReassignChecklistDialog(record.toJSON(), subject);
            }
        },
        handleReassignTaskOwner: function (e) {
            var record = e.record;

            if (hasWriteAccess(record)) {
                this.taskReassignDialog.showDialog(record.get('id'));
            }
        },
        handlePrioritise: function (e) {
            var record = e.record;

            if (hasWriteAccess(record)) {
                this.priorityDialog.showDialog(record.get('id'));
            }
        },
        handlePause: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasWriteAccess(record) && permissions.canPause) {
                this.statusDialog.showPauseChecklistDialog(record.toJSON(), subject);
            }
        },
        handleResume: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasWriteAccess(record) && permissions.canResume) {
                this.statusDialog.showResumeChecklistDialog(record.toJSON(), subject);
            }
        },
        handleResetEndDate: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasWriteAccess(record) && permissions.canResetEndDate) {
                this.statusDialog.showResetChecklistEndDateDialog(record.toJSON(), subject);
            }
        },
        handleStartEarly: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasWriteAccess(record) && permissions.canStartEarly) {
                this.statusDialog.showChecklistStartDateDialog(record.toJSON(), subject);
            }
        },
        handleViewErrors: function (e) {
            var permissions = this.get('permissions'),
                record = e.record,
                subject = this.getSubject(record);
            if (hasReadDetailAccess(record) && permissions.canViewErrors) {
                this.statusDialog.showChecklistErrorsDialog(record.toJSON(), subject);
            }
        }
    }, {
        ATTRS: {
            permissions: {
                value: {}
            }
        }
    });

    var PaginatedFilteredChecklistInstanceSearchResultList = Y.Base.create("paginatedFilteredChecklistInstanceSearchResultList", Y.usp.checklist.PaginatedChecklistInstanceList, [Y.usp.ResultsFilterURLMixin], {
        whiteList: ['status', 'priority', 'dueDateFrom', 'dueDateTo', 'checklistDefinitionId', 'memberId', 'subjectId']
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    var BaseChecklistInstanceResults = Y.Base.create('baseChecklistInstanceResults', Y.usp.app.Results, [], {
        getResultsListModel: function (config) {
            var subject = config.subject;

            return new PaginatedFilteredChecklistInstanceSearchResultList({
                url: L.sub(config.url, {
                    subjectType: subject.subjectType.toUpperCase(),
                    subjectId: subject.subjectId
                }),
                requestedAccess: 'READ_SUMMARY',
                filterModel: config.filterModel
            });
        },
        // results list formatters
        progressFormatter: function (o) {
            return Y.Handlebars.templates.checklistInstanceResultsPercentComplete({
                modelData: o.data,
                labels: this.labels || {},
                expanded: false
            });
        },
        priorityFormatter: function (o) {
            var checklistPriorityEnums = Y.usp.checklist.enum.ChecklistInstancePriority.values;
            Y.Array.each(checklistPriorityEnums, function (checklistInstancePriority) {
                if (o.value == checklistInstancePriority.enumValue) {
                    o.rowClass += checklistInstancePriority.name.toLowerCase() + '-priority';
                }
            });

            return USPFormatters.enumType(o);
        },
        progressFormatterExpanded: function (o) {
            return Y.Handlebars.templates.checklistInstanceResultsPercentComplete({
                modelData: o.data,
                labels: this.labels || {},
                expanded: true
            });
        },
        nameFormatterExpanded: function (o) {
            var data = Y.merge(o.data, {
                //Use the marked library
                //!!ENSURE THIS IS LOADED !!
                description: marked ? marked(o.data.description || '') : o.data.description
            });
            return Y.Handlebars.templates.checklistInstanceResultsName({
                modelData: data,
                labels: this.labels || {},
                permissions: {
                    canView: this.permissions.canView && o.record.hasAccessLevel('READ_DETAIL') === true
                },
                expanded: true
            });
        },
        nameFormatterCollapsed: function (o) {
            return Y.Handlebars.templates.checklistInstanceResultsName({
                modelData: o.data,
                labels: this.labels || {},
                permissions: {
                    canView: this.permissions.canView && o.record.hasAccessLevel('READ_DETAIL') === true
                },
                expanded: false
            });
        },
        dateDueFormatterExpanded: function (o) {
            return Y.Handlebars.templates.checklistInstanceResultsDateDue({
                modelData: o.data,
                labels: this.labels || {},
                expanded: true
            });
        },
        ownershipFormatter: function (ownershipDetails) {
            let ownership = '';

            if (ownershipDetails) {
                ownership = ownershipDetails.value.ownerDetails.name;
                var owningTeam = ownershipDetails.value.owningTeamDetails;
                if (owningTeam && owningTeam.name !== ownership) {
                    ownership = ownership + ' / ' + owningTeam.name;
                } else if (ownershipDetails.value.ownerDetails.type === 'PERSON') {
                    ownership = ownership + ' / No owning team';
                }
            }
            return ownership;
        },
        getActions: function (permissions, labels) {
            return [{
                clazz: 'view',
                title: labels.viewChecklistTitle,
                label: labels.viewChecklist,
                visible: function () {
                    return permissions.canView === true && this.record.hasAccessLevel('READ_DETAIL') === true;
                }
            }, {
                clazz: 'prioritise',
                title: labels.prioritiseChecklistTitle,
                label: labels.prioritiseChecklist,
                visible: function () {
                    //ensure visible if user can prioritise, or can escalate / deescalate when a priority has already been set
                    return (permissions.canPrioritise === true && ((this.record.get('priority') ? (permissions.canEscalate || permissions.canDeescalate) : true))) && this.record.hasAccessLevel('WRITE') === true;
                },
                enabled: function () {
                    var instanceStatus = this.record.get('status');

                    return (instanceStatus !== 'INITIALIZING' && instanceStatus !== 'NOT_STARTED' && instanceStatus !== 'COMPLETE' && instanceStatus !== 'ARCHIVED');
                }
            }, {
                clazz: 'reassign',
                title: labels.reassignChecklistTitle,
                label: labels.reassignChecklist,
                visible: function () {
                    return permissions.canReassign === true && this.record.hasAccessLevel('WRITE') === true;
                },
                enabled: function () {
                    var instanceStatus = this.record.get('status');

                    return (instanceStatus !== 'ARCHIVED' && instanceStatus !== 'INITIALIZING' && (instanceStatus !== 'IN_ERROR' || _hasError(this.record, 'ChecklistAssignOwnerException')) && instanceStatus !== 'COMPLETE');
                }
            }, {
                clazz: 'reassignTaskOwner',
                title: labels.reassignTaskTitle,
                label: labels.reassignTask,
                visible: function () {
                    return permissions.canReassign === true && this.record.hasAccessLevel('WRITE') === true;
                },
                enabled: true
            }, {
                clazz: 'pause',
                title: labels.pauseChecklistTitle,
                label: labels.pauseChecklist,
                visible: function () {
                    //only visible if checklist is configured to pause
                    return this.record.get('canPause') === true && permissions.canPause === true && this.record.hasAccessLevel('WRITE') === true;
                },
                enabled: function () {
                    var instanceStatus = this.record.get('status');

                    return (instanceStatus !== 'PAUSED' && instanceStatus !== 'INITIALIZING' && instanceStatus !== 'IN_ERROR' && instanceStatus !== 'COMPLETE' && instanceStatus !== 'ARCHIVED');
                }
            }, {
                clazz: 'resume',
                title: labels.resumeChecklistTitle,
                label: labels.resumeChecklist,
                visible: function () {
                    //only visible if checklist is configured to pause
                    return this.record.get('canPause') === true && permissions.canResume === true && this.record.hasAccessLevel('WRITE') === true;
                },
                enabled: function () {
                    return this.record.get('status') === 'PAUSED';
                }
            }, {
                clazz: 'resetEndDate',
                title: labels.resetChecklistEndDateTitle,
                label: labels.resetChecklistEndDate,
                enabled: function () {
                    var instanceStatus = this.record.get('status');

                    //and only available if not paused, in error, or complete
                    return (instanceStatus !== 'PAUSED' && instanceStatus !== 'INITIALIZING' && instanceStatus !== 'IN_ERROR' && instanceStatus !== 'COMPLETE' && instanceStatus !== 'ARCHIVED');
                },
                visible: function () {
                    //only visible if checklist is configured to calculation mode TO_END
                    return permissions.canResetEndDate === true && this.record.get('calculationMode') === 'TO_END' && this.record.hasAccessLevel('WRITE') === true;
                }
            }, {
                clazz: 'startEarly',
                title: labels.startChecklistTitle,
                label: labels.startChecklist,
                enabled: function () {
                    return this.record.get('status') === 'NOT_STARTED';
                },
                visible: function () {
                    //only visible if checklist is not started
                    return permissions.canStartEarly === true && this.record.get('status') == 'NOT_STARTED' && this.record.hasAccessLevel('WRITE') === true;
                }
            }, {
                clazz: 'delete',
                title: labels.deleteChecklistTitle,
                label: labels.deleteChecklist,
                visible: function () {
                    if (permissions.canDelete === true && this.record.hasAccessLevel('WRITE') === true) {
                        return (this.record.get('status') !== 'ARCHIVED');
                    }
                    return false;
                }
            }, {
                clazz: 'remove',
                title: labels.removeChecklistTitle,
                label: labels.removeChecklist,
                visible: function () {
                    return permissions.canRemove && this.record.hasAccessLevel('WRITE') === true;
                }
            }, {
                clazz: 'viewErrors',
                title: labels.viewErrorTitle,
                label: labels.viewError,
                visible: function () {
                    return permissions.canViewErrors && this.record.hasAccessLevel('READ_DETAIL') === true && this.record.get('status') === 'IN_ERROR' && this.data.errors.length > 0;
                }
            }];
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function () {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableExpandableCellsPlugin
                    }];
                }
            }
        }
    });

    var GroupChecklistInstanceResults = Y.Base.create('groupChecklistInstanceResults', BaseChecklistInstanceResults, [], {
        getColumnConfiguration: function (config) {

            var labels = config.labels || {},
                permissions = config.permissions;

            return ([{
                key: 'priority',
                label: labels.priority,
                width: '4%',
                sortable: true,
                formatter: this.priorityFormatter,
                enumTypes: Y.usp.checklist.enum.ChecklistInstancePriority.values
            }, {
                key: 'checklistDefinitionName',
                label: labels.name,
                width: '28%',
                sortable: true,
                allowHTML: true,
                labels: labels,
                collapsedFormatter: this.nameFormatterCollapsed,
                expandedFormatter: this.nameFormatterExpanded,
                //permissions will be used within the above formatters
                permissions: {
                    canView: permissions.canView
                }
            }, {
                key: 'status',
                label: ' ',
                width: '4%',
                allowHTML: true,
                formatter: statusFormatter
            }, {
                key: 'subject!name',
                label: labels.subjectName,
                width: '16%',
                sortable: true,
                formatter: function (o) {
                    return ([o.value, '(' + o.data['subject!identifier'] + ')']).join(' ');
                }
            }, {
                key: 'dateDue',
                label: labels.dateDue,
                sortable: true,
                width: '14%',
                labels: labels,
                allowHTML: true,
                collapsedFormatter: USPFormatters.dateTime,
                expandedFormatter: this.dateDueFormatterExpanded
            }, {
                key: 'percentComplete',
                label: labels.percentComplete,
                width: '10%',
                allowHTML: true,
                labels: labels,
                collapsedFormatter: this.progressFormatter,
                expandedFormatter: this.progressFormatterExpanded
            }, {
                key: 'ownershipDetails',
                label: labels.ownerName,
                width: '16%',
                formatter: this.ownershipFormatter
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '8%',
                formatter: USPFormatters.actions,
                items: this.getActions(permissions, labels)
            }]);

        }
    });

    var PersonChecklistInstanceResults = Y.Base.create('personChecklistInstanceResults', BaseChecklistInstanceResults, [], {
        getColumnConfiguration: function (config) {

            var labels = config.labels || {},
                permissions = config.permissions;

            return ([{
                key: 'priority',
                label: labels.priority,
                width: '5%',
                sortable: true,
                formatter: this.priorityFormatter,
                enumTypes: Y.usp.checklist.enum.ChecklistInstancePriority.values
            }, {
                key: 'checklistDefinitionName',
                label: labels.name,
                width: '30%',
                sortable: true,
                allowHTML: true,
                labels: labels,
                collapsedFormatter: this.nameFormatterCollapsed,
                expandedFormatter: this.nameFormatterExpanded,
                //permissions will be used within the above formatters
                permissions: {
                    canView: permissions.canView
                }
            }, {
                key: 'status',
                label: ' ',
                width: '5%',
                allowHTML: true,
                formatter: statusFormatter
            }, {
                key: 'dateDue',
                label: labels.dateDue,
                sortable: true,
                width: '15%',
                labels: labels,
                allowHTML: true,
                collapsedFormatter: USPFormatters.dateTime,
                expandedFormatter: this.dateDueFormatterExpanded
            }, {
                key: 'percentComplete',
                label: labels.percentComplete,
                width: '15%',
                allowHTML: true,
                labels: labels,
                collapsedFormatter: this.progressFormatter,
                expandedFormatter: this.progressFormatterExpanded
            }, {
                key: 'ownershipDetails',
                label: labels.ownerName,
                width: '22%',
                formatter: this.ownershipFormatter
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '8%',
                formatter: USPFormatters.actions,
                items: this.getActions(permissions, labels)
            }]);

        }
    });

    var GroupChecklistInstanceFilter = Y.Base.create('groupChecklistInstanceFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.GroupChecklistInstanceResultsFilterModel,
        filterType: Y.app.GroupChecklistInstanceResultsFilter,
        filterContextType: Y.app.ChecklistInstanceFilterContext
    });

    Y.namespace('app.checklist').GroupChecklistInstanceFilteredResults = Y.Base.create('groupChecklistInstanceFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: GroupChecklistInstanceFilter,
        resultsType: GroupChecklistInstanceResults
    });

    var PersonChecklistInstanceFilter = Y.Base.create('personChecklistInstanceFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.PersonChecklistInstanceResultsFilterModel,
        filterType: Y.app.PersonChecklistInstanceResultsFilter,
        filterContextType: Y.app.ChecklistInstanceFilterContext
    });

    Y.namespace('app.checklist').PersonChecklistInstanceFilteredResults = Y.Base.create('personChecklistInstanceFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: PersonChecklistInstanceFilter,
        resultsType: PersonChecklistInstanceResults
    });

    Y.namespace('app.checklist').PersonChecklistInstanceSearchControllerView = Y.Base.create('personChecklistInstanceSearchControllerView', BaseChecklistInstanceSearchControllerView, [], {
        resultsListClass: Y.app.checklist.PersonChecklistInstanceFilteredResults
    });

    Y.namespace('app.checklist').GroupChecklistInstanceSearchControllerView = Y.Base.create('groupChecklistInstanceSearchControllerView', BaseChecklistInstanceSearchControllerView, [], {
        resultsListClass: Y.app.checklist.GroupChecklistInstanceFilteredResults
    });

}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'app-results',
        'app-toolbar',
        'app-filtered-results',
        'app-filter',
        'escape',
        'results-formatters',
        'results-table-menu-plugin',
        'results-table-keyboard-nav-plugin',
        'results-table-search-state-plugin',
        'results-table-expandable-cells-plugin',
        'checklist-instance-results-filter',
        'checklist-instance-filter-context',
        'checklist-instance-dialog-view',
        'checklist-instance-priority-dialog-views',
        'checklist-instance-task-reassign-dialog-views',
        'usp-checklist-ChecklistInstance',
        'handlebars-helpers',
        'handlebars-checklist-partials',
        'handlebars-checklist-templates',
        'checklist-priority-enumeration'
    ]
});