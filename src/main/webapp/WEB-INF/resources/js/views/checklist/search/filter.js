YUI.add('checklist-instance-results-filter', function (Y) {

    var L = Y.Lang,
        DB = Y.usp.DataBindingUtil;

    Y.namespace('app').BaseChecklistInstanceResultsFilterModel = Y.Base.create('checklistInstanceResultsFilterModel', Y.usp.ResultsFilterModel, [Y.usp.ModelValidate], {
        customValidation: function (attrs) {
            //enforce our validation first - if we succeed this will call the parent validate
            var errors = {},
                dueDateFrom = attrs['dueDateFrom'],
                dueDateTo = attrs['dueDateTo'];

            if (L.isNumber(dueDateFrom) && L.isNumber(dueDateTo)) {
                if (dueDateFrom > dueDateTo) {
                    errors['dueDateTo'] = 'Due date from must be before due date to';
                }
            }
            return errors;
        }
    }, {
        ATTRS: {
            status: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: function (v) {
                    var value = Y.usp.ResultsFilterModel.getListValue(v) || [];

                    if (value.indexOf('NOT_STARTED') > -1 && value.indexOf('INITIALIZING') === -1) {
                        //insert the INITIALIZING value if NOT_STARTED is added
                        value.push('INITIALIZING');
                    }

                    return value;
                },
                //default status list defined here
                value: ['NOT_STARTED', 'ON_TIME', 'LAST_REMINDER', 'OVERDUE', 'PAUSED']
            },
            priority: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: ['NONE', 'LOW', 'MEDIUM', 'HIGH']
            },
            dueDateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            dueDateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            checklistDefinitionId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null,
                valueFn: function () {
                    var urlParameters = window.location.search,
                        checklistDefinitionId = null,
                        regExpValue = new RegExp('[?&]checklistDefinitionId=([^&#]*)').exec(urlParameters);

                    if (regExpValue) {
                        checklistDefinitionId = [parseInt(regExpValue[1], 10)];
                    }

                    return checklistDefinitionId;
                }
            }
        }
    });

    Y.namespace('app').PersonChecklistInstanceResultsFilterModel = Y.Base.create('personChecklistInstanceResultsFilterModel', Y.app.BaseChecklistInstanceResultsFilterModel, [Y.usp.ModelValidate], {});

    Y.namespace('app').GroupChecklistInstanceResultsFilterModel = Y.Base.create('groupChecklistInstanceResultsFilterModel', Y.app.BaseChecklistInstanceResultsFilterModel, [Y.usp.ModelValidate], {}, {
        ATTRS: {
            //Member ID is only used as part of the Group filter
            memberId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            }
        }
    });

    Y.namespace('app').UserChecklistInstanceResultsFilterModel = Y.Base.create('userChecklistInstanceResultsFilterModel', Y.app.BaseChecklistInstanceResultsFilterModel, [Y.usp.ModelValidate], {
        customValidation: function (attrs) {
            //enforce our validation first - if we succeed this will call the parent validate
            var errors = {},
                startDateFrom = attrs['startDateFrom'],
                startDateTo = attrs['startDateTo'];

            if (L.isNumber(startDateFrom) && L.isNumber(startDateTo)) {
                if (startDateFrom > startDateTo) {
                    errors['startDateTo'] = 'Start date from must be before start date to';
                }
            }

            return errors;
        }
    }, {
        ATTRS: {
            subjectId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            },
            ownerId: {
                value: null
            },
            status: {
                //default status list defined here
                //** NOTE - changing this will change both the CardView and the results list view **//
                value: ['ON_TIME', 'LAST_REMINDER', 'OVERDUE']
            },
            startDateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            startDateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            teamId: {
                value: null
            },
            includeTeamOwners: {
                value: false
            },
            includeTeamMembers: {
                value: false
            },
            includeNoOwningTeam: {
                setter: Y.usp.ResultsFilterModel.setBooleanValue,
                getter: Y.usp.ResultsFilterModel.getBooleanValue
            },
            owningTeamsForPersonOwners: {
                value: true
            },
            selectedTeamIds: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            },
        }
    });

    Y.namespace('app').UserTeamChecklistInstanceResultsFilterModel = Y.Base.create('userTeamChecklistInstanceResultsFilterModel', Y.app.UserChecklistInstanceResultsFilterModel, [Y.usp.ModelValidate], {}, {
        ATTRS: {
            includeTeamOwners: {
                value: true
            },
            owningTeamsForPersonOwners: {
                value: false
            }
        }
    });

    Y.namespace('app').UserTeamMembersChecklistInstanceResultsFilterModel = Y.Base.create('userTeamMembersChecklistInstanceResultsFilterModel', Y.app.UserChecklistInstanceResultsFilterModel, [Y.usp.ModelValidate], {}, {
        ATTRS: {
            selectedOwnerPersonIds: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            },
            includeTeamMembers: {
                value: true
            },
            owningTeamsForPersonOwners: {
                value: true
            }
        }
    });

    Y.namespace('app').BaseChecklistInstanceResultsFilter = Y.Base.create('baseChecklistInstanceResultsFilter', Y.usp.ResultsFilter, [Y.usp.ResultsFilterModelSyncMixin], {
        events: {
            '#checklistResultsFilter_dateDueShortcuts_week a': { click: '_handleDateDuePreselect' },
            '#checklistResultsFilter_dateDueShortcuts_month a': { click: '_handleDateDuePreselect' },
            '#checklistResultsFilter_clearDate': { click: '_handleClearDate' },
            '#checklistResultsFilter_typeStatusPriority': { click: '_handleClearTypeStatusPriorityFilter' }
        },
        initializer: function () {
            //initialise our date calendars
            this.dueDateFromCalendar = new Y.usp.CalendarPopup();
            this.dueDateToCalendar = new Y.usp.CalendarPopup();

            //set as bubble targets
            this.dueDateFromCalendar.addTarget(this);
            this.dueDateToCalendar.addTarget(this);
        },
        _getChecklistDefinitions: function () {
            var model = new Y.usp.checklist.ChecklistDefinitionList({
                //require minimum of ReadSummary L3 security access
                requestedAccess: 'READ_SUMMARY',
                url: this.get('checklistDefinitionListURL')
            });
            return this.resolvePromise(model);
        },
        resolvePromise: function (model) {
            return new Y.Promise(function (resolve, reject) {
                var data = model.load(function (err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },
        render: function () {
            //get the container 
            var instance = this,
                container = instance.get('container');

            //call into superclass to render
            Y.app.BaseChecklistInstanceResultsFilter.superclass.render.call(this);

            //set the input node for the calendar
            this.dueDateFromCalendar.set('inputNode', container.one('#checklistResultsFilter_dueDateFrom'));
            this.dueDateToCalendar.set('inputNode', container.one('#checklistResultsFilter_dueDateTo'));

            this.dueDateFromCalendar.render();
            this.dueDateToCalendar.render();

            //build a list of checklist definition names - this is as a result of an API call to find only applicable definitions
            this._getChecklistDefinitions().then(function (types) {
                var select = container.one('#checklistResultsFilter_checklistDefinitionId');

                Y.FUtil.setSelectOptions(select, types.toJSON().map(function (t) {
                    return ({ name: t.name + ' (version: ' + t.version + ')', id: t.id });
                }), null, null, false, true);

                //now we have out select box populated - we need to ensure that any default selection is made
                DB.setElementValue(select._node, instance.get('model').get('checklistDefinitionId'));
            });

            //add class name to container to allow us to identify filter
            container.addClass(this.name);

            this.filterTabView = new Y.TabView({
                srcNode: container.one('#checklistResultsTabbedFilters'),
                render: container.one('#checklistResultsFilter')
            });

            return this;
        },
        destructor: function () {
            //destroy the calendars
            this.dueDateFromCalendar.destroy();
            delete this.dueDateFromCalendar;

            this.dueDateToCalendar.destroy();
            delete this.dueDateToCalendar;

            if (this.filterTabView) {
                this.filterTabView.destroy();

                delete this.filterTabView;
            }
        },
        updateView: function () {
            var container = this.get('container'),
                model = this.get('model'),
                isDateFilterSet = !!(model.get('dueDateFrom') || model.get('dueDateTo') || model.get('startDateFrom') || model.get('startDateTo')),
                checklistDefinitionId = model.get('checklistDefinitionId'),
                status = model.get('status'),
                priority = model.get('priority'),
                isDefinitionIdFilterSet = checklistDefinitionId && checklistDefinitionId.length > 0,
                isStatusFilterSet = status && status.length > 0,
                isPriorityFilterSet = priority && priority.length > 0,
                dateFilterIndicator = container.one('.dateFilterSpan'),
                typeFilterIndicator = container.one('.typeStatusPriorityFilterSpan');

            /* -- Set the filter icon on the tab view -- */
            if (dateFilterIndicator) {
                dateFilterIndicator.toggleView(isDateFilterSet);
            }
            if (typeFilterIndicator) {
                typeFilterIndicator.toggleView(isDefinitionIdFilterSet || isStatusFilterSet || isPriorityFilterSet);
            }
            
        },
        _handleClearDate: function (e) {
            var container = this.get('container'),
                model = this.get('model');

            e.preventDefault();

            //clear date model data            
            model.reset('dueDateFrom');
            model.reset('dueDateTo');

            // clear the inputs directly - this picks up invalid values not in model but in the UI
            container.one('#checklistResultsFilter_dueDateFrom').set("value", '');
            container.one('#checklistResultsFilter_dueDateTo').set("value", '');

            //fire apply event
            this.fire('filter:apply');
        },
        _handleClearTypeStatusPriorityFilter: function (e) {
            var model = this.get('model');

            e.preventDefault();

            //clear date model data
            model.reset('checklistDefinitionId');
            model.reset('status');
            model.reset('priority');

            //fire apply event
            this.fire('filter:apply');
        },
        /**
         * Handle change event on due date preselect input
         */
        _handleDateDuePreselect: function (e) {
            
            e.preventDefault();
            var t=e.currentTarget, today=new Date(), now=moment(), dueDateFrom, dueDateTo, period, modifyPeriod;
            switch (t._node.dataset.period) {
                case 'month': 
                    period = 'month';
                    modifyPeriod = 'months';
                    break;
                case 'week':
                default:
                    period = 'isoweek';
                    modifyPeriod = 'weeks';
                    break;
            }  
            switch (t._node.dataset.occasion) {
                case 'last':
                    now.subtract(1, modifyPeriod);
                    break;
                case 'next':
                    now.add(1, modifyPeriod);
                    break;
                case 'this':
                default:
                    break;
            }
            dueDateFrom = now.startOf(period).toDate();
            dueDateTo = now.endOf(period).toDate();
            this.get("model").setAttrs({
                dueDateTo: dueDateTo.getTime(),
                dueDateFrom: dueDateFrom.getTime()
            },{
                src:'filter'
            });
        },
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            model: {},
            filterConfig: {}
        }
    });

    Y.namespace('app').GroupChecklistInstanceResultsFilter = Y.Base.create('groupChecklistInstanceResultsFilter', Y.app.BaseChecklistInstanceResultsFilter, [], {
        template: Y.Handlebars.templates['groupChecklistResultsFilter'],
        events: {
            '#checklistResultsFilter_subject': { click: '_handleClearSubjectFilter' },
        },
        initializer: function () {
            this.events = Y.merge(this.events, Y.app.GroupChecklistInstanceResultsFilter.superclass.events);
        },
        _getTeamMemberIds: function () {
            //annoying that we have to use a paginated list here but there is no support for a standard list in the REST api on person
            var model = new Y.usp.person.PaginatedPersonGroupMembershipDetailsList({
                url: this.get('groupMemberListURL')
            });
            return this.resolvePromise(model);
        },
        render: function () {
            var instance = this,
                container = this.get('container');

            //call into superclass to render
            Y.app.GroupChecklistInstanceResultsFilter.superclass.render.call(this);

            //Load the member ids if the subject is of type group

            this._getTeamMemberIds().then(function (membershipDetails) {
                var select = container.one('#checklistResultsFilter_memberId'),
                    members = membershipDetails.toJSON().map(function (member) {
                        //convert to simple id,value object
                        return ({
                            id: member['personVO!id'],
                            name: [member['personVO!name'], '(' + member['personVO!personIdentifier'] + ')'].join(' ')
                        });
                    });

                Y.FUtil.setSelectOptions(select, members, null, null, false, true);

                //now we have out select box populated - we need to ensure that any default selection is made
                DB.setElementValue(select._node, instance.get('model').get('memberId'));
            });

            return this;
        },
        updateView: function () {
            var container = this.get('container'),
                model = this.get('model'),
                memberId = model.get('memberId'),
                subjectFilterIndicator = container.one('.subjectFilterSpan'),
                isSubjectFilterSet = memberId && memberId.length > 0;

            //call into superclass to update view
            Y.app.GroupChecklistInstanceResultsFilter.superclass.updateView.call(this);
            /* -- Set the filter icon on the tab view -- */
            if (subjectFilterIndicator) {
                subjectFilterIndicator.toggleView(isSubjectFilterSet);
            }
        },
        _handleClearSubjectFilter: function (e) {
            var model = this.get('model');

            e.preventDefault();

            //clear date model data
            model.reset('memberId');

            //fire apply event
            this.fire('filter:apply');
        }
    }, {
        ATTRS: {
            groupMemberListURL: {
                getter: function (val) {
                    var subject = this.get('subject') || {};
                    return L.sub(val, { subjectId: subject.subjectId, sortBy: encodeURI('[{"pgm.person.surname":"asc"},{"pgm.person.forename":"asc"}]') });
                }
            },
            checklistDefinitionListURL: {
                getter: function (val) {
                    var subject = this.get('subject') || {};
                    return L.sub(val, { subjectType: subject.subjectType.toUpperCase(), subjectId: subject.subjectId, sortBy: encodeURI('[{"name":"asc"}]') });
                }
            },
            subject: {}
        }
    });

    Y.namespace('app').PersonChecklistInstanceResultsFilter = Y.Base.create('personChecklistInstanceResultsFilter', Y.app.BaseChecklistInstanceResultsFilter, [], {
        template: Y.Handlebars.templates['personChecklistResultsFilter']
    }, {
        ATTRS: {
            checklistDefinitionListURL: {
                getter: function (val) {
                    var subject = this.get('subject') || {};
                    return L.sub(val, { subjectType: subject.subjectType.toUpperCase(), subjectId: subject.subjectId, sortBy: encodeURI('[{"name":"asc"}]') });
                }
            },
            subject: {}
        }
    });

    Y.namespace('app').UserChecklistInstanceResultsFilter = Y.Base.create('userChecklistInstanceResultsFilter', Y.app.BaseChecklistInstanceResultsFilter, [], {
        template: Y.Handlebars.templates['userChecklistResultsFilter'],
        events: {
            '#checklistResultsFilter_dateDueShortcuts': {
                change: '_handleDateDuePreselect'
            },
            '#checklistResultsFilter_dateStartShortcuts': {
                change: '_handleDateStartPreselect'
            }
        },
        initializer: function () {
            this.events = Y.merge(this.events, Y.app.UserChecklistInstanceResultsFilter.superclass.events);

            //initialise our date calendars
            this.startDateFromCalendar = new Y.usp.CalendarPopup({maximumDate: new Date()});
            this.startDateToCalendar = new Y.usp.CalendarPopup({maximumDate: new Date()});

            //set as bubble targets
            this.startDateFromCalendar.addTarget(this);
            this.startDateToCalendar.addTarget(this);
        },
        destructor: function () {
            //destroy the calendars
            this.startDateFromCalendar.destroy();
            delete this.startDateFromCalendar;

            this.startDateToCalendar.destroy();
            delete this.startDateToCalendar;
        },
        _handleClearDate: function (e) {
            var container = this.get('container'),
                model = this.get('model');

            //clear date model data
            model.reset('dueDateFrom');
            model.reset('dueDateTo');          
            model.reset('startDateFrom');
            model.reset('startDateTo');
            model.reset('checklistResultsFilter_dateDueShortcuts');
            model.reset('checklistResultsFilter_dateStartShortcuts');

            // clear the inputs directly - this picks up invalid values not in model but in the UI
            container.one('#checklistResultsFilter_startDateFrom').set("value", '');
            container.one('#checklistResultsFilter_startDateTo').set("value", '');
            container.one('#checklistResultsFilter_dueDateFrom').set("value", '');
            container.one('#checklistResultsFilter_dueDateTo').set("value", '');
            container.one('#checklistResultsFilter_dateDueShortcuts').set("value", '');
            container.one('#checklistResultsFilter_dateStartShortcuts').set("value", '');

            //hide the conjunctions between the filters on reset
            container.one('#dueDateFromAnd').setStyle('display', 'none');
            container.one('#dueDateToAnd').setStyle('display', 'none');
            container.one('#startDateFromAnd').setStyle('display', 'none');

            //fire apply event
            this.fire('filter:apply');
        },
        /**
         * Handle change event on due date preselect input
         */
        _handleDateDuePreselect: function (e) {
            var periods = this._getPeriod(e),
                period = periods.period,
                now = periods.now,
                dueDateFrom = now.startOf(period).toDate(),
                dueDateTo = now.endOf(period).toDate();

            this.get("model").setAttrs({
                dueDateTo: dueDateTo.getTime(),
                dueDateFrom: dueDateFrom.getTime()
            }, {
                src: 'filter'
            });
        },
        _handleDateStartPreselect: function (e) {
            var currentDate = moment() ;
            var periods = this._getPeriod(e),
                period = periods.period,
                now = periods.now,
                startDateFrom = now.startOf(period).toDate(),
                startDateTo;

            if(now.endOf(period).isAfter(currentDate))
            {
                startDateTo = currentDate.toDate();
            } else {
                startDateTo = now.endOf(period).toDate();
            }
            
            this.get("model").setAttrs({
                startDateTo: startDateTo.getTime(),
                startDateFrom: startDateFrom.getTime()
            }, {
                src: 'filter'
            });
        },
        _getPeriod: function (e) {
            e.preventDefault();
            var value = e.currentTarget.get('value').split(','),
                now = moment(),
                period, modifyPeriod;
                
            switch (value[1]) {
                case 'day':
                    period = 'day';
                    modifyPeriod = 'days';
                    break;
                case 'month':
                    period = 'month';
                    modifyPeriod = 'months';
                    break;
                case 'week':
                    period = 'isoweek';
                    modifyPeriod = 'weeks';
                    break;
                default:
                    break;
            }    
            switch (value[0]) {
                case 'last':
                    now.subtract(1, modifyPeriod);
                    break;
                case 'next':
                    now.add(1, modifyPeriod);
                     break;
                case 'this':                   
                default:
                    break;
            }     
            return ({
                period: period,
                now: now
            });
        },
        _getSubjectIds: function () {
            var model = new Y.usp.person.PersonSummaryList({
                url: this.get('subjectListURL')
            });
            return this.resolvePromise(model);
        },
        _getOwningTeams: function () {
            var model = new Y.usp.ModelList({
                url: this.get('owningTeamsListURL')
            });
            return this.resolvePromise(model);
        },
        render: function () {
            var instance = this,
                container = this.get('container');

            //call into superclass to render
            Y.app.UserChecklistInstanceResultsFilter.superclass.render.call(this);

            //set the input node for the calendar
            this.startDateFromCalendar.set('inputNode', container.one('#checklistResultsFilter_startDateFrom'));
            this.startDateToCalendar.set('inputNode', container.one('#checklistResultsFilter_startDateTo'));

            this.startDateFromCalendar.render();
            this.startDateToCalendar.render();

            //Load the subject ids for this user

            this._getSubjectIds().then(function (subjectList) {
                var select = container.one('#checklistResultsFilter_subjectId'),
                    subjects = subjectList.toJSON().map(function (subject) {
                        //convert to simple id,value object
                        return ({
                            id: subject['id'],
                            name: [subject['name'], '(' + subject['personIdentifier'] + ')'].join(' ')
                        });
                    });

                Y.FUtil.setSelectOptions(select, subjects, null, null, false, true);

                //now we have out select box populated - we need to ensure that any default selection is made
                DB.setElementValue(select._node, instance.get('model').get('subjectId'));
            });

            //load owning teams ids
            this._getOwningTeams().then(function (teamsList) {
                var select = container.one('#checklistResultsFilter_owningTeamsIds'),
                    teams = teamsList.toJSON().map(function (team) {
                        //convert to simple id,value object
                        return ({
                            id: team['id'],
                            name: [team['name'], '(' + team['organisationIdentifier'] + ')'].join(' ')
                        });
                    });

                Y.FUtil.setSelectOptions(select, teams, null, null, false, true);

                //now we have out select box populated - we need to ensure that any default selection is made
                DB.setElementValue(select._node, instance.get('model').get('selectedTeamIds'));
            });

            return this;
        },
        updateView: function () {
            var container = this.get('container'),
                subjectFilterIndicator = container.one('.subjectFilterSpan'),
                ownerFilterIndicator = container.one('.ownerFilterSpan'),
                model = this.get('model'),
                subjectId = model.get('subjectId'),
                teamsIds = model.get('selectedTeamIds'),
                memberIds = model.get('selectedOwnerPersonIds'),
                isNoOwningTeamFilterSet = model.get('includeNoOwningTeam'),
                isSubjectFilterSet = subjectId && subjectId.length > 0,
                isTeamFilterSet = teamsIds && teamsIds.length > 0,
                isMemberFilterSet = memberIds && memberIds.length > 0;

            //call into superclass to update view
            Y.app.UserChecklistInstanceResultsFilter.superclass.updateView.call(this);
            /* -- Set the filter icon on the tab view -- */
            if (subjectFilterIndicator) {
                subjectFilterIndicator.toggleView(isSubjectFilterSet);
            }
            if (ownerFilterIndicator) {
                ownerFilterIndicator.toggleView(isTeamFilterSet || isNoOwningTeamFilterSet || !!isMemberFilterSet);
            }

            //reset the quick due date and start date filters
            if (container.hasChildNodes())  {
                if ((!model.get('dueDateFrom')) && (!model.get('dueDateTo')) && (!model.get('startDateFrom')) && (!model.get('startDateTo'))) {
                    //reset the quick due date and start date filters
                    container.one('#checklistResultsFilter_dateDueShortcuts').set("value", '');
                    container.one('#checklistResultsFilter_dateStartShortcuts').set("value", '');

                    //hide the conjunctions between the filters on reset
                    container.one('#dueDateFromAnd').setStyle('display', 'none');
                    container.one('#dueDateToAnd').setStyle('display', 'none');
                    container.one('#startDateFromAnd').setStyle('display', 'none');
                } else if ((!model.get('dueDateFrom')) && (!model.get('dueDateTo'))) {
                    //reset the quick due date filters
                    container.one('#checklistResultsFilter_dateDueShortcuts').set("value", '');

                    //hide the conjunctions between the filters on reset
                    container.one('#dueDateFromAnd').setStyle('display', 'none');
                    container.one('#dueDateToAnd').setStyle('display', 'none');
                } else if ((!model.get('startDateFrom')) && (!model.get('startDateTo'))) {
                    //reset the quick start date filters
                    container.one('#checklistResultsFilter_dateStartShortcuts').set("value", '');

                    //hide the conjunctions between the filters on reset
                    container.one('#dueDateToAnd').setStyle('display', 'none');
                    container.one('#startDateFromAnd').setStyle('display', 'none');
                }

                //display the conjunctions between the filters based on the selections
                if(model.get('dueDateFrom') && model.get('dueDateTo')) {
                    container.one('#dueDateFromAnd').setStyle('display', 'block');
                }
                if(model.get('dueDateFrom') && model.get('dueDateTo') && model.get('startDateFrom') && model.get('startDateTo')) {
                    container.one('#dueDateToAnd').setStyle('display', 'block');
                }
                if(model.get('startDateFrom') && model.get('startDateTo')) {
                    container.one('#startDateFromAnd').setStyle('display', 'block');
                }
                if(model.get('dueDateTo') && (model.get('startDateFrom') ||  model.get('startDateTo'))){
                    container.one('#dueDateToAnd').setStyle('display', 'block');
                }
                if(model.get('dueDateFrom') && (model.get('startDateFrom') ||  model.get('startDateTo'))){
                    container.one('#dueDateToAnd').setStyle('display', 'block');
                }
            }  
        }
    }, {
        ATTRS: {
            subjectListURL: {
                getter: function (val) {
                    var ownerId = this.get('ownerId'),
                        model = this.get('model');

                    return L.sub(val, {
                        ownerId: ownerId,
                        includeTeamOwners: model.get('includeTeamOwners'),
                        includeTeamMembers: model.get('includeTeamMembers'),
                        sortBy: encodeURI('[{"name":"asc"}]')
                    });
                }
            },
            ownerId: {
                value: ''
            },
            checklistDefinitionListURL: {
                getter: function (val) {
                    var ownerId = this.get('ownerId'),
                        model = this.get('model');

                    return L.sub(val, {
                        ownerId: ownerId,
                        includeTeamOwners: model.get('includeTeamOwners'),
                        includeTeamMembers: model.get('includeTeamMembers'),
                        sortBy: encodeURI('[{"name":"asc"}]')
                    });
                }
            },
            owningTeamsListURL: {
                getter: function (val) {
                    var ownerId = this.get('ownerId'),
                        model = this.get('model');

                    return L.sub(val, {
                        ownerId: ownerId,
                        owningTeamsForPersonOwners: model.get('owningTeamsForPersonOwners'),
                        includeTeamMembers: model.get('includeTeamMembers'),
                        sortBy: encodeURI('[{"name":"asc"}]')
                    });
                }
            }
        }
    });

    Y.namespace('app').UserTeamChecklistInstanceResultsFilter = Y.Base.create('userTeamChecklistInstanceResultsFilter', Y.app.UserChecklistInstanceResultsFilter, [], {});

    Y.namespace('app').UserTeamMembersChecklistInstanceResultsFilter = Y.Base.create('userTeamMembersChecklistInstanceResultsFilter', Y.app.UserChecklistInstanceResultsFilter, [], {
        _getTeamMemberIds: function () {
            var model = new Y.usp.person.PersonSummaryList({
                url: this.get('teamMembersListURL')
            });
            return this.resolvePromise(model);
        },
        render: function () {
            var instance = this,
                container = this.get('container');

            //invoke superclass render
            Y.app.UserTeamMembersChecklistInstanceResultsFilter.superclass.render.call(this);

            //Load the userIds

            this._getTeamMemberIds().then(function (personList) {
                var select = container.one('#checklistResultsFilter_teamMemberIds'),
                    people = personList.toJSON().map(function (person) {
                        //convert to simple id,value object
                        return ({
                            id: person['id'],
                            name: [person['name'], '(' + person['personIdentifier'] + ')'].join(' ')
                        });
                    });

                Y.FUtil.setSelectOptions(select, people, null, null, false, true);

                //now we have our select box populated - we need to ensure that any default selection is made
                DB.setElementValue(select._node, instance.get('model').get('selectedOwnerPersonIds'));
            });
            return this;
        },
    }, {
        ATTRS: {
            teamMembersListURL: {
                getter: function (val) {
                    var ownerId = this.get('ownerId');
                    return L.sub(val, {
                        ownerId: ownerId,
                        sortBy: encodeURI('[{"name":"asc"}]')
                    });
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'form-util',
        'results-filter',
        'event-custom-base',
        'tabview',
        'handlebars-checklist-templates',
        'datatype-date-math',
        'calendar-popup',
        'usp-date',
        'promise',
        'usp-checklist-ChecklistDefinition',
        'usp-person-PersonGroupMembershipDetails',
        'usp-person-PersonSummary',
        'usp-organisation-Organisation',
        'data-binding',
        'usp-validate'
    ]
});