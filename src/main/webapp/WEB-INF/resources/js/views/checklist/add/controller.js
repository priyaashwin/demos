YUI.add('checklist-instance-add-controller', function(Y) {

  var L = Y.Lang,
    A = Y.Array,
    ChecklistOwnershipUtil = Y.app.checklist.ChecklistOwnershipUtil;

  Y.namespace('app.checklist').ChecklistInstanceAddControllerView = Y.Base.create('checklistInstanceAddControllerView', Y.View, [], {

    initializer: function(config) {
      var searchConfig = config.searchConfig || {},
        subject = config.subject || {},
        permissions = config.permissions || {},
        dialogConfig = Y.mix({
          subject: subject
        }, config.dialogConfig || {}),
        //clone errors config - as it is manipulated by the plugin
        errorsConfig=Y.mix({},config.errorsConfig||{});

      var resultsConfig = {
        searchConfig: Y.merge(searchConfig, {
          //mix the subject into the search config so it is available when getting the URL
          subject: subject
        }),
        filterConfig: Y.merge(config.filterConfig, {
          permissions: permissions,
          subject: subject
        }),
        filterContextConfig: config.filterContextConfig,
        permissions: permissions
      };

      this.results = new Y.app.checklist.AddChecklistFilteredResults(resultsConfig).addTarget(this);

      //create checklist instance dialog view
      this.dialog = new Y.app.ChecklistInstanceDialog(dialogConfig).render().addTarget(this);

      //plug in the errors handling - and configure the appropriate messages
      //using supplied errors config object
      this.plug(Y.Plugin.usp.ModelErrorsPlugin, errorsConfig);

      //Setup an handler for promise failures
      this.handlePromiseError = Y.bind(function(error) {
        //On an error - close the dialog and allow the error to be picked up by the main page view
        this.dialog.hide();
        this.fire('error', {
          error: error
        });
      }, this);


      //event handlers
      this._checklistEvtHandlers = [
        this.on('*:add', this.handleAdd, this),
        this.dialog.on('*:error', this._handleDialogError, this),
        //handler for the create checklist event - fired when we have everything in place to perform the create
        this.on('*:createChecklist', this._handleCreateChecklistInstance, this),
        //subscribe to form view existing event
        this.dialog.on('showExistingChecklistInstanceView:view', function(e) {
          //launch the checklist
          this._handleLaunchChecklist(e.id);
        }, this),
      ];
    },
    destructor: function() {
      this._checklistEvtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._checklistEvtHandlers;

      //remove ModelErrors plugin
      this.unplug(Y.Plugin.usp.ModelErrorsPlugin);

      this.dialog.removeTarget(this);
      this.dialog.destroy();
      delete this.dialog;

      this.results.removeTarget(this);
      this.results.destroy();
      delete this.results;

    },
    render: function() {
      var contentNode = Y.one(Y.config.doc.createDocumentFragment());

      //append the results
      contentNode.append(this.results.render().get('container'));

      //set fragment into container
      this.get('container').setHTML(contentNode);

      return this;
    },
    _handleDialogError: function(e) {
      //check target for error
      var target = e.target;
      //if the target is from our Dialog based Model then don't let it propagate
      if (target.name === 'newChecklistInstanceModel') {
        e.stopPropagation();
      }
      //otherwise - let the error flow up to the error handler on this view
    },
    _handleCreateChecklistInstance: function(e) {
      var dialogConfig = this.get('dialogConfig'),
        subject = e.subject,
        data = e.checklistData,
        checklistDefinition = e.checklistDefinition,
        checklistInstance;
      //Ensure we are showing the processing view   
      this.dialog.showView('checklistProcessing');
      checklistInstance = new Y.usp.checklist.NewChecklistInstance({
        checklistDefinitionId: checklistDefinition.get('id'),
        url: L.sub(dialogConfig.addChecklistInstanceURL, {
          id: subject.subjectId
        })
      });
      if (data) {
        //if additional data is present - set it against our model
        checklistInstance.setAttrs(data.toJSON ? data.toJSON() : data);
      }
      //finally we get to make the thing
      this.createChecklistInstance(checklistInstance, subject, checklistDefinition);
    },
    createChecklistInstance: function(checklistInstanceModel, subject, checklistDefinition) {
      var dialogConfig = this.get('dialogConfig');
      // Do the save
      checklistInstanceModel.save({}, Y.bind(function(err, response) {
        if (err === null) {
          if (response.getResponseHeader('warning')) {
            this._handleSuccessWithWarnings(JSON.parse(response.responseText).warnings, JSON.parse(response.responseText).secondaryResourceURIs, checklistDefinition);
          } else {
            //fire success message - delay of not a group checklist - as the page will -reload
            Y.fire('infoMessage:message', {
              message: subject.subjectType === 'group' ? dialogConfig.groupSuccessMessage : dialogConfig.successMessage,
              delayed: subject.subjectType !== 'group'
            });
            if (subject.subjectType === 'group') {
              this.fire('viewChecklistResults');
            } else {
              //don't take the user to a not started checklist - if endDate specified. If the user requested
              //an enddate > than the suggested via the definition then this checklist will be not-started.
              if (checklistDefinition) {
                var defnEndDate = checklistDefinition.get('calculatedDueDate');
                var checklistEndDate = checklistInstanceModel.get('endDate');
                if ((checklistEndDate && defnEndDate) && (checklistEndDate > defnEndDate)) {
                  this.fire('viewChecklistResults');
                  return;
                }
              }
              //launch the checklist
              this._handleLaunchChecklist(checklistInstanceModel.get('id'));
            }
          }
        } else {
          this.fire('error', {
            error: err
          });
          //close the dialog
          this.dialog.hide();
        }
      }, this));
    },
    _handleLaunchChecklist: function(id) {
      //Loading....   
      this.dialog.showView('checklistProcessing');
      //off we go...
      window.location = L.sub(this.get('viewURL'), {
        id: id
      });
    },
    _handleSuccessWithWarnings: function(warnings, checklistResourceURIs, checklistDefinition) {
      var checklistName = checklistDefinition.get('name'),
        dialogConfig = this.get('dialogConfig'),
        subject=this.get('subject');
      this.dialog.showView('showExistingGroupChecklistInstances', {
        model: new Y.app.ExistingGroupChecklistInstancesModel({
          warnings: warnings.map(function(warning) {
            return warning.split(':')[1].substring(1);
          }),
          checklistResourceURIs: checklistResourceURIs
        }),
        otherData: {
          subject: subject,
          checklistName: checklistName
        },
        checklistDefinition: checklistDefinition,
        subject: subject,
        successMessage: dialogConfig.groupSuccessMessage
      }, function() {
        //Get the button by name out of the footer and enable it.
        this.getButton('cancelButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showAddDialog: function(subject, checklistDefinition, ownershipArray, cfg) {
      var dialogConfig = this.get('dialogConfig') || {},
          subject=this.get('subject');
      this.dialog.showView('addChecklistInstance', {
        checklistDefinition: checklistDefinition,
        model: new Y.app.NewChecklistInstanceModel(Y.merge(cfg, {
          startDate: cfg.requireStartDate ? new Date() : null,
          endDate: cfg.requireEndDate ? checklistDefinition.get('calculatedDueDate') : null,
          checklistDefinitionId: checklistDefinition.get('id')
        })),
        otherData: {
          subject: subject
        },
        owners: ownershipArray,
        subject: subject
      }, function(view) {
        var model = view.get('model'),
          addConfig = dialogConfig.views.addChecklistInstance,
          //update narrative to reflect appropriate add - take a copy of the narrative object
          //we don't want to update the original config object
          narrative = Y.merge({}, addConfig.narrative);

        if (model.requireOwnership) {
          if (model.requireStartDate) {
            narrative.summary = narrative.requireOwnership.requireStartDate.summary;
          } else if (model.requireEndDate) {
            narrative.summary = narrative.requireOwnership.requireEndDate.summary;
          } else {
            narrative.summary = narrative.requireOwnership.summary;
          }
        } else {
          if (model.requireStartDate) {
            narrative.summary = narrative.requireStartDate.summary;
          } else if (model.requireEndDate) {
            narrative.summary = narrative.requireEndDate.summary;
          }
        }

        if (narrative) {
          this.updateNarrative(narrative);
        }

        //enable the button
        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    _getExistingChecklistInstancesPromise: function(subject, checklistDefinitionId) {
      var url = this.get('dialogConfig').existingChecklistInstanceURL;
      return new Y.Promise(function(resolve, reject) {
        var model = new Y.usp.checklist.PaginatedChecklistInstanceList({
          url: L.sub(url, {
            subjectId: subject.subjectId,
            checklistDefinitionId: checklistDefinitionId
          })
        }).load(function(err) {
          if (err !== null) {
            //failed to get the existing form instances for some reason so reject the promise
            reject(err);
          } else {
            //success, so resolve the promise
            resolve(model);
          }
        });
      });
    },
    _examineChecklistDefinitionConfiguration: function(subject, checklistDefinition) {
      var dialogConfig = this.get('dialogConfig');

      checklistAddDialogCheck = Y.bind(function(ownership) {
        var calculationMode = checklistDefinition.get('calculationMode'),
            promptForStart = checklistDefinition.get('canBackdate') === true,
            promptForEnd = calculationMode === 'TO_END',
            potentialOwner = null,
            ownershipArray = ownership ? ownership.toJSON() : [];

        var owners = ownershipArray.map(function(entry) {
          var owner = entry.ownerDetails,
              team = entry.owningTeamDetails;

          return ({
            ownerSubjectType: owner.type,
            ownerId: owner.id,
            owningTeamId: team.id
          });
        });

        if ( owners.length === 1 ) {
          potentialOwner = owners[0];
        }

        //determine if we can proceed straight to the add
        if (promptForStart || promptForEnd || (!potentialOwner && checklistDefinition.get('hasOwnershipConfigurations'))) {
          //show the dialog to the user
          this.showAddDialog(subject, checklistDefinition, ownershipArray, Y.merge({
            requireStartDate: promptForStart,
            requireEndDate: promptForEnd,
            requireOwnership: !potentialOwner,
            isPotentialOwnerNotEmpty: owners.length !== 0
          }, potentialOwner || {}));
        } else {
          //construct a new checklist instance model to be used during the add
          this.fire('createChecklist', {
            subject: subject,
            checklistDefinition: checklistDefinition,
            checklistData: potentialOwner
          });
        }
      }, this);

      if(checklistDefinition.get('hasOwnershipConfigurations')) {
        Y.Promise.resolve(ChecklistOwnershipUtil.getPotentialOwnersPromise(checklistDefinition.get('id'), dialogConfig.potentialOwnersURL).then(checklistAddDialogCheck).then(undefined, this.handlePromiseError));
      } else {
        //when worklist has no ownership config then call function with null data.
        checklistAddDialogCheck(null);
      }
    },
    handleAdd: function(e) {
      var record = e.record,
        subject = this.get('subject'),
        permissions = this.get('permissions');

      //clear any existing errors shown - we call into the errors plugin
      this.modelErrors.clearErrors();

      if (permissions.canAdd) {
        this._handleAddChecklist(subject, record);
      }
    },
    _handleAddChecklist: function(subject, checklistDefinition) {
      var checklistName = checklistDefinition.get('name'),
        checklistDefinitionId = checklistDefinition.get('id'),
        allowsMultipleInstances = checklistDefinition.get('cardinality') === 'UNCONSTRAINED';
      //Put up a processing dialog to stop the user clicking any additional buttons
      this.dialog.showView('checklistProcessing');
      if (subject.subjectType === 'group') {
        //Proceed with add instances for each group member
        this._examineChecklistDefinitionConfiguration(subject, checklistDefinition);
      } else {
        var resolveChecklistInstances = Y.bind(function(checklistInstances) {
          //if we have instances here and only one instance is allowed - we prompt the user to view that instance
          if (checklistInstances.size() > 0 && !allowsMultipleInstances) {
            this.dialog.showView('showExistingInstance', {
              //Pass the first checklist instance - there should really only be 1
              model: checklistInstances.item(0),
              otherData: {
                subject: subject,
                checklistName: checklistName
              }
            }, function() {
              //Get the button by name out of the footer and enable it.
              this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
          } else {
            //Otherwise the add can proceed as normal
            this._examineChecklistDefinitionConfiguration(subject, checklistDefinition);
          }
        }, this);
        //When adding for an individual we check first for an existing checklist instance
        Y.Promise.resolve(this._getExistingChecklistInstancesPromise(subject, checklistDefinitionId)).then(resolveChecklistInstances).then(undefined, this.handlePromiseError);
      }
    },
  }, {
    ATTRS: {
      permissions: {
        value: {}
      },
      viewURL: {
        value: '#none'
      },
      dialogConfig: {

      },
      subject: {

      },
      currentPersonId: {
        value: null
      },
      currentPersonTeamId: {
        value: null
      }
    }
  });

}, '0.0.1', {
  requires: ['yui-base',
    'event-custom-base',
    'view',
    'promise',
    'checklist-add-results',
    'checklist-ownership-util',
    'model-errors-plugin',
    'checklist-instance-dialog-views',
    'usp-checklist-NewChecklistInstance'
  ]
});