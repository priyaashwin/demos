YUI.add('checklist-add-filter', function(Y) {
    'use-strict';

    Y.namespace('app.checklist').AddChecklistInstanceFilterForm = Y.Base.create('addChecklistInstanceFilterForm', Y.usp.ResultsFilterModel, [], {
    }, {
        ATTRS: {
            name: {
                USPType: 'String',
                value:''
            }
        }
    });


    Y.namespace('app.checklist').AddChecklistInstanceFilterView = Y.Base.create('addChecklistInstanceFilterView', Y.usp.app.Filter, [], {
        filterModelType: Y.app.checklist.AddChecklistInstanceFilterForm,
        filterTemplate: Y.Handlebars.templates["addChecklistFilter"],
        filterContextTemplate: Y.Handlebars.templates["addChecklistActiveFilter"]
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-filter',
        'results-filter',
        'handlebars-helpers',
        'handlebars-checklist-templates'
    ]
});