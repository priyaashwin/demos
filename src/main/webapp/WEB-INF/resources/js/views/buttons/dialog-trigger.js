

YUI.add('app-dialog-trigger', function (Y) {
		
	
	Y.namespace('app.views').DialogTrigger = Y.Base.create('dialogTrigger', Y.app.views.ButtonTrigger, [], {

		events: {							
			'[data-trigger="dialog"]': {	
				click: '_triggerDialog'				
			}			
		},
		
		/**
		 * @method initializer
		 */
		initializer: function(config) {			
			
			var model = this.get('model');
			
			model.set('trigger', 'dialog');

		},			
		
		
		/**
		 * @method _triggerDialog
		 * @description
		 * 	
		 * 	Note: TIL event is a global variable in IE, hence using variable 'evt'. 
		 * 	See http://stackoverflow.com/questions/1510094/is-event-a-reserved-word-in-javascript
		 */
			
		_triggerDialog: function(e) {				
			var action, evt;
			
			e.preventDefault();
			
			action = e.currentTarget.getAttribute('data-action');	
			evt = e.currentTarget.getAttribute('data-event');
			
			Y.fire(evt, {
				action: action
			});
		}

		
	});
	
}, '0.0.1', {
	    
	requires: [
       'yui-base',
       'event-custom',
       'node',
       'view',
       'app-button-trigger']
});