

YUI.add('app-button-trigger', function (Y) {
		
	'use-strict';
	
	var templateEngine = new Y.Template(Y.Template.Micro);
	 
	var CSS_BTN_ACTIVE = 'pure-button-active';
	var CSS_BTN_DISABLED = 'pure-button-disabled';	
	var HTML_BUTTON_TMPL = '<a href="<%=this.url%>" target="<%=this.target%>" data-action="<%=this.action%>" data-event="<%=this.event%>" data-trigger class="pure-button <%=this.cssActive%> <%=this.cssButton%>" title="<%=this.title%>" aria-label="<%=this.title%>">\
					 <i class="<%=this.cssIcon%>"></i><span><%=this.label%></span></a>';
	
	//compile the template
	var HTML_BUTTON = templateEngine.compile(HTML_BUTTON_TMPL);
				
	
	Y.namespace('app.views').ButtonTrigger = Y.Base.create('buttonTrigger', Y.View, [Y.View.NodeMap], {
		
		/**
		 * @property events
		 */
		events: {
			'a[data-trigger]': {
				click: '_onClick',
			}
		},		
		
		
		/**
		 * @property template
		 */
		template: HTML_BUTTON,
		
		
		/**
		 * @method initializer
		 * @description TODO move to the model
		 */		
		initializer: function(config) {
			
			if(this.get('isActive')) {
				this.get('model').set('cssActive', this.get('activeClass'));
			}
						
			this.evtHandlers = [];
			this.evtHandlers.push(this.after('isActiveChange', this._afterActiveChange, this));
			this.evtHandlers.push(this.after('isEnabledChange', this._afterEnabledChange, this));
		},
		
		
		/**
		 * @method render
		 * @description render template
		 */		
		render: function(){
							
			var	container, html, model, jsonObject; 
			
			container = this.get('container');									
			model = this.get('model');
			
			jsonObject=model.toJSON();
					
			jsonObject.url=jsonObject.url||'#none';
			jsonObject.target=jsonObject.target||'';
			jsonObject.action=jsonObject.action||'';
			jsonObject.event=jsonObject.event||'';
			jsonObject.cssActive=jsonObject.cssActive||'';
			jsonObject.cssButton=jsonObject.cssButton||'';
			jsonObject.title=jsonObject.title||'';
			jsonObject.cssIcon=jsonObject.cssIcon||'';
			jsonObject.label=jsonObject.label||'';		
			
			container.setHTML(this.template(jsonObject));

			return this;
			
		},
		
		
		
		/**
		 * @method setAction
		 * @description Sets the data-action on the node directly, bypassing the model.
		 */		
		setAction: function(action, once) {			
			this.get('container').one('a[data-trigger]').setAttribute('data-action', action);
		},
		

		/**
		 * @method disable
		 * @description will trigger a call to _afterEnabledChange.
		 */		
		disable: function() {
			this.set('isEnabled', false);
		},
				
		
		/**
		 * @method enable
		 * @description will trigger an a call to _afterEnabledChange.
		 */		
		enable: function() {
			this.set('isEnabled', true);
		},
		
		
		/**
		 * @method payload
		 * @description sugar method to attach data to the 'e' event parameter.
		 * 	Passing a function as first paramter will attach the functions return value.
		 * 	If a context is provided this will be the 'this object' when function is called.
		 * 
		 * @param valObjOrFn {object || function}
		 */
		payload: function(valObjOrFn, context) {						
			
			this._payloadContext = '';
			
			if(Y.Lang.isObject(context) || Y.Lang.isFunction(context)) {
				this._payloadContext = context;
			}
								
			this._payload = valObjOrFn;
			
			
		},
	
		
		
		/**
		 * @method _onClick
		 * @description 
		 * 
		 * 	Won't prevent the default action if link has non-fragmented url (i.e no '#') and a target attribute (e.g '_blank')
		 * 
		 * 	If an event and action has been provided will fire the event, passing the action. 
		 * 
		 * 	If allowToggle is set the isActive attribute will toggle value. This triggers a call to 
		 * 	_afterActiveChange to toggle the cssActive class.
		 * 
		 */	
		_onClick: function(e) {
			
			var action, evt, node, id, payload; 
			
			node = e.currentTarget;			
			action = node.getAttribute('data-action');
			evt = node.getAttribute('data-event');
			id = this.get('model').get('id');
			payload = this.get('payload');

	    	//Leave if disabled
			if(!this.get('isEnabled')) {
				e.preventDefault();
	    		return;	
	    	}	
			
			// Allow for normal link behaviour
			if(!node.getAttribute('target') && e.currentTarget.getAttribute('href').indexOf('#')!==-1) {
	    		e.preventDefault();
	    	}
			
			this.set('isActive', !this.get('isActive'));

	    	// payload is both merged into the e object and placed below in e.data
			// - former to allow existing events to continue to work
			// - latter to allow nicer packaging/dumber listeners/smaller listener-to-listener api	
			if(evt) {
	    		Y.fire(evt, Y.merge({
		    		action: action,
		    		data: payload, 
		    		id: id
		    	}, payload));
	    	}
		},
		
		
		
		/**
		 * @method _onActiveChange
		 * @description
		 * @private
		 */				
		_afterActiveChange: function(e) {	
			
			if(e.silent || !this.get('allowToggle')) { 
				return; 	
			}

			if(this.get('isActive')) {
				this.get('model').set('cssActive', this.get('activeClass'));				
			} else {
				this.get('model').set('cssActive', '');
			}	
			
			this.render();
		},
		
		
		
		_afterEnabledChange: function(e) {

			this.setAttrs({
				isActive: false
			}, { silent: true });	

			
			if(this.get('isEnabled')) {				 
				this.get('container').all("a")
				 	.removeAttribute("disabled")
				 	.removeAttribute("aria-hidden")
				 	.removeAttribute("tabindex")				 
				 	.setAttribute("aria-disabled", "false")
				 	.removeClass(this.get('disabledClass'))
				 	.removeClass(this.get('activeClass'));			 
			} else {			
				
				this.get('container').all("a")
					.setAttribute ("disabled","disabled")
					.setAttribute ("aria-disabled","true")
					.setAttribute("aria-hidden", "true")
					.setAttribute ("tabindex", -1)
					.removeClass(this.get('activeClass'))
					.addClass(this.get('disabledClass'));
								
			}
		}
	
	}, {
		ATTRS: {
	
			/**
			 * @attribute activeClass
			 * @description
			 */			
			activeClass: {
				value: CSS_BTN_ACTIVE
			},
			
			
			/**
			 * @attribute disabledClass
			 * @description
			 */			
			disabledClass: {
				value: CSS_BTN_DISABLED
			},
			
			
			/**
			 * @attribute allowToggle
			 * @description Setting true applies/removes a views activeClass depending on the 'isActive' toggle.
			 * Note: the toggle will continue if set to false - this attribute just prevents the application of the class.    
			 */			
			allowToggle: {
				value: false
			},
			
	
			/**
			 * @attribute isActive
			 * @description
			 */			
			isActive: {
				value: false
			},						
			
			
			/**
			 * @attribute isEnabled
			 * @description
			 */			
			isEnabled: {
				value: true
			},
			
			
			payload: {				
				getter: function() {										
					if(Y.Lang.isFunction(this._payload)) {
						return this._payload.call(this._payloadContext || this);
					} else {
						return this._payload;
					}
				},
				setter: function(value) {
					if(Y.Lang.isObject(this._payload) || Y.Lang.isFunction(this._payload)) {
						return this._payload;
					} else {
						return {};
					}
				},
			},

		}
	});
	
}, '0.0.1', {
	    
	requires: [
       'yui-base',
       'event-custom',
       'node',
       'view',
       'view-node-map',
       'template-micro'
       ]
});