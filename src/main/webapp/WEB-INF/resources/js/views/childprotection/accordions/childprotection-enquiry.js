YUI.add('childprotection-enquiry-accordion-views', function(Y) {
    'use-strict';

    var L = Y.Lang,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates;

    Y.namespace('app.childprotection').ChildProtectionEnquiryResults = Y.Base.create('childProtectionEnquiryResults', Y.usp.app.Results, [], {
        //set the tablePanelType to be an Accordion Table Panel
        tablePanelType: Y.usp.AccordionTablePanel,

        getResultsListModel: function(config) {
            var subject = config.subject;

            return new Y.usp.childprotection.PaginatedChildProtectionEnquiryWithEnquirerSubjectList({
                url: L.sub(config.url, {
                    subjectId: subject.subjectId
                })
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions;
            return ([{
                key: 'enquiryRequestDate',
                label: labels.enquiryRequestDate,
                sortable: true,
                width: '20%',
                formatter: USPFormatters.date
            }, {
                key: 'enquirerSubject!name',
                label: labels.subjectName,
                sortable: true,
                width: '25%'
            }, {
                key: 'enquiryReason',
                label: labels.enquiryReason,
                sortable: true,
                width: '20%',
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.childprotection.enquiryReason.category.codedEntries
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'viewCPEnquiry',
                    title: labels.viewEnquiry,
                    label: labels.viewEnquiry,
                    visible: permissions.canViewCPEnquiry
                }, {
                    clazz: 'removeCPEnquiry',
                    title: labels.removeEnquiry,
                    label: labels.removeEnquiry,
                    visible: permissions.canDeleteCPEnquiry
                }]
            }]);
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app.childprotection').ChildProtectionEnquiryAccordion = Y.Base.create('childProtectionEnquiryAccordion', Y.View, [], {
      initializer: function(config) {
        this.searchResults = new Y.app.childprotection.ChildProtectionEnquiryResults(config).addTarget(this);

        this.dialog = new Y.app.childprotection.ChildProtectionEnquiryDialog(config.dialogConfig).addTarget(this).render();

        this._evtHandlers = [
          this.on('*:addCPEnquiry', this._handleAddCPEnquiry, this),
          this.on('*:viewCPEnquiry', this._handleViewCPEnquiry, this),
          this.on('*:removeCPEnquiry', this._handleRemoveCPEnquiry, this),
          this.on('*:saved', this.searchResults.reload, this.searchResults)
        ];
      },
      render: function() {
        this.get('container').append(this.searchResults.render().get('container'));

        this._displayAddButton();

        return this;
      },
      _displayAddButton: function() {
        var permissions = this.get('permissions') || {},
          buttonConfig = this.get('addButtonConfig') || {};

        this.toolbar = new Y.usp.app.AppToolbar({
          permissions: permissions,
          //set the toolbar node to the Button Holder on the TablePanel inside the belonging results
          toolbarNode: this.searchResults.getTablePanel().getButtonHolder()
        }).addTarget(this);

        if (permissions.canAddCPEnquiry) {
          //add a button to the toolbar if user had add permission
          this.toolbar.addButton({
            icon: buttonConfig.icon,
            className: 'pull-right pure-button-active',
            action: 'addCPEnquiry',
            title: buttonConfig.title,
            ariaLabel: buttonConfig.ariaLabel,
            label: buttonConfig.label
          });
        }
      },
      _handleAddCPEnquiry: function() {
        var permissions = this.get('permissions') || {};
        var subject = this.get('searchConfig').subject;
        var dialogConfig = this.get('dialogConfig').views.addChildProtectionEnquiry;

        if (permissions.canAddCPEnquiry) {
          this.dialog.showView('addChildProtectionEnquiry', {
            model: new Y.app.childprotection.NewChildProtectionEnquiryForm({
              url: Y.Lang.sub(dialogConfig.config.url, {
                subjectId: subject.subjectId
              }),
              labels: dialogConfig.validationLabels || {},
              personId: subject.subjectId
            }),
            otherData: {
              subject: subject
            }
          }, function(view) {
            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          });
        }
      },
      _handleViewCPEnquiry: function(e) {
        var record = e.record;
        var permissions = this.get('permissions') || {};
        var subject = this.get('searchConfig').subject;
        var dialogConfig = this.get('dialogConfig').views.viewChildProtectionEnquiry,
         subjectIsPerson = record.get('enquirerSubject').type === 'PERSON' ? 'PERSON' : null,
         teamAddressDisplay = (null != e.record.get('enquirerTeamAddress')) ? e.record.get('enquirerTeamAddress').location.location : null,
         enquirerAddress = (null !== record.get('enquirerTeam') ? teamAddressDisplay :
        			e.record.get('enquirerAddress').location.location);

        if (permissions.canViewCPEnquiry) {
          this.dialog.showView('viewChildProtectionEnquiry', {
            model: new Y.usp.childprotection.ChildProtectionEnquiryWithEnquirerSubject({
              url: Y.Lang.sub(dialogConfig.config.url, {
                id: record.get('id')
              })
            }),
            otherData: {
              subject: subject,
              enquirerAddress: enquirerAddress,
              subjectIsPerson: subjectIsPerson
            }
          }, {
            modelLoad: true
          });
        }
      },
      _handleRemoveCPEnquiry: function(e) {
        var record = e.record;
        var permissions = this.get('permissions') || {};
        var dialogConfig = this.get('dialogConfig').views.deleteChildProtectionEnquiry;

        if (permissions.canDeleteCPEnquiry) {
          this.dialog.showView('deleteChildProtectionEnquiry', {
            model: new Y.usp.childprotection.ChildProtectionEnquiryWithEnquirerSubject({
              url: Y.Lang.sub(dialogConfig.config.url, {
                id: record.get('id')
              })
            }),
            otherData: {
              subject: this.get('searchConfig').subject
            }
          }, {
            modelLoad: true
          }, function(view) {
            this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            //get rid of the cancel button - we have a no button instead
            this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
          });
        }
      }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-results',
        'app-toolbar',
        'results-formatters',
        'results-templates',
        'categories-childprotection-component-EnquiryReason',
        'usp-childprotection-ChildProtectionEnquiryWithEnquirerSubject',
        'childprotection-dialog-views'
    ]
});
