YUI.add('childprotection-controller-view', function(Y) {
    'use-strict';

    Y.namespace('app.childprotection').ChildProtectionControllerView = Y.Base.create('childProtectionControllerView', Y.View, [], {
        initializer: function(config) {
          this.childProtectionEnquiryAccordion = new Y.app.childprotection.ChildProtectionEnquiryAccordion(config.childProtectionEnquiryAccordionConfig).addTarget(this);
        },
        destructor: function() {
            this.childProtectionEnquiryAccordion.destroy();
            delete this.childProtectionEnquiryAccordion;
        },
        render: function() {
            this.get('container').append(this.childProtectionEnquiryAccordion.render().get('container'));
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'promise',
        'childprotection-enquiry-accordion-views'
    ]
});
