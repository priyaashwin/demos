YUI.add('person-summary-administration-view', function (Y) {
    var A = Y.Array,
        L = Y.Lang;

    Y.namespace('app').CaseNoteConfigurableTypeReferenceTab = Y.Base.create('caseNoteConfigurableTypeReferenceTab', Y.usp.AccordionTablePanel, [], {
        initializer: function (config) {
            var labels = config.labels,
                permissions = config.permissions || {};

            this.personSummaryAdminDialog = new Y.app.PersonSummaryAdministrationDialog({
                views: {
                    updateCaseNoteConfigurableTypeReferenceView: {
                        headerTemplate: config.dialogConfig.update.headerTemplate
                    }
                }
            });
            this.personSummaryAdminDialog.render();

            this.results = new Y.usp.PaginatedResultsTable({
                columns: [{
                    key: 'identifier',
                    label: labels.identifier,
                    sortable: false,
                    width: '18%'
                }, {
                    key: 'caseNoteEntryType',
                    label: labels.caseNoteEntryType,
                    sortable: false,
                    width: '18%',
                    formatter: Y.usp.ColumnFormatters.codedEntry,
                    codedEntries: Y.uspCategory.casenote.entryType.category.codedEntries,
                }, {
                    label: labels.update,
                    className: 'pure-table-actions',
                    width: '8%',
                    formatter: Y.usp.ColumnFormatters.actions,
                    items: [{
                        clazz: 'edit',
                        title: labels.update,
                        label: labels.update,
                        visible: function () {
                            return permissions.canUpdate;
                        }
                    }]
                }],

                data: new Y.usp.casenote.PaginatedCaseNoteConfigurableTypeReferenceList({
                    url: this.get('url')
                }),

                initialLoad: config.initialLoad,
                initialMessage: config.initialMessage,
                noDataMessage: config.noDataMessage,

                plugins: [{
                    fn: Y.Plugin.usp.ResultsTableMenuPlugin
                }, {
                    fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                }]
            });

            this._evtHandlers = [
                this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this),
                Y.on('caseNotePersonSummaryAdminForm:showDialog', this._handleShowDialog, this),
                Y.on('caseNoteConfigurableTypeReference:saved', this._handleSave, this)
            ];
        },

        render: function () {

            //superclass call
            Y.app.CaseNoteConfigurableTypeReferenceTab.superclass.render.call(this);

            this.renderResults(this.results);

            return this;
        },

        destructor: function () {
            //destroy event handlers
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            //destroy results table
            if (this.results) {
                this.results.destroy();
                delete this.results;
            }
        },

        _handleRowClick: function (e) {
            var target = e.currentTarget,
                cId = target.get('id'),
                record = this.results.getRecord(cId);
            e.preventDefault();
            if (target.hasClass('edit')) {
                Y.fire('caseNotePersonSummaryAdminForm:showDialog', {
                    action: 'edit',
                    id: record.get('id'),
                    identifier: record.get('identifier'),
                    objectVersion: record.get('objectVersion'),
                });
            }
        },

        _handleShowDialog: function (e) {
            var dialogConfig = this.get('dialogConfig').update;
            switch (e.action) {
            case 'edit':
                this.personSummaryAdminDialog.showView('updateCaseNoteConfigurableTypeReferenceView', {
                    labels: dialogConfig.labels,
                    model: new Y.app.UpdateCaseNoteConfigurableTypeReferenceForm({
                        url: L.sub(dialogConfig.url, {
                            id: e.id
                        }),
                        id: e.id,
                        identifier: e.identifier,
                        objectVersion: e.objectVersion
                    })
                }, function (view) {
                    var narrative = dialogConfig.labels.narrative;
                    view.get('container').one('.narrative-summary').setHTML(narrative);
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
                break;
            }
        },

        _handleSave: function (e) {
            Y.fire('infoMessage:message', {
                message: e.successMessage
            });

            this.results.reload();
        }
    });

    var formatTeams = function (o) {
        var data = o.record.toJSON();
        var teams = data.associatedTeams || [];
        var teamsArray = [],
            teamsCount = teams.length,
            teamsOnHover = [],
            moreTeamsCount = 0,
            moreTeams = false,
            splicingSize = 5,
            teamTitle = 'team{plural}',
            OtherTeamTitle = 'Other team{plural}',
            otherTitle = 'other{plural}',
            noTeamsLabel = 'There are no teams to display',
            template = Y.Handlebars.templates['teamFormatter'];
        if (teams && teams !== null && teams.length > 0) {
            A.each(teams, function (team) {
                teamsArray.push(team.name);
            });
        }
        if (teamsCount > splicingSize) {
            if (teamsCount > splicingSize + 1) {
                var splicingValue = teamsCount - splicingSize;
                teamsOnHover = teamsArray.splice(splicingSize, splicingValue);
            } else {
                teamsOnHover = teamsArray.splice(splicingSize, 1);
            }
            moreTeamsCount = teamsCount - splicingSize;
            moreTeams = true;
        }
        if (!teamsCount) {
            teamTitle = '';
        } else if (teamsCount > 0) {
            if (!moreTeamsCount) {
                OtherTeamTitle = '';
                otherTitle = '';
            }
            teamTitle = Y.Lang.sub(teamTitle, {
                plural: (teamsCount > 1) ? 's' : ''
            });
            if (moreTeamsCount > 0) {
                OtherTeamTitle = Y.Lang.sub(OtherTeamTitle, {
                    plural: (moreTeamsCount > 1) ? 's' : ''
                });
                otherTitle = Y.Lang.sub(otherTitle, {
                    plural: (moreTeamsCount > 1) ? 's' : ''
                });
            }
        }
        var teamDetails = teamsArray.join(', ');

        return template({
            modelData: {
                count: teamsCount,
                teams: teamsArray,
                teamDetails: teamDetails,
                teamsOnHover: teamsOnHover,
                moreTeams: moreTeams,
                moreTeamsCount: moreTeamsCount,
                teamTitle: teamTitle,
                title: OtherTeamTitle,
                otherTitle: otherTitle,
                noTeamsLabel: noTeamsLabel
            }
        });
    };

    Y.namespace('app').FormConfigurableTypeReferenceTab = Y.Base.create('formConfigurableTypeReferenceTab', Y.usp.app.Results, [], {
        tablePanelType: Y.usp.AccordionTablePanel,
        events: {
            '.pure-table-data tr a': {
                click: '_handleRowClick'
            },
            '[data-button-action="addFormConfigurable"]': {
                click: '_handleAddFormConfigurable'
            }
        },
        initializer: function (config) {
            this.personSummaryAdminDialog = new Y.app.PersonSummaryAdministrationDialog(config.dialogConfig).addTarget(this).render();

            this._evtHandlers = [
                Y.on('personSummaryAdminForm:showDialog', this._handleShowDialog, this),
                this.on('*:saved', this._handleSave, this)
            ];
        },

        getResultsListModel: function () {
            return new Y.usp.form.PaginatedFormConfigurableTypeReferenceWithTeamsList({
                url: this.get('url')
            });
        },
        getColumnConfiguration: function () {
            var labels = this.get('labels'),
                permissions = this.get('permissions');

            return ([{
                key: 'identifier',
                label: labels.identifier,
                sortable: false,
                width: '18%'
            }, {
                key: 'formDefinitionName',
                label: labels.form,
                sortable: false,
                width: '18%',
            }, {
                label: labels.update,
                className: 'pure-table-actions',
                width: '8%',
                formatter: Y.usp.ColumnFormatters.actions,
                items: [{
                        clazz: 'edit',
                        title: labels.update,
                        label: labels.update,
                        visible: function () {
                            return permissions.canUpdate;
                        }
                    },
                    {
                        clazz: 'delete',
                        title: labels.delete,
                        label: labels.delete,
                        visible: function () {
                            return permissions.canDelete;
                        }
                    }
                ]
            }]);
        },

        render: function () {
            var permissions = this.get('permissions') || {};
            var labels = this.get('labels');
            //superclass call
            Y.app.FormConfigurableTypeReferenceTab.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this.toolbar.addButton({
                icon: 'fa fa-plus-circle',
                className: 'pull-right pure-button-active',
                action: 'addFormConfigurable',
                title: labels.add,
                ariaLabel: labels.add,
                label: labels.add
            });
            return this;
        },

        destructor: function () {
            //destroy event handlers
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            //destroy results table
            if (this.results) {
                this.results.destroy();
                delete this.results;
            }

            if (this.personSummaryAdminDialog) {
                this.personSummaryAdminDialog.removeTarget(this);
                this.personSummaryAdminDialog.destroy();
                delete this.personSummaryAdminDialog;
            }
        },

        _handleRowClick: function (e) {
            var target = e.currentTarget,
                cId = target.get('id'),
                results = this.get('results'),
                record = results.getRecord(cId);
            e.preventDefault();
            if (target.hasClass('edit')) {
                Y.fire('personSummaryAdminForm:showDialog', {
                    action: 'edit',
                    id: record.get('id'),
                    identifier: record.get('identifier'),
                    objectVersion: record.get('objectVersion'),
                    associatedTeams: record.get('associatedTeams'),
                    formDefinitionUid: record.get('formDefinitionUID'),
                    url: this.get('dialogConfig').views.updateFormConfigurableTypeReferenceView.url,
                    teamUrl: this.get('dialogConfig').views.updateFormConfigurableTypeReferenceView.teamUrl,
                    formDefinitionUrl: this.get('dialogConfig').views.updateFormConfigurableTypeReferenceView.formDefinitionUrl
                });
            } else if (target.hasClass('delete')) {
                Y.fire('personSummaryAdminForm:showDialog', {
                    action: 'delete',
                    id: record.get('id'),
                    url: this.get('dialogConfig').views.deleteFormConfigurableTypeReferenceView.url
                });
            }
        },

        _handleAddFormConfigurable: function () {
            Y.fire('personSummaryAdminForm:showDialog', {
                action: 'add',
                url: this.get('dialogConfig').views.addFormConfigurableTypeReferenceView.url,
                teamUrl: this.get('dialogConfig').views.addFormConfigurableTypeReferenceView.teamUrl,
                formDefinitionUrl: this.get('dialogConfig').views.addFormConfigurableTypeReferenceView.formDefinitionUrl
            });
        },

        _handleShowDialog: function (e) {
            var teamList;
            switch (e.action) {
            case 'add':
                teamList = new Y.usp.organisation.TeamList({
                    url: e.teamUrl
                });
                teamList.load(function (err, response) {
                    this.personSummaryAdminDialog.showView('addFormConfigurableTypeReferenceView', {
                        formDefinitionUrl: e.formDefinitionUrl,
                        teamList: JSON.parse(response.response),
                        model: new Y.app.AddFormConfigurableTypeReferenceForm({
                            url: e.url,
                        }),
                    }, function () {
                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
                }.bind(this));
                break;
            case 'edit':
                teamList = new Y.usp.organisation.TeamList({
                    url: e.teamUrl
                });
                teamList.load(function (err, response) {
                    this.personSummaryAdminDialog.showView('updateFormConfigurableTypeReferenceView', {
                        formDefinitionUrl: e.formDefinitionUrl,
                        formDefinitionUid: e.formDefinitionUid,
                        teamList: JSON.parse(response.response),
                        model: new Y.app.UpdateFormConfigurableTypeReferenceForm({
                            url: L.sub(e.url, {
                                id: e.id
                            }),
                            id: e.id,
                            identifier: e.identifier,
                            objectVersion: e.objectVersion,
                            associatedTeams: e.associatedTeams
                        })
                    }, function () {
                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
                }.bind(this));
                break;
            case 'delete':
                this.personSummaryAdminDialog.showView('deleteFormConfigurableTypeReferenceView', {
                    model: new Y.app.UpdateFormConfigurableTypeReferenceForm({
                        url: L.sub(e.url, {
                            id: e.id
                        }),
                        id: e.id,
                    }),
                    id: e.id
                }, function () {
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
                break;
            }

        },

        _handleSave: function () {
            this.get('results').reload();
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function () {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                        cfg: {
                            state: 'expanded',
                            summaryFormatter: formatTeams
                        }
                    }];
                }
            }
        }
    });

    Y.namespace('app').PersonSummaryAdminTabsView = Y.Base.create('personSummaryAdminTabsView', Y.usp.app.AppTabbedPage, [], {
        views: {
            casenote: {
                type: Y.app.CaseNoteConfigurableTypeReferenceTab
            },
            form: {
                type: Y.app.FormConfigurableTypeReferenceTab
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'app-results',
        'app-toolbar',
        'event-custom',
        'app-tabbed-page',
        'results-templates',
        'results-formatters',
        'results-table-menu-plugin',
        'results-table-keyboard-nav-plugin',
        'results-table-summary-row-plugin',
        'paginated-results-table',
        'usp-form-FormConfigurableTypeReferenceWithTeams',
        'usp-casenote-CaseNoteConfigurableTypeReference',
        'accordion-panel',
        'usp-form-UpdateFormConfigurableTypeReference',
        'usp-casenote-UpdateCaseNoteConfigurableTypeReference',
        'person-summary-administration-dialog-views',
        'categories-casenote-component-EntryType',
        'usp-organisation-Team',
    ]
});