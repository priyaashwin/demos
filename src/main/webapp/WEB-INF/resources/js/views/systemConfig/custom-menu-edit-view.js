YUI.add('app-custom-menu-form-edit', function(Y) {
    var TOKEN_SYNTAX = {
            start: '{{',
            end: '}}'
        },
        getDomain = function(url) {
            return typeof url === 'string' && url !== '' ? url.split('?')[0] : '';
        },
        getDecodedURI = function(URI) {
            return decodeURIComponent(URI).replace(TOKEN_SYNTAX.start, '').replace(TOKEN_SYNTAX.end, '');
        },
        getEncodedURI = function(types, type) {
            var encodeURI = encodeURIComponent(type);
            return types.indexOf(type) == -1 ? encodeURI : TOKEN_SYNTAX.start + encodeURI + TOKEN_SYNTAX.end;
        };
    Y.namespace('app.custommenu').EditCustomMenuItemView = Y.Base.create('editCustomMenuItemForm', Y.usp.custommenu.CustomMenuItemView, [], {
        template: Y.Handlebars.templates['customMenuItemEditView'],
        events: {
            'input[name="menuUrl"]': {
                valuechange: 'handleMenuUrlChange'
            }
        },
        initializer: function() {
            this.parametersView = new ParametersView({
                types: this.get('availableTokenData')
            });
            this.parametersView.on('urlChange', this.handleUrlChange, this);
            this.get('model').after('*:load', function() {
                this.parametersView.set('url', this.get('model').get('menuUrl'));
            }, this);
        },
        handleUrlChange: function(e) {
            //update the value in the url input
            this.get('container').one('input[name="menuUrl"]').set('value', e.newVal);
        },
        render: function() {
            Y.app.custommenu.EditCustomMenuItemView.superclass.render.call(this);
            this.get('container').one('#menuUrlParameters').setHTML(this.parametersView.render().get('container'));
            return this;
        },
        handleMenuUrlChange: function(e) {
            this.parametersView.set('url', e.currentTarget.get('value'));
        }
    });
    var ParametersView = Y.Base.create('parametersView', Y.View, [], {
        template: Y.Handlebars.templates['customMenuUrlParametersView'],
        initializer: function() {
            this._paramEvts = [
                this.after('urlChange', this.handleUrlChange, this),
                this.after('parametersChange', this.render, this),
                this.after('*:updateParameter', this.handleParameterChange, this)
            ];
            this.parameterViews = [];
        },
        destructor: function() {
            this._paramEvts.forEach(function(handler) {
                handler.detach();
            });
            this.parameterViews.forEach(function(parameterView) {
                parameterView.destroy();
            });
            this.parameterViews = null;
        },
        handleUrlChange: function(e) {
            var parameters = this.parseParameters(e.newVal);
            parameters.push(this.get('ADDITIONAL_PARAM'));
            //update the parameters with the parsed parameters (and a new empty value)
            this.set('parameters', parameters);
        },
        handleParameterChange: function(e) {
            //object of type {index: num, parameterName:'',parameterType:''}
            var parameters = this.get('parameters');
            parameters[e.index] = {
                name: e.parameterName,
                type: e.parameterType
            };
            //update the url - which will in turn update the parameters when it is re-parsed
            this.set('url', this.constructUrl(parameters));
        },
        constructUrl: function(parameters) {
            var nameValues = parameters.filter(function(parameter) {
                return parameter.name !== '';
            }).map(function(parameter) {
                var p = [];
                if(p.name!==''){
                  p.push(parameter.name);
                }
                if (parameter.type !== '') {
                    p.push(getEncodedURI(this.get('types'), parameter.type));
                } else {
                    p.push('');
                }
                return p.join('=');
            }.bind(this));
            
            return getDomain(this.get('url')) + '?' + nameValues.join('&');
        },
        parseParameters: function(url) {
            return this.extractParameters(url).map(function(param) {
              var p=param.split('=');
                return {
                    name: p[0],
                    type: getDecodedURI(p[1]||'')
                };
            });
        },
        extractParameters: function(url) {
            if (typeof url !== 'string' || url === '') return [];
            var querystring = url.split('?')[1];
            if (!querystring || querystring === '') return [];
            return querystring.split('&');
        },
        render: function() {
            var container = this.get('container'),
                types = this.get('types'),
                parameters = this.get('parameters');
            //ensure the parameter views match the length of the parameters array and either remove or create as necessary
            if (this.parameterViews.length !== parameters.length) {
                //adjust length
                if (this.parameterViews.length > parameters.length) {
                    //trim parameters - clean up views as we do so
                    this.parameterViews.splice(0, this.parameterViews.length - parameters.length).forEach(function(view) {
                        //remove event target
                        view.removeTarget(this);
                        //remove the view container
                        view.get('container').remove(true);
                        //destroy the view
                        view.destroy();
                    }, this);
                } else {
                    //add new views for the number of items required
                    for (var i = 0, l = parameters.length - this.parameterViews.length; i < l; i++) {
                        var parameterView = new ParameterView({
                            types: types
                        }).render();
                        //add event target
                        parameterView.addTarget(this);
                        this.parameterViews.push(parameterView);
                        //append the view container into this view
                        container.append(parameterView.get('container'));
                    }
                }
            }
            //update parameters in the views
            this.parameterViews.forEach(function(view, i) {
                var parameter = parameters[i];
                view.setAttrs({
                    index: i,
                    name: parameter.name,
                    type: parameter.type
                });
            });
            return this;
        },
    }, {
        ATTRS: {
            parameters: {
                value: []
            },
            ADDITIONAL_PARAM: {
                value: {
                    name: '',
                    type: ''
                }
            },
            url: {
                value: ''
            },
            types: {
                getter: function(values) {
                    return Object.keys(values).map(function(key) {
                        return key
                    });
                }
            }
        }
    });
    var ParameterView = Y.Base.create('parameterView', Y.View, [], {
        template: Y.Handlebars.templates['customMenuUrlParametersView'],
        events: {
            'input[name="type"]': {
                click: 'handleShowAC',
                focus: 'handleShowAC',
                valuechange: 'handleTypeValueChange',
            },
            'input[name="name"]': {
                valuechange: 'handleNameValueChange',
            }
        },
        initializer: function() {
            this._paramEvts = [this.after('nameChange', this.handleNameChange, this),
                this.after('typeChange', this.handleTypeChange, this),
                this.after('*:select', this.handleSelect, this)
            ];
        },
        handleSelect: function(e) {
            var value = e.result.raw;
            this._set('type', value);
            this.fire('updateParameter', {
                index: this.get('index'),
                parameterName: this.get('name'),
                parameterType: value
            });
        },
        handleNameChange: function(e) {
            this.get('container').one('input[name="name"]').set('value', e.newVal);
        },
        handleNameValueChange: function(e) {
            var value = e.currentTarget.get('value');
            this._set('name', value);
            this.fire('updateParameter', {
                index: this.get('index'),
                parameterName: value,
                parameterType: this.get('type')
            });
        },
        handleTypeChange: function(e) {
            this.get('container').one('input[name="type"]').set('value', e.newVal);
        },
        handleTypeValueChange: function(e) {
            var value = e.currentTarget.get('value');
            this._set('type', value);
            this.fire('updateParameter', {
                index: this.get('index'),
                parameterName: this.get('name'),
                parameterType: value
            });
        },
        handleShowAC: function(e) {
            var input = e.currentTarget;
            if (this.autocomplete) {
                this.autocomplete.fire('query', {
                    inputValue: input.get('value'),
                    query: input.get('value'),
                    src: 'ui'
                });
            }
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    name: this.get('name'),
                    type: this.get('type')
                });
            container.setHTML(html);
            this.autocomplete = new Y.usp.PopupAutoComplete({
                delay: 0,
                inputNode: container.one('input[name="type"]'),
                allowBrowserAutocomplete: false,
                source: this.get('types'),
                scrollIntoView: true,
                resultHighlighter: 'phraseMatch',
                resultFilters: function(query, results) {
                    return results.filter(function(result) {
                        return result.text.toLowerCase().indexOf(query.toLowerCase()) !== -1;
                    });
                }
            }).render();
            //add as event target
            this.autocomplete.addTarget(this);
            return this;
        },
        destructor: function() {
            this._paramEvts.forEach(function(handler) {
                handler.detach();
            });
            delete this._paramEvts;
            //unplug autocomplete
            if (this.autocomplete) {
                this.autocomplete.removeTarget(this);
                this.autocomplete.destroy();
                delete this.autocomplete;
            }
        }
    }, {
        ATTRS: {
            index: {
                value: 0
            },
            name: {
                value: ''
            },
            type: {
                value: ''
            },
            types: {
                value: []
            }
        }
    });
}, '0.0.1', {
    requires: [
      'yui-base',
      'event-valuechange',
      'handlebars-base',
      'handlebars-helpers',
      'usp-custommenu-CustomMenuItem',
      'handlebars-systemConfig-templates',
      'event-custom',
      'popup-autocomplete']
});