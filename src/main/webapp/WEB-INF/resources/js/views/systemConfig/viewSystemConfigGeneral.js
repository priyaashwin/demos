YUI.add('admin-system-config-screen', function(Y) {
    var AU = Y.app.utils,
        H = Y.Handlebars;

    /**
     *  Administration system configuration General view
     */
    Y.namespace('app.security').SystemConfigGeneralModel = Y.Base.create('systemConfigGeneralModel',
        Y.usp.security.SecurityConfiguration, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
            form: '#generalConfigForm'
        }, {
            ATTRS: {


                multipleLoginTimeoutUserInput: {
                    getter: function() {
                        var v = this.get('multipleLoginTimeout');
                        return v ? Math.round(v / 60000) : 0;
                    },
                    setter: function(v) {
                        this.set('multipleLoginTimeout', v * 60000);
                    }
                }
            }

        });

    Y.namespace('app.security').SystemConfigGeneralView = Y.Base.create('systemConfigGeneralView', Y.usp.security.SecurityConfigurationView, [], {
        events: {
            'input[name="accountLockOutEnabledCheckbox"]': {
                click: '_enableAccountLockOutInputFields'
            },
            '#generalConfigForm input, #generalConfigForm select': {
                change: '_enableSaveOrCancel'
            },
            '#timeOutForAccountUnlock-input': {
                keyup: '_acceptNumeric'
            },
            '#cancelSystemConfig': {
                click: '_cancelSystemConfig'
            },
            '#saveSystemConfig': {
                click: '_saveSystemConfig'
            },
            '#multipleLoginPeriod-input': {
                keyup: '_acceptNumeric'
            }
        },

        template: Y.Handlebars.templates['systemConfigGeneralView'],

        render: function() {
            var container = this.get('container');
            model = this.get('model');

            var html = this.template({
                labels: this.get('labels'),
                modelData: model.toJSON()
            });

            container.setHTML(html);

            this.initSystemConfigInputs();

            return this;

        },
        initSystemConfigInputs: function() {
            var container = this.get('container'),
                enableLockCheckbox = container.one('input[name="accountLockOutEnabledCheckbox"]'),
                selectLoginLimitInput = container.one('select[name="failedLoginAttemptsLimit"]'),
                selfServicePassRstInput = container.one('input[name="ssprEnabled"]');

            if (this.get('model').get('accountLockOutEnabled') && enableLockCheckbox) {
                enableLockCheckbox.set('checked', true);
            }

            if (this.get('model').get('ssprEnabled') && selfServicePassRstInput) {
                container.one('input[name="ssprEnabled"][value=true]').set('checked', true);
            } else {
                container.one('input[name="ssprEnabled"][value=false]').set('checked', true);
            }

            this._enableAccountLockOutInputFields(enableLockCheckbox);

            if (selectLoginLimitInput) {
                Y.FUtil.setSelectOptions(selectLoginLimitInput,
                    this.get('valuesFailedLoginAttemptLimit'), null, null, true, true, null);
                selectLoginLimitInput.set('value', "" + this.get('model').get('failedLoginAttemptsLimit'));
            }

            container.all('#saveSystemConfig,#cancelSystemConfig').setAttribute('disabled', 'disabled');

        },

        _saveSystemConfig: function(e) {

            var labels = this.get('labels'),
                self = this,
                selfServicePassRstCurrentValue = this.get('container').one('input[name="ssprEnabled"]:checked').get('value');

            e.preventDefault();

            this.resetValidationErrors();

            model.setAttrs({
                accountLockOutEnabled: this.get('accountLockOutEnabled'),
                ssprEnabled: selfServicePassRstCurrentValue ? selfServicePassRstCurrentValue : false
            }, {
                //don't trigger the onchange listener as we will re-render after save
                silent: true
            });

            model.url = '/eclipse/rest/securityConfiguration/' + model.get('id');
            model.save({
                transmogrify: Y.usp.security.UpdateSecurityConfigurationWithGeneralDetails
            }, function(err, response) {

                if (err === null) {
                    Y.fire('infoMessage:message', {
                        message: labels.securityConfigurationDetailsSuccessMessage
                    });
                    Y.fire('security:systemConfigurationChanged', {});
                } else {
                    var e = {};
                    e.code = 400;
                    var responseText = Y.JSON.parse(response.responseText);
                    if (responseText.validationErrors) {
                        e.msg = {
                            validationErrors: responseText.validationErrors
                        };
                    } else {
                        // Show object errors like SMS not configured or Email not configured
                        e.msg = {
                            validationErrors: {
                                objError: responseText.message
                            }
                        };
                    }

                    self.set('systemConfigGeneralView-errorExists', true);
                    self.fire('error', {
                        error: e
                    });
                }
            });

        },

        _cancelSystemConfig: function(e) {

            this.resetValidationErrors();

            if (this.get('systemConfigGeneralView-errorExists')) {
                Y.fire('security:systemConfigurationChanged', {});
                this.set('systemConfigGeneralView-errorExists', false);
            }

            this.render();
        },

        _enableSaveOrCancel: function(e) {
            this.get('container').all('#saveSystemConfig,#cancelSystemConfig').removeClass('pure-button-disabled').removeAttribute('disabled');
        },

        //to enable or disable input fields based on checkbox - Active on multiple login failure
        _enableAccountLockOutInputFields: function(e) {

            var container = this.get('container'),
                enableAccountLockcheckboxInput = e.currentTarget || e;

            if (!enableAccountLockcheckboxInput.get('checked')) {
                container.one('select[name="failedLoginAttemptsLimit"]').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
                container.one('input[name="timeOutForAccountUnlock"]').set('disabled', 'disabled').setAttribute('readonly', 'readonly');
            } else {
                container.one('select[name="failedLoginAttemptsLimit"]').removeAttribute('disabled').removeAttribute('readonly');
                container.one('input[name="timeOutForAccountUnlock"]').removeAttribute('disabled').removeAttribute('readonly');
            }

        },

        _acceptNumeric: function(e) {
            var currentValue = e.currentTarget.get('value'),
                inputId = e.currentTarget.get('id'),
                nos = 0;

            if (inputId == 'multipleLoginPeriod-input') {
                nos = 3;
            } else if (inputId == 'timeOutForAccountUnlock-input') {
                nos = 4;
            }

            e.currentTarget.set("value", AU.AcceptNumeric(currentValue, nos));
            this._enableSaveOrCancel(e);
        },

        resetValidationErrors: function() {
            var validationNode = this.get('container').ancestor('.yui3-widget').one('.message-error');
            if (validationNode) {
                validationNode.remove();
            }

            this.get('container').all('input.error, select.error, fieldset.error').each(function(input) {
                input.removeClass('error');
            });
        },
    }, {
        ATTRS: {
            valuesFailedLoginAttemptLimit: {
                value: [{
                        'id': '3',
                        'name': '3'
                    },
                    {
                        'id': '5',
                        'name': '5'
                    },
                    {
                        'id': '10',
                        'name': '10'
                    },
                    {
                        'id': '15',
                        'name': '15'
                    },
                    {
                        'id': '20',
                        'name': '20'
                    },
                    {
                        'id': '25',
                        'name': '25'
                    },
                    {
                        'id': '30',
                        'name': '30'
                    },
                    {
                        'id': '35',
                        'name': '35'
                    },
                    {
                        'id': '40',
                        'name': '40'
                    },
                    {
                        'id': '45',
                        'name': '45'
                    },
                    {
                        'id': '50',
                        'name': '50'
                    }
                ]
            },
            accountLockOutEnabled: {
                getter: function(val) {
                    var container = this.get('container');
                    if (container.one('input[name="accountLockOutEnabledCheckbox"]').get('checked')) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    });

    Y.namespace('app.security').SystemConfigGeneralAccordion = Y.Base.create('systemConfigGeneralAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {

            this.systemConfigGeneralView = new Y.app.security.SystemConfigGeneralView(Y.mix(config));

            this.systemConfigGeneralView.addTarget(this);

            this.plug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
        },
        render: function() {
            //render the system config view
            var systemConfigGeneralViewContainer = this.systemConfigGeneralView.render().get('container');

            //superclass call
            Y.app.security.SystemConfigGeneralAccordion.superclass.render.call(this);

            //stick the system config container into the accordion
            this.getAccordionBody().appendChild(systemConfigGeneralViewContainer);

            return this;
        },
        destructor: function() {
            this.systemConfigGeneralView.destroy();
            this.unplug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
            delete this.systemConfigGeneralView;
        }
    });

    /**
     *  Administration system configuration SMS view
     */

    Y.namespace('app.security').SystemSmsConfigurationView = Y.Base.create('SystemSmsConfigurationView', Y.usp.security.SecurityConfigurationView, [], {
        events: {
            '#systemSmsConfigurationForm select': {
                change: '_enableSaveOrCancel'
            },
            '#systemSmsConfigurationForm input, #systemSmsConfigurationForm textarea': {
                keyup: '_enableSaveOrCancel'
            },
            '#cancelSmsConfiguration': {
                click: '_cancelSystemConfig'
            },
            '#saveSmsConfiguration': {
                click: '_saveSystemConfig'
            },
            '#timeToLive-input': {
                keyup: '_acceptNumeric'
            }
        },

        template: Y.Handlebars.templates['systemSmsConfigurationView'],

        render: function() {
            var container = this.get('container');

            model = this.get('model');

            var html = this.template({
                labels: this.get('labels'),
                modelData: model.get('twoFactorSmsConfiguration')
            });

            container.setHTML(html);

            this.initSystemConfigInputs();

            return this;

        },
        initSystemConfigInputs: function() {
            var container = this.get('container');
            var timeToLiveInput = container.one('input[name="ttl"]');
            if (this.get('model').toJSON().twoFactorSmsConfiguration) {
                timeToLiveInput.set('value', "" + Math.round((this.get('model').toJSON().twoFactorSmsConfiguration.ttl) / (60000)));
            }
            container.all('#saveSmsConfiguration,#cancelSmsConfiguration').setAttribute('disabled', 'disabled');

        },
        _saveSystemConfig: function(e) {

            var container = this.get('container'),
                labels = this.get('labels'),
                self = this;
            e.preventDefault();

            this.resetValidationErrors();
            model.setAttrs({
                ttl: (container.one('input[name="ttl"]').get('value') * (60000)),
                smsContent: (container.one('textarea[name="smsContent"]').get('value'))
            }, {
                //don't trigger the onchange listener as we will re-render after save
                silent: true
            });

            model.url = '/eclipse/rest/securityConfiguration/' + model.get('id');
            model.save({
                transmogrify: Y.usp.security.UpdateSecurityConfigurationWithTwoFactorSms
            }, function(err, response) {

                if (err === null) {
                    Y.fire('infoMessage:message', {
                        message: labels.twoFactorSmsSuccessMessage
                    });
                    Y.fire('security:systemConfigurationChanged', {});
                } else {
                    var e = {};
                    e.code = 400;
                    e.msg = {
                        validationErrors: Y.JSON.parse(response.responseText).validationErrors
                    };
                    self.fire('error', {
                        error: e
                    });
                }
            });

        },

        _cancelSystemConfig: function(e) {

            this.resetValidationErrors();

            this.render();
        },


        _acceptNumeric: function(e) {
            var currentValue = e.currentTarget.get('value'),
                inputId = e.currentTarget.get('id'),
                nos = 0;

            if (inputId == 'timeToLive-input') {
                nos = 2;
            }
            e.currentTarget.set("value", AU.AcceptNumeric(currentValue, nos));
            this._enableSaveOrCancel(e);
        },
        _enableSaveOrCancel: function(e) {
            this.get('container').all('#saveSmsConfiguration,#cancelSmsConfiguration').removeClass('pure-button-disabled').removeAttribute('disabled');
        },

        resetValidationErrors: function() {
            var validationNode = this.get('container').ancestor('.yui3-widget').one('.message-error');
            if (validationNode) {
                validationNode.remove();
            }

            this.get('container').all('input.error, select.error, fieldset.error').each(function(input) {
                input.removeClass('error');
            });
        }

    }, {
        ATTRS: {

        }
    });

    Y.namespace('app.security').SystemSmsConfigurationAccordion = Y.Base.create('systemSmsConfigurationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {

            this.systemSmsConfigurationView = new Y.app.security.SystemSmsConfigurationView(Y.mix(config));

            this.systemSmsConfigurationView.addTarget(this);

            this.plug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
        },
        render: function() {
            //render the system config view
            var systemSmsConfigurationViewContainer = this.systemSmsConfigurationView.render().get('container');

            //superclass call
            Y.app.security.SystemSmsConfigurationAccordion.superclass.render.call(this);

            //stick the system config container into the accordion
            this.getAccordionBody().appendChild(systemSmsConfigurationViewContainer);

            return this;
        },
        destructor: function() {
            this.systemSmsConfigurationView.destroy();
            this.unplug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
            delete this.systemSmsConfigurationView;
        }
    });

    /**
     *  Administration system configuration Terms and Conditions view
     */

    Y.namespace('app.security').SystemTermsAndConditionsConfigView = Y.Base.create('SystemTermsAndConditionsConfigView', Y.usp.security.SecurityConfigurationView, [], {
        events: {
            '#systemTermsAndConditionsConfigForm input': {
                keyup: '_enableSaveOrCancel'
            },
            '#cancelTermsAndConditionsConfig': {
                click: '_cancelSystemConfig'
            },
            '#saveTermsAndConditionsConfig': {
                click: '_saveSystemConfig'
            }
        },

        template: Y.Handlebars.templates['systemTermsAndConditionsConfigView'],

        render: function() {
            var container = this.get('container');

            model = this.get('model');

            var html = this.template({
                labels: this.get('labels'),
                modelData: model.get('termsAndConditionsConfiguration')
            });

            container.setHTML(html);
            //this.constructor.superclass.render.call(this);

            this.initSystemConfigInputs();

            return this;

        },
        initSystemConfigInputs: function() {
            var container = this.get('container');
            container.all('#saveTermsAndConditionsConfig,#cancelTermsAndConditionsConfig').setAttribute('disabled', 'disabled');

        },
        _saveSystemConfig: function(e) {

            var container = this.get('container'),
                labels = this.get('labels'),
                self = this;
            e.preventDefault();
            this.resetValidationErrors();


            model.setAttrs({
                url: container.one('input[name="url"]').get('value')
            }, {
                //don't trigger the onchange listener as we will re-render after save
                silent: true
            });

            model.url = '/eclipse/rest/securityConfiguration/' + model.get('id');
            model.save({
                transmogrify: Y.usp.security.UpdateSecurityConfigurationWithTermsAndConditions
            }, function(err, response) {

                if (err === null) {
                    Y.fire('infoMessage:message', {
                        message: labels.termsAndConditionsSuccessMessage
                    });
                    Y.fire('security:systemConfigurationChanged', {});
                } else {
                    var e = {};
                    e.code = 400;
                    e.msg = {
                        validationErrors: Y.JSON.parse(response.responseText).validationErrors
                    };
                    self.fire('error', {
                        error: e
                    });
                }
            });

        },

        _cancelSystemConfig: function(e) {

            this.resetValidationErrors();

            this.render();
        },

        _enableSaveOrCancel: function(e) {
            this.get('container').all('#saveTermsAndConditionsConfig,#cancelTermsAndConditionsConfig').removeClass('pure-button-disabled').removeAttribute('disabled');
        },

        resetValidationErrors: function() {
            var validationNode = this.get('container').ancestor('.yui3-widget').one('.message-error');
            if (validationNode) {
                validationNode.remove();
            }

            this.get('container').all('input.error, select.error, fieldset.error').each(function(input) {
                input.removeClass('error');
            });
        }

    }, {
        ATTRS: {

        }
    });

    Y.namespace('app.security').SystemTermsAndConditionsConfigAccordion = Y.Base.create('systemTermsAndConditionsConfigAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            this.systemTermsAndConditionsConfigView = new Y.app.security.SystemTermsAndConditionsConfigView(Y.mix(config));

            this.systemTermsAndConditionsConfigView.addTarget(this);

            this.plug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
        },
        render: function() {

            var systemTermsAndConditionsConfigViewContainer = this.systemTermsAndConditionsConfigView.render().get('container');

            //superclass call
            Y.app.security.SystemTermsAndConditionsConfigAccordion.superclass.render.call(this);


            this.getAccordionBody().appendChild(systemTermsAndConditionsConfigViewContainer);

            return this;
        },
        destructor: function() {
            this.systemTermsAndConditionsConfigView.destroy();
            this.unplug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
            delete this.systemTermsAndConditionsConfigView;
        }
    });

    /**
     *  Administration system configuration Forgotten Password Email view
     */

    Y.namespace('app.security').SystemForgottenPswdEmailView = Y.Base.create('systemForgottenPswdEmailView', Y.usp.security.SecurityConfigurationView, [], {
        events: {
            '#systemForgottenPswdForm select': {
                change: '_enableSaveOrCancel'
            },
            '#systemForgottenPswdForm input, #systemForgottenPswdForm textarea': {
                keyup: '_enableSaveOrCancel'
            },
            '#cancelSystemForgottenPswdEmail': {
                click: '_cancelSystemConfig'
            },
            '#saveSystemForgottenPswdEmail': {
                click: '_saveSystemConfig'
            }
        },

        template: Y.Handlebars.templates['systemForgottenPswdEmailView'],

        render: function() {
            var container = this.get('container');

            model = this.get('model');

            var html = this.template({
                labels: this.get('labels'),
                modelData: model.get('forgottenPasswordEmailConfiguration')
            });

            container.setHTML(html);
            //this.constructor.superclass.render.call(this);

            this.initSystemConfigInputs();

            return this;

        },
        initSystemConfigInputs: function() {
            var container = this.get('container'),
                timeToLiveInput = container.one('select[name="ttl"]');

            if (timeToLiveInput) {
                Y.FUtil.setSelectOptions(timeToLiveInput,
                    this.get('timeToLiveOptions'), null, null, true, true, null);
                if (this.get('model').get('forgottenPasswordEmailConfiguration')) {
                    timeToLiveInput.set('value', "" + Math.round((this.get('model').get('forgottenPasswordEmailConfiguration').ttl) / (1000 * 60 * 60)));
                }
            }

            container.all('#saveSystemForgottenPswdEmail,#cancelSystemForgottenPswdEmail').setAttribute('disabled', 'disabled');

        },
        _saveSystemConfig: function(e) {

            var container = this.get('container'),
                labels = this.get('labels'),
                self = this,
                emailAdd = container.one('input[name="fromAddress"]').get('value');

            e.preventDefault();
            this.resetValidationErrors();

            model.setAttrs({
                fromAddress: emailAdd,
                subject: container.one('input[name="subject"]').get('value'),
                ttl: (container.one('select[name="ttl"]').get('value') * (1000 * 60 * 60)),
                content: container.one('textarea[name="content"]').get('value')
            }, {
                //don't trigger the onchange listener as we will re-render after save
                silent: true
            });

            model.url = '/eclipse/rest/securityConfiguration/' + model.get('id');
            model.save({
                transmogrify: Y.usp.security.UpdateSecurityConfigurationWithForgottenPasswordEmailDetails
            }, function(err, response) {

                if (err === null) {
                    Y.fire('infoMessage:message', {
                        message: labels.forgottenPasswordEmailDetailsSuccessMessage
                    });
                    Y.fire('security:systemConfigurationChanged', {});
                } else {
                    var e = {};
                    e.code = 400;
                    e.msg = {
                        validationErrors: Y.JSON.parse(response.responseText).validationErrors
                    };
                    self.fire('error', {
                        error: e
                    });
                }
            });
        },

        _cancelSystemConfig: function(e) {

            this.resetValidationErrors();

            this.render();
        },

        _enableSaveOrCancel: function(e) {
            this.get('container').all('#saveSystemForgottenPswdEmail,#cancelSystemForgottenPswdEmail').removeClass('pure-button-disabled').removeAttribute('disabled');
        },

        resetValidationErrors: function() {
            var validationNode = this.get('container').ancestor('.yui3-widget').one('.message-error');
            if (validationNode) {
                validationNode.remove();
            }

            this.get('container').all('input.error, select.error, fieldset.error').each(function(input) {
                input.removeClass('error');
            });
        }

    }, {
        ATTRS: {
            timeToLiveOptions: {
                getter: function() {
                    var options = [];
                    for (var i = 1; i <= 144; i++) {
                        options.push({
                            'id': i,
                            'name': i
                        });
                    }
                    return options;
                }
            }
        }
    });

    Y.namespace('app.security').SystemForgottenPswdEmailAccordion = Y.Base.create('systemForgottenPswdEmailAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {

            this.systemForgottenPswdEmailView = new Y.app.security.SystemForgottenPswdEmailView(Y.mix(config));

            this.systemForgottenPswdEmailView.addTarget(this);

            this.plug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
        },
        render: function() {
            //render the system config view
            var systemForgottenPswdEmailViewContainer = this.systemForgottenPswdEmailView.render().get('container');

            //superclass call
            Y.app.security.SystemForgottenPswdEmailAccordion.superclass.render.call(this);

            //stick the system config container into the accordion
            this.getAccordionBody().appendChild(systemForgottenPswdEmailViewContainer);

            return this;
        },
        destructor: function() {
            this.systemForgottenPswdEmailView.destroy();
            this.unplug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
            delete this.systemForgottenPswdEmailView;
        }
    });

    /**
     *  Administration system configuration Login details emails view
     */

    Y.namespace('app.security').SystemLoginDetailsEmailsView = Y.Base.create('systemLoginDetailsEmailsView', Y.usp.security.SecurityConfigurationView, [], {
        events: {
            '#systemLoginDetailsEmailsForm select': {
                change: '_enableSaveOrCancel'
            },
            '#systemLoginDetailsEmailsForm input, #systemLoginDetailsEmailsForm textarea': {
                keyup: '_enableSaveOrCancel'
            },
            '#cancelSystemLoginDetailsEmails': {
                click: '_cancelSystemConfig'
            },
            '#saveSystemLoginDetailsEmails': {
                click: '_saveSystemConfig'
            }
        },

        template: Y.Handlebars.templates['systemLoginDetailsEmailsView'],

        render: function() {
            var container = this.get('container');

            model = this.get('model');

            var html = this.template({
                labels: this.get('labels'),
                modelData: model.get('emailLoginDetailsConfiguration')
            });

            container.setHTML(html);
            //this.constructor.superclass.render.call(this);

            var systemLoginDetailsEmails = new Y.TabView({
                srcNode: '#systemLoginDetailsEmailsTabView',
                render: true
            });

            this.initSystemConfigInputs();

            return this;

        },
        initSystemConfigInputs: function() {
            var container = this.get('container');

            container.all('#saveSystemLoginDetailsEmails,#cancelSystemLoginDetailsEmails').setAttribute('disabled', 'disabled');

        },
        _saveSystemConfig: function(e) {

            var container = this.get('container'),
                labels = this.get('labels'),
                self = this,
                emailAdd = container.one('input[name="fromAddress"]').get('value');

            e.preventDefault();
            this.resetValidationErrors();

            model.setAttrs({
                fromAddress: emailAdd,
                userSubject: container.one('input[name="userSubject"]').get('value'),
                passwordSubject: container.one('input[name="passwordSubject"]').get('value'),
                userContent: container.one('textarea[name="userContent"]').get('value'),
                passwordContent: container.one('textarea[name="passwordContent"]').get('value')
            }, {
                //don't trigger the onchange listener as we will re-render after save
                silent: true
            });

            model.url = '/eclipse/rest/securityConfiguration/' + model.get('id');
            model.save({
                transmogrify: Y.usp.security.UpdateSecurityConfigurationWithEmailLoginDetails
            }, function(err, response) {

                if (err === null) {
                    Y.fire('infoMessage:message', {
                        message: labels.loginDetailsEmailsSuccessMessage
                    });
                    Y.fire('security:systemConfigurationChanged', {});
                } else {
                    var e = {};
                    e.code = 400;
                    e.msg = {
                        validationErrors: Y.JSON.parse(response.responseText).validationErrors
                    };
                    self.fire('error', {
                        error: e
                    });
                }
            });
        },

        _cancelSystemConfig: function(e) {

            this.resetValidationErrors();

            this.render();
        },

        _enableSaveOrCancel: function(e) {
            this.get('container').all('#saveSystemLoginDetailsEmails,#cancelSystemLoginDetailsEmails').removeClass('pure-button-disabled').removeAttribute('disabled');
        },

        resetValidationErrors: function() {
            var validationNode = this.get('container').ancestor('.yui3-widget').one('.message-error');
            if (validationNode) {
                validationNode.remove();
            }

            this.get('container').all('input.error, select.error, fieldset.error').each(function(input) {
                input.removeClass('error');
            });
        }

    }, {
        ATTRS: {}
    });

    Y.namespace('app.security').SystemLoginDetailsEmailsAccordion = Y.Base.create('systemLoginDetailsEmailsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {

            this.systemLoginDetailsEmailsView = new Y.app.security.SystemLoginDetailsEmailsView(Y.mix(config));

            this.systemLoginDetailsEmailsView.addTarget(this);

            this.plug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
        },
        render: function() {
            //render the system config view
            var systemLoginDetailsEmailsViewContainer = this.systemLoginDetailsEmailsView.render().get('container');

            //superclass call
            Y.app.security.SystemLoginDetailsEmailsAccordion.superclass.render.call(this);

            //stick the system config container into the accordion
            this.getAccordionBody().appendChild(systemLoginDetailsEmailsViewContainer);

            return this;
        },
        destructor: function() {
            this.systemLoginDetailsEmailsView.destroy();
            this.unplug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
            delete this.systemLoginDetailsEmailsView;
        }
    });

    /**
     *  Administration system custom menu links view
     */

    Y.namespace('app.security').SystemCustomMenuLinksConfigView = Y.Base.create('systemCustomMenuLinksConfigView', Y.usp.custommenu.CustomMenuItemView, [], {
        events: {
            '#systemCustomMenuLinkEdit': {
                click: '_editCustomMenuItem'
            },

        },
        initializer: function(e) {
            this.after('customMenuLinksChange', this.render, this);

            Y.on('customMenuItems:refreshResults', function(e) {

                this.customMenuLinksDataModel.load(Y.bind(function(err, res) {
                    if (err) {
                        return console.log(err);
                    }

                    this.set('customMenuLinks', this.customMenuLinksDataModel.toJSON());

                }, this));
            }, this);

        },
        template: Y.Handlebars.templates['systemCustomMenuLinksConfigView'],

        render: function() {
            var container = this.get('container'),

                model = this.get('model');
            var html = this.template({
                labels: this.get('labels'),
                customMenuLinks: this.get('customMenuLinks')
            });

            container.setHTML(html);
            //this.constructor.superclass.render.call(this);

            return this;

        },
        _editCustomMenuItem: function(e) {
            e.preventDefault();
            var customMenuItemId = e.currentTarget.getAttribute('data-item-id');
            Y.fire('customMenuItem:showDialog', {
                action: 'editCustomMenuItem',
                id: customMenuItemId
            });

        }
    }, {
        ATTRS: {

            customMenuLinks: {
                value: [],
                valueFn: function() {

                    this.customMenuLinksDataModel = new Y.usp.custommenu.PaginatedCustomMenuItemList({
                        url: this.get('customMenuListUrl')
                    });

                    this.customMenuLinksDataModel.load(Y.bind(function(err, res) {
                        if (err) {
                            return console.log(err);
                        }

                        this.set('customMenuLinks', this.customMenuLinksDataModel.toJSON());

                    }, this));

                    return [];

                }

            },
        }
    });

    Y.namespace('app.security').SystemCustomMenuLinksConfigAccordion = Y.Base.create('systemCustomMenuLinksConfigAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {

            this.systemCustomMenuLinksConfigView = new Y.app.security.SystemCustomMenuLinksConfigView(Y.mix(config));

            this.systemCustomMenuLinksConfigView.addTarget(this);

            this.plug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
        },
        render: function() {
            var systemCustomMenuLinksConfigViewContainer = this.systemCustomMenuLinksConfigView.render().get('container');

            //superclass call
            Y.app.security.SystemCustomMenuLinksConfigAccordion.superclass.render.call(this);

            //stick the system config container into the accordion
            this.getAccordionBody().appendChild(systemCustomMenuLinksConfigViewContainer);

            return this;
        },
        destructor: function() {
            this.systemCustomMenuLinksConfigView.destroy();
            this.unplug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
            delete this.systemCustomMenuLinksConfigView;
        }
    });

    /**
     *  Administration system configuration properties display view
     */

    Y.namespace('app.security').SystemConfigurationPropertiesDisplayView = Y.Base.create('systemConfigurationPropertiesDisplayView', Y.View, [], {
        events: {},

        template: Y.Handlebars.templates['systemConfigurationPropertiesDisplayView'],

        render: function() {
            var container = this.get('container');

            model = this.get('model');

            var html = this.template({
                labels: this.get('labels'),
                //modelData: model.toJSON(),
                smtpConfigProperties: this.get('smtpConfigProperties'),
                smsConfigProperties: this.get('smsConfigProperties'),
                ldapConfigProperties: this.get('ldapConfigProperties')
            });

            container.setHTML(html);
            //this.constructor.superclass.render.call(this);

            this.initSystemConfigInputs();

            return this;

        },
        initSystemConfigInputs: function() {
            var container = this.get('container');

        }
    }, {
        ATTRS: {
            smtpConfigProperties: {},
            smsConfigProperties: {},
            ldapConfigProperties: {}
        }
    });

    Y.namespace('app.security').SystemConfigurationPropertiesDisplayAccordion = Y.Base.create('systemConfigurationPropertiesDisplayAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {

            this.systemConfigurationPropertiesDisplayView = new Y.app.security.SystemConfigurationPropertiesDisplayView(Y.mix(config));

            this.systemConfigurationPropertiesDisplayView.addTarget(this);

            this.plug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
        },
        render: function() {
            var systemConfigurationPropertiesDisplayViewContainer = this.systemConfigurationPropertiesDisplayView.render().get('container');

            //superclass call
            Y.app.security.SystemConfigurationPropertiesDisplayAccordion.superclass.render.call(this);

            //stick the system config container into the accordion
            this.getAccordionBody().appendChild(systemConfigurationPropertiesDisplayViewContainer);

            return this;
        },
        destructor: function() {
            this.systemConfigurationPropertiesDisplayView.destroy();
            this.unplug(Y.Plugin.usp.AccordionPanelModelErrorsPlugin);
            delete this.systemConfigurationPropertiesDisplayView;
        }
    });

    /**
     *  Administration system configuration properties display view
     */

    Y.namespace('app.security').SystemEnvironmentDetailsView = Y.Base.create('systemEnvironmentDetailsView', Y.View, [], {
        template: Y.Handlebars.templates['systemEnvironmentDetailsView'],

        render: function() {
            var container = this.get('container');

            model = this.get('model');

            var html = this.template({
                labels: this.get('labels'),
                environmentProperties: this.get('environmentProperties')
            });

            container.setHTML(html);

            return this;
        }
    });

    Y.namespace('app.security').SystemEnvironmentDetailsAccordion = Y.Base.create('systemEnvironmentDetailsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            this.systemEnvironmentDetailsView = new Y.app.security.SystemEnvironmentDetailsView(Y.mix(config));
            this.systemEnvironmentDetailsView.addTarget(this);
        },
        render: function() {
            Y.app.security.SystemEnvironmentDetailsAccordion.superclass.render.call(this);

            this.getAccordionBody().appendChild(this.systemEnvironmentDetailsView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.systemEnvironmentDetailsView.destroy();
            delete this.systemEnvironmentDetailsView;
        }
    });

    /***
     * 
     * Administration system configuration view
     * 
     */
    Y.namespace('app.security').SystemConfigView = Y.Base.create('systemConfigView', Y.View, [], {

        initializer: function() {
            var systemGeneralConfig = this.get('systemGeneralConfig'),
                systemForgottenPswdEmailConfig = this.get('systemForgottenPswdEmailConfig'),
                systemLoginDetailsEmailsConfig = this.get('systemLoginDetailsEmailsConfig'),
                systemSmsConfig = this.get('systemSmsConfig'),
                systemTermsAndConditionsConfig = this.get('systemTermsAndConditionsConfig'),
                systemCustomMenuLinksConfig = this.get('systemCustomMenuLinksConfig'),
                systemConfigurationPropertiesDisplay = this.get('systemConfigurationPropertiesDisplay'),
                systemEnvironmentDetailsConfig = this.get('systemEnvironmentDetailsConfig');

            //create system views
            this.systemGeneralView = new Y.app.security.SystemConfigGeneralAccordion(Y.merge(systemGeneralConfig, {
                model: this.get('model')
            }));
            this.systemForgottenPswdEmailView = new Y.app.security.SystemForgottenPswdEmailAccordion(Y.merge(systemForgottenPswdEmailConfig, {
                model: this.get('model')
            }));

            this.systemLoginDetailsEmailsView = new Y.app.security.SystemLoginDetailsEmailsAccordion(Y.merge(systemLoginDetailsEmailsConfig, {
                model: this.get('model')
            }));

            this.systemSmsConfigurationView = new Y.app.security.SystemSmsConfigurationAccordion(Y.merge(systemSmsConfig, {
                model: this.get('model')
            }));

            this.systemTermsAndConditionsConfigView = new Y.app.security.SystemTermsAndConditionsConfigAccordion(Y.merge(systemTermsAndConditionsConfig, {
                model: this.get('model')
            }));

            this.systemCustomMenuLinksConfigView = new Y.app.security.SystemCustomMenuLinksConfigAccordion(Y.merge(systemCustomMenuLinksConfig, {
                model: this.get('model')
            }));

            this.systemConfigurationPropertiesDisplayView = new Y.app.security.SystemConfigurationPropertiesDisplayAccordion(Y.merge(systemConfigurationPropertiesDisplay, {
                model: this.get('model')
            }));

            this.systemEnvironmentDetailsView = new Y.app.security.SystemEnvironmentDetailsAccordion(systemEnvironmentDetailsConfig);

            // Add event targets
            this.systemGeneralView.addTarget(this);
            this.systemForgottenPswdEmailView.addTarget(this);
            this.systemSmsConfigurationView.addTarget(this);
            this.systemLoginDetailsEmailsView.addTarget(this);
            this.systemTermsAndConditionsConfigView.addTarget(this);
            this.systemCustomMenuLinksConfigView.addTarget(this);
            this.systemConfigurationPropertiesDisplayView.addTarget(this);
            this.systemEnvironmentDetailsView.addTarget(this);

            // commented out code is how to manually add accordions to the AccordionToggleAllPlugin
            this.plug(Y.Plugin.app.AccordionToggleAllPlugin, {
                srcNode: '#collapse-button',
                //autoDetect: false
            });

            //this.accordionToggleAll.addAccordion(this.systemGeneralView);
        },

        render: function() {
            // Will use the container property when we create the PersonDetailsView to attach this containers content
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.systemGeneralView.render().get('container'));
            contentNode.append(this.systemTermsAndConditionsConfigView.render().get('container'));
            contentNode.append(this.systemSmsConfigurationView.render().get('container'));
            contentNode.append(this.systemLoginDetailsEmailsView.render().get('container'));
            contentNode.append(this.systemForgottenPswdEmailView.render().get('container'));
            contentNode.append(this.systemCustomMenuLinksConfigView.render().get('container'));
            contentNode.append(this.systemConfigurationPropertiesDisplayView.render().get('container'));
            contentNode.append(this.systemEnvironmentDetailsView.render().get('container'));


            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up identification view
            this.systemGeneralView.destroy();
            this.systemForgottenPswdEmailView.destroy();
            this.systemSmsConfigurationView.destroy();
            this.systemLoginDetailsEmailsView.destroy();
            this.systemCustomMenuLinksConfigView.destroy();
            this.systemConfigurationPropertiesDisplayView.destroy();
            this.systemEnvironmentDetailsView.destroy();
            this.unplug(Y.Plugin.app.AccordionToggleAllPlugin);
            delete this.systemGeneralView;
            delete this.systemForgottenPswdEmailView;
            delete this.systemSmsConfigurationView;
            delete this.systemLoginDetailsEmailsView;
            delete this.systemCustomMenuLinksConfigView;
            delete this.systemConfigurationPropertiesDisplayView;
            delete this.systemEnvironmentDetailsView;
        }
    }, {
        ATTRS: {
            model: {},
            systemGeneralView: {
                value: {
                    labels: {}
                }
            },
            systemForgottenPswdEmailView: {
                value: {
                    labels: {}
                }
            },
            systemSmsConfigurationView: {
                value: {
                    labels: {}
                }
            },
            systemTermsAndConditionsConfigView: {
                value: {
                    labels: {}
                }
            },
            systemLoginDetailsEmailsView: {
                value: {
                    labels: {}
                }
            },
            systemCustomMenuLinksConfigView: {
                value: {
                    labels: {}
                }
            },
            systemConfigurationPropertiesDisplayView: {
                value: {
                    labels: {}
                }
            },
            systemEnvironmentDetailsView: {
                value: {
                    labels: {}
                }
            }
        }
    });

    H.registerHelper('increment', function(number, options) {

        if (typeof(number) === 'undefined' || number === null)
            return null;

        // Increment by inc parameter if it exists or just by one
        return number + (options.hash.inc || 1);
    });


}, '0.0.1', {
    requires: ['yui-base',
        'app-utils',
        'form-util',
        'model',
        'model-form-link',
        'model-transmogrify',
        'usp-security-SecurityConfiguration',
        'usp-security-UpdateSecurityConfigurationWithGeneralDetails',
        'usp-security-UpdateSecurityConfigurationWithForgottenPasswordEmailDetails',
        'usp-security-UpdateSecurityConfigurationWithTwoFactorSms',
        'usp-security-UpdateSecurityConfigurationWithEmailLoginDetails',
        'usp-security-UpdateSecurityConfigurationWithTermsAndConditions',
        'usp-custommenu-CustomMenuItem',
        'model-errors-plugin',
        'accordion-model-errors-plugin',
        'accordion-panel',
        'handlebars-base',
        'handlebars-helpers',
        'handlebars-systemConfig-templates',
        'event-custom',
        'app-accordion-toggle-all',
        'escape',
        'paginated-results-table',
        'results-formatters',
        'results-templates',
        'results-table-keyboard-nav-plugin',
        'info-message',
        'json-parse',
        'tabview'
    ]
});