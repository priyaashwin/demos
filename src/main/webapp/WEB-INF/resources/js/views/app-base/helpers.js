


Y.add('app-helpers', function (Y) {
	
	var H = Y.Handlebars;
	
	
	/**
	 * @method setChecked
	 * @description Checks a CheckBox/Radio if given input value matches currentValue
	 */
	H.registerHelper("setChecked", function (value, currentValue) {
	    return (value===currentValue) ? "checked" : ""; 
	});

	/**
	 * @method setDisabled
	 * @description Sets the element to disabled if true
	 */
	H.registerHelper("setDisabled", function (o, value) {
	    return (o && o[value]===true) ? "disabled=disabled" : ""; 
	});
	
	
}, '0.0.1', {
    requires: [
      'yui-base',
      'handlebars-base', 	     	 
      'handlebars-helpers',

    ]
});

