

YUI.add('app-mixin-multi-view', function(Y) {

	var A=Y.Array, L=Y.Lang, O=Y.Object;
	
	
	function MultiView(config) {}

	
	MultiView.prototype = { 
								
		initializer: function(config) {
			
			this._childViews = [];
			
			this._$$addViews(config, this.getViews());

			Y.Do.after(this._$$render, this, 'render', this);
			Y.Do.after(this._$$destructor, this, 'destroy', this);
			 
		},

		
		/**
		 * @description Stub function
		 * @returns null
		 */
		getViews: function(){
			return null;
		},
			
		
		/**
		 * @method _$$addView
		 * @description
		 * @param cfg
		 * @param name
		 * @param values
		 * @private
		 */
		_$$addView: function(cfg, name, values) {
			
			var viewFn = values.type;

			name = values.name || name;
					
			//create new view, allowing events to bubble
			this[name] = new viewFn(Y.merge(values, cfg[values.src] || {}));			
			this[name].name = name;						
			this[name].addTarget(this);
			this[name]._$$cfg = cfg[values.src];
			
			this._childViews.push(this[name]);
			
		},
		
		/**
		 * @method _$$addViews
		 * @param views
		 * @param cfg
		 * @private
		 */
		_$$addViews: function(cfg, views) {
			
			if(Y.Lang.isArray(views)) {
				Y.Array.each(views, function(view) {
					this._$$addView(cfg, view.name, view);
				}, this);
			} else if(Y.Lang.isObject(views)) {
				O.each(views, function(view, name) {				
					if(L.isFunction(view.type)) {
						this._$$addView(cfg, name, view);
					}			
				}, this);
			}
			
		
		},
		
		
		/**
		 * @method _$$render
		 * @description AOP method called after render.
		 */
		_$$render: function() {
			
			var container = this.get('container'),
				origRetVal = Y.Do.originalRetVal;
			
			container.prepend(this._$$renderViews());
			
			return origRetVal;
			
		},
		
		
		/**
		 * @method _$$renderViews
		 * @returns contentNode
		 */
		_$$renderViews: function() {
			
			var contentNode = Y.one(Y.config.doc.createDocumentFragment());

			A.each(this._childViews, function(view) {
				var container = this._$$renderView(view);
				container && contentNode.append(container);
			}, this);	
			
			return contentNode;
		
		},
		
		
		
		/**
		 * @method _$$renderView
		 * @description Renders child view.
		 * @param view
		 * @returns null || container
		 * 	if not found in doc or successfully appended then return null
		 * 	else return container
		 */
		_$$renderView: function(view) {
			
			var container = this[view.name].render().get('container'),
				hook = this.get('container').one(view.get('hook'));

			if(hook) {
				hook.setHTML(container);
				view.fire('attached', { name: view.name });
				return null;
			} else if(container.inDoc()) {
				return null;
			} else {
				return container;
			}
			
		},
		
		
		

		
		
		/**
		 * @method _$$destructor
		 * @description AOP method to call private destroy on childViews
		 */
		_$$destructor: function() {
	   			
			if(this._childViews) {        
				A.each(this._childViews, function(item) {         
					item.destroy();   
				});        
				this._childViews = null;
			}
    
		}

	};

	Y.namespace('app.mixin').MultiView = MultiView;

	
}, '0.0.1', {
    requires: ['yui-base',
               'event-custom-base',
               'data-binding'
               ]
});
