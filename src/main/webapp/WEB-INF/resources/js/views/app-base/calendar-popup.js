
YUI.add('app-calendar-popup', function (Y) {


	/** 
	 * Y.app.CalenderPopup
	 * ========================
	 * Extends Y.usp.CalendarPopup
	 * 
	 * We need the start date in Add Relationship to have an enabled/disabled state. The available disable and enable
	 * methods in CalendarPopup only effect the input node, not the date-picker button which remains click-able even 
	 * if input has been disabled.
	 *  
	 * _handleButtonClick 	- method overrides Y.usp.CalendarPopup method to not do anything if 'buttonDisabled' is true
	 * enableButton 		- removes CSS disabled class and sets a 'buttonDisabled' state to true
	 * disableButton 		- adds CSS disabled class and sets a 'buttonDisabled' state to false
	 * getButton			- helper to return the button node
	 * 
	 */
	
	var CalendarPopup = Y.Base.create('calendarPopup', Y.usp.CalendarPopup, [], { 
		_handleButtonClick: function (e) {
            e.preventDefault();
            var instance = this;
            
            if(instance.get('buttonDisabled')) {
               return;
            }
            
            if (instance.get('visible') === true) {
            	instance.hide();
            } else {
            	instance.show();
            }           
        },
        getButton: function() {
        	return this._calendarButton.one(this.get('SELECTOR_CAL_BUTTON'));
        },
        enableButton: function() {
        	this.set('buttonDisabled', false);
        	this.getButton().removeClass(this.get('CSS_BUTTON_DISABLED'));
        },
        disableButton: function() {
        	this.set('buttonDisabled', true);
        	this.getButton().addClass(this.get('CSS_BUTTON_DISABLED'));
        }
	},{
        ATTRS: {
        	SELECTOR_CAL_BUTTON: {
        		value: '.pure-button-cal'
        	},
        	CSS_BUTTON_DISABLED: {
        		value: 'pure-button-disabled'
        	},
        	buttonDisabled: {
        		value: false
        	}
        }
	});
	
	
	
	Y.namespace('app').CalendarPopup = CalendarPopup;
	
	
	
	
}, '0.0.1', {
	  requires: [
	    'yui-base',
	    'calendar-popup'
	  ]
});
	