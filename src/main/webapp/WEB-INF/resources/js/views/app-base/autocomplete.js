
YUI.add('app-views-autocomplete', function (Y) {

    var O = Y.Object, Q = Y.QueryString,
    
    DEFAULT_MAX_RESULT = 7,
    
    CSS_AC_INPUT = 'yui3-skin-sam',

    
    BaseView = Y.Base.create('autocomplete', Y.View, [], {
	
    	initializer: function(config) {			
    		this.template = config.template || Y.Handlebars.templates.baseAC;
		},
		
		destructor: function() {
			if(this.acWidget) {
				this.acWidget.unplug(Y.Plugin.usp.PopupAutoCompletePlugin);
			}
		},
 
    	render: function() {	
				
			var container = this.get('container');
		 											
			container.setHTML(this.template({					
				labels: this.get('labels')
			}));
			
			this.acWidget = this.initAC(container.one('input'));

			return this;
		
		},
		
		
		getConfig: function() {
			return {};
		},
		
		
		getParams: function(o) {
			var parsed = {};
			
			if(!o) {
				return '';
			} 
			
			O.each(o, function(v,k){					
				if(v && (typeof(v)==="number" || v.length>0)){						
					parsed[k]=v;					
				}				
			});

			return Q.stringify(parsed);	
		},
		
		
		initAC: function(node) {
			
			if(!node) {
				return null;
			}

			node.plug(Y.Plugin.usp.PopupAutoCompletePlugin, this.getConfig());
			node.ancestor('div').addClass(this.get('acClass')); 
			node.ac.on('select', this.onSelect, this);
			node.ac.on('results', this.onResults, this);
			
			return node;
		
		},
		
		
		resetAC: function() {
			this.acWidget.set('value', '');
			this.set('selected', null);
		},
					
				
		onSelect: function(e) {
			this.set('selected', e.result.raw.id);			
		},
		
		
		onResults: function(e) {
			return;			
		}
		

	},{ 
		ATTRS: {

			acClass: {
				value: CSS_AC_INPUT
			},
			
			labels: {
				value: {
					label: '',
					placeholder: ''
				}
			},

			maxResults: {
				value: DEFAULT_MAX_RESULT
			},
	
			selected: {
				value: null,
				setter: function(val) {
					this.fire('selected', { 'selected': val});
					return val;
				}
			},
			
			url: {
				value: ''
			}
   		
		}
		
	});	

	

	Y.namespace('app.views').BaseAC = BaseView;
	

}, '0.0.1', {	
	requires: [
	  'yui-base', 
	  'event-custom',
	  'node', 
	  'querystring',
      'handlebars-base',
      'handlebars-helpers',
      'handlebars-person-templates',
	  'popup-autocomplete-plugin', 
	  'autocomplete-filters', 
	  'autocomplete-highlighters'
    ]
});