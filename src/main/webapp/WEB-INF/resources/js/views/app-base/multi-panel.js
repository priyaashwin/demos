



YUI.add('app-multi-panel', function (Y) {
	
	var DIALOG_HEADER = '<h3 tabindex="0" class="iconic-title"><span class="fa-stack">\
		<i class="fa fa-circle fa-stack-1x"></i><i class="fa {icon} fa-inverse fa-stack-1x"></i>\
		</span>{title}</h3>';


	var BaseMultiPanel = Y.Base.create('baseMultiPanel', Y.usp.MultiPanelPopUp, [], {
		
		initializer: function(config) {			
			this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
		},
		
		destructor: function() {
			this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
		}
	
	}, {
		

		ATTRS: {

			buttons: {
				value: ['close' ]
			},
			
			icon: {
				value: ''
			},
			
			width: {
				value: 560
			}			
		
		}
	
	});

	
	Y.namespace('app').BaseMultiPanel = BaseMultiPanel;	
	
	Y.namespace('app').AppStdDialog = {
		BTN_LABEL_CANCEL: '<i class="fa fa-times"></i> Cancel',
		BTN_LABEL_EDIT: '<i class="fa fa-pencil-square-o"></i> Edit',
		BTN_LABEL_SAVE: '<i class="fa fa-check"></i> Save',
		BTN_LABEL_NO: '<i class="fa fa-times"></i> No',
		BTN_LABEL_YES: '<i class="fa fa-check"></i> Yes',
		BTN_LABEL_PROCEED: '<i class="fa fa-check"></i> Proceed',
		DIALOG_HEADER: DIALOG_HEADER
	};
	

}, '0.0.1', {
	
	requires: [
	   'yui-base',
	   'event-custom',
	   'form-util',	         
	   'multi-panel-popup',
	   'multi-panel-model-errors-plugin'
    ]
});