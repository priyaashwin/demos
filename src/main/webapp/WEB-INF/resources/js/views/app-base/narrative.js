

YUI.add('app-views-narrative', function (Y) {
		
	'use-strict';
	
	var L = Y.Lang,

	HTML_NARRATIVE = Y.Handlebars.templates.narrative,
		
					
	/**
	 * @class BaseNarrative
	 */
	BaseNarrative = Y.Base.create('baseNarrative', Y.View, [], {

		initializer: function(config){					
			
			if(config.templateName) {
				this.template = Y.Handlebars.templates[config.templateName];
			} else {
				this.template = HTML_NARRATIVE;	
			}
		},
					
		render: function(){																	
			
			this.get('container').setHTML(this.template({
				modelData: Y.merge(this.get('narrative'), this.get('subject')),
			})).addClass(this.get('narrativeClass'));
			
			return this;			
		
		}

	}, { 
		ATTRS: {
	
			/**
			 * @attribute narrative
			 * @description 
			 */
			narrative: {								
				setter: function(val) {
					val = L.isString(val)? { summary: val } : val;
					return val;
				},
				value: {
					summary: '',
					description: ''
				}			
			},
			
			/**
			 * @attribute narrativeClass
			 * @description Adds class to the narrative container
			 * 	e.g. Narratives in dialogs outside a form may need 'non-form-narrative' added
			 * 	to create a 1em top-margin.
			 */
			narrativeClass: {
				value: ''
			},
			
			/**
			 * @attribute subject
			 * @description 
			 */
			subject: {
				value: {
					id: '',
					name: ''										
				}
			},
			
			/**
			 * @attribute subjectType
			 * @description Could be person, group, organisation (or team),
			 */
			subjectType: {
				value: ''
			}	

		}
	
	});

	
	
	Y.namespace('app.views').BaseNarrative = BaseNarrative;

	
	
}, '0.0.1', {
	    
	requires: [
       'yui-base',
       'event-custom',
       'view',
       'handlebars-base',
       'handlebars-relationship-templates'
       ]
});