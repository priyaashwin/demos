YUI.add('document-controller-view', function (Y) {

    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.document').DocumentControllerView = Y.Base.create('documentControllerView', Y.View, [], {
        events: {
            '.documentView > div': {
                'downloadDocument': 'handleDownloadDocument'
            },
            '.pure-table': {
                'prepareIndividualDocumentRequestForRemove': 'handlePrepareRemoveIndividualDocument'
            }
        },
        initializer: function (config) {
            this.documentView = new Y.app.document.DocumentView({
                labels: config.labels,
                permissions: config.permissions,
                codedEntries: config.codedEntries,
                enumTypes: config.enums,
                resultsFilterConfig: config.resultsFilterConfig,
                properties: {
                    subjectId: config.subject.subjectId,
                    userId: config.user.userId,
                    urls: config.urls,
                    componentType: uspDocument.DocumentView,
                }
            }).addTarget(this);

            this.individualOutputRemoveController = new Y.app.remove.RemoveControllerView(config.removeDialogConfig).render();
            this.outputRemoveController = new Y.app.remove.RemoveControllerView(config.removeAllDialogConfig).render();

            this._evtHandlers = [
                Y.on('deletion:removeSaved', function () {
                    //refresh the view after deletion
                    this.documentView.render();
                }, this)
            ];
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(this.documentView.render().get('container'));
            this.get('container').append(contentNode);
            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.documentView.removeTarget(this);
            this.documentView.destroy;
            delete this.documentView;
        },
        handleDownloadDocument: function (e) {
            var attachmentId = e._event.detail.attachmentId;
            var documentType = e._event.detail.documentType;

            var url = this.get('urls').outputsURLS.downloadDocumentURL;
            downloadURL = L.sub((url), {
                attachmentId: attachmentId,
                documentType: documentType
            });

            // attempt the download
            Y.later(500, this, function () {
                window.location.href = downloadURL;
            });
        },
        handlePrepareRemoveIndividualDocument: function (e) {
            Y.fire('removeDialog:prepareForIndividualDocumentRemove', {
                id: e._event.detail.id
            });
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-toolbar',
        'document-view',
        'remove-controller-view'
    ]
});