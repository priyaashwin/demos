YUI.add('document-view', function (Y) {
    'use-strict';
    Y.namespace('app.document').DocumentView = Y.Base.create('documentView', Y.usp.ReactView, [], {
        reactFactoryType: uspDocument.DocumentView,
        events: {
            '.usp-accordion': {
                'prepareForRemove': '_handlePrepareForRemove'
            }
        },
        initializer: function () {
            this.filterButton = Y.one('.filter');
            this._evtHandlers = [
                this.filterButton.on('click', this._handleOnFilterButtonClicked, this),
            ];
            this.after('showFilterChange', this.render, this);
        },
        renderComponent: function () {
            var labels = this.get('labels'),
                permissions = this.get('permissions'),
                codedEntries = this.get('codedEntries'),
                enumTypes = this.get('enumTypes'),
                resultsFilterConfig = this.get('resultsFilterConfig'),
                properties = this.get('properties'),
                showFilter = this.get('showFilter');

            if (this.reactAppContainer) {
                ReactDOM.render(this.componentFactory({
                    labels: labels,
                    permissions: permissions,
                    codedEntries: codedEntries,
                    enumTypes: enumTypes,
                    resultsFilterConfig: resultsFilterConfig,
                    subjectId: properties.subjectId,
                    userId: properties.userId,
                    urls: properties.urls,
                    showFilter: showFilter,
                }), this.reactAppContainer.getDOMNode());
            }
        },
        _handleOnFilterButtonClicked: function () {
            this.set('showFilter', !this.get('showFilter'));
        },
        _handlePrepareForRemove: function () {
            Y.fire('removeDialog:prepareForDocumentsRemove');
        }
    }, {
        ATTRS: {
            labels: {
                value: null
            },
            permissions: {
                value: {}
            },
            enumTypes: {
                value: {}
            },
            resultsFilterConfig: {
                value: {}
            },
            properties: {
                value: {}
            },
            showFilter: {
                value: null
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'usp-view',
        'usp-react-view'
    ]
});
