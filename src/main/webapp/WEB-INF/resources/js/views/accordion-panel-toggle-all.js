

YUI.add('app-accordion-toggle-all', function(Y) {

	'use-strict';
	
	var CSS_ACTIVE_CLASS = 'closed',
		CSS_INACTIVE_CLASS = '',
		TITLE_ALL_CLOSED = 'Open all accordions',
		TITLE_ALL_OPENED = 'Close all accordions';
    
	
	
	function AccordionToggleAllPlugin(config) {
        AccordionToggleAllPlugin.superclass.constructor.apply(this, arguments);
    }


    /**
     * @property NAME
     * @description accordionToggleAllPlugin
     * @type {String}
     * @static
     */         
    AccordionToggleAllPlugin.NAME = 'accordionToggleAllPlugin';


    /**
     * The accordionToggleAll instance will be placed on the View instance under
     * the accordionToggleAll namespace. It can be accessed via VIEW_NAME.accordionToggleAll;
     * @property NS
     * @type {String}
     * @static
     */
    AccordionToggleAllPlugin.NS = 'accordionToggleAll';


    /**
     * Static property used to define the default attribute
     * configuration for the AccordionToggleAllPlugin plugin.
     *
     * @property ATTRS
     * @type Object
     * @static
     */
    AccordionToggleAllPlugin.ATTRS = {};


    /* AccordionToggleAllPlugin extends the base Plugin.Base class */
    Y.extend(AccordionToggleAllPlugin, Y.Plugin.Base, {

        initializer: function(config) {
            
        	this.btnToggleAll = Y.View.NodeMap.getByNode(this.get('srcNode'));
        	
        	this._accordions = [];
        	this._evtHandlers = [];
            this._evtHandlers.push(this.btnToggleAll.after('isActiveChange', this._toggleAll, this));
            this._evtHandlers.push(this.after('allClosedChange', this._afterAllClosed, this));
            this._evtHandlers.push(this.after('allOpenedChange', this._afterAllOpened, this)); 
            
            if(this.get('autoDetect')) {
            	this._initAccordions();
            }
            
        },

        destructor: function() {         
            
            Y.Array.each(this._evtHandlers, function(item) {
            	item.detach();
            });
            
            this._accordions = null;
            this._evtHandlers = null;
        },
        
        
        /**
         * @method addAccordion
         * @description Allows addition of accordions and wires a 'closed' changed listener.  Method is useful for when 
         * 	there host has nested accordions, which autoDetect wouldn't find.
         * 
         * 	e.g. this.accordionToggleAll.addAccordion(this.myAccordion);
         * 	e.g. this.accordionToggleAll.addAccordion(this.myNestView.myOtherAccordion);
         * 
         */
        addAccordion: function(accordion) {
        	this._accordions.push(accordion);
        	this._evtHandlers.push(accordion.after('closedChange', this._afterClosedChange, this));
        	this.set('states' + '.' + accordion.name, accordion.get('closed'));
        },


        /**
         * @method _initAccordions
         * @description Builds an array of accordions if autoDetect is switched on.
         */
        _initAccordions: function() {

            var host = this.get('host');

            Y.Object.each(host, function(value, key) {
                if(value instanceof(Y.usp.AccordionPanel)) {
                    this.addAccordion(value);
                }
            }, this);
        },

        _toggleAll: function(e) {
            Y.Array.each(this._accordions, function(accordion) {
                accordion.set('closed', e.newVal);
            });
            
            if(e.newVal) {
            	this.btnToggleAll.get('model').setAttrs({
        			title: TITLE_ALL_CLOSED
        		});
            } else {
            	this.btnToggleAll.get('model').setAttrs({
        			title: TITLE_ALL_OPENED
        		});
            }
            
            this.btnToggleAll.render();

        },
        
        
        
        /**
         * @method _afterClosedChange
         * @description Watches individual accordions for open/close activity.  
         * 	If triggered calculates whether all accordions are open or closed setting the 'allClosed' or 'allOpened' attributes accordingly. 
         * 	(This would trigger '_afterAllClosed' or '_afterAllOpened'.)
         */
        
        _afterClosedChange: function(e) {
        	
        	var closed = 0;
        	var opened = 0;
        	
        	this.set('states' + '.' + e.target.name, e.newVal);

			Y.Object.each(this.get('states'), function(value, key) {
                value ? closed += 1 : opened += 1;
            }, this);
			
			(closed===this._accordions.length) ? this.set('allClosed', true) : this.set('allClosed', false);
			(opened===this._accordions.length) ? this.set('allOpened', true) : this.set('allOpened', false);

        },
        
        
        
        /**
         * @method _afterAllClosed
         * @description Tells the button to switch to a open all state by setting it's isActive to true and
         * 	amends it's title accordingly.
         */
        _afterAllClosed: function(e) {
        	if(e.newVal===true && !this.btnToggleAll.get('isActive')) {
        		
        		this.btnToggleAll.get('model').setAttrs({
        			cssActive: CSS_ACTIVE_CLASS,
        			title: TITLE_ALL_CLOSED
        		});
        		   		
        		this.btnToggleAll.setAttrs({
        			isActive: true
        		});

        	}
        },
        
        
        
        /**
         * @method _afterAllClosed
         * @description Tells the button to switch to a close all state by setting it's isActive to false and
         * 	amends it's title accordingly.
         */    
        _afterAllOpened: function(e) {
        	if(e.newVal===true && this.btnToggleAll.get('isActive')) {

        		this.btnToggleAll.get('model').setAttrs({
        			cssActive: CSS_INACTIVE_CLASS,
        			title: TITLE_ALL_OPENED
        		});
        		
        		this.btnToggleAll.setAttrs({
        			isActive: false
        		});

        	}        	
        }


    }, {
        
        ATTRS: {

        	/**
        	 * @attribute autoDetect
        	 * @description determines whether to automatically detect accordions in host's subviews.
        	 */
            autoDetect: {
            	value: true
            },
        	
        	
        	allClosed: {
            	value: null
            },
            
            allOpened: {
            	value: null 
            },
        	
        	
            
            /**
             * @attribute srcNode
             * @description Holds the id of the global toggle button, so it can reference using Y.View.NodeMap
             */
            srcNode: {
        		value: ''
        	},
        	
        	
        	/**
        	 * @attribute states
        	 * @description keeps a boolean value of all the accordion closed states (true) or open states (false) 
        	 */
            states: {
                value: {}
            }
            

            
        }

    });
    
    
    Y.namespace('Plugin.app').AccordionToggleAllPlugin = AccordionToggleAllPlugin;



}, '0.0.1', {
    requires: ['plugin', 'yui-base', 'view-node-map']
});