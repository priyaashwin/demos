YUI.add('aftercare-controller-view', function (Y) {

    'use-strict';

    Y.namespace('app.aftercare').AfterCareControllerView = Y.Base.create('aftercareControllerView', Y.View, [], {
        events: {
            '.after-care-records': {
                'endAfterCare': 'showAfterCareEndDialog',
                'addContact': 'showAfterCareAddContactDialog',
                'addProvision': 'showAfterCareProvisionAddDialog',
                'viewProvision': 'showAfterCareProvisionViewDialog',
                'editProvision': 'showAfterCareProvisionUpdateDialog'
            },
            '.aftercare': {
                'afterCareLoaded': 'handleAfterCareLoaded'
            }
        },
        initializer: function (config) {
            this.aftercareDialog = new Y.app.aftercare.AfterCareDialog(config.dialogConfig).addTarget(this).render();

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            this.toolbar.disableButton('addAfterCare');

            var codedEntries = {
                economicActivity: Y.uspCategory.careleaver.aftercareEconomicActivity.category,
                accommodation: Y.uspCategory.careleaver.aftercareAccommodationType.category,
                serviceEndReason: Y.uspCategory.careleaver.aftercareServiceEndReason.category
            };

            var resultsConfig = Y.merge({ searchConfig: config.resultsConfig }, { codedEntries: codedEntries });
            resultsConfig.searchConfig.codedEntries = codedEntries;

            this.aftercareView = new Y.app.aftercare.AfterCareView({
                labels: config.labels.viewAfterCare,
                subject: config.subject,
                url: config.resultsConfig.url,
                permissions: config.permissions,
                codedEntries: codedEntries,
                enums: {
                    afterCareProvisionType: Y.usp.careleaver.enum.AfterCareProvisionType.values
                }
            }).addTarget(this);

            this._evtHandlers = [
                this.on('*:addAfterCare', this.showAfterCareAddDialog, this),
                this.on('*:updateAfterCareProvision', this.showAfterCareProvisionUpdateDialog, this),
                this.on('*:saved', this.handleSave, this),
                this.on('endAfterCareView:dateSelected', this.handleDateSelection, this),
            ];
        },

        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(this.aftercareView.render().get('container'));
            this.get('container').setHTML(contentNode);
            return this;
        },

        handleAfterCareLoaded: function (e) {
            var latestAfterCare = e._event.detail.latestAfterCare || {};
            if (!latestAfterCare.hasOwnProperty('endOfServicesDate') || latestAfterCare.endOfServicesDate) {
                this.toolbar.enableButton('addAfterCare');
            } else {
                this.toolbar.disableButton('addAfterCare');
            }
        },

        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;

            this.aftercareDialog.removeTarget(this);
            this.aftercareDialog.destroy();
            delete this.aftercareDialog;

            this.aftercareView.removeTarget(this);
            this.aftercareView.destroy();
            delete this.aftercareView;
        },

        showAfterCareAddDialog: function (e) {
            var config = this.get('dialogConfig.views.add.config');
            this.aftercareDialog.showView('add', {
                model: new Y.app.aftercare.NewAfterCareForm({
                    url: config.url
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        showAfterCareEndDialog: function (e) {
            var config = this.get('dialogConfig.views.end.config');
            this.aftercareDialog.showView('end', {
                model: new Y.app.aftercare.EndAfterCareForm({
                    url: config.url
                }),
                otherData: {
                    aftercareId: e._event.detail.aftercareId,
                    subject: this.get('subject'),
                    afterCare: e._event.detail.aftercare
                }
            }, function () {
                this.getButton('endButton', Y.WidgetStdMod.FOOTER).set('disabled', true);
            });
        },

        showAfterCareAddContactDialog: function (e) {
            var config = this.get('dialogConfig.views.addContact.config');
            this.aftercareDialog.showView('addContact', {
                model: new Y.app.aftercare.NewAfterCareContactForm({
                    url: config.url
                }),
                otherData: {
                    aftercareId: e._event.detail.aftercareId,
                    currentAccommodation: e._event.detail.currentAccommodation,
                    currentEconomicActivity: e._event.detail.currentEconomicActivity,
                    subject: this.get('subject')
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        showAfterCareProvisionAddDialog: function (e) {
            var config = this.get('dialogConfig.views.addProvision.config');
            this.aftercareDialog.showView('addProvision', {
                model: new Y.app.aftercare.NewAfterCareProvisionForm({
                    url: config.url
                }),
                otherData: {
                    aftercareId: e._event.detail.aftercareId,
                    subject: this.get('subject')
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        showAfterCareProvisionViewDialog: function (e) {
            var config = this.get('dialogConfig.views.viewProvision.config');
            this.aftercareDialog.showView('viewProvision', {
                model: new Y.usp.careleaver.AfterCareProvision({
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.aftercareProvisionId
                    })
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, {
                    modelLoad: true
                }, function (view) {
                    if (!this.views.viewProvision.permissions.canEditProvision || view.get('model').get('endDate') !== null) {
                        this.removeButton('editButton', Y.WidgetStdMod.FOOTER);
                    } else {
                        this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    }
                });
        },

        showAfterCareProvisionUpdateDialog: function (e) {
            var config = this.get('dialogConfig.views.editProvision.config'),
                recordId = (e._event.detail) ?
                    e._event.detail.aftercareProvisionId :
                    e.aftercareProvisionId;

            this.aftercareDialog.showView('editProvision', {
                model: new Y.app.aftercare.UpdateAfterCareProvisionForm({
                    url: Y.Lang.sub(config.url, {
                        id: recordId
                    })
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, {
                    modelLoad: true
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
        },

        handleSave: function (e) {
            //refresh the view on adding a after care record or ending one
            this.aftercareView.refresh();
        },

        handleDateSelection: function (e) {
            this.aftercareDialog.getButton('endButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        }
    });
}, '0.0.1', {
        requires: [
            'yui-base',
            'view',
            'app-toolbar',
            'aftercare-dialog',
            'aftercare-view',
            'usp-careleaver-AfterCare',
            'careleaver-component-enumerations',
            'categories-careleaver-component-AftercareAccommodationType'
        ]
    });