YUI.add('aftercare-view', function(Y) {
    'use-strict';
    Y.namespace('app.aftercare').AfterCareView = Y.Base.create('aftercareView', Y.usp.ReactView, [], {
        reactFactoryType: uspAfterCare.AfterCareView,
        renderComponent: function () {
          var labels=this.get('labels'),
              subject=this.get('subject'),
              url=this.get('url'),
              permissions=this.get('permissions'),
              codedEntries=this.get('codedEntries'),
              enums=this.get('enums');

          if (this.reactAppContainer) {
             var component;

             ReactDOM.render(this.componentFactory({
                    labels: labels,
                    subject: subject,
                    url: url,
                    permissions: permissions,
                    codedEntries: codedEntries,
                    enums: enums
                }), this.reactAppContainer.getDOMNode(), function () {
                    component = this;
             });

             this.component = component;
          }
        },
        refresh: function() {
            if (this.component) {
                this.component.loadData();
            }
        }

    }, {
      ATTRS: {
        labels: {},
        subject: {},
        url: {},
        permissions: {},
        codedEntries: {}
      }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'usp-view',
        'usp-react-view'
    ]
});