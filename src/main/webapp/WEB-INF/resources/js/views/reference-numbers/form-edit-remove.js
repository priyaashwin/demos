

YUI.add('app-reference-number-edit-remove-form', function (Y) {
	
			
	var EditForm = Y.Base.create('editReferenceNumberForm', Y.usp.organisation.UpdateOrganisationReferenceNumberView,[], {
		events:{
			'a.removeLink': {click: '_removeReferenceNumber'}
		},
		
		template: Y.Handlebars.templates["referenceNumberEditForm"],

        render: function() {
			var container = this.get('container'),
			html = this.template({
				labels: this.get('labels'),
				modelData: this.get('model').toJSON(),
				targetName:this.get('contextName'),
				targetId:this.get('contextId'),
				narrative: this.get('narrative'),
				codedEntries:{
	    			referenceNumberType:Y.uspCategory.organisation.referenceNumberType.category.codedEntries 
	    		}
			});
			container.setHTML(html);
			return this;
        },
        
		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#'+inputId+'-input');
		},
		
		_removeReferenceNumber:function(e){
			e.preventDefault();
			e.target.setStyle("visibility","hidden");
			this.get('container').one('#referenceNumberLabel_'+e.target.getAttribute('data-remove-id')).addClass('disabled');
			var referenceNumberInput = this.get('container').one('#referenceNumber_'+e.target.getAttribute('data-remove-id'));
			referenceNumberInput.setAttribute("data-remove-num",true);
			referenceNumberInput.set("value","");
			referenceNumberInput.set("disabled",true);
		},
		
		},{
			
		    ATTRS: {

		    	contextId: {
		    		value: ''
		    	},
		    	
		    	contextName: {
		    		value: ''
		    	},
		    	
		    	contextType: {
		    		value: ''
		    	},
		    	
		    	contextPrefix: {
		    		getter: function() {
		    			var prefix = '';
		    			switch(this.get('contextType')) {
		    			case 'organisation':
		    				prefix='ORG';
		    				break;
		    			case 'person':
		    				prefix='PER';
		    				break;
		    			};			    		
		    			return prefix;
		    		}
		    	},
		    	
		    	
		    	contextNameID: {
		    		getter: function() {
		    			return Y.Lang.sub('{name} ({prefix}{id})', {		    						    				
		    				id: this.get('contextId'),
		    				name: this.get('contextName'),
		    				prefix: this.get('contextPrefix')
		    			});
		    					    			 
		    		}
		    	},
		    	
		    	narrative: {
		    		getter: function() {
		    			return {		    						    				
		    				summary: this.get('labels').narrativeSummary,
		    				description: this.get('contextNameID') 
		    			};
		    					    			 
		    		}
		    	},
		    	
		    	activeReferenceNumbers:{}		    	
		    }
	
		});	
	
	Y.namespace('app.referenceNumber').EditForm = EditForm;
		
		
	
}, '0.0.1', {
	  requires: ['yui-base',
	             'form-util',
	             'handlebars',
	             'handlebars-helpers',
	             'handlebars-organisation-templates', 
	             'handlebars-person-templates',
	             'usp-organisation-UpdateOrganisationReferenceNumber',
	             'event-custom']
	});
	
	
	

	
