

YUI.add('app-reference-number-add-form', function (Y) {
	
			
	var AddForm = Y.Base.create('addReferenceNumberForm', Y.usp.organisation.NewOrganisationView,[], {
		events : {
			'#referenceNumberAddForm' : {
				submit : '_doNothingOnSubmit'
			}
		},
		template: Y.Handlebars.templates["referenceNumberAddForm"],

        render: function() {
			var container = this.get('container'),
			html = this.template({
				labels: this.get('labels'),
				modelData: this.get('model').toJSON(),				
				narrative: this.get('narrative')
			});

			container.setHTML(html);
			this.initReferenceTypes();				
			return this;
        },
        _doNothingOnSubmit : function(e){
        	e.preventDefault();
        },
		getInput: function(inputId) {
			var container=this.get('container');
			return container.one('#'+inputId+'-input');
		},
		
		initReferenceTypes : function(){			
			var activeReferenceType =  Y.uspCategory.organisation.referenceNumberType.category.getActiveCodedEntries(),
			    i,j;
			// Add the code as a property in the coded entry, used to set the option value
			Y.Object.each(activeReferenceType, function(referenceNumberType, key){
				referenceNumberType.code = key;
			});
			// Array of coded entries to pass to Y.FUtil.setSelectOptions
			var activeReferenceTypeArray = Y.Object.values(activeReferenceType);
			
			//Match associated reference numbers with active reference types and filter types array.
						
			for(j=0; j<this.get('activeReferenceNumbers').length; j++){
				for(i=0; i<activeReferenceTypeArray.length; i++){	
					if(this.get('activeReferenceNumbers')[j]===activeReferenceTypeArray[i].code){						
						activeReferenceTypeArray.splice(i,1);
					}		
				}
			}
			
			Y.FUtil.setSelectOptions(this.getInput('type'), activeReferenceTypeArray, null, null, true, true, 'code');
		}
		},{
			
		    ATTRS: {

		    	contextId: {
		    		value: ''
		    	},
		    	
		    	contextName: {
		    		value: ''
		    	},
		    	
		    	contextType: {
		    		value: ''
		    	},
		    	
		    	contextPrefix: {
		    		getter: function() {
		    			var prefix = '';
		    			switch(this.get('contextType')) {
		    			case 'organisation':
		    				prefix='ORG';
		    				break;
		    			case 'person':
		    				prefix='PER';
		    				break;
		    			};			    		
		    			return prefix;
		    		}
		    	},
		    	
		    	
		    	contextNameID: {
		    		getter: function() {
		    			return Y.Lang.sub('{name} ({prefix}{id})', {		    						    				
		    				id: this.get('contextId'),
		    				name: this.get('contextName'),
		    				prefix: this.get('contextPrefix')
		    			});
		    					    			 
		    		}
		    	},
		    	
		    	narrative: {
		    		getter: function() {
		    			return {		    						    				
		    				summary: this.get('labels').narrativeSummary,
		    				description: this.get('contextNameID') 
		    			};
		    					    			 
		    		}
		    	},
		    	
		    	activeReferenceNumbers:{}		    	
		    }
	
		});	
	
	Y.namespace('app.referenceNumber').AddForm = AddForm;
		
		
	
}, '0.0.1', {
	  requires: ['yui-base',
	             'form-util',
	             'handlebars',
	             'handlebars-helpers',
	             'handlebars-organisation-templates', 
	             'handlebars-person-templates',
	             'event-custom']
	});
	
	
	

	
