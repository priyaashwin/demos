YUI.add('relationship-filter',function (Y) {
  var L = Y.Lang;
  
  Y.namespace('app.relationship').RelationshipFilterModel = Y.Base.create('relationshipFilterModel', Y.usp.ResultsFilterModel, [], {
    customValidation: function(attrs) {
      //enforce our validation first - if we succeed this will call the parent validate
      var errors={},
        dateFrom=attrs['dateFrom'],
        dateTo=attrs['dateTo'];
      
      if(L.isNumber(dateFrom) && L.isNumber(dateTo)){
          if (dateFrom>dateTo) {
              errors['dateTo']=this.labels.validation.dateTo;
          }
      }
      
      return errors;
    }
  },{
      ATTRS: {
          relationshipTypes:{
              USPType: 'String',
              setter: Y.usp.ResultsFilterModel.setListValue,
              getter: Y.usp.ResultsFilterModel.getListValue,
              value:[]
          },
          /**
           * This stores the actual selected temporalStatusFilter value which boils down to a single value
           */
          temporalStatusFilter:{
            value:'',
            setter:function(val){
              var status=[];
              switch(val){
                case 'ALL':
                  status=['ACTIVE','INACTIVE_HISTORIC','INACTIVE_PENDING'];
                 break;
                case 'ACTIVE':
                  status=['ACTIVE'];
                break;
                case 'ACTIVE_OR_INACTIVE_HISTORIC':
                  status=['ACTIVE', 'INACTIVE_HISTORIC'];
                break;
                case 'ACTIVE_OR_INACTIVE_PENDING':
                  status=['ACTIVE', 'INACTIVE_PENDING'];
                break;
                case 'INACTIVE_HISTORIC':
                  status=['INACTIVE_HISTORIC'];
                break;
                case 'INACTIVE_HISTORIC_OR_INACTIVE_PENDING':
                  status=['INACTIVE_HISTORIC', 'INACTIVE_PENDING'];
                break;
              }
              
              this.set('status', status);
              return val;
            },
            getter:function(val){
              var status=this.get('status')||[];
              
              var temporalStatusFilter='';
              if(status.length===3){
                temporalStatusFilter='ALL';
              }else{
                //look for ACTIVE
                if(status.find(function(v){
                  return v==='ACTIVE';
                })){
                  if(status.length===1){
                    temporalStatusFilter='ACTIVE';
                  }else{
                    if(status.find(function(v){
                      return v==='INACTIVE_HISTORIC';
                    })){
                      temporalStatusFilter='ACTIVE_OR_INACTIVE_HISTORIC';
                    }else if(status.find(function(v){
                        return v==='INACTIVE_PENDING';
                      })){
                      temporalStatusFilter='ACTIVE_OR_INACTIVE_PENDING';
                    }
                  }
                  //look for INACTIVE_HISTORIC
                }else if(status.find(function(v){
                  return v==='INACTIVE_HISTORIC';
                })){
                  if(status.length===1){
                    temporalStatusFilter='INACTIVE_HISTORIC';
                  }else if(status.find(function(v){
                    return v==='INACTIVE_PENDING';
                  })){
                    temporalStatusFilter='INACTIVE_HISTORIC_OR_INACTIVE_PENDING';
                  }
                }else if(status.find(function(v){
                  return v==='INACTIVE_PENDING';
                })){
                  temporalStatusFilter='INACTIVE_PENDING';
                }
              }
              return temporalStatusFilter;
            }
          },
          status: {
            USPType: 'String',
            setter: function(val){
              var newVal=Y.usp.ResultsFilterModel.setListValue.call(this, val)||[];
              var isDate=(this.get('dateFrom')||this.get('dateTo'));
              
              if(newVal && newVal.length>0 && isDate){
                //only one of state or date can be active - clear the dates
                this.reset('dateFrom');
                this.reset('dateTo');
              }
              return newVal;
            },
            getter: Y.usp.ResultsFilterModel.getListValue,
            value: []
          },
          dateFrom:{
            USPType: 'Date',
            value: null,
            setter: function(val){
              var newVal=Y.usp.ResultsFilterModel.setDateFrom.call(this, val);
              var statusList=this.get('status');
              if(newVal && statusList && statusList.length>0){
                //only one of state or date can be active - clear the state
                this.reset('status');
              }
              
              return newVal;
            },
            getter: Y.usp.ResultsFilterModel.getDateFrom
          },
          dateTo:{
              USPType: 'Date',
              value: null,
              setter: function(val){
                var newVal=Y.usp.ResultsFilterModel.setDateTo.call(this, val);
                var statusList=this.get('status');
                if(newVal && statusList && statusList.length>0){
                  //only one of state or date can be active - clear the state
                  this.reset('status');
                }
                
                return newVal;
              },
              getter: Y.usp.ResultsFilterModel.getDateTo
          }
      }
  });
  
  Y.namespace('app.relationship').RelationshipResultsFilter = Y.Base.create('relationshipResultsFilter', Y.usp.app.ResultsFilter, [], {
    template:Y.Handlebars.templates['relationshipFilter'],
    initializer: function() {
        //initialise our date calendars
        this.dateFromCalendar = new Y.usp.CalendarPopup();
        this.dateToCalendar = new Y.usp.CalendarPopup();
        
        //set as bubble targets
        this.dateFromCalendar.addTarget(this);
        this.dateToCalendar.addTarget(this);
    },    
    render: function() {
        //get the container 
        var instance=this,
            container = instance.get('container');
        
        //call into superclass to render
        Y.app.relationship.RelationshipResultsFilter.superclass.render.call(this);
        
        //set the input node for the calendar
        this.dateFromCalendar.set('inputNode', container.one('#relationshipResultsFilter_dateFrom'));
        this.dateToCalendar.set('inputNode', container.one('#relationshipResultsFilter_dateTo'));
                    
        this.dateFromCalendar.render();
        this.dateToCalendar.render();
        
        //add class name to container to allow us to identify filter
        container.addClass(this.name);
 
        return this;
    },  
    destructor: function() {
        //destroy the calendars
        this.dateFromCalendar.destroy();
        delete this.dateFromCalendar;
        
        this.dateToCalendar.destroy();
        delete this.dateToCalendar;
    }
  }, {
      // Specify attributes and static properties for your View here.
      ATTRS: {    
          model: {},
          filterConfig: {}
      }
  });
  
  
  Y.namespace('app.relationship').RelationshipFilterContext=Y.Base.create('relationshipFilterContext', Y.usp.app.FilterContext, [], {
    template: Y.Handlebars.templates["relationshipActiveFilter"],
    blacklist:['showSecondary'],
    events:{
      '.filter-message a.close':{
        click:'hideMessage'
      }
    },
    hideMessage:function(e){
      e.preventDefault();
      this.get('container').all('div.filter-message').hide('fadeOut');
    }
  });
  
  Y.namespace('app.relationship').RelationshipFilter = Y.Base.create('relationshipFilter', Y.usp.app.Filter, [], {
    filterModelType: Y.app.relationship.RelationshipFilterModel,
    filterContextType: Y.app.relationship.RelationshipFilterContext,
    filterType:Y.app.relationship.RelationshipResultsFilter
  });
  
}, '0.0.1', {
    requires: ['yui-base',
               'calendar-popup',
               'results-filter',
               'app-filter',
               'handlebars-helpers',
               'handlebars-relationship-templates']
});