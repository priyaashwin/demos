YUI.add('relationships-person-organisation-forms', function (Y) {

    'use strict';

    var A = Y.Array,
        E = Y.Escape,
        O = Y.Object,
        L = Y.Lang,

        codes = {
            closeReasonsPerOrg: Y.uspCategory.relationship.personOrganisationRelationshipClosureReason.category.getActiveCodedEntries()
        };

    var AddForm = Y.Base.create('relationshipOrganisationAddForm', Y.View, [Y.app.mixin.MultiView], {

        events: {
            '#relatedOrganisationList li a': { click: '_syncEntity' },
            'input[name="personOrganisationRelationshipType"]': { click: '_handlePerOrgRelTypeClick' }
        },

        initializer: function (config) {
            this.template = config.template || Y.Handlebars.templates.relationshipOrganisationAdd;
            this.allowedAttributes = config.allowedAttributes;
        },

        getViews: function (config) {

            return {
                attributesPanel: {
                    hook: '#attributes-panel-hook.organisation',
                    type: Y.app.relationship.AttributesPanel,
                    relationshipType: this.get('relationshipType'),
                    targetEntity: this.get('subject'),
                    //Adding an age attribute to subject in view.jsp does not seem to have any impact. Hence a new variable is used.
                    targetAge: this.get('otherData.age'),
                    isEdit: false,
                    allowedAttributes: this.allowedAttributes
                },
                narrativePanel: {
                    hook: '#narrative-panel.organisation',
                    type: Y.app.views.BaseNarrative,
                    narrative: this.get('labels.narrative'),
                    subject: this.get('subject')
                }
            };

        },

        render: function () {
            var con = this.get('container');
            con.setHTML(this.template({
                modelData: this.get('subject'),
                labels: this.get('labels'),
                isRelationshipType: this.get('isRelationshipType'),
                otherData: this.get('otherData')
            }));

            this.initAutoComplete();

            this.startDateCalendar = new Y.app.CalendarPopup({
                inputNode: this.getInput('startDate'),
                minimumDate: new Date(1900, 1, 1)
            });

            this.startDateCalendar.render();
            this.startDateCalendar.disableButton();

            if (this.get('isRelationshipType').ClientTeamRelationshipType) {
                con.one('#employeeOrOther').remove();
            }

            return this;
        },

        destructor: function () {
            //destroy autocomplete
            var relatedOrganisationSearch = this.getInput('relatedOrganisationSearch'),
                calNode = this.getInput('startDate');

            if (relatedOrganisationSearch) {
                relatedOrganisationSearch.unplug();
                relatedOrganisationSearch.destroy();
            }

            if (this.startDateCalendar) {
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }

            if (calNode) {
                calNode.destroy();
                calNode = null;
            }

        },

        getInput: function (inputId) {
            var container = this.get('container');
            return container.one('#relationship_' + inputId);
        },
        getLabel: function (inputId) {
            var container = this.get('container');
            return container.one('#relationship_' + inputId + '_label');
        },
        getWrapper: function (inputId) {
            var container = this.get('container');
            return container.one('#relationship_' + inputId + '_wrapper');
        },

        //TODO share across views.
        initAutoComplete: function () {
            var relatedOrganisationSearch = this.getInput('relatedOrganisationSearch'),
                isTeam = this.get('relationshipType') === 'ClientTeamRelationshipType' ? '&organisationTypes=TEAM' : '';

            // Adjust auto complete drop down according to the width of the input box 
            var inputWidth = '35em';

            /*
            // Mobile devices have input field hidden initially (Hence width is calculated as 0), therefore we fix 50% width for mobiles only
            if (inputWidth==0) {inputWidth="50%";} */

            var applicationMaxResults = 10;
            var ds = new Y.DataSource.IO({
                source: L.sub('/eclipse/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&byOrganisationType=true{isTeam}&nameOrOrgIdOrPrevName=', {
                    isTeam: isTeam
                }),
                //source:'/eclipse/rest/organisation?useSoundex=false&escapeSpecialCharacters=true&appendWildcard=true&nameOrId=', //smi&useSoundex=false&escapeSpecialCharacters=true&appendWildcard=true
                ioConfig: { headers: { 'Accept': "application/vnd.olmgroup-usp.organisation.OrganisationWithAddressAndPreviousNames+json" } }
            });

            ds.plug(Y.Plugin.DataSourceJSONSchema, {
                schema: {
                    resultListLocator: "results"
                }
            });

            var input = relatedOrganisationSearch.plug(Y.Plugin.usp.PopupAutoCompletePlugin, {
                allowBrowserAutocomplete: false,
                enableCache: false,
                maxResults: applicationMaxResults,
                minQueryLength: 1,
                width: inputWidth,
                resultListLocator: function (response) {
                    return response || [];
                },
                resultTextLocator: 'organisationIdentifier',
                requestTemplate: "{query}" + '&escapeSpecialCharacters=true&appendWildcard=true&pageSize=' + applicationMaxResults,

                source: ds,

                resultFormatter: function (query, results) {

                    return A.map(results, function (result) {
                        var location = (result.raw.address) ? result.raw.address.location || {} : {},
                            address = location.location || null;
                        var type = Y.uspCategory.organisation.organisationType.category.codedEntries[result.raw.type];
                        var subType = Y.uspCategory.organisation.organisationSubType.category.codedEntries[result.raw.subType];
                        var typeSubTypeField = type ? type.name + ((subType) ? ' - ' + subType.name : '') : '';

                        var rRow = '<div title="' + E.html(result.raw.name) + '">' +
                            '<div>' + '<span>' + E.html(result.raw.organisationIdentifier) + ' - ' + E.html(result.raw.name) + '</span>' + '</div>' +
                            ((address !== null) ? ('<div class="txt-color fl"><span> ' + E.html(address) + '</span></div>') : '') +
                            ((type !== null) ? ('<div class="txt-color fl"><span>' + typeSubTypeField + '</span></div>') : '') +
                            Y.app.search.organisation.RenderMatchingField(result.raw, query) +
                            '</div>';
                        return rRow;

                    });
                }
            });

            //add the skin css to the container
            input.ancestor('div').addClass("yui3-skin-sam");

            input.ac.on('select', function (e) {
                // Hmm not sure if this is the best way to check for unselectable class 

                var relatedOrganisationSearchNode = this.getInput('relatedOrganisationSearch'),
                    relatedOrganisationItemTemplate = '<li class="data-item" id="relatedOrganisation{organisationId}"><span>{name} ({organisationId})</span><a href="#none" class="remove small">Change</a></li>',
                    result = e.result,
                    rawResult = result.raw,
                    relatedOrganisationList = Y.one('#relatedOrganisationList');

                e.preventDefault();

                // Make a note of the gender type for any gendered roles to display
                this.setAttrs({
                    relatedOrganisationId: rawResult.id,
                    relatedOrganisationName: result.raw.name,
                    selectedOrganisation: rawResult.id,
                }, { silent: true });

                //clear value and hide
                relatedOrganisationSearchNode.set('value', '');
                relatedOrganisationSearchNode.select();
                relatedOrganisationSearchNode.ac.hide();
                relatedOrganisationSearch.hide();

                // for now we are only going to allow add one relationship at a time, 
                // so if a organisation has already been selected, then remove them
                relatedOrganisationList.one('ul').all('li').some(function (o) {
                    o.remove(true);
                });

                relatedOrganisationList.one('ul').append(L.sub(relatedOrganisationItemTemplate, {
                    organisationId: E.html(rawResult.organisationIdentifier),
                    name: E.html(result.raw.name)
                }));

                this.getInput('startDate').removeAttribute('disabled');
                this.getInput('startDateEstimated').removeAttribute('disabled');
                this.getLabel('startDate').removeClass('label-disabled');
                this.startDateCalendar.enableButton();

                // update AttributesPanel with related organisation
                this.attributesPanel && this.attributesPanel.set('relatedEntity', {
                    entityType: 'organisation',
                    id: rawResult.id,
                    name: rawResult.name,
                    identifier: rawResult.organisationIdentifier
                });

                this.attributesPanel.enablePanel();

            }, this);

            // we check returned results dispalying an info message if none are returned 
            relatedOrganisationSearch.ac.on('results', function (e) {

                var selectedNode = this.get('container').one('#relatedOrganisationList');

                if (e.results.length === 0) {
                    this.getWrapper('noSearchResults').show();
                    selectedNode.hide();
                } else {
                    this.getWrapper('noSearchResults').hide();
                    selectedNode.show();
                }
            }, this);

        },
        _handlePerOrgRelTypeClick: function (e) {
            if (e.target.get('value').length) {
                Y.one('#attributes-panel-hook').hide(true);
            } else {
                Y.one('#attributes-panel-hook').show(true);
            }
        },

        _syncEntity: function (e) {

            e.preventDefault();
            e.currentTarget.ancestor('li').remove(true);

            this.initAutoComplete();

            this.setAttrs({
                relatedOrganisationId: '',
                relatedOrganisationName: '',
                selectedOrganisation: '',
            }, { silent: true });

            this.getInput('relatedOrganisationSearch').show();
            this.startDateCalendar.disableButton();
            this.getInput('startDate').set('value', '').setAttribute('disabled', 'disabled');
            this.getInput('startDateEstimated').set('checked', '').setAttribute('disabled', 'disabled');
            this.getLabel('startDate').addClass('label-disabled');

            this.attributesPanel && this.attributesPanel.set('relationshipRole', '');
            this.attributesPanel.disablePanel();
        }

    }, {

        ATTRS: {

            labels: {
                writeOnce: 'initOnly',
                value: {},
            },

            isRelationshipType: {
                getter: function () {
                    var val = {
                        OrganisationalRelationshipType: false,
                        TeamRelationshipType: false
                    };
                    val[this.get('relationshipType')] = true;
                    return val;
                }
            },

            relationshipType: {
                writeOnce: 'initOnly',
                value: null
            },

            /**
             * @attribute subject
             * @description subject is the targetRelation/targetEntity
             */
            subject: {
                writeOnce: 'initOnly',
                value: {},
                setter: function (val) {
                    this.set('relatedPersonId', val.id);
                    return;
                },
            }

        }

    });

    var EditForm = Y.Base.create('relationshipOrganisationEditForm', Y.View, [Y.app.mixin.MultiView], {

        events: {
            '#relationshipOrganisationEditForm': {
                submit: '_handleSubmit'
            }
        },

        initializer: function (config) {
            this.template = config.template || Y.Handlebars.templates.relationshipOrganisationEdit;
            this.get('model').after('load', this._syncPanel, this);
            this.allowedAttributes = config.allowedAttributes;
        },

        destructor: function () {

            if (this.startDateCalendar) {
                this.startDateCalendar.destroy();
                delete this.startDateCalendar;
            }

            if (this.startDateInput) {
                this.startDateInput.destroy();
                this.startDateInput = null;
            }
        },

        /**
         * @method getViews
         * @description Mixed in using MultiView
         * 	
         * Nothing is passed in, unlike in the Add forms.
         */
        getViews: function (config) {

            return {
                attributesPanel: {
                    hook: '#attributes-panel-hook.organisation',
                    type: Y.app.relationship.AttributesPanel,
                    targetEntity: this.get('subject'),
                    isEdit: true,
                    allowedAttributes: this.allowedAttributes
                }
            };
        },

        render: function () {
            var model = this.get('model');
            var con = this.get('container');
            var isEmployee = false;
            if (model.get('personOrganisationRelationshipTypeVO') && model.get('personOrganisationRelationshipTypeVO').relationshipClass === 'EMPLOYEREMPLOYEE') {
                isEmployee = true
            }
            con.setHTML(this.template({
                codedEntries: this.get('codedEntries'),
                labels: this.get('labels'),
                modelData: model.toJSON(),
                teamView: model.toJSON().organisationVO.organisationType === 'TEAM',
                isEmployee: isEmployee
            }));

            this._initCalender();

            if (model.get('organisationVO').organisationType !== 'TEAM') {
                con.one('#employeeOrOther').show();
                if (isEmployee) {
                    con.one('#attributes-panel-hook').hide(false);
                }
            }

            return this;

        },

        /**
         * @method _syncPanel
         * @description
         * 
         * 	This view inherits from Y.View because we want more control of the render.
         * 
         * 	After the render, but before we updatePanel, any views 
         * 	in getViews will be attached to the view.
         * 
         * 	This means the attributesPanel is attached displaying nothing because 
         * 	relationshipType determines which attributes should display.
         */
        _syncPanel: function () {

            var attrs = this.get('model').toJSON();

            this.set('relatedEntity', {
                id: attrs.organisationVO.id,
                name: attrs.organisationVO.name,
                identifier: attrs.organisationVO.organisationIdentifier,
                entityType: 'organisation'
            });

            this.set('targetEntity', Y.merge(this.get('targetEntity'), {
                id: attrs.personVO.id,
                name: attrs.personVO.name,
                identifier: attrs.personVO.personIdentifier,
                personTypes: attrs.personVO.personTypes,
                age: attrs.personVO.age,
                entityType: 'person'
            }));

            this.render();

            this.attributesPanel.updatePanel(Y.merge(attrs, {
                relationshipType: this.get('relationshipType'),
                relationshipRole: this.get('relationshipRole'),
                targetEntity: this.get('targetEntity'),
                relatedEntity: this.get('relatedEntity')
            }));

        },

        _initCalender: function () {

            this.startDateInput = this.get('container').one('#relationship_startDate');

            if (!this.startDateInput) {
                return;
            }

            this.startDateCalendar = new Y.app.CalendarPopup({
                inputNode: this.startDateInput,
                minimumDate: new Date(1900, 1, 1)
            }).render();
        },

        _handleSubmit: function (e) {
            e.preventDefault();
        },

    }, {

        ATTRS: {
            codedEntries: {
                value: {
                    organisationType: Y.uspCategory.organisation.organisationType.category.codedEntries,
                    organisationSubType: Y.uspCategory.organisation.organisationSubType.category.codedEntries
                }
            }

        }

    });

    var EndForm = Y.Base.create('endPersonOrganisationRelationshipView',
        Y.usp.relationship.UpdateClosePersonOrganisationRelationshipView, [], {

            events: {
                '#relationshipOrganisationCloseForm': {
                    submit: '_handleSubmit'
                }
            },

            initializer: function (config) {
                this.template = config.template || Y.Handlebars.templates["relationshipOrganisationCloseDialog"];
            },

            render: function () {
                var model = this.get('model');
                var con = this.get('container');
                var isEmployee = false;
                if (model.get('personOrganisationRelationshipTypeVO') && model.get('personOrganisationRelationshipTypeVO').relationshipClass === 'EMPLOYEREMPLOYEE') {
                    isEmployee = true
                }
                con.setHTML(this.template({
                    modelData: model.toJSON(),
                    labels: this.get('labels'),
                    endRelationship: this.get('endRelationship'),
                    codedEntries: this.get('codedEntries'),
                    teamView: model.toJSON().organisationVO.organisationType === 'TEAM',
                    isEmployee: isEmployee
                }));

                if (model.get('organisationVO').organisationType !== 'TEAM') {
                    con.one('#employeeOrOther').show();
                }
                this._initCloseReason();

                //call render on our calendar
                this.closeDateCalendar = new Y.app.CalendarPopup({
                    inputNode: this.getInput('closeDate'),
                    minimumDate: this.get('model').get('startDate')
                });

                this.closeDateCalendar.render();

                return this;

            },

            _handleSubmit: function (e) {
                e.preventDefault();
            },

            _initCloseReason: function () {
                var typeInput = this.get('container').one('#personOrganisationRelationship-input');

                typeInput && Y.FUtil.setSelectOptions(typeInput, O.values(codes.closeReasonsPerOrg), null, null, true, true, 'code');
            },

            destructor: function () {
                var calNode = this.getInput('closeDate');

                if (this.closeDateCalendar) {
                    this.closeDateCalendar.destroy();
                    delete this.closeDateCalendar;
                }

                if (calNode) {
                    calNode.destroy();
                    calNode = null;
                }
            },

            getInput: function (inputId) {
                var container = this.get('container');
                return container.one('#relationship_' + inputId);
            },

        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        organisationType: Y.uspCategory.organisation.organisationType.category.codedEntries,
                        organisationSubType: Y.uspCategory.organisation.organisationSubType.category.codedEntries,
                    }
                }
            }
        });

    var ViewForm = Y.Base.create('viewPersonOrganisationRelationshipView',
        Y.usp.relationship.PersonOrganisationRelationshipView, [], {

            initializer: function (config) {
                this.template = config.template || Y.Handlebars.templates["relationshipOrganisationDialog"];
            },

            render: function () {
                var model = this.get('model');
                var con = this.get('container');
                var isEmployee = false;
                if (model.get('personOrganisationRelationshipTypeVO') && model.get('personOrganisationRelationshipTypeVO').relationshipClass === 'EMPLOYEREMPLOYEE') {
                    isEmployee = true
                }
                con.setHTML(this.template({
                    modelData: model.toJSON(),
                    codedEntries: this.get('codedEntries'),
                    labels: this.get('labels'),
                    deleteRelationship: this.get('deleteRelationship'),
                    teamView: model.toJSON().organisationVO.organisationType === 'TEAM',
                    isEmployee: isEmployee
                }));

                if (model.get('organisationVO').organisationType !== 'TEAM') {
                    con.one('#employeeOrOther').show();
                }

                return this;

            }

        }, {
            ATTRS: {
                codedEntries: {
                    value: {
                        organisationType: Y.uspCategory.organisation.organisationType.category.codedEntries,
                        organisationSubType: Y.uspCategory.organisation.organisationSubType.category.codedEntries,
                        closeReason: Y.uspCategory.relationship.personOrganisationRelationshipClosureReason.category.codedEntries
                    }
                }
            }
        });

    Y.namespace('app.relationship').AddPersonOrganisationRelationshipView = AddForm;
    Y.namespace('app.relationship').EditPersonOrganisationRelationshipView = EditForm;
    Y.namespace('app.relationship').EndPersonOrganisationRelationshipView = EndForm;
    Y.namespace('app.relationship').ViewPersonOrganisationRelationshipView = ViewForm;

}, '0.0.1', {
    requires: ['yui-base',
        'popup-autocomplete-plugin',
        'datasource-io',
        'datasource-jsonschema',
        'form-util',
        'handlebars',
        'handlebars-helpers',
        'handlebars-relationship-templates',
        'usp-relationship-PersonOrganisationRelationship',
        'usp-relationship-NewPersonOrganisationRelationship',
        'usp-relationship-UpdateClosePersonOrganisationRelationship',
        'usp-relationship-UpdatePersonOrganisationRelationship',
        'categories-organisation-component-OrganisationType',
        'categories-organisation-component-OrganisationSubType',
        'categories-relationship-component-PersonOrganisationRelationshipClosureReason',
        'organisation-name-matcher-highlighter',
        'calendar-popup',
        'app-calendar-popup',
        'app-mixin-multi-view',
        'app-views-narrative',
        'relationships-attributes-panel'
    ]
});