YUI.add('relationship-person-organisation-results', function (Y) {
  'use strict';
    
  
  Y.Do.after(function(){
    var schema=Y.Do.originalRetVal;
    schema.resultFields.push({key:'organisationVO', locator:'organisationVO'});
    schema.resultFields.push({key:'personVO', locator:'personVO'});
    schema.resultFields.push({key:'personOrganisationRelationshipTypeVO', locator:'personOrganisationRelationshipTypeVO'});
    schema.resultFields.push({key:'organisationVO!id', locator:'organisationVO.id'});
    schema.resultFields.push({key:'organisationVO!name', locator:'organisationVO.name'});
    schema.resultFields.push({key:'organisationVO!type', locator:'organisationVO.type'});
    schema.resultFields.push({key:'organisationVO!subType', locator:'organisationVO.subType'});
    schema.resultFields.push({key:'organisationVO!organisationIdentifier', locator:'organisationVO.organisationIdentifier'});
    schema.resultFields.push({key:'personOrganisationRelationshipTypeVO!roleAName', locator:'personOrganisationRelationshipTypeVO!roleAName'});
  }, Y.usp.relationshipsrecording.PersonOrganisationRelationshipsResult, 'getSchema', Y.usp.relationshipsrecording.PersonOrganisationRelationshipsResult);
  
  var A = Y.Array, 
    L = Y.Lang,
    E = Y.Escape,
    USPFormatters = Y.usp.ColumnFormatters, 
    USPTemplates = Y.usp.ColumnTemplates,
    APPFormatters = Y.app.ColumnFormatters,
    RelationshipFormatters = Y.app.relationship.ColumnFormatters;
  
  Y.namespace('app.relationship').PersonOrganisationRelationshipResults = Y.Base.create('personOrganisationRelationshipResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      var subject = config.subject;
      
      return new Y.usp.relationshipsrecording.PaginatedPersonOrganisationRelationshipsResultList({
          url: L.sub(config.url, {
            id: subject.subjectId,
            subjectType: subject.subjectType
          })
      });
    },    
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
          permissions = config.permissions;
      return ([{
        key: 'organisationVO!organisationIdentifier',
        label: labels.id,
        width: '13%',
        clazz:'viewOrganisation',
        title:labels.viewOrganisation,
        formatter: APPFormatters.linkable
      }, {
          key: 'organisationVO!name',
          label: labels.name,
          sortable: true,
          allowHTML:true,
          width: '32%',
          clazz:'viewOrganisation',
          title:labels.viewOrganisation,
          formatter: APPFormatters.linkable,
          url:'#none'
      }, {       
          key: 'organisationVO!type',
          label: labels.relationship,
          width: '25%',
          allowHTML:true,
          cellTemplate:USPTemplates.clickable(labels.relationshipView, 'viewOrganisationRelationship'),
          formatter: RelationshipFormatters.formatIcons,
          allowedAttributes: config.allowedAttributes
      }, {
        key: 'startDate',
        label: labels.startDate,
        sortable: true,
        allowHTML:true,
        width: '11%',
        formatter: APPFormatters.estimatedDate, 
        estimatedKey: 'startDateEstimated'
      }, {
        key: 'closeDate',
        label: labels.closeDate,
        sortable: true,
        width: '11%',
        formatter: USPFormatters.date
      }, {
        key:'mobile-summary',
        label: 'Summary',
        className: 'mobile-summary pure-table-desktop-hidden',
        width: '0%',
        allowHTML:true,
        formatter:RelationshipFormatters.summaryPersonOrganisationFormat,
        cellTemplate:'<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
      }, {
          label: labels.actions,
          className: 'pure-table-actions',
          width: '8%',
          formatter: USPFormatters.actions,
          items: [
            {
              clazz: 'viewOrganisationRelationship',
              label: labels.view,
              title: labels.viewTitle,
              visible: permissions.canViewOrganisationRelationship
            },           
            {
              clazz: 'editOrganisationRelationship',
              label: labels.edit,
              title: labels.editTitle,
              visible:function(){
                if(this.record.get('organisationVO').organisationType==='TEAM'){
                  return permissions.canEditClientPersonTeamRelationship;
                } else {
                  return permissions.canEditOrganisationRelationship;
                }
              },
              enabled:function(){
                return (!(this.record.get('temporalStatus')==="INACTIVE_HISTORIC") && (this.record.get('personOrganisationRelationshipTypeVO')?(!(this.record.get('personOrganisationRelationshipTypeVO')._type === "ProfessionalTeamRelationshipType")) :true));
              }
            },
            {
              clazz: 'closeOrganisationRelationship',
              label: labels.close,
              title: labels.closeTitle,
              visible:function(){
                if(this.record.get('organisationVO').organisationType==='TEAM'){
                  return permissions.canEndClientPersonTeamRelationship;
                } else {
                  return permissions.canCloseOrganisationRelationship;
                }
              },
              enabled:function(){
                return (!(this.record.get('temporalStatus')==="INACTIVE_HISTORIC") && (this.record.get('personOrganisationRelationshipTypeVO')?(!(this.record.get('personOrganisationRelationshipTypeVO')._type === "ProfessionalTeamRelationshipType")) :true));
              }
            },
            {
              clazz: 'removeOrganisationRelationship',
              label: labels.remove,
              title: labels.removeTitle,
              visible:function(){
                if(this.record.get('organisationVO').organisationType==='TEAM'){
                  return permissions.canRemoveClientPersonTeamRelationship;
                } else {
                  return permissions.canRemoveOrganisationRelationship;
                }
              }
            }
          ]
      }]);
    }
  },{
    ATTRS:{
      resultsListPlugins: {
        valueFn: function() {
            return [{
                fn: Y.Plugin.usp.ResultsTableMenuPlugin
            }, {
                fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
            }, {
              fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
              cfg: {
                state: 'expanded',
                summaryFormatter:RelationshipFormatters.summaryPersonOrganisationFormat
              }
            }];
        }
      }
    }
  });
}, '0.0.1', {
    requires: ['yui-base',
               'event-custom',
               'app-results',
               'usp-relationship-PersonOrganisationRelationship',
               'usp-relationshipsrecording-PersonOrganisationRelationshipsResult',
               'caserecording-results-formatters',
               'relationship-results-formatters',
               'results-formatters',
               'results-templates',
               'results-table-summary-row-plugin'
               ]
});
