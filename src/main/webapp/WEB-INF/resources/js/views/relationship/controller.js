YUI.add('relationship-controller-view', function(Y) {
    'use-strict';

    var L = Y.Lang,
        O = Y.Object;
    
    var EVENT_REFRESH_RESULTS = 'relationship:dataChanged';
    
     Y.namespace('app.relationship').RelationshipTabView = Y.Base.create('relationshipTabView', Y.app.tab.FilteredTabsView, [], {
       views: {
        personRelationships: {
           type: Y.app.relationship.PersonPersonRelationshipFilteredResults
         },
         organisationRelationships: {
           type: Y.app.relationship.PersonOrganisationRelationshipResults
         }
       },
       tabs: {
         personRelationships: {},
         organisationRelationships: {}
       }
      });
    

    Y.namespace('app.relationship').RelationshipControllerView = Y.Base.create('relationshipControllerView', Y.View, [], {
      initializer: function(config) {
        this.toolbar = new Y.usp.app.AppToolbar({
            permissions: config.permissions
        }).addTarget(this);
        
        var personPersonConfig=config.personPersonConfig||{};
        var personOrganisationConfig=config.personOrganisationConfig||{};
        var personPersonResultsConfig = {
                searchConfig: Y.merge(personPersonConfig.searchConfig, {
                  //mix the subject into the search config so it is available when getting the URL
                  subject: config.subject
                }),
                filterConfig: personPersonConfig.filterConfig,
                filterContextConfig: personPersonConfig.filterContextConfig
        };
        
        var personOrganisationResultsConfig={
            searchConfig: Y.merge(personOrganisationConfig.searchConfig, {
              //mix the subject into the search config so it is available when getting the URL
              subject: config.subject
            })
        };
        
        var tabsConfig={
             personRelationships:Y.merge({
                 config:personPersonResultsConfig
               },config.tabs.personRelationships),
             organisationRelationships:Y.merge({
               config:personOrganisationResultsConfig
             },config.tabs.organisationRelationships)
        };
        
        this.relationshipTabs=new Y.app.relationship.RelationshipTabView({
          serverRouting:false,
          container:Y.one('<div/>'),
          tabs:tabsConfig,
          toolbarNode:this.toolbar.get('toolbarNode')
        }).addTarget(this);

        //configure the dialog for output template - no need to add as a bubble target
        this.downloadDialog = new Y.app.relationship.DownloadDialog(config.downloadDialogConfig, {
            //mix the subject into the search config so it is available when getting the URL
            subject: config.subject
        }).render();

        this._relationshipEvtHandlers = [
            this.on('*:addRelationship', this.showAddDialog, this),
            this.on('*:viewGraph', this.showGraph, this),
            this.on('*:viewList', this.showList, this),
            this.on('*:viewRelationships',this.viewPersonRelationships, this),
            this.on('*:viewOrganisation',this.viewOrganisation, this),
            this.on('*:viewPersonRelationship',this.showViewPersonRelationshipDialog, this),
            this.on('*:addPersonRelationship',this.showAddPersonRelationshipDialog, this),
            this.on('*:editPersonRelationship',this.showEditPersonRelationshipDialog, this),
            this.on('*:closePersonRelationship',this.showClosePersonRelationshipDialog, this),
            this.on('*:removePersonRelationship',this.showRemovePersonRelationshipDialog, this),
            this.on('*:viewOrganisationRelationship',this.showViewOrganisationRelationshipDialog, this),
            this.on('*:editOrganisationRelationship',this.showEditOrganisationRelationshipDialog, this),
            this.on('*:removeOrganisationRelationship',this.showRemoveOrganisationRelationshipDialog, this),
            this.on('*:closeOrganisationRelationship',this.showCloseOrganisationRelationshipDialog, this),
            this.on('*:generateGedcom', this.showGenerateGedcomDialog, this),
            this.on('relationshipTabView:activeViewChange', this._setFilterContextVisibility, this),
            this.after('relationshipTabView:activeViewChange', this._setToolbar, this),
            
            //reload results on save
            Y.on(EVENT_REFRESH_RESULTS, this.refreshResults, this)
        ];
      },      
      destructor: function() {
        this._relationshipEvtHandlers.forEach(function(handler) {
            handler.detach();
        });
        delete this._relationshipEvtHandlers;
        
        this.relationshipTabs.removeTarget(this);
        this.relationshipTabs.destroy();
        delete this.relationshipTabs;
        
        if (this.downloadDialog) {
          this.downloadDialog.destroy();
          delete this.downloadDialog;
        }

        this.toolbar.removeTarget(this);
        this.toolbar.destroy();
        delete this.toolbar;
      },
      refreshResults:function(e){        
        var tabIndex=e.tabIndex;
        if(tabIndex===1){
          if(this.relationshipTabs.get('activeTab')!=='organisationRelationships'){
            this.relationshipTabs.activateTab('organisationRelationships');
            //active tab - this will reload
            return;
          }
        }else{
          if(this.relationshipTabs.get('activeTab')!=='personRelationships'){
            //active tab - this will reload
            this.relationshipTabs.activateTab('personRelationships');
            return;
          }
        }
        //refresh current tab
        this.relationshipTabs.get('activeView').get('results').reload();       
      },
      showViewPersonRelationshipDialog:function(e){
        this._firePersonRelationshipDialog(e.record, 'view');
      },
      showEditPersonRelationshipDialog:function(e){
        var record=e.record;
        this._firePersonRelationshipDialog(record, 'edit', {
          targetEntity:this.get('subject'),
          relationshipType: record.get('typeDiscriminator')
        });
      },
      showRemovePersonRelationshipDialog:function(e){
        this._firePersonRelationshipDialog(e.record, 'delete');
      },
      showClosePersonRelationshipDialog:function(e){
        this._firePersonRelationshipDialog(e.record, 'close');
      },
      showViewOrganisationRelationshipDialog:function(e){
        var record=e.record;
        var permissions=this.get('permissions');
        this._fireOrganisationRelationshipDialog(record, 'viewPersonOrganisationRelationship', {
          canEditOrganisationRelationship : permissions.canEditOrganisationRelationship,
          canEditClientPersonTeamRelationship: permissions.canEditClientPersonTeamRelationship
        });
      },
      showEditOrganisationRelationshipDialog:function(e){
        var record=e.record;
        this._fireOrganisationRelationshipDialog(record, 'editPersonOrganisationRelationship');
      },
      showRemoveOrganisationRelationshipDialog:function(e){
          this._fireOrganisationRelationshipDialog(e.record, 'deletePersonOrganisationRelationship');
        },
      showCloseOrganisationRelationshipDialog:function(e){
        this._fireOrganisationRelationshipDialog(e.record, 'endPersonOrganisationRelationship');
      },
      
      showAddPersonRelationshipDialog:function(e){
        var record=e.record;
        var id = record.get('id'),
        relatedPersonId = record.get('personId'),
        personId = record.get('roleAPersonId')!==relatedPersonId?record.get('roleAPersonId'):record.get('roleBPersonId'),
        accessAllowed = record.get('accessAllowed');
        
        // If nominated person is a professional only
        // or the selected person is a professional only
        // we can skip relationship type dialog as only
        // a professional relationship can be added
        var subject=this.get('subject'),
            personTypes=subject.personTypes||[];

        
        if ((personTypes.indexOf('OTHER')<0 && personTypes.indexOf('CLIENT')<0) || (record.get('personTypes').indexOf('OTHER')<0 && record.get('personTypes').indexOf('CLIENT')<0)){
          Y.fire('relationship:showDialog',{
            targetPersonId: subject.subjectId,
            targetPersonName: subject.name,
            relatedPerson: record.toJSON(),
            relatedPersonRoles:record.get('personTypes'),
            action:'add',
            personRoles:personTypes,
            relationshipType:'ProfessionalRelationshipType',
            relationshipTypes: {
              FamilialRelationshipType:'familial',
              SocialRelationshipType:'social',
              ProfessionalRelationshipType:'professional'
            }
          });
        }else{
          Y.fire('relationship:showDialog', {
            action: 'addRelationshipType',
            personId: personId,
            relatedPerson: record.toJSON(),
            relatedPersonRoles:record.get('personTypes'),
            personRoles:personTypes,
            relatedPersonName: record.toJSON().name
          });
        }
      },
      showAddDialog:function(e){
        var subject=this.get('subject');
        
        //if on the organisation relationships tab - we pass a flag to control the selection
        var isOrganisationRelationship=this.relationshipTabs.get('activeTab')==='organisationRelationships';        

        Y.fire('relationship:showDialog',{
          action: 'addRelationshipType',
          isOrganisationRelationship:isOrganisationRelationship,
          personId: subject.subjectId,
          personName: subject.subjectName,
          personRoles: subject.personTypes,
          subject: subject,
          clientTeamId: this.get('client_team_id'),
          relationshipTypes: [{
            id:'FamilialRelationshipType',
            name:'Familial'
          },{
            id:'SocialRelationshipType',
            name:'Social'
          },{
            id:'ProfessionalRelationshipType',
            name:'Professional'
          }]
        });
      },
      _firePersonRelationshipDialog:function(record, action, properties){
        var id = record.get('id'),
        relatedPersonId = record.get('personId'),
        personId = record.get('roleAPersonId')!==relatedPersonId?record.get('roleAPersonId'):record.get('roleBPersonId'),
        accessAllowed = record.get('accessAllowed');
        
        //fire view relationship
        Y.fire('relationship:showDialog', Y.mix({
          id: id,
          personId: personId,
          action: action,
          accessAllowed: accessAllowed
        }, properties));
      },
      _fireOrganisationRelationshipDialog:function(record, action, properties){        
        var id = record.get('id');
        var organisationType = record.get('organisationVO').organisationType;
                  
        //fire view relationship
        Y.fire('personOrganisationRelationship:showDialog', Y.mix({
          id: id,
          organisationType: organisationType,
          action: action,
        }, properties));
      },      
      viewPersonRelationships:function(e){
        var record=e.record;
        var relatedPersonId = record.get('personId');
        var personId = record.get('roleAPersonId')!==relatedPersonId?record.get('roleAPersonId'):record.get('roleBPersonId');
        //TODO - this could just set the person subject
        //but for now we will re-direct
        window.location=L.sub(this.get('viewRelationshipsURL'),{personId:relatedPersonId});
      },
      viewOrganisation:function(e){
        var record=e.record;
        var organisation=record.get('organisationVO');
        
        window.location=L.sub(this.get('viewOrganisationURL'),{organisationId:organisation.id});
      },
      showGraph:function(){        
        var subject=this.get('subject');
        //show loading 
        window.location=L.sub(this.get('viewGraphURL'),{subjectId:subject.subjectId});
      },
      showList:function(){
        //reload - the header - fire with empty payload
        Y.fire('contentContext:update',{});
        //switch back to person tab - and reset the filter
        this.relationshipTabs.activateTab('personRelationships');
        
        var filter=this.relationshipTabs.get('activeView').get('filter');
        if(filter){
          //clear all filters
          filter.get('resultsFilter').clearAll()
        }
      },      
      showGenerateGedcomDialog:function(e){
        var subject=this.get('subject');
        var config=this.get('downloadDialogConfig');
        this.downloadDialog.showView('generateGedcom',{
          model: new Y.usp.relationship.NewGenerateGedcom({
            url:config.generateURL,
            personId:subject.subjectId
          }),
          otherData:{
            subjectName:subject.subjectName,
            subjectIdentifier:subject.subjectIdentifier
          }
        });
      },
      render:function(){
        this.get('container').setHTML(this.relationshipTabs.render().get('container'));
        
        if (this.relationshipTabs.hasRoute(this.relationshipTabs.getPath())) {
          this.relationshipTabs.dispatch();
        } else {
          this.relationshipTabs.activateTab('personRelationships');
        }
        return this;
      },
      _setFilterContextVisibility:function(e){
        var newView=e.newVal;
        var config=this.get('personPersonConfig')||{};
        var personPersonFilterContextConfig=config.filterContextConfig||{};
        if(personPersonFilterContextConfig.container){
          var contextNode=Y.one(personPersonFilterContextConfig.container);
          if(newView.name==='personPersonRelationshipFilteredResults'){
            //ensure the filterContext which could live outside the tabs (controlled via configuration) is visible
            contextNode.show('fadeIn');
          }else{
            contextNode.hide('fadeOut');
          }
        }
      },
      _setToolbar:function(e){
        var newView=e.newVal;
        
        switch(newView.name){
          case 'personPersonRelationshipFilteredResults':
            //enable list, graph, add, gedcom
            this.relationshipTabs.enableButton('viewList');
            this.relationshipTabs.enableButton('viewGraph');
            this.relationshipTabs.enableButton('addRelationship');
            this.relationshipTabs.enableButton('generateGedcom');
          break;
          case 'personOrganisationRelationshipResults':
            //enable list and add only
            this.relationshipTabs.enableButton('viewList');
            this.relationshipTabs.enableButton('addRelationship');
            this.relationshipTabs.disableButton('viewGraph');
            this.relationshipTabs.disableButton('generateGedcom');
            this.relationshipTabs.disableButton('filter');
          break;
        }
      }
    },{
      ATTRS:{
        personPersonConfig:{
          value:{
            searchConfig:{},
            filterConfig:{},
            filterContextConfig:{}
          }
        },
        personOrganisationConfig:{
          value:{
            searchConfig:{}
          }
        },
        permissions:{
          value:{}
        },
        subject:{},
        tabs:{},
        viewRelationshipsURL:{
          value:''
        }
      }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'app-toolbar',
               'relationship-person-person-results',
               'relationship-person-organisation-results',
               'usp-relationship-NewGenerateGedcom',
               'filtered-tabs-view',
               'relationship-download-dialog'
    ]
});