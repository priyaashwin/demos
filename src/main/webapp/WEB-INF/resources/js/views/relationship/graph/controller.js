YUI.add('relationship-graph-controller-view', function(Y) {
  'use-strict';

  var L = Y.Lang;
  var AUTO_GENERATED_ID = 'automaticRelationshipsList';

    Y.namespace('app.relationship').RelationshipGraphControllerView = Y.Base.create('relationshipGraphControllerView', Y.View, [], {
      initializer: function(config) {
        var graphConfig = config.graphConfig || {};

        this.toolbar = new Y.usp.app.AppToolbar({
          permissions: config.permissions
        }).addTarget(this);

        this.toolbar.get('toolbarNode').setStyle('position', 'relative').setStyle('z-index', '10');

        this.filter = new Y.app.relationship.RelationshipFilter({
          filterConfig: config.filterConfig,
          filterContextConfig: config.filterContextConfig
        }).addTarget(this);

        this.genogram = new Y.app.genogram.ControllerView(config.genogramConfig);

        var graphDataClass = Y.Base.create("graphData", Y.usp.relationships.RelationshipsModel, [], {
            url: graphConfig.url,
            getURL: function(action, options) {
                var url = this._substituteURL(this.url, Y.merge(this.getAttrs(), options));
                if (options.includeSuggestions) {
                    url += "?includeSuggestions=true" + "&" + Math.random();
                    if (options.suggestionsRelationshipId) {
                        url += "&suggestionsRelationshipId=" + options.suggestionsRelationshipId;
                    }
                } else {
                    url = url + '?' + Math.random();
                }
                return url;
            }
        });

        this.graphData = new graphDataClass();

        //holds ID's of automatically generated relationships.
        this.automaticData = [];

        var Storage = Y.uspSessionStorage;

        // check for automatically generated id's that may have been created via the list view
        if (Storage.hasStoreValue(AUTO_GENERATED_ID)) {
          try {
            this.automaticData = Y.JSON.parse(Storage.getStoreValue(AUTO_GENERATED_ID));
            Storage.removeStoreValue(AUTO_GENERATED_ID);
          } catch (e) {
            //ignored
          }
        }

        this.graph = this.setupGraph();

        this._relationshipEvtHandlers = [
          this.on('*:addRelationship', this.showAddDialog, this),
          this.on('*:viewList', this.showList, this),
          this.on('relationshipFilterModel:change', function() {
              //apply the filter change to the graph - async
              Y.later(50, this.graph, this.graph.filterGraph);
          }, this),
          Y.on('relationshipFilterModel:reset', function(){
            this.filter.get('resultsFilter').clearAll();
          }, this),
          Y.on('relationship:dataChanged', this.reload, this),
          Y.delegate('click', this.handleGraphToolbar, '#graphToolbar', 'li .pure-button', this),
          Y.delegate('click', this.handleToolbar, '#sectionToolbar', '.pure-button, li.pure-menu-item a', this)
        ];
      },
      destructor: function() {
        this._relationshipEvtHandlers.forEach(function(handler) {
            handler.detach();
        });
        delete this._relationshipEvtHandlers;

        this.toolbar.removeTarget(this);
        this.toolbar.destroy();
        delete this.toolbar;

        this.genogram.removeTarget(this);
        this.genogram.destroy();
        delete this.genogram;
      },
      render: function() {
        var container = this.get('container');
        var contentNode = Y.one(Y.config.doc.createDocumentFragment());
        var subject = this.get('subject');

        //Results filter
        contentNode.append(this.filter.render().get('container'));

        //finally set the content into the container
        container.setHTML(contentNode);

        this.graph.render();

        var query = Y.QueryString.parse(window.location.search.substr(1));
        if (query.suggestionsRelationshipId) {
          this.graph.setAttrs({
            suggestionsRelationshipId: query.suggestionsRelationshipId
          }, {silent: true });
          this.graph.load({
            personId: subject.subjectId,
            includeSuggestions: true,
            suggestionsRelationshipId: query.suggestionsRelationshipId
          });
        } else {
          this.graph.load({
            personId: subject.subjectId
          });
        }

        return this;
      },
      handleGraphToolbar: function(e) {
        var t = e.currentTarget;
        var filter = this.filter;
        var labels = this.get('graphConfig.labels');
        var toggleSecondaryFilter = function(toggle) {
            filter.get('filterModel').set('showSecondary', toggle);
        };

        e.preventDefault();

        if (t.hasClass('toggleSuggestions') && t.hasClass('inactive')) {
          t.removeClass('inactive').addClass('active').setAttribute('title', labels.hideSuggestions);
          Y.fire('relationship:dataChanged', {
            action: 'add',
            includePersonSuggestions: true
          });
        } else if (t.hasClass('toggleSuggestions') && t.hasClass('active')) {
          t.removeClass('active').addClass('inactive').setAttribute('title', labels.showSuggestions);
          Y.fire('relationship:dataChanged', {
              action: 'add',
          });

          // remove no suggestion messages.
          if (Y.one('#suggestionInfo')) {
              Y.one('#relationshipUserInfo h4').setHTML();
              Y.one('#relationshipUserInfo').hide('fadeOut');
          }
        } else if (t.hasClass('zoomIn')) {
          this.graph.zoomIn();
        } else if (t.hasClass('zoomOut')) {
          this.graph.zoomOut();
        } else if (t.hasClass('toggleKey') && t.hasClass('inactive')) {
          t.removeClass('inactive').addClass('active').setAttribute('title', labels.buttonHideKey);
          Y.one('html').addClass('key-visible');
          this.graph.showKey();
        } else if (t.hasClass('toggleKey') && t.hasClass('active')) {
          t.removeClass('active').addClass('inactive').setAttribute('title', labels.buttonShowKey);
          Y.one('html').removeClass('key-visible');
          this.graph.hideKey();
        } else if (t.hasClass('toggleSecondary') && t.hasClass('inactive')) {
          t.removeClass('inactive').addClass('active').setAttribute('title', labels.buttonHideSecondary);
          toggleSecondaryFilter(true);
        } else if (t.hasClass('toggleSecondary') && t.hasClass('active')) {
          t.removeClass('active').addClass('inactive').setAttribute('title', labels.buttonShowSecondary);
          toggleSecondaryFilter(false);
        } else if (t.hasClass('redraw')) {
          this.graph.redraw();
        }
      },
      handleToolbar: function(e) {
        var t = e.currentTarget;
        var permissions = this.get('permissions');

        if (t.hasClass('print')) {
          e.preventDefault();
          this.print();
        } else if (t.hasClass('swimlane')) {
          e.preventDefault();
          this.hideGenogram();
          this.showGraph();
          this.graph.redrawSwimlane();
        } else if (t.hasClass('hub_n_spoke')) {
          e.preventDefault();
          this.hideGenogram();
          this.showGraph();
          this.graph.redrawHubAndSpoke();
        } else if(t.hasClass('genogram') && permissions.canViewGenogram) {
          e.preventDefault();
          this.hideGraph();
          this.showGenogram();
        }
      },
      print: function() {
        var activeView = this.get('activeView');
        var permissions = this.get('permissions');
        if(activeView==='graph') {
          this.graph.print();
        } else if(activeView==='genogram' && permissions.canViewGenogram===true) {
          this.genogram.print();
        }
      },
      hideGenogram: function() {
        this.genogram.hide();
      },
      hideGraph: function() {
        this.graph.get('contentBox').setStyle('display', 'none');
      },
      showGenogram: function() {
        this.set('activeView', 'genogram');
        this.genogram.show();
      },
      showGraph: function() {
        this.set('activeView', 'graph');
        this.graph.get('contentBox').setStyle('display', 'block');
      },
      showList:function(){
        var subject=this.get('subject');
        window.location=L.sub(this.get('viewListUrl'),{
          subjectId:subject.subjectId,
          subjectType:subject.subjectType
        });
      },
      setupGraph: function() {
          var container = this.get('container'),
              permissions = this.get('permissions') || {},
              graphConfig = this.get('graphConfig') || {},
              labels = graphConfig.labels || {},
              subject = this.get('subject');

          var filterModel = this.filter.get('filterModel');
          var client_team_id = graphConfig.client_team_id;

          var graph = new Y.usp.Graph({
              contentBox: graphConfig.container,
              canView: permissions.canView,
              canAdd: permissions.canAdd,
              msgServerErrorLine1: labels.msgServerErrorLine1,
              msgServerErrorLine2: labels.msgServerErrorLine2,
              data: this.graphData,
              //edge visibility function
              edgeVisibilityFn: function(classifier, rootCell) {
                  var showEdge = true,
                      temporalStatus = this.value.temporalStatus,
                      startDate = this.value.startDate,
                      closeDate = this.value.closeDate,
                      isTypeSelected = (filterModel.get('relationshipTypes') || []).length > 0,
                      isStateSelected = (filterModel.get('status') || []).length > 0,
                      selectedStartDate = filterModel.get('dateFrom'),
                      selectedCloseDate = filterModel.get('dateTo');

                  var isDateSelected = selectedStartDate || selectedCloseDate;

                  var type = "",
                      classifierMatches = false;

                  if (this.source.id !== rootCell.id && classifier !== 'SUGGESTION') {
                      showEdge = filterModel.get('showSecondary') === true;
                  }

                  var relationshipTypes = filterModel.get('relationshipTypes') || [];

                  if (showEdge === true) {
                      switch (classifier) {
                          case "FamilialRelationshipTypeVO":
                              showEdge = relationshipTypes.indexOf('FamilialRelationshipType') !== -1;
                              type = "FamilialRelationshipTypeVO";
                              break;
                          case "SocialRelationshipTypeVO":
                              showEdge = relationshipTypes.indexOf('SocialRelationshipType') !== -1;
                              type = "SocialRelationshipTypeVO";
                              break;
                          case "ProfessionalRelationshipTypeVO":
                              showEdge = relationshipTypes.indexOf('ProfessionalRelationshipType') !== -1;
                              type = "ProfessionalRelationshipTypeVO";
                              break;

                              // Right now suggestions use the familial filter.
                          case "SUGGESTION":
                              showEdge = relationshipTypes.indexOf('FamilialRelationshipType') !== -1;
                              break;
                          default:
                              showEdge = false;
                      }
                      if (!isTypeSelected && !isStateSelected && !selectedStartDate && !selectedCloseDate) {
                          showEdge = true;
                      }
                  }

                  if (showEdge === true && classifier == type) {
                      classifierMatches = true;
                  }

                  if (isStateSelected && (classifierMatches || !isTypeSelected)) {
                      var status = filterModel.get('status') || [];
                      switch (temporalStatus) {
                          case "ACTIVE":
                              showEdge = status.indexOf('ACTIVE') !== -1;
                              break;

                          case "INACTIVE_HISTORIC":
                              showEdge = status.indexOf('INACTIVE_HISTORIC') !== -1;
                              break;

                          case "INACTIVE_PENDING":
                              showEdge = status.indexOf('INACTIVE_PENDING') !== -1;
                              break;
                          default:
                              showEdge = false;
                      }
                  }

                  if (isDateSelected && (classifierMatches || !isTypeSelected)) {
                      showEdge = true;
                      if (selectedCloseDate) {
                          if (startDate > selectedCloseDate) {
                              showEdge = false;
                          }
                      }
                      if (selectedStartDate) {
                          if (closeDate && (closeDate < selectedStartDate)) {
                              showEdge = false;
                          }
                      }
                  }

                  // TODO:
                  // This is a fix to stop secondary relationships from showing when
                  // they shouldn't.  ^^ This function is a bit more complicated than it
                  // should be and should probably be rewritten.
                  if (showEdge) {
                      // if this is a secondary relationship
                      if (this.source.id !== rootCell.id && classifier !== 'SUGGESTION') {
                          // if we're not showing secondary relationships
                          if (!filterModel.get('showSecondary')) {
                              // hide this relationship
                              showEdge = false;
                          }
                      }

                  }

                  return showEdge;
              },
              //vertex menu function
              vertexMenuCallbackFn: function(menu, value, isRoot) {

                  var instance = this;
                  if (permissions.canView) {

                      if (!isRoot) {
                          //cell must be of type PERSON and not root
                          if ("PERSON" === value._type) {
                              menu.addSeparator();
                              menu.addItem('switch focus to', null, function() {

                                  // ensure automatic relationships are cleared down - we don't want them to display when switching focus
                                  //Storage.removeStoreValue(AUTO_GENERATED_ID);
                                  instance.setAttrs({
                                      automatedData: []
                                  }, {
                                      silent: true
                                  });

                                  //set current subject
                                  Y.currentSubject.setCurrentSubject(value.id, 'person');

                                  window.location = L.sub(graphConfig.diagramUrl, {
                                      personId: value.id
                                  });
                              });
                          }
                      }
                  }


                  if (permissions.canAdd && value.accessAllowed) {
                      if ("PERSON" === value._type) {
                          //cell must be of type PERSON and not root
                          if (!isRoot) {
                              menu.addItem('add relationship', null, function() {
                                  var personTypes = subject.personTypes;
                                  var personName = subject.subjectName;
                                  if (
                                      (personTypes.indexOf('OTHER') < 0 && personTypes.indexOf('CLIENT') < 0) ||
                                      (value.personTypes.indexOf('OTHER') < 0 && value.personTypes.indexOf('CLIENT') < 0)
                                  ) {
                                      Y.fire('relationship:showDialog', {
                                          targetPersonId: graph.rootCellId,
                                          targetPersonName: personName,
                                          action: 'add',
                                          personRoles: personTypes,
                                          relatedPerson: {
                                              personPersonId: "PER" + value.id,
                                              relatedPersonId: value.id,
                                              surname: value.surname,
                                              forename: value.forename,
                                              name: value.name,
                                              gender: value.gender,
                                              relatedPersonDoB: value.dateOfBirth,
                                              relatedPersonDoBEstimated: value.dateOfBirthEstimated
                                          },
                                          relatedPersonRoles: value.personTypes,
                                          relationshipType: 'ProfessionalRelationshipType',
                                          relationshipTypes: {
                                              FamilialRelationshipType: 'familial',
                                              SocialRelationshipType: 'social',
                                              ProfessionalRelationshipType: 'professional'
                                          }
                                      });
                                  } else {

                                      Y.fire('relationship:showDialog', {
                                          action: 'addRelationshipType',
                                          relatedPersonId: value.id,
                                          relatedPersonDoB: value.dateOfBirth,
                                          relatedPersonDoBEstimated: value.dateOfBirthEstimated,
                                          personId: graph.rootCellId,
                                          personRoles: personTypes,
                                          targetPersonName: value.name,
                                          relatedPersonRoles: value.personTypes,
                                          relatedPerson: {
                                              personPersonId: "PER" + value.id,
                                              relatedPersonId: value.id,
                                              surname: value.surname,
                                              forename: value.forename,
                                              name: value.name,
                                              gender: value.gender,
                                              relatedPersonDoB: value.dateOfBirth,
                                              relatedPersonDoBEstimated: value.dateOfBirthEstimated
                                          },
                                          relationshipTypes: [{
                                              id: 'FamilialRelationshipType',
                                              name: 'Familial'
                                          }, {
                                              id: 'SocialRelationshipType',
                                              name: 'Social'
                                          }, {
                                              id: 'ProfessionalRelationshipType',
                                              name: 'Professional'
                                          }, ]
                                      });


                                  }
                              });
                          } else {
                              menu.addItem('add relationship', null, function() {
                                  //fire custom event
                                  var personTypes = subject.personTypes;
                                  var personName = subject.subjectName;
                                  var clientTeamId = client_team_id;
                                  if (personTypes.indexOf('OTHER') < 0 && personTypes.indexOf('CLIENT') < 0) {
                                      Y.fire('relationship:showDialog', {
                                          targetPersonId: graph.rootCellId,
                                          targetPersonName: personName,
                                          action: 'add',
                                          personRoles: personTypes,
                                          relationshipType: 'ProfessionalRelationshipType',
                                          relationshipTypes: {
                                              FamilialRelationshipType: 'familial',
                                              SocialRelationshipType: 'social',
                                              ProfessionalRelationshipType: 'professional'
                                          }
                                      });
                                  } else {
                                      Y.fire('relationship:showDialog', {
                                          action: 'addRelationshipType',
                                          personId: graph.rootCellId,
                                          personRoles: personTypes,
                                          targetPersonName: personName,
                                          clientTeamId: clientTeamId,
                                          relationshipTypes: [{
                                              id: 'FamilialRelationshipType',
                                              name: 'Familial'
                                          }, {
                                              id: 'SocialRelationshipType',
                                              name: 'Social'
                                          }, {
                                              id: 'ProfessionalRelationshipType',
                                              name: 'Professional'
                                          }, ]
                                      });
                                  }
                              });
                          }
                      }
                  }

              },

                afterLoadCallbackFn: function() {
                    return graphConfig.loadCallback.call(null, graph.get('data'));
                },

                //edge menu function
                edgeMenuCallbackFn: function(menu, value, source) {
                    var personId = source.value.id;

                    if (value.classifier === 'SUGGESTION') {

                        var suggestions = value.suggestedRelationshipVOs;
                        var targetPersonID = 'PER_' + value.target;
                        var targetPerson;
                        // if relationship in future, main person cell will not exist
                        // and suggestion cell will have suffix '_0'
                        if (this.graph.model.cells[targetPersonID]) {
                            targetPerson = this.graph.model.cells[targetPersonID];
                        } else {
                            targetPerson = this.graph.model.cells[targetPersonID + '_0'];
                        }
                        var suggestionRelationshipId = graph.get('suggestionsRelationshipId');

                        if (suggestions) {
                            for (var i = 0; i < suggestions.length; i++) {

                                var suggestion = suggestions[i];
                                menu.addItem(suggestion.relationshipLabel, null, function(e) {

                                    var selectedRelationship = e.srcElement ? e.srcElement.textContent : e.target.textContent;
                                    var selectedSuggestion = null;
                                    for (i = 0; i < suggestions.length; i++) {
                                        if (suggestions[i].relationshipLabel == selectedRelationship) {
                                            selectedSuggestion = suggestions[i];
                                            break;
                                        }
                                    }

                                    var relationshipLabel = selectedSuggestion.relationshipLabel;
                                    if (!(relationshipLabel.indexOf(" of ") > -1) && !(relationshipLabel.indexOf(" for by") > -1)) {
                                        relationshipLabel = relationshipLabel + ' of';
                                    }

                                    Y.fire('relationship:showDialog', {
                                        suggestionRelationshipId: suggestionRelationshipId,
                                        rootPersonId: graph.rootCellId,
                                        roleAPersonId: source.value.id,
                                        roleBPersonId: targetPerson.value.id,
                                        roleAPersonIdentifier: source.value.personId,
                                        roleBPersonIdentifier: targetPerson.value.personId,
                                        roleAPersonName: source.value.name,
                                        roleBPersonName: targetPerson.value.name,
                                        relationshipTypeId: selectedSuggestion.relationshipTypeId,
                                        relationshipTypeDescriptor: selectedSuggestion.discriminatorType,
                                        relationshipLabel: relationshipLabel,
                                        qualifier: selectedSuggestion.personPersonRelationshipQualifier,
                                        role: selectedSuggestion.role,
                                        relationshipStartDate: selectedSuggestion.relationshipStartDate,
                                        startDateEstimated: selectedSuggestion.relationshipStartDateEstimated,
                                        action: 'saveSuggestion'
                                    });
                                });
                            }
                        }
                    } else if (personId) {

                        var targetPersonID = 'PER_' + value.target,
                            targetPerson, accessAllowed;
                        // if relationship in future, main person cell will not exist
                        // and suggestion cell will have suffix '_0'
                        if (this.graph.model.cells[targetPersonID]) {
                            targetPerson = this.graph.model.cells[targetPersonID];
                        } else {
                            targetPerson = this.graph.model.cells[targetPersonID + '_0'];
                        }

                        accessAllowed = targetPerson.value.accessAllowed;

                        if (permissions.canView) {
                            menu.addItem('view relationship', null, function() {
                                //fire edit relationship
                                Y.fire('relationship:showDialog', {
                                    id: value.relationshipId,
                                    personId: personId,
                                    action: 'view',
                                    accessAllowed: accessAllowed
                                });
                            });
                        }

                        var isInactiveHistoric = value.isInactiveHistoric;
                        if (permissions.canEdit && accessAllowed) {
                            if (!isInactiveHistoric) {
                                menu.addItem('edit relationship', null, function() {
                                    //fire edit relationship

                                    Y.fire('relationship:showDialog', {
                                        id: value.relationshipId,
                                        personId: personId,
                                        action: 'edit'
                                    });
                                });
                            }
                        }

                        if (permissions.canClose && accessAllowed) {
                            if (!isInactiveHistoric) {
                                menu.addItem('end relationship', null, function() {
                                    //fire end relationship
                                    Y.fire('relationship:showDialog', {
                                        id: value.relationshipId,
                                        personId: personId,
                                        action: 'close'
                                    });
                                });
                            }
                        }

                        if (permissions.canRemove && accessAllowed) {
                            menu.addItem('remove relationship', null, function() {
                                //fire remove relationship
                                Y.fire('relationship:showDialog', {
                                    id: value.relationshipId,
                                    personId: personId,
                                    action: 'delete'
                                });
                            });
                        }

                    }
                },
                //Load custom template
                template: Y.RelationshipRecording.nodeTemplateLrg,
                keyTemplate: Y.RelationshipRecording.keyTemplate,
                menuTemplate: Y.RelationshipRecording.menuTemplate,
                automatedData: this.automaticData
            });

            return graph;
        },
        reload: function(e) {
          if(this.get('activeView')==='graph') {
            this.reloadGraph(e);
          } else {
            this.reloadGenogram(e);
          }
        },
        reloadGenogram: function(e) {
          this.showGenogram();
        },
        reloadGraph: function(e) {
            var graph = this.graph;
            var Storage = Y.uspSessionStorage;

            //reload the graph
            var autoIdList = [];
            var automaticRelationships;
            var clearAutomated = function() {
                graph.setAttrs({
                    automatedData: []
                }, {
                    silent: true
                });
            };

            if (!e.newRelationshipId && !e.preventClear) {
                clearAutomated();
            }

            if (Storage.hasStoreValue(AUTO_GENERATED_ID)) {
                automaticRelationships = Y.JSON.parse(Storage.getStoreValue(AUTO_GENERATED_ID));
                Storage.removeStoreValue(AUTO_GENERATED_ID);
                automaticRelationships.forEach(function(id) {
                    autoIdList.push(id);
                    Y.log('Automatically generated added Id from session storage', id);
                });
            }

            if (autoIdList.length > 0) {
                Y.log('Automatically generated added to graph');
                graph.setAttrs({
                    automatedData: autoIdList
                }, {
                    silent: true
                });
            }

            //don't trigger the onchange listener
            graph.setAttrs({
                suggestionsRelationshipId: e.newRelationshipId
            }, {
                silent: true
            });

            if (e.newRelationshipId) {
                graph.setAttrs({
                    suggestionsRelationshipId: e.newRelationshipId
                }, {
                    //don't trigger the onchange listener
                    silent: true
                });
                if (Y.one('#suggestionsToggle') && Y.one('#suggestionsToggle').hasClass('active')) {
                    Y.one('#suggestionsToggle').removeClass('active').addClass('inactive');
                }
                graph.load({
                    personId: graph.get('data').get('rootId'),
                    includeSuggestions: true,
                    suggestionsRelationshipId: e.newRelationshipId
                });
            } else if (e.includePersonSuggestions === true) {
                graph.load({
                    personId: graph.get('data').get('rootId'),
                    includeSuggestions: true
                });
            } else {
                if (Y.one('#suggestionsToggle') && Y.one('#suggestionsToggle').hasClass('active')) {
                    Y.one('#suggestionsToggle').removeClass('active').addClass('inactive');
                }
                graph.load({
                    personId: graph.get('data').get('rootId')
                });
            }
        },
        showAddDialog: function() {
            var subject = this.get('subject'),
                personTypes = subject.personTypes || [];

            Y.fire('relationship:showDialog', {
                action: 'addRelationshipType',
                personRoles: personTypes,
                personId: subject.subjectId,
                relationshipTypes: [{
                    id: 'FamilialRelationshipType',
                    name: 'Familial'
                }, {
                    id: 'SocialRelationshipType',
                    name: 'Social'
                }, {
                    id: 'ProfessionalRelationshipType',
                    name: 'Professional'
                }]
            });
        }
    }, {
      ATTRS: {
        activeView: {
          value: 'graph'
        },
        filterConfig: {},
        filterContextConfig: {},
        genogramGonfig: {
          value: {}
        },
        graphConfig: {
          value: {}
        },
        permissions: {
            value: {}
        },
        subject: {}
      }
    });
}, '0.0.1', {
    requires: [
      'yui-base',
      'view',
      'app-toolbar',
      'relationship-filter',
      'person-relationship-model',
      'usp-session-storage',
      'usp-graph',
      'nodeTemplate',
      'graph-template',
      'json-parse',
      'yui-later',
      'querystring-parse',
      'genogram-controller-view'
    ]
});
