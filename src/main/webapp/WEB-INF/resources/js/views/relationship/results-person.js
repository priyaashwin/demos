YUI.add('relationship-person-person-results', function (Y) {
  'use strict';
  
var A = Y.Array, 
  L = Y.Lang,
  E = Y.Escape,
  USPFormatters = Y.usp.ColumnFormatters, 
  USPTemplates = Y.usp.ColumnTemplates,
  APPFormatters = Y.app.ColumnFormatters,
  RelationshipFormatters = Y.app.relationship.ColumnFormatters;

  Y.namespace('app.relationship').PersonPersonRelationshipResults = Y.Base.create('personPersonRelationshipResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      var subject = config.subject;
      
      return new Y.app.relationship.PaginatedFilteredAsymmetricPersonPersonRelationshipResultList({
          url: L.sub(config.url, {
            id: subject.subjectId,
            subjectType: subject.subjectType
          }),
          filterModel: config.filterModel
      });
    },  
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
          permissions = config.permissions;
      return ([{
        key: 'personPersonId',
        label: labels.id,
        width: '13%',
        clazz:'viewRelationships',
        title:labels.viewRelationship,
        formatter: APPFormatters.linkable
      }, {
          key: 'name',
          label: labels.name,
          sortable: true,
          allowHTML:true,
          width: '32%',
          clazz:'viewRelationships',
          title:labels.viewRelationship,
          formatter: APPFormatters.linkablePopOverWarnings,
          url:'#none'
      }, {       
          key: 'relationship',
          label: labels.relationship,
          width: '25%',
          allowHTML:true,
          formatter: RelationshipFormatters.formatIcons,
          allowedAttributes: config.allowedAttributes,
          attributeDisplay: {  
        	  hideNamedPersonRelationshipAttribute: config.hideNamedPersonRelationship
          }
      }, {
        key: 'startDate',
        label: labels.startDate,
        sortable: true,
        width: '11%',
        allowHTML:true,
        formatter: APPFormatters.estimatedDate, 
        estimatedKey: 'startDateEstimated'
      }, {
        key: 'closeDate',
        label: labels.closeDate,
        sortable: true,
        width: '11%',
        formatter: USPFormatters.date
      }, {
        key:'mobile-summary',
        label: 'Summary',
        className: 'mobile-summary pure-table-desktop-hidden',
        width: '0%',
        allowHTML:true,
        formatter:RelationshipFormatters.summaryPersonPersonFormat,
        cellTemplate:'<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
      }, {
          label: labels.actions,
          className: 'pure-table-actions',
          width: '8%',
          formatter: USPFormatters.actions,
          items: [
            {
              clazz: 'addPersonRelationship',
              label: labels.add,
              title: labels.addTitle,
              visible:permissions.canAddPersonRelationship,
              enabled:function(){
                return this.record.get('accessAllowed')
              }
            },
            {
              clazz: 'editPersonRelationship',
              label: labels.edit,
              title: labels.editTitle,
              visible:permissions.canEditPersonRelationship,
              enabled:function(){
                return !(this.record.get('temporalStatus')=="INACTIVE_HISTORIC")&&this.record.get('accessAllowed');
              }
            },
            {
              clazz: 'closePersonRelationship',
              label: labels.close,
              title: labels.closeTitle,
              visible:permissions.canClosePersonRelationship,
              enabled:function(){
                return !(this.record.get('temporalStatus')=="INACTIVE_HISTORIC")&&this.record.get('accessAllowed');
              }
            },
            {
              clazz: 'removePersonRelationship',
              label: labels.remove,
              title: labels.removeTitle,
              visible:permissions.canRemovePersonRelationship,
              enabled:function(){
                return this.record.get('accessAllowed');
              }
            }
          ]
      }]);
    }
  },{
    ATTRS:{
      resultsListPlugins: {
        valueFn: function() {
            return [{
                fn: Y.Plugin.usp.ResultsTableMenuPlugin
            }, {
                fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
            }, {
              fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
              cfg: {
                state: 'expanded',
                summaryFormatter:RelationshipFormatters.summaryPersonPersonFormat
              }
            },{
              fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
            }];
        }
      }
    }
  });
  
  Y.namespace('app.relationship').PaginatedFilteredAsymmetricPersonPersonRelationshipResultList = Y.Base.create("paginatedFilteredAsymmetricPersonPersonRelationshipResultList",  Y.usp.relationshipsrecording.PaginatedAsymmetricPersonPersonRelationshipResultList,  [Y.usp.ResultsFilterURLMixin], {
      whitelist: ["relationshipTypes", "temporalStatusFilter", "dateFrom", "dateTo"],
      staticData: {
        useSoundex: false,
        appendWildcard: true
      }
    }, {
      ATTRS: {
        filterModel: {
            writeOnce: 'initOnly'
        }
    }
  });
  
  Y.namespace('app.relationship').PersonPersonRelationshipFilteredResults = Y.Base.create('personPersonRelationshipFilteredResults', Y.usp.app.FilteredResults, [], {
    filterType: Y.app.relationship.RelationshipFilter,
    resultsType: Y.app.relationship.PersonPersonRelationshipResults
  });
  
}, '0.0.1', {
    requires: ['yui-base',
               'app-results',
               'app-filtered-results',
               'usp-relationshipsrecording-AsymmetricPersonPersonRelationshipResult',
               'results-filter',
               'caserecording-results-formatters',
               'relationship-results-formatters',
               'results-formatters',
               'results-templates',
               'relationship-filter',
               'results-table-summary-row-plugin',
               'results-table-search-state-plugin']
});
