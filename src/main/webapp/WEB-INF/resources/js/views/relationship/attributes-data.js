


YUI.add('relationships-attributes-data', function(Y) {

	var A=Y.Array,

	REL_TYPE_FAMILIAL = 'FamilialRelationshipType',
	REL_TYPE_SOCIAL = 'SocialRelationshipType',
	REL_TYPE_ORGANISATIONAL = 'OrganisationalRelationshipType',
	REL_TYPE_PROFESSIONAL = 'ProfessionalRelationshipType',
	REL_TYPE_TEAM = 'ClientTeamRelationshipType',

	//Attributes for all relationships, (to add attribute to relationship add relationship to typeDiscriminator array in that attribute)
	_data = [
		{ id: 'ico-attribute-pr', name: 'parentalResponsibility', typeDiscriminator: [REL_TYPE_FAMILIAL, REL_TYPE_ORGANISATIONAL], label: 'Parental responsibility', abbrv: 'PR', code: 'attribute-pr', hidden: false},
		{ id: 'ico-attribute-nok', name: 'nextOfKin', typeDiscriminator: [REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Next of kin', abbrv: 'NoK', code: 'attribute-nok', hidden: false},
		{ id: 'ico-attribute-c', name: 'mainCarer', typeDiscriminator: [REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Carer', abbrv: 'C', code: 'attribute-c', hidden: false},
		{ id: 'ico-attribute-nc', name: 'nonContact', typeDiscriminator: [REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Non contact', abbrv: 'NC', code: 'attribute-nc', hidden: false},
		{ id: 'ico-attribute-rc', name: 'restrictedContact', typeDiscriminator: [REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Restricted contact', abbrv: 'RC', code: 'attribute-rc', hidden: false},
		{ id: 'ico-attribute-aw', name: 'allocatedWorker', typeDiscriminator: [REL_TYPE_PROFESSIONAL], label: 'Allocated worker', abbrv: 'AW', code: 'attribute-aw', hidden: false},
		{ id: 'ico-attribute-np', name: 'namedPerson', typeDiscriminator: [REL_TYPE_PROFESSIONAL], label: 'Named person', abbrv: 'NP', code: 'attribute-np', hidden: false},
		{ id: 'ico-attribute-pw', name: 'primaryWorker', typeDiscriminator: [REL_TYPE_PROFESSIONAL], label: 'Primary worker', abbrv: 'PW', code: 'attribute-pw', hidden: false},
		{ id: 'ico-attribute-kw',	name: 'keyWorker', typeDiscriminator: [REL_TYPE_PROFESSIONAL], label: 'Key worker', abbrv: 'KW', code: 'attribute-kw', hidden: false},
		{ id: 'ico-attribute-lp', name: 'leadProfessional', typeDiscriminator: [REL_TYPE_PROFESSIONAL], label: 'Lead professional', abbrv: 'LP', code: 'attribute-lp', hidden: false},
		{ id: 'ico-attribute-fr', name: 'financialRepresentative', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Financial representative',	abbrv: 'FR', code: 'attribute-fr', hidden: false},
		{ id: 'ico-attribute-cm', name: 'careManager', typeDiscriminator: [REL_TYPE_PROFESSIONAL], label: 'Care manager', abbrv: 'CM', code: 'attribute-cm', hidden: false},
		{ id: 'ico-attribute-rla', name: 'responsibleLocalAuthority', typeDiscriminator: [REL_TYPE_ORGANISATIONAL], label: 'Responsible local authority', abbrv: 'RLA', code: 'attribute-rla', hidden: false},
		{ id: 'ico-attribute-ec', name: 'emergencyContact', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL, REL_TYPE_ORGANISATIONAL], label: 'Emergency contact', abbrv: 'EC', code: 'attribute-ec', hidden: false},
		{ id: 'ico-attribute-kh', name: 'keyHolder', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL, REL_TYPE_ORGANISATIONAL], label: 'Key holder', abbrv: 'KH', code: 'attribute-kh', hidden: false},
		{ id: 'ico-attribute-dwp', name: 'dwpAppointee', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL, REL_TYPE_ORGANISATIONAL], label: 'DWP appointee', abbrv: 'DWP', code: 'attribute-dwp', hidden: false},
		{ id: 'ico-attribute-g', name: 'guardian', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Guardian', abbrv: 'G', code: 'attribute-g', hidden: false},
		{ id: 'ico-attribute-gw', name: 'guardianWelfare', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Guardian (Welfare)', abbrv: 'GW', code: 'attribute-gw', hidden: false},
		{ id: 'ico-attribute-gf', name: 'guardianFinancial', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Guardian (Financial)', abbrv: 'GF', code: 'attribute-gf', hidden: false},
		{ id: 'ico-attribute-cadf', name: 'courtAppointedDeputyFinancial', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Court Appointed Deputy (Financial)', abbrv: 'CADF', code: 'attribute-cadf', hidden: false},
		{ id: 'ico-attribute-cad', name: 'courtAppointedDeputy', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL, REL_TYPE_ORGANISATIONAL], label: 'Court Appointed Deputy', abbrv: 'CAD', code: 'attribute-cad', hidden: false},
		{ id: 'ico-attribute-poah', name: 'powerOfAttorneyHealth', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Lasting power of attorney health and welfare', abbrv: 'POAH', code: 'attribute-poah', hidden: false},
		{ id: 'ico-attribute-poaf', name: 'powerOfAttorneyFinance', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Power of attorney Finance and property', abbrv: 'POAF', code: 'attribute-poaf', hidden: false},
		{ id: 'ico-attribute-nr', name: 'nearestRelative', typeDiscriminator: [REL_TYPE_FAMILIAL], label: 'Nearest Relative (Mental Health Act)', abbrv: 'NR', code: 'attribute-nr', hidden: false},
		{ id: 'ico-attribute-at', name: 'allocatedTeam', typeDiscriminator: [REL_TYPE_TEAM], label: 'Allocated team', abbrv: 'AT', code: 'attribute-at'},
		{ id: 'ico-attribute-ad', name: 'advocate', typeDiscriminator: [REL_TYPE_PROFESSIONAL, REL_TYPE_FAMILIAL, REL_TYPE_SOCIAL], label: 'Advocate', abbrv: 'AD', code: 'attribute-ad', hidden: false}
	],

	Attributes = {
			getData: function() {
				return _data.slice(0);
			}
	};

	Y.namespace('mock.relationships').Attributes = Attributes;

}, '0.0.1', 
{
	requires: ['yui-base','event-delegate', 'event-custom','json']
});