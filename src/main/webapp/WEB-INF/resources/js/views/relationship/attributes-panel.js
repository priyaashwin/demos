YUI.add('relationships-attributes-panel', function (Y) {
    'use-strict';
    var A = Y.Array,
        O = Y.Object,
        H = Y.Handlebars,
        REL_TYPE_FAMILIAL = 'FamilialRelationshipType',
        REL_TYPE_ORGANISATIONAL = 'OrganisationalRelationshipType',
        REL_TYPE_PROFESSIONAL = 'ProfessionalRelationshipType',
        REL_TYPE_SOCIAL = 'SocialRelationshipType',
        REL_TYPE_TEAM = 'ClientTeamRelationshipType',
        REL_TYPE_IGNORE = '<div id="no-attributes-to-display"></div>',
        ROLE_A = 'ROLE_A',
        ROLE_B = 'ROLE_B',
        /**
         * @class AttributesPanel
         * @description
         *  
         *  The panel entire panel should start disabled in add dialogs, 
         *  but can be enabled in Edit dialog.
         *  
         */
        AttributesPanel = Y.Base.create('attributesPanel', Y.View, [], {
            events: {
                'input[type=checkbox]': {
                    change: '_syncValues'
                }
            },
            /**
             * @method initialiser
             * @description
             */
            initializer: function (config) {
                this.template = Y.Handlebars.templates.attributesPanel;

                this._evtHandlers = [
                    this.after('panelActiveChange', this._handlePanelActive, this),
                    this.before('relationshipRoleChange', this._disablePanel, this),
                    this.after('relationshipRoleChange', this.enablePanel, this),
                ];

                this.hideNamedPersonRelationship = config.hideNamedPersonRelationship;

                this.allowedAttributes = config.allowedAttributes;
            },
            destructor: function () {
                this._evtHandlers.forEach(function (handler) {
                    handler.detach();
                });
                delete this._evtHandlers;
            },
            getRelationshipAttributes: function () {
                var role = this.get('relationshipRole'),
                    isEdit = this.get('isEdit'),
                    relationshipType = this.get('relationshipType'),
                    relatedEntity = this.get('relatedEntity'),
                    targetEntity = this.get('targetEntity');

                if (this.get('panelActive')) {
                    //if this is a union, we need to return the actual role selected 
                    if (role && role.roleClass === 'UNION') {
                        var unionCheck = Y.one('input[name=unionType]:checked');
                        if (unionCheck) {
                            var unionRole = this.findRole(unionCheck.get('value'));
                            if (unionRole) {
                                role = unionRole;
                            }
                        }
                    }
                    var getRoleEnd = function () {
                        var roleEnd, roleTokens;
                        if (!isEdit) {
                            roleTokens = role.id.split('_');
                            if (roleTokens.length == 2) {
                                roleEnd = roleTokens[1];
                            }
                        } else {
                            if (relationshipType !== REL_TYPE_ORGANISATIONAL || relationshipType !== REL_TYPE_TEAM) {
                                if (this.get('personId') === this.get('roleAPersonId')) {
                                    roleEnd = 'A';
                                } else if (this.get('personId') === this.get('roleBPersonId')) {
                                    roleEnd = 'B';
                                }
                            }
                        }
                        return roleEnd;
                    }.bind(this);

                    // Set age variables
                    var targetEntityAge = 0;
                    var relatedEntityAge = 0;
                    var targetAge = this.get('targetAge');

                    if (typeof targetEntity.age !== 'undefined' && targetEntity.age) {
                        targetEntityAge = parseInt(targetEntity.age);
                    } else if (targetAge) {
                        targetEntityAge = parseInt(targetAge);
                    }

                    if (typeof relatedEntity.age !== 'undefined' && relatedEntity.age) {
                        relatedEntityAge = parseInt(relatedEntity.age);
                    }

                    if (role) {
                        if (relationshipType !== REL_TYPE_ORGANISATIONAL && relationshipType !== REL_TYPE_TEAM) {
                            switch (getRoleEnd()) {
                            case 'A':
                                {
                                    relatedEntity.value = ROLE_A;
                                    targetEntity.value = ROLE_B;
                                    if (relationshipType === REL_TYPE_PROFESSIONAL) {
                                        relatedEntity.disabled = this.getAttributes(false);
                                        targetEntity.disabled = this.getAttributes(true);
                                    }
                                    break;
                                }
                            case 'B':
                                {
                                    relatedEntity.value = ROLE_B;
                                    targetEntity.value = ROLE_A;
                                    if (relationshipType === REL_TYPE_PROFESSIONAL) {
                                        relatedEntity.disabled = this.getAttributes(true);
                                        targetEntity.disabled = this.getAttributes(false);
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    var clearAndDisableAttributes = function (attributes,entityType) {
                    	attributes.map(function(attr){
                    		entityType.checked[attr] = '';
                    		entityType.disabled[attr] = true;
                          });   
                    };

                    if (relationshipType === REL_TYPE_FAMILIAL) {
                        if (targetEntityAge >= 18) {
                            //clear and disable for related person
                        	clearAndDisableAttributes(["parentalResponsibility"],relatedEntity);
                        }
                        if (relatedEntityAge >= 18) {
                            //clear and disable for target person
                        	clearAndDisableAttributes(["parentalResponsibility"],targetEntity);
                        }
                    } else if (relationshipType === REL_TYPE_ORGANISATIONAL) {
                    	var disabledAttributes = ["emergencyContact","keyHolder","dwpAppointee","parentalResponsibility","courtAppointedDeputy"];
                    	clearAndDisableAttributes(disabledAttributes,targetEntity);
                        if (targetEntityAge >= 18) {
                        	clearAndDisableAttributes(["parentalResponsibility"],relatedEntity);
                        }
                    } else if (relationshipType === REL_TYPE_TEAM){
                    	//AllocatedTeam can't be set on client side
                    	clearAndDisableAttributes(["allocatedTeam"],targetEntity);
                    }
                }
                return {
                    relatedEntity: relatedEntity,
                    targetEntity: targetEntity
                };
            },
            render: function () {
                var self = this;
                if (!this.get('relationshipType')) {
                    this.get('container').setHTML(REL_TYPE_IGNORE);
                    return this;
                }
                var attrs = this.getRelationshipAttributes();
                var jsonAttributes = this.get('attributesByType').toJSON();
                var adultAttributes = {
                    'emergencyContact': 'EMERGENCY_CONTACT',
                    'keyHolder': 'KEY_HOLDER',
                    'dwpAppointee': 'DWP_APPOINTEE',
                    'guardian': 'GUARDIAN',
                    'guardianWelfare': 'GUARDIAN_WELFARE',
                    'courtAppointedDeputyFinancial': 'COURT_APPOINTED_DEPUTY_FINANCIAL',
                    'courtAppointedDeputy': 'COURT_APPOINTED_DEPUTY',
                    'powerOfAttorneyHealth': 'POWER_OF_ATTORNEY_HEALTH',
                    'powerOfAttorneyFinance': 'POWER_OF_ATTORNEY_FINANCE',
                    'nearestRelative': 'NEAREST_RELATIVE',
                    'guardianFinancial': 'GUARDIAN_FINANCIAL'
                };
                Object.values(jsonAttributes).forEach(function (attr) {
                    if (attr && (attr.name === 'namedPerson') && self.hideNamedPersonRelationship) {
                        attr.hidden = true;
                    }

                    if (adultAttributes[attr.name] && self.allowedAttributes.indexOf(adultAttributes[attr.name]) === -1)  {
                        attr.hidden = true;
                    }
                });
                this.get('container').setHTML(this.template({
                    modelData: jsonAttributes,
                    panelActive: this.get('panelActive'),
                    relatedEntity: attrs.relatedEntity,
                    targetEntity: attrs.targetEntity,
                })).addClass('pure-g-r');
                return this;
            },
            /**
             * @method updatePanel
             * @description This method is called from Edit Views. The order 
             * 	we set is important:
             *  
             *  relationshipType: determines which set of attributes  
             *  targetEntity, relatedEntity: determines which are disabled.
             *  setValues: determines which are checked
             *  relationshipRole: triggers a render
             *  
             */
            updatePanel: function (attrs) {
                this.set('relationshipType', attrs.relationshipType);
                this._setABRoles(attrs);
                //This is to resolve role End of the relationship 
                this.set('personId', attrs.personId);
                this.set('roleAPersonId', attrs.roleAPersonId);
                this.set('roleBPersonId', attrs.roleBPersonId);
                //End of deciding
                this.set('editPanel', true);
                this.set('targetEntity', attrs.targetEntity);
                this.set('relatedEntity', attrs.relatedEntity);
                this.setValues(attrs);
                this.set('relationshipRole', attrs.personPersonRelationshipTypeId + '_' + (attrs.personId === attrs.roleAPersonId ? 'A' : 'B'));
            },
            _handlePanelActive: function (e) {
                if (e.newVal) {
                    var targetEntity = this.get('targetEntity');
                    var relatedEntity = this.get('relatedEntity');

                    //reset disabled attributes when panel is re-enabled
                    targetEntity.disabled = this.getAttributes(false);
                    relatedEntity.disabled = this.getAttributes(false);

                    //these will be re-set to the correct values in the getAttributes call via
                    //the render method
                }
                this.render();
            },
            /**
             * @method enablePanel
             * @description Allows a parent view to control AttributesPanel's
             * 	active status.
             */
            enablePanel: function () {
                this.set('panelActive', true);
            },
            /**
             * @method disablePanel
             * @description Allows a parent view to control AttributesPanel's
             * 	active status.
             */
            disablePanel: function () {
                this.set('panelActive', false);
                this.set('relatedEntity', {});
            },
            getAttributes: function (value) {
                var retVal = {};
                A.each(this.get('attributesByType').toJSON(), function (item) {
                    retVal[item.name] = value;
                });
                return retVal;
            },
            /**
             * @method getValues
             * @description returns output of our form value.
             *  can be called just before model.save and should be silent e.g. 		 
             *  mode.setAttrs(view.getValues(), { silent: true });
             */
            getValues: function (type) {
                var related = this.get('relatedEntity');
                var target = this.get('targetEntity');
                var values = {};
                O.each(this.getAttributes(''), function (value, key) {
                    if (related.checked[key] && target.checked[key]) {
                        values[key] = 'BOTH';
                    } else if (related.checked[key] || target.checked[key]) {
                        values[key] = related.checked[key] || target.checked[key];
                    } else if (type === 'Update' || type === 'Add') {
                        values[key] = null;
                    }
                });
                return values;
            },
            /**
             * @method setValues
             * @description
             */
            setValues: function (attrs) {
                var related = this.get('relatedEntity'),
                    target = this.get('targetEntity'),
                    inverted = this.get('rolesInverted');
                O.each(this.getAttributes(''), function (value, key) {
                    switch (attrs[key]) {
                    case 'BOTH':
                        related.checked[key] = related.value;
                        target.checked[key] = target.value;
                        break;
                    case 'ROLE_A':
                        inverted ? target.checked[key] = target.value : related.checked[key] = related.value;
                        break;
                    case 'ROLE_B':
                        inverted ? related.checked[key] = related.value : target.checked[key] = target.value;
                        break;
                    }
                    this.set('relatedEntity', related, {
                        silent: true
                    });
                    this.set('targetEntity', target, {
                        silent: true
                    });
                }, this);
            },
            /**
             * @method clrValues
             * @description Clears down check box values 
             * 
             * 	This should only be call be called from Add Relationships
             * 
             * 	relationshipRole will trigger a render and set panelActive
             * 	to true which is why it is cleared first
             */
            clrValues: function () {
                this.set('relationshipRole', '');
                this.set('relatedEntity.checked', '');
                this.set('targetEntity.checked', '');
                this.set('panelActive', false);
            },
            /**
             * @method _disablePanel
             * @description Changing role triggers this method.
             */
            _disablePanel: function () {
                this.set('panelActive', false);
            },
            _entityIsPro: function (o) {
                return o.personTypes && o.personTypes.indexOf('PROFESSIONAL') > -1;
            },
            _setValue: function (o) {
                var entity = this.get(o.target);
                entity.checked[o.name] = '';
                if (o.checked) {
                    entity.checked[o.name] = o.value;
                }
                this.set(o.target, entity, {
                    silent: true
                });
            },
            _setABRoles: function (attrs) {
                if (attrs.relatedEntity && attrs.relatedEntity.entityType === 'organisation') {
                    this.set('rolesInverted', true);
                    this.set('relatedEntityRole', ROLE_B);
                    this.set('targetEntityRole', ROLE_A);
                } else if (!attrs.roleAPersonId || !attrs.roleBPersonId) {
                    return;
                } else if (attrs.targetEntity && attrs.targetEntity.id == attrs.roleAPersonId) {
                    this.set('rolesInverted', true);
                    this.set('relatedEntityRole', ROLE_B);
                    this.set('targetEntityRole', ROLE_A);
                }
            },
            _syncValues: function (e) {
                var node = e.currentTarget;
                this._setValue({
                    checked: node.get('checked'),
                    name: node.get('id'),
                    target: node.getAttribute('data-target'),
                    value: node.get('value')
                });
            },
            findRole: function (id) {
                //lookup role 
                var relationshipType = this.get('relationshipType'),
                    role;
                var findRole = function (roles, id) {
                    return A.find(roles || [], function (role) {
                        return role.id === id;
                    });
                };
                if (relationshipType === 'FamilialRelationshipType') {
                    role = findRole(this.get('familialRelationshipRoles'), id);
                } else if (relationshipType === 'SocialRelationshipType') {
                    role = findRole(this.get('socialRelationshipRoles'), id);
                } else if (relationshipType === 'ProfessionalRelationshipType') {
                    role = findRole(this.get('professionalRelationshipRoles'), id);
                }
                return role;
            }
        }, {
            ATTRS: {
                /**
                 * @attribute panelActive			
                 */
                panelActive: {
                    value: false
                },
                /**
                 * @attribute attributes
                 * @description ModelList containing all attributes 
                 */
                attributes: {
                    valueFn: function () {
                        return new Y.ModelList({
                            items: Y.mock.relationships.Attributes.getData()
                        });
                    }
                },
                /**
                 * @attribute attributesByType
                 * @description ModelList of filtered attributes
                 */
                attributesByType: {
                    getter: function () {
                        var rt = this.get('relationshipType');
                        return new Y.ModelList({
                            items: this.get('attributes').toJSON().filter(function (attribute) {
                                return attribute.typeDiscriminator.indexOf(rt) !== -1;
                            }).map(function(attribute) {
                                return Y.merge(attribute, {
                                    typeDiscriminator:rt
                                });
                            })
                        });
                    }
                },
                /**
                 * @attribute relationshipType
                 * @description Determines initial set of attributes to display.
                 * 
                 * 	- The relatedEntity can overrule this value as
                 * 	  professional/professional attributes do not exist.
                 * 				 
                 *  - AttibutePanel's parent view sets value.
                 *  
                 */
                relationshipType: {
                    value: null
                },
                /**
                 * @attribute relationshipRole
                 * @description Determines when panel is 'active'
                 * 
                 *  Inputs located before the attributes panel start off disabled.
                 *  
                 *  The relationship start date (immediately above AttibutePanel) is enabled once 
                 *  a relationship (relationshipRole) has been selected.
                 *  
                 *  This is the point the AttibutePanel can active.
                 * 
                 *  AttibutePanel parent view sets relationshipRole values.
                 *
                 */
                relationshipRole: {
                    setter: 'findRole'
                },
                /**
                 * @attribute targetEntity
                 * @description 
                 * 
                 * 	targetEntity always on the right, 
                 * 	Could be person or organisation
                 * 	AttibutePanel parent view sets values.
                 * 
                 * 	Enabled/Disabled rules.
                 * 	-----------------------
                 * 	organisationAttributes - all disabled.
                 *  professionalAttributes - all enabled if professional && !(targetEntity===professional && relatedEntity===professional)
                 *  					   - all disabled if personType client.
                 *  familialAttributes 	   - all two-way attributes enabled
                 *  					
                 *  
                 *  
                 */
                targetEntity: {
                    value: {},
                    setter: function (targetEntity, name, options) {
                        var rt = this.get('relationshipType');
                        options = options || {};
                        if (options.silent) {
                            return targetEntity;
                        }
                        targetEntity.checked = this.getAttributes('');
                        targetEntity.disabled = this.getAttributes(false);
                        if ((rt === REL_TYPE_ORGANISATIONAL || rt === REL_TYPE_TEAM) && !this.get('rolesInverted')) {
                            targetEntity.value = this.get('relatedEntityRole');
                        } else {
                            targetEntity.value = this.get('targetEntityRole');
                        }
                        if (rt && rt === REL_TYPE_ORGANISATIONAL) {
                            targetEntity.disabled = this.getAttributes(true);
                        }
                        if (rt && rt === REL_TYPE_PROFESSIONAL && !this._entityIsPro(targetEntity)) {
                            targetEntity.disabled = this.getAttributes(true);
                        }
                        return targetEntity;
                    }
                },
                /**
                 * @attribute relatedEntity
                 * @description 
                 * 
                 * 	relatedEntity entity is always on the left. 
                 * 	Can only be a person.
                 * 	AttibutePanel parent view sets it's values.
                 * 
                 *	organisationAttributes - all enabled
                 */
                relatedEntity: {
                    value: {},
                    setter: function (relatedEntity, name, options) {
                        var rt = this.get('relationshipType');
                        options = options || {};
                        if (options.silent) {
                            return relatedEntity;
                        }
                        relatedEntity.checked = this.getAttributes('');
                        relatedEntity.disabled = this.getAttributes(false);
                        if ((rt === REL_TYPE_ORGANISATIONAL || rt === REL_TYPE_TEAM) && !this.get('rolesInverted')) {
                            relatedEntity.value = this.get('targetEntityRole');
                        } else {
                            relatedEntity.value = this.get('relatedEntityRole');
                        }
                        if (rt && rt === REL_TYPE_PROFESSIONAL && this._entityIsPro(this.get('targetEntity'))) {
                            relatedEntity.disabled = this.getAttributes(true);
                        }
                        return relatedEntity;
                    }
                },
                targetEntityRole: {
                    value: ROLE_B,
                },
                relatedEntityRole: {
                    value: ROLE_A,
                },
                isEdit: {
                    value: false
                },
                rolesInverted: {
                    value: false
                },
                familialRelationshipRoles: {
                    value: []
                },
                socialRelationshipRoles: {
                    value: []
                },
                professionalRelationshipRoles: {
                    value: []
                }
            }
        });

    H.registerHelper('isAttributeChecked', function (o, name) {
        return o ? (o[name]) ? 'checked' : '' : '';
    });

    H.registerHelper('isAttributeDisabled', function (o, name) {
        return o ? (o[name] === true) ? 'disabled' : '' : '';
    });

    Y.namespace('app.relationship').AttributesPanel = AttributesPanel;

}, '0.0.1', {
    requires: [
        'yui-base',
        'escape',
        'event-custom',
        'model',
        'app-helpers',
        'handlebars-relationship-templates',
        'relationships-attributes-data'
    ]
});