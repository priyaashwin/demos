/** 
 * formatters relationships results tables
 * @module relationships-results-formatters
 * @author Rich S
 * @since 1.0.0
 */

YUI.add('relationship-results-formatters', function(Y) {

    var L = Y.Lang,
        A = Y.Array,
        E = Y.Escape,
        REL_TYPE_ORGANISATIONAL = 'OrganisationalRelationshipType',
        REL_TYPE_CLIENT_TEAM = 'ClientTeamRelationshipType',
        REL_TYPE_PROFESSIONAL_TEAM = 'ProfessionalTeamRelationshipType',

        Formatters = {

            automationIcon: function(o) {
                if (o.record.get("automated")) {
                    return ' ' + '<span class="automated" title="Automated">' + E.html(o.record.get("relationship")) + '<span class="hidden">Automated</span></span>';
                }
            },
            summaryPersonPersonFormat: function(o) {
                var record=o.record,
                    isAddressDoNotDisclose = record.get('doNotDisclose'),
                    iconDND = isAddressDoNotDisclose ? '<div class="pop-up-data" data-content="Do not disclose" data-align="bottom"><span class="do-not-disclose"><i class="fa fa-ban" aria-hidden="true"></i></span></div>' : '',
                    addressHTML = '<strong>Address</strong> <div class="addressLocation-wrapper">{iconDND}<span class="address">{address}</span></div> ',
                    row, dobInfo = [],
                    gender,
                    dob = record.get('dateOfBirth'),
                    dobEstimated = record.get('dateOfBirthEstimated'),
                    age = record.get('age'),
                    address = record.get('address')||'',
                    lifeState = record.get('lifeState'),
                    dueDate = record.get('dueDate');

                
                address = address || 'None';
                address = L.sub(addressHTML, {
                    iconDND: iconDND,
                    address: E.html(address)
                });

                dobInfo = Y.app.ColumnFormatters.formatDateBasedOnLifeState(lifeState, dueDate, dob, dobEstimated, age, false);

                var ethnicity = Y.uspCategory.person.ethnicity.category.codedEntries[record.get('ethnicity')];

                var ethnicityInfo = '';

                if (ethnicity) {
                    ethnicityInfo = '<strong> Ethnicity</strong> ' + E.html(ethnicity.name);
                }

                gender = Y.uspCategory.person.gender.category.codedEntries[record.get('gender')];
                genderInfo = '';
                if (gender) {
                    genderInfo = '<strong> Gender</strong> ' + E.html(gender.name);
                }
                return ('<div>' + address + genderInfo + dobInfo.join(' ') + ethnicityInfo + '</div>');
            },
            summaryPersonOrganisationFormat: function(o) {
                var record=o.record,
                    isAddressDoNotDisclose = record.get('doNotDisclose'),
                    iconDND = isAddressDoNotDisclose ? '<div class="pop-up-data" data-content="Do not disclose" data-align="bottom"><span class="do-not-disclose"><i class="fa fa-ban" aria-hidden="true"></i></span></div>' : '',
                    addressHTML = '<strong>Address</strong> <div class="addressLocation-wrapper">{iconDND}<span class="address">{address}</span></div> ',
                    type = (record.get('organisationVO').organisationType === 'TEAM') ? {
                        name: 'Team'
                    } : (Y.uspCategory.organisation.organisationType.category.codedEntries[record.get('organisationVO!type')]),
                    subType = Y.uspCategory.organisation.organisationSubType.category.codedEntries[record.get('organisationVO!subType')],
                    address = record.get('address') || '';
                   

                var typeLabel = '',
                    subTypeLabel = '';

                
                address = address || 'None';
                address = L.sub(addressHTML, {
                    iconDND: iconDND,
                    address: E.html(address)
                });
                if (type) {
                    typeLabel = '<strong> Type </strong>' + E.html(type.name);
                }
                if (subType) {
                    subTypeLabel = '<strong> Subtype </strong>' + E.html(subType.name);
                }

                return ('<div>' + address + typeLabel + subTypeLabel + '</div>');
            },
            /** 
             * function name : formatRelationshipAttributeIcons 
             * Name space : Y.app.relationships.FormatRelationshipAttributeIcons
             * ========================
             * This is a formatter for populating right attribute Icons based on the Direction of attribute and Role 
             * but it is specific to only relationships list view
             * This function takes entire row data and matches the typeDiscriminator attribute on the VO 
             * Based on the Role direction all respective properties are populated as an array and display using handlebar
             * 
             **/

            formatIcons: function(o) {

                var attributesCodes = {
                    'pr': {code: 'PARENTAL_RESPONSIBILITY', name: 'parentalResponsibility', text: 'Parental responsibility', textopp: 'Has Parental responsibility for'},
                    'nok': {code: 'NEXT_OF_KIN', name: 'nextOfKin', text: 'Next of kin', textopp: 'Next of Kin for'},
                    'c': {code: 'MAIN_CARER', name: 'mainCarer', text: 'Carer', textopp: 'Cared for by'},
                    'nc': {code: 'NON_CONTACT', name: 'nonContact', text: 'Non contact', textopp: 'Not to be contacted by'},
                    'rc': {code: 'RESTRICTED_CONTACT', name: 'restrictedContact', text: 'Restricted contact', textopp: 'Restricted contact with'},
                    'ec': {code: 'EMERGENCY_CONTACT', name: 'emergencyContact', text: 'Emergency contact', textopp: 'Emergency contact for'},
                    'kh': {code: 'KEY_HOLDER', name: 'keyHolder', text: 'Key holder', textopp: 'Key holder for'},
                    'aw': {code: 'ALLOCATED_WORKER', name: 'allocatedWorker', text: 'Allocated worker', textopp: 'Client of Allocated Worker'},
                    'np': {code: 'NAMED_PERSON', name: 'namedPerson', text: 'Named person', textopp: 'Named person of'},
                    'pw': {code: 'PRIMARY_WORKER', name: 'primaryWorker', text: 'Primary worker', textopp: 'Client of primary worker'},
                    'kw': {code: 'KEY_WORKER', name: 'keyWorker', text: 'Key worker', textopp: 'Client of Key Worker'},
                    'lp': {code: 'LEAD_PROFESSIONAL', name: 'leadProfessional', text: 'Lead professional', textopp: 'Client of lead professional'},
                    'fr': {code: 'FINANCIAL_REPRESENTATIVE', name: 'financialRepresentative', text: 'Financial representative', textopp: 'Financial representative of'},
                    'cm': {code: 'CARE_MANAGER', name: 'careManager', text: 'Care manager', textopp: 'Client of Care Manager'},
                    'rla': {code: 'RESPONSIBLE_LOCAL_AUTHORITY', name: 'responsibleLocalAuthority', text: 'Responsible local authority', textopp: ''},
                    'dwp': {code: 'DWP_APPOINTEE', name: 'dwpAppointee', text: 'DWP Appointee', textopp: 'DWP Appointee for'},
                    'g': {code: 'GUARDIAN', name: 'guardian', text: 'Guardian', textopp: 'Subject of Guardianship'},
                    'gw': {code: 'GUARDIAN_WELFARE', name: 'guardianWelfare', text: 'Guardian (Welfare)', textopp: 'Subject of Guardianship (Welfare)'},
                    'gf': {code: 'GUARDIAN_FINANCIAL', name: 'guardianFinancial', text: 'Guardian (Financial)', textopp: 'Subject of Guardianship (Financial)'},
                    'cadf': {code: 'COURT_APPOINTED_DEPUTY_FINANCIAL', name: 'courtAppointedDeputyFinancial', text: 'Court Appointed Deputy (Financial)', textopp: 'Court Appointed Deputy (Financial) for'},
                    'cad': {code: 'COURT_APPOINTED_DEPUTY', name: 'courtAppointedDeputy', text: 'Court Appointed Deputy', textopp: 'Court Appointed Deputy for'},
                    'poah': {code: 'POWER_OF_ATTORNEY_HEALTH', name: 'powerOfAttorneyHealth', text: 'Lasting power of attorney health and welfare', textopp: 'Lasting power of attorney health and welfare for'},
                    'poaf': {code: 'POWER_OF_ATTORNEY_FINANCE', name: 'powerOfAttorneyFinance', text: 'Power of attorney Finance and property', textopp: 'Power of attorney Finance and property for'},
                    'nr': {code: 'NEAREST_RELATIVE', name: 'nearestRelative', text: 'Nearest Relative (Mental Health Act)', textopp: 'Nearest Relative (Mental Health Act) for'},
                    'ad': {code: 'ADVOCATE', name: 'advocate', text: 'Advocate', textopp: 'Client Of Advocate'}
                };

                var relationshipsAttributes = {
                    'FamilialRelationshipType': ['pr', 'nok', 'c', 'nc', 'rc', 'ec', 'fr', 'kh', 'dwp', 'g', 'gw', 'gf' , 'cad', 'cadf', 'poah', 'poaf', 'nr','ad'],
                    'SocialRelationshipType': ['pr', 'nok', 'c', 'nc', 'rc', 'ec', 'fr', 'kh', 'dwp', 'g', 'gw', 'gf', 'cad', 'cadf', 'poah', 'poaf','ad'],
                    'ProfessionalRelationshipType': ['aw', 'np', 'pw', 'kw', 'lp', 'fr', 'cm', 'ec', 'kh', 'dwp', 'g', 'gw', 'gf', 'cad', 'cadf', 'poah', 'poaf','ad'],
                    'OrganisationalRelationshipType': ['pr', 'rla', 'ec', 'kh', 'dwp', 'cad']
                };

                var data = o.record.toJSON(),
                    relationship = data.relationship,
                    professionalTeamName = data.professionalTeamName,
                    organisationTypeCodedEntries = null,
                    automated = false,
                    /**This holds objects of css icon value and label
                     * attributesArray = [
                     *             {
                     *              iconCSS : 'attribute-aw',
                     *              label : 'Allocated Worker'
                     *            },
                     *             {
                     *              iconCSS : 'attribute-pw',
                     *              label : 'Primary Worker'          
                     *            }
                     *           ]
                     *  
                     **/

                    attributesArray = [],
                    role = null,
                    bothRoles = 'BOTH',
                    attributeDisplay = o.column.attributeDisplay || {},
                    allowedAttributes = o.column.allowedAttributes || [];

                if (data.personId === data.roleAPersonId) {
                    role = 'ROLE_A';
                } else if (data.personId === data.roleBPersonId) {
                    role = 'ROLE_B';
                }
                //Just to check relationship is automated
                if (data.automated) {
                    automated = data.automated;
                }

                if ((data.organisationVO && data.personVO ) || data.typeDiscriminator === REL_TYPE_ORGANISATIONAL) {
                    organisationTypeCodedEntries = Y.uspCategory.organisation.organisationType.category.codedEntries;
                    relationship = o.record.get('organisationVO!type');

                    relationshipsAttributes[REL_TYPE_ORGANISATIONAL].map(function (key) {
                        if (data[attributesCodes[key].name] === 'ROLE_B' && allowedAttributes.indexOf(attributesCodes[key].code) !== -1) {
                            attributesArray.push({
                                iconCSS: 'attribute-' + key,
                                label:  attributesCodes[key].text
                            });
                        }
                    });

                    if (data.personOrganisationRelationshipTypeVO) {
                        if (data.personOrganisationRelationshipTypeVO._type === REL_TYPE_CLIENT_TEAM ||
                              data.personOrganisationRelationshipTypeVO._type === REL_TYPE_PROFESSIONAL_TEAM) {
                            organisationTypeCodedEntries = null;
                            relationship = Formatters.capitaliseFirstLetter(data.organisationVO.organisationType);
                            if (data.allocatedTeam && (data.allocatedTeam === 'ROLE_B')) {
                                attributesArray.push({
                                    iconCSS: 'attribute-at',
                                    label: 'Allocated Team'
                                });
                            }
                        }
                    }
                } else {
                    var attributes = relationshipsAttributes[data.typeDiscriminator] || {};
                    attributes.map(function (key) {
                        if (data[attributesCodes[key].name]  && allowedAttributes.indexOf(attributesCodes[key].code) !== -1 &&
                                !(key === 'np' && attributeDisplay.hideNamedPersonRelationshipAttribute)) {
                            if(data[attributesCodes[key].name] === role || data[attributesCodes[key].name] === bothRoles) {
                                attributesArray.push({
                                    iconCSS: 'attribute-' + key,
                                    label: attributesCodes[key].text
                                });
                            } else {
                                attributesArray.push({
                                    iconCSS: 'attribute-' + key + '-opp',
                                    label: attributesCodes[key].textopp
                                });
                            }
                        }
                    });
                }
                
                if (data.personPersonRelationshipType === 'Professional' && !professionalTeamName ) {
                	professionalTeamName = "None Recorded";
                }

                var template = Y.Handlebars.templates.attributesIconsDisplay;
                return template({
                    modelData: {
                        relationship: relationship,
                        professionalTeamName: professionalTeamName,
                        multipleBirth: {
                          hasSiblings: data.multipleBirth,
                          identicalSibling: data.identicalSibling,
                          label: "Multiple " + (data.identicalSibling ? "identical sibling(s)": "birth sibling(s)"),
                          total: data.numberOfMultipleBirthSiblings,
                        },
                        attributes: attributesArray,
                        automated: automated,
                        codedEntries: organisationTypeCodedEntries
                    }
                });
            },
            capitaliseFirstLetter: function(s) {
                return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
            }
        };

    Y.namespace('app.relationship').ColumnFormatters = Formatters;

}, '0.0.1', {
    requires: [
        'yui-base',
        'escape',
        'app-utils-person',
        'usp-date',
        'handlebars-person-templates',
        'results-formatters',
        'handlebars-relationship-templates',
        'categories-organisation-component-OrganisationType',
        'categories-organisation-component-OrganisationSubType',
        'categories-person-component-Ethnicity',
        'categories-person-component-Gender'
    ]
});