YUI.add('careleaver-controller-view', function (Y) {

    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.careleaver').CareLeaverControllerView = Y.Base.create('careLeaverControllerView', Y.View, [], {

        initializer: function (config) {
            this.careLeaverDialog = new Y.app.careleaver.CareLeaverDialog(config.dialogConfig).addTarget(this).render();

            this.careLeaverRemoveController = new Y.app.remove.RemoveControllerView(config.removeConfig).render();

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            var codedEntries = {
                careLeaverEligibility: Y.secured.uspCategory.careleaver.careLeaverEligibility.category,
                reasonQualifying: Y.uspCategory.careleaver.reasonQualifying.category,
                inTouch: Y.uspCategory.careleaver.inTouch.category,
                mainActivity: Y.secured.uspCategory.careleaver.mainActivity.category,
                accommodation: Y.secured.uspCategory.careleaver.accommodation.category,
                accommodationSuitability: Y.uspCategory.careleaver.accommodationSuitability.category
            };

            var resultsConfig = Y.merge({ searchConfig: config.resultsConfig }, { codedEntries: codedEntries });
            resultsConfig.searchConfig.codedEntries = codedEntries;

            this.careLeaverResults = new Y.app.careleaver.CareLeaverResults(resultsConfig).addTarget(this);

            this._evtHandlers = [
                this.on('*:addCareLeaver', this.showCareLeaverAddDialog, this),
                this.on('*:viewCareLeaver', this.showCareLeaverViewDialog, this),
                this.on('*:prepareRemoveCareLeaver', this.handlePrepareRemoveCareLeaverDialog, this),
                this.on('*:saved', this.careLeaverResults.reload, this.careLeaverResults),
                Y.on('deletion:removeSaved', this.careLeaverResults.reload, this.careLeaverResults)
            ];
        },

        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(this.careLeaverResults.render().get('container'));
            this.get('container').setHTML(contentNode);
            return this;
        },

        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;

            this.careLeaverDialog.removeTarget(this);
            this.careLeaverDialog.destroy();
            delete this.careLeaverDialog;

            this.careLeaverResults.removeTarget(this);
            this.careLeaverResults.destroy();
            delete this.careLeaverResults;
        },

        showCareLeaverAddDialog: function (e) {
            var showWelshView = this.get('showWelshView'),
                config,
                view;
            if (showWelshView) {  
            	config = this.get('dialogConfig.views.addWelsh.config');
            	view = 'addWelsh';
            } else {
            	config = this.get('dialogConfig.views.add.config');
            	view = 'add';
            }
            this.careLeaverDialog.showView(view, {
                model: new Y.app.careleaver.NewCareLeaverForm({
                    url: config.url
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        handlePrepareRemoveCareLeaverDialog: function (e) {
            Y.fire('removeDialog:prepareForIndividualCareLeaverRemove', {
                id: e.record.get('id')
            });
        },


        showCareLeaverViewDialog: function (e) {
            var record = e.record,
                showWelshView = this.get('showWelshView'),
                view,
                config;
            
            if (showWelshView) {  
            	config = this.get('dialogConfig.views.viewWelsh.config');
            	view = 'viewWelsh';
            } else {
            	config = this.get('dialogConfig.views.view.config');
            	view = 'view';
            }
            
            this.careLeaverDialog.showView(view, {
                model: new Y.usp.careleaver.CareLeaver({
                    url: Y.Lang.sub(config.url, {
                        id: record.get('id')
                    })
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, {
                modelLoad: true
            });
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'promise',
        'view',
        'app-toolbar',
        'careleaver-dialog',
        'careleaver-results',
        'usp-careleaver-CareLeaver',
        'model-errors-plugin',
        'usp-deletion-DeletionRequest',
        'usp-deletion-NewDeletionRequest',
        'remove-controller-view',
        'secured-categories-careleaver-component-CareLeaverEligibility',
        'secured-categories-careleaver-component-MainActivity',
        'secured-categories-careleaver-component-Accommodation'
    ]
});