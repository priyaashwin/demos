YUI.add('careleaver-results', function (Y) {
    'use strict';

    var A = Y.Array,
        L = Y.Lang,
        E = Y.Escape,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates,
        APPFormatters = Y.app.ColumnFormatters;

    Y.namespace('app.careleaver').CareLeaverResults = Y.Base.create('careleaverResults', Y.usp.app.Results, [], {

        getResultsListModel: function (config) {
            var subject = config.subject;

            return new Y.usp.careleaver.PaginatedCareLeaverList({
                url: L.sub(config.url, {
                    subjectId: subject.subjectId
                })
            });
        },
        getColumnConfiguration: function (config) {
            var labels = config.labels || {},
                permissions = config.permissions;

            if (config.showWelshView) {
                return ([{
                        key: 'contactDate',
                        label: labels.results.contactDate,
                        sortable: true,
                        width: '25%',
                        formatter: USPFormatters.date,
                        cellTemplate: USPTemplates.clickable(labels.results.options.view.title, 'viewCareLeaver', permissions.canView)
                    },
                    {
                        label: labels.results.inTouch,
                        key: 'inTouch',
                        formatter: USPFormatters.codedEntry,
                        codedEntries: Y.uspCategory.careleaver.inTouch.category.codedEntries,
                        width: '25%'
                    },
                    {
                        label: labels.results.eligibility,
                        key: 'careLeaverEligibility',
                        formatter: USPFormatters.codedEntry,
                        codedEntries: Y.secured.uspCategory.careleaver.careLeaverEligibility.category.codedEntries,
                        width: '30%'
                    },
                    {
                        label: labels.results.actions,
                        className: 'pure-table-actions',
                        width: '20%',
                        formatter: USPFormatters.actions,
                        items: [{
                            clazz: 'viewCareLeaver',
                            label: labels.results.options.view.label,
                            title: labels.results.options.view.title,
                            visible: permissions.canView
                        }, {
                            clazz: 'prepareRemoveCareLeaver',
                            label: labels.results.options.remove.label,
                            title: labels.results.options.remove.title,
                            visible: permissions.canRemoveCareLeaver
                        }]
                    }
                ]);

            }
            return ([{
                    key: 'contactDate',
                    label: labels.results.contactDate,
                    sortable: true,
                    width: '15%',
                    formatter: USPFormatters.date,
                    cellTemplate: USPTemplates.clickable(labels.results.options.view.title, 'viewCareLeaver', permissions.canView)
                },
                {
                    label: labels.results.inTouch,
                    key: 'inTouch',
                    formatter: USPFormatters.codedEntry,
                    codedEntries: Y.uspCategory.careleaver.inTouch.category.codedEntries,
                    width: '15%'
                },
                {
                    label: labels.results.eligibility,
                    key: 'careLeaverEligibility',
                    formatter: USPFormatters.codedEntry,
                    codedEntries: Y.secured.uspCategory.careleaver.careLeaverEligibility.category.codedEntries,
                    width: '30%'
                },
                {
                    label: labels.results.reasonQualifying,
                    key: 'reasonQualifying',
                    formatter: USPFormatters.codedEntry,
                    codedEntries: Y.uspCategory.careleaver.reasonQualifying.category.codedEntries,
                    width: '30%'
                },
                {
                    label: labels.results.actions,
                    className: 'pure-table-actions',
                    width: '10%',
                    formatter: USPFormatters.actions,
                    items: [{
                        clazz: 'viewCareLeaver',
                        label: labels.results.options.view.label,
                        title: labels.results.options.view.title,
                        visible: permissions.canView
                    }, {
                        clazz: 'prepareRemoveCareLeaver',
                        label: labels.results.options.remove.label,
                        title: labels.results.options.remove.title,
                        visible: permissions.canRemoveCareLeaver
                    }]
                }
            ]);
        }

    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function () {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }];
                }
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'app-results',
        'secured-categories-careleaver-component-CareLeaverEligibility',
        'categories-careleaver-component-ReasonQualifying',
        'categories-careleaver-component-InTouch',
        'secured-categories-careleaver-component-MainActivity',
        'secured-categories-careleaver-component-Accommodation',
        'categories-careleaver-component-AccommodationSuitability',
        'usp-careleaver-CareLeaver',
        'caserecording-results-formatters',
        'results-formatters',
        'results-templates'
    ]
});