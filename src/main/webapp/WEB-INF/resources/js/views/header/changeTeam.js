YUI.add('change-team-view', function(Y) {
    'use-strict';

    Y.namespace('app.home').ChangeTeamControllerView = Y.Base.create('changeTeamControllerView', Y.View, [], {
      template: Y.Handlebars.templates['changeTeamsList'],
      events: {
        "ul li.pure-menu-item a": {
          click: "_handleChangeTeam"
        }
      },
      initializer: function(config) {
        var container = this.get('container'),
            getTeamsURL = config.getTeamsURL;

        //plug in button menu for graph style
        container.plug(Y.Plugin.usp.SplitButtonPlugin);

        this._evtHandlers = [
          this.after("teamsChange", this.render, this)
        ];

        var model = new Y.usp.Model({url: getTeamsURL}).load(function(err, res) {
          if (res.response) {
            this.set('teams', JSON.parse(res.response));
          }
        }.bind(this));
      },
      render: function() {
        var container = this.get('container'),
            selectedProfileDescription = this.get('selectedProfileDescription'),
            selectedProfileId = this.get('selectedProfileId'),
            teams = this.get('teams'),
            teamsList,
            html;

        if (teams) {
          teamsList = teams.filter(function(team) {
            return team.profileId.toString() !== selectedProfileId;
          });
        }

        html = this.template({
          teams: teamsList,
          selectedProfileDescription: selectedProfileDescription
        });

        container.setHTML(html);

        return this;
      },
      destructor: function() {
        this.get('container').unplug(Y.Plugin.usp.SplitButtonPlugin);

        this._evtHandlers.forEach(function(handler) {
          handler.detach();
        });
        delete this._evtHandlers;
      },
      _handleChangeTeam: function(e) {
        var switchTeamURL = this.get('switchTeamURL'),
            rootURL = this.get('rootURL'),
            profileId = e.target._node.dataset.id;

        new Y.usp.Model({
          url: switchTeamURL,
          profileId: profileId
        }).save(function(err, res) {
          if(!err) {
            sessionStorage.clear();
            document.location.assign(rootURL);
          }
        });
      }
    }, {
      ATTRS: {
        teams: [],
        selectedProfileDescription: null,
        selectedProfileId: null
      }
    });
}, '0.0.1', {
  requires: [
    'yui-base',
    'view',
    'event-custom',
    'usp-model',
    'split-button-plugin',
    'handlebars',
    'handlebars-header-templates',
    'handlebars-helpers'
  ]
});
