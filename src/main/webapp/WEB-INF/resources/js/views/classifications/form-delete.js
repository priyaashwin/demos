
YUI.add('classification-form-delete', function (Y) {


	
	var DeleteForm = Y.Base.create('deleteClassification', Y.usp.classification.ClassificationAssignmentView, [], {

		initializer: function(config) { 
			this.template = config.template || Y.Handlebars.templates["classificationAssignmentDeleteDialog"];			
		},

		render: function() {
			
            this.constructor.superclass.render.call(this);
         
			return this;
		}
				
	}, { 
		
		ATTRS: {

			model: {
				valueFn: function() {
					return new Y.app.classification.DeleteClassificationModel({	          
						url: this.get('url'),
						deletedUrl: this.get('deletedUrl'),
						originalUrl: this.get('originalUrl')
					}); 
				}
			},
			
			narrative: {			
				value: null		
			},
		
			subject: {			    								
				value: {			    											
					name: '',			    											
					id: ''			    								
				}			    					
			}		    			
		}
	});
	
	
	Y.namespace('app.classification').DeleteForm = DeleteForm;
	

}, '0.0.1', {
	
	requires: ['yui-base',
	           'event-custom',
	           'calendar-popup',
	           'handlebars-helpers',
	           'handlebars-classification-templates',
	           'usp-date',
	           'usp-classification-ClassificationAssignment',
	           'classification-models',
	           'classification-autocomplete'
	           
	          
	          ]
});