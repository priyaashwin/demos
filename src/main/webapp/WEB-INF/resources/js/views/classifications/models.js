


YUI.add('classification-models', function (Y) {
	

	Y.namespace('app.classification').AddClassificationModel = Y.Base.create('addClassificationModel', 
   			Y.usp.person.NewManagedClassificationAssignment, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {    
		  form:'#classification-add-form'   
	});

	Y.namespace('app.classification').EndClassificationModel = Y.Base.create('endClassificationModel', 
			Y.usp.classification.ClassificationAssignment, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {     		  
  		endUrl:'/eclipse/rest/managedClassificationAssignment/{id}/status?action=close',
  		originalUrl: '/eclipse/rest/classificationAssignment/{id}',
		form:'#personClassificationAssignmentEndForm'   	 
	});
	
	Y.namespace('app.classification').DeleteClassificationModel = Y.Base.create('deleteClassificationModel',
			Y.usp.classification.ClassificationAssignment, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {     		  
    	deletedUrl:'/eclipse/rest/managedClassificationAssignment/{id}/status?action=deleted',
    	originalUrl: '/eclipse/rest/classificationAssignment/{id}',
		form:'#personClassificationAssignmentDeleteForm'   	 
	});


}, '0.0.1', {
	
	requires: ['yui-base',
	           'event-custom',
	           'model', 
	           'model-form-link', 
	           'model-transmogrify',		    
	           'usp-classification-ClassificationAssignment',
	           'usp-person-NewManagedClassificationAssignment',
	           'usp-classification-UpdateEndClassificationAssignment',
	           'usp-date'
	          ]
});