
YUI.add('classification-form-add', function (Y) {

	
	var AddForm = Y.Base.create('addClassification', Y.View, [Y.app.mixin.MultiView], {
		initializer: function(config) {				
			var subject=config.subject||{};
			
			this.set('model', new Y.app.classification.AddClassificationModel({
				url: config.url,
				startDate:new Date().getTime(),
				subjectId:subject.personId,
				subjectType:subject.type.toUpperCase()						
			}));
		
		
			this.template = config.template || Y.Handlebars.templates['classificationAddClassification'];
			this.set('subject', subject);
			this.on('autocomplete:selected', this._onSelected, this);
			
			this.startDateCalendar = new Y.usp.CalendarPopup({});
		},
		
		destructor: function() {			
			if(this.startDateCalendar) {
				this.startDateCalendar.destroy();
			}
		},
	          			
		render: function() {	
				
			var container = this.get('container');
				
			container.setHTML(this.template({					
				labels: this.get('labels'),					
				modelData: this.get('model').toJSON(),
				otherData: this.get('otherData')
			}));
				
			
			//set the calendar input node
			this.startDateCalendar.set('inputNode',container.one('#classification-start-date'));
			//render the calendar
			this.startDateCalendar.render();

			return this;
		
		},
		
		getViews: function() {

			return [
			   { 
				   name: 'narrative',
				   hook:'#classification-add-form-narrative', 				   
				   narrative: this.get('narrative'),
				   subject: this.get('subject'),
				   type: Y.app.views.BaseNarrative 
			   },
			   { 				   
				   name: 'autocomplete',
				   hook:'#classification-add-form-autocomplete', 				   
				   type: Y.app.classification.AutoCompleteView
			   }
			   
			];
		},
		
		_onSelected: function(e) {			
			this.set('selectedClassification', e.selected);
		}
		
	

	},{ 
		ATTRS: {
			    		
			model: {				
				value:undefined
			},
						
			narrative: {
				value: null
			},

			
			subject: {
				value: {
					name: '',
					id: '',
					type:''
				}
			},
			
			url: {
				value: ''
			}
			    		
		}
		
	});	

	Y.namespace('app.classification').AddForm = AddForm;
	

}, '0.0.1', {
	
	requires: ['yui-base',
	           'event-custom',
	           'handlebars-helpers',
	           'handlebars-classification-templates',         	           	           
	           'calendar-popup',	          
	           'app-mixin-multi-view',
	           'app-views-narrative',
	           'classification-autocomplete',
	           'classification-models',
	    	   'usp-date'
	          ]
});