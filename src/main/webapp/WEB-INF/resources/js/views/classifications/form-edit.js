
YUI.add('classification-form-edit', function (Y) {


	var EditForm = Y.Base.create('editClassification', Y.usp.classification.UpdateEndClassificationAssignmentView, [], {

		initializer: function(config) { 
			this.template = config.template || Y.Handlebars.templates["classificationAssignmentEndDialog"];			
		},

		render: function() {
			
			var model = this.get('model');

            this.constructor.superclass.render.call(this);
            
            this.initEndReason();
            
            this.endDateCalendar = new Y.usp.CalendarPopup({				
				inputNode: this.get('container').one('#classification_endDate')		
			});
            
            if(!model.get('endDate')){
            	model.set('endDate' , Y.USPDate.parseDateTime(new Date()));
            }
			
            this.endDateCalendar.render();
            
			return this;
		},
        initEndReason: function() {
        	var endReason = this.get('container').one('#classificationEndForm_endReasonSelect');
            if (endReason) { 
                Y.FUtil.setSelectOptions(endReason, Y.Object.values(Y.uspCategory.classification.endReason.category.getActiveCodedEntries()), null, null, true, true,'code');
             }
         }
				
	}, { 
		
		ATTRS: {

			model: {
				valueFn: function() {
					return new Y.app.classification.EndClassificationModel({	          
						url: this.get('url')      
					}); 
				}
			},
			
			narrative: {			
				value: null		
			},
		
			subject: {			    								
				value: {			    											
					name: '',			    											
					id: ''			    								
				}			    					
			}		    			
		}
	});
	
	
	Y.namespace('app.classification').EditForm = EditForm;
	
	
	

}, '0.0.1', {
	
	requires: ['yui-base',
	           'event-custom',
	           'calendar-popup',
	           'handlebars-helpers',
	           'handlebars-classification-templates',
	           'usp-date',
	           'usp-classification-UpdateEndClassificationAssignment',
	           'classification-models',
	           'classification-autocomplete',
	           'categories-classification-component-EndReason'
	          
	          ]
});