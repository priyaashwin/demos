
YUI.add('classification-autocomplete', function (Y) {

    var L = Y.Lang, O = Y.Object, 
    
    DEFAULT_PARAMS = {								
		status:["ACTIVE"],
		pageNumber:[1], 				
		pageSize:[-1], 				
		s:[[{"id":"asc"}]]			
	},
	
    
  /**
   * @class AutoCompleteView 
   * Y.app.views.BaseAC:  Manages AutoComplete Widget Life cycle.
   * Y.app.classification.MixinBase: Label origin and sugar, added in config.jsp. 
   */
    
	AutoCompleteView = Y.Base.create('autocomplete', Y.app.views.BaseAC, [Y.app.classification.MixinBase], {
		
		events: {
			'a[data-action="clear"]': { click: '_onClickClear' }
		},
    	
    	initializer: function(config) {			
    		this.template = config.template || Y.Handlebars.templates.classificationAC;	
		},
		getConfig: function() {
			return {
			    maxResults: this.get('maxResults'),
			    resultHighlighter: 'phraseMatch',
			    resultFilters: [this.filter],
			    resultTextLocator: 'display'
			};
		},
		filter: function(query, results, caseSensitive) {
			
	        var search=new RegExp(query,'i');
	        var matched = results.filter(function(val){
	            if((val.raw.notManuallyAssignable && !val.raw.canAddNotManuallyAssignable) || val.raw.restricted) {
	              return false;
	            }
	            var partialWordMatch =  val.raw.display.match(search)?true:false;
	            var camelMatch = val.raw.display.replace(/\W*(\w)\w*/g, '$1').toUpperCase().indexOf(query.toUpperCase())!=-1;
	            return partialWordMatch || camelMatch;	            
	        });
	        
	        return matched;

		},

		_onClickClear: function(e) {			
			e.preventDefault();
			this.resetAC();
		},
		


	},{ 
		ATTRS: {
			
			maxResults: {
				value: 50
			},
			data: {
				valueFn: function() {
					var data=[], names=[], 

					results = new Y.usp.classification.PaginatedClassificationWithClassificationGroupsListList({
						url: this.get('url')
					});
					var canAddNotManuallyAssignable = this.getCanAddNotManuallyAssignable();
					results.load({headers:{Accept:results.model.MIME_TYPE+'+json;requestedAccess=WRITE'}},Y.bind(function(err, res) {				
						
						results.each(function(model, index){
			    			names = model.get('classificationGroupsList'); 
			    			names.reverse();
			    			names.push(model.get('name'));
			    			data.push({id: model.get('id'), notManuallyAssignable: model.get('notManuallyAssignable'), canAddNotManuallyAssignable: canAddNotManuallyAssignable, restricted: model.get('restricted'), display: names.join("->")});
			    		});
						
						this.acWidget.ac.set('source', data);
			    		
					}, this));
					
					return results;

				}
			
			},
			
			labels: {
				writeOnce: 'initOnly',
				valueFn: function() {
					var labels = this.getLabels(this.name) || {};
					labels.popover = this.getLabels('popover', this.name); 
					return labels;
				}
			},

	
			params: {							
				value: DEFAULT_PARAMS,
				getter: function(val) {				
					return this.getParams(val);    
				}			
			},
			
			
			url: {
				writeOnce: 'initOnly',
				value: '/classification/?',
				getter: function(val) {
					return (this.getRESTUrl(val) + this.get('params'));
				}
			}
			    		
		}
		
	});	

	

	Y.namespace('app.classification').AutoCompleteView = AutoCompleteView;
	

}, '0.0.1', {	
	requires: [
	  'yui-base', 'event-custom',       
	  'usp-person-NewManagedClassificationAssignment', 	           
	  'usp-classification-ClassificationWithClassificationGroupsList',	           
	  'app-views-autocomplete',	           
	  'classification-base'
	]
});