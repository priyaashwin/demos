

YUI.add('classification-mixin', function (Y) {


	var L = Y.Lang, O = Y.Object,

	Mixin = function() {}; 	

	Mixin.ATTRS = {
			
			commonLabels: {
				value: {}
			},
						
			dialogTitles: {				
				value: {}
			},
			
			icon: {
				value: 'fa-tags'
			},
			
			narratives: {
				value: {}
			},
						
			onSuccessMessages: {				
				value: {}
			}
	};
	
	Mixin.prototype = {
	  		
    		initializer: function(config) {
    				
    			O.each(this.views, function(view, name) {                     					
    					
    				view._common= {				  	            		    						
    					dialogTitle: config.dialogTitles && config.dialogTitles[name] || config.dialogTitle,	
    					onSuccessMessage: config.onSuccessMessages[name] || config.onSuccessMessage,	
    					narrative: config.narratives && config.narratives[name] || ''   					
    				};
    				
    			}, this);
    			
    			Y.Do.before(this._$$showView, this, 'showView', this);
    			
    		},
    			
    			
    		_$$showView: function(name, config, b, c) {	
    				   				
    			var view = this.views[name];
   				
    			config = Y.merge(view._common, config);			   							  				
    			view.headerTemplate = L.sub(view.headerTemplate || '', {    					
    				icon: this.get('icon'),
    				title: config && config.dialogTitle   				
    			});

    		}
    };
	
	Y.namespace('app.classification').Mixin = Mixin;
    			

    
}, '0.0.1', {

	requires: ['yui-base', 'event-custom']
    
});
    	