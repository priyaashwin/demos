YUI.add('notification-recipients', function (Y) {
    'use strict';

    var USER_ICON = 'fa-person',
        TEAM_ICON = 'eclipse-team';

    Y.namespace('app.notification').AddRecipientSelect = Y.Base.create('addRecipientSelect', Y.View, [Y.Plugin.Host], {
        initializer: function () {
            this.plug(Y.Plugin.usp.MenuPlugin);
        },
        template: Y.Handlebars.templates.addRecipient,
        events: {
            '[data-action="add"]': {
                click: 'addRecipientForType'
            }
        },
        addRecipientForType: function (e) {
            e.preventDefault();

            this.fire('add', {
                type: e.currentTarget.getData('type')
            });
        },
        render: function () {
            var container = this.get('container');

            container.setHTML(this.template({
                types: this.get('types'),
                labels: this.get('labels')
            }));
            return this;
        }
    }, {
        ATTRS: {
            labels: {
                values: {}
            },
            types: {
                value: [{
                    type: 'USER',
                    label: 'User',
                    icon: USER_ICON
                }, {
                    type: 'TEAM',
                    label: 'Team',
                    icon: TEAM_ICON
                }]
            }
        }
    });

    Y.namespace('app.notification').RecipientPersonAutocompleteView = Y.Base.create('recipientPersonAutocompleteView', Y.View, [], {
        initializer: function (config) {
            this.personAutocompleteView = new Y.app.PersonAutoCompleteResultView({
                autocompleteURL: config.autocompleteURL,
                rowTemplate: 'ROW_TEMPLATE_SIMPLE',
                labels: config.labels
            });

            this.personAutocompleteView.addTarget(this);
        },
        destructor: function () {
            if (this.personAutocompleteView) {
                this.personAutocompleteView.removeTarget(this);
                this.personAutocompleteView.destroy();
                delete this.personAutocompleteView;
            }
        },
        render: function () {
            var container = this.get('container');

            container.setHTML(this.personAutocompleteView.render().get('container'));

            return this;
        }
    }, {
        ATTRS: {
            autocompleteURL: {
                value: null
            },
            labels: {
                value: {}
            }
        }
    });

    Y.namespace('app.notification').RecipientOrganisationAutocompleteView = Y.Base.create('recipientOrganisationAutocompleteView', Y.View, [], {
        initializer: function (config) {
            this.organisationAutocompleteView = new Y.app.OrganisationAutoCompleteResultView({
                autocompleteURL: config.autocompleteURL,
                rowTemplate: 'ROW_TEMPLATE_SIMPLE',
                labels: config.labels
            });

            this.organisationAutocompleteView.addTarget(this);
        },
        destructor: function () {
            if (this.organisationAutocompleteView) {
                this.organisationAutocompleteView.removeTarget(this);
                this.organisationAutocompleteView.destroy();
                delete this.organisationAutocompleteView;
            }
        },
        render: function () {
            var container = this.get('container');

            container.setHTML(this.organisationAutocompleteView.render().get('container'));
            return this;
        }
    }, {
        ATTRS: {
            autocompleteURL: {
                value: null
            },
            labels: {
                value: {}
            }
        }
    });

    Y.namespace('app.notification').AddRecipientView = Y.Base.create('addRecipient', Y.View, [], {
        initializer: function () {
            this._evts = [
                this.after('typeChange', this.handleTypeChange, this),
                this.after('autocompleteViewChange', this.render, this)
            ];
        },
        destructor: function () {
            this._evts.forEach(function (handle) {
                handle.detach();
            });

            delete this._evts;
        },
        handleTypeChange: function (e) {
            var autocompleteView = this.get('autocompleteView'),
                labels = this.get('labels') || {},
                personAutocompleteURL = this.get('personAutocompleteURL'),
                organisationAutocompleteURL = this.get('organisationAutocompleteURL');

            if (autocompleteView) {
                autocompleteView.removeTarget(this);
                autocompleteView.destroy();
                autocompleteView = null;
            }

            switch (e.newVal) {
            case 'USER':
                autocompleteView = new Y.app.notification.RecipientPersonAutocompleteView({
                    autocompleteURL: personAutocompleteURL,
                    labels: labels.personAutocomplete
                }).addTarget(this);
                break;

            case 'TEAM':
                autocompleteView = new Y.app.notification.RecipientOrganisationAutocompleteView({
                    autocompleteURL: organisationAutocompleteURL,
                    labels: labels.organisationAutocomplete
                }).addTarget(this);
                break;
            }
            this.set('autocompleteView', autocompleteView);
        },
        render: function () {
            var container = this.get('container'),
                autocompleteView = this.get('autocompleteView');
            if (autocompleteView) {
                container.setHTML(autocompleteView.render().get('container'));
            } else {
                container.setHTML('<div></div>');
            }

            return this;
        }
    }, {
        ATTRS: {
            type: {
                value: null
            },
            autocompleteView: {
                values: null
            },
            personAutocompleteURL: {
                value: null
            },
            organisationAutocompleteURL: {
                value: null
            },
            labels: {
                value: {}
            }
        }
    });

    Y.namespace('app.notification').RecipientListView = Y.Base.create('recipientListView', Y.View, [], {
        template: Y.Handlebars.templates.recipientList,
        events: {
            '[data-action="remove"]': {
                click: 'removeRecipient'
            },
            'input[type="checkbox"][name="includeDirectTargets"]': {
                change: 'toggleIncludeDirectTargets'
            }
        },
        initializer: function (config) {
            this.addRecipientSelect = new Y.app.notification.AddRecipientSelect({
                labels: config.labels
            }).addTarget(this);
            this.addRecipientView = new Y.app.notification.AddRecipientView({
                labels: config.labels,
                personAutocompleteURL: config.personAutocompleteURL,
                organisationAutocompleteURL: config.organisationAutocompleteURL,
                //pass the type through to the add recipient view - this will determine the 
                //pick list presented
                type: this.get('currentlyAddingType'),
                hideIncludeDirectTargets: this.get('hideIncludeDirectTargets')
            }).addTarget(this);

            this._evts = [
                this.after('notificationTargetsChange', this.render, this),
                this.after('currentlyAddingTypeChange', this.handleCurrentlyAddingTypeChange, this),
                this.on('addRecipientSelect:add', this.handleAddRecipientSelect, this),
                this.after('*:select', this.handleRecipientSelection, this)
            ];
        },
        handleRecipientSelection: function (e) {
            var currentlyAddingType = this.get('currentlyAddingType'),
                notificationTargets = this.get('notificationTargets') || {},
                recipients = notificationTargets.targets || [],
                selection = e.result.raw || {};

            //don't allow duplicates
            if (!recipients.find(function (recipient) {
                    return (recipient.id === selection.id && recipient.type === currentlyAddingType);
                })) {
                switch (currentlyAddingType) {
                case 'USER':
                    recipients.push({
                        icon: USER_ICON,
                        name: selection.name,
                        identifier: selection.personIdentifier,
                        id: selection.id,
                        type: currentlyAddingType
                    });
                    break;
                case 'TEAM':
                    recipients.push({
                        icon: TEAM_ICON,
                        name: selection.name,
                        identifier: selection.organisationIdentifier,
                        id: selection.id,
                        type: currentlyAddingType
                    });

                    break;
                }
            }

            //update recipients - reset to trigger change handler
            this.set('notificationTargets', Y.merge(notificationTargets, {
                targets: recipients
            }));

            //not currently adding
            this.set('currentlyAddingType', null);
        },
        toggleIncludeDirectTargets: function (e) {
            var notificationTargets = this.get('notificationTargets') || {};

            this.set('notificationTargets', Y.merge(notificationTargets, {
                includeDirectTargets: e.currentTarget.get('checked')
            }));
        },
        handleAddRecipientSelect: function (e) {
            this.set('currentlyAddingType', e.type);
        },
        handleCurrentlyAddingTypeChange: function (e) {
            this.addRecipientView.set('type', e.newVal);
        },
        removeRecipient: function (e) {
            var notificationTargets = this.get('notificationTargets') || {},
                recipients = notificationTargets.targets || [],
                currentTarget = e.currentTarget,
                idx;

            e.preventDefault();

            idx = Number(currentTarget.getData('idx'));

            //splice to remove the recipient at idx
            recipients.splice(idx, 1);

            this.set('notificationTargets', Y.merge(notificationTargets, {
                targets: recipients
            }));
        },
        destructor: function () {
            this._evts.forEach(function (handle) {
                handle.detach();
            });

            delete this._evts;

            this.addRecipientSelect.removeTarget(this);
            this.addRecipientSelect.destroy();
            delete this.addRecipientSelect;

            this.addRecipientView.removeTarget(this);
            this.addRecipientView.destroy();
            delete this.addRecipientView;
        },
        render: function () {
            var container = this.get('container'),
                labels = this.get('labels'),
                notificationTargets = this.get('notificationTargets'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            if (notificationTargets) {
                contentNode.setHTML(this.template({
                    labels: labels,
                    notificationTargets: notificationTargets
                }));

                contentNode.one('#addRecipientButton').setHTML(this.addRecipientSelect.render().get('container'));
                contentNode.one('#addRecipientAutocomplete').setHTML(this.addRecipientView.render().get('container'));

            } else {
                //no content if notifications target undefined
                contentNode.setHTML('<div></div>');
            }
            if (this.get('hideIncludeDirectTargets') && contentNode.one('#includeDirectTargets_container')) {
                contentNode.one('#includeDirectTargets_container').hide();
            }
            container.setHTML(contentNode);

            //re-align dialog
            this.fire('align');

            return this;
        }
    }, {
        ATTRS: {
            notificationTargets: {
                value: undefined
            },
            /**
             * Holds the recipient type currently
             * being added by the user. Once the type
             * is added, or the add is canceled, this
             * attribute will revert to undefined
             *
             * @attribute currentlyAddingType
             */
            currentlyAddingType: {
                value: null
            },
            personAutocompleteURL: {
                value: null
            },
            organisationAutocompleteURL: {
                value: null
            },
            labels: {
                value: {}
            }
        }
    });

    Y.namespace('app.notification').RecipientsView = Y.Base.create('recipientsView', Y.View, [], {
        template: Y.Handlebars.templates.recipientDialog,
        events: {
            'input[type="checkbox"][name="send-notifications"]': {
                change: 'toggleEnableNotification'
            }
        },
        initializer: function (config) {
            var model = config.model;

            this._recipientEvts = [
                this.after('enableNotificationChange', this.handleEnableNotificationChange, this)
            ];

            this.recipientListView = new Y.app.notification.RecipientListView({
                labels: config.labels,
                personAutocompleteURL: config.personAutocompleteURL,
                organisationAutocompleteURL: config.organisationAutocompleteURL,
                notificationTargets: model.get('notificationTargets'),
                hideIncludeDirectTargets: this.get('subject').subjectType === 'organisation'
            }).addTarget(this);
        },
        render: function () {
            var container = this.get('container'),
                labels = this.get('labels'),
                enableNotification = this.get('enableNotification');

            container.setHTML(this.template({
                enableNotification: enableNotification,
                labels: labels
            }));

            container.one('#recipientsView').setHTML(this.recipientListView.render().get('container'));

            return this;
        },
        destructor: function () {
            this._recipientEvts.forEach(function (handle) {
                handle.detach();
            });

            delete this._recipientEvts;

            this.recipientListView.removeTarget(this);
            this.recipientListView.destroy();
            delete this.recipientListView;
        },
        handleEnableNotificationChange: function (e) {
            if (!e.newVal) {
                //clear down the previous notification targets
                this.recipientListView.set('notificationTargets', undefined);
            } else {
                this.recipientListView.set('notificationTargets', {
                    //default includes direct targets in notification
                    //i.e. allocated worker
                    includeDirectTargets: this.get('subject').subjectType !== 'organisation',
                    targets: []
                });
            }
        },
        getNotificationTargetsVO: function () {
            if (!this.get('enableNotification')) {
                //If notifications are not enabled - then notification targets is not valid
                return undefined;
            }

            var notificationTargets = this.recipientListView.get('notificationTargets');

            return new Y.usp.event.notification.NotificationTargets({
                includeDirectTargets: notificationTargets.includeDirectTargets,
                targets: notificationTargets.targets.map(function (target) {
                    var type;

                    switch (target.type) {
                    case 'USER':
                        type = 'PERSON';
                        break;
                    case 'TEAM':
                        type = 'ORGANISATION';
                        break;
                    }

                    return ({
                        id: target.id,
                        type: type
                    });
                })
            });
        },
        toggleEnableNotification: function (e) {
            //toggle model attribute that will enable show/hide of notification panel 
            this.set('enableNotification', e.currentTarget.get('checked'));
        }
    }, {
        ATTRS: {
            enableNotification: {
                value: false
            },
            labels: {
                value: {}
            },
            personAutocompleteURL: {
                value: null
            },
            organisationAutocompleteURL: {
                value: null
            }
        }
    });

    Y.namespace('usp.event.notification').NotificationTargets = Y.Base.create('notificationTargets', Y.usp.Model, [], {}, {
        ATTRS: {
            /**
             * Default to include direct targets in notification
             * i.e. allocated worker
             */
            includeDirectTargets: {
                value: true
            },
            targets: {
                value: []
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'person-autocomplete-view',
        'organisation-autocomplete-view',
        'handlebars-notification-templates',
        'pluginhost-base',
        'menu-plugin',
        'usp-model'
    ]
});