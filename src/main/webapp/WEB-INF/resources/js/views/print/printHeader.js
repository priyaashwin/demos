

YUI.add('print-header-view', function(Y) {
	
	var PrintHeaderView = Y.Base.create('printHeaderView', Y.View, [], {
		render: function() {

	        var container = this.get('container');

	        container.setHTML(this.template({
				labels: this.get('labels'),
				model: this.get('model').toJSON()
			}));
	
	        return this;
        }      
	});

	
	Y.namespace('app').PrintHeaderView = PrintHeaderView;



}, '0.0.1', {
  requires: ['yui-base',
  			 'fuzzy-date',
             'view',
             'handlebars',
             'handlebars-helpers'
            ]
});