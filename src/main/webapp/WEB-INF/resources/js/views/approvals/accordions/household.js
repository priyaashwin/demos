YUI.add('household-accordion-views', function(Y) {
    'use-strict';

    var USPFormatters = Y.usp.ColumnFormatters,
        APPFormatters = Y.app.ColumnFormatters;
    
    var getAddressLocation = function(o) {
        var record = o,
        location;
        
        if (record.get('location')) {
        	location = record.get('location');
        } else if (record.get('addressForHousehold')) {
        	location = record.get('addressForHousehold').location || {};
        } else { 
        	location = {};
        }
        return location.location;
    },

    formatAddress = function(o) {
        var record = o.record,
        location = getAddressLocation(record),
        address = USPFormatters.textFormat({
            	value: location
            });
            if(!address){
                if(!!record.get('unknownLocationType')){
                  address = Y.uspCategory.address.unknownLocation.category.codedEntries[record.get('unknownLocationType')].name;
                }
            }
        return ('<div class="pure-g-r"><div class="pure-u-1-2 l-box"><strong>Address:</strong> <span>' + (address || '') + '</span></div>');
    };

    Y.namespace('app.approvals').SameLocationAddressResults = Y.Base.create('sameLocationAddressResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.usp.relationship.PaginatedPersonAddressRelationshipNameList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels;

            return [{
                key: 'personName',
                label: labels.personName,
                width: '20%'
            }, {
                key: 'relationshipName',
                label: labels.relationshipName,
                width: '20%'
            }, {
                key: 'relationshipType',
                label: labels.relationshipType,
                width: '20%'
            }, {
                key: 'dateOfBirth!calculatedDate',
                label: labels.dateOfBirth,
                formatter: USPFormatters.fuzzyDate,
                maskField: 'dateOfBirth.fuzzyDateMask',
                width: '20%'
            }, {
                key: 'age',
                label: labels.age,
                width: '20%'
            }];
        }
    });

    Y.namespace('app.approvals').WiderFamilyAddressResults = Y.Base.create('widerFamilyAddressResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
        	return new Y.usp.relationship.PaginatedAsymmetricPersonPersonRelationshipHouseholdDetailsList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels;

            return [{
                key: 'personVO.name',
                label: labels.personName,
                width: '20%'
            }, {
                key: 'genderSpecificRoleClass',
                label: labels.relationshipName,
                width: '20%'
            }, {
                key: 'personPersonRelationshipType',
                label: labels.relationshipType,
                width: '20%'
            }, {
                key: 'personVO.dateOfBirth!calculatedDate',
                label: labels.dateOfBirth,
                formatter: USPFormatters.fuzzyDate,
                maskField: 'dateOfBirth.fuzzyDateMask',
                width: '20%'
            }, {
                key: 'personVO.age',
                label: labels.age,
                width: '20%'
            }];
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                        cfg: {
                            state: 'expanded',
                            summaryFormatter: formatAddress
                        }
                    }];
                }
            }
        }
    });


    Y.namespace('app.approvals').CurrentHouseholdAccordionView = Y.Base.create('currentHouseholdAccordionView', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            var sameLocationAddressResultsConfig = config.sameLocationAddressResultsConfig || {};
            var widerFamilyAddressResultsConfig = config.widerFamilyAddressResultsConfig || {};

            this.sameLocationAddressResults = new Y.app.approvals.SameLocationAddressResults(sameLocationAddressResultsConfig).addTarget(this);
            this.widerFamilyAddressResults = new Y.app.approvals.WiderFamilyAddressResults(widerFamilyAddressResultsConfig).addTarget(this);

        },
        
        destructor: function() {
            this.sameLocationAddressResults.removeTarget(this);
            this.sameLocationAddressResults.destroy();
            delete this.sameLocationAddressResults;

            this.widerFamilyAddressResults.removeTarget(this);
            this.widerFamilyAddressResults.destroy();
            delete this.widerFamilyAddressResults;
        },
        render: function() {
            Y.app.approvals.CurrentHouseholdAccordionView.superclass.render.call(this);

            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //add the tabs container
            contentNode.append(this.sameLocationAddressResults.render().get('container'));
            contentNode.append(this.widerFamilyAddressResults.render().get('container'));

            this.getAccordionBody().setHTML(contentNode);

            return this;
        }
    });


    Y.namespace('app.approvals').CurrentPlacementResults = Y.Base.create('currentPlacementResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.usp.childlookedafter.PaginatedPlacementWithPlacementSubjectList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels;

            return [{
                key: 'person!name',
                label: labels.personName,
                formatter: APPFormatters.linkable,
                clazz: 'viewPlacementSubjectDetails',
                width: '35%'
            }, {
                key: 'person!dateOfBirth!calculatedDate',
                label: labels.dateOfBirth,
                formatter: USPFormatters.date,
                width: '10%'
            }, {
                key: 'startDate',
                label: labels.startDate,
                formatter: USPFormatters.date,
                width: '20%'
            }, {
                key: 'placementType!name',
                label: labels.placementType,
                width: '35%'
            }];
        }
    });

    Y.namespace('app.approvals').HistoricPlacementResults = Y.Base.create('historicPlacementResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.usp.childlookedafter.PaginatedPlacementWithPlacementSubjectList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels;

            return [{
                key: 'person!name',
                label: labels.personName,
                formatter: APPFormatters.linkable,
                clazz: 'viewPlacementSubjectDetails',
                width: '35%'
            }, {
                key: 'person!dateOfBirth!calculatedDate',
                label: labels.dateOfBirth,
                formatter: USPFormatters.date,
                width: '10%'
            }, {
                key: 'startDate',
                label: labels.startDate,
                formatter: USPFormatters.date,
                width: '10%'
            }, {
                key: 'endDate',
                label: labels.endDate,
                formatter: USPFormatters.date,
                width: '10%'
            }, {
                key: 'placementType!name',
                label: labels.placementType,
                width: '35%'
            }];
        }
    });
    Y.namespace('app.approvals').PlacementAccordionView = Y.Base.create('placementAccordionView', Y.usp.AccordionTablePanel, [], {
        initializer: function(config) {
            var historicPlacementResultsConfig = config.historicPlacementResultsConfig || {};
            var currentPlacementResultsConfig = config.currentPlacementResultsConfig || {};

            this.historicPlacementResults = new Y.app.approvals.HistoricPlacementResults(historicPlacementResultsConfig).addTarget(this);
            this.currentPlacementResults = new Y.app.approvals.CurrentPlacementResults(currentPlacementResultsConfig).addTarget(this);

        },
        destructor: function() {
            this.historicPlacementResults.removeTarget(this);
            this.historicPlacementResults.destroy();
            delete this.historicPlacementResults;

            this.currentPlacementResults.removeTarget(this);
            this.currentPlacementResults.destroy();
            delete this.currentPlacementResults;
        },
        render: function() {
            Y.app.approvals.PlacementAccordionView.superclass.render.call(this);

            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //add the tabs container
            contentNode.append(this.currentPlacementResults.render().get('container'));
            contentNode.append(this.historicPlacementResults.render().get('container'));

            this.getAccordionBody().setHTML(contentNode);

            return this;
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'accordion-panel',
        'results-formatters',
        'caserecording-results-formatters',
        'app-results',
        'usp-relationship-AsymmetricPersonPersonRelationshipHouseholdDetails',
        'usp-relationship-PersonAddressRelationshipName',
        'usp-childlookedafter-PlacementWithPlacementSubject',
        'results-templates',
        'usp-relationshipsrecording-PersonDetails',
        'results-table',
        'results-table-summary-row-plugin',
        'categories-address-component-UnknownLocation'
    ]
});