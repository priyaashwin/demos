YUI.add('approval-registrations-view', function (Y) {
    /* global uspApprovals, ReactDOM */
    'use-strict';

    Y.namespace('app.approvals').RegistrationsView = Y.Base.create('registrationsView', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            this.registrationsContentView = new Y.app.approvals.RegistrationsContentView(Y.merge(config.registration, {
                personDetails: config.personDetails,
                permissions: config.permissions
            })).addTarget(this);
        },

        render: function () {
            Y.app.approvals.RegistrationsView.superclass.render.call(this);

            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.registrationsContentView.render().get('container'));
            this.getAccordionBody().setHTML(contentNode);

            return this;
        },
        reload: function () {
            if (this.registrationsContentView) {
                this.registrationsContentView.refresh();
            }
        },
        destructor: function () {
            this.registrationsContentView.removeTarget(this);
            this.registrationsContentView.destroy();
            delete this.registrationsContentView;
        }
    });

    Y.namespace('app.approvals').RegistrationsContentView = Y.Base.create('registrationsContentView', Y.usp.ReactView, [], {
        events: {
            //these events are picking up events fired by the REACT component 
            '.approvals': {
                'viewPartnerDetails': 'handleViewPartnerDetails',
                'selectedRecordChange': 'handleSelectedRecordChange',
                'addCarerApproval': 'handleAddCarerApproval',
                'viewCarerApproval': 'handleViewCarerApproval',
                'updateCarerApproval': 'handleUpdateCarerApproval',
                'prepareRemoveFosterCarerApproval': 'handlePrepareRemoveFosterCarerApproval',
                'endFosterCarerRegistration': 'handleEndFosterCarerRegistration',
                'editFosterCarerRegistration': 'handleEditFosterCarerRegistration',
                'prepareRemoveApprovalRegistration': 'handlePrepareRemoveApprovalRegistration',
                'editFosterApprovalData': 'handleEditFosterApprovalData'
            }
        },
        initializer: function (config) {
            config.personDetails.after('*:load', this.renderComponent, this);
        },
        handleViewPartnerDetails: function (e) {
            var eventDetail = e._event.detail || {};
            if (eventDetail.personId !== undefined) {
                //map from react to world of YUI
                this.fire('viewPartnerDetails', {
                    record: new Y.Model({ synchronisedPartner: { id: eventDetail.personId } })
                });
            }
        },
        handleSelectedRecordChange: function (e) {
            var eventDetail = e._event.detail || {};
            if (eventDetail && eventDetail.record !== undefined) {
                //set selected record into view to drive dependent views
                this.set('selectedRecord', new Y.Model(eventDetail.record));
            } else {
                this.set('selectedRecord', undefined);
            }
        },
        handleAddCarerApproval: function (e) {
            this._fireWithRecord(e._event.detail, 'addCarerApproval');
        },
        handleViewCarerApproval: function (e) {
            this._fireWithRecord(e._event.detail, 'viewCarerApproval');
        },
        handleUpdateCarerApproval: function (e) {
            this._fireWithRecord(e._event.detail, 'updateCarerApproval');
        },
        handlePrepareRemoveFosterCarerApproval: function (e) {
            this._fireWithRecord(e._event.detail, 'prepareRemoveFosterCarerApproval');
        },
        handleEndFosterCarerRegistration: function (e) {
            this._fireWithRecord(e._event.detail, 'endFosterCarerRegistration');
        },
        handleEditFosterCarerRegistration: function (e) {
            this._fireWithRecord(e._event.detail, 'editFosterCarerRegistration');
        },
        handlePrepareRemoveApprovalRegistration: function (e) {
            this._fireWithRecord(e._event.detail, 'prepareRemoveApprovalRegistration');
        },
        handleEditFosterApprovalData: function (e) {
            this._fireWithRecord(e._event.detail, 'editFosterApprovalData');
        },
        _fireWithRecord: function (eventDetail, action) {
            if (eventDetail && eventDetail.record !== undefined) {
                this.fire(action, {
                    record: new Y.Model(eventDetail.record),
                    //registration will be present if this is an approval triggered event
                    registration: eventDetail.registration
                });
            } else {
                //no payload
                this.fire(action);
            }
        },
        reactFactoryType: uspApprovals.ApprovalsView,
        renderComponent: function () {
            var labels = this.get('labels'),
                subject = this.get('subject'),
                url = this.get('url'),
                permissions = this.get('permissions'),
                codedEntries = this.get('codedEntries'),
                enums = this.get('enums'),
                personDetails = this.get('personDetails');

            if (this.reactAppContainer) {
                var component;

                ReactDOM.render(this.componentFactory({
                    labels: labels,
                    subject: subject,
                    url: url,
                    permissions: permissions,
                    codedEntries: codedEntries,
                    enums: enums,
                    subjectDetails: personDetails.toJSON(),
                }), this.reactAppContainer.getDOMNode(), function () {
                    component = this;
                });

                this.component = component;
            }
        },
        refresh: function () {
            if (this.component) {
                this.component.loadData();
            }
        }

    }, {
        ATTRS: {
            labels: {},
            subject: {},
            url: {},
            permissions: {},
            codedEntries: {
                value: {
                    deregistrationReason: Y.uspCategory.childlookedafter.deregistrationReason.category
                }
            },
            enums: {
                value: {
                    genderPermitted: Y.usp.childlookedafter.enum.GenderSelection.values
                }
            },
            selectedRecord: {
                value: null
            },
            personDetails: {}
        }
    });

}, '0.0.1', {
    requires: [
        'model',
        'yui-base',
        'view',
        'usp-view',
        'usp-react-view',
        'categories-childlookedafter-component-DeregistrationReason',
        'childlookedafter-component-enumerations',
    ]
});