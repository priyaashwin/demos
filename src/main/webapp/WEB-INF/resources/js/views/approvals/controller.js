YUI.add('approvals-controller-view', function (Y) {
    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.approvals').ApprovalsTabsView = Y.Base.create('approvalsTabsView', Y.usp.app.AppTabbedPage, [], {

        views: {
            approvals: {
                type: Y.app.ApprovalsContainerView
            },
            adoptiveCarerApproval: {
                type: Y.app.AdoptiveCarerApprovalContainerView
            },
            household: {
                type: Y.app.HouseholdContainerView
            },
            training: {
                type: Y.app.CarerTrainingContainerView
            },
            availability: {
                type: Y.app.FosterCarerSuspensionView
            }
        },
        tabs: {
            approvals: {},
            adoptiveCarerApproval: {},
            household: {},
            training: {},
            availability: {}
        },
        initializer: function (config) {
            var tabs = this.get('tabs'),
                permissions = config.permissions;

            if (!permissions.canViewFosterCarerApprovalAccordion) {
                //remove the Foster approvals tab from tabs
                if (tabs['approvals']) {
                    delete tabs['approvals'];
                }
            }

            if (!permissions.canViewAdoptiveCarerApprovalAccordion) {
                //remove the Adoptive approvals tab from tabs
                if (tabs['adoptiveCarerApproval']) {
                    delete tabs['adoptiveCarerApproval'];
                }
            }

            if (!permissions.canViewCurrentHouseholdAccordion) {
                //remove Household tab from tabs
                if (tabs['household']) {
                    delete tabs['household'];
                }
            }

            if (!permissions.canViewCarerTraining) {
                //remove the Taining tab from tabs
                if (tabs['training']) {
                    delete tabs['training'];
                }
            }

            if (!permissions.canViewFosterCarerSuspensionAccordion) {
                //remove the Availability tab from tabs
                if (tabs['availability']) {
                    delete tabs['availability'];
                }
            }
        }
    });

    Y.namespace('app.approvals').ApprovalsControllerView = Y.Base.create('approvalsControllerView', Y.View, [], {
        initializer: function (config) {
            var subject = config.subject || {};
            var dialogConfig = config.dialogConfig || {};
            var registrationRemoveConfig = config.removeConfig.removeRegistrationConfig || {};
            var fosterCarerApprovalRemoveConfig = config.removeConfig.removeFosterCarerApprovalConfig || {};
            var adoptiveCarerApprovalRemoveConfig = config.removeConfig.removeAdoptiveCarerApprovalConfig || {};

            this.approvalRegistrationRemoveController = new Y.app.remove.RemoveControllerView(registrationRemoveConfig).render();

            this.fosterCarerApprovalRemoveController = new Y.app.remove.RemoveControllerView(fosterCarerApprovalRemoveConfig).render();

            this.adoptiveCarerApprovalRemoveController = new Y.app.remove.RemoveControllerView(adoptiveCarerApprovalRemoveConfig).render();

            this.personDetails = new Y.usp.relationshipsrecording.PersonDetails({
                url: L.sub(this.get('personDetailsURL'), {
                    id: subject.subjectId
                })
            });

            this.currentFosterCarerRegistration = new Y.usp.childlookedafter.FosterCarerRegistrationWithOrganisationAndPerson({
                url: L.sub(this.get('currentFosterCarerRegistrationUrl'), {
                    id: subject.subjectId
                })
            });

            //plug in error handling
            this.plug(Y.Plugin.usp.ModelErrorsPlugin);

            //Setup an handler for promise failures
            this.handlePromiseError = Y.bind(function (error) {
                //On an error - close the dialog and allow the error to be picked up by the main page view
                this.dialog.hide();
                this._hideMask();
                this.fire('error', {
                    error: error
                });
            }, this);

            //mix the personDetails model in where needed
            var tabsConfig = Y.mix(config.tabs, {
                approvals: {
                    config: {
                        personDetails: this.personDetails,
                        currentFosterCarerRegistration: this.currentFosterCarerRegistration
                    }
                },
                adoptiveCarerApproval: {
                    config: {
                        personDetails: this.personDetails
                    }
                },
                training: {
                    config: {
                        personDetails: this.personDetails
                    }
                },
                availability: {
                    config: {
                        personDetails: this.personDetails,
                        currentFosterCarerRegistration: this.currentFosterCarerRegistration
                    }
                }
            }, true, undefined, 0, true);

            this.approvalsTabs = new Y.app.approvals.ApprovalsTabsView({
                container: '#tabs',
                root: config.root,
                tabs: tabsConfig,
                permissions: config.permissions
            }).addTarget(this);

            this.dialog = new Y.app.carerApproval.ChildCarerApprovalDialog(dialogConfig).addTarget(this).render();

            this._evtHandlers = [
                this.on('*:saved', this._handleSave, this),
                this.on('*:addCarerApproval', this.handleAddCarerApproval, this),
                this.on('*:endFosterCarerRegistration', this.handleEndFosterCarerRegistration, this),
                this.on('*:editFosterCarerRegistration', this.handleEditFosterCarerRegistration, this),
                this.on('*:viewCarerApproval', this.handleViewCarerApproval, this),
                this.on('*:prepareRemoveFosterCarerApproval', this.handlePrepareRemoveFosterCarerApproval, this),
                this.on('*:prepareRemoveApprovalRegistration', this.handlePrepareRemoveApprovalRegistration, this),
                this.on('*:updateCarerApproval', this.handleUpdateCarerApproval, this),
                this.on('*:addSpecificChildApproval', this.handleAddSpecificChildApproval, this),
                this.on('*:deleteSpecificChildApproval', this.handleDeleteSpecificChildApproval, this),
                this.on('*:addCarerExemption', this.handleAddCarerExemption, this),
                this.on('*:updateCarerExemption', this.handleUpdateCarerExemption, this),
                this.on('*:deleteCarerExemption', this.handleDeleteCarerExemption, this),
                this.on('*:addAdoptiveCarerApproval', this.handleAddAdoptiveCarerApproval, this),
                this.on('*:viewAdoptiveCarerApproval', this.handleViewAdoptiveCarerApproval, this),
                this.on('*:updateAdoptiveCarerApproval', this.handleUpdateAdoptiveCarerApproval, this),
                this.on('*:prepareRemoveAdoptiveCarerApproval', this.handlePrepareRemoveAdoptiveCarerApproval, this),
                this.on('*:addCarerTraining', this.handleAddCarerTraining, this),
                this.on('*:addFosterCarerTSDStandards', this.handleAddFosterCarerTSDStandards, this),
                this.on('*:viewFosterCarerTSDStandards', this.handleViewFosterCarerTSDStandards, this),
                this.on('*:viewFosterCarerTSDStandardsHistory', this.handleViewFosterCarerTSDStandardsHistory, this),
                this.on('*:addCarerSuspension', this.handleAddCarerSuspension, this),
                this.on('*:viewCarerSuspension', this.handleViewCarerSuspension, this),
                this.on('*:endCarerSuspension', this.handleEndCarerSuspension, this),
                this.on('*:addFosterCarerInvestigation', this.handleAddFosterCarerInvestigation, this),
                this.on('*:viewFosterCarerInvestigation', this.handleViewFosterCarerInvestigation, this),
                this.on('*:updateFosterCarerInvestigation', this.handleUpdateFosterCarerInvestigation, this),
                this.on('*:endFosterCarerInvestigation', this.handleEndFosterCarerInvestigation, this),
                this.on('*:viewPartnerDetails', this.handleViewPartnerDetails, this),
                this.on('*:viewPlacementSubjectDetails', this.handleViewPlacementSubjectDetails, this),
                this.on('*:deleteChildCarerTraining', this.handleDeleteChildCarerTraining, this),
                this.on('*:editFosterApprovalData', this.handleEditFosterApprovalData, this),
                Y.on('deletion:removeSaved', this._handleSave, this)
            ];
        },
        _handleSave: function () {
            var activeTabView = this.approvalsTabs.get('activeView');
            if (activeTabView) {
                this._reloadData();
                //set the person details into the current active tab view
                if (activeTabView.reload) {
                    activeTabView.reload();
                }
            }
        },
        _reloadData: function () {
            this._showMask();
            //clear any existing errors shown - we call into the errors plugin
            this.modelErrors.clearErrors();

            Y.Promise.all([this._loadPersonDetails(), this._loadCurrentFosterCarerRegistration()]).then(function () {
                this._hideMask();
            }.bind(this)).then(undefined, this.handlePromiseError);
        },
        _showMask: function () {
            var container = this.get('container');
            if (!container.busy) {
                container.plug(Y.Plugin.BusyOverlay, {
                    css: 'loading-main-mask'
                });
            }
            container.busy.show();
        },
        _hideMask: function () {
            var container = this.get('container');
            container.busy.hide();
        },
        handleEndFosterCarerRegistration: function (e) {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var errorDialogConfig = this.get('dialogConfig').views.endFosterCarerRegistrationError;

            if (permissions.canEndFosterCarerRegistration) {

                var registration = e.record;
                this._loadCurrentFosterCarerRegistrationCount(new Y.usp.childlookedafter.FosterCarerRegistrationCounts({
                    id: registration.get('id'),
                    subjectId: subject.subjectId,
                    url: errorDialogConfig.config.url
                })).then(function (data) {
                    var verdict = data.toJSON();
                    if (verdict.canDeregister) {
                        this.dialog.showView('endFosterCarerRegistrationForm', {
                            model: new Y.app.carerApproval.UpdateEndFosterCarerRegistrationForm(registration.toJSON()),
                            otherData: {
                                subject: subject
                            }
                        }, function () {
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                    } else {
                        this.dialog.showView('endFosterCarerRegistrationError', {
                            model: data,
                            otherData: {
                                subject: subject
                            }
                        });
                    }
                }.bind(this));
            }
        },
        handleEditFosterApprovalData: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.updateFosterCarerApprovalData;
            if (permissions.canEditFosterApprovalData) {
                this.dialog.showView('updateFosterCarerApprovalData', {
                    model: new Y.app.carerApproval.UpdateFosterCarerApprovalDataForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        }),
                        labels: dialogConfig.validationLabels
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true,
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleEditFosterCarerRegistration: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.editFosterCarerRegistration;

            if (permissions.canEditFosterCarerRegistration) {
                this.dialog.showView('editFosterCarerRegistration', {
                    model: new Y.app.carerApproval.UpdateFosterCarerRegistrationForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        }),
                        labels: dialogConfig.validationLabels
                    }),
                    otherData: {
                        subject: subject
                    }
                }, {
                    modelLoad: true,
                }, function (view) {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleAddCarerApproval: function (e) {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addFosterApproval;

            var registration = e.record;
            if (permissions.canAddCarerApproval) {
                this.dialog.showView('addFosterApproval', {
                    model: new Y.app.carerApproval.NewFosterCarerDetailsForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            subjectId: subject.subjectId
                        }),
                        labels: dialogConfig.validationLabels || {}
                    }),
                    otherData: {
                        subject: subject,
                        //details of current foster care registration
                        fosterCarerApprovalContext: dialogConfig.config.fosterCarerApprovalContext,
                        organisation: registration ? registration.get('organisation') : undefined,
                        currentFosterCarerRegistrationId: registration ? registration.get('id') : undefined,
                        fosterCarerRegistrationPrimaryCarer: registration ? registration.get('primaryCarer') : undefined
                    },
                    personDetailsModel: this.personDetails
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleViewCarerApproval: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.viewFosterApproval;
            if (permissions.canViewCarerApproval) {
                this.dialog.showView('viewFosterApproval', {
                    model: new Y.app.carerApproval.FosterApproval({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        fosterCarerApprovalContext: dialogConfig.config.fosterCarerApprovalContext,
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    if (!permissions.canUpdateCarerApproval) {
                        //remove button
                        this.removeButton('editButton', Y.WidgetStdMod.FOOTER);
                    } else {
                        this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    }
                });
            }
        },
        handleUpdateCarerApproval: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.editFosterApproval;
            if (permissions.canUpdateCarerApproval) {
                this.dialog.showView('editFosterApproval', {
                    model: new Y.app.carerApproval.UpdateCarerDetailsForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject'),
                        canEditDates: permissions.canEditApprovalDates,
                        fosterCarerApprovalContext: dialogConfig.config.fosterCarerApprovalContext
                    }
                }, {
                    modelLoad: true,
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handlePrepareRemoveFosterCarerApproval: function (e) {
            var view = this.approvalsTabs.get('activeView');
            this.modelErrors.clearErrors();
            //registration is a plan js object - not a YUI Model
            var registration = e.registration;
            if (registration.approvals.length === 1) {
                this.showRemoveLastFosterApprovalWarning();
            } else {
                Y.fire('removeDialog:prepareForIndividualFosterCarerApprovalRemove', {
                    id: e.record.get('id')
                });
            }
        },
        handlePrepareRemoveApprovalRegistration: function (e) {
                Y.fire('removeDialog:prepareForIndividualFosterCarerRegistrationRemove', {
                    id: e.record.get('id')
                });
        },
        showRemoveLastFosterApprovalWarning: function () {
            this.dialog.showView('removeLastFosterApprovalWarning', {}, function () {
                this.getButton('okButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //get rid of the cancel button - we have a no button instead
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },
        handleAddSpecificChildApproval: function () {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addSpecificChildApproval;
            var fosterCarerApproval;
            var activeTabView = this.approvalsTabs.get('activeView');
            if (activeTabView) {
                //Lookup the foster care approval id (currently selected) from the
                //specific child approval accordion attribute
                var accordion = activeTabView.specificChildApprovalAccordion;
                fosterCarerApproval = accordion ? accordion.get('fosterCarerApproval') : null;
            }
            if (fosterCarerApproval && permissions.canAddSpecificChildApproval) {
                this.dialog.showView('addSpecificChildApproval', {
                    model: new Y.app.carerApproval.NewSpecificChildApprovalForm({
                        url: dialogConfig.config.url,
                        labels: dialogConfig.validationLabels || {},
                        fosterCarerApprovalId: fosterCarerApproval.get('id')
                    }),
                    otherData: {
                        subject: subject
                    },
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleDeleteSpecificChildApproval: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.deleteSpecificChildApproval;
            if (permissions.canDeleteSpecificChildApproval) {
                this.dialog.showView('deleteSpecificChildApproval', {
                    model: new Y.usp.childlookedafter.SpecificChildApproval({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true,
                }, function () {
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //get rid of the cancel button - we have a no button instead
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        handleAddCarerExemption: function () {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addCarerExemption;

            if (permissions.canAddCarerExemption) {
                this.dialog.showView('addCarerExemption', {
                    model: new Y.app.carerApproval.NewCarerExemptionForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            subjectId: subject.subjectId
                        })
                    }),
                    otherData: {
                        subject: subject
                    }
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleUpdateCarerExemption: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.editCarerExemption;
            if (permissions.canUpdateCarerExemption) {
                this.dialog.showView('editCarerExemption', {
                    model: new Y.app.carerApproval.UpdateCarerExemptionForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleDeleteCarerExemption: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.deleteCarerExemption;
            if (permissions.canDeleteCarerExemption) {
                this.dialog.showView('deleteCarerExemption', {
                    model: new Y.app.carerApproval.DeleteCarerExemptionForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //get rid of the cancel button - we have a no button instead
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        handleDeleteChildCarerTraining: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.deleteChildCarerTraining;
            if (permissions.canDeleteChildCarerTraining) {
                this.dialog.showView('deleteChildCarerTraining', {
                    model: new Y.app.carerApproval.DeleteChildCarerTrainingForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //get rid of the cancel button - we have a no button instead
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        handleAddAdoptiveCarerApproval: function () {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addAdoptiveApproval;

            if (permissions.canAddAdoptiveCarerApproval) {
                this.dialog.showView('addAdoptiveApproval', {
                    model: new Y.app.carerApproval.NewAdoptiveCarerDetailsForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            subjectId: subject.subjectId
                        }),
                        labels: dialogConfig.validationLabels || {}
                    }),
                    otherData: {
                        subject: subject,
                        adopterApprovalContext: dialogConfig.config.adopterApprovalContext
                    },
                    personDetailsModel: this.personDetails
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleViewAdoptiveCarerApproval: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.viewAdoptiveApproval;
            if (permissions.canViewAdoptiveCarerApproval) {
                this.dialog.showView('viewAdoptiveApproval', {
                    model: new Y.usp.childlookedafter.ChildCarerApproval({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function (view) {
                    //check if user does not have permission - or the item has an end date - no edit allowed in this situation
                    if (!permissions.canUpdateAdoptiveCarerApproval || view.get('model').get('endDate') !== null) {
                        //remove button
                        this.removeButton('editButton', Y.WidgetStdMod.FOOTER);
                    } else {
                        this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    }
                });
            }
        },
        handleUpdateAdoptiveCarerApproval: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.editAdoptiveApproval;
            if (permissions.canUpdateAdoptiveCarerApproval) {
                this.dialog.showView('editAdoptiveApproval', {
                    model: new Y.app.carerApproval.UpdateAdoptiveCarerDetailsForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handlePrepareRemoveAdoptiveCarerApproval: function (e) {
            var record = e.record;

            Y.fire('removeDialog:prepareForIndividualAdoptiveCarerApprovalRemove', {
                id: record.get('id')
            });
        },
        handleAddCarerTraining: function () {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addCarerTraining;

            if (permissions.canAddCarerTraining) {
                this.dialog.showView('addCarerTraining', {
                    model: new Y.app.carerApproval.NewCarerTrainingForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            subjectId: subject.subjectId
                        })
                    }),
                    otherData: {
                        subject: subject
                    }
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleAddFosterCarerTSDStandards: function () {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addFosterCarerTSDStandards;

            if (permissions.canAddFosterCarerTSDStandards) {
                this.dialog.showView('addFosterCarerTSDStandards', {
                    model: new Y.app.carerApproval.NewFosterCarerTSDStandardsForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            subjectId: subject.subjectId
                        })
                    }),
                    otherData: {
                        subject: subject
                    }
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleViewFosterCarerTSDStandards: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.viewFosterCarerTSDStandards;
            if (permissions.canViewFosterCarerTSDStandards) {
                this.dialog.showView('viewFosterCarerTSDStandards', {
                    model: new Y.usp.childlookedafter.FosterCarerTSDStandards({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    if (!permissions.canUpdateFosterCarerTSDStandards) {
                        //remove button
                        this.removeButton('editButton', Y.WidgetStdMod.FOOTER);
                    } else {
                        this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    }
                });
            }
        },
        handleViewFosterCarerTSDStandardsHistory: function () {
            var permissions = this.get('permissions') || {};
            var config = this.get('carerTrainingConfig').tsdstandardsConfig.searchConfig;
            if (permissions.canViewFosterCarerTSDStandards) {
                this.dialog.showView('viewFosterCarerTSDStandardsHistory', Y.merge(config), {
                    modelLoad: false
                }, function () {});
            }
        },
        handleAddCarerSuspension: function () {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addCarerSuspension;

            if (permissions.canAddCarerSuspension) {
                this.dialog.showView('addCarerSuspension', {
                    model: new Y.app.carerApproval.NewCarerSuspensionForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            subjectId: subject.subjectId
                        }),
                        labels: dialogConfig.labels
                    }),
                    otherData: {
                        subject: subject
                    }
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleViewCarerSuspension: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.viewCarerSuspension;
            if (permissions.canViewCarerSuspension) {
                this.dialog.showView('viewCarerSuspension', {
                    model: new Y.usp.childlookedafter.ChildCarerSuspension({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function (view) {
                    //check if user does not have permission - or the item has an end date - no edit allowed in this situation
                    if (!permissions.canUpdateCarerSuspension || view.get('model').get('endDate') !== null) {
                        //remove button
                        this.removeButton('editButton', Y.WidgetStdMod.FOOTER);
                    } else {
                        this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    }
                });
            }
        },
        handleEndCarerSuspension: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.editCarerSuspension;
            if (permissions.canEndFosterCarerSuspension) {
                this.dialog.showView('editCarerSuspension', {
                    model: new Y.app.carerApproval.UpdateCarerSuspensionForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleAddFosterCarerInvestigation: function () {
            var permissions = this.get('permissions') || {};
            var subject = this.get('subject');
            var dialogConfig = this.get('dialogConfig').views.addCarerUnderInvestigation;

            if (permissions.canAddFosterCarerInvestigation) {
                this.dialog.showView('addCarerUnderInvestigation', {
                    model: new Y.app.carerApproval.NewCarerUnderInvestigationForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            subjectId: subject.subjectId
                        })
                    }),
                    otherData: {
                        subject: subject
                    }
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleViewFosterCarerInvestigation: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.viewCarerInvestigation;
            if (permissions.canViewFosterCarerInvestigation) {
                this.dialog.showView('viewCarerInvestigation', {
                    model: new Y.usp.childlookedafter.FosterCarerUnderInvestigation({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                });
            }
        },
        handleUpdateFosterCarerInvestigation: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.editCarerInvestigation;
            if (permissions.canUpdateFosterCarerInvestigation) {
                this.dialog.showView('editCarerInvestigation', {
                    model: new Y.app.carerApproval.UpdateCarerUnderInvestigationForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleEndFosterCarerInvestigation: function (e) {
            var record = e.record;
            var permissions = this.get('permissions') || {};
            var dialogConfig = this.get('dialogConfig').views.endCarerInvestigation;
            if (permissions.canEndFosterCarerInvestigation) {
                this.dialog.showView('endCarerInvestigation', {
                    model: new Y.app.carerApproval.EndCarerUnderInvestigationForm({
                        url: Y.Lang.sub(dialogConfig.config.url, {
                            id: record.get('id')
                        })
                    }),
                    otherData: {
                        subject: this.get('subject')
                    }
                }, {
                    modelLoad: true
                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        handleViewPartnerDetails: function (e) {
            this._changeContext(e.record.get('synchronisedPartner').id);
        },
        handleViewPlacementSubjectDetails: function (e) {
            this._changeContext(e.record.get('person').id);
        },
        _changeContext: function (id) {
            var personDetailsUrl = L.sub(this.get('changeContextURL'), {
                id: id
            });
            window.location = personDetailsUrl;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            var container = this.get('container');
            if (container.busy) {
                container.unplug(Y.Plugin.BusyOverlay);
            }

            //unplug errors plugin
            this.unplug(Y.Plugin.usp.ModelErrorsPlugin);

            this.approvalsTabs.removeTarget(this);
            this.approvalsTabs.destroy();
            delete this.approvalsTabs;

            this.dialog.removeTarget(this);
            this.dialog.destroy();
            delete this.dialog;
        },
        _loadPersonDetails: function () {
            return new Y.Promise(function (resolve, reject) {
                var data = this.personDetails.load(function (err) {
                    if (err !== null) {
                        //failed to get the form instance types for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            }.bind(this));
        },
        _loadCurrentFosterCarerRegistrationCount: function (fosterCarerRegistrationCounts) {
            return new Y.Promise(function (resolve, reject) {
                var data = fosterCarerRegistrationCounts.load(function (err) {
                    if (err !== null && err.code !== 404) {
                        //failed
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            }.bind(this));
        },
        _loadCurrentFosterCarerRegistration: function () {
            return new Y.Promise(function (resolve, reject) {
                var data = this.currentFosterCarerRegistration.load(function (err) {
                    if (err !== null && err.code !== 404) {
                        //failed to get the form instance types for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            }.bind(this));
        },
        render: function () {
            this.get('container').setHTML('loading ...');

            //ensure our data is loaded before we render
            Y.Promise.all([this._loadPersonDetails(), this._loadCurrentFosterCarerRegistration()]).then(function () {
                    var contentNode = Y.one(Y.config.doc.createDocumentFragment());

                    //add the tabs container
                    contentNode.append(this.approvalsTabs.render().get('container'));

                    this.get('container').setHTML(contentNode);

                    //ensure we load the approvals tab
                    if (this.approvalsTabs.hasRoute(this.approvalsTabs.getPath())) {
                        this.approvalsTabs.dispatch();
                    } else {
                        var tabs = this.approvalsTabs.get('tabs');
                        if (tabs['approvals']) {
                            this.approvalsTabs.activateTab('approvals');
                        } else {
                            this.approvalsTabs.activateTab('adoptiveCarerApproval');
                        }
                    }
                }.bind(this))
                .catch(function () {
                    this.get('container').setHTML('Error - unable to load');
                }.bind(this));
        }
    }, {
        ATTRS: {
            personDetailsURL: {
                value: null
            },
            subject: {
                value: {}
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'promise',
        'app-tabbed-page',
        'approvals-view',
        'gallery-busyoverlay',
        'model-errors-plugin',
        'usp-relationshipsrecording-PersonDetails',
        'usp-childlookedafter-FosterCarerRegistrationCounts',
        'usp-childlookedafter-FosterCarerRegistrationWithOrganisationAndPerson',
        'usp-childlookedafter-ChildCarerApproval',
        'usp-childlookedafter-ChildCarerSuspension',
        'carerapproval-dialog-views',
        'remove-controller-view'
    ]
});