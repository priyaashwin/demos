YUI.add('foster-approvals-accordion-views', function(Y) {
    'use-strict';

    var L = Y.Lang;

    var USPFormatters = Y.usp.ColumnFormatters;

    Y.namespace('app.approvals').SpecificChildApprovalAccordionView = Y.Base.create('specificChildApprovalAccordionView', Y.usp.app.Results, [], {
        //set the tablePanelType to be an Accordion Table Panel
        tablePanelType: Y.usp.AccordionTablePanel,
        initializer: function(config) {
            this._specificChildApprovalEvts = [
                this.after('fosterCarerApprovalChange', this._handleFosterCarerApprovalChange, this),
                config.currentFosterCarerRegistration.after('*:load', function() {
                    this._disableAddButtonCheck();
                }, this),
                config.currentFosterCarerRegistration.after('*:error', function() {
                    this.get('currentFosterCarerRegistration').removeAttr('organisation');
                    this._disableAddButtonCheck();
                }, this)
            ];
        },
        destructor: function() {
            this._specificChildApprovalEvts.forEach(function(handle) {
                handle.detach();
            });
            delete this._specificChildApprovalEvts;

            if (this.toolbar) {
                this.toolbar.removeTarget(this);
                this.toolbar.destroy();
                delete this.toolbar;
            }
        },
        _handleFosterCarerApprovalChange: function() {
            var fosterCarerApproval = this.get('fosterCarerApproval');
            var fosterCarerApprovalId = fosterCarerApproval ? fosterCarerApproval.get('id') : null;

            //update the url even if it is to something invalid
            // - or we could end up showing incorrect data on a refresh
            this.get('results').get('data').url = L.sub(this.get('searchConfig').url, {
                id: fosterCarerApprovalId
            });

            if (fosterCarerApprovalId) {
                this.get('results').reload();
            } else {
                //clear results as we have no selection
                this.get('results').get('data').reset();
            }

            //update the add button
            this._disableAddButtonCheck();
            //set the title
            this._setTitle();
        },
        _setTitle: function() {
            var fosterCarerApproval = this.get('fosterCarerApproval');
            var title = [this.get('tablePanelConfig.title') || 'Specific approval'];
            if (fosterCarerApproval) {
                title.push(' - ');
                title.push(Y.USPDate.format(fosterCarerApproval.get('startDate')));
                if (null != fosterCarerApproval.get('endDate')){
                    title.push(' - ');
                    title.push(Y.USPDate.format(fosterCarerApproval.get('endDate')));
                }
                title.push(' - ');
                title.push(fosterCarerApproval.get('typeOfCareAssignmentVO.codedEntryVO.name'));
            }
            this.getTablePanel().set('title', title.join(' '));
        },
        _disableAddButtonCheck: function() {
            var permissions = this.get('permissions') || {}, canAdd = false;

            if (permissions.canAddSpecificChildApproval && this.get('fosterCarerApproval') && this.get('registeredOrganisationExists') ) {
                canAdd = true;
            }

            if (canAdd) {
                //enable button
                this.toolbar.enableButton('addSpecificChildApproval');
            } else {
                //disable button
                this.toolbar.disableButton('addSpecificChildApproval');
            }
        },
        render: function() {
            var permissions = this.get('permissions'),
                labels = this.get('labels');

            Y.app.approvals.SpecificChildApprovalAccordionView.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this.toolbar.addButton({
                icon: 'fa fa-plus-circle',
                className: 'pull-right pure-button-active',
                action: 'addSpecificChildApproval',
                title: labels.addSpecificChildApproval,
                ariaLabel: labels.addSpecificChildApprovalAriaLabel,
                label: labels.add
            });

            //configure the button
            this._disableAddButtonCheck();
            //set the title
            this._setTitle();
            return this;
        },
        getResultsListModel: function(config) {
            var fosterCarerApproval = this.get('fosterCarerApproval');
            var id = fosterCarerApproval ? fosterCarerApproval.get('id') : null;

            return new Y.usp.childlookedafter.PaginatedSpecificChildApprovalWithPersonList({
                url: L.sub(config.url, {
                    id: id
                })
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                    key: 'childApprovedFor!personIdentifier',
                    label: labels.id,
                    width: '21%'
                },
                {
                    key: 'childApprovedFor!name',
                    label: labels.name,
                    width: '70%'
                }, {
                    label: labels.actions,
                    className: 'pure-table-actions',
                    allowHTML: true,
                    width: '9%',
                    formatter: USPFormatters.actions,
                    items: [{
                        clazz: 'deleteSpecificChildApproval',
                        title: 'Remove approval',
                        label: 'remove approval',
                        visible: permissions.canDeleteSpecificChildApproval
                    }]
                }
            ];
        }
    }, {
        ATTRS: {
            initialLoad: {
                value: false
            },
            currentFosterCarerRegistration: {},
            registeredOrganisationExists: {
                getter: function() {
                    return this.get('currentFosterCarerRegistration').get('organisation') !== undefined;
                }
            },
            fosterCarerApproval: {}
        }
    });

    Y.namespace('app.approvals').ExemptionsAccordionView = Y.Base.create('exemptionsAccordionView', Y.usp.app.Results, [], {
        //set the tablePanelType to be an Accordion Table Panel
        tablePanelType: Y.usp.AccordionTablePanel,
        initializer: function(config) {
            this._exemptionsEvts = [
                config.personDetails.after('*:load', function() {
                    //Check permission and style  button accordingly
                    this._displayAddButtonCheck();
                }, this),
                config.currentFosterCarerRegistration.after('*:load', function() {
                    this._displayAddButtonCheck();
                }, this),
                config.currentFosterCarerRegistration.after('*:error', function() {
                    this.get('currentFosterCarerRegistration').removeAttr('organisation');
                    this._displayAddButtonCheck();
                }, this)
            ];
        },
        destructor: function() {
            this._exemptionsEvts.forEach(function(handle) {
                handle.detach();
            });
            delete this._exemptionsEvts;
        },
        getResultsListModel: function(config) {

            return new Y.usp.childlookedafter.PaginatedFosterCarerExemptionList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                key: 'startDate',
                label: labels.startDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '15%'
            }, {
                key: 'endDate',
                label: labels.endDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '15%'
            }, {
                key: 'type',
                label: labels.exemptionType,
                formatter: USPFormatters.enumType,
                enumTypes: Y.usp.childlookedafter.enum.ExemptionType.values,
                width: '25%'
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                allowHTML: true,
                width: '9%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'updateCarerExemption',
                    title: 'Edit exemption',
                    label: 'edit exemption',
                    visible: permissions.canUpdateCarerExemption
                }, {
                    clazz: 'deleteCarerExemption',
                    title: 'Remove exemption',
                    label: 'remove exemption',
                    visible: permissions.canDeleteCarerExemption
                }]
            }];
        },
        isFosterCarer: function(personTypes) {
            return !!(personTypes || []).find(function(type) {
                return type === 'FOSTER_CARER';
            });
        },
        _displayAddButtonCheck: function() {
            var personDetails = this.get('personDetails');

            var permissions = this.get('permissions') || {},
                labels = this.get('labels') || {};

            var canAdd = false;

            var hasDemographicsForChildCarer = personDetails ? personDetails.get('hasDemographicsForChildCarer') : false;
            if (this.get('registeredOrganisationExists') && permissions.canAddCarerExemption) {
                if (hasDemographicsForChildCarer) {
                    canAdd = true;
                }
            }

            //remove add button
            this.toolbar.removeButton('addCarerExemption');

            if (canAdd) {
                //add button
                this.toolbar.addButton({
                    icon: 'fa fa-plus-circle',
                    className: 'pull-right pure-button-active',
                    action: 'addCarerExemption',
                    title: labels.addCarerExemption,
                    ariaLabel: labels.addCarerExemptionAriaLabel,
                    label: labels.add
                });
            }
        },
        render: function() {
            var permissions = this.get('permissions');

            Y.app.approvals.ExemptionsAccordionView.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this._displayAddButtonCheck();

            return this;
        }
    }, {
      ATTRS: {
        currentFosterCarerRegistration: {},
        registeredOrganisationExists: {
          getter: function() {
            return this.get('currentFosterCarerRegistration').get('organisation') !== undefined;
          }
        }
      }
    });


}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'accordion-panel',
        'app-results',
        'results-formatters',
        'results-templates',
        'results-table-summary-row-plugin',
        'app-results',
        'usp-childlookedafter-FosterCarerApproval',
        'usp-childlookedafter-FosterCarerExemption',
        'childlookedafter-component-enumerations',
        'usp-childlookedafter-SpecificChildApprovalWithPerson',
        'app-toolbar',
        'usp-date',
        'handlebars',
        'handlebars-helpers',
        'handlebars-carerApproval-templates',
    ]
});
