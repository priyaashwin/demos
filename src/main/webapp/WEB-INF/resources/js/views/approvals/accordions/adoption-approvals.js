YUI.add('adoption-approvals-accordion-views', function (Y) {
    'use-strict';

    var Micro = Y.Template.Micro,
        MISSING_DEMOGRAPHIC_DATA_TEMPLATE = Micro.compile('<div class="add-foster-approval-button pull-right"><span class="h3 pop-up-data " data-title="" data-content="<%=this.missingDemographicLong%>" ><a class="help"></a><span class="pure-visible-desktop"><%=this.missingDemographicShort%></span></span></div>'),
        ESTIMATED_DOB_APPROVAL_DATA_TEMPLATE = Micro.compile('<div class="add-foster-approval-button pull-right"><span class="h3 pop-up-data " data-title="" data-content="<%=this.estimatedDOBExceptionLong%>" ><a class="help"></a><span class="pure-visible-desktop"><%=this.estimatedDOBExceptionShort%></span></span></div>');

    var USPFormatters = Y.usp.ColumnFormatters,
        APPFormatters = Y.app.ColumnFormatters;

    Y.namespace('app.approvals').AdoptiveCarerApprovalDetailsAccordionView = Y.Base.create('adoptiveCarerApprovalDetailsAccordionView', Y.usp.app.Results, [], {
        initializer: function (config) {
            this._adoptiveCarerApprovaEvts = [
                config.personDetails.after('*:load', function () {
                    //Check permission and style  button accordingly
                    this._displayAddButtonCheck();
                }, this)
            ];
        },
        destructor: function () {
            this._adoptiveCarerApprovaEvts.forEach(function (handle) {
                handle.detach();
            });
            delete this._adoptiveCarerApprovaEvts;

            if (this.toolbar) {
                this.toolbar.removeTarget(this);
                this.toolbar.destroy();
                delete this.toolbar;
            }
        },
        _displayAddButtonCheck: function () {
            var personDetails = this.get('personDetails');

            var permissions = this.get('permissions') || {},
                labels = this.get('labels') || {};

            var canAdd = false;

            var adoptorFlag = personDetails ? this._isPersonAdoptorCarer(personDetails.get('personTypes')) : false;
            var hasDemographicsForChildCarer = personDetails ? personDetails.get('hasDemographicsForChildCarer') : false;
            var dateOfBirthEstimated = personDetails ? personDetails.get('dateOfBirthEstimated') : false;
            if (adoptorFlag && permissions.canAddAdoptiveCarerApproval) {
                if (hasDemographicsForChildCarer && !dateOfBirthEstimated) {
                    var tooltip = this.toolbar.get('toolbarNode').one('.pop-up-data');
                    if (tooltip) {
                        tooltip.destroy(true);
                    }
                    canAdd = true;
                }
            }

            //remove Add button before adding another one
            this.toolbar.removeButton('addAdoptiveCarerApproval');
            if (canAdd) {
                //add button
                this.toolbar.addButton({
                    icon: 'fa fa-plus-circle',
                    className: 'pull-right pure-button-active',
                    action: 'addAdoptiveCarerApproval',
                    title: labels.addAdoptiveCarerApproval,
                    ariaLabel: labels.addAdoptiveCarerApprovalAriaLabel,
                    label: labels.add
                });
            } else {
                //remove help text
                this.toolbar.get('toolbarNode').all('.add-foster-approval-button').remove(true);
                if (!hasDemographicsForChildCarer) {
                    //insert the tooltip
                    this.toolbar.get('toolbarNode').appendChild(MISSING_DEMOGRAPHIC_DATA_TEMPLATE({
                        missingDemographicShort: labels.missingDemographicShort,
                        missingDemographicLong: labels.missingDemographicLong
                    }));
                } else if (dateOfBirthEstimated) {
                    //insert the tooltip
                    this.toolbar.get('toolbarNode').appendChild(ESTIMATED_DOB_APPROVAL_DATA_TEMPLATE({
                        estimatedDOBExceptionShort: labels.estimatedDOBExceptionShort,
                        estimatedDOBExceptionLong: labels.estimatedDOBExceptionLong
                    }));
                }
            }
        },
        _isPersonAdoptorCarer: function (personTypes) {
            return !!(personTypes || []).find(function (type) {
                return type === 'ADOPTER';
            });
        },
        render: function () {
            var permissions = this.get('permissions');

            Y.app.approvals.AdoptiveCarerApprovalDetailsAccordionView.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this._displayAddButtonCheck();

            return this;
        },
        getResultsListModel: function (config) {
            return new Y.usp.childlookedafter.PaginatedAdoptiveCarerApprovalList({
                url: config.url
            });
        },
        getColumnConfiguration: function (config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                key: 'startDate',
                label: labels.startDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '15%'
            }, {
                key: 'endDate',
                label: labels.endDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '15%'
            }, {
                key: 'genderPermitted',
                label: labels.genderPermitted,
                formatter: USPFormatters.enumType,
                enumTypes: Y.usp.childlookedafter.enum.GenderSelection.values,
                sortable: true,
                width: '12%'
            }, {
                key: 'disabilityPermitted',
                label: labels.disabilityPermitted,
                formatter: USPFormatters.yesNo,
                sortable: true,
                width: '12%'
            }, {
                key: 'childrenPlaced',
                label: labels.childrenPlaced,
                sortable: true,
                width: '25%'
            }, {
                key: 'lowerAgePermitted',
                label: labels.ageRangePermitted,
                formatter: APPFormatters.carerApprovalAgeRange,
                width: '12%'
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                allowHTML: true,
                width: '9%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'viewAdoptiveCarerApproval',
                    title: 'View approval',
                    label: 'view approval',
                    visible: permissions.canViewAdoptiveCarerApproval
                }, {
                    clazz: 'updateAdoptiveCarerApproval',
                    title: 'Edit approval',
                    label: 'edit approval',
                    visible: function () {
                        return permissions.canUpdateAdoptiveCarerApproval && !this.record.get('endDate');
                    }
                }, {
                    clazz: 'prepareRemoveAdoptiveCarerApproval',
                    title: 'Remove approval',
                    label: 'remove approval',
                    visible: permissions.canDeleteAdoptiveCarerApproval
                }]
            }];
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'results-formatters',
        'caserecording-results-formatters',
        'app-results',
        'event-custom',
        'usp-childlookedafter-AdoptiveCarerApproval',
        'childlookedafter-component-enumerations',
        'template-micro',
        'app-toolbar'
    ]
});