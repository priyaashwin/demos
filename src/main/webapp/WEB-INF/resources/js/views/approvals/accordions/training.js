YUI.add('training-accordion-views', function(Y) {
    'use-strict';

    var USPFormatters = Y.usp.ColumnFormatters,
        L = Y.Lang;

    Y.namespace('app.approvals').TrainingAccordionView = Y.Base.create('trainingAccordionView', Y.usp.app.Results, [], {
        initializer: function(config) {
            this._trainingEvts = [
                config.personDetails.after('*:load', function() {
                    //Check permission and style  button accordingly
                    this._displayAddButtonCheck();
                }, this)
            ];
        },
        getResultsListModel: function(config) {
            return new Y.usp.childlookedafter.PaginatedFosterCarerTrainingList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels,
            permissions = config.permissions;

            return [{
                key: 'startDate',
                label: labels.startDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '15%'
            }, {
                key: 'endDate',
                label: labels.endDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '15%'
            }, {
                key: 'courseCode',
                label: labels.trainingCourse,
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.childlookedafter.training.category.codedEntries,
                sortable: true,
                width: '70%',
                allowHTML: true                
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                allowHTML: true,
                width: '10%',
                formatter: USPFormatters.actions,                
                items: [{
                    clazz: 'deleteChildCarerTraining',
                    title: labels.deleteCarerTrainingTitle,
                    label: labels.deleteCarerTraining,
                    visible: permissions.canDeleteChildCarerTraining
                }]
            }];
        },
        render: function() {
            var permissions = this.get('permissions');

            Y.app.approvals.TrainingAccordionView.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this._displayAddButtonCheck();

            return this;
        },
        _isPersonCarer: function(personTypes) {
            return !!(personTypes || []).find(function(type) {
                return (type === 'FOSTER_CARER' || type === 'ADOPTER');
            });
        },
        _displayAddButtonCheck: function() {
            var personDetails = this.get('personDetails');

            var permissions = this.get('permissions') || {},
                labels = this.get('labels') || {};

            var canAdd = false;
            
            var isCarer = personDetails ? this._isPersonCarer(personDetails.get('personTypes')) : false;

            if (isCarer && permissions.canAddCarerTraining) {
                canAdd = true;
            }

            //remove Add button before adding another one
            this.toolbar.removeButton('addCarerTraining');
            if (canAdd) {
                //add button
                this.toolbar.addButton({
                    icon: 'fa fa-plus-circle',
                    className: 'pull-right pure-button-active',
                    action: 'addCarerTraining',
                    title: labels.addCarerTraining,
                    ariaLabel: labels.addCarerTrainingAriaLabel,
                    label: labels.add
                });
            }
        },
        destructor: function() {
            this._trainingEvts.forEach(function(handle) {
                handle.detach();
            });
            delete this._trainingEvts;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;
        }
    });
    
    Y.namespace('app.approvals').TSDStandardsAccordionView = Y.Base.create('tsdStandardsAccordionView', Y.usp.app.Results, [], {
        initializer: function(config) {
            // to avoid the race condition between the two events (personDetails:load and PaginatedFosterCarerTSDStandardsList:load)
            // TSDStandardsAddButton variable is set to false (in PaginatedFosterCarerTSDStandardsList:load) when list has an entry to avoid adding Add button
            this.TSDStandardsAddButton = true;
            this._tsdStandardsEvts = [
                config.personDetails.after('*:load', function() {
                    //Check permission and show/hide Add button accordingly
                    this._displayAddButtonCheck();
                }, this),
                this.after('PaginatedFosterCarerTSDStandardsList:load', function(e) {
                    var results = e.parsed;
                    if (results.length > 0) {
                    	//Remove Add button when a record exists. Only one record is allowed to be added.  
                    	this.toolbar.removeButton('addFosterCarerTSDStandards');
                      this.TSDStandardsAddButton = false;
                    }
                }, this)                
            ];
        },        
        getResultsListModel: function(config) {
            return new Y.usp.childlookedafter.PaginatedFosterCarerTSDStandardsList({
            	url: L.sub(config.url, {
            		pageNumber: 1,
            		pageSize: 1
                })
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels,
            	permissions = config.permissions;            

            return [{
                key: 'statusDate',
                label: labels.statusDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '20%'
            }, {
                key: 'statusCode',
                label: labels.statusCode,
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.childlookedafter.fostercarertsdstandardsstatus.category.codedEntries,
                sortable: true,
                width: '70%',
                allowHTML: true
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                allowHTML: true,
                width: '10%',
                formatter: USPFormatters.actions,                
                items: [{
                    clazz: 'addFosterCarerTSDStandards',
                    title: labels.progressTitle,
                    label: labels.progress,
                    visible: permissions.canAddFosterCarerTSDStandards
                }, {
                    clazz: 'viewFosterCarerTSDStandards',
                    title: labels.viewTitle,
                    label: labels.view,
                    visible: permissions.canViewFosterCarerTSDStandards
                }, {
                    clazz: 'viewFosterCarerTSDStandardsHistory',
                    title: labels.viewHistoryTitle,
                    label: labels.viewHistory,
                    visible: permissions.canViewFosterCarerTSDStandards
                }]
            }];
        },
        render: function() {
            var permissions = this.get('permissions');

            Y.app.approvals.TSDStandardsAccordionView.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this._displayAddButtonCheck();
   
            return this;
        },
        _isPersonFosterCarer: function(personTypes) {
            return !!(personTypes || []).find(function(type) {
                return (type === 'FOSTER_CARER');
            });
        },
        _displayAddButtonCheck: function() {
            var personDetails = this.get('personDetails');         
           
            var permissions = this.get('permissions') || {},
                labels = this.get('labels') || {};
           
            var canAdd = false;

            var isCarer = personDetails ? this._isPersonFosterCarer(personDetails.get('personTypes')) : false;                 
           
            if (isCarer && permissions.canAddFosterCarerTSDStandards) {
                canAdd = true;
            }

            //remove Add button before adding another one
            this.toolbar.removeButton('addFosterCarerTSDStandards');
            if (canAdd && this.TSDStandardsAddButton) {
                //add button
                this.toolbar.addButton({
                    icon: 'fa fa-plus-circle',
                    className: 'pull-right pure-button-active',
                    action: 'addFosterCarerTSDStandards',
                    title: labels.addFosterCarerTSDStandards,
                    areaLabel: labels.addFosterCarerTSDStandardsAreaLabel,
                    label: labels.add
                });
            }
        },
        destructor: function() {
            this._tsdStandardsEvts.forEach(function(handle) {
                handle.detach();
            });
            delete this._tsdStandardsEvts;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;
        }
        
    });

    Y.namespace('app.approvals').FosterCarerTSDStandardsHistoryResults = Y.Base.create('fosterCarerTSDStandardsHistoryResults', Y.usp.app.Results, [], {
    	
        getResultsListModel: function(config) {
        	return new Y.usp.childlookedafter.PaginatedFosterCarerTSDStandardsList({
            url: config.url            
          });
        },
        getColumnConfiguration: function(config) {
          var labels = config.labels || {},
              permissions = config.permissions;
          
          return ([{
              key: 'statusDate',
              label: labels.statusDate,
              formatter: USPFormatters.date,
              sortable: true,
              width: '25%'
          }, {
              key: 'statusCode',
              label: labels.statusCode,
              formatter: USPFormatters.codedEntry,
              codedEntries: Y.uspCategory.childlookedafter.fostercarertsdstandardsstatus.category.codedEntries,
              sortable: true,
              width: '75%',
              allowHTML: true
          }]);
        }
      });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'results-formatters',
        'app-results',
        'usp-childlookedafter-FosterCarerTraining',
        'categories-childlookedafter-component-Training',
        'usp-childlookedafter-FosterCarerTSDStandards',
        'categories-childlookedafter-component-Fostercarertsdstandardsstatus',
        'results-templates',
        'app-toolbar'
    ]
});