YUI.add('approvals-view', function (Y) {
    'use-strict';

    Y.namespace('app').ApprovalsContainerView = Y.Base.create('approvalsContainerView', Y.View, [], {
        initializer: function (config) {
            if (config.permissions.canViewFosterCarerApprovalAccordion) {
                this.fosterApprovalRegistrationsAccordion = new Y.app.approvals.RegistrationsView(Y.merge(config.registrationsConfig, {
                    personDetails: config.personDetails,
                    permissions: config.permissions
                })).addTarget(this);
            }

            if (config.permissions.canViewSpecificChildApprovalAccordion) {
                this.specificChildApprovalAccordion = new Y.app.approvals.SpecificChildApprovalAccordionView(Y.merge(config.specificChildApprovalConfig, {
                    permissions: config.permissions,
                    currentFosterCarerRegistration: config.currentFosterCarerRegistration,
                    searchConfig: Y.merge(config.specificChildApprovalConfig.searchConfig, {
                        permissions: config.permissions
                    })
                })).addTarget(this);
            }

            if (config.permissions.canViewFosterExemptionAccordion) {
                this.exemptionsAccordion = new Y.app.approvals.ExemptionsAccordionView(Y.merge(config.exemptionsConfig, {
                    personDetails: config.personDetails,
                    currentFosterCarerRegistration: config.currentFosterCarerRegistration,
                    permissions: config.permissions,
                    searchConfig: Y.merge(config.exemptionsConfig.searchConfig, {
                        permissions: config.permissions
                    })
                })).addTarget(this);
            }

            if (this.fosterApprovalRegistrationsAccordion) {
                this._approvalsEvts = [
                    this.fosterApprovalRegistrationsAccordion.on('*:selectedRecordChange', this.handleSelectRecordChange, this)
                ];
            }
        },
        handleSelectRecordChange: function (e) {
            //push the record into the other accordions to load the data
            this.specificChildApprovalAccordion.set('fosterCarerApproval', e.newVal);
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //add the tabs container
            if (this.fosterApprovalRegistrationsAccordion) {

                contentNode.append(this.fosterApprovalRegistrationsAccordion.render().get('container'));
            }

            if (this.specificChildApprovalAccordion) {
                contentNode.append(this.specificChildApprovalAccordion.render().get('container'));
            }

            if (this.exemptionsAccordion) {
                contentNode.append(this.exemptionsAccordion.render().get('container'));
            }

            this.get('container').setHTML(contentNode);

            return this;
        },

        destructor: function () {
            if (this.fosterApprovalRegistrationsAccordion) {
                this._approvalsEvts.forEach(function (handler) {
                    handler.detach();
                });
                delete this._approvalsEvts;
            }

            //clean up
            if (this.fosterApprovalRegistrationsAccordion) {
                this.fosterApprovalRegistrationsAccordion.removeTarget(this);
                this.fosterApprovalRegistrationsAccordion.destroy();
                delete this.fosterApprovalRegistrationsAccordion;
            }

            if (this.specificChildApprovalAccordion) {
                this.specificChildApprovalAccordion.removeTarget(this);
                this.specificChildApprovalAccordion.destroy();
                delete this.specificChildApprovalAccordion;
            }

            if (this.exemptionsAccordion) {
                this.exemptionsAccordion.removeTarget(this);
                this.exemptionsAccordion.destroy();
                delete this.exemptionsAccordion;
            }
        },
        reload: function () {
            //reload foster care registrations
            if (this.fosterApprovalRegistrationsAccordion) {
                this.fosterApprovalRegistrationsAccordion.reload();
            }

            if(this.specificChildApprovalAccordion){
                this.specificChildApprovalAccordion.get('results').reload();
            }
            //reload the exemptions
            if (this.exemptionsAccordion) {
                this.exemptionsAccordion.get('results').reload();
            }
        }
    }, {
        ATTRS: {
            personDetails: {}
        }
    });

    Y.namespace('app').AdoptiveCarerApprovalContainerView = Y.Base.create('adoptiveCarerApprovalContainerView', Y.View, [], {
        initializer: function (config) {
            if (config.permissions.canViewAdoptiveCarerApprovalAccordion) {
                this.adoptiveCarerApprovalDetailsAccordion = new Y.app.approvals.AdoptiveCarerApprovalDetailsAccordionView(Y.merge(config.adoptiveCarerApprovalDetailsConfig, {
                    personDetails: config.personDetails,
                    permissions: config.permissions,
                    searchConfig: Y.merge(config.adoptiveCarerApprovalDetailsConfig.searchConfig, {
                        permissions: config.permissions
                    })
                })).addTarget(this);
            }
        },
        destructor: function () {
            if (this.adoptiveCarerApprovalDetailsAccordion) {
                this.adoptiveCarerApprovalDetailsAccordion.removeTarget(this);
                this.adoptiveCarerApprovalDetailsAccordion.destroy();
                delete this.adoptiveCarerApprovalDetailsAccordion;
            }
        },
        render: function () {
            if (this.adoptiveCarerApprovalDetailsAccordion) {
                this.get('container').append(this.adoptiveCarerApprovalDetailsAccordion.render().get('container'));
            }

            return this;
        },
        reload: function () {
            //reload the results
            if (this.adoptiveCarerApprovalDetailsAccordion) {
                this.adoptiveCarerApprovalDetailsAccordion.get('results').reload();
            }
        }
    }, {
        ATTRS: {
            personDetails: {}
        }
    });

    Y.namespace('app').HouseholdContainerView = Y.Base.create('householdContainerView', Y.View, [], {
        initializer: function (config) {
            if (config.permissions.canViewCurrentHouseholdAccordion) {
                this.currentHouseholdAccordion = new Y.app.approvals.CurrentHouseholdAccordionView(config.currentHouseholdConfig).addTarget(this);
            }
            if (config.permissions.canViewPlacementAccordion) {
                this.placementsAccordion = new Y.app.approvals.PlacementAccordionView(config.placementConfig).addTarget(this);
            }
        },
        destructor: function () {
            if (this.currentHouseholdAccordion) {
                this.currentHouseholdAccordion.removeTarget(this);
                this.currentHouseholdAccordion.destroy();
                delete this.currentHouseholdAccordion;
            }

            if (this.placementsAccordion) {
                this.placementsAccordion.removeTarget(this);
                this.placementsAccordion.destroy();
                delete this.placementsAccordion;
            }
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //add the tabs container
            if (this.currentHouseholdAccordion) {
                contentNode.append(this.currentHouseholdAccordion.render().get('container'));
            }
            if (this.placementsAccordion) {
                contentNode.append(this.placementsAccordion.render().get('container'));
            }

            this.get('container').setHTML(contentNode);

            return this;
        },
        reload: function () {
            //reload the results
            if (this.currentHouseholdAccordion) {
                this.currentHouseholdAccordion.get('results').reload();
            }
            if (this.placementsAccordion) {
                this.placementsAccordion.get('results').reload();
            }
        }
    });

    Y.namespace('app').CarerTrainingContainerView = Y.Base.create('carerTrainingContainerView', Y.View, [], {
        initializer: function (config) {
            if (config.permissions.canViewCarerTraining) {
                this.trainingAccordionView = new Y.app.approvals.TrainingAccordionView(Y.merge(config.trainingConfig, {
                    permissions: config.permissions,
                    personDetails: config.personDetails,
                    searchConfig: Y.merge(config.trainingConfig.searchConfig, {
                        permissions: config.permissions
                    })
                })).addTarget(this);
            }
            if (config.permissions.canViewFosterCarerTSDStandards) {
                this.tsdStandardsAccordionView = new Y.app.approvals.TSDStandardsAccordionView(Y.merge(config.tsdstandardsConfig, {
                    permissions: config.permissions,
                    personDetails: config.personDetails,
                    searchConfig: Y.merge(config.tsdstandardsConfig.searchConfig, {
                        permissions: config.permissions
                    })
                })).addTarget(this);
            }
        },
        destructor: function () {
            if (this.trainingAccordionView) {
                this.trainingAccordionView.removeTarget(this);
                this.trainingAccordionView.destroy();
                delete this.trainingAccordionView;
            }
            if (this.tsdStandardsAccordionView) {
                this.tsdStandardsAccordionView.removeTarget(this);
                this.tsdStandardsAccordionView.destroy();
                delete this.tsdStandardsAccordionView;
            }
        },
        render: function () {
            if (this.trainingAccordionView) {
                this.get('container').append(this.trainingAccordionView.render().get('container'));
            }
            if (this.tsdStandardsAccordionView) {
                this.get('container').append(this.tsdStandardsAccordionView.render().get('container'));
            }
            return this;
        },
        reload: function () {
            //reload the results
            if (this.trainingAccordionView) {
                this.trainingAccordionView.get('results').reload();
            }
            if (this.tsdStandardsAccordionView) {
                this.tsdStandardsAccordionView.get('results').reload();
            }
        }
    }, {
        ATTRS: {
            personDetails: {}
        }
    });

    Y.namespace('app').FosterCarerSuspensionView = Y.Base.create('fosterCarerSuspensionView', Y.View, [], {
        initializer: function (config) {
            if (config.permissions.canViewFosterCarerSuspensionAccordion) {
                this.fosterCarerSuspensionAccordionView = new Y.app.approvals.FosterCarerSuspensionAccordionView(Y.merge(config.fosterCarerSuspensionAccordionConfig, {
                    personDetails: config.personDetails,
                    currentFosterCarerRegistration: config.currentFosterCarerRegistration,
                    permissions: config.permissions,
                    searchConfig: Y.merge(config.fosterCarerSuspensionAccordionConfig.searchConfig, {
                        permissions: config.permissions
                    })
                })).addTarget(this);
            }

            if (config.permissions.canViewFosterCarerInvestigationAccordion) {
                this.fosterCarerUnderInvestigationAccordionView = new Y.app.approvals.FosterCarerUnderInvestigationAccordionView(Y.merge(config.fosterCarerUnderInvestigationAccordionConfig, {
                    personDetails: config.personDetails,
                    currentFosterCarerRegistration: config.currentFosterCarerRegistration,
                    permissions: config.permissions,
                    searchConfig: Y.merge(config.fosterCarerUnderInvestigationAccordionConfig.searchConfig, {
                        permissions: config.permissions
                    })
                })).addTarget(this);
            }
        },

        destructor: function () {
            if (this.fosterCarerSuspensionAccordionView) {
                this.fosterCarerSuspensionAccordionView.removeTarget(this);
                this.fosterCarerSuspensionAccordionView.destroy();
                delete this.fosterCarerSuspensionAccordionView;
            }

            if (this.fosterCarerUnderInvestigationAccordionView) {
                this.fosterCarerUnderInvestigationAccordionView.removeTarget(this);
                this.fosterCarerUnderInvestigationAccordionView.destroy();
                delete this.fosterCarerUnderInvestigationAccordionView;
            }
        },

        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //add the tabs container
            if (this.fosterCarerSuspensionAccordionView) {
                contentNode.append(this.fosterCarerSuspensionAccordionView.render().get('container'));
            }
            if (this.fosterCarerUnderInvestigationAccordionView) {
                contentNode.append(this.fosterCarerUnderInvestigationAccordionView.render().get('container'));
            }

            this.get('container').setHTML(contentNode);

            return this;
        },
        reload: function () {
            //reload the results
            if (this.fosterCarerSuspensionAccordionView) {
                this.fosterCarerSuspensionAccordionView.get('results').reload();
            }
            if (this.fosterCarerUnderInvestigationAccordionView) {
                this.fosterCarerUnderInvestigationAccordionView.get('results').reload();
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'results-formatters',
        'app-results',
        'app-tabbed-page',
        'foster-approvals-accordion-views',
        'adoption-approvals-accordion-views',
        'household-accordion-views',
        'training-accordion-views',
        'availability-accordion-views',
        'approval-registrations-view'
    ]
});