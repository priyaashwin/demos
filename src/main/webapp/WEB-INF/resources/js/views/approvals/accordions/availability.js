YUI.add('availability-accordion-views', function(Y) {
    'use-strict';

    var USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates,
        isFosterCarer = function(personTypes) {
            return !!(personTypes || []).find(function(type) {
              return type === 'FOSTER_CARER';
            });
        };

    Y.namespace('app.approvals').FosterCarerSuspensionAccordionView = Y.Base.create('fosterCarerSuspensionAccordionView', Y.usp.app.Results, [], {
        //set the tablePanelType to be an Accordion Table Panel
        tablePanelType: Y.usp.AccordionTablePanel,
        initializer: function(config) {
            this._fosterCarerSuspensionEvts = [
                config.personDetails.after('*:load', function() {
                    //set availability
                    this._displayAvailabilityCheck();
                }, this),
                config.currentFosterCarerRegistration.after('*:load', function() {
                    this._displayAddButtonCheck();
                }, this),
                config.currentFosterCarerRegistration.after('*:error', function() {
                    this.get('currentFosterCarerRegistration').removeAttr('organisation');
                    this._displayAddButtonCheck();
                }, this)
            ];
        },
        destructor: function() {
            this._fosterCarerSuspensionEvts.forEach(function(handle) {
                handle.detach();
            });
            delete this._fosterCarerSuspensionEvts;

            if (this.toolbar) {
                this.toolbar.removeTarget(this);
                this.toolbar.destroy();
                delete this.toolbar;
            }
        },
        _displayAvailabilityCheck: function() {
            var available = this.get('personDetails').get('isAvailableAsFosterCarer') === true;

            var availabilityNode = this.getTablePanel().getAccordionHead().one('.foster-availability');
            if (availabilityNode) {
                if (available) {
                    availabilityNode.removeClass('unavailable').addClass('available');
                } else {
                    availabilityNode.removeClass('available').addClass('unavailable');
                }
            }
        },
        render: function() {
            var permissions = this.get('permissions') || {};

            Y.app.approvals.FosterCarerSuspensionAccordionView.superclass.render.call(this);
            //add availability node
            this.getTablePanel().getAccordionHead().one('h3 a').appendChild('<span class="foster-availability"></span>');

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this._displayAddButtonCheck();

            this._displayAvailabilityCheck();

            return this;
        },
        _displayAddButtonCheck: function() {
            var labels = this.get('labels') || {},
            permissions = this.get('permissions') || {},
            personDetails = this.get('personDetails');

            //remove add button
            this.toolbar.removeButton('addCarerSuspension');

            if(permissions.canAddCarerSuspension && this.get('registeredOrganisationExists')) {

                if(isFosterCarer(personDetails.get('personTypes'))){
                    this.toolbar.addButton({
                        icon: 'fa fa-plus-circle',
                        className: 'pull-right pure-button-active',
                        action: 'addCarerSuspension',
                        title: labels.addCarerSuspension,
                        ariaLabel: labels.addCarerSuspensionAriaLabel,
                        label: labels.add
                    });
                }
            }
        },
        getResultsListModel: function(config) {
            return new Y.usp.childlookedafter.PaginatedFosterCarerSuspensionList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                key: 'startDate',
                label: labels.startDate,
                formatter: USPFormatters.date,
                cellTemplate: USPTemplates.clickable('view availability', 'viewCarerSuspension', permissions.canViewCarerSuspension),
                sortable: true,
                width: '20%'
            }, {
                key: 'endDate',
                label: labels.endDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '20%'
            }, {
                key: 'reason',
                label: labels.suspensionReason,
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.childlookedafter.fosterCarerSuspensionReason.category.codedEntries,
                sortable: true,
                width: '41%',
                allowHTML: true
            }, {
                key: 'placesUnavailable',
                label: labels.placesUnavailable,
                sortable: false,
                width: '10%',
                allowHTML: true
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                allowHTML: true,
                width: '9%',
                formatter: USPFormatters.actions,
                items: [{
                        clazz: 'viewCarerSuspension',
                        title: 'View availability',
                        label: 'view availability',
                        visible: permissions.canViewCarerSuspension
                    },
                    {
                        clazz: 'endCarerSuspension',
                        title: 'Edit availability',
                        label: 'edit availability',
                        visible: function() {
                            return permissions.canEndFosterCarerSuspension && !this.record.get('endDate');
                        }
                    }
                ]
            }];
        }
    }, {
      ATTRS: {
        currentFosterCarerRegistration: {},
        registeredOrganisationExists: {
          getter: function() {
            return this.get('currentFosterCarerRegistration').get('organisation') !== undefined;
          }
        }
      }
    });


    Y.namespace('app.approvals').FosterCarerUnderInvestigationAccordionView = Y.Base.create('fosterCarerUnderInvestigationAccordionView', Y.usp.app.Results, [], {
        //set the tablePanelType to be an Accordion Table Panel
        tablePanelType: Y.usp.AccordionTablePanel,
        initializer: function(config) {
            this._fosterCarerSuspensionEvts = [
                config.currentFosterCarerRegistration.after('*:load', function() {
                    this._displayAddButtonCheck();
                }, this),
                config.currentFosterCarerRegistration.after('*:error', function() {
                    this.get('currentFosterCarerRegistration').removeAttr('organisation');
                    this._displayAddButtonCheck();
                }, this)
            ];
        },
        getResultsListModel: function(config) {
            return new Y.usp.childlookedafter.PaginatedFosterCarerUnderInvestigationList({
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                key: 'startDate',
                label: labels.startDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '25%'
            }, {
                key: 'endDate',
                label: labels.endDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '25%'
            }, {
                key: 'allegedAbuse',
                label: labels.allegedAbuse,
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.childlookedafter.fosterCarerUnderInvestigationAllegedAbuse.category.codedEntries,
                sortable: true,
                width: '25%',
                allowHTML: true
            }, {
                key: 'outcome',
                label: labels.outcome,
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.childlookedafter.fosterCarerUnderInvestigationOutcome.category.codedEntries,
                sortable: true,
                width: '25%',
                allowHTML: true
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                allowHTML: true,
                width: '9%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'updateFosterCarerInvestigation',
                    title: labels.edit,
                    label: labels.edit,
                    visible: function() {
                        return permissions.canUpdateFosterCarerInvestigation && !this.record.get('endDate');
                    }
                }, {
                    clazz: 'endFosterCarerInvestigation',
                    title: labels.end,
                    label: labels.end,
                    visible: function() {
                        return permissions.canEndFosterCarerInvestigation && !this.record.get('endDate');
                    }
                }, {
                    clazz: 'viewFosterCarerInvestigation',
                    title: labels.view,
                    label: labels.view,
                    visible: permissions.canViewFosterCarerInvestigation
                }]
            }];
        },
        render: function() {
            var permissions = this.get('permissions') || {};

            Y.app.approvals.FosterCarerUnderInvestigationAccordionView.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                //set the toolbar node to the Button Holder on the TablePanel
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);
            
            this._displayAddButtonCheck();

            return this;
        },
        _displayAddButtonCheck: function() {
          var labels = this.get('labels') || {},
            permissions = this.get('permissions') || {},
            personDetails = this.get('personDetails');

            //remove add button
            this.toolbar.removeButton('addFosterCarerInvestigation');

            if (permissions.canAddFosterCarerInvestigation && this.get('registeredOrganisationExists')) {

                if(isFosterCarer(personDetails.get('personTypes'))) {

                    this.toolbar.addButton({
                        icon: 'fa fa-plus-circle',
                        className: 'pull-right pure-button-active',
                        action: 'addFosterCarerInvestigation',
                        title: labels.addCarerUnderInvestigation,
                        ariaLabel: labels.addCarerUnderInvestigationAriaLabel,
                        label: labels.add
                    });

                }
            }

            return this;
        }
    }, {
      ATTRS: {
        currentFosterCarerRegistration: {},
        registeredOrganisationExists: {
          getter: function() {
            return this.get('currentFosterCarerRegistration').get('organisation') !== undefined;
          }
        }
      }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'accordion-panel',
        'approvals-view',
        'results-formatters',
        'app-results',
        'usp-childlookedafter-FosterCarerSuspension',
        'usp-childlookedafter-FosterCarerUnderInvestigation',
        'categories-childlookedafter-component-FosterCarerSuspensionReason',
        'categories-childlookedafter-component-FosterCarerUnderInvestigationAllegedAbuse',
        'categories-childlookedafter-component-FosterCarerUnderInvestigationOutcome',
        'results-templates',
        'app-results',
        'app-toolbar'
    ]
});
