YUI.add('adoptive-child', function(Y) {
  'use strict';

  Y.namespace('app.adoptive').ChildListView = Y.Base.create('ChildListView', Y.View, [], {
    template: Y.Handlebars.templates.childList,
    events:{
      '[data-action="remove"]': {
        click: 'removeChild'
      }
    },
    initializer: function() {
      this.childAutocompleteView = new Y.app.ClientAutoCompleteResultView({
        autocompleteURL: this.get('personAutocompleteURL'),
        rowTemplate: 'ROW_TEMPLATE_FULL',
        labels: this.get('labels')
      }).addTarget(this);

      this._evtHandlers = [
        this.after('adoptiveChildAssociationsChange', this.render, this),
        this.after('*:select', this.handleChildSelection, this)
      ];
    },
    destructor: function() {
      this._evtHandlers.forEach(function(handler) {
        handler.detach();
      });

      delete this._evtHandlers;

      if (this.childAutocompleteView) {
        this.childAutocompleteView.removeTarget(this);
        this.childAutocompleteView.destroy();
        delete this.childAutocompleteView;
      }
    },
    render: function() {
      var container = this.get('container'),
          labels = this.get('labels'),
          adoptiveChildAssociations = this.get('adoptiveChildAssociations') || [],
          contentNode = Y.one(Y.config.doc.createDocumentFragment()),
          isAutocompleteDisplayed = !!this.get('personAutocompleteURL');


      contentNode.setHTML(this.template({
        labels: labels,
        adoptiveChildAssociations: adoptiveChildAssociations,
        isAutocompleteDisplayed: isAutocompleteDisplayed
      }));
      
      if (isAutocompleteDisplayed) {
        contentNode.one('#addAdoptiveChildAutocomplete').setHTML(this.childAutocompleteView.render().get('container'));
      }

      container.setHTML(contentNode);

      //re-align dialog
      this.fire('align');

      return this;
    },
    removeChild: function(e) {
      var adoptiveChildAssociations = this.get('adoptiveChildAssociations'),
          currentTarget = e.currentTarget,
          idx;

      e.preventDefault();

      idx = Number(currentTarget.getData('idx'));

      //splice to remove the recipient at idx
      adoptiveChildAssociations.splice(idx, 1);

      this.set('adoptiveChildAssociations', adoptiveChildAssociations);
    },
    handleChildSelection: function(e) {
      var adoptiveChild = this.get('adoptiveChildAssociations') || [],
          selection = e.result.raw || {};

      //don't allow duplicates
      if (!adoptiveChild.find(function(child) {
          return (child.id === selection.id);
        })) {
            adoptiveChild.push({
              name: selection.name,
              personIdentifier: selection.personIdentifier,
              id: selection.id,
            });
      }

      //update children
      this.set('adoptiveChildAssociations', adoptiveChild);
    }
  }, {
    ATTRS: {
      adoptiveChildAssociations: {
        value: undefined
      },
      personAutocompleteURL: {
        value: null
      },
      labels: {
        value: {}
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
             'handlebars-carerApproval-templates',
             'persion-autocomplete-view'
             ]
});
