YUI.add('classification-group-config-form-view', function(Y) {

    Y.namespace('app.classification.configuration').ViewClassificationGroupForm = Y.Base.create('viewClassificationGroupForm', Y.usp.classification.ClassificationWithCodePathView, [], {

        events: {

        },

        initializer: function() {
            this._evtHandlers = [];
        },

        destructor: function() {

        },

        render: function() {
            var container = this.get('container');
            var template = Y.Handlebars.templates["viewClassificationGroupDialog"];

            container.setHTML(template({
                labels: this.get('labels'),
                otherData: this.get('otherData'),
                hierarchyPath: this.get('codePath'),
                modelData: this.get('model').toJSON()
            }));

            return this;
        },

        getInput: function(id) {
            return this.get('container').one('#' + id + '-input');
        },

        getLabel: function(id) {
            return this.get('container').one('#' + id + '-label');
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {}
            },
            codePath: {
                value: '',
                getter: function() {
                    var model = this.get('model').toJSON();
                    if (model.codePath.length > 0) {
                        return model.codePath.join(" -> ");
                    }
                    return '';
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'event-custom',
        'form-util',
        'model-form-link',
        'handlebars',
        'handlebars-helpers',
        'usp-classification-ClassificationGroupWithCodePath',
        'handlebars-classification-configuration-templates'
    ]
});