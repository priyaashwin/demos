YUI.add('classification-config-form-view', function(Y) {

    Y.namespace('app.classification.configuration').ViewClassificationForm = Y.Base.create('viewClassificationForm', Y.usp.classification.ClassificationWithCodePathView, [], {

        events: {

        },

        initializer: function() {
            this._evtHandlers = [];
        },

        destructor: function() {

        },

        render: function() {
            var container = this.get('container');
            var template = Y.Handlebars.templates["viewClassificationDialog"];

            container.setHTML(template({
                labels: this.get('labels'),
                otherData: this.get('otherData'),
                modelData: this.get('model').toJSON(),
                hierarchyPath: this.get('codePath'),
            }));

            return this;
        },

        getInput: function(id) {
            return this.get('container').one('#' + id + '-input');
        },

        getLabel: function(id) {
            return this.get('container').one('#' + id + '-label');
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {}
            },
            codePath: {
                value: '',
                getter: function() {
                    var model = this.get('model').toJSON();
                    if (model.codePath.length > 0) {
                        return model.codePath.join(" -> ");
                    }
                    return '';
                }
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'event-custom',
        'form-util',
        'model-form-link',
        'handlebars',
        'handlebars-helpers',
        'usp-classification-ClassificationWithCodePath',
        'handlebars-classification-configuration-templates'
    ]
});