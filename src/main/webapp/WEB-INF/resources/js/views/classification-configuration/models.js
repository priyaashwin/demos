


YUI.add('classification-configuration-models', function (Y) {
	

	Y.namespace('app.classification.configuration').AddClassificationModel = Y.Base.create('addClassificationModel', 
   			Y.usp.classification.NewClassification, [Y.usp.ModelFormLink], {    
		  form:'#classification-add-form',
		  url:'/eclipse/rest/classificationGroup/{id}/classification'
	});

	Y.namespace('app.classification.configuration').EditClassificationModel = Y.Base.create('editClassificationModel', 
			Y.usp.classification.ClassificationWithCodePath, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {     		  
		form:'#classification-edit-form',
		url: '/eclipse/rest/classification/{id}'
	});
	
	Y.namespace('app.classification.configuration').DeleteClassificationModel = Y.Base.create('deleteClassificationModel',
			Y.usp.classification.ClassificationWithCodePath, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {     		  
    	archivedUrl:'/eclipse/rest/classification/{id}/status?action=archived',
    	url: '/eclipse/rest/classification/{id}',
    	originalUrl: '/eclipse/rest/classification/{id}',
		form:'#classification-delete-form'   	 
	});
	
	Y.namespace('app.classification.configuration').ViewClassificationModel = Y.Base.create('viewClassificationModel',
	        Y.usp.classification.ClassificationWithCodePath, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
	    form: '#classificiation-view-form',
	    url: '/eclipse/rest/classification/{id}'
	});
	
	Y.namespace('app.classification.configuration').AddClassificationGroupModel = Y.Base.create('addClassificationGroupModel', 
   			Y.usp.classification.NewClassificationGroup, [Y.usp.ModelFormLink], {    
		  form:'#classification-group-add-form',
		  url: '/eclipse/rest/classificationGroup/{id}/childGroup',
			  
	});
	
	Y.namespace('app.classification.configuration').EditClassificationGroupModel = Y.Base.create('editClassificationGroupModel', 
			Y.usp.classification.ClassificationGroupWithCodePath, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {     		  
		form:'#classification-group-edit-form',
		url:'/eclipse/rest/classificationGroup/{id}'
	});
	
	Y.namespace('app.classification.configuration').DeleteClassificationGroupModel = Y.Base.create('deleteClassificationGroupModel',
			Y.usp.classification.ClassificationGroupWithCodePath, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {     		  
    	archivedUrl:'/eclipse/rest/classificationGroup/{id}/status?action=archived',
    	url: '/eclipse/rest/classificationGroup/{id}',
    	originalUrl: '/eclipse/rest/classificationGroup/{id}',
		form:'#classification-group-delete-form'   	 
	});
	
    Y.namespace('app.classification.configuration').ViewClassificationGroupModel = Y.Base.create('viewClassificationGroupModel',
            Y.usp.classification.ClassificationGroupWithCodePath, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
        form: '#classificiation-group-view-form',
        url: '/eclipse/rest/classificationGroup/{id}'
    });

}, '0.0.1', {
	
	requires: ['yui-base',
	           'event-custom',
	           'model', 
	           'model-form-link', 
	           'model-transmogrify',		    
	           'usp-classification-NewClassification',
	           'usp-classification-NewClassificationGroup',
	           'usp-classification-ClassificationGroupWithCodePath',
	           'usp-classification-ClassificationWithCodePath',
	           'usp-date'
	          ]
});