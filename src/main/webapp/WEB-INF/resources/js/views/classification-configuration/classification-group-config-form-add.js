

YUI.add('classification-group-config-form-add', function(Y) {

		
	var E = Y.Escape,
    	
		L = Y.Lang,
    	
    	A = Y.Array;
	
	//Declare New Classification group model here

	
	
	Y.namespace('app.classification.configuration').AddClassificationGroupForm = Y.Base.create('addClassificationGroupForm', Y.View, [], {
		
		events: {
			
		},
		
		initializer: function() { 
			this._evtHandlers = [];			
		},
		
		destructor: function(){
			
		},
		

		render: function() {
			
			var container = this.get('container');
			var template = Y.Handlebars.templates["addClassificationGroupDialog"];
     
			container.setHTML(template({
				labels: this.get('labels'),
				otherData: this.get('otherData'),
				modelData: this.get('model').toJSON(),
				hierarchyPath: this.get('codePath')
			}));


			return this;
		},
		
		getInput: function(id) {			
			return this.get('container').one('#'+id+'-input');
		},
		
		
		getLabel: function(id) {			
			return this.get('container').one('#'+id+'-label');
		},
		
		

	},{
		
		ATTRS: {
			
			codedEntries:{					
				value: {
				}				
			},
			codePath: {
				value: '',
				getter:function(){
					   if(this.get('otherData').codePath.length > 0){
						   return this.get('otherData').codePath.join(" -> ");
					   }
	                   return '';
				}
			}
	    }
	
	});	
	
}, '0.0.1', {
	  requires: ['yui-base',
	             'event-custom',
	             'form-util',
	             'model-form-link',
	             'handlebars',
	             'handlebars-helpers',
	             'handlebars-classification-configuration-templates']
	});