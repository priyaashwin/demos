YUI.add('dashboard-controller-view', function (Y) {
    'use-strict';

    var isPresent = function (v) {
        if (v !== null && v !== undefined) {
            if (typeof v === 'string') {
                return v !== 'null' && v !== 'undefined' && v.trim().length > 0;
            } else {
                return true;
            }
        }
        return false;
    }
    Y.namespace('app.home.dashboard').DashboardControllerView = Y.Base.create('dashboardControllerView', Y.View, [], {
        initializer: function (config) {
            var dashboardConfig = config.dashboardConfig || {};
            switch (dashboardConfig.type) {
            case 'google':
                if (dashboardConfig.google && isPresent(dashboardConfig.google.url)) {
                    this.dashboardView = new Y.app.home.dashboard.GoogleDashboardView(dashboardConfig.google || {}).addTarget(this);
                } else {
                    this.dashboardView = new Y.app.home.dashboard.NotConfigured().addTarget(this);
                }
                break;
            default:
                if (dashboardConfig.system && isPresent(dashboardConfig.system.baseUrl) && isPresent(dashboardConfig.system.dashboardId)) {
                    this.dashboardView = new Y.app.home.dashboard.SystemDashboardView(dashboardConfig.system || {}).addTarget(this);
                } else {
                    this.dashboardView = new Y.app.home.dashboard.NotConfigured().addTarget(this);
                }
                break;
            }
        },

        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            if (this.dashboardView) {
                contentNode.append(this.dashboardView.render().get('container'));
            }
            this.get('container').append(contentNode);
            return this;
        },

        destructor: function () {
            if (this.dashboardView) {
                this.dashboardView.removeTarget(this);
                this.dashboardView.destroy();
                delete this.dashboardView;
            }
        }
    }, {
        ATTRS: {
            type: {
                value: ''
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'dashboard-view',
        'model'
    ]
});