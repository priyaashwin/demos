YUI.add('submitted-forms-controller', function(Y) {
  'use-strict';

  var L = Y.Lang,
    USPFormatters = Y.usp.ColumnFormatters,
    USPTemplates = Y.usp.ColumnTemplates,
    FormsFormatters = Y.app.form.ColumnFormatters,
    AttachmentFormatters = Y.app.attachment.ColumnFormatters,
    FUtil = Y.FUtil,
    DB = Y.usp.DataBindingUtil;

  Y.namespace('app.home.form').PaginatedFilteredSubmittedFormsResultList = Y.Base.create("paginatedFilteredSubmittedFormsResultList", Y.usp.form.PaginatedFormInstanceWithAttachmentsCountList, [Y.usp.ResultsFilterURLMixin], {
    whiteList: ["submittedDateFrom", "submittedDateTo", "submitterIds", "formDefinitionUIDs"],
    staticData: {
      useSoundex: false,
      appendWildcard: false
    }
  }, {
    ATTRS: {
      filterModel: {
        writeOnce: 'initOnly'
      }
    }
  });

  Y.namespace('app.home.form').SubmittedFormsFilterModel = Y.Base.create('submittedFormsFilterModel', Y.usp.ResultsFilterModel, [], {
    customValidation: function(attrs) {
      var errors = {},
        submittedDateFrom = attrs['submittedDateFrom'],
        submittedDateTo = attrs['submittedDateTo'],
        labels = this.labels || {},
        validationLabels = labels.validation || {};

      if (L.isNumber(submittedDateFrom) && L.isNumber(submittedDateTo)) {
        if (submittedDateFrom > submittedDateTo) {
          errors['submittedDateFrom'] = validationLabels.endBeforeSubmittedDate;
        }
      }
      return errors;
    }
  }, {
    ATTRS: {
      submitterIds: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      formDefinitionUIDs: {
        USPType: 'UUID',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      submittedDateFrom: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateFrom,
        getter: Y.usp.ResultsFilterModel.getDateFrom
      },
      submittedDateTo: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateTo,
        getter: Y.usp.ResultsFilterModel.getDateTo
      }
    }
  });

  Y.namespace('app.home.form').SubmittedFormsFilterView = Y.Base.create('submittedFormsFilterView', Y.usp.app.ResultsFilter, [], {
    template: Y.Handlebars.templates['submittedFormsFilter'],
    events: {
      '#submittedDateRangeShortcuts a': {
        click: '_handleSubmittedDatePreselect'
      }
    },
    initializer: function() {
      this.submittedDateFromCalendar = new Y.usp.CalendarPopup();
      this.submittedDateToCalendar = new Y.usp.CalendarPopup();

      //set as bubble targets
      this.submittedDateFromCalendar.addTarget(this);
      this.submittedDateToCalendar.addTarget(this);
    },
    destructor: function() {
      //destroy the calendars
      this.submittedDateFromCalendar.destroy();
      delete this.submittedDateFromCalendar;

      this.submittedDateToCalendar.destroy();
      delete this.submittedDateToCalendar;
    },
    render: function() {
      var container = this.get('container');

      //call into superclass to render
      Y.app.home.form.SubmittedFormsFilterView.superclass.render.call(this);

      //set the input node for the calendar
      this.submittedDateFromCalendar.set('inputNode', container.one('input[name="submittedDateFrom"]'));
      this.submittedDateToCalendar.set('inputNode', container.one('input[name="submittedDateTo"]'));

      this.submittedDateFromCalendar.render();
      this.submittedDateToCalendar.render();

      //build a list of submitters
      this._getFormSubmitters().then(function(submitters) {
        this._updateList('select[name="submitterIds"]', submitters.toJSON());
      }.bind(this));

      //build a list of form names
      this._getFormInstanceTypes().then(function(types) {
        this._updateList('select[name="formDefinitionUIDs"]', types.toJSON(), 'formDefinitionId');
      }.bind(this));

      return this;
    },
    _getFormSubmitters: function() {
      var model = new Y.usp.person.PersonList({
        url: this.get('submittersURL')
      });

      return new Y.Promise(function(resolve, reject) {
        var data = model.load(function(err) {
          if (err !== null) {
            //failed to get the form instance types for some reason so reject the promise
            reject(err);
          } else {
            //success, so resolve the promise
            resolve(data);
          }
        });
      });
    },
    _getFormInstanceTypes: function() {
          var model = new Y.usp.form.PaginatedFormDefinitionList({
            //require minimum of ReadSummary access
            requestedAccess: 'READ_SUMMARY',
            url: L.sub(this.get('submittedFormInstanceTypesURL'), {
             sortBy: encodeURI('[{"name":"asc"}]')
            })
          });
      return new Y.Promise(function(resolve, reject) {
        var data = model.load(function(err) {
          if (err !== null) {
            //failed to get the form instance types for some reason so reject the promise
            reject(err);
          } else {
            //success, so resolve the promise
            resolve(data);
          }
        });
      });
    },
    _updateList: function(nodeId, values, valueAttr) {
      var container = this.get('container'),
        select = container.one(nodeId);

      if (select) {
        FUtil.setSelectOptions(select, values, null, null, false, true, valueAttr);

        //now we have out select box populated - we need to ensure that any current selection is made
        DB.setElementValue(select.getDOMNode(), select.get('name'));
      }
    },
    _handleSubmittedDatePreselect: function(e) {
      e.preventDefault();

      var t = e.currentTarget,
        today = new Date(),
        submittedDateFrom;

      // Filtering includes today, so a day less than required.
      if (t.hasClass('lastWeek')) {
        submittedDateFrom = Y.Date.addDays(today, -6); // last week
      } else if (t.hasClass('lastMonth')) {
        submittedDateFrom = Y.Date.addDays(today, -29); // last 30 days
      } else {
        submittedDateFrom = Y.Date.addDays(today, -364); // last year
      }

      this.get("model").setAttrs({
        submittedDateTo: null,
        submittedDateFrom: submittedDateFrom.getTime()
      });

      this.applyFilter();
    }
  }, {
    ATTRS: {
      submittersURL: {
        value: null
      },
      submittedFormInstanceTypesURL: {
        value: null
      }
    }
  });
  Y.namespace('app.home.form').SubmittedFormsResults = Y.Base.create('submittedFormsResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      return new Y.app.home.form.PaginatedFilteredSubmittedFormsResultList({
        url: config.url,
        //Required access level is READ_SUMMARY as user needs to be able to read form
        requestedAccess: 'READ_SUMMARY',
        //plug in the filter model
        filterModel: config.filterModel
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;
      return ([{
        key: 'subjectType',
        label: ' ',
        width: '2%',
        sortable: false,
        allowHTML: true,
        infoClass: 'tool-tip',
        formatter: FormsFormatters.formSubjectIcon
      }, {
        key: 'formDefinitionName',
        label: labels.type,
        width: '18%',
        formatter: FormsFormatters.formName,
        cellTemplate: USPTemplates.clickable(labels.viewForm, 'view', permissions.canView)
      }, {
        key: 'formIdentifier',
        label: labels.formId,
        width: '11%',
      }, {
        key: 'subjectName',
        label: labels.subject,
        width: '20%',
        formatter: FormsFormatters.formSubject,
        cellTemplate: USPTemplates.clickable(labels.viewSubject, 'viewSubject', permissions.canViewSubject)
      }, {
        key: 'dateSubmitted',
        label: labels.submittedDate,
        sortable: true,
        width: '15%',
        formatter: USPFormatters.date
      }, {
        key: 'submitterName',
        label: labels.submitterName,
        width: '20%'
      }, {
        key: "attachmentsCount",
        label: labels.attachmentCount,
        width: '4%',
        allowHTML: true,
        formatter: AttachmentFormatters.attachment
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '10%',
        formatter: USPFormatters.actions,
        items: [{
          clazz: 'view',
          title: labels.viewFormTitle,
          label: labels.viewForm,
          enabled: permissions.canView
        }, {
          clazz: 'reject',
          title: labels.rejectFormTitle,
          label: labels.rejectForm,
          visible: function() {
            if (permissions.canReject === true && this.record.hasAccessLevel('WRITE') === true) {
              return (this.data.requireAuthorisation && this.data.status === 'SUBMITTED');
            }
            return false;
          }
        }, {
          clazz: 'authorise',
          title: labels.approveFormTitle,
          label: labels.approveForm,
          visible: function() {
            if (permissions.canAuthorise === true && this.record.hasAccessLevel('WRITE') === true) {
              return (this.data.requireAuthorisation && this.data.status === 'SUBMITTED');
            }
            return false;
          }
        }, {
          clazz: 'reviewHistory',
          title: labels.reviewHistoryTitle,
          label: labels.reviewHistory,
          enabled: function() {
            if (permissions.canView === true) {
              return (this.data.hasRejections || ((this.data.dateSubmitted !== null) && (this.data.dateCompleted !== null)));
            }
            return false;
          },
          visible: function() {
            if (permissions.canView === true && this.record.hasAccessLevel('READ_DETAIL') === true) {
              return (this.data.requireAuthorisation);
            }
            return false;
          }
        }]
      }]);
    }
  }, {
    ATTRS: {
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
          }];
        }
      }
    }
  });

  Y.namespace('app.home.form').SubmittedFormsFilter = Y.Base.create('submittedFormsFilter', Y.usp.app.Filter, [], {
    filterModelType: Y.app.home.form.SubmittedFormsFilterModel,
    filterType: Y.app.home.form.SubmittedFormsFilterView,
    filterContextTemplate: Y.Handlebars.templates["submittedFormsActiveFilter"]
  });


  Y.namespace('app.home.form').SubmittedFormsResultsView = Y.Base.create('submittedFormsResultsView', Y.usp.app.FilteredResults, [], {
    filterType: Y.app.home.form.SubmittedFormsFilter,
    resultsType: Y.app.home.form.SubmittedFormsResults
  });


  Y.namespace('app.home.form').SubmittedFormsControllerView = Y.Base.create('submittedFormsControllerView', Y.View, [], {
    initializer: function(config) {
      this.toolbar = new Y.usp.app.AppToolbar({
        permissions: config.permissions
      }).addTarget(this);

      var resultsConfig = {
        searchConfig: config.searchConfig,
        filterConfig: config.filterConfig,
        filterContextConfig: config.filterContextConfig,
        permissions: config.permissions
      };

      this.results = new Y.app.home.form.SubmittedFormsResultsView(resultsConfig).addTarget(this);

      this.formAttachmentDialog = new Y.app.form.FormInstanceAttachmentDialog(config.attachmentDialogConfig).addTarget(this).render();
      this.formDialog = new Y.app.form.FormInstanceDialog(config.formDialogConfig).addTarget(this).render();

      this._evtHandlers = [
        Y.Global.on('formEditorStatusChangeEvent', this.findStatusFormDialog, this),
        this.on('*:saved', this.results.reload, this.results),
        this.on('submittedFormsResults:view', this.handleViewFormInstance, this),
        this.on('submittedFormsResults:viewSubject', this.handleViewSubject, this),
        this.on('submittedFormsResults:authorise', this.showAuthoriseFormDialog, this),
        this.on('submittedFormsResults:reject', this.showRejectFormDialog, this),
        this.on('submittedFormsResults:reviewHistory', this.handleReviewHistory, this),
        this.on('*:manageFiles', this.showManageFilesAttachmentDialog, this),
        this.on('formInstanceAttachmentResults:view', this.handleDownloadFile, this)
      ];
    },
    destructor: function() {
      this._evtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._evtHandlers;

      this.formAttachmentDialog.removeTarget(this);
      this.formAttachmentDialog.destroy();
      delete this.formAttachmentDialog;

      this.formDialog.removeTarget(this);
      this.formDialog.destroy();
      delete this.formDialog;

      this.results.removeTarget(this);
      this.results.destroy();
      delete this.results;
      this.toolbar.removeTarget(this);
      this.toolbar.destroy();
      delete this.toolbar;
    },
    render: function() {
      var contentNode = Y.one(Y.config.doc.createDocumentFragment());

      contentNode.append(this.results.render().get('container'));

      this.get('container').setHTML(contentNode);

      return this;
    },
    _getSubjectFromRecord: function(record) {
      return {
        subjectType: record.get('subjectType').toLowerCase(),
        subjectName: record.get('subjectName'),
        subjectIdentifier: record.get('subjectIdentifier'),
        subjectId: record.get('subjectId')
      };
    },
    handleViewFormInstance: function(e) {
      var form = e.record,
        formEditorURL = this.get('formEditorURL');

      if (form) {
        Y.app.FormLauncher.open(formEditorURL, form.get('id'), form.get('formDefinitionName'));
      }
    },
    handleViewSubject: function(e) {
      var subject = this._getSubjectFromRecord(e.record),
        permissions = this.get('permissions'),
        viewSubjectURL = this.get('viewSubjectURL');

      //show the processing view
      this.formDialog.showView('processing');
      
      if (subject.subjectType === 'group' && permissions.canViewGroup) {
        window.location = L.sub(viewSubjectURL, subject);
      } else if (subject.subjectType === 'person' && permissions.canViewPerson) {
        window.location = L.sub(viewSubjectURL, subject);
      }
    },
    showManageFilesAttachmentDialog: function(e) {
      var record = e.record,
        permissions = this.get('permissions'),
        subject = this._getSubjectFromRecord(record) || {};

      //L3 READ_DETAIL required to see files
      if (permissions.canView && permissions.canViewAttachment && record.hasAccessLevel('READ_DETAIL')) {
        this.formAttachmentDialog.showView('manageFiles', {
          model: record,
          otherData: {
            subject: subject,
            name: record.get('formDefinitionName')
          }
        }, function(view) {
          //remove the upload button
          this.removeButton('uploadFileButton', Y.WidgetStdMod.FOOTER);
        });
      }
    },
    handleDownloadFile: function(e) {
      var record = e.record,
        formInstance = e.formInstance,
        //note this is the permission from the event, i.e. attachment permissions
        permissions = e.permissions,
        downloadURL;

      //must have view permission and L3 READ_DETAIL access
      if (permissions.canView && formInstance.hasAccessLevel('READ_DETAIL')) {
        downloadURL = L.sub(this.get('downloadAttachmentURL'), {
          id: record.get('id'),
          formInstanceId: formInstance.get('id')
        });
        //attempt the download
        Y.later(500, this, function() {
          window.location.href = downloadURL;
        });
      }
    },
    findStatusFormDialog: function(e) {
      var entry = {};
      entry.record = new Y.usp.form.FormInstance(e.record.toJSON());
      switch(e.statusButton) {
        case 'submit':
          this.showSubmitFormDialog(entry);
        break;
        case 'complete':
          this.showCompleteFormDialog(entry);
        break;
        case 'authorise':
          this.showAuthoriseFormDialog(entry);
        break;
      }
    },
    _showFormDialog: function(record, view, model) {
      var subject = this._getSubjectFromRecord(record);

      //show the submit dialog
      this.formDialog.showView(view, {
        model: model,
        otherData: {
          subject: subject || {},
          name: record.get('formDefinitionName')
        }
      }, function() {
        //Get the button by name out of the footer and enable it.
        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showAuthoriseFormDialog: function(e) {
      var record = e.record,
        dialogConfig = this.get('formDialogConfig'),
        url = dialogConfig.views.approveForm.config.url;

      if (record.hasAccessLevel('WRITE')) {
        this._showFormDialog(record, 'approveForm', new Y.app.form.FormReviewCommentsForm({
          url: L.sub(url, {
            action: 'authorised'
          }),
          id: record.get('id')
        }));
      }
    },
    showRejectFormDialog: function(e) {
      var record = e.record,
        dialogConfig = this.get('formDialogConfig'),
        url = dialogConfig.views.rejectForm.config.url;

      if (record.hasAccessLevel('WRITE')) {
        this._showFormDialog(record, 'rejectForm', new Y.app.form.FormReviewCommentsForm({
          url: L.sub(url, {
            action: 'rejected'
          }),
          id: record.get('id')
        }));
      }
    },
    handleReviewHistory: function(e) {
      var reviewHistoryURL = this.get('reviewHistoryURL'),
          record = e.record,
          permissions = this.get('permissions');

      if (permissions.canView){ 
        //show the processing view
        this.formDialog.showView('processing');


        window.location = L.sub(reviewHistoryURL, {
            id: record.get('id')
        });
      }
  }

    
  }, {
    ATTRS: {
      downloadAttachmentURL: {},
      formEditorURL: {},
      viewSubjectURL: {},
      reviewHistoryURL:{}
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
            'view',
            'promise',
            'app-toolbar',
            'app-filter',
            'results-filter',
            'app-filtered-results',
            'results-formatters',
            'results-templates',
            'handlebars-helpers',
            'handlebars-form-templates',
            'calendar-popup',
            'usp-person-Person',
            'results-table-search-state-plugin',
            'usp-form-FormInstanceWithAttachmentsCount',
            'results-table-menu-plugin',
            'results-table-keyboard-nav-plugin',
            'results-table-search-state-plugin',
            'data-binding',
            'usp-form-FormDefinition',
            'form-util',
            'form-results-formatters',
            'attachment-results-formatters',
            'usp-form-FormInstanceAttachment',
            'form-attachment-dialog',
            'form-launcher',
            'form-instance-dialog'
            ]
});