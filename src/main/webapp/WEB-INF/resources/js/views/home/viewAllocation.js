YUI.add('home-allocation', function(Y) {
    var A = Y.Array, 
        L = Y.Lang,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates;
    //FIXME - This is a hack advised by Barney until the framework is updated to allow access to
    // attributes of VOs where the attribute is a type of VO.
    Y.Do.after(function() {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({
            key: 'accessedPerson',
            locator: 'accessedPerson'
        });
        schema.resultFields.push({
            key: 'accessedPerson!gender',
            locator: 'accessedPerson.gender'
        });
    }, Y.usp.relationshipsrecording.PersonAccessWarningCount, 'getSchema', Y.usp.relationshipsrecording.PersonAccessWarningCount);
    
    Y.namespace('app').HomeAllocationView = Y.Base.create('homeAllocationView', Y.View, [], {
        initializer:function(config){
          var recentRecordsViewConfig=config.recentRecordsViewConfig||{},
              caseLoadViewConfig=config.caseLoadViewConfig||{};
          
          this.recentRecordsView=new Y.app.HomeRecentRecordsView(recentRecordsViewConfig);
          
          this.caseLoadView=new Y.app.HomeCaseLoadView(caseLoadViewConfig);
                    
          //update style of toolbar buttons to indicate enablement
          this.get('toolbarNode').all('.pure-button-loading').removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
        },
        render:function(){
            var container=this.get('container');
            
            this.recentRecordsView.render();
            this.caseLoadView.render();
            
            return this;
        },
        destructor:function(){
          if(this.recentRecordsView){
            this.recentRecordsView.destroy();
            
            delete this.recentRecordsView;
          }
          
          if(this.caseLoadView){
            this.caseLoadView.destroy();
            
            delete this.caseLoadView;
          }
        }
    },{
      ATTRS:{
        recentRecordsViewConfig:{
          
        },
        caseLoadViewConfig:{
          
        },
        toolbarNode: {
          getter: Y.one,
          writeOnce: true
        }
      }
    });
    
    
    Y.namespace('app').HomeRecentRecordsView = Y.Base.create('homeRecentRecordsView', Y.View, [], {
        initializer: function(config) {
            var searchConfig=config.searchConfig,
                labels = searchConfig.labels,
                codedEntries = this.get('codedEntries');
            //configure the provider results table
            this.results = new Y.usp.PaginatedResultsTable({
                sortBy: [{
                    'accessDate': 'desc'
                }],
                //declare the columns
                columns: [
                    // TODO - There is a better way to write this, probably a function in the context 
                    // where you pass the current value and it return boolean to display or not. 
                    // A plugin could be created based on this so don't copy the code below.
                    {
                        label: 'Date',
                        formatter: function(o) {
                            var accessDate = Y.USPDate.formatDateValue(o.data.accessDate);
                            var previousAccessDate = this.data.toJSON()[o.rowIndex - 1];
                            if (previousAccessDate) {
                                var previousAccessDateString = Y.USPDate.formatDateValue(previousAccessDate.accessDate);
                                if (accessDate === previousAccessDateString) {
                                    return;
                                } else {
                                    return accessDate;
                                }
                            } else {
                                return accessDate;
                            }
                        },
                        width: '4%'
                    }, {
                    	key:"name",
                        label: labels.name,
                        allowHTML: true,
                        formatter: Y.app.ColumnFormatters.linkablePopOverWarnings, 
                        width: '10%',
                        //cellTemplate: USPTemplates.clickable(labels.showPerson, 'showPerson')
                    }, {
                        key: 'accessedPerson!gender',
                        label: labels.gender,
                        formatter: USPFormatters.codedEntry,
                        codedEntries: codedEntries.gender,
                        width: '1%'
                    }, {
                        key: 'accessedPerson',
                        label: labels.dob,
                        formatter: Y.app.ColumnFormatters.lifeStateFormatter,
                        allowHTML: true,
                        width: '9%'
                    }
                ],
                //configure the data set
                data: new Y.usp.relationshipsrecording.PaginatedPersonAccessWarningCountList({
                    url: L.sub(searchConfig.url, {userId:searchConfig.userId})
                }),
                noDataMessage: searchConfig.noDataMessage,
                plugins: [
                    //plugin Menu handler
                    {
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    },
                    //plugin Keyboard nav
                    {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }
                ]
            });
            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                title:searchConfig.title,
                tableId: this.name
            });
            this._evtHandlers = [this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this)];
            // Add event targets
            this.results.addTarget(this);
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }
            if (this.results) {
                //destroy the results table
                this.results.destroy();
                delete this.results;
            }
            if (this._tablePanel) {
                this._tablePanel.destroy();
                delete this._tablePanel;
            }
        },
        render: function() {
            var container = this.get('container'),
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));
            //render results
            this._tablePanel.renderResults(this.results);
            //finally set the content into the container
            container.setHTML(contentNode);
            return this;
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                record = this.results.getRecord(target.get("id"));
            e.preventDefault();
            if (target.hasClass('switch-focus')) {
                window.location=L.sub(this.get('searchConfig').viewPersonUrl,{personId:record.get('accessedPerson.id')});
            }
        },
        reload: function(reset) {
            this.results.reload();
        }
    }, {
        ATTRS: {
            searchConfig: {
                value: {
                    labels: {},
                    title: '',
                    noDataMessage: '',
                    url: '',
                    userId:null,
                    viewPersonUrl:''
                }
            },
            codedEntries: {
                value: {
                    gender: Y.uspCategory.person.gender.category.codedEntries
                }
            }
        }
    });
    Y.namespace('app').HomeCaseLoadView = Y.Base.create('homeCaseLoadView', Y.View, [], {
        initializer: function(config) {
            var searchConfig=config.searchConfig,
                labels = searchConfig.labels,
                codedEntries = this.get('codedEntries');
            //configure the provider results table
            this.results = new Y.usp.PaginatedResultsTable({                
                sortBy: [{
                    'surname': 'asc'
                }, {
                    'forename': 'asc'
                }],
                //declare the columns
                columns: [{
                	key:"name",
                	label: labels.name,
                    allowHTML: true,
                    formatter: Y.app.ColumnFormatters.linkablePopOverWarnings,
                    width: '12%',
                    //cellTemplate: USPTemplates.clickable(labels.showPerson, 'showPerson')
                }, {
                    key: 'gender',
                    label: labels.gender,
                    formatter: USPFormatters.codedEntry,
                    codedEntries: codedEntries.gender,
                    width: '3%'
                }, {
                    label: labels.dob,
                    formatter: Y.app.ColumnFormatters.lifeStateFormatter,
                    allowHTML: true,
                    width: '9%'
                }],
                //configure the data set
                data: new Y.usp.relationshipsrecording.PaginatedAsymmetricPersonPersonRelationshipResultList({
                    url: L.sub(searchConfig.url, {userId:searchConfig.userId})
                }),
                noDataMessage: searchConfig.noDataMessage,
                plugins: [
                    //plugin Menu handler
                    {
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    },
                    //plugin Keyboard nav
                    {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }
                ]
            });
            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                title:searchConfig.title,
                tableId: this.name
            });
            this._evtHandlers = [this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this)];
            // Add event targets
            this.results.addTarget(this);
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }
            if (this.results) {
                //destroy the results table
                this.results.destroy();
                delete this.results;
            }
            if (this._tablePanel) {
                this._tablePanel.destroy();
                delete this._tablePanel;
            }
        },
        render: function() {
            var container = this.get('container'),
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));
            //render results
            this._tablePanel.renderResults(this.results);
            //finally set the content into the container
            container.setHTML(contentNode);
            return this;
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                record = this.results.getRecord(target.get("id"));
            
            e.preventDefault();
            if (target.hasClass('switch-focus')) {
                window.location=L.sub(this.get('searchConfig').viewPersonUrl,{personId:record.get('personId')});
            }
        },
        reload: function(reset) {
            this.results.reload();
        }
    }, {
        ATTRS: {
            searchConfig: {
                value: {
                    labels: {},
                    title: '',
                    noDataMessage: '',
                    url: '',
                    userId:null,
                    viewPersonUrl:''
                }
            },
            codedEntries: {
                value: {
                    gender: Y.uspCategory.person.gender.category.codedEntries
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base', 
               'accordion-panel', 
               'event-custom', 
               'usp-date', 
               'usp-relationshipsrecording-PersonAccessWarningCount', 
               'usp-relationshipsrecording-AsymmetricPersonPersonRelationshipResult', 
               'categories-person-component-Gender', 
               'paginated-results-table', 
               'results-formatters', 
               'results-templates', 
               'results-table-keyboard-nav-plugin', 
               'caserecording-results-formatters']
});