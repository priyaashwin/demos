YUI.add('checklist-home-tabs', function(Y) {
    var A = Y.Array,
        L = Y.Lang;
    var CHECKLIST_TYPE = 'BasicChecklistInstanceWithOwnerAndSubject';

    Y.namespace('app.checklist').HomeTabsView = Y.Base.create('homeTabsView', Y.app.tab.FilteredTabsView, [], {
        events: {
            '#listsRoot .card-list-container': {
                'reassignChecklist': 'reassignChecklist',
                'pauseChecklist': 'pauseChecklist',
                'resumeChecklist': 'resumeChecklist',
                'resetChecklistEndDate': 'resetChecklistEndDate',
                'startChecklist': 'startChecklist',
                'deleteChecklist': 'deleteChecklist',
                'removeChecklist': 'removeChecklist',
                'checklistInstancePrioritiseView:saved': '_reloadLists',
                'loadChecklistPage': 'handleTaskLinkClick'
            }
        },
        views: {
            summary: {
                type: Y.app.checklist.MySummaryView
            },
            own: {
                type: Y.app.checklist.ChecklistHomeTab
            },
            team: {
                type: Y.app.checklist.ChecklistHomeTab
            },
            members: {
                type: Y.app.checklist.ChecklistHomeTab
            }
        },
        tabs: {
            summary: {},
            own: {},
            team: {},
            members: {}
        },
        initializer: function(config) {
            var dialogConfig = config.dialogConfig || {},
                currentUserId = config.currentUserId,
                tabs = this.get('tabs');

            if (!config.isMySummaryTabDisplayed) {
                //remove the summary tab from the app-tabs
                if (tabs['summary']) {
                    delete tabs['summary'];
                }
            }

            if (!this.get('canViewMembersChecklists')) {
                //remove the members tab from the app-tabs
                if (tabs['members']) {
                    delete tabs['members'];
                }
            }
            //create checklist instance status dialog view
            this.checklistInstanceStatusDialogView = new Y.app.ChecklistInstanceStatusDialogView({
                dialogConfig: dialogConfig,
                currentPersonId: currentUserId
            }).render();
            this._evtHandlers = [
                //subscribe to events where we need to update our card list
                this.on(['checklistInstanceStatusChangeView:saved',
                  'checklistInstancePauseView:saved',
                  'checklistInstanceResumeView:saved',
                  'checklistInstanceRemoveView:saved',
                  'checklistInstanceReassignView:saved',
                  'checklistInstanceResetEndDateView:saved',
                  'checklistInstanceStartView:saved'], this._reloadLists, this),
                this.after('activeTabChange', this._handleActiveTabChange, this),
            ];
            //add event targets
            this.checklistInstanceStatusDialogView.addTarget(this);
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }
            if (this.checklistInstanceStatusDialogView) {
                this.checklistInstanceStatusDialogView.removeTarget(this);
                this.checklistInstanceStatusDialogView.destroy();
                delete this.checklistInstanceStatusDialogView;
            }
        },
        render: function() {
            var container = this.get('container'),
                toolbar = this.get('toolbar');

            //plug in mask - will unplug when the container is destroyed
            container.plug(Y.Plugin.BusyOverlay, {
                css: 'card-view-mask'
            });
            Y.app.checklist.HomeTabsView.superclass.render.call(this);
            return this;
        },
        showMask: function() {
            this.get('container').busy.show();
        },
        hideMask: function() {
            this.get('container').busy.hide();
        },
        reassignChecklist: function(e) {
            var checklist,
                subject,
                detail = e._event.detail || {};
            checklist = detail._type === CHECKLIST_TYPE ? detail : null;
            if (checklist) {
                subject = checklist.subject || {};
                this.checklistInstanceStatusDialogView.showReassignChecklistDialog(checklist, subject);
            }
        },
        pauseChecklist: function(e) {
            var checklist,
                subject,
                detail = e._event.detail || {};
            checklist = detail._type === CHECKLIST_TYPE ? detail : null;
            if (checklist) {
                subject = checklist.subject || {};
                this.checklistInstanceStatusDialogView.showPauseChecklistDialog(checklist, subject);
            }
        },
        resumeChecklist: function(e) {
            var checklist,
                subject,
                detail = e._event.detail || {};
            checklist = detail._type === CHECKLIST_TYPE ? detail : null;
            if (checklist) {
                subject = checklist.subject || {};
                this.checklistInstanceStatusDialogView.showResumeChecklistDialog(checklist, subject);
            }
        },
        resetChecklistEndDate: function(e) {
            var checklist,
                subject,
                detail = e._event.detail || {};
            checklist = detail._type === CHECKLIST_TYPE ? detail : null;
            if (checklist) {
                subject = checklist.subject || {};
                this.checklistInstanceStatusDialogView.showResetChecklistEndDateDialog(checklist, subject);
            }
        },
        startChecklist: function(e) {
            var checklist,
                subject,
                detail = e._event.detail || {};
            checklist = detail._type === CHECKLIST_TYPE ? detail : null;
            if (checklist) {
                subject = checklist.subject || {};
                this.checklistInstanceStatusDialogView.showChecklistStartDateDialog(checklist, subject);
            }
        },
        deleteChecklist: function(e) {
            var checklist,
                subject,
                detail = e._event.detail || {};
            checklist = detail._type === CHECKLIST_TYPE ? detail : null;
            if (checklist) {
                subject = checklist.subject || {};
                this.checklistInstanceStatusDialogView.showDeleteChecklistDialog(checklist, subject);
            }
        },
        removeChecklist: function(e) {
            var checklist,
                subject,
                detail = e._event.detail || {};
            checklist = detail._type === CHECKLIST_TYPE ? detail : null;
            if (checklist) {
                subject = checklist.subject || {};
                this.checklistInstanceStatusDialogView.showRemoveChecklistDialog(checklist, subject);
            }
        },
        handleTaskLinkClick: function(e) {
            var record = e._event.detail.record,
                checklist = record.checklistInstance,
                subject = checklist.subject || {};
            //fire an event to update the menu to be in the scope of the person (always ensure the menu is correct)
            Y.fire('currentSubject:set', {
                id: subject.id,
                type: (subject.subjectType || '').toLowerCase()
            });
            //go to the checklist instance page
            window.location = L.sub(this.get('taskURL'), {
              checklistId: checklist.id,
              taskId: record.taskDefinitionId
            });
        },
        _handleActiveTabChange: function(e) {
            this.setContextHeader(e);
        },
        setContextHeader: function(e) {
            var tabInfo = this.get('tabs')[e.newVal] || {};
            this.setContextHeaderTitle(tabInfo.title);
        },
        setContextHeaderTitle: function(_title) {
            var contextHeaderNode = this.get('contextHeaderNode'),
                title = _title || '';

            if (contextHeaderNode) {
                //update the title
                contextHeaderNode.setHTML(title);
            }
        },
        _reloadLists: function() {
          this.get('activeView').reloadLists();
        }
    }, {
        ATTRS: {
            currentUserId: {},
            viewURL: {
                value: ''
            },
            taskURL: {
                value: ''
            },
            /**
             * @attribute contextHeaderNode
             * @description The node that hosts the context title for the page
             * @default null
             * @type HTMLElement|Node|String
             */
            contextHeaderNode: {
                getter: Y.one,
                writeOnce: true
            },
            /**
             * @attribute canViewMembersChecklists
             * @description Controls access to the members tab
             * @default false
             * @type Boolean
             */
            canViewMembersChecklists: {
                value: false
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
              'filtered-tabs-view',
              'checklist-home-tab',
              'checklist-instance-dialog-view',
              'gallery-busyoverlay']
});