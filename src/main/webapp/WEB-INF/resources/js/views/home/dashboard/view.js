YUI.add('dashboard-view', function (Y) {
    'use-strict';

    /* global uspDashboard, ReactDOM */

    Y.namespace('app.home.dashboard').NotConfigured = Y.Base.create('notConfigured', Y.usp.View, [], {
        template: Y.Handlebars.templates.dashboardNotConfigured,
        render: function () {

            this.get('container').setHTML(this.template({}));

            return this;
        }
    });

    Y.namespace('app.home.dashboard').GoogleDashboardView = Y.Base.create('googleDashboardView', Y.usp.View, [], {
        template: Y.Handlebars.templates.dashboardView,
        render: function () {
            var html = this.template({
                dashboardUrl: this.get('url')
            });

            this.get('container').setHTML(html);

            return this;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app.home.dashboard').SystemDashboardView = Y.Base.create('systemDashboardView', Y.usp.ReactView, [], {
        reactFactoryType: uspDashboard.Dashboard,
        renderComponent: function () {
            if (this.reactAppContainer) {
                var component;

                ReactDOM.render(this.componentFactory({
                    dashboardId: this.get('dashboardId'),
                    baseUrl: this.get('baseUrl'),
                    colours : this.get('colours'),
                    borderColours : this.get('borderColours')
                }), this.reactAppContainer.getDOMNode(), function () {
                    component = this;
                });

                this.component = component;
            }
        }
    }, {
        ATTRS: {
            baseUrl: {
                value: ''
            },
            colours: {},
            borderColours : {},
            dashBoardId: {}
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'usp-view',
        'usp-react-view',
        'handlebars-dashboard-templates'
    ]
});