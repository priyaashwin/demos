YUI.add('authorisation-controller-view', function(Y) {
    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.home.authorisation').AuthorisationTabsView = Y.Base.create('authorisationTabsView', Y.usp.app.AppTabbedPage, [], {
        views: {
            forms: {
                type: Y.app.home.form.SubmittedFormsControllerView
            },
            warnings: {
                type: Y.app.home.authorisation.AuthorisationWarningsContainerView
            }
        },
        tabs: {
            forms: {},
            warnings: {}
        },
        initializer:function(config){
          var permissions=config.permissions||{};
          var tabs = this.get('tabs');
          
          if (!(permissions.canAuthoriseForm&&permissions.canViewForm)){
            //remove the authorise form tab
            if (tabs['forms']) {
                delete tabs['forms'];
            }
          }
          if(!(permissions.canAuthoriseWarning&&permissions.canViewWarning)){
            //remove warnings tab
            if (tabs['warnings']) {
                delete tabs['warnings'];
            }
          }
        }
    });

    Y.namespace('app.home.authorisation').AuthorisationControllerView = Y.Base.create('authorisationControllerView', Y.View, [], {
      initializer: function(config) {
          var tabsConfig = config.tabs;
          
          this.authorisationTabs = new Y.app.home.authorisation.AuthorisationTabsView({
              container: '#tabs',
              root: config.root,
              tabs: tabsConfig,
              permissions: config.permissions
          }).addTarget(this);
      },

      render: function() {
        var contentNode = Y.one(Y.config.doc.createDocumentFragment()),
          tabs, activeTab;
        
        this.get('container').setHTML("loading ...");
        
        //add the tabs container
        contentNode.append(this.authorisationTabs.render().get('container'));

        this.get('container').setHTML(contentNode);

        if (this.authorisationTabs.hasRoute(this.authorisationTabs.getPath())) {
            this.authorisationTabs.dispatch();
        } else {
          //get the list of tabs from the view and select the first
          tabs=this.authorisationTabs.get('tabs');
          if(tabs){
            if(tabs['forms']){
              activeTab='forms';
            }else if(tabs['warnings']){
              activeTab='warnings';
            }
            if(activeTab){
              this.authorisationTabs.activateTab(activeTab);
            }
          }
        }
      }
    }, {
       ATTRS: {}
    });
}, '0.0.1', {
     requires: [
         'yui-base',
         'view',
         'promise',
         'app-toolbar',
         'app-filter',
         'app-tabbed-page',
         'authorisation-warnings-view',
         'submitted-forms-controller'
     ]
 });