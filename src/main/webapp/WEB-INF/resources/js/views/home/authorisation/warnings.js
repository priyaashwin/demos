YUI.add('authorisation-warnings-view', function (Y) {
    'use-strict';

    var L = Y.Lang,
        A = Y.Array,
        O = Y.Object,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates,
        FUtil = Y.FUtil,
        DB = Y.usp.DataBindingUtil;

    Y.namespace('app.home.authorisation').PaginatedFilteredWarningsResultList = Y.Base.create("paginatedFilteredWarningsResultList", Y.usp.person.PaginatedPersonWarningAssignmentList, [Y.usp.ResultsFilterURLMixin], {
      whiteList: ["warningCodes"]
    }, {
      ATTRS: {
        filterModel: {
          writeOnce: 'initOnly'
        }
      }
    });

    Y.namespace('app.home.authorisation').WarningsResults = Y.Base.create('warningsResults', Y.usp.app.Results, [], {
        getResultsListModel: function (config) {
            return new Y.app.home.authorisation.PaginatedFilteredWarningsResultList({
                url: config.url,
                //Required access level is WRITE
                RecordAccessLevel: 'WRITE',
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function (config) {
            var labels = config.labels || {},
                permissions = config.permissions;
            return ([{
                key: 'codedEntryVO.name',
                label: labels.warningCategory,
                width: '18%',
                cellTemplate: USPTemplates.clickable('view', 'view', permissions.canView)
            }, {
                key: 'subjectPerson.name',
                label: labels.subject,
                width: '20%',
                cellTemplate: USPTemplates.clickable('view person', 'viewSubject', permissions.canViewPerson)
            }, {
                key: 'startDate',
                label: labels.startDate,
                width: '20%',
                formatter: USPFormatters.date
            }, {
                key: 'authorisation.requestDate',
                label: labels.submittedDate,
                sortable: true,
                width: '15%',
                formatter: USPFormatters.date
            }, {
                key: 'authorisation.requestedBy',
                label: labels.submitterName,
                width: '20%'
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'view',
                    title: labels.viewWarningTitle,
                    label: labels.viewWarning,
                    enabled: permissions.canView
                }, {
                    clazz: 'reject',
                    title: labels.rejectWarningTitle,
                    label: labels.rejectWarning,
                    visible: function () {
                        return permissions.canAuthorise;
                    }
                }, {
                    clazz: 'authorise',
                    title: labels.approveWarningTitle,
                    label: labels.approveWarning,
                    visible: function () {
                        return permissions.canAuthorise;
                    }
                }]
            }]);
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function () {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    }];
                }
            }
        }
    });

    Y.namespace('app.home.authorisation').BaseWarningView = Y.Base.create('baseWarningView', Y.View, [], {
        render: function () {
            var container = this.get('container'),
                html;

            html = this.template({
                modelData: this.get('model').toJSON(),
                otherData: this.get('otherData'),
                labels: this.get('labels'),
            });

            // Render this view's HTML into the container element.
            container.setHTML(html);
            return this;
        }
    }, {
        ATTRS: {
            model: {},
            otherData: {}
        }
    });

    Y.namespace('app.home.authorisation').ViewWarning = Y.Base.create('viewWarning', Y.app.home.authorisation.BaseWarningView, [], {
        template: Y.Handlebars.templates["homeWarningViewDialog"]
    });

    Y.namespace('app.home.authorisation').ApproveRejectPersonWarning = Y.Base.create('approveRejectPersonWarning', Y.app.home.authorisation.BaseWarningView, [], {
        template: Y.Handlebars.templates["personWarningAuthoriseRejectDialog"]
    });

    Y.namespace('app.home.authorisation').WarningDialog = Y.Base.create('warningDialog', Y.usp.app.AppDialog, [], {
        initializer: function (config) {
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        views: {
            viewWarning: {
                type: Y.app.home.authorisation.ViewWarning
            },
            approveWarning: {
                type: Y.app.home.authorisation.ApproveRejectPersonWarning,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Authorise',
                    action: 'handleSave'
                }]
            },
            rejectWarning: {
                type: Y.app.home.authorisation.ApproveRejectPersonWarning,
                buttons: [{
                    name: 'saveButton',
                    labelHTML: '<i class="fa fa-check"></i> Reject',
                    action: 'handleSave'
                }]
            }
        }
    });

    Y.namespace('app.home.authorisation').WarningsFilterModel = Y.Base.create('warningsFilterModel', Y.usp.ResultsFilterModel, [], {

    }, {
        ATTRS: {
            warningCodes: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            }
        }
    });

    Y.namespace('app.home.authorisation').WarningsFilterView = Y.Base.create('warningsFilterView', Y.usp.app.ResultsFilter, [], {
        template: Y.Handlebars.templates['warningsFilter'],
        render: function () {
            var container = this.get('container');
            //call into superclass to render
            Y.app.home.authorisation.WarningsFilterView.superclass.render.call(this);

            this._getwarningCodes().then(function(warningCodes) {
                this._updateList('select[name="warningCodes"]', warningCodes);
            }.bind(this));

            return this;
        },
        _getwarningCodes: function() {
        	
            return new Y.Promise(function (resolve, reject) {
                var modelList = new Y.usp.configuration.PaginatedCodedEntryList({
                	url: this.get('warningCodesURL')
                }).load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));
            
        },
        _updateList: function(nodeId, values) {
        	
            var container = this.get('container'),
                select = container.one(nodeId),
                codedEntryOptions={};
            
            values.results.forEach(function(codedEntry){
            	//add coded entry option to our "map" using the code as the key
            	codedEntryOptions[codedEntry.codedEntryVO.code]={
            		name: codedEntry.codedEntryVO.name,
            		id: codedEntry.codedEntryVO.code
            	}
            });
            
            //iterate the object keys (codes) and build the final array of options
            var options=Object.keys(codedEntryOptions).map(function(optionKey){
            	return codedEntryOptions[optionKey];
            });
            
            if (select) {
                FUtil.setSelectOptions(select, options, null, null, false, true);

                //now we have our select box populated - we need to ensure that any current selection is made
                DB.setElementValue(select.getDOMNode(), this.get('model').get('warningCodes'));
            }
        }
    });

    Y.namespace('app.home.authorisation').WarningsFilter = Y.Base.create('WarningsFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.home.authorisation.WarningsFilterModel,
        filterType: Y.app.home.authorisation.WarningsFilterView,
        filterContextTemplate: Y.Handlebars.templates["warningsActiveFilter"]
    });

    Y.namespace('app.home.authorisation').WarningsResultsView = Y.Base.create('warningsResultsView', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.home.authorisation.WarningsFilter,
        resultsType: Y.app.home.authorisation.WarningsResults
    });

    Y.namespace('app.home.authorisation').AuthorisationWarningsContainerView = Y.Base.create('authorisationWarningsContainerView', Y.View, [], {
        initializer: function (config) {
            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
              }).addTarget(this);

            var resultsConfig = {
                searchConfig: config.searchConfig,
                filterConfig: config.filterConfig,
                filterContextConfig: config.filterContextConfig,
                permissions: config.permissions
            };

            this.results = new Y.app.home.authorisation.WarningsResultsView(resultsConfig).addTarget(this);
            this.warningDialog = new Y.app.home.authorisation.WarningDialog(config.warningDialogConfig).addTarget(this).render();

            this._evtHandlers = [
                this.on('*:saved', this.results.reload, this.results),
                this.on('warningsResults:view', this.handleViewWarning, this),
                this.on('warningsResults:viewSubject', this.handleViewSubject, this),
                this.on('warningsResults:authorise', this.showAuthoriseWarningDialog, this),
                this.on('warningsResults:reject', this.showRejectWarningDialog, this)
            ];
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.results.removeTarget(this);
            this.results.destroy();
            delete this.results;

            this.warningDialog.removeTarget(this);
            this.warningDialog.destroy();
            delete this.warningDialog;
            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.results.render().get('container'));

            this.get('container').setHTML(contentNode);

            return this;
        },
        _showWarningDialog: function (record, view, model) {
            var subject = this._getSubjectFromRecord(record);

            //show the submit dialog
            this.warningDialog.showView(view, {
                model: model,
                otherData: {
                    subject: subject || {},
                    name: record.get('subjectPerson.name')
                }
            }, function () {
                var saveButton = this.getButton('saveButton', Y.WidgetStdMod.FOOTER);
                if (saveButton) {
                    //Get the button by name out of the footer and enable it.
                    saveButton.set('disabled', false);
                }
            });
        },
        _getSubjectFromRecord: function (record) {
            return {
                subjectType: 'person',
                subjectName: record.get('subjectPerson.name'),
                subjectIdentifier: record.get('subjectPerson.personIdentifier'),
                subjectId: record.get('subjectPerson.id')
            };
        },
        handleViewWarning: function (e) {
            var record = e.record,
                permissions = this.get('permissions');

            if (permissions.canView) {
                /*this.warningDialog.showView('viewWarning', {
                    model: record
                });*/
                this._showWarningDialog(record, 'viewWarning', record);
            }
        },
        handleViewSubject: function (e) {
            var subject = this._getSubjectFromRecord(e.record),
                permissions = this.get('permissions'),
                viewSubjectURL = this.get('viewSubjectURL');

            if (permissions.canViewPerson) {
                window.location = L.sub(viewSubjectURL, subject);
            }
        },
        showAuthoriseWarningDialog: function (e) {
            var record = e.record,
                permissions = this.get('permissions'),
                dialogConfig = this.get('warningDialogConfig'),
                url = dialogConfig.views.approveWarning.config.url,
                model = new Y.usp.Model({
                    url: url,
                    id: record.get('id'),
                    warningCategory: record.get('codedEntryVO').name
                });

            if (permissions.canAuthorise) {
                this._showWarningDialog(record, 'approveWarning', model);
            }
        },
        showRejectWarningDialog: function (e) {
            var record = e.record,
                permissions = this.get('permissions'),
                dialogConfig = this.get('warningDialogConfig'),
                url = dialogConfig.views.rejectWarning.config.url,
                model = new Y.usp.Model({
                    url: url,
                    id: record.get('id'),
                    warningCategory: record.get('codedEntryVO').name
                });

            if (permissions.canAuthorise) {
                this._showWarningDialog(record, 'rejectWarning', model);
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'app-toolbar',
        'usp-person-PersonWarningAssignment',
        'personwarning-dialog-views',
        'results-formatters',
        'results-templates',
        'handlebars-helpers',
        'usp-person-Person',
        'results-table-search-state-plugin',
        'results-table-menu-plugin',
        'results-table-keyboard-nav-plugin',
        'results-table-search-state-plugin',
        'data-binding',
        'usp-configuration-CodedEntry'
    ]
});
