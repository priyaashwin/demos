YUI.add('checklist-home-tab', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        J = Y.JSON,
        HOME_VIEW_TEMPLATE = '<div class="pure-g-r" id="listsRoot"><div id="column1List" class="pure-u-1-3"></div><div id="column2List" class="pure-u-1-3  resp-l-box"></div><div id="column3List" class="pure-u-1-3"></div></div>';
    Y.namespace('app.checklist').ChecklistHomeTab = Y.Base.create('checklistHomeTab', Y.View, [], {
        initializer: function(config) {
            var currentPersonId = config.currentPersonId,
                currentTeamId = config.currentTeamId,
                searchConfig = config.searchConfig || {},
                createFactory = function(type) {
                    return React.createElement.bind(null, type);
                },
                type = config.type;

            this.tabType = type;
            //setup a React factory for the checklistCardView
            this.checklistCardViewFactory = createFactory(uspChecklist.ChecklistCardView);
            var ChecklistHomeFilter = Y.Base.create('checklistHomeFilter', Y.usp.app.Filter, [], {
                filterContextType: Y.app.ChecklistInstanceFilterContext,
                getFilterModel: function(config) {
                    var model;
                    switch (type) {
                        case 'TEAM':
                            model = new Y.app.UserTeamChecklistInstanceResultsFilterModel(config)
                            break;
                        case 'MEMBERS':
                            model = new Y.app.UserTeamMembersChecklistInstanceResultsFilterModel(config);
                            break;
                        default:
                            model = new Y.app.UserChecklistInstanceResultsFilterModel(config);
                    }
                    var searchState = sessionStorage.getItem('tab.' + type + '.' + model.name);
                    searchState = searchState ? JSON.parse(searchState) : {};
                    model.initFromSearchState(searchState);
                    return model;
                },
                getResultsFilter: function(config) {
                    var filter,
                        filterConfig = Y.merge(config, {
                            ownerId: currentPersonId,
                            teamId: currentTeamId
                        });
                    switch (type) {
                        case 'TEAM':
                            filterConfig.labels = Y.merge(filterConfig.labels, {
                                ownerId: filterConfig.labels.owner.team
                            });
                            filter = new Y.app.UserTeamChecklistInstanceResultsFilter(filterConfig);
                            break;
                        case 'MEMBERS':
                            filterConfig.labels = Y.merge(filterConfig.labels, {
                                ownerId: filterConfig.labels.owner.member
                            });
                            filter = new Y.app.UserTeamMembersChecklistInstanceResultsFilter(filterConfig);
                            break;
                        default:
                            filter = new Y.app.UserChecklistInstanceResultsFilter(filterConfig);
                    }
                    return filter;
                }
            });
            this.filter = new ChecklistHomeFilter(config);
            //add this as bubble target for events
            this.filter.addTarget(this);

            //construct a new FilteredModelList which will apply our filter to the base URL
            var modelList = [
                'status',
                'priority',
                'dueDateFrom',
                'dueDateTo',
                'startDateFrom',
                'startDateTo',
                'checklistDefinitionId',
                'memberId',
                'subjectId',
                'includeTeamOwners',
                'includeTeamMembers',
                'includeNoOwningTeam',
                'teamId',
                'selectedOwnerPersonIds',
                'selectedTeamIds'
            ];
            this.dataModel = new uspChecklist.FilteredModelList(searchConfig.url, modelList);
            this._evtHandlers = [
                //subscribe to events where we need to update our card list
                this.on('filter:apply', function(e) {
                    this.reloadLists();
                }, this)
            ];
        },
        render: function() {
            var container = this.get('container'),
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());
            //append the filter container
            contentNode.append(this.filter.render().get('container'));
            //add the base layout for the 3 column view
            var homeViewNode = contentNode.appendChild(HOME_VIEW_TEMPLATE);
            //lookup nodes for the 3 lists
            this.column1List = homeViewNode.one('#column1List');
            this.column2List = homeViewNode.one('#column2List');
            this.column3List = homeViewNode.one('#column3List');
            //render an instance of the HomeView into each list
            this.reloadLists();
            //finally set the content into the container
            container.appendChild(contentNode);
            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }
            //remove event targets
            if (this.filter) {
                this.filter.removeTarget(this);
                this.filter.destroy();
                delete this.filter;
            }

            if (this.column1List) {
                //unmount the component
                ReactDOM.unmountComponentAtNode(this.column1List.getDOMNode());
                //clean up the node completely
                this.column1List.destroy(true);
                delete this.column1List;
            }
            if (this.column2List) {
                //unmount the component
                ReactDOM.unmountComponentAtNode(this.column2List.getDOMNode());
                //clean up the node completely
                this.column2List.destroy(true);
                delete this.column2List;
            }
            if (this.column3List) {
                //unmount the component
                ReactDOM.unmountComponentAtNode(this.column3List.getDOMNode());
                //clean up the node completely
                this.column3List.destroy(true);
                delete this.column3List;
            }
        },
        reloadLists: function() {
            var searchConfig = this.get('searchConfig'),
                filterConfig = this.get('filterConfig'),
                permissions = searchConfig.permissions,
                labels = searchConfig.labels,
                filterLabels = filterConfig.labels || {},
                currentPersonId = this.get('currentPersonId');
            var filter = this.filter,
                filterModel = filter.get('filterModel');
            
            // Store the search params in the session local storage
            sessionStorage.setItem('tab.' + this.tabType + '.' + filterModel.name, JSON.stringify(filterModel.toJSON()));
            //invoke a re-render on the lists
            this._renderColumn1List(currentPersonId, labels, filterLabels, permissions, true);
            this._renderColumn2List(currentPersonId, labels, filterLabels, permissions, true);
            this._renderColumn3List(currentPersonId, labels, filterLabels, permissions, true);
        },
        _getFilterStatusText: function(status, labels, filterLabels) {
            var title, headerLabels = labels.header || {};
            if (status && status.length > 1) {
                title = L.sub(headerLabels.multipleStatus, {
                    statuses: status.filter(function(f) {
                        //initializing is not shown on the title
                        return f !== 'INITIALIZING';
                    }).map(function(f) {
                        var l;
                        switch (f) {
                            case 'OVERDUE':
                                l = filterLabels.overdue;
                                break;
                            case 'IN_ERROR':
                                l = filterLabels.inError;
                                break;

                            case 'LAST_REMINDER':
                                l = filterLabels.lastReminder;
                                break;
                            case 'PAUSED':
                                l = filterLabels.paused;
                                break;
                            case 'ON_TIME':
                                l = filterLabels.onTime;
                                break;
                            default:
                                l = 'unknown';
                        }
                        return l;
                    }).join(' or ')
                });
            } else if (status && status.length === 1) {
                switch (status[0]) {
                    case 'OVERDUE':
                        title = headerLabels.overdue;
                        break;
                    case 'IN_ERROR':
                        title = headerLabels.inError;
                        break;
                    case 'LAST_REMINDER':
                        title = headerLabels.lastReminder;
                        break;
                    case 'PAUSED':
                        title = headerLabels.paused;
                        break;
                    case 'ON_TIME':
                        title = headerLabels.onTime;
                        break;
                    default:
                        title = 'unknown';
                }
            }
            return title;
        },
        _renderColumn1List: function(currentPersonId, labels, filterLabels, permissions, reload) {
            var title = labels.header.overdue;
            //work around until we decide what the filter means
            var filter = this.filter.get('filterModel').toJSON();
            //column 1 can support the following items
            //OVERDUE
            //IN_ERROR
            if (filter.status.length === 0) {
                //no filter selected - include just OVERDUE by default
                filter.status = ['OVERDUE'];
            } else {
                filter.status = filter.status.filter(function(s) {
                    return s === 'OVERDUE' || s === 'IN_ERROR';
                });
            }
            title = this._getFilterStatusText(filter.status, labels, filterLabels);
            this._renderList(currentPersonId, filter, labels, permissions, title, this.column1List, reload);
        },
        _renderColumn2List: function(currentPersonId, labels, filterLabels, permissions, reload) {
            var title = labels.header.lastReminder;
            //work around until we decide what the filter means
            var filter = this.filter.get('filterModel').toJSON();
            //column 2 can support the following items
            //PAUSED
            //LAST_REMINDER
            if (filter.status.length === 0) {
                //no filter selected - include all the above
                filter.status = ['LAST_REMINDER', 'PAUSED'];
            } else {
                filter.status = filter.status.filter(function(s) {
                    return s === 'PAUSED' || s === 'LAST_REMINDER';
                });
            }
            title = this._getFilterStatusText(filter.status, labels, filterLabels);
            this._renderList(currentPersonId, filter, labels, permissions, title, this.column2List, reload);
        },
        _renderColumn3List: function(currentPersonId, labels, filterLabels, permissions, reload) {
            var title = labels.header.onTime;
            //work around until we decide what the filter means
            var filter = this.filter.get('filterModel').toJSON();
            //column 3 can support the following items
            //INITIALIZING
            //ON_TIME
            if (filter.status.length === 0) {
                //no filter selected - include all the above
                filter.status = ['INITIALIZING', 'ON_TIME'];
            } else {
                filter.status = filter.status.filter(function(s) {
                    return (s === 'INITIALIZING' || s === 'ON_TIME');
                });
            }
            title = this._getFilterStatusText(filter.status, labels, filterLabels);
            this._renderList(currentPersonId, filter, labels, permissions, title, this.column3List, reload);
        },
        _renderList: function(currentPersonId, filter, labels, permissions, title, node, reload) {
            var listTitle = title,
                invalid = false,
                requestParams = {
                    ownerId: currentPersonId,
                    teamId: this.get('currentTeamId'),
                    sortBy: J.stringify([{
                        dateDue: 'asc'
                    }])
                },
                dialogConfig = Y.merge(this.get('reactChecklistDialogConfig'), {enums: Y.usp.checklist.enum});
            
            if(reload){
              //include timestamp to force reload and prevent cache of endpoint params
              requestParams.t=Date.now();
            }
            
            if (filter.status.length === 0) {
                invalid = true;
                title = labels.header.noFilterItems;
            }
            var url = this.dataModel.getURL(requestParams, filter);
            ReactDOM.render(this.checklistCardViewFactory({
              dialogProps: dialogConfig,
              viewProps: {
                url: url,
                personSummaryURL: this.get('personSummaryURL'),
                checklistURL: this.get('checklistURL'),
                labels: labels,
                permissions: permissions,
                title: listTitle || '{totalResults}',
                invalid: invalid === true ? true : false
              }
            }), node.getDOMNode(), function() {
                node.getDOMNode().__component = this;
            });
        }
    }, {
        ATTRS: {
            currentPersonId: {},
            currentTeamId: {},
            personSummaryURL: {},
            checklistURL: {}
        }
    });    
    
    
    Y.namespace('app.checklist').MySummaryView = Y.Base.create('mySummaryView', Y.View, [], {
        initializer: function(config) {
            var searchConfig=config.searchConfig,
                labels = searchConfig.labels.checklist,
                currentPersonId = config.currentPersonId,
                currentTeamId = config.currentTeamId;
            //configure the results table
            this.results = new Y.usp.PaginatedResultsTable({
                //declare the columns
                columns: [
                    {
                    	key:'teamName',
                        label: labels.teamTitle,
                        width: '25%'
                    }, {
                    	key:'lateCount',
                        label: labels.state.overdueTitle,
                        width: '12%',
                        formatter: this.zeroToHyphen
                    }, {
                        key: 'reminderCount',
                        label: labels.state.lastReminderTitle,
                        width: '12%',
                        formatter: this.zeroToHyphen
                    }, {
                        key: 'onTimeCount',
                        label: labels.state.onTimeTitle,
                        width: '12%',
                        formatter: this.zeroToHyphen
                    }, {
                        key: 'pausedCount',
                        label: labels.state.pausedTitle,
                        width: '12%',
                        formatter: this.zeroToHyphen
                    }, {
                        key: 'errorCount',
                        label: labels.state.errorTitle,
                        width: '12%',
                        formatter: this.zeroToHyphen
                    }, {
                        key: 'clientCount',
                        label: labels.clientTitle,
                        width: '15%',
                        formatter: this.zeroToHyphen
                    }
                ],
                //configure the data set
                data: new Y.usp.checklist.TeamTaskInstanceSummaryList({
                	url: L.sub(searchConfig.summaryUrl, 
                			{ownerId:currentPersonId, teamId:currentTeamId})
                }),
                noDataMessage: searchConfig.noDataMessage,
                plugins: [
                    //plugin Menu handler
                    {
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    },
                    //plugin Keyboard nav
                    {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }
                ]
            });
            
            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                title:searchConfig.title,
                tableId: this.name
            });
            
            // Add event targets
            this.results.addTarget(this);
        },
        destructor: function() {            
            if (this.results) {
                //destroy the results table
                this.results.removeTarget(this);
                this.results.destroy();
                delete this.results;
            }
            if (this._tablePanel) {
                this._tablePanel.destroy();
                delete this._tablePanel;
            }
        },
        render: function() {
            var container = this.get('container'),
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());
            
            contentNode.append(this._tablePanel.render().get('container'));
            //render results
            this._tablePanel.renderResults(this.results);
            //finally set the content into the container
            container.setHTML(contentNode);
            return this;
        },        
        reload: function(reset) {
            this.results.reload();
        },
        zeroToHyphen: function(o) {
            if (o.value === 0) {
              return '-';
            }
            return o.value;
        }
    }, {
        ATTRS: {
            searchConfig: {
                labels: {},
                noDataMessage: ''
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'app-filter',
               'checklist-instance-results-filter',
               'checklist-instance-filter-context',
               'json-stringify',
               'paginated-results-table',
               'usp-checklist-TeamTaskInstanceSummary',
               'results-table-keyboard-nav-plugin',
                'checklist-priority-enumeration'
               ]
});