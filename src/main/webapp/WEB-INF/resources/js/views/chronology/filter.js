YUI.add('chronology-filter', function(Y) {
    'use-strict';

    var L = Y.Lang,
        FUtil = Y.FUtil,
        DB = Y.usp.DataBindingUtil;

    Y.namespace('app.chronology').ChronologyFilterModel = Y.Base.create('chronologyFilterModel', Y.app.entry.BaseEntryFilterModel, [], {});

    Y.namespace('app.chronology').ChronologyFilterView = Y.Base.create('chronologyFilterView', Y.app.entry.BaseEntryFilterView, [], {        
        getFilterItemsModel: function(id) {
            return new Y.usp.relationshipsrecording.ChronologyFilterItems({
                url: L.sub(this.get('filterItemsUrl'), {
                    id: id
                })
            });
        },
        getRecordModel: function(subjectId, subjectType) {
            return new Y.usp.chronology.Chronology({
                url: L.sub(this.get('chronologyRecordUrl'), {
                    subjectType: subjectType
                }),
                id: subjectId
            });
        }
    }, {
        ATTRS: {
            entryTypes: {
                value: Y.secured.uspCategory.chronology.entryType.category.codedEntries
            },
            entrySubTypes: {
              value: Y.secured.uspCategory.chronology.entrySubType.category.codedEntries
            },
            chronologyRecordUrl: {}
        }
    });


    Y.namespace('app.chronology').ChronologyFilterContext = Y.Base.create('chronologyFilterContext', Y.app.entry.BaseEntryFilterContext, [], {});
    
    Y.namespace('app.chronology').ChronologyFilter = Y.Base.create('chronologyFilter', Y.app.entry.BaseEntryFilter, [], {
        filterModelType: Y.app.chronology.ChronologyFilterModel,
        filterType: Y.app.chronology.ChronologyFilterView,
        filterContextType: Y.app.chronology.ChronologyFilterContext
    });
}, '0.0.1', {
    requires: ['yui-base',
        'entry-filter',
        'secured-categories-chronology-component-EntryType',
        'secured-categories-chronology-component-EntrySubType',
        'usp-relationshipsrecording-ChronologyFilterItems',
        'usp-chronology-Chronology'
    ]
});