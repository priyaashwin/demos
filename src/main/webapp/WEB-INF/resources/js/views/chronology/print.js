YUI.add('chronology-results-print', function(Y) {
    'use-strict';

    var USPFormatters = Y.usp.ColumnFormatters;

    Y.namespace('app.chronology').ChronologyPrintView = Y.Base.create('chronologyPrintView', Y.View, [], {
        initializer: function(config) {
            this.results = new Y.app.chronology.ChronologyResultsPrint({
                searchConfig: Y.merge(config.searchConfig, {
                    subject: config.subject,
                    filterModel: new Y.app.chronology.ChronologyFilterModel()
                }),
                //turn off styling on the table to just basic
                tablePanelConfig: {
                    tableClass: 'pure-table'
                }
            });
        },
        render: function() {
            this.get('container').setHTML(this.results.render().get('container'));

            //get rid of the results count
            this.results.get('results').get('resultsCount').remove();
            //and the paginator
            this.results.get('results').get('paginatorContainer').remove();
            return this;
        },
        destructor: function() {
            this.results.destroy();
            delete this.results;
        }
    });

    Y.namespace('app.chronology').ChronologyResultsPrint = Y.Base.create('chronologyResultsPrint', Y.app.chronology.ChronologyResults, [], {
        initializer: function(config) {
            this.initSortBy();
            this.initViewMode();
            this.initFilterModel();

            this.get('results').before('*:render', function(e) {
                // Before we render we need to check column visibility for either event date, recorded date or last edited date.
                // We do this by getting the sortBy, first item and using YUI's Object each to determine the key.
                Y.Object.each(this.get('results').get('sortBy')[0], function(val, key) {
                    this.get('results').modifyColumn(key, {
                        className: ''
                    });
                }.bind(this));
            }, this);

        },
        _getValueFromStorage: function(key) {
            var value = localStorage.getItem(key);

            if (value) {
                sessionStorage.setItem(key, value);
                delete localStorage[key];
            }

            if (!value) {
                value = sessionStorage.getItem(key);
            }

            return value;
        },
        initSortBy: function() {
            var orderBy = this._getValueFromStorage('orderBy');

            if (orderBy) {
                try {
                    orderBy = Y.JSON.parse(orderBy);
                } catch (ignore) {}
            }

            if (!orderBy) {
                orderBy = [{
                    "eventDate!calculatedDate": "desc"
                }];
            }

            this.get('results').set('orderBy', orderBy);
        },
        initViewMode: function() {
            var viewMode = this._getValueFromStorage('viewMode');

            this.get('results').viewModePlugin.setMode(viewMode);
        },
        initFilterModel: function() {
            var filterData = this._getValueFromStorage('filterData');

            if (filterData) {
                try {
                    filterData = Y.JSON.parse(filterData);
                } catch (ignore) {}
            }

            this.get('resultsListModel').get('filterModel').setAttrs(filterData);
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                impactEnabled = config.impactEnabled;

            var columns=([{
                key: "createdStamp!auditDate",
                label: labels.recordedDate,
                startHidden: true,
                width: '10%',
                readingView: '12%',
                formatter: USPFormatters.dateTime,
                className: "yui3-hide"
            }, {
                key: "lastEditedStamp!auditDate",
                label: labels.editedDate,
                width: '10%',
                readingView: '12%',
                formatter: USPFormatters.dateTime,
                className: "yui3-hide"
            }, {
                key: "eventDate!calculatedDate",
                label: labels.calculatedDate,
                width: '10%',
                readingView: '12%',
                maskField: 'eventDate!fuzzyDateMask',
                formatter: USPFormatters.fuzzyDate
            }, {
                key: "event",
                label: labels.event,
                className: 'truncatedRichText',
                formatter: USPFormatters.textFormat,
                allowHTML: true,
                width: '20%',
                readingView: '30%'
            }, {
                key: "action",
                label: labels.action,
                className: 'truncatedRichText',
                formatter: USPFormatters.textFormat,
                allowHTML: true,
                width: '25%',
                readingView: '30%'
            }, {
                key: "entryType",
                label: labels.entryType,
                width: impactEnabled?'8%':'11%',
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.secured.uspCategory.chronology.entryType.category.codedEntries,
                //coded entry formatter escapes content
                allowHTML: true
            }, {
                key: "source",
                label: labels.source,
                width: '10%',
                formatter: Y.app.source.SourceWithSourceOrganisationFormatter
            }, {
                key: "practitioner",
                label: labels.practitioner,
                width: impactEnabled?'10%':'12%',
                formatter: this.appendPractitionerOrganisationFormatter
            }, {
                key: "status",
                label: labels.status,
                width: '5%',
                allowHTML: true,
                formatter: this.statusIndicator
            }]);
            
            if(impactEnabled){
              //insert the impact column if impact is enabled
              columns.splice(5,0,{
                key: "impact",
                label: labels.impact,
                width: '5%',
                allowHTML: true,
                formatter: this.impactFormatter,
                type:'Chronology event'
              });
            }
            return columns;
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.app.ViewModePlugin
                    }];
                }
            }
        },
    });
}, '0.0.1', {
    requires: ['yui-base',
        'chronology-results',
        'json-parse',
        'results-formatters',
        'secured-categories-chronology-component-EntryType',
        'app-view-mode-plugin',
        'source-form'
    ]
});