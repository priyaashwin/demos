YUI.add('chronology-results', function(Y) {
    'use-strict';

    var L = Y.Lang,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates,
        APPFormatters = Y.app.ColumnFormatters;
    
    Y.Do.after(function() {
            var schema = Y.Do.originalRetVal;
            schema.resultFields.push({
                key: 'eventDate!calculatedDate',
                locator: 'eventDate.calculatedDate'
            }, {
                key: 'createdStamp!auditDate',
                locator: 'createdStamp.auditDate'
            }, {
                key: 'lastEditedStamp!auditDate',
                locator: 'lastEditedStamp.auditDate'
            }, {
                key: 'eventDate!fuzzyDateMask',
                locator: 'eventDate.fuzzyDateMask'
            }, {
                key: 'subjectIdName!id',
                locator: 'subjectIdName.id'
            }, {
                key: 'subjectIdName!name',
                locator: 'subjectIdName.name'
            }, {
                key: 'subjectIdName!identifier',
                locator: 'subjectIdName.identifier'
            }, {
                key: 'subjectIdName!type',
                locator: 'subjectIdName.type'
            });

            return new Y.Do.AlterReturn(null, schema);
        },
        Y.usp.chronology.ChronologyEntryWithVisibility, 'getSchema',
        Y.usp.chronology.ChronologyEntryWithVisibility);

    Y.namespace('app.chronology').PaginatedFilteredChronologyEntryWithVisibilityList = Y.Base.create("paginatedFilteredChronologyEntryWithVisibilityList",
        Y.usp.chronology.PaginatedChronologyEntryWithVisibilityList, [Y.usp.ResultsFilterURLMixin], {
        whitelist: ['startDate', 'endDate', 'startRecordedDate', 'endRecordedDate',
                    'startEditedDate', 'endEditedDate', 'impactList', 'sourceList',
                    'otherSource', 'sourceOrgList', 'otherSourceOrg', 'pracList', 'pracOrgList', 'groupIds', 
                    'entryTypeList', 'entrySubTypeList', 'statusList', 'excludeGroupEntries'
            
            ],
            staticData: {
                useSoundex: false,
                appendWildcard: true
            }
        }, {
            ATTRS: {
                filterModel: {
                    writeOnce: 'initOnly'
                }
            }
        });

    Y.namespace('app.chronology').ChronologyResults = Y.Base.create('chronologyResults', Y.app.entry.BaseEntryResults, [], {
        getResultsListModel: function(config) {
            var subject = config.subject;

            return new Y.app.chronology.PaginatedFilteredChronologyEntryWithVisibilityList({
                url: L.sub(config.url, {
                    id: subject.subjectId,
                    subjectType: subject.subjectType
                }),
                requestedAccess: 'READ_SUMMARY',
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions,
                subject = config.subject || {},
                subjectType = subject.subjectType,
                impactEnabled=config.impactEnabled;

            var columns=([{
                key: "createdStamp!auditDate",
                label: labels.recordedDate,
                sortable:true,
                startHidden: true,
                width: '10%',
                readingView: '12%',
                formatter: USPFormatters.dateTime,
                className: "yui3-hide",
                cellTemplate: USPTemplates.clickable(labels.viewTitle, 'view', permissions.canView)
            }, {
                key: "lastEditedStamp!auditDate",
                label: labels.editedDate,
                sortable:true,
                startHidden: true,
                width: '10%',
                readingView: '12%',
                formatter: USPFormatters.dateTime,
                className: "yui3-hide",
                cellTemplate: USPTemplates.clickable(labels.viewTitle, 'view', permissions.canView)
            }, {
                key: "eventDate!calculatedDate",
                label: labels.calculatedDate,
                sortable:true,
                width: '10%',
                readingView: '12%',
                maskField: 'eventDate!fuzzyDateMask',
                formatter: APPFormatters.fuzzyDateLinkForReadSummaryFormatter,
                title: labels.viewTitle,
                url: config.urlForView,
                permissions: permissions
            }, {
                key: "event",
                label: labels.event,
                className: 'truncatedRichText',
                width: '25%',
                readingView: '35%',
                allowHTML: true,
                expandedFormatter: this.textWithTypeFullFormatter,
                collapsedFormatter: this.textWithTypeTruncateFormatter,
                subjectType: subjectType,
                type:'Chronology event'
            }, {
              key: "action",
              label: labels.action,
              className: 'truncatedRichText',
              width: '25%',
              readingView: '35%',
              allowHTML: true,
              expandedFormatter: this.textFullFormatter,
              collapsedFormatter: this.textTruncateFormatter,
              subjectType: subjectType,
              type:'Chronology event'
          }, {
                key: "entryType",
                label: labels.entryType,
                width: impactEnabled?'8%':'18%',
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.secured.uspCategory.chronology.entryType.category.codedEntries,
                //coded entry formatter escapes content
                allowHTML: true
            }, {
                key: "source",
                label: labels.source,
                width: '10%',
                formatter: Y.app.source.SourceWithSourceOrganisationFormatter
            }, {
                key: "practitioner",
                label: labels.practitioner,
                width: impactEnabled?'10%':'13%',
                formatter: this.appendPractitionerOrganisationFormatter
            }, {
                key: "status",
                label: labels.status,
                width: '5%',
                allowHTML: true,
                formatter: this.statusFormatter,
                type:'Event'
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '7%',
                readingView: '10%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'view',
                    label: labels.view,
                    title: labels.viewTitle,
                    visible: function() {
                        return permissions.canView && this.record.hasAccessLevel('READ_DETAIL')===true;
                    }
                }, {
                    clazz: 'complete',
                    label: labels.complete,
                    title: labels.completeTitle,
                    visible: function() {
                        //Status must be DRAFT
                        return permissions.canComplete && this.data.status == "DRAFT" && this.record.hasAccessLevel('WRITE')===true;
                    }
                }, {
                    clazz: 'uncomplete',
                    label: labels.uncomplete,
                    title: labels.uncompleteTitle,
                    visible: function() {
                        //Status must be COMPLETE
                        return permissions.canUncomplete && this.data.status == "COMPLETE" && this.record.hasAccessLevel('WRITE')===true;
                    }
                }, {
                    clazz: 'softDelete',
                    label: labels.deleteEntry,
                    title: labels.deleteEntryTitle,
                    visible: function() {
                        //Status must NOT be DELETED
                        return permissions.canDelete && this.data.status !== 'DELETED' && this.record.hasAccessLevel('WRITE')===true;
                    }
                }, {
                    clazz: 'hardDelete',
                    label: labels.remove,
                    title: labels.removeTitle,
                    visible: function() {
                        return permissions.canRemove && this.record.hasAccessLevel('WRITE')===true;
                    }
                }]
            }]);
            
            if(impactEnabled){
              //insert the impact column if impact is enabled
              columns.splice(6,0,{
                key: "impact",
                label: labels.impact,
                width: '5%',
                allowHTML: true,
                formatter: this.impactFormatter,
                type:'Chronology event'
              });
            }
            return columns;
        }
    });

    Y.namespace('app.chronology').ChronologyFilteredResults = Y.Base.create('chronologyFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.chronology.ChronologyFilter,
        resultsType: Y.app.chronology.ChronologyResults,
        updateResults: function() {
            //force a refresh of the filter options
            this.get('filter').updateFilterListOptions();
            //reload list
            this.reload();
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-filtered-results',
        'results-filter',
        'results-formatters',
        'results-templates',
        'usp-chronology-ChronologyEntryWithVisibility',
        'secured-categories-chronology-component-EntryType',
        'secured-categories-chronology-component-EntrySubType',
        'chronology-filter',
        'entry-results',
        'source-form',
        'caserecording-results-formatters'
    ]
});