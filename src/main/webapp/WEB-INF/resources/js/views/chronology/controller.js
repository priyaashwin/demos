YUI.add('chronology-controller-view', function (Y) {
    'use-strict';

    var L = Y.Lang,
        O = Y.Object,
        E = Y.Escape;

    Y.namespace('app.chronology').ChronologyControllerView = Y.Base.create('chronologyControllerView', Y.app.entry.BaseEntryControllerView, [], {
        resultsListClass: Y.app.chronology.ChronologyFilteredResults,
        dialogClass: Y.app.chronology.ChronologyDialog,
        linkFromType: 'CHRONOLOGY',
        dialogAddModelClass: Y.app.chronology.NewChronologyEntryForm,
        dialogViewModelClass: Y.app.chronology.UpdateChronologyEntryForm,
        dialogUpdateModelClass: Y.app.chronology.UpdateChronologyEntryForm,
        buildChecklistMessageParameters: function (parameters) {
            return {
                entryState: E.html((parameters['CHRONOLOGY_ENTRY_STATE'] || {}).displayValue),
                entryType: E.html((parameters['CHRONOLOGY_ENTRY_TYPE'] || {}).displayValue),
            };
        }
    }, {
        ATTRS: {
            impactEnabled: {
                value: false
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'entry-controller-view',
        'chronology-results',
        'chronology-dialog',
        'chronology-output-document-dialog-views',
        'escape'
    ]
});