YUI.add('childlookedafter-controller-view', function (Y) {
    /* global uspChildlookedafter, Promise */
    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.childlookedafter').ChildLookedAfterControllerView = Y.Base.create('childLookedAfterControllerView', Y.View, [], {

        events: {
            '.component-container > div': {
                'addEpisodeOfCare': 'showEpisodeOfCareAddDialog',
                'viewEpisodeOfCare': 'showEpisodeOfCareViewDialog',
                'editEpisodeOfCare': 'showEpisodeOfCareEditDialog',
                'deleteEpisodeOfCare': 'determineEpisodeOfCareDeletionDialog',
                'viewMigClaEpisodeOfCare': 'showMigClaEpisodeOfCareViewDialog',
                'deleteAbsenceFromPlacement': 'showAbsenceFromPlacementArchiveDialog',
                'noteAbsence': 'showAbsenceFromPlacementAddDialog',
                'editAbsenceFromPlacement': 'showAbsenceFromPlacementEditDialog',
                'archiveAbsenceFromPlacement': 'showAbsenceFromPlacementArchiveDialog',
                'viewAbsenceFromPlacement': 'showAbsenceFromPlacementViewDialog',
                'endPeriodOfCare': 'showPeriodOfCareEndDialog',
                'deletePeriodOfCare': 'determinePeriodOfCareDeletionDialog',
                'addTemporaryPlacement': 'showTemporaryPlacementAddDialog',
                'editTemporaryPlacement': 'showTemporaryPlacementEditDialog',
                'viewTemporaryPlacement': 'showTemporaryPlacementViewDialog',
                'deleteTemporaryPlacement': 'showTemporaryPlacementDeleteDialog',
                'addAdoptionInformation': 'showAdoptionInformationAddDialog',
                'viewAdoptionInformation': 'showAdoptionInformationViewDialog',
                'editAdoptionInformation': 'showAdoptionInformationEditDialog',
                'periodsOfCareLoaded': 'handlePeriodsOfCareLoaded',
            },
            '#location_toggleAddLocation': {
                click: 'showLocationAddDialog'
            }
        },
        showLocationAddDialog: function (e) {
            var eventRoot = e.type;

            // Hide dialog where location dialog was launched from
            if (eventRoot.includes('periodOfCare')) {
                this.periodOfCareDialog.hide();
            } else if (eventRoot.includes('episodeOfCare')) {
                this.episodeOfCareDialog.hide();
            }

            var config = this.get('dialogConfig.location.views.add.config');
            this.locationDialog.showView('add', {
                model: new Y.app.location.NewLocationForm({
                    url: config.url
                }),
                otherData: {
                    eventRoot: eventRoot
                }
            }, function () {
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                this.removeButton('closeButton', Y.WidgetStdMod.HEADER);
            });
        },
        handleAfterLocationAdd: function (e) {
            var eventRoot = e.eventRoot,
                locationId = e.locationId;

            // Show dialog where location dialog was launched
            if (eventRoot.includes('periodOfCare')) {
                this.periodOfCareDialog.show();
                if (locationId) {
                    this.periodOfCareDialog.get('activeView').handleSetNewLocation(locationId);
                }
            } else if (eventRoot.includes('episodeOfCare')) {
                this.episodeOfCareDialog.show();
                if (locationId) {
                    this.episodeOfCareDialog.get('activeView').handleSetNewLocation(locationId);
                }
            }

        },
        initializer: function (config) {
            var labels = config.labels.periodsOfCareView;
            var periodOfCareConfig = config.dialogConfig.periodOfCare;
            var episodeOfCareConfig = config.dialogConfig.episodeOfCare;
                 
            if (config.showScottishView) {
                var scottishLabels = config.scottishLabels.periodsOfCareView;
                var scottishDialogConfig = config.scottishDialogConfig;
                var episodeOfCareTable = Y.merge(labels.episodeOfCareTable, {
                    header: scottishLabels.episodeOfCareTable.header,
                    edit: scottishLabels.episodeOfCareTable.edit,
                    view: scottishLabels.episodeOfCareTable.view,
                    deleteEpisodeOfCare: scottishLabels.episodeOfCareTable.deleteEpisodeOfCare
                });
                labels = Y.merge(labels, {
                    title: scottishLabels.title,
                    accordionTitle: scottishLabels.accordionTitle,
                    noResultsMain: scottishLabels.noResultsMain,
                    addEpisodeOfCare: scottishLabels.addEpisodeOfCare,
                    endPeriodOfCare: scottishLabels.endPeriodOfCare,
                    deletePeriodOfCare: scottishLabels.deletePeriodOfCare,
                    episodeOfCareTable: episodeOfCareTable
                });
                periodOfCareConfig = scottishDialogConfig.periodOfCare;
                episodeOfCareConfig = scottishDialogConfig.episodeOfCare;
            }

            this.periodOfCareDialog = new Y.app.childlookedafter.PeriodOfCareDialog(periodOfCareConfig).addTarget(this).render();
            this.episodeOfCareDialog = new Y.app.childlookedafter.EpisodeOfCareDialog(episodeOfCareConfig).addTarget(this).render();
            this.absenceFromPlacementDialog = new Y.app.childlookedafter.AbsenceFromPlacementDialog(config.dialogConfig.absenceFromPlacement).addTarget(this).render();
            this.temporaryPlacementDialog = new Y.app.childlookedafter.TemporaryPlacementDialog(config.dialogConfig.temporaryPlacement).addTarget(this).render();
            this.adoptionInformationDialog = new Y.app.childlookedafter.AdoptionInformationDialog(config.dialogConfig.adoptionInformation).addTarget(this).render();
            this.locationDialog = new Y.app.location.LocationDialog(config.dialogConfig.location).addTarget(this).render();

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            this.toolbar.disableButton('addPeriodOfCare');

            //load personDetailsPromise as a promise to use as default values
            var personDetailsPromise = new Y.Promise(function (resolve, reject) {

                this.get('personDetails').load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            personDetailsPromise.then(function (data) {
                //Check permission and style  button accordingly
                this._displayAddButtonCheck(data);

            }.bind(this));

            this.childLookedAfterView = new Y.app.childlookedafter.ChildLookedAfterView({
                componentType: uspChildlookedafter.ChildLookedAfterView,
                componentConfig: {
                    labels: labels,
                    subject: config.subject,
                    url: config.url,
                    pictureUrl: config.pictureUrl,
                    permissions: config.permissions,
                    migClaPeriodUrl: config.migClaPeriodUrl,
                    showScottishView: config.showScottishView,
                    showWelshView: config.showWelshView,
                    codedEntries: {
                        adopterLegalStatus: Y.uspCategory.childlookedafter.adopterLegalStatus.category
                    },
                    enums: {
                        placementCategory: Y.usp.childlookedafter.enum.PlacementCategory.values
                    }
                }
            }).addTarget(this);

            //output only valid for PERSON subject
            if (this.get('subject').subjectType === 'person') {
                //configure the dialog for output template - no need to add as a bubble target
                this.outputDocumentDialog = new Y.app.output.OutputDocumentDialog(config.outputDialogConfig, {
                    //mix the subject into the search config so it is available when getting the URL
                    subject: config.subject
                }).render();
            }

            this._evtHandlers = [
                this.on('*:addPeriodOfCare', this.showPeriodOfCareAddDialog, this),
                this.on('*:showAdoptionInformationEditDialog', this.showAdoptionInformationEditDialog, this),
                this.on('*:output', this.showOutputDialog, this),
                this.on('*:saved', this.handleSave, this),
                this.after('latestPeriodOfCareChange', this.handleLatestPeriodOfCareChange, this),
                this.after('*:open:locationView', this.showLocationAddDialog, this),
                this.after('*:close:locationView', this.handleAfterLocationAdd, this),
                this.on('periodOfCare:deletePeriod', this.showPeriodOfCareDelete, this),
                this.on('temporaryPlacement:deleteTemporaryPlacement', this.showTemporaryPlacementDelete, this),
                this.on('episodeOfCare:deleteEpisode', this.showEpisodeOfCareDelete, this),
                this.on('periodOfCare:deletePeriod', this.showPeriodOfCareDelete, this)
            ];
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(this.childLookedAfterView.render().get('container'));
            this.get('container').append(contentNode);
            return this;
        },

        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.childLookedAfterView.removeTarget(this);
            this.childLookedAfterView.destroy();
            delete this.childLookedAfterView;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;

            this.periodOfCareDialog.removeTarget(this);
            this.periodOfCareDialog.destroy();
            delete this.periodOfCareDialog;

            this.episodeOfCareDialog.removeTarget(this);
            this.episodeOfCareDialog.destroy();
            delete this.episodeOfCareDialog;

            this.absenceFromPlacementDialog.removeTarget(this);
            this.absenceFromPlacementDialog.destroy();
            delete this.absenceFromPlacementDialog;

            this.temporaryPlacementDialog.removeTarget(this);
            this.temporaryPlacementDialog.destroy();
            delete this.temporaryPlacementDialog;

            this.adoptionInformationDialog.removeTarget(this);
            this.adoptionInformationDialog.destroy();
            delete this.adoptionInformationDialog;

            this.locationDialog.removeTarget(this);
            this.locationDialog.destroy();
            delete this.locationDialog;

            if (this.outputDocumentDialog) {
                this.outputDocumentDialog.destroy();
                delete this.outputDocumentDialog;
            }
        },
        showOutputDialog: function () {
            if (this.get('subject').subjectType === 'person') {
                this.outputDocumentDialog.showView('produceDocument', {
                    //pass the dialog so we can reposition it after the render
                    dialog: this.outputDocumentDialog,
                    model: new Y.Model(this.get('subject'))
                });
            }
        },

        showPeriodOfCareAddDialog: function () {
            var showScottishView = this.get('showScottishView');
            var showWelshView = this.get('showWelshView');
            var dialogConfig,
                modelConstructor,
                view;
            
            if (showScottishView) {
              dialogConfig = this.get('scottishDialogConfig.periodOfCare.views.addScottish');
              view = 'addScottish';
              modelConstructor = Y.app.childlookedafter.NewScottishPeriodOfCareForm;
            } else if (showWelshView) {
              dialogConfig = this.get('dialogConfig.periodOfCare.views.add');
              view = 'addWelsh';
              modelConstructor = Y.app.childlookedafter.NewWelshPeriodOfCareForm;
            } else {
              dialogConfig = this.get('dialogConfig.periodOfCare.views.add');	
              view = 'add';
              modelConstructor = Y.app.childlookedafter.NewPeriodOfCareForm;
            }
            
            var config = dialogConfig.config,
                addConfig = {
                    model: new modelConstructor({
                        url: config.url,
                        labels: dialogConfig.labels                        
                    }),
                    otherData: {
                        scottishView: showScottishView,
                        showWelshView: showWelshView,
                        subject: this.get('subject')
                    }
                };
            this.periodOfCareDialog.showView(view, addConfig, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        
        showEpisodeOfCareAddDialog: function (e) {
            var periodOfCareId = e._event.detail.periodOfCareId,
                indEpTypeLegalStatus = e._event.detail.indEpTypeLegalStatus,
                config = this.get('dialogConfig.episodeOfCare.views.add.config'),
                admittanceConfig = this.get('dialogConfig.episodeOfCare.views.addAdmittance.config');

            var latestEpisodePromise = new Y.Promise(function (resolve, reject) {
                new Y.usp.childlookedafter.EpisodeOfCareWithPlacements({
                    url: L.sub(this.get('dialogConfig.periodOfCare.latestEpisodeOfCareURL'), {
                        periodOfCareId: e._event.detail.periodOfCareId
                    })
                }).load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            Promise.all([latestEpisodePromise]).then(function (value) {
                this.episodeOfCareDialog.showView('add', {
                    model: new Y.app.childlookedafter.NewEpisodeOfCareForm({ url: config.url }),
                    otherData: {
                        subject: this.get('subject'),
                        periodOfCareId: periodOfCareId,
                        indEpTypeLegalStatus: indEpTypeLegalStatus,
                        showScottishView: this.get('showScottishView'),
                        showWelshView: this.get('showWelshView')
                    },
                    supportMultipleLegalStatusEntries: this.get('supportMultipleLegalStatusEntries')
                },
                    function () {
                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
            }.bind(this), function (error) {
                if (error.code === 404 && error.msg === '') {
                    this.episodeOfCareDialog.showView('addAdmittance', {
                        model: new Y.app.childlookedafter.NewAdmittanceEpisodeOfCareForm({ url: admittanceConfig.url }),
                        otherData: {
                            subject: this.get('subject'),
                            periodOfCareId: e._event.detail.periodOfCareId,
                            indEpTypeLegalStatus: e._event.detail.indEpTypeLegalStatus,
                            showScottishView: this.get('showScottishView')
                        },
                        supportMultipleLegalStatusEntries: this.get('supportMultipleLegalStatusEntries')
                    },
                        function () {
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                }
            }.bind(this));
        },

        showEpisodeOfCareViewDialog: function (e) {
            var config = this.get('dialogConfig.episodeOfCare.views.view.config');
            var displayProperties = this.get('dialogConfig.episodeOfCare.views.view.displayProperties');

            this.episodeOfCareDialog.showView('view', {
                model: new Y.app.childlookedafter.EpisodeOfCareForm({
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.episodeOfCareId
                    })
                }),
                otherData: {
                    subject: this.get('subject'),
                    episodeOfCareId: e._event.detail.episodeOfCareId,
                    displayProperties: displayProperties
                }
            }, {
                modelLoad: true
            });
        },

        //EpisodeOfCare Deletion Script

        getEpisodeDeletionRequest: function (url, episodeOfCareId) {
            return new Y.Promise(function (resolve, reject) {
                var model = new Y.usp.deletion.DeletionRequestList({
                    url: Y.Lang.sub(url, {
                        episodeOfCareId: episodeOfCareId
                    })
                });

                model.load(function (err, res) {
                    (err) ? reject(err) : resolve(JSON.parse(res.response));
                }.bind(this));
            });
        },

        determineEpisodeOfCareDeletionDialog: function (e) {
            var episodeOfCareId = e._event.detail.episodeOfCareId,
                urlConfig = this.get('dialogConfig.episodeOfCare.views.deleteEpisode.config');

            if (e._event.detail.isAdmissionRecord) {
                this.showEpisodeOfCareDeleteLastEpsiodeWarning();
            } else {
                this.getEpisodeDeletionRequest(urlConfig.url, episodeOfCareId).then(function (data) {
                    //get deletion requests that are not in an end state
                    var deletionRequest = data.find(function (r) {
                        return !(r.deletionState === 'COMPLETE' || r.deletionState === 'FAILED');
                    });

                    if (deletionRequest) {
                        if (deletionRequest.deletionState === 'READY_TO_DELETE') {
                            // episode of care has been prepared for deletion so carry out deletion
                            this.showEpisodeOfCareDeletionDialog(deletionRequest.id, episodeOfCareId);
                        } else {
                            //a deletion is in progress, or the preparation is still in progress
                            this.showEpisodeOfCareDeletionInProgressMessage()
                        }
                    } else {
                        // episode of care hasn't been prepared for deletion so prepare for deletion
                        this.showEpisodeOfCarePrepareDeletionDialog(episodeOfCareId);
                    }
                }.bind(this));
            }
        },

        showEpisodeOfCareDeletionInProgressMessage: function () {
            this.episodeOfCareDialog.showView('deleteInProgress', {});
        },

        showEpisodeOfCareDeleteLastEpsiodeWarning: function () {
            this.episodeOfCareDialog.showView('deleteLastEpisodeWarning', {}, function () {
                this.getButton('okButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //remove the cancel button
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },

        showEpisodeOfCarePrepareDeletionDialog: function (episodeOfCareId) {
            var urlConfig = this.get('dialogConfig.episodeOfCare.views.deleteEpisode.config');

            this.episodeOfCareDialog.showView('prepareDeleteEpisode', {
                model: new Y.usp.deletion.NewDeletionRequest({
                    url: Y.Lang.sub(urlConfig.prepareUrl, {
                        episodeOfCareId: episodeOfCareId
                    })
                }),

                otherData: {
                    subject: this.get('subject'),
                    episodeOfCareId: episodeOfCareId,
                    labels: this.get('labels').prepareDeleteEpisodeOfCare,
                    urlConfig: urlConfig
                }
            }, function () {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //remove the cancel button
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },

        showEpisodeOfCareDelete: function (e) {
            this.showEpisodeOfCareDeletionDialog(e.deleteRequestId, e.episodeOfCareId);
        },

        showEpisodeOfCareDeletionDialog: function (deletionRequestId, episodeOfCareId) {
            var urlConfig = this.get('dialogConfig.episodeOfCare.views.deleteEpisode.config');

            this.episodeOfCareDialog.showView('deleteEpisode', {
                model: new Y.usp.deletion.DeletionRequest({
                    url: Y.Lang.sub(urlConfig.deleteUrl, {
                        episodeOfCareId: episodeOfCareId,
                        deletionRequestId: deletionRequestId
                    }),
                    id: deletionRequestId
                }),

                otherData: {
                    subject: this.get('subject'),
                    episodeOfCareId: episodeOfCareId,
                    deletionRequestId: deletionRequestId,
                    labels: this.get('labels').deleteEpisodeOfCare,
                    urlConfig: urlConfig
                }
            }, function () {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //remove the cancel button
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },


        showEpisodeOfCareEditDialog: function (e) {
            var config = this.get('dialogConfig.episodeOfCare.views.edit.config'),
                displayProperties = this.get('dialogConfig.episodeOfCare.views.view.displayProperties');

            this.episodeOfCareDialog.showView('edit', {
                model: new Y.app.childlookedafter.UpdateEpisodeOfCareForm({
                    labels: this.get('dialogConfig.episodeOfCare.views.edit.labels'),
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.episodeOfCareId
                    })
                }),
                otherData: {
                    subject: this.get('subject'),
                    episodeOfCareId: e._event.detail.episodeOfCareId,
                    displayProperties: displayProperties
                }
            }, {
                modelLoad: true
            },
                function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });

        },
        showMigClaEpisodeOfCareViewDialog: function (e) {
            var view = 'viewFirstMigClaEpisode';
            if (e._event.detail.episodeOfCare.episodeIndex) {
                view = 'viewOtherMigClaEpisode';
            }
            this.episodeOfCareDialog.showView(view, {
                model: new Y.usp.Model(e._event.detail.episodeOfCare),
                otherData: {
                    subject: this.get('subject')
                }
            });
        },

        showAbsenceFromPlacementAddDialog: function (e) {
            var config = this.get('dialogConfig.absenceFromPlacement.views.add.config');
            this.absenceFromPlacementDialog.showView('add', {
                model: new Y.app.childlookedafter.NewAbsenceFromPlacementForm({
                    url: config.url,
                    labels: this.get('dialogConfig.absenceFromPlacement.views.add.labels')
                }),
                otherData: {
                    subject: this.get('subject'),
                    periodOfCareId: e._event.detail.periodOfCareId
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        showAbsenceFromPlacementEditDialog: function (e) {
            var config = this.get('dialogConfig.absenceFromPlacement.views.edit.config');
            this.absenceFromPlacementDialog.showView('edit', {
                model: new Y.app.childlookedafter.UpdateAbsenceFromPlacementForm({
                    labels: this.get('dialogConfig.absenceFromPlacement.views.edit.labels'),
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.absenceFromPlacementId
                    })
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        showAbsenceFromPlacementViewDialog: function (e) {
            var config = this.get('dialogConfig.absenceFromPlacement.views.view.config');
            this.absenceFromPlacementDialog.showView('view', {
                model: new Y.usp.childlookedafter.AbsenceFromPlacement({
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.absenceFromPlacementId
                    })
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, {
                modelLoad: true
            });
        },

        showAbsenceFromPlacementArchiveDialog: function (e) {
            var config = this.get('dialogConfig.absenceFromPlacement.views.archive.config');
            this.absenceFromPlacementDialog.showView('archive', {
                model: new Y.usp.childlookedafter.AbsenceFromPlacement({
                    id: e._event.detail.absenceFromPlacementId,
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.absenceFromPlacementId
                    })
                }),
                otherData: {
                    subject: this.get('subject'),
                    periodOfCareId: e._event.detail.periodOfCareId
                }
            }, {}, function () {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        showPeriodOfCareEndDialog: function (e) {
            var config = this.get('dialogConfig.periodOfCare.views.end.config');
            var labels = this.get('showScottishView') ? this.get('scottishLabels') : this.get('labels');
            this.periodOfCareDialog.showView('end', {
                model: new Y.app.childlookedafter.EndPeriodOfCareForm({
                    url: config.url
                }),
                otherData: {
                    subject: this.get('subject'),
                    periodOfCareId: e._event.detail.periodOfCareId
                }
            }, function () {
                this.getButton('endButton', Y.WidgetStdMod.FOOTER).set('disabled', false).set('labelHTML', '<i class="fa fa-check"></i> ' + labels.periodsOfCareView.endPeriodOfCare);
            });
        },

        getPeriodDeletionRequest: function (url, periodOfCareId) {
            return new Y.Promise(function (resolve, reject) {
                var model = new Y.usp.deletion.DeletionRequestList({
                    url: Y.Lang.sub(url, {
                        periodOfCareId: periodOfCareId
                    })
                });

                model.load(function (err, res) {
                    (err) ? reject(err) : resolve(JSON.parse(res.response));
                }.bind(this));
            });
        },

        determinePeriodOfCareDeletionDialog: function (e) {
            var periodOfCareId = e._event.detail.periodOfCareId,
                urlConfig = this.get('dialogConfig.periodOfCare.views.deletePeriod.config');

            this.getPeriodDeletionRequest(urlConfig.url, periodOfCareId).then(function (data) {
                //get deletion requests that are not in an end state
                var deletionRequest = data.find(function (r) {
                    return !(r.deletionState === 'COMPLETE' || r.deletionState === 'FAILED');
                });

                if (deletionRequest) {
                    if (deletionRequest.deletionState === 'READY_TO_DELETE') {
                        // period of care has been prepared for deletion so carry out deletion
                        this.showPeriodOfCareDeletionDialog(deletionRequest.id, periodOfCareId);
                    } else {
                        //a deletion is in progress, or the prepare is still in progress
                        this.showPeriodOfCareDeletionInProgressMessage()
                    }
                } else {
                    // period of care hasn't been prepared for deletion so prepare for deletion
                    this.showPeriodOfCarePrepareDeletionDialog(periodOfCareId);
                }
            }.bind(this));
        },
        showPeriodOfCareDeletionInProgressMessage: function () {
            this.periodOfCareDialog.showView('deleteInProgress', {});
        },
        showPeriodOfCarePrepareDeletionDialog: function (periodOfCareId) {
            var urlConfig = this.get('dialogConfig.periodOfCare.views.deletePeriod.config');

            this.periodOfCareDialog.showView('prepareDeletePeriod', {
                model: new Y.usp.deletion.NewDeletionRequest({
                    url: Y.Lang.sub(urlConfig.prepareUrl, {
                        periodOfCareId: periodOfCareId
                    })
                }),

                otherData: {
                    subject: this.get('subject'),
                    periodOfCareId: periodOfCareId,
                    labels: this.get('labels').prepareDeletePeriodOfCare,
                    urlConfig: urlConfig
                }
            }, function () {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //remove the cancel button
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },

        showPeriodOfCareDelete: function (e) {
            this.showPeriodOfCareDeletionDialog(e.deletionRequestId, e.periodOfCareId);
        },

        showPeriodOfCareDeletionDialog: function (deletionRequestId, periodOfCareId) {
            var urlConfig = this.get('dialogConfig.periodOfCare.views.deletePeriod.config');

            this.periodOfCareDialog.showView('deletePeriod', {
                model: new Y.usp.deletion.DeletionRequest({
                    url: Y.Lang.sub(urlConfig.deleteUrl, {
                        periodOfCareId: periodOfCareId,
                        deletionRequestId: deletionRequestId
                    }),
                    id: deletionRequestId
                }),

                otherData: {
                    subject: this.get('subject'),
                    periodOfCareId: periodOfCareId,
                    deletionRequestId: deletionRequestId,
                    labels: this.get('labels').deletePeriodOfCare,
                    urlConfig: urlConfig
                }
            }, function () {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //remove the cancel button
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },

        showTemporaryPlacementAddDialog: function (e) {
            var config = this.get('dialogConfig.temporaryPlacement.views.add.config');
            var displayProperties = this.get('dialogConfig.temporaryPlacement.views.add.displayProperties');
            var validationLabels = this.get('dialogConfig.temporaryPlacement.views.add.validationLabels');

            this.temporaryPlacementDialog.showView('add', {
                model: new Y.app.childlookedafter.NewTemporaryPlacementForm({
                    url: config.url,
                    labels: validationLabels
                }),
                otherData: {
                    subject: this.get('subject'),
                    periodOfCareId: e._event.detail.periodOfCareId
                },
                displayProperties: {
                    showProviderTypeField: displayProperties.showProviderTypeField
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        _getTemporaryPlacementTypeClassifications: function (url) {
            return new Y.usp.classification.ClassificationWithCodePathList({
                url: url
            });
        },

        _getPlacementTypes: function (data) {
            var placementTypes = {};
            Y.Array.each(data, function (type) {
                placementTypes[type.code] = {
                    active: true,
                    id: type.id,
                    code: type.code,
                    name: type.name,
                    description: type.name,
                    system: type.system
                };
            });
            return placementTypes;
        },

        _getTypesOfCareForSubject: function (url, carerSubject) {
            if (!carerSubject) return null;
            var typeOfCareURL = Y.Lang.sub(url, {
                subjectId: carerSubject.id
            });
            return new Promise(function (resolve, reject) {
                Y.io(typeOfCareURL, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    on: {
                        success: function (trId, response) {
                            try {
                                resolve(JSON.parse(response.response).results);
                            } catch (e) {
                                reject(e);
                            }
                        }
                    }
                });
            });
        },

        _getTypeOfCareOptions: function (data) {
            var codedEntry = {};
            Y.Array.each(data, function (item) {
                var codedEntryVO = item.typeOfCareAssignmentVO.codedEntryVO;
                codedEntry[codedEntryVO.code] = {
                    active: codedEntryVO.active,
                    id: codedEntryVO.id,
                    code: codedEntryVO.code,
                    name: codedEntryVO.name,
                    description: codedEntryVO.name,
                    system: codedEntryVO.system
                };
            });
            return codedEntry;
        },

        showTemporaryPlacementEditDialog: function (e) {
            var config = this.get('dialogConfig.temporaryPlacement.views.edit.config'),
                labels = this.get('dialogConfig.temporaryPlacement.views.edit.labels'),
                displayProperties = this.get('dialogConfig.temporaryPlacement.views.add.displayProperties'),
                episodeOfCare = e._event.detail.episodeOfCare,
                placement = episodeOfCare.placement,
                carerSubject = placement.carerSubject,
                placementId = placement.id;

            var temporaryPlacementTypePromise = new Y.Promise(function (resolve, reject) {
                this._getTemporaryPlacementTypeClassifications(config.temporaryPlacementTypesURL).load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            Promise.all([temporaryPlacementTypePromise, this._getTypesOfCareForSubject(config.typeOfCareURL, carerSubject)]).then(function (data) {
                this.temporaryPlacementDialog.showView('edit', {
                    model: new Y.app.childlookedafter.UpdateTemporaryPlacementForm({
                        url: Y.Lang.sub(config.url, {
                            id: placementId
                        }),
                    }),
                    otherData: {
                        placement: placement,
                        carerSubject: carerSubject,
                        episodeOfCareId: episodeOfCare.id,
                        periodOfCareId: episodeOfCare.periodOfCareId,
                        subject: this.get('subject')
                    },
                    displayProperties: {
                        showProviderTypeField: displayProperties.showProviderTypeField
                    },
                    codedEntries: {
                        placementTypes: this._getPlacementTypes(data[0]),
                        typeOfCares: this._getTypeOfCareOptions(data[1]),
                        placementProvisions: Y.uspCategory.childlookedafter.placementProvision.category.codedEntries,
                        carerTypes: {
                            'PERSON': { code: 'PERSON', name: labels.carerTypePerson },
                            'ORGANISATION': { code: 'ORGANISATION', name: labels.carerTypeOrganisation }
                        }
                    }
                }, {
                    modelLoad: true
                },
                    function () {
                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
            }.bind(this));
        },

        showTemporaryPlacementViewDialog: function (e) {
            var config = this.get('dialogConfig.temporaryPlacement.views.view.config'),
                labels = this.get('dialogConfig.temporaryPlacement.views.edit.labels'),
                displayProperties = this.get('dialogConfig.temporaryPlacement.views.add.displayProperties'),
                placement = e._event.detail.episodeOfCare.placement,
                carerSubject = placement.carerSubject;

            var temporaryPlacementTypePromise = new Y.Promise(function (resolve, reject) {
                this._getTemporaryPlacementTypeClassifications(config.temporaryPlacementTypesURL).load(function (err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            Promise.all([temporaryPlacementTypePromise, this._getTypesOfCareForSubject(config.typeOfCareURL, carerSubject)]).then(function (data) {
                this.temporaryPlacementDialog.showView('view', {
                    model: new Y.usp.Model({
                        placement: placement,
                        carerSubject: carerSubject
                    }),
                    otherData: {
                        subject: this.get('subject')
                    },
                    displayProperties: {
                        showProviderTypeField: displayProperties.showProviderTypeField
                    },
                    codedEntries: {
                        placementTypes: this._getPlacementTypes(data[0]),
                        typeOfCares: this._getTypeOfCareOptions(data[1]),
                        placementProvisions: Y.uspCategory.childlookedafter.placementProvision.category.codedEntries,
                        carerTypes: {
                            'PERSON': { code: 'PERSON', name: labels.carerTypePerson },
                            'ORGANISATION': { code: 'ORGANISATION', name: labels.carerTypeOrganisation }
                        }
                    }
                });
            }.bind(this));
        },

        showTemporaryPlacementDelete: function (e) {
            this.showTemporaryPlacementDeleteDialog(e.tempPlacementId);
        },

        showTemporaryPlacementDeleteDialog: function (e) {
            var urlConfig = this.get('dialogConfig.temporaryPlacement.views.deleteTempPlacement.config');

            this.temporaryPlacementDialog.showView('deleteTempPlacement', {
                model: new Y.usp.childlookedafter.Placement({
                    url: Y.Lang.sub(urlConfig.url, {
                        tempPlacementId: e._event.detail.tempPlacementId
                    })
                }),

                otherData: {
                    subject: this.get('subject'),
                    tempPlacementId: e._event.detail.tempPlacementId,
                }
            }, function () {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //remove the cancel button
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },

        showAdoptionInformationAddDialog: function (e) {
            var config = this.get('dialogConfig.adoptionInformation.views.add.config');
            this.adoptionInformationDialog.showView('addWithPeriodOfCare', {
                model: new Y.app.childlookedafter.NewAdoptionInformationForm({
                    url: config.url
                }),
                otherData: {
                    subject: this.get('subject'),
                    periodOfCareId: e._event.detail.periodOfCareId
                }
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },

        showAdoptionInformationViewDialog: function (e) {
            var config = this.get('dialogConfig.adoptionInformation.views.view.config');

            this.adoptionInformationDialog.showView('view', {
                model: new Y.app.childlookedafter.AdoptionInformationForm({
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.adoptionInformationId
                    })
                }),
                otherData: {
                    subject: this.get('subject'),
                    id: e._event.detail.adoptionInformationId
                }
            }, {
                modelLoad: true
            }, function () {
                if (!this.views.view.permissions.canUpdateAdoptionInformation) {
                    this.removeButton('editButton', Y.WidgetStdMod.FOOTER);
                }
            });
        },

        showAdoptionInformationEditDialog: function (e) {
            var config = this.get('dialogConfig.adoptionInformation.views.edit.config');
            var adoptionInformationId = (e._event.detail) ?
                e._event.detail.adoptionInformationId :
                e._event.details[0].adoptionInformationId;

            this.adoptionInformationDialog.showView('edit', {
                model: new Y.app.childlookedafter.UpdateAdoptionInformationForm({
                    url: Y.Lang.sub(config.url, {
                        id: adoptionInformationId
                    })
                }),
                otherData: {
                    subject: this.get('subject'),
                    id: adoptionInformationId
                }
            }, {
                modelLoad: true
            }, function () {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });

        },

        handleSave: function () {
            //refresh the view on adding a new period of care
            this.childLookedAfterView.refresh();
        },

        /**
         * @method chkAddPeriodButton
         * Add period of care button is not just permissions based.  We may need to disable
         * button if there is an open period of care when page first loaded.
         */
        chkAddPeriodButton: function () {
            var latestPeriodOfCare = this.get('latestPeriodOfCare'),
                diedDischargeReasonCode = 'E2';

            if (this.get('permissions.canDelete') ||
                (this.get('permissions.canAddPeriodOfCare') && (latestPeriodOfCare === null || (latestPeriodOfCare.endDate !== null && latestPeriodOfCare.dischargeReason !== diedDischargeReasonCode)))) {
                this.toolbar.enableButton('addPeriodOfCare')
            } else {
                this.toolbar.disableButton('addPeriodOfCare')
            }
        },

        _displayAddButtonCheck: function (personData) {
            var toolbar = this.toolbar.get('toolbarNode');
            if (!this._hasDemographics(personData)) {
                toolbar.one('#addPeriodOfCare').setStyle('display', 'none');
                toolbar.one('.h3.pop-up-data').removeClass('hidden');
            } else {
                this.chkAddPeriodButton();
            }
        },

        _hasDemographics: function (personData) {
            if (personData.lifeState === 'UNBORN' || !personData.ethnicity || personData.ethnicity === 'NOT_KNOWN' || personData.gender === 'UNKNOWN' || personData.gender === 'INDETERMINATE' || !personData.dateOfBirth || personData.dateOfBirthEstimated || !personData.personTypes.includes('CLIENT')) {
                return false;
            }

            return true;
        },

        handlePeriodsOfCareLoaded: function (e) {
            this.set('latestPeriodOfCare', e._event.detail.latestPeriodOfCare || null);
        },

        handleLatestPeriodOfCareChange: function () {
            this.chkAddPeriodButton();
        }
    }, {
        ATTRS: {
            personDetails: {
                valueFn: function () {
                    return new Y.usp.relationshipsrecording.PersonDetails({
                        url: this.get('dialogConfig.periodOfCare.personDetailsModelURL')
                    })
                }
            },
            latestPeriodOfCare: {
                value: null
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-toolbar',
        'childlookedafter-view',
        'periodofcare-dialog',
        'episodeofcare-dialog',
        'absencefromplacement-dialog',
        'temporaryplacement-dialog',
        'adoptioninformation-dialog',
        'output-document-dialog-views',
        'categories-childlookedafter-component-AdopterLegalStatus',
        'categories-childlookedafter-component-PlacementProvision',
        'usp-relationshipsrecording-PersonDetails',
        'usp-deletion-DeletionRequest',
        'usp-deletion-NewDeletionRequest',
        'usp-childlookedafter-Placement',
        'usp-childlookedafter-PeriodOfCare',
        'adoptinformation-view',
        'childlookedafter-component-enumerations',
        'location-dialog'
    ]
});