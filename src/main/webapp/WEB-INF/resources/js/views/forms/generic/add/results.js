YUI.add('add-form-results', function (Y) {
    'use-strict';

    var L = Y.Lang,
        CaseRecordingFormatters = Y.app.ColumnFormatters;

    Y.namespace('app.form').PaginatedFilteredFormDefinitionResultList = Y.Base.create('paginatedFilteredFormDefinitionResultList', Y.usp.form.PaginatedFormDefinitionList, [Y.usp.ResultsFilterURLMixin], {
        whiteList: ['name'],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app.form').AddFormResults = Y.Base.create('addFormResults', Y.usp.app.Results, [], {
        getResultsListModel: function (config) {
            var subject = config.subject,
                permissions = config.permissions;

            //the permission canViewAutomatedAdd controls whether forms
            //flagged as restricted appear in the search results

            return new Y.app.form.PaginatedFilteredFormDefinitionResultList({
                url: L.sub(config.url, {
                    //if the user is unable to view restricted form definition - we must exclude them
                	excludePreferAutomatedAdd: (permissions.canViewAutomatedAdd !== true),
                    subjectId: subject.subjectId,
                    subjectType: (subject.subjectType || '').toUpperCase()
                }),
                //Required access level is WRITE as user needs to be able to write form
                requestedAccess: 'WRITE',
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function (config) {
            var labels = config.labels || {},
                permissions = config.permissions;

            return ([{
                key: 'name',
                label: labels.name,
                width: '80%',
                sortable: true
            }, {
                key: 'version',
                label: labels.version,
                width: '10%',
                sortable: true
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                formatter: CaseRecordingFormatters.button,
                button: {
                    clazz: 'add',
                    iconClazz: 'fa fa-plus-circle',
                    title: labels.addFormTitle,
                    label: labels.addForm,
                    enabled: function () {
                        if (permissions.canAdd === true && this.record.hasAccessLevel('WRITE') === true) {
                            return true;
                        }
                        return false;
                    }
                }
            }]);
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function () {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    }];
                }
            },
        }
    });

    Y.namespace('app.form').AddFormFilteredResults = Y.Base.create('addFormFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.form.AddFormFilterView,
        resultsType: Y.app.form.AddFormResults
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-filtered-results',
        'results-filter',
        'results-table-search-state-plugin',
        'caserecording-results-formatters',
        'add-form-filter',
        'usp-form-FormDefinition'
    ]
});