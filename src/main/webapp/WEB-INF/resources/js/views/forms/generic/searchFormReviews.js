YUI.add('form-reviews-view', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        Q = Y.QueryString,
        E = Y.Escape,
        USPTemplates = Y.usp.ColumnTemplates,
        USPFormatters = Y.usp.ColumnFormatters,
        FormsFormatters = Y.app.form.ColumnFormatters;

    Y.namespace('app').FormReviewListView = Y.Base.create('formReviewListView', Y.View, [], {
        initializer: function() {
            var searchConfig = this.get('searchConfig'),
                labels = searchConfig.labels,
                permissions = searchConfig.permissions;

            //configure the review results table
            this.results = new Y.usp.PaginatedResultsTable({
                //configure the initial sort
                sortBy: [{
                        reviewDate: 'desc'
                    }],
                //declare the columns
                columns: [
                {
                    key: 'reviewDate',
                    label: labels.reviewDate,
                    width: '15%',
                    sortable: false,
                    formatter: Y.usp.ColumnFormatters.date
                }, {
                    key: 'status',
                    label: labels.reviewedStatus,
                    width: '10%',
                    sortable: false,
                    allowHTML: true,
                    formatter: FormsFormatters.formReviewState
                }, {
                    key: 'reviewerName',
                    label: labels.reviewedBy,
                    width: '15%',
                    sortable: false
                }, {
                    key: 'comments',
                    label: labels.comments,
                    width: '60%',
                    sortable: false,
                    allowHTML: true,
                    expandedFormatter: this.commentsExpanded,
                    collapsedFormatter: this.commentsCollapsed
                }],
                //configure the data set
                data: new Y.usp.form.PaginatedFormInstanceReviewList({
                    url: searchConfig.url
                }),
                noDataMessage: labels.noReviews,
                plugins: [
                    //plugin Keyboard nav
                    {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    },
                    {
                        fn: Y.Plugin.usp.ResultsTableExpandableCellsPlugin
                    }
                ]
            });

            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                tableId: this.name
            });

            this._evtHandlers = [
                this.results.delegate('click', this._handleRowClick, '.pure-table-data tr button, .pure-table-data tr a', this)
            ];

            // Add event targets
            this.results.addTarget(this);
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            if (this.results) {
                //destroy the results table
                this.results.destroy();

                delete this.results;
            }

            if (this._tablePanel) {
                this._tablePanel.destroy();

                delete this._tablePanel;
            }
        },
        render: function() {
            var container = this.get('container'),
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.

            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));

            //render results
            this._tablePanel.renderResults(this.results);

            //finally set the content into the container
            container.setHTML(contentNode);

            return this;
        },
        // strips out HTML markup from comments attribute and truncates if exceeds 150 shars
        commentsCollapsed: function(o) {
            var valueText,
            removeMarkup = function (html) {
                var div = document.createElement("div");
                div.innerHTML = html,
                childNode=div.childNodes[0];
                return E.html(childNode.textContent||childNode.innerText||"");
            };
            
            if (o.value) {
                valueText = removeMarkup(o.value);
                if (valueText.length > 150) {
                    return valueText.substr(0, 150).concat('...');
                }
                return valueText;
            }
            return '';
        },
        // shows the full comments attribute including markup in a scrollable dive
        commentsExpanded: function(o) {
           return '<div style="max-height:400px; overflow-y:auto">' + o.value + '</div>';
        }
    }, {
        ATTRS: {
            searchConfig: {
                url: {},
                noDataMessage: {},
                permissions: {}
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
                'view',
                'escape',
                'accordion-panel',
                'querystring-stringify',
                'paginated-results-table',
                'results-formatters',
                'results-templates',
                'results-table-menu-plugin',
                'results-table-keyboard-nav-plugin',
                'results-table-expandable-cells-plugin',
                'usp-form-FormInstanceReview',
                'form-results-formatters'
                ]
});