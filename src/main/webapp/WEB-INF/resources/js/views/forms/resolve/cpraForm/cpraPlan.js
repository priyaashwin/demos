YUI.add('cpraform-plan-view', function(Y) {
    var A = Y.Array,
        L = Y.Lang;


    Y.namespace('app').PlanView = Y.Base.create('planView', Y.View, [], {
        template: Y.Handlebars.templates['cpraFormKeyAreasView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                labels: this.get('labels'),
                formModel: this.get('formModel').toJSON()
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {}
        }
    });

    Y.namespace('app').PlanTable = Y.Base.create('planTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                modelList = new Y.usp.resolveassessment.KeyAreaList({
                    url: L.sub(this.get('url'), {
                        id: this.get('model').get('id'),
                        planType: this.get('planType')
                    })
                });

            this.results = new Y.app.PlanView({
                modelList: modelList,
                formModel: this.get('model'),
                labels: config.labels
            });

            this._planTableEvtHandlers = [
                container.delegate('click', this._handleRowClick, '.keyArea .pure-button', this)
            ];

            //load model list
            modelList.load();
        },

        render: function() {
            // superclass call
            Y.app.PlanTable.superclass.render.call(this);

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._planTableEvtHandlers) {
                A.each(this._planTableEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._planTableEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();


            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id'),
                    planName: this.get('planName')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            labels: {
                value: {}
            },
            /**
             * @Attribute model The careplan model for which we want to show key areas
             */
            model: {},
            /**
             * @Attribute url - base URL for loading key areas
             */
            url: {},
            /**
             * @Attribute planType - Substituted into the URL
             */
            planType: {},
            planName: {
                getter: function() {
                    var planType = this.get('planType'),
                        enumObj = A.find(Y.usp.resolveassessment.enum.CarePlanType.values, function(o) {
                            return o.enumName === planType;
                        }) || {};
                    return enumObj.name || '';
                }
            }
        }
    });


    Y.namespace('app').PlanAccordion = Y.Base.create('planAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            var planType = this.get('planType');

            this.planTable = new Y.app.PlanTable(Y.merge(config, {
                planType: planType,
                title: config.labels.title
            }));

            // Add event targets
            this.planTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.planTable.render().get('container'));

            //superclass call
            Y.app.PlanAccordion.superclass.render.call(this);
            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.planTable.destroy();
            delete this.planTable;
        },
        reload: function() {
            this.planTable.reload();
        }
    }, {
        ATTRS: {
            planType: {},
            title: {
                getter: function(val) {
                    var planType = this.get('planType'),
                        enumObj = A.find(Y.usp.resolveassessment.enum.CarePlanType.values, function(o) {
                            return o.enumName === planType;
                        }) || {};
                    return L.sub(val, {
                        planType: enumObj.name || ''
                    });
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'accordion-panel',
               'results-table',
               'results-table-keyboard-nav-plugin',
               'results-table-menu-plugin',
               'results-formatters',
               'usp-resolveassessment-KeyArea',
               'resolveassessment-component-enumerations'
              ]
});