YUI.add('intakeform-referral-tab', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        ADD_BUTTON_TEMPLATE = '<div class="pull-right"><button class="pure-button-active pure-button {id}" disabled="disabled"><i class="fa fa-plus"></i> {label}</button></div>';

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };


    Y.namespace('app').IntakeFormReferrerView = Y.Base.create('intakeFormReferrerView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormReferrerView']
    });

    Y.namespace('app').IntakeFormFutureView = Y.Base.create('intakeFormFutureView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormFutureView']
    });

    Y.namespace('app').SupportPlanView = Y.Base.create('supportPlanView', Y.View, [], {
        template: Y.Handlebars.templates['intakeFormSupportPlanView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {}
        }
    });

    Y.namespace('app').SupportPlanTable = Y.Base.create('supportPlanTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                formModel=this.get('model'),
                modelList = new Y.usp.resolveassessment.IntakeSupportPlanList({
                    url: L.sub(this.get('url'), {
                        id: formModel.get('id')
                    })
                });

            this.results = new Y.app.SupportPlanView({
                modelList: modelList,
                formModel: formModel,
                labels: config.labels
            });

            this._supportPlansEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.supportPlan .pure-button', this)
            ];

            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            formModel.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('labels').addButton;

            // superclass call
            Y.app.SupportPlanTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._supportPlansEvtHandlers) {
                A.each(this._supportPlansEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._supportPlansEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }
        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event
                    this.fire('add', {
                        id: this.get('model').get('id')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id')
                });
            } else if (target.hasClass('remove')) {
                this.fire('remove', {
                    id: record.get('id')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading placement history
             */
            url: {},
            /**
             * @Attribute labels - The view labels
             */
            labels: {}
        }
    });

    Y.namespace('app').IntakeFormInterventionAndSupportPlansAccordion = Y.Base.create('intakeFormInterventionAndSupportPlansAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //setup support plan table
            this.supportPlanTable = new Y.app.SupportPlanTable(Y.merge(config.supportPlanConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.supportPlanTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.supportPlanTable.render().get('container'));

            //superclass call
            Y.app.IntakeFormInterventionAndSupportPlansAccordion.superclass.render.call(this);

            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.supportPlanTable.destroy();
            delete this.supportPlanTable;
        },
        reload: function() {
            this.supportPlanTable.reload();
        }
    });


    Y.namespace('app').IntakeFormReferrerAccordion = Y.Base.create('intakeFormReferrerAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormReferrerView = new Y.app.IntakeFormReferrerView(config);

            // Add event targets
            this.intakeFormReferrerView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormReferrerAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormReferrerView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormReferrerView.destroy();

            delete this.intakeFormReferrerView;
        }
    });

    Y.namespace('app').IntakeFormFutureAccordion = Y.Base.create('intakeFormFutureAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormFutureView = new Y.app.IntakeFormFutureView(config);

            // Add event targets
            this.intakeFormFutureView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormFutureAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormFutureView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormFutureView.destroy();

            delete this.intakeFormFutureView;
        }
    });

    Y.namespace('app').IntakeFormReferralTab = Y.Base.create('intakeFormReferralTab', Y.View, [], {
        initializer: function() {
            var referrerConfig = this.get('referrerConfig'),
                futureConfig = this.get('futureConfig'),
                interventionAndSupportPlansConfig = this.get('interventionAndSupportPlansConfig');

            //create the accordion views
            this.referrerAccordion = new Y.app.IntakeFormReferrerAccordion(
                Y.merge(
                    referrerConfig, {
                        model: this.get('model')
                    }));

            this.futureAccordion = new Y.app.IntakeFormFutureAccordion(
                Y.merge(
                    futureConfig, {
                        model: this.get('model')
                    }));

            this.interventionAndSupportPlansAccordion = new Y.app.IntakeFormInterventionAndSupportPlansAccordion(
                Y.merge(
                    interventionAndSupportPlansConfig, {
                        model: this.get('model')
                    }));


            // Add event targets
            this.referrerAccordion.addTarget(this);
            this.futureAccordion.addTarget(this);
            this.interventionAndSupportPlansAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.referrerAccordion.render().get('container'));
            contentNode.append(this.futureAccordion.render().get('container'));
            contentNode.append(this.interventionAndSupportPlansAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties
            this.referrerAccordion.destroy();
            delete this.referrerAccordion;

            this.futureAccordion.destroy();
            delete this.futureAccordion;

            this.interventionAndSupportPlansAccordion.destroy();
            delete this.interventionAndSupportPlansAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            referrerConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            futureConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            interventionAndSupportPlansConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeFormFullDetails'
              ]
});