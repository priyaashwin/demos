YUI.add('form-instance-results-view', function(Y) {
  'use-strict';

  /**
   * A view for displaying form instance results and the associated filter
   * 
   * A thin wrapper around the appropriate search view
   */
  Y.namespace('app.form').FormInstanceResultsView = Y.Base.create('formInstanceResultsView', Y.View, [], {
    initializer: function(config) {
      var subject = config.subject,
        subjectType = subject.subjectType,
        resultsConfig = config.resultsConfig || {};

      //setup config object for the view
      var viewConfig = Y.mix({
        subject: subject
      }, resultsConfig);


      this.formInstanceResults = new Y.app.form.FormInstanceControllerView(viewConfig).addTarget(this);
    },
    render: function() {
      var container = this.get('container');

      container.setHTML(this.formInstanceResults.render().get('container'));
      return this;
    },
    destructor: function() {
      if (this.formInstanceResults) {
        this.formInstanceResults.removeTarget(this);
        this.formInstanceResults.destroy();
        delete this.formInstanceResults;
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'form-instance-search-controller'
  ]
});