YUI.add('form-search-view', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        Q = Y.QueryString,
        DRAFT_STATUS = 'DRAFT',
        COMPLETE_STATUS = 'COMPLETE',
        AUTHORISED_STATUS = 'AUTHORISED',
        DELETE_STATUS = 'ARCHIVED',
        USPTemplates = Y.usp.ColumnTemplates,
        USPFormatters = Y.usp.ColumnFormatters,
        FormsFormatters = Y.app.form.ColumnFormatters;

    Y.namespace('app').PaginatedFilteredFormSearchResultList = Y.Base.create("paginatedFilteredFormSearchResultList", Y.usp.resolveassessment.PaginatedFormSearchResultList, [], {
        whiteList: ["status"],
        //Note includeDeleted is a special case here so not in the whitelist

        //override get url to allow us to link to a filter form
        getURL: function(action, options) {
            var filterModel = this.get("filterModel"),
                url = this._substituteURL(this.url, Y.merge(this.getAttrs(), options)),
                canViewArchived = this.get("canViewArchived"),
                data, parsedData = {},
                qStr, tObj;

            if (filterModel) {
                data = filterModel.toJSON();

                //remove ID from data
                delete data['id'];

                A.each(this.whiteList, function(v) {
                    tObj = data[v];
                    if (typeof tObj !== "undefined" && tObj !== null) {
                        if (L.isArray(tObj) && tObj.length === 0) {
                            return;
                        }
                        parsedData[v] = data[v];
                    }
                });

                //if no state filter applied then set statuses to search by default 
                if (typeof parsedData.status === "undefined" || parsedData.status === null) {
                    if (canViewArchived === true) {
                        parsedData.status = [DRAFT_STATUS, COMPLETE_STATUS, AUTHORISED_STATUS, DELETE_STATUS];
                    } else {
                        parsedData.status = [DRAFT_STATUS, COMPLETE_STATUS, AUTHORISED_STATUS];
                    }
                }

                //append filter data to URL
                qStr = Q.stringify(parsedData);
                if (qStr.length > 0) {
                    url = url.concat("&" + qStr);
                }
            }
            return url;
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            },
            canViewArchived: {
                value: false
            }
        }
    });

    Y.namespace('app').FormSearchView = Y.Base.create('formSearchView', Y.View, [], {
        initializer: function() {
            var searchConfig = this.get('searchConfig'),
                filterContextConfig = this.get('filterContextConfig'),
                filterConfig = this.get('filterConfig'),
                statusDialogConfig = this.get('statusDialogConfig'),
                labels = searchConfig.labels,
                permissions = searchConfig.permissions;

            //configure the filter model
            this.filterModel = new Y.app.FormResultsFilterModel();

            //configure the results filter context
            this.filterContext = new Y.app.FormFilterContext(Y.merge(filterContextConfig, {
                model: this.filterModel
            }));

            //configure the results filter
            this.resultsFilter = new Y.app.FormResultsFilter(Y.merge(filterConfig, {
                model: this.filterModel
            }));

            //create form dialog
            this.formStatusDialog = new Y.app.ResolveStatusFormDialog({
                headerContent: statusDialogConfig.defaultHeader,
                views: {
                    /* !!! Merge with existing view configuration defined in resolvestatusform.js !!! */
                    completeForm: {
                        headerTemplate: statusDialogConfig.completeConfig.headerTemplate
                    },
                    reopenForm: {
                        headerTemplate: statusDialogConfig.reopenConfig.headerTemplate
                    },
                    rejectForm: {
                        headerTemplate: statusDialogConfig.rejectConfig.headerTemplate
                    },
                    approveForm: {
                        headerTemplate: statusDialogConfig.approveConfig.headerTemplate
                    },
                    removeForm: {
                        headerTemplate: statusDialogConfig.deleteConfig.headerTemplate
                    }
                }
            });

            this.formStatusDialog.render();

            //configure the form results table
            this.results = new Y.usp.PaginatedResultsTable({
                //configure the initial sort
                sortBy: [{
                        status: 'desc'
                    }, {
                        dateStarted: 'desc'
                }],
                //declare the columns
                columns: [{
                    key: 'dateStarted',
                    label: labels.startDate,
                    sortable: true,
                    width: '15%',
                    formatter: USPFormatters.date
                }, {
                    key: 'dateCompleted',
                    label: labels.completedDate,
                    sortable: true,
                    width: '15%',
                    formatter: USPFormatters.date
                }, {
                    key: 'dateAuthorised',
                    label: labels.authorisedDate,
                    sortable: true,
                    width: '15%',
                    formatter: USPFormatters.date
                }, {
                    key: 'formType',
                    label: labels.type,
                    sortable: true,
                    width: '15%',
                    formatter: FormsFormatters.formType,
                    cellTemplate: USPTemplates.clickable(labels.viewForm, 'viewForm', permissions.canView)
                }, {
                    key: 'lastUpdated',
                    label: labels.editDate,
                    sortable: true,
                    width: '15%',
                    formatter: USPFormatters.date
                }, {
                    key: 'initiatorName',
                    label: labels.user,
                    width: '15%'
                }, {
                    key: 'status',
                    label: labels.state,
                    width: '10%',
                    sortable: true,
                    allowHTML: true,
                    formatter: FormsFormatters.formState
                }, {
                    label: labels.actions,
                    className: 'pure-table-actions',
                    width: '8%',
                    formatter: USPFormatters.actions,
                    items: [{
                        clazz: 'viewForm',
                        title: labels.viewFormTitle,
                        label: labels.viewForm,
                        enabled: permissions.canView
                    }, {
                        clazz: 'completeForm',
                        title: labels.completeFormTitle,
                        label: labels.completeForm,
                        enabled: function() {
                            if (permissions.canComplete === true) {
                                return (this.record.get('status') === 'DRAFT');
                            }
                            return false;
                        }
                    }, {
                        clazz: 'reopenForm',
                        title: labels.reopenFormTitle,
                        label: labels.reopenForm,
                        enabled: function() {
                            if (permissions.canReopen === true) {
                                return (this.record.get('status') === 'COMPLETE');
                            }
                            return false;
                        }
                    }, {
                        clazz: 'rejectForm',
                        title: labels.rejectFormTitle,
                        label: labels.rejectForm,
                        visible: permissions.canReject,
                        enabled: function() {
                            if (permissions.canReject === true) {
                                return (this.record.get('status') === 'COMPLETE');
                            }
                            return false;
                        }
                    }, {
                        clazz: 'approveForm',
                        title: labels.approveFormTitle,
                        label: labels.approveForm,
                        visible: permissions.canApprove,
                        enabled: function() {
                            if (permissions.canApprove === true) {
                                return (this.record.get('status') === 'COMPLETE');
                            }
                            return false;
                        }
                    }, {
                        clazz: 'deleteForm',
                        title: labels.deleteFormTitle,
                        label: labels.deleteForm,
                        enabled: function() {
                            if (permissions.canDelete === true) {
                                return (this.record.get('status') === 'DRAFT' || this.record.get('status') === 'COMPLETE');
                            }
                            return false;
                        }
                    }]
                }],
                //configure the data set
                data: new Y.app.PaginatedFilteredFormSearchResultList({
                    url: searchConfig.url,
                    canViewArchived: searchConfig.permissions.canViewArchived,
                    //plug in the filter model
                    filterModel: this.filterModel
                }),
                noDataMessage: searchConfig.noDataMessage,
                plugins: [
                    //plugin Menu handler
                    {
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    },
                    //plugin Keyboard nav
                    {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }
                ]
            });

            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                tableId: this.name
            });

            this._evtHandlers = [
                this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this),
                //listen for filter event
                this.on('filter:apply', function(e) {

                    //reload results
                    this.results.reload();
                }, this),
                //subscribe to form status change events
                this.on('formStatusChangeView:saved', function(e) {
                    //reload results
                    this.results.reload();
                }, this),
                this.on('rejectFormView:saved', function(e) {

                    //reload results
                    this.results.reload();
                }, this)
            ];

            // Add event targets
            this.filterContext.addTarget(this);
            this.resultsFilter.addTarget(this);
            this.results.addTarget(this);
            this.formStatusDialog.addTarget(this);
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            if (this.results) {
                //destroy the results table
                this.results.destroy();

                delete this.results;
            }

            if (this.filterContext) {
                this.filterContext.destroy();

                delete this.filterContext;
            }

            if (this.resultsFilter) {
                this.resultsFilter.destroy();

                delete this.resultsFilter;
            }

            if (this.formStatusDialog) {
                this.formStatusDialog.destroy();

                delete this.formStatusDialog;
            }

            if (this._tablePanel) {
                this._tablePanel.destroy();

                delete this._tablePanel;
            }
        },
        render: function() {
            var container = this.get('container'),
                filterContextContainer,

                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.

            //Render the filter context (this lives outside this view container)
            filterContextContainer = this.filterContext.render().get('container');

            if (!filterContextContainer.inDoc()) {
                contentNode.append(filterContextContainer);
            }

            //Results filter
            contentNode.append(this.resultsFilter.render().get('container'));

            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));

            //render results
            this._tablePanel.renderResults(this.results);

            //finally set the content into the container
            container.setHTML(contentNode);

            return this;
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                record = this.results.getRecord(target.get("id")),
                subject = this.get('subject');

            e.preventDefault();

            //check what link was clicked by checking CSS classes
            if (target.hasClass('viewForm')) {
                //go to the form view URL
                if (record.get("formType") === 'PlacementReport') {
                    window.location = (L.sub(this.get('searchConfig').prFormURL, {
                        id: record.get('id')
                    }));
                } else if (record.get("formType") === 'CarePlanRiskAssessment') {
                    window.location = (L.sub(this.get('searchConfig').cpraFormURL, {
                        id: record.get('id')
                    }));
                } else if (record.get("formType") === 'IntakeForm') {
                    window.location = (L.sub(this.get('searchConfig').intakeFormURL, {
                        id: record.get('id')
                    }));
                }
            } else {
                var url, formName, formType;
                if (record.get("formType") === 'PlacementReport') {
                    url = this.get('statusDialogConfig').prFormURL;
                    formName = this.get('prFormName');
                    formType = this.get('statusDialogConfig').prFormType;
                } else if (record.get("formType") === 'CarePlanRiskAssessment') {
                    url = this.get('statusDialogConfig').cpraFormURL;
                    formName = this.get('cpraFormName');
                    formType = this.get('statusDialogConfig').cpraFormType;
                } else if (record.get("formType") === 'IntakeForm') {
                    url = this.get('statusDialogConfig').intakeFormURL;
                    formName = this.get('intakeFormName');
                    formType = this.get('statusDialogConfig').intakeFormType;
                }

                if (target.hasClass('completeForm')) {

                    this.formStatusDialog.showView('completeForm', {
                            message: this.get('statusDialogConfig').completeConfig.message,
                            model: new Y.app.StatusChangeForm({
                                url: (L.sub(url, {
                                    action: 'complete'
                                })),
                                id: record.get('id')
                            }),
                            formModel: {
                                subject: subject,
                                status: record.get('status')
                            },
                            otherData: {
                                narrative: L.sub(this.get('statusDialogConfig').completeConfig.narrative, {
                                    formName: formName
                                }),
                                formType: formType
                            },
                            successMessage: this.get('statusDialogConfig').completeConfig.successMessage
                        },
                        function() {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                } else if (target.hasClass('reopenForm')) {
                    this.formStatusDialog.showView('reopenForm', {
                            message: this.get('statusDialogConfig').reopenConfig.message,
                            model: new Y.app.StatusChangeForm({
                                url: (L.sub(url, {
                                    action: 'draft'
                                })),
                                id: record.get('id')
                            }),
                            formModel: {
                                subject: subject,
                                status: record.get('status')
                            },
                            otherData: {
                                narrative: L.sub(this.get('statusDialogConfig').reopenConfig.narrative, {
                                    formName: formName
                                }),
                                formType: formType
                            },
                            successMessage: this.get('statusDialogConfig').reopenConfig.successMessage
                        },
                        function() {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                } else if (target.hasClass('rejectForm')) {
                    this.formStatusDialog.showView('rejectForm', {
                            message: this.get('statusDialogConfig').rejectConfig.message,
                            model: new Y.app.RejectStatusForm({
                                url: (L.sub(url, {
                                    action: 'rejected'
                                })),
                                id: record.get('id')
                            }),
                            formModel: {
                                subject: subject,
                                status: record.get('status')
                            },
                            otherData: {
                                narrative: L.sub(this.get('statusDialogConfig').rejectConfig.narrative, {
                                    formName: formName
                                }),
                                formType: formType
                            },
                            successMessage: this.get('statusDialogConfig').rejectConfig.successMessage,
                            labels: this.get('statusDialogConfig').rejectConfig.labels
                        },
                        function() {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                } else if (target.hasClass('approveForm')) {
                    this.formStatusDialog.showView('approveForm', {
                            message: this.get('statusDialogConfig').approveConfig.message,
                            model: new Y.app.StatusChangeForm({
                                url: (L.sub(url, {
                                    action: 'authorised'
                                })),
                                id: record.get('id')
                            }),
                            formModel: {
                                subject: subject,
                                status: record.get('status')
                            },
                            otherData: {
                                narrative: L.sub(this.get('statusDialogConfig').approveConfig.narrative, {
                                    formName: formName
                                }),
                                formType: formType
                            },
                            successMessage: this.get('statusDialogConfig').approveConfig.successMessage
                        },
                        function() {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                } else if (target.hasClass('deleteForm')) {
                    this.formStatusDialog.showView('removeForm', {
                            message: this.get('statusDialogConfig').deleteConfig.message,
                            model: new Y.app.StatusChangeForm({
                                url: (L.sub(url, {
                                    action: 'archived'
                                })),
                                id: record.get('id')
                            }),
                            formModel: {
                                subject: subject,
                                status: record.get('status')
                            },
                            otherData: {
                                narrative: L.sub(this.get('statusDialogConfig').deleteConfig.narrative, {
                                    formName: formName
                                }),
                                formType: formType
                            },
                            successMessage: this.get('statusDialogConfig').deleteConfig.successMessage
                        },
                        function() {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                }
            }
        }
    }, {
        ATTRS: {
            searchConfig: {
                url: {},
                prFormURL: {},
                cpraFormURL: {},
                intakeFormURL: {},
                noDataMessage: {},
                permissions: {}
            },
            filterContextConfig: {
                labels: {}
            },
            filterConfig: {
                labels: {}
            },
            statusDialogConfig: {},
            subject: {},
            prFormName: {},
            cpraFormName: {},
            intakeFormName: {}
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
                'view',
                'accordion-panel',
                'querystring-stringify',
                'paginated-results-table',
                'results-formatters',
                'results-templates',
                'results-table-menu-plugin',
                'results-table-keyboard-nav-plugin',
                'formstatus-dialog-views',
                'usp-resolveassessment-FormSearchResult',
                'form-results-filter',
                'form-filter-context',
                'caserecording-results-formatters',
                'form-results-formatters'
                ]
});