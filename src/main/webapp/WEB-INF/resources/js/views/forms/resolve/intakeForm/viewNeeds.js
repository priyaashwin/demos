YUI.add('intakeform-needs-tab', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        ADD_BUTTON_TEMPLATE = '<div class="pull-right"><button class="pure-button-active pure-button {id}" disabled="disabled"><i class="fa fa-plus"></i> {label}</button></div>';

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormSpiritualNeedsView = Y.Base.create('intakeFormSpiritualNeedsView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormSpiritualNeedsView']
    });

    Y.namespace('app').IntakeFormSocialView = Y.Base.create('intakeFormSocialView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormSocialView'],
        initializer: function() {
            var list = this.get('timetableModelList');

            // We'll re-render the view after the list loads.
            list.after('*:load', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    timetableModelList: this.get('timetableModelList').toJSON()
                });

            Y.log("render " + this.name, "debug");

            // Render this view's HTML into the container element.
            container.setHTML(html);

            return this;
        }
    });

    Y.namespace('app').PersonalCareNeedView = Y.Base.create('personalCareNeedView', Y.View, [], {
        template: Y.Handlebars.templates['intakeFormPersonalCareNeedView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            codedEntries: {
                value: {
                    needTypes: Y.uspCategory.resolveassessment.need.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app').PersonalCareNeedTable = Y.Base.create('personalCareNeedTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                formModel = this.get('model'),
                modelList = new Y.usp.resolveassessment.IntakePersonalCareNeedList({
                    url: L.sub(this.get('url'), {
                        id: formModel.get('id')
                    })
                });

            this.results = new Y.app.PersonalCareNeedView({
                modelList: modelList,
                formModel: formModel,
                labels: config.labels
            });

            this._personalCareNeedsEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.personalCareNeed .pure-button', this)
            ];

            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            formModel.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('labels').addButton;

            // superclass call
            Y.app.PersonalCareNeedTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._personalCareNeedsEvtHandlers) {
                A.each(this._personalCareNeedsEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._personalCareNeedsEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }
        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event
                    this.fire('add', {
                        id: this.get('model').get('id')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id')
                });
            } else if (target.hasClass('remove')) {
                this.fire('remove', {
                    id: record.get('id')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading placement history
             */
            url: {},
            /**
             * @Attribute labels - The view labels
             */
            labels: {}
        }
    });

    Y.namespace('app').IntakeFormPersonalCareNeedAccordion = Y.Base.create('intakeFormPersonalCareNeedAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //setup history table
            this.personalCareNeedTable = new Y.app.PersonalCareNeedTable(Y.merge(config.personalCareNeedConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.personalCareNeedTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.personalCareNeedTable.render().get('container'));

            //superclass call
            Y.app.IntakeFormPersonalCareNeedAccordion.superclass.render.call(this);
            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.personalCareNeedTable.destroy();
            delete this.personalCareNeedTable;
        },
        reload: function() {
            this.personalCareNeedTable.reload();
        }
    });

    Y.namespace('app').IntakeFormSpiritualNeedsAccordion = Y.Base.create('intakeFormSpiritualNeedsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormSpiritualNeedsView = new Y.app.IntakeFormSpiritualNeedsView(config);

            // Add event targets
            this.intakeFormSpiritualNeedsView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormSpiritualNeedsAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormSpiritualNeedsView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormSpiritualNeedsView.destroy();

            delete this.intakeFormSpiritualNeedsView;
        }
    });

    Y.namespace('app').IntakeFormSocialAccordion = Y.Base.create('intakeFormSocialAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            this.timetableModelList = new Y.usp.resolveassessment.IntakeTimetableEventList({
                url: L.sub(this.get('url'), {
                    id: this.get('model').get('id')
                })
            });

            //Create view passing on configuration object
            this.intakeFormSocialView = new Y.app.IntakeFormSocialView(Y.merge(config, {
                timetableModelList: this.timetableModelList
            }));

            //load model list
            this.timetableModelList.load();

            // Add event targets
            this.intakeFormSocialView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormSocialAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormSocialView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.timetableModelList.destroy();
            delete this.timetableModelList;

            this.intakeFormSocialView.destroy();
            delete this.intakeFormSocialView;
        },
        reload: function() {
            this.timetableModelList.load();
        }
    });

    Y.namespace('app').SocialSupportView = Y.Base.create('socialSupportView', Y.View, [], {
        template: Y.Handlebars.templates['intakeFormSocialSupportView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            codedEntries: {
                value: {
                    supportTypes: Y.uspCategory.resolveassessment.support.category.codedEntries
                }
            }
        }
    });

    Y.namespace('app').SocialSupportTable = Y.Base.create('socialSupportTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                formModel = this.get('model'),
                modelList = new Y.usp.resolveassessment.IntakeSupportList({
                    url: L.sub(this.get('url'), {
                        id: formModel.get('id')
                    })
                });

            this.results = new Y.app.SocialSupportView({
                modelList: modelList,
                formModel: formModel,
                labels: config.labels
            });

            this._socialSupportEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.socialSupport .pure-button', this)
            ];

            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            formModel.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('labels').addButton;

            // superclass call
            Y.app.SocialSupportTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._socialSupportEvtHandlers) {
                A.each(this._socialSupportEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._socialSupportEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }
        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event
                    this.fire('add', {
                        id: this.get('model').get('id')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id')
                });
            } else if (target.hasClass('remove')) {
                this.fire('remove', {
                    id: record.get('id')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading social support
             */
            url: {},
            /**
             * @Attribute labels - The view labels
             */
            labels: {}
        }
    });

    Y.namespace('app').IntakeFormSocialSupportAccordion = Y.Base.create('intakeFormSocialSupportAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            this.socialSupportTable = new Y.app.SocialSupportTable(Y.merge(config.supportConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.socialSupportTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.socialSupportTable.render().get('container'));

            //superclass call
            Y.app.IntakeFormSocialSupportAccordion.superclass.render.call(this);
            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.socialSupportTable.destroy();
            delete this.socialSupportTable;
        },
        reload: function() {
            this.socialSupportTable.reload();
        }
    });

    Y.namespace('app').IntakeFormNeedsTab = Y.Base.create('intakeFormNeedsTab', Y.View, [], {
        initializer: function() {
            var personalCareConfig = this.get('personalCareConfig'),
                spiritualNeedsConfig = this.get('spiritualNeedsConfig'),
                socialConfig = this.get('socialConfig'),
                socialSupportConfig = this.get('socialSupportConfig');

            //create the accordion views
            this.personalCareNeedAccordion = new Y.app.IntakeFormPersonalCareNeedAccordion(
                Y.merge(
                    personalCareConfig, {
                        model: this.get('model')
                    }));

            this.spiritualNeedsAccordion = new Y.app.IntakeFormSpiritualNeedsAccordion(
                Y.merge(
                    spiritualNeedsConfig, {
                        model: this.get('model')
                    }));

            this.socialAccordion = new Y.app.IntakeFormSocialAccordion(
                Y.merge(
                    socialConfig, {
                        model: this.get('model')
                    }));

            this.socialSupportAccordion = new Y.app.IntakeFormSocialSupportAccordion(
                Y.merge(
                    socialSupportConfig, {
                        model: this.get('model')
                    }));

            // Add event targets
            this.personalCareNeedAccordion.addTarget(this);
            this.spiritualNeedsAccordion.addTarget(this);
            this.socialAccordion.addTarget(this);
            this.socialSupportAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.personalCareNeedAccordion.render().get('container'));
            contentNode.append(this.spiritualNeedsAccordion.render().get('container'));
            contentNode.append(this.socialAccordion.render().get('container'));
            contentNode.append(this.socialSupportAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties
            this.personalCareNeedAccordion.destroy();
            delete this.personalCareNeedAccordion;

            this.spiritualNeedsAccordion.destroy();
            delete this.spiritualNeedsAccordion;

            this.socialAccordion.destroy();
            delete this.socialAccordion;

            this.socialSupportAccordion.destroy();
            delete this.socialSupportAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            personalCareConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            spiritualNeedsConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            socialConfig: {
                value: {
                    title: {},
                    labels: {},
                    url: []
                }
            },
            socialSupportConfig: {
                value: {
                    title: {},
                    labels: {},
                    url: []
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeFormFullDetails',
               'usp-resolveassessment-IntakePersonalCareNeed',
               'usp-resolveassessment-IntakeTimetableEvent',
               'usp-resolveassessment-IntakeSupport',
               'categories-resolveassessment-component-Need',
               'categories-resolveassessment-component-Support'
              ]
});