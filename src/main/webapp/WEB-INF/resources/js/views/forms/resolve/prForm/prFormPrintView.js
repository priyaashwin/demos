YUI.add('prform-print', function(Y) {
    Y.namespace('app').PRFormPrintView = Y.Base.create('prFormPrintView', Y.View, [], {
        initializer: function(config) {
            this.printView = new Y.usp.resolveassessment.PlacementReportView({
                template: Y.Handlebars.templates['prFormPrintView'],
                model: config.model,
                labels: config.labels

            });
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.printView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this.printView.destroy();
            delete this.printView;
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'datatype-date',
               'view',
               'handlebars-helpers',
               'handlebars-prform-print-templates',
               'handlebars-resolveform-templates',
               'usp-resolveassessment-PlacementReport'
              ]
});