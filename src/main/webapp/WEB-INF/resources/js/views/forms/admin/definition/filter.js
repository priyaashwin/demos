YUI.add('form-definition-filter', function(Y) {
  "use strict";

  var DB = Y.usp.DataBindingUtil;

  Y.namespace('app.admin.form').FormDefinitionFilterModel = Y.Base.create('formDefinitionFilterModel', Y.usp.ResultsFilterModel, [], {}, {
    ATTRS: {
      status: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        //default status list defined here
        value: ['DRAFT', 'PUBLISHED']
      },
      securityDomain: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      name: {
        USPType: 'String',
        value: ''
      }
    }
  });

  Y.namespace('app.admin.form').FormDefinitionResultsFilter = Y.Base.create('formDefinitionResultsFilter', Y.usp.app.ResultsFilter, [], {
    template: Y.Handlebars.templates['formTypeResultsFilter'],
    initializer: function() {
      this.after('securityDomainsChange', this.renderSecurityDomains, this);
    },
    render: function() {
      //get the container 
      var instance = this,
        container = instance.get('container');

      //call into superclass to render
      Y.app.admin.form.FormDefinitionResultsFilter.superclass.render.call(this);
      //flag to track if rendered
      this.rendered = true;

      this.renderSecurityDomains();

      return this;
    },
    renderSecurityDomains: function() {
      var select, securityDomains, container = this.get('container');
      if (this.rendered) {
        select = container.one('#formTypeFilter_securityDomain');
        securityDomains = this.get('securityDomains') || [];

        Y.FUtil.setSelectOptions(select, securityDomains, null, null, false, true, 'code');

        //now we have out select box populated - we need to ensure that any default selection is made
        DB.setElementValue(select._node, this.get('model').get('securityDomain'));
      }
    }
  }, {
    ATTRS: {
      otherData: {
        value: {
          statusList: [{
            code: "DRAFT",
            name: "Draft"
          }, {
            code: "PUBLISHED",
            name: "Published"
          }, {
            code: "ARCHIVED",
            name: "Deleted"
          }]
        }
      }
    },
    securityDomains: {
      value: []
    }
  });

  Y.namespace('app.admin.form').FormDefinitionFilter = Y.Base.create('formDefinitionFilter', Y.usp.app.Filter, [], {
    filterModelType: Y.app.admin.form.FormDefinitionFilterModel,
    filterType: Y.app.admin.form.FormDefinitionResultsFilter,
    filterContextTemplate: Y.Handlebars.templates["formTypeActiveFilter"]
  });

}, '0.0.1', {
  requires: ['yui-base',
    'app-filter',
    'results-filter',
    'handlebars-helpers',
    'handlebars-form-templates',
    'data-binding',
    'form-util'
  ]
});