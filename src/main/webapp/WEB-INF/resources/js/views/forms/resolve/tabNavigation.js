YUI.add('tab-navigation', function(Y) {

    function TabNavigation() {}

    TabNavigation.prototype = {
        initializer: function() {
            //listen for navigation event within tabs
            this.on('*:navigateToSection', function(e) {
                var target = e.target,
                    id = e.id,
                    tContainer, mv, accordion = target[id];

                //ASSUME that the ID passed in the section matches the ID within the target object
                if (accordion) {
                    tContainer = accordion.get('container');
                    if (tContainer) {
                        mv = this.get('container').getX();

                        if (accordion.get('closed') !== 'undefined') {
                            //ASSUME an accordion
                            //open if necessary                            
                            accordion.set('closed', false);
                        }

                        tContainer.once('scrollIntoViewFinished', function() {
                            //focus on title
                            Y.later(5, this, function() {
                                var toggle = this.one('a.accordion-toggle');
                                if (toggle) {
                                    toggle.focus();
                                }
                            });
                        });

                        //scroll into view
                        tContainer.scrollIntoView({
                            anim: {
                                duration: 0.2
                            },
                            margin: {
                                top: mv
                            }
                        });
                    }
                }
            }, this);
        }
    };

    Y.namespace('app').TabNavigation = TabNavigation;
}, '0.0.1', {
    requires: ['yui-base',
               'gallery-scrollintoview',
               'anim']
});