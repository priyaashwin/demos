YUI.add('prform-health-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };
    
    Y.namespace('app').PRFormHealthView = Y.Base.create('prFormHealthView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormHealthView']
    });

    Y.namespace('app').PRFormHealthAccordion = Y.Base.create('prFormHealthAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormHealth View and configure the template
            this.prFormHealthView = new Y.app.PRFormHealthView(Y.merge(config, {}));

            // Add event targets
            this.prFormHealthView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormHealthViewContainer = this.prFormHealthView.render().get('container');

            //superclass call
            Y.app.PRFormHealthAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormHealthViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormHealthView.destroy();

            delete this.prFormHealthView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormMedicationView = Y.Base.create('prFormMedicationView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormMedicationView']
    });

    Y.namespace('app').PRFormMedicationAccordion = Y.Base.create('prFormMedicationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormMedication View and configure the template
            this.prFormMedicationView = new Y.app.PRFormMedicationView(Y.merge(config, {}));

            // Add event targets
            this.prFormMedicationView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormMedicationViewContainer = this.prFormMedicationView.render().get('container');

            //superclass call
            Y.app.PRFormMedicationAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormMedicationViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormMedicationView.destroy();

            delete this.prFormMedicationView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormGPView = Y.Base.create('prFormGPView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormGPView']
    });

    Y.namespace('app').PRFormGPAccordion = Y.Base.create('prFormGPAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormGP View and configure the template
            this.prFormGPView = new Y.app.PRFormGPView(Y.merge(config, {}));

            // Add event targets
            this.prFormGPView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormGPViewContainer = this.prFormGPView.render().get('container');

            //superclass call
            Y.app.PRFormGPAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormGPViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormGPView.destroy();

            delete this.prFormGPView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormHealthTab = Y.Base.create('prFormHealthTab', Y.View, [], {
        initializer: function() {
            var healthConfig = this.get('healthConfig'),
                medicationConfig = this.get('medicationConfig'),
                gpConfig = this.get('gpConfig');

            //create health view
            this.healthView = new Y.app.PRFormHealthAccordion(Y.merge(healthConfig, {
                model: this.get('model')
            }));
            this.medicationView = new Y.app.PRFormMedicationAccordion(Y.merge(medicationConfig, {
                model: this.get('model')
            }));
            this.gpView = new Y.app.PRFormGPAccordion(Y.merge(gpConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.healthView.addTarget(this);
            this.medicationView.addTarget(this);
            this.gpView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.healthView.render().get('container'));
            contentNode.append(this.medicationView.render().get('container'));
            contentNode.append(this.gpView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up health view
            this.healthView.destroy();
            delete this.healthView;

            //clean up medication view
            this.medicationView.destroy();
            delete this.medicationView;

            //clean up gp view
            this.gpView.destroy();
            delete this.gpView;
        }
    }, {
        ATTRS: {
            model: {},
            healthConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            medicationConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            gpConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-prform-templates',
               'usp-resolveassessment-PlacementReport'
              ]
});