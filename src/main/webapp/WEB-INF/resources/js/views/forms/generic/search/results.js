YUI.add('form-instance-results', function(Y) {
    'use-strict';

    var L = Y.Lang,
        E = Y.Escape,
        USPTemplates = Y.usp.ColumnTemplates,
        USPFormatters = Y.usp.ColumnFormatters,
        FormsFormatters = Y.app.form.ColumnFormatters,
        AttachmentFormatters = Y.app.attachment.ColumnFormatters;

    Y.namespace('app.form').PaginatedFilteredFormInstanceSearchResultList = Y.Base.create("paginatedFilteredFormInstanceSearchResultList", Y.usp.form.PaginatedFormInstanceWithAttachmentsCountList, [Y.usp.ResultsFilterURLMixin], {
        whiteList: ["status", "formDefinitionId", "mostRecentOnly", "startDateFrom", "startDateTo", 
                    "submittedDateFrom", "submittedDateTo", "completionDateFrom", "completionDateTo",
                    'hasAttachments', 'attachmentSource', 'attachmentOtherSource',
                    'attachmentSourceOrganisation', 'attachmentOtherSourceOrganisation', 
                    'attachmentTitle'],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app.form').FormInstanceResults = Y.Base.create('formInstanceResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            var subject = config.subject || {};
            return new Y.app.form.PaginatedFilteredFormInstanceSearchResultList({
                url: L.sub(config.url, {
                    subjectId: subject.subjectId,
                    subjectType: (subject.subjectType || '').toUpperCase()
                }),
                //Required access level is READ_SUMMARY as user needs to be able to read form
                requestedAccess: 'READ_SUMMARY',
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions,
                subject = config.subject || {};
            return ([{
                key: 'subjectType',
                label: ' ',
                width: '4%',
                readingView: '4%',
                sortable: false,
                allowHTML: true,
                formatter: FormsFormatters.formSubjectIcon
            }, {
                key: 'formDefinitionName',
                label: labels.type,
                sortable: true,
                width: '17%',
                readingView: '20%',
                formatter: FormsFormatters.formName,
                cellTemplate: USPTemplates.clickable(labels.viewForm, 'view', permissions.canView)
            }, {
                key: 'formIdentifier',
                label: labels.formId,
                sortable: true,
                width: '9%',
                readingView: '20%'
            }, {
                key: 'activityDate',
                label: labels.activityDate,
                sortable: true,
                width: '9%',
                readingView: '19%',
                formatter: USPFormatters.date
            }, {
                key: 'dateStarted',
                label: labels.startDate,
                sortable: true,
                width: '9%',
                formatter: USPFormatters.date
            }, {
                key: 'dateSubmitted',
                label: labels.submittedDate,
                sortable: true,
                width: '9%',
                formatter: USPFormatters.date,
            }, {
                key: 'submitterName',
                label: labels.submittedBy,
                width: '8%',
                readingView: '19%',
                formatter: FormsFormatters.nameFormatter
            }, {
                key: 'dateCompleted',
                label: labels.completedDate,
                sortable: true,
                width: '9%',
                formatter: USPFormatters.date,
            }, {
                key: 'completerName',
                label: labels.completedBy,
                width: '8%',
                formatter: FormsFormatters.nameFormatter
            }, {
                key: "attachmentsCount",
                label: labels.attachmentCount,
                width: '4%',
                readingView: '4%',
                allowHTML: true,
                formatter: AttachmentFormatters.attachment
            }, {
                key: 'status',
                label: labels.state,
                width: '4%',
                readingView: '4%',
                sortable: true,
                allowHTML: true,
                formatter: FormsFormatters.formState
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                readingView: '10%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'view',
                    title: labels.viewFormTitle,
                    label: labels.viewForm,
                    enabled: permissions.canView
                }, {
                    clazz: 'submit',
                    title: labels.submitFormTitle,
                    label: labels.submitForm,
                    visible: function() {
                        if (permissions.canSubmit === true && this.record.hasAccessLevel('WRITE')===true) {
                            return (this.data.requireAuthorisation && this.data.status === 'DRAFT');
                        }
                        return false;
                    }
                }, {
                    clazz: 'withdraw',
                    title: labels.withdrawFormTitle,
                    label: labels.withdrawForm,
                    visible: function() {
                        if (permissions.canWithdraw === true && this.record.hasAccessLevel('WRITE')===true) {
                            return (this.data.requireAuthorisation && this.data.status === 'SUBMITTED');
                        }
                        return false;
                    }
                }, {
                    clazz: 'complete',
                    title: labels.completeFormTitle,
                    label: labels.completeForm,
                    visible: function() {
                        if (permissions.canComplete === true && this.record.hasAccessLevel('WRITE')===true) {
                            return (!this.data.requireAuthorisation && this.data.status === 'DRAFT');
                        }
                        return false;
                    }
                }, {
                    clazz: 'uncomplete',
                    title: labels.uncompleteFormTitle,
                    label: labels.uncompleteForm,
                    visible: function() {
                        if (permissions.canUncomplete === true && this.record.hasAccessLevel('WRITE')===true) {
                            return (!this.data.requireAuthorisation && this.data.status === 'COMPLETE');
                        }
                        return false;
                    }
                }, {
                    clazz: 'reject',
                    title: labels.rejectFormTitle,
                    label: labels.rejectForm,
                    visible: function() {
                        if (permissions.canReject === true && this.record.hasAccessLevel('WRITE')===true) {
                            return (this.data.requireAuthorisation && this.data.status === 'SUBMITTED');
                        }
                        return false;
                    }
                }, {
                    clazz: 'authorise',
                    title: labels.approveFormTitle,
                    label: labels.approveForm,
                    visible: function() {
                        if (permissions.canAuthorise === true && this.record.hasAccessLevel('WRITE')===true) {
                            return (this.data.requireAuthorisation && this.data.status === 'SUBMITTED');
                        }
                        return false;
                    }
                }, {
                    clazz: 'softDelete',
                    title: labels.deleteFormTitle,
                    label: labels.deleteForm,
                    visible: function() {
                        if (permissions.canSoftDelete === true && this.record.hasAccessLevel('WRITE')===true) {
                            return (this.data.status !== 'ARCHIVED');
                        }
                        return false;
                    }
                }, {
                    clazz: 'hardDelete',
                    title: labels.removeFormTitle,
                    label: labels.removeForm,
                    visible: function() {
                        return permissions.canHardDelete && this.record.hasAccessLevel('WRITE')===true;
                    }
                }, {
                    clazz: 'uploadAttachment',
                    label: labels.uploadFile,
                    title: labels.uploadFileTitle,
                    visible: function() {
                        // Status must be DRAFT or COMPLETE if user has canAddCompleteAttachment
                      if(this.record.hasAccessLevel('WRITE')===true && permissions.canUploadAttachment){
                        return (this.data.status == "DRAFT" || (this.data.status == "COMPLETE" && permissions.canUploadAttachmentOnCompleteInstance));
                      }
                      
                      return false;
                    }
                }, {
                    clazz: 'output',
                    title: labels.outputFormTitle,
                    label: labels.outputForm,
                    visible: function() {
                        var form = this.record;
                        //must have permission to output
                        if (permissions.canOutput === true  && this.record.hasAccessLevel('READ_DETAIL')===true) {
                            //TODO - could we check for output templates at this point?
                            return (form.get('subjectType') === "PERSON") || (form.get('subjectType') === "GROUP" && subject && subject.subjectType === 'person');
                        }
                        return false;
                    }
                }, {
                    clazz: 'reviewHistory',
                    title: labels.reviewHistoryTitle,
                    label: labels.reviewHistory,
                    enabled: function() {
                        if (permissions.canView === true) {
                            return (this.data.hasRejections || ((this.data.dateSubmitted !== null) && (this.data.dateCompleted !== null)));
                        }
                        return false;
                    },
                    visible: function() {
                        if (permissions.canView === true && this.record.hasAccessLevel('READ_DETAIL')===true) {
                            return (this.data.requireAuthorisation);
                        }
                        return false;
                    }
                }]
            }]);
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    }, {
                        fn: Y.Plugin.app.ViewModePlugin
                    }];
                }
            },
            readingView: {
                getter: function() {
                    return this.get('results').viewModePlugin.get('mode');
                },
                setter: function(v) {
                    this.get('results').viewModePlugin.set('mode', v);
                }
            }
        }
    });

    Y.namespace('app.form').FormInstanceFilteredResults = Y.Base.create('formInstanceFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.form.FormInstanceFilter,
        resultsType: Y.app.form.FormInstanceResults
    });
}, '0.0.1', {
    requires: ['yui-base',
        'escape',
        'app-filtered-results',
        'results-filter',
        'results-formatters',
        'results-templates',
        'results-table-search-state-plugin',
        'usp-form-FormInstanceWithAttachmentsCount',
        'form-results-formatters',
        'attachment-results-formatters',
        'form-instance-filter',
        'app-view-mode-plugin'
    ]
});