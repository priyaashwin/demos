YUI.add('intakeform-speech-tab', function(Y) {
    var L = Y.Lang;

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormCommunicationIssuesView = Y.Base.create('intakeFormCommunicationIssuesView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormCommunicationIssuesView']
    });

    Y.namespace('app').IntakeFormEatingView = Y.Base.create('intakeFormEatingView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormEatingView']
    });

    Y.namespace('app').IntakeFormCommunicationIssuesAccordion = Y.Base.create('intakeFormCommunicationIssuesAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormCommunicationIssuesView = new Y.app.IntakeFormCommunicationIssuesView(config);

            // Add event targets
            this.intakeFormCommunicationIssuesView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormCommunicationIssuesAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormCommunicationIssuesView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormCommunicationIssuesView.destroy();

            delete this.intakeFormCommunicationIssuesView;
        }
    });

    Y.namespace('app').IntakeFormEatingAccordion = Y.Base.create('intakeFormEatingAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormEatingView = new Y.app.IntakeFormEatingView(config);

            // Add event targets
            this.intakeFormEatingView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormEatingAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormEatingView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormEatingView.destroy();

            delete this.intakeFormEatingView;
        }
    });

    Y.namespace('app').IntakeFormSpeechTab = Y.Base.create('intakeFormSpeechTab', Y.View, [], {
        initializer: function() {
            var therapyConfig = this.get('therapyConfig'),
                issuesConfig = this.get('issuesConfig'),
                eatingConfig = this.get('eatingConfig');

            //create the accordion views
            this.therapyAccordion = new Y.app.IntakeFormReferencedPersonAccordion(
                Y.merge(
                    therapyConfig, {
                        model: new Y.usp.resolveassessment.IntakeReferencedPerson({
                            url: L.sub(therapyConfig.url, {
                                id: this.get('model').get('id')
                            })
                        }).load(),
                        formModel: this.get('model')
                    }));

            this.issuesAccordion = new Y.app.IntakeFormCommunicationIssuesAccordion(
                Y.merge(
                    issuesConfig, {
                        model: this.get('model')
                    }));
            this.eatingAccordion = new Y.app.IntakeFormEatingAccordion(
                Y.merge(
                    eatingConfig, {
                        model: this.get('model')
                    }));

            // Add event targets
            this.therapyAccordion.addTarget(this);
            this.issuesAccordion.addTarget(this);
            this.eatingAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.therapyAccordion.render().get('container'));
            contentNode.append(this.issuesAccordion.render().get('container'));
            contentNode.append(this.eatingAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties
            this.therapyAccordion.destroy();
            delete this.therapyAccordion;

            this.issuesAccordion.destroy();
            delete this.issuesAccordion;

            this.eatingAccordion.destroy();
            delete this.eatingAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            therapyConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            issuesConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            eatingConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'intakeform-referenced-person-view',
               'usp-resolveassessment-IntakeFormFullDetails'
              ]
});