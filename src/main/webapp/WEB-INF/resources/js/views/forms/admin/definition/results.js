YUI.add('form-definition-results', function(Y) {
  "use strict";

  var USPTemplates = Y.usp.ColumnTemplates,
    USPFormatters = Y.usp.ColumnFormatters,
    FormsFormatters = Y.app.form.ColumnFormatters;

  Y.namespace('app.admin.form').PaginatedFilteredFormDefinitionResultList = Y.Base.create("paginatedFilteredFormDefinitionResultList", Y.usp.form.PaginatedFormDefinitionWithInstanceCountList, [
    Y.usp.ResultsFilterURLMixin
  ], {
    whiteList: ["name", "status", "securityDomain"],
    staticData: {
      useSoundex: false,
      appendWildcard: true
    }
  }, {
    ATTRS: {
      filterModel: {
        writeOnce: 'initOnly'
      }
    }
  });

  Y.namespace('app.admin.form').FormDefinitionResults = Y.Base.create('formDefinitionResults', Y.usp.app.Results, [], {
    securityDomainFormatter: function(o) {
      var val = '';
      var securityDomains = this.get('securityDomains');
      var ce = securityDomains[o.value];
      if (ce) {
        val = ce.name;
      }
      return val;
    },
    getResultsListModel: function(config) {
      return new Y.app.admin.form.PaginatedFilteredFormDefinitionResultList({
        url: config.url,
        //plug in the filter model
        filterModel: config.filterModel
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions

      return ([{
        key: 'name',
        label: labels.name,
        sortable: true,
        width: '32%',
        cellTemplate: USPTemplates.clickable(labels.viewFormType, 'view', permissions.canView)
      }, {
        key: 'securityDomain',
        label: labels.securityDomain,
        //NOTE - we can't use the standard CodedEntry formatter
        //here as the security domains are loaded async and
        //will not be available until the results list renders
        formatter: this.securityDomainFormatter.bind(this),
        width: '24%'
      }, {
        key: 'version',
        label: labels.version,
        width: '10%',
        sortable: true
      }, {
        key: 'status',
        label: labels.state,
        width: '10%',
        sortable: true,
        allowHTML: true,
        formatter: FormsFormatters.formTypeState
      }, {
        key: 'hasMappings',
        label: labels.mappings,
        width: '8%',
        sortable: false,
        formatter: USPFormatters.yesNo
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '10%',
        formatter: USPFormatters.actions,
        items: [{
          clazz: 'view',
          title: labels.viewFormTypeTitle,
          label: labels.viewFormType,
          enabled: true
        }, {
          clazz: 'edit',
          title: labels.editFormTypeTitle,
          label: labels.editFormType,
          enabled: function() {
            if (permissions.canEdit === true) {
              return (this.record.get('status') === 'DRAFT');
            }
            return false;
          }
        }, {
          clazz: 'editDesign',
          title: labels.editFormDefinitionTitle,
          label: labels.editFormDefinition,
          enabled: permissions.canEdit
        }, {
          clazz: 'viewMappings',
          title: labels.viewMappingsTitle,
          label: labels.viewMappings,
          enabled: function() {
            return permissions.canViewMappings;
          }
        }, {
          clazz: 'publish',
          title: labels.publishFormTypeTitle,
          label: labels.publishFormType,
          enabled: function() {
            if (permissions.canPublish === true) {
              return (this.record.get('status') === 'DRAFT');
            }
            return false;
          }
        }, {
          clazz: 'archive',
          title: labels.deleteFormTypeTitle,
          label: labels.deleteFormType,
          enabled: function() {
            if (permissions.canArchive === true) {
              return (this.record.get('status') === 'PUBLISHED');
            }
            return false;
          }
        }, {
          clazz: 'remove',
          title: labels.removeFormTypeTitle,
          label: labels.removeFormType,
          enabled: function() {
            if (permissions.canRemove === true) {
              return (this.record.get('status') === 'DRAFT' || (this.record.get('status') === 'ARCHIVED' && this.record.get('instanceCount') === 0));
            }
            return false;
          }
        }]
      }]);
    }
  }, {
    ATTRS: {
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
          }];
        }
      },
      securityDomains: {
        setter: function(val) {
          var securityDomains = {};

          (val || []).forEach(function(sd) {
            if (sd.code) {
              securityDomains[sd.code] = sd;
            }
          });

          return securityDomains;
        }
      }
    }
  });

  Y.namespace('app.admin.form').FormDefinitionFilteredResults = Y.Base.create('formDefinitionFilteredResults', Y.usp.app.FilteredResults, [], {
    filterType: Y.app.admin.form.FormDefinitionFilter,
    resultsType: Y.app.admin.form.FormDefinitionResults
  });

}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'results-formatters',
    'results-templates',
    'form-definition-filter',
    'app-filtered-results',
    'app-filter',
    'app-results',
    'results-table-search-state-plugin',
    'usp-form-FormDefinitionWithInstanceCount',
    'form-results-formatters'
  ]
});