YUI.add('form-instance-add-view', function(Y) {
  'use-strict';

  /**
   * A view for displaying the form instance add screen and the associated filter
   */
  Y.namespace('app.form').FormInstanceAddView = Y.Base.create('formInstanceAddView', Y.View, [], {
    initializer: function(config) {
      var subject = config.subject,
          addConfig=config.addConfig||{};

      //setup config object for the view
      var viewConfig = Y.mix({
        subject: subject
      }, addConfig);

      //construct an instance of the results view
      this.formInstanceAddResults = new Y.app.form.FormInstanceAddControllerView(viewConfig).addTarget(this);
    },
    render: function() {
      var container = this.get('container');

      container.setHTML(this.formInstanceAddResults.render().get('container'));
      return this;
    },
    destructor: function() {
      this.formInstanceAddResults.removeTarget(this);
      this.formInstanceAddResults.destroy();
      delete this.formInstanceAddResults;
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'form-instance-add-controller'
  ]
});