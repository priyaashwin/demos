YUI.add('intakeform-print', function(Y) {
    Y.namespace('app').IntakeFormPrintView = Y.Base.create('intakeformPrintView', Y.View, [], {
        initializer: function(config) {
            this.printView = new Y.usp.resolveassessment.IntakeFormFullDetailsExView({
                template: Y.Handlebars.templates['intakeFormPrintView'],
                model: config.model,
                labels: config.labels,
                otherData: config.otherData,
                codedEntries: config.codedEntries
            });
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.printView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this.printView.destroy();
            delete this.printView;
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'datatype-date',
               'view',
               'handlebars-helpers',
               'handlebars-intakeform-print-templates',
               'handlebars-resolveform-templates',
               'usp-resolveassessment-IntakeFormFullDetailsEx',
               'resolveassessment-component-enumerations',
               'categories-resolveassessment-component-Need'
              ]
});