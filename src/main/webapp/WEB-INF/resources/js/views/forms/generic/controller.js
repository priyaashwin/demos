YUI.add('form-controller', function(Y) {
    'use-strict';
    var Micro = Y.Template.Micro,
        MicroSyntax = {
        code: /\{\{%([\s\S]+?)%\}\}/g,
        escapedOutput: /\{\{(?!%)([\s\S]+?)\}\}/g,
        rawOutput: /\{\{\{([\s\S]+?)\}\}\}/g
      };
    
    /**
     * A Controller view that extends Y.App and manages 2 view instances
     * These are `results` the standard results view
     * and `add` the add form view
     */
    Y.namespace('app.form').FormControllerView = Y.Base.create('formControllerView', Y.App, [], {
        initializer: function(config) {
            var permissions = config.permissions || {};

            //setup a new toolbar instance - use this to look for specific events
            //i.e. don't use the bubbled events from toolbars created in the view
            //instances
            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions
            }).addTarget(this);

            //listener for link from checklist
            this.checklistLink = new Y.app.ChecklistLinkFrom({
                type: 'FORM'
            });

            this._controllerEvtHandlers = [
                this.toolbar.on('appToolbar:add', this.showAddView, this),
                this.on('*:viewFormResults', this.showResultsView, this),
                this.checklistLink.on('checklistLinkFrom:linkFrom', this._handleLinkFromChecklist, this),
                this.after('navigate', this._resetScroll, this)
            ];
        },
        destructor: function() {
            this.toolbar.destroy();
            delete this.toolbar;

            this._controllerEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._controllerEvtHandlers;

            if (this._messageNode) {
                this._messageNode.destroy();

                delete this._messageNode;
            }

            if (this.checklistLink) {
                this.checklistLink.destroy();

                delete this.checklistLink;
            }
        },
        _resetScroll:function(){
          Y.one('html').removeClass('noScroll');
        },        
        render: function() {
            var container = this.get('container');
            this._messageNode = Y.Node.create('<div></div>');

            Y.app.form.FormControllerView.superclass.render.call(this);

            //insert message node before
            container.insert(this._messageNode, 'before');

            //check if we linked from a checklist
            this.checklistLink.checkForLinkFromChecklist();

            return this;
        },
        views: {
            results: {
                type: Y.app.form.FormInstanceResultsView
            },
            add: {
                type: Y.app.form.FormInstanceAddView
            }
        },
        showAddView: function() {
            this.navigate(this.get('addRoute'));
        },
        showResultsView: function() {
            this.navigate(this.get('viewRoute'));
        },
        handleRouteResults: function() {
            var subject = this.get('subject'),
                resultsConfig = this.get('resultsConfig'),
                activeView = this.get('activeView');

            var activeView = this.get('activeView');
            if (!activeView || activeView.name !== 'formInstanceResultsView') {
              //set title
              Y.one('h3 .context-title').set('text', resultsConfig.title || '');
  
              //set style on first button
              this.toolbar.get('toolbarNode').one('li').removeClass('last');
  
              //setup buttons - show add button
              this.toolbar.showButton('add');
  
              this.showView('results', {
                  resultsConfig: resultsConfig,
                  subject: subject
              });
            }
        },
        handleRouteAdd: function() {
            var subject = this.get('subject'),
                addConfig = this.get('addConfig'),
                activeView = this.get('activeView');
            
            var activeView = this.get('activeView');
            if (!activeView || activeView.name !== 'formInstanceAddView') {
              //set title
              Y.one('h3 .context-title').set('text', addConfig.title || '');
              //set style on first button
              this.toolbar.get('toolbarNode').one('li').addClass('last');
  
              //setup buttons - hide add button
              this.toolbar.hideButton('add');
  
              this.showView('add', {
                  addConfig: addConfig,
                  subject: subject
              });
            }
        },
        /**
         * Because the router fires for all changes to the HTML5 history object
         * we only route if the history state is not replace.
         * 
         * `replace` is triggered by the ResultsSearchStatePlugin so fires
         * false positive routing
         */
        _afterHistoryChange: function(e) {
            if (e.src === 'replace') {
                return;
            }

            Y.app.form.FormControllerView.superclass._afterHistoryChange.apply(this, arguments);
        },
        _handleLinkFromChecklist: function(e) {
            var id = e.targetEntityId,
                action = e.action,
                taskState = e.taskState,
                parameters = e.parameters || {},
                config = this.get('linkFromConfig'),
                permissions = config.permissions,
                labels = config.labels || {},
                subject = this.get('subject'),
                checklistInstanceId = e.checklistInstanceId;

            var instanceURL = config.formInstanceURL;

            var formTypeParameters = parameters['FORM_TYPE'] || {};
            var formDefinition = formTypeParameters.value;

            var proceedWithLink = new Promise(function(resolve, reject) {
                var activeView = this.get('activeView');
                if (null === activeView) {
                    this.once('activeViewChange', function(e) {
                        resolve(e.newVal);
                    }, this);
                } else {
                    resolve(activeView);
                }
            }.bind(this));

            proceedWithLink.then(function(view) {
                var form;
                if (action === 'VIEW') {
                    //we should view the form
                    if (id !== undefined) {
                        //we should show a message about the form
                        //attempt to load the form instance via REST
                        form = new Y.usp.form.FormInstance({
                            url: instanceURL,
                            id: id
                        }).load(function(err) {
                            if (!err) {
                                view.formInstanceResults.fire('viewForm', {
                                    record: form
                                });
                            }
                        }.bind(this));
                    }
                } else if (action === 'ADD' && permissions.canAdd && formDefinition) {
                  var formSubTypeParameter=parameters['FORM_SUB_TYPE'];
                  
                    view.formInstanceAddResults.fire('addForm', {
                        checklistInstanceId: checklistInstanceId,
                        formDefinition: formDefinition,
                        //if the sub-type is specified - then use it
                        subType: formSubTypeParameter?formSubTypeParameter.value:null,
                        subject: subject
                    });
                }
                if (taskState === 'ACTIVE') {
                    //show a message
                    new Y.usp.DismissibleMessage({
                        message: Micro.render(labels.checklistLinkFromTemplate, {
                          status: (parameters['FORM_STATE'] || {}).displayValue,
                          formType: (parameters['FORM_TYPE'] || {}).displayValue,
                          formSubType: (parameters['FORM_SUB_TYPE'] || {}).displayValue
                        },MicroSyntax)
                    }).render(this._messageNode);
                }
            }.bind(this));
        }
    }, {
        serverRouting: true,
        ATTRS: {
            routes: {
                value: [{
                        path: '/view',
                        callbacks: 'handleRouteResults'
                    },
                    {
                        path: '/add',
                        callbacks: 'handleRouteAdd'
                    }
                ]
            },
            subject: {
                value: {}
            },
            resultsConfig: {
                value: {}
            },
            addConfig: {
                value: {}
            },
            addRoute: {
                value: '#none'
            },
            viewRoute: {
                value: '#none'
            },
            permissions: {
                value: {}
            },
            linkFromConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-base',
        'app-toolbar',
        'form-instance-results-view',
        'form-instance-add-view',
        'checklist-link-from',
        'dismissible-message',
        'usp-form-FormInstance',
        'template-micro'
    ]
});