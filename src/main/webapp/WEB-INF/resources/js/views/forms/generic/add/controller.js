YUI.add('form-instance-add-controller', function (Y) {
    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.form').FormInstanceAddControllerView = Y.Base.create('formInstanceAddControllerView', Y.View, [], {
        initializer: function (config) {
            //clone errors config - as it is manipulated by the plugin
            var errorsConfig = Y.mix({}, config.errorsConfig || {});

            var resultsConfig = {
                searchConfig: Y.merge(config.searchConfig, {
                    //mix the subject into the search config so it can be used in URL generation
                    subject: config.subject
                }),
                filterConfig: Y.merge(config.filterConfig, {
                    //mix the subject into the filter config so it can be
                    //used in the URL to load definitions in the filter
                    permissions: config.permissions,
                    subject: config.subject
                }),
                filterContextConfig: config.filterContextConfig,
                quickFiltersConfig: config.quickFiltersConfig,
                permissions: config.permissions
            };

            this.results = new Y.app.form.AddFormFilteredResults(resultsConfig).addTarget(this);

            this.dialog = new Y.app.form.AddFormInstanceDialog(config.dialogConfig).addTarget(this).render();

            //plug in the errors handling - and configure the appropriate messages
            //using supplied errors config object
            this.plug(Y.Plugin.usp.ModelErrorsPlugin, errorsConfig);

            //Setup an handler for promise failures
            this.handlePromiseError = Y.bind(function (error) {
                //On an error - close the dialog and allow the error to be picked up by the main page view
                this.dialog.hide();
                this.fire('error', {
                    error: error
                });
            }, this);

            this._formInstanceEvtHandlers = [
                this.on('*:add', this.handleAddFormInstance, this),
                this.dialog.on('*:error', this._handleDialogError, this),
                this.on('showExistingInstanceView:view', function (e) {
                    //launch the form - no message
                    this._handleLaunchForm(e.id, e.formName);
                }, this),
                //subscribe to add multiple success event
                this.on('addMultipleFormInstanceView:success', function (e) {
                    this._handleAddFormInstance(e.model, e.subject, e.formType, e.linkTarget);
                }, this),
                //subscribe to add where instance exists for group success event
                this.on('addWhereInstanceExistsForGroupView:success', function (e) {
                    this._handleAddFormInstance(e.model, e.subject, e.formType, e.linkTarget);
                }, this),
                //subscribe to mapping selection event
                this.on('addFormSelectMappingView:success', function (e) {
                    this._handleCheckSubTypBeforeCreateFormInstance(e.model, e.subject, e.formType, e.linkTarget);
                }, this),
                //subscribe to subType selection event
                this.on('addFormSelectSubTypeView:success', function (e) {
                    //call into create function
                    this._handleCreateFormInstance(e.model, e.subject, e.linkTarget);
                }, this),
                /* checklist link from */
                this.on('addForm', this.handleLinkFromAddFormInstance, this)
            ];
        },
        destructor: function () {
            this._formInstanceEvtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._formInstanceEvtHandlers;

            //remove ModelErrors plugin
            this.unplug(Y.Plugin.usp.ModelErrorsPlugin);

            this.dialog.removeTarget(this);
            this.dialog.destroy();
            delete this.dialog;

            this.results.removeTarget(this);
            this.results.destroy();
            delete this.results;
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.results.render().get('container'));

            this.get('container').setHTML(contentNode);

            return this;
        },
        _handleDialogError: function (e) {
            //check target for error
            var target = e.target;
            //if the target is from our Dialog based Model then don't let it propagate
            if (target.name === 'newFormInstanceModel') {
                e.stopPropagation();
            }
            //otherwise - let the error flow up to the error handler on this view
        },
        handleAddFormInstance: function (e) {
            var formType = e.record,
                subject = this.get('subject');

            //clear any existing errors shown - we call into the errors plugin
            this.modelErrors.clearErrors();

            this._handleAddForm(subject, formType);
        },
        _handleAddForm: function (subject, formType, subType, linkTarget) {
            var formName = formType.get('name'),            
                formInstances,
                groupScopeInstances,
                applicableFormMappings,
                labels = this.get('labels'),
                model = new Y.app.NewFormInstanceModel({
                    subjectId: subject.subjectId,
                    formDefinitionId: formType.get('id'),
                    name: formName,
                }),
                permissions = this.get('permissions') || {};

            //A form can only be added if the definition is in the PUBLISHED state
            if (formType.get('status') !== 'PUBLISHED') {
                //hide any existing dialog display
                this.dialog.hide();
                //show an error
                this.fire('error', {
                    error: {
                        message: labels.formDefinitionStateInvalid || 'Invalid form type'
                    }
                });
                //abort
                return;
            }
            this.dialog.showView('addFormLoading');

            if (subType) {
                //if a sub-type has been specified - find it on the form definition
                //and set the ID into the model
                var subTypeDefn = (formType.get('subTypes') || []).find(function (stype) {
                    return stype.name === subType;
                });
                if (subTypeDefn) {
                    model.set('subTypeId', subTypeDefn.id, { silent: true });
                }
            }

            //resolve our promise to get existing form instances and any applicable form mappings
            Y.Promise.all([this._getExistingFormInstances(subject, formType.get('id')),
                    this._getApplicableFormMapppings(subject, formType.get('id'))
                ])
                .then(function (data) {
                        formInstances = data[0];
                        applicableFormMappings = data[1];

                        //augment the formType model with our applicableFormMappings - this will then be used in the mappings dialog
                        //WE SET SILENT BECAUSE WE DON'T WANT THE RESULTS LIST TO SEE THE CHANGE AND RE-RENDER
                        formType.set('destinationFormMappings', applicableFormMappings.toJSON(), {
                            silent: true
                        });

                        if (formInstances.size() > 0) {
                            if (formType.get('cardinality') === 'UNIQUE_PER_SUBJECT') {
                                groupScopeInstances = formInstances.filter(function (formInstance) {
                                    return formInstance.get('subjectType') === 'GROUP';
                                });

                                if (subject.subjectType !== 'group' && groupScopeInstances.length === formInstances.size()) {
                                    // Warn the user that one already exists for one or more of this subject's groups. 
                                    // So show dialog asking whether want to proceed with adding a form for the individual 
                                    this.dialog.showView('addWhereInstanceExistsForGroup', {
                                            model: model,
                                            otherData: {
                                                linkTarget: linkTarget,
                                                subject: subject,
                                                formName: formName,
                                                formType: formType,
                                                groupScopeInstances: JSON.parse(JSON.stringify(groupScopeInstances))
                                            }
                                        },
                                        function () {
                                            //Get the button by name out of the footer and enable it.
                                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                                        });
                                } else {
                                    // Cannot create new form of this type, because one already exists. 
                                    // So show dialog asking whether want to view form instead 
                                    var isRestricted;

                                    if (formType.get('preferAutomatedAdd') === true && permissions.canViewAutomatedAdd !== true) {
                                        //if the form is of restricted display and the user does not have permission
                                        //to view restricted - then include pass an additional flag to the view to
                                        //control rendering of additional info message
                                        isRestricted=true;
                                    }
                                    this.dialog.showView('showExistingInstance', {
                                            model: model,
                                            otherData: {
                                                subject: subject,
                                                formName: formName,
                                                formType: formType,
                                                existingFormInstances: formInstances
                                            },
                                            isRestricted:isRestricted
                                        },
                                        function () {
                                            //Get the button by name out of the footer and enable it.
                                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                                        });
                                }
                            } else {
                                // Show dialog asking whether want to add another instance 
                                this.dialog.showView('addMultipleInstance', {
                                        model: model,
                                        otherData: {
                                            linkTarget: linkTarget,
                                            subject: subject,
                                            formType: formType,
                                            formName: formName
                                        }
                                    },
                                    function () {
                                        //Get the button by name out of the footer and enable it.
                                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                                    });
                            }
                        } else {
                            //Proceed with add instance
                            this._handleAddFormInstance(model, subject, formType, linkTarget);
                        }
                    }.bind(this),
                    this.handlePromiseError
                );
        },

        _getExistingFormInstances: function (subject, formTypeId) {
            var model = new Y.usp.form.PaginatedFormInstanceList({
                url: L.sub(this.get('existingFormInstanceTypesURL'), {
                    subjectId: subject.subjectId,
                    subjectType: subject.subjectType.toUpperCase(),
                    formTypeId: formTypeId,
                    sortBy: encodeURI('[{"dateStarted":"desc"}]')
                })
            });
            return new Y.Promise(function (resolve, reject) {
                var data = model.load(function (err) {
                    if (err !== null) {
                        //failed to get the existing form instances for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },
        _getApplicableFormMapppings: function (subject, formTypeId) {
            var model = new Y.usp.form.FormMappingList({
                url: L.sub(this.get('applicableFormMappingsURL'), {
                    id: formTypeId,
                    subjectId: subject.subjectId,
                    subjectType: subject.subjectType.toUpperCase()
                })
            });
            return new Y.Promise(function (resolve, reject) {
                var data = model.load(function (err) {
                    if (err !== null) {
                        //failed to get the existing form instances for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },
        _handleAddFormInstance: function (model, subject, formType, linkTarget) {
            if (formType.get('destinationFormMappings').length === 0) {
                //no form mapping id
                model.set('formMappingId', null);

                //perform the create
                this._handleCheckSubTypBeforeCreateFormInstance(model, subject, formType, linkTarget);
            } else if (formType.get('destinationFormMappings').length === 1) {
                // There is only one destination form mapping available so use it
                model.set('formMappingId', formType.get('destinationFormMappings')[0].id);
                //perform the create
                this._handleCheckSubTypBeforeCreateFormInstance(model, subject, formType, linkTarget);
            } else if (formType.get('destinationFormMappings').length > 1) {
                // More than one destination form mapping available so will need a dialog with the user to choose one
                this._handleSelectMapping(model, subject, formType, linkTarget);
            }
        },
        /**
         * Prompts for mappings and duplicated checks should have all taken
         * place before this method is called.
         * 
         * This method checks for sub-types on the form definition and prompts
         * the user to optionally select a sub-type for this form instance.
         */
        _handleCheckSubTypBeforeCreateFormInstance: function (model, subject, formType, linkTarget) {
            if (formType.get('subTypes').length > 0 && !model.get('subTypeId')) {
                //user can optionally specify sub-type
                this._handleSelectSubType(model, subject, formType, linkTarget);
            } else {
                //there are no sub-types so proceed with the standard form creation
                this._handleCreateFormInstance(model, subject, linkTarget);
            }
        },
        _handleCreateFormInstance: function (model, subject, linkTarget) {
            var errorsConfig = this.get('errorsConfig') || {},
                formInstance;

            this.dialog.showView('addFormLoading');

            if (linkTarget && linkTarget.type === 'CHECKLIST' && linkTarget.id) {
                //create a new form model to establish a link to the checklist instance
                formInstance = new Y.usp.formchecklist.NewFormInstanceWithChecklistResource({
                    url: L.sub(this.get('addFormInstanceURL'), {
                        id: subject.subjectId,
                        subjectType: subject.subjectType
                    }),
                    //subject type is required
                    subjectType: (subject.subjectType || '').toUpperCase(),
                    checklistInstanceId: linkTarget.id
                });
            } else {
                //create a new form model instance based on the passed in model
                formInstance = new Y.usp.form.NewFormInstance({
                    url: L.sub(this.get('addFormInstanceURL'), {
                        id: subject.subjectId,
                        subjectType: subject.subjectType
                    })
                });
            }
            //set the data
            formInstance.setAttrs(model.toJSON());

            // Do the save
            formInstance.save({}, Y.bind(function (err, response) {
                if (err === null) {
                    //launch the form
                    this._handleLaunchForm(formInstance.get('id'), formInstance.get('name'), true);
                } else {
                    var activeView = this.dialog.get('activeView');
                    if (activeView && (activeView.name === 'processingView')) {
                        //don't leave the user with a hung loading dialog - close it before we fire the error
                        this.dialog.hide();
                    }
                    switch (err.code) {
                    case 401:
                        {
                            this.fire('error', {
                                error: {
                                    message: errorsConfig.accessDenied.message
                                }
                            });
                            break;
                        }
                    default:
                        {
                            this.fire('error', {
                                error: err,
                                response: response
                            });
                        }
                    }
                }
            }, this));
        },
        _handleLaunchForm: function (id, formName, showMessage) {
            var labels = this.get('labels');

            //fire success message
            if (showMessage) {
                Y.fire('infoMessage:message', {
                    message: labels.successMessage
                });
            }

            // navigate to the view for the new form.
            Y.app.FormLauncher.open(this.get('viewFormURL'), id, formName);

            //return to results page
            this.fire('viewFormResults');
        },
        _handleSelectMapping: function (model, subject, formType, linkTarget) {
            var formName = formType.get('name');

            //show dialog so user can select mapping
            this.dialog.showView('addFormSelectMapping', {
                    model: model,
                    otherData: {
                        subject: subject,
                        formName: formName,
                        //pass in the form type model
                        formType: formType,
                        linkTarget: linkTarget
                    }
                },
                function () {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
        },
        _handleSelectSubType: function (model, subject, formType, linkTarget) {
            var formName = formType.get('name');

            //show dialog so user can select mapping
            this.dialog.showView('addFormSelectSubType', {
                    model: model,
                    otherData: {
                        subject: subject,
                        formName: formName,
                        //pass in the form type model
                        formType: formType,
                        linkTarget: linkTarget
                    }
                },
                function () {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
        },
        _getLatestFormDefinition: function (formDefinitionGUID) {
            var definitionURL = this.get('definitionURL');

            var model = new Y.usp.form.FormDefinition({
                id: formDefinitionGUID,
                url: definitionURL
            });
            return new Y.Promise(function (resolve, reject) {
                var data = model.load(function (err) {
                    if (err !== null) {
                        //failed to get the existing form instances for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },

        handleLinkFromAddFormInstance: function (e) {
            var checklistInstanceId = e.checklistInstanceId,
                formDefinition = e.formDefinition,
                subType = e.subType,
                subject = e.subject;

            //proceed to add the form instance
            //clear any existing errors shown - we call into the errors plugin
            this.modelErrors.clearErrors();

            var linkTarget;
            //if checklist instance id was present in the payload
            if (checklistInstanceId) {
                linkTarget = {
                    type: 'CHECKLIST',
                    id: checklistInstanceId
                }
            }
            //show the loading dialog while we get the form definition
            this.dialog.showView('addFormLoading');
            this._getLatestFormDefinition(formDefinition).then(function (formDefinition) {
                this._handleAddForm(subject, formDefinition, subType, linkTarget);
            }.bind(this)).catch(function (err) {
                switch (err.code) {
                case 401:
                    {
                        this.fire('error', {
                            error: {
                                message: 'Your permission settings do not allow you to create an instance of this form.'
                            }
                        });
                        break;
                    }
                default:
                    {
                        this.fire('error', {
                            error: err
                        });
                    }
                }
                this.dialog.hide();
            }.bind(this));
        }
    }, {
        ATTRS: {
            errorsConfig: {},
            subject: {},
            labels: {},
            dialogConfig: {},
            existingFormInstanceTypesURL: {
                value: ''
            },
            applicableFormMappingsURL: {
                value: ''
            },
            addFormInstanceURL: {
                value: ''
            },
            viewFormURL: {
                value: ''
            },
            definitionURL: {
                value: ''
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'add-form-results',
        'add-form-dialog',
        'model-errors-plugin',
        'escape',
        'promise',
        'usp-form-NewFormInstance',
        'usp-formchecklist-NewFormInstanceWithChecklistResource',
        'usp-form-FormInstance',
        'usp-form-FormMapping',
        'form-launcher'
    ]
});