YUI.add('cpraform-risks-view', function(Y) {
    var A = Y.Array,
        L = Y.Lang,
        ADD_BUTTON_TEMPLATE = '<div class="pull-right"><button class="pure-button-active pure-button {id}" disabled="disabled"><i class="fa fa-plus"></i> {label}</button></div>';

    Y.namespace('app').RiskView = Y.Base.create('riskView', Y.View, [], {
        template: Y.Handlebars.templates['cpraFormIdentifiedRisksView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form models changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels'),
                enumTypes: this.get('enumTypes')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            enumTypes: {
                value: {
                    impact: Y.usp.resolveassessment.enum.Impact.values,
                    likelihood: Y.usp.resolveassessment.enum.Likelihood.values
                }
            }
        }
    });

    Y.namespace('app').RiskTable = Y.Base.create('riskTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                formModel = this.get('model'),
                modelList = new Y.usp.resolveassessment.IdentifiedRiskList({
                    url: L.sub(this.get('url'), {
                        id: this.get('model').get('id'),
                        planType: this.get('planType'),
                        riskType: this.get('riskType')
                    })
                });

            this.results = new Y.app.RiskView({
                modelList: modelList,
                labels: config.labels,
                formModel: this.get('model')
            });

            this._riskTableEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.identifiedRisk .pure-button', this)
            ];
            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            formModel.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('addButtonLabel');

            // superclass call
            Y.app.RiskTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._riskTableEvtHandlers) {
                A.each(this._riskTableEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._riskTableEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }

        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event with plan type and risk type
                    this.fire('add', {
                        id: this.get('model').get('id'),
                        planType: this.get('planType'),
                        riskType: this.get('riskType'),
                        narrativeRiskType: this.get('narrativeRiskType')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit-risk')) {
                this.fire('edit', {
                    id: record.get('id'),
                    riskType: this.get('riskType'),
                    narrativeRiskType: this.get('narrativeRiskType')
                });
            } else if (target.hasClass('remove-risk')) {
                this.fire('remove', {
                    id: record.get('id'),
                    riskType: this.get('riskType'),
                    narrativeRiskType: this.get('narrativeRiskType')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading risks
             */
            url: {},
            /**
             * @Attribute planType - Substituted into the URL
             */
            planType: {},
            /**
             * @Attribute riskType - Substituted into the URL (as type)
             */
            riskType: {}
        }
    });

    Y.namespace('app').RiskTypeView = Y.Base.create('riskTypeView', Y.View, [], {
        initializer: function(config) {
            //Create a new risk table - pass on the config
            this.riskTable = new Y.app.RiskTable(config);
            // Add event targets
            this.riskTable.addTarget(this);
        },
        render: function() {
            //render the table
            this.get('container').setHTML(this.riskTable.render().get('container'));
            return this;
        },
        destructor: function() {
            this.riskTable.destroy();
            delete this.riskTable;
        },
        reload: function() {
            this.riskTable.reload();
        }
    }, {
        ATTRS: {}
    });
    Y.namespace('app').RiskSetAccordion = Y.Base.create('riskSetAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            var planType = this.get('planType');
            //setup risk accordions
            this.part1 = new Y.app.RiskTypeView(Y.merge(config, {
                planType: planType,
                riskType: 'TO_SELF',
                title: config.labels.part1.title,
                narrativeRiskType: config.labels.part1.narrativeRiskType,
                addButtonLabel: config.labels.part1.addButtonLabel
            }));
            this.part2 = new Y.app.RiskTypeView(Y.merge(config, {
                planType: planType,
                riskType: 'TO_OTHERS',
                title: config.labels.part2.title,
                narrativeRiskType: config.labels.part2.narrativeRiskType,
                addButtonLabel: config.labels.part2.addButtonLabel
            }));
            this.part3 = new Y.app.RiskTypeView(Y.merge(config, {
                planType: planType,
                riskType: 'TO_STAFF',
                title: config.labels.part3.title,
                narrativeRiskType: config.labels.part3.narrativeRiskType,
                addButtonLabel: config.labels.part3.addButtonLabel
            }));
            this.part4 = new Y.app.RiskTypeView(Y.merge(config, {
                planType: planType,
                riskType: 'WITHIN_PHYSICAL_ENVIRONMENT',
                title: config.labels.part4.title,
                narrativeRiskType: config.labels.part4.narrativeRiskType,
                addButtonLabel: config.labels.part4.addButtonLabel
            }));
            // Add event targets
            this.part1.addTarget(this);
            this.part2.addTarget(this);
            this.part3.addTarget(this);
            this.part4.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.part1.render().get('container'));
            contentNode.append(this.part2.render().get('container'));
            contentNode.append(this.part3.render().get('container'));
            contentNode.append(this.part4.render().get('container'));
            //superclass call
            Y.app.RiskSetAccordion.superclass.render.call(this);
            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.part1.destroy();
            delete this.part1;
            this.part2.destroy();
            delete this.part2;
            this.part3.destroy();
            delete this.part3;
            this.part4.destroy();
            delete this.part4;
        },
        reload: function(riskType) {
            switch (riskType) {
                case 'TO_SELF':
                    this.part1.reload();
                    break;
                case 'TO_OTHERS':
                    this.part2.reload();
                    break;
                case 'TO_STAFF':
                    this.part3.reload();
                    break;
                case 'WITHIN_PHYSICAL_ENVIRONMENT':
                    this.part4.reload();
                    break;
                default:
            }
        }
    }, {
        ATTRS: {
            planType: {},
            narrativeRiskType: {},
            title: {
                getter: function(val) {
                    var planType = this.get('planType'),
                        enumObj = A.find(Y.usp.resolveassessment.enum.CarePlanType.values, function(o) {
                            return o.enumName === planType;
                        }) || {};
                    return L.sub(val, {
                        planType: enumObj.name || ''
                    });
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'accordion-panel',
               'usp-resolveassessment-IdentifiedRisk',
               'resolveassessment-component-enumerations',
               'handlebars-helpers',
               'handlebars-cpraform-templates'
              ]
});