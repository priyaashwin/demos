YUI.add('form-status-buttons', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        STATE_DRAFT = 'DRAFT',
        STATE_COMPLETE = 'COMPLETE',
        STATE_AUTHORISED = 'AUTHORISED',
        STATE_DELETED = 'ARCHIVED';
    Y.namespace('app').FormStatusContextButton = Y.Base.create('formStatusContextButton', Y.app.ContentContextButton, [], {
        containerTemplate:'<li />',
        render: function() {
            //NOTE this method will be called for every model change
            var status = this.get('model').get('status');
            
            //Don't show the buttons if not permitted
            if(this.get('isPermitted') === false) {
            	return this;
            }
            
            if (this.get('isPermitted') === false || status === STATE_DRAFT && this.get('statusDraft') === false || status === STATE_COMPLETE && this.get('statusComplete') === false || status === STATE_AUTHORISED && this.get('statusAuthorised') === false || status === STATE_DELETED && this.get('statusArchived') === false) {
                this.disable();
            } else {
                this.enable();
            }
            // Call the superclass to actually do the render
            Y.app.FormStatusContextButton.superclass.render.call(this);
            
            return this;
        }
    }, {
        ATTRS: {
            statusDraft: {
                value: false
            },
            statusComplete: {
                value: false
            },
            statusAuthorised: {
                value: false
            },
            statusArchived: {
                value: false
            },
            isPermitted: {
                value: false
            }
        }
    });
    Y.namespace('app').FormStatusButtons = Y.Base.create('formStatusButtons', Y.View, [], {
        initializer: function() {
            var dialogConfig = this.get('dialogConfig'),
                buttonsConfig = this.get('buttonsConfig'),
                formName=dialogConfig.formName;
            
            //array for buttons
            this.buttons = [
            //complete button
            new Y.app.FormStatusContextButton(Y.mix({
                events: {
                    '.pure-button': {
                        click: function(e) {
                            e.preventDefault();
                            if (this.get('enabled')) {
                                //fire custom event
                                this.fire('statusChange', {
                                    action: 'complete'
                                });
                            }
                        }
                    }
                },
                model: this.get('model')
            }, buttonsConfig.complete)),
            //reopen button
            new Y.app.FormStatusContextButton(Y.mix({
                events: {
                    '.pure-button': {
                        click: function(e) {
                            e.preventDefault();
                            if (this.get('enabled')) {
                                //fire custom event
                                this.fire('statusChange', {
                                    action: 'reopen'
                                });
                            }
                        }
                    }
                },
                model: this.get('model')
            }, buttonsConfig.reopen)),
            //reject button
            new Y.app.FormStatusContextButton(Y.mix({
                events: {
                    '.pure-button': {
                        click: function(e) {
                            e.preventDefault();
                            if (this.get('enabled')) {
                                //fire custom event
                                this.fire('statusChange', {
                                    action: 'reject'
                                });
                            }
                        }
                    }
                },
                model: this.get('model')
            }, buttonsConfig.reject)),
            //approve button
            new Y.app.FormStatusContextButton(Y.mix({
                events: {
                    '.pure-button': {
                        click: function(e) {
                            e.preventDefault();
                            if (this.get('enabled')) {
                                //fire custom event
                                this.fire('statusChange', {
                                    action: 'approve'
                                });
                            }
                        }
                    }
                },
                model: this.get('model')
            }, buttonsConfig.approve)),
            //remove button
            new Y.app.FormStatusContextButton(Y.mix({
                events: {
                    '.pure-button': {
                        click: function(e) {
                            e.preventDefault();
                            if (this.get('enabled')) {
                                //fire custom event
                                this.fire('statusChange', {
                                    action: 'remove'
                                });
                            }
                        }
                    }
                },
                model: this.get('model')
            }, buttonsConfig.remove))];
            //create status change dialog
            this.statusChangeDialog = new Y.app.ResolveStatusFormDialog({
                headerContent: dialogConfig.defaultHeader,
                views: { /* !!! Merge with existing view configuration defined in resolvestatusform.js !!! */
                    completeForm: {
                        headerTemplate: dialogConfig.complete.headerTemplate
                    },
                    reopenForm: {
                        headerTemplate: dialogConfig.reopen.headerTemplate
                    },
                    rejectForm: {
                        headerTemplate: dialogConfig.reject.headerTemplate
                    },
                    approveForm: {
                        headerTemplate: dialogConfig.approve.headerTemplate
                    },
                    removeForm: {
                        headerTemplate: dialogConfig.remove.headerTemplate
                    }
                }
            });
            //add bubble targets
            this.statusChangeDialog.addTarget(this);
            //add button bubble targets
            A.each(this.buttons, function(button) {
                button.addTarget(this);
            }, this);
            //add events
            this.on('*:statusChange', function(e, config, view) {
                var model = view.get('model'),
                    formType,
                    formConfig = config[e.action],                    
                    actionParam = (e.action === 'remove' ? 'archived' : e.action === 'approve' ? 'authorised' : e.action === 'reject' ? 'rejected' : e.action === 'reopen' ? 'draft' : 'complete'),
                    viewToShow = e.action + 'Form',
                    url,
                    formTypeLabel='';
                    
                    
                    if (model.get("formType")==='PlacementReport'){
                        formType='placementReport';
                    }else if (model.get("formType")==='CarePlanRiskAssessment'){
                        formType='carePlanRiskAssessment';
                    }else if(model.get("formType")==='IntakeForm'){
                        formType='intakeForm';
                    }

                    formTypeLabel = config[formType].label;
                    
                    url = L.sub(config.url, {
                        formType: formType,
                        actionParam: actionParam
                    });
                    
                if (e.action == 'reject') {
                    this.showView(viewToShow, {
                        message: formConfig.message,
                        model: new Y.app.RejectStatusForm({
                            url: url,
                            id: model.get('id')
                        }),
                        formModel:model,
                        otherData: {
                            narrative:L.sub(formConfig.narrative, {formName:formName}),
                            formType: formTypeLabel
                        },
                        successMessage:formConfig.successMessage,
                        labels: formConfig.labels
                    }, function() {
                        //Get the button by name out of the footer and enable it.
                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
                } else {
                    this.showView(viewToShow, {
                        message: formConfig.message,
                        model: new Y.app.StatusChangeForm({
                            url: url,
                            id: model.get('id')
                        }),
                        formModel:model,
                        otherData: {
                            narrative:L.sub(formConfig.narrative, {formName:formName}),                            
                            formType: formTypeLabel                            
                        },
                        successMessage:formConfig.successMessage,
                        labels: formConfig.labels
                    }, function() {
                        //Get the button by name out of the footer and enable it.
                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
                }
            }, this.statusChangeDialog, dialogConfig, this);
        },
        render: function() {
            this.statusChangeDialog.render();
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            //render buttons
            A.each(this.buttons, function(button) {
                contentNode.append(button.render().get('container'));
            });
            //finally set the content into the container
            this.get('container').setHTML(contentNode);
            return this;
        },
        destructor: function() {
            //clean up dialog
            this.statusChangeDialog.destroy();
            delete this.statusChangeDialog;
            //destroy buttons
            A.each(this.buttons, function(button) {
                button.destroy();
            });
            //null out
            this.buttons = null;
        }
    }, {
        ATTRS: {
            dialogConfig: {},
            buttonsConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'content-context-button',
               'formstatus-dialog-views']
});