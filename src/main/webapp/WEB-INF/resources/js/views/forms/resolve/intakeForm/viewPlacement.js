YUI.add('intakeform-placement-tab', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        ADD_BUTTON_TEMPLATE = '<div class="pull-right"><button class="pure-button-active pure-button {id}" disabled="disabled"><i class="fa fa-plus"></i> {label}</button></div>';

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormBriefHistoryView = Y.Base.create('intakeFormBriefHistoryView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormPlacementBriefHistoryView']
    });

    Y.namespace('app').IntakeFormCurrentPlacementView = Y.Base.create('intakeFormCurrentPlacementView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormCurrentPlacementView']
    });

    Y.namespace('app').IntakeFormPlacementBriefHistoryAccordion = Y.Base.create('intakeFormPlacementBriefHistoryAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormBriefHistoryView = new Y.app.IntakeFormBriefHistoryView(config);

            // Add event targets
            this.intakeFormBriefHistoryView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormPlacementBriefHistoryAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormBriefHistoryView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormBriefHistoryView.destroy();

            delete this.intakeFormBriefHistoryView;
        }
    });

    Y.namespace('app').PreviousPlacementView = Y.Base.create('previousPlacementView', Y.View, [], {
        template: Y.Handlebars.templates['intakeFormPreviousPlacementView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {}
        }
    });

    Y.namespace('app').PreviousPlacementTable = Y.Base.create('previousPlacementTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                formModel = this.get('model'),
                modelList = new Y.usp.resolveassessment.IntakePreviousPlacementList({
                    url: L.sub(this.get('url'), {
                        id: formModel.get('id')
                    })
                });

            this.results = new Y.app.PreviousPlacementView({
                modelList: modelList,
                formModel: formModel,
                labels: config.labels
            });

            this._previousPlacementsEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.previousPlacement .pure-button', this)
            ];

            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            formModel.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('labels').addButton;

            // superclass call
            Y.app.PreviousPlacementTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._previousPlacementsEvtHandlers) {
                A.each(this._previousPlacementsEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._previousPlacementsEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }
        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event
                    this.fire('add', {
                        id: this.get('model').get('id')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id')
                });
            } else if (target.hasClass('remove')) {
                this.fire('remove', {
                    id: record.get('id')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading placement history
             */
            url: {},
            /**
             * @Attribute labels - The view labels
             */
            labels: {}
        }
    });

    Y.namespace('app').IdentifiedRiskView = Y.Base.create('identifiedRiskView', Y.View, [], {
        template: Y.Handlebars.templates['intakeFormIdentifiedRiskView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels'),
                enumTypes: this.get('enumTypes')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            enumTypes: {
                value: {
                    impact: Y.usp.resolveassessment.enum.Impact.values,
                    likelihood: Y.usp.resolveassessment.enum.Likelihood.values
                }
            }
        }
    });

    Y.namespace('app').IdentifiedRiskTable = Y.Base.create('identifiedRiskTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                formModel = this.get('model'),
                modelList = new Y.usp.resolveassessment.IntakeRiskAssessmentList({
                    url: L.sub(this.get('url'), {
                        id: formModel.get('id')
                    })
                });

            this.results = new Y.app.IdentifiedRiskView({
                modelList: modelList,
                formModel: formModel,
                labels: config.labels
            });

            this._identifiedRisksEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.identifiedRisk .pure-button', this)
            ];

            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            formModel.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('labels').addButton;

            // superclass call
            Y.app.IdentifiedRiskTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._identifiedRisksEvtHandlers) {
                A.each(this._identifiedRisksEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._identifiedRisksEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }
        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event
                    this.fire('add', {
                        id: this.get('model').get('id')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id')
                });
            } else if (target.hasClass('remove')) {
                this.fire('remove', {
                    id: record.get('id')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading placement history
             */
            url: {},
            /**
             * @Attribute labels - The view labels
             */
            labels: {}
        }
    });

    Y.namespace('app').IntakeFormPreviousPlacementAccordion = Y.Base.create('intakeFormPreviousPlacementAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //setup history table
            this.previousPlacementTable = new Y.app.PreviousPlacementTable(Y.merge(config.previousPlacementConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.previousPlacementTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.previousPlacementTable.render().get('container'));

            //superclass call
            Y.app.IntakeFormPreviousPlacementAccordion.superclass.render.call(this);
            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.previousPlacementTable.destroy();
            delete this.previousPlacementTable;
        },
        reload: function() {
            this.previousPlacementTable.reload();
        }
    });


    Y.namespace('app').IntakeFormCurrentPlacementAccordion = Y.Base.create('intakeFormCurrentPlacementAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormCurrentPlacementView = new Y.app.IntakeFormCurrentPlacementView(config);

            // Add event targets
            this.intakeFormCurrentPlacementView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormCurrentPlacementAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormCurrentPlacementView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormCurrentPlacementView.destroy();

            delete this.intakeFormCurrentPlacementView;
        }
    });

    Y.namespace('app').IntakeFormIdentifiedRisksAccordion = Y.Base.create('intakeFormIdentifiedRisksAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //setup identified risks table
            this.identifiedRiskTable = new Y.app.IdentifiedRiskTable(Y.merge(config.identifiedRiskConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.identifiedRiskTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.identifiedRiskTable.render().get('container'));

            //superclass call
            Y.app.IntakeFormIdentifiedRisksAccordion.superclass.render.call(this);
            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.identifiedRiskTable.destroy();
            delete this.identifiedRiskTable;
        },
        reload: function() {
            this.identifiedRiskTable.reload();
        }
    });

    Y.namespace('app').IntakeFormPlacementTab = Y.Base.create('intakeFormPlacementTab', Y.View, [], {
        initializer: function() {
            var briefHistoryConfig = this.get('briefHistoryConfig'),
                historyConfig = this.get('historyConfig'),
                currentPlacementConfig = this.get('currentPlacementConfig'),
                identifiedRisksConfig = this.get('identifiedRisksConfig');

            //create the accordion views
            this.briefHistoryAccordion = new Y.app.IntakeFormPlacementBriefHistoryAccordion(
                Y.merge(
                    briefHistoryConfig, {
                        model: this.get('model')
                    }));

            this.historyAccordion = new Y.app.IntakeFormPreviousPlacementAccordion(
                Y.merge(
                    historyConfig, {
                        model: this.get('model')
                    }));

            this.currentPlacementAccordion = new Y.app.IntakeFormCurrentPlacementAccordion(
                Y.merge(
                    currentPlacementConfig, {
                        model: this.get('model')
                    }));

            this.identifiedRisksAccordion = new Y.app.IntakeFormIdentifiedRisksAccordion(
                Y.merge(
                    identifiedRisksConfig, {
                        model: this.get('model')
                    }));

            // Add event targets
            this.briefHistoryAccordion.addTarget(this);
            this.historyAccordion.addTarget(this);
            this.currentPlacementAccordion.addTarget(this);
            this.identifiedRisksAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.briefHistoryAccordion.render().get('container'));
            contentNode.append(this.historyAccordion.render().get('container'));
            contentNode.append(this.currentPlacementAccordion.render().get('container'));
            contentNode.append(this.identifiedRisksAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties
            this.briefHistoryAccordion.destroy();
            delete this.briefHistoryAccordion;

            this.historyAccordion.destroy();
            delete this.historyAccordion;

            this.currentPlacementAccordion.destroy();
            delete this.currentPlacementAccordion;

            this.identifiedRisksAccordion.destroy();
            delete this.identifiedRisksAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            briefHistoryConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            historyConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            currentPlacementConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            identifiedRisksConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeRiskAssessment',
               'usp-resolveassessment-IntakePreviousPlacement',
               'usp-resolveassessment-IntakeFormFullDetails',
               'resolveassessment-component-enumerations'
              ]
});