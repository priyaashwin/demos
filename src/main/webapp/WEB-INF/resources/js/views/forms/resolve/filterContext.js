YUI.add('form-filter-context', function (Y) {

	 Y.namespace('app').FormFilterContext=Y.Base.create('formFilterContext', Y.View, [], {
		 	template: Y.Handlebars.templates["formActiveFilter"],
		 	events: {
		 	    'a': {click: 'removeFilter'}

		 	  },
			initializer: function () {
			    var model = this.get('model');

			    // Re-render this view when the model changes
				model.after('change', this.render, this);
			},
			render:function(){
				var container = this.get('container'),
				//use handlebars template
                html = this.template({
                    labels:this.get('labels'),
                    modelData:this.get('model').toJSON()
                });

				//set active class on container
                if(this.get('model').get('showAnyFilter')===true){
                    container.addClass('active');
                }else{
                    container.removeClass('active');
                }

				//set html into container
				container.setHTML(html);

                //fire custom event to re-size fixed header
                Y.fire('header:resize');

				return this;
			},removeFilter:function(e){
				e.preventDefault();
				var t=e.currentTarget,model=this.get("model");
				if(t.hasClass("remove-status-filter")){
					model.set("status", [],{src: "filter"});
				}
				else if(t.hasClass("removeAll")){
					this.get("model").reset();
					this.fire("filter:apply");
					
				}
			}
	},{
        // Specify attributes and static properties for your View here.
        ATTRS: {
            //an attribute to contain template labels
            labels:{}
        }
	});
}, '0.0.1', {
    requires: ['yui-base','view','model','event-custom-base', 'handlebars-resolveform-templates']
});