YUI.add('intakeform-physio-tab', function(Y) {
    var L = Y.Lang;

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormMobilityView = Y.Base.create('intakeFormMobilityView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormMobilityView']
    });

    Y.namespace('app').IntakeFormOccupationalIssuesView = Y.Base.create('intakeFormOccupationalIssuesView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormOccupationalIssuesView']
    });

    Y.namespace('app').IntakeFormMobilityAccordion = Y.Base.create('intakeFormMobilityAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormMobilityView = new Y.app.IntakeFormMobilityView(config);

            // Add event targets
            this.intakeFormMobilityView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormMobilityAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormMobilityView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormMobilityView.destroy();

            delete this.intakeFormMobilityView;
        }
    });

    Y.namespace('app').IntakeFormOccupationalIssuesAccordion = Y.Base.create('intakeFormOccupationalIssuesAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormOccupationalIssuesView = new Y.app.IntakeFormOccupationalIssuesView(config);

            // Add event targets
            this.intakeFormOccupationalIssuesView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormOccupationalIssuesAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormOccupationalIssuesView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormOccupationalIssuesView.destroy();

            delete this.intakeFormOccupationalIssuesView;
        }
    });

    Y.namespace('app').IntakeFormPhysioTab = Y.Base.create('intakeFormPhysioTab', Y.View, [], {
        initializer: function() {
            var physiotherapyConfig = this.get('physiotherapyConfig'),
                mobilityConfig = this.get('mobilityConfig'),
                occupationalTherapistConfig = this.get('occupationalTherapistConfig'),
                occupationalIssuesConfig = this.get('occupationalIssuesConfig');

            //create the accordion views            
            this.physiotherapyAccordion = new Y.app.IntakeFormReferencedPersonAccordion(
                Y.merge(
                    physiotherapyConfig, {
                        model: new Y.usp.resolveassessment.IntakeReferencedPerson({
                            url: L.sub(physiotherapyConfig.url, {
                                id: this.get('model').get('id')
                            })
                        }).load(),
                        formModel: this.get('model')
                    }));


            this.mobilityAccordion = new Y.app.IntakeFormMobilityAccordion(
                Y.merge(
                    mobilityConfig, {
                        model: this.get('model')
                    }));

            this.occupationalTherapistAccordion = new Y.app.IntakeFormReferencedPersonAccordion(
                Y.merge(
                    occupationalTherapistConfig, {
                        model: new Y.usp.resolveassessment.IntakeReferencedPerson({
                            url: L.sub(occupationalTherapistConfig.url, {
                                id: this.get('model').get('id')
                            })
                        }).load(),
                        formModel: this.get('model')
                    }));


            this.occupationalIssuesAccordion = new Y.app.IntakeFormOccupationalIssuesAccordion(
                Y.merge(
                    occupationalIssuesConfig, {
                        model: this.get('model')
                    }));

            // Add event targets            
            this.physiotherapyAccordion.addTarget(this);
            this.mobilityAccordion.addTarget(this);
            this.occupationalTherapistAccordion.addTarget(this);
            this.occupationalIssuesAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.physiotherapyAccordion.render().get('container'));
            contentNode.append(this.mobilityAccordion.render().get('container'));
            contentNode.append(this.occupationalTherapistAccordion.render().get('container'));
            contentNode.append(this.occupationalIssuesAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties            
            this.physiotherapyAccordion.destroy();
            delete this.physiotherapyAccordion;

            this.mobilityAccordion.destroy();
            delete this.mobilityAccordion;

            this.occupationalTherapistAccordion.destroy();
            delete this.occupationalTherapistAccordion;

            this.occupationalIssuesAccordion.destroy();
            delete this.occupationalIssuesAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            physiotherapyConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            mobilityConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            occupationalTherapistConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            occupationalIssuesConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'intakeform-referenced-person-view',
               'usp-resolveassessment-IntakeFormFullDetails'
              ]
});