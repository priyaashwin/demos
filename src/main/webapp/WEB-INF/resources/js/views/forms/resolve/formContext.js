YUI.add('form-content-context', function(Y) {
    var FormContentContext = Y.Base.create('formContentContext', Y.View, [], {
        events: {
            '.WarningView': {
                click: 'fireWarningDialog'
            }
        },
        //context template
        template: Y.Handlebars.templates["formViewContext"],
        initializer: function() {
            var model = this.get('model'),
                personModel = this.get('personModel');

            //set bubble target
            model.addTarget(this);
            personModel.addTarget(this);

            this.on('*:personWarningsChange', function(e){
              this.get('personModel').set('warningsCount', (e.newVal||[]).length);
            }, this);
            // Re-render this view when the model or person model changes
            this.after('*:change', this.render, this);

            Y.on('contentContext:update', Y.bind(function(e) {
                //bind into either the model or person model
                if (e.data['noOfPersonWarnings'] !== 'undefined') {
                    this.get('personModel').setAttrs({
                        'warningsCount': e.data['noOfPersonWarnings']
                    });
                } else {
                    this.get('model').setAttrs(e.data);
                }
            }, this), this);
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    modelData: this.get('model').toJSON(),
                    personModel: this.get('personModel').toJSON(),
                    labels: this.get('labels')
                });

            //set html into container
            container.setHTML(html);
            //fire custom event to resize header
            Y.fire('header:resize');
            return this;
        },
        fireWarningDialog: function(e) {
            var canAddWarning = this.get('canAddWarning'),
                canUpdateWarning = this.get('canUpdateWarning'),
                personModel = this.get('personModel');

            if (canAddWarning || canUpdateWarning) {
                Y.fire('warnings:showDialog', {
                    id: personModel.get('id'),
                    action: 'addEditPersonWarning'
                });
            } else {
                Y.fire('warnings:showDialog', {
                    id: personModel.get('id'),
                    action: 'viewPersonWarning'
                });
            }
        }
    }, {
        ATTRS: {
            labels: {},
            otherData: {},
            personModel: {}
        }
    });
    Y.namespace('app').FormContentContext = FormContentContext;
}, '0.0.1', {
    requires: ['yui-base',
               'event-custom-base',
               'handlebars-helpers',
               'handlebars-resolveform-templates']
});