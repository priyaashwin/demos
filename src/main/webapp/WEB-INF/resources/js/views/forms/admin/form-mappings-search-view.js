YUI.add('form-mappings-search-view', function(Y) {
    var A = Y.Array,
        L = Y.Lang,
        USPTemplates = Y.usp.ColumnTemplates,
        USPFormatters = Y.usp.ColumnFormatters;

    Y.namespace('app').FormMappingsSearchView = Y.Base.create('formMappingsSearchView', Y.View, [], {
        initializer: function() {
            var searchConfig = this.get('searchConfig'),
                contextConfig = this.get('contextConfig') || {},
                buttonsConfig = this.get('buttonsConfig') || {},
                addButtonConfig = buttonsConfig.add || {},
                formMappingDialogConfig = this.get('formMappingDialogConfig'),
                labels = searchConfig.labels,
                permissions = searchConfig.permissions,
                instance = this,
                destinationOptionsVisible=function(){
                    //only visible if this is a destination mapping
                    return this.record.get('destinationFormDefinitionId') === contextModel.get('formDefinitionId');
                };

            //access the context model
            var contextModel = contextConfig.model;

            //configure the context view
            this.context = new Y.app.ContentContext({
                container: contextConfig.container,
                template: contextConfig.template,
                model: contextModel
            });

            //history object with default values from the view attributes
            this.history=new Y.History({
                initialState: {                
                    formDefinitionId:contextModel.get('formDefinitionId'),
                    name:contextModel.get('name')
                }
            });
            
            //check if we came into the page with history configured - these will only not match if the page was loaded with a hash value
            if(this.history.get('formDefinitionId')!=contextModel.get('formDefinitionId')){
                //check we have a formDefinitionId
                if(this.history.get('formDefinitionId')!==undefined && this.history.get('formDefinitionId')!==null){
                    //make them match
                    contextModel.setAttrs({
                        formDefinitionId: this.history.get('formDefinitionId'),
                        name:  this.history.get('name')
                    });
                }
            }
            
            //configure the add button
            this.addMappingButton = new Y.app.ContentContextButton({
                events: {
                    '.pure-button': {
                        click: function(e) {

                            e.preventDefault();

                            if (permissions.canAdd) {
                                instance.formMappingDialog.showView('addFormMapping', {
                                    otherData: {
                                        narrative: L.sub(formMappingDialogConfig.addConfig.narrative, {
                                            formType: this.get('model').get('name')
                                        })
                                    },
                                    labels: formMappingDialogConfig.addConfig.labels,
                                    definitionsURL: formMappingDialogConfig.definitionsURL,
                                    definitionURL: formMappingDialogConfig.definitionURL,
                                    formDefinitionId: this.get('model').get('formDefinitionId'),
                                    //new form mapping model
                                    model: new Y.app.NewFormMappingForm({
                                        url: L.sub(formMappingDialogConfig.addConfig.url, {
                                            id: this.get('model').get('formDefinitionId')
                                        })
                                    })
                                }, function() {
                                    //Get the button by name out of the footer and enable it.
                                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                                });
                            }
                        }
                    }
                },
                container: addButtonConfig.container,
                //button template
                template: addButtonConfig.template,
                //model is the context model
                model: contextModel
            });

            //configure the form results table
            this.results = new Y.usp.PaginatedResultsTable({
                //configure the initial sort
                sortBy: [{
                    name: 'desc'
                }],
                //declare the columns
                columns: [{
                    key: 'name',
                    label: labels.name,
                    sortable: true,
                    width: '70%'
                }, {
                    key: 'destinationFormDefinitionId',
                    label: labels.direction,
                    width: '15%',
                    formatter: function(o) {
                        var str = [],
                            val = o.value;
                        if (val !== contextModel.get('formDefinitionId')) {
                            str.push('copy from');
                        } else if (val === contextModel.get('formDefinitionId')) {
                            str.push('copy to');
                        }

                        if (o.data.preferred) {
                            str.push('(preferred)');
                        }

                        return str.join(' ');
                    }
                }, {
                    label: labels.actions,
                    className: 'pure-table-actions',
                    width: '15%',
                    formatter: USPFormatters.actions,
                    items: [
                        /*{
                        clazz: 'view',
                        title: labels.viewTitle,
                        label: labels.view,
                        enabled: permissions.canView
                    },*/
                         {
                            clazz: 'view',
                            title: labels.viewTitle,
                            label: labels.view,
                            visible: destinationOptionsVisible,
                            enabled: permissions.canView
                        },
                        {
                            clazz: 'edit',
                            title: labels.editTitle,
                            label: labels.edit,
                            visible: destinationOptionsVisible,
                            enabled: permissions.canEdit
                        }, {
                            clazz: 'preferred',
                            title: labels.setPreferredTitle,
                            label: labels.setPreferred,
                            visible: function(){
                                //only visible if this is a destination mapping AND  if NOT preferred
                                return (this.record.get('destinationFormDefinitionId') === contextModel.get('formDefinitionId')) && !this.record.get('preferred');
                            },
                            enabled: permissions.canEdit
                        }, {
                            clazz: 'clearPreferred',
                            title: labels.clearPreferredTitle,
                            label: labels.clearPreferred,
                            visible: function(){
                                //only visible if this is a destination mapping AND  if IS preferred
                                return (this.record.get('destinationFormDefinitionId') === contextModel.get('formDefinitionId')) && this.record.get('preferred');
                            },
                            enabled: permissions.canEdit
                        },{
                            clazz: 'remove',
                            title: labels.removeTitle,
                            label: labels.remove,
                            visible:  destinationOptionsVisible,
                            enabled: permissions.canEdit
                        },{
                            clazz: 'focus',
                            title: labels.focusTitle,
                            label: labels.focus,
                            visible:  function(){
                                //visible ONLY if this IS NOT the destination
                                return (this.record.get('destinationFormDefinitionId') !== contextModel.get('formDefinitionId'));
                            }
                        }
                    ]
                }],
                //configure the data set
                data: new Y.usp.form.PaginatedFormMappingList({
                    url: L.sub(searchConfig.url, {
                        formDefinitionId: contextModel.get('formDefinitionId')
                    })
                }),
                noDataMessage: searchConfig.noDataMessage,
                plugins: [{
                    //plugin Menu handler
                    fn: Y.Plugin.usp.ResultsTableMenuPlugin
                }, {
                    //plugin Keyboard nav
                    fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                }]
            });

            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                tableId: this.name
            });


            //configure our dialog
            this.formMappingDialog = new Y.app.FormMappingDialog({
                headerContent: formMappingDialogConfig.defaultHeader,
                width: 760,
                views: {
                    /* !!! Merge with existing view configuration defined in form-mapping-dialog-views.js !!! */
                    addFormMapping: {
                        headerTemplate: formMappingDialogConfig.addConfig.headerTemplate,
                        successMessage: formMappingDialogConfig.addConfig.successMessage
                    },
                    editFormMapping: {
                        headerTemplate: formMappingDialogConfig.editConfig.headerTemplate,
                        successMessage: formMappingDialogConfig.editConfig.successMessage
                    },
                    viewFormMapping: {
                        headerTemplate: formMappingDialogConfig.viewConfig.headerTemplate,
                        successMessage: formMappingDialogConfig.viewConfig.successMessage
                    },                    
                    removeFormMapping: {
                        headerTemplate: formMappingDialogConfig.removeConfig.headerTemplate,
                        successMessage: formMappingDialogConfig.removeConfig.successMessage
                    },
                    preferredFormMapping:{
                        headerTemplate: formMappingDialogConfig.preferredConfig.headerTemplate,
                        successMessage: formMappingDialogConfig.preferredConfig.successMessage
                    }
                }
            });

            // Add event targets
            this.results.addTarget(this);
            this.addMappingButton.addTarget(this);
            this.formMappingDialog.addTarget(this);

            //when the formDefinitionId changes, we can reload our model
            contextModel.on('formDefinitionIdChange', function(e) {
                //update the URL
                instance.results.data.url = L.sub(searchConfig.url, {
                    formDefinitionId: e.newVal
                });
                //reload the results
                instance.results.reload();
            });

            //add event handler for add button click
            this._evtHandlers = [
                //subscribe to events that we want to trigger a reload of the results
                this.on(['newFormMappingView:saved', 
                         'editFormMappingView:saved', 
                         'preferredFormMappingView:saved', 
                         'removeFormMappingView:saved'], function(e) {
                    //reload results
                    this.results.reload();
                }, this),
                //subscribe to reload event and scroll page and dialog close
                this.on('*:pageUpdate', function(e) {
                    Y.config.win.scrollTo(0, 0);
                }),
                this.on('formMappingDialog:visibleChange', function(e) {
                    //when the dialog closes - scroll to top
                    if(!e.newVal){
                        Y.config.win.scrollTo(0, 0);
                    }
                }),                
                
                //subscribe to the click handler on the results table
                this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this),
                
                //change handler for history - i.e. user popped history item
                this.history.on('history:change', function (e) {
                    var newVal=e.newVal||{}; 
                    //only update if we actually have a formDefinitionId 
                    //(Safari browser pops the history state even though the user hasn't used the back/forward browser buttons)
                    if(newVal.formDefinitionId!==undefined && newVal.formDefinitionId!==null){
                        //update the context model with data from this history change event
                        this.context.get('model').setAttrs({
                            formDefinitionId: newVal.formDefinitionId,
                            name: newVal.name
                        });
                    }
                }, this),
                //subscribe to edit button click from a view
                this.on('viewFormMappingView:edit', function(e) {
                        //handle the edit button click
                        this._handleEditButton(e);
                }, this),
            ];
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            if (this.results) {
                //destroy the results table
                this.results.destroy();

                delete this.results;
            }

            if (this._tablePanel) {
                this._tablePanel.destroy();

                delete this._tablePanel;
            }

            if (this.context) {
                this.context.destroy();

                delete this.context;
            }

            if (this.addMappingButton) {
                this.addMappingButton.destroy();

                delete this.addMappingButton;
            }

            if (this.formMappingDialog) {
                this.formMappingDialog.destroy();

                delete this.formMappingDialog;
            }
        },
        render: function() {
            var container = this.get('container'),
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.

            //render some standalone items
            this.addMappingButton.render();
            this.context.render();
            this.formMappingDialog.render();

            if(this.get('searchConfig').resultsCountNode){
                //Use our pre-defined results count node
                this._tablePanel.set('resultsCount', this.get('searchConfig').resultsCountNode);
            }
            
            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));

            //render results
            this._tablePanel.renderResults(this.results);

            //finally set the content into the container
            container.setHTML(contentNode);

            return this;
        },
        _handleEditButton:function(e){
            var id=e.id,
                name=e.name,
                formDefinitionId=e.formDefinitionId,
                formDefinitionName=e.formDefinitionName,
                searchConfig = this.get('searchConfig'),
                permissions = searchConfig.permissions,                
                sourceFormDefinitionId=e.sourceFormDefinitionId;
            
            this._handleEdit(id, name, formDefinitionId, formDefinitionName, sourceFormDefinitionId, permissions);
        },
        _handleEdit:function(id, name, destinationFormDefinitionId, destinationFormName, selectedFormDefinitionId, permissions){
            //click handler for table row            
            var formMappingDialogConfig = this.get('formMappingDialogConfig'),
                dialogConfig = formMappingDialogConfig,
                sourceFormDefinitionId=selectedFormDefinitionId||null;
            
            //hang onto the original sourceFormDefinitionId - we use it to pre-select in the dialog                
            sourceFormDefinitionId = this.context.get('model').get('formDefinitionId');
            
            //If the destinationFormDefinitionId is not equal to our current context, then we must switch
            if (destinationFormDefinitionId !== this.context.get('model').get('formDefinitionId')) {
                //set a history item for this form definition - that will trigger a context change via the history change event
                this.history.add({
                    formDefinitionId:destinationFormDefinitionId,
                    name:destinationFormName
                },{merge:false});
            }

            //at this point the context will have been updated
            this.formMappingDialog.showView('editFormMapping', {
                    model: new Y.app.EditFormMappingForm({
                        url: dialogConfig.editConfig.url
                    }),
                    otherData: {
                        narrative: L.sub(dialogConfig.editConfig.narrative, {
                            formType: this.context.get('model').get('name'),
                            name: name
                        })
                    },
                    labels: dialogConfig.editConfig.labels,
                    definitionsURL: formMappingDialogConfig.definitionsURL,
                    definitionURL: formMappingDialogConfig.definitionURL,
                    formDefinitionId: this.context.get('model').get('formDefinitionId'),
                    initialSourceFormDefinitionId: sourceFormDefinitionId
                }, {
                    modelLoad: true,
                    payload: {
                        id: id
                    }
                },
                function(view) {
                    //render the view - we don't re-render on model change (this is NOT normal so don't copy this example)
                    //we do it here because there is an embedded REACT component in our view which we don't want to destroy
                    view.render();
                    //Get the button by name out of the footer and set disabled attribute to inverse of edit permission
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', !permissions.canEdit);
                    
                    //align the dialog - we do this here as the dialog dimensions are likely to have changed due to the REACT component render
                    Y.later(100, this, this.align);
                });
        },
        _handleRowClick: function(e) {
            //click handler for table row            
            var target = e.currentTarget,
                record = this.results.getRecord(target.get("id")),
                formMappingDialogConfig = this.get('formMappingDialogConfig'),
                destinationFormDefinitionId = record.get('destinationFormDefinitionId'),
                destinationFormName = record.get('destinationFormDefinitionName'),
                searchConfig = this.get('searchConfig'),
                dialogConfig = formMappingDialogConfig,
                permissions = searchConfig.permissions,
                sourceFormDefinitionId;

            //prevent default action
            e.preventDefault();

            if(target.hasClass('focus')){
                //switch focus
                //If the destinationFormDefinitionId is not equal to our current context, then we must switch
                if (destinationFormDefinitionId !== this.context.get('model').get('formDefinitionId')) {

                    //hang onto the original sourceFormDefinitionId - we use it to pre-select in the dialog
                    sourceFormDefinitionId = this.context.get('model').get('formDefinitionId');
                    
                    //set a history item for this form definition - that will trigger a context change via the history change event
                    this.history.add({
                        formDefinitionId:destinationFormDefinitionId,
                        name:destinationFormName
                    },{merge:false});                    
                }                
            }
            else if (target.hasClass('edit') && permissions.canEdit) {
                this._handleEdit( record.get('id'),  record.get('name'), destinationFormDefinitionId, destinationFormName, sourceFormDefinitionId, permissions);
            }
            else if (target.hasClass('view') && permissions.canView) {                
                //hang onto the original sourceFormDefinitionId - we use it to pre-select in the dialog                    
                sourceFormDefinitionId = this.context.get('model').get('formDefinitionId');
                
                //If the destinationFormDefinitionId is not equal to our current context, then we must switch
                if (destinationFormDefinitionId !== this.context.get('model').get('formDefinitionId')) {
                    //set a history item for this form definition - that will trigger a context change via the history change event
                    this.history.add({
                        formDefinitionId:destinationFormDefinitionId,
                        name:destinationFormName
                    },{merge:false});
                }

                //at this point the context will have been updated
                this.formMappingDialog.showView('viewFormMapping', {
                        model: new  Y.usp.form.FormMappingWithControlMappings({
                            url: dialogConfig.viewConfig.url
                        }),
                        otherData: {
                            narrative: L.sub(dialogConfig.viewConfig.narrative, {
                                formType: this.context.get('model').get('name'),
                                name: record.get('name')
                            })
                        },
                        labels: dialogConfig.viewConfig.labels,
                        definitionsURL: formMappingDialogConfig.definitionsURL,
                        definitionURL: formMappingDialogConfig.definitionURL,
                        formDefinitionId: this.context.get('model').get('formDefinitionId'),
                        initialSourceFormDefinitionId: sourceFormDefinitionId
                    }, {
                        modelLoad: true,
                        payload: {
                            id: record.get('id')
                        }
                    },
                    function(view) {
                        //render the view - we don't re-render on model change (this is NOT normal so don't copy this example)
                        //we do it here because there is an embedded REACT component in our view which we don't want to destroy
                        view.render();
                        //align the dialog - we do this here as the dialog dimensions are likely to have changed due to the REACT component render
                        Y.later(100, this, this.align);
                        
                        //if we have edit rights - enable the edit button
                        //Set the button disabled attribute to match the inverse of the edit permission - 
                        this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', !permissions.canEdit);
                    });
            } else if (target.hasClass('remove')  && permissions.canRemove) {
                this.formMappingDialog.showView('removeFormMapping', {
                        model: new Y.app.RemoveFormMappingForm({
                            url: dialogConfig.removeConfig.url,
                            //ensure the ID is set so we can perform our remove
                            id: record.get('id')
                        }),
                        otherData: {
                            narrative: L.sub(dialogConfig.removeConfig.narrative, {
                                formType: destinationFormName,
                                name: record.get('name')
                            })
                        },
                        labels: dialogConfig.removeConfig.labels,
                        message: dialogConfig.removeConfig.message
                    },
                    function(view) {
                        //Get the button by name out of the footer and enable it.
                        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
            }else if ((target.hasClass('preferred')||target.hasClass('clearPreferred'))  && permissions.canEdit){
                var isClear=target.hasClass('clearPreferred'),
                    narrativeKey=(isClear)?dialogConfig.preferredConfig.clear.narrative:dialogConfig.preferredConfig.set.narrative,
                    messageKey=(isClear)?dialogConfig.preferredConfig.clear.message:dialogConfig.preferredConfig.set.message;
                
                this.formMappingDialog.showView('preferredFormMapping', {
                    model: new Y.app.PreferredFormMappingForm({
                        url: L.sub(dialogConfig.preferredConfig.url,{preferred:!isClear}),
                        //ensure the ID is set
                        id: record.get('id')
                    }),
                    otherData: {
                        narrative: L.sub(narrativeKey, {
                            formType: destinationFormName,
                            name: record.get('name')
                        }),
                        preferred:!isClear
                    },
                    labels: dialogConfig.preferredConfig.labels,
                    message:  L.sub(messageKey,{name: record.get('name')})
                },
                function(view) {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        }
    }, {
        ATTRS: {
            searchConfig: {
                value:{
                    url: {},
                    noDataMessage: {},
                    permissions: {},
                    resultsCountNode:{}
                }
            },
            buttonsConfig: {
                value:{
                    container: null,
                    template: null
                }
            },
            contextConfig: {
                value:{
                    container: null,
                    template: null,
                    model: null
                }
            },
            formMappingDialogConfig: {

            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'accordion-panel',
        'paginated-results-table',
        'results-formatters',
        'results-templates',
        'results-table-menu-plugin',
        'results-table-keyboard-nav-plugin',
        'content-context',
        'content-context-button',
        'form-mapping-dialog-views',
        'history',
        'usp-form-FormMappingWithControlMappings',
        'yui-later'
    ]
});