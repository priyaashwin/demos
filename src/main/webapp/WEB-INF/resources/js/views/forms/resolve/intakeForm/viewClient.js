YUI.add('intakeform-client-tab', function(Y) {
    var L = Y.Lang;

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormClientInformationView = Y.Base.create('intakeFormClientInformationView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [], {
        template: Y.Handlebars.templates['intakeFormClientInformationView']
    });

    Y.namespace('app').IntakeFormConsentView = Y.Base.create('intakeFormConsentView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormConsentView']
    });


    Y.namespace('app').IntakeFormClientInformationAccordion = Y.Base.create('intakeFormClientInformationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormClientInformationView = new Y.app.IntakeFormClientInformationView(config);

            // Add event targets
            this.intakeFormClientInformationView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormClientInformationAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormClientInformationView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormClientInformationView.destroy();

            delete this.intakeFormClientInformationView;
        }
    });

    Y.namespace('app').IntakeFormConsentAccordion = Y.Base.create('intakeFormConsentAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormConsentView = new Y.app.IntakeFormConsentView(config);

            // Add event targets
            this.intakeFormConsentView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormConsentAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormConsentView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormConsentView.destroy();

            delete this.intakeFormConsentView;
        }
    });


    Y.namespace('app').IntakeFormClientTab = Y.Base.create('intakeFormClientTab', Y.View, [], {
        initializer: function() {
            var informationConfig = this.get('informationConfig'),
                nextOfKinConfig = this.get('nextOfKinConfig'),
                mainCarerConfig = this.get('mainCarerConfig'),
                consentConfig = this.get('consentConfig'),
                peopleNotToBeContactedConfig = this.get('peopleNotToBeContactedConfig');

            //create the accordion views
            this.informationAccordion = new Y.app.IntakeFormClientInformationAccordion(
                Y.merge(
                    informationConfig, {
                        model: this.get('model')
                    }));

            this.nextOfKinAccordion = new Y.app.IntakeFormReferencedPersonAccordion(
                Y.merge(
                    nextOfKinConfig, {
                        model: new Y.usp.resolveassessment.IntakeReferencedPerson({
                            url: L.sub(nextOfKinConfig.url, {
                                id: this.get('model').get('id')
                            })
                        }).load(),
                        formModel: this.get('model'),
                        includeRole: true
                    }));

            this.mainCarerAccordion = new Y.app.IntakeFormReferencedPersonAccordion(
                Y.merge(
                    mainCarerConfig, {
                        model: new Y.usp.resolveassessment.IntakeReferencedPerson({
                            url: L.sub(mainCarerConfig.url, {
                                id: this.get('model').get('id')
                            })
                        }).load(),
                        formModel: this.get('model'),
                        includeRole: true
                    }));

            this.consentAccordion = new Y.app.IntakeFormConsentAccordion(
                Y.merge(
                    consentConfig, {
                        model: this.get('model')
                    }));

            this.peopleNotToBeContactedAccordion = new Y.app.IntakeFormReferencedPeopleAccordion(
                Y.merge(
                    peopleNotToBeContactedConfig, {
                        model: this.get('model'),
                        involvement: 'NOT_TO_BE_CONTACTED'
                    }));

            // Add event targets
            this.informationAccordion.addTarget(this);
            this.nextOfKinAccordion.addTarget(this);
            this.mainCarerAccordion.addTarget(this);
            this.consentAccordion.addTarget(this);
            this.peopleNotToBeContactedAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.informationAccordion.render().get('container'));
            contentNode.append(this.nextOfKinAccordion.render().get('container'));
            contentNode.append(this.mainCarerAccordion.render().get('container'));
            contentNode.append(this.consentAccordion.render().get('container'));
            contentNode.append(this.peopleNotToBeContactedAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties
            this.informationAccordion.destroy();
            delete this.informationAccordion;

            this.nextOfKinAccordion.destroy();
            delete this.nextOfKinAccordion;

            this.mainCarerAccordion.destroy();
            delete this.mainCarerAccordion;

            this.consentAccordion.destroy();
            delete this.consentAccordion;

            this.peopleNotToBeContactedAccordion.destroy();
            delete this.peopleNotToBeContactedAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            informationConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            nextOfKinConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            mainCarerConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            consentConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            peopleNotToBeContactedConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'intakeform-referenced-person-view',
               'usp-resolveassessment-IntakeReferencedPerson',
               'usp-resolveassessment-IntakeFormFullDetails'
              ]
});