YUI.add('prform-skills-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').PRFormSkillsView = Y.Base.create('prFormSkillsView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormSkillsView']
    });

    Y.namespace('app').PRFormSkillsAccordion = Y.Base.create('prFormSkillsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormSkills View and configure the template
            this.prFormSkillsView = new Y.app.PRFormSkillsView(Y.merge(config, {}));

            // Add event targets
            this.prFormSkillsView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormSkillsViewContainer = this.prFormSkillsView.render().get('container');

            //superclass call
            Y.app.PRFormSkillsAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormSkillsViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormSkillsView.destroy();

            delete this.prFormSkillsView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormIdentityView = Y.Base.create('prFormIdentityView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormIdentityView']
    });

    Y.namespace('app').PRFormIdentityAccordion = Y.Base.create('prFormIdentityAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormIdentity View and configure the template
            this.prFormIdentityView = new Y.app.PRFormIdentityView(Y.merge(config, {}));

            // Add event targets
            this.prFormIdentityView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormIdentityViewContainer = this.prFormIdentityView.render().get('container');

            //superclass call
            Y.app.PRFormIdentityAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormIdentityViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormIdentityView.destroy();

            delete this.prFormIdentityView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormLeisureView = Y.Base.create('prFormLeisureView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormLeisureView']
    });

    Y.namespace('app').PRFormLeisureAccordion = Y.Base.create('prFormLeisureAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormLeisure View and configure the template
            this.prFormLeisureView = new Y.app.PRFormLeisureView(Y.merge(config, {}));

            // Add event targets
            this.prFormLeisureView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormLeisureViewContainer = this.prFormLeisureView.render().get('container');

            //superclass call
            Y.app.PRFormLeisureAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormLeisureViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormLeisureView.destroy();

            delete this.prFormLeisureView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormSkillsTab = Y.Base.create('prFormSkillsTab', Y.View, [], {
        initializer: function() {
            var skillsConfig = this.get('skillsConfig'),
                identityConfig = this.get('identityConfig'),
                leisureConfig = this.get('leisureConfig');

            //create skills view
            this.skillsView = new Y.app.PRFormSkillsAccordion(Y.merge(skillsConfig, {
                model: this.get('model')
            }));
            this.identityView = new Y.app.PRFormIdentityAccordion(Y.merge(identityConfig, {
                model: this.get('model')
            }));
            this.leisureView = new Y.app.PRFormLeisureAccordion(Y.merge(leisureConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.skillsView.addTarget(this);
            this.identityView.addTarget(this);
            this.leisureView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.skillsView.render().get('container'));
            contentNode.append(this.identityView.render().get('container'));
            contentNode.append(this.leisureView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up skills view
            this.skillsView.destroy();
            delete this.skillsView;

            //clean up identity view
            this.identityView.destroy();
            delete this.identityView;

            //clean up leisure view
            this.leisureView.destroy();
            delete this.leisureView;
        }
    }, {
        ATTRS: {
            model: {},
            skillsConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            identityConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            leisureConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-prform-templates',
               'usp-resolveassessment-PlacementReport'
              ]
});