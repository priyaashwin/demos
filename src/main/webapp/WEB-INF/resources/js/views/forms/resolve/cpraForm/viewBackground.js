YUI.add('cpraform-background-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').CPRAFormIntroductionView = Y.Base.create('cpraFormIntroductionView', Y.usp.resolveassessment.CarePlanRiskAssessmentView, [], {
        template: Y.Handlebars.templates['cpraFormIntroductionView']
    });

    Y.namespace('app').CPRAFormBackgroundView = Y.Base.create('cpraFormBackgroundView', Y.usp.resolveassessment.CarePlanRiskAssessmentView, [], {
        template: Y.Handlebars.templates['cpraFormBackgroundView']
    });

    Y.namespace('app').CPRAFormHistoryView = Y.Base.create('cpraHistoryView', Y.usp.resolveassessment.CarePlanRiskAssessmentView, [], {
        template: Y.Handlebars.templates['cpraFormHistoryView']
    });

    Y.namespace('app').CPRAFormCommsAndRisksView = Y.Base.create('cpraFormCommsAndRisksView', Y.usp.resolveassessment.CarePlanRiskAssessmentView, [BaseFormView], {
        template: Y.Handlebars.templates['cpraFormCommsAndRisksView']
    }, {
        ATTRS: {
            codedEntries:{
                value:{
                    historicalRiskLevels:Y.usp.resolveassessment.enum.HistoricalRiskLevel.values
                }
            }
        }
    });

    Y.namespace('app').CPRAFormIntroductionAccordion = Y.Base.create('cpraFormIntroductionAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new CPRAFormIntroduction View and configure the template
            this.cpraFormIntroductionView = new Y.app.CPRAFormIntroductionView(config);

            // Add event targets
            this.cpraFormIntroductionView.addTarget(this);
        },
        render: function() {
            //render the view
            var cpraFormIntroductionViewContainer = this.cpraFormIntroductionView.render().get('container');

            //superclass call
            Y.app.CPRAFormIntroductionAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(cpraFormIntroductionViewContainer);

            return this;
        },
        destructor: function() {
            this.cpraFormIntroductionView.destroy();

            delete this.cpraFormIntroductionView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').CPRAFormBackgroundAccordion = Y.Base.create('cpraFormBackgroundAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new CPRAFormBackground View and configure the template
            this.cpraFormBackgroundView = new Y.app.CPRAFormBackgroundView(config);

            // Add event targets
            this.cpraFormBackgroundView.addTarget(this);
        },
        render: function() {
            //render the view
            var cpraFormBackgroundViewContainer = this.cpraFormBackgroundView.render().get('container');

            //superclass call
            Y.app.CPRAFormBackgroundAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(cpraFormBackgroundViewContainer);

            return this;
        },
        destructor: function() {
            this.cpraFormBackgroundView.destroy();

            delete this.cpraFormBackgroundView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').CPRAFormHistoryAccordion = Y.Base.create('cpraFormHistoryAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new CPRAFormBackground View and configure the template
            this.cpraFormHistoryView = new Y.app.CPRAFormHistoryView(config);

            // Add event targets
            this.cpraFormHistoryView.addTarget(this);
        },
        render: function() {
            //render the view
            var cpraFormHistoryViewContainer = this.cpraFormHistoryView.render().get('container');

            //superclass call
            Y.app.CPRAFormHistoryAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(cpraFormHistoryViewContainer);

            return this;
        },
        destructor: function() {
            this.cpraFormHistoryView.destroy();

            delete this.cpraFormHistoryView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').CPRAFormCommsAndRisksAccordion = Y.Base.create('cpraFormCommsAndRisksAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new CPRAFormBackground View and configure the template
            this.cpraFormCommsAndRisksView = new Y.app.CPRAFormCommsAndRisksView(config);

            // Add event targets
            this.cpraFormCommsAndRisksView.addTarget(this);
        },
        render: function() {
            //render the view
            var cpraFormCommsAndRisksViewContainer = this.cpraFormCommsAndRisksView.render().get('container');

            //superclass call
            Y.app.CPRAFormCommsAndRisksAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(cpraFormCommsAndRisksViewContainer);

            return this;
        },
        destructor: function() {
            this.cpraFormCommsAndRisksView.destroy();

            delete this.cpraFormCommsAndRisksView;
        }
    });

    Y.namespace('app').CPRAFormBackgroundTab = Y.Base.create('cpraFormBackgroundTab', Y.View, [], {
        initializer: function() {
            var introductionConfig = this.get('introductionConfig'),
                backgroundConfig = this.get('backgroundConfig'),
                historyConfig = this.get('historyConfig'),
                risksConfig = this.get('risksConfig');

            //create background view
            this.introductionView = new Y.app.CPRAFormIntroductionAccordion(
                Y.merge(
                    introductionConfig, {
                        model: this.get('model')
                    }));

            this.backgroundView = new Y.app.CPRAFormBackgroundAccordion(
                Y.merge(
                    backgroundConfig, {
                        model: this.get('model')
                    }));

            this.historyView = new Y.app.CPRAFormHistoryAccordion(
                Y.merge(
                    historyConfig, {
                        model: this.get('model')
                    }));

            this.risksView = new Y.app.CPRAFormCommsAndRisksAccordion(
                Y.merge(
                    risksConfig, {
                        model: this.get('model')
                    }));

            // Add event targets
            this.introductionView.addTarget(this);
            this.backgroundView.addTarget(this);
            this.historyView.addTarget(this);
            this.risksView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.introductionView.render().get('container'));
            contentNode.append(this.backgroundView.render().get('container'));
            contentNode.append(this.historyView.render().get('container'));
            contentNode.append(this.risksView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up introduction view
            this.introductionView.destroy();
            delete this.introductionView;

            //clean up background view
            this.backgroundView.destroy();
            delete this.backgroundView;

            //clean up background view
            this.historyView.destroy();
            delete this.historyView;

            //clean up background view
            this.risksView.destroy();
            delete this.risksView;
        }
    }, {
        ATTRS: {
            model: {},
            introductionConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            backgroundConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            historyConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            risksConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-cpraform-templates',
               'usp-resolveassessment-CarePlanRiskAssessment',
               'resolveassessment-component-enumerations'
              ]
});