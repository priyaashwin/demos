YUI.add('add-form-filter', function (Y) {
    'use-strict';

    Y.namespace('app.form').AddFormFilterModel = Y.Base.create('addFormFilterModel', Y.usp.ResultsFilterModel, [], {}, {
        ATTRS: {
            name: {
                USPType: 'String',
                value: ''
            }
        }
    });

    Y.namespace('app.form').AddFormFilterView = Y.Base.create('addFormFilterView', Y.usp.app.Filter, [], {
        filterModelType: Y.app.form.AddFormFilterModel,
        filterTemplate: Y.Handlebars.templates['addFormFilter'],
        filterContextTemplate: Y.Handlebars.templates['addFormActiveFilter']
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-filter',
        'results-filter',
        'handlebars-helpers',
        'handlebars-form-templates'
    ]
});