YUI.add('form-add-buttons', function(Y) {
    var A = Y.Array;

    Y.namespace('app').FormAddContextButton = Y.Base.create('formAddContextButton', Y.app.ContentContextButton, [], {
        containerTemplate: '<li />',
        render: function() {

            //NOTE this method will be called for every model change
            if (this.get('name') === 'addPR') {
                if (this.get('canAdd') === true && this.get('model').get('canAddPR') === true) {
                    this.enable();
                } else {
                    this.disable();
                }
            } else if (this.get('name') === 'addCPRA') {
                if (this.get('canAdd') === true && this.get('model').get('canAddCPRA') === true) {
                    this.enable();
                } else {
                    this.disable();
                }
            } else if (this.get('name') === 'addIntake') {
                if (this.get('canAdd') === true && this.get('model').get('canAddIntake') === true) {
                    this.enable();
                } else {
                    this.disable();
                }
            }
            // Call the superclass to actually do the render
            Y.app.FormAddContextButton.superclass.render.call(this);

            return this;
        }
    }, {
        ATTRS: {
            canAdd: {
                value: false
            }
        }
    });

    Y.namespace('app').FormAddButtons = Y.Base.create('formAddButtons', Y.View, [], {
        initializer: function() {
            var buttonsConfig = this.get('buttonsConfig');

            //array for buttons
            this.buttons = [

                //Add Intake button
                new Y.app.FormAddContextButton(Y.mix({
                    name: 'addIntake',
                    model: this.get('model')
                }, buttonsConfig.addIntake)),

                //Add PR button    
                new Y.app.FormAddContextButton(Y.mix({
                    name: 'addPR',
                    model: this.get('model')
                }, buttonsConfig.addPR)),

                //Add CPRA button
                new Y.app.FormAddContextButton(Y.mix({
                    name: 'addCPRA',
                    model: this.get('model')
                }, buttonsConfig.addCPRA))
            ];

            //add button bubble targets
            A.each(this.buttons, function(button) {
                button.addTarget(this);
            }, this);
        },

        render: function() {

            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            //render buttons
            A.each(this.buttons, function(button) {
                contentNode.append(button.render().get('container'));
            });
            //finally set the content into the container
            this.get('container').append(contentNode);
            return this;
        },
        destructor: function() {
            //destroy buttons
            A.each(this.buttons, function(button) {
                button.destroy();
            });
            //null out
            this.buttons = null;
        },
        /**
         * Turns off all buttons
         */
        disableButtons:function(){
            A.each(this.buttons, function(button) {
                button.disable();
            });
        }
    }, {
        ATTRS: {
            buttonsConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'content-context-button',
               'formstatus-dialog-views'
            ]
});