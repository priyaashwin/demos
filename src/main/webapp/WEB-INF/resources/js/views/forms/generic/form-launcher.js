YUI.add('form-launcher', function(Y) {
    var L = Y.Lang;
    
    /**
     * FormLauncher provides a static helper method to launch a form;
     * 
     * @class FormLauncher
     * @namespace usp.forms
     */

    function FormLauncher(config) {}

    /**
     * Opens a form
     * @method open
     * @param {Number} id the id of the form to open
     * @param {String} formName the name of the form type
     * @param {String} controlId optional localId of a control to deep link to
     */
    FormLauncher.open = function(formURL, id, formName, memberId, controlId) {
        if(memberId){
            formURL=formURL+'&memberId='+memberId;
        }
        if (controlId){
            formURL=formURL+'&controlId='+controlId;
        }
        window.open( 
            L.sub(formURL,{id:id}),  
            formName+'_'+id,
            'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1024,height=768,left=100,top=100'
        );      
    };
    Y.namespace('app').FormLauncher = FormLauncher;
}, '0.0.1', {
    requires: ['yui-base']
});