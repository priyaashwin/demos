YUI.add('prform-summary-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').PRFormSummaryView = Y.Base.create('prFormSummaryView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormSummaryView']
    });

    Y.namespace('app').PRFormSummaryAccordion = Y.Base.create('prFormSummaryAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormSummary View and configure the template
            this.prFormSummaryView = new Y.app.PRFormSummaryView(Y.merge(config, {}));

            // Add event targets
            this.prFormSummaryView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormSummaryViewContainer = this.prFormSummaryView.render().get('container');

            //superclass call
            Y.app.PRFormSummaryAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormSummaryViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormSummaryView.destroy();

            delete this.prFormSummaryView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormSummaryTab = Y.Base.create('prFormSummaryTab', Y.View, [], {
        initializer: function() {
            var summaryConfig = this.get('summaryConfig');

            //create summary view
            this.summaryView = new Y.app.PRFormSummaryAccordion(Y.merge(summaryConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.summaryView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.summaryView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up summary view
            this.summaryView.destroy();
            delete this.summaryView;
        }
    }, {
        ATTRS: {
            model: {},
            summaryConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-prform-templates',
               'usp-resolveassessment-PlacementReport']
});