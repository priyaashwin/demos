YUI.add('form-instance-context', function(Y) {
    var FormInstanceContext = Y.Base.create('formInstanceContext', Y.app.ContentContext, [], {
        formContextTemplate: Y.Handlebars.templates["formViewContext"],
        render: function() {
           var container=this.get('container');

           //now render the form context
           container.setHTML(this.formContextTemplate({
               labels:this.get('labels'),
               modelData:this.get('model').toJSON()
           }));
           
          return this;
        }
    }, {
        ATTRS: {
            formModel: {}
        }
    });
    Y.namespace('app').FormInstanceContext = FormInstanceContext;
}, '0.0.1', {
    requires: ['yui-base',
               'content-context',
		       'handlebars-helpers',
               'handlebars-form-templates']
});