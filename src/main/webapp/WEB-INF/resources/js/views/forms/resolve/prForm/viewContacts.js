YUI.add('prform-contacts-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').PRFormContactsView = Y.Base.create('prFormContactsView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormContactsView']
    });

    Y.namespace('app').PRFormContactsAccordion = Y.Base.create('prFormContactsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormContacts View and configure the template
            this.prFormContactsView = new Y.app.PRFormContactsView(Y.merge(config, {}));

            // Add event targets
            this.prFormContactsView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormContactsViewContainer = this.prFormContactsView.render().get('container');

            //superclass call
            Y.app.PRFormContactsAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormContactsViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormContactsView.destroy();

            delete this.prFormContactsView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormContactsTab = Y.Base.create('prFormContactsTab', Y.View, [], {
        initializer: function() {
            var contactsConfig = this.get('contactsConfig');

            //create contacts view
            this.contactsView = new Y.app.PRFormContactsAccordion(Y.merge(contactsConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.contactsView.addTarget(this);

        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.contactsView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up contacts view
            this.contactsView.destroy();
            delete this.contactsView;
        }
    }, {
        ATTRS: {
            model: {},
            contactsConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-prform-templates',
               'usp-resolveassessment-PlacementReport'
              ]
});