YUI.add('intakeform-referenced-person-view', function(Y) {
    var A = Y.Array,
        L = Y.Lang,
        ADD_BUTTON_TEMPLATE = '<div class="pull-right"><button class="pure-button-active pure-button {id}" disabled="disabled"><i class="fa fa-plus"></i> {label}</button></div>';


    Y.namespace('app').IntakeFormReferencedPersonView = Y.Base.create('intakeFormReferencedPersonView', Y.usp.resolveassessment.IntakeReferencedPersonView, [], {
        initializer: function() {
            var formModel = this.get('formModel');

            // force a re-render on the form model change
            formModel.after('*:change', this.render, this);
        },
        events: {
            '.edit-form': {
                click: '_handleEditClick'
            }
        },
        template: Y.Handlebars.templates['intakeFormReferencedPersonView'],
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    otherData: this.get('otherData'),
                    formModel: this.get('formModel').toJSON(),
                    codedEntries: this.get('codedEntries')
                });

            // Render this view's HTML into the container element.
            container.setHTML(html);

            return this;
        },
        _handleEditClick: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('edit', {
                id: this.get('model').get('id')
            });
        }
    }, {
        ATTRS: {
            formModel: {}
        }
    });

    Y.namespace('app').IntakeFormReferencedPersonWithRoleView = Y.Base.create('intakeFormReferencedPersonWithRoleView', Y.app.IntakeFormReferencedPersonView, [], {
        template: Y.Handlebars.templates['intakeFormReferencedPersonWithRoleView']
    });

    Y.namespace('app').IntakeFormReferencedPersonAccordion = Y.Base.create('intakeFormReferencedPersonAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            var includeRole = this.get('includeRole');

            //Create view passing on configuration object
            this.intakeFormReferencedPersonView = includeRole === true ? new Y.app.IntakeFormReferencedPersonWithRoleView(config) : new Y.app.IntakeFormReferencedPersonView(config);

            // Add event targets
            this.intakeFormReferencedPersonView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormReferencedPersonAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormReferencedPersonView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormReferencedPersonView.destroy();

            delete this.intakeFormReferencedPersonView;
        }
    }, {
        ATTRS: {
            includeRole: {
                value: false
            }
        }
    });

    Y.namespace('app').IntakeFormReferencedPeopleView = Y.Base.create('intakeFormReferencedPeopleView', Y.View, [], {
        template: Y.Handlebars.templates['intakeFormReferencedPeopleView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {}
        }
    });

    Y.namespace('app').IntakeFormReferencedPeopleTable = Y.Base.create('intakeFormReferencedPeopleTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                formModel = this.get('model'),
                modelList = new Y.usp.resolveassessment.IntakeReferencedPersonList({
                    url: L.sub(this.get('url'), {
                        id: formModel.get('id')
                    })
                });

            this.results = new Y.app.IntakeFormReferencedPeopleView({
                modelList: modelList,
                formModel: formModel,
                labels: config.labels
            });

            this._refPersonEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.referencedPerson .pure-button', this)
            ];

            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            formModel.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('labels').addButton;

            // superclass call
            Y.app.IntakeFormReferencedPeopleTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._refPersonEvtHandlers) {
                A.each(this._refPersonEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._refPersonEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }
        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event
                    this.fire('add', {
                        id: this.get('model').get('id'),
                        involvement: this.get('involvement')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id'),
                    involvement: record.get('involvement')
                });
            } else if (target.hasClass('remove')) {
                this.fire('remove', {
                    id: record.get('id'),
                    involvement: record.get('involvement')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading referenced people list
             */
            url: {},
            /**
             * @Attribute labels - The view labels
             */
            labels: {},
            involvement: {},
            involvementName: {}
        }
    });


    Y.namespace('app').IntakeFormReferencedPeopleAccordion = Y.Base.create('intakeFormReferencedPeopleAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //setup referenced people table
            this.referencedPeopleTable = new Y.app.IntakeFormReferencedPeopleTable(Y.merge(config.referencedPeopleConfig, {
                model: this.get('model'),
                //the involvement enum value
                involvement: config.involvement,
                involvementName: config.involvementName
            }));

            // Add event targets
            this.referencedPeopleTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.referencedPeopleTable.render().get('container'));

            //superclass call
            Y.app.IntakeFormReferencedPeopleAccordion.superclass.render.call(this);

            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.referencedPeopleTable.destroy();
            delete this.referencedPeopleTable;
        },
        reload: function() {
            this.referencedPeopleTable.reload();
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeReferencedPerson'
              ]
});