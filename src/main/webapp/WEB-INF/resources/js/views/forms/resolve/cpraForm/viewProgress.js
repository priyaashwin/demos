YUI.add('cpraform-progress-tab', function(Y) {
    var A = Y.Array,
        L = Y.Lang;

    Y.namespace('app').ProgressReportView = Y.Base.create('progressReportView', Y.View, [], {
        template: Y.Handlebars.templates['cpraFormProgressReportView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels'),
                enumTypes: this.get('enumTypes')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            enumTypes: {
                value: {
                    planType: Y.usp.resolveassessment.enum.CarePlanType.values
                }
            }
        }
    });

    Y.namespace('app').ProgressReportTable = Y.Base.create('progressReportTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                modelList = new Y.usp.resolveassessment.ProgressReportList({
                    url: L.sub(this.get('url'), {
                        id: this.get('model').get('id')
                    })
                });

            this.results = new Y.app.ProgressReportView({
                modelList: modelList,
                formModel: this.get('model'),
                labels: config.labels
            });

            this._progressEvtHandlers = [
                container.delegate('click', this._handleRowClick, '.progressReport .pure-button', this)
            ];

            //load model list
            modelList.load();
        },
        render: function() {
            // superclass call
            Y.app.ProgressReportTable.superclass.render.call(this);

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._progressEvtHandlers) {
                A.each(this._progressEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._progressEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading risks
             */
            url: {}
        }
    });

    Y.namespace('app').ProgressReportAccordion = Y.Base.create('progressReportAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //setup progress table
            this.progressReportTable = new Y.app.ProgressReportTable(Y.merge(config.progressReportConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.progressReportTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.progressReportTable.render().get('container'));

            //superclass call
            Y.app.ProgressReportAccordion.superclass.render.call(this);
            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.progressReportTable.destroy();
            delete this.progressReportTable;
        },
        reload: function() {
            this.progressReportTable.reload();
        }
    });

    Y.namespace('app').CPRAFormProgressTab = Y.Base.create('cpraFormProgressTab', Y.View, [], {
        initializer: function(config) {
            //setup progress accordion
            this.progressReportAccordion = new Y.app.ProgressReportAccordion(Y.merge(config.progressConfig, {
                model: this.get('model')
            }));
            this.progressReportAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.progressReportAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this.progressReportAccordion.destroy();
            delete this.progressReportAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            progressConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-cpraform-templates',
               'usp-resolveassessment-ProgressReport'
              ]
});