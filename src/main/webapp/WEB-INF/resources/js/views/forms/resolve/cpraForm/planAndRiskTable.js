YUI.add('cpraform-plan-and-risk-table', function(Y) {
    Y.namespace('app').CPRAPlanAndRiskTable = Y.Base.create('cpraPlanAndRiskTable', Y.View, [], {
        initializer: function(config) {
            //setup plan table
            this.planAccordion = new Y.app.PlanAccordion(Y.merge(config.supportPlan, {
                planType: this.get('planType'),
                model: this.get('model')
            }));
            this.planAccordion.addTarget(this);

            //setup a set of risk accordions
            this.riskSetAccordion = new Y.app.RiskSetAccordion(Y.merge(config.identifiedRisks, {
                planType: this.get('planType'),
                model: this.get('model')
            }));
            this.riskSetAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.planAccordion.render().get('container'));

            contentNode.append(this.riskSetAccordion.render().get('container'));
            //finally set the content into the container
            this.get('container').setHTML(contentNode);
            return this;
        },
        destructor: function() {
            this.planAccordion.destroy();
            delete this.planAccordion;

            this.riskSetAccordion.destroy();
            delete this.riskSetAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            planType: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'cpraform-risks-view',
               'cpraform-plan-view'
              ]
});