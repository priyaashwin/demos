YUI.add('cpraform-tabs', function(Y) {
    var L = Y.Lang;
    Y.namespace('app').CPRAFormTabsView = Y.Base.create('cpraFormTabsView', Y.usp.app.AppTabbedPage, [Y.app.TabNavigation], {
        views: {
            background: {
                type: Y.app.CPRAFormBackgroundTab
            },
            health: {
                type: Y.app.CPRAFormHealthTab
            },
            emotional: {
                type: Y.app.CPRAFormEmotionalTab
            },
            identity: {
                type: Y.app.CPRAFormIdentityTab
            },
            selfcare: {
                type: Y.app.CPRAFormSelfCareTab
            },
            ils: {
                type: Y.app.CPRAFormILSTab
            },
            daycare: {
                type: Y.app.CPRAFormDayCareTab
            },
            finance: {
                type: Y.app.CPRAFormFinanceTab
            },
            consultation: {
                type: Y.app.CPRAFormConsultationTab
            },
            specialists: {
                type: Y.app.CPRAFormSpecialistsTab
            },
            progress: {
                type: Y.app.CPRAFormProgressTab
            },
            views: {
                type: Y.app.CPRAFormViewsTab
            },
            rejection: {
                type: Y.app.CPRAFormRejectionTab
            }
        },
        initializer: function(config) {
            var dialogConfig = this.get('dialogConfig'),
                formName = dialogConfig.formName,
                successMessage = dialogConfig.successMessage,
                keyAreaSuccessMessage = dialogConfig.supportPlanConfig.edit.successMessage,
                progressReportSuccessMessage = dialogConfig.progressReportConfig.edit.successMessage,
                _self = this;

            //create form dialog
            this.formDialog = new Y.app.CPRAFormDialog({
                headerContent: dialogConfig.defaultHeader,
                /* !!! Merge with existing view configuration defined in cpraform.js !!! */
                views: {
                    editCPRAFormCommsAndRisks: {
                        headerTemplate: dialogConfig.commsAndRisksConfig.edit.headerTemplate
                    },
                    editCPRAFormServiceUser: {
                        headerTemplate: dialogConfig.viewsConfig.edit.headerTemplate
                    },
                    editCPRAFormRelative: {
                        headerTemplate: dialogConfig.relativeConfig.edit.headerTemplate
                    },
                    editCPRAFormCarersPerspective: {
                        headerTemplate: dialogConfig.carersPerspectiveConfig.edit.headerTemplate
                    },
                    editCPRAFormRejection: {
                        headerTemplate: dialogConfig.rejectionConfig.edit.headerTemplate
                    },
                    addIdentifiedRisk: {
                        headerTemplate: dialogConfig.identifierRiskConfig.add.headerTemplate
                    },
                    editIdentifiedRisk: {
                        headerTemplate: dialogConfig.identifierRiskConfig.edit.headerTemplate
                    },
                    removeIdentifiedRisk: {
                        headerTemplate: dialogConfig.identifierRiskConfig.remove.headerTemplate
                    },
                    editKeyArea: {
                        headerTemplate: dialogConfig.supportPlanConfig.edit.headerTemplate
                    },
                    editProgressReport: {
                        headerTemplate: dialogConfig.progressReportConfig.edit.headerTemplate
                    }
                }
            });

            this.on('cpraFormCommsAndRisksView:editForm', function(e, config) {
                this.showView('editCPRAFormCommsAndRisks', {
                    labels: config.labels,
                    model: new Y.app.UpdateCarePlanRiskAssessmentForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.commsAndRisksConfig.edit);

            //setup event handlers for view based events
            this.on('cpraFormServiceUserView:editForm', function(e, config) {
                this.showView('editCPRAFormServiceUser', {
                    labels: config.labels,
                    model: new Y.app.UpdateCarePlanRiskAssessmentForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.viewsConfig.edit);

            this.on('cpraFormRelativeView:editForm', function(e, config) {
                this.showView('editCPRAFormRelative', {
                    labels: config.labels,
                    model: new Y.app.UpdateCarePlanRiskAssessmentForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.relativeConfig.edit);

            this.on('cpraFormCarersPerspectiveView:editForm', function(e, config) {
                this.showView('editCPRAFormCarersPerspective', {
                    labels: config.labels,
                    model: new Y.app.UpdateCarePlanRiskAssessmentForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.carersPerspectiveConfig.edit);

            this.on('cpraFormRejectionView:editForm', function(e, config, tabs) {
                this.showView('editCPRAFormRejection', {
                    labels: config.labels,
                    model: new Y.app.UpdateCarePlanRiskAssessmentFormRejectionForm({
                        url: config.url,
                        subject: tabs.get('activeView').get('model').get('subject')
                    }),
                    saveUrl: config.saveUrl,
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.rejectionConfig.edit, this);

            //subscribe to add risk event
            this.on('riskTable:add', function(e, config) {
                this.showView('addIdentifiedRisk', {
                    labels: config.labels,
                    model: new Y.app.AddIdentifiedRiskForm({
                        url: L.sub(config.url, {
                            id: e.id
                        }),
                        planType: e.planType,
                        type: e.riskType
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName,
                            narrativeRiskType: e.narrativeRiskType
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage,
                    riskType: e.riskType
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.identifierRiskConfig.add);
            //subscribe to edit risk event
            this.on('riskTable:edit', function(e, config) {
                this.showView('editIdentifiedRisk', {
                    labels: config.labels,
                    model: new Y.app.UpdateIdentifiedRiskForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName,
                            narrativeRiskType: e.narrativeRiskType
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage,
                    riskType: e.riskType
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.identifierRiskConfig.edit);
            //subscribe to remove risk event
            this.on('riskTable:remove', function(e, config) {
                this.showView('removeIdentifiedRisk', {
                    model: new Y.app.RemoveIdentifiedRiskForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        message: config.message,
                        narrative: L.sub(config.narrative, {
                            formName: formName,
                            narrativeRiskType: e.narrativeRiskType
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage,
                    riskType: e.riskType
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.identifierRiskConfig.remove);

            //subscribe to edit key area event
            this.on('planTable:edit', function(e, config) {
                this.showView('editKeyArea', {
                    labels: config.labels,
                    model: new Y.app.UpdateKeyAreaForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName,
                            planName: e.planName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.supportPlanConfig.edit);


            //subscribe to edit progress report event
            this.on('progressReportTable:edit', function(e, config) {
                this.showView('editProgressReport', {
                    labels: config.labels,
                    model: new Y.app.UpdateProgressReportForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.progressReportConfig.edit);

            //setup this as a bubble target for the form dalog
            this.formDialog.addTarget(this);
            //listen for riskTable changes via formDialog
            this.formDialog.on(['*:savedRisk', '*:removedRisk'], function(e) {
                if (e.riskType) {
                    this.get('activeView').riskSetAccordion.reload(e.riskType);
                }
            }, this);

            //listen for keyarea changes via formDialog
            this.formDialog.on('*:savedKeyArea', function(e) {
                var view = this.get('activeView'),
                    planTable = view.planAccordion || view.planTable;

                //reload
                planTable.reload();

                Y.fire('infoMessage:message', {
                    message: keyAreaSuccessMessage
                });
            }, this);

            //listen for progress report changes via formDialog
            this.formDialog.on('*:savedProgressReport', function(e) {
                this.get('activeView').progressReportAccordion.reload();

                Y.fire('infoMessage:message', {
                    message: progressReportSuccessMessage
                });
            }, this);

            /** Success messages **/
            //listen for any save event after an edit
            this.on('*:saved', function(e) {
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: successMessage
                });
            });

            this.on('addIdentifiedRiskView:savedRisk', function(e) {
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: this.successMessage
                });

            }, dialogConfig.identifierRiskConfig.add);

            this.on('editIdentifiedRiskView:savedRisk', function(e) {
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: this.successMessage
                });

            }, dialogConfig.identifierRiskConfig.edit);

            this.on('removeIdentifiedRiskView:removedRisk', function(e) {
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: this.successMessage
                });

            }, dialogConfig.identifierRiskConfig.remove);
        },
        render: function() {
            //superclass call
            Y.app.CPRAFormTabsView.superclass.render.call(this);
            //render the dialog
            this.formDialog.render();
            return this;
        },
        destructor: function() {
            //clean up dialog
            this.formDialog.destroy();
            delete this.formDialog;
        }
    }, {
        ATTRS: {
            dialogConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'app-tabbed-page',
               'tab-navigation',
               'cpraform-background-tab',
               'cpraform-plan-and-risks-tabs',
               'cpraform-views-tab',
               'cpraform-rejection-tab',
               'cpraform-progress-tab',
               'cpraform-dialog-views'
              ]
});