YUI.add('intakeform-health-tab', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        ADD_BUTTON_TEMPLATE = '<div class="pull-right"><button class="pure-button-active pure-button {id}" disabled="disabled"><i class="fa fa-plus"></i> {label}</button></div>';

    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormHealthView = Y.Base.create('intakeFormHealthView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormHealthView']
    });

    Y.namespace('app').IntakeFormGPView = Y.Base.create('intakeFormGPView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormGPView']
    });

    Y.namespace('app').IntakeFormHealthActionPlanView = Y.Base.create('intakeFormHealthActionPlanView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormHealthActionPlanView']
    });

    Y.namespace('app').IntakeFormLifestyleView = Y.Base.create('intakeFormLifestyleView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormLifestyleView']
    });

    Y.namespace('app').MedicationView = Y.Base.create('medicationView', Y.View, [], {
        template: Y.Handlebars.templates['intakeFormMedicationView'],
        initializer: function() {
            var list = this.get('modelList'),
                formModel = this.get('formModel');

            // Re-render this view when a model is added to or removed from the model
            // list.
            list.after(['add', 'remove', 'reset'], this.render, this);

            // We'll also re-render the view whenever the data of one of the models in
            // the list changes.
            list.after('*:change', this.render, this);

            // We'll also re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                template = this.template;

            container.setHTML(template({
                modelList: this.get('modelList').toJSON(),
                formModel: this.get('formModel').toJSON(),
                labels: this.get('labels')
            }));

            return this;
        }
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {}
        }
    });

    Y.namespace('app').MedicationTable = Y.Base.create('medicationTable', Y.usp.SimplePanel, [], {
        initializer: function(config) {
            var container = this.get('container'),
                model = this.get('model'),
                modelList = new Y.usp.resolveassessment.IntakeMedicationsList({
                    url: L.sub(this.get('url'), {
                        id: model.get('id')
                    })
                });

            this.results = new Y.app.MedicationView({
                modelList: modelList,
                formModel: model,
                labels: config.labels
            });

            this._medicationsEvtHandlers = [
                container.delegate('click', this._handleButtonClick, '.table-header .pure-button', this),
                container.delegate('click', this._handleRowClick, '.medication .pure-button', this)
            ];

            //publish events
            this.publish('add', {
                preventable: true
            });

            //load model list
            modelList.load();

            // update the button state as a result of the parent model change
            model.after('*:change', this._updateButtonState, this);
        },
        _updateButtonState: function() {
            if (this.addButton) {
                if (this.get('model').get('canUpdate') === true) {
                    this.addButton.removeAttribute('disabled');
                } else {
                    this.addButton.setAttribute('disabled', 'disabled');
                }
            }
        },
        render: function() {
            var label = this.get('labels').addButton;

            // superclass call
            Y.app.MedicationTable.superclass.render.call(this);


            //Add the button into the header
            this.addButton = this.get('buttonHolder').appendChild(L.sub(ADD_BUTTON_TEMPLATE, {
                id: 'add',
                label: label
            })).one('.add');

            //ensure button is set to correct state (i.e. can we add)
            this._updateButtonState();

            //render results
            this.get('panelContent').append(this.results.render().get('container'));

            return this;
        },
        destructor: function() {
            //unbind event handles
            if (this._medicationsEvtHandlers) {
                A.each(this._medicationsEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._medicationsEvtHandlers = null;
            }

            this.results.destroy();
            delete this.results;

            if (this.addButton) {
                this.addButton.destroy();
                delete this.addButton;
            }
        },
        _handleButtonClick: function(e) {
            var target = e.currentTarget;
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('add')) {
                if (this.get('model').get('canUpdate') === true) {
                    //fire add event
                    this.fire('add', {
                        id: this.get('model').get('id')
                    });
                }
            }
        },
        _handleRowClick: function(e) {
            //click handler for table row
            var target = e.currentTarget,
                cId = target.getData("id"),
                record = this.results.get('modelList').getById(cId);
            e.preventDefault();
            //check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) {
                this.fire('edit', {
                    id: record.get('id')
                });
            } else if (target.hasClass('remove')) {
                this.fire('remove', {
                    id: record.get('id')
                });
            }
        },
        reload: function() {
            this.results.get('modelList').load();
        }
    }, {
        ATTRS: {
            /**
             * @Attribute url - base URL for loading placement history
             */
            url: {},
            /**
             * @Attribute labels - The view labels
             */
            labels: {}
        }
    });

    Y.namespace('app').IntakeFormHealthAccordion = Y.Base.create('intakeFormHealthAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormHealthView = new Y.app.IntakeFormHealthView(config);

            // Add event targets
            this.intakeFormHealthView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormHealthAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormHealthView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormHealthView.destroy();

            delete this.intakeFormHealthView;
        }
    });

    Y.namespace('app').IntakeFormGPAccordion = Y.Base.create('intakeFormGPAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormGPView = new Y.app.IntakeFormGPView(config);

            // Add event targets
            this.intakeFormGPView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormGPAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormGPView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormGPView.destroy();

            delete this.intakeFormGPView;
        }
    });


    Y.namespace('app').IntakeFormMedicationsAccordion = Y.Base.create('intakeFormMedicationsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //setup medication table
            this.medicationTable = new Y.app.MedicationTable(Y.merge(config.medicationConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.medicationTable.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.medicationTable.render().get('container'));

            //superclass call
            Y.app.IntakeFormMedicationsAccordion.superclass.render.call(this);

            //stick the fragment into the accordion
            this.getAccordionBody().appendChild(contentNode);
            return this;
        },
        destructor: function() {
            this.medicationTable.destroy();
            delete this.medicationTable;
        },
        reload: function() {
            this.medicationTable.reload();
        }
    });

    Y.namespace('app').IntakeFormHealthActionPlanAccordion = Y.Base.create('intakeFormHealthActionPlanAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormHealthActionPlanView = new Y.app.IntakeFormHealthActionPlanView(config);

            // Add event targets
            this.intakeFormHealthActionPlanView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormHealthActionPlanAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormHealthActionPlanView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormHealthActionPlanView.destroy();

            delete this.intakeFormHealthActionPlanView;
        }
    });

    Y.namespace('app').IntakeFormLifestyleAccordion = Y.Base.create('intakeFormLifestyleAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormLifestyleView = new Y.app.IntakeFormLifestyleView(config);

            // Add event targets
            this.intakeFormLifestyleView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormLifestyleAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormLifestyleView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormLifestyleView.destroy();

            delete this.intakeFormLifestyleView;
        }
    });

    Y.namespace('app').IntakeFormHealthTab = Y.Base.create('intakeFormHealthTab', Y.View, [], {
        initializer: function() {
            var healthConfig = this.get('healthConfig'),
                gpConfig = this.get('gpConfig'),
                professionalsConfig = this.get('professionalsConfig'),
                medicationsConfig = this.get('medicationsConfig'),
                healthActionPlanConfig = this.get('healthActionPlanConfig'),
                lifestyleConfig = this.get('lifestyleConfig');

            //create the accordion views
            this.healthAccordion = new Y.app.IntakeFormHealthAccordion(
                Y.merge(
                    healthConfig, {
                        model: this.get('model')
                    }));

            this.gpAccordion = new Y.app.IntakeFormGPAccordion(
                Y.merge(
                    gpConfig, {
                        model: this.get('model')
                    }));

            this.professionalsAccordion = new Y.app.IntakeFormReferencedPeopleAccordion(
                Y.merge(
                    professionalsConfig, {
                        model: this.get('model'),
                        involvement: 'PROFESSIONAL'
                    }));

            this.medicationsAccordion = new Y.app.IntakeFormMedicationsAccordion(
                Y.merge(
                    medicationsConfig, {
                        model: this.get('model')
                    }));

            this.healthActionPlanAccordion = new Y.app.IntakeFormHealthActionPlanAccordion(
                Y.merge(
                    healthActionPlanConfig, {
                        model: this.get('model')
                    }));


            this.intakeFormLifestyleAccordion = new Y.app.IntakeFormLifestyleAccordion(
                Y.merge(
                    lifestyleConfig, {
                        model: this.get('model')
                    }));

            // Add event targets            
            this.healthAccordion.addTarget(this);
            this.gpAccordion.addTarget(this);
            this.professionalsAccordion.addTarget(this);
            this.medicationsAccordion.addTarget(this);
            this.healthActionPlanAccordion.addTarget(this);
            this.intakeFormLifestyleAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.healthAccordion.render().get('container'));
            contentNode.append(this.gpAccordion.render().get('container'));
            contentNode.append(this.professionalsAccordion.render().get('container'));
            contentNode.append(this.medicationsAccordion.render().get('container'));
            contentNode.append(this.healthActionPlanAccordion.render().get('container'));
            contentNode.append(this.intakeFormLifestyleAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties            
            this.healthAccordion.destroy();
            delete this.healthAccordion;

            this.gpAccordion.destroy();
            delete this.gpAccordion;

            this.professionalsAccordion.destroy();
            delete this.professionalsAccordion;

            this.medicationsAccordion.destroy();
            delete this.medicationsAccordion;

            this.healthActionPlanAccordion.destroy();
            delete this.healthActionPlanAccordion;

            this.intakeFormLifestyleAccordion.destroy();
            delete this.intakeFormLifestyleAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            healthConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            gpConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            professionalsConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            medicationsConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            healthActionPlanConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            lifestyleConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeFormFullDetails'
              ]
});