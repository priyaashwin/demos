YUI.add('prform-educationemployment-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };
    
    Y.namespace('app').PRFormEducationEmploymentView = Y.Base.create('prFormEducationEmploymentView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormEducationEmploymentView']
    });

    Y.namespace('app').PRFormEducationEmploymentAccordion = Y.Base.create('prFormEducationEmploymentAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormEducationEmployment View and configure the template
            this.prFormEducationEmploymentView = new Y.app.PRFormEducationEmploymentView(Y.merge(config, {}));

            // Add event targets
            this.prFormEducationEmploymentView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormEducationEmploymentViewContainer = this.prFormEducationEmploymentView.render().get('container');

            //superclass call
            Y.app.PRFormEducationEmploymentAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormEducationEmploymentViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormEducationEmploymentView.destroy();

            delete this.prFormEducationEmploymentView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormSocialView = Y.Base.create('prFormSocialView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {        
        template: Y.Handlebars.templates['prFormSocialView']
    });

    Y.namespace('app').PRFormSocialAccordion = Y.Base.create('prFormSocialAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormSocial View and configure the template
            this.prFormSocialView = new Y.app.PRFormSocialView(Y.merge(config, {}));

            // Add event targets
            this.prFormSocialView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormSocialViewContainer = this.prFormSocialView.render().get('container');

            //superclass call
            Y.app.PRFormSocialAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormSocialViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormSocialView.destroy();

            delete this.prFormSocialView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormEducationEmploymentTab = Y.Base.create('prFormEducationEmploymentTab', Y.View, [], {
        initializer: function() {
            var educationemploymentConfig = this.get('educationemploymentConfig'),
                socialConfig = this.get('socialConfig');

            //create educationemployment view
            this.educationemploymentView = new Y.app.PRFormEducationEmploymentAccordion(Y.merge(educationemploymentConfig, {
                model: this.get('model')
            }));
            this.socialView = new Y.app.PRFormSocialAccordion(Y.merge(socialConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.educationemploymentView.addTarget(this);
            this.socialView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.educationemploymentView.render().get('container'));
            contentNode.append(this.socialView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up educationemployment view
            this.educationemploymentView.destroy();
            delete this.educationemploymentView;

            //clean up social view
            this.socialView.destroy();
            delete this.socialView;
        }
    }, {
        ATTRS: {
            model: {},
            educationemploymentConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            socialConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-prform-templates',
               'usp-resolveassessment-PlacementReport'
              ]
});