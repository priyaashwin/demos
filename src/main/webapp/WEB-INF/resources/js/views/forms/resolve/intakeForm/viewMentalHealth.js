YUI.add('intakeform-mentalhealth-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormMentalHealthView = Y.Base.create('intakeFormMentalHealthView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormMentalHealthView']
    });

    Y.namespace('app').IntakeFormPsychologyView = Y.Base.create('intakeFormPsychologyView', Y.usp.resolveassessment.IntakeFormFullDetailsView, [BaseFormView], {
        template: Y.Handlebars.templates['intakeFormPsychologyView']
    });


    Y.namespace('app').IntakeFormMentalHealthAccordion = Y.Base.create('intakeFormMentalHealthAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormMentalHealthView = new Y.app.IntakeFormMentalHealthView(config);

            // Add event targets
            this.intakeFormMentalHealthView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormMentalHealthAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormMentalHealthView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormMentalHealthView.destroy();

            delete this.intakeFormMentalHealthView;
        }
    });

    Y.namespace('app').IntakeFormPsychologyAccordion = Y.Base.create('intakeFormPsychologyAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormPsychologyView = new Y.app.IntakeFormPsychologyView(config);

            // Add event targets
            this.intakeFormPsychologyView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormPsychologyAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormPsychologyView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormPsychologyView.destroy();

            delete this.intakeFormPsychologyView;
        }
    });


    Y.namespace('app').IntakeFormMentalHealthTab = Y.Base.create('intakeFormMentalHealthTab', Y.View, [], {
        initializer: function() {
            var mentalHealthConfig = this.get('mentalHealthConfig'),
                psychologyConfig = this.get('psychologyConfig');

            //create the accordion views
            this.mentalHealthAccordion = new Y.app.IntakeFormMentalHealthAccordion(
                Y.merge(
                    mentalHealthConfig, {
                        model: this.get('model')
                    }));


            this.psychologyAccordion = new Y.app.IntakeFormPsychologyAccordion(
                Y.merge(
                    psychologyConfig, {
                        model: this.get('model')
                    }));


            // Add event targets            
            this.mentalHealthAccordion.addTarget(this);
            this.psychologyAccordion.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.mentalHealthAccordion.render().get('container'));
            contentNode.append(this.psychologyAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties            
            this.mentalHealthAccordion.destroy();
            delete this.mentalHealthAccordion;

            this.psychologyAccordion.destroy();
            delete this.psychologyAccordion;
        }
    }, {
        ATTRS: {
            model: {},
            mentalHealthConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            psychologyConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeFormFullDetails'
              ]
});