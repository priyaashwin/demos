YUI.add('prform-tabs', function(Y) {
    var L = Y.Lang;

    Y.namespace('app').PRFormTabsView = Y.Base.create('prFormTabsView', Y.usp.app.AppTabbedPage, [Y.app.TabNavigation], {
        views: {
            background: {
                type: Y.app.PRFormBackgroundTab
            },
            health: {
                type: Y.app.PRFormHealthTab
            },
            educationemployment: {
                type: Y.app.PRFormEducationEmploymentTab
            },
            skills: {
                type: Y.app.PRFormSkillsTab
            },
            contacts: {
                type: Y.app.PRFormContactsTab
            },
            summary: {
                type: Y.app.PRFormSummaryTab
            },
            rejection: {
                type: Y.app.PRFormRejectionTab
            }
        },
        initializer: function(config) {
            var dialogConfig = this.get('dialogConfig'),
                formName = dialogConfig.formName,
                successMessage = dialogConfig.successMessage;

            //create form dialog
            this.formDialog = new Y.app.PRFormDialog({
                headerContent: dialogConfig.defaultHeader,
                /* !!! Merge with existing view configuration defined in prform.js !!! */
                views: {
                    editPRFormBackground: {
                        headerTemplate: dialogConfig.backgroundConfig.edit.headerTemplate
                    },
                    editPRFormContacts: {
                        headerTemplate: dialogConfig.contactsConfig.edit.headerTemplate
                    },
                    editPRFormEducationEmployment: {
                        headerTemplate: dialogConfig.educationemploymentConfig.edit.headerTemplate
                    },
                    editPRFormSocial: {
                        headerTemplate: dialogConfig.socialConfig.edit.headerTemplate
                    },
                    editPRFormHealth: {
                        headerTemplate: dialogConfig.healthConfig.edit.headerTemplate
                    },
                    editPRFormMedication: {
                        headerTemplate: dialogConfig.medicationConfig.edit.headerTemplate
                    },
                    editPRFormGP: {
                        headerTemplate: dialogConfig.gpConfig.edit.headerTemplate
                    },
                    editPRFormSkills: {
                        headerTemplate: dialogConfig.skillsConfig.edit.headerTemplate
                    },
                    editPRFormIdentity: {
                        headerTemplate: dialogConfig.identityConfig.edit.headerTemplate
                    },
                    editPRFormLeisure: {
                        headerTemplate: dialogConfig.leisureConfig.edit.headerTemplate
                    },
                    editPRFormSummary: {
                        headerTemplate: dialogConfig.summaryConfig.edit.headerTemplate
                    },
                    editPRFormRejection: {
                        headerTemplate: dialogConfig.rejectionConfig.edit.headerTemplate
                    }
                }
            });
            //setup event handlers for view based events
            this.on('prFormBackgroundView:editForm', function(e, config) {
                this.showView('editPRFormBackground', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.backgroundConfig.edit);
            this.on('prFormContactsView:editForm', function(e, config) {
                this.showView('editPRFormContacts', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.contactsConfig.edit);
            //subscribe to edit forms view event
            this.on('prFormEducationEmploymentView:editForm', function(e, config) {
                this.showView('editPRFormEducationEmployment', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.educationemploymentConfig.edit);
            this.on('prFormSocialView:editForm', function(e, config) {
                this.showView('editPRFormSocial', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.socialConfig.edit);
            //subscribe to edit forms view event
            this.on('prFormHealthView:editForm', function(e, config) {
                this.showView('editPRFormHealth', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.healthConfig.edit);
            this.on('prFormMedicationView:editForm', function(e, config) {
                this.showView('editPRFormMedication', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.medicationConfig.edit);
            this.on('prFormGPView:editForm', function(e, config) {
                this.showView('editPRFormGP', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.gpConfig.edit);
            //subscribe to edit forms view event
            this.on('prFormSkillsView:editForm', function(e, config) {
                this.showView('editPRFormSkills', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.skillsConfig.edit);
            this.on('prFormIdentityView:editForm', function(e, config) {
                this.showView('editPRFormIdentity', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.identityConfig.edit);
            this.on('prFormLeisureView:editForm', function(e, config) {
                this.showView('editPRFormLeisure', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.leisureConfig.edit);
            //subscribe to edit form view event
            this.on('prFormSummaryView:editForm', function(e, config) {
                this.showView('editPRFormSummary', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementReportForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.summaryConfig.edit);
            this.on('prFormRejectionView:editForm', function(e, config, tabs) {
                this.showView('editPRFormRejection', {
                    labels: config.labels,
                    model: new Y.app.UpdatePlacementFormRejectionForm({
                        url: config.url,
                        subject: tabs.get('activeView').get('model').get('subject')
                    }),
                    saveUrl: config.saveUrl,
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.rejectionConfig.edit, this);
            //setup this as a bubble target for the form dalog
            this.formDialog.addTarget(this);

            //listen for any save event after an edit
            this.on('*:saved', function(e) {
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: successMessage
                });
            });
        },
        render: function() {
            //superclass call
            Y.app.PRFormTabsView.superclass.render.call(this);
            //render the dialog
            this.formDialog.render();
            return this;
        },
        destructor: function() {
            //clean up dialog
            this.formDialog.destroy();
            delete this.formDialog;
        }
    }, {
        ATTRS: {
            dialogConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'app-tabbed-page',
               'tab-navigation',
               'prform-background-tab',
               'prform-health-tab',
               'prform-educationemployment-tab',
               'prform-skills-tab',
               'prform-contacts-tab',
               'prform-summary-tab',
               'prform-rejection-tab',
               'prform-dialog-views'
              ]
});