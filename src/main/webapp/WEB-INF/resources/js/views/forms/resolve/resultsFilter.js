YUI.add('form-results-filter', function(Y) {

    var O = Y.Object,
        A = Y.Array;

    Y.namespace('app').FormResultsFilterModel = Y.Base.create('formResultsFilterModel', Y.Model, [], {}, {
        ATTRS: {
            status: {
                value: []
            },
            showStatus: {
                getter: function() {
                    return (this.get('status').length > 0);
                }
            },
            showAnyFilter: {
                getter: function() {
                    return (this.get('showStatus'));
                }
            }
        }
    });


    Y.namespace('app').FormResultsFilter = Y.Base.create('formResultsFilter', Y.usp.ResultsFilter, [], {
        template: Y.Handlebars.templates['formResultsFilter'],
        events: {
            '#formFilter_status': {
                change: '_setStatus'
            },
            '#formFilter_clearAll': {
                click: '_handleClearAll'
            },
            '#formResultsFilterForm': {
                submit: '_handleSubmit'
            }

        },
        initializer: function() {},
        render: function() {
            //call superclass
            Y.app.FormResultsFilter.superclass.render.call(this);

            var instance = this,
                //get the container and find the filter form
                container = instance.get('container'),
                filterForm = container.one('form'),
                status = container.one('#formFilter_status');

            //add class name to container to allow us to identify filter
            container.addClass(this.name);

            //we won't need this node reference again and we don't want to load nodes
            filterForm.destroy();

            //build a list of form status (excludes the archived status)
            Y.FUtil.setSelectOptions(status, O.values(this._getStatus()), null, null, false, true, 'code');

            return this;
        },
        destructor: function() {
            //unbind event handlers
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;

            }
        },
        updateView: function(changed) {
            var P = Y.app.FormResultsFilter,
                instance = this,
                container = instance.get('container');
            O.each(changed, function(change, key) {
                switch (key) {
                    case 'status':
                        P._setSelectboxes('#formFilter_status', change.newVal, container);
                        break;

                    default:                    
                }
            });
        },
        _setStatus: function(e) {
            this.get('model').set('status', this._getOptions(e), {
                src: 'filter'
            });
        },
        _getOptions: function(e) {
            var val = [];
            e.currentTarget.get('options').each(function(elem) {
                if (elem.get('selected') === true) {
                    val.push(elem.get('value'));
                }
            });

            return val;
        },
        _handleClearAll: function(e) {
            e.preventDefault();

            //clear model data
            this.get('model').reset();

            //fire apply event
            this.fire('filter:apply');
        },
        _handleSubmit: function(e) {
            //stop the submit
            e.preventDefault();
        },
        _getStatus: function() {
            var statuses = [];

            // hand-crank a list of form status
            // will be able to do this much better once Barny supplies an Enumeration REST service

            statuses.push({
                code: "DRAFT",
                name: "Draft"
            });
            statuses.push({
                code: "COMPLETE",
                name: "Complete"
            });
            statuses.push({
                code: "AUTHORISED",
                name: "Authorised"
            });
            if (this.get('canViewArchived')) {
                statuses.push({
                    code: "ARCHIVED",
                    name: "Deleted"
                });
            }

            return statuses;
        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            canViewArchived: {
                value: false
            }
        },
        _setSelectboxes: function(selector, newVal, container) {
            var selected = newVal || [];
            container.one(selector).get('options').each(function(elem) {
                elem.set('selected', A.indexOf(selected, elem.get('value')) >= 0);
            });
        }
    });

}, '0.0.1', {
    requires: ['yui-base', 'results-filter', 'event-custom-base',
               'handlebars-helpers', 'handlebars-resolveform-templates',
               'form-util']
});