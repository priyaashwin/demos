YUI.add('intakeform-tabs', function(Y) {
    var L = Y.Lang;

    Y.namespace('app').IntakeFormTabsView = Y.Base.create('intakeFormTabsView', Y.usp.app.AppTabbedPage, [Y.app.TabNavigation], {
        views: {
            client: {
                type: Y.app.IntakeFormClientTab
            },
            placement: {
                type: Y.app.IntakeFormPlacementTab
            },
            referral: {
                type: Y.app.IntakeFormReferralTab
            },
            health: {
                type: Y.app.IntakeFormHealthTab
            },
            mentalHealth: {
                type: Y.app.IntakeFormMentalHealthTab
            },
            physio: {
                type: Y.app.IntakeFormPhysioTab
            },
            speech: {
                type: Y.app.IntakeFormSpeechTab
            },
            needs: {
                type: Y.app.IntakeFormNeedsTab
            },
            app1Comms: {
                type: Y.app.IntakeFormApp1CommsTab
            },
            app2MentalHealth: {
                type: Y.app.IntakeFormApp2MentalHealthTab
            },
            app3Behaviour: {
                type: Y.app.IntakeFormApp3BehaviourTab
            },
            rejection: {
                type: Y.app.IntakeFormRejectionTab
            }
        },
        initializer: function(config) {
            var dialogConfig = this.get('dialogConfig'),
                formName = dialogConfig.formName,
                successMessage = dialogConfig.successMessage,
                _self = this;

            //create form dialog
            this.formDialog = new Y.app.IntakeFormDialog({
                headerContent: dialogConfig.defaultHeader,
                /* !!! Merge with existing view configuration defined in intakeform.js !!! */
                views: {
                    editIntakeFormConsent: {
                        headerTemplate: dialogConfig.consentConfig.edit.headerTemplate
                    },
                    editIntakeFormBriefHistory: {
                        headerTemplate: dialogConfig.briefHistoryConfig.edit.headerTemplate
                    },
                    editIntakeFormCurrentPlacement: {
                        headerTemplate: dialogConfig.currentPlacementConfig.edit.headerTemplate
                    },
                    editPRFormRejection: {
                        headerTemplate: dialogConfig.rejectionConfig.edit.headerTemplate
                    },
                    addPreviousPlacement: {
                        headerTemplate: dialogConfig.previousPlacementConfig.add.headerTemplate
                    },
                    editPreviousPlacement: {
                        headerTemplate: dialogConfig.previousPlacementConfig.edit.headerTemplate
                    },
                    removePreviousPlacement: {
                        headerTemplate: dialogConfig.previousPlacementConfig.remove.headerTemplate
                    },
                    addPersonalCareNeed: {
                        headerTemplate: dialogConfig.personalCareNeedConfig.add.headerTemplate
                    },
                    editPersonalCareNeed: {
                        headerTemplate: dialogConfig.personalCareNeedConfig.edit.headerTemplate
                    },
                    removePersonalCareNeed: {
                        headerTemplate: dialogConfig.personalCareNeedConfig.remove.headerTemplate
                    },
                    editIntakeFormReferrer: {
                        headerTemplate: dialogConfig.referrerConfig.edit.headerTemplate
                    },
                    editIntakeFormFuture: {
                        headerTemplate: dialogConfig.futureConfig.edit.headerTemplate
                    },
                    addSupportPlan: {
                        headerTemplate: dialogConfig.supportPlanConfig.add.headerTemplate
                    },
                    editSupportPlan: {
                        headerTemplate: dialogConfig.supportPlanConfig.edit.headerTemplate
                    },
                    removeSupportPlan: {
                        headerTemplate: dialogConfig.supportPlanConfig.remove.headerTemplate
                    },
                    addMedication: {
                        headerTemplate: dialogConfig.medicationConfig.add.headerTemplate
                    },
                    editMedication: {
                        headerTemplate: dialogConfig.medicationConfig.edit.headerTemplate
                    },
                    removeMedication: {
                        headerTemplate: dialogConfig.medicationConfig.remove.headerTemplate
                    },
                    editIntakeFormLifestyle: {
                        headerTemplate: dialogConfig.lifestyleConfig.edit.headerTemplate
                    },
                    editIntakeFormHealthActionPlan: {
                        headerTemplate: dialogConfig.healthActionPlanConfig.edit.headerTemplate
                    },
                    addIdentifiedRisk: {
                        headerTemplate: dialogConfig.identifiedRisksConfig.add.headerTemplate
                    },
                    editIdentifiedRisk: {
                        headerTemplate: dialogConfig.identifiedRisksConfig.edit.headerTemplate
                    },
                    removeIdentifiedRisk: {
                        headerTemplate: dialogConfig.identifiedRisksConfig.remove.headerTemplate
                    },
                    editIntakeFormEating: {
                        headerTemplate: dialogConfig.eatingConfig.edit.headerTemplate
                    },
                    editIntakeFormSocial: {
                        headerTemplate: dialogConfig.socialConfig.edit.headerTemplate
                    },
                    editIntakeFormCommunicationIssues: {
                        headerTemplate: dialogConfig.communicationIssuesConfig.edit.headerTemplate
                    },
                    editIntakeFormSpiritualNeeds: {
                        headerTemplate: dialogConfig.spiritualNeedsConfig.edit.headerTemplate
                    },
                    editIntakeReferencedPerson: {
                        headerTemplate: dialogConfig.referencedPersonConfig.edit.headerTemplate
                    },
                    addIntakeReferencedPersonWithRole: {
                        headerTemplate: dialogConfig.referencedPersonConfig.add.headerTemplate
                    },
                    editIntakeReferencedPersonWithRole: {
                        headerTemplate: dialogConfig.referencedPersonConfig.edit.headerTemplate
                    },
                    removeIntakeReferencedPerson: {
                        headerTemplate: dialogConfig.referencedPersonConfig.remove.headerTemplate
                    },
                    editIntakeFormMobility: {
                        headerTemplate: dialogConfig.mobilityConfig.edit.headerTemplate
                    },
                    editIntakeFormHealth: {
                        headerTemplate: dialogConfig.healthConfig.edit.headerTemplate
                    },
                    editIntakeFormMentalHealth: {
                        headerTemplate: dialogConfig.mentalHealthConfig.edit.headerTemplate
                    },
                    editIntakeFormPsychology: {
                        headerTemplate: dialogConfig.psychologyConfig.edit.headerTemplate
                    },
                    editIntakeFormOccupationalIssues: {
                        headerTemplate: dialogConfig.occupationalIssuesConfig.edit.headerTemplate
                    },
                    editIntakeFormAppendixTimeOrientation: {
                        headerTemplate: dialogConfig.app2TimeOrientationConfig.edit.headerTemplate
                    },
                    editIntakeFormAppendixAppetite: {
                        headerTemplate: dialogConfig.app2AppetiteConfig.edit.headerTemplate
                    },
                    editIntakeFormAppendixOtherWellbeing: {
                        headerTemplate: dialogConfig.app2OtherConfig.edit.headerTemplate
                    },
                    addSocialSupport: {
                        headerTemplate: dialogConfig.socialSupportConfig.add.headerTemplate
                    },
                    editSocialSupport: {
                        headerTemplate: dialogConfig.socialSupportConfig.edit.headerTemplate
                    },
                    removeSocialSupport: {
                        headerTemplate: dialogConfig.socialSupportConfig.remove.headerTemplate
                    }
                }
            });
            //setup event handlers for view based events
            this.on('intakeFormConsentView:editForm', function(e, config) {
                this.showView('editIntakeFormConsent', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeConsentForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeConsent,
                    pathSuffix: 'consent'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.consentConfig.edit);

            this.on('intakeFormBriefHistoryView:editForm', function(e, config) {
                this.showView('editIntakeFormBriefHistory', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeBriefHistoryForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeHistory,
                    pathSuffix: 'history'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.briefHistoryConfig.edit);

            this.on('intakeFormCurrentPlacementView:editForm', function(e, config) {
                this.showView('editIntakeFormCurrentPlacement', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeCurrentPlacementForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeCurrentPlacement,
                    pathSuffix: 'currentPlacement'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.currentPlacementConfig.edit);

            this.on('intakeFormReferrerView:editForm', function(e, config) {
                this.showView('editIntakeFormReferrer', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeReferralForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeReferral,
                    pathSuffix: 'referral'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.referrerConfig.edit);

            this.on('intakeFormFutureView:editForm', function(e, config) {
                this.showView('editIntakeFormFuture', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeFutureForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeFuture,
                    pathSuffix: 'future'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.futureConfig.edit);

            this.on('intakeFormLifestyleView:editForm', function(e, config) {
                this.showView('editIntakeFormLifestyle', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeLifestyleForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeLifestyle,
                    pathSuffix: 'lifestyle'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.lifestyleConfig.edit);

            this.on('intakeFormHealthActionPlanView:editForm', function(e, config) {
                this.showView('editIntakeFormHealthActionPlan', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeHealthActionPlanForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeMedication,
                    pathSuffix: 'medication'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.healthActionPlanConfig.edit);

            this.on('intakeFormEatingView:editForm', function(e, config) {
                this.showView('editIntakeFormEating', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeEatingForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeSpeech,
                    pathSuffix: 'speech'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.eatingConfig.edit);

            this.on('intakeFormSocialView:editForm', function(e, config) {
                this.showView('editIntakeFormSocial', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeSocialForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeSocial,
                    pathSuffix: 'social'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.socialConfig.edit);


            this.on('intakeFormCommunicationIssuesView:editForm', function(e, config) {
                this.showView('editIntakeFormCommunicationIssues', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeCommunicationIssuesForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeSpeech,
                    pathSuffix: 'speech'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.communicationIssuesConfig.edit);

            this.on('intakeFormSpiritualNeedsView:editForm', function(e, config) {
                this.showView('editIntakeFormSpiritualNeeds', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeSpiritualNeedsForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeSpiritual,
                    pathSuffix: 'spiritual'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.spiritualNeedsConfig.edit);

            this.on('intakeFormMobilityView:editForm', function(e, config) {
                this.showView('editIntakeFormMobility', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeMobilityForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakePhysiotherapy,
                    pathSuffix: 'physiotherapy'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.mobilityConfig.edit);

            this.on('intakeFormHealthView:editForm', function(e, config) {
                this.showView('editIntakeFormHealth', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeHealthForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeHealth,
                    pathSuffix: 'health'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.healthConfig.edit);

            this.on('intakeFormMentalHealthView:editForm', function(e, config) {
                this.showView('editIntakeFormMentalHealth', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeMentalHealthForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeMentalHealth,
                    pathSuffix: 'mentalHealth'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.mentalHealthConfig.edit);

            this.on('intakeFormPsychologyView:editForm', function(e, config) {
                this.showView('editIntakeFormPsychology', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakePsychologyForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakePsychology,
                    pathSuffix: 'psychology'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.psychologyConfig.edit);

            this.on('intakeFormOccupationalIssuesView:editForm', function(e, config) {
                this.showView('editIntakeFormOccupationalIssues', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeOccupationalIssuesForm({
                        url: config.url
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    },
                    transmogrify: Y.usp.resolveassessment.UpdateIntakeMobility,
                    pathSuffix: 'mobility'
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.occupationalIssuesConfig.edit);

            this.on('intakeFormApp1CommsView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneCommunicationForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneCommunication
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1CommunicationConfig.edit);

            this.on('intakeFormApp1Family1View:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneFamilyHistoryForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneFamilyHistory
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1FamilyHistory1Config.edit);

            this.on('intakeFormApp1Family2View:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneFamilyHistoryForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneFamilyHistory
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1FamilyHistory2Config.edit);

            this.on('intakeFormApp1ReviewsView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneReviewForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneLastMedicalReviewDate'},
                                             {selector:'#intakeEditForm_appendixOneLastMedicationReviewCheck'},
                                             {selector:'#intakeEditForm_appendixOneLastBloodTestDate'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneReview
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1ReviewConfig.edit);

            this.on('intakeFormApp1VisionView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneProblemsForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneLastEyeTestDate'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneProblems
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1VisionConfig.edit);
  
            this.on('intakeFormApp1HearingView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneProblemsForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneLastHearingAssessmentDate'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneProblems
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1HearingConfig.edit);
            
            this.on('intakeFormApp1DentalView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneProblemsForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneLastDentalAppointmentDate'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneProblems
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1DentalConfig.edit);
 
            this.on('intakeFormApp1FootcareView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneFootCareForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneLastChiropodyAppointmentDate'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneFootCare
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1FootCareConfig.edit);
 
            this.on('intakeFormApp1ContinenceView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneContinenceForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneLastContinenceAppointmentDate'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneContinence
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1ContinenceConfig.edit);
            
            this.on('intakeFormApp1EpilepsyView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneEpilepsyForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneDateLastEpilepsyAppointment'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneEpilepsy
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1EpilepsyConfig.edit);
            
            this.on('intakeFormApp1SkinView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneSkinForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneDateLastSkinAppointment'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneSkin
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1SkinConfig.edit);
            
            this.on('intakeFormApp1BreathingCirculationView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneBreathingCirculationForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneDateLastCirculationAppointment'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneBreathingCirculation
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1BreathingCirculationConfig.edit);
                            
            this.on('intakeFormApp1MensHealthView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixOneMensHealthForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixOneLastDateTesticularExamination'}],
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixOneMensHealth
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app1MensHealthConfig.edit);

            this.on('intakeFormApp2ContextView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoContextForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_dateAndTimeOfAssessment', style:'DateTime'}],
                        /*
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoContext
                        */
                        //!!! We are using a custom model - this will be removed after the client cartridge updates
                        transmogrify: Y.app.UpdateIntakeAppendixTwoContext
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2ContextConfig.edit);
            
            this.on('intakeFormApp2MentalState1View:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoMentalStateForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoMentalState
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2MentalState1Config.edit);

            this.on('intakeFormApp2MentalState2View:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoMentalStateForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoMentalState
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2MentalState2Config.edit);

            this.on('intakeFormApp2MentalState3View:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoMentalStateForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoMentalState
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2MentalState3Config.edit);

            this.on('intakeFormApp2PlaceOrientationView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoOrientationForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoOrientation
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2PlaceOrientationConfig.edit);

            this.on('intakeFormApp2TimeOrientationView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoTimeOrientationForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoOrientation
                    });
                this.showAppendixView('editIntakeFormAppendixTimeOrientation', editConfig, model);
            }, this.formDialog, dialogConfig.app2TimeOrientationConfig.edit);

            this.on('intakeFormApp2SleepPatternView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoWellbeingForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoWellbeing
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2SleepPatternConfig.edit);

            this.on('intakeFormApp2AppetiteView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoAppetiteForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoWellbeing
                    });
                this.showAppendixView('editIntakeFormAppendixAppetite', editConfig, model);
            }, this.formDialog, dialogConfig.app2AppetiteConfig.edit);

            this.on('intakeFormApp2FeelingsView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoWellbeingForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoWellbeing
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2FeelingsConfig.edit);

            this.on('intakeFormApp2OtherView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoOtherForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoWellbeing
                    });
                this.showAppendixView('editIntakeFormAppendixOtherWellbeing', editConfig, model);
            }, this.formDialog, dialogConfig.app2OtherConfig.edit);

            this.on('intakeFormApp2OutcomeView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixTwoOutcomeForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixTwoOutcome
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app2OutcomeConfig.edit);

            this.on('intakeFormApp3ContextView:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixThreeContextForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        popupCalendarsConfig: [{selector:'#intakeEditForm_appendixThreeContextDate', style:'DateTime'}],
                        /*
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixThreeContext
                        */
                        //!!! We are using a custom model - this will be removed after the client cartridge updates
                        transmogrify: Y.app.UpdateIntakeAppendixThreeContext
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app3ContextConfig.edit);

            this.on('intakeFormApp3Behaviour1View:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixThreeBehaviourForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixThreeBehaviour
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app3Behaviour1Config.edit);

            this.on('intakeFormApp3Behaviour2View:editForm', function(e, config) {
                var model = new Y.app.UpdateIntakeAppendixThreeBehaviourForm({
                        url: config.url
                    }),
                    editConfig = Y.merge(config, {
                        formModel: _self.get('activeView').get('formModel').toJSON(),
                        appendixType: _self.get('activeView').get('model').toJSON()._type,
                        formName: formName,
                        transmogrify: Y.usp.resolveassessment.UpdateIntakeAppendixThreeBehaviour
                    });
                this.views.editIntakeFormAppendix.headerTemplate = config.headerTemplate;
                this.showAppendixView('editIntakeFormAppendix', editConfig, model);
            }, this.formDialog, dialogConfig.app3Behaviour2Config.edit);

            this.on('intakeFormRejectionView:editForm', function(e, config, tabs) {
                this.showView('editIntakeFormRejection', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeFormRejectionForm({
                        url: config.url,
                        subject: tabs.get('activeView').get('model').get('subject')
                    }),
                    saveUrl: config.saveUrl,
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        })
                    }
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.rejectionConfig.edit, this);

            //subscribe to add previous placement event
            this.on('previousPlacementTable:add', function(e, config) {
                this.showView('addPreviousPlacement', {
                    labels: config.labels,
                    model: new Y.app.AddIntakePreviousPlacementForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.previousPlacementConfig.add);

            //subscribe to edit previous placement event
            this.on('previousPlacementTable:edit', function(e, config) {
                this.showView('editPreviousPlacement', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakePreviousPlacementForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.previousPlacementConfig.edit);
            //subscribe to remove previous placement event
            this.on('previousPlacementTable:remove', function(e, config) {
                this.showView('removePreviousPlacement', {
                    model: new Y.app.RemoveIntakePreviousPlacementForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        message: config.message,
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.previousPlacementConfig.remove);

            //subscribe to add personal care need event
            this.on('personalCareNeedTable:add', function(e, config) {
                this.showView('addPersonalCareNeed', {
                    labels: config.labels,
                    model: new Y.app.AddIntakePersonalCareNeedForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.personalCareNeedConfig.add);
            //subscribe to edit personal care need event
            this.on('personalCareNeedTable:edit', function(e, config) {
                this.showView('editPersonalCareNeed', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakePersonalCareNeedForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.personalCareNeedConfig.edit);
            //subscribe to remove personal care need event
            this.on('personalCareNeedTable:remove', function(e, config) {
                this.showView('removePersonalCareNeed', {
                    model: new Y.app.RemoveIntakePersonalCareNeedForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        message: config.message,
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.personalCareNeedConfig.remove);

            //subscribe to add support plan event
            this.on('supportPlanTable:add', function(e, config) {
                this.showView('addSupportPlan', {
                    labels: config.labels,
                    model: new Y.app.AddIntakeSupportPlanForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.supportPlanConfig.add);
            //subscribe to edit support plan event
            this.on('supportPlanTable:edit', function(e, config) {
                this.showView('editSupportPlan', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeSupportPlanForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.supportPlanConfig.edit);
            //subscribe to remove support plan event
            this.on('supportPlanTable:remove', function(e, config) {
                this.showView('removeSupportPlan', {
                    model: new Y.app.RemoveIntakeSupportPlanForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        message: config.message,
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.supportPlanConfig.remove);


            //subscribe to add social support event
            this.on('socialSupportTable:add', function(e, config) {
                this.showView('addSocialSupport', {
                    labels: config.labels,
                    model: new Y.app.AddIntakeSupportForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.socialSupportConfig.add);

            //subscribe to edit social support event
            this.on('socialSupportTable:edit', function(e, config) {
                this.showView('editSocialSupport', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeSupportForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.socialSupportConfig.edit);

            //subscribe to remove social support event
            this.on('socialSupportTable:remove', function(e, config) {
                this.showView('removeSocialSupport', {
                    model: new Y.app.RemoveIntakeSupportForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        message: config.message,
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.socialSupportConfig.remove);


            //subscribe to add medication event
            this.on('medicationTable:add', function(e, config) {
                this.showView('addMedication', {
                    labels: config.labels,
                    model: new Y.app.AddIntakeMedicationForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.medicationConfig.add);
            //subscribe to edit medication event
            this.on('medicationTable:edit', function(e, config) {
                this.showView('editMedication', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeMedicationForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.medicationConfig.edit);
            //subscribe to remove medication event
            this.on('medicationTable:remove', function(e, config) {
                this.showView('removeMedication', {
                    model: new Y.app.RemoveIntakeMedicationForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        message: config.message,
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.medicationConfig.remove);

            //subscribe to add identified risk event
            this.on('identifiedRiskTable:add', function(e, config) {
                this.showView('addIdentifiedRisk', {
                    labels: config.labels,
                    model: new Y.app.AddIntakeIdentifiedRiskForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.identifiedRisksConfig.add);
            //subscribe to edit identified risk event
            this.on('identifiedRiskTable:edit', function(e, config) {
                this.showView('editIdentifiedRisk', {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeIdentifiedRiskForm({
                        url: L.sub(config.url, {
                            id: e.id
                        })
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.identifiedRisksConfig.edit);
            //subscribe to remove identified risk event
            this.on('identifiedRiskTable:remove', function(e, config) {
                this.showView('removeIdentifiedRisk', {
                    model: new Y.app.RemoveIntakeIdentifiedRiskForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        message: config.message,
                        narrative: L.sub(config.narrative, {
                            formName: formName
                        }),
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: config.successMessage
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.identifiedRisksConfig.remove);

            //subscribe to referenced people table add (referenced person)
            this.on('intakeFormReferencedPeopleTable:add', function(e, config) {
                var involvementName = e.target.get('involvementName'),
                    //Use either the professional or familial relationship types list
                    relationshipTypes = (e.involvement === 'PROFESSIONAL') ? config.professionalRelationshipTypes : config.familialRelationshipTypes;

                this.showView('addIntakeReferencedPersonWithRole', {
                    labels: config.labels,
                    model: new Y.app.AddIntakeReferencedPersonForm({
                        url: L.sub(config.url, {
                            id: e.id
                        }),
                        //set involvement from event
                        involvement: e.involvement
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName,
                            involvement: involvementName
                        }),
                        involvementName: involvementName,
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    relationshipTypes: relationshipTypes,
                    successMessage: L.sub(config.successMessage, {
                        involvement: involvementName
                    })
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.referencedPersonConfig.add);

            //subscribe to remove referenced person event
            this.on('intakeFormReferencedPeopleTable:remove', function(e, config) {
                var involvementName = e.target.get('involvementName');

                this.showView('removeIntakeReferencedPerson', {
                    model: new Y.app.RemoveIntakeReferencedPersonForm({
                        url: config.url,
                        involvement: e.involvement
                    }),
                    otherData: {
                        message: L.sub(config.message, {
                            involvement: involvementName
                        }),
                        narrative: L.sub(config.narrative, {
                            formName: formName,
                            involvement: involvementName
                        }),
                        involvementName: involvementName,
                        formModel: _self.get('activeView').get('model').toJSON()
                    },
                    successMessage: L.sub(config.successMessage, {
                        involvement: involvementName
                    })
                },
                {
                    //We have to perform a model load to get data for the narrative - i.e. the person name
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.                    
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.referencedPersonConfig.remove);

            //subscribe to edit referenced person
            this.on(['intakeFormReferencedPersonView:edit', 'intakeFormReferencedPersonWithRoleView:edit', 'intakeFormReferencedPeopleTable:edit'], function(e, config) {
                var involvementName = e.target.get('involvementName'),
                    view = (e.target.name === 'intakeFormReferencedPersonView') ? 'editIntakeReferencedPerson' : 'editIntakeReferencedPersonWithRole',
                    //Use either the professional or familial relationship types list
                    relationshipTypes = (e.involvement === 'PROFESSIONAL') ? config.professionalRelationshipTypes : config.familialRelationshipTypes;

                this.showView(view, {
                    labels: config.labels,
                    model: new Y.app.UpdateIntakeReferencedPersonForm({
                        url: config.url,
                        id: e.id
                    }),
                    otherData: {
                        narrative: L.sub(config.narrative, {
                            formName: formName,
                            involvement: involvementName
                        }),
                        involvementName: involvementName,
                        activeInvolvementName: involvementName.charAt(0).toUpperCase() + involvementName.slice(1),
                        formModel: _self.get('activeView').get('model').toJSON(),
                        mandatoryInputs: (e.target.name === 'intakeFormReferencedPeopleTable') ? true : false
                    },
                    relationshipTypes: relationshipTypes,
                    successMessage: L.sub(config.successMessage, {
                        involvement: involvementName
                    })
                }, {
                    modelLoad: true,
                    payload: {
                        id: e.id
                    }
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }, this.formDialog, dialogConfig.referencedPersonConfig.edit);


            //setup this as a bubble target for the form dialog
            this.formDialog.addTarget(this);

            //listen for previousPlacementTable changes via formDialog
            this.formDialog.on(['*:savedPreviousPlacement', '*:removedPreviousPlacement'], function(e) {
                //reload results
                this.get('activeView').historyAccordion.reload();
            }, this);
            //listen for personalCareNeedTable changes via formDialog
            this.formDialog.on(['*:savedPersonalCareNeed', '*:removedPersonalCareNeed'], function(e) {
                //reload results
                this.get('activeView').personalCareNeedAccordion.reload();
            }, this);
            //listen for supportPlanTable changes via formDialog
            this.formDialog.on(['*:savedSupportPlan', '*:removedSupportPlan'], function(e) {
                //reload results
                this.get('activeView').interventionAndSupportPlansAccordion.reload();
            }, this);
            //listen for medicationTable changes via formDialog
            this.formDialog.on(['*:savedMedication', '*:removedMedication'], function(e) {
                //reload results
                this.get('activeView').medicationsAccordion.reload();
            }, this);
            //listen for identifiedRiskTable changes via formDialog
            this.formDialog.on(['*:savedIdentifiedRisk', '*:removedIdentifiedRisk'], function(e) {
                //reload results
                this.get('activeView').identifiedRisksAccordion.reload();
            }, this);
            //listen for social timetable changes via formDialog
            this.formDialog.on('*:savedSocial', function(e) {
                //reload timetable
                this.get('activeView').socialAccordion.reload();
            }, this);
            //listen for social support changes via formDialog
            this.formDialog.on(['*:savedSocialSupport', '*:removedSocialSupport'], function(e) {
                //reload results
                this.get('activeView').socialSupportAccordion.reload();
            }, this);

            //Fire success messages for previous placement events
            this.on(['*:savedPreviousPlacement', '*:removedPreviousPlacement'], function(e) {
                var message;
                if (!e.id) {
                    //add
                    message = this.add.successMessage;
                } else {
                    //edit or remove
                    if (e.type === "savedPreviousPlacement") {
                        //edit
                        message = this.edit.successMessage;
                    } else if (e.type === "removedPreviousPlacement") {
                        message = this.remove.successMessage;
                    }
                }
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: message
                });
            }, dialogConfig.previousPlacementConfig);

            //Fire success messages for personal care need events
            this.on(['*:savedPersonalCareNeed', '*:removedPersonalCareNeed'], function(e) {
                var message;
                if (!e.id) {
                    //add
                    message = this.add.successMessage;
                } else {
                    //edit or remove
                    if (e.type === "editIntakePersonalCareNeedView:savedPersonalCareNeed") {
                        //edit
                        message = this.edit.successMessage;
                    } else if (e.type === "removeIntakePersonalCareNeedView:removedPersonalCareNeed") {
                        message = this.remove.successMessage;
                    }
                }
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: message
                });
            }, dialogConfig.personalCareNeedConfig);

            //Fire success messages for support plan events
            this.on(['*:savedSupportPlan', '*:removedSupportPlan'], function(e) {
                var message;
                if (!e.id) {
                    //add
                    message = this.add.successMessage;
                } else {
                    //edit or remove
                    if (e.type === "editIntakeSupportPlanView:savedSupportPlan") {
                        //edit
                        message = this.edit.successMessage;
                    } else if (e.type === "removeIntakeSupportPlanView:removedSupportPlan") {
                        message = this.remove.successMessage;
                    }
                }
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: message
                });
            }, dialogConfig.supportPlanConfig);

            //Fire success messages for medication events
            this.on(['*:savedMedication', '*:removedMedication'], function(e) {
                var message;
                if (!e.id) {
                    //add
                    message = this.add.successMessage;
                } else {
                    //edit or remove
                    if (e.type === "editIntakeMedicationView:savedMedication") {
                        //edit
                        message = this.edit.successMessage;
                    } else if (e.type === "removeIntakeMedicationView:removedMedication") {
                        message = this.remove.successMessage;
                    }
                }
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: message
                });
            }, dialogConfig.medicationConfig);

            //Fire success messages for identified risk events
            this.on(['*:savedIdentifiedRisk', '*:removedIdentifiedRisk'], function(e) {
                var message;
                if (!e.id) {
                    //add
                    message = this.add.successMessage;
                } else {
                    //edit or remove
                    if (e.type === "editIntakeIdentifiedRiskView:savedIdentifiedRisk") {
                        //edit
                        message = this.edit.successMessage;
                    } else if (e.type === "removeIntakeIdentifiedRiskView:removedIdentifiedRisk") {
                        message = this.remove.successMessage;
                    }
                }
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: message
                });
            }, dialogConfig.identifiedRisksConfig);

            //Fire success messages for social support
            this.on(['*:savedSocialSupport', '*:removedSocialSupport'], function(e) {
                var message;
                if (!e.id) {
                    //add
                    message = this.add.successMessage;
                } else {
                    //edit or remove
                    if (e.type === "editIntakeSupportView:savedSocialSupport") {
                        //edit
                        message = this.edit.successMessage;
                    } else if (e.type === "removeIntakeSupportView:removedSocialSupport") {
                        message = this.remove.successMessage;
                    }
                }
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: message
                });
            }, dialogConfig.socialSupportConfig);


            //listen for referenced person save event and update the appropriate accordion 
            this.on(['*:savedReferencedPerson', '*:removedReferencedPerson'], function(e) {
                var view = this.get('activeView');
                switch (e.involvement) {
                    case 'NEXT_OF_KIN':
                        view.nextOfKinAccordion.get('model').load();
                        break;
                    case 'MAIN_CARER':
                        view.mainCarerAccordion.get('model').load();
                        break;
                    case 'SPEECH_LANGUAGE_THERAPIST':
                        view.therapyAccordion.get('model').load();
                        break;
                    case 'PHYSIOTHERAPIST':
                        view.physiotherapyAccordion.get('model').load();
                        break;
                    case 'OCCUPATIONAL_THERAPIST':
                        view.occupationalTherapistAccordion.get('model').load();
                        break;
                    case 'NOT_TO_BE_CONTACTED':
                        //reload results table
                        view.peopleNotToBeContactedAccordion.reload();
                        break;
                    case 'PROFESSIONAL':
                        //reload results table
                        view.professionalsAccordion.reload();
                        break;

                }

                //now fire the success message - get the success message out of the dialog view
                //It was put in there when the dialog was shown (as we needed to substitute data into the message)
                Y.fire('infoMessage:message', {
                    message: e.target.get('successMessage')
                });
            });

            //listen for any save event after an edit 
            // include appendix save and special edit social save event            
            this.on(['*:saved', '*:savedAppendix', '*:savedSocial'], function(e) {
                //now fire the success message
                Y.fire('infoMessage:message', {
                    message: successMessage
                });
            });
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormTabsView.superclass.render.call(this);
            //render the dialog
            this.formDialog.render();
            return this;
        },
        destructor: function() {
            //clean up dialog
            this.formDialog.destroy();
            delete this.formDialog;
        }
    }, {
        ATTRS: {
            dialogConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'app-tabbed-page',
               'tab-navigation',
               'intakeform-client-tab',
               'intakeform-placement-tab',
               'intakeform-referral-tab',
               'intakeform-health-tab',
               'intakeform-mentalhealth-tab',
               'intakeform-physio-tab',
               'intakeform-speech-tab',
               'intakeform-needs-tab',
               'intakeform-app1comms-tab',
               'intakeform-app2mentalhealth-tab',
               'intakeform-app3behaviour-tab',
               'intakeform-rejection-tab',
               'intakeform-dialog-views',
               'usp-relationship-FamilialRelationshipType',
               'usp-relationship-ProfessionalRelationshipType',
               'usp-resolveassessment-UpdateIntakeConsent',
               'usp-resolveassessment-UpdateIntakeHistory',
               'usp-resolveassessment-UpdateIntakeCurrentPlacement',
               'usp-resolveassessment-UpdateIntakeReferral',
               'usp-resolveassessment-UpdateIntakeFuture',
               'usp-resolveassessment-UpdateIntakeSpeech',
               'usp-resolveassessment-UpdateIntakeSpiritual'
              ]
});