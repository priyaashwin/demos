YUI.add('form-definition-controller-view', function(Y) {
    'use-strict';

    var L = Y.Lang,
        Q = Y.QueryString;

    Y.namespace('app.admin.form').FormDefinitionControllerView = Y.Base.create('formDefinitionControllerView', Y.View, [], {
        initializer: function(config) {
            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            var resultsConfig = {
                searchConfig: config.searchConfig,
                filterConfig: config.filterConfig,
                filterContextConfig: config.filterContextConfig,
                permissions: config.permissions
            };

            this.formDefinitionResults = new Y.app.admin.form.FormDefinitionFilteredResults(resultsConfig).addTarget(this);

            this.formDefinitionDialog = new Y.app.admin.form.FormDefinitionDialog(config.dialogConfig).addTarget(this).render();

            this._formDefinitionEvtHandlers = [
                this.on('*:saved', this.formDefinitionResults.reload, this.formDefinitionResults),
                this.on('*:editDesign', this.launchFormDesigner, this),
                this.on('*:edit', this.showEditFormDefinitionDialog, this),
                this.on('*:view', this.showViewFormDefinitionDialog, this),
                this.on('*:publish', this.showPublishFormDefinitionDialog, this),
                this.on('*:archive', this.showArchiveFormDefinitionDialog, this),
                this.on('*:remove', this.showRemoveFormDefinitionDialog, this),
                this.on('*:viewMappings', this.showFormMappings, this),
                this.on('*:addFormDesign', this.launchFormDesigner, this),
                this.after('securityDomainsChange', this.setupSecurityDomains, this),
                Y.Global.on('refreshList', this.formDefinitionResults.reload, this.formDefinitionResults),
                this.after('loadedChange', this._renderSecurityDomainList, this)

            ];

            this._getSecurityDomains().then(function(results) {
                this.set('securityDomains', results.toJSON() || []);
            }.bind(this));
        },
        setupSecurityDomains: function(e) {
            //push security domains into filter
            this.formDefinitionResults.get('filter').get('resultsFilter').set('securityDomains', e.newVal);
            //push into results
            this.formDefinitionResults.get('results').set('securityDomains', e.newVal);

            this.set('loaded', true);
        },
        destructor: function() {
            this._formDefinitionEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._formDefinitionEvtHandlers;

            this.formDefinitionResults.removeTarget(this);
            this.formDefinitionResults.destroy();
            delete this.formDefinitionResults;

            this.formDefinitionDialog.removeTarget(this);
            this.formDefinitionDialog.destroy();
            delete this.formDefinitionDialog;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;
        },
        render: function() {
            //render occurs once loaded attribute is set
            return this;
        },
        _renderSecurityDomainList: function() {
            if (this.get('loaded')) {
                this.get('container').setHTML(this.formDefinitionResults.render().get('container'));
            }
        },
        _getSecurityDomains: function() {
            var model = new Y.usp.securityextension.PaginatedSecurityDomainList({
                url: this.get('securityDomainUrl')
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },
        
        getFormDefinition : function(id) {
        	var model = new Y.usp.form.DateControlFullDetails({
                url: L.sub(this.get('dateControlFullDetailsUrl'), {
                	id: id
                })
            });
        	return new Y.Promise(function(resolve, reject) {
        		var data = model.load(function(err) {
        			if (err !== null) {
        				reject(err);
        			} else {
        				resolve(data);
        			}
        		});
        	});
        },
        
        showViewFormDefinitionDialog: function(e) {
            var record = e.record,
                permissions = e.permissions;

            var config = this.get('dialogConfig').views.view.config;

            if (permissions.canView) {
                this.formDefinitionDialog.showView('view', {
                    model: new Y.app.admin.form.FormDefinitionViewForm({
                        id: record.get('id'),
                        url: config.url
                    }),
                    securityDomains: this.get('securityDomains')
                }, {
                    modelLoad: true
                });
            }
        },
        showEditFormDefinitionDialog: function(e) {
            var record = e.record,
                permissions = e.permissions;  

            var config = this.get('dialogConfig').views.edit.config;
            
            this.getFormDefinition(record.get('id')).then(function(results) {
            	var formDefinition = results.toJSON(),
            		activityDateControlArray = formDefinition.results;  // array of date fields in the form
            	
            	if (permissions.canEdit) {
            		this.formDefinitionDialog.showView('edit', {
            			model: new Y.app.admin.form.FormDefinitionEditForm({
            				id: record.get('id'),
            				url: config.url,
            				labels: config.validationLabels
            			}),
            			securityDomains: this.get('securityDomains'),
            			activityDateControlArray: activityDateControlArray
            		}, {
            			modelLoad: true
            		}, function(view) {
            			this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            		});
            	}
            }.bind(this));
        },
        showPublishFormDefinitionDialog: function(e) {
            var record = e.record,
                permissions = e.permissions;

            var config = this.get('dialogConfig').views.publish.config;

            if (permissions.canPublish) {
                this.formDefinitionDialog.showView('publish', {
                    model: new Y.app.admin.form.FormDefinitionStatusChangeForm(
                        Y.merge(record.toJSON(), {
                            url: L.sub(config.url, {
                                action: config.action
                            })
                        }))
                }, function(view) {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        showArchiveFormDefinitionDialog: function(e) {
            var record = e.record,
                permissions = e.permissions;

            var config = this.get('dialogConfig').views.archive.config;

            if (permissions.canArchive) {
                this.formDefinitionDialog.showView('archive', {
                    model: new Y.app.admin.form.FormDefinitionStatusChangeForm(
                        Y.merge(record.toJSON(), {
                            url: L.sub(config.url, {
                                action: config.action
                            })
                        }))
                }, function(view) {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        showRemoveFormDefinitionDialog: function(e) {
            var record = e.record,
                permissions = e.permissions;

            var config = this.get('dialogConfig').views.remove.config;

            if (permissions.canRemove) {
                this.formDefinitionDialog.showView('remove', {
                    model: new Y.app.admin.form.FormDefinitionRemoveForm(
                        Y.merge(record.toJSON(), {
                            url: config.url
                        }))
                }, function(view) {
                    this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        showFormMappings: function(e) {
            var record = e.record;
            window.location = L.sub(this.get('formMappingsUrl'), {
                formDefinitionId: record.get('id')
            });
        },
        launchFormDesigner: function(e) {
            var record = e.record,
                id;

            if (record) {
                id = record.get('id');
            }

            var designerConfig = this.get('designerConfig'),
                designerURL = designerConfig.designerURL,
                url;

            if (!(id === undefined || id === null)) {
                url = designerURL + '?' + Q.stringify({
                    id: id
                });
            } else {
                url = designerURL;
            }

            var ref = window.open(
                url,
                'Form Designer',
                'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1024,height=768,left=100,top=100'
            );
            if (ref) {
                //attempt to move to front
                ref.focus();
            }

            return;
        }
    }, {
        ATTRS: {
            searchConfig: {
                value: {
                    url: {},
                    noDataMessage: {},
                    permissions: {},
                    resultsCountNode: {}
                }
            },
            filterContextConfig: {
                value: {
                    labels: {}
                }
            },
            filterConfig: {
                value: {
                    labels: {}
                }
            },
            dialogConfig: {
                value: {
                    labels: {}
                }
            },
            securityDomainUrl: {
                value: ''
            },
            securityDomains: {
                value: []
            },
            formMappingsUrl: {
                value: ''
            },
            loaded: {
                value: false
            },
            dateControlFullDetailsUrl: {
            	value: ''
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-toolbar',
        'view',
        'form-definition-results',
        'form-definition-dialog-views',
        'usp-securityextension-SecurityDomain',
        'querystring-stringify',
        'usp-form-DateControlFullDetails'
    ]
});