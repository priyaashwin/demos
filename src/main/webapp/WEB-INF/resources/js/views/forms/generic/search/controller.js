YUI.add('form-instance-search-controller', function(Y) {
    'use-strict';

    var L = Y.Lang;

    Y.namespace('app.form').FormInstanceControllerView = Y.Base.create('formInstanceControllerView', Y.View, [], {
        initializer: function(config) {
            var resultsConfig = {
                searchConfig: Y.merge(config.searchConfig, {
                    //mix the subject into the search config so it can be used in URL generation
                    subject: config.subject
                }),
                filterConfig: Y.merge(config.filterConfig, {
                    //mix the subject into the filter config so it can be
                    //used in the URL to load definitions in the filter
                    subject: config.subject
                }),
                filterContextConfig: config.filterContextConfig,
                quickFiltersConfig: config.quickFiltersConfig,
                permissions: config.permissions
            };

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            this.formInstanceResults = new Y.app.form.FormInstanceFilteredResults(resultsConfig).addTarget(this);

            this.formAttachmentDialog = new Y.app.form.FormInstanceAttachmentDialog(config.attachmentDialogConfig).addTarget(this).render();

            this.formDialog = new Y.app.form.FormInstanceDialog(config.dialogConfig).addTarget(this).render();

            //configure the dialog for output template - no need to add as a bubble target
            this.outputDocumentDialog = new Y.app.output.OutputDocumentDialog(config.outputDialogConfig).render();

            this._formInstanceEvtHandlers = [
                Y.Global.on('formEditorStatusChangeEvent', this.findStatusFormDialog, this),
                this.on('formInstanceResults:view', this.handleViewFormInstance, this),
                this.on('formInstanceResults:reviewHistory', this.handleReviewHistory, this),
                this.on('formInstanceResults:submit', this.showSubmitFormDialog, this),
                this.on('formInstanceResults:withdraw', this.showWithdrawFormDialog, this),
                this.on('formInstanceResults:complete', this.showCompleteFormDialog, this),
                this.on('formInstanceResults:uncomplete', this.showUncompleteFormDialog, this),
                this.on('formInstanceResults:softDelete', this.showSoftDeleteFormDialog, this),
                this.on('formInstanceResults:hardDelete', this.showHardDeleteFormDialog, this),
                this.on('formInstanceResults:authorise', this.showAuthoriseFormDialog, this),
                this.on('formInstanceResults:reject', this.showRejectFormDialog, this),
                this.on('*:uploadAttachment', this.showUploadAttachmentDialog, this),
                this.on('*:manageFiles', this.showManageFilesAttachmentDialog, this),
                this.on('*:saved', this.formInstanceResults.reload, this.formInstanceResults),
                //output
                this.on('*:output', this.showOutputDialog, this),
                this.on('formInstanceAttachmentResults:view', this.handleDownloadFile, this),
                this.on('formInstanceAttachmentResults:remove', this.showRemoveAttachmentDialog, this),
                /* checklist link from */
                this.on('viewForm', this.handleViewFormInstance, this),
                this.on('*:readingView', this.showReadingView, this),
                this.after('*:readingViewChange', this._afterReadingViewChange, this)
            ];
        },
        destructor: function() {
            this._formInstanceEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._formInstanceEvtHandlers;

            this.formDialog.removeTarget(this);
            this.formDialog.destroy();
            delete this.formDialog;

            this.formAttachmentDialog.removeTarget(this);
            this.formAttachmentDialog.destroy();
            delete this.formAttachmentDialog;

            this.outputDocumentDialog.removeTarget(this);
            this.outputDocumentDialog.destroy();
            delete this.outputDocumentDialog;

            this.formInstanceResults.removeTarget(this);
            this.formInstanceResults.destroy();
            delete this.formInstanceResults;
        },
        render: function() {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.formInstanceResults.render().get('container'));

            //set state of reading button
            this._setReadingButton(this.formInstanceResults.get('results').get('readingView'));

            this.get('container').setHTML(contentNode);

            return this;
        },
        _loadFormDefinition: function(id) {
            var url = this.get('formDefinitionURL');
            return new Y.Promise(function(resolve, reject) {
                var model = new Y.usp.form.FormDefinition({
                    url: url,
                    id: id
                });
                model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(model);
                    }
                });
            });
        },
        handleViewFormInstance: function(e) {
            var form = e.record,
                memberId = null,
                subject,
                formEditorURL = this.get('formEditorURL');

            if (form) {
                //if the form is a group form - and we are viewing an individual - then set the memberId
                if ('GROUP' === form.get('subjectType')) {
                    subject = this.get('subject');
                    if (subject && subject.subjectType === 'person') {
                        //set the memberId
                        memberId = subject.subjectId;
                    }
                }
                Y.app.FormLauncher.open(formEditorURL, form.get('id'), form.get('formDefinitionName'), memberId);
            }
        },
        handleReviewHistory: function(e) {
            var reviewHistoryURL = this.get('reviewHistoryURL'),
                record = e.record,
                permissions = this.get('permissions');

            if (permissions.canView) {
                //show the processing view
                this.formDialog.showView('processing');


                window.location = L.sub(reviewHistoryURL, {
                    id: record.get('id')
                });
            }
        },
        _getSubjectFromRecord: function(record) {
            var subject = this.get('subject');

            if ('GROUP' === record.get('subjectType')) {
                subject = {
                    subjectType: 'group',
                    subjectName: record.get('subjectName'),
                    subjectIdentifier: record.get('subjectIdentifier'),
                    subjectId: record.get('subjectId')
                };
            }
            return subject;
        },
        findStatusFormDialog: function(e) {
            var entry = {};
            entry.record = new Y.usp.form.FormInstance(e.record.toJSON());
            switch(e.statusButton) {
              case 'submit':
                this.showSubmitFormDialog(entry);
              break;
              case 'complete':
                this.showCompleteFormDialog(entry);
              break;
              case 'authorise':
                this.showAuthoriseFormDialog(entry);
              break;
            }
        },
        showSubmitFormDialog: function(e) {
            var record = e.record,
                permissions = this.get('permissions');

            if (permissions.canSubmit && record.hasAccessLevel('WRITE')) {
                this._showStatusChangeDialogWithFormDefinition(record, 'submitForm', 'submitted');
            }
        },
        showWithdrawFormDialog: function(e) {
            var record = e.record,
                permissions = this.get('permissions');

            if (permissions.canWithdraw && record.hasAccessLevel('WRITE')) {
                this._showStatusChangeDialog(record, 'withdrawForm', 'withdrawn');
            }
        },
        showCompleteFormDialog: function(e) {
            var record = e.record,
                permissions = this.get('permissions');

            if (permissions.canComplete && record.hasAccessLevel('WRITE')) {
                this._showStatusChangeDialogWithFormDefinition(record, 'completeForm', 'complete');
            }
        },
        showUncompleteFormDialog: function(e) {
            var record = e.record,
                permissions = this.get('permissions');

            if (permissions.canUncomplete && record.hasAccessLevel('WRITE')) {
                this._showStatusChangeDialog(record, 'uncompleteForm', 'draft');
            }
        },
        showSoftDeleteFormDialog: function(e) {
            var record = e.record,
                permissions = this.get('permissions');

            if (permissions.canSoftDelete && record.hasAccessLevel('WRITE')) {
                this._showStatusChangeDialog(record, 'deleteForm', 'archived');
            }
        },
        showHardDeleteFormDialog: function(e) {
            var record = e.record,
                dialogConfig = this.get('dialogConfig'),
                url = dialogConfig.views.removeForm.config.url,
                permissions = this.get('permissions');

            if (permissions.canHardDelete && record.hasAccessLevel('WRITE')) {
                this._showStatusChangeDialog(record, 'removeForm', undefined, url);
            }
        },
        _showStatusChangeDialog: function(record, view, action, _url) {
            var dialogConfig = this.get('dialogConfig'),
                url = _url || dialogConfig.statusChangeURL;

            if (record.hasAccessLevel('WRITE')) {
                this._showFormDialog(record, view, new Y.app.form.FormInstanceStatusChangeForm({
                    url: L.sub(url, {
                        action: action
                    }),
                    id: record.get('id'),
                }));
            }
        },
        _showFormDialog: function(record, view, model, formDefinition) {
            var subject = this._getSubjectFromRecord(record);

            //show the submit dialog
            this.formDialog.showView(view, {
                model: model,
                formDefinition: formDefinition,
                otherData: {
                    subject: subject || {},
                    name: record.get('formDefinitionName')
                }
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        _showStatusChangeDialogWithFormDefinition: function(record, view, action, _url) {
            var dialogConfig = this.get('dialogConfig'),
                url = _url || dialogConfig.statusChangeURL;

            //show the processing view
            this.formDialog.showView('processing');

            //load the form definition
            this._loadFormDefinition(record.get('formDefinitionId')).then(function(formDefinition) {
                //show the submit dialog
                this._showFormDialog(record, view, new Y.app.form.FormInstanceStatusChangeForm({
                    url: L.sub(url, {
                        action: action
                    }),
                    subTypeId: (formDefinition.get('subTypes').find(function(subType) {
                        return subType.name == record.get('subType');
                    }) || {}).id,
                    id: record.get('id'),
                }), formDefinition);
            }.bind(this)).then(undefined, function(err) {
                var view = this.formDialog.get('activeView');
                if (view) {
                    view.fire('error', {
                        error: err
                    });
                }
            }.bind(this));
        },
        showAuthoriseFormDialog: function(e) {
            var record = e.record,
                dialogConfig = this.get('dialogConfig'),
                url = dialogConfig.views.approveForm.config.url;

            if (record.hasAccessLevel('WRITE')) {
                this._showFormDialog(record, 'approveForm', new Y.app.form.FormReviewCommentsForm({
                    url: L.sub(url, {
                        action: 'authorised'
                    }),
                    id: record.get('id')
                }));
            }
        },
        showRejectFormDialog: function(e) {
            var record = e.record,
                dialogConfig = this.get('dialogConfig'),
                url = dialogConfig.views.rejectForm.config.url;

            if (record.hasAccessLevel('WRITE')) {
                this._showFormDialog(record, 'rejectForm', new Y.app.form.FormReviewCommentsForm({
                    url: L.sub(url, {
                        action: 'rejected'
                    }),
                    id: record.get('id')
                }));
            }
        },
        showOutputDialog: function(e) {
            var record = e.record,
                formDefinitionURL = this.get('formDefinitionURL'),
                subject = this.get('subject'),
                isGroupForm = 'GROUP' === record.get('subjectType');

            if ((subject.subjectType || '').toUpperCase() !== 'PERSON') {
                //Output is only supported when viewing an individual
                return;
            }

            if (record.hasAccessLevel('READ_DETAIL')) {
                this.outputDocumentDialog.showView('produceDocument', {
                    dialog: this.outputDocumentDialog,
                    //pass the form instance id
                    formInstanceId: record.get('id'),
                    isGroupForm: isGroupForm,
                    subjectId: subject.subjectId,
                    //pass the dialog so we can reposition it after the render
                    model: new Y.usp.form.FormDefinitionWithOutputTemplates({
                        url: formDefinitionURL,
                        id: record.get('formDefinitionId')
                    })
                }, {
                    modelLoad: true
                });
            }
        },
        showUploadAttachmentDialog: function(e) {
            var record = e.record,
                permissions = this.get('permissions'),
                subject = this._getSubjectFromRecord(record) || {},
                dialogConfig = this.get('attachmentDialogConfig').views.uploadFile.config,
                url;

            if (e.returnTo) {
                //determine where the user should be returned to on closing the upload dialog.
                //This is because there is an upload link on the manage files dialog, and the
                //user should return to that when the upload dialog is closed.
            }

            //L3 WRITE needed to upload attachments
            if (permissions.canUploadAttachment && record.hasAccessLevel('WRITE')) {
                if (record.get('status') === 'DRAFT') {
                    url = dialogConfig.url;
                } else if (record.get('status') === 'COMPLETE' && permissions.canUploadAttachmentOnCompleteInstance) {
                    url = dialogConfig.urlComplete;
                } else {
                    //no access
                    return;
                }

                this.formAttachmentDialog.showView('uploadFile', {
                    model: new Y.app.form.NewFormInstanceAttachment({
                        formInstanceId: record.get('id'),
                        inline: false,
                        url: url,
                        labels: dialogConfig.validationLabels
                    }),
                    //needed to handle the return to dialog
                    formInstance: record,
                    otherData: {
                        subject: subject,
                        name: record.get('formDefinitionName')
                    },
                    returnTo: e.returnTo
                });
            }
        },
        showManageFilesAttachmentDialog: function(e) {
            var record = e.record,
                permissions = this.get('permissions'),
                subject = this._getSubjectFromRecord(record) || {};

            //L3 READ_DETAIL required to see files
            if (permissions.canView && permissions.canViewAttachment && record.hasAccessLevel('READ_DETAIL')) {
                this.formAttachmentDialog.showView('manageFiles', {
                    model: record,
                    otherData: {
                        subject: subject,
                        name: record.get('formDefinitionName')
                    }
                }, function(view) {
                    //remove the upload button if the record is not draft and user has L3 WRITE access
                    if (record.get('status') === 'DRAFT' && record.hasAccessLevel('WRITE')) {
                        this.getButton('uploadFileButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    } else {
                        this.removeButton('uploadFileButton', Y.WidgetStdMod.FOOTER);
                    }
                });
            }
        },
        handleDownloadFile: function(e) {
            var record = e.record,
                formInstance = e.formInstance,
                //note this is the permission from the event, i.e. attachment permissions
                permissions = e.permissions,
                downloadURL;

            //must have view permission and L3 READ_DETAIL access
            if (permissions.canView && formInstance.hasAccessLevel('READ_DETAIL')) {
                downloadURL = L.sub(this.get('downloadAttachmentURL'), {
                    id: record.get('id'),
                    formInstanceId: formInstance.get('id')
                });
                //attempt the download
                Y.later(500, this, function() {
                    window.location.href = downloadURL;
                });
            }
        },
        showRemoveAttachmentDialog: function(e) {
            var record = e.record,
                formInstance = e.formInstance,
                //note this is the permission from the event, i.e. attachment permissions
                permissions = e.permissions,
                subject = this._getSubjectFromRecord(formInstance) || {},
                dialogConfig = this.get('attachmentDialogConfig'),
                url = dialogConfig.views.removeFile.config.url;

            //Need L3 WRITE access to remove attachment
            if (permissions.canView && permissions.canRemove && formInstance.hasAccessLevel('WRITE')) {
                this.formAttachmentDialog.showView('removeFile', {
                    model: new Y.usp.form.FormInstanceAttachment({
                        id: record.get('id'),
                        formInstanceId: formInstance.get('id'),
                        url: url
                    }),
                    //needed to handle the return to dialog
                    formInstance: formInstance,
                    otherData: {
                        subject: subject,
                        name: formInstance.get('formDefinitionName')
                    },
                    returnTo: 'manageFiles'
                }, function(view) {
                    //enable yes button
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //don't want a cancel button on this
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        showReadingView: function() {
            var state = this.formInstanceResults.get('results').get('readingView');

            if (state === 'reading') {
                state = 'initial';
            } else {
                state = 'reading';
            }
            this.formInstanceResults.get('results').set('readingView', state);
        },
        _afterReadingViewChange: function(e) {
            this._setReadingButton(e.newVal);
        },
        _setReadingButton: function(mode) {
            var readingCfg = this.get('readingCfg'),
                node = readingCfg.triggerNode,
                button = Y.one(node),
                active = !(mode === 'reading'),
                cfg = active ? readingCfg.active : readingCfg.inactive;

            this.toolbar.updateButton(node, cfg);
            active ? button.addClass(readingCfg.active.className) : button.removeClass(readingCfg.active.className);
        }
    }, {
        ATTRS: {
            formInstanceURL: {},
            formDefinitionURL: {},
            formEditorURL: {},
            reviewHistoryURL: {},
            downloadAttachmentURL: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'form-instance-results',
        'form-instance-dialog',
        'form-attachment-dialog',
        'form-launcher',
        'form-output-document-dialog-views',
        'output-document-dialog-views',
        'usp-form-FormDefinitionWithOutputTemplates',
        'usp-form-FormDefinition',
        'usp-form-FormInstanceAttachment',
        'yui-later',
        'app-toolbar'
    ]
});