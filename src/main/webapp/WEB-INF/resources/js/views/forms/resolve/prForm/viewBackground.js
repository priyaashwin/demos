YUI.add('prform-background-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };
    
    Y.namespace('app').PRFormBackgroundView = Y.Base.create('prFormBackgroundView', Y.usp.resolveassessment.PlacementReportView, [BaseFormView], {
        template: Y.Handlebars.templates['prFormBackgroundView']
    });

    Y.namespace('app').PRFormBackgroundAccordion = Y.Base.create('prFormBackgroundAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new PRFormBackground View and configure the template
            this.prFormBackgroundView = new Y.app.PRFormBackgroundView(Y.merge(config, {}));

            // Add event targets
            this.prFormBackgroundView.addTarget(this);
        },
        render: function() {
            //render the view
            var prFormBackgroundViewContainer = this.prFormBackgroundView.render().get('container');

            //superclass call
            Y.app.PRFormBackgroundAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(prFormBackgroundViewContainer);

            return this;
        },
        destructor: function() {
            this.prFormBackgroundView.destroy();

            delete this.prFormBackgroundView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').PRFormBackgroundTab = Y.Base.create('prFormBackgroundTab', Y.View, [], {
        initializer: function() {
            var backgroundConfig = this.get('backgroundConfig');

            //create background view
            this.backgroundView = new Y.app.PRFormBackgroundAccordion(Y.merge(backgroundConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.backgroundView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.backgroundView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up background view
            this.backgroundView.destroy();
            delete this.backgroundView;
        }
    }, {
        ATTRS: {
            model: {},
            backgroundConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-prform-templates',
               'usp-resolveassessment-PlacementReport'
              ]
});