YUI.add('intakeform-app2mentalhealth-tab', function(Y) {
    function BaseAppendixFormView() {}
    BaseAppendixFormView.prototype = {
        initializer: function() {
            var formModel = this.get('formModel');

            // We'll re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    formModel: this.get('formModel').toJSON(),
                    enumTypes: this.get('enumTypes')
                });

            // Render this view's HTML into the container element.
            container.setHTML(html);

            return this;
        },
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    //Views 

    //Context
    Y.namespace('app').IntakeFormApp2ContextView = Y.Base.create('intakeFormApp2ContextView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2ContextView']
    });

    //MentalState1
    Y.namespace('app').IntakeFormApp2MentalState1View = Y.Base.create('intakeFormApp2MentalState1View', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2MentalState1View']
    });

    //MentalState2
    Y.namespace('app').IntakeFormApp2MentalState2View = Y.Base.create('intakeFormApp2MentalState2View', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2MentalState2View']
    });

    //MentalState3
    Y.namespace('app').IntakeFormApp2MentalState3View = Y.Base.create('intakeFormApp2MentalState3View', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2MentalState3View']
    });

    //PlaceOrientation
    Y.namespace('app').IntakeFormApp2PlaceOrientationView = Y.Base.create('intakeFormApp2PlaceOrientationView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2PlaceOrientationView']
    });

    //TimeOrientation
    Y.namespace('app').IntakeFormApp2TimeOrientationView = Y.Base.create('intakeFormApp2TimeOrientationView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2TimeOrientationView']
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            enumTypes: {
                value: {
                    dayPart: Y.usp.resolveassessment.enum.DayPart.values
                }
            }
        }
    });

    //SleepPattern
    Y.namespace('app').IntakeFormApp2SleepPatternView = Y.Base.create('intakeFormApp2SleepPatternView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2SleepPatternView']
    });

    //Appetite
    Y.namespace('app').IntakeFormApp2AppetiteView = Y.Base.create('intakeFormApp2AppetiteView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2AppetiteView']
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            enumTypes: {
                value: {
                    appetite: Y.usp.resolveassessment.enum.Appetite.values
                }
            }
        }
    });

    //Feelings
    Y.namespace('app').IntakeFormApp2FeelingsView = Y.Base.create('intakeFormApp2FeelingsView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2FeelingsView']
    });

    //Other
    Y.namespace('app').IntakeFormApp2OtherView = Y.Base.create('intakeFormApp2OtherView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2OtherView']
    }, {
        ATTRS: {
            modelList: {},
            formModel: {},
            labels: {},
            enumTypes: {
                value: {
                    anxiety: Y.usp.resolveassessment.enum.Anxiety.values
                }
            }
        }
    });

    //Outcome
    Y.namespace('app').IntakeFormApp2OutcomeView = Y.Base.create('intakeFormApp2OutcomeView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp2OutcomeView']
    });

    //Accordions

    //Context
    Y.namespace('app').IntakeFormApp2ContextAccordion = Y.Base.create('intakeFormApp2ContextAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2ContextView = new Y.app.IntakeFormApp2ContextView(config);

            // Add event targets
            this.intakeFormApp2ContextView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2ContextAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2ContextView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2ContextView.destroy();

            delete this.intakeFormApp2ContextView;
        }
    });

    //MentalState1
    Y.namespace('app').IntakeFormApp2MentalState1Accordion = Y.Base.create('intakeFormApp2MentalState1Accordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2MentalState1View = new Y.app.IntakeFormApp2MentalState1View(config);

            // Add event targets
            this.intakeFormApp2MentalState1View.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2MentalState1Accordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2MentalState1View.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2MentalState1View.destroy();

            delete this.intakeFormApp2MentalState1View;
        }
    });

    //MentalState2
    Y.namespace('app').IntakeFormApp2MentalState2Accordion = Y.Base.create('intakeFormApp2MentalState2Accordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2MentalState2View = new Y.app.IntakeFormApp2MentalState2View(config);

            // Add event targets
            this.intakeFormApp2MentalState2View.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2MentalState2Accordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2MentalState2View.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2MentalState2View.destroy();

            delete this.intakeFormApp2MentalState2View;
        }
    });

    //MentalState3
    Y.namespace('app').IntakeFormApp2MentalState3Accordion = Y.Base.create('intakeFormApp2MentalState3Accordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2MentalState3View = new Y.app.IntakeFormApp2MentalState3View(config);

            // Add event targets
            this.intakeFormApp2MentalState3View.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2MentalState3Accordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2MentalState3View.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2MentalState3View.destroy();

            delete this.intakeFormApp2MentalState3View;
        }
    });

    //PlaceOrientation
    Y.namespace('app').IntakeFormApp2PlaceOrientationAccordion = Y.Base.create('intakeFormApp2PlaceOrientationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2PlaceOrientationView = new Y.app.IntakeFormApp2PlaceOrientationView(config);

            // Add event targets
            this.intakeFormApp2PlaceOrientationView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2PlaceOrientationAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2PlaceOrientationView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2PlaceOrientationView.destroy();

            delete this.intakeFormApp2PlaceOrientationView;
        }
    });

    //TimeOrientation
    Y.namespace('app').IntakeFormApp2TimeOrientationAccordion = Y.Base.create('intakeFormApp2TimeOrientationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2TimeOrientationView = new Y.app.IntakeFormApp2TimeOrientationView(config);

            // Add event targets
            this.intakeFormApp2TimeOrientationView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2TimeOrientationAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2TimeOrientationView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2TimeOrientationView.destroy();

            delete this.intakeFormApp2TimeOrientationView;
        }
    });

    Y.namespace('app').IntakeFormApp2SleepPatternAccordion = Y.Base.create('intakeFormApp2SleepPatternAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2SleepPatternView = new Y.app.IntakeFormApp2SleepPatternView(config);

            // Add event targets
            this.intakeFormApp2SleepPatternView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2SleepPatternAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2SleepPatternView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2SleepPatternView.destroy();

            delete this.intakeFormApp2SleepPatternView;
        }
    });

    Y.namespace('app').IntakeFormApp2AppetiteAccordion = Y.Base.create('intakeFormApp2AppetiteAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2AppetiteView = new Y.app.IntakeFormApp2AppetiteView(config);

            // Add event targets
            this.intakeFormApp2AppetiteView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2AppetiteAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2AppetiteView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2AppetiteView.destroy();

            delete this.intakeFormApp2AppetiteView;
        }
    });

    Y.namespace('app').IntakeFormApp2FeelingsAccordion = Y.Base.create('intakeFormApp2FeelingsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2FeelingsView = new Y.app.IntakeFormApp2FeelingsView(config);

            // Add event targets
            this.intakeFormApp2FeelingsView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2FeelingsAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2FeelingsView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2FeelingsView.destroy();

            delete this.intakeFormApp2FeelingsView;
        }
    });

    Y.namespace('app').IntakeFormApp2OtherAccordion = Y.Base.create('intakeFormApp2OtherAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2OtherView = new Y.app.IntakeFormApp2OtherView(config);

            // Add event targets
            this.intakeFormApp2OtherView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2OtherAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2OtherView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2OtherView.destroy();

            delete this.intakeFormApp2OtherView;
        }
    });

    Y.namespace('app').IntakeFormApp2OutcomeAccordion = Y.Base.create('intakeFormApp2OutcomeAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp2OutcomeView = new Y.app.IntakeFormApp2OutcomeView(config);

            // Add event targets
            this.intakeFormApp2OutcomeView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp2OutcomeAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp2OutcomeView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp2OutcomeView.destroy();

            delete this.intakeFormApp2OutcomeView;
        }
    });

    //Tab
    Y.namespace('app').IntakeFormApp2MentalHealthTab = Y.Base.create('intakeFormApp2MentalHealthTab', Y.View, [], {
        initializer: function() {
            var app2ContextConfig = this.get('app2ContextConfig'),
                app2MentalState1Config = this.get('app2MentalState1Config'),
                app2MentalState2Config = this.get('app2MentalState2Config'),
                app2MentalState3Config = this.get('app2MentalState3Config'),
                app2PlaceOrientationConfig = this.get('app2PlaceOrientationConfig'),
                app2TimeOrientationConfig = this.get('app2TimeOrientationConfig'),
                app2SleepPatternConfig = this.get('app2SleepPatternConfig'),
                app2AppetiteConfig = this.get('app2AppetiteConfig'),
                app2FeelingsConfig = this.get('app2FeelingsConfig'),
                app2OtherConfig = this.get('app2OtherConfig'),
                app2OutcomeConfig = this.get('app2OutcomeConfig');

            //create the accordion views
            this.app2ContextAccordion = new Y.app.IntakeFormApp2ContextAccordion(
                Y.merge(
                    app2ContextConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2MentalState1Accordion = new Y.app.IntakeFormApp2MentalState1Accordion(
                Y.merge(
                    app2MentalState1Config, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2MentalState2Accordion = new Y.app.IntakeFormApp2MentalState2Accordion(
                Y.merge(
                    app2MentalState2Config, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2MentalState3Accordion = new Y.app.IntakeFormApp2MentalState3Accordion(
                Y.merge(
                    app2MentalState3Config, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2PlaceOrientationAccordion = new Y.app.IntakeFormApp2PlaceOrientationAccordion(
                Y.merge(
                    app2PlaceOrientationConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2TimeOrientationAccordion = new Y.app.IntakeFormApp2TimeOrientationAccordion(
                Y.merge(
                    app2TimeOrientationConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2SleepPatternAccordion = new Y.app.IntakeFormApp2SleepPatternAccordion(
                Y.merge(
                    app2SleepPatternConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2AppetiteAccordion = new Y.app.IntakeFormApp2AppetiteAccordion(
                Y.merge(
                    app2AppetiteConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2FeelingsAccordion = new Y.app.IntakeFormApp2FeelingsAccordion(
                Y.merge(
                    app2FeelingsConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2OtherAccordion = new Y.app.IntakeFormApp2OtherAccordion(
                Y.merge(
                    app2OtherConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app2OutcomeAccordion = new Y.app.IntakeFormApp2OutcomeAccordion(
                Y.merge(
                    app2OutcomeConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));

            // Add event targets            
            this.app2ContextAccordion.addTarget(this);
            this.app2MentalState1Accordion.addTarget(this);
            this.app2MentalState2Accordion.addTarget(this);
            this.app2MentalState3Accordion.addTarget(this);
            this.app2PlaceOrientationAccordion.addTarget(this);
            this.app2TimeOrientationAccordion.addTarget(this);
            this.app2SleepPatternAccordion.addTarget(this);
            this.app2AppetiteAccordion.addTarget(this);
            this.app2FeelingsAccordion.addTarget(this);
            this.app2OtherAccordion.addTarget(this);
            this.app2OutcomeAccordion.addTarget(this);
            
            //load appendix model - load ASAP 
            this.get('model').load();
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.app2ContextAccordion.render().get('container'));
            contentNode.append(this.app2MentalState1Accordion.render().get('container'));
            contentNode.append(this.app2MentalState2Accordion.render().get('container'));
            contentNode.append(this.app2MentalState3Accordion.render().get('container'));
            contentNode.append(this.app2PlaceOrientationAccordion.render().get('container'));
            contentNode.append(this.app2TimeOrientationAccordion.render().get('container'));
            contentNode.append(this.app2SleepPatternAccordion.render().get('container'));
            contentNode.append(this.app2AppetiteAccordion.render().get('container'));
            contentNode.append(this.app2FeelingsAccordion.render().get('container'));
            contentNode.append(this.app2OtherAccordion.render().get('container'));
            contentNode.append(this.app2OutcomeAccordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties            
            this.app2ContextAccordion.destroy();
            delete this.app2ContextAccordion;

            this.app2MentalState1Accordion.destroy();
            delete this.app2MentalState1Accordion;

            this.app2MentalState2Accordion.destroy();
            delete this.app2MentalState2Accordion;

            this.app2MentalState3Accordion.destroy();
            delete this.app2MentalState3Accordion;

            this.app2PlaceOrientationAccordion.destroy();
            delete this.app2PlaceOrientationAccordion;

            this.app2TimeOrientationAccordion.destroy();
            delete this.app2TimeOrientationAccordion;

            this.app2SleepPatternAccordion.destroy();
            delete this.app2SleepPatternAccordion;

            this.app2AppetiteAccordion.destroy();
            delete this.app2AppetiteAccordion;

            this.app2FeelingsAccordion.destroy();
            delete this.app2FeelingsAccordion;

            this.app2OtherAccordion.destroy();
            delete this.app2OtherAccordion;

            this.app2OutcomeAccordion.destroy();
            delete this.app2OutcomeAccordion;

        }
    }, {
        ATTRS: {
            model: {},
            formModel: {},
            app2ContextConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2MentalState1Config: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2MentalState2Config: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2MentalState3Config: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2PlaceOrientationConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2TimeOrientationConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2SleepPatternConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2AppetiteConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2FeelingsConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2OtherConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app2OutcomeConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeForm'
            ]
});