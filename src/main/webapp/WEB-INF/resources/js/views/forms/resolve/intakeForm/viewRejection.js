YUI.add('intakeform-rejection-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormRejectionView = Y.Base.create('intakeFormRejectionView', Y.usp.resolveassessment.FormRejectionView, [BaseFormView], {
        template: Y.Handlebars.templates['formRejectionView']
    });

    Y.namespace('app').IntakeFormRejectionAccordion = Y.Base.create('intakeFormRejectionAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new IntakeFormRejection View and configure the template
            this.intakeFormRejectionView = new Y.app.IntakeFormRejectionView(Y.merge(config, {}));

            // Add event targets
            this.intakeFormRejectionView.addTarget(this);
        },
        render: function() {
            //render the view
            var intakeFormRejectionViewContainer = this.intakeFormRejectionView.render().get('container');

            //superclass call
            Y.app.IntakeFormRejectionAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(intakeFormRejectionViewContainer);

            return this;
        },
        destructor: function() {
            this.intakeFormRejectionView.destroy();

            delete this.intakeFormRejectionView;
        }
    }, {
        ATTRS: {}
    });

    Y.namespace('app').IntakeFormRejectionTab = Y.Base.create('intakeFormRejectionTab', Y.View, [], {
        initializer: function() {
            var rejectionConfig = this.get('rejectionConfig');

            //create rejection view
            this.rejectionView = new Y.app.IntakeFormRejectionAccordion(Y.merge(rejectionConfig, {
                model: this.get('model')
            }));

            // Add event targets
            this.rejectionView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.rejectionView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up rejection view
            this.rejectionView.destroy();
            delete this.rejectionView;
        }
    }, {
        ATTRS: {
            model: {},
            rejectionConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-resolveform-templates',
               'usp-resolveassessment-FormRejection'
              ]
});