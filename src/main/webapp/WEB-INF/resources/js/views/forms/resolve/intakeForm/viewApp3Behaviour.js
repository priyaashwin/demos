YUI.add('intakeform-app3behaviour-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseAppendixFormView() {}
    BaseAppendixFormView.prototype = {
        initializer: function() {
            var formModel = this.get('formModel');

            // We'll re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    formModel: this.get('formModel').toJSON(),
                    codedEntries: this.get('codedEntries')
                });

            // Render this view's HTML into the container element.
            container.setHTML(html);

            return this;
        },
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').IntakeFormApp3ContextView = Y.Base.create('intakeFormApp3ContextView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp3ContextView']
    });

    Y.namespace('app').IntakeFormApp3Behaviour1View = Y.Base.create('intakeFormApp3Behaviour1View', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp3Behaviour1View']
    });

    Y.namespace('app').IntakeFormApp3Behaviour2View = Y.Base.create('intakeFormApp3Behaviour2View', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp3Behaviour2View']
    });

    Y.namespace('app').IntakeFormApp3ContextAccordion = Y.Base.create('intakeFormApp3ContextAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp3ContextView = new Y.app.IntakeFormApp3ContextView(config);

            // Add event targets
            this.intakeFormApp3ContextView.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp3ContextAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp3ContextView.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp3ContextView.destroy();
            delete this.intakeFormApp3ContextView;
        }
    });

    Y.namespace('app').IntakeFormApp3Behaviour1Accordion = Y.Base.create('intakeFormApp3Behaviour1Accordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp3Behaviour1View = new Y.app.IntakeFormApp3Behaviour1View(config);
            // Add event targets
            this.intakeFormApp3Behaviour1View.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp3Behaviour1Accordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp3Behaviour1View.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp3Behaviour1View.destroy();
            delete this.intakeFormApp3Behaviour1View;
        }
    });

    Y.namespace('app').IntakeFormApp3Behaviour2Accordion = Y.Base.create('intakeFormApp3Behaviour2Accordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create view passing on configuration object
            this.intakeFormApp3Behaviour2View = new Y.app.IntakeFormApp3Behaviour2View(config);
            // Add event targets
            this.intakeFormApp3Behaviour2View.addTarget(this);
        },
        render: function() {
            //superclass call
            Y.app.IntakeFormApp3Behaviour2Accordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp3Behaviour2View.render().get('container'));

            return this;
        },
        destructor: function() {
            this.intakeFormApp3Behaviour2View.destroy();
            delete this.intakeFormApp3Behaviour2View;
        }
    });

    Y.namespace('app').IntakeFormApp3BehaviourTab = Y.Base.create('intakeFormApp3BehaviourTab', Y.View, [], {
        initializer: function() {
            var app3ContextConfig = this.get('app3ContextConfig'),
                app3Behaviour1Config = this.get('app3Behaviour1Config'),
                app3Behaviour2Config = this.get('app3Behaviour2Config');

            //create the accordion views
            this.app3ContextAccordion = new Y.app.IntakeFormApp3ContextAccordion(
                Y.merge(
                    app3ContextConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app3Behaviour1Accordion = new Y.app.IntakeFormApp3Behaviour1Accordion(
                Y.merge(
                    app3Behaviour1Config, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app3Behaviour2Accordion = new Y.app.IntakeFormApp3Behaviour2Accordion(
                Y.merge(
                    app3Behaviour2Config, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            // Add event targets            
            this.app3ContextAccordion.addTarget(this);
            this.app3Behaviour1Accordion.addTarget(this);
            this.app3Behaviour2Accordion.addTarget(this);
            
            //load appendix model - load ASAP 
            this.get('model').load();            
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.app3ContextAccordion.render().get('container'));
            contentNode.append(this.app3Behaviour1Accordion.render().get('container'));
            contentNode.append(this.app3Behaviour2Accordion.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up accordion views and delete properties            
            this.app3ContextAccordion.destroy();
            delete this.app3ContextAccordion;

            this.app3Behaviour1Accordion.destroy();
            delete this.app3Behaviour1Accordion;

            this.app3Behaviour2Accordion.destroy();
            delete this.app3Behaviour2Accordion;
        }
    }, {
        ATTRS: {
            model: {},
            app3ContextConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app3Behaviour1Config: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            app3Behaviour2Config: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeForm'
              ]
});