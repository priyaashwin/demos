YUI.add('cpraform-plan-and-risks-tabs', function(Y) {
    Y.namespace('app').CPRAFormHealthTab = Y.Base.create('cpraFormHealthTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'HEALTH_AND_MEDICATION'
            }
        }
    });

    Y.namespace('app').CPRAFormEmotionalTab = Y.Base.create('cpraFormEmotionalTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'EMOTIONAL_AND_BEHAVIOURAL'
            }
        }
    });

    Y.namespace('app').CPRAFormIdentityTab = Y.Base.create('cpraFormIdentityTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'IDENTITY_FAMILY_AND_SOCIAL_RELATIONSHIPS'
            }
        }
    });

    Y.namespace('app').CPRAFormIdentityTab = Y.Base.create('cpraFormIdentityTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'IDENTITY_FAMILY_AND_SOCIAL_RELATIONSHIPS'
            }
        }
    });

    Y.namespace('app').CPRAFormSelfCareTab = Y.Base.create('cpraFormSelfCareTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'SOCIAL_PRESENTATION_AND_SELF_CARE_SKILLS'
            }
        }
    });


    Y.namespace('app').CPRAFormILSTab = Y.Base.create('cpraFormILSTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'INDEPENDENT_LIVING_SKILLS'
            }
        }
    });

    Y.namespace('app').CPRAFormDayCareTab = Y.Base.create('cpraFormDayCareTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'DAY_CARE_PLAN'
            }
        }
    });

    Y.namespace('app').CPRAFormFinanceTab = Y.Base.create('cpraFormFinanceTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'FINANCE'
            }
        }
    });

    Y.namespace('app').CPRAFormConsultationTab = Y.Base.create('cpraFormConsultationTab', Y.app.PlanAccordion, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'MANAGER_STAFF_CONSULTATION'
            }
        }
    });

    Y.namespace('app').CPRAFormSpecialistsTab = Y.Base.create('cpraFormSpecialistsTab', Y.app.CPRAPlanAndRiskTable, [], {}, {
        ATTRS: {
            planType: {
                //this should/could be from the enumeration
                value: 'SPECIALIST_SERVICES'
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'cpraform-plan-and-risk-table'
              ]
});