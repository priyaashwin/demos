YUI.add('cpraform-views-tab', function(Y) {
    /**
     * Common functions and properties that will be augmented against views
     */
    function BaseFormView() {}
    BaseFormView.prototype = {
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    Y.namespace('app').CPRAFormServiceUserView = Y.Base.create('cpraFormServiceUserView', Y.usp.resolveassessment.CarePlanRiskAssessmentView, [BaseFormView], {
        template: Y.Handlebars.templates['cpraFormServiceUserView']
    });

    Y.namespace('app').CPRAFormRelativeView = Y.Base.create('cpraFormRelativeView', Y.usp.resolveassessment.CarePlanRiskAssessmentView, [BaseFormView], {
        template: Y.Handlebars.templates['cpraFormRelativeView']
    });

    Y.namespace('app').CPRAFormCarersPerspectiveView = Y.Base.create('cpraFormCarersPerspectiveView', Y.usp.resolveassessment.CarePlanRiskAssessmentView, [BaseFormView], {
        template: Y.Handlebars.templates['cpraFormCarersPerspectiveView']
    });

    Y.namespace('app').CPRAFormServiceUserAccordion = Y.Base.create('cpraFormServiceUserAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new CPRAFormServiceUser View and configure the template
            this.cpraFormServiceUserView = new Y.app.CPRAFormServiceUserView(config);

            // Add event targets
            this.cpraFormServiceUserView.addTarget(this);
        },
        render: function() {
            //render the view
            var cpraFormServiceUserViewContainer = this.cpraFormServiceUserView.render().get('container');

            //superclass call
            Y.app.CPRAFormServiceUserAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(cpraFormServiceUserViewContainer);

            return this;
        },
        destructor: function() {
            this.cpraFormServiceUserView.destroy();

            delete this.cpraFormServiceUserView;
        }
    }, {
        ATTRS: {
            canUpdate: {
                value: false
            }
        }
    });

    Y.namespace('app').CPRAFormRelativeAccordion = Y.Base.create('cpraFormRelativeAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new CPRAFormRelative View and configure the template
            this.cpraFormRelativeView = new Y.app.CPRAFormRelativeView(config);

            // Add event targets
            this.cpraFormRelativeView.addTarget(this);
        },
        render: function() {
            //render the view
            var cpraFormRelativeViewContainer = this.cpraFormRelativeView.render().get('container');

            //superclass call
            Y.app.CPRAFormRelativeAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(cpraFormRelativeViewContainer);

            return this;
        },
        destructor: function() {
            this.cpraFormRelativeView.destroy();

            delete this.cpraFormRelativeView;
        }
    }, {
        ATTRS: {
            canUpdate: {
                value: false
            }
        }
    });

    Y.namespace('app').CPRAFormCarersPerspectiveAccordion = Y.Base.create('cpraFormCarersPerspectiveAccordion', Y.usp.AccordionPanel, [], {
        initializer: function(config) {
            //Create a new CPRAFormServiceUser View and configure the template
            this.cpraFormCarersPerspectiveView = new Y.app.CPRAFormCarersPerspectiveView(config);

            // Add event targets
            this.cpraFormCarersPerspectiveView.addTarget(this);
        },
        render: function() {
            //render the view
            var cpraFormCarersPerspectiveViewContainer = this.cpraFormCarersPerspectiveView.render().get('container');

            //superclass call
            Y.app.CPRAFormCarersPerspectiveAccordion.superclass.render.call(this);

            //stick the view container into the accordion
            this.getAccordionBody().appendChild(cpraFormCarersPerspectiveViewContainer);

            return this;
        },
        destructor: function() {
            this.cpraFormCarersPerspectiveView.destroy();

            delete this.cpraFormCarersPerspectiveView;
        }
    }, {
        ATTRS: {
            canUpdate: {
                value: false
            }
        }
    });

    Y.namespace('app').CPRAFormViewsTab = Y.Base.create('cpraFormViewsTab', Y.View, [], {
        initializer: function() {
            var viewsConfig = this.get('viewsConfig'),
                relativeConfig = this.get('relativeConfig'),
                carersConfig = this.get('carersConfig');

            //create Views view
            this.serviceUserView = new Y.app.CPRAFormServiceUserAccordion(
                Y.merge(
                    viewsConfig, {
                        model: this.get('model')
                    }));

            this.relativeView = new Y.app.CPRAFormRelativeAccordion(
                Y.merge(
                    relativeConfig, {
                        model: this.get('model')
                    }));

            this.carersView = new Y.app.CPRAFormCarersPerspectiveAccordion(
                Y.merge(
                    carersConfig, {
                        model: this.get('model')
                    }));

            // Add event targets
            this.serviceUserView.addTarget(this);
            this.relativeView.addTarget(this);
            this.carersView.addTarget(this);
        },
        render: function() {
            //render the portions of the page
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.serviceUserView.render().get('container'));
            contentNode.append(this.relativeView.render().get('container'));
            contentNode.append(this.carersView.render().get('container'));

            //finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            //clean up service user view
            this.serviceUserView.destroy();
            delete this.serviceUserView;

            //clean up relative view
            this.relativeView.destroy();
            delete this.relativeView;

            //clean up carers view
            this.carersView.destroy();
            delete this.carersView;
        }
    }, {
        ATTRS: {
            model: {},
            viewsConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            relativeConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            },
            carersConfig: {
                value: {
                    title: {},
                    labels: {}
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-cpraform-templates',
               'usp-resolveassessment-CarePlanRiskAssessment'
               ]
});