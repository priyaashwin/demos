YUI.add('intakeform-app1comms-tab', function (Y) {    
    function BaseAppendixFormView() {}
    BaseAppendixFormView.prototype = {
        initializer: function() {
            var formModel = this.get('formModel');

            // We'll re-render the view whenever the data of the parent form model changes.
            formModel.after('*:change', this.render, this);
        },
        render: function() {
            var container = this.get('container'),
                html = this.template({
                    labels: this.get('labels'),
                    modelData: this.get('model').toJSON(),
                    formModel: this.get('formModel').toJSON(),
                    codedEntries: this.get('codedEntries')
                });

            // Render this view's HTML into the container element.
            container.setHTML(html);

            return this;
        },
        _editForm: function(e) {
            e.preventDefault();

            //Fire the edit event
            this.fire('editForm', {
                id: this.get('model').get('id')
            });
        },
        events: {
            '.edit-form': {
                click: '_editForm'
            }
        }
    };

    //Views 
    
    Y.namespace('app').IntakeFormApp1CommsView = Y.Base.create('intakeFormApp1CommsView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1CommunicationView']
    });

    Y.namespace('app').IntakeFormApp1Family1View = Y.Base.create('intakeFormApp1Family1View', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1Family1View']
    });

    Y.namespace('app').IntakeFormApp1Family2View = Y.Base.create('intakeFormApp1Family2View', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1Family2View']
    });
 
    Y.namespace('app').IntakeFormApp1ReviewsView = Y.Base.create('intakeFormApp1ReviewsView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1ReviewsView']
    });

    Y.namespace('app').IntakeFormApp1VisionView = Y.Base.create('intakeFormApp1VisionView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1VisionView']
    });

    Y.namespace('app').IntakeFormApp1HearingView = Y.Base.create('intakeFormApp1HearingView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1HearingView']
    });
 
    Y.namespace('app').IntakeFormApp1DentalView = Y.Base.create('intakeFormApp1DentalView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1DentalView']
    });
    
    Y.namespace('app').IntakeFormApp1FootcareView = Y.Base.create('intakeFormApp1FootcareView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1FootcareView']
    });
    
    Y.namespace('app').IntakeFormApp1ContinenceView = Y.Base.create('intakeFormApp1ContinenceView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1ContinenceView']
    });
    
    Y.namespace('app').IntakeFormApp1EpilepsyView = Y.Base.create('intakeFormApp1EpilepsyView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1EpilepsyView']
    });
    
    Y.namespace('app').IntakeFormApp1SkinView = Y.Base.create('intakeFormApp1SkinView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1SkinView']
    });
    
    Y.namespace('app').IntakeFormApp1BreathingCirculationView = Y.Base.create('intakeFormApp1BreathingCirculationView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1BreathingCirculationView']
    });
    
    Y.namespace('app').IntakeFormApp1MensHealthView = Y.Base.create('intakeFormApp1MensHealthView', Y.usp.resolveassessment.IntakeFormView, [BaseAppendixFormView], {
        template: Y.Handlebars.templates['intakeFormApp1MensHealthView']
    });
    
    Y.namespace('app').IntakeFormApp1CommsAccordion = Y.Base.create('intakeFormApp1CommsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1CommsView=new Y.app.IntakeFormApp1CommsView(config);

            // Add event targets
            this.intakeFormApp1CommsView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1CommsAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1CommsView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1CommsView.destroy();

            delete this.intakeFormApp1CommsView;
        }
    });  
 
    Y.namespace('app').IntakeFormApp1Family1Accordion = Y.Base.create('intakeFormApp1Family1Accordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1Family1View=new Y.app.IntakeFormApp1Family1View(config);

            // Add event targets
            this.intakeFormApp1Family1View.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1Family1Accordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1Family1View.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1Family1View.destroy();

            delete this.intakeFormApp1Family1View;
        }
    });  

    Y.namespace('app').IntakeFormApp1Family2Accordion = Y.Base.create('intakeFormApp1Family2Accordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1Family2View=new Y.app.IntakeFormApp1Family2View(config);

            // Add event targets
            this.intakeFormApp1Family2View.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1Family2Accordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1Family2View.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1Family2View.destroy();

            delete this.intakeFormApp1Family2View;
        }
    });

    Y.namespace('app').IntakeFormApp1ReviewsAccordion = Y.Base.create('intakeFormApp1ReviewsAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1ReviewsView=new Y.app.IntakeFormApp1ReviewsView(config);

            // Add event targets
            this.intakeFormApp1ReviewsView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1ReviewsAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1ReviewsView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1ReviewsView.destroy();

            delete this.intakeFormApp1ReviewsView;
        }
    });
 
    Y.namespace('app').IntakeFormApp1VisionAccordion = Y.Base.create('intakeFormApp1VisionAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1VisionView=new Y.app.IntakeFormApp1VisionView(config);

            // Add event targets
            this.intakeFormApp1VisionView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1VisionAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1VisionView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1VisionView.destroy();

            delete this.intakeFormApp1VisionView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1HearingAccordion = Y.Base.create('intakeFormApp1HearingAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1HearingView=new Y.app.IntakeFormApp1HearingView(config);

            // Add event targets
            this.intakeFormApp1HearingView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1HearingAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1HearingView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1HearingView.destroy();

            delete this.intakeFormApp1HearingView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1DentalAccordion = Y.Base.create('intakeFormApp1DentalAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1DentalView=new Y.app.IntakeFormApp1DentalView(config);

            // Add event targets
            this.intakeFormApp1DentalView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1DentalAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1DentalView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1DentalView.destroy();

            delete this.intakeFormApp1DentalView;
        }
    });

    Y.namespace('app').IntakeFormApp1FootcareAccordion = Y.Base.create('intakeFormApp1FootcareAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1FootcareView=new Y.app.IntakeFormApp1FootcareView(config);

            // Add event targets
            this.intakeFormApp1FootcareView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1FootcareAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1FootcareView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1FootcareView.destroy();

            delete this.intakeFormApp1FootcareView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1ContinenceAccordion = Y.Base.create('intakeFormApp1ContinenceAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1ContinenceView=new Y.app.IntakeFormApp1ContinenceView(config);

            // Add event targets
            this.intakeFormApp1ContinenceView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1ContinenceAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1ContinenceView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1ContinenceView.destroy();

            delete this.intakeFormApp1ContinenceView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1EpilepsyAccordion = Y.Base.create('intakeFormApp1EpilepsyAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1EpilepsyView=new Y.app.IntakeFormApp1EpilepsyView(config);

            // Add event targets
            this.intakeFormApp1EpilepsyView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1EpilepsyAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1EpilepsyView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1EpilepsyView.destroy();

            delete this.intakeFormApp1EpilepsyView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1SkinAccordion = Y.Base.create('intakeFormApp1SkinAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1SkinView=new Y.app.IntakeFormApp1SkinView(config);

            // Add event targets
            this.intakeFormApp1SkinView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1SkinAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1SkinView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1SkinView.destroy();

            delete this.intakeFormApp1SkinView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1BreathingCirculationAccordion = Y.Base.create('intakeFormApp1BreathingCirculationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1BreathingCirculationView=new Y.app.IntakeFormApp1BreathingCirculationView(config);

            // Add event targets
            this.intakeFormApp1BreathingCirculationView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1BreathingCirculationAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1BreathingCirculationView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1BreathingCirculationView.destroy();

            delete this.intakeFormApp1BreathingCirculationView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1MensHealthAccordion = Y.Base.create('intakeFormApp1MensHealthAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //Create view passing on configuration object
            this.intakeFormApp1MensHealthView=new Y.app.IntakeFormApp1MensHealthView(config);

            // Add event targets
            this.intakeFormApp1MensHealthView.addTarget(this);
        },
        render:function(){
            //superclass call
            Y.app.IntakeFormApp1MensHealthAccordion.superclass.render.call(this);

            //render the view and append to the accordion
            this.getAccordionBody().appendChild(this.intakeFormApp1MensHealthView.render().get('container'));

            return this;
        },
        destructor:function(){
            this.intakeFormApp1MensHealthView.destroy();

            delete this.intakeFormApp1MensHealthView;
        }
    });
    
    Y.namespace('app').IntakeFormApp1CommsTab = Y.Base.create('intakeFormApp1CommsTab', Y.View, [], {
    	initializer: function () {
    		var app1CommsConfig=this.get('app1CommsConfig'),
    		app1Family1Config=this.get('app1Family1Config'),
    		app1Family2Config=this.get('app1Family2Config'),
    		app1ReviewsConfig=this.get('app1ReviewsConfig'),
    		app1VisionConfig=this.get('app1VisionConfig'),
    		app1HearingConfig=this.get('app1HearingConfig'),
    		app1DentalConfig=this.get('app1DentalConfig'),
    		app1FootcareConfig=this.get('app1FootcareConfig'),
    		app1ContinenceConfig=this.get('app1ContinenceConfig'),
    		app1EpilepsyConfig=this.get('app1EpilepsyConfig'),
    		app1SkinConfig=this.get('app1SkinConfig'),
    		app1BreathingCirculationConfig=this.get('app1BreathingCirculationConfig'),
    		app1MensHealthConfig=this.get('app1MensHealthConfig');

        	//create the accordion views
            this.app1CommsAccordion=new Y.app.IntakeFormApp1CommsAccordion(
                Y.merge(
                    app1CommsConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1Family1Accordion=new Y.app.IntakeFormApp1Family1Accordion(
                Y.merge(
                    app1Family1Config, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1Family2Accordion=new Y.app.IntakeFormApp1Family2Accordion(
                Y.merge(
                    app1Family2Config, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1ReviewsAccordion=new Y.app.IntakeFormApp1ReviewsAccordion(
                Y.merge(
                    app1ReviewsConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1VisionAccordion=new Y.app.IntakeFormApp1VisionAccordion(
                Y.merge(
                    app1VisionConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1HearingAccordion=new Y.app.IntakeFormApp1HearingAccordion(
                Y.merge(
                    app1HearingConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1DentalAccordion=new Y.app.IntakeFormApp1DentalAccordion(
                Y.merge(
                    app1DentalConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1FootcareAccordion=new Y.app.IntakeFormApp1FootcareAccordion(
                Y.merge(
                    app1FootcareConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1ContinenceAccordion=new Y.app.IntakeFormApp1ContinenceAccordion(
                Y.merge(
                    app1ContinenceConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1EpilepsyAccordion=new Y.app.IntakeFormApp1EpilepsyAccordion(
                Y.merge(
                    app1EpilepsyConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1SkinAccordion=new Y.app.IntakeFormApp1SkinAccordion(
                Y.merge(
                    app1SkinConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1BreathingCirculationAccordion=new Y.app.IntakeFormApp1BreathingCirculationAccordion(
                Y.merge(
                    app1BreathingCirculationConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            this.app1MensHealthAccordion=new Y.app.IntakeFormApp1MensHealthAccordion(
                Y.merge(
                    app1MensHealthConfig, {
                        model: this.get('model'),
                        formModel: this.get('formModel')
                    }));
            // Add event targets            
            this.app1CommsAccordion.addTarget(this);
            this.app1Family1Accordion.addTarget(this);
            this.app1Family2Accordion.addTarget(this);
            this.app1ReviewsAccordion.addTarget(this);
            this.app1VisionAccordion.addTarget(this);
            this.app1HearingAccordion.addTarget(this);
            this.app1DentalAccordion.addTarget(this);
            this.app1FootcareAccordion.addTarget(this);
            this.app1ContinenceAccordion.addTarget(this);
            this.app1EpilepsyAccordion.addTarget(this);
            this.app1SkinAccordion.addTarget(this);
            this.app1BreathingCirculationAccordion.addTarget(this);
            this.app1MensHealthAccordion.addTarget(this);
            
            //load appendix model - load ASAP 
            this.get('model').load();
        },
    	render:function(){
    		//render the portions of the page
    		var contentNode=Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
    		contentNode.append(this.app1CommsAccordion.render().get('container'));
    		contentNode.append(this.app1Family1Accordion.render().get('container'));
    		contentNode.append(this.app1Family2Accordion.render().get('container'));
    		contentNode.append(this.app1ReviewsAccordion.render().get('container'));
    		contentNode.append(this.app1VisionAccordion.render().get('container'));
    		contentNode.append(this.app1HearingAccordion.render().get('container'));
    		contentNode.append(this.app1DentalAccordion.render().get('container'));
    		contentNode.append(this.app1FootcareAccordion.render().get('container'));
    		contentNode.append(this.app1ContinenceAccordion.render().get('container'));
    		contentNode.append(this.app1EpilepsyAccordion.render().get('container'));
    		contentNode.append(this.app1SkinAccordion.render().get('container'));
    		contentNode.append(this.app1BreathingCirculationAccordion.render().get('container'));
    		contentNode.append(this.app1MensHealthAccordion.render().get('container'));
    		
    		//finally set the content into the container
            this.get('container').setHTML(contentNode);

            return this;
    	},
    	destructor: function () {
    		//clean up accordion views and delete properties    	    
            this.app1CommsAccordion.destroy();
            delete this.app1CommsAccordion;
            
            this.app1Family1Accordion.destroy();
            delete this.app1Family1Accordion;
            
            this.app1Family2Accordion.destroy();
            delete this.app1Family2Accordion;
            
            this.app1ReviewsAccordion.destroy();
            delete this.app1ReviewsAccordion;
            
            this.app1VisionAccordion.destroy();
            delete this.app1VisionAccordion;
            
            this.app1HearingAccordion.destroy();
            delete this.app1HearingAccordion;
            
            this.app1DentalAccordion.destroy();
            delete this.app1DentalAccordion;
            
            this.app1FootcareAccordion.destroy();
            delete this.app1FootcareAccordion;
            
            this.app1ContinenceAccordion.destroy();
            delete this.app1ContinenceAccordion;
            
            this.app1EpilepsyAccordion.destroy();
            delete this.app1EpilepsyAccordion;
            
            this.app1SkinAccordion.destroy();
            delete this.app1SkinAccordion;
            
            this.app1BreathingCirculationAccordion.destroy();
            delete this.app1BreathingCirculationAccordion;
            
            this.app1MensHealthAccordion.destroy();
            delete this.app1MensHealthAccordion;
        }
    },{
    	ATTRS:{
    		model:{},
    		app1CommsConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1Family1Config:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1Family2Config:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1ReviewsConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1VisionConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1HearingConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1DentalConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1FootcareConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1ContinenceConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1EpilepsyConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1SkinConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1BreathingCirculationConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            },
    		app1MensHealthConfig:{
                value:{
                    title:{},
                    labels:{}
                }
            }
    	}
    });
}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'handlebars-helpers',
               'handlebars-intakeform-templates',
               'usp-resolveassessment-IntakeFormFullDetails']
});