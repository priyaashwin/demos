YUI.add('form-instance-filter', function(Y) {
    'use-strict';

    var L = Y.Lang,
        FUtil = Y.FUtil,
        DB = Y.usp.DataBindingUtil;

    Y.namespace('app.form').FormInstanceFilterModel = Y.Base.create('formInstanceFilterModel', Y.usp.ResultsFilterModel, [Y.app.attachment.EntityWithAttachmentFilterModelMixin], {
        customValidation: function(attrs) {
            //enforce our validation first - if we succeed this will call the parent validate
            var errors = {},
                startDateFrom = attrs['startDateFrom'],
                startDateTo = attrs['startDateTo'],
                completionDateFrom = attrs['completionDateFrom'],
                completionDateTo = attrs['completionDateTo'],
                submittedDateFrom = attrs['submittedDateFrom'],
                submittedDateTo = attrs['submittedDateTo'],
                labels = this.labels || {},
                validationLabels = labels.validation || {};

            if (L.isNumber(startDateFrom) && L.isNumber(startDateTo)) {
                if (startDateFrom > startDateTo) {
                    errors['startDateFrom'] = validationLabels.endBeforeStartDate;
                }
            }

            if (L.isNumber(completionDateFrom) && L.isNumber(completionDateTo)) {
                if (completionDateFrom > completionDateTo) {
                    errors['completionDateFrom'] = validationLabels.endBeforeCompletionDate;
                }
            }

            if (L.isNumber(submittedDateFrom) && L.isNumber(submittedDateTo)) {
                if (submittedDateFrom > submittedDateTo) {
                    errors['submittedDateFrom'] = validationLabels.endBeforeSubmittedDate;
                }
            }

            return errors;
        }
    }, {
        ATTRS: {
            formDefinitionId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            startDateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            startDateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            completionDateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            completionDateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            submittedDateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            submittedDateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            status: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                //default status list defined here
                value: ['DRAFT', 'SUBMITTED', 'COMPLETE']
            },
            mostRecentOnly: {
                USPType: 'Boolean',
                setter: Y.usp.ResultsFilterModel.setBooleanValue,
                getter: Y.usp.ResultsFilterModel.getBooleanValue,
                value: true
            }
        }
    });

    Y.namespace('app.form').FormInstanceFilterView = Y.Base.create('formInstanceFilterView', Y.usp.app.ResultsFilter, [Y.app.attachment.EntityWithAttachmentFilterViewMixin], {
        template: Y.Handlebars.templates['formResultsFilter'],
        events: {
            '#startDateRangeShortcuts a': {
                click: '_handleStartDatePreselect'
            },
            '#submittedDateRangeShortcuts a': {
                click: '_handleSubmittedDatePreselect'
            },
            '#completionDateRangeShortcuts a': {
                click: '_handleCompletionDatePreselect'
            }
        },
        initializer: function() {
            this.startDateFromCalendar = new Y.usp.CalendarPopup();
            this.startDateToCalendar = new Y.usp.CalendarPopup();
            this.submittedDateFromCalendar = new Y.usp.CalendarPopup();
            this.submittedDateToCalendar = new Y.usp.CalendarPopup();
            this.completionDateFromCalendar = new Y.usp.CalendarPopup();
            this.completionDateToCalendar = new Y.usp.CalendarPopup();

            //set as bubble targets
            this.startDateFromCalendar.addTarget(this);
            this.startDateToCalendar.addTarget(this);
            this.submittedDateFromCalendar.addTarget(this);
            this.submittedDateToCalendar.addTarget(this);
            this.completionDateFromCalendar.addTarget(this);
            this.completionDateToCalendar.addTarget(this);
        },
        destructor: function() {
            //destroy the calendars
            this.startDateFromCalendar.destroy();
            delete this.startDateFromCalendar;

            this.startDateToCalendar.destroy();
            delete this.startDateToCalendar;

            this.submittedDateFromCalendar.destroy();
            delete this.submittedDateFromCalendar;

            this.submittedDateToCalendar.destroy();
            delete this.submittedDateToCalendar;

            this.completionDateFromCalendar.destroy();
            delete this.completionDateFromCalendar;

            this.completionDateToCalendar.destroy();
            delete this.completionDateToCalendar;

            if (this.filterTabView) {
                this.filterTabView.destroy();

                delete this.filterTabView;
            }
        },
        render: function() {
            var container = this.get('container');

            //call into superclass to render
            Y.app.form.FormInstanceFilterView.superclass.render.call(this);

            //set the input node for the calendar
            this.startDateFromCalendar.set('inputNode', container.one('input[name="startDateFrom"]'));
            this.startDateToCalendar.set('inputNode', container.one('input[name="startDateTo"]'));
            this.submittedDateFromCalendar.set('inputNode', container.one('input[name="submittedDateFrom"]'));
            this.submittedDateToCalendar.set('inputNode', container.one('input[name="submittedDateTo"]'));
            this.completionDateFromCalendar.set('inputNode', container.one('input[name="completionDateFrom"]'));
            this.completionDateToCalendar.set('inputNode', container.one('input[name="completionDateTo"]'));

            this.startDateFromCalendar.render();
            this.startDateToCalendar.render();
            this.submittedDateFromCalendar.render();
            this.submittedDateToCalendar.render();
            this.completionDateFromCalendar.render();
            this.completionDateToCalendar.render();

            this.filterTabView = new Y.TabView({
                srcNode: container.one('#formFilterTabsSrc'),
                render: container.one('#formFilterTabs')
            });

            //build a list of form types (includes only those types for which the subject has a form instance)
            this._getFormInstanceTypes().then(function(types) {
                this._updateList('select[name="formDefinitionId"]', types.toJSON());
            }.bind(this));

            return this;
        },
        _getFormInstanceTypes: function() {
            var subject = this.get('subject') || {},
                model = new Y.usp.form.FormDefinitionList({
                  //require minimum of ReadSummary access
                    requestedAccess: 'READ_SUMMARY',
                    url: L.sub(this.get('formInstanceTypesURL'), {
                        subjectId: subject.subjectId,
                        subjectType: (subject.subjectType || '').toUpperCase(),
                        sortBy: encodeURI('[{"name":"asc"}]')
                    })
                });

            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed to get the form instance types for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },
        _updateList: function(nodeId, values) {
            var container = this.get('container'),
                select = container.one(nodeId);

            if (select) {
                FUtil.setSelectOptions(select, values, null, null, false, true);

                //now we have out select box populated - we need to ensure that any current selection is made
                DB.setElementValue(select.getDOMNode(), select.get('name'));
            }
        },
        updateView: function() {
            var container = this.get('container'),
                model = this.get('model'),
                isNameAndStateFilterSet,
                isMostRecentOnlyFilterSet,
                isStartDateFilterSet,
                isSubmittedDateFilterSet,
                isCompletionDateFilterSet,
                nameAndStateFilterIndicator = container.one('.typeFilterSpan'),
                startDateFilterIndicator = container.one('.startDateFilterSpan'),
                submittedDateFilterIndicator = container.one('.submittedDateFilterSpan'),
                completionDateFilterIndicator = container.one('.completionDateFilterSpan'),
                nameAndStateFilterCount = 0;

            nameAndStateFilterCount += (model.get('status') || []).length;
            nameAndStateFilterCount += (model.get('formDefinitionId') || []).length;

            isNameAndStateFilterSet = nameAndStateFilterCount > 0;
            isMostRecentOnlyFilterSet = (model.get('mostRecentOnly') || []).length > 0;

            isStartDateFilterSet = !!(model.get('startDateFrom') || model.get('startDateTo'));
            isSubmittedDateFilterSet = !!(model.get('submittedDateFrom') || model.get('submittedDateTo'));
            isCompletionDateFilterSet = !!(model.get('completionDateFrom') || model.get('completionDateTo'));

            /* -- Set the filter icon on the tab view -- */
            if (nameAndStateFilterIndicator) {
                nameAndStateFilterIndicator.toggleView(isNameAndStateFilterSet || isMostRecentOnlyFilterSet);
            }

            if (startDateFilterIndicator) {
                startDateFilterIndicator.toggleView(isStartDateFilterSet);
            }
            if (submittedDateFilterIndicator) {
                submittedDateFilterIndicator.toggleView(isSubmittedDateFilterSet);
            }
            if (completionDateFilterIndicator) {
                completionDateFilterIndicator.toggleView(isCompletionDateFilterSet);
            }
        },

        /**
         * Handle change event on an entry date preselect input
         */
        _handleStartDatePreselect: function(e) {
            e.preventDefault();

            var t = e.currentTarget,
                today = new Date(),
                startDateFrom;

            // Filtering includes today, so a day less than required.
            if (t.hasClass('lastWeek')) {
                startDateFrom = Y.Date.addDays(today, -6); // last week
            } else if (t.hasClass('lastMonth')) {
                startDateFrom = Y.Date.addDays(today, -29); // last 30 days
            } else {
                startDateFrom = Y.Date.addDays(today, -364); // last year
            }

            this.get("model").setAttrs({
                startDateTo: null,
                startDateFrom: startDateFrom.getTime()
            });

            this.applyFilter();
        },
        _handleSubmittedDatePreselect: function(e) {
            e.preventDefault();

            var t = e.currentTarget,
                today = new Date(),
                submittedDateFrom;

            // Filtering includes today, so a day less than required.
            if (t.hasClass('lastWeek')) {
                submittedDateFrom = Y.Date.addDays(today, -6); // last week
            } else if (t.hasClass('lastMonth')) {
                submittedDateFrom = Y.Date.addDays(today, -29); // last 30 days
            } else {
                submittedDateFrom = Y.Date.addDays(today, -364); // last year
            }

            this.get("model").setAttrs({
                submittedDateTo: null,
                submittedDateFrom: submittedDateFrom.getTime()
            });

            this.applyFilter();
        },
        _handleCompletionDatePreselect: function(e) {
            e.preventDefault();

            var t = e.currentTarget,
                today = new Date(),
                completionDateFrom;

            // Filtering includes today, so a day less than required.
            if (t.hasClass('lastWeek')) {
                completionDateFrom = Y.Date.addDays(today, -6); // last week
            } else if (t.hasClass('lastMonth')) {
                completionDateFrom = Y.Date.addDays(today, -29); // last 30 days
            } else {
                completionDateFrom = Y.Date.addDays(today, -364); // last year
            }

            this.get("model").setAttrs({
                completionDateTo: null,
                completionDateFrom: completionDateFrom.getTime()
            });

            this.applyFilter();
        }
    });

    Y.namespace('app.form').FormInstanceFilter = Y.Base.create('formInstanceFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.form.FormInstanceFilterModel,
        filterType: Y.app.form.FormInstanceFilterView,
        filterContextTemplate: Y.Handlebars.templates["formActiveFilter"],
        quickFiltersTemplate: Y.Handlebars.templates["formQuickFilters"]
    });
}, '0.0.1', {
    requires: ['yui-base',
        'calendar-popup',
        'tabview',
        'app-filter',
        'results-filter',
        'promise',
        'handlebars-helpers',
        'handlebars-form-templates',
        'usp-form-FormDefinition',
        'datatype-date-math',
        'attachment-filter'
    ]
});