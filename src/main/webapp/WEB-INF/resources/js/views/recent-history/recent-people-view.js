

YUI.add('app-recent-people-view', function (Y) {
	
	
	Y.namespace('app').RecentPeopleView = Y.Base.create('recentPeopleView', Y.View, [], {
	    
		initializer: function (config) {    	
		    var model=config.model;
		    
    		this.recentPeopleTablePanel = new Y.app.RecentPeopleTablePanel(config);

        	this.recentPeopleTablePanel.addTarget(this);

        	//when the user model changes - we need to reload the recent results
        	model.after('change', function(){
        	    this.reload(true);
        	}, this.recentPeopleTablePanel);
        },
        
    	render:function(){
    		// Will use the container property when we create the HomePageDetailsView to attach this containers content
    		var contentNode=Y.one(Y.config.doc.createDocumentFragment());
    		
    		var container = this.get('container');
    		
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
    		contentNode.append(this.recentPeopleTablePanel.render().get('container'));
    		
    		//finally set the content into the container
    		container.setHTML(contentNode);
            
            // Hide the 'Showing records of ...
            if(container.one('#recentPeopleTablePanelResultsCount')) {
            	container.one('#recentPeopleTablePanelResultsCount').hide();
            }
            return this;
    	},
    	
    	destructor: function () { 
            this.recentPeopleTablePanel.destroy(); 
            delete this.recentPeopleTablePanel;
        }
    },{
    	ATTRS:{
    		model:{},
    		searchConfig:{
				value:{
					labels:{},
					noDataMessage:{},
					url:{},
				}
	  		}
    	}
    });
	
	

}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'app-recent-people-table-panel']
});