YUI.add('app-recent-records-dialog', function(Y) {
    Y.namespace('app').RecentRecordsDialog = Y.Base.create('recentRecordsDialog', Y.usp.MultiPanelPopUp, [], {
        initializer:function(){
            //plug in the MultiPanel errors handling
            this.plug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);           
        },        
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
        },
        views:{
            recentPeopleView: {
                headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i <i class="eclipse-historic fa-inverse fa-stack-1x"></i></span> {{this.labels.recentRecords}}</h3>',
                type: Y.app.RecentPeopleView,
                buttons:[]
            }
         }           
    },{
        CSS_PREFIX: 'yui3-panel',
        ATTRS:{
            width:{
                value:700
            },
            buttons:{
                value:['close']
            }
        }
    });    
}, '0.0.1', {
    requires: ['yui-base', 
               'multi-panel-popup', 
               'multi-panel-model-errors-plugin',
               'app-recent-people-view']
});