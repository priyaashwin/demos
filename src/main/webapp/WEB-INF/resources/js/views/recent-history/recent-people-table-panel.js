

YUI.add('app-recent-people-table-panel', function (Y) {
	
	var A=Y.Array, L=Y.Lang;
	
	Y.Do.after(function(){
		var schema=Y.Do.originalRetVal;
		schema.resultFields.push({key:'accessedPerson', locator:'accessedPerson'});
		schema.resultFields.push({key:'accessedPerson!gender', locator:'accessedPerson.gender'});		
	}, Y.usp.relationshipsrecording.PersonAccessWarningCount, 'getSchema', Y.usp.relationshipsrecording.PersonAccessWarningCount);
	
	
	
	/**
	 * @namespace Y.app.RecentPeopleTablePanel
	 * @description 
	 */
	
	
	Y.namespace('app').RecentPeopleTablePanel = Y.Base.create('recentPeopleTablePanel', Y.View, [Y.app.PersonSearchNavigation], {		
		/**
		 * @method initializer
		 */
		initializer: function(config) {
			var searchConfig=config.searchConfig,
			    labels = searchConfig.labels,
				codedEntries = this.get('codedEntries'),
				USPFormatters = Y.usp.ColumnFormatters,
				USPTemplates = Y.usp.ColumnTemplates;
			
			
			var id=this.get('model').get('id');
			
			var columns = [
				{ label:'Date', formatter:function(o){
					var accessDate = Y.USPDate.formatDateValue(o.data.accessDate);
					var previousAccessDate = this.data.toJSON()[o.rowIndex-1]; 
					if(previousAccessDate){
						var previousAccessDateString = Y.USPDate.formatDateValue(previousAccessDate.accessDate);
						if(accessDate === previousAccessDateString){
							return;
						}else{
							return accessDate;
						}
					}else{
						return accessDate;
					}
				}, width:'4%'},	
				{ label:labels.name, allowHTML:true,formatter:Y.app.ColumnFormatters.personName, width:'10%', cellTemplate:USPTemplates.clickable(labels.showPerson,'showPerson')},
				{ key: 'accessedPerson!gender',label:labels.gender,formatter: USPFormatters.codedEntry, codedEntries:codedEntries.gender, width:'1%'},
				{ key:'accessedPerson', label:labels.dob, formatter:Y.app.ColumnFormatters.lifeStateFormatter, allowHTML:true, width:'9%'}		
			               
			];
			
			this.results = new Y.usp.PaginatedResultsTable({
				sortBy:  [{'accessDate':'desc'}],
				columns: columns,			
				noDataMessage: searchConfig.noDataMessage,
				plugins: [
			        {
			            fn:Y.Plugin.usp.ResultsTableKeyboardNavPlugin
		            }],
				data: new Y.usp.relationshipsrecording.PaginatedPersonAccessWarningCountList({
					url: L.sub(searchConfig.url, {userId : id})
				}),						
				//prevent an initial load if we have not yet got an ID in the model
				initialLoad:!!id
			});
			
            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                tableId: this.name
            });
			
            // Add event targets
            this.results.addTarget(this);
            
			this._evtHandlers=[
			  this.results.delegate('click', this._handleActionClick, '.pure-table-data .showPerson', this),
			];
			
		},
		
		/**
		 * @method detructor
		 */
		destructor: function () {
			
			if (this._evtHandlers) {
            	A.each(this._evtHandlers, function (item) {
            		item.detach();
            	});

                this._evtHandlers = null;
            };

            if (this.results) {
                //destroy the results table
                this.results.destroy();

                delete this.results;
            }
            
            if (this._tablePanel) {
                this._tablePanel.destroy();

                delete this._tablePanel;
            }            
		},
		
		/**
		 * @method render
		 */
		render: function () {
		    var container = this.get('container'),
		    //render the portions of the page
            contentNode = Y.one(Y.config.doc.createDocumentFragment());
		    		
            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));

            //render results
            this._tablePanel.renderResults(this.results);
               
            //finally set the content into the container
            container.setHTML(contentNode);            
	
			return this;
		},
		
		/**
		 * @method _handleActionClick
		 * @description click on a row link will navigate to the equivalent page for the person click
		 * 	i.e if in relationships the context will be switch and remain in relationships.
		 */
		
		_handleActionClick:function(e){
			
			var t=e.currentTarget;
			
			e.preventDefault();
			
			if(t.hasClass('showPerson')){
				record=this.results.getRecord(t.get("id")),
				personId=record.get('accessedPerson.id');

                //functionality provided by PersonSearchNavigation mixin
                this.navigateToPerson(personId);    
			} 
			
		},		
		/**
		 * @method reload
		 * @description Reloads the views paginated table.
		 */
		reload:function(reset){
		    var searchConfig=this.get('searchConfig'),
		        model=this.get('model');
		    this.results.get('data').url=L.sub(searchConfig.url, {userId : model.get('id')});
			this.results.reload(reset);
		}
	},{ 
		ATTRS:{
		    searchConfig:{
			    value:{
					labels:{},
					title:{},
					noDataMessage:{},
					url:{},
					container:{},
				}
			},
			codedEntries:{
				value:{
					gender: Y.uspCategory.person.gender.category.codedEntries
				}
			}			
		}

	});



}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'accordion-panel',
               'handlebars-base',
               'handlebars-helpers',
               'event-custom',
               'usp-date',
               'escape',
               'paginated-results-table',
               'results-formatters',
               'results-templates',
               'results-table-keyboard-nav-plugin',
               'categories-person-component-Gender',
               'usp-relationshipsrecording-PersonAccessWarningCount',
               'caserecording-results-formatters',
               'person-search-navigation']
});
