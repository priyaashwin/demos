YUI.add('security-domain-results', function(Y) {
    'use-strict';
    var USPFormatters = Y.usp.ColumnFormatters;

    Y.namespace('app.admin.team').PaginatedFilteredSecurityDomainResultList = Y.Base.create("paginatedFilteredSecurityDomainResultList",  Y.usp.securityextension.PaginatedSecurityDomainList, [
        Y.usp.ResultsFilterURLMixin
    ], {
        whiteList: ["name"],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app.admin.team').SecurityDomainResults = Y.Base.create('securityDomainResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.app.admin.team.PaginatedFilteredSecurityDomainResultList({
                url: config.url,
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions,
                defaultSecurityDomainCode=config.defaultSecurityDomainCode||'';
            return ([{
                key: 'name',
                label: labels.name,
                sortable: true,
                width: '30%'
            }, {
                key: 'description',
                label: labels.description,
                width: '60%'
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                formatter: USPFormatters.actions,
                items: [{
                  clazz: 'editSecurityDomain',
                  title: labels.editDomainTitle,
                  label: labels.editDomain,
                  visible: permissions.canEditSecurityDomain,
                  enabled: function(){
                    return this.data.system===false;
                  }
                },{
                clazz: 'fillSecurityDomain',
                title: labels.fillDomainTitle,
                label: labels.fillDomain,
                visible: permissions.canFillSecurityDomain,
                enabled: function(){
                  return this.data.code!==defaultSecurityDomainCode;
                }
              },{
                clazz: 'removeSecurityDomain',
                title: labels.removeDomainTitle,
                label: labels.removeDomain,
                visible: permissions.canRemoveSecurityDomain,
                enabled: function(){
                  return this.data.system===false&&this.data.identifiers.length===0;
                }
              }]
            }]);
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    }];
                }
            }
        }
    });

    Y.namespace('app.admin.team').SecurityDomainFilteredResults = Y.Base.create('securityDomainFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.admin.team.SecurityDomainFilter,
        resultsType: Y.app.admin.team.SecurityDomainResults
    });

}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'results-formatters',
        'security-domain-filter',
        'app-filtered-results',
        'app-filter',
        'app-results',
        'results-table-search-state-plugin',
        'usp-securityextension-SecurityDomain'
    ]
});