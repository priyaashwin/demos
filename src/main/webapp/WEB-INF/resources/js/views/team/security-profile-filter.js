YUI.add('security-profile-filter', function(Y) {

    Y.namespace('app.admin.team').SecurityProfileFilterModel = Y.Base.create('securityProfileFilterModel', Y.usp.ResultsFilterModel, [], {
    },{
        ATTRS: {
          name:{
                USPType: 'String',
                value:''
            }
        }
    });

    Y.namespace('app.admin.team').SecurityProfileFilter = Y.Base.create('SecurityProfileFilter', Y.usp.app.Filter, [], {
      filterModelType: Y.app.admin.team.SecurityProfileFilterModel,
      filterTemplate: Y.Handlebars.templates['securityProfileResultsFilter'],
      filterContextTemplate: Y.Handlebars.templates["securityProfileActiveFilter"]
    });   
}, '0.0.1', {
    requires: ['yui-base',
               'app-filter',
               'results-filter',
               'handlebars-helpers',
               'handlebars-organisation-templates'
              ]
});