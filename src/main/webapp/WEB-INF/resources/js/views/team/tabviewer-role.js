YUI.add('team-tabviewer-role', function(Y) {
    'use-strict';
    var A = Y.Array;
    var RolePersonAutoCompleteView = Y.Base.create('rolePersonAutoCompleteView', Y.View, [], {
        initializer: function(config) {
            this.personAutoCompleteView = new Y.app.PersonAutoCompleteView({
                autocompleteURL: config.autocompleteURL,
                inputName: Y.guid(),
                formName: 'teamAddForm',
                rowTemplate: 'ROW_TEMPLATE_SIMPLE',
                preventSecuredSelection:false,
                labels: config.labels
            });
            this.personAutoCompleteView.addTarget(this);
        },
        destructor: function() {
            if (this.personAutoCompleteView) {
                this.personAutoCompleteView.removeTarget(this);
                this.personAutoCompleteView.destroy();
                delete this.personAutoCompleteView;
            }
        },
        clearInput:function(){
          this.personAutoCompleteView.clearInput();
        },
        render: function() {
            var container = this.get('container');
            container.append(this.personAutoCompleteView.render().get('container'));
            return this;
        }
    });
    /**
     * @class TabviewerRole
     * @description
     */
    Y.namespace('app.team').TabviewerRole = Y.Base.create('tabviewerRole', Y.View, [], {
        template: Y.Handlebars.templates.teamManageMembersRole,
        events: {
            'a[data-action="remove"]': {
                click: 'handleRemoveMember'
            }
        },
        initializer: function(config) {
            //handle selection from the autocomplete view
            this.after('*:select', function(e) {
                this.addMember(e.result.raw);
                
                this.personAutocompleteView.clearInput();
            }, this);
            this.after('roleMembersChange', this.renderRoleMembers, this);
            this.personAutocompleteView = new RolePersonAutoCompleteView({
                autocompleteURL: config.personAutocompleteURL,
                labels: config.labels
            });
            
            this.personAutocompleteView.addTarget(this);
            
            this._roleMembersId = Y.guid();
        },
        destructor: function() {
            if (this.personAutocompleteView) {
                this.personAutocompleteView.removeTarget(this);
                this.personAutocompleteView.destroy();
                delete this.personAutocompleteView;
            }
        },
        render: function() {
            var container = this.get('container'),
                contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(this.personAutocompleteView.render().get('container'));
            contentNode.append('<div id="' + this._roleMembersId + '"></div>');
            container.append(contentNode);
            this.renderRoleMembers();
            return this;
        },
        renderRoleMembers: function() {
            var container = this.get('container'),
                contentNode = container.one('#' + this._roleMembersId);
            if (contentNode) {
                contentNode.setHTML(this.template({
                    labels: this.get('labels'),
                    roleMembers: this.get('roleMembers')
                }));
            }
        },
        addMember: function(data) {
            var roleMembers = this.get('roleMembers') || [];
            //if it doesn't already contain it
            if (!A.find(roleMembers, function(member) {
                return member.id === data.id;
            })) {
                roleMembers.push(data);
            }
            //this will trigger an attribute change event
            this.set('roleMembers', roleMembers);
        },
        handleRemove: function(id) {
            var roleMembers = this.get('roleMembers') || [],
                personId=Number(id);
            
            //this will trigger an attribute change event
            this.set('roleMembers', A.filter(roleMembers, function(member){
              return member.id !== personId;
            }));
        },
        handleRemoveMember: function(e) {
            e.preventDefault();
            this.handleRemove(e.currentTarget.getData('id'));
        }
    }, {
        ATTRS: {
            /**
             * @attribute labels
             * @description labels used in handlebars
             */
            labels: {
                value: {}
            },
            /**
             * @attribute roleMembers
             * @description returns members with matching type
             */
            roleMembers: {
                value: []
            },
            personAutocompleteURL: {}
        }
    });

}, '0.0.1', {
    requires: ['yui-base', 'view', 'event-delegate', 'event-custom', 'person-autocomplete-view']
});