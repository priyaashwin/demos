YUI.add('team-results', function(Y) {
    'use-strict';
    var L = Y.Lang,
        A = Y.Array,
        USPTemplates = Y.usp.ColumnTemplates,
        USPFormatters = Y.usp.ColumnFormatters,
        USPColumnFormatters = Y.app.ColumnFormatters;

    Y.namespace('app.admin.team').PaginatedFilteredTeamWithAddressResultList = Y.Base.create("paginatedFilteredTeamWithAddressResultList", Y.usp.organisation.PaginatedTeamWithAddressAndSecurityConfigurationList, [
        Y.usp.ResultsFilterURLMixin
    ], {
        whiteList: ["nameOrOrgIdOrPrevName"],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app.admin.team').TeamResults = Y.Base.create('teamResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.app.admin.team.PaginatedFilteredTeamWithAddressResultList({
                url: config.url,
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions;
            return ([{
                key: 'name',
                label: labels.comboNameID,
                sortable: true,
                formatter: USPColumnFormatters.linkablePopOverTeamName,
                width: '30%',
                title: labels.viewTeamTitle,
                clazz: 'view'
            }, {
                key: 'description',
                label: labels.description,
                width: '35%'
            }, {
              key: 'securityConfigurationName',
              label: labels.securityRecordTypeConfiguration,
              width: '25%'
            }, {
              key:'mobile-summary',
              label: 'Summary',
              className: 'mobile-summary pure-table-desktop-hidden',
              width: '0%',
              allowHTML:true,
              formatter:USPColumnFormatters.summaryAddressFormat,
              cellTemplate:'<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'view',
                    title: labels.viewTeamTitle,
                    label: labels.viewTeam,
                    enabled: permissions.canView
                }, {
                  clazz: 'viewOrganisation',
                  title: labels.viewOrganisationTitle,
                  label: labels.viewOrganisation,
                  enabled: permissions.canViewOrganisation
                }, {
                    clazz: 'edit',
                    title: labels.editTeamTitle,
                    label: labels.editTeam,
                    visible: permissions.canUpdate
                }]
            }]);
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function() {
                    return [{
                      fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                      fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                      cfg: {
                        state: 'expanded',
                        summaryFormatter:USPColumnFormatters.summaryAddressFormat
                      }
                    },{
                      fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    }];
                }
            }
        }
    });
    
    Y.namespace('app.admin.team').TeamFilteredResults = Y.Base.create('teamFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.admin.team.TeamFilter,
        resultsType: Y.app.admin.team.TeamResults
    });
    
}, '0.0.1', {
    requires: ['yui-base', 
               'view', 
               'results-templates', 
               'results-formatters', 
               'team-filter',
               'caserecording-results-formatters',
               'app-filtered-results', 
               'app-filter', 
               'app-results', 
               'usp-organisation-TeamWithAddressAndSecurityConfiguration',
               'results-table-summary-row-plugin',
               'results-table-search-state-plugin']
});