YUI.add('team-controller-view', function(Y) {
  'use-strict';

  var L = Y.Lang;
  
  Y.namespace('app.admin.team').SecurityAdminTabsView = Y.Base.create('securityAdminTabsView', Y.app.tab.FilteredTabsView, [], {
    views: {
      teams: {
        type: Y.app.admin.team.TeamFilteredResults
      },
      profiles: {
        type: Y.app.admin.team.SecurityProfileFilteredResults
      },
      domains: {
        type: Y.app.admin.team.SecurityDomainFilteredResults
      },
      accessLevels: {
        type: Y.app.admin.security.profile.AccessLevels
      },
      domainContents: {
        type: Y.app.admin.security.domain.DomainContents
      }
    },
    tabs: {
      teams: {},
      profiles: {},
      domains: {}
    }
  });

  Y.namespace('app.admin.team').TeamControllerView = Y.Base.create('teamControllerView', Y.View, [], {
    initializer: function(config) {
      var tabsConfig = config.tabs || {};
      
      var accessLevelsConfig=this.get('accessLevelsConfig')||{};
      var domainContentsConfig=this.get('domainContentsConfig')||{};

      this.toolbar = new Y.usp.app.AppToolbar({
        permissions: config.permissions
      }).addTarget(this);

      this.securityAdminTabs = new Y.app.admin.team.SecurityAdminTabsView({
        container: '#footabs',
        root: config.root,
        tabs: tabsConfig,
        toolbarNode: this.toolbar.get('toolbarNode')
      }).addTarget(this);

      this.securityAdminTabs.route('/profile/:id/accessLevels', function (request) {
        //no active tab
        this._set('activeTab',null);
        this.showView('accessLevels',  Y.merge({}, accessLevelsConfig, {
          permissions:{
            canEdit:config.permissions.canEditAccessLevels,
            canSave:config.permissions.canEditAccessLevels
          },
          model:new Y.usp.securityextension.SecurityRecordTypeConfigurationFullDetails({
            url:accessLevelsConfig.url,
            id:request.params.id
          })
        }));
      });
      
      this.securityAdminTabs.route('/domain/:id/contents', function (request) {
        //no active tab
        this._set('activeTab',null);
        this.showView('domainContents', Y.merge({}, 
          domainContentsConfig,
          {
            permissions:{
              canSave:config.permissions.canFillSecurityDomain
            },
            model:new Y.usp.securityextension.SecurityDomain({
              url:domainContentsConfig.url,
              id:request.params.id
            })
          }
        ));
      });
      
      this.teamDialog = new Y.app.admin.team.TeamDialog(config.teamDialogConfig).addTarget(this).render();
      this.teamSecurityDialog = new Y.app.admin.team.TeamSecurityDialog(config.teamSecurityDialogConfig).addTarget(this).render();

      this._teamEvtHandlers = [
        this.on('*:addTeam', this.showAddTeamDialog, this),
        this.on('*:view', this.showViewTeamDialog, this),
        this.on('*:viewOrganisation', this.showOrganisationView, this),
        this.on('*:edit', this.showEditTeamDialog, this),
        this.on('*:saved', this.refreshView, this),
        this.on('*:addSecurityDomain', this.showAddSecurityDomainDialog, this),
        this.on('*:editSecurityDomain', this.showEditSecurityDomainDialog, this),
        this.on('*:addSecurityProfile', this.showAddSecurityProfileDialog, this),
        this.on('*:editSecurityProfile', this.showEditSecurityProfileDialog, this),
        this.on('*:duplicateSecurityProfile', this.showDuplicateSecurityProfileDialog, this),
        this.on('*:viewAccessLevels', this.showViewAccessLevels, this),
        this.on('*:fillSecurityDomain', this.showSecurityDomainContents, this),
        //cancel button within the security domain contents view
        this.on('domainContents:cancel', this.showDomains, this),
        this.on('*:removeSecurityProfile', this.showRemoveSecurityProfileDialog, this),
        this.on('*:removeSecurityDomain', this.showRemoveSecurityDomainDialog, this),
        this.after('securityAdminTabsView:activeViewChange', this._setToolbar, this),
        //as the results count must live outside the tab view - we must ensure it is cleared on a tab change
        this.before('securityAdminTabsView:activeViewChange', this._clearResultsCount, this),
      ];
      
      //setup binding on refresh function
      this.refreshView=this.refreshView.bind(this);
      //and add event listener
      document.addEventListener('team:saved', this.refreshView);
    },
    destructor: function() {
      this._teamEvtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._teamEvtHandlers;

      this.teamDialog.removeTarget(this);
      this.teamDialog.destroy();
      delete this.teamDialog;

      this.teamSecurityDialog.removeTarget(this);
      this.teamSecurityDialog.destroy();
      delete this.teamSecurityDialog;

      this.securityAdminTabs.removeTarget(this);
      this.securityAdminTabs.destroy();
      delete this.securityAdminTabs;
      
      this.toolbar.removeTarget(this);
      this.toolbar.destroy();
      delete this.toolbar;
      
      this.tabsContainer.destroy(true);
      delete this.tabsContainer;
      
      document.removeEventListener('team:saved', this.refreshView);
    },
    render: function() {
      var contentNode = Y.one(Y.config.doc.createDocumentFragment());

      //add the tabs container
      contentNode.append(this.securityAdminTabs.render().get('container'));

      this.get('container').setHTML(contentNode);

      //ensure we load the team tab
      if (this.securityAdminTabs.hasRoute(this.securityAdminTabs.getPath())) {
        this.securityAdminTabs.dispatch();
      } else {
        this.securityAdminTabs.activateTab('teams');
      }

      return this;
    },
    refreshView: function() {
      //get the active tab and re-load to update
      var view = this.securityAdminTabs.get('activeView');
      //all views have a results list - if this were to change a guard would be needed here
      view.get('results').reload();
    },
    showOrganisationView:function(e){
      var record = e.record,
      permissions = e.permissions,
      organisationViewURL=this.get('organisationViewURL');
      
      if (permissions.canView === true) {
        window.location=L.sub(organisationViewURL, {
          id:record.get('id')
        });
      }
    },
    showAddTeamDialog: function() {
      var permissions = this.get('permissions');
      if (permissions.canAddTeam) {
       //fire event
        Y.fire('teamDialog:add');
      }
    },
    showDomains:function(){
      var root=this.get('root');
      this.securityAdminTabs.navigate(root+'/domains');
    },
    showViewTeamDialog: function(e) {
      var record = e.record,
        permissions = e.permissions;

      if (permissions.canView === true) {
        this.teamDialog.showView('viewTeam', {
          model: record
        }, function(view) {
          if (permissions.canUpdate) {
            //Get the save button by name out of the footer and enable it.
            this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
          }
        });
      }
    },
    showEditTeamDialog: function(e) {
      var record = e.record,
        permissions = e.permissions || this.get('permissions');

      if (permissions.canUpdate === true) {
        //show the loading view while we start processing
        this.teamDialog.showView('teamProcessing');
        Y.Promise.all([this.teamDialog.getTeamRelationshipTypesPromise()]).then(function(data) {
          this._showEditDialog(data[0].results, record);
        }.bind(this)).
        then(null, function(err) {
          var view = this.teamDialog.get('activeView');
          if (view) {
            view.fire('error', {
              error: err
            });
          }
        }.bind(this));
      }
    },
    _showEditDialog: function(professionalTeamRelationshipTypes, record) {
      var config = this.get('teamDialogConfig').views.editTeam.config;

      this.teamDialog.showView('editTeam', {
        model: new Y.app.admin.team.UpdateTeamAndProfessionalRelationships({
          url: config.url,
          id: record.get('id'),
          labels:config.validationLabels
        }),
        professionalTeamRelationshipTypes: professionalTeamRelationshipTypes
      }, {
        modelLoad: true
      }, function(view) {
        //Get the save button by name out of the footer and enable it.
        this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
      });
    },
    showAddSecurityDomainDialog: function() {
      var config = this.get('teamSecurityDialogConfig').views.addSecurityDomain.config;
      var permissions = this.get('permissions');
      if (permissions.canAddSecurityDomain) {
        this.teamSecurityDialog.showView('addSecurityDomain', {
          model: new Y.app.admin.team.NewSecurityDomainForm({
            code: uuid.v4(),
            url: config.url
          })
        }, function(view) {
          //Get the save button by name out of the footer and enable it.
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showEditSecurityDomainDialog: function(e) {
      var permissions = e.permissions,
        record = e.record;

      var config = this.get('teamSecurityDialogConfig').views.updateSecurityDomain.config;

      if (permissions.canEditSecurityDomain) {
        this.teamSecurityDialog.showView('updateSecurityDomain', {
          model: new Y.app.admin.team.UpdateSecurityDomainForm({
            id: record.get('id'),
            url: config.url
          })
        }, {
          modelLoad: true
        }, function(view) {
          //Get the save button by name out of the footer and enable it.
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showAddSecurityProfileDialog: function() {
      var config = this.get('teamSecurityDialogConfig').views.addSecurityProfile.config;
      var permissions = this.get('permissions');
      if (permissions.canAddSecurityProfile) {
        this.teamSecurityDialog.showView('addSecurityProfile', {
          model: new Y.app.admin.team.NewSecurityRecordTypeConfigurationForm({
            url: config.url
          })
        }, function(view) {
          //Get the save button by name out of the footer and enable it.
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showEditSecurityProfileDialog: function(e) {
      var permissions = e.permissions,
        record = e.record;

      var config = this.get('teamSecurityDialogConfig').views.updateSecurityProfile.config;

      if (permissions.canEditSecurityProfile) {
        this.teamSecurityDialog.showView('updateSecurityProfile', {
          model: new Y.app.admin.team.UpdateSecurityRecordTypeConfigurationForm({
            id: record.get('id'),
            url: config.url
          })
        }, {
          modelLoad: true
        }, function(view) {
          //Get the save button by name out of the footer and enable it.
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showDuplicateSecurityProfileDialog:function(e){
      var permissions = e.permissions,
      record = e.record;
      
      var config = this.get('teamSecurityDialogConfig').views.duplicateSecurityProfile.config;

      if (permissions.canDuplicateSecurityProfile) {
        var data=record.toJSON();
        
        //shuffle the id into the copyFromId attribute
        data.copyFromId=data.id;
        delete data.id;
        
        this.teamSecurityDialog.showView('duplicateSecurityProfile', {
          model: new Y.app.admin.team.DuplicateSecurityRecordTypeConfigurationForm(
                  Y.merge(data, {url:config.url}))
        }, function(view) {
          //Get the save button by name out of the footer and enable it.
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }

    },
    showRemoveSecurityProfileDialog:function(e){
      var permissions = e.permissions,
      record = e.record;

      var config = this.get('teamSecurityDialogConfig').views.removeSecurityProfile.config;

      if (permissions.canRemoveSecurityProfile) {
        this.teamSecurityDialog.showView('removeSecurityProfile', {
          model: new Y.app.admin.team.RemoveSecurityRecordTypeConfigurationForm(
                  Y.merge(record.toJSON(), {url:config.url}))
        }, function(view) {
          //Get the save button by name out of the footer and enable it.
          this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showRemoveSecurityDomainDialog:function(e){
      var permissions = e.permissions,
      record = e.record;

      var config = this.get('teamSecurityDialogConfig').views.removeSecurityDomain.config;

      if (permissions.canRemoveSecurityDomain) {
        this.teamSecurityDialog.showView('removeSecurityDomain', {
          model: new Y.app.admin.team.RemoveSecurityDomainForm(
                  Y.merge(record.toJSON(), {url:config.url}))
        }, function(view) {
          //Get the save button by name out of the footer and enable it.
          this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },

    showViewAccessLevels:function(e){
      var record=e.record,
          root=this.get('root');
      var permissions = this.get('permissions');
      if (permissions.canViewAccessLevels===true){
        this.securityAdminTabs.navigate(root+'profile/'+record.get('id')+'/accessLevels');
      }
    },
    showSecurityDomainContents:function(e){
      var record=e.record,
        root=this.get('root');
      var permissions = this.get('permissions');
      if (permissions.canFillSecurityDomain===true){
        this.securityAdminTabs.navigate(root+'domain/'+record.get('id')+'/contents');
      }
    },
    _clearResultsCount: function(e) {
      var activeView = this.securityAdminTabs.get('activeView');
      if (activeView) {
        var searchConfig = activeView.get('searchConfig') || {};
        if (searchConfig.resultsCountNode) {
          var n = Y.one(searchConfig.resultsCountNode);
          if (n) {
            n.setHTML('<span>&nbsp;</span>');
          }
        }
      }
    },
    _setAccessLevelContextHeader:function(){
      var labels=this.get('labels');
      this.setContextHeader(labels.accessLevelTitle);
    },
    _setDomainContentsContextHeader:function(){
      var labels=this.get('labels');
      this.setContextHeader(labels.domainContentsTitle);
    },
    setContextHeader:function(label){
      var tabs=this.securityAdminTabs,
      contextNode=this.get('contextTitleNode');
      
      var model=tabs.get('activeView').get('model');
      
      contextNode.set('text', L.sub(label, {
        name:model.get('name')
      }));

    },
    _setToolbar: function(e) {
      var newView = e.newVal,
        prevView=e.prevVal||{},
        toolbar = this.toolbar,
        tabs=this.securityAdminTabs,
        contextNode=this.get('contextTitleNode'),
        labels=this.get('labels');
        

      var permissions = this.get('permissions');

      var isSharedFilterContext=this.get('filterContext.shared');
      
      var toolbarConfig = newView.get('toolbar');
        
      switch(prevView.name){
        case 'accessLevels':
          /* falls through */
        case 'domainContents':{
          if(prevView.name==='accessLevels'){
            //remove load handler
            prevView.get('model').detach('*:load', this._setAccessLevelContextHeader, this);
          }else if(prevView.name==='domainContents'){
            //remove load handler
            prevView.get('model').detach('*:load', this._setDomainContentsContextHeader, this);
          }
          //show the tabs and the toolbar buttons
          tabs.show();
          toolbar.showButtons(['filter','addTeam','addSecurityDomain', 'addSecurityProfile']);
          
          //show filter context
          if(isSharedFilterContext){
              Y.one(this.get('filterContext.container')).show(true);
          }
          
          break;
        }
      }
      
      switch (newView.name) {
        case 'teamFilteredResults':
          contextNode.set('text', labels.teamAdminTitle);
          toolbar.updateButton('.add-button', {
              action:'addTeam', 
              title:toolbarConfig.addTitle, 
              ariaLabel:toolbarConfig.addAriaLabel
          });
          if (permissions.canAddTeam) {
            toolbar.enableButton('addTeam');
          } else {
            toolbar.disableButton('addTeam');
          }
          break;
        case 'securityDomainFilteredResults':
          contextNode.set('text', labels.securityDomainTitle);
          toolbar.updateButton('.add-button', {
              action:'addSecurityDomain', 
              title: toolbarConfig.addTitle, 
              ariaLabel: toolbarConfig.addAriaLabel
          });
          
          if (permissions.canAddSecurityDomain) {
            toolbar.enableButton('addSecurityDomain');
          } else {
            toolbar.disableButton('addSecurityDomain');
          }
          break;
        case 'securityProfileFilteredResults':
          contextNode.set('text', labels.securityProfileTitle);
          toolbar.updateButton('.add-button', {
              action:'addSecurityProfile', 
              title: toolbarConfig.addTitle, 
              ariaLabels:toolbarConfig.addAriaLabel
          });
          
          if (permissions.canAddSecurityProfile) {
            toolbar.enableButton('addSecurityProfile');
          } else {
            toolbar.disableButton('addSecurityProfile');
          }
          break;
        case 'accessLevels':
          /* falls through */
        case 'domainContents':{
          if(newView.name==='accessLevels'){
            contextNode.set('text', L.sub(labels.accessLevelTitle, {
              name:'loading...'
            }));
            
            //set the context header once the model has loaded
            newView.get('model').after('*:load', this._setAccessLevelContextHeader, this);
          }else if(newView.name==='domainContents'){
            contextNode.set('text', L.sub(labels.domainContentsTitle, {
              name:'loading...'
            }));
            //set the context header once the model has loaded
            newView.get('model').after('*:load', this._setDomainContentsContextHeader, this);
          }
          
          //disable all buttons
          toolbar.disableButtons();
          toolbar.hideButtons(['filter','addTeam','addSecurityDomain', 'addSecurityProfile']);
          
          //hide the tabs
          tabs.hide();
          
          //hide filter context
          if(isSharedFilterContext){
              Y.one(this.get('filterContext.container')).hide(true);
          }

          break;
        }
      }
    }
  }, {
    ATTRS: {
      teamDialogConfig: {},
      teamSecurityDialogConfig: {},
      accessLevelsConfig:{},
      domainContentsConfig:{},
      contextTitleNode:{
        getter: Y.one,
        writeOnce: true
      },
      labels:{
        value:{}
      },
      organisationViewURL:{}
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-toolbar',
    'view',
    'team-results',
    'security-profile-results',
    'security-domain-results',
    'team-dialog',
    'promise',
    'filtered-tabs-view',
    'team-security-dialog',
    'security-profile-access-levels',
    'security-domain-contents',
    'usp-securityextension-SecurityDomain',
    'usp-securityextension-SecurityRecordTypeConfigurationFullDetails'
  ]
});