YUI.add('team-filter', function(Y) {

    Y.namespace('app.admin.team').TeamFilterModel = Y.Base.create('teamFilterModel', Y.usp.ResultsFilterModel, [], {
    },{
        ATTRS: {
          nameOrOrgIdOrPrevName:{
                USPType: 'String',
                value:''
            }
        }
    });

    Y.namespace('app.admin.team').TeamFilter = Y.Base.create('teamFilter', Y.usp.app.Filter, [], {
      filterModelType: Y.app.admin.team.TeamFilterModel,
      filterTemplate: Y.Handlebars.templates['teamResultsFilter'],
      filterContextTemplate: Y.Handlebars.templates["teamActiveFilter"]
    });   
}, '0.0.1', {
    requires: ['yui-base',
               'app-filter',
               'results-filter',
               'handlebars-helpers',
               'handlebars-organisation-templates'
              ]
});