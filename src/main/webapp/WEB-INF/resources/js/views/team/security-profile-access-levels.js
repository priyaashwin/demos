YUI.add('security-profile-access-levels', function(Y) {
  'use-strict';

  var O = Y.Object;

  var ACCESS_LEVEL_VALUES = O.values(Y.app.admin.security.profile.AccessLevelValues);

  Y.namespace('app.admin.security.profile').AccessLevels = Y.Base.create('accessLevels', Y.app.admin.security.base.ReactAppView, [], {
    reactFactoryType: uspSecurityAdmin.AccessLevel,
    initializer: function() {
      this.__accessLevelEventBindings = [
        this.after('isEditChange', this.handleAfterEditChange, this),
        this.on('*:edit', this.handleEdit, this),
        this.on('*:cancel', this.handleCancel, this)
      ];
    },
    handleAfterEditChange: function() {
      this.setToolbarButtons();
      this.renderComponent();
    },
    handleEdit: function() {
      var permissions = this.get('permissions'),
        model = this.get('model');

      if (permissions.canEdit === true && model.get('system') === false) {
        //flip to edit mode
        this.set('isEdit', true);
      }
    },
    handleCancel: function() {
      this.set('isEdit', false);
    },
    renderComponent: function() {
      var model = this.get('model');

      var modelData = model.toJSON();

      var component;

      ReactDOM.render(this.componentFactory({
        securityAccessUrl: this.get('securityAccessUrl'),
        accessLevels: ACCESS_LEVEL_VALUES,
        securityRecordTypeConfiguration: modelData,
        isEdit: this.get('isEdit')
      }), this.reactAppContainer.getDOMNode(), function() {
        component = this;
      });
      this.accessLevelComponent = component;
    },
    handleSave: function(e) {
      //from the accessLevel component - get the set of overrides
      var accessLevels = this.accessLevelComponent.getUpdateRecordAccessLevels();

      var updateModel = new Y.usp.securityextension.UpdateSecurityRecordTypeConfigurationAccessLevels({
        url: this.get('updateAccessLevelsUrl'),
        id: this.get('model').get('id'),
        objectVersion: this.get('model').get('objectVersion'),
        defaultAccessLevel: this.accessLevelComponent.getDefaultAccessLevel(),
        accessLevels: accessLevels.map(function(accessLevel) {
          return new Y.usp.securityextension.UpdateRecordAccessLevel(accessLevel);
        })
      });


      updateModel.save({}, Y.bind(function(err, response) {
        if (err === null) {
          this.get('model').load(function() {
            this.set('isEdit', false, {
              noRender: true
            });
            this.set('busy', false);
          }.bind(this));
        } else {
          this.fire('error', err);
          this.set('busy', false);
        }
      }, this));
    },
    getButtons: function(permissions) {
      var buttons = [],
        labels = this.get('labels'),
        model = this.get('model'),
        //defect - system is either null or false
        isSystem = model.get('system') !== false;

      var isEdit = this.get('isEdit');

      if (!isEdit && permissions.canEdit) {
        buttons.push({
          className: 'fa fa-pencil',
          action: 'edit',
          disabled: isSystem,
          first: true,
          last: true,
          label: labels.edit,
          title: labels.editTitle,
          ariaLabel: labels.editAriaLabel
        });
      } else if (isEdit && permissions.canEdit) {
        buttons.push({
          className: 'fa fa-check',
          action: 'save',
          first: true,
          last: false,
          disabled: isSystem,
          label: labels.save,
          title: labels.saveTitle,
          ariaLabel: labels.saveAriaLabel
        }, {
          className: 'fa fa-times',
          action: 'cancel',
          first: false,
          last: true,
          label: labels.cancel,
          title: labels.cancelTitle,
          ariaLabel: labels.cancelAriaLabel
        });
      }

      return buttons;
    },
    destructor: function() {
      //unbind event handles
      this.__accessLevelEventBindings.forEach(function(handler) {
        handler.detach();
      });
      delete this.__accessLevelEventBindings;


      delete this.accessLevelsContainer;
    }

  }, {
    ATTRS: {
      isEdit: {
        value: false
      },
      permissions: {
        canEdit: false
      },
      securityAccessUrl: {
        value: ''
      },
      updateAccessLevelsUrl: {
        value: ''
      }
    }
  });

}, '0.0.1', {
  requires: ['yui-base',
    'react-security-app-view',
    'usp-securityextension-SecurityRecordTypeConfiguration',
    'usp-securityextension-UpdateSecurityRecordTypeConfigurationAccessLevels',
    'usp-securityextension-UpdateRecordAccessLevel',
    'access-level-values'
  ]
});