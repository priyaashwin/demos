YUI.add('access-level-values', function(Y) {
  //map of Enum values to something we can display in the UI
  Y.namespace('app.admin.security.profile').AccessLevelValues={
    'NONE':{
      key:'NONE',
      value:'X',
      label:'No access',
      colour:'#cacfd5',
      fontColour:'#fff'
    },
    'READ_SUMMARY':{
      key:'READ_SUMMARY',
      value:'Rs',
      label:'Read summary',
      colour:'#78caf1',
      fontColour:'#fff'
    },
    'READ_DETAIL':{
      key:'READ_DETAIL',
      value:'Rd',
      label:'Read detail',
      colour:'#5ed4af',
      fontColour:'#fff'
    },
    'WRITE':{
      key:'WRITE',
      value:'W',
      label:'Write',
      colour:'#fba26a',
      fontColour:'#fff'
    },
    'ADMIN':{
      key:'ADMIN',
      value:'A',
      label:'Admin',
      colour:'#ef7171',
      fontColour:'#fff'
    }
  };
}, '0.0.1', {
  requires: ['yui-base']
});
