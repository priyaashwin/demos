YUI.add('team-members-view', function(Y) {
    'use-strict';

    var L = Y.Lang;

    Y.Do.after(function() {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({
            key: 'personVO',
            locator: 'personVO'
        });
        schema.resultFields.push({
            key: 'personVO!name',
            locator: 'personVO.name'
        });
        schema.resultFields.push({
            key: 'personVO!personIdentifier',
            locator: 'personVO.personIdentifier'
        });
        schema.resultFields.push({
            key: 'personOrganisationRelationshipTypeVO!roleAName',
            locator: 'personOrganisationRelationshipTypeVO.roleAName'
        });
    }, Y.usp.relationship.PersonOrganisationRelationship, 'getSchema', Y.usp.relationship.PersonOrganisationRelationship);

    Y.namespace('app.admin.team').TeamMembersResults = Y.Base.create('resultsTeamMembers', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.usp.relationship.PaginatedPersonOrganisationRelationshipList({
                url: L.sub(config.url, {
                    id: this.get('teamId')
                })
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions;
            return ([{
                key: 'personVO!name',
                label: labels.comboNameID,
                formatter: function(o) {
                    var val = [];
                    val.push(o.data['personVO!name']);
                    if (o.data['personVO!personIdentifier']) {
                        val.push('(' + o.data['personVO!personIdentifier'] + ')');
                    }

                    return val.join(' ');
                },
            }, {
                key: 'personOrganisationRelationshipTypeVO!roleAName',
                label: labels.role
            }]);
        }
    }, {
        ATTRS: {
            teamId: {}
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-results',
        'usp-relationship-PersonOrganisationRelationship'
    ]
});