YUI.add('security-domain-filter', function(Y) {

    Y.namespace('app.admin.team').SecurityDomainFilterModel = Y.Base.create('securityDomainFilterModel', Y.usp.ResultsFilterModel, [], {
    },{
        ATTRS: {
          name:{
                USPType: 'String',
                value:''
            }
        }
    });

    Y.namespace('app.admin.team').SecurityDomainFilter = Y.Base.create('SecurityDomainFilter', Y.usp.app.Filter, [], {
      filterModelType: Y.app.admin.team.SecurityDomainFilterModel,
      filterTemplate: Y.Handlebars.templates['securityDomainResultsFilter'],
      filterContextTemplate: Y.Handlebars.templates["securityDomainActiveFilter"]
    });   
}, '0.0.1', {
    requires: ['yui-base',
               'app-filter',
               'results-filter',
               'handlebars-helpers',
               'handlebars-organisation-templates'
              ]
});