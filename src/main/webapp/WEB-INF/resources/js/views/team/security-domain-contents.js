YUI.add('security-domain-contents', function(Y) {
  'use-strict';

  var L = Y.Lang,
      Micro = Y.Template.Micro;
  
  Y.namespace('app.admin.security.domain').DomainContents = Y.Base.create('domainContents', Y.app.admin.security.base.ReactAppView, [], {
    template:Micro.compile('<div class="pure-alert pure-alert-block pure-alert-info"><h4><%=this.labels.prompt%></h4></div>'),
    reactFactoryType: uspSecurityAdmin.SecurityDomainContents,
    initializer: function() {
      this.__domainContentsEventBindings = [
        this.on('appToolbar:cancel', this.handleCancel, this)
      ];
    },
    handleCancel: function() {
      this.fire('cancel');
    },
    handleSave: function(e) {
      var successMessage=this.get('successMessage'),
          model=this.get('model');
      
      //from the domain contents component - get the new identifiers
      var identifiers = this.securityDomainComponent.getUpdateSecurityDomainIdentifiers();

      
      var updateModel = new Y.usp.securityextension.UpdateSecurityDomainWithAssignments({
        url: this.get('updateDomainAssignmentsUrl'),
        id:model.get('id'),
        name:model.get('name'),
        objectVersion: model.get('objectVersion'),
        identifiers:identifiers.map(function(i){
          return ({
            type:i.type,
            identifier:i.identifier,
            _type:"StringIdentifier"
          });
        })
        
      });
      
      updateModel.save({}, Y.bind(function(err, response) {
        if (err === null) {
          this.fireMessage(successMessage);
          //return to previous screen
          this.handleCancel();
        } else {
          this.fire('error', err);
        }

        this.set('busy', false);
      }, this));
    },
    renderComponent: function() {
      if (!this.loaded) {
        this.loadDefaultSecurityDomain().then(function() {
          this._renderComponent();
          this.loaded = true;
        }.bind(this));
      } else {
        this._renderComponent();
      }
    },
    loadDefaultSecurityDomain: function() {
      var model = new Y.usp.securityextension.SecurityDomain({
        url: L.sub(this.get('defaultSecurityDomainUrl'), {
          securityDomainCode: this.get('defaultSecurityDomainCode')
        })
      });

      this.set('defaultSecurityDomainModel', model);

      return new Y.Promise(function(resolve, reject) {
        var data = model.load(function(err) {
          if (err !== null) {
            //failed for some reason so reject the promise
            reject(err);
          } else {
            //success, so resolve the promise
            resolve(data);
          }
        });
      });
    },
    _renderComponent: function() {
      var rightSecurityDomainModel = this.get('model'),
        leftSecurityDomainModel = this.get('defaultSecurityDomainModel');

      var rightSecurityDomain = rightSecurityDomainModel.toJSON();
      var leftSecurityDomain = leftSecurityDomainModel.toJSON();

      var component;

      ReactDOM.render(this.componentFactory({
        leftSecurityDomain: leftSecurityDomain,
        rightSecurityDomain: rightSecurityDomain
      }), this.reactAppContainer.getDOMNode(), function() {
        component = this;
      });

      this.securityDomainComponent = component;
    },
    getButtons: function(permissions) {
      var buttons = [],
        labels = this.get('labels'),
        model = this.get('model'),
        //defect - system is either null or false
        isSystem = model.get('system') !== false;

      if (!isSystem) {
        buttons.push({
          className: 'fa fa-check',
          action: 'save',
          first: true,
          last: false,
          disabled: isSystem,
          label: labels.save,
          title: labels.saveTitle,
          ariaLabel: labels.saveAriaLabel
        }, {
          className: 'fa fa-times',
          action: 'cancel',
          first: false,
          last: true,
          label: labels.cancel,
          title: labels.cancelTitle,
          ariaLabel: labels.cancelAriaLabel
        });
      }

      return buttons;
    },
    destructor: function() {
      //unbind event handles
      this.__domainContentsEventBindings.forEach(function(handler) {
        handler.detach();
      });
      delete this.__domainContentsEventBindings;

      delete this.securityDomainComponent;
    }
  }, {
    ATTRS: {
      defaultSecurityDomainUrl: {
        value: ''
      },
      defaultSecurityDomainCode: {
        value: ''
      },
      updateDomainAssignmentsUrl: {
        value: ''
      }
    }
  });

}, '0.0.1', {
  requires: ['yui-base',
    'react-security-app-view',
    'usp-securityextension-SecurityDomain',
    'usp-securityextension-UpdateSecurityDomainWithAssignments',
    'template-micro'
  ]
});