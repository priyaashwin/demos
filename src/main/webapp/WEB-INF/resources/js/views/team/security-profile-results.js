YUI.add('security-profile-results', function(Y) {
    'use-strict';
    var O=Y.Object;
    var USPFormatters = Y.usp.ColumnFormatters;
    var ACCESS_LEVEL_VALUES = O.values(Y.app.admin.security.profile.AccessLevelValues);
    
    var defaultAccessLevelFormatter=function(o){
      //NOTE - Babel polyfill is available which gives us findIndex
      var selectedIndex=ACCESS_LEVEL_VALUES.findIndex(function(elem){return elem.key===o.value});
      var accessLevelString='';
      var values=ACCESS_LEVEL_VALUES.map(function(info,i){
        var selected=false, style='';
        if(i<=selectedIndex && (selectedIndex===0||i>0)){
          selected=true;
          style='background-color:'+info.colour+';color:'+info.fontColour+';';
        }
        return ('<span style="'+style+'">'+info.value+'</span>');
      });
      
      if(selectedIndex>-1){
        var selectedAccessLevel=ACCESS_LEVEL_VALUES[selectedIndex];
        accessLevelString=selectedAccessLevel.label;
      }
      return ('<div class="view-record-access-level pop-up-data" data-align="top" data-content="'+accessLevelString+'">'+values.join('')+'</div>');
    };
    
    Y.namespace('app.admin.team').PaginatedFilteredSecurityProfileResultList = Y.Base.create("paginatedFilteredSecurityProfileResultList", Y.usp.securityextension.PaginatedSecurityRecordTypeConfigurationList, [
        Y.usp.ResultsFilterURLMixin
    ], {
        whiteList: ["name"],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app.admin.team').SecurityProfileResults = Y.Base.create('securityProfileResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.app.admin.team.PaginatedFilteredSecurityProfileResultList({
                url: config.url,
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions;
            return ([{
                key: 'name',
                label: labels.name,
                sortable: true,
                width: '30%'
            }, {
                key: 'description',
                label: labels.description,
                width: '40%'
            }, {
              key:'defaultAccessLevel',
              label: labels.defaultAccessLevel,
              width: '20%',
              formatter:defaultAccessLevelFormatter,
              allowHTML:true
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                formatter: USPFormatters.actions,
                items: [{
                  clazz: 'editSecurityProfile',
                  title: labels.editProfileTitle,
                  label: labels.editProfile,
                  visible: permissions.canEditSecurityProfile,
                  enabled:function(){
                    return this.data.system===false;
                  }
                },{
                  clazz: 'duplicateSecurityProfile',
                  title: labels.duplicateProfileTitle,
                  label: labels.duplicateProfile,
                  visible: permissions.canDuplicateSecurityProfile
                },{
                  clazz: 'viewAccessLevels',
                  title: labels.viewAccessLevelsTitle,
                  label: labels.viewAccessLevels,
                  visible: permissions.canViewAccessLevels
                },{
                  clazz: 'removeSecurityProfile',
                  title: labels.removeProfileTitle,
                  label: labels.removeProfile,
                  visible: permissions.canRemoveSecurityProfile,
                  enabled:function(){
                    return this.data.system===false&&this.data.securityBodiesAssigned===0;
                  }
                }]
            }]);
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    }];
                }
            }
        }
    });

    Y.namespace('app.admin.team').SecurityProfileFilteredResults = Y.Base.create('securityProfileFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.admin.team.SecurityProfileFilter,
        resultsType: Y.app.admin.team.SecurityProfileResults
    });

}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'results-formatters',
        'security-profile-filter',
        'app-filtered-results',
        'app-filter',
        'app-results',
        'results-table-search-state-plugin',
        'usp-securityextension-SecurityRecordTypeConfiguration',
        'access-level-values'
    ]
});