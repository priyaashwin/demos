YUI.add('react-security-app-view', function(Y) {
  'use-strict';

  var Micro = Y.Template.Micro;
  var EVT_INFO_MESSAGE="infoMessage:message";

  var BTN_TEMPLATE=Micro.compile('<li class="react-security-app-button-li <%=this.first?"first":""%> <%=this.last?"last":""%>"><a href="#none" data-button-action="<%=this.action%>" class="pure-button usp-fx-all" aria-label="<%=this.ariaLabel%>" title="<%=this.title%>" tabindex="1"><i class="<%=this.className%>"></i><span> <%=this.label%></span></a></li>');
  
  var SuccessMessageMicroSyntax = {
          code: /\{\{%([\s\S]+?)%\}\}/g,
          escapedOutput: /\{\{(?!%)([\s\S]+?)\}\}/g,
          rawOutput: /\{\{\{([\s\S]+?)\}\}\}/g
  };
  
  
  Y.namespace('app.admin.security.base').ReactAppView = Y.Base.create('reactAppView', Y.View, [], {
    reactFactoryType:undefined,
    initializer:function(config){
      this.toolbar = new Y.usp.app.AppToolbar({
        permissions: Y.merge(config.permissions, {
          canCancel:true
        })
      }).addTarget(this);

      
      var model=this.get('model');
      
      var createFactory = function(type) {
        return React.createElement.bind(null, type);
      };
    
      this.__eventBindings=[
                            this.on('*:save', this._handleSave, this),
                            model.after('load', this.handleAfterModelLoad, this),
                            this.after('busyChange',this._handleBusyChange, this)
      ];
    
      this.componentFactory = createFactory(this.reactFactoryType);

      //plug in error handling
      this.plug(Y.Plugin.usp.ModelErrorsPlugin);
      
      //load the model - this will trigger a render when complete
      model.load();
    },
    _handleBusyChange:function(e){
      if(e.newVal){
        this._showMask();
      }else{
        this._hideMask();
      }
    },
    render:function(){
      var container=this.get('container'),
          model=this.get('model');
      
      if(this.template){
        container.setHTML(
          this.template({
            modelData:model?model.toJSON():{},
            labels:this.get('labels')
          }));
      }
      if(!this.reactAppContainer){
        this.reactAppContainer=Y.Node.create('<div style="position:relative"></div>');
      }

      container.appendChild(this.reactAppContainer);
      container.addClass(this.name);
      
      return this;
    },
    handleAfterModelLoad:function(){
      this.setToolbarButtons();
      this.renderComponent();
    },
    renderComponent:function(){
      
    },
    _handleSave:function(e){
      if(this.get('busy')){
        //don't allow more than one save
        return;
      }
      
      this.set('busy', true);
      //clear any existing errors shown - we call into the errors plugin
      this.modelErrors.clearErrors();
      
      if(this.handleSave){
        this.handleSave();
      }
    },
    _showMask:function(){
      var container=this.get('container');
      if(!container.busy){
        container.plug(Y.Plugin.BusyOverlay, {
          css: 'loading-main-mask'
        });
      }
      
      container.busy.show();
    },
    _hideMask:function(){
      var container=this.get('container');
      container.busy.hide();
    },
    setToolbarButtons:function(){
      var toolbar=this.toolbar,
          model=this.get('model'),
          permissions=this.get('permissions');
      
      var buttons=this.getButtons(permissions||{});
    
      //remove existing buttons
      toolbar.get('toolbarNode').all('.react-security-app-button-li').remove(true);
      
      //add buttons
      buttons.forEach(function(b){
        toolbar.get('toolbarNode').one('ul').appendChild(BTN_TEMPLATE(b));
      });
      
      var disabledButtons=buttons.filter(function(b){
        return b.disabled;
      });
      
      disabledButtons.forEach(function(b){
        toolbar.disableButton(b.action);
      });
    },
    getButtons:function(){
      return [];
    },
    fireMessage:function(message){
      var model=this.get('model');
      var data=model?model.toJSON():{};
      
      if (message) {
        // fire success message
        Y.fire(EVT_INFO_MESSAGE, {
          //execute template for success message
          message: Micro.render(message, data, SuccessMessageMicroSyntax)
        });
      }
    },
    destructor:function(){
      var container=this.get('container');
      if(container.busy){
        container.unplug(Y.Plugin.BusyOverlay);
      }
      
      //unbind event handles
      this.__eventBindings.forEach(function(handler) {
        handler.detach();
      });
      delete this.__eventBindings;
      
      //remove the buttons we added      
      this.toolbar.get('toolbarNode').all('.react-security-app-button-li').remove(true);
            
      //destroy the toolbar
      this.toolbar.removeTarget(this);
      
      this.toolbar.destroy();
      delete this.toolbar;
      
      if(this.reactAppContainer){
        ReactDOM.unmountComponentAtNode(this.reactAppContainer.getDOMNode());
        
        this.reactAppContainer.destroy(true);
        
        delete this.reactAppContainer;
      
      }
      //unplug errors plugin
      this.unplug(Y.Plugin.usp.ModelErrorsPlugin);
    }
    
  },{
    ATTRS:{
      labels:{
        value:{}
      },
      permissions:{
      },
      busy:{
        value:false
      }
    }
  });
  
}, '0.0.1', {
  requires: ['yui-base',
             'view',
             'gallery-busyoverlay',
             'app-toolbar',
             'template-micro',
             'model-errors-plugin'
  ]
});