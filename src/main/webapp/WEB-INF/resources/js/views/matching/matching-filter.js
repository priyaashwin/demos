YUI.add('matching-filter', function(Y) {

    Y.namespace('app.matching').MatchingFilterModel = Y.Base.create('matchingFilterModel', Y.usp.ResultsFilterModel, [], {}, {
        ATTRS: {
            fosterCareType: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            },
            gender: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            },
            supportsAge: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            },
            supportsDisabled: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            }
        }
    });

    Y.namespace('app.matching').MatchingResultsFilter = Y.Base.create('matchingResultsFilter', Y.usp.ResultsFilter, [Y.usp.ResultsFilterModelSyncMixin], {
        template: Y.Handlebars.templates.matchingResultsFilter,
        events: {
            '#matchingFilter_clearAll': {
                click: '_handleClearAll'
            }
        },
        _handleClearAll: function(e) {
            e.preventDefault();
            this.clearAll();
        },
    }, {
        ATTRS: {
            otherData: {
                value: {
                    fosterCareType: Y.secured.uspCategory.childlookedafter.typeOfCare.category.getActiveCodedEntries(),
                    //TODO This probably should use gender coded entry.  Story only explicitly mentions male/female
                    gender: [{
                        code: "MALE",
                        name: "Male"
                    }, {
                        code: "FEMALE",
                        name: "Female"
                    }],
                    //TODO should this use a coded entry?
                    supportsDisabled: [{
                        code: "true",
                        name: "Yes"
                    }, {
                        code: "false",
                        name: "No"
                    }]
                }
            }
        }
    });

    Y.namespace('app.matching').MatchingFilter = Y.Base.create('matchingFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.matching.MatchingFilterModel,
        filterType: Y.app.matching.MatchingResultsFilter,
        filterTemplate: Y.Handlebars.templates.matchingFilterTemplate,
        filterContextTemplate: Y.Handlebars.templates.matchingFilterContext
    });


}, '0.0.1', {
    requires: [
        'yui-base',
        'results-filter',
        'app-filter',
        'handlebars-helpers',
        'handlebars-matching-templates',
        'secured-categories-childlookedafter-component-TypeOfCare'
    ]
});