YUI.add('matching-controller-view', function(Y) {

    Y.namespace('app.matching').MatchingControllerView = Y.Base.create('matchingControllerView', Y.View, [], {

        initializer: function(config) {

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            this.results = new Y.app.matching.MatchingFilteredResults({
                filterConfig: config.filterConfig,
                filterContextConfig: config.filterContextConfig,
                searchConfig: config.searchConfig
            }).addTarget(this);
            
            this.get('container').delegate('click', this.showSummary, '.pure-table-data tr a', this);
        },

        showSummary: function(e) {
        	var node = e.currentTarget;
            e.preventDefault();
            
            if(node.hasClass('switch-focus')) {
               window.location=node.get('href');
            }
        },

        render: function() {
            this.get('container').setHTML(this.results.render().get('container'));
            return this;
        },

        destructor: function() {

            this._matchingEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._matchingEvtHandlers;

            this.matchingResults.removeTarget(this);
            this.matchingResults.destroy();
            delete this.matchingResults;
            
            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-toolbar',
        'matching-results',
        'handlebars-matching-templates'
    ]
});