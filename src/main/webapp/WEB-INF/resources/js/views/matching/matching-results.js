YUI.add('matching-results', function(Y) {

	var AppColumnFormatters = Y.app.ColumnFormatters;
		
    var careTypesFormatter = function(o) {
    	var stats = o.record.get('careTypeStatistics');
    	var response="";
    	var codedEntries = Y.uspCategory.childlookedafter.typeOfCare.category.codedEntries
    	stats.forEach(function(careType){
    		var typeOfCareCodedEntry = codedEntries[careType.typeOfCare];
    		var typeOfCareName = typeOfCareCodedEntry?typeOfCareCodedEntry.name : 'Other';
    		response+=typeOfCareName + " - placements: " + careType.placements +" available: " + careType.placesAvailable + "<br />";
    	});
    	return response;
    };
    
    var maxAvailablePlacesFormatter = function(o) {
    	// getting first registration since registration details will be the same for the foster carer
        var maxAvailablePlaces = o.record.get('approvalSummaries!fosterCarerSummary').currentApprovals[0].fosterCarerRegistration.maxAvailablePlaces;
        return maxAvailablePlaces;
    };
    
    var maxAvailableRoomsFormatter = function(o) {
    	// getting first registration since registration details will be the same for the foster carer
        var maxAvailableRooms = o.record.get('approvalSummaries!fosterCarerSummary').currentApprovals[0].fosterCarerRegistration.maxAvailableRooms;
        return maxAvailableRooms;
    };
    

    Y.Do.after(function() {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({
            key: 'approvalSummaries!fosterCarerSummary!status',
            locator: 'approvalSummaries.fosterCarerSummary.status'
        });
        schema.resultFields.push({
            key: 'name',
            locator: 'name'
        });
        schema.resultFields.push({
            key: 'supportedCareTypes',
            locator: 'supportedCareTypes'
        });
        schema.resultFields.push({
            key: 'addresses',
            locator: 'addresses'
        });
        schema.resultFields.push({
            key: 'personIdentifier',
            locator: 'personIdentifier'
        });
        schema.resultFields.push({
            key: 'careTypeStatistics',
            locator: 'careTypeStatistics'
        });
        schema.resultFields.push({
            key: 'approvalSummaries!fosterCarerSummary',
            locator: 'approvalSummaries.fosterCarerSummary'
        });
        return new Y.Do.AlterReturn(null, schema);
    }, Y.usp.childlookedafter.ChildCarerApproval, 'getSchema', Y.usp.childlookedafter.ChildCarerApproval);


    Y.namespace('app.matching').MatchingResults = Y.Base.create('matchingResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.app.matching.PaginatedFilteredChildCarerApprovalList({
                filterModel: config.filterModel,
                url: config.url
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {};
            var permissions = config.permissions;
            
            return [{
                key: 'name',
                label: labels.carer,
                width: '50%',
                formatter: AppColumnFormatters.linkable,
                allowHTML: true,
                url: config.summaryUrl
            }, {
                key: 'approvalSummaries',
                formatter: maxAvailablePlacesFormatter,
                allowHTML: true,
                label: labels.maxAvailablePlaces,
                width: '10%'
            }, {
                key: 'approvalSummaries',
                formatter: maxAvailableRoomsFormatter,
                allowHTML: true,
                label: labels.maxAvailableRooms,
                width: '10%'
            }, {
                key: 'careTypeStatistics',
                formatter: careTypesFormatter,
                allowHTML: true,
                label: labels.types,
                width: '30%'
            }];
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                      fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                      cfg: {
                        state: 'expanded',
                        summaryFormatter:AppColumnFormatters.summaryAddressFormat
                      }
                    }];
                }
            }
        }
    });


    Y.namespace('app.matching').PaginatedFilteredChildCarerApprovalList = Y.Base.create("paginatedFilteredChildCarerApprovalList",
        Y.usp.childlookedafter.PaginatedChildCarerApprovalList, [Y.usp.ResultsFilterURLMixin], {
            whitelist: ['fosterCareType', 'gender', 'supportsDisabled', 'supportsAge'],
            staticData: {
                useSoundex: false,
                appendWildcard: true
            }
        }, {
            ATTRS: {
                filterModel: {
                    writeOnce: 'initOnly'
                }
            }
        });


    Y.namespace('app.matching').MatchingFilteredResults = Y.Base.create('matchingFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.matching.MatchingFilter,
        resultsType: Y.app.matching.MatchingResults,
        updateResults: function() {
            this.get('filter').updateFilterListOptions();
            this.reload();
        }
    });





}, '0.0.1', {
    requires: [
        'yui-base',
        'app-results',
        'app-filtered-results',
        'results-table-summary-row-plugin',
        'results-filter',
        'matching-filter',
        'usp-childlookedafter-ChildCarerApproval',
        'caserecording-results-formatters',
        'categories-childlookedafter-component-TypeOfCare'
    ]
});