YUI.add('source-form', function(Y) {
  var L = Y.Lang,
    O = Y.Object,
    Micro = Y.Template.Micro;
  var OTHER_VALUE = '___OTHER___';
  var OTHER_OPTION_GROUP = '<optgroup label="Other"><option value="' + OTHER_VALUE + '">Please specify</option></optGroup>';
  
  var SOURCE_OUTPUT_TEMPLATE=Micro.compile('<div class="pure-g-r"><div class="pure-u-1-2 l-box"><strong>Source organisation:</strong> <span><%=this.sourceOrganisation%></span></div><div class="pure-u-1-2 l-box"><strong>Source:</strong> <span><%=this.source%></span></div></div>');
  
  var isEmpty=function(val) {
    return (typeof val === 'undefined' || val === '' || val === null);
  };

  /**
   * Provide validation and serialization for model with Source and SourceOrganisation
   */
  function FormWithSourceMixin(config) {
    if(!this.customValidation){
      this.customValidation=this.validateSource;
    }else{
      Y.Do.after(function(attribs){
        var errors=Y.Do.originalRetVal || {};
        
        Y.mix(errors, this.validateSource(attribs));
        
        Y.Do.AlterReturn("Update with merged errors", errors);
      }, this, 'customValidation', this);
    }
    
    Y.Do.after(this._cleanData, this, 'serialize', this);
    
    this.labels = (config||{}).labels||{};
  }
  FormWithSourceMixin.prototype = {
    /**
     * Validation for attachment with source to ensure other value specified when
     * "Other" is selected 
     */
    validateSource: function(attrs) {
      var errors = {},
        labels = this.labels || {};
      var source = attrs.source,
        otherSource = L.trim(attrs.otherSource || '');
      var sourceOrganisation = attrs.sourceOrganisation,
        otherSourceOrganisation = L.trim(attrs.otherSourceOrganisation || '');
      
      if (sourceOrganisation === OTHER_VALUE && otherSourceOrganisation.length === 0) {
        errors['otherSourceOrganisation'] = labels.otherSourceOrganisation||'You must specificy a value for other source organisation';
      }
      if(sourceOrganisation === OTHER_VALUE && source.length!==0 && source !== OTHER_VALUE){
        errors['source'] = labels.sourceMustBeOtherOrNull||'If source organisation is Other, source must either be blank, or also set to Other';
      }
      
      if (source === OTHER_VALUE && otherSource.length === 0) {
        errors['otherSource'] = labels.otherSource||'You must specificy a value for other source';
      }
      return errors;
    },
    /**
     * amend the data for the serialize function to exclude the '___OTHER___' value
     */
    _cleanData: function() {
      //re-parse the json
      var data=Y.JSON.parse(Y.Do.currentRetVal);
      if (data.source === OTHER_VALUE) {
        data.source = null;
      }
      if (data.sourceOrganisation === OTHER_VALUE) {
        data.sourceOrganisation = null;
      }
      
      return new Y.Do.AlterReturn('Exclude other source values', Y.JSON.stringify(data));
    }
  };
  
  Y.namespace('app.source').FormWithSourceMixin = FormWithSourceMixin;
  
  Y.namespace('app.source').SelectGroup = Y.Base.create('selectGroup', Y.View, [], {
    events: {
      'select': {
        change: 'handleSelectChange'
      },
      'input': {
        change: 'handleInputChange'
      }
    },
    initializer: function() {
      this._evts = [
          this.after('selectedValueChange', this.syncModelValue, this),
          this.after('valueChange', this.syncModelValue, this),
          this.after('parentValueChange', this.constrainSelectionValues, this)
        ];
      
      this.values=[];
      //setup codes
      if(this.get('constrainByParentSelection')){
        
      }else{
        this.values=O.values(this.get('category').getActiveCodedEntries());
      }
    },
    destructor: function() {
      this._evts.forEach(function(handler) {
        handler.detach();
      });
      delete this._evts;
    },
    constrainSelectionValues:function(e){
      var thisSelectedValue=this.get('selectedValue');
      var parentVal=e.newVal||'';
      
      if(this.get('constrainByParentSelection')){
        if(OTHER_VALUE!==parentVal && parentVal!==''){
          //constrain the selection 
          this.values=O.values(this.get('category').getActiveCodedEntries()).filter(function(codedEntry){
            return codedEntry.parentCode===parentVal;
          });
          
          if(OTHER_VALUE!==thisSelectedValue){
            //reset
            this.set('selectedValue', '', {
              silent:true
            });
          }
        }else{
          this.values=[];
          
          if(OTHER_VALUE===parentVal){
            //remove the selection - can only select null or OTHER_VALUE
            if(OTHER_VALUE!==thisSelectedValue||!isEmpty(this.selectedValue)){
              this.set('selectedValue', '',{
                silent:true
              });
            }
          }else{
            this.set('selectedValue', '',{
              silent:true
            });
          }
        }
        //re-render the updated select
        this.render();
      }
    },
    syncModelValue: function() {
      var container = this.get('container'),
        select = container.one('select'),
        input = container.one('input'),
        selectedValue = this.get('selectedValue')||'',
        value = this.get('value')||'';
      //if there is no selectedValue but an "other" value is specified
      //ensure the inputs are set as necessary
      if (isEmpty(selectedValue) && !isEmpty(value)) {
        selectedValue = OTHER_VALUE;
      }
      //select the pre-selected value
      select.set('value', selectedValue);
      input.set('value', value);
      //now sync the inputs
      this.syncInput();
    },
    handleSelectChange: function(e) {
      this.set('selectedValue', e.currentTarget.get('value'));
    },
    handleInputChange:function(e){
      this.set('value', e.currentTarget.get('value'));
    },
    lookupExistingEntry: function() {
      var  category = this.get('category'),
          selectedValue = this.get('selectedValue'),
          codes=category.codedEntries||[];
    
      var codedEntry, entry = {
        code: null,
        name: null
      };
      
      if (!isEmpty(selectedValue)) {
        codedEntry = codes[selectedValue];
        if (codedEntry) {
          entry.code = codedEntry.code;
          entry.name = codedEntry.name;
        }
      }
      return entry;
    },
    render: function() {
      this._setSelectOptions();
      
      this.syncModelValue();
      
      return this;
    },
    _setSelectOptions: function(){
      var container = this.get('container'),
      select = container.one('select');
    
      var existingSelection = this.lookupExistingEntry() || {};
      Y.FUtil.setSelectOptions(select, this.values, existingSelection.code, existingSelection.name, true, true, 'code');
      select.append(OTHER_OPTION_GROUP);
    },
    syncInput: function() {
      var container = this.get('container'),
        select = container.one('select'),
        input = container.one('input');
      var inputParent = input.ancestor('.otherInput', false, container);
      var selectedOption = select.get('options').item(select.get('selectedIndex'));
      var selectedValue=selectedOption?selectedOption.get('value'):'';
      if (OTHER_VALUE===selectedValue) {
        //reveal the input box
        inputParent.show();
      } else {
        //disable and clear input
        input.set('value', '');
        //hide via parent
        inputParent.hide();
      }
      //ensure the dialog realigns now we have updated the DOM
      this.fire('align');
    }
  }, {
    ATTRS: {
      category: {
        value: {}
      },
      selectedValue: {
        value: ''
      },
      value: {
        value: ''
      },
      /**
       * Turns on the ability to constrain the available
       * selections based on the selection in the parent.
       */
      constrainByParentSelection:{
        value:false,
      },
      /**
       * An attribute to track the parent selection
       * This allows the selection list to be filtered
       * by the parent code
       */
      parentValue:{
        value:null
      }
    }
  });
  
  Y.namespace('app.source').SourceForm = Y.Base.create('sourceForm', Y.View, [], {
    template: Y.Handlebars.templates.sourceAddEditDialog,
    initializer:function(){
      this._sourceEvents=[
        this.after('*:selectedValueChange', this.handleSelectedValueChange, this)
      ];
    },
    handleSelectedValueChange:function(e){
      //only want value changes in the sourceOrganisation
      if(e.target==this.sourceOrganisationSelectGroup){
        //update the source select group with the selected value
        this.sourceSelectGroup.set('parentValue', e.newVal);
      }
    },
    render: function() {
      var container = this.get('container'),
        model = this.get('model'),
        html = this.template({
          modelData: model.toJSON(),
          otherData: this.get('otherData'),
          codedEntries: this.get('codedEntries'),
          labels: this.get('labels'),
          isMandatory: this.get('mandatory')
        });
      
      container.setHTML(html);
      
      if (!this.sourceOrganisationSelectGroup) {
        this.sourceOrganisationSelectGroup = new Y.app.source.SelectGroup({
          container: container.one('#sourceOrganisationSelectGroup'),
          category: this.get('categories.sourceOrganisation'),
          value: model.get('otherSourceOrganisation'),
          selectedValue: model.get('sourceOrganisation')
        });
        this.sourceOrganisationSelectGroup.addTarget(this).render();
      } else {
        this.sourceSelectGroup.setAttrs({
          value: model.get('otherSourceOrganisation'),
          selectedValue: model.get('sourceOrganisation')
        });
      }
      
      if (!this.sourceSelectGroup) {
        this.sourceSelectGroup = new Y.app.source.SelectGroup({
          container: container.one('#sourceSelectGroup'),
          category: this.get('categories.source'),
          value: model.get('otherSource'),
          selectedValue: model.get('source'),
          parentValue: this.sourceOrganisationSelectGroup.get('selectedValue'),
          constrainByParentSelection:true
        });
        this.sourceSelectGroup.addTarget(this).render();
      } else {
        this.sourceSelectGroup.setAttrs({
          value: model.get('otherSource'),
          selectedValue: model.get('source'),
          //pass in the selection from the source organisation
          parentValue: this.sourceOrganisationSelectGroup.get('selectedValue')
        });
      }      
      return this;
    },
    destructor: function() {
      this._sourceEvents.forEach(function(handler) {
        handler.detach();
      });
      delete this._sourceEvents;
      
      if (this.sourceSelectGroup) {
        this.sourceSelectGroup.removeTarget(this);
        this.sourceSelectGroup.destroy();
        delete this.sourceSelectGroup;
      }
      if (this.sourceOrganisationSelectGroup) {
        this.sourceOrganisationSelectGroup.removeTarget(this);
        this.sourceOrganisationSelectGroup.destroy();
        delete this.sourceOrganisationSelectGroup;
      }
    }
  }, {
    ATTRS: {
      categories: {
        value: {
          source: Y.uspCategory.core.source.category,
          sourceOrganisation: Y.uspCategory.core.sourceOrganisation.category
        }
      },
      mandatory:{
        value:false
      }
    }
  });
  
  Y.namespace('app.source').SourceView = Y.Base.create('sourceView', Y.usp.View, [], {
    template: Y.Handlebars.templates.sourceViewDialog
  }, {
    ATTRS: {
      codedEntries: {
        value: {
          source: Y.uspCategory.core.source.category.codedEntries,
          sourceOrganisation: Y.uspCategory.core.sourceOrganisation.category.codedEntries
        }
      }
    }
  });
  
  Y.namespace('app.source').SourceSummaryFormatter = function(o) {
    var record=o.record;
    var source=record.get('source'), otherSource=record.get('otherSource');
    var sourceOrganisation=record.get('sourceOrganisation'), otherSourceOrganisation=record.get('otherSourceOrganisation');
    
    var sourceOutput='';
    var sourceOrganisationOutput='';
    
    if(!isEmpty(source)){
      sourceOutput=(Y.uspCategory.core.source.category.codedEntries[source]||{}).name;
    }else{
      sourceOutput=otherSource;
    }

    if(!isEmpty(sourceOrganisation)){
      sourceOrganisationOutput=(Y.uspCategory.core.sourceOrganisation.category.codedEntries[sourceOrganisation]||{}).name;
    }else{
      sourceOrganisationOutput=otherSourceOrganisation;
    }

    return SOURCE_OUTPUT_TEMPLATE({
      source:sourceOutput,
      sourceOrganisation:sourceOrganisationOutput
    });
  };
  
  Y.namespace('app.source').SourceFormatter = function(o) {
    var record=o.record,
        sourceOutput;
    
    var source=record.get('source'), otherSource=record.get('otherSource');
    
    if(!isEmpty(source)){
      sourceOutput=(Y.uspCategory.core.source.category.codedEntries[source]||{}).name;
    }else{
      sourceOutput=otherSource;
    }
    return sourceOutput;
  };
  
  Y.namespace('app.source').SourceOrganisationFormatter = function(o) {
    var record=o.record,
        sourceOutput;
    
    var source=record.get('sourceOrganisation'), otherSource=record.get('otherSourceOrganisation');
    
    if(!isEmpty(source)){
      sourceOutput=(Y.uspCategory.core.sourceOrganisation.category.codedEntries[source]||{}).name;
    }else{
      sourceOutput=otherSource;
    }
    return sourceOutput;
  };
  
  Y.namespace('app.source').SourceWithSourceOrganisationFormatter = function(o) {
    var source=Y.app.source.SourceFormatter(o);
    var sourceOrganisation=Y.app.source.SourceOrganisationFormatter(o);
    
    return (sourceOrganisation || '') + (source ? ' (' +source + ')' : '');
  };
  
}, '0.0.1', {
  requires: ['yui-base',
    'event-custom-base',
    'usp-view',
    'form-util',
    'handlebars-helpers',
    'handlebars-source-templates',
    'categories-core-component-Source',
    'categories-core-component-SourceOrganisation',
    'template-micro'
  ]
});