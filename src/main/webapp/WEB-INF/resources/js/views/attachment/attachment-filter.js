/**
 * 
 * Provides common mixin classes to provide a generic attachment filter
 * functionality
 * 
 * @module attachment-filter
 */
YUI.add('attachment-filter', function(Y) {
  var O=Y.Object;
  
  /**
   * Provide common filter model attributes for entity with attachment filtering
   */
  function EntityWithAttachmentFilterModelMixin() {
    this.after('hasAttachmentsChange', this._validateHasAttachments, this);
    this.after(['attachmentSourceChange', 
      'attachmentOtherSourceChange', 
      'attachmentSourceOrganisationChange', 
      'attachmentOtherSourceOrganisationChange',
      'attachmentTitleChange'], this._validateAttachmentAttributeChange, this);
  }


  EntityWithAttachmentFilterModelMixin.prototype={
      _validateHasAttachments:function(e){
        //hasAttachments is a string value
        if(e.newVal&&(e.newVal.length===1&&e.newVal[0]==='false')){
          //user has specifically excluded results with attachments - so clear all other attachment filters
          this.reset('attachmentSource');
          this.reset('attachmentOtherSource');
          this.reset('attachmentSourceOrganisation');
          this.reset('attachmentOtherSourceOrganisation');
          this.reset('attachmentTitle');
        }
      },
      _validateAttachmentAttributeChange:function(e){
        if(e.newVal&&e.newVal.length!==0){
          //if hasAttachments is explicitly set to false - must reset it
          if(this.get('hasAttachments').length===1&&this.get('hasAttachments')[0]==='false'){
            //must set the hasAttachments attribute - note this is a string value
            this.reset('hasAttachments');
          }
        }
      }
  };
  
  EntityWithAttachmentFilterModelMixin.ATTRS = {
     /**
      * This is a tri-state so is represented by a string value
      */
    hasAttachments: {
      setter: Y.usp.ResultsFilterModel.setListValue,
      getter: Y.usp.ResultsFilterModel.getListValue,
      value: []
    },
    attachmentSource: {
      USPType: 'String',
      setter: Y.usp.ResultsFilterModel.setListValue,
      getter: Y.usp.ResultsFilterModel.getListValue,
      value: []
    },
    attachmentOtherSource: {
      USPType: 'String',
      setter: Y.usp.ResultsFilterModel.setListValue,
      getter: Y.usp.ResultsFilterModel.getListValue,
      value: []
    },
    attachmentSourceOrganisation: {
      USPType: 'String',
      setter: Y.usp.ResultsFilterModel.setListValue,
      getter: Y.usp.ResultsFilterModel.getListValue,
      value: []
    },
    attachmentOtherSourceOrganisation: {
      USPType: 'String',
      setter: Y.usp.ResultsFilterModel.setListValue,
      getter: Y.usp.ResultsFilterModel.getListValue,
      value: []
    },
    attachmentTitle: {
      USPType: 'String',
      setter: Y.usp.ResultsFilterModel.setListValue,
      getter: Y.usp.ResultsFilterModel.getListValue,
      value: []
    }
  };

  /**
   * Provide common filter view functionality for filtering entity with attachments
   */
  function EntityWithAttachmentFilterViewMixin(config) {
    //AOP to execute our update view after the host class
    if (!this.updateView) {
      this.updateView = this._updateViewWithAttachmentFilter;
    } else {
      Y.Do.after(this._updateViewWithAttachmentFilter, this, 'updateView', this);
    }

    //AOP to execute update our lists after the host class render
    Y.Do.after(this._updateFilterListOptionsForAttachmentFilter, this, 'render', this);

    //ensure the showAttachmentFilter attribute is set - this is used to drive the template    
    var otherData = config.otherData || {};
    otherData.showAttachmentFilter = true;
    this.set('otherData', otherData);
  }

  EntityWithAttachmentFilterViewMixin.prototype = {
    _updateViewWithAttachmentFilter: function() {
      var container = this.get('container'),
        model = this.get('model'),
        otherData = this.get('otherData') || {},
        attachmentFilterCount = 0,
        showAttachmentFilter = otherData.showAttachmentFilter,
        attachmentFilterIndicator = container.one('.attachmentFilterSpan');

      if (showAttachmentFilter === true) {
        attachmentFilterCount += (model.get('attachmentSource') || []).length;
        attachmentFilterCount += (model.get('attachmentSourceOrganisation') || []).length;
        attachmentFilterCount += (model.get('attachmentOtherSource') || []).length;
        attachmentFilterCount += (model.get('attachmentOtherSourceOrganisation') || []).length;
        attachmentFilterCount += (model.get('hasAttachments') || []).length;
        attachmentFilterCount += (model.get('attachmentTitle') || []).length;

        if (attachmentFilterIndicator) {
          attachmentFilterIndicator.toggleView(attachmentFilterCount > 0);
        }
      }
    },
    _updateFilterListOptionsForAttachmentFilter: function() {
      //list of coded entries for source and sourceOrganisation
      var sourceTypes =  O.values(this.get('sourceTypes') || {}),
        sourceOrganisationTypes = O.values(this.get('sourceOrganisationTypes') || {});

      this._updateList('select[name="attachmentSource"]', sourceTypes.map(function(source) {
        return {
          name: source.name,
          id: source.code
        };
      }));
      this._updateList('select[name="attachmentSourceOrganisation"]', sourceOrganisationTypes.map(function(source) {
        return {
          name: source.name,
          id: source.code
        };
      }));
    }
  };

  EntityWithAttachmentFilterViewMixin.ATTRS = {
    sourceTypes: {
      value: Y.uspCategory.core.source.category.codedEntries
    },
    sourceOrganisationTypes: {
      value: Y.uspCategory.core.sourceOrganisation.category.codedEntries
    }
  };


  Y.namespace('app.attachment').EntityWithAttachmentFilterModelMixin = EntityWithAttachmentFilterModelMixin;
  Y.namespace('app.attachment').EntityWithAttachmentFilterViewMixin = EntityWithAttachmentFilterViewMixin;
}, '0.0.1', {
  requires: ['yui-base',
            'event-custom-base',
            'results-filter',
            'categories-core-component-Source',
            'categories-core-component-SourceOrganisation'
            ]
});