YUI.add('entry-filter', function(Y) {
  'use-strict';

  var L = Y.Lang,
    FUtil = Y.FUtil,
    DB = Y.usp.DataBindingUtil,
    A = Y.Array;

  Y.namespace('app.entry').BaseEntryFilterModel = Y.Base.create('baseEntryFilterModel', Y.usp.ResultsFilterModel, [], {
    initializer: function() {
      this.after('excludeGroupEntriesChange', this._validateGroupIdsValue, this);
      this.after('groupIdsChange', this._validateExcludeGroupEntriesValue, this);
    },
    _validateGroupIdsValue: function(e) {
      if (e.newVal === true) {
        //can't have groupIds when showing only person entries
        this.set('groupIds', []);
      }
    },
    _validateExcludeGroupEntriesValue: function(e) {
      if (e.newVal && e.newVal.length > 0) {
        this.set('excludeGroupEntries', false);
      }
    },
    customValidation: function(attrs) {
      //enforce our validation first - if we succeed this will call the parent validate
      var errors = {},
        endDate = attrs['endDate'],
        startDate = attrs['startDate'],
        startRecordedDate = attrs.startRecordedDate || {},
        endRecordedDate = attrs.endRecordedDate || {},
        startEditedDate = attrs.startEditedDate || {},
        endEditedDate = attrs.endEditedDate || {},

        labels = this.labels || {},
        validationLabels = labels.validation || {};

      if (L.isNumber(startDate) && L.isNumber(endDate)) {
        if (startDate > endDate) {
          errors['startDate'] = validationLabels.startDateBeforeEndDate;
        }
      }

      if (L.isNumber(startRecordedDate) && L.isNumber(endRecordedDate)) {
        if (startRecordedDate > endRecordedDate) {
          errors['startRecordedDate'] = validationLabels.startDateBeforeEndRecordedDate;
        }
      }

      if (L.isNumber(startEditedDate) && L.isNumber(endEditedDate)) {
        if (startEditedDate > endEditedDate) {
          errors['startEditedDate'] = validationLabels.startDateBeforeEndEditedDate;
        }
      }

      return errors;
    }

  }, {
    ATTRS: {
      startDate: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateFrom,
        getter: Y.usp.ResultsFilterModel.getDateFrom
      },
      endDate: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateTo,
        getter: Y.usp.ResultsFilterModel.getDateTo
      },
      startRecordedDate: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateFrom,
        getter: Y.usp.ResultsFilterModel.getDateFrom
      },
      endRecordedDate: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateTo,
        getter: Y.usp.ResultsFilterModel.getDateTo
      },
      startEditedDate: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateFrom,
        getter: Y.usp.ResultsFilterModel.getDateFrom
      },
      endEditedDate: {
        USPType: 'Date',
        value: null,
        setter: Y.usp.ResultsFilterModel.setDateTo,
        getter: Y.usp.ResultsFilterModel.getDateTo
      },
      impactList: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      sourceList: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      otherSource: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      sourceOrgList: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      otherSourceOrg: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      pracList: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      pracOrgList: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      groupIds: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      /**
       * This attribute is the store of data for the 
       * entryType/subType list
       */
      entryTypeSubTypeList: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        value: []
      },
      /**
       * values are sourced from the entryTypeSubTypeList
       * 
       * This attribute is readonly
       */
      entryTypeList: {
        value: [],
        getter: function() {
          var entryTypeSubTypeList = this.get('entryTypeSubTypeList') || [];
          //return first element which represents entry type
          //array is de-duplicated
          return A.dedupe(entryTypeSubTypeList.map(function(val) {
            return val.split('->')[0] || '';
          }));
        },
        readonly: true
      },
      /**
       * values are sourced from the entryTypeSubTypeList
       * 
       * This attribute is readonly
       */
      entrySubTypeList: {
        value: [],
        getter: function() {
          var entryTypeSubTypeList = this.get('entryTypeSubTypeList') || [];
          //return second element which represents entry subType
          //array is de-duplicated
          return A.dedupe(entryTypeSubTypeList.map(function(val) {
            return val.split('->')[1] || '';
          }));
        },
        readonly: true
      },
      statusList: {
        USPType: 'String',
        setter: Y.usp.ResultsFilterModel.setListValue,
        getter: Y.usp.ResultsFilterModel.getListValue,
        //default status list defined here
        value: ["COMPLETE", "DRAFT"]
      },
      excludeGroupEntries: {
        USPType: 'Boolean',
        setter: Y.usp.ResultsFilterModel.setBooleanValue,
        getter: Y.usp.ResultsFilterModel.getBooleanValue,
        value: false
      },
      /**
       * Controls access to the group filter
       */
      showGroupFilter: {
        USPType: 'Boolean',
        value: true
      },
      subjectSeenIndicatorVisible: {
        value: false
      },
      impactEnabled: {
        value: false
      },
      groupList: {
        value: []
      }
    }
  });

  Y.namespace('app.entry').BaseEntryFilterView = Y.Base.create('baseEntryFilterView', Y.usp.app.ResultsFilter, [], {
    template: Y.Handlebars.templates["entryResultsFilter"],
    /**
     * You must provide an implementation of this - to return the filter items
     * required to populate the lists in the filter
     */
    getFilterItemsModel: function() {
      return new Y.Model();
    },
    /**
     * You must provide an implementation of this - to return the model
     * that will return the Record that entries are attached to.
     * e.g. CaseNoteRecord
     */
    getRecordModel: function() {
      return new Y.Model();
    },
    events: {
      '#dateRangeShortcuts a': {
        click: '_handleDatePreselect'
      }
    },
    initializer: function() {
      //initialise our date calendars
      this.startDateCalendar = new Y.usp.CalendarPopup();
      this.endDateCalendar = new Y.usp.CalendarPopup();
      this.startRecordedDateCalendar = new Y.usp.CalendarPopup();
      this.endRecordedDateCalendar = new Y.usp.CalendarPopup();
      this.startEditedDateCalendar = new Y.usp.CalendarPopup();
      this.endEditedDateCalendar = new Y.usp.CalendarPopup();

      //set as bubble targets
      this.startDateCalendar.addTarget(this);
      this.endDateCalendar.addTarget(this);
      this.startRecordedDateCalendar.addTarget(this);
      this.endRecordedDateCalendar.addTarget(this);
      this.startEditedDateCalendar.addTarget(this);
      this.endEditedDateCalendar.addTarget(this);

      this._baseEntryFilterViewEvtHandles = [
        this.after('sourceListChange', this.updateSourceList, this),
        this.after('sourceOrgListChange', this.updateSourceOrgList, this),
        this.after('practitionerListChange', this.updatePractitionerList, this),
        this.after('practitionerOrgListChange', this.updatePractitionerOrgList, this),
        this.after('entryTypeSubTypeListChange', this.updateEntryTypeSubTypeList, this)
      ];
    },
    _getFilterItems: function(id) {
      var model = this.getFilterItemsModel(id);

      return new Y.Promise(function(resolve, reject) {
        var data = model.load(function(err) {
          if (err !== null) {
            //failed for some reason so reject the promise
            reject(err);
          } else {
            //success, so resolve the promise
            resolve(data);
          }
        });
      });
    },
    _getRecord: function() {
      var subject = this.get('subject') || {},
        model = this.getRecordModel(subject.subjectId, subject.subjectType);

      if (this.get('recordId') === null || this.get('recordId') === undefined || this.get('recordId') === '') {
        return new Y.Promise(function(resolve, reject) {
          model.load(function(err) {
            if (err !== null) {
              //failed for some reason so reject the promise
              reject(err);
            } else {
              //success, so resolve the promise with the id
              resolve(model.get('id'));
            }
          });
        });
      } else {
        return Y.when(this.get('recordId'));
      }
    },
    updateFilterListOptions: function() {
      //list of coded entries for entry type
      var entryTypes = this.get('entryTypes') || {},
        entrySubTypes = this.get('entrySubTypes') || {};

      //fire a promise to load the record 
      this._getRecord().then(function(id) {
        this.set('recordId', id);

        return this._getFilterItems(id);
      }.bind(this)).then(function(model) {
        this.set('sourceList', model.get('sources').filter(function(s) {
          //filter null or empty string
          return !!s;
        }).map(function(s) {
          var codedEntry = Y.uspCategory.core.source.category.codedEntries[s] || {};
          return {
            id: codedEntry.code,
            name: codedEntry.name || 'Unknown'
          };
        }));

        this.set('sourceOrgList', model.get('sourceOrgs').filter(function(s) {
          //filter null or empty string
          return !!s;
        }).map(function(s) {
          var codedEntry = Y.uspCategory.core.sourceOrganisation.category.codedEntries[s] || {};
          return {
            id: codedEntry.code,
            name: codedEntry.name || 'Unknown'
          };
        }));
        this.set('impactList', model.get('impacts').map(function(i) {
          return {
            name: i,
            id: i
          };
        }));
        this.set('practitionerList', model.get('practitioners').map(function(p) {
          return {
            name: p.name,
            id: p.personId
          };
        }));
        this.set('practitionerOrgList', model.get('practitionerOrgs').map(function(p) {
          return {
            name: p,
            id: p
          };
        }));

        //set up entry types
        var expandedEntryTypes = model.get('entryTypes').map(function(e) {
          var entry = entryTypes[e] || {
            code: e,
            name: e
          };
          return {
            id: entry.code,
            name: entry.name
          };
        });

        //now add sub - types
        model.get('entrySubTypes').forEach(function(e) {
          var entry = entrySubTypes[e] || {
            code: e,
            name: e
          };
          var parentEntry = entryTypes[entry.parentCode] || {};

          //add the expanded code
          expandedEntryTypes.push({
            id: (parentEntry.code || '') + '->' + entry.code,
            name: (parentEntry.name || '') + ' -> ' + entry.name
          });
        }.bind(this));


        // sort and set

        this.set('entryTypeSubTypeList', expandedEntryTypes.sort(function(a, b) {
          return (a.name).localeCompare(b.name);
        }));
      }.bind(this));
    },
    updateSourceList: function(e) {
      this._updateList('select[name="sourceList"]', e.newVal);
    },
    updateSourceOrgList: function(e) {
      this._updateList('select[name="sourceOrgList"]', e.newVal);
    },
    updatePractitionerList: function(e) {
      this._updateList('select[name="pracList"]', e.newVal);
    },
    updatePractitionerOrgList: function(e) {
      this._updateList('select[name="pracOrgList"]', e.newVal);
    },
    updateEntryTypeSubTypeList: function(e) {
      this._updateList('select[name="entryTypeSubTypeList"]', e.newVal);
    },
    _updateList: function(nodeId, values) {
      var container = this.get('container'),
        select = container.one(nodeId);

      if (select) {
        FUtil.setSelectOptions(select, values, null, null, false, true);

        //now we have out select box populated - we need to ensure that any current selection is made
        DB.setElementValue(select.getDOMNode(), select.get('name'));
      }
    },
    destructor: function() {
      this._baseEntryFilterViewEvtHandles.forEach(function(handle) {
        handle.detach();
      });

      delete this._baseEntryFilterViewEvtHandles;

      //destroy the calendars
      this.startDateCalendar.destroy();
      delete this.startDateCalendar;

      this.endDateCalendar.destroy();
      delete this.endDateCalendar;

      this.startRecordedDateCalendar.destroy();
      delete this.startRecordedDateCalendar;

      this.endRecordedDateCalendar.destroy();
      delete this.endRecordedDateCalendar;

      this.startEditedDateCalendar.destroy();
      delete this.startEditedDateCalendar;

      this.endEditedDateCalendar.destroy();
      delete this.endEditedDateCalendar;



      if (this.filterTabView) {
        this.filterTabView.destroy();

        delete this.filterTabView;
      }
    },
    render: function() {
      var container = this.get('container');

      //call into superclass to render
      Y.app.entry.BaseEntryFilterView.superclass.render.call(this);

      //sync the possible values for the filter
      this.updateFilterListOptions();

      this._updateGroupList();

      //set the input node for the calendar
      this.startDateCalendar.set('inputNode', container.one('input[name="startDate"]'));
      this.endDateCalendar.set('inputNode', container.one('input[name="endDate"]'));

      this.startRecordedDateCalendar.set('inputNode', container.one('input[name="startRecordedDate"]'));
      this.endRecordedDateCalendar.set('inputNode', container.one('input[name="endRecordedDate"]'));

      this.startEditedDateCalendar.set('inputNode', container.one('input[name="startEditedDate"]'));
      this.endEditedDateCalendar.set('inputNode', container.one('input[name="endEditedDate"]'));



      this.startDateCalendar.render();
      this.endDateCalendar.render();

      this.startRecordedDateCalendar.render();
      this.endRecordedDateCalendar.render();

      this.startEditedDateCalendar.render();
      this.endEditedDateCalendar.render();

      this.filterTabView = new Y.TabView({
        srcNode: container.one('#entryFilterTabsSrc'),
        render: container.one('#entryFilterTabs')
      });

      return this;
    },
    _updateGroupList: function() {
      this._updateList('select[name="groupIds"]', this.get('groupList'));
    },
    _handleDatePreselect: function(e) {
      e.preventDefault();

      var t = e.currentTarget,
        today = new Date(),
        startDate;

      // Filtering includes today, so a day less than required.
      if (t.hasClass('lastWeek')) {
        startDate = Y.Date.addDays(today, -6); // last week
      } else if (t.hasClass('lastMonth')) {
        startDate = Y.Date.addDays(today, -29); // last 30 days
      } else {
        startDate = Y.Date.addDays(today, -364); // last year
      }

      this.get("model").setAttrs({
        endDate: null,
        startDate: startDate.getTime()
      }, {
        src: 'filter'
      });
    },
    updateView: function() {
      var container = this.get('container'),
        model = this.get('model'),
        isDateFilterSet = !!(model.get('startDate') || model.get('endDate') || model.get('startRecordedDate') || model.get('endRecordedDate') || model.get('startEditedDate') || model.get('endEditedDate')),
        isPeopleFilterSet, isTypeFilterSet,
        dateFilterIndicator = container.one('.dateFilterSpan'),
        peopleFilterIndicator = container.one('.peopleFilterSpan'),
        typeFilterIndicator = container.one('.typeFilterSpan'),
        groupFilterIndicator = container.one('.groupFilterSpan'),
        peopleFilterCount = 0,
        typeFilterCount = 0,
        groupFilterCount = 0,
        showGroupFilter = model.get('showGroupFilter'),
        subjectSeenIndicatorVisible = model.get('subjectSeenIndicatorVisible');

      peopleFilterCount += (model.get('pracList') || []).length;
      peopleFilterCount += (model.get('pracOrgList') || []).length;
      peopleFilterCount += (model.get('sourceList') || []).length;
      peopleFilterCount += (model.get('sourceOrgList') || []).length;
      peopleFilterCount += (model.get('otherSource') || []).length;
      peopleFilterCount += (model.get('otherSourceOrg') || []).length;


      isPeopleFilterSet = peopleFilterCount > 0;

      typeFilterCount += (model.get('entryTypeList') || []).length;
      typeFilterCount += (model.get('entrySubTypeList') || []).length;
      typeFilterCount += (model.get('impactList') || []).length;
      typeFilterCount += (model.get('statusList') || []).length;
      if(subjectSeenIndicatorVisible) typeFilterCount += (model.get('subjectSeenIndicatorList') || []).length;
      isTypeFilterSet = typeFilterCount > 0;

      /* -- Set the filter icon on the tab view -- */
      if (dateFilterIndicator) {
        dateFilterIndicator.toggleView(isDateFilterSet);
      }

      if (peopleFilterIndicator) {
        peopleFilterIndicator.toggleView(isPeopleFilterSet);
      }
      if (typeFilterIndicator) {
        typeFilterIndicator.toggleView(isTypeFilterSet);
      }

      if (showGroupFilter) {
        groupFilterCount += (model.get('groupIds') || []).length;
        groupFilterCount += Number(!!model.get('excludeGroupEntries'));

        groupFilterIndicator.toggleView(groupFilterCount > 0);
      }
    }
  }, {
    ATTRS: {
      filterItemsUrl: {},
      groupList: {
        value: []
      },
      recordId: {},
      subject: {},
      entryTypes: {
        value: {}
      },
      entrySubTypes: {
        value: {}
      }
    }
  });


  Y.namespace('app.entry').BaseEntryFilterContext = Y.Base.create('baseEntryFilterContext', Y.usp.app.FilterContext, [], {
    template: Y.Handlebars.templates["entryActiveFilter"],
    blacklist: ['groupList', 'showGroupFilter', 'impactEnabled', 'entryTypeSubTypeList', 'subjectSeenIndicatorVisible']
  });

  Y.namespace('app.entry').BaseEntryFilter = Y.Base.create('baseEntryFilter', Y.usp.app.Filter, [], {
    disableFilter: function() {
      this._savedFilter = this.get('filterModel').toJSON();
      this.get('resultsFilter').disableFilter({
        //set the status to whatever the user picked
        statusList: this._savedFilter.statusList,
        //don't show group entries
        excludeGroupEntries: true
      });

      this.get('toolbarNode').one('.filter').setAttribute('disabled', 'disabled');
    },
    enableFilter: function() {
      var filterModel = this.get('filterModel');

      filterModel.reset();
      if (this._savedFilter) {
        this.get('filterModel').setAttrs(this._savedFilter);
        delete this._savedFilter;
      }
      this.get('resultsFilter').enableFilter();
      this.get('resultsFilter').applyFilter();
      this.get('toolbarNode').one('.filter').removeAttribute('disabled');
    },

    updateFilterListOptions: function() {
      this.get('resultsFilter').updateFilterListOptions();
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'calendar-popup',
    'form-util',
    'results-filter',
    'app-filter',
    'tabview',
    'promise',
    'handlebars-helpers',
    'handlebars-entry-templates',
    'categories-core-component-Source',
    'categories-core-component-SourceOrganisation'
  ]
});