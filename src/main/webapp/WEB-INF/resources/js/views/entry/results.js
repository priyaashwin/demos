YUI.add('entry-results', function(Y) {
  'use-strict';

  var L = Y.Lang,
    Micro = Y.Template.Micro,
    USPFormatters = Y.usp.ColumnFormatters;

  var CSS_HIDDEN_CLASS = 'yui3-hide';
  var CSS_COLUMN_ACTION_CLASS = 'pure-table-actions';

  var groupTypeIconTemplate = Micro.compile('<i class="fa fa-users entry-type group secondary-icon" title="Group <%=this.type%> - <%=this.name%> (<%=this.identifier%>)"><span class="hidden">Group <%=this.type%></span></i>');
  var personTypeIconTemplate = Micro.compile('<i class="fa fa-user entry-type person secondary-icon" title="Person <%=this.type%> - <%=this.name%> (<%=this.identifier%>)"><span class="hidden">Person <%=this.type%></span></i>');
  var statusTemplate = Micro.compile('<i data-title="<%=this.type%> status" data-content="<%=this.status%>" class="icon-state icon-state-<%=this.status.toLowerCase()%> tool-tip"><span class="hidden">State-<%=this.status.toLowerCase()%></span></i>');
  var impactTemplate = Micro.compile('<i class="fa <%=this.icon%> impact-<%=this.impact.toLowerCase()%> tool-tip" data-title="Impact" data-content="<%=this.impact%>"><span class="hidden">Impact-<%=this.impact.toLowerCase()%></span></i>');

  var RowUpdateModel = Y.Base.create("rowUpdateModel", Y.Model, [Y.ModelSync.REST], {}, {
    ATTRS: {
      position: {
        value: "BEFORE_NEIGHBOUR"
      },
      neighbourId: {}
    }
  });
  var appendPractitionerOrganisation = function(o) {
    return (o.value || '') + (o.data.practitionerOrganisation ? ' (' + o.data.practitionerOrganisation + ')' : '');
  };

  var textFull = function(o, isText) {
    var formatted = '';

    if ('undefined' !== typeof o.value && null !== o.value) {
      formatted = isText ? USPFormatters.textFormat(o) : o.value;
    }

    return '<div style="max-height:400px; overflow-y:auto">' + formatted + '</div>';
  };

  var textTruncate = function(o) {
    if (o.value === null) {
      return '';
    }
    //FIXME: resource-heavy client-side implementation of HTML->plaintext - probably better handled by a specific VO and server-side transformation
    var tmpNode = document.createElement("DIV"),
      truncatedText, words, imageCount = 0,
      imageMatcher = /<img/g,
      imageTitleTemplate = ' <i class="fa fa-picture-o" title="This {type} contains {imageCount} image{plural}">',
      htmlEventText = o.value.replace(/src[ ]*=[ ]*/, "nonLoadingSrc=");

    tmpNode.innerHTML = htmlEventText;

    //iterate over image matches, counting them
    while (imageMatcher.exec(htmlEventText)) {
      imageCount++;
    }

    truncatedText = Y.Escape.html(tmpNode.textContent || tmpNode.innerText || "");
    words = truncatedText.split(" ");
    truncatedText = words.slice(0, 40).join(" ") + (words.length > 40 ? '...' : '') +
      (imageCount > 0 ?
        L.sub(imageTitleTemplate, {
          type: o.column.type || '',
          imageCount: imageCount,
          plural: (imageCount > 1 ? 's' : '')
        }) :
        ''
      );

    return truncatedText;
  };

  var partialVisibilityIcon = function(o) {
    if (!o.record.get('visibleToAll')) {
      return Y.Lang.sub('<span class="fa attribute-partial-visibilty" title="{title}"></span>', {
        title: 'Partial visibility'
      });
    }
    return '';
  };

  var entryTypeIcon = function(o) {
    var template;
    switch (o.data.subjectIdName.type) {
      case 'GROUP':
        template = groupTypeIconTemplate;
        break;
      case 'PERSON':
        template = personTypeIconTemplate;
        break;
    }
    return template({
      name: o.data.subjectIdName.name,
      identifier: o.data.subjectIdName.identifier,
      type: o.column.type || ''
    });
  };

  var statusIndicator = function(o) {
    var status = '';
    if (o.value) {
      status = statusTemplate({
        status: o.value,
        type: o.column.type
      });
    }
    return status;
  };

  var impactIndicator = function(o) {
    var impact = '',
      icon = '';
    if (o.value) {
      switch (o.value) {
        case 'POSITIVE':
          icon = 'fa-check';
          break;
        case 'NEGATIVE':
          icon = 'fa-minus';
          break;
        case 'NEUTRAL':
          icon = 'fa-times';
          break;
        default:
          icon = 'fa-question';
      }

      impact = impactTemplate({
        impact: o.value,
        icon: icon
      });
    }
    return impact;
  };

  Y.namespace('app.entry').BaseEntryResults = Y.Base.create('baseEntryResults', Y.usp.app.Results, [], {
    initializer: function(config) {
      //hang onto that update url
      this.set('updateRowUrl', config.searchConfig.updateRowUrl);

      this._baseEntryResultsEvtHandlers = [
        this.after('reorderChange', this._handleReorderChange, this),
        this.after('orderbyChange', this._handleOrderbyChange, this),
        this.on('drag:enter', this.handleDragEnter, this),
        this.on('drag:start', this.handleDragStart, this),
        this.on('drag:end', this.handleDragEnd, this),
        this.on('drag:exit', this.handleDragExit, this),
        this.on('drag:drophit', this.handleDragExit, this),
        this.on('*:rowDropped', this.handleRowDropped, this),
        this.after('*:rowDropped', this.handleAfterRowDropped, this)
      ];
    },
    destructor: function() {
      this._baseEntryResultsEvtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._baseEntryResultsEvtHandlers;
    },
    appendPractitionerOrganisationFormatter: function(o) {
      return appendPractitionerOrganisation(o);
    },
    textWithTypeTruncateFormatter: function(o) {
      var subjectType = this.subjectType,
        htmlEventText = '';

      var text = [];
      if (subjectType === 'group') {
        text.push(partialVisibilityIcon(o));
      } else if (subjectType === 'person') {
        text.push(entryTypeIcon(o));
      }

      if (typeof o.value !== 'undefined' && o.value !== null) {
        htmlEventText = USPFormatters.textFormat(o);
      }
      text.push(textTruncate({value: htmlEventText}));

      return text.join('');
    },
    textTruncateFormatter: function(o) {
      var htmlEventText = '';
      if (typeof o.value !== 'undefined' && o.value !== null) {
        htmlEventText = USPFormatters.textFormat(o);
      }
      return textTruncate({value: htmlEventText});
    },
    textWithTypeFullFormatter: function(o) {
      var subjectType = this.subjectType;

      var text = [];
      if (subjectType === 'group') {
        text.push(partialVisibilityIcon(o));
      } else if (subjectType === 'person') {
        text.push(entryTypeIcon(o));
      }
      text.push(textFull(o, true));
      return text.join('');
    },
    textFullFormatter: function(o) {
      return textFull(o, true);
    },
    richTextWithTypeTruncateFormatter: function(o) {
      var subjectType = this.subjectType;

      var text = [];
      if (subjectType === 'group') {
        text.push(partialVisibilityIcon(o));
      } else if (subjectType === 'person') {
        text.push(entryTypeIcon(o));
      }

      text.push(textTruncate(o));

      return text.join('');
    },
    richTextTruncateFormatter: function(o) {
      return textTruncate(o);
    },
    richTextWithTypeFullFormatter: function(o) {
      var subjectType = this.subjectType;

      var text = [];
      if (subjectType === 'group') {
        text.push(partialVisibilityIcon(o));
      } else if (subjectType === 'person') {
        text.push(entryTypeIcon(o));
      }
      text.push(textFull(o));
      return text.join('');
    },
    richTextFullFormatter: function(o) {
      return textFull(o);
    },
    typeFormatter: function(o) {
      return entryTypeIcon(o);
    },
    statusFormatter: function(o) {
      return statusIndicator(o);
    },
    impactFormatter: function(o) {
      return impactIndicator(o);
    },
    _handleReorderChange: function(e) {
      var results = this.get('results'),
        columns = results.get('columns');

      results.placeRowHolder = {};

      if (e.newVal) {
        columns.splice(0, 0, {
          label: ' ',
          width: '2%',
          //initial width is necessary to support the reading view
          initialWidth: '2%',
          readingView: true
        });

        //insert a new column
        results.set('columns', columns);
        //plug in DD support to results table
        results.plug(Y.Plugin.usp.ResultsTableDDPlugin);
      } else {
        columns.splice(0, 1);

        results.set('columns', columns);
        //un-plug  DD 
        results.unplug(Y.Plugin.usp.ResultsTableDDPlugin);
      }
    },
    _handleOrderbyChange: function(e) {
      var results = this.get('results');
      switch (e.newVal) {
        case 'event-date':
          results.modifyColumn("actions", {
            className: CSS_COLUMN_ACTION_CLASS
          });
          results.modifyColumn("createdStamp!auditDate", {
            className: CSS_HIDDEN_CLASS
          });
          results.modifyColumn("lastEditedStamp!auditDate", {
            className: CSS_HIDDEN_CLASS
          });
          results.sort([{
            "eventDate!calculatedDate": "desc"
          }]);
          break;
        case 'recorded-date':
          results.modifyColumn("createdStamp!auditDate", {
            className: ""
          });
          results.modifyColumn("lastEditedStamp!auditDate", {
            className: CSS_HIDDEN_CLASS
          });
          results.modifyColumn("actions", {
            className: CSS_HIDDEN_CLASS
          });
          results.sort([{
            "createdStamp!auditDate": "desc"
          }]);
          break;
        case 'last-edited-date':
          results.modifyColumn("lastEditedStamp!auditDate", {
            className: ""
          });
          results.modifyColumn("createdStamp!auditDate", {
            className: CSS_HIDDEN_CLASS
          });
          results.modifyColumn("actions", {
            className: CSS_HIDDEN_CLASS
          });
          results.sort([{
            "lastEditedStamp!auditDate": "desc"
          }]);
          break;
      }
    },
    isMoveAllowed: function(sourceRecord, targetRecord) {
      if (!sourceRecord && !targetRecord) {
        return false;
      }
      // Obtain the allowedRange from server side.
      var eventDate = sourceRecord.get('eventDate'),
        upperBound = eventDate.allowedRange.upperBoundDate,
        lowerBound = eventDate.allowedRange.lowerBoundDate,
        unrestricted = eventDate.allowedRange.unrestricted,
        targetCalculatedDate = targetRecord.get('eventDate!calculatedDate'),
        retVal = false;

      // Handle the entry with NO DATE
      if (upperBound === null && lowerBound === null && unrestricted === true) {
        upperBound = Number.MAX_VALUE;
        lowerBound = Number.MIN_VALUE;
      }
      //Return true if both the target and the source are null (i.e. they've never been moved apart from either side of others with null dates)
      //OR
      //The target is not null and the target date is within the bounds of the source range.
      if ((targetCalculatedDate === null && sourceRecord.get('eventDate!calculatedDate') === null)) {
        retVal = true;
      } else {
        retVal = targetCalculatedDate !== null && lowerBound <= targetCalculatedDate && targetCalculatedDate <= upperBound;
      }
      return retVal;
    },
    handleDragEnter: function(e) {
      //Set the drop style
      var results = this.get('results'),
        data = results.get('data'),
        targetNode = e.drop.get('node'),
        targetRecord = results.getRecord(targetNode),
        sourceRecord = results.getRecord(e.drag.get('node')),
        sourceIdx, targetIdx, mod;

      //Remove any placeRow that has been left over
      if (results.placeRowHolder[targetNode.getAttribute('id')]) {
        results.placeRowHolder[targetNode.getAttribute('id')].remove(false);
      }
      if (this.isMoveAllowed(sourceRecord, targetRecord)) {
        //Because it is not possible to know if drag:enter or drag:exit will be called first
        //The placeRows will need to be unique and just one row cannot be used and moved.
        //Rather than creating a new node I'm using cloneNode which I think is _slightly_ better
        results.placeRowHolder[targetNode.getAttribute('id')] = results.placeRow.cloneNode(true);
        results.placeRowHolder[targetNode.getAttribute('id')].setAttribute('id', targetNode.getAttribute('id') + '_placeRow');
        sourceIdx = data.indexOf(sourceRecord);
        targetIdx = data.indexOf(targetRecord);
        mod = (results.getColumn("eventDate!calculatedDate").sortDir);
        //if the final source Id is greater than the target Id then item moved down
        if (((sourceIdx - targetIdx) * mod) < 0) {
          targetNode.insert(results.placeRowHolder[targetNode.getAttribute('id')], "before");
        } else {
          targetNode.insert(results.placeRowHolder[targetNode.getAttribute('id')], "after");
        }
        targetNode.removeClass('drop-invalid');
      } else {
        targetNode.addClass('drop-invalid');
      }
    },
    handleDragStart: function(e) {
      //Use Y.later to make this call async
      Y.later(1, this, function() {
        var results = this.get('results'),
          sourceRecord = results.getRecord(e.target.get('node'));

        this.get('results').placeRow = Y.Node.create('<tr id="temp-place" class="drop-here"><td colspan="10"><i class="fa fa-check-square"></i>&nbsp;Drop entry here</td></tr>');
        results.data.each(function(m) {
          if (!this.isMoveAllowed(sourceRecord, m)) {
            results.getRow(m).get('children').addClass('drop-outside');
          }
        }.bind(this));
      });
    },
    handleDragEnd: function(e) {
      //Use Y.later to make this call async
      Y.later(1, this, function() {
        var results = this.get('results');
        results.body._container.all('td.drop-outside').removeClass("drop-outside");
      });
    },
    handleDragExit: function(e) {
      //put the style back
      var node = e.drop.get('node'),
        results = this.get('results');

      node.removeClass('drop-invalid');

      if (results.placeRowHolder[node.getAttribute('id')]) {
        results.placeRowHolder[node.getAttribute('id')].remove(false);
      }
    },
    handleRowDropped: function(e) {
      var sourceRecord = e.sourceRecord,
        targetRecord = e.targetRecord,
        results = this.get('results');

      //show mask on datatable to prevent additional drag/drop
      results.showMask();

      if (!this.isMoveAllowed(sourceRecord, targetRecord)) {
        //prevent drop
        e.preventDefault();
        results.hideMask();
      }
    },
    handleAfterRowDropped: function(e) {
      var results = this.get('results'),
        data = results.get('data'),
        sourceRecord = e.sourceRecord,
        targetRecord = e.targetRecord,
        sourceIdx = data.indexOf(sourceRecord),
        targetIdx = data.indexOf(targetRecord),
        mod = (results.getColumn("eventDate!calculatedDate").sortDir),
        //if the final source Id is greater than the target Id then item moved down
        direction = ((sourceIdx - targetIdx) * mod) > 0 ? "AFTER_NEIGHBOUR" : "BEFORE_NEIGHBOUR",
        updateRowModel;


      //make an adhoc Model with sync layer to save the data
      updateRowModel = new RowUpdateModel({
        url: this.get('updateRowUrl'),
        id: sourceRecord.get('id'),
        neighbourId: targetRecord.get('id'),
        //set order based on current sort
        position: direction
      });

      updateRowModel.save(function(err, response) {
        if (err !== null) {
          alert("Unable to complete drag and drop");
          //reload the table
          results.reload();
        } else {
          results.hideMask();
        }
      });
    }
  }, {
    ATTRS: {
      resultsListPlugins: {
        valueFn: function() {
          return [{
            fn: Y.Plugin.usp.ResultsTableMenuPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
          }, {
            fn: Y.Plugin.usp.ResultsTableExpandableCellsPlugin
          }, {
            fn: Y.Plugin.app.ViewModePlugin
          }];
        }
      },
      reorder: {
        value: false
      },
      readingView: {
        getter: function() {
          return this.get('results').viewModePlugin.get('mode');
        },
        setter: function(v) {
          this.get('results').viewModePlugin.set('mode', v);
        }
      },
      orderby: {
        value: 'event-date'
      },
      updateRowUrl: {
        value: ''
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'results-table-expandable-cells-plugin',
    'results-table-DD-plugin',
    'results-formatters',
    'app-view-mode-plugin',
    'model',
    'model-sync-rest',
    'template-micro'
  ]
});