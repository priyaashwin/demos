YUI.add('entry-controller-view', function(Y) {
    'use-strict';

    var L = Y.Lang,
        O = Y.Object;

    Y.namespace('app.entry').BaseEntryControllerView = Y.Base.create('baseEntryControllerView', Y.View, [], {
        resultsListClass: undefined,
        dialogClass: undefined,
        dialogAddModelClass: undefined,
        dialogViewModelClass: undefined,
        dialogUpdateModelClass: undefined,
        dialogNotifyModelClass: undefined,
        linkFromType: '',
        _getConstructor: function(type) {
            return L.isString(type) ? O.getValue(Y, type.split('.')) : type;
        },
        initializer: function(config) {
            var subject = this.get('subject') || {},
                subjectType = subject.subjectType,
                orderByTrigger,
                impactEnabled=this.get('impactEnabled');

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            var resultsConfig = {
                searchConfig: Y.merge(config.searchConfig, {
                    //mix the subject into the search config so it is available when getting the URL
                    subject: config.subject,
                    updateRowUrl: config.updateRowUrl,
                    impactEnabled: impactEnabled
                }),
                filterConfig: Y.merge(config.filterConfig, {
                    //only person filter should show the group tab
                    showGroupFilter: subjectType === 'person',
                    impactEnabled: impactEnabled
                }),
                filterContextConfig: config.filterContextConfig,
                permissions: config.permissions
            };

            var ResultsConstructor = this._getConstructor(this.resultsListClass);
            this.results = new ResultsConstructor(resultsConfig).addTarget(this);

            var DialogConstructor = this._getConstructor(this.dialogClass);
            this.dialog = new DialogConstructor(config.dialogConfig).addTarget(this).render();

            //output only valid for PERSON subject
            if (subjectType === 'person') {
                //configure the dialog for output template - no need to add as a bubble target
                this.outputDocumentDialog = new Y.app.output.OutputDocumentDialog(config.outputDialogConfig, {
                    //mix the subject into the search config so it is available when getting the URL
                    subject: config.subject
                }).render();
            }
            //          
            //order-by select box
            orderByTrigger = Y.one(config.orderByCfg.triggerNode);

            //listener for link from checklist
            this.checklistLink = new Y.app.ChecklistLinkFrom({
                type: this.linkFromType
            });

            this._entryEvtHandlers = [
                 //toolbar add
                this.on('appToolbar:add', this.showAddDialog, this),
                //results actions
                this.on('*:view', this.showViewDialog, this),
                this.on('*:sendNotification', this.showNotificationDialog, this),
                this.on('*:hardDelete', this.showHardDeleteDialog, this),
                this.on('*:softDelete', this.showSoftDeleteDialog, this),
                this.on('*:complete', this.showCompleteDialog, this),
                this.on('*:uncomplete', this.showUncompleteDialog, this),
                //reload results on save
                this.on('*:saved', this.results.updateResults, this.results),
                //reading view
                this.on('*:readingView', this.showReadingView, this),
                this.after('*:readingViewChange', this._afterReadingViewChange, this),
                //reorder button
                this.on('*:reorder', this.reorderResults, this),
                this.on('*:reorderChange', this._onReorderChange, this),
                this.after('*:reorderChange', this._afterReorderChange, this),
                this.after('*:orderbyChange', this._afterOrderbyChange, this),
                //output
                this.on('*:output', this.showOutputDialog, this),
                //print
                this.on('*:print', this.openPrintView, this),

                this.checklistLink.on('checklistLinkFrom:linkFrom', this._handleLinkFromChecklist, this),

                this.dialog.on('casenote:dateChange', this.enableSaveButton, this),

                Y.on('deletion:removeSaved', function () {
                    //refresh the view after deletion
                    this.results.reload();
                }, this)
            ];
            if (orderByTrigger) {
                //attach delegate to order-by
                this._entryEvtHandlers.push(orderByTrigger.on('change', this._handleOrderByChange, this));
            }
        },
        destructor: function() {
            this._entryEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._entryEvtHandlers;

            this.dialog.removeTarget(this);
            this.dialog.destroy();
            delete this.dialog;

            this.results.removeTarget(this);
            this.results.destroy();
            delete this.results;

            if (this.outputDocumentDialog) {
                this.outputDocumentDialog.destroy();
                delete this.outputDocumentDialog;
            }

            this.checklistLink.destroy();
            delete this.checklistLink;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;

            if (this._messageNode) {
                this._messageNode.destroy();

                delete this._messageNode;
            }
        },
        render: function() {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            this._messageNode = Y.Node.create('<div></div>');

            contentNode.append(this._messageNode);

            contentNode.append(this.results.render().get('container'));

            //set state of reading button
            this._setReadingButton(this.results.get('results').get('readingView'));

            //set state of reorder button
            this._setReorderButton(this.results.get('results').get('reorder'));

            this.get('container').setHTML(contentNode);

            //check if we linked from a checklist
            this.checklistLink.checkForLinkFromChecklist();

            return this;
        },
        _handleOrderByChange: function(e) {
            var t = e.currentTarget,
                action = t.get('value');

            //set into results view
            this.results.get('results').set('orderby', action);
        },
        /**
         * Loads active group members given the group id
         */
        _loadActiveGroupMembers: function(id, url) {
            //active group members cached against instance
            if (this.activeGroupMembers && this.activeGroupMembers.id === id) {
                //resolve immediately
                return Y.when(this.activeGroupMembers.data);
            }
            return new Y.Promise(function(resolve, reject) {
                var model = new Y.usp.person.PaginatedPersonGroupMembershipDetailsList({
                    url: L.sub(url, {
                        id: id
                    })
                });
                model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    } else {
                        //cache the results
                        this.activeGroupMember = {
                            id: id,
                            data: model.toJSON()
                        };

                        //success, so resolve the promise
                        resolve(model.toJSON());
                    }
                });
            });
        },
        /**
         * Shows the ADD dialog with appropriate configuration
         */
        showAddDialog: function() {
            var subject = this.get('subject'),
                practitioner = this.get('practitioner'),
                permissions = this.get('permissions'),
                dialogConfig = this.get('dialogConfig'),
                isGroupEntry = subject.subjectType === 'group';

            if (permissions.canAdd) {
                //show the processing view while we load the model
                this.dialog.showView('processing');

                var promises = [];

                if (isGroupEntry) {
                    promises.push(this._loadActiveGroupMembers(subject.subjectId, dialogConfig.activeGroupMembersURL));
                }

                Y.Promise.all(promises).then(function(data) {
                    this._showAddDialogWithData(data[0]||[], subject, practitioner);
                }.bind(this)).then(null, function(err) {
                    var view = this.dialog.get('activeView');
                    if (view) {
                        view.fire('error', {
                            error: err
                        });
                    }
                }.bind(this));
            }
        },
        _showAddDialogWithData: function(activeGroupMembers, subject, practitioner) {
            var isGroupEntry = subject.subjectType === 'group',
                dialogConfig = this.get('dialogConfig'),
                config=dialogConfig.views.add.config,
                validationLabels = dialogConfig.views.add.validationLabels,
                impactEnabled = this.get('impactEnabled'),
                notificationEnabled=this.get('notificationEnabled');

            var ModelConstructor = this._getConstructor(this.dialogAddModelClass);

            this.dialog.showView('add', {
                isGroup: isGroupEntry,
                impactEnabled:impactEnabled,
                notificationEnabled:notificationEnabled,
                model: new ModelConstructor({
                    url: L.sub(config.url, {
                        subjectId: subject.subjectId,
                        subjectType: subject.subjectType
                    }),
                    //default visibility to all active group members
                    personVisibility:[],
                    visibleToAll:true,
                    practitionerOrganisation: practitioner.organisationName,
                    practitioner: practitioner.personName,
                    practitionerId: practitioner.personId,
                    //labels to allow access to validation messages
                    //labels for the view are automatically provided
                    labels: validationLabels
                }),
                otherData: {
                    subject: subject,
                    activeGroupMembersList: activeGroupMembers
                }
            }, function() {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                this.getButton('completeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        _loadEntry: function(id, url) {
            var subjectType = this.get('subject').subjectType;
            var ModelConstructor = this._getConstructor(this.dialogViewModelClass);

            return new Y.Promise(function(resolve, reject) {
                var model = new ModelConstructor({
                    url: L.sub(url, {
                        subjectType: subjectType
                    }),
                    id: id
                });
                model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(model);
                    }
                });
            });
        },        
        showViewDialog: function(e) {
            this._showViewDialog(e.record||e.id);
        },
        _showViewDialog: function(record, defaultTab) {
            var dialogConfig = this.get('dialogConfig'),
                config = dialogConfig.views.view.config,
                permissions = this.get('permissions'),
                id;

            if (permissions.canView) {
              //show the processing view while we load the model
              this.dialog.showView('processing');

              if(L.isNumber(record)){
                //record is an ID
                id=record;
              }else{
                id=record.get('id');
              }

              var promises=[];
              //load the entry
              this._loadEntry(id, config.url).then(function(entry){
                var subject = this._getSubjectFromRecord(entry);
                
                if (subject.subjectType === 'group') {
                  promises.push(this._loadActiveGroupMembers(subject.subjectId, dialogConfig.activeGroupMembersURL));
                }
                Y.Promise.all(promises).then(function(data) {
                  this._showViewDialogWithData(entry, data[0] || [], defaultTab, permissions);
                }.bind(this));
              }.bind(this)).then(null, function(err) {
                  var view = this.dialog.get('activeView');
                  if (view) {
                      view.fire('error', {
                          error: err
                      });
                  }
              }.bind(this));
                
            }
        },
        _showViewDialogWithData: function(record, activeGroupMembers, defaultTab, permissions) {
              var dialogConfig = this.get('dialogConfig'),
                status = record.get('status'),
                subject = this._getSubjectFromRecord(record),
                isGroupEntry = subject.subjectType === 'group',
                impactEnabled = this.get('impactEnabled'),
                validationLabels,
                _self=this;

            var isEdit = false;

            var canWrite = record.hasAccessLevel('WRITE');
            if (permissions.canUpdate && status !== 'DELETED' && canWrite) {
                //default to edit
                isEdit = true;
                if(status==='COMPLETE'){
                  validationLabels = dialogConfig.views.editComplete.validationLabels;
                }else{
                  validationLabels = dialogConfig.views.edit.validationLabels;
                }
                
                record.labels=validationLabels;
            }
            // A 'view'WITHOUT edit permission will show the view dialog 
            // (or if the status = 'DELETED')
            this.dialog.showView(isEdit ? (status === 'COMPLETE' ? 'editComplete' : 'edit') : 'view', {
                isGroup: isGroupEntry,
                impactEnabled:impactEnabled,
                model: record,
                otherData: {
                    subject: subject,
                    defaultTab: defaultTab,
                    activeGroupMembersList: activeGroupMembers
                }
            }, function() {
                if (isEdit && status !== 'COMPLETE') {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                }
                _self.afterShowViewDialog(record, permissions);
            });
        },
        enableSaveButton: function() {
            this.dialog.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        },
        afterShowViewDialog: function(record, permissions) {
        },
        showNotificationDialog:function(e) {
        	var	record = e.record,
        		subject = this.get('subject'),
        		dialogConfig = this.get('dialogConfig')
        		config = dialogConfig.views.sendNotification.config,
        		notificationEnabled = this.get('notificationEnabled');
        	
            var ModelConstructor = this._getConstructor(this.dialogNotifyModelClass);

        	this.dialog.showView('sendNotification', {
        		notificationEnabled:notificationEnabled,
        		model: new ModelConstructor({
        			url:config.url,
        			id:record.get('id')
        		}),
        		otherData:{
        			subjectId:subject.subjectId,
        			subject:subject
        		},
        	},
        	function(view){
    			this.getButton('sendNotificationButton', Y.WidgetStdMod.FOOTER).set('disabled', false)
        	});        	
        },        
        showHardDeleteDialog: function(e) {
            var permissions = e.permissions,
                config = this.get('dialogConfig').views.hardDelete.config,
                record = e.record,
                subject = this._getSubjectFromRecord(record);

            //set the url into the record - so we can delete it
            record.url = config.url;

            if (permissions.canRemove) {
                this.dialog.showView('hardDelete', {
                    model: record,
                    otherData: {
                        subject: subject,
                    }
                }, function() {
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //remove the cancel button
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        showSoftDeleteDialog: function(e) {
            var permissions = e.permissions,
                config = this.get('dialogConfig').views.softDelete.config,
                record = e.record,
                subject = this._getSubjectFromRecord(record);

            //set the url into the record - so we can delete it
            record.url = config.url;

            if (permissions.canDelete) {
                this.dialog.showView('softDelete', {
                    model: record,
                    otherData: {
                        subject: subject,
                    }
                }, function() {
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //remove the cancel button
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        showCompleteDialog: function(e) {
            var permissions = e.permissions,
                config = this.get('dialogConfig').views.complete.config,
                record = e.record,
                subject = this._getSubjectFromRecord(record);

            var ModelConstructor = this._getConstructor(this.dialogUpdateModelClass);

            if (permissions.canComplete) {
                this.dialog.showView('complete', {
                    model: new ModelConstructor({
                        url: config.url,
                        id: record.get('id')
                    }),
                    otherData: {
                        subject: subject,
                    }
                }, {
                    modelLoad: true
                }, function(view) {
                    //update the URL to be the complete URL
                    view.get('model').url = config.completeURL;

                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //remove the cancel button
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        showUncompleteDialog: function(e) {
            var permissions = e.permissions,
                config = this.get('dialogConfig').views.uncomplete.config,
                record = e.record,
                subject = this._getSubjectFromRecord(record);

            var ModelConstructor = this._getConstructor(this.dialogUpdateModelClass);

            if (permissions.canUncomplete) {
                this.dialog.showView('uncomplete', {
                    model: new ModelConstructor({
                        url: config.url,
                        id: record.get('id')
                    }),
                    otherData: {
                        subject: subject,
                    }
                }, {
                    modelLoad: true
                }, function(view) {
                    //update the URL to be the complete URL
                    view.get('model').url = config.uncompleteURL;

                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    //remove the cancel button
                    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
                });
            }
        },
        showReadingView: function() {
            var state = this.results.get('results').get('readingView');
            
            if (state === 'reading') {
                state = 'initial';
            } else {
                state = 'reading';
            }
            this.results.get('results').set('readingView', state);
        },
        _afterReadingViewChange: function(e) {
            this._setReadingButton(e.newVal);
        },
        _setReadingButton: function(mode) {
            var readingCfg = this.get('readingCfg'),
                button = Y.one(readingCfg.triggerNode),
                active;

            switch (mode) {
                case 'reading':
                    active = false;
                    break;
                default:
                    active = true;
            }

            this._setButton(button, readingCfg, active);

        },
        reorderResults: function() {
            this.results.get('results').set('reorder', !this.results.get('results').get('reorder'));
        },
        _onReorderChange: function(e) {
            //filter is disabled when re-ordering 
            if (e.newVal) {
                this.results.get('filter').disableFilter();
            } else {
                this.results.get('filter').enableFilter();
            }
        },
        _afterReorderChange: function(e) {
            this._setReorderButton(e.newVal);
        },
        _setReorderButton: function(reorder) {
            var reorderCfg = this.get('reorderCfg'),
                button = Y.one(reorderCfg.triggerNode),
                active = false;

            if (reorder === true) {
                active = true;
            }

            this._setButton(button, reorderCfg, active);
        },
        _afterOrderbyChange: function(e) {
            var addEnabled = true,
                reorderEnabled = true,
                addButton,
                reorderButton;

            switch (e.newVal) {
                case 'recorded-date':
                    /* falls through */
                case 'last-edited-date':
                    addEnabled = false;
                    reorderEnabled = false;
                    break;
            }

            addButton = this.toolbar.get('toolbarNode').one('a[data-button-action="add"]');
            reorderButton = this.toolbar.get('toolbarNode').one('a[data-button-action="reorder"]');

            if (!reorderEnabled) {
                //ensure reorder not in progress by setting the property on the results
                this.results.get('results').set('reorder', false);
            }

            //update disabled attribute - do this last
            this._setButton(addButton, null, false, !addEnabled);
            this._setButton(reorderButton, this.get('reorderCfg'), false, !reorderEnabled);

        },
        _setButton: function(button, cfg, active, disabled) {
            if (button) {
                if (cfg) {
                    if (cfg.inactive.className) {
                        if (active && !disabled) {
                            button.removeClass(cfg.inactive.className);
                        } else {
                            button.addClass(cfg.inactive.className);
                        }
                    }
                    if (cfg.active.className) {
                        if (active && !disabled) {
                            button.addClass(cfg.active.className);
                        } else {
                            button.removeClass(cfg.active.className);
                        }
                    }
                }
                if (disabled) {
                    button.setAttribute('disabled', 'disabled');
                } else {
                    button.removeAttribute('disabled');

                    if (cfg) {
                        if (cfg.active.title || cfg.inactive.title) {
                            var title = (active) ? (cfg.active.title || '') : (cfg.inactive.title || '');
                            button.setAttribute('aria-label', title).setAttribute('title', title);
                        }

                        if (cfg.active.label || cfg.inactive.lable) {
                            var label = (active) ? (cfg.active.label || '') : (cfg.inactive.label || '');
                            button.one('span').setHTML(label);
                        }
                        if (cfg.active.icon || cfg.inactive.icon) {
                            var icon = (active) ? (cfg.active.icon || '') : (cfg.inactive.icon || '');
                            button.one('i').setAttribute('class', icon);
                        }
                    }
                }
            }
        },
        /**
         * Constructs a subject object from the record details
         */
        _getSubjectFromRecord: function(record) {
            return {
                subjectType: record.get('subjectIdName.type').toLowerCase(),
                subjectName: record.get('subjectIdName.name'),
                subjectIdentifier: record.get('subjectIdName.identifier'),
                subjectId: record.get('subjectIdName.id'),
            };
        },

        _handleLinkFromChecklist: function(e) {
            var id = e.targetEntityId,
                parameters = e.parameters || {},
                labels = this.get('labels');
                
            if (e.action === 'VIEW') {
              if (id !== undefined) {
                this.fire('view', {
                    id: id,
                    permissions: this.get('permissions')
                });
              }
            }
            
            
            if (e.taskState !== 'COMPLETE') {
              //show a message
              new Y.usp.DismissibleMessage({
                  message: L.sub(labels.checklistLinkFrom, this.buildChecklistMessageParameters(parameters))
              }).render(this._messageNode);
            }
        },
        buildChecklistMessageParameters:function(parameters){
          return {};
        },
        showOutputDialog: function(e) {
            var results = this.results.get('filter');
            var filter = results.get('filterModel');
        	if (this.get('subject').subjectType === 'person') {
                this.outputDocumentDialog.showView('produceDocument', {
                    //pass the dialog so we can reposition it after the render
                    dialog: this.outputDocumentDialog,
                    model: new Y.Model(this.get('subject')),
                    filterModel: filter
                });
            }
        },

        openPrintView: function(e) {
            var url, ref,
                subject = this.get('subject'),
                results = this.results.get('results').get('results'),
                filter = results.get('filterModel');

            try {
                // If the print button is clicked save sortBy in localStorage, this will be picked up by the new tab/window that opens       
                localStorage.setItem('sortBy', Y.JSON.stringify(results.get("sortBy")));
                localStorage.setItem('viewMode', results.viewModePlugin.get('mode'));
                localStorage.setItem('filterData', Y.JSON.stringify(filter.toJSON()));
            } catch (ignore) {}

            url = L.sub(this.get('printURL'), {
                id: subject.subjectId,
                contextType: subject.subjectType
            });

            ref = window.open(url, 'print', 'toolbar=1,scrollbars=1,location=1,statusbar=1,menubar=1,resizable=1,width=1024,height=768,left=100,top=100');

            if (ref) {
                //attempt to move to front
                ref.focus();
            }
            return;
        }
    }, {
        ATTRS: {
            /**
             * An object representing the subject
             * 
             * person or group
             * 
             * @attribute subject
             */
            subject: {
                value: {
                    subjectType: null,
                    subjectName: null,
                    subjectIdentifier: null,
                    subjectId: null
                }
            },
            reading: {

            },
            orderBy: {

            },
            practitioner: {

            },
            printURL: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'model',
        'app-toolbar',
        'checklist-link-from',
        'promise',
        'usp-person-PersonGroupMembershipDetails',
        'output-document-dialog-views',
        'json-stringify',
        'dismissible-message'
    ]
});
