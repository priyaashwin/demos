YUI.add('settings-coded-entry-view', function(Y) {
  Y.namespace('app').SettingsCodedEntryTabsView = Y.Base.create(
      'settingsCodedEntryTabsView', Y.usp.app.AppTabbedPage, [], {
        views : {
          caseNoteEntryType : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          chronologyEntryType : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          personReferenceType : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          personProfessionalTitle : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          personWarnings : {
            type : Y.app.settings.PersonWarningsAppCodedEntryAdminView
          },
          personAlerts : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          checklistPauseReason : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          trainingCourseCode : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          typeOfCare : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          reasonForUnavailability : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          outputCategory : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          sourceOrganisation : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          nonVerbalLanguages : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          belongingsType : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          belongingsLocation : {
            type : Y.usp.app.AppCodedEntryAdminView
          },
          classificationEndReason : {
              type : Y.usp.app.AppCodedEntryAdminView
            },
          typeOfDisability : {
        	  type : Y.usp.app.AppCodedEntryAdminView
          }
        }
      });

}, '0.0.1', {
  requires : [ 'yui-base', 'app-tabbed-page', 'app-coded-entry-admin-view', 'person-warnings-coded-entry-view' ]
});
