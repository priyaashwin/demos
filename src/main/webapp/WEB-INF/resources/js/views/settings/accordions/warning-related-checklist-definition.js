YUI.add('warning-related-checklist-definition-accordion-view', function(Y) {
    'use-strict';
    
	var E = Y.Escape,
	L = Y.Lang;

    Y.namespace('app.settings').EditWarningRelatedChecklistDefinitionForm = Y.Base.create('editWarningRelatedChecklistDefinitionForm', Y.usp.checklist.NewWarningRelatedChecklistDefinition, [Y.usp.ModelFormLink], {
        form: '#editWarningRelatedChecklistDefinitionDialog'
    });
	
    Y.namespace('app.settings').EditWarningRelatedChecklistDefinitionView = Y.Base.create('EditWarningRelatedChecklistDefinitionView', Y.usp.checklist.ChecklistDefinitionView, [], {
        //bind template
        template: Y.Handlebars.templates["editWarningRelatedChecklistDefinitionDialog"],
        initializer: function() {
            this.checklistDefinitionAutocompleteView = new Y.app.checklist.ChecklistDefinitionAutoCompleteResultView({
                autocompleteURL: this.get('config').dialogConfig.views.edit.checklistDefinitionAutocompleteURL,
                inputName: 'name',
                formName: 'editFormDefinitionForm',
                labels: {
                  placeholder: this.get('config').dialogConfig.views.edit.labels.placeholder
                }
            });
        },
        render: function() {
            var container = this.get('container'),
                _self=this;
            Y.app.settings.EditWarningRelatedChecklistDefinitionView.superclass.render.call(this);
  
            if (this.checklistDefinitionAutocompleteView) {
                container.one('#checklistDefinitionIdAutocomplete').append(this.checklistDefinitionAutocompleteView.render().get('container'));
                
                var relatedChecklistDefinition = {
                        definitionId: _self.get('otherData').definitionId,
                        name: _self.get('otherData').name
                      };
                if(relatedChecklistDefinition.definitionId && relatedChecklistDefinition.name) {
                	this.checklistDefinitionAutocompleteView.set('selectedChecklistDefinition', relatedChecklistDefinition);
                }
            }
            
        }
    });
	
    Y.namespace('app.settings').WarningRelatedChecklistDefinitionDialog= Y.Base.create('warningRelatedChecklistDefinitionDialog', Y.usp.app.AppDialog, [], {
        views:{
             edit:{
                 type:Y.app.settings.EditWarningRelatedChecklistDefinitionView,
                 buttons: [{
                     name: 'saveButton',
                     labelHTML: '<i class="fa fa-check"></i> Save',
                     action: function(e) {

                         var view = this.get('activeView'),
                         selected = view.checklistDefinitionAutocompleteView.get('selectedChecklistDefinition'),
                         newId = selected ? selected.definitionId : '', 
                         newName = selected ? selected.name : '';
                         
                         view.get('model').set('definitionId', newId);
                         this.handleSave(e);
                         
                         if(newId && newName) {
                             Y.fire('checklistAccordion:updateDefinition', {newName: newName, newId: newId});
                         }
                     },
                     disabled: true
                 }]
             }
         }
    });
	
    Y.namespace('app.settings').WarningRelatedChecklistDefinitionView = Y.Base.create('WarningRelatedChecklistDefinitionView', Y.usp.View, [], {
        template: Y.Handlebars.templates["warningRelatedChecklistDefinitionView"],
    });
    
    Y.namespace('app.settings').WarningRelatedChecklistDefinitionAccordionView = Y.Base.create('WarningRelatedChecklistDefinitionAccordionView', Y.usp.AccordionPanel, [], {
        initializer: function() {
         this.warningRelatedChecklistDefinitionView = new Y.app.settings.WarningRelatedChecklistDefinitionView(Y.merge(this.get('warningRelatedChecklistDefinitionConfig')));
         this.warningRelatedChecklistDefinitionView.addTarget(this);

         this.dialog = new Y.app.settings.WarningRelatedChecklistDefinitionDialog(this.get('warningRelatedChecklistDefinitionConfig').dialogConfig).addTarget(this).render();
         
         this._evtHandlers = [
                              Y.delegate('click', this.editChecklist, '#buttonContainer', 'button', this),
                              Y.on('checklistAccordion:updateDefinition', this.handleDefinitionChange, this)
                              ];
        },
    	render: function() {
            Y.app.settings.WarningRelatedChecklistDefinitionAccordionView.superclass.render.call(this);
            this.getAccordionBody().appendChild(this.warningRelatedChecklistDefinitionView.render().get('container'));
            
            this.checklistDefinition= new Y.usp.checklist.ChecklistDefinition({
                url: this.get('warningRelatedChecklistDefinitionConfig').dialogConfig.views.edit.getUrl
              });
            
            this.checklistDefinition.load();
            this.checklistDefinition.after('load', 
                function(e) {
                   Y.fire('checklistAccordion:updateDefinition', {newName: this.checklistDefinition.get('name'), newId: this.checklistDefinition.get('definitionId')});
                }, this);
            
            return this;
        },
        destructor: function() {
            this._evtHandlers.forEach(function(handler) {
                handler.detach();
            });
        },
        editChecklist: function(e) {
            this.dialog.showView('edit', {
                model: new Y.app.settings.EditWarningRelatedChecklistDefinitionForm({
                    url: this.get('warningRelatedChecklistDefinitionConfig').dialogConfig.views.edit.getUrl
                }),
                labels: this.get('warningRelatedChecklistDefinitionConfig').dialogConfig.views.edit.labels,
                config: this.get('warningRelatedChecklistDefinitionConfig'),
                otherData: {
                    definitionId: this.get('definitionId'),
                    name: this.get('definitionName')
                }
            }, function() {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
	    },
	    handleDefinitionChange: function(e) {
	        this.get('container').one('#warningRelatedChecklistDefinitionId').setHTML(E.html(e.newName));
            this.set('definitionId', e.newId);
            this.set('definitionName', e.newName);
	    }
        
    },{
        ATTRS:{
            definitionId: '',
            definitionName: ''
        }
    });

    
    
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-dialog',
        'accordion-panel',
        'usp-checklist-ChecklistDefinition',
        'usp-checklist-NewWarningRelatedChecklistDefinition',
        'handlebars-settings-templates',
        'checklist-definition-autocomplete-view'
    ]
});