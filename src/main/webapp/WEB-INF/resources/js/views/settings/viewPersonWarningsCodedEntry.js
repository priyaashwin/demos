YUI.add('person-warnings-coded-entry-view', function(Y) {
	Y.namespace('app.settings').PersonWarningsAppCodedEntryAdminView = Y.Base.create('personWarningsAppCodedEntryAdminView', Y.usp.app.AppCodedEntryAdminView, [], {
	 initializer: function(config) {
	   this.warningRelatedChecklistDefinitionAccordion = new Y.app.settings.WarningRelatedChecklistDefinitionAccordionView(Y.merge(config, config.warningRelatedChecklistDefinitionConfig)).addTarget(this);
	 },
	 render: function() {
		var contentNode = Y.one(Y.config.doc.createDocumentFragment());
		Y.app.settings.PersonWarningsAppCodedEntryAdminView.superclass.render.call(this);
		
        contentNode.append(this.warningRelatedChecklistDefinitionAccordion.render().get('container'));

        this.get('container').append(contentNode);
        return this;
	 },
	 destructor: function() {
	   this.warningRelatedChecklistDefinitionAccordion.removeTarget(this);
       this.warningRelatedChecklistDefinitionAccordion.destroy();
       delete this.warningRelatedChecklistDefinitionAccordion;
	 }
	});
}, '0.0.1', {
    requires: [
               'yui-base',
               'event-custom',
               'view',
               'usp-view',
               'app-coded-entry-admin-view',
               'warning-related-checklist-definition-accordion-view'
               ]
       });