YUI.add('holiday-results', function(Y) {
  var USPFormatters = Y.usp.ColumnFormatters,
    USPTemplates = Y.usp.ColumnTemplates,
    USPColumnFormatters = Y.app.ColumnFormatters;

  Y.namespace('app.admin.calendar').HolidayResults = Y.Base.create('holidayResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      return new Y.usp.calendar.PaginatedCalendarEventBundleList({
        url: config.url
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;
      return ([{
          key: 'name',
          label: labels.name,
          width: '40%',
          cellTemplate: USPTemplates.clickable(labels.viewHoliday, 'view', permissions.canView)
        },
        {
          key: 'description',
          label: labels.description,
          width: '30%'
        },
        {
          key: 'status',
          label: labels.status,
          width: '20%',
          allowHTML: true,
          formatter: USPColumnFormatters.calendarState,
        }, {
          label: labels.actions,
          className: 'pure-table-actions',
          width: '10%',
          formatter: USPFormatters.actions,
          items: [{
              clazz: 'view',
              title: labels.viewHolidayTitle,
              label: labels.viewHoliday,
              visible: permissions.canView
            },
            {
              clazz: 'update',
              title: labels.editHolidayTitle,
              label: labels.editHoliday,
              visible: permissions.canUpdate
            },
            {
              clazz: 'activate',
              title: labels.activateHolidayTitle,
              label: labels.activateHoliday,
              visible: permissions.canActivate,
              enabled: function() {
                return (this.record.get('status') === 'IN_ACTIVE');
              }
            },
            {
              clazz: 'delete',
              title: labels.deleteHolidayTitle,
              label: labels.deleteHoliday,
              visible: permissions.canDelete
            }
          ]
        }
      ]);
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'results-templates',
    'results-formatters',
    'caserecording-results-formatters',
    'usp-calendar-CalendarEventBundle'
  ]
});