YUI.add('calendar-days-search-view', function(Y) {
	var L = Y.Lang,
	  A = Y.Array,
      USPFormatters = Y.usp.ColumnFormatters;
	
    var prototypePropsAndMethods = {
      initializer: function() {
        var searchConfig = this.get('searchConfig'),
          permissions = searchConfig.permissions,
          labels = searchConfig.labels,
		  calendarDaysDialogConfig = this.get('calendarDaysDialogConfig');

        this.results = new Y.usp.PaginatedResultsTable({
          sortBy: [{day: 'asc'}],
          columns: [
            {
            	key: 'day',
            	label: labels.day,
            	width: '40%',
            	formatter: USPFormatters.enumType,
            	enumTypes: Y.usp.calendar.enum.DayOfWeek.values
            },
            {
            	key: 'startTime',
            	label: labels.startTime,
            	width: '20%',
            	formatter: USPFormatters.date,
            	maskField: "%H:%M"
            },
            {
            	key: 'endTime',
            	label: labels.endTime,
            	width: '20%',
            	formatter: USPFormatters.date,
            	maskField: "%H:%M"
            },
			{
				label: labels.actions,
				className: 'pure-table-actions',
				width: '10%',
				formatter: USPFormatters.actions,
				items: [
				        {
				        	clazz: 'editCalendarWorkingDay',
				        	title: labels.editCalendarDayTitle,
				        	label: labels.editCalendarDay,
				        	enabled: permissions.canUpdate
				        },
				        {
				        	clazz: 'deleteCalendarWorkingDay',
				        	title: labels.deleteCalendarDayTitle,
				        	label: labels.deleteCalendarDay,
				        	enabled: permissions.canDelete
				        }
				]
			}
          ],
          data: new Y.usp.calendar.PaginatedWorkingDayList({
			url: L.sub(searchConfig.url, {
				calendarId: searchConfig.calendar.id
			})
          }),
          noDataMessage: searchConfig.noDataMessage,
          plugins: [
                    { fn: Y.Plugin.usp.ResultsTableMenuPlugin },
                    { fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin }
                   ]
        });
        
        this._tablePanel = new Y.usp.TablePanel({
			tableId: this.name
        }); 
        
	    this.calendarDaysDialog = new Y.app.CalendarDaysDialog({
		  headerContent: calendarDaysDialogConfig.defaultHeader,            
		  views: {
		    addCalendarWorkingDay: {                   
			  headerTemplate: calendarDaysDialogConfig.addCalendarDaysConfig.headerTemplate,
			  successMessage: calendarDaysDialogConfig.addCalendarDaysConfig.successMessage
		    },
		    editCalendarWorkingDay: {
			  headerTemplate: calendarDaysDialogConfig.editCalendarDaysConfig.headerTemplate,
			  successMessage: calendarDaysDialogConfig.editCalendarDaysConfig.successMessage
		    },
		    deleteCalendarWorkingDay: {
			  headerTemplate: calendarDaysDialogConfig.deleteCalendarDaysConfig.headerTemplate,
			  successMessage: calendarDaysDialogConfig.deleteCalendarDaysConfig.successMessage
		    }
		  }
	    }).render();
	    
	    this._evtHandlers = [
            this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this),
	        this.calendarDaysDialog.on('*:saved', function(e) {
	        	this.results.reload();
	        }, this)
		];
	    
	    Y.on('calendarDays:tabActive', function(e) {
	    	this.results.reload();
	    }, this);
	    
		Y.on('calendarDays:showDialog', function(e) {
			switch(e.action){
			  case 'add':
				calendarDaysDialogConfig.addCalendarDaysConfig.labels.narrative = L.sub(calendarDaysDialogConfig.addCalendarDaysConfig.labels.narrative, {calendar: searchConfig.calendar.name});
				this.showView('addCalendarWorkingDay', {
					labels: calendarDaysDialogConfig.addCalendarDaysConfig.labels,
					model: new Y.app.NewCalendarDaysForm({
						url: L.sub(calendarDaysDialogConfig.addCalendarDaysConfig.url, {id: searchConfig.calendar.id})
					})
				  }, function(view){
					this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                      
				  });
			   break;
			  default:
			}
		}, this.calendarDaysDialog);
      },

      destructor: function() {
		  if (this._evtHandlers) {
			  A.each(this._evtHandlers, function(item) {
				  item.detach();
			  });
			  //null out
			  this._evtHandlers = null;
		  }
		  
    	  if (this.results) {
			  this.results.destroy();
			  delete this.results;
    	  }
        
		  if (this._tablePanel) {
			  this._tablePanel.destroy();
			  delete this._tablePanel;
		  }
		  
		  if (this.calendarDaysDialog) {
			  this.calendarDaysDialog.destroy();
			  delete this.calendarDaysDialog;
		  }
      },

      render: function() {
        var container = this.get('container'),
          searchConfig = this.get('searchConfig'),
          contentNode = Y.one(Y.config.doc.createDocumentFragment());
        
		if(searchConfig.resultsCountNode){
			this._tablePanel.set('resultsCount', searchConfig.resultsCountNode);
		}
		
		if(searchConfig.titleNode && searchConfig.calendar.name !== "null"){
			searchConfig.titleNode.set('text', searchConfig.calendar.name);
		}
		
		contentNode.append(this._tablePanel.render().get('container'));
		this._tablePanel.renderResults(this.results);
		container.setHTML(contentNode);
 
        return this;
      },
      
      _handleRowClick: function(e) {
    	  var target = e.currentTarget,
    	  	calendarName = this.get('searchConfig').calendar.name,
    	    record = this.results.getRecord(target.get('id')),
    	    workingDayId = record.get('id'),
    	    workingDayDay = record.get('day'),
		    calendarDaysEditDialogConfig = this.get('calendarDaysDialogConfig').editCalendarDaysConfig,
		    calendarDaysDeleteDialogConfig = this.get('calendarDaysDialogConfig').deleteCalendarDaysConfig;
    	  
    	  if (target.hasClass('editCalendarWorkingDay')) {
			  calendarDaysEditDialogConfig.labels.narrative = L.sub(calendarDaysEditDialogConfig.labels.narrative, {calendar: calendarName});
			  this.calendarDaysDialog.showView('editCalendarWorkingDay', {
				  labels: calendarDaysEditDialogConfig.labels,
				  model: new Y.app.UpdateCalendarDaysForm({
					  url: L.sub(calendarDaysEditDialogConfig.url, {id: workingDayId})
				  }),
				  otherData: {
					  calendar: {
						  id: this.get('searchConfig').calendar.id,
						  name: calendarName
					  }
				  }
			  },
			  { modelLoad: true, payload: {id: workingDayId} },
			  function(view) {
				  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			  });
    	  } else if (target.hasClass('deleteCalendarWorkingDay')) {
    		 this.calendarDaysDialog.showView('deleteCalendarWorkingDay', {
    			 labels: {
    				 narrative: L.sub(calendarDaysDeleteDialogConfig.labels.narrative, {workingDay: workingDayDay, calendar: calendarName})
    			 },
    			 model: new Y.app.DeleteCalendarDaysForm({
    				 url: L.sub(calendarDaysDeleteDialogConfig.url, {id: workingDayId})
    			 })
    		 },
    		 function(view) {
				  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
    		 }); 
    	  }
      }
    };
    
    var staticPropsAndMethods = {
      ATTRS: {
        searchConfig: {
        }
      }
    };

    var calendarDaysSearchView = Y.Base.create(
      'calendarDaysSearchView',
      Y.View,
      [],
      prototypePropsAndMethods,
      staticPropsAndMethods
    );
    
    Y.namespace('app').CalendarDaysSearchView = calendarDaysSearchView;
}, '0.0.1', {
  requires: [
    'yui-base',
    'view',
    'accordion-panel',
    'paginated-results-table',
	'calendar-days-dialog-views',
	'results-templates',
	'results-formatters',
	'results-table-menu-plugin',
    'results-table-keyboard-nav-plugin',
	'caserecording-results-formatters',
    'usp-calendar-Calendar',
    'usp-calendar-WorkingDay'
  ]
});