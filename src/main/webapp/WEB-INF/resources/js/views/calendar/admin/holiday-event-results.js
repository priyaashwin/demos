YUI.add('holiday-event-results', function(Y) {
  var L = Y.Lang,
    USPFormatters = Y.usp.ColumnFormatters;

  Y.namespace('app.admin.calendar').HolidayEventResults = Y.Base.create('holidayEventResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      var holiday = config.holiday;

      return new Y.usp.calendar.PaginatedCalendarEventList({
        url: L.sub(config.url, {
          holidayId: holiday.id
        })
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;
      return ([{
        key: 'name',
        label: labels.name,
        width: '30%',
        sortable: true
      }, {
        key: 'description',
        label: labels.description,
        width: '40%'
      }, {
        key: 'startDate',
        label: labels.date,
        width: '20%',
        formatter: USPFormatters.date,
        sortable: true
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '10%',
        formatter: USPFormatters.actions,
        items: [{
          clazz: 'update',
          title: labels.editHolidayEventTitle,
          label: labels.editHolidayEvent,
          visible: permissions.canUpdate
        }, {
          clazz: 'delete',
          title: labels.deleteHolidayEventTitle,
          label: labels.deleteHolidayEvent,
          visible: permissions.canDelete
        }]
      }]);
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'results-formatters',
    'usp-calendar-CalendarEvent'
  ]
});