YUI.add('holiday-controller', function(Y) {
  'use-strict';

  var L = Y.Lang;

  Y.namespace('app.admin.calendar').HolidayControllerView = Y.Base.create('holidayControllerView', Y.View, [], {
    initializer: function(config) {
      this.toolbar = new Y.usp.app.AppToolbar({
        permissions: config.permissions
      }).addTarget(this);

      var resultsConfig = {
        searchConfig: config.searchConfig,
        permissions: config.permissions,
      };

      this.holidayResults = new Y.app.admin.calendar.HolidayResults(resultsConfig).addTarget(this);

      this.holidayDialog = new Y.app.admin.calendar.HolidayDialog(config.dialogConfig).addTarget(this).render();

      this._holidayEvtHandlers = [
        this.on('*:add', this.showAddDialog, this),
        this.on('*:view', this.showViewCalendarEventBundle, this),
        this.on('*:viewCalendar', this.showViewCalendar, this),
        this.on('*:update', this.showUpdateDialog, this),
        this.on('*:activate', this.showActivateDialog, this),
        this.on('*:delete', this.showDeleteDialog, this),
        this.on('*:saved', this.holidayResults.reload, this.holidayResults)
      ];
    },
    destructor: function() {
      this._holidayEvtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._holidayEvtHandlers;

      this.holidayResults.removeTarget(this);
      this.holidayResults.destroy();
      delete this.holidayResults;

      this.holidayDialog.removeTarget(this);
      this.holidayDialog.destroy();
      delete this.holidayDialog;

      this.toolbar.removeTarget(this);
      this.toolbar.destroy();
      delete this.toolbar;
    },
    render: function() {
      var contentNode = Y.one(Y.config.doc.createDocumentFragment());

      //append the results
      contentNode.append(this.holidayResults.render().get('container'));

      //set fragment into container
      this.get('container').setHTML(contentNode);

      return this;
    },
    showViewCalendarEventBundle: function(e) {
      var permissions = this.get('permissions'),
        record = e.record,
        url = this.get('viewURL');

      if (permissions.canView === true) {
        window.location = L.sub(url, {
          id: record.get('id')
        });
      }
    },
    showViewCalendar: function(e) {
      var permissions = this.get('permissions')
        url = this.get('viewCalendarURL');

      if (permissions.canViewCalendar === true) {
        window.location = url
      }
    },
    
    showAddDialog: function(e) {
      var permissions = this.get('permissions'),
        config = this.get('dialogConfig').views.addHoliday.config;

      if (permissions.canAdd === true) {
        this.holidayDialog.showView('addHoliday', {
          model: new Y.app.admin.calendar.NewHolidayForm({
            url: config.url,
            labels: config.validationLabels || {}
          })
        }, function() {
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showUpdateDialog: function(e) {
      var permissions = this.get('permissions'),
        config = this.get('dialogConfig').views.updateHoliday.config,
        record = e.record;

      if (permissions.canUpdate === true) {
        this.holidayDialog.showView('updateHoliday', {
          model: new Y.app.admin.calendar.UpdateHolidayForm({
            id: record.get('id'),
            url: config.url,
            labels: config.validationLabels || {}
          })
        }, {
          modelLoad: true
        }, function() {
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showDeleteDialog: function(e) {
      var permissions = this.get('permissions'),
        config = this.get('dialogConfig').views.deleteHoliday.config,
        record = e.record;

      if (permissions.canDelete === true) {
        this.holidayDialog.showView('deleteHoliday', {
          model: new Y.usp.calendar.CalendarEventBundle({
            id: record.get('id'),
            url: config.url
          })
        }, {
          modelLoad: true
        }, function() {
          this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showActivateDialog: function(e) {
      var permissions = this.get('permissions'),
        config = this.get('dialogConfig').views.activateHoliday.config,
        record = e.record;

      if (permissions.canActivate === true) {
        this.holidayDialog.showView('activateHoliday', {
          model: new Y.app.admin.calendar.UpdateHolidayForm({
            id: record.get('id'),
            url: config.url
          })
        }, {
          modelLoad: true
        }, function() {
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    }
  }, {
    ATTRS: {
      viewCalendarURL:{
        value: '#none'
      },
      viewURL: {
        value: '#none'
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-toolbar',
    'view',
    'holiday-results',
    'holiday-dialog-views',
    'usp-calendar-UpdateCalendarEventBundle'
  ]
});