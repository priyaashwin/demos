YUI.add('calendar-search-view', function(Y) {
	var L = Y.Lang,
      A = Y.Array,
      USPTemplates = Y.usp.ColumnTemplates,
      USPFormatters = Y.usp.ColumnFormatters,
	  USPColumnFormatters = Y.app.ColumnFormatters;

    var prototypePropsAndMethods = {
      initializer: function() {
        var searchConfig = this.get('searchConfig'),
          permissions = searchConfig.permissions,
          labels = searchConfig.labels,
		  calendarDialogConfig = this.get('calendarDialogConfig');

        this.results = new Y.usp.PaginatedResultsTable({
          sortBy: [{ name: 'asc' }],
          columns: [
            {
            	key: 'name',
            	label: labels.name,
            	width: '35%',
            	sortable: true,
            	cellTemplate: USPTemplates.clickable(labels.viewCalendar, 'viewCalendar', permissions.canView)
            },
            {
            	key: 'description',
            	label: labels.description,
            	width: '35%'
            },
            {
            	key: 'status',
            	label: labels.state,
            	width: '10%',
				allowHTML: true,
				formatter: USPColumnFormatters.calendarState,
            	sortable: true
            },
            {
            	key: 'defaultCalendar',
            	label: labels.defaultCalendar,
            	width: '10%',
            	allowHTML: true,
            	formatter: USPColumnFormatters.calendarDefault
			},
			{
				label: labels.actions,
				className: 'pure-table-actions',
				width: '10%',
				formatter: USPFormatters.actions,
				items: [
				        {
				        	clazz: 'viewCalendar',
				        	title: labels.viewCalendarTitle,
				        	label: labels.viewCalendar,
				        	enabled: permissions.canView
				        },
				        {
				        	clazz: 'editCalendar',
				        	title: labels.editCalendarTitle,
				        	label: labels.editCalendar,
				        	enabled: permissions.canUpdate
				        },
				        {
				        	clazz: 'activateCalendar',
				        	title: labels.activateCalendarTitle,
				        	label: labels.activateCalendar,
							enabled: function() {
								if (permissions.canActivate === true) {
									return (this.record.get('status') === 'IN_ACTIVE');
								}
								return false;
							}
				        },
				        {
				        	clazz: 'deactivateCalendar',
				        	title: labels.deactivateCalendarTitle,
				        	label: labels.deactivateCalendar,
							enabled: function() {
								if (permissions.canDeactivate === true) {
									var isNotDefaultCalendar = this.record.get('defaultCalendar') === false,
									  isActive = this.record.get('status') == 'ACTIVE';
									return (isNotDefaultCalendar && isActive);
								}
								return false;
							}
				        },
				        {
				        	clazz: 'setDefaultCalendar',
				        	title: labels.setDefaultCalendarTitle,
				        	label: labels.setDefaultCalendar,
							enabled: function() {
								if (permissions.canUpdate === true) {
									var isNotDefaultCalendar = this.record.get('defaultCalendar') === false,
									  isActive = this.record.get('status') == 'ACTIVE';
									return (isNotDefaultCalendar && isActive);
								}
								return false;
							}
				        }
				]
			}
          ],
          data: new Y.usp.calendar.PaginatedCalendarList({
            url: searchConfig.url
          }),
          noDataMessage: searchConfig.noDataMessage,
          plugins: [
                    { fn: Y.Plugin.usp.ResultsTableMenuPlugin },
                    { fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin }
                   ]
        });
        
        this._tablePanel = new Y.usp.TablePanel({
			tableId: this.name
        }); 
        
	    this.calendarDialog = new Y.app.CalendarDialog({
		  headerContent: calendarDialogConfig.defaultHeader,            
		  views: {
		    addCalendar: {                   
			  headerTemplate: calendarDialogConfig.addCalendarConfig.headerTemplate,
			  successMessage: calendarDialogConfig.addCalendarConfig.successMessage
		    },
		    editCalendar: {                   
			  headerTemplate: calendarDialogConfig.editCalendarConfig.headerTemplate,
			  successMessage: calendarDialogConfig.editCalendarConfig.successMessage
		    },
		    activateCalendar: {                   
			  headerTemplate: calendarDialogConfig.activateCalendarConfig.headerTemplate,
			  successMessage: calendarDialogConfig.activateCalendarConfig.successMessage
		    },
		    deactivateCalendar: {                   
			  headerTemplate: calendarDialogConfig.deactivateCalendarConfig.headerTemplate,
			  successMessage: calendarDialogConfig.deactivateCalendarConfig.successMessage
		    },
		    setDefaultCalendar: {                   
			  headerTemplate: calendarDialogConfig.setDefaultCalendarConfig.headerTemplate,
			  successMessage: calendarDialogConfig.setDefaultCalendarConfig.successMessage
		    }
		  }
	    }).render();
	    
        this._evtHandlers = [
            this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this),
			this.calendarDialog.on('*:saved', function(e) {
				this.results.reload();
			}, this),
		];
        
		Y.on('calendar:showDialog', function(e) {
			switch(e.action){
			  case 'add':
				this.showView('addCalendar', {
					labels: calendarDialogConfig.addCalendarConfig.labels,
					model: new Y.app.NewCalendarForm({
						url: calendarDialogConfig.addCalendarConfig.url
					})
				  }, function(view){
					this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                      
				  });
			   break;
			  default:
			}
		}, this.calendarDialog);
      },

      destructor: function() {
		  if (this._evtHandlers) {
			  A.each(this._evtHandlers, function(item) {
				  item.detach();
			  });
			  //null out
			  this._evtHandlers = null;
		  }
		  
    	  if (this.results) {
			  this.results.destroy();
			  delete this.results;
    	  }
        
		  if (this._tablePanel) {
			  this._tablePanel.destroy();
			  delete this._tablePanel;
		  }
		  
		  if (this.calendarDialog) {
			  this.calendarDialog.destroy();
			  delete this.calendarDialog;
		  }
      },

      render: function() {
        var container = this.get('container'),
          contentNode = Y.one(Y.config.doc.createDocumentFragment());
        
		if(this.get('searchConfig').resultsCountNode){
			this._tablePanel.set('resultsCount', this.get('searchConfig').resultsCountNode);
		}
		
		contentNode.append(this._tablePanel.render().get('container'));
		this._tablePanel.renderResults(this.results);
		container.setHTML(contentNode);
 
        return this;
      },
      
      _handleRowClick: function(e) {
    	  var target = e.currentTarget,
    	    record = this.results.getRecord(target.get('id')),
    	    calendarName = record.get('name'),
    	    calendarId = record.get('id'),
    	    viewURL = L.sub(this.get('searchConfig').viewURL, {id: calendarId, name: calendarName}),
		    calendarEditDialogConfig = this.get('calendarDialogConfig').editCalendarConfig,
		    calendarActivateDialogConfig = this.get('calendarDialogConfig').activateCalendarConfig,
		    calendarDeactivateDialogConfig = this.get('calendarDialogConfig').deactivateCalendarConfig,
		    calendarSetDefaultDialogConfig = this.get('calendarDialogConfig').setDefaultCalendarConfig;
    	  
    	  if (target.hasClass('viewCalendar')) {
    		  window.location = viewURL;
    	  } else if (target.hasClass('editCalendar')) {
			  this.calendarDialog.showView('editCalendar', {
				  labels: calendarEditDialogConfig.labels,
				  model: new Y.app.UpdateCalendarForm({
					  url: L.sub(calendarEditDialogConfig.url, {id: calendarId})
				  })
			  },
			  { modelLoad: true, payload: {id: calendarId} },
			  function(view) {
				  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                      
			  });
    	  } else if (target.hasClass('activateCalendar')) {
			  this.calendarDialog.showView('activateCalendar', {
				  labels: {
					  narrative: L.sub(calendarActivateDialogConfig.labels.narrative, {calendar: calendarName}),
				  },
				  model: new Y.app.CalendarStatusChangeForm({
					  url: L.sub(calendarActivateDialogConfig.url, {id: calendarId, action: 'active'}),
					  id: calendarId
				  })
			  },
			  function(view) {
				  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                      
			  });
    	  } else if (target.hasClass('deactivateCalendar')) {
			  this.calendarDialog.showView('deactivateCalendar', {
				  labels: {
					  narrative: L.sub(calendarDeactivateDialogConfig.labels.narrative, {calendar: calendarName}),
				  },
				  model: new Y.app.CalendarStatusChangeForm({
					  url: L.sub(calendarDeactivateDialogConfig.url, {id: calendarId, action: 'inactive'}),
					  id: calendarId
				  })
			  },
			  function(view) {
				  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                      
			  });
    	  } else if (target.hasClass('setDefaultCalendar')) {
			  this.calendarDialog.showView('setDefaultCalendar', {
				  labels: {
					  narrative: L.sub(calendarSetDefaultDialogConfig.labels.narrative, {calendar: calendarName}),
				  },
				  model: new Y.app.CalendarSetDefaultForm({
					  url: L.sub(calendarSetDefaultDialogConfig.url, {id: calendarId, 'default': 'true'}),
					  id: calendarId
				  })
			  },
			  function(view) {
				  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                      
			  });
    	  }
      }
    };

    var staticPropsAndMethods = {
      ATTRS: {
        searchConfig: {
        }
      }
    };

    var calendarSearchView = Y.Base.create(
      'calendarSearchView',
      Y.View,
      [],
      prototypePropsAndMethods,
      staticPropsAndMethods
    );

    Y.namespace('app').CalendarSearchView = calendarSearchView;
}, '0.0.1', {
  requires: [
    'yui-base',
    'view',
    'accordion-panel',
    'paginated-results-table',
	'calendar-dialog-views',
	'results-templates',
	'results-formatters',
	'results-table-menu-plugin',
    'results-table-keyboard-nav-plugin',
	'caserecording-results-formatters',
    'usp-calendar-Calendar'
  ]
});
