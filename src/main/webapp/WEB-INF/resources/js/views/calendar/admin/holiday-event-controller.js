YUI.add('holiday-event-controller', function(Y) {
  'use-strict';

  var L = Y.Lang;

  Y.namespace('app.admin.calendar').HolidayEventControllerView = Y.Base.create('holidayEventControllerView', Y.View, [], {
    initializer: function(config) {
      this.toolbar = new Y.usp.app.AppToolbar({
        permissions: config.permissions
      }).addTarget(this);

      var resultsConfig = {
        searchConfig: Y.merge(config.searchConfig,{
          holiday:this.get('holiday')
        }),
        permissions: config.permissions,
      };

      this.holidayEventResults = new Y.app.admin.calendar.HolidayEventResults(resultsConfig).addTarget(this);

      this.holidayEventDialog = new Y.app.admin.calendar.HolidayEventDialog(config.dialogConfig).addTarget(this).render();

      this._holidayEvtHandlers = [
        this.on('*:add', this.showAddDialog, this),
        this.on('*:update', this.showUpdateDialog, this),
        this.on('*:delete', this.showDeleteDialog, this),
        this.on('*:saved', this.holidayEventResults.reload, this.holidayEventResults)
      ];
    },
    destructor: function() {
      this._holidayEvtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._holidayEvtHandlers;

      this.holidayEventResults.removeTarget(this);
      this.holidayEventResults.destroy();
      delete this.holidayEventResults;

      this.holidayEventDialog.removeTarget(this);
      this.holidayEventDialog.destroy();
      delete this.holidayEventDialog;

      this.toolbar.removeTarget(this);
      this.toolbar.destroy();
      delete this.toolbar;
    },
    render: function() {
      var contentNode = Y.one(Y.config.doc.createDocumentFragment());

      //append the results
      contentNode.append(this.holidayEventResults.render().get('container'));

      //set fragment into container
      this.get('container').setHTML(contentNode);

      return this;
    },
    showAddDialog: function(e) {
      var permissions = this.get('permissions'),
        config = this.get('dialogConfig').views.addHolidayEvent.config,
        holiday = this.get('holiday');

      if (permissions.canAdd === true) {
        this.holidayEventDialog.showView('addHolidayEvent', {
          model: new Y.app.admin.calendar.NewHolidayEventForm({
            url: L.sub(config.url, {
              holidayId: holiday.id
            }),
            labels: config.validationLabels || {}
          }),
          otherData: {
            holiday: holiday
          }
        }, function() {
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showUpdateDialog: function(e) {
      var permissions = this.get('permissions'),
        config = this.get('dialogConfig').views.updateHolidayEvent.config,
        holiday = this.get('holiday'),
        record = e.record;

      if (permissions.canUpdate === true) {
        this.holidayEventDialog.showView('updateHolidayEvent', {
          model: new Y.app.admin.calendar.UpdateHolidayEventForm({
            id: record.get('id'),
            url: config.url,
            labels: config.validationLabels || {}
          }),
          otherData: {
            holiday: holiday
          }
        }, {
          modelLoad: true
        }, function() {
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    },
    showDeleteDialog: function(e) {
      var permissions = this.get('permissions'),
        config = this.get('dialogConfig').views.deleteHolidayEvent.config,
        holiday = this.get('holiday'),
        record = e.record;

      if (permissions.canDelete === true) {
        this.holidayEventDialog.showView('deleteHolidayEvent', {
          model: new Y.usp.calendar.CalendarEvent({
            id: record.get('id'),
            url: config.url
          }),
          otherData: {
            holiday: holiday
          }
        }, {
          modelLoad: true
        }, function() {
          this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
    }
  }, {
    ATTRS: {
      holiday: {
        value: {
          id: null,
          name: null
        }
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-toolbar',
    'view',
    'holiday-event-results',
    'holiday-event-dialog-views',
    'usp-calendar-CalendarEvent'
  ]
});