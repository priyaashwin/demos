YUI.add('calendar-nw-days-search-view', function(Y) {
	var L = Y.Lang,
	  A = Y.Array,
      USPFormatters = Y.usp.ColumnFormatters;
	
    var prototypePropsAndMethods = {
      initializer: function() {
        var searchConfig = this.get('searchConfig'),
          permissions = searchConfig.permissions,
          labels = searchConfig.labels,
		  calendarNWDaysDialogConfig = this.get('calendarNWDaysDialogConfig');

        this.results = new Y.usp.PaginatedResultsTable({
          sortBy: [{ 'name': 'asc' }],
          columns: [
            {
            	key: 'calendarEventBundle!name',
            	label: labels.name,
            	width: '30%'
            },
            {
            	key: 'calendarEventBundle!description',
            	label: labels.description,
            	width: '40%'
            },
			{
				label: labels.actions,
				className: 'pure-table-actions',
				width: '10%',
				formatter: USPFormatters.actions,
				items: [
				        {
				        	clazz: 'deleteCalendarNonWorkingDay',
				        	title: labels.deleteCalendarNWDayTitle,
				        	label: labels.deleteCalendarNWDay,
				        	enabled: permissions.canDelete
				        }
				]
			}
          ],
          data: new Y.usp.calendar.PaginatedCalendarCalendarEventBundleWithCalendarEventBundleList({
			url: L.sub(searchConfig.url, {
				calendarId: searchConfig.calendar.id
			})
          }),
          noDataMessage: searchConfig.noDataMessage,
          plugins: [
                    { fn: Y.Plugin.usp.ResultsTableMenuPlugin },
                    { fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin }
                   ]
        });
        
        this._tablePanel = new Y.usp.TablePanel({
			tableId: this.name
        });
        
	    this.calendarNWDaysDialog = new Y.app.CalendarNWDaysDialog({
		  headerContent: calendarNWDaysDialogConfig.defaultHeader,            
		  views: {
		    addCalendarNonWorkingDay: {
			  headerTemplate: calendarNWDaysDialogConfig.addCalendarNWDaysConfig.headerTemplate,
			  successMessage: calendarNWDaysDialogConfig.addCalendarNWDaysConfig.successMessage
		    },
		    deleteCalendarNonWorkingDay: {
			  headerTemplate: calendarNWDaysDialogConfig.deleteCalendarNWDaysConfig.headerTemplate,
			  successMessage: calendarNWDaysDialogConfig.deleteCalendarNWDaysConfig.successMessage
		    }
		  }
	    }).render();
	    
	    this._evtHandlers = [
            this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this),
	        this.calendarNWDaysDialog.on('*:saved', function(e) {
	        	this.results.reload();
	        }, this)
		];
        
	    Y.on('calendarNWDays:tabActive', function(e) {
	    	this.results.reload();
	    }, this);
	    
		Y.on('calendarNWDays:showDialog', function(e) {
			switch(e.action){
			  case 'add':
				calendarNWDaysDialogConfig.addCalendarNWDaysConfig.labels.narrative = L.sub(calendarNWDaysDialogConfig.addCalendarNWDaysConfig.labels.narrative, {calendar: searchConfig.calendar.name});
				this.showView('addCalendarNonWorkingDay', {
					labels: calendarNWDaysDialogConfig.addCalendarNWDaysConfig.labels,
					calendarEventBundleListURL: calendarNWDaysDialogConfig.addCalendarNWDaysConfig.calendarEventBundleListURL,
					calendarCalendarEventBundleListURL: calendarNWDaysDialogConfig.addCalendarNWDaysConfig.calendarCalendarEventBundleListURL,
					calendar: searchConfig.calendar,
					model: new Y.app.NewCalendarNWDaysForm({
						url: L.sub(calendarNWDaysDialogConfig.addCalendarNWDaysConfig.url)
					})
				  }, function(view){
					this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);                      
				  });
			   break;
			  default:
			}
		}, this.calendarNWDaysDialog);
      },

      destructor: function() {
		  if (this._evtHandlers) {
			  A.each(this._evtHandlers, function(item) {
				  item.detach();
			  });
			  //null out
			  this._evtHandlers = null;
		  }
		  
    	  if (this.results) {
			  this.results.destroy();
			  delete this.results;
    	  }
        
		  if (this._tablePanel) {
			  this._tablePanel.destroy();
			  delete this._tablePanel;
		  }
		  
		  if (this.calendarNWDaysDialog) {
			  this.calendarNWDaysDialog.destroy();
			  delete this.calendarNWDaysDialog;
		  }
      },

      render: function() {
        var container = this.get('container'),
          searchConfig = this.get('searchConfig'),
          contentNode = Y.one(Y.config.doc.createDocumentFragment());
        
		if(searchConfig.resultsCountNode){
			this._tablePanel.set('resultsCount', searchConfig.resultsCountNode);
		}
		
		if(searchConfig.titleNode && searchConfig.calendar.name !== "null"){
			searchConfig.titleNode.set('text', searchConfig.calendar.name);
		}
		
		contentNode.append(this._tablePanel.render().get('container'));
		this._tablePanel.renderResults(this.results);
		container.setHTML(contentNode);
 
        return this;
      },
      
      _handleRowClick: function(e) {
    	  var target = e.currentTarget,
    	  	calendarName = this.get('searchConfig').calendar.name,
    	  	calendarId = this.get('searchConfig').calendar.id,
    	    record = this.results.getRecord(target.get('id')),
    	    nonWorkingDayCollId = record.get('id'),
    	    nonWorkingDayName = record.get('calendarEventBundle!name'),
		    calendarNWDaysDeleteDialogConfig = this.get('calendarNWDaysDialogConfig').deleteCalendarNWDaysConfig;
		    
    	  if (target.hasClass('deleteCalendarNonWorkingDay')) {
    		 this.calendarNWDaysDialog.showView('deleteCalendarNonWorkingDay', {
    			 labels: {
    				 narrative: L.sub(calendarNWDaysDeleteDialogConfig.labels.narrative, {holidayName: nonWorkingDayName, calendar: calendarName})
    			 },
    			 model: new Y.app.DeleteCalendarNWDaysForm({
    				 url: L.sub(calendarNWDaysDeleteDialogConfig.url, {id: nonWorkingDayCollId})
    			 })
    		 },
    		 function(view) {
				  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
    		 }); 
    	  }
      }
    };
    
    var staticPropsAndMethods = {
      ATTRS: {
        searchConfig: {
        }
      }
    };

    var calendarNWDaysSearchView = Y.Base.create(
      'calendarNWDaysSearchView',
      Y.View,
      [],
      prototypePropsAndMethods,
      staticPropsAndMethods
    );
    
    Y.namespace('app').CalendarNWDaysSearchView = calendarNWDaysSearchView;
}, '0.0.1', {
  requires: [
    'yui-base',
    'view',
    'accordion-panel',
    'paginated-results-table',
	'calendar-nw-days-dialog-views',
	'results-templates',
	'results-formatters',
	'results-table-menu-plugin',
    'results-table-keyboard-nav-plugin',
	'caserecording-results-formatters',
    'usp-calendar-CalendarCalendarEventBundleWithCalendarEventBundle'
  ]
});
