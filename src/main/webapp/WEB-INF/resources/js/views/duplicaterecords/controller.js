YUI.add('duplicaterecords-controller', function (Y) {

    'use-strict';

    Y.namespace('app.admin.duplicaterecords').DuplicateRecordsControllerView = Y.Base.create('duplicateRecordsControllerView', Y.View, [], {

        initializer: function (config) {
            this.duplicateRecordsDialog = new Y.app.admin.duplicaterecords.DuplicateRecordsDialog(config.dialogConfig).addTarget(this).render();

            var resultsConfig=Y.merge(config.duplicateRecordSetsConfig,{
            	  permissions: config.permissions
            	});

            this.duplicateRecordSetsAccordionView = new Y.app.admin.duplicaterecords.DuplicateRecordsFilteredResults(resultsConfig).addTarget(this);

            this.duplicateRecordsAccordionView = new Y.app.admin.duplicaterecords.DuplicateRecordsAccordionView(Y.merge(config.duplicateRecordsConfig, {
                searchConfig: Y.merge(config.duplicateRecordsConfig.searchConfig, {
                    permissions: config.permissions
                })
            })).addTarget(this);

            this._evtHandlers = [
                this.after('*:load', this.handleDuplicateRecordsResultsChange, this),
                this.after('*:saved', this.handleSave, this),
                this.on('*:linkDuplicateRecords', this.showLinkDuplicateRecordsDialog, this),
                this.on('*:setAsPrimaryDuplicateRecord', this.setPrimaryDuplicateRecord, this),
                this.on('*:setAsRecordToMerge', this.setRecordToMerge, this),
                this.on('*:unlinkRecord', this.showUnlinkDuplicateRecordDialog, this),
                this.after('primaryDuplicateRecordIdChange', this.handlePrimaryDuplicateRecordChange, this),
                this.after('duplicateRecordListChange', this.handleDuplicateRecordListChange, this),
                this.after('*:selectedRecordChange', this.handleSelectedDuplicateRecordSetChange, this)
            ];
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.duplicateRecordSetsAccordionView.render().get('container'));
            contentNode.append(this.duplicateRecordsAccordionView.render().get('container'));

            this.get('container').setHTML(contentNode);

            return this;
        },

        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.duplicateRecordsDialog.removeTarget(this);
            this.duplicateRecordsDialog.destroy();
            delete this.duplicateRecordsDialog;

            this.duplicateRecordSetsAccordionView.removeTarget(this);
            this.duplicateRecordSetsAccordionView.destroy();
            delete this.duplicateRecordSetsAccordionView;

            this.duplicateRecordsAccordionView.removeTarget(this);
            this.duplicateRecordsAccordionView.destroy();
            delete this.duplicateRecordsAccordionView;
        },

        setPrimaryDuplicateRecord: function (e) {
            var primaryDuplicateRecordId = (this.get('primaryDuplicateRecordId') !== e.record.get('id')) ? e.record.get('id') : null,
                duplicateRecordList = this.get('duplicateRecordList'),
                recordIndex = duplicateRecordList.indexOf(primaryDuplicateRecordId),
                recordInDuplicateRecordList = (primaryDuplicateRecordId && recordIndex !== -1) ? true : false;

            if (recordInDuplicateRecordList) {
                //Ensure record cannot be set as primary and as a record to merge.
                duplicateRecordList.splice(recordIndex, 1);
                this.set('duplicateRecordList', duplicateRecordList);
            }

            this.set('primaryDuplicateRecordId', primaryDuplicateRecordId);
        },

        setRecordToMerge: function (e) {
            var duplicateRecordList = this.get('duplicateRecordList'),
                recordId = e.record.get('id'),
                recordIndex = duplicateRecordList.indexOf(recordId);

            if (recordIndex === -1) {
                if (recordId === this.get('primaryDuplicateRecordId')) {
                    //Ensure record cannot be set as primary and as a record to merge.
                    this.set('primaryDuplicateRecordId', null);
                }
                duplicateRecordList.push(e.record.get('id'));
            } else {
                duplicateRecordList.splice(recordIndex, 1);
            }

            this.set('duplicateRecordList', duplicateRecordList);
        },

        handleSelectedDuplicateRecordSetChange: function (e) {
            if (e.newVal !== null) {
                this.resetDuplicateRecords();
                this.duplicateRecordsAccordionView.get('results').get('data').url = Y.Lang.sub(this.get('duplicateRecordsConfig').searchConfig.url, {
                    id: e.newVal.get('id')
                });

                this.duplicateRecordsAccordionView.reload({
                    reset: true
                });
            }
        },

        handleDuplicateRecordsResultsChange: function () {
            this.duplicateRecordsAccordionView.highlightPrimaryRecord(this.get('primaryDuplicateRecordId'));
            this.duplicateRecordsAccordionView.highlightDuplicateRecords(this.get('duplicateRecordList'));
        },

        handleSave: function () {
            this.duplicateRecordsAccordionView.reload();
            this.resetDuplicateRecords();
        },

        handlePrimaryDuplicateRecordChange: function () {
            this.duplicateRecordsAccordionView.highlightPrimaryRecord(this.get('primaryDuplicateRecordId'));
            this.duplicateRecordsAccordionView.setLinkButton((this.get('primaryDuplicateRecordId') !== null), (this.get('duplicateRecordList').length > 0));
        },

        handleDuplicateRecordListChange: function () {
            this.duplicateRecordsAccordionView.highlightDuplicateRecords(this.get('duplicateRecordList'));
            this.duplicateRecordsAccordionView.setLinkButton((this.get('primaryDuplicateRecordId') !== null), (this.get('duplicateRecordList').length > 0));
        },

        resetDuplicateRecords: function () {
            this.set('primaryDuplicateRecordId', null);
            this.set('duplicateRecordList', []);
        },

        showLinkDuplicateRecordsDialog: function (e) {
            var permissions = this.get('permissions') || {};
            var config = this.get('dialogConfig').views.link.config;
            if (permissions.canLinkDuplicateRecords) {
                this.duplicateRecordsDialog.showView('link', {
                    model: new Y.usp.person.UpdatePersonDuplicates({
                        url: Y.Lang.sub(config.url, {
                            id: this.get('primaryDuplicateRecordId')
                        })
                    }),
                    otherData: {
                        primaryDuplicateRecordId: this.get('primaryDuplicateRecordId'),
                        duplicateRecordList: this.get('duplicateRecordList')
                    }
                }, {}, function () {
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },

        showUnlinkDuplicateRecordDialog: function (e) {
            var permissions = this.get('permissions') || {};
            var config = this.get('dialogConfig').views.unlink.config;
            if (permissions.canLinkDuplicateRecords) {
                this.duplicateRecordsDialog.showView('unlink', {
                    model: new Y.app.admin.duplicaterecords.UnlinkDuplicateRecordForm({
                        url: Y.Lang.sub(config.url, {
                            id: e.record.get('id')
                        })
                    })
                }, {}, function () {
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        }

    }, {
        ATTRS: {
            primaryDuplicateRecordId: {
                value: null
            },
            duplicateRecordList: {
                value: []
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-toolbar',
        'duplicaterecords-results-view',
        'duplicaterecords-filter',
        'duplicaterecords-dialog-views',
        'usp-person-UpdatePersonDuplicates'
    ]
});