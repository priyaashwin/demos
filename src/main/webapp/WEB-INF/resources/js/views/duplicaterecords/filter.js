YUI.add('duplicaterecords-filter', function(Y) {
	  "use strict";	

	  Y.namespace('app.admin.duplicaterecords').DuplicateRecordsFilterModel = Y.Base.create('duplicateRecordsFilterModel', Y.usp.ResultsFilterModel, [], {}, {
	    ATTRS: {
	      nameOrId: {
	        USPType: 'String',
	        value: ''
	        }
	    }
	  });
	  
	  Y.namespace('app.admin.duplicaterecords').DuplicateRecordsFilter = Y.Base.create('duplicateRecordsFilter', Y.usp.app.Filter, [], {
	    filterModelType: Y.app.admin.duplicaterecords.DuplicateRecordsFilterModel,
	    filterTemplate: Y.Handlebars.templates['duplicateRecordsResultsFilter'],
	    filterContextTemplate: Y.Handlebars.templates['duplicateRecordsActiveFilter']
	  });
	  
	  }, '0.0.1', {
		  requires: ['yui-base',
		    'app-filter',
		    'results-filter',
		    'handlebars-helpers',
		    'handlebars-duplicaterecords-templates',
		    'data-binding',
		    'form-util'
		  ]
		});