YUI.add('duplicaterecords-results-view', function (Y) {
    'use-strict';

    var USPFormatters = Y.usp.ColumnFormatters,
        PAGINATOR_TEMPLATE = '<ul  class="pure-pagebar">\
        <li>\
          <ul class="pure-paginator"><li><a class="pure-button {pageLinkClass}" data-pglink="first" href="#" title="First page"><i class="fa fa-fast-backward icon-fast-backward"></i></a></li>\
            <li><a class="pure-button {pageLinkClass}" data-pglink="prev" href="#" title="Previous page"><i class="fa fa-step-backward icon-step-backward"></i></a></li>\
              {pageLinks}\
            <li><a class="pure-button {pageLinkClass}" data-pglink="next" href="#" title="Next page"><i class="fa fa-step-forward icon-step-forward"></i></a></li>\
          </ul>\
        </li>\
        <li>\
        <ul class="pure-perpage"><li><span>Rows per page: {selectRowsPerPage}</span></li></ul>\
        </li></ul>',
        PAGE_LINK_TEMPLATE = '<li><a class="pure-button {pageLinkClass}" data-pglink="{page}" href="#" title="Page {page}">{page}</a></li>';

    //custom PaginatedResultsTable template for DuplicateRecordSetsAccordionView to omit the 'last' pagination control, due to unusual behaviour caused by
    //REST service implementation
    Y.namespace('app.admin.duplicaterecords').PaginatedResultsTable = Y.Base.create('paginatedResultsTable', Y.usp.PaginatedResultsTable, [], {}, {
        CSS_PREFIX: 'pure-table',
        ATTRS: {
            paginator: {
                valueFn: function () {
                    var container = this.get('paginatorContainer');
                    return new Y.usp.USPPaginatorView({
                        model: new Y.usp.USPPaginatorModel({
                            itemsPerPage: 10
                        }),
                        container: container,
                        paginatorTemplate: PAGINATOR_TEMPLATE,
                        pageLinkTemplate: PAGE_LINK_TEMPLATE,
                        maxPageLinks: 5,
                        pageOptions: [5, 10, 25, 50, 100]
                    });
                },
                readOnly: true
            }
        }
    });
    
    Y.namespace('app.admin.duplicaterecords').PaginatedFilteredDuplicateRecordsResultList = Y.Base.create("paginatedFilteredDuplicateRecordsResultList", Y.usp.person.PaginatedPersonList, [
        Y.usp.ResultsFilterURLMixin
      ], {
        whiteList: ["nameOrId"]
      }, {
        ATTRS: {
          filterModel: {
            writeOnce: 'initOnly'
          }
        }
      });

    Y.namespace('app.admin.duplicaterecords').DuplicateRecordSetsAccordionView = Y.Base.create('duplicateRecordSetsAccordionView', Y.usp.app.Results, [], {
        resultsType: Y.app.admin.duplicaterecords.PaginatedResultsTable,
        tablePanelType: Y.usp.AccordionTablePanel,

        // Once the issue raised in OLM-30542 is resolved then this function needs to be removed. 
        initializer: function (config) {
        	this.getTablePanel().set('title', 'Duplicate record sets');
        },

        getResultsListModel: function (config) {
            return new Y.app.admin.duplicaterecords.PaginatedFilteredDuplicateRecordsResultList({
                url: config.url,
                //plug in the filter model
                filterModel: config.filterModel
              });
        },

        getColumnConfiguration: function (config) {
            var labels = config.labels;

            return [{
                key: 'forename',
                label: labels.forename,
                width: '30%'
            }, {
                key: 'surname',
                label: labels.surname,
                width: '30%'
            }, {
                key: 'dateOfBirth!calculatedDate',
                label: labels.dateOfBirth,
                formatter: USPFormatters.fuzzyDate,
                maskField: 'dateOfBirth.fuzzyDateMask',
                width: '30%'
            }, {
                key: 'gender',
                label: labels.gender,
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.person.gender.category.codedEntries,
                width: '10%'
            }];
        },

        rowClick: function (target, record) {
            this.get('results').selectRow(record.get('id'));

            Y.app.admin.duplicaterecords.DuplicateRecordSetsAccordionView.superclass.rowClick.apply(this, arguments);
        },

        render: function () {
            Y.app.admin.duplicaterecords.DuplicateRecordSetsAccordionView.superclass.render.call(this);

            //omit results count for table due to unusual behaviour caused by REST service implementation
            this.get('results').get('resultsCount').remove();

            return this;
        }
    });

    Y.namespace('app.admin.duplicaterecords').DuplicateRecordsAccordionView = Y.Base.create('duplicateRecordsAccordionView', Y.usp.app.Results, [], {
        tablePanelType: Y.usp.AccordionTablePanel,
        getResultsListModel: function (config) {
            return new Y.usp.person.PaginatedPersonList({
                url: config.url
            });
        },

        getColumnConfiguration: function (config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                    key: 'id',
                    label: labels.id,
                    sortable: true,
                    width: '10%'
                },
                {
                    key: 'forename',
                    label: labels.forename,
                    sortable: true,
                    width: '20%'
                }, {
                    key: 'surname',
                    label: labels.surname,
                    sortable: true,
                    width: '20%'
                }, {
                    key: 'dateOfBirth!calculatedDate',
                    label: labels.dateOfBirth,
                    formatter: USPFormatters.fuzzyDate,
                    maskField: 'dateOfBirth.fuzzyDateMask',
                    width: '20%'
                }, {
                    key: 'gender',
                    label: labels.gender,
                    formatter: USPFormatters.codedEntry,
                    codedEntries: Y.uspCategory.person.gender.category.codedEntries,
                    width: '10%'
                },
                {
                    key: 'duplicated',
                    label: labels.duplicated,
                    formatter: function (o) {
                        if (o.value === 'MASTER') {
                            return 'Primary';
                        }
                        return USPFormatters.enumType(o);
                    },
                    enumTypes: Y.usp.person.enum.DuplicateStatus.values,
                    width: '11%'
                },
                {
                    label: 'actions',
                    className: 'pure-table-actions',
                    allowHTML: true,
                    width: '9%',
                    formatter: USPFormatters.actions,
                    items: [{
                        clazz: 'setAsPrimaryDuplicateRecord',
                        title: 'toggle as primary record',
                        label: 'toggle as primary record',
                        visible: permissions.canLinkDuplicateRecords,
                        enabled: function(o) {
                          return (this.record.get('duplicated') !== 'DUPLICATE')? true:false;
                        }
                    }, {
                        clazz: 'setAsRecordToMerge',
                        title: 'toggle as duplicate record',
                        label: 'toggle as duplicate record',
                        visible: permissions.canLinkDuplicateRecords
                    },{
                    	clazz: 'unlinkRecord',
                        title: 'unlink record',
                        label: 'unlink record',
                        visible: permissions.canLinkDuplicateRecords,
                        enabled: function(o) {
                            return (this.record.get('duplicated') === 'DUPLICATE')? true:false;
                        }
                    }]
                }
            ];
        },

        render: function () {
            Y.app.admin.duplicaterecords.DuplicateRecordsAccordionView.superclass.render.call(this);
            this._initLinkButton();

            return this;
        },

        destructor: function () {
            if (this.toolbar) {
                this.toolbar.removeTarget(this);
                this.toolbar.destroy();
                delete this.toolbar;
            }
        },

        _initLinkButton: function () {
            var config = this.get('searchConfig')
            permissions = config.permissions,
                labels = config.labels;
            //add toolbar instance and link button
            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this.toolbar.addButton({
                icon: 'fa fa-plus-circle',
                className: 'pull-right pure-button-active',
                action: 'linkDuplicateRecords',
                title: labels.linkButtonTitle,
                ariaLabel: labels.linkButtonTitle,
                label: labels.linkButtonLabel
            });

            this.toolbar.disableButton('linkDuplicateRecords');
        },

        setLinkButton: function (hasPrimaryRecordSelected, hasDuplicateRecordsSelected) {
            if (hasPrimaryRecordSelected && hasDuplicateRecordsSelected) {
                this.toolbar.enableButton('linkDuplicateRecords');
            } else {
                this.toolbar.disableButton('linkDuplicateRecords');
            }
        },

        highlightPrimaryRecord: function (primaryRecordId) {
            var selectedRecord = this.get('results').getRecord(primaryRecordId),
                row = this.get('results').getRow(selectedRecord);

            this.get('container').one('#accordionTablePanelDuplicate_records').all('tr').removeClass('primary-merge-record-row');

            if (row) {
                row.addClass('primary-merge-record-row');
            }
        },

        highlightDuplicateRecords: function (duplicateRecordIds) {
            this.get('container').one('#accordionTablePanelDuplicate_records').all('tr').removeClass('merge-record-row');

            duplicateRecordIds.forEach(function (duplicateRecordId) {
                var selectedRecord = this.get('results').getRecord(duplicateRecordId),
                    row = this.get('results').getRow(selectedRecord);

                if (row) {
                    row.addClass('merge-record-row');
                }
            }, this);
        }
    }, {
        ATTRS: {
            initialLoad: {
                value: false
            }
        }
    });

    Y.namespace('app.admin.duplicaterecords').DuplicateRecordsFilteredResults = Y.Base.create('duplicateRecordsFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.admin.duplicaterecords.DuplicateRecordsFilter,
        resultsType: Y.app.admin.duplicaterecords.DuplicateRecordSetsAccordionView
      });
    
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-filter',
        'app-results',
        'results-formatters',
        'results-templates',
        'duplicaterecords-filter',
        'app-filtered-results',
        'results-table-search-state-plugin',
        'categories-person-component-Gender',
        'person-component-enumerations',
        'usp-person-Person'
    ]
});