YUI.add('audit-filter', function (Y) {
    var L = Y.Lang;

    Y.namespace('app.audit').BaseAuditFilterView = Y.Base.create('auditFilterView', Y.usp.app.ResultsFilter, [], {
        events: {
            '.filterReset': {
                click: '_handleResetAutoCompleteFields'
            }
        },

        initializer: function () {
            var autoCompletePlaceholder = this.get('labels').autoCompletePlaceholder,
                autoCompletePlaceholderSecurityUser = this.get('labels').autoCompletePlaceholderSecurityUser;

            this.dateFromCalendar = new Y.usp.CalendarPopup();
            this.dateToCalendar = new Y.usp.CalendarPopup();

            this.dateFromCalendar.addTarget(this);
            this.dateToCalendar.addTarget(this);

            this.clientAutoCompleteView = new Y.app.PersonAutoCompleteResultView({
                autocompleteURL: this.get('urls').personAutocompleteURL,
                inputName: 'clientSubjectSearch',
                labels: {
                    placeholder: autoCompletePlaceholder
                }
            });
            this.clientAutoCompleteView.addTarget(this);

            this.userAutoCompleteView = new Y.app.SecurityUserAutoCompleteResultView({
                autocompleteURL: this.get('urls').securityUserAutocompleteURL,
                inputName: 'userSubjectSearch',
                labels: {
                    placeholder: autoCompletePlaceholderSecurityUser
                }
            });
            this.userAutoCompleteView.addTarget(this);

            this._evtHandlers = [
                this.clientAutoCompleteView.after('selectedPersonChange', this._handleClientChange, this),
                this.userAutoCompleteView.after('selectedPersonChange', this._handleUserChange, this),
                this.get('model').on('filter:reset', this._handleResetAutoCompleteFields, this)
            ];

        },

        render: function () {
            var container = this.get('container');

            //call into superclass to render
            Y.app.audit.BaseAuditFilterView.superclass.render.call(this);

            //set the input node for the calendar
            this.dateFromCalendar.set('inputNode', container.one('.dateFrom'));
            this.dateToCalendar.set('inputNode', container.one('.dateTo'));

            this.dateFromCalendar.render();
            this.dateToCalendar.render();

            container.one('.clientSubjectSearchWrapper').append(this.clientAutoCompleteView.render().get('container'));
            container.one('.userSubjectSearchWrapper').append(this.userAutoCompleteView.render().get('container'));

            return this;
        },

        _handleClientChange: function (e) {
            var selectedPerson = e.target.get('selectedPerson');
            this.get('model').set('personId', (selectedPerson) ? selectedPerson.id : null, {src: 'filter'});
        },
        _handleUserChange: function (e) {
            var selectedPerson = e.target.get('selectedPerson');
            this.get('model').set('userId', (selectedPerson) ? selectedPerson.id : null, {src: 'filter'});
        },
        _handleResetAutoCompleteFields: function () {
            //reset attributes in model inherited from auto-complete inputs to ensure active filter view is reset correctly
            this.clientAutoCompleteView.set('selectedPerson', null);
            this.userAutoCompleteView.set('selectedPerson', null);
        },
        destructor: function () {

            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.dateFromCalendar.destroy();
            delete this.dateFromCalendar;

            this.dateToCalendar.destroy();
            delete this.dateToCalendar;

            if (this.clientAutoCompleteView) {
                this.clientAutoCompleteView.removeTarget(this);
                this.clientAutoCompleteView.destroy();
                delete this.clientAutoCompleteView;
            }

            if (this.userAutoCompleteView) {
                this.userAutoCompleteView.removeTarget(this);
                this.userAutoCompleteView.destroy();
                delete this.userAutoCompleteView;
            }

        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            model: {},
            filterConfig: {}
        }
    });

    Y.namespace('app.audit').BaseAuditFilterContext = Y.Base.create('baseAuditFilterContext', Y.usp.app.FilterContext, [], {
        events: {
            '.filter-message a.close': {
                click: 'hideMessage'
            }
        },
        hideMessage: function (e) {
            e.preventDefault();
            this.get('container').all('div.filter-message').hide('fadeOut');
        }
    });

    Y.namespace('app.audit').PersonAccessFilterModel = Y.Base.create('personAccessFilterModel', Y.usp.ResultsFilterModel, [], {
        customValidation: function (attrs) {
            //enforce our validation first - if we succeed this will call the parent validate
            var errors = {},
                dateFrom = attrs['accessDateFrom'],
                dateTo = attrs['accessDateTo'];

            if (L.isNumber(dateFrom) && L.isNumber(dateTo)) {
                if (dateFrom > dateTo) {
                    errors['accessDateTo'] = this.labels.validation.dateTo;
                }
            }

            return errors;
        }
    }, {
        ATTRS: {
            accessDateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            accessDateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            personId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            userId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            }
        }
    });

    Y.namespace('app.audit').PersonAccessResultsFilterView = Y.Base.create('personAccessResultsFilterView', Y.app.audit.BaseAuditFilterView, [], {
        template: Y.Handlebars.templates['personAccessFilter'],
    });

    Y.namespace('app.audit').PersonAccessFilterContext = Y.Base.create('personAccessFilterContext', Y.app.audit.BaseAuditFilterContext, [], {
        template: Y.Handlebars.templates['personAccessActiveFilter'],
    });

    Y.namespace('app.audit').PersonAccessFilter = Y.Base.create('personAccessFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.audit.PersonAccessFilterModel,
        filterContextType: Y.app.audit.PersonAccessFilterContext,
        filterType: Y.app.audit.PersonAccessResultsFilterView
    });

    Y.namespace('app.audit').EventLogFilterModel = Y.Base.create('eventLogFilterModel', Y.usp.ResultsFilterModel, [], {
        customValidation: function (attrs) {
            //enforce our validation first - if we succeed this will call the parent validate
            var errors = {},
                dateFrom = attrs['dateFrom'],
                dateTo = attrs['dateTo'];

            if (L.isNumber(dateFrom) && L.isNumber(dateTo)) {
                if (dateFrom > dateTo) {
                    errors['dateTo'] = this.labels.validation.dateTo;
                }
            }

            return errors;
        }
    }, {
        ATTRS: {
            dateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            dateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            subjectId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            code: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            userId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            }
        }
    });

    Y.namespace('app.audit').EventLogResultsFilterView = Y.Base.create('eventLogResultsFilterView', Y.app.audit.BaseAuditFilterView, [], {
        template: Y.Handlebars.templates['eventLogFilter'],
        //override method due to difference in attribute name i.e. subjectId vs personId
        _handleClientChange: function (e) {
            var selectedPerson = e.target.get('selectedPerson');
            this.get('model').set('subjectId', (selectedPerson) ? selectedPerson.id : null, {src: 'filter'});
        },
    });

    Y.namespace('app.audit').EventLogFilterContext = Y.Base.create('eventLogFilterContext', Y.app.audit.BaseAuditFilterContext, [], {
        template: Y.Handlebars.templates['eventLogActiveFilter']
    });

    Y.namespace('app.audit').EventLogFilter = Y.Base.create('eventLogFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.audit.EventLogFilterModel,
        filterContextType: Y.app.audit.EventLogFilterContext,
        filterType: Y.app.audit.EventLogResultsFilterView
    });

}, '0.0.1', {
    requires: ['yui-base',
        'calendar-popup',
        'results-filter',
        'app-filter',
        'handlebars-helpers',
        'handlebars-audit-templates',
        'person-autocomplete-view',
        'security-user-autocomplete-view'
    ]
});