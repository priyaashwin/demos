YUI.add('audit-controller-view', function (Y) {
    'use-strict';

    Y.namespace('app.audit').AuditControllerView = Y.Base.create('auditControllerView', Y.View, [], {
        initializer: function (config) {
            this.set('personAccessCSVUrl', config.personAccessAccordionConfig.searchConfig.urlCSV);
            this.set('eventLogCSVUrl', config.eventLogAccordionConfig.searchConfig.urlCSV);

            this.auditPersonAccessAccordionView = new Y.app.audit.PersonAccessAccordionView(Y.merge(config.personAccessAccordionConfig, {
                searchConfig: Y.merge(config.personAccessAccordionConfig.searchConfig, {
                    permissions: config.personAccessAccordionConfig.permissions
                })
            })).addTarget(this);

            this.auditEventLogAccordionView = new Y.app.audit.EventLogAccordionView(Y.merge(config.eventLogAccordionConfig, {
                searchConfig: Y.merge(config.eventLogAccordionConfig.searchConfig, {
                    permissions: config.eventLogAccordionConfig.permissions
                })
            })).addTarget(this);

            this._evtHandlers = [
                this.on('*:personAccessDownload', this.handlePersonAccessDownload, this),
                this.on('*:eventLogDownload', this.handleEventLogDownload, this),
                this.on('filter:apply', this._handleFilterChange, this)
            ];

        },
        destructor: function () {
            this.auditPersonAccessAccordionView.destroy();
            delete this.auditPersonAccessAccordionView;

            this.auditEventLogAccordionView.destroy();
            delete this.auditEventLogAccordionView;
        },
        render: function () {
            var container = this.get('container');
            container.append(this.auditPersonAccessAccordionView.render().get('container'));
            container.append(this.auditEventLogAccordionView.render().get('container'));

        },
        _handleFilterChange: function (e) {
            //on filter change, we want to amend the relevant CSV URL so that we retrieve only filtered results
            var filterContext = e.target.name,
                filterAttributes = [],
                url = '';

            if (filterContext === 'personAccessResultsFilterView') {
                filterAttributes = ["personId", "userId", "accessDateFrom", "accessDateTo"];
                url = this._constructCSVUrl(e.target.get('model'), filterAttributes, this.get('personAccessAccordionConfig').searchConfig.urlCSV);
                this.set('personAccessCSVUrl', url);
            } else if (filterContext === 'eventLogResultsFilterView') {
                filterAttributes = ["subjectId", "userId", "code", "dateFrom", "dateTo"];
                url = this._constructCSVUrl(e.target.get('model'), filterAttributes, this.get('eventLogAccordionConfig').searchConfig.urlCSV);
                this.set('eventLogCSVUrl', url);
            }
        },
        handlePersonAccessDownload: function (e) {
            this.handleDownload(this.get('personAccessCSVUrl'));
        },
        handleEventLogDownload: function (e) {
            this.handleDownload(this.get('eventLogCSVUrl'));
        },
        handleDownload: function (url) {
            window.location = url;
        },
        _constructCSVUrl: function (filterModel, filterAttributes, url) {
            filterAttributes.forEach(function (filterAttribute) {
                Y.Object.each(filterModel.getAttrs(), function (value, key) {
                    if (filterAttribute === key && value && (!Array.isArray(value) || Array.isArray(value) && value.length > 0 )) {
                        url = url + "&" + Y.QueryString.stringify(value, null, key);
                    }
                }, this);
            });

            return url;
        }

    }, {
        ATTRS: {
            personAccessCSVUrl: {
                value: ''
            },
            eventLogCSVUrl: {
                value: ''
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'promise',
        'audit-personaccess-accordion-views',
        'audit-eventlog-accordion-views',
        'querystring-stringify'
    ]
});