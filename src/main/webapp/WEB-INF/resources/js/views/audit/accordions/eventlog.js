YUI.add('audit-eventlog-accordion-views', function (Y) {
    'use-strict';

    var USPFormatters = Y.usp.ColumnFormatters,
        APPFormatters = Y.app.ColumnFormatters;

    Y.namespace('app.audit').EventLogResults = Y.Base.create('eventLogResults', Y.usp.app.Results, [], {
        getResultsListModel: function (config) {
            return new Y.app.audit.PaginatedFilteredEventLogList({
                url: config.url,
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function (config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                key: 'eventDate',
                label: labels.eventDate,
                formatter: USPFormatters.dateTime,
                sortable: false,
                width: '20%'
            }, {
                key: 'eventSubject!name',
                label: labels.clientName,
                sortable: false,
                width: '20%'
            }, {
                key: 'eventCode',
                label: labels.eventCode,
                sortable: false,
                width: '20%'
            }, {
                key: 'eventSummary',
                label: labels.eventSummary,
                sortable: false,
                width: '20%'
            }, {
                key: 'instigatingUserName',
                label: labels.userName,
                sortable: false,
                width: '20%'
            }];
        },
        render: function () {
            var labels = this.get('searchConfig').labels || {},
                permissions = this.get('searchConfig').permissions || {};

            Y.app.audit.EventLogResults.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this.toolbar.addButton({
                icon: 'fa fa-download',
                className: 'pull-right pure-button-active eventLogDownload',
                action: 'eventLogDownload',
                title: labels.exportTitle,
                ariaLabel: labels.exportAriaLabel,
                label: labels.exportLabel
            });

            this.toolbar.addButton({
                icon: 'fa fa-filter',
                className: 'pull-right pure-button-active filter',
                action: 'filter',
                title: labels.filterTitle,
                ariaLabel: labels.filterAriaLabel,
                label: labels.filterLabel
            });

            return this;
        },
    });

    Y.namespace('app.audit').PaginatedFilteredEventLogList = Y.Base.create("paginatedFilteredEventLogList", Y.usp.audit.PaginatedAuditEventLogEntryList, [Y.usp.ResultsFilterURLMixin], {
        whitelist: ["dateFrom", "dateTo", "subjectId", "userId", "code"],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app.audit').EventLogFilteredResults = Y.Base.create('eventLogFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.audit.EventLogFilter,
        resultsType: Y.app.audit.EventLogResults,
        initializer: function () {
            this._events = [
                this.get('results').on('*:filter', this._handleFilterButtonClick, this)
            ];
        },
        _handleFilterButtonClick: function (e) {
            var filterVisible = this.get('filter').get('showFilter');

            this.get('filter').set('showFilter', (filterVisible) ? false : true);
        }
    });

    Y.namespace('app.audit').EventLogAccordionView = Y.Base.create('eventLogAccordionView', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            this.eventLogResults = new Y.app.audit.EventLogFilteredResults(config).addTarget(this);
        },

        render: function () {
            Y.app.audit.EventLogAccordionView.superclass.render.call(this);

            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.eventLogResults.render().get('container'));
            this.getAccordionBody().setHTML(contentNode);

            return this;
        },

        destructor: function () {
            this.eventLogResults.removeTarget(this);
            this.eventLogResults.destroy();
            delete this.eventLogResults;
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'results-formatters',
        'app-results',
        'event-custom',
        'usp-audit-AuditEventLogEntry',
        'app-toolbar'
    ]
});