YUI.add('adoptinformation-view', function(Y) {

    'use-strict';
    
    Y.namespace('app.adoptioninformation').AdoptionInformationView = Y.Base.create('adoptionInformationView', Y.View, [], {

        initializer: function() {
            var createFactory = function(type) {
                return React.createElement.bind(null, type);
            };

            this.componentViewFactory = createFactory(this.getComponentType());
        },

        render: function() {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            //create and reference a container for our component
            this.componentContainer = contentNode.appendChild(Y.Lang.sub('<div class="component-container" />'));

            //now render the component
            this.renderComponent();

            //finally set the content into the view container
            this.get('container').appendChild(contentNode);

            return this;
        },

        renderComponent: function() {
            var componentConfig = this.getComponentConfig();
            var component;

            ReactDOM.render(this.componentViewFactory(componentConfig), this.componentContainer.getDOMNode(), function() {
                component = this;
            });
            this.component = component;
        },

        getComponentConfig: function() {
            return this.get('componentConfig');
        },

        getComponentType: function() {
            return this.get('componentType');
        },

        destructor: function() {
            //unmount existing component
            if (this.componentContainer) {
                ReactDOM.unmountComponentAtNode(this.componentContainer.getDOMNode());
                this.componentContainer.destroy(true);
                delete this.componentContainer;
            }

            if (this.component) {
                delete this.component;
            }
        },

        refresh: function() {
            if (this.component) {
                this.component.loadData();
            }
        }
    }, {
        ATTRS: {
            componentConfig: {
                value: {}
            },
            componentType: {
                value: null
            }
        }
    });
    
}, '0.0.1', {
    requires: [
        'yui-base',
        'view'
    ]
});