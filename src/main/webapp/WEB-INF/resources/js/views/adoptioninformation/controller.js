YUI.add('adoptinformation-controller-view', function(Y) {

    'use-strict';

    Y.namespace('app.adoptioninformation').AdoptionInformationControllerView = Y.Base.create('adoptionInformationControllerView', Y.View, [], {

        events: {
            '.component-container > div': {
                'addAdoptionInformation': 'showAdoptionInformationAddDialog',
                'viewAdoptionInformation': 'showAdoptionInformationViewDialog',
                'editAdoptionInformation': 'showAdoptionInformationEditDialog'
                               	
            }
        },

        initializer: function(config) {
            this.adoptionInformationDialog = new Y.app.childlookedafter.AdoptionInformationDialog(config.dialogConfig.adoptionInformation).addTarget(this).render();

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            //load personDetailsPromise as a promise to use as default values
            var personDetailsPromise = new Y.Promise(function(resolve, reject) {

                this.get('personDetails').load(function(err, res) {
                    err ? reject(err) : resolve(JSON.parse(res.response));
                });
            }.bind(this));

            personDetailsPromise.then(function(data) {
                //Check permission and style  button accordingly
                this._displayAddButtonCheck(data);

            }.bind(this));
            

            this.adoptionInformationView = new Y.app.adoptioninformation.AdoptionInformationView({
                componentType: uspAdoptioninformation.AdoptionInformationView,
                componentConfig: {
                    labels: config.labels.adoptionInformationView,
                    subject: config.subject,
                    url: config.url,
                    pictureUrl: config.pictureUrl,
                    permissions: config.permissions,
                    codedEntries:{
                      adopterLegalStatus: Y.uspCategory.childlookedafter.adopterLegalStatus.category
                    }
                }
            }).addTarget(this);

            this._evtHandlers = [
                this.on('*:addAdoptionInformation', this.showAdoptionInformationAddDialog, this),
                this.on('*:showAdoptionInformationEditDialog', this.showAdoptionInformationEditDialog, this),
                this.on('*:saved', this.handleSave, this)
            ];
        },

        render: function() {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(this.adoptionInformationView.render().get('container'));
            this.get('container').append(contentNode);
            return this;
        },

        destructor: function() {
            this._evtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.adoptionInformationView.removeTarget(this);
            this.adoptionInformationView.destroy();
            delete this.adoptionInformationView;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;
            
            this.adoptionInformationDialog.removeTarget(this);
            this.adoptionInformationDialog.destroy();
            delete this.adoptionInformationDialog();
        },

        showAdoptionInformationAddDialog: function(e) {
            var config = this.get('dialogConfig.adoptionInformation.views.add.config');
            this.adoptionInformationDialog.showView('add', {
                model: new Y.app.childlookedafter.NewAdoptionInformationForm({
                    url: config.url
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, function() {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        
        showAdoptionInformationViewDialog: function(e) {
        	var config = this.get('dialogConfig.adoptionInformation.views.view.config');
        	
            this.adoptionInformationDialog.showView('view', {
                model: new Y.app.childlookedafter.AdoptionInformationForm({
                    url: Y.Lang.sub(config.url, {
                        id: e._event.detail.adoptionInformationId
                    })
                }),
                otherData: {
                    subject: this.get('subject'),
                    id: e._event.detail.adoptionInformationId
                }
            }, {
                modelLoad: true
            }, function(view) {
                if (!this.views.view.permissions.canUpdateAdoptionInformation) {
                    this.removeButton('editButton', Y.WidgetStdMod.FOOTER);
                }
            });
        },
        
        showAdoptionInformationEditDialog: function(e) {
        	var config = this.get('dialogConfig.adoptionInformation.views.edit.config');
        	var adoptionInformationId = (e._event.detail) ? 
        		e._event.detail.adoptionInformationId : 
        		e._event.details[0].adoptionInformationId;
        	
        	this.adoptionInformationDialog.showView('edit', {
                model: new Y.app.childlookedafter.UpdateAdoptionInformationForm({
                    url: Y.Lang.sub(config.url, {
                        id: adoptionInformationId
                    })
                }),
                otherData: {
                    subject: this.get('subject'),
                    id: adoptionInformationId
                }
            }, {
                modelLoad: true
            }, function(view) {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        	
        },

        handleSave: function(e) {
            //refresh the view on adding a new record
            this.adoptionInformationView.refresh();
        },
        chkAddPeriodButton: function() {
            if (this.get('permissions.canAddAdoptionInformation')) {
            	this.toolbar.enableButton('addAdoptionInformation');
            } else {
            	this.toolbar.disableButton('addAdoptionInformation');
            }
        },

        
        _displayAddButtonCheck: function(personData) {
          var link = this.toolbar.get('toolbarNode');
          if (!this._hasDemographics(personData)) {
            link.one('.h3.pop-up-data').removeClass('hidden');
            this.toolbar.hideButton('addAdoptionInformation', true);
            
          } else {
            this.chkAddPeriodButton();
          }
        },
        
        _hasDemographics : function(personData) {
          if(personData.lifeState === 'UNBORN' || !personData.ethnicity || personData.ethnicity === 'NOT_KNOWN'
          || personData.gender === 'UNKNOWN' || personData.gender === 'INDETERMINATE' || !personData.dateOfBirth || personData.dateOfBirthEstimated) {
            return false;
          }
          return true;
        }
    }, {
        ATTRS: {
            personDetails: {
                valueFn: function() {
                    return new Y.usp.relationshipsrecording.PersonDetails({
                        url: this.get('dialogConfig.adoptionInformation.personDetailsModelURL')
                    })
                }
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-toolbar',
        'adoptinformation-view',
        'adoptioninformation-dialog',
        'categories-childlookedafter-component-AdopterLegalStatus',
        'usp-relationshipsrecording-PersonDetails',
        'childlookedafter-view'
        
]
});