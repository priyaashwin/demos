

YUI.add('app-reference-numbers-accordion', function(Y) {	

	Y.namespace('app.views').ReferenceNumbersAccordion = Y.Base.create('referenceNumbersAccordion', Y.usp.AccordionPanel, [], {
    	
		initializer: function(config) {

    		this.ReferenceNumbersView = new Y.app.views.ReferenceNumbers(Y.mix(config));
    		this.ReferenceNumbersView.addTarget(this);
    	},
        render:function(){

        	Y.app.views.ReferenceNumbersAccordion.superclass.render.call(this);
        	this.getAccordionBody().appendChild(this.ReferenceNumbersView.render().get('container'));

        	return this;
        },
        destructor:function(){
        	
        	if(this.ReferenceNumbersView) {
        		this.ReferenceNumbersView.destroy();
        		delete this.ReferenceNumbersView;
        	}

        }
    });
	
	
}, '0.0.1', {
    requires: ['yui-base',               
               'accordion-panel',
               'app-reference-numbers-view'
              ]
});