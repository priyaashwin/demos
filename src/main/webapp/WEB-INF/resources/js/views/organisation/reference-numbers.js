YUI.add('app-reference-numbers-view', function(Y) {

    'use-strict';

    var EVENT_SHOW_DIALOG = 'referenceNumber:showDialog';
    var ACTION_ADD = 'addReferenceNumber';
    var ACTION_EDIT_REMOVE = 'editRemoveReferenceNumber',

        CONTEXT_UPDATE_EVENT = 'contentContext:update';
    var O = Y.Object;

    Y.namespace('app.views').ReferenceNumbers = Y.Base.create('referenceNumbersView', Y.usp.organisation.OrganisationReferenceNumberView, [], {
        events: {
            '.add-referenceNumber': {
                click: '_addReferenceNumber'
            },
            '.editRemove-referenceNumber': {
                click: '_editRemoveReferenceNumber'
            }
        },

        template: Y.Handlebars.templates['personReferenceNumberIdentificationView'],

        //function to match Active CodedEntries with that of Active ReferenceNumbers associated with person
        checkActiveReferenceTypes: function(referenceNumbers) {

            // Count active coded entries for referenceType

            var countActiveTypes = 0;
            var activeReferenceTypes = this.get('referenceTypes');

            O.each(activeReferenceTypes, function() {
                countActiveTypes += 1;
            });

            var countActiveNumbers = 0;
            var activeNumbersArray = [];

            if (referenceNumbers) {
                O.each(referenceNumbers, function(referenceNumber) {
                    if (Y.uspCategory.organisation.referenceNumberType.category.codedEntries[referenceNumber.type].active) {
                        countActiveNumbers += 1;
                        activeNumbersArray.push(referenceNumber.type);
                    }
                });
                //set view attribute to pass the array to organisationDialog
                this.activeNumbers = activeNumbersArray;
            }

            // Hide add button when there are equal number of activeTypes and activeNumbers.
            if (countActiveTypes == countActiveNumbers) {
                return false;
            }

            // show add button by default
            return true;

        },

        initializer: function() {
            var modelList = this.get('modelList');
            this._evtHandlers = [];
            this._evtHandlers.push(this.get('modelList').after('load', this.render, this));

            this.publish('editRemoveReferenceNumber', {
                preventable: true
            });
            this.publish('addReferenceNumber', {
                preventable: true
            });
            this._evtHandlers.push(Y.on(CONTEXT_UPDATE_EVENT, this._updateContextName, this));
        },

        render: function() {

            var data = {
                referenceNumbers: this.get('modelList').toJSON()
            };

            var html = this.template({
                labels: this.get('labels'),
                modelData: data,
                codedEntries: this.get('codedEntries'),
                showAdd: this.checkActiveReferenceTypes(this.get('modelList').toJSON()),
                orgComponent: true,
                canAddReferenceNumber: this.get('canAddReferenceNumber') ? '' : 'none;',
                canEditReferenceNumber: this.get('canUpdateReferenceNumber') ? '' : 'none;',
                canRemoveReferenceNumber: this.get('canRemoveReferenceNumber') ? '' : 'none;'
            });

            this.get('container').setHTML(html);

            return this;
        },

        _editRemoveReferenceNumber: function(e) {
            var model = this.get('model');
            e.preventDefault();

            Y.fire(EVENT_SHOW_DIALOG, {
                action: ACTION_EDIT_REMOVE,
                contextId: this.get('contextId'),
                contextName: this.get('contextName'),
                contextType: this.get('contextType'),
                canRemoveReferenceNumber: this.get('canRemoveReferenceNumber'),
                activeReferenceNumbers: this.activeNumbers,
                id: model.get('id')
            });
        },

        _addReferenceNumber: function(e) {

            e.preventDefault();

            switch (this.get('contextType')) {

                case 'organisation':

                    Y.fire(EVENT_SHOW_DIALOG, {
                        action: ACTION_ADD,
                        contextId: this.get('contextId'),
                        contextName: this.get('contextName'),
                        contextType: this.get('contextType'),
                        activeReferenceNumbers: this.activeNumbers
                    });

                    break;

                case 'person':

                    Y.fire(EVENT_SHOW_DIALOG, {
                        action: ACTION_ADD,
                        contextId: this.get('contextId'),
                        contextName: this.get('contextName'),
                        contextType: this.get('contextType'),
                        activeReferenceNumbers: this.activeNumbers
                    });

                    break;
            };



        },
        /**
         * @method _updateContextName
         * @description Person and Organisation have differently shaped data. We use contextType to help determine where that is. 
         * Whenever name is changed from different places , it needs to be reflected elsewhere in the system
         */
        _updateContextName: function(e) {
            switch (this.get('contextType')) {

                case 'organisation':
                    name = this.set('contextName', e.data.name || '');
                    break;

                case 'person':
                    Y.bind(function(e) {
                        this.get('model').setAttrs(e.data);
                    }, this);
                    break;
            };
        }
    }, {
        ATTRS: {

            codedEntries: {
                getter: function() {

                    var codes = {};

                    if (this.get('contextType') === 'organisation') {
                        codes.referenceNumberType = Y.uspCategory.organisation.referenceNumberType.category.codedEntries;
                    } else if (this.get('contextType') === 'person') {
                        codes.referenceNumberType = Y.uspCategory.person.referenceType.category.codedEntries;
                    }

                    return codes;

                }
            },

            referenceTypes: {
                getter: function() {
                    if (this.get('contextType') === 'organisation') {
                        return Y.uspCategory.organisation.referenceNumberType.category.getActiveCodedEntries();
                    } else if (this.get('contextType') === 'person') {
                        return Y.secured.uspCategory.person.referenceType.category.getActiveCodedEntries(null,'write');
                    }

                }
            },

            contextId: {
                value: ''
            },

            contextName: {
                value: ''
            },

            contextType: {
                value: ''
            },

            activeNumbers: {

            },

            modelList: {
                value: ''
            }

        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'handlebars-base',
        'handlebars-helpers',
        'event-custom',
        'usp-person-PersonReferenceNumber',
        'usp-organisation-OrganisationReferenceNumber',
        'secured-categories-person-component-Referencetype',
        'categories-organisation-component-ReferenceNumberType',
        'app-accordion-toggle-all',
        'escape',
        'info-message'
    ]
});