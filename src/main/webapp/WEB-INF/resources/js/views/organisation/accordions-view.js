YUI.add('app-organisation-accordions', function (Y) {

    var L = Y.Lang;

    var OrganisationTreeView = Y.Base.create('organisationTreeView', Y.usp.ReactView, [], {
        events: {
            '.usp-accordion': {
                'organisation:view': 'handleView'
            }
        },
        reactFactoryType: uspViewOrganisation.OrganisationTree,
        handleView: function (e) {
            var eventDetail = e._event.detail || {};
            if (eventDetail.organisationId !== undefined) {
                window.location = L.sub(this.get('viewOrganisationURL'), {
                    id: eventDetail.organisationId
                });
            }
        },
        render: function () {
            var model = this.get('model');

            //only render if this is not a single root object
            if (model.get('rightNum') && model.get('rightNum') > 2) {
                OrganisationTreeView.superclass.render.call(this);
            }
            return this;
        }
    }, {
        ATTRS: {
            properties: {
                value: {
                    endpoints: {
                        organisationEndpoint: ''
                    }
                }
            },
            viewOrganisationURL: {}
        }
    });

    Y.namespace('app.views').OrganisationAccordions = Y.Base.create('organisationAccordions', Y.View, [], {
        /**
         * @method initializer
         * @description contactDetailsAccordion and it's sub-view have a modelList passed in the 'config.contactDetailsConfig'
         */
        initializer: function (config) {

            var model = this.get('model');

            this.identificationDetailsAccordion = new Y.app.views.OrganisationIdentificationDetailsAccordion(Y.merge(config.identificationDetailsConfig, {
                model: model
            }));

            this.organisationTree = new OrganisationTreeView(Y.merge(config.organisationTreeConfig, {
                model: model
            }));

            this.contactDetailsAccordion = new Y.app.views.ContactDetailsAccordion(config.contactDetailsConfig);

            this.addressesAccordion = new Y.app.views.AddressesAccordion(Y.merge(config.addressesConfig, {
                model: model
            }));

            this.referenceNumbersAccordion = new Y.app.views.ReferenceNumbersAccordion(Y.merge(config.referenceNumbersConfig, {
                model: model
            }));

            this.identificationDetailsAccordion.addTarget(this);
            this.contactDetailsAccordion.addTarget(this);
            this.addressesAccordion.addTarget(this);
            this.referenceNumbersAccordion.addTarget(this);

            this.plug(Y.Plugin.app.AccordionToggleAllPlugin, {
                srcNode: '#collapse-button',
            });

        },
        /**
         * @method destructor
         */
        destructor: function () {

            if (this._evtHandlers) {
                Y.Array(this._evtHandlers, function (item) {
                    item.detach();
                });
            }

            if (this.identificationDetailsAccordion) {
                this.identificationDetailsAccordion.destroy();
                delete this.identificationDetailsAccordion;
            }

            if (this.addressesAccordion) {
                this.addressesAccordion.destroy();
                delete this.addressesAccordion;
            }

            if (this.contactDetailsAccordion) {
                this.contactDetailsAccordion.destroy();
                delete this.contactDetailsAccordion;
            }

            if (this.referenceNumbersAccordion) {
                this.referenceNumbersAccordion.destroy();
                delete this.referenceNumbersAccordion;
            }

            if (this.organisationTree) {
                this.organisationTree.destroy();

                delete this.organisationTree;
            }

        },
        /**
         * @method render
         */
        render: function () {

            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.identificationDetailsAccordion.render().get('container'));
            contentNode.append(this.organisationTree.render().get('container'));
            contentNode.append(this.addressesAccordion.render().get('container'));
            contentNode.append(this.contactDetailsAccordion.render().get('container'));
            contentNode.append(this.referenceNumbersAccordion.render().get('container'));

            this.get('container').setHTML(contentNode);

            return this;

        }
    }, {
        ATTRS: {
            /**
             * @attribute model
             * @description Not sure if this will be shared across multiple accordions and their views yet
             */
            model: {
                value: {}
            },
            /**
             * @attribute addressesConfig
             */
            addressesConfig: {
                value: {
                    labels: {}
                }
            },
            /**
             * @attribute identificationDetailsConfig
             */
            identificationDetailsConfig: {
                value: {
                    labels: {}
                }
            },
            /**
             * @attribute contactDetailsConfig
             */
            contactDetailsConfig: {
                value: {
                    labels: {}
                }
            },
            /**
             * @attribute referenceNumbersConfig
             */
            referenceNumbersConfig: {
                value: {
                    labels: {}
                }
            },
            toolbarNode: {
                getter: Y.one,
                writeOnce: true
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'app-accordion-toggle-all',
        'app-organisation-identification-details-accordion',
        'app-addresses-accordion',
        'app-contact-details-accordion',
        'app-reference-numbers-accordion',
        'usp-react-view'
    ]
});