YUI.add('app-organisation-identification-details-view', function (Y) {

    'use-strict';

    var ACTION_EDIT = 'editOrganisation',

        ACTION_END = 'endOrganisation',

        ACTION_OWNERSHIP = 'addOrganisationOwnership',

        EVENT_SHOW_DIALOG = 'organisation:showDialog',

        CSS_EDIT_BUTTON = 'pure-button pure-button-active floatRightButtonMargin',

        HTML_BUTTON = '<button type="button" class="{css}" data-trigger="{action}" data-id="{id}"><i class="fa fa-pencil-square-o"></i>{label}</button>',

        HTML_END_BUTTON = '<button type="button" class="{css}" data-trigger="{action}" data-id="{id}"><i class="fa fa-times-circle"></i>{label}</button>',

        HTML_OWNERSHIP_BUTTON = '<button type="button" class="{css}" data-trigger="{action}" data-id="{id}"><i class="fa fa-share-alt"></i>{label}</button>';

    Y.namespace('app.views').OrganisationIdentificationDetails = Y.Base.create('organisationIdentificationDetails', Y.usp.organisation.OrganisationView, [], {

        template: Y.Handlebars.templates['organisationDetailsView'],

        events: {
            'button[data-trigger]': {
                click: '_onClick',
            },

            '.change-OrganisationName': {
                click: '_changeOrganisationName'
            },
        },
        render: function () {
            var html = this.template({

                labels: this.get('labels'),
                codedEntries: this.get('codedEntries'),
                previousNames: this.previousNamesSetup(this.get('model')),
                modelData: this.get('model').toJSON(),
                canChangeOrganisationName: this.get('canChangeOrganisationName') ? '' : 'none;',
                teamView: this.get('model').toJSON().organisationType === 'TEAM'
            });
            this.get('container').setHTML(html);
            this.renderButtons();
            return this;
        },

        previousNamesSetup: function (model) {
            var otherNamesList = model.get('previousNames');
            if (otherNamesList) {
                var previousNames = [];

                function storePreviousName(otherName) {
                    if (otherName.type === "PREVIOUS") {
                        previousNames.push(otherName.name);
                    }

                }

                otherNamesList.forEach(storePreviousName);

                return previousNames;
            }
        },
        renderButtons: function () {

            var container = this.get('container'),
                buttonHolder = '';

            var isTeam = this.get('model').get('organisationType') === 'TEAM';

            buttonHolder = container.one('.pure-button-group.button-holder');
            if (buttonHolder && this.get('canEditOrganisation')) {
                buttonHolder.appendChild(Y.Lang.sub(HTML_BUTTON, {
                    action: ACTION_EDIT,
                    css: CSS_EDIT_BUTTON,
                    id: this.get('model').get('id'),
                    label: this.get('labels').editButtonLabel
                }));
            }

            if (buttonHolder && (this.get('canEndTeam') && isTeam) || (this.get('canEndOrganisation') && !isTeam)) {
                if (null == this.get('model').get('endDate')) {
                    buttonHolder.appendChild(Y.Lang.sub(HTML_END_BUTTON, {
                        action: ACTION_END,
                        css: CSS_EDIT_BUTTON,
                        id: this.get('model').get('id'),
                        label: this.get('labels').endButtonLabel
                    }));
                }
            }
            if (buttonHolder && (this.get('canAddOrganisationOwnership'))) {
                buttonHolder.appendChild(Y.Lang.sub(HTML_OWNERSHIP_BUTTON, {
                    action: ACTION_OWNERSHIP,
                    css: CSS_EDIT_BUTTON,
                    id: this.get('model').get('id'),
                    label: this.get('labels').changeParentLabel
                }));
            }
        },

        _onClick: function (e) {
            var t = e.currentTarget;

            e.preventDefault();

            Y.fire(EVENT_SHOW_DIALOG, {
                action: t.getAttribute('data-trigger'),
                id: t.getAttribute('data-id'),
                isTeam: this.get('model').get('organisationType') === 'TEAM',
                objectVersion: this.get('model').get('objectVersion')
            });
        },

        _changeOrganisationName: function (e) {
            var t = e.currentTarget;

            e.preventDefault();

            Y.fire(EVENT_SHOW_DIALOG, {
                action: t.getAttribute('data-trigger'),
                id: this.get('model').get('id')
            });
        }

    }, {

        ATTRS: {

            codedEntries: {
                value: {
                    organisationType: Y.uspCategory.organisation.organisationType.category.codedEntries,
                    organisationSubType: Y.uspCategory.organisation.organisationSubType.category.codedEntries
                }
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'usp-organisation-Organisation',
        'handlebars-organisation-templates',
        'categories-organisation-component-OrganisationType',
        'categories-organisation-component-OrganisationSubType',
        'usp-organisation-OrganisationWithPreviousNames',
        'info-message',
        'event-custom'
    ]
});