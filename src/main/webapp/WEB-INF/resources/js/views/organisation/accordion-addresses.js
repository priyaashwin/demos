YUI.add('app-addresses-accordion', function (Y) {

    var USPFormatters = Y.usp.ColumnFormatters,

        ACTION_ADD_ADDRESS = 'addAddress',
        ACTION_END_ADDRESS = 'endAddress',
        ACTION_REMOVE_ADDRESS = 'removeAddress',
        ACTION_UPDATE_ADDRESS_REOPEN = 'updateAddressReopen',
        ACTION_UPDATE_START_END_ADDRESS_DATES = 'updateStartEndAddressDates',
        ACTION_MOVE_ADDRESS = 'moveAddressLocation',
        EVENT_SHOW_DIALOG = 'address:showDialog',
        EVENT_REFRESH_RESULTS = 'address:refreshResults',
        CONTEXT_UPDATE_EVENT = 'contentContext:update',
        HTML_ADD_BUTTON = '<div class="pull-right"><button class="pure-button-active pure-button {id}"><i class="fa fa-plus-circle"></i> {label}</button></div>';

    Y.Do.after(function () {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({ key: 'startDate!calculatedDate', locator: 'startDate.calculatedDate' });
        schema.resultFields.push({ key: 'endDate!calculatedDate', locator: 'endDate.calculatedDate' });
        return new Y.Do.AlterReturn(null, schema);
    }, Y.usp.organisation.OrganisationAddress, 'getSchema', Y.usp.organisation.OrganisationAddress);

    /**
     * if ever used for person...
     */
    Y.Do.after(function () {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({ key: 'startDate!calculatedDate', locator: 'startDate.calculatedDate' });
        schema.resultFields.push({ key: 'endDate!calculatedDate', locator: 'endDate.calculatedDate' });
        return new Y.Do.AlterReturn(null, schema);
    }, Y.usp.person.PersonAddress, 'getSchema', Y.usp.person.PersonAddress);

    Y.namespace('app.views').AddressesAccordion = Y.Base.create('addressesAccordion', Y.usp.AccordionTablePanel, [], {

        events: {
            '.DND-address-toggle': {
                click: '_handleDoNotDiscloseClick'
            }
        },

        initializer: function (config) {
            this.events = Y.merge(this.events, Y.app.views.AddressesAccordion.superclass.events);

            var codedEntries, labels;

            config = config || {};
            labels = config.labels || {};

            codedEntries = this.get('codedEntries');

            this._actions = [];
            this._actions.push({
                clazz: 'closeAddress',
                title: 'End address',
                label: labels.endAction,
                visible: function () {
                    return (this.record.get('temporalStatus') !== 'INACTIVE_HISTORIC') && config.canEndAddress;
                }
            });
            this._actions.push({ clazz: 'removeAddress', title: 'Remove address', label: labels.removeAction, visible: config.canRemoveAddress });
            this._actions.push({
                clazz: 'updateStartEndAddressDates',
                title: labels.updateStartEndAddressDates,
                label: labels.updateStartEndAddressDates,
                visible: function () {
                    return (this.record.get('type') === 'WORK' && this.record.get('usage') === 'PERMANENT' && config.canUpdateAddressStartEnd)
                }
            });
            this._actions.push({
                clazz: 'updateAddressReopen',
                title: labels.updateAddressReopen,
                label: labels.updateAddressReopen,
                visible: function () {
                    return (this.record.get('type') === 'WORK' && this.record.get('usage') === 'PERMANENT' && config.canUpdateAddressReopen && this.record.get('endDate'))
                }
            });
            this._actions.push({
                clazz: 'moveAddressLocation',
                title: 'Move address',
                label: labels.moveAddressLocation,
                visible: function () {
                    return (
                        this.record.get('type') == 'WORK' &&
                        this.record.get('usage') == 'PERMANENT' &&
                        config.canUpdateResidentLocation &&
                        !Y.uspCategory.address.type.category.codedEntries[
                            this.record.get('type')
                        ].notManuallyAssignable
                    );
                }
            });

            this._columns = [];
            this._columns.push({ key: 'location!location', label: labels.address, width: '27%', allowHTML: true, labels: labels, formatter: USPFormatters.addressLocationGoogleMapLink });
            this._columns.push({ key: 'doNotDisclose', label: labels.doNotDisclose, width: '8%', allowHTML: true, labels: labels, formatter: USPFormatters.iconDND, permissions: config.doNotDisclosePemissions});
            this._columns.push({ key: 'startDate!calculatedDate', label: labels.startDate, formatter: USPFormatters.fuzzyDate, maskField: 'startDate.fuzzyDateMask', sortable: true, width: '10%' });
            this._columns.push({ key: 'endDate!calculatedDate', label: labels.endDate, formatter: USPFormatters.fuzzyDate, maskField: 'endDate.fuzzyDateMask', sortable: true, width: '10%' });
            this._columns.push({ key: 'type', label: labels.type, formatter: USPFormatters.codedEntry, codedEntries: codedEntries.addressType, sortable: true, width: '10%' });
            this._columns.push({ key: 'usage', label: labels.usage, formatter: USPFormatters.codedEntry, codedEntries: codedEntries.addressUsage, sortable: true, width: '10%' });
            this._columns.push({ label: labels.addressResultsActions, className: 'pure-table-actions', allowHTML: true, width: '9%', formatter: USPFormatters.actions, items: this._actions });

            this.results = new Y.usp.PaginatedResultsTable({
                sortBy: [{ 'startDate!calculatedDate': 'desc' }, { 'endDate!calculatedDate': 'desc' }],
                columns: this._columns,
                data: new Y.usp.organisation.PaginatedOrganisationAddressList({
                    url: config.url
                }),
                noDataMessage: config.noDataMessage,
                plugins: [{ fn: Y.Plugin.usp.ResultsTableMenuPlugin }, { fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin }]
            });

            this._evtHandlers = [
                Y.on(['organisation:addressChanged', 'addressDoNotDisclose:addressChanged'], this.reload, this),
                Y.on('addressDoNotDisclose:error', this._handlDoNotDiscloseError, this),
                this.get('container').delegate('click', this._handleAddButton, '.table-header .pure-button', this),
                this.results.delegate('click', this._handleTableRow, '.pure-table-data .pure-menu-item a', this),
                Y.on(EVENT_REFRESH_RESULTS, this.reload, this),
                Y.on(CONTEXT_UPDATE_EVENT, this._updateContextName, this)
            ];

            //plug in the errors handling
            this.plug(Y.Plugin.usp.ModelErrorsPlugin);
        },

        destructor: function () {

            if (this._evtHandlers) {
                Y.Array.each(this._evtHandlers, function (item) {
                    item.detach();
                });
            };

            this._evtHandlers = null;
            this.results.destroy();
            delete this.results;

            //remove ModelErrors plugin
            this.unplug(Y.Plugin.usp.ModelErrorsPlugin);
        },

        render: function () {

            Y.app.views.AddressesAccordion.superclass.render.call(this);

            var labels = this.get('labels') || {};

            if (this.get('canAddAddress')) {
                this.addButton = this.getButtonHolder().appendChild(Y.Lang.sub(HTML_ADD_BUTTON, {
                    id: 'addAddress',
                    label: labels.addButtonLabel
                }));
            }

            this.renderResults(this.results);

            return this;
        },

        reload: function () {
            this.results.reload();
        },
        /**
         * @method _updateContextName
         * @description Person and Organisation have differently shaped data. We use contextType to help determine where that is. 
         * Whenever name is changed from different places , it needs to be reflected elsewhere in the system
         */
        _updateContextName: function (e) {
            switch (this.get('contextType')) {

            case 'organisation':
                name = this.set('contextName', e.data.name || '');
                break;

            case 'person':
                Y.bind(function (e) {
                    this.get('model').setAttrs(e.data);
                }, this);
                break;
            };
        },

        _handleAddButton: function (e) {

            var id = '',
                name = '';

            e.preventDefault();

            switch (this.get('contextType')) {

            case 'organisation':
                id = this.get('contextId');
                name = this.get('contextName');
                break;

            case 'person':
                id = this.get('model').get('personIdentifier');
                name = this.get('model').get('name');
                break;
            };

            Y.fire(EVENT_SHOW_DIALOG, {
                action: ACTION_ADD_ADDRESS, // note person action is addPersonAddress, so rename or provide two different actions.
                contextId: id,
                contextName: name,
                contextType: this.get('contextType')
            });

        },

        _handleTableRow: function (e) {
            var id = '',
                name = '';

            e.preventDefault();

            switch (this.get('contextType')) {

            case 'organisation':
                id = this.get('contextId');
                name = this.get('contextName');
                break;

            case 'person':
                id = this.get('model').get('personIdentifier');
                name = this.get('model').get('name');
                break;
            };
            var target = e.currentTarget,
                resultAction = null,
                record = this.results.getRecord(target.get("id")),
                rowAddressId = record.get('id'),
                locationId = record.get('location').id;
            if (target.hasClass('closeAddress')) {

                resultAction = ACTION_END_ADDRESS;
            } else if (target.hasClass('removeAddress')) {
                resultAction = ACTION_REMOVE_ADDRESS;
            } else if (target.hasClass('updateStartEndAddressDates')) {
                resultAction = ACTION_UPDATE_START_END_ADDRESS_DATES;
            } else if (target.hasClass('updateAddressReopen')) {
                resultAction = ACTION_UPDATE_ADDRESS_REOPEN;
            } else if (target.hasClass('moveAddressLocation')) {
                resultAction = ACTION_MOVE_ADDRESS;
            }

            Y.fire(EVENT_SHOW_DIALOG, {
                action: resultAction, // note person action is addPersonAddress, so rename or provide two different actions.
                contextId: id,
                contextName: name,
                contextType: this.get('contextType'),
                addressId: rowAddressId,
                locationId: locationId
            });
        },

        _handleDoNotDiscloseClick: function (e) {
            var urls = this.get('doNotDiscloseURLs');
            Y.app.address.ToggleDoNotDiscloseSwitch.handleDoNotDiscloseClick(e, urls);
        },

        _handlDoNotDiscloseError: function(e) {
            //fire error event so plugin can pick that
            this.fire('error', {error: e});
        }

    }, {

        ATTRS: {

            canAddAddress: {
                value: false
            },

            canEndAddress: {
                value: false
            },

            canRemoveAddress: {
                value: false
            },

            codedEntries: {
                value: {
                    addressType: Y.uspCategory.address.type.category.codedEntries,
                    addressUsage: Y.uspCategory.address.usage.category.codedEntries
                }
            },

            labels: {
                value: {}
            },

            noDataMessage: {
                value: ''
            },

            title: {
                value: ''
            },

            url: {
                value: {}
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'event-custom',
        'paginated-results-table',
        'accordion-panel',
        'results-formatters',
        'results-templates',
        'results-table-keyboard-nav-plugin',
        'results-table-menu-plugin',
        'categories-address-component-Type',
        'categories-address-component-Usage',
        'usp-person-PersonAddress',
        'usp-organisation-OrganisationAddress',
        'app-address-switch-do-not-disclose',
        'model-errors-plugin'
    ]
});