

YUI.add('app-contact-details-accordion', function(Y) {	

	Y.namespace('app.views').ContactDetailsAccordion = Y.Base.create('contactDetailsAccordion', Y.usp.AccordionPanel, [], {
    	
		initializer: function(config) {

    		this.ContactDetailsView = new Y.app.views.ContactDetails(Y.mix(config));
    		this.ContactDetailsView.addTarget(this);
    	},
        render:function(){

        	Y.app.views.ContactDetailsAccordion.superclass.render.call(this);
        	this.getAccordionBody().appendChild(this.ContactDetailsView.render().get('container'));

        	return this;
        },
        destructor:function(){
        	
        	if(this.ContactDetailsView) {
        		this.ContactDetailsView.destroy();
        		delete this.ContactDetailsView;
        	}

        }
    });
	
	
}, '0.0.1', {
    requires: ['yui-base',               
               'accordion-panel',
               'app-contact-details-view'
              ]
});