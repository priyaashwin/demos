YUI.add('app-contact-details-view', function (Y) {

    var A = Y.Array,

        ACTION_ADD_CONTACT = 'addContact',

        ACTION_EDIT_REMOVE_CONTACT = 'editRemoveContact',

        EVENT_SHOW_ORG_DIALOG = 'organisation:showDialog',

        CONTEXT_UPDATE_EVENT = 'contentContext:update';

    EVENT_SHOW_PER_DIALOG = 'person:showDialog';

    Y.namespace('app.contacts').AbstractContactView = Y.Base.create('AbstractContactView', Y.View, [], {

        /**
         * @method _getContactTypeAndSubtype
         * @param 
         * @description
         * 	example for Mobile:
         * 		type = 'TelephoneContacts'
         * 		subTypes = 'Mobile'
         * 		subTypeKey = 'telephoneType'
         * 
         * 	example for Telephone Contacts (these exclude mobile)
         */
        _getContactTypeAndSubtypes: function (type, subTypes, subTypeKey) {

            var contacts = [];

            if (subTypes && Y.Lang.isString(subTypes)) {
                subTypes = [subTypes];
            }

            A.each(this.get('modelList').toJSON(), function (contact) {
                if (contact._type === type) {
                    A.each(subTypes, function (subType) {
                        if (subType === contact[subTypeKey]) {
                            contacts.push(contact);
                        }
                    });
                }
            });

            return this._groupContacts(contacts);
        },

        _getContactByType: function (type) {

            var contacts = [];

            A.each(this.get('modelList').toJSON(), function (contact) {
                if (contact._type === type) {
                    contacts.push(contact);
                }
            });

            return this._groupContacts(contacts);
        },

        /**
         * @method _groupContacts
         */
        _groupContacts: function (contacts) {

            if (contacts && contacts.length > 0) {

                var contactTypeList = [],
                    contactDetailsList = [],
                    prevType = contacts[0].contactType,
                    prevUsage = contacts[0].contactUsage,
                    j = 0,
                    k = 0;

                contacts.forEach(function (contact) {

                    // the value, isVerified and startDate fields are now stored in the contactDetails object

                    var contactDetails = {
                        value: null,
                        isVerified: null,
                        verifiedDate: null,
                        startDate: null
                    };

                    if (contact.contactType != prevType || contact.contactUsage != prevUsage) {
                        contactTypeList[j] = {
                            contactType: prevType,
                            contactUsage: prevUsage,
                            contactDetails: contactDetailsList
                        };
                        j++, k = 0;
                        contactDetailsList = [];
                        prevType = contact.contactType;
                        prevUsage = contact.contactUsage;
                    }

                    if (contact.number) {
                        contactDetails.value = contact.number;
                    } else if (contact.address) {
                        contactDetails.value = contact.address;
                    } else if (contact.identifier) {
                        contactDetails.value = contact.identifier;
                    }

                    if (contact.verified === true) {
                        contactDetails.isVerified = true;
                        contactDetails.verifiedDate = contact.verifiedDate;
                    }

                    contactDetails.startDate = contact.startDate;
                    contactDetailsList[k] = Object.assign({}, contactDetails);

                    k++;
                });

                contactTypeList[j] = {
                    contactType: prevType,
                    contactUsage: prevUsage,
                    contactDetails: contactDetailsList
                };

                return contactTypeList;
            }

        }
    }, {

        ATTRS: {

            model: {
                value: null
            },

            modelList: {
                value: null
            },

            /**
             * @attribute contextType
             * @description Holds the selected context. Used to determine event outcomes and location of data, since person has a flat model
             * 	and organisation uses a modelList.
             * 	'organisation', 'person'
             */
            contextType: {
                value: null
            },

            /**
             * @attribute contextId
             */
            contextId: {
                value: null
            },

            /**
             * @attribute emailContacts
             */
            emailContacts: {
                getter: function (value) {
                    return this._getContactByType('EmailContact');
                    // this.get('model') ? return this.get('model').get('emailContacts') : return this._getContactByType('EmailContact')
                }
            },

            /**
             * @attribute webContacts
             */
            webContacts: {
                getter: function (value) {
                    return this._getContactByType('WebContact');
                }
            },

            /**
             * @attribute
             */
            faxContacts: {
                getter: function (value) {
                    return this._getContactTypeAndSubtypes('TelephoneContact', 'FAX', 'telephoneType');
                }
            },

            /**
             * @attribute
             */
            mobileContacts: {
                getter: function (value) {
                    return this._getContactTypeAndSubtypes('TelephoneContact', 'MOBILE', 'telephoneType');
                }
            },

            /**
             * @attribute
             */
            socialMediaContacts: {
                getter: function (value) {
                    return this._getContactByType('SocialMediaContact');
                }
            },

            /**
             * @attribute
             */
            telephoneContacts: {
                getter: function (value) {
                    return this._getContactTypeAndSubtypes('TelephoneContact', ['LANDLINE', 'TEXTPHONE'], 'telephoneType');
                }
            },

            codedEntries: {
                value: {
                    contactType: Y.uspCategory.contact.contactType.category.codedEntries,
                    contactUsage: Y.uspCategory.contact.contactUsage.category.codedEntries,
                }
            },

            templateBaseData: {
                getter: function (value) {
                    return {
                        modelData: this.get('modelList').toJSON(),
                        emailContacts: this.get('emailContacts'),
                        webContacts: this.get('webContacts'),
                        faxContacts: this.get('faxContacts'),
                        mobileContacts: this.get('mobileContacts'),
                        socialMediaContacts: this.get('socialMediaContacts'),
                        telephoneContacts: this.get('telephoneContacts'),
                        webContacts: this.get('webContacts'),
                        codedEntries: this.get('codedEntries')
                    }

                }

            }

        }

    });

    Y.namespace('app.views').ContactDetails = Y.Base.create('contactDetails', Y.app.contacts.AbstractContactView, [], {
        initializer: function () {

            this._evtHandlers = [];
            this._evtHandlers.push(this.get('modelList').after('load', this.render, this));
            this.publish('editRemoveContact', { preventable: true });
            this.publish('addContact', { preventable: true });
            this._evtHandlers.push(Y.on(CONTEXT_UPDATE_EVENT, this._updateContextName, this));
        },

        template: Y.Handlebars.templates['personContactIdentificationView'],

        events: {
            '.add-contact': {
                click: '_addContact'
            },
            '.editRemove-contact': {
                click: '_editRemoveContact'
            }
        },

        render: function () {

            var html = this.template(
                Y.merge(this.get('templateBaseData'), {
                    labels: this.get('labels'),
                    canAddContact: this.get('canAddContact') ? '' : 'none;',
                    canEditContact: this.get('canEditContact') ? '' : 'none;'
                }));

            this.get('container').setHTML(html);

            return this;

        },

        /**
         * @method _addContact
         * @description Person and Organisation have differently shaped data. We use contextType to help determine where that is. 
         */
        _addContact: function (e) {

            var action, event = '',
                id = '',
                name = '',
                model = this.get('model');

            action = ACTION_ADD_CONTACT; // note person action is addPersonContact, so rename or provide two different actions.
            e.preventDefault();

            switch (this.get('contextType')) {

            case 'organisation':

                event = EVENT_SHOW_ORG_DIALOG;
                id = this.get('contextId');
                name = this.get('contextName');
                break;

            case 'person':
                if (model) {
                    event = EVENT_SHOW_PER_DIALOG;
                    id = model.get('personIdentifier');
                    name = model.get('name');
                }

                break;
            };

            Y.fire(event, {
                action: action,
                id: id,
                name: name
            });

        },

        /**
         * @method _editContact
         * @description Person and Organisation have differently shaped data. We use contextType to help determine where that is. 
         */
        _editRemoveContact: function (e) {

            var action, event = '',
                id = '',
                name = '',
                model = this.get('model');

            action = ACTION_EDIT_REMOVE_CONTACT;
            e.preventDefault();

            switch (this.get('contextType')) {

            case 'organisation':

                event = EVENT_SHOW_ORG_DIALOG;
                id = this.get('contextId');
                name = this.get('contextName');
                canRemoveContact = this.get('canRemoveContact')
                break;

            case 'person':
                if (model) {
                    event = EVENT_SHOW_PER_DIALOG;
                    id = model.get('personIdentifier');
                    name = model.get('name');
                    canRemoveContact = this.get('canRemoveContact');
                }

                break;
            };

            Y.fire(event, {
                action: action,
                id: id,
                name: name,
                canRemoveContact: canRemoveContact
            });
        },

        /**
         * @method _updateContextName
         * @description Person and Organisation have differently shaped data. We use contextType to help determine where that is. 
         * Whenever name is changed from different places , it needs to be reflected elsewhere in the system
         */
        _updateContextName: function (e) {
            switch (this.get('contextType')) {

            case 'organisation':
                name = this.set('contextName', e.data.name || '');
                break;

            case 'person':
                Y.bind(function (e) {
                    this.get('model').setAttrs(e.data);
                }, this);
                break;
            };
        }

    }, {
        ATTRS: {

            canAddContact: {
                value: false
            },

            canEditContact: {
                value: false
            },

            canRemoveContact: {
                value: false
            },

            /**
             * @attribute contextType
             * @description Holds the selected context. Used to determine event outcomes and location of data, since person has a flat model
             * 	and organisation uses a modelList.
             * 	'organisation', 'person'
             */
            contextType: {
                value: null
            },

            /**
             * @attribute contextId
             */
            contextId: {
                value: null
            },
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'usp-contact-Contact',
        'categories-contact-component-ContactType',
        'categories-contact-component-ContactUsage',
        'handlebars-base',
        'handlebars-helpers',
        'handlebars-person-templates'
    ]
});