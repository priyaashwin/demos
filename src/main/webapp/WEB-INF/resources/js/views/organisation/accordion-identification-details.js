


YUI.add('app-organisation-identification-details-accordion', function (Y) {

	Y.namespace('app.views').OrganisationIdentificationDetailsAccordion = Y.Base.create('organisationIdentificationDetailsAccordion', Y.usp.AccordionPanel, [], {
    	
		
		initializer: function(config) {

    		this.organisationDetailsIdentificationView=new Y.app.views.OrganisationIdentificationDetails(Y.mix(config));
            this.organisationDetailsIdentificationView.addTarget(this);
    	
		},
		
        render:function(){

        	var organisationDetailsIdentificationViewContainer=this.organisationDetailsIdentificationView.render().get('container');

        	//superclass call
        	Y.app.views.OrganisationIdentificationDetailsAccordion.superclass.render.call(this);
        	
        	//stick the organisation details container into the accordion
        	this.getAccordionBody().appendChild(organisationDetailsIdentificationViewContainer);

        	return this;
        },
        destructor:function(){
        	this.organisationDetailsIdentificationView.destroy();

        	delete this.organisationDetailsIdentificationView;
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
               'accordion-panel',
               'app-organisation-identification-details-view'
              ]
});