YUI.add('content-context-button', function (Y) {
          Y.namespace('app').ContentContextButton=Y.Base.create('contentContextButton', Y.app.ContentContext, [Y.View.NodeMap], {            	 
        	  initializer:function(){
        		  
        		  this.before('enabledChange', Y.bind(this._beforeEnabledChange, this));
        	  },
        	  _beforeEnabledChange:function(e){
        		  this._setEnabled(e.newVal);
        	  },
             enable: function(){
            	 this.set("enabled",true);
             },
             
             render: function(){
            	 Y.app.ContentContextButton.superclass.render.call(this);
            	 
            	 this._setEnabled(this.get('enabled'));
            	 
            	 return this;
             },
             
             _setEnabled:function(val){
            	 if(val){
            		 this.get('container').all("a").removeAttribute("disabled").removeClass('pure-button-disabled')
            		 .setAttribute("aria-disabled", "false").removeAttribute("aria-hidden").removeAttribute("tabindex");
            	 }else{
            		 this.get('container').all("a").setAttribute ("disabled","disabled").addClass('pure-button-disabled')
            		 .setAttribute ("aria-disabled","true").setAttribute("aria-hidden", "true").setAttribute ("tabindex", -1);
            	 }
             },
             
             disable: function(){
            	 this.set("enabled",false);
             }
       },{
    	   ATTRS: {
    		   enabled:{value:true}
    	   }
       });
}, '0.0.1', {
    requires: ['yui-base','content-context','view-node-map']
});
