YUI.add('allocation-view', function (Y) {
    'use-strict';
    Y.namespace('app.allocation').AllocationTabView = Y.Base.create('allocationView', Y.usp.ReactView, [], {
        initializer: function (config) {
            this.toolbar = new Y.usp.app.AppToolbar({ permissions: config.permissions }).addTarget(this)
            this.after('showFilterChange', this.render, this);
        },
        reactFactoryType: uspAllocation.AllocationTabView,
        renderComponent: function () {
            var codedEntries = this.get('codedEntries'),
                showFilter = this.get('showFilter'),
                allocationConfig = this.get('allocationConfig'),
                teamAllocationConfig = this.get('teamAllocationConfig'),
                colleaguesAllocationConfig = this.get('colleaguesAllocationConfig'),
                accessedClientsConfig = this.get('accessedClientsConfig'),
                resultsFilterConfig = this.get('resultsFilterConfig');

            if (this.reactAppContainer) {
                var component;

                ReactDOM.render(this.componentFactory({
                    codedEntries: codedEntries,
                    showFilter: showFilter,
                    allocationConfig: allocationConfig,
                    teamAllocationConfig: teamAllocationConfig,
                    colleaguesAllocationConfig: colleaguesAllocationConfig,
                    accessedClientsConfig: accessedClientsConfig,
                    resultsFilterConfig: resultsFilterConfig,
                    toolbar: this.toolbar
                }), this.reactAppContainer.getDOMNode(), function () {
                    component = this;
                });
                this.component = component;
            }
        },
        destructor: function () {
            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar
        },
    }, {
        ATTRS: {
            /**
             * @attribute showFilter
             * @description A toggle to determine if the filter should be shown. This attribute is automatically updated in response to clicks on the toolbar filter button.
             * @default false
             * @type boolean
             */
            showFilter: {
                value: false
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'usp-view',
        'usp-react-view'
    ]
});