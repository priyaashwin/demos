YUI.add('allocation-controller-view', function (Y) {
    /* global uspAllocation, ReactDOM*/
    'use-strict';
    
    Y.namespace('app.allocation').AllocationControllerView = Y.Base.create('allocationControllerView', Y.View, [], {
        initializer: function (config) {
            this.allocationTabView = new Y.app.allocation.AllocationTabView({
                componentType: uspAllocation.AllocationTabView,
                allocationConfig: config.allocationConfig,
                teamAllocationConfig: config.teamAllocationConfig,
                colleaguesAllocationConfig: config.colleaguesAllocationConfig,
                accessedClientsConfig: config.accessedClientsConfig,
                resultsFilterConfig: config.resultsFilterConfig,
                codedEntries: {
                    gender: Y.uspCategory.person.gender.category,
                    warnings: Y.secured.uspCategory.person.warnings.category,
                    contactType: Y.uspCategory.contact.contactType.category
                },
            }).addTarget(this);
            this.filterButton = Y.one('.filter');
            this._evtHandlers = [
                this.filterButton.on('click', this._handleOnFilterButtonClicked, this),
            ];
        },
        render: function () {
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());
            contentNode.append(this.allocationTabView.render().get('container'));
            this.get('container').append(contentNode);
            return this;
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.allocationTabView.removeTarget(this);
            this.allocationTabView.destroy();
            delete this.allocationTabView;
        },
        _handleOnFilterButtonClicked: function () {
            this.allocationTabView.set('showFilter', !this.allocationTabView.get('showFilter'));
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-toolbar',
        'allocation-view', 
        'categories-contact-component-ContactType',
        'secured-categories-person-component-Warnings'
    ]
});