YUI.add('subject-context', function(Y) {

    var L = Y.Lang,
        E = Y.Escape,
        H = Y.Handlebars,
        SubjectContext = Y.Base.create('subjectContext', Y.app.ContentContext, [], {
            template: Y.Handlebars.templates["subjectContext"],
            render: function() {
                var container = this.get('container'),
                    labels = this.get('labels') || {},
                    model = this.get('model'),
                    data = model ? model.toJSON() : {},
                    isPersonModel = (model instanceof(PersonContextModel)),
                    isGroupModel = (model instanceof(GroupContextModel)),
                    isOrganisationModel = (model instanceof(OrganisationContextModel));

                if (isPersonModel) {
                    data.identifier = data.personId;
                } else if (isGroupModel) {
                    data.identifier = data.groupId;
                } else if (isOrganisationModel) {
                    data.identifier = data.organisationIdentifier;
                }
                data.cssNoContext =  'no-context';
                
                var html = this.template({
                    icon: this.get('icon'),
                    subTitle: this.get('subTitle'),
                    labels: this.get('labels'),
                    modelData: data
                });

                //set html into container
                container.setHTML(html);

                //fire custom event to resize header
                Y.fire('header:resize');

                return this;
            },
            handlePersonSummaryClick: function(e) {
                // Redirect to the person summary, unless we're already there
                if (window.location.toString().indexOf('/summary/person?') < 0) {
                    window.location = this.get('personSummaryURL');
                }
            }
        }, {
            ATTRS: {
                /**
                 * The labels passed to the template
                 * @attribute labels
                 * @type Object
                 * @default {}
                 **/
                labels: {
                    value: {
                    }
                },
                /**
                 * The icon to represent this context
                 * @attribute icon
                 * @type String
                 * @default ''
                 **/
                icon: {
                    value: ''
                },
                /**
                 * The sub-title for the page - this usually relates to the type of thing the page is about e.g. 'Forms'
                 * @attribute subTitle
                 * @type String
                 * @default ''
                 **/
                subTitle: {
                    value: ''
                }
            }
        }),
        ContextModel = Y.Base.create('contextModel', Y.Model, [], {}),
        PersonContextModel = Y.Base.create('personContextModel', ContextModel, [], {}, {
            ATTRS: {
                personId: {
                    value: ''
                },
                name: {
                    value: '',
                    getter: function(val, name) {
                        if (this.get('hasProfessionRole') || !val || !L.trim(val).length) {
                            return [
                                this.get('forename') || '',
                                this.get('surname') || ''
                            ].join(' ');
                        }
                        return val;
                    }
                },
                forename: {
                    value: ''
                },
                surname: {
                    value: ''
                },
                hasProfessionRole: {
                    value: false
                },
                noOfPersonWarnings: {
                    value: 0,
                    setter: function(val) {
                        return Number(val);
                    },
                    validator: function(val, name) {
                        return (/^\d+$/).test(val);
                    }
                },
                warnings: {
                    value: []
                }
            }
        }),
        GroupContextModel = Y.Base.create('groupContextModel', ContextModel, [], {}, {
            ATTRS: {
                groupId: {
                    value: ''
                },
                name: {
                    value: ''
                }
            }
        }),
        OrganisationContextModel = Y.Base.create('organisationContextModel', ContextModel, [], {}, {
            ATTRS: {
                organisationId: {
                    value: ''
                },
                name: {
                    value: ''
                }
            }
        });


    Y.namespace('app').GroupContextModel = GroupContextModel;
    Y.namespace('app').PersonContextModel = PersonContextModel;
    Y.namespace('app').OrganisationContextModel = OrganisationContextModel;
    Y.namespace('app').SubjectContext = SubjectContext;
}, '0.0.1', {
    requires: ['yui-base',
        'content-context',
        'event-custom-base',
        'model',
        'view-node-map',
        'handlebars-base',
        'handlebars-helpers',
        'handlebars-context-templates'
    ]
});