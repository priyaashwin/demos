YUI.add('content-context', function (Y) {
	var L=Y.Lang;
	Y.namespace('app').ContentContext=Y.Base.create('contentContext', Y.View, [], {
		template: {},
		initializer: function () {
		    var model = this.get('model');
		    // Re-render this view when the model changes
			model.after('change', this.render, this);
		    
			Y.on('contentContext:update', Y.bind(function(e){
				this.get('model').setAttrs(e.data);
			},this), this);			    
		},
		render:function(){
			var	container, html, model; 
			
			container = this.get('container');									
			model = this.get('model');
			html = '';
						
			if(typeof this.template === 'function') {
				html = this.template(model.toJSON());
			} 
			
			if(typeof this.template === 'string') {
				html = Y.Lang.sub(this.template, model.toJSON());
			}
							
			container.setHTML(html);

			return this;
		}
	});
	
	
}, '0.0.1', {
    requires: ['yui-base','view','event-custom-base']
});