YUI.add('rule-definition-search-view', function (Y) {
    var L = Y.Lang,
        A = Y.Array,
        Q = Y.QueryString,
        USPFormatters = Y.usp.ColumnFormatters,
        USPColumnFormatters = Y.app.ColumnFormatters,
        Micro = Y.Template.Micro,
        VALIDATION_STATUS_TEMPLATE = Micro.compile('<a href="#" class="tool-tip validation-state" data-title="Rule State" data-content="<%=this.validationStatus%>" aria-label="<%=this.validationStatus%> validation state"><i class="<%=this.icon%>" aria-hidden="true"></i>');

    Y.namespace('app').PaginatedFilteredRuleDefinitionSearchResultList = Y.Base.create("paginatedFilteredRuleDefinitionSearchResultList", Y.usp.rule.PaginatedRuleDefinitionList, [Y.usp.ResultsFilterURLMixin], {
        whiteList: ["name", "status", "validationStatus"],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app').RuleDefinitionSearchView = Y.Base.create('ruleDefinitionSearchView', Y.View, [], {
        initializer: function () {
            var validationStatusFormatter = function(o) {
                var valstatus = o.data['validationStatus'];
                if (valstatus === 'INVALID') {
                    return VALIDATION_STATUS_TEMPLATE({
                        validationStatus: 'BROKEN',
                        icon: 'fa fa-exclamation-triangle'
                    });
                } else if (valstatus === 'VALID') {
                    return VALIDATION_STATUS_TEMPLATE({
                        validationStatus: 'is valid',
                        icon: 'fa fa-check-circle'
                    })
                } else if (valstatus === 'UNCHECKED') {
                    return VALIDATION_STATUS_TEMPLATE({
                        validationStatus: 'awaiting validation',
                        icon: 'fa fa-question-circle'
                    });
                }
            }

            var searchConfig = this.get('searchConfig'),
                filterContextConfig = this.get('filterContextConfig'),
                filterConfig = this.get('filterConfig'),
                ruleDefinitionDialogConfig = this.get('ruleDefinitionDialogConfig'),
                labels = searchConfig.labels,
                permissions = searchConfig.permissions,
                versionEnabled = searchConfig.versionEnabled,
                columns = ([{
                        key: 'name',
                        label: labels.name,
                        sortable: true,
                        width: versionEnabled ? '23%' : '28%'
                    }, {
                        key: 'description',
                        label: labels.description,
                        allowHTML: true,
                        width: versionEnabled ? '43%' : '48%',
                        expandable: true
                    }, {
                        key: 'status',
                        label: labels.state,
                        width: '6%',
                        sortable: true,
                        allowHTML: true,
                        formatter: USPColumnFormatters.ruleState
                    },

                    {
                        key: 'validationStatus',
                        label: labels.validationStatus,
                        width: '10%',
                        sortable: true,
                        allowHTML: true,
                        formatter: validationStatusFormatter
                    },

                    {
                        label: labels.actions,
                        className: 'pure-table-actions',
                        width: '8%',
                        formatter: USPFormatters.actions,
                        items: [{
                            clazz: 'editRule',
                            title: labels.editRuleTitle,
                            label: labels.editRule,
                            enabled: permissions.canUpdate
                        }, {
                            clazz: 'publishRule',
                            title: labels.publishRuleTitle,
                            label: labels.publishRule,
                            enabled: function () {
                                if (permissions.canPublish === true) {
                                    return (this.record.get('status') === 'DRAFT');
                                }
                                return false;
                            }
                        }, {
                            clazz: 'deleteRule',
                            title: labels.deleteRuleTitle,
                            label: labels.deleteRule,
                            enabled: function () {
                                if (permissions.canArchive === true) {
                                    return (this.record.get('status') === 'PUBLISHED');
                                }
                                return false;
                            }
                        }, {
                            clazz: 'removeRule',
                            title: labels.removeRuleTitle,
                            label: labels.removeRule,
                            enabled: function () {
                                if (permissions.canRemove === true) {
                                    return (this.record.get('status') === 'DRAFT');
                                }
                                return false;
                            }
                        }]
                    }
                ]);

            if (versionEnabled) {
                //insert the impact column if impact is enabled
                columns.splice(2, 0, {
                    key: "version",
                    label: labels.version,
                    width: '10%',
                    sortable: true
                });
            }

            //configure the filter model
            this.filterModel = new Y.app.RuleDefinitionResultsFilterModel();

            //configure the results filter context
            this.filterContext = new Y.app.RuleDefinitionFilterContext(Y.merge(filterContextConfig, {
                model: this.filterModel
            }));

            //configure the results filter
            this.resultsFilter = new Y.app.RuleDefinitionResultsFilter(Y.merge(filterConfig, {
                model: this.filterModel
            }));

            //create rule definition dialog
            this.ruleDefinitionDialog = new Y.app.RuleDefinitionDialog({
                headerContent: ruleDefinitionDialogConfig.defaultHeader,
                views: {
                    /* !!! Merge with existing view configuration defined in rule-definition-dialog-views.js !!! */
                    publishRule: {
                        headerTemplate: ruleDefinitionDialogConfig.publishConfig.headerTemplate,
                        successMessage: ruleDefinitionDialogConfig.publishConfig.successMessage
                    },
                    deleteRule: {
                        headerTemplate: ruleDefinitionDialogConfig.deleteConfig.headerTemplate,
                        successMessage: ruleDefinitionDialogConfig.deleteConfig.successMessage
                    },
                    removeRule: {
                        headerTemplate: ruleDefinitionDialogConfig.removeConfig.headerTemplate,
                        successMessage: ruleDefinitionDialogConfig.removeConfig.successMessage
                    }
                }
            });
            this.ruleDefinitionDialog.render();

            //configure the rule results table
            this.results = new Y.usp.PaginatedResultsTable({
                //configure the initial sort
                sortBy: [{
                    name: 'asc'
                }, {
                    version: 'desc'
                }],
                //declare the columns
                columns: columns,
                //configure the data set
                data: new Y.app.PaginatedFilteredRuleDefinitionSearchResultList({
                    url: searchConfig.url,
                    //plug in the filter model
                    filterModel: this.filterModel
                }),
                noDataMessage: searchConfig.noDataMessage,
                plugins: [
                    //plugin Menu handler
                    {
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    },
                    //plugin Keyboard nav
                    {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    },
                    //plugin search state
                    {
                        fn: Y.Plugin.usp.ResultsTableSearchStatePlugin
                    },
                    //plugin cell expander
                    {
                        fn: Y.Plugin.usp.ResultsTableExpandableCellsPlugin
                    }
                ]
            });

            //setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                tableId: this.name
            });

            this._evtHandlers = [
                this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this),
                //subscribe to lots of events that we want to trigger a reload of the results
                this.on(['ruleDefinitionStatusChangeView:saved',
                    'filter:apply'
                ], function (e) {
                    //reload results
                    this.results.reload();
                }, this)
            ];

            // Add event targets
            this.filterContext.addTarget(this);
            this.resultsFilter.addTarget(this);
            this.results.addTarget(this);
            this.ruleDefinitionDialog.addTarget(this);
        },
        destructor: function () {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            if (this.results) {
                //destroy the results table
                this.results.destroy();

                delete this.results;
            }

            if (this.filterContext) {
                this.filterContext.destroy();

                delete this.filterContext;
            }

            if (this.resultsFilter) {
                this.resultsFilter.destroy();

                delete this.resultsFilter;
            }

            if (this.ruleDefinitionDialog) {
                this.ruleDefinitionDialog.destroy();

                delete this.ruleDefinitionDialog;
            }

            if (this._tablePanel) {
                this._tablePanel.destroy();

                delete this._tablePanel;
            }
        },
        render: function () {
            var searchConfig = this.get('searchConfig'),
                container = this.get('container'),
                filterContextContainer,
                filterTabView,
                //render the portions of the page
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.

            //Render the filter context (this lives outside this view container)
            filterContextContainer = this.filterContext.render().get('container');

            if (!filterContextContainer.inDoc()) {
                contentNode.append(filterContextContainer);
            }

            //Results filter
            contentNode.append(this.resultsFilter.render().get('container'));

            if (this.get('searchConfig').resultsCountNode) {
                //Use our pre-defined results count node
                this._tablePanel.set('resultsCount', this.get('searchConfig').resultsCountNode);
            }

            //results table layout
            contentNode.append(this._tablePanel.render().get('container'));

            //render results
            this._tablePanel.renderResults(this.results);

            //finally set the content into the container
            container.setHTML(contentNode);

            return this;
        },

        _handleRowClick: function (e) {
            //click handler for table row
            var target = e.currentTarget,
                record = this.results.getRecord(target.get("id")),
                url,
                rule = record.get('name'),
                permissions = this.get('searchConfig').permissions;
            //prevent default action
            e.preventDefault();

            //check what link was clicked by checking CSS classes
            if (target.hasClass('editRule')) {
                this.launchDesigner(record.get('id'));
            } else {
                url = this.get('ruleDefinitionDialogConfig').ruleDefinitionStatusURL;

                if (target.hasClass('publishRule')) {
                    this.ruleDefinitionDialog.showView('publishRule', {
                            message: this.get('ruleDefinitionDialogConfig').publishConfig.message,
                            model: new Y.app.RuleDefinitionStatusChangeForm({
                                url: (L.sub(url, {
                                    action: 'published'
                                })),
                                id: record.get('id')
                            }),
                            otherData: {
                                narrative: L.sub(this.get('ruleDefinitionDialogConfig').publishConfig.narrative, {
                                    rule: rule
                                }),
                                rule: rule
                            }
                        },
                        function () {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                } else if (target.hasClass('deleteRule')) {
                    this.ruleDefinitionDialog.showView('deleteRule', {
                            message: this.get('ruleDefinitionDialogConfig').deleteConfig.message,
                            model: new Y.app.RuleDefinitionStatusChangeForm({
                                url: (L.sub(url, {
                                    action: 'archived'
                                })),
                                id: record.get('id')
                            }),
                            otherData: {
                                narrative: L.sub(this.get('ruleDefinitionDialogConfig').deleteConfig.narrative, {
                                    rule: rule
                                }),
                                rule: rule
                            }
                        },
                        function () {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                } else if (target.hasClass('removeRule')) {
                    this.ruleDefinitionDialog.showView('removeRule', {
                            message: this.get('ruleDefinitionDialogConfig').removeConfig.message,
                            model: new Y.app.RuleDefinitionRemoveForm({
                                url: this.get('ruleDefinitionDialogConfig').ruleDefinitionURL,
                                id: record.get('id')
                            }),
                            otherData: {
                                narrative: L.sub(this.get('ruleDefinitionDialogConfig').removeConfig.narrative, {
                                    rule: rule
                                }),
                                rule: rule
                            }
                        },
                        function () {
                            //Get the button by name out of the footer and enable it.
                            this.getButton('removeButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        });
                }
            }
        },
        launchDesigner: function (id) {
            var designerConfig = this.get('designerConfig'),
                designerURL = designerConfig.designerURL,
                url;

            if (!(id === undefined || id === null)) {
                url = designerURL + '?' + Q.stringify({ id: id });
            } else {
                url = designerURL;
            }

            var ref = window.open(
                url,
                'Rule Designer',
                'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1024,height=768,left=100,top=100'
            );
            if (ref) {
                //attempt to move to front
                ref.focus();
            }

            return;
        }
    }, {
        ATTRS: {
            searchConfig: {
                value: {
                    url: {},
                    noDataMessage: {},
                    permissions: {},
                    resultsCountNode: {}
                }
            },
            filterContextConfig: {
                value: {
                    labels: {}
                }
            },
            filterConfig: {
                value: {
                    labels: {}
                }
            },
            ruleDefinitionDialogConfig: {
                value: {
                    labels: {}
                }
            }
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'template-micro',
        'view',
        'accordion-panel',
        'querystring-stringify',
        'paginated-results-table',
        'results-filter',
        'results-formatters',
        'results-templates',
        'results-table-menu-plugin',
        'results-table-keyboard-nav-plugin',
        'results-table-search-state-plugin',
        'results-table-expandable-cells-plugin',
        'usp-rule-RuleDefinition',
        'rule-definition-results-filter',
        'rule-definition-filter-context',
        'rule-definition-dialog-views',
        'caserecording-results-formatters'
    ]
});