YUI.add('rule-definition-filter-context', function (Y) {

    Y.namespace('app').RuleDefinitionFilterContext=Y.Base.create('ruleDefinitionFilterContext', Y.usp.ResultsFilterContext, [], {
        template: Y.Handlebars.templates["ruleDefinitionActiveFilter"],
        initializer:function(){
            //plug in error handling
           this.plug(Y.Plugin.usp.FilterErrorsPlugin);           
        },
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.FilterErrorsPlugin);
        }
     });
}, '0.0.1', {
    requires: ['results-filter', 'handlebars-rule-templates']
});