YUI.add('rule-definition-results-filter', function(Y) {

    var O = Y.Object,
        A = Y.Array,
        L = Y.Lang;

    Y.namespace('app').RuleDefinitionResultsFilterModel = Y.Base.create('ruleDefinitionResultsFilterModel', Y.usp.ResultsFilterModel, [Y.usp.ModelValidate], {       
    },{
        ATTRS: {
            status: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                //default status list defined here
                value: ['DRAFT', 'PUBLISHED']
            },
            name:{
                USPType: 'String',
                value:''
            },
            validationStatus: { 
            	USPType: 'String',
            	setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
            	value: ''
            	
            } 
        }
    });


    Y.namespace('app').RuleDefinitionResultsFilter = Y.Base.create('ruleDefinitionResultsFilter', Y.usp.ResultsFilter, [Y.usp.ResultsFilterModelSyncMixin], {
        template: Y.Handlebars.templates['ruleDefinitionResultsFilter'],
        events: {
            '#ruleFilter_clearAll': {click: '_handleClearAll'}

        },
        render: function() {
            //get the container 
            var instance=this,
                container = instance.get('container');
            
            //call into superclass to render
            Y.app.RuleDefinitionResultsFilter.superclass.render.call(this);
            
            
            //add class name to container to allow us to identify filter
            container.addClass(this.name);
            
            return this;
        },
        _handleClearAll: function(e) {
            e.preventDefault();
            //clear everything
            this.clearAll();
        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {            
            otherData:{
                value:{
                    statusList:[{
                        code: "DRAFT",
                        name: "Draft"
                    },
                    {
                        code: "PUBLISHED",
                        name: "Published"
                    },
                    {
                        code: "ARCHIVED",
                        name: "Deleted"
                    }],
                    
                    validationList:[{
                        code: "UNCHECKED",
                        name: "Unchecked"
                    },
                    {
                        code: "INVALID",
                        name: "Invalid"
                    },
                    {
                        code: "VALID",
                        name: "Valid"
                    }]

                }
            }     

        }
    });

}, '0.0.1', {
    requires: ['yui-base',
               'results-filter',
               'handlebars-helpers',
               'handlebars-checklist-templates']
});