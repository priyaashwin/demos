/* global window */

YUI.add('groupofrights-search-view', function(Y) {
    var A = Y.Array, 
        USPFormatters = Y.usp.ColumnFormatters,
        groupOfRightsDialog = this.groupOfRightsDialog;
    
    Y.namespace('app').GroupOfRightsSearchView = Y.Base.create('groupOfRightsSearchView', Y.View, [], {
        
        initializer : function() {
            var searchConfig = this.get('searchConfig'), labels = searchConfig.labels, canUpdate = searchConfig.canUpdate;
            
            // create dialog
            groupOfRightsDialog = new Y.app.GroupOfRightsDialog({
                headerContent : 'Loading...',
                // View config mixed into existing config
                views : {
                    addGroupOfRights : {
                        headerTemplate : this.get('addConfig').headerTemplate
                    },
                    editGroupOfRights : {
                        headerTemplate : this.get('editConfig').headerTemplate
                    }
                }
            }).render();
            
            // listen for edit button click event to do delete? from view?
            groupOfRightsDialog.on('*:editThisGroupOfRights', function(e) {
                // fire edit event
                this.fire('edit', {
                    id : e.id
                });
            }, this);
            
            // listen for save event
            groupOfRightsDialog.on('*:saved', function(e) {
                // display the success message
                Y.fire('infoMessage:message', {
                    message : e.successMessage
                });
                
                this.results.reload();
            }, this);
            
            // update style of toolbar buttons to indicate enablement
            Y.all('#sectionToolbar .pure-button-loading').removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
            
            // configure the security group of rights results table
            this.results = new Y.usp.PaginatedResultsTable({
                // configure the initial sort
                sortBy : [
                    {
                        name : 'asc'
                    }
                ],
                // declare the columns
                columns : [
                    {
                        key : 'code',
                        label : labels.code,
                        width : '20%',
                        sortable : true
                    }, {
                        key : 'name',
                        label : labels.name,
                        width : '20%',
                        sortable : true
                    }, {
                        key : 'description',
                        label : labels.description,
                        width : '20%'
                    }, {
                        key : 'rights',
                        label : labels.rights,
                        width : '35%',
                        formatter : function(o) {
                            if (o.data.rights.length) {
                                var rights = [];
                                
                                for (var i = 0; i < o.data.rights.length; i++) {
                                    if (o) {
                                        rights.push(o.data.rights[i].name);
                                    }
                                }
                                
                                rights.sort();
                                return rights.join(', ');
                            }
                            
                        }
                    }, {
                        key : 'active',
                        label : labels.active,
                        width : '5%',
                        formatter : USPFormatters.yesNo,
                        sortable : true
                    }, {
                        label : labels.actions,
                        className : 'pure-table-actions',
                        width : '5%',
                        allowHTML: true,                      
                        formatter : USPFormatters.actions,
                        items : [
                                 {
                                    clazz : 'edit',
                                    title : labels.editGroupOfRights,
                                    label : labels.editGroupOfRights,
                                    enabled : canUpdate,
                                    visible : function() {
                                    // if system group of rights, ensure the
                                    // option is hidden
                                    return !this.record.get('system');
                                }
                            }
                        ]
                    }
                ],
                // configure the data set
                data : new Y.usp.security.PaginatedSecurityGroupOfRightsWithRightsList({
                    url : searchConfig.url
                }),
                noDataMessage : searchConfig.noDataMessage,
                plugins : [
                    // plugin Menu handler
                    {
                        fn : Y.Plugin.usp.ResultsTableMenuPlugin
                    },
                    // plugin Keyboard nav
                    {
                        fn : Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }
                ]
            });
            
            // setup table panel
            this._tablePanel = new Y.usp.TablePanel({
                tableId : this.name
            });
            
            this._evtHandlers = [
                Y.delegate('click', this._addButtonClick, '#sectionToolbar', '.pure-button,li.pure-menu-item a', this), this.results.delegate('click', this._handleRowClick, '.pure-table-data tr a', this), this.on('add', this._showAddGroupOfRightsDialog, this), this.on('edit', this._showEditGroupOfRightsDialog, this)
            ];
            // publish events
            this.publish('edit', {
                preventable : true
            });
        },
        destructor : function() {
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                // null out
                this._evtHandlers = null;
            }
            
            if (this.results) {
                // destroy the results table
                this.results.destroy();
                // delete the property
                delete this.results;
            }
            
            if (this._tablePanel) {
                this._tablePanel.destroy();
                
                delete this._tablePanel;
            }
        },
        render : function() {
            var container = this.get('container'),
            // create a new document fragment
            contentNode = Y.one(Y.config.doc.createDocumentFragment());
            
            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            
            if(this.get('searchConfig').resultsCountNode){
                //Use our pre-defined results count node
                this._tablePanel.set('resultsCount', this.get('searchConfig').resultsCountNode);
            }
            
            // results table layout
            contentNode.append(this._tablePanel.render().get('container'));
            
            // render results
            this._tablePanel.renderResults(this.results);
            
            this.results.get('data').on('load', function(e) {
                window.scrollTo(0, 0);
            });
            
            // finally set the content into the container
            container.setHTML(contentNode);
            
            return this;
        },
        _addButtonClick : function(e) {
            
            var target = e.currentTarget;
            
            e.preventDefault();
            
            // check what link was clicked by checking CSS classes
            if (target.hasClass('add-groupofrights')) {
                // fire add event
                this.fire('add');
            }
        },
        _handleRowClick : function(e) {
            // click handler for table row
            var target = e.currentTarget, cId = target.get('id'), record = this.results.getRecord(cId);
            
            e.preventDefault();
            
            // check what link was clicked by checking CSS classes
            if (target.hasClass('edit')) { // fire edit event
                this.fire('edit', {
                    id : record.get('id')
                });
            }
        },
        _showAddGroupOfRightsDialog : function(e) {
            // fetch the available rights and pass through to the
            // addGroupOfRights view when loaded
            var addConfig = this.get('addConfig'), rightsList = new Y.usp.security.PaginatedSecurityRightList({
                url : addConfig.rightsURL
            });
            rightsList.load(function(err, response) {
                groupOfRightsDialog.showView('addGroupOfRights', {
                    labels : addConfig.labels,
                    model : new Y.app.NewGroupOfRightsForm({
                        url : addConfig.gorURL
                    }),
                    availableRightsJSON : rightsList.toJSON()
                }, function(view) {
                    // Get the save button by name out of the footer and enable
                    // it.
                    groupOfRightsDialog.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            });
        },
        _showEditGroupOfRightsDialog : function(e) {
            var editConfig = this.get('editConfig'),
            // fetch the available rights and pass through to the
            // editGroupOfRights view when loaded
            rightList = new Y.usp.security.PaginatedSecurityRightList({
                url : editConfig.rightsURL
            });
            
            rightList.load(function(err, response) {
                
                groupOfRightsDialog.showView('editGroupOfRights', {
                    labels : editConfig.labels,
                    model : new Y.app.UpdateGroupOfRightsForm({
                        url : editConfig.groupOfrightsURL
                    }),
                    updateUrl : editConfig.updateURL,
                    availableRightsJSON : rightList.toJSON(),
                    targetIdentifier : e.groupOfRightsIdentifier
                }, {
                    modelLoad : true,
                    payload : {
                        id : e.id
                    }
                }, function(view) {
                    // show the cancel button
                    this.getButton('cancelButton', Y.WidgetStdMod.FOOTER).show();
                    // Get the save button by name out of the footer and enable
                    // it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            });
        }
    }, {
        ATTRS : {
            searchConfig : {
                value:{
                    url: {},
                    noDataMessage: {},
                    resultsCountNode:{}
                }
            }
        }
    });
    
}, '0.0.1', {
    requires : [
        'yui-base', 'view', 'accordion-panel', 'paginated-results-table', 'results-templates', 'results-formatters', 'results-table-menu-plugin', 'usp-security-SecurityGroupOfRightsWithRights'
    ]
});