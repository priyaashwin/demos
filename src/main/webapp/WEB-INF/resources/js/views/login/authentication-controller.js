YUI.add('authentication-controller', function (Y) {

    Y.namespace('app.login').AuthenticationControllerView = Y.Base.create('authenticationControllerView', Y.View, [], {
        initializer: function (config) {
            var serverSession = new Y.usp.session.ServerSession(config.sessionInfo, config.sessionDuration, config.keepAliveURL);
            //start the server session monitor - no need to destroy this in the destructor as it is required for
            //the duration of the page
            serverSession.start();

            //create new dialog instance - render and add this as an event bubble target
            this.dialog = new Y.app.login.SessionDialog(config.dialogConfig).addTarget(this).render();

            this._evts = [
                this.on('*:secondaryPasswordRequired', this.handleSecondaryPasswordRequired, this),
                this.on('*:incompleteAuthentication', this.handleIncompleteAuthentication, this),
                this.on('*:accountExpired', this.handleAccountExpired, this),
                Y.on('session:timeout', this.handleSessionTimeout, this),
                Y.on('session:logout', this.handleSessionLogout, this),
                Y.on('session:switch', this.handleSessionSwitch, this),
                Y.on('session:ok', this.handleSessionOK, this),
                Y.on('session:restore', this.handleSessionRestore, this),
            ];
        },
        handleSessionRestore: function () {
            //let the user know the session has been restored
            Y.fire('infoMessage:message', {
                message: 'Your Session has been restored in another window'
            });
        },
        handleSessionOK: function () {
            //hide the dialog
            this.dialog.hide();
        },
        handleSessionSwitch: function (e) {
            this.dialog.showView('sessionSwitchMessage', { from: e.from, to: e.to });
        },
        handleSessionLogout: function (sessionInfo) {
            var message = this.get('dialogConfig.views.loginForm.messageLogout');
            this.showLoginForm(sessionInfo, message);
        },
        handleSessionTimeout: function (sessionInfo) {
            var message = this.get('dialogConfig.views.loginForm.messageTimeout');
            this.showLoginForm(sessionInfo, message);
        },
        handleAccountExpired: function (){
            this.dialog.showView('accountExpired');
        },
        showLoginForm: function (sessionInfo, message) {
            var url = this.get('dialogConfig.views.loginForm.authenticateURL');
            // Bump login dialog above any other Dialogs
            this.dialog.showView('loginForm', {
                model: new Y.app.login.LoginModel({
                    url: url,
                    userName: sessionInfo.username,
                    profileId: sessionInfo.profileId,
                    profileName: sessionInfo.profileDesc,
                }),
                message: message,
                sessionInfo: sessionInfo
            }, {}, function (view) {
                //Get the button by name out of the footer and enable it.
                this.getButton('loginButton', Y.WidgetStdMod.FOOTER).set('disabled', false);

                //tell view to focus the password Field
                view.focusPassword();
            });
        },
        handleSecondaryPasswordRequired: function (e) {
            var requiredIndices = e.requiredIndices,
                profileId = e.profileId,
                url = this.get('dialogConfig.views.secondaryPassword.secondaryPasswordURL');

            this.dialog.showView('secondaryPassword', {
                model: new(Y.app.login.getSecondaryPasswordModel(requiredIndices))({
                    url: url,
                    profileId: profileId
                }),
                otherData: {
                    requiredIndices: requiredIndices
                }
            }, {}, function (view) {
                //Get the button by name out of the footer and enable it.
                this.getButton('submitButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //tell view to focus the first input field
                view.focusSecondaryPassword();
            });
        },
        handleIncompleteAuthentication: function () {
            this.dialog.showView('incompleteAuthMessage');
        },
        destructor: function () {
            this._evts.forEach(function (handler) {
                handler.detach();
            });
            delete this._evts;

            this.dialog.removeTarget(this);
            this.dialog.destroy();
            delete this.dialog;
        },
        render: function () {
            return this;
        }
    }, {
        ATTRS: {
            serverDuration: {},
            sessionInfo: {},
            keepAliveURL: {}            
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'event-custom',
        'login-dialog-views',
        'server-session'
    ]
});