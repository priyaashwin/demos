YUI.add('genogram-templates', function (Y) {

    Y.namespace('app.genogram').keyTemplate = Y.Template.Micro.compile(
  		'<div>\
		     <div class="graph-key-header">\
		        <div class="pure-g-r">\
		           <div class="pure-u-1 l-box">\
                 <h3>Genogram<span>Layout key</span></h3>\
		           </div>\
		        </div>\
		     </div>\
         <div class="graph-key-content">\
           <div class="pure-g-r">\
             <div class="pure-u-1 l-box">\
               <ul class="key-node-icon initial">\
                 <li class="key-type-heading">Gender icons</li>\
                 <li><span class="genogram-gender genogram-gender-male"></span><span class="key-descriptor">Male</span></li>\
                 <li><span class="genogram-gender genogram-gender-female"></span><span class="key-descriptor">Female</span></li>\
                 <li><span class="genogram-gender genogram-gender-unknown"></span><span class="key-descriptor">Unknown</span></li>\
               <ul>\
               <ul class="key-node-icon initial">\
                 <li class="key-type-heading">Marriage or </li>\
                 <li class="key-type-heading">other unions </li>\
                 <li><span class="genogram-line genogram-line-marriage"></span><span class="key-descriptor">Marriage</span></li>\
                 <li><span class="genogram-line genogram-line-separated"></span><span class="key-descriptor">Separated</span></li>\
                 <li><span class="genogram-line genogram-line-divorced"></span><span class="key-descriptor">Divorced</span></li>\
               <ul>\
               <ul class="key-node-icon initial">\
                 <li class="key-type-heading">Other</li>\
    		     <li><span class="genogram-deceased"></span><span class="key-descriptor">Deceased</span></li>\
               </ul>\
             </div>\
           </div>\
         </div>\
      <div>'
    );

    Y.namespace('app.genogram').menuTemplate = Y.Template.Micro.compile(
      '<div id="graphToolbar" class="pure-group">\
        <ul class="pure-button-group">\
          <li class="pure-menu pure-menu-open"><a href="#" class="pure-button" data-action="zoomIn" title="Zoom in"><i class="fa fa-search-plus"></i></a></li>\
          <li class="pure-menu pure-menu-open"><a href="#" class="pure-button" data-action="zoomOut" title="Zoom out"><i class="fa fa-search-minus"></i></a></li>\
          <li class="pure-menu pure-menu-open"><a href="#" class="pure-button" data-action="toggleKey" title="Show key" id="keyToggle"><i class="fa fa-key"></i></a></li>\
          <li class="pure-menu pure-menu-open"><a href="#" class="pure-button" data-action="redraw" title="Redraw current diagram"><i class="fa fa-refresh"></i></a></li>\
        </ul>\
      </div>'
    );

}, '0.0.1', {
    requires: ['template-micro','datatype-date']
});
