YUI.add('genogram-controller-view', function (Y) {

  var KEY_DIV = '<div class="genogram-key"></div>';

  var ControllerView = Y.Base.create('controllerView', Y.View, [], {

    events: {
      '#genogram-controls': {
        'toggleKey': 'toggleKey',
        'refreshGenogram': 'refreshGenogram'
      }
    },

    initializer: function (config) {

      var createFactory = function (type) {
        return React.createElement.bind(null, type);
      };

      this.keyTemplate = Y.app.genogram.keyTemplate;
      this.printGenogramDialog = new Y.app.relationship.PrintGenogramDialog(config.dialogConfig).addTarget(this).render();

      this._genogramPrintEvtHandlers = [
        this.on('*:doPrint', this.doPrint, this)
      ];

      this.genogramViewFactory = createFactory(uspGenogram.Genogram);

      this.get('container').plug(Y.Plugin.BusyOverlay, {
        css: 'graph-disabled-mask'
      });

      this.setBusyOverlay(true);

      this.get('model').after('load', function (e) {
          this.setBusyOverlay(false);
          this.renderGenogram();
      }, this);

      this.render();
    },

    destructor: function () {

      if (this.genogramContainer) {
        ReactDOM.unmountComponentAtNode(this.genogramContainer.getDOMNode());
        this.genogramContainer.destroy(true);
        delete this.genogramContainer;
      }

      this._genogramPrintEvtHandlers.forEach(function (handler) {
        handler.detach();
      });

      this.printGenogramDialog.removeTarget(this);
      this.printGenogramDialog.destroy();
      delete this.printGenogramDialog;

      delete this._genogramPrintEvtHandlers;
    },

    renderGenogram: function () {
      var node = this.genogramContainer;
      var data = this.get('model').getNodes();
      var subject = this.get('model').get('subject');
      var canSaveLayout = this.get('permissions').canSaveGenogramLayout;

      ReactDOM.render(this.genogramViewFactory({
          subjectId : subject.id,
          saveUrl :  this.get('saveLayoutUrl'),
          loadUrl : this.get('getLayoutUrl'),
          data: data,
          permissions : {canSaveLayout: canSaveLayout},
          subject: subject,
          filter: this.get('filterConfig')
      }), node.getDOMNode(), function () {
        node.getDOMNode().__component = this;
      });
    },

    render: function () {
      var contentNode = Y.one(Y.config.doc.createDocumentFragment());
      var key = Y.Node.create(this.keyTemplate({
        layoutName: 'Genogram'
      }));

      contentNode.appendChild('<div id="genogram-container"></div>');
      this.genogramContainer = contentNode.one('#genogram-container');
      this.get('container').append(contentNode);

      if (!this.keyContainer) {
        this.keyContainer = Y.Node.create('<div class="graph-key"></div>');
        this.keyContainer.appendTo(this.get('container'));
        this.keyContainer.hide();
        key.appendTo(this.keyContainer);
      }
      return this;
    },

    toggleKey: function () {
      this.get('keyActive') ? this.hideKey() : this.showKey();
      this.set('keyActive', !this.get('keyActive'));
    },

    refreshGenogram: function () {
      this.setBusyOverlay(true);
      this.get('model').load();
    },

    setBusyOverlay: function (show) {
      var container = this.get('container');
      if (show) {
        container.busy.show();
      } else {
        container.busy.hide();
      }
    },

    hide: function () {
      this.get('container').hide();
    },

    show: function () {
      this.get('container').show();
      this.get('model').load();
    },

    showKey: function () {
      if (this.get('keyEnabled') === true) {
        this.keyContainer.show('fadeIn', {
          easing: 'ease-in',
          duration: 0.3
        });
      }
    },

    hideKey: function () {
      if (this.get('keyEnabled') === true) {
        this.keyContainer.hide('fadeOut', {
          easing: 'ease-out',
          duration: 0.3
        });
      }
    },
    print: function () {
      this.printGenogramDialog.showView('print');
    },
    doPrint: function (input) {
      var svg = document.querySelector('#svgRoot');
      var str = new XMLSerializer().serializeToString(svg);
      var blob = new Blob([str], { type: 'image/svg+xml' });
      var filename = '{name} ({identifier}) genogram.{extension}';
      var extension = 'svg';

      switch (input.format) {
        case "JPEG":
          extension = 'jpg';
          break;
        case "PNG":
          extension = 'png';
          break;
        default:
          extension = 'svg';
          break;
      }

      filename = Y.Lang.sub(filename, {
        name: this.get('subject.name').replace(/[^\w\d\s]/g, ''),
        identifier: this.get('subject.identifier'),
        extension: extension
      });

      if (input.format === 'SVG') {
        this.downloadImage(blob, filename);
      }
      else {
        this.exportSVG2Image(svg, filename,
          input.format,
          this.triggerDownload
        );
      }

    },

    downloadImage: function (blob, filename) {
      var a = document.createElement('a');
      var canvas = document.createElement('canvas');
      var ctx = canvas.getContext('2d');
      var domURL = window.URL || window.webkitURL || window;
      var url = domURL.createObjectURL(blob);
      var img = new Image;

      if (canvas.toBlob !== undefined) {
        canvas.toBlob(function (blob) {
          img.onload = function () {
            var png = canvas.toDataURL('image/png');
            ctx.drawImage(this, 0, 0);
            domURL.revokeObjectURL(url);
          };
          img.src = url;
          document.body.appendChild(img);
        });
      }

      a.style = "display: none";
      a.href = url;
      a.download = filename;

      // IE 11
      if (window.navigator.msSaveBlob !== undefined) {
        window.navigator.msSaveBlob(blob, filename);
        return;
      }

      document.body.appendChild(a);
      requestAnimationFrame(function () {
        a.click();
        domURL.revokeObjectURL(url);
        document.body.removeChild(a);
      });
    },
    exportSVG2Image: function (svg, filename, format, callback) {
      if (!svg) { return; }
      var domURL = window.URL || window.webkitURL || window;

      var copy = svg.cloneNode(true);
      var data = new XMLSerializer().serializeToString(copy);

      //set size for the canvas
      var canvas = document.createElement("canvas");
      var svgBoundSize = svg.getBoundingClientRect();
      canvas.width = svgBoundSize.width;
      canvas.height = svgBoundSize.height;

      var context = canvas.getContext("2d");
      context.clearRect(0, 0, canvas.width, canvas.height);

      var svgBlob = new Blob([data], { type: "image/svg+xml;charset=utf-8" });
      var url = domURL.createObjectURL(svgBlob);
      var img = new Image();
      img.setAttribute('crossOrigin', 'anonymous');
      img.onload = function () {
        context.drawImage(img, 0, 0, canvas.width, canvas.height);
        switch (format) {
          case "JPEG":
            if (canvas.toDataURL !== undefined) {
              callback(domURL, canvas.toDataURL('image/jpeg', 1.0), filename);
            }
            break;
          case "PNG":
            if (canvas.toDataURL !== undefined) {
              callback(domURL, canvas.toDataURL('image/png'), filename);
            }
            break;
        }
      };
      img.src = url;
  },
    triggerDownload: function (domURL, imgURI, filename) {
      var a = document.createElement('a');
      a.style = "display: none";
      a.href = imgURI;
      a.download = filename;

      document.body.appendChild(a);
      requestAnimationFrame(function () {
        a.click();
        domURL.revokeObjectURL(imgURI);
        document.body.removeChild(a);
      });
    }
  },
  {
    ATTRS: {
      keyActive: {
        value: false
      },
      keyEnabled: {
        value: true
      },
      model: {
        valueFn: function () {
          return new Y.app.genogram.GenogramModel({
            subject: this.get('subject'),
            url: Y.Lang.sub(this.get('routes.data'), {
              id: this.get('subject.id')
            })
          });
        }
      },
      subject: {
        value: null,
        setter: function (val) {
          return ({
            type: val.subjectType || val.type,
            name: val.subjectName || val.name,
            identifier: val.subjectIdentifier || val.identifier,
            id: val.subjectId || val.id,
            personTypes: val.personTypes,
            gender: val.gender
          });
        }
      }
    }
  });

  Y.namespace('app.genogram').ControllerView = ControllerView;
  }, '0.0.1', {
  requires: [
    'yui-base',
    'gallery-busyoverlay',
    'genogram-model',
    'genogram-templates',
    'relationship-printgenogram-dialog'
  ]
});
