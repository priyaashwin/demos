YUI.add('genogram-model', function(Y) {
  var blacklist = [
    'COUSIN',
    'GRANDPARENTCHILD',
    'GREATGRANDPARENTCHILD',
    'GREATUNCLEAUNTNEPHEWNIECE',
    '2GUNCLEAUNTNEPHEWNIECE',
    'UNCLEAUNTNEPHEWNIECE'
  ];

  var GenogramModel = Y.Base.create("genogramModel", Y.Model, [Y.ModelSync.REST], {

    parse: function(response) {
      var parsedResponse;
      var result = {};

      try {
        parsedResponse = Y.JSON.parse(response);
        result.relationships = parsedResponse;
        result.people = this._initPeople(parsedResponse);
        result.nodes = this._initNodes(result.people);

      } catch(ex) {
        Y.log("Loading of data from server for Y.usp.Genogram.GenogramsModel failed. No results defined in response body "+ex);
        this.fire('error', {
          error: ex,
          response: response,
          src: 'parse'
        });
      }

      return result;
    },

    getNodes: function() {
      var nodes = this.get('nodes');
      return nodes ? nodes.toJSON() : null;
    },

    getRoot: function() {
      return this.get('people').getById(this.get('subject.id'));
    },

    getURL: function(action, options) {
      var url = this._substituteURL(this.url, Y.merge(this.getAttrs(), options));
      if (options.includeSuggestions) {
          url += "?includeSuggestions=true" + "&" + Math.random();
          if (options.suggestionsRelationshipId) {
              url += "&suggestionsRelationshipId=" + options.suggestionsRelationshipId;
          }
      } else {
          url = url + '?' + Math.random();
      }
      return url;
    },

    _initContext: function(subject) {
      return ({
        id: Number(subject.id),
        identifier: subject.identifier,
        gender: subject.gender,
        lifeState: subject.lifeState,
        name: subject.name,
        personTypes: subject.personTypes || [],
        personalRelationships: []
      });
    },

    _initPeople: function(relationships) {
      var data = {};
      var subject = this.get('subject') || {};

      Y.Array.each(relationships, function(relationship) {
        var personA = relationship.roleAPersonVO;
        var personB = relationship.roleBPersonVO;

        if(this._isBlacklisted(relationship)) {
          return;
        }

        if(!data[personA.personIdentifier]) {
          data[personA.personIdentifier] = this._initPerson(relationships, personA);
        }

        if(!data[personB.personIdentifier]) {
          data[personB.personIdentifier] = this._initPerson(relationships, personB);
        }
      }, this);

      //ensure we always display at least the context
      if(subject.identifier && !data[subject.identifier]) {
        data[subject.identifier] = this._initContext(subject);
      }

      return new Y.usp.relationships.PersonList({
        items: Y.Object.values(data)
      });
    },

    _initPerson: function(relationships, person) {
      return ({
        id: person.id,
        identifier: person.personIdentifier,
        age: person.age,
        dateOfBirth: person.dateOfBirth,
        diedDate: person.diedDate,
        gender: person.gender,
        lifeState: person.lifeState,
        name: person.name,
        personTypes: person.personTypes,
        personalRelationships: this._initPersonRelations(relationships, person)
      });
    },

    _initPersonRelations: function(relationships, person) {
      var data = [];
      Y.Array.each(relationships, function(relationship) {
        var roleAPersonVO = relationship.roleAPersonVO;
        var roleBPersonVO = relationship.roleBPersonVO;
        var relationshipType = relationship.personPersonRelationshipTypeVO;
        var relationshipRoleGenderTypeVOs = relationshipType.relationshipRoleGenderTypeVOs;
        var genderedRelationship;

        if(person.id===roleAPersonVO.id) {
          genderedRelationship = relationshipRoleGenderTypeVOs[roleBPersonVO.gender]||{};
          data.push({
            step: relationship.step,
            targetId: roleBPersonVO.id,
            adoptive: relationship.adoptive,
            targetGender: roleBPersonVO.gender,
            targetRoleClass: relationshipType.roleBClass,
            label: genderedRelationship.roleBName || relationshipType.roleBName
          });
        } else if(person.id===roleBPersonVO.id) {
          genderedRelationship = relationshipRoleGenderTypeVOs[roleBPersonVO.gender]||{};
          data.push({
            step: relationship.step,
            targetId: roleAPersonVO.id,
            adoptive: relationship.adoptive,
            targetGender: roleAPersonVO.gender,
            targetRoleClass: relationshipType.roleAClass,
            label: genderedRelationship.roleAName || relationshipType.roleAName
          });
        }
      });
      return data;
    },

    _initNodes: function(people) {
      var nodes = new GenogramNodeModelList();

      people.each(function(person) {
        var personData = person.toJSON();
        var born, died;

        if(personData.dateOfBirth) {
          born = 'Born: ' + Y.USPDate.format(personData.dateOfBirth.calculatedDate);
        }

        if(personData.diedDate) {
          died = personData.diedDate ? 'Died: ' + Y.USPDate.format(personData.diedDate.calculatedDate) : '';
        }

        if(personData.personTypes.indexOf('PROFESSIONAL')===-1) {
          nodes.add(new GenogramNodeModel({
            id: personData.id.toString(),
            key: personData.id.toString(),
            born: born,
            died: died,
            personData: person.toJSON(),
            gender: personData.gender,
            name: personData.name,
            lifeState: personData.lifeState
          }));
        }
      });

      return nodes;
    },

   /**
    * @method _isBlacklisted
    * Some relationships, for example grandparent/grandchild aren't used to add people deirectly to
    * genogram.
    */
    _isBlacklisted: function(relationship) {
      var relationshipType = relationship.personPersonRelationshipTypeVO;
      var relationshipClass = relationshipType.relationshipClass;
      var personA = relationship.roleAPersonVO.name;
      var personB = relationship.roleBPersonVO.name;
      if((blacklist||[]).indexOf(relationshipClass )!== -1){
        Y.log('ignoring relationship: ' +  relationshipClass + ' between ' + personA + ' and ' + personB, 'debug');
        return true;
      } else {
        return false;
      }
    }

  }, {
    ATTRS: {
      nodes: {},
      people: {},
      relationships: {},
      subject: {
        value: null
      }
    },
    UnionTypes: {
      DIVORCED: 'Divorced',
      MARRIED: 'Marriage',
      SEPARATED: 'Separated'
    }
  });


  var GenogramNodeModel = Y.Base.create('genogramNodeModel', Y.Model, [], {

    initializer: function(config) {
      if(config.personData) {
        this._initRelationships(config.personData);
      }
    },

    getNodeType: function(val) {
      var value = '';
      var gender = this.get('gender') || '';
      var lifeState = this.get('lifeState');

      if (lifeState==='UNBORN' && gender==='MALE') {
        value = 'UM';
      } else if (lifeState==='UNBORN' && gender==='FEMALE') {
        value = 'UF';
      } else if (lifeState==='UNBORN' && (gender==='UNKNOWN' || gender==='INDETERMINATE')) {
        value = 'UU';
      } else {
        value = gender.charAt(0, 1);
      }
      return value;
    },

    getParentKey: function(val) {
      return val && val.toString();
    },

    _initRelationships: function(val) {
      var personalRelationships = val.personalRelationships || [];
      var unions = [];
      var ux = [];
      var vir = [];

      Y.Array.each(personalRelationships, function(relationship) {
        var key = relationship.targetId;
        switch(relationship.targetRoleClass) {
          case 'PARENT':
            this.set('adoptive', relationship.adoptive || false);
            if(this._isMother(relationship)) {
              if(this.get('motherKey') !== ""){
                this.set('fatherKey', key); // may have two civil mothers
              } else {
                this.set('motherKey', key);
              }
            } else if(this._isFather(relationship)) {
              if(this.get('fatherKey') !== ""){
                this.set('motherKey', key); // may have two civil fathers
              } else {
                this.set('fatherKey', key);
              }
            }
            break;
          case 'HUSBAND':
            vir.push({
              category: GenogramModel.UnionTypes.MARRIED,
              key: key
            });
            break;
          case 'EX-HUSBAND':
            vir.push({
              category: GenogramModel.UnionTypes.DIVORCED,
              key: key
            });
            break;
          case 'SEPARATED-HUSBAND':
            vir.push({
              category: GenogramModel.UnionTypes.SEPARATED,
              key: key
            });
            break;
          case 'Fiancé':
            vir.push({
              category: GenogramModel.UnionTypes.MARRIED,
              key: key
            });
            break;
          case 'WIFE':
            ux.push({
              category: GenogramModel.UnionTypes.MARRIED,
              key: key
            });
            break;
          case 'EX-WIFE':
            ux.push({
              category: GenogramModel.UnionTypes.DIVORCED,
              key: key
            });
            break;
          case 'SEPARATED-WIFE':
            ux.push({
              category: GenogramModel.UnionTypes.SEPARATED,
              key: key
            });
            break;
          case 'Committed Partner':
            ux.push({
              category: GenogramModel.UnionTypes.MARRIED,
              key: key
            });
            break;
          case 'CIVIL PARTNERSHIP':
            ux.push({
              category: GenogramModel.UnionTypes.MARRIED,
              key: key
            });
            break;
          case 'EXCIVILPARTNER':
            ux.push({
              category: GenogramModel.UnionTypes.DIVORCED,
              key: key
            });
            break;
          case 'Fiancée':
            ux.push({
              category: GenogramModel.UnionTypes.MARRIED,
              key: key
            });
            break;
        };
      }, this);

      this.setAttrs({
        unions: ux.concat(vir),
        ux: ux,
        vir: vir
      });
    },

    _isFather: function(val) {
      return (val.targetRoleClass==='PARENT'
        && val.targetGender==='MALE'
        && val.inLaw!==true
        && val.step!==true);
    },

    _isMother: function(val) {
      return (val.targetRoleClass==='PARENT'
        && val.targetGender==='FEMALE'
        && val.inLaw!==true
        && val.step!==true);
    }

  }, {
    ATTRS: {
      born: {},
      died: {},
      gender: {},
      adoptive: {
        value: false
      },
      fatherKey: {
        getter: 'getParentKey',
        value: ''
      },
      motherKey: {
        getter: 'getParentKey',
        value: ''
      },
      name: {},
      nodeType: {
        getter: 'getNodeType'
      },
      personData: {},
      unions: {
        value: []
      },
      ux: {
        value: []
      },
      vir: {
        value: []
      }
    }
  });


  var GenogramNodeModelList = Y.Base.create('genogramNodeModelList', Y.ModelList, [], {
    model: GenogramNodeModel
  });


  Y.namespace('app.genogram').GenogramModel = GenogramModel;
  Y.namespace('app.genogram').GenogramNodeModel = GenogramNodeModel;
  Y.namespace('app.genogram').GenogramNodeModelList = GenogramNodeModelList;
  Y.namespace('app.genogram').UnionTypes = GenogramModel.UnionTypes;


}, '0.0.1', {
    requires: [
      'yui-base',
      'model-sync-rest',
      'json-parse',
      'person-relationship-model',
      'usp-date'
    ]
});
