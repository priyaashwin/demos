YUI.add('user-results-filter-context', function(Y) {
    Y.namespace('app.admin.user').UserFilterContext = Y.Base.create('userFilterContext', Y.usp.ResultsFilterContext, [], {
        template: Y.Handlebars.templates["userActiveFilter"],
        initializer: function() {
            //plug in error handling
            this.plug(Y.Plugin.usp.FilterErrorsPlugin);
        },
        destructor: function() {
            //unplug everything we know about
            this.unplug(Y.Plugin.usp.FilterErrorsPlugin);
        }
    });
}, '0.0.1', {
    requires: ['results-filter', 'handlebars-user-templates']
});