YUI.add('user-results-filter', function(Y) {
    Y.namespace('app.admin.user').UserResultsFilterModel = Y.Base.create('userResultsFilterModel', Y.usp.ResultsFilterModel, [], {}, {
        ATTRS: {
            nameOrUserName: {
                USPType: 'String',
                value: ''
            },
            roleIds: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            }
        }
    });
    Y.namespace('app.admin.user').UserResultsFilter = Y.Base.create('userResultsFilter', Y.usp.app.ResultsFilter, [], {
        template: Y.Handlebars.templates['userFilter'],
        initializer: function() {
            //get the roles list as soon as possible
            this._getRoles().then(function(roleList) {
                var roles = roleList.toJSON().map(function(role) {
                    //convert to simple id,value object
                    return ({
                        id: role['id'],
                        active: role['active'],
                        description: role['description']
                    });
                });
                this.set('otherData', {
                    roles: roles
                });
            }.bind(this));
            this.after('otherDataChange', this.render, this);
        },
        _getRoles: function() {
            var model = new Y.usp.security.PaginatedSecurityRoleSummaryList({
                url: this.get('securityRoleListURL')
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    }else{
                      //success, so resolve the promise
                      resolve(data);
                    }
                });
            });
        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            roles: {
                value: []
            },
            securityRoleListURL: {
                value: ''
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base', 
               'app-filter',
               'handlebars-helpers', 
               'handlebars-user-templates', 
               'usp-security-SecurityRoleSummary'
               ]
});