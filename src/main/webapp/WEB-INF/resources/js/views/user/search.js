/* global window */
YUI.add('user-search-view', function(Y) {
    var L = Y.Lang,
        A = Y.Array,
        O = Y.Object,
        E = Y.Escape,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates;
    Y.namespace('app.admin.user').PaginatedFilteredUserSearchResultList = Y.Base.create("paginatedFilteredUserSearchResultList", Y.usp.person.PaginatedSecurityUserWithRolesAndPersonList, [Y.usp
        .ResultsFilterURLMixin
    ], {
        whiteList: ["nameOrUserName", "roleIds"],
        staticData: {
            useSoundex: false,
            appendWildcard: true,
            securityUserFilter: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            },
            context: {
                value: 'NONE'
            }
        }
    });
    Y.namespace('app.admin.user').UserFilter = Y.Base.create('userFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.admin.user.UserResultsFilterModel,
        filterContextType: Y.app.admin.user.UserFilterContext,
        filterType: Y.app.admin.user.UserResultsFilter
    });
    Y.namespace('app.admin.user').UserResults = Y.Base.create('userResults', Y.usp.app.Results, [], {
        getResultsListModel: function(config) {
            return new Y.app.admin.user.PaginatedFilteredUserSearchResultList({
                url: config.url,
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions,
                loggedInUserId = config.loggedInUserId;
            // declare the columns
            return ([{
                key: 'userIdentifier',
                label: labels.id,
                width: '4%',
                cellTemplate: USPTemplates.clickable(labels.viewUser, 'view', permissions.canView)
            }, {
                key: 'name',
                label: labels.name,
                width: '10%',
                sortable: true,
                cellTemplate: USPTemplates.clickable(labels.viewUser, 'view', permissions.canView)
            }, {
                key: 'userName',
                label: labels.userName,
                width: '8%',
                sortable: true
            }, {
                key: 'roles',
                allowHTML: true,
                label: labels.roles,
                width: '20%',
                formatter: function(o) {
                    if (o.data.profileNames) {
                    	return o.data.profileNames.map(function(profileName) {
                    		return E.html(profileName);
                    	}).join(', ');
                    }
                    return labels.noRoles;
                }
            }, {
                key: 'person',
                label: labels.person,
                width: '12%',
                formatter: function(o) {
                	var person = o.record.get('person');
                	var name;
                	if (person) {
                		name = o.record.get('person!name') + ' (' + o.record.get('person!personIdentifier') + ')';
                	} else {
                		name = 'No Associated Person';
                	}
                    return name;
                }
            }, {
                key: 'enabled',
                label: labels.enabled,
                width: '8%'
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '8%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'view',
                    title: labels.viewUser,
                    label: labels.viewUser,
                    enabled: permissions.canView
                }, {
                    clazz: 'edit',
                    title: labels.editUser,
                    label: labels.editUser,
                    enabled: permissions.canUpdate,
                    visible: function() {
                        // if system user, ensure the option is
                        // hidden
                        return !this.record.get('system');
                    }
                }, {
                    clazz: 'changePassword',
                    title: labels.changeUserPassword,
                    label: labels.changeUserPassword,
                    enabled: permissions.canChangePassword
                }, {
                    clazz: 'enableUser',
                    title: labels.enableUser,
                    label: labels.enableUser,
                    enabled: permissions.canUpdate,
                    visible: function() {
                        return !this.record.get('enabled');
                    }
                }, {
                    clazz: 'disableUser',
                    title: labels.disableUser,
                    label: labels.disableUser,
                    enabled: permissions.canUpdate,
                    visible: function() {
                        // if user himself, ensure the option is
                        // hidden,
                        var isVisible = false;
                        if (this.record.get('enabled') && this.record.get('id') !== loggedInUserId) {
                            isVisible = true;
                        }
                        return isVisible;
                    }
                }]
            }]);
        }
    });
    Y.namespace('app.admin.user').UserFilteredResults = Y.Base.create('userFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.admin.user.UserFilter,
        resultsType: Y.app.admin.user.UserResults,
        initializer: function(config) {
            //create checklist instance dialog view and add as event target
            this.userDialog = new Y.app.UserDialog(config.dialogConfig || {}).render().addTarget(this);
            
            this.toolbar=new Y.usp.app.AppToolbar({
              permissions:config.searchConfig.permissions
            }).addTarget(this);
            
            this._userEvtHandlers = [
                this.on('*:addUser', this.showAddDialog, this),
                this.on('*:view', this.showViewDialog, this),
                this.on('*:edit', this.showEditDialog, this),
                this.on('*:changePassword', this.showChangePasswordDialog, this),
                this.on('*:disableUser', this.showDisableUserDialog, this),
                this.on('*:enableUser', this.showEnableUserDialog, this),
                this.on('*:saved', this.reload, this)
            ];
        },
        destructor: function() {
            //unbind event handles
            if (this._userEvtHandlers) {
                A.each(this._userEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._userEvtHandlers = null;
            }
            if (this.userDialog) {
                this.userDialog.removeTarget(this);
                this.userDialog.destroy();
                delete this.userDialog;
            }
            
            if(this.toolbar){
              this.toolbar.removeTarget(this);
              this.toolbar.destroy();
              
              delete this.toolbar;
            }
        },
        _getRoles: function() {
            var model = new Y.usp.security.PaginatedSecurityRoleSummaryList({
                url: this.get('securityRoleListURL')
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    }else{
                      //success, so resolve the promise
                      resolve(data);
                    }
                });
            });
        },
        _getSystemConfig: function() {
            var model = new Y.usp.security.SecurityConfiguration({
                url: this.get('systemConfigURL')
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    }else{
                      //success, so resolve the promise
                      resolve(data);
                    }
                });
            });
        },
        showAddDialog: function(e) {
            //show the loading view while we start processing
            this.userDialog.showView('userProcessing');
            Y.Promise.all([this._getSystemConfig(),
                this._getRoles()
            ]).then(function(data) {
                this._showAddDialog(data[1].toJSON(), data[0].toJSON());
            }.bind(this)).
            then(null, function(err) {
                var view = this.userDialog.get('activeView');
                if (view) {
                    view.fire('error', {
                        error: err
                    });
                }
            }.bind(this));
        },
        _showAddDialog: function(availableRolesJSON, systemConfigDetails) {
            var config = this.get('dialogConfig').views.addUser.config;
            this.userDialog.showView('addUser', {
                model: new Y.app.NewUserForm({
                    url: config.url
                }),
                availableRolesJSON: availableRolesJSON,
                systemConfigDetails: systemConfigDetails,
            }, function(view) {
                //TODO - this is unnecessary - we know the view is in the DOM at this point
                view.shown();
                //Get the save button by name out of the footer and enable it.              
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        showEditDialog: function(e) {
            var record = e.record,
                //change password event can come from dialog then permissions not on event
                permissions = e.permissions||this.get('searchConfig').permissions;
            
            if (permissions.canUpdate === true) {
                //show the loading view while we start processing
                this.userDialog.showView('userProcessing');
                Y.Promise.all([this._getSystemConfig(),
                    this._getRoles()
                ]).then(function(data) {
                    this._showEditDialog(record, data[1].toJSON(), data[0].toJSON());
                }.bind(this)).
                then(null, function(err) {
                    var view = this.userDialog.get('activeView');
                    if (view) {
                        view.fire('error', {
                            error: err
                        });
                    }
                }.bind(this));
            }
        },
        _showEditDialog: function(record, availableRolesJSON, systemConfigDetails) {
            var config = this.get('dialogConfig').views.editUser.config
            this.userDialog.showView('editUser', {
                model: new Y.app.UpdateUserForm({
                    url: config.url,
                    id: record.get('id')
                }),
                availableRolesJSON: availableRolesJSON,
                systemConfigDetails: systemConfigDetails
            }, {
                modelLoad: true
            }, function(view) {
                //TODO - this is unnecessary - we know the view is in the DOM at this point
                view.shown();
                //Get the save button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                if (this.getButton('changePasswordButton', Y.WidgetStdMod.FOOTER) != null) {
                    this.getButton('changePasswordButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                }
            });
        },
        showViewDialog: function(e) {
            var permissions = e.permissions,
                record = e.record,
                config = this.get('dialogConfig').views.viewUser.config;
            if (permissions.canView === true) {
                this.userDialog.showView('viewUser', {
                    model: new Y.usp.person.SecurityUserWithRolesAndPerson({
                        url: config.url,
                        id: record.get('id')
                    })
                }, {
                    modelLoad: true
                }, function(view) {
                    // if this is a system user, hide the buttons
                    if (record.get('system') === true) {
                        this.getButton('editButton', Y.WidgetStdMod.FOOTER).hide();
                        this.getButton('changePasswordButton', Y.WidgetStdMod.FOOTER).hide();
                    } else {
                        if (permissions.canUpdate === true) {
                            this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                            this.getButton('changePasswordButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                        }
                    }
                });
            }
        },
        showChangePasswordDialog: function(e) {
            var record = e.record,
                //change password event can come from dialog then permissions not on event
                permissions = e.permissions||this.get('searchConfig').permissions,
                config = this.get('dialogConfig').views.changeUserPassword.config;
            if (permissions.canUpdate === true) {
                this.userDialog.showView('changeUserPassword', {
                    model: new Y.app.UpdateUserPasswordForm({
                        url: config.url,
                        id: record.get('id'),
                        objectVersion: record.get('objectVersion')
                    }),
                    otherData: {
                        targetName: record.get('name'),
                        targetIdentifier: record.get('userIdentifier')
                    }
                }, function(view) {
                    //Get the save button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        showDisableUserDialog: function(e) {
            var record = e.record,
                permissions = e.permissions,
                config = this.get('dialogConfig').views.disableUser.config;
            if (permissions.canUpdate === true) {
                this.userDialog.showView('disableUser', {
                    model: new Y.usp.person.SecurityUserWithRolesAndPerson({
                        id: record.get('id'),
                        url: config.url
                    }),
                }, {
                    modelLoad: true
                }, function(view) {
                    //the update URL is no the same as the save url
                    view.get('model').url = config.updateURL;
                    //Get the button by name out of the footer and enable it.
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },
        showEnableUserDialog: function(e) {
            var record = e.record,
                permissions = e.permissions,
                config = this.get('dialogConfig').views.enableUser.config;
            if (permissions.canUpdate === true) {
                this.userDialog.showView('enableUser', {
                    model: new Y.usp.person.SecurityUserWithRolesAndPerson({
                        url: config.url,
                        id: record.get('id')
                    }),
                }, {
                    modelLoad: true,
                }, function(view) {
                    //the update URL is not the same as the save url
                    view.get('model').url = config.updateURL;
                    //Get the button by name out of the footer and enable it.
                    this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                })
            }
        }
    }, {
        ATTRS: {
            securityRoleListURL: {
                value: ''
            },
            systemConfigURL: {
                value: ''
            },
            searchConfig: {
                value: {
                    loggedInUserId: '',
                    permissions: {
                        canUpdate: false,
                        canView: false,
                        canChangePassword: false
                    }
                }
            },
            dialogConfig: {}
        }
    });
}, '0.0.1', {
    requires: ['yui-base', 
               'view', 
               'escape', 
               'promise', 
               'results-templates', 
               'results-formatters', 
               'user-results-filter', 
               'user-results-filter-context',
               'usp-person-SecurityUserWithRolesAndPerson', 
               'usp-security-SecurityRoleSummary', 
               'usp-security-SecurityConfiguration', 
               'app-filtered-results', 
               'app-filter', 
               'app-results',
               'app-toolbar',
               'user-dialog-views'
              ]
});