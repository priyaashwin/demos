YUI.add('belongings-history-results', function(Y) {

  var L = Y.Lang,
    USPFormatters = Y.usp.ColumnFormatters,
    USPTemplates = Y.usp.ColumnTemplates;

  Y.namespace('app.person.belongings').BelongingHistoryResults = Y.Base.create('belongingHistoryResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      return new Y.usp.belongings.PaginatedBelongingsSnapshotList({
        url: L.sub(config.url, {
          id: config.id
        })
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;
      return ([{
        key: 'landingDate',
        label: labels.dateReceived,
        sortable: true,
        width: '25%',
        formatter: USPFormatters.date
      }, {
        key: 'location',
        label: labels.location,
        sortable: true,
        width: '25%',
        formatter: USPFormatters.codedEntry,
        codedEntries: Y.uspCategory.belongings.location.category.codedEntries
      }, {
        key: 'recorder!name',
        label: labels.recorder,
        sortable: true,
        width: '30%'
      }, {
        key: 'status',
        label: labels.status,
        sortable: true,
        width: '20%',
        formatter: USPFormatters.enumType,
        enumTypes: Y.usp.belongings.enum.BelongingsStatus.values
      }]);
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'results-formatters',
    'results-templates',
    'usp-belongings-BelongingsSnapshot',
    'categories-belongings-component-Location',
    'belongings-component-enumerations'
  ]
});
