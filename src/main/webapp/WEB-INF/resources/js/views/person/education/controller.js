YUI.add('education-controller-accordion', function (Y) {
    Y.namespace('app.person.education').EducationAccordion = Y.Base.create('educationAccordion', Y.usp.AccordionPanel, [], {

        initializer: function (config) {
        	Y.on('person:personDetailChanged', this.handleSave, this);
            this.personEducationView = new Y.app.PersonEducationView(config);
            this.personEducationView.addTarget(this);
            this.educationDialog = new Y.app.person.education.EducationDialog(config.dialogConfig).addTarget(this).render();

            this._educationEvtHandlers = [
                this.on('*:editEducation', this.showUpdateDialog, this),
                this.on('*:saved', this.handleSave, this)
            ];
        },
        render: function () {
            //render the person education view
            var personEducationViewContainer = this.personEducationView.render().get('container');
            //superclass call
            Y.app.person.education.EducationAccordion.superclass.render.call(this);

            //append the person education container into the accordion body
            this.getAccordionBody().appendChild(personEducationViewContainer);
            return this;
        },

        handleSave: function (e) {
            //refresh the view
            this.get('model').load();
        },

        showUpdateDialog: function (e) {
            var personModel = this.get('personModel'),
                educationModel = this.get('model'),
                permissions = this.get('permissions'),
                config = this.get('dialogConfig').views.updateEducation.config,
                subject = this.get('dialogConfig').views.updateEducation.subject;

            if (permissions.canUpdateEducation === true) {
                this.educationDialog.showView('updateEducation', {
                    model: new Y.app.person.education.PersonEducationModel({
                        url: Y.Lang.sub(config.url, {
                            subjectId: subject.subjectId
                        })
                    }),
                    educationModel: educationModel,
                    otherData: {
                        //just extract what we need out of the person model for the subject
                        subject: {
                            name: personModel.get('name'),
                            personIdentifier: personModel.get('personIdentifier'),
                            id: personModel.get('id')
                        }
                    }
                }, {
                    modelLoad: true

                }, function () {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },

        destructor: function () {
            this.personEducationView.removeTarget(this);
            this.personEducationView.destroy();
            delete this.personEducationView;

            this.educationDialog.removeTarget(this);
            this.educationDialog.destroy();
            delete this.educationDialog;

            this._educationEvtHandlers.forEach(function (handler) {
                handler.detach();
            });
        }
    });

    Y.namespace('app').PersonEducationView = Y.Base.create('personEducationView', Y.usp.person.PersonEducationView, [], {
        events: {
            '#editEducation': {
                click: 'editEducation'
            }
        },
        template: Y.Handlebars.templates['personEducationView'],
        initializer: function () {
            //publish events
            this.publish('editEducation', {
                preventable: true

            });
            this.get('personModel').after('change', this.render, this);
        },
        render: function () {
            var age = this.get('personModel').get('age'),
                estimatedDateOfBirth = this.get('personModel').get('dateOfBirthEstimated'),
                errorMessage,
                isPersonEducationVisible = this.isPersonEducationVisible(age, estimatedDateOfBirth);

            var html = this.template({
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                enumTypes: this.get('enumTypes'),
                isPersonEducationVisible: isPersonEducationVisible,
                errorMessage: this.setNoDataMessage(isPersonEducationVisible, estimatedDateOfBirth),
                canUpdateEducation: this.get('permissions.canUpdateEducation')
            });

            this.get('container').setHTML(html);
            return this
        },

        //edit Button click action
        editEducation: function (e) {
            model = this.get('model');
            e.preventDefault();
            //Fire the Edit Event
            this.fire("editEducation")
        },

        isPersonEducationVisible: function (personAge, personEstimatedDateOfBirth) {
            var isPersonInEducation = true,
                age = personAge,
                estimatedDateOfBirth = personEstimatedDateOfBirth;

            if (!age || age < 3 || age > 21 || estimatedDateOfBirth) {
                isPersonInEducation = false;
            }
            return isPersonInEducation;
        },

        setNoDataMessage: function (isEducationVisible, estimatedDateOfBirth) {
            var displayMessage = '';

            if (!isEducationVisible) {
                if (estimatedDateOfBirth) {
                    displayMessage = this.get('labels.noDataMessage');
                } else {
                    displayMessage = this.get('labels.noEducationMessage');
                }
            }
            return displayMessage;
        },
    }, {
        ATTRS: {
            enumTypes: {
                value: {
                    nationalCurriculumYear: Y.usp.person.enum.NationalCurriculumYear.values,
                    keyStage: Y.usp.person.enum.KeyStage.values
                },
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'accordion-panel',
        'handlebars-helpers',
        'education-dialog',
        'usp-person-PersonEducation',
        'education-facet-enumerations',
        'app-PersonEducationView',
        'usp-person-UpdatePersonEducation'

    ]
});