YUI.add('subjectaccessrequest-controller-accordion', function(Y) {
    Y.namespace('app.person.subjectaccessrequest').SubjectAccessRequestAccordion = Y.Base.create('subjectAccessRequestAccordion', Y.View, [], {
        initializer: function(config) {
            var model = this.get('model');

            var accordionConfig = {
                tablePanelConfig:{
                    title:config.title,
                    closed:config.closed
                },
                searchConfig: Y.merge(config.searchConfig, {
                    //mix the subjectId from the model into the searchConfig
                    subject: {
                        subjectId: model.get('id')
                    }
                })
            };

            this.subjectAccessRequestResults = new Y.app.person.subjectaccessrequest.SubjectAccessRequestResults(accordionConfig).addTarget(this);

            this.subjectAccessRequestDialog = new Y.app.person.subjectaccessrequest.SubjectAccessRequestDialog(config.dialogConfig).addTarget(this).render();

            this._subjectAccessRequestEvtHandlers = [
                              this.on('*:addSubjectAccessRequest', this.showAddDialog, this),
                              this.on('*:download', this.downloadAttachment, this),
                              this.on('*:fail', this.failSubjectAccessRequest, this),
                              this.on('*:saved', this.reloadResults, this)
                            ];
        },
        getAccordion:function(){
            return this.subjectAccessRequestResults.getTablePanel();
        },
        downloadAttachment: function(e) {
        	var downloadUrl = this.get('downloadUrl');
        	window.location=Y.Lang.sub(downloadUrl, {
                id: e.record.get('subjectAccessRequestAttachmentVO').id
        	});
        },
        failSubjectAccessRequest: function(e) {
        	var successfullyFailedMessage = this.get('searchConfig').labels.successfullySetAsFailed;
        	var model = this.get('model');
        	var failUrl = Y.Lang.sub(this.get('failUrl'), {
                id: e.record.get('id')
        	});
        	model.url = failUrl;
        	model.save(Y.bind(function(err, res) {
        		if (err) {
        			 alert('Sorry, this operation failed');
        		 } else {
             		Y.fire('infoMessage:message', {
                 		message: successfullyFailedMessage
                 	});
             		this.fire('saved');
        		 }
        	}, this));
        },
        reloadResults: function() {
        	this.subjectAccessRequestResults.reload(this.subjectAccessRequestResults);
        },
        render: function() {
            var permissions = this.get('permissions') || {},
            buttonConfig = this.get('addButtonConfig') || {},
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

                //append the results
                contentNode.append(this.subjectAccessRequestResults.render().get('container'));

                //construct the toolbar with the accordion button holder as the container
                this.toolbar = new Y.usp.app.AppToolbar({
                    permissions: permissions,
                    //set the toolbar node to the Button Holder on the TablePanel inside the subject access request results
                    toolbarNode: this.subjectAccessRequestResults.getTablePanel().getButtonHolder()
                }).addTarget(this);

                if (permissions.canAddSubjectAccessRequest) {
                    //add a button to the toolbar if user had add permission
                    this.toolbar.addButton({
                        icon: buttonConfig.icon,
                        className: 'pull-right pure-button-active',
                        action: 'addSubjectAccessRequest',
                        title: buttonConfig.title,
                        ariaLabel: buttonConfig.ariaLabel,
                        label: buttonConfig.label
                    });
                }

            //set fragment into container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this._subjectAccessRequestEvtHandlers.forEach(function(handler) {
                handler.detach();
            });

            this.subjectAccessRequestResults.removeTarget(this);
            this.subjectAccessRequestResults.destroy();
            delete this.subjectAccessRequestResults;

            if (this.toolbar) {
                this.toolbar.removeTarget(this);
                this.toolbar.destroy();
                delete this.toolbar;
            }

            this.subjectAccessRequestDialog.removeTarget(this);
            this.subjectAccessRequestDialog.destroy();
            delete this.subjectAccessRequestDialog;
        },
        showAddDialog:function(e){
            var personModel=this.get('model'),
                permissions = this.get('permissions'),
                config = this.get('dialogConfig').views.addSubjectAccessRequest.config;

            if(permissions.canAddSubjectAccessRequest){
              this.subjectAccessRequestDialog.showView('addSubjectAccessRequest', {
                model: new Y.app.person.subjectaccessrequest.NewSubjectAccessRequestForm({
                    //set the person id
                    personId:personModel.get('id'),
                    url: config.url
                }),
                otherData: {
                    //just extract what we need out of the person model for the subject
                    subject:{
                        name:personModel.get('name'),
                        personIdentifier:personModel.get('personIdentifier')
                    }
                }
              }, function(){
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
              });
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'accordion-panel',
        'subjectaccessrequest-results',
        'app-toolbar',
        'subjectaccessrequest-dialog'
    ]
});
