YUI.add('person-details-screen', function (Y) {
    var L = Y.Lang,
        A = Y.Array,
        E = Y.Escape,
        UP = Y.app.utils.person;

    var citizenshipResultsTable = Y.Base.create('citizenshipResultsTable', Y.usp.app.Results, [], {
        resultsType: Y.usp.ResultsTable,
        getResultsListModel: function (config) {
            return new Y.usp.person.CitizenshipList({
                items: this.get('citizenships')
            });
        },
        getColumnConfiguration: function (config) {
            var labels = config.labels || {};
            return ([{
                    key: 'citizenship',
                    label: labels.citizenship,
                    width: '50%',
                    formatter: Y.usp.ColumnFormatters.codedEntry,
                    codedEntries: Y.uspCategory.address.countryCode.category.codedEntries
                },
                {
                    key: 'startDate',
                    label: labels.startDate,
                    width: '25%',
                    formatter: Y.usp.ColumnFormatters.date
                },
                {
                    key: 'endDate',
                    label: labels.endDate,
                    width: '25%',
                    formatter: Y.usp.ColumnFormatters.date
                }
            ]);
        }
    });

    /***
     * 
     * Person Core Identification Details
     * 
     */
    Y.namespace('app').PersonDetailsIdentificationView = Y.Base.create('personDetailsIdentificationView', Y.usp.person.PersonView, [], {
        events: {
            '.edit-personDetails': {
                click: '_editPersonDetails'
            },
            '.change-personRoles': {
                click: '_changePersonRoles'
            },
            '.add-personRoles': {
                click: '_addPersonRoles'
            },
            '.edit-PersonName': {
                click: '_editPersonName'
            },
            '.remove-PersonName': {
                click: '_removePersonName'
            },
            '.change-Unborn': {
                click: '_changeUnbornPerson'
            },
            '.change-NotCarried': {
                click: '_changeNotCarriedPerson'
            },
            '.change-Alive': {
                click: '_changeAlivePerson'
            },
            '.edit-deceasedToAlive': {
                click: '_editDeceasedToAlive'
            },
            '.edit-picture': {
                click: '_editPicture'
            },
            '.remove-picture': {
                click: '_removePicture'
            }
        },

        template: Y.Handlebars.templates["personDetailsView"],
        previousNamesSetup: function (model) {
            var otherNamesList = model.get('otherNames');
            if (otherNamesList) {
                var previousNames = [];
                var previousNamesObject = {};
                var previousNameObject = {};

                function storePreviousName(otherName) {
                    var previousName = "";
                    if (otherName.type === "PREVIOUS") {

                        if (otherName.title) {
                            var titleEntry = Y.uspCategory.person.title.category.codedEntries[otherName.title];
                            var titleName = titleEntry ? titleEntry.name : "(Invalid title code provided)";
                            previousName = titleName + " ";
                        }
                        if (otherName.forename) {
                            previousName = previousName + otherName.forename + " ";
                        }
                        if (otherName.middleNames) {
                            previousName = previousName + otherName.middleNames + " ";
                        }
                        if (otherName.surname) {
                            previousName = previousName + otherName.surname;
                        }
                        previousNameObject = { "id": otherName.id, "name": previousName };
                        previousNames.push(previousNameObject);

                    }
                }

                otherNamesList.forEach(storePreviousName);

                return previousNames;
            }
        },
        aliasesSetup: function (model) {
            var otherNamesList = model.get('otherNames');
            if (otherNamesList) {
                var aliases = [];

                function storeAlias(otherName) {
                    var alias = "";
                    if (otherName.type === "ALIAS") {
                        if (otherName.title) {
                            var titleEntry = Y.uspCategory.person.title.category.codedEntries[otherName.title];
                            var titleName = titleEntry ? titleEntry.name : "(Invalid title code provided)";
                            alias = alias + " ";
                        }
                        if (otherName.forename) {
                            alias = alias + otherName.forename + " ";
                        }
                        if (otherName.middleNames) {
                            alias = alias + otherName.middleNames + " ";
                        }
                        if (otherName.surname) {
                            alias = alias + otherName.surname;
                        }

                        aliases.push(alias);
                    }
                }

                otherNamesList.forEach(storeAlias);

                return aliases;
            }
        },
        yearSetup: function (model) {
            var age = model.get('age');
            var ageSetup;
            if (age > 1) {
                ageSetup = ' (' + age + ' years)';
            } else if (age < 0) {
                ageSetup = '';
            } else {
                ageSetup = ' (' + age + ' year)';
            }

            return ageSetup;
        },
        getPersonTypes: function (type) {

            var personTypes = this.get('model').toJSON().personTypes;
            var lifeState = UP.GetImpliedLifeState(this.get('model').get('lifeState'));

            if (personTypes) {

                var hasOther = personTypes.indexOf('OTHER') > -1;
                var hasClient = personTypes.indexOf('CLIENT') > -1;
                var hasProfessional = personTypes.indexOf('PROFESSIONAL') > -1;

                if (type == 'otherAliveOnly') {
                    if (lifeState === 'ALIVE' && hasOther && !hasClient && !hasProfessional) {
                        return true;
                    }
                }

                if (type == 'allAlivePersonTypes') {
                    if (lifeState === 'UNBORN' || lifeState === 'NOT_CARRIED_TO_TERM' || lifeState === 'DECEASED' || (lifeState === 'ALIVE' && hasOther && hasClient && hasProfessional)) {
                        return true;
                    }
                }

                if (type == 'notProfessionalOnly') {
                    if (hasOther || hasClient || !hasProfessional) {
                        return true;
                    }
                }

            }

        },
        initializer: function () {
            //publish events
            this.publish('editPersonDetails', {
                preventable: true
            });
            this.publish('editPersonName', {
                preventable: true
            });
            this.publish('changePersonRoles', {
                preventable: true
            });
            this.publish('addPersonRoles', {
                preventable: true
            });
        },

        render: function () {
            var nhsFirstThreeDigits = "",
                nhsSecondThreeDigits = "",
                nhsLastFourDigits = "",
                notProfessionalOnly = this.getPersonTypes('notProfessionalOnly'),
                lifeState = Y.app.utils.person.GetImpliedLifeState(this.get('model').get('lifeState')),
                isUnborn = lifeState === 'UNBORN',
                isNotCarried = lifeState === 'NOT_CARRIED_TO_TERM',
                isAlive = lifeState === 'ALIVE',
                isDeceased = lifeState === 'DECEASED',
                nhsNumber = this.get('model').get('nhsNumber');

            if (nhsNumber) {
                nhsFirstThreeDigits = nhsNumber.substr(0, 3);
                nhsSecondThreeDigits = nhsNumber.substr(3, 3);
                nhsLastFourDigits = nhsNumber.substr(6, 4);
            }
            var html = this.template({

                labels: this.get('labels'),
                codedEntries: this.get('codedEntries'),
                previousNames: this.previousNamesSetup(this.get('model')),
                aliases: this.aliasesSetup(this.get('model')),
                modelData: this.get('model').toJSON(),
                nhsFirstThreeDigits: nhsFirstThreeDigits,
                nhsSecondThreeDigits: nhsSecondThreeDigits,
                nhsLastFourDigits: nhsLastFourDigits,
                canEdit: this.get('canEdit') ? '' : 'none;',
                canDeletePersonName: this.get('canDeletePersonName') ? this.get('canDeletePersonName') : 'none;',
                age: this.yearSetup(this.get('model')),
                otherAliveOnly: this.getPersonTypes('otherAliveOnly'),
                allAlivePersonTypes: this.getPersonTypes('allAlivePersonTypes'),
                notProfessionalOnly: notProfessionalOnly,
                unbornPerson: isUnborn,
                notCarriedToTermPerson: isNotCarried,
                alivePerson: isAlive,
                deceasedPerson: isDeceased,
                notProfessionalOnlyOrDeceased: notProfessionalOnly || isDeceased,
                canUpdateDeceasedToAlive: this.get('canUpdateDeceasedToAlive'),
                canAddPersonType: this.get('canAddPersonType'),
                enumTypes: this.get('enumTypes')
            });

            this.get('container').setHTML(html);

            var pictureId = this.get('model').get('pictureId');

            if (pictureId && this.get('canViewPersonPicture')) {
                var pictureUrl = L.sub(this.get('pictureUrl'), { pictureId: pictureId });
                this.get('container').one('.person-image').set('src', pictureUrl);
                this.get('container').one('.person-image-options-separator').setStyle('display', 'inline');
                this.get('container').one('.remove-picture').setStyle('display', 'inline');
            } else {
                this.get('container').one('.person-image').set('src', 'img/anon.png');
                this.get('container').one('.person-image-options-separator').setStyle('display', 'none');
                this.get('container').one('.remove-picture').setStyle('display', 'none');
            }

            this.citizenshipResults = new citizenshipResultsTable({
                tablePanelConfig: {
                    title: 'Citizenships'
                },
                searchConfig: {
                    labels: this.get('labels'),
                    initialLoad: false
                },
                citizenships: this.get('model').get('citizenships')
            });
            this.get('container').one('#citizenships-table').setHTML(this.citizenshipResults.render().get('container'));

            return this;
        },

        destructor: function () {
            if (this.citizenshipResults) {
                this.citizenshipResults.destroy();
                delete this.citizenshipResults;
            }
        },

        _editPersonDetails: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'editPerson',
                id: model.get('id'),
                personRoles: model.get('personTypes'),
                lifeState: model.get('lifeState'),
                pictureUrl: this.get('pictureUrl'),
                pictureInfoUrl: this.get('pictureInfoUrl')
            });
        },
        _editPersonName: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'changePersonName',
                id: model.get('id')
            });
        },
        _removePersonName: function (e) {
            var model = this.get('model');
            e.preventDefault();
            Y.fire('person:showDialog', {
                action: 'removePersonName',
                id: e.target._node.id,
                name: e.target._node.name
            });
        },
        _changeUnbornPerson: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'changeUnborn',
                id: model.get('id')
            });
        },
        _changeNotCarriedPerson: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'changeNotCarried',
                id: model.get('id')
            });
        },
        _changeAlivePerson: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'changeAlive',
                id: model.get('id'),
                personRoles: model.get('personTypes'),
                lifeState: model.get('lifeState')
            });
        },
        _editDeceasedToAlive: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'changeDeceasedToAliveAdministratively',
                id: model.get('id')
            });
        },
        _changePersonRoles: function (e) {
            var model = this.get('model');
            e.preventDefault();
            // Can only change role if person has only role of OTHER
            if (model.get('personTypes').indexOf('CLIENT') > -1 || model.get('personTypes').indexOf('PROFESSIONAL') > -1) {
                return;
            } else {
                Y.fire('person:showDialog', {
                    action: 'changePersonRoles',
                    id: model.get('id'),
                    personRoles: model.get('personTypes')
                });
            }
        },
        _addPersonRoles: function (e) {
            var model = this.get('model');
            e.preventDefault();
            // Can only add role if person doesn't currently have all three roles 
            if (model.get('personTypes').indexOf('CLIENT') > -1 && model.get('personTypes').indexOf('PROFESSIONAL') > -1 && model.get('personTypes').indexOf('OTHER') > -1) {
                return;
            } else {
                Y.fire('person:showDialog', {
                    action: 'addPersonRoles',
                    id: model.get('id'),
                    personRoles: model.get('personTypes').slice()
                });
            }
        },
        _editPicture: function (e) {
            var model = this.get('model');
            e.preventDefault();

            Y.fire('personPicture:showDialog', {
                action: 'editPersonPicture',
                personDetailsModel: model,
            });
        },
        _removePicture: function (e) {
            var model = this.get('model');
            e.preventDefault();

            Y.fire('personPicture:showDialog', {
                action: 'removePersonPicture',
                personDetailsModel: model,
                id: model.get('id'),
            });
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    ethnicity: Y.uspCategory.person.ethnicity.category.codedEntries,
                    title: Y.uspCategory.person.title.category.codedEntries,
                    professionalTitle: Y.uspCategory.person.professionalTitle.category.codedEntries,
                    personType: Y.uspCategory.person.personType.category.codedEntries,
                    gender: Y.uspCategory.person.gender.category.codedEntries,
                    genderIdentity: Y.uspCategory.person.genderIdentity.category.codedEntries,
                    sexualOrientation: Y.uspCategory.person.sexualOrientation.category.codedEntries,
                    country: Y.uspCategory.address.countryCode.category.codedEntries,
                    religion: Y.uspCategory.person.religion.category.codedEntries
                }
            },
            enumTypes: {
                value: {
                    notCarriedToTerm: Y.usp.person.enum.NotCarriedToTermType.values
                }
            }
        }
    });

    Y.namespace('app').PersonDetailsIdentificationAccordion = Y.Base.create('personDetailsIdentificationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {

            this.personDetailsIdentificationView = new Y.app.PersonDetailsIdentificationView(config);

            this.personDetailsIdentificationView.addTarget(this);
        },
        render: function () {
            //render the person details view
            var personDetailsIdentificationViewContainer = this.personDetailsIdentificationView.render().get('container');

            //superclass call
            Y.app.PersonDetailsIdentificationAccordion.superclass.render.call(this);

            //stick the person details container into the accordion
            this.getAccordionBody().appendChild(personDetailsIdentificationViewContainer);

            return this;
        },
        destructor: function () {
            this.personDetailsIdentificationView.removeTarget(this);

            this.personDetailsIdentificationView.destroy();

            delete this.personDetailsIdentificationView;
        }
    });

    /***
     * 
     * Person Reference Number Identification Details
     * 
     */
    Y.namespace('app').PersonReferenceNumberIdentificationView = Y.Base.create('personReferenceNumberIdentificationView', Y.usp.person.PersonReferenceNumberView, [], {
        events: {
            '.add-referenceNumber': {
                click: '_addReferenceNumber'
            },
            '.editRemove-referenceNumber': {
                click: '_editRemoveReferenceNumber'
            },
            '.person-link': {
                click: '_handleClickPersonLink'
            }
        },

        template: Y.Handlebars.templates['personReferenceNumberIdentificationView'],

        //function to match Active CodedEntries with that of Active ReferenceNumbers associated with person
        checkActiveReferenceTypes: function (model) {

            // Count active coded entries for referenceType
            var activeReferenceTypes = Y.secured.uspCategory.person.referencetype.category.getActiveCodedEntries(null, 'write');
            var countActiveTypes = 0;

            Y.Object.each(activeReferenceTypes, function () {
                countActiveTypes += 1;
            });

            // Count Active ReferenceNumbers associated with person
            var referenceNumbers = model.get('referenceNumbers');
            var countActiveNumbers = 0;
            var activeNumbersArray = new Array();

            if (referenceNumbers) {
                Y.Object.each(referenceNumbers, function (referenceNumber) {
                    if (Y.secured.uspCategory.person.referencetype.category.codedEntries[referenceNumber.type].active) {
                        countActiveNumbers += 1;
                        activeNumbersArray.push(referenceNumber.type);
                    }
                });
                //set view attribute to pass the array to personDialog
                this.activeNumbers = activeNumbersArray;
            }

            // Hide add button when there are equal number of activeTypes and activeNumbers.
            if (countActiveTypes == countActiveNumbers) {
                return false;
            }

            // show add button by default
            return true;

        },

        initializer: function (config) {
            //publish events
            this.publish('editRemoveReferenceNumber', {
                preventable: true
            });
            this.publish('addReferenceNumber', {
                preventable: true
            });
            this.personLinkUrl = config.personLinkUrl;
        },

        render: function () {

            var html = this.template({
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries'),
                modelData: this.get('model').toJSON(),
                showAdd: this.checkActiveReferenceTypes(this.get('model')),
                canAddReferenceNumber: this.get('canAddReferenceNumber') ? '' : 'none;',
                canEditReferenceNumber: this.get('canEditReferenceNumber') ? '' : 'none;',
                canRemoveReferenceNumber: this.get('canRemoveReferenceNumber') ? '' : 'none;'
            });

            this.get('container').setHTML(html);

            return this;
        },

        _editRemoveReferenceNumber: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'editRemovePersonReferenceNumber',
                perId: model.get('personIdentifier'),
                name: model.get('name'),
                id: model.get('id'),
                canRemoveReferenceNumber: this.get('canRemoveReferenceNumber')

            });
        },
        _addReferenceNumber: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'addPersonReferenceNumber',
                id: model.get('personIdentifier'),
                name: model.get('name'),
                activeReferenceNumbers: this.activeNumbers
            });

        },
        _handleClickPersonLink: function (e) {
            window.location = Y.Lang.sub(this.personLinkUrl, { id: e.currentTarget.getAttribute('data-type-id') });
        }
    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    // Needs to be all reference numbers, not just active ones (so that all are shown).
                    referenceNumberType: Y.secured.uspCategory.person.referencetype.category.codedEntries
                }
            },
            activeNumbers: {},
            personLinkUrl: { value: 'notset' }
        }
    });

    Y.namespace('app').ReferenceNumberPersonIdentificationAccordion = Y.Base.create('referenceNumberPersonIdentificationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            //We are creating a new personReferenceNumberIdentificationView within this view using the this keyword
            //and mixing in the config parameter
            this.personReferenceNumberIdentificationView = new Y.app.PersonReferenceNumberIdentificationView(config);

            // Add event targets
            this.personReferenceNumberIdentificationView.addTarget(this);
        },
        render: function () {
            //render the reference number view
            var personReferenceNumberViewContainer = this.personReferenceNumberIdentificationView.render().get('container');

            //superclass call
            Y.app.ReferenceNumberPersonIdentificationAccordion.superclass.render.call(this);

            //stick the reference number view container into the accordion
            this.getAccordionBody().appendChild(personReferenceNumberViewContainer);

            return this;
        },
        destructor: function () {
            this.personReferenceNumberIdentificationView.removeTarget(this);

            this.personReferenceNumberIdentificationView.destroy();

            delete this.personReferenceNumberIdentificationView;
        }
    });

    /***
     * 
     * Person Contact Identification Details
     * 
     */
    Y.namespace('app').PersonContactIdentificationView = Y.Base.create('personContactIdentificationView', Y.usp.contact.ContactView, [], {
        events: {
            '.add-contact': {
                click: '_addContact'
            },
            '.editRemove-contact': {
                click: '_editRemoveContact'
            }
        },

        template: Y.Handlebars.templates['personContactIdentificationView'],

        initializer: function () {
            //publish events
            this.publish('editRemoveContact', {
                preventable: true
            });
            this.publish('addContact', {
                preventable: true
            });
        },

        render: function () {
            var tcontacts = this.get('model').get('telephoneContacts'),
                mcontacts = this.get('model').get('mobileContacts'),
                fcontacts = this.get('model').get('faxContacts'),
                econtacts = this.get('model').get('emailContacts'),
                wcontacts = this.get('model').get('webContacts'),
                scontacts = this.get('model').get('socialMediaContacts');

            var html = this.template({
                labels: this.get('labels'),
                codedEntries: this.get('codedEntries'),
                modelData: this.get('model').toJSON(),
                telephoneContacts: this.groupContacts(tcontacts),
                mobileContacts: this.groupContacts(mcontacts),
                faxContacts: this.groupContacts(fcontacts),
                emailContacts: this.groupContacts(econtacts),
                socialMediaContacts: this.groupContacts(scontacts),
                webContacts: this.groupContacts(wcontacts),
                canAddContact: this.get('canAddContact') ? '' : 'none;',
                canEditContact: this.get('canEditContact') ? '' : 'none;'

            });

            this.get('container').setHTML(html);

            return this;

        },

        groupContacts: function (contactList) {

            if (contactList && contactList.length > 0) {
                var contactTypeList = [];
                var contactDetailsList = [];
                var prevType = contactList[0].contactType,
                    prevUsage = contactList[0].contactUsage,
                    j = 0,
                    k = 0;

                function groupContactsBySubTypes(contact) {

                    // the value, isVerified and startDate fields are now stored in the contactDetails object
                    var contactDetails = {
                        value: null,
                        isVerified: null,
                        verifiedDate: null,
                        startDate: null
                    };

                    if (contact.contactType != prevType || contact.contactUsage != prevUsage) {
                        var contactTypeRec = {
                            contactDetails: contactDetailsList,
                            contactType: prevType,
                            contactUsage: prevUsage
                        };

                        contactTypeList[j] = contactTypeRec;
                        j++, k = 0;

                        contactDetailsList = [];
                        prevType = contact.contactType;
                        prevUsage = contact.contactUsage;
                    }
                    if (contact.number) {
                        contactDetails.value = contact.number;
                    } else if (contact.address) {
                        contactDetails.value = contact.address;
                    } else if (contact.identifier) {
                        contactDetails.value = contact.identifier;
                    }
                    if (contact.verified == true) {
                        contactDetails.isVerified = true;
                        contactDetails.verifiedDate = contact.verifiedDate;
                    }
                    contactDetails.startDate = contact.startDate;
                    contactDetailsList[k] = Object.assign({}, contactDetails);

                    k++;

                }

                contactList.forEach(groupContactsBySubTypes);

                var contactTypeRec = {
                    contactType: prevType,
                    contactUsage: prevUsage,
                    contactDetails: contactDetailsList
                };

                contactTypeList[j] = contactTypeRec;
                return contactTypeList;
            }

        },

        _editRemoveContact: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event  		
            Y.fire('person:showDialog', {
                action: 'editRemoveContact',
                perId: model.get('personIdentifier'),
                name: model.get('name'),
                id: model.get('id'),
                telephoneContacts: model.get('telephoneContacts'),
                mobileContacts: model.get('mobileContacts'),
                faxContacts: model.get('faxContacts'),
                emailContacts: model.get('emailContacts'),
                socialMediaContacts: model.get('socialMediaContacts'),
                webContacts: model.get('webContacts'),
                canRemoveContact: this.get('canRemoveContact')

            });
        },
        _addContact: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'addPersonContact',
                id: model.get('personIdentifier'),
                name: model.get('name'),
                personTypes: model.get('personTypes')
            });

        }

    }, {
        ATTRS: {
            codedEntries: {
                value: {
                    contactType: Y.uspCategory.contact.contactType.category.codedEntries,
                    contactUsage: Y.uspCategory.contact.contactUsage.category.codedEntries,
                }
            }
        }
    });

    Y.namespace('app').PersonContactIdentificationAccordion = Y.Base.create('personContactIdentificationAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {

            this.personContactIdentificationView = new Y.app.PersonContactIdentificationView(config);

            this.personContactIdentificationView.addTarget(this);
        },
        render: function () {
            //render the person details view
            var personContactIdentificationViewContainer = this.personContactIdentificationView.render().get('container');

            //superclass call
            Y.app.PersonContactIdentificationAccordion.superclass.render.call(this);

            //stick the person details container into the accordion
            this.getAccordionBody().appendChild(personContactIdentificationViewContainer);

            return this;
        },
        destructor: function () {
            this.personContactIdentificationView.removeTarget(this);

            this.personContactIdentificationView.destroy();

            delete this.personContactIdentificationView;
        }
    });

    /***
     * 
     * Person Languages Details
     * 
     */

    Y.namespace('app').PersonLanguagesView = Y.Base.create('personLanguagesView', Y.usp.person.PersonWithLanguagesView, [], {
        events: {
            '#addLanguagesButton': {
                click: '_addLanguages'
            },
            '#editLanguagesButton': {
                click: '_editLanguages'
            }
        },

        template: Y.Handlebars.templates['personLanguagesView'],

        render: function () {
            var languages = this.get('model').get('personLanguageAssignmentVOs'),
                proficiencyInOfficialLanguage = this.get('model').get('proficiencyInOfficialLanguage'),
                interpreterRequired = this.get('model').get('interpreterRequired'),
                nonVerbalLanguages = this.get('model').get('personNonVerbalLanguageAssignmentVOs'),
                otherLanguages = this.otherLanguagesExist(),
                officialLanguage = true;
            if (languages) {
                officialLanguage = this.isPrimaryLanguageOfficialLanguage();
            }
            var html = this.template({
                languages: languages,
                otherLanguages: otherLanguages,
                nonVerbalLanguages: nonVerbalLanguages,
                interpreterRequired: interpreterRequired ? 'Yes' : 'No',
                proficiencyInOfficialLanguage: proficiencyInOfficialLanguage ? Y.uspCategory.person.proficiencyInOfficialLanguage.category.codedEntries[proficiencyInOfficialLanguage].name : '',
                labels: this.get('labels'),
                modelData: this.get('model').toJSON(),
                canUpdateLanguage: this.get('canUpdateLanguage') ? '' : 'none;',
                officialLanguage: officialLanguage
            });

            this.get('container').setHTML(html);

            return this;

        },

        otherLanguagesExist: function () {
            var languages = this.get('model').get('personLanguageAssignmentVOs');
            if (typeof languages != 'undefined' && languages.length) {
                for (var i = 0, len = languages.length; i < len; i++) {
                    if (!languages[i].primaryLanguage) {
                        return true;
                        break;
                    };
                };
            }
            return false;
        },
        isPrimaryLanguageOfficialLanguage: function () {
            var languages = this.get('model').get('personLanguageAssignmentVOs');
            if (typeof languages != 'undefined' && languages.length) {
                for (var i = 0, len = languages.length; i < len; i++) {
                    if (languages[i].primaryLanguage) {
                        return languages[i].codedEntryVO.code === localLanguageCode;
                        break;
                    }
                }
            }
            return false;
        },
        _addLanguages: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'updatePersonLanguage',
                id: this.get('model').get('id'),
                targetPersonId: model.get('personIdentifier'),
                targetPersonName: model.get('name'),
                edit: false
            });
        },
        _editLanguages: function (e) {
            var model = this.get('model');
            e.preventDefault();
            //Fire the edit event
            Y.fire('person:showDialog', {
                action: 'updatePersonLanguage',
                id: this.get('model').get('id'),
                targetPersonId: model.get('personIdentifier'),
                targetPersonName: model.get('name'),
                edit: true
            });
        }

    }, {
        ATTRS: {}
    });

    Y.namespace('app').PersonLanguagesAccordion = Y.Base.create('personLanguagesAccordion', Y.usp.AccordionPanel, [], {
        initializer: function (config) {

            this.personLanguagesView = new Y.app.PersonLanguagesView(config);

            this.personLanguagesView.addTarget(this);
        },
        render: function () {
            //render the person details view
            var personLanguagesViewContainer = this.personLanguagesView.render().get('container');

            //superclass call
            Y.app.PersonLanguagesAccordion.superclass.render.call(this);

            //stick the person details container into the accordion
            this.getAccordionBody().appendChild(personLanguagesViewContainer);

            return this;
        },
        destructor: function () {
            this.personLanguagesView.destroy();

            delete this.personLanguagesView;
        }
    });

    /***
     * 
     * Person Details View
     * 
     */
    Y.namespace('app').PersonDetailsView = Y.Base.create('personDetailsView', Y.View, [], {

        initializer: function (config) {
            var referenceNumberConfig = config.referenceNumberConfig || {};
            var personDetailsConfig = config.personDetailsConfig || {};
            var personContactsConfig = config.personContactsConfig || {};
            var personAddressConfig = config.personAddressConfig || {};
            var personEducationConfig = config.personEducationConfig || {};
            var personLanguagesConfig = config.personLanguagesConfig || {};
            var outputDialogConfig = config.outputDialogConfig || {};
            var personBelongingsConfig = config.personBelongingsConfig || {};
            var personSubjectAccessRequestConfig = config.personSubjectAccessRequestConfig || {};

            if (referenceNumberConfig.canViewReferenceNumber === true) {
                //create reference number identification view
                this.referenceNumbersView = new Y.app.ReferenceNumberPersonIdentificationAccordion(Y.merge(referenceNumberConfig, {
                    model: this.get('model')
                }));
            }
            this.personDetailsView = new Y.app.PersonDetailsIdentificationAccordion(Y.merge(personDetailsConfig, {
                model: this.get('model')
            }));
            if (personContactsConfig.canViewContact === true) {
                this.personContactView = new Y.app.PersonContactIdentificationAccordion(Y.merge(personContactsConfig, {
                    model: this.get('model')
                }));
            }
            this.personLanguagesView = new Y.app.PersonLanguagesAccordion(Y.merge(personLanguagesConfig, {
                model: this.get('model')
            }));

            if (personAddressConfig.permissions.canView === true) {
                this.personAddressView = new Y.app.person.address.AddressAccordion(Y.merge(personAddressConfig, {
                    model: this.get('model')
                }));
            }

            if (personEducationConfig.permissions.canView === true) {
                var url = personEducationConfig.dialogConfig.views.updateEducation.config.url
                var personEducationModel = new Y.usp.person.PersonEducation({
                    url: L.sub(url, {
                        subjectId: personEducationConfig.subject.subjectId
                    })
                });

                personEducationModel.load();
                this.personEducationView = new Y.app.person.education.EducationAccordion(Y.merge(personEducationConfig, {
                    model: personEducationModel,
                    personModel: this.get('model')
                }));
            }
            if (personBelongingsConfig.permissions.canView === true) {
                this.belongingsAccordion = new Y.app.person.belongings.BelongingsAccordion(Y.merge(personBelongingsConfig, {
                    model: this.get('model')
                }));
            }
            if (personSubjectAccessRequestConfig.permissions.canView === true) {
                this.subjectAccessRequestAccordion = new Y.app.person.subjectaccessrequest.SubjectAccessRequestAccordion(Y.merge(personSubjectAccessRequestConfig, {
                    model: this.get('model')
                }));
            }
            //configure the dialog for output template - no need to add as a bubble target
            this.outputDocumentDialog = new Y.app.output.OutputDocumentDialog(config.outputDialogConfig).render();

            // Add event targets
            if (this.referenceNumbersView) {
                this.referenceNumbersView.addTarget(this);
            }
            this.personDetailsView.addTarget(this);
            if (this.personContactView) {
                this.personContactView.addTarget(this);
            }
            if (this.personEducationView) {
                this.personEducationView.addTarget(this)
            }
            this.personLanguagesView.addTarget(this);
            if (this.personAddressView) {
                this.personAddressView.addTarget(this);
            }
            this._personEvtHandlers = [
                Y.on('person:outputDocument', this.showOutputDialog, this)
            ];
            this.plug(Y.Plugin.app.AccordionToggleAllPlugin, {
                srcNode: '#collapse-button',
                autoDetect: false
            });

            //add accordion panels to toggle
            if (this.referenceNumbersView) {
                this.accordionToggleAll.addAccordion(this.referenceNumbersView);
            }
            this.accordionToggleAll.addAccordion(this.personDetailsView);
            if (this.personContactView) {
                this.accordionToggleAll.addAccordion(this.personContactView);
            }
            this.accordionToggleAll.addAccordion(this.personLanguagesView);

            if (this.personEducationView) {
                this.accordionToggleAll.addAccordion(this.personEducationView);
            }
            if (this.personAddressView) {
                this.accordionToggleAll.addAccordion(this.personAddressView.getAccordion());
            }
            if (this.belongingsAccordion) {
                this.accordionToggleAll.addAccordion(this.belongingsAccordion.getAccordion());
            }
            if (this.subjectAccessRequestAccordion) {
                this.accordionToggleAll.addAccordion(this.subjectAccessRequestAccordion.getAccordion());
            }
        },
        showOutputDialog: function (e) {
            var model = this.get('model');

            this.outputDocumentDialog.showView('produceDocument', {
                //pass the dialog so we can reposition it after the render
                dialog: this.outputDocumentDialog,
                model: model
            });
        },
        render: function () {
            // Will use the container property when we create the PersonDetailsView to attach this containers content
            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            // This renders each of the views into the document fragment,
            // then sets the fragment as the contents of this view's container.
            contentNode.append(this.personDetailsView.render().get('container'));
            if (this.personAddressView) {
                contentNode.append(this.personAddressView.render().get('container'));
            }
            if (this.personContactView) {
                contentNode.append(this.personContactView.render().get('container'));
            }
            if (this.referenceNumbersView) {
                contentNode.append(this.referenceNumbersView.render().get('container'));
            }
            contentNode.append(this.personLanguagesView.render().get('container'));

            if (this.personEducationView) {
                //education accordion exists only if person has view education permission
                contentNode.append(this.personEducationView.render().get('container'));
            }
            if (this.belongingsAccordion) {
                //belongings accordion exists only if person has view belongings permission
                contentNode.append(this.belongingsAccordion.render().get('container'));
            }
            if (this.subjectAccessRequestAccordion) {
                //subject access request accordion exists only if person has view subject access request permission
                contentNode.append(this.subjectAccessRequestAccordion.render().get('container'));
            }
            //finally set the content into the container
            this.get('container').setHTML(contentNode);
            return this;
        },
        destructor: function () {
            this._personEvtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._personEvtHandlers;
            if (this.referenceNumbersView) {
                this.referenceNumbersView.destroy();
                delete this.referenceNumbersView;
            }
            this.personDetailsView.destroy();
            delete this.personDetailsView;
            if (this.personContactView) {
                this.personContactView.destroy();
                delete this.personContactView;
            }
            if (this.personEducationView) {
                this.personEducationView.destroy();
                delete this.personEducationView;
            }
            if (this.personAddressView) {
                this.personAddressView.destroy();
                delete this.personAddressView;
            }
            if (this.belongingsAccordion) {
                this.belongingsAccordion.destroy();
                delete this.belongingsAccordion;
            }
            if (this.subjectAccessRequestAccordion) {
                this.subjectAccessRequestAccordion.destroy();
                delete this.subjectAccessRequestAccordion;
            }
        }
    }, {
        ATTRS: {
            model: {},
            referenceNumberConfig: {
                value: {
                    labels: {}
                }
            },
            personDetailsConfig: {
                value: {
                    labels: {}
                }
            },
            personContactsConfig: {
                value: {
                    labels: {}
                }
            },
            personLanguagesConfig: {
                value: {
                    labels: {}
                }
            },
            personEducationConfig: {
                value: {
                    labels: {}
                }
            },
            personAddressConfig: {
                value: {
                    labels: {},
                    noDataMessage: {},
                    url: {},
                }
            },
            outputDialogConfig: {
                value: {}
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'accordion-panel',
        'handlebars-base',
        'handlebars-helpers',
        'event-custom',
        'usp-person-Citizenship',
        'usp-person-PersonReferenceNumber',
        'usp-contact-Contact',
        'usp-person-PersonWithLanguages',
        'usp-contact-NewContact',
        'usp-contact-NewEmailContact',
        'usp-contact-NewWebContact',
        'usp-contact-NewSocialMediaContact',
        'usp-person-PersonEducation',
        'usp-contact-NewTelephoneContact',
        'secured-categories-person-component-Referencetype',
        'categories-person-component-Ethnicity',
        'categories-person-component-Title',
        'categories-person-component-ProfessionalTitle',
        'categories-person-component-PersonType',
        'categories-person-component-Gender',
        'categories-person-component-GenderIdentity',
        'categories-person-component-SexualOrientation',
        'categories-person-component-Religion',
        'categories-contact-component-ContactType',
        'categories-contact-component-ContactUsage',
        'categories-contact-component-SocialMediaType',
        'categories-contact-component-TelephoneType',
        'categories-contact-component-ContactUsage',
        'categories-address-component-CountryCode',
        'categories-person-component-ProficiencyInOfficialLanguage',
        'app-accordion-toggle-all',
        'escape',
        'paginated-results-table',
        'results-formatters',
        'results-templates',
        'results-table-keyboard-nav-plugin',
        'info-message',
        'app-utils-person',
        'app-results',
        'results-table',
        'results-formatters',
        'person-output-document-dialog-views',
        'output-document-dialog-views',
        'belongings-controller-accordion',
        'education-controller-accordion',
        'subjectaccessrequest-controller-accordion',
        'person-address-controller-accordion',
        'person-component-enumerations'
    ]
});