YUI.add('belongings-results', function(Y) {

    var L = Y.Lang,
        USPFormatters = Y.usp.ColumnFormatters,
        USPTemplates = Y.usp.ColumnTemplates;

    var formatDescription = function(o) {
        var record = o.record,
            description = USPFormatters.textFormat({
                value: record.get('description')
            }),
            notes = USPFormatters.textFormat({
                value: record.get('notes')
            });

        return ('<div class="pure-g-r"><div class="pure-u-1-2 l-box"><strong>Description:</strong> <span>' + (description || '') + '</span></div><div class="pure-u-1-2 l-box"><strong>Notes:</strong> <span>' + (notes || '') + '</span></div></div>');
    };

    var formatDestructionDate = function(o) {
        if (o.record.get('status') !== 'DESTROYED') {
            return null;
        }
        var now = new Date();
        now.setHours(0, 0, 0, 0);
        var destDate = new Date(o.value);
        destDate.setHours(0, 0, 0, 0);
        if (destDate > now.getTime()) {
            return null;
        }
        return USPFormatters.date(o);
    };

    var formatStatus = function(o) {
        var destructionDate = o.record.get('destructionDate');
        if (!!destructionDate && o.value === 'SCHEDULED_FOR_DESTRUCTION') {
            var scheduledDestruction = {
                value: destructionDate
            };
            return o.column.destructionLabel + ' ' + USPFormatters.date(scheduledDestruction);
        }
        return USPFormatters.enumType(o);
    };

    var isStored = function(status) {
        if (status === 'STORED' || status === 'SCHEDULED_FOR_DESTRUCTION') {
            return true;
        }
        return false;
    }

    Y.namespace('app.person.belongings').BelongingsResults = Y.Base.create('belongingsResults', Y.usp.app.Results, [], {
        //set the tablePanelType to be an Accordion Table Panel
        tablePanelType: Y.usp.AccordionTablePanel,

        getResultsListModel: function(config) {
            var subject = config.subject;

            return new Y.usp.belongings.PaginatedBelongingsWithStatusDetailsList({
                url: L.sub(config.url, {
                    subjectId: subject.subjectId
                })
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {},
                permissions = config.permissions;
            return ([{
                key: 'dateReceived',
                label: labels.dateReceived,
                sortable: true,
                width: '10%',
                formatter: USPFormatters.date,
                cellTemplate: USPTemplates.clickable(labels.viewBelongings, 'view', permissions.canView)
            }, {
                key: 'location',
                label: labels.location,
                sortable: true,
                width: '20%',
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.belongings.location.category.codedEntries
            }, {
                key: 'reference',
                label: labels.reference,
                sortable: true,
                width: '15%',
                cellTemplate: USPTemplates.clickable(labels.viewBelongings, 'view', permissions.canView)
            }, {
                key: 'type',
                label: labels.type,
                sortable: true,
                width: '20%',
                formatter: USPFormatters.codedEntry,
                codedEntries: Y.uspCategory.belongings.type.category.codedEntries
            }, {
                key: 'status',
                label: labels.status,
                sortable: true,
                width: '15%',
                formatter: formatStatus,
                enumTypes: Y.usp.belongings.enum.BelongingsStatus.values,
                destructionLabel: labels.destructionScheduled
            }, {
                key: 'destructionDate',
                label: labels.destructionDate,
                sortable: true,
                width: '10%',
                formatter: formatDestructionDate
            }, {
                label: labels.actions,
                className: 'pure-table-actions',
                width: '10%',
                formatter: USPFormatters.actions,
                items: [{
                    clazz: 'view',
                    title: labels.viewBelongings,
                    label: labels.viewBelongings,
                    visible: permissions.canView
                }, {
                    clazz: 'update',
                    title: labels.updateBelongings,
                    label: labels.updateBelongings,
                    visible: permissions.canUpdate,
                    enabled: function() {
                        //must be stored to update an item
                        return isStored(this.data.status);
                    }
                }, {
                    clazz: 'move',
                    title: labels.moveBelongings,
                    label: labels.moveBelongings,
                    visible: permissions.canMove,
                    enabled: function() {
                        //must be stored to move item
                        return isStored(this.data.status);
                    }
                }, {
                    clazz: 'return',
                    title: labels.returnBelongings,
                    label: labels.returnBelongings,
                    visible: permissions.canReturn,
                    enabled: function() {
                        //must be stored to return item
                        return isStored(this.data.status);
                    }
                }, {
                    clazz: 'destroy',
                    title: labels.destroyBelongings,
                    label: labels.destroyBelongings,
                    visible: permissions.canDestroy,
                    enabled: function() {
                        //must be stored to destroy item
                        return isStored(this.data.status);
                    }
                }, {
                    clazz: 'history',
                    title: labels.historyBelongings,
                    label: labels.historyBelongings,
                    visible: permissions.canViewHistory
                }]
            }]);
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                //override plugins to include SummaryRowPlugin
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
                        cfg: {
                            state: 'expanded',
                            summaryFormatter: formatDescription
                        }
                    }];
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-results',
        'results-formatters',
        'results-templates',
        'results-table-summary-row-plugin',
        'usp-belongings-BelongingsWithStatusDetails',
        'categories-belongings-component-Location',
        'categories-belongings-component-Type',
        'belongings-component-enumerations'
    ]
});