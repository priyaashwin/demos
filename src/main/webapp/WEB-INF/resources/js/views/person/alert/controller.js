YUI.add('person-alert-controller-view', function(Y) {
    'use-strict';
    
    Y.namespace('app.person.alert').PersonAlertControllerView = Y.Base.create('personAlertControllerView', Y.View, [], {
        initializer: function(config) {
            this.personAlertDialog = new Y.app.person.alert.PersonAlertDialog(config.dialogConfig).addTarget(this).render();
            
            this._personAlertEvtHandlers=[
                //global event via Y from toolbar and header bar
                Y.on('viewAlerts', this.showAlertsDialog, this),
                //after save - trigger refresh of header by firing global event
                this.after('*:save', function(){
                    Y.fire("contentContext:update",{});
                })
            ];
            
        },
        destructor:function(){
            this._personAlertEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            delete this._personAlertEvtHandlers;

            this.personAlertDialog.removeTarget(this);
            this.personAlertDialog.destroy();
            delete this.personAlertDialog;
            
        },
        showAlertsDialog:function(){
            var subject = this.get('subject'),
                permissions = this.get('permissions')||{};
            
            //If the user has permission then show the "manage" view, otherwise a readonly view
            if (permissions.canUpdateAlerts) {
            	var updateAlertsConfig = this.get('dialogConfig').views.manageAlerts.config||{};
                this.personAlertDialog.showView('manageAlerts', {
                    model:new Y.app.person.alert.PersonAlerts({
                        url:updateAlertsConfig.url,
                        labels: updateAlertsConfig.validationLabels
                    }),
                    otherData:{
                        subject:subject
                    }
                },
                {
                   modelLoad: true,
                   payload:{
                       subjectId:subject.subjectId
                   }
                },
                 function(){
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }else if(permissions.canViewAlerts){
            	var viewAlertsConfig = this.get('dialogConfig').views.viewAlerts.config||{};
                this.personAlertDialog.showView('viewAlerts',{
                    model:new Y.app.person.alert.PersonAlerts({
                        url:viewAlertsConfig.url
                    }),
                    otherData:{
                        subject:subject
                    }
                },{
                    modelLoad: true,
                    payload:{
                        subjectId:subject.subjectId
                    }
                 });
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'person-alert-dialog',
               'usp-person-PersonAlert'
               ]
});
