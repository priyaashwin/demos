YUI.add('person-address-results', function(Y) {

    //AOP to add additional schema values to PersonAddress
    Y.Do.after(function() {
        var schema = Y.Do.originalRetVal;
        schema.resultFields.push({
            key: 'startDate!calculatedDate',
            locator: 'startDate.calculatedDate'
        }, {
            key: 'endDate!calculatedDate',
            locator: 'endDate.calculatedDate'
        });
    
        return new Y.Do.AlterReturn(null, schema);
    }, Y.usp.person.PersonAddress, 'getSchema', Y.usp.person.PersonAddress);

    
  var L = Y.Lang,
    USPFormatters = Y.usp.ColumnFormatters,
    USPTemplates = Y.usp.ColumnTemplates,
    addressOrUnknownFormatter = function addressOrUnknownFormatter(o){
      if(o.record.get('unknownLocationType')){
        return o.column.codedEntries[o.record.get('unknownLocationType')].name;
      }
        return USPFormatters.addressLocationGoogleMapLink(o);
    }

  Y.namespace('app.person.address').AddressResults = Y.Base.create('addressResults', Y.usp.app.Results, [], {
    //set the tablePanelType to be an Accordion Table Panel
    tablePanelType: Y.usp.AccordionTablePanel,
    events: {
        '.DND-address-toggle': {
            click: '_handleDoNotDiscloseClick'
        }
    },
    initializer: function () {
      this.events = Y.merge(this.events, Y.app.person.address.AddressResults.superclass.events);
      //plug in the errors handling
      this.plug(Y.Plugin.usp.ModelErrorsPlugin);

      this._evtHandlers = [
        Y.on('addressDoNotDisclose:addressChanged', this._reload, this),
        Y.on('addressDoNotDisclose:error', this._handlDoNotDiscloseError, this)
      ];
    },
    destructor: function () {
      //remove ModelErrors plugin
      this.unplug(Y.Plugin.usp.ModelErrorsPlugin);
    },
    getResultsListModel: function(config) {
      var subject = config.subject;

      return new Y.usp.person.PaginatedPersonAddressList({
        url: L.sub(config.url, {
          subjectId: subject.subjectId
        })
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;

      return([{
        key:  'location!location',
        label: labels.address,
        width: '30%',
        allowHTML: true,
        codedEntries: Y.uspCategory.address.unknownLocation.category.codedEntries,
        formatter:   addressOrUnknownFormatter,
        labels: labels
      }, {
        key: 'doNotDisclose',
        label: labels.doNotDisclose,
        allowHTML: true,
        width: '10%',
        formatter: USPFormatters.iconDND,
        permissions: permissions
      }, {
          key: 'startDate!calculatedDate',
          label: labels.startDate,
          formatter: USPFormatters.fuzzyDate,
          maskField: 'startDate.fuzzyDateMask',
          sortable: true,
          width: '14%'
      }, {
          key: 'endDate!calculatedDate',
          label: labels.endDate,
          formatter: USPFormatters.fuzzyDate,
          maskField: 'endDate.fuzzyDateMask',
          sortable: true,
          width: '14%'
      }, {
          key: 'type',
          label: labels.type,
          formatter: USPFormatters.codedEntry,
          codedEntries: Y.uspCategory.address.type.category.codedEntries,
          sortable: true,
          width: '11%'
      }, {
          key: 'usage',
          label: labels.usage,
          formatter: USPFormatters.codedEntry,
          codedEntries: Y.uspCategory.address.usage.category.codedEntries,
          sortable: true,
          width: '11%'
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '10%',
        formatter: USPFormatters.actions,
        items: [{
          clazz: 'end',
          title: labels.endAddress,
          label: labels.endAddress,
          visible: function() {
              // hide end address option unless there is no existing end date
              // or end date is in future
              return (this.record.get('temporalStatus') !== 'INACTIVE_HISTORIC') && permissions.canEnd
              && !(Y.uspCategory.address.type.category.codedEntries[this.record.get('type')].notManuallyAssignable);              
          }
        }, {
          clazz: 'endAdd',
          title: labels.moveAddress,
          label: labels.moveAddress,
          visible: function() {
              // Display for Active address with type=HOME, usage==PERMANENT only
              return ((this.record.get('type') == "HOME") && (this.record.get('usage')=='PERMANENT'))
              &&(this.record.get('temporalStatus')=='ACTIVE')&&permissions.canEndAdd
              && !(Y.uspCategory.address.type.category.codedEntries[this.record.get('type')].notManuallyAssignable);
          }
        }, {
            clazz: 'moveAddressLocation',
            title: labels.moveAddressLocation,
            label: labels.moveAddressLocation,
            visible: function() {
                return ((this.record.get('type') == "HOME") && (this.record.get('usage')=='PERMANENT'))
                && permissions.canUpdateResidentLocation
                && this.record.get('personTypes').includes('FOSTER_CARER')
                && !(Y.uspCategory.address.type.category.codedEntries[this.record.get('type')].notManuallyAssignable);
            }
          }, {
          clazz: 'remove',
          title: labels.removeAddress,
          label: labels.removeAddress,
          visible: permissions.canRemove,
        }, {
        	clazz:'updateStartEnd',
        	title:labels.updateAddressDates,
        	label:labels.updateAddressDates,
        	visible: permissions.canUpdateAddressStartEnd
        }, {
        	clazz:'updateAddressReopen',
        	title:labels.updateAddressReopen,
        	label:labels.updateAddressReopen,
        	visible: function() {
                // hide reopen address option if there is no existing end date or no permission
                var endDate = this.record.get('endDate');
                var addressIsOpen = endDate == null || endDate.calculatedDate == null;
                return !addressIsOpen && permissions.canUpdateAddressReopen;
            }
        }]
      }]);
    },
    _handleDoNotDiscloseClick: function(e) {
        var urls = this.get('searchConfig').doNotDiscloseURLs;
        Y.app.address.ToggleDoNotDiscloseSwitch.handleDoNotDiscloseClick(e, urls);
    },
    _handlDoNotDiscloseError: function(e) {
        //fire error event so plugin can pick that
        this.fire('error', {error: e});
    },
    _reload: function() {
        //there are two event named 'person:addressChanged' to update address in accordion and header
        Y.fire('person:addressChanged');
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'results-formatters',
    'results-templates',
    'usp-person-PersonAddress',
    'categories-address-component-Type',
    'categories-address-component-Usage',
    'app-address-switch-do-not-disclose',
    'model-errors-plugin'
  ]
});