YUI.add('person-lock-handler', function(Y) {
    var A = Y.Array,
        L = Y.Lang;

    Y.namespace('app').PersonLockHandler = Y.Base.create('personLockHandler', Y.Base, [], {

        initializer: function(config) {
            this.personLockDialog = new Y.app.person.lock.PersonLockDialog(config.dialogConfig).addTarget(this).render();

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: config.permissions
            }).addTarget(this);

            // set up our event handlers
            this._evtHandlers = [
                this.on('*:addLock', this.showAddLockDialog, this),
                this.on('*:updateSecurityAccessAllowedRecord', this.showUpdateSecurityAccessAllowedRecord, this),
                this.on('*:updateSecurityAccessDeniedRecord', this.showUpdateSecurityAccessDeniedRecord, this),
                this.on('*:deleteSecurityAccessAllowedRecord', this.showDeleteSecurityAccessAllowedRecord, this),
                this.on('*:deleteSecurityAccessDeniedRecord', this.showDeleteSecurityAccessDeniedRecord, this),
                this.on('*:selectedLockType', this._showAddAccessRecordListDialog, this),
                this.on('*:saved', this._handleSave, this),
                this.on('*:lockOverride', this._recordLockOverride, this),
                this.on('*:lockRemoved', this._postRecordOverride, this),
                this.after('securityAccessRecordInfoChange', this.securityAccessRecordDataChange, this)
            ];
            this._loadSecurityAccessRecord();
        },
        destructor: function() {
            this.personLockDialog.removeTarget(this);
            this.personLockDialog.destroy();
            delete this.personLockDialog;

            this.toolbar.removeTarget(this);
            this.toolbar.destroy();
            delete this.toolbar;

            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                // null out
                this._evtHandlers = null;
            }
        },
        _postRecordOverride: function(){
            document.location.reload();
        },
        securityAccessRecordDataChange: function(){
            this._updateToolbar();
        },
        _loadSecurityAccessRecord: function(){

            var subject = this.get('subject'),
                urls = this.get('urls');
            
            //load the security allowed record
            var securityAccessAllowedRecordModel = new Y.usp.security.SecurityAccessRecord({
                url: L.sub(urls.accessAllowedForSubjectUrl, {
                    subjectId: subject.subjectId,
                    subjectType: (subject.subjectType || '').toUpperCase()
                })
            }).load(function(err, response) {
                if (err === null) {
                    //store the latest security record id
                    this.set('securityAccessRecordInfo', {id : securityAccessAllowedRecordModel.get('id'), 
                    type : 'WHITELIST', isEnabled : true});
                } else {
                    var securityAccessDeniedRecordModel = new Y.usp.security.SecurityAccessRecord({
                        url: L.sub(urls.accessDeniedForSubjectUrl, {
                            subjectId: subject.subjectId,
                            subjectType: (subject.subjectType || '').toUpperCase()
                        })
                    });
                    securityAccessDeniedRecordModel.load(function(err, response) {
                        if (err === null) {
                            //store the latest security record id
                            this.set('securityAccessRecordInfo', { id : securityAccessDeniedRecordModel.get('id'),
                                type : 'BLACKLIST', isEnabled : true });
                        } 
                    }.bind(this));
                }
            }.bind(this));

            this.reset('securityAccessRecordInfo');
        },
        _recordLockOverride: function(event){
            var securityInfo = this.get('securityAccessRecordInfo');
            this.personLockDialog.showView('recordLockOverride', {
                id : securityInfo.id,
                type : securityInfo.type,
                url : this.get('urls').recordLockOverrideUrl,
                message : this.get('dialogConfig').views.recordOverride.message
            });
        },
        _handleSave: function(e) {
            var permissions = this.get('permissions') || {},
                subject = this.get('subject') || {},
                urls = this.get('urls');

            //just turn off the addLock button until callback complete
            this.toolbar.disableButton('addLock');

            //check user has not been denied access by making call to person
            new Y.usp.person.SecuredPerson({
                url: L.sub(urls.securedPersonUrl, {
                    id: subject.subjectId
                })
            }).load(function(err, response) {
                if (response.status === 200) {
                    permissions.isSubjectAccessAllowed = true;
                    //perform an update of the toolbar with the new state
                    this._loadSecurityAccessRecord();
                } else {
                    permissions.isSubjectAccessAllowed = false;
                    //reload the page
                    document.location.reload();
                }
            }.bind(this));
        },
        _updateToolbar: function() {
            var permissions = this.get('permissions') || {}; 
            var toolbar = this.toolbar;

            var hideAddLockButton = function() {
                toolbar.disableButton('addLock');
                //hide with no transition
                toolbar.hideButton('addLock', true);
            };

            var hideLockOverrideButton = function(){
                toolbar.hideButton('lockOverride', true);
            };

            var showLockOverrideButton = function() {
                toolbar.enableButton('lockOverride');
                //show with no transition
                toolbar.showButton('lockOverride', true);
            };

            var showAddLockButton = function() {
                hideSplitButton();

                toolbar.enableButton('addLock');
                //show with no transition
                toolbar.showButton('addLock', true);
            };

            var hideSplitButton = function() {
                var splitButton = toolbar.get('toolbarNode').one('#personLockButtons');
                splitButton.unplug(Y.Plugin.usp.SplitButtonPlugin);
                splitButton.hide();
            };

            var showSplitButton = function(lockType) {
                var splitButton = toolbar.get('toolbarNode').one('#personLockButtons');
                var canEdit = false,
                    canDelete = false;
                switch (lockType) {
                    case 'ALLOWED_LIST':
                        canEdit = permissions.canUpdateSecurityAccessAllowedRecord === true;
                        canDelete = permissions.canDeleteSecurityAccessAllowedRecord === true;
                        break;
                    case 'DENIED_LIST':
                        canEdit = permissions.canUpdateSecurityAccessDeniedRecord === true;
                        canDelete = permissions.canDeleteSecurityAccessDeniedRecord === true;
                        break;

                }

                var hideButton = function(className) {
                    toolbar.updateButton('.' + className, {
                        action:className + 'None'
                    });
                    toolbar.hideButton(className + 'None');
                    toolbar.disableButton(className + 'None');
                };
                var showButton = function(className, action) {
                    toolbar.updateButton('.' + className, {
                        action:action
                    });
                    toolbar.enableButton(action);
                    toolbar.showButton(action);
                };

                if (canEdit) {
                    var action = lockType === 'ALLOWED_LIST' ? 'updateSecurityAccessAllowedRecord' : 'updateSecurityAccessDeniedRecord';
                    showButton('editLock', action);
                } else {
                    hideButton('editLock');
                }

                if (canDelete) {
                    if (lockType === 'ALLOWED_LIST') {
                        hideButton('removeLock');
                        //show delete button
                        showButton('deleteLock', 'deleteSecurityAccessAllowedRecord');
                    } else {
                        hideButton('deleteLock');
                        //show remove button
                        showButton('removeLock', 'deleteSecurityAccessDeniedRecord');
                    }
                } else {
                    hideButton('deleteLock');
                    hideButton('removeLock');
                }                
                splitButton.plug(Y.Plugin.usp.SplitButtonPlugin);
                hideAddLockButton();
                hideLockOverrideButton();
                splitButton.show();
                
            };

            if (permissions.isSubjectAccessAllowed && (permissions.canAddSecurityAccessAllowedRecord ||
                    permissions.canUpdateSecurityAccessAllowedRecord ||
                    permissions.canDeleteSecurityAccessAllowedRecord ||
                    permissions.canAddSecurityAccessDeniedRecord ||
                    permissions.canUpdateSecurityAccessDeniedRecord ||
                    permissions.canDeleteSecurityAccessDeniedRecord)) {

                if(this.get('securityAccessRecordInfo').type === 'BLACKLIST'){
                    //show the split button - this will hide the add lock button
                    showSplitButton('DENIED_LIST');
                } else if(this.get('securityAccessRecordInfo').type === 'WHITELIST'){
                    //show the split button - this will hide the add lock button
                    showSplitButton('ALLOWED_LIST');
                } else {
                    //show add lock button - this will hide the split button
                    showAddLockButton();
                    hideLockOverrideButton();
                }
            } else if(!permissions.isSubjectAccessAllowed && permissions.canLockOverride){
                showLockOverrideButton();
            } else {
                //default is to hide all buttons
                hideSplitButton();
                hideAddLockButton();
            }
        },
        showAddLockDialog: function() {
            var permissions = this.get('permissions' || {}),
                subject = this.get('subject') || {};

            if (permissions.canAddSecurityAccessAllowedRecord || permissions.canAddSecurityAccessDeniedRecord) {
                this._showAddPersonLockDialog(subject.subjectId);
            }
        },
        showUpdateSecurityAccessAllowedRecord: function() {
            this._showEditAccessRecordListDialog('ALLOWED_LIST');
        },
        showUpdateSecurityAccessDeniedRecord: function() {
            this._showEditAccessRecordListDialog('DENIED_LIST');
        },
        showDeleteSecurityAccessAllowedRecord: function() {
            this._showDeleteDialog('ALLOWED_LIST');
        },
        showDeleteSecurityAccessDeniedRecord: function() {
            this._showDeleteDialog('DENIED_LIST');
        },
        _showAddPersonLockDialog: function(personId) {
            var permissions = this.get('permissions');

            this.personLockDialog.showView('addPersonLock', {
                personId: personId,
                permissions: permissions
            });
        },
        _showAddAccessRecordListDialog: function(e) {
            var urls = this.get('urls') || {};

            var modelClass, url, view;

            if (e.selectedLockType === 'ALLOWED_LIST') {
                modelClass = Y.usp.security.NewSecurityAccessAllowedRecord;
                url = urls.accessAllowedUrl;
                view = 'addAccessAllowedList';
            } else { // DENIED_LIST
                modelClass = Y.usp.security.NewSecurityAccessDeniedRecord;
                url = urls.accessDeniedUrl;
                view = 'addAccessDeniedList';
            }

            this.personLockDialog.showView(view, {
                model: new modelClass({
                    url: url
                }),
                personId: e.personId
            }, function(view) {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        _showEditAccessRecordListDialog: function(lockType) {
            var urls = this.get('urls') || {};

            //the securityAccessRecordInfo.id attribute holds the last looked up access record id
            if (!this.get('securityAccessRecordInfo').isEnabled) {
                return;
            }

            var modelClass, view;

            if (lockType === 'ALLOWED_LIST') {
                modelClass = Y.app.person.lock.SecurityAccessAllowedRecord;
                view = 'editAccessAllowedList';
            } else { // DENIED_LIST
                modelClass = Y.app.person.lock.SecurityAccessDeniedRecord;
                view = 'editAccessDeniedList';
            }

            this.personLockDialog.showView(view, {
                model: new modelClass({
                    url: urls.accessRecordUrl
                })
            }, {
                modelLoad: true,
                payload: {
                    id: this.get('securityAccessRecordInfo').id
                }
            }, function(view) {
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        _showDeleteDialog: function(lockType) {
            var urls = this.get('urls') || {};

            //the securityAccessRecordInfo.id attribute holds the last looked up access record id
            if (!this.get('securityAccessRecordInfo').isEnabled) {
                return;
            }

            var modelClass, view;

            if (lockType === 'ALLOWED_LIST') {
                modelClass = Y.app.person.lock.SecurityAccessAllowedRecord;
                view = 'deleteAccessAllowedList';
            } else { // DENIED_LIST
                modelClass = Y.app.person.lock.SecurityAccessDeniedRecord;
                view = 'removeAccessDeniedList';
            }

            this.personLockDialog.showView(view, {
                model: new modelClass({
                    url: urls.accessRecordUrl
                })
            }, {
                modelLoad: true,
                payload: {
                    id: this.get('securityAccessRecordInfo').id
                }
            }, function(view) {
                // remove the cancel button
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);

                // get the yes and no buttons by name out of the footer and enable
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                this.getButton('noButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        }
    }, {
        ATTRS: {
            dialogConfig: {},
            permissions: {
                value: {
                    canAddSecurityAccessAllowedRecord: false
                }
            },
            subject: {

            },
            urls: {
                value: {
                    accessRecordUrl: '',
                    accessAllowedUrl: '',
                    accessDeniedUrl: ''
                }
            },
            securityAccessRecordInfo: {
                value: {
                    id : null,
                    type : null,
                    isEnabled: false
                }
            }
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'event-custom',
        'app-toolbar',
        'split-button-plugin',
        'person-lock-dialog',
        'usp-security-NewSecurityAccessAllowedRecord',
        'usp-security-NewSecurityAccessDeniedRecord',
        'usp-person-SecuredPerson'
    ]
});