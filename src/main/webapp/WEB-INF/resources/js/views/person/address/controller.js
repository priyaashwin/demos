YUI.add('person-address-controller-accordion', function(Y) {

    Y.namespace('app.person.address').AddressAccordion = Y.Base.create('addressAccordion', Y.View, [], {
        initializer: function(config) {
            var model = this.get('model');

            var accordionConfig = {
                tablePanelConfig:{
                    title:config.title,
                    closed:config.closed
                },
                searchConfig: Y.merge(config.searchConfig, {
                    //mix the subjectId from the model into the searchConfig
                    subject: {
                        subjectId: model.get('id')
                    }
                })
            };

            this.addressResults = new Y.app.person.address.AddressResults(accordionConfig).addTarget(this);
            
            this._addressEvtHandlers = [

              this.on('*:addAddress', this.showAddDialog, this),
              this.on('*:end', this.showEndDialog, this),
              this.on('*:endAdd', this.showEndAddDialog, this),
              this.on('*:moveAddressLocation', this.showUpdateAddressLocation, this),
              this.on('*:remove', this.showRemoveDialog, this),
              this.on('*:updateStartEnd', this.showUpdateStartEndDialog, this),
              this.on('*:updateAddressReopen', this.showUpdateReopenDialog, this),
              //When the address dialog is refactored - change to listen only on save event
              //this.on('*:saved', this.addressResults.reload, this.addressResults)
              //for now listen to global event
              Y.on('person:addressChanged', this.addressResults.reload, this.addressResults)
            ];
        },
        getAccordion:function(){
          return this.addressResults.getTablePanel();
        },
        render: function() {
            var permissions = this.get('permissions') || {},
                buttonConfig = this.get('addButtonConfig') || {},
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

                //append the results
                contentNode.append(this.addressResults.render().get('container'));

                //construct the toolbar with the accordion button holder as the container
                this.toolbar = new Y.usp.app.AppToolbar({
                    permissions: permissions,
                    //set the toolbar node to the Button Holder on the TablePanel inside the address results
                    toolbarNode: this.addressResults.getTablePanel().getButtonHolder()
                }).addTarget(this);

                if (permissions.canAddAddress) {
                    //add a button to the toolbar if user had add permission
                    this.toolbar.addButton({
                        icon: buttonConfig.icon,
                        className: 'pull-right pure-button-active',
                        action: 'addAddress',
                        title: buttonConfig.title,
                        ariaLabel: buttonConfig.ariaLabel,
                        label: buttonConfig.label
                    });
                }

            //set fragment into container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this._addressEvtHandlers.forEach(function(handler) {
                handler.detach();
            });
            
            this.addressResults.removeTarget(this);
            this.addressResults.destroy();
            delete this.addressResults;

            if (this.toolbar) {
                this.toolbar.removeTarget(this);
                this.toolbar.destroy();
                delete this.toolbar;
            }

        },
        showAddDialog:function(e){
            var personModel=this.get('model'),
                permissions = this.get('permissions');

            if(permissions.canAddAddress===true){
                Y.fire('person:showDialog', {
                    action: 'addPersonAddress',
                    targetPerson: {
                        id: personModel.get('id'),
                        name: personModel.get('name')
                    }
                });
            }
        },
        showEndDialog:function(e){
            if(e.permissions.canEnd===true){
                this.showAddressDialog(e.record, 'endPersonAddress');
            }
        },
        showEndAddDialog:function(e){
            if(e.permissions.canEndAdd===true){
                this.showAddressDialog(e.record, 'movePersonAddress');
            }
        },
        showUpdateAddressLocation:function(e){
        	this.showAddressDialog(e.record, 'updateAddressLocation');
        },
        showRemoveDialog:function(e){
            if(e.permissions.canRemove===true){
                this.showAddressDialog(e.record, 'removePersonAddress');
            }
        },
        showUpdateStartEndDialog:function(e){
        	this.showAddressDialog(e.record, 'updateStartEnd');
        },
        showUpdateReopenDialog:function(e){
        	this.showAddressDialog(e.record, 'updateAddressReopen');
        	
        },
        showAddressDialog:function(record, action){
            var personModel=this.get('model'),
                urls=this.get('urls'),
                locationConfig=this.get('locationConfig');
            
            Y.fire('person:showDialog', {
                action: action,
                personAddressId: record.get('id'),
                targetPersonId: personModel.get('id'),
                targetPersonName: personModel.get('name'),
                urls:urls,
                locationConfig:locationConfig
            });
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'accordion-panel',
        'person-address-results',
        'app-toolbar'
    ]
});