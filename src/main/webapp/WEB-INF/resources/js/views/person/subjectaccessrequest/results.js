YUI.add('subjectaccessrequest-results', function(Y) {

  var L = Y.Lang,
    USPFormatters = Y.usp.ColumnFormatters,
    USPTemplates = Y.usp.ColumnTemplates;

  Y.namespace('app.person.subjectaccessrequest').SubjectAccessRequestResults = Y.Base.create('subjectaccessrequestResults', Y.usp.app.Results, [], {
    //set the tablePanelType to be an Accordion Table Panel
    tablePanelType: Y.usp.AccordionTablePanel,

    getResultsListModel: function(config) {
      var subject = config.subject;

      return new Y.usp.subjectaccessrequest.PaginatedSubjectAccessRequestList({
        url: L.sub(config.url, {
          subjectId: subject.subjectId
        })
      });
    },
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
        permissions = config.permissions;
      return ([{
        key: 'createdStamp!auditDate',
        label: labels.startDate,
        sortable: true,
        width: '20%',
        formatter: USPFormatters.dateTime,
      }, {
        key: 'status',
        label: labels.status,
        sortable: true,
        width: '15%',
        formatter: USPFormatters.enumType,
        enumTypes: Y.usp.subjectaccessrequest.enum.SubjectAccessRequestStatus.values
      }, {
        label: labels.actions,
        className: 'pure-table-actions',
        width: '10%',
        formatter: USPFormatters.actions,
        items: [{
            clazz: 'download',
            title: labels.downloadSubjectAccessRequest,
            label: labels.downloadSubjectAccessRequest,
            visible: function(){
              return this.record.get('status')!=='FAILED' && permissions.canView;
            },
            enabled: function(){
              return this.record.get('status')==='COMPLETE';
            }
          },{
              clazz: 'fail',
              title: labels.failSubjectAccessRequest,
              label: labels.failSubjectAccessRequest,
              visible: function(){
                return this.record.get('status')!=='FAILED' && permissions.canView;
              },
              enabled: function(){
                return this.record.get('status')==='PENDING';
              }
            }]
      }]);
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'app-results',
    'results-formatters',
    'results-templates',
    'results-table-summary-row-plugin',
    'usp-subjectaccessrequest-SubjectAccessRequest',
    'subjectaccessrequest-component-enumerations'
  ]
});