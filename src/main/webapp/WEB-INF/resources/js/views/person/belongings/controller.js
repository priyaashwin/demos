YUI.add('belongings-controller-accordion', function(Y) {
    Y.namespace('app.person.belongings').BelongingsAccordion = Y.Base.create('belongingsAccordion', Y.View, [], {
        initializer: function(config) {
            var model = this.get('model');

            var accordionConfig = {
                tablePanelConfig:{
                    title:config.title,
                    closed:config.closed
                },
                searchConfig: Y.merge(config.searchConfig, {
                    //mix the subjectId from the model into the searchConfig
                    subject: {
                        subjectId: model.get('id')
                    }
                })
            };

            this.belongingsResults = new Y.app.person.belongings.BelongingsResults(accordionConfig).addTarget(this);

            this.belongingsDialog = new Y.app.person.belongings.BelongingsDialog(config.dialogConfig).addTarget(this).render();

            this._belongingsEvtHandlers = [
                              this.on('*:addBelongings', this.showAddDialog, this),
                              this.on('*:view', this.showViewDialog, this),
                              this.on('*:move', this.showMoveDialog, this),
                              this.on('*:destroy', this.showDestroyDialog, this),
                              this.on('*:return', this.showReturnDialog, this),
                              this.on('*:update', this.showUpdateDialog, this),
                              this.on('*:history', this.showHistoryDialog, this),
                              this.on('*:saved', this.belongingsResults.reload, this.belongingsResults)
                            ];
        },
        getAccordion:function(){
            return this.belongingsResults.getTablePanel();
        },
        render: function() {
            var permissions = this.get('permissions') || {},
                buttonConfig = this.get('addButtonConfig') || {},
                contentNode = Y.one(Y.config.doc.createDocumentFragment());

                //append the results
                contentNode.append(this.belongingsResults.render().get('container'));

                //construct the toolbar with the accordion button holder as the container
                this.toolbar = new Y.usp.app.AppToolbar({
                    permissions: permissions,
                    //set the toolbar node to the Button Holder on the TablePanel inside the belonging results
                    toolbarNode: this.belongingsResults.getTablePanel().getButtonHolder()
                }).addTarget(this);

                if (permissions.canAddBelongings) {
                    //add a button to the toolbar if user had add permission
                    this.toolbar.addButton({
                        icon: buttonConfig.icon,
                        className: 'pull-right pure-button-active',
                        action: 'addBelongings',
                        title: buttonConfig.title,
                        ariaLabel: buttonConfig.ariaLabel,
                        label: buttonConfig.label
                    });
                }

            //set fragment into container
            this.get('container').setHTML(contentNode);

            return this;
        },
        destructor: function() {
            this._belongingsEvtHandlers.forEach(function(handler) {
                handler.detach();
            });

            this.belongingsResults.removeTarget(this);
            this.belongingsResults.destroy();
            delete this.belongingsResults;

            if (this.toolbar) {
                this.toolbar.removeTarget(this);
                this.toolbar.destroy();
                delete this.toolbar;
            }

            this.belongingsDialog.removeTarget(this);
            this.belongingsDialog.destroy();
            delete this.belongingsDialog;
        },
        showAddDialog:function(e){
            var personModel=this.get('model'),
                permissions = this.get('permissions'),
                config = this.get('dialogConfig').views.addBelongings.config;

            if(permissions.canAddBelongings===true){
              this.belongingsDialog.showView('addBelongings', {
                model: new Y.app.person.belongings.NewBelongingsForm({
                    //set the subject id
                    ownerSubjectId:personModel.get('id'),
                    url: config.url
                }),
                otherData: {
                    //just extract what we need out of the person model for the subject
                    subject:{
                        name:personModel.get('name'),
                        personIdentifier:personModel.get('personIdentifier')
                    }
                }
              }, function(){
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
              });
            }
        },
        showViewDialog: function(e) {
            var record = e.record,
                personModel=this.get('model'),
                permissions = this.get('permissions'),
                config = this.get('dialogConfig').views.viewBelongings.config;

            if(permissions.canView===true){
              this.belongingsDialog.showView('viewBelongings', {
                model: new Y.usp.belongings.BelongingsWithStatusDetails({
                    id: record.get('id'),
                    url: config.url
                })
              },{
                  modelLoad: true
              });
            }
        },
        showUpdateDialog: function(e){
          var record = e.record,
          personModel=this.get('model'),
          permissions = this.get('permissions'),
          config = this.get('dialogConfig').views.updateBelongings.config;

      if(permissions.canUpdate===true){
        this.belongingsDialog.showView('updateBelongings', {
          model: new Y.app.person.belongings.UpdateBelongings({
              id: record.get('id'),
              url: config.url
          })
        },{
            modelLoad: true
        }, function(){
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
        });
      }
        },
        showHistoryDialog: function(e){
          var record = e.record,
          permissions = this.get('permissions'),
          config = this.get('dialogConfig').views.historyBelongings.config;

          if(permissions.canHistory===true){
            this.belongingsDialog.showView('historyBelongings', Y.merge(config,{
              id: record.get('id'),
              model: record
            }),{
                modelLoad: false
            }, function(){
              this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
          }
        },
        showMoveDialog: function(e) {
            var record = e.record,
                personModel=this.get('model'),
                permissions = this.get('permissions'),
                config = this.get('dialogConfig').views.moveBelongings.config;

            if(permissions.canMove===true){
              this.belongingsDialog.showView('moveBelongings', {
                model: new Y.app.person.belongings.UpdateMoveBelongings({
                    url: config.url,
                    id: record.get('id')
                })
              },{
                  modelLoad: true
              }, function(){
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
              });
            }
        },
        showReturnDialog: function(e) {
            var record = e.record,
                personModel=this.get('model'),
                permissions = this.get('permissions'),
                config = this.get('dialogConfig').views.returnBelongings.config;

            if(permissions.canReturn===true){
              this.belongingsDialog.showView('returnBelongings', {
                model: new Y.app.person.belongings.UpdateReturnBelongings({
                    url: config.url,
                    id: record.get('id'),
                    labels:config.validationLabels
                })
              },{
                  modelLoad: true
              }, function(){
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
              });
            }
        },
        showDestroyDialog: function(e) {
            var record = e.record,
                personModel=this.get('model'),
                permissions = this.get('permissions'),
                config = this.get('dialogConfig').views.destroyBelongings.config;

            if(permissions.canDestroy===true){
              this.belongingsDialog.showView('destroyBelongings', {
                model: new Y.app.person.belongings.UpdateDestroyBelongings({
                    url: config.url,
                    id: record.get('id')
                })
              },{
                  modelLoad: true
              }, function(){
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
              });
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'accordion-panel',
        'belongings-results',
        'app-toolbar',
        'belongings-dialog',
        'usp-belongings-BelongingsWithStatusDetails'
    ]
});
