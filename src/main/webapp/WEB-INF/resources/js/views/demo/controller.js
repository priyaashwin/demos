YUI.add('demo-controller-view', function (Y) {
    'use-strict';

    Y.namespace('app.demo').DemoControllerView = Y.Base.create('demoControllerView', Y.View, [], {
        initializer: function (config) {
            this.set('personAccessCSVUrl', config.personAccessAccordionConfig.searchConfig.urlCSV);

            this.demoPersonAccessAccordionView = new Y.app.demo.PersonAccessAccordionView(Y.merge(config.personAccessAccordionConfig, {
                searchConfig: Y.merge(config.personAccessAccordionConfig.searchConfig, {
                    permissions: config.personAccessAccordionConfig.permissions
                })
            })).addTarget(this);

            this._evtHandlers = [
                this.on('*:personAccessDownload', this.handlePersonAccessDownload, this),
                this.on('filter:apply', this._handleFilterChange, this)
            ];

        },
        destructor: function () {
            this.demoPersonAccessAccordionView.destroy();
            delete this.demoPersonAccessAccordionView;
        },
        render: function () {
            var container = this.get('container');
            container.append(this.demoPersonAccessAccordionView.render().get('container'));
        },
        _handleFilterChange: function (e) {
            //on filter change, we want to amend the relevant CSV URL so that we retrieve only filtered results
            var filterContext = e.target.name,
                filterAttributes = [],
                url = '';

            if (filterContext === 'personAccessResultsFilterView') {
                filterAttributes = ["personId", "userId", "accessDateFrom", "accessDateTo"];
                url = this._constructCSVUrl(e.target.get('model'), filterAttributes, this.get('personAccessAccordionConfig').searchConfig.urlCSV);
                this.set('personAccessCSVUrl', url);
            }
        },
        handlePersonAccessDownload: function (e) {
            this.handleDownload(this.get('personAccessCSVUrl'));
        },
        handleEventLogDownload: function (e) {
            this.handleDownload(this.get('eventLogCSVUrl'));
        },
        handleDownload: function (url) {
            window.location = url;
        },
        _constructCSVUrl: function (filterModel, filterAttributes, url) {
            filterAttributes.forEach(function (filterAttribute) {
                Y.Object.each(filterModel.getAttrs(), function (value, key) {
                    if (filterAttribute === key && value && (!Array.isArray(value) || Array.isArray(value) && value.length > 0 )) {
                        url = url + "&" + Y.QueryString.stringify(value, null, key);
                    }
                }, this);
            });

            return url;
        }

    }, {
        ATTRS: {
            personAccessCSVUrl: {
                value: ''
            },
            eventLogCSVUrl: {
                value: ''
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'promise',
        'demo-personaccess-accordion-views',
        'querystring-stringify'
    ]
});