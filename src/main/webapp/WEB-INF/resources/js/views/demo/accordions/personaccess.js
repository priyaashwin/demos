YUI.add('demo-personaccess-accordion-views', function (Y) {
    'use-strict';

    var USPFormatters = Y.usp.ColumnFormatters,
        APPFormatters = Y.app.ColumnFormatters;

    Y.namespace('app.demo').PersonAccessResults = Y.Base.create('personAccessResults', Y.usp.app.Results, [], {
        getResultsListModel: function (config) {
            return new Y.app.demo.PaginatedFilteredPersonAccessList({
                url: config.url,
                filterModel: config.filterModel
            });
        },

        getColumnConfiguration: function (config) {
            var labels = config.labels,
                permissions = config.permissions;

            return [{
                key: 'accessDate',
                label: labels.accessDate,
                formatter: USPFormatters.date,
                sortable: true,
                width: '33%'
            }, {
                key: 'accessedPerson!name',
                label: labels.accessedPersonName,
                sortable: false,
                width: '33%'
            }, {
                key: 'accessorPerson!name',
                label: labels.accessorName,
                sortable: false,
                width: '33%'
            }];
        },

        render: function () {
            var labels = this.get('searchConfig').labels || {},
                permissions = this.get('searchConfig').permissions || {};

            Y.app.demo.PersonAccessResults.superclass.render.call(this);

            this.toolbar = new Y.usp.app.AppToolbar({
                permissions: permissions,
                toolbarNode: this.getTablePanel().getButtonHolder()
            }).addTarget(this);

            this.toolbar.addButton({
                icon: 'fa fa-download',
                className: 'pull-right pure-button-active personAccessDownload',
                action: 'personAccessDownload',
                title: labels.exportTitle,
                ariaLabel: labels.exportAriaLabel,
                label: labels.exportLabel
            });

            this.toolbar.addButton({
                icon: 'fa fa-filter',
                className: 'pull-right pure-button-active filter',
                action: 'filter',
                title: labels.filterTitle,
                ariaLabel: labels.filterAriaLabel,
                label: labels.filterLabel
            });

            return this;
        }

    });

    Y.namespace('app.demo').PaginatedFilteredPersonAccessList = Y.Base.create("paginatedFilteredPersonAccessList", Y.usp.access.PaginatedPersonAccessWithAccessorDetailsList, [Y.usp.ResultsFilterURLMixin], {
        whitelist: ["accessDateFrom", "accessDateTo", "personId", "userId"],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            }
        }
    });

    Y.namespace('app.demo').PersonAccessFilteredResults = Y.Base.create('personAccessFilteredResults', Y.usp.app.FilteredResults, [], {
        filterType: Y.app.demo.PersonAccessFilter,
        resultsType: Y.app.demo.PersonAccessResults,
        initializer: function () {
            this._events = [
                this.get('results').on('*:filter', this._handleFilterButtonClick, this)
            ];
        },
        _handleFilterButtonClick: function (e) {
            var filterVisible = this.get('filter').get('showFilter');

            this.get('filter').set('showFilter', (filterVisible) ? false : true);
        }
    });

    Y.namespace('app.demo').PersonAccessAccordionView = Y.Base.create('personAccessAccordionView', Y.usp.AccordionPanel, [], {
        initializer: function (config) {
            this.personAccessResults = new Y.app.demo.PersonAccessFilteredResults(config).addTarget(this);
        },

        render: function () {
            Y.app.demo.PersonAccessAccordionView.superclass.render.call(this);

            var contentNode = Y.one(Y.config.doc.createDocumentFragment());

            contentNode.append(this.personAccessResults.render().get('container'));
            this.getAccordionBody().setHTML(contentNode);

            return this;
        },

        destructor: function () {
            this.personAccessResults.removeTarget(this);
            this.personAccessResults.destroy();
            delete this.personAccessResults;
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'app-results',
        'app-filtered-results',
        'results-formatters',
        'results-filter',
        'demo-filter',
        'app-results',
        'event-custom',
        'usp-access-PersonAccessWithAccessorDetails',
        'app-toolbar'
    ]
});