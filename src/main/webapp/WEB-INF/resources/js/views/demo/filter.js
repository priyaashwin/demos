YUI.add('demo-filter', function (Y) {
    var L = Y.Lang;

    Y.namespace('app.demo').BaseDemoFilterView = Y.Base.create('demoFilterView', Y.usp.app.ResultsFilter, [], {
        events: {
            '.filterReset': {
                click: '_handleResetAutoCompleteFields'
            }
        },

        initializer: function () {
            var autoCompletePlaceholder = this.get('labels').autoCompletePlaceholder,
                autoCompletePlaceholderSecurityUser = this.get('labels').autoCompletePlaceholderSecurityUser;

            this.dateFromCalendar = new Y.usp.CalendarPopup();
            this.dateToCalendar = new Y.usp.CalendarPopup();

            this.dateFromCalendar.addTarget(this);
            this.dateToCalendar.addTarget(this);

            this.clientAutoCompleteView = new Y.app.PersonAutoCompleteResultView({
                autocompleteURL: this.get('urls').personAutocompleteURL,
                inputName: 'clientSubjectSearch',
                labels: {
                    placeholder: autoCompletePlaceholder
                }
            });
            this.clientAutoCompleteView.addTarget(this);

            this.userAutoCompleteView = new Y.app.SecurityUserAutoCompleteResultView({
                autocompleteURL: this.get('urls').securityUserAutocompleteURL,
                inputName: 'userSubjectSearch',
                labels: {
                    placeholder: autoCompletePlaceholderSecurityUser
                }
            });
            this.userAutoCompleteView.addTarget(this);

            this._evtHandlers = [
                this.clientAutoCompleteView.after('selectedPersonChange', this._handleClientChange, this),
                this.userAutoCompleteView.after('selectedPersonChange', this._handleUserChange, this),
                this.get('model').on('filter:reset', this._handleResetAutoCompleteFields, this)
            ];

        },

        render: function () {
            var container = this.get('container');

            //call into superclass to render
            Y.app.demo.BaseDemoFilterView.superclass.render.call(this);

            //set the input node for the calendar
            this.dateFromCalendar.set('inputNode', container.one('.dateFrom'));
            this.dateToCalendar.set('inputNode', container.one('.dateTo'));

            this.dateFromCalendar.render();
            this.dateToCalendar.render();

            container.one('.clientSubjectSearchWrapper').append(this.clientAutoCompleteView.render().get('container'));
            container.one('.userSubjectSearchWrapper').append(this.userAutoCompleteView.render().get('container'));

            return this;
        },

        _handleClientChange: function (e) {
            var selectedPerson = e.target.get('selectedPerson');
            this.get('model').set('personId', (selectedPerson) ? selectedPerson.id : null, {src: 'filter'});
        },
        _handleUserChange: function (e) {
            var selectedPerson = e.target.get('selectedPerson');
            this.get('model').set('userId', (selectedPerson) ? selectedPerson.person.id : null, {src: 'filter'});
        },
        _handleResetAutoCompleteFields: function () {
            //reset attributes in model inherited from auto-complete inputs to ensure active filter view is reset correctly
            this.clientAutoCompleteView.set('selectedPerson', null);
            this.userAutoCompleteView.set('selectedPerson', null);
        },
        destructor: function () {

            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.dateFromCalendar.destroy();
            delete this.dateFromCalendar;

            this.dateToCalendar.destroy();
            delete this.dateToCalendar;

            if (this.clientAutoCompleteView) {
                this.clientAutoCompleteView.removeTarget(this);
                this.clientAutoCompleteView.destroy();
                delete this.clientAutoCompleteView;
            }

            if (this.userAutoCompleteView) {
                this.userAutoCompleteView.removeTarget(this);
                this.userAutoCompleteView.destroy();
                delete this.userAutoCompleteView;
            }

        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            model: {},
            filterConfig: {}
        }
    });

    Y.namespace('app.demo').BaseDemoFilterContext = Y.Base.create('baseDemoFilterContext', Y.usp.app.FilterContext, [], {
        events: {
            '.filter-message a.close': {
                click: 'hideMessage'
            }
        },
        hideMessage: function (e) {
            e.preventDefault();
            this.get('container').all('div.filter-message').hide('fadeOut');
        }
    });

    Y.namespace('app.demo').PersonAccessFilterModel = Y.Base.create('personAccessFilterModel', Y.usp.ResultsFilterModel, [], {
        customValidation: function (attrs) {
            //enforce our validation first - if we succeed this will call the parent validate
            var errors = {},
                dateFrom = attrs['accessDateFrom'],
                dateTo = attrs['accessDateTo'];

            if (L.isNumber(dateFrom) && L.isNumber(dateTo)) {
                if (dateFrom > dateTo) {
                    errors['accessDateTo'] = this.labels.validation.dateTo;
                }
            }

            return errors;
        }
    }, {
        ATTRS: {
            accessDateFrom: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateFrom,
                getter: Y.usp.ResultsFilterModel.getDateFrom
            },
            accessDateTo: {
                USPType: 'Date',
                value: null,
                setter: Y.usp.ResultsFilterModel.setDateTo,
                getter: Y.usp.ResultsFilterModel.getDateTo
            },
            personId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            },
            userId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: []
            }
        }
    });

    Y.namespace('app.demo').PersonAccessResultsFilterView = Y.Base.create('personAccessResultsFilterView', Y.app.demo.BaseDemoFilterView, [], {
        template: Y.Handlebars.templates['personAccessFilter'],
    });

    Y.namespace('app.demo').PersonAccessFilterContext = Y.Base.create('personAccessFilterContext', Y.app.demo.BaseDemoFilterContext, [], {
        template: Y.Handlebars.templates['personAccessActiveFilter'],
    });

    Y.namespace('app.demo').PersonAccessFilter = Y.Base.create('personAccessFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.demo.PersonAccessFilterModel,
        filterContextType: Y.app.demo.PersonAccessFilterContext,
        filterType: Y.app.demo.PersonAccessResultsFilterView
    });

}, '0.0.1', {
    requires: ['yui-base',
        'calendar-popup',
        'results-filter',
        'app-filter',
        'handlebars-helpers',
        'handlebars-demo-templates',
        'person-autocomplete-view',
        'security-user-autocomplete-view'
    ]
});