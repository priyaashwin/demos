YUI.add('organisation-relationship-controller-view', function(Y) {
  'use-strict';
  var L = Y.Lang;
  Y.namespace('app.relationship').OrganisationRelationshipControllerView = Y.Base.create('organisationRelationshipControllerView', Y.View, [], {
    initializer: function(config) {
      var personOrganisationConfig = config.personOrganisationConfig || {};
      
      var resultsConfig = {
        searchConfig: Y.merge(personOrganisationConfig.searchConfig, {
          //mix the subject into the search config so it can be used in URL generation
          subject: config.subject
        }),
        permissions: config.permissions
      };
      this.relResults = new Y.app.relationship.OrganisationPersonOrganisationRelationshipResults(resultsConfig).addTarget(this);
      this._relationshipEvtHandlers = [
        this.on('*:viewPerson', this.viewPerson, this)
      ];
    },
    destructor: function() {
      this._relationshipEvtHandlers.forEach(function(handler) {
        handler.detach();
      });
      delete this._relationshipEvtHandlers;
      this.relResults.removeTarget(this);
      this.relResults.destroy();
      delete this.relResults;
    },
    viewPerson: function(e) {
      var record = e.record;
      var person = record.get('personVO');
      window.location = L.sub(this.get('viewPersonURL'), {
        personId: person.id
      });
    },
    render: function() {
      var contentNode = Y.one(Y.config.doc.createDocumentFragment());
      contentNode.append(this.relResults.render().get('container'));
      this.get('container').append(contentNode);
      return this;
    }
  }, {
    ATTRS: {
      personOrganisationConfig: {
        value: {
          searchConfig: {}
        }
      },
      permissions: {
        value: {}
      },
      subject: {}
    }
  });
}, '0.0.1', {
  requires: ['yui-base', 'view', 'relationship-organisation-person-results']
});