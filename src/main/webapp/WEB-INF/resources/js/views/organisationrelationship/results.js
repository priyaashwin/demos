YUI.add('relationship-organisation-person-results', function (Y) {
  'use strict';
    
  
  var L = Y.Lang,
    USPFormatters = Y.usp.ColumnFormatters, 
    APPFormatters = Y.app.ColumnFormatters,
    RelationshipFormatters = Y.app.relationship.ColumnFormatters;
  
  Y.namespace('app.relationship').OrganisationPersonOrganisationRelationshipResults = Y.Base.create('organisationPersonOrganisationRelationshipResults', Y.usp.app.Results, [], {
    getResultsListModel: function(config) {
      var subject = config.subject;
      
      return new Y.usp.relationshipsrecording.PaginatedOrganisationPersonOrganisationRelationshipsResultList({
          url: L.sub(config.url, {
            id: subject.subjectId,
            subjectType: subject.subjectType
          })
      });
    },    
    getColumnConfiguration: function(config) {
      var labels = config.labels || {},
          permissions = config.permissions;
      return ([{
        key: 'personVO!personIdentifier',
        label: labels.id,
        width: '13%',
        clazz:'viewPerson',
        title:labels.viewPerson,
        formatter: APPFormatters.linkable
      }, {
          key: 'personVO!name',
          label: labels.name,
          sortable: true,
          allowHTML:true,
          width: '57%',
          clazz:'viewPerson',
          title:labels.viewPerson,
          formatter: APPFormatters.linkablePopOverWarnings,
          url:'#none'
      },  {
        key: 'startDate',
        label: labels.startDate,
        sortable: true,
        width: '11%',
        formatter: APPFormatters.estimatedDate, 
        estimatedKey: 'startDateEstimated'
      }, {
        key: 'closeDate',
        label: labels.closeDate,
        sortable: true,
        width: '11%',
        formatter: USPFormatters.date
      }, {
        key:'mobile-summary',
        label: 'Summary',
        className: 'mobile-summary pure-table-desktop-hidden',
        width: '0%',
        allowHTML:true,
        formatter:RelationshipFormatters.summaryPersonOrganisationFormat,
        cellTemplate:'<td {headers} rowspan="1" colspan="100%" data-title="Summary" class="{className}"><div>{content}</div></td>'
      }, {
          label: labels.actions,
          className: 'pure-table-actions',
          width: '8%',
          formatter: USPFormatters.actions,
          items: [
          ]
      }]);
    }
  },{
    ATTRS:{
      resultsListPlugins: {
        valueFn: function() {
            return [{
                fn: Y.Plugin.usp.ResultsTableMenuPlugin
            }, {
                fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
            }, {
              fn: Y.Plugin.usp.ResultsTableSummaryRowPlugin,
              cfg: {
                state: 'expanded',
                summaryFormatter:RelationshipFormatters.summaryPersonOrganisationFormat
              }
            }];
        }
      }
    }
  });
}, '0.0.1', {
    requires: ['yui-base',
               'event-custom',
               'app-results',
               'usp-relationship-PersonOrganisationRelationship',
               'usp-relationshipsrecording-OrganisationPersonOrganisationRelationshipsResult',
               'caserecording-results-formatters',
               'relationship-results-formatters',
               'results-formatters',
               'results-templates',
               'results-table-summary-row-plugin'
               ]
});
