Y.add('remove-controller-view', function (Y) {
    'use-strict';

    var L = Y.Lang;

    var handlerEvents = {
        adoptiveCarerApproval: 'removeDialog:prepareForIndividualAdoptiveCarerApprovalRemove',
        careLeaver: 'removeDialog:prepareForIndividualCareLeaverRemove',
        caseNotes: 'removeDialog:prepareForCaseNotesRemove',
        chronologies: 'removeDialog:prepareForChronologiesRemove',
        document: 'removeDialog:prepareForIndividualDocumentRemove',
        documents: 'removeDialog:prepareForDocumentsRemove',
        fosterCarerApproval: 'removeDialog:prepareForIndividualFosterCarerApprovalRemove',
        fosterCarerRegistration: 'removeDialog:prepareForIndividualFosterCarerRegistrationRemove',
        person: 'removeDialog:prepareForIndividualPersonRemove',
        personClassification: 'removeDialog:prepareForIndividualPersonClassificationRemove'
    }

    Y.namespace('app.remove').RemoveControllerView = Y.Base.create('removeControllerView', Y.View, [], {
        initializer: function (config) {
            this.removeDialog = new Y.app.remove.RemoveDialog(config.dialogConfig).addTarget(this).render();
            this._evtHandlers = [
                this.on('removeDialog:removeRecord', this.handleShowRemove, this),
                this.on('removeDialog:removeInProgress', this.handleRemoveInProgress, this),
                Y.on(handlerEvents[config.type], this.handleShowPrepareRemove, this),
                this.after('*:saved', this.handleSave, this)
            ];
            
        },
        destructor: function () {
            this._evtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._evtHandlers;

            this.removeDialog.removeTarget(this);
            this.removeDialog.destroy();
            delete this.removeDialog;
        },
        getRecordId: function (url) {
            return new Y.Promise(function (resolve, reject) {
                var model = new Y.usp.Model({
                    url: url
                }).load(function (err, res) {
                    (err) ? reject(err) : resolve(model);
                }.bind(this));
            });
        },
        handleShowPrepareRemove: function (e) {
            var viewConfig = this.get('dialogConfig').views.prepareRemove,
                urlConfig = viewConfig.config,
                prepareUrl = urlConfig.prepareUrl;

            switch (this.get('type')) {
                case 'caseNotes':
                case 'chronologies':
                    this.getRecordId(urlConfig.url).then(function (data) {
                        prepareUrl = L.sub(prepareUrl, {
                            id: data.toJSON().id
                        });
                        this.showPrepareRemoveDialog(prepareUrl);
                    }.bind(this));
                    break;
                case 'personClassification':
                case 'adoptiveCarerApproval':
                case 'fosterCarerApproval':
                case 'document':
                case 'fosterCarerRegistration':
                case 'careLeaver':
                    prepareUrl = L.sub(prepareUrl, { id: e.id });
                    this.showPrepareRemoveDialog(prepareUrl);
                    break;
                default:
                    this.showPrepareRemoveDialog(prepareUrl);
            }
        },
        showPrepareRemoveDialog: function (prepareUrl) {
            this.removeDialog.showView('prepareRemove', {
                model: new Y.usp.deletion.NewDeletionRequest({
                    url: prepareUrl
                }),
                otherData: {
                    subject: this.get('subject'),
                    type: this.get('type')
                }
            }, function (view) {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //get rid of the cancel button - we have a no button instead
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },
        handleShowRemove: function (e) {
            this.showRemoveDialog(e.deletionRequestId);
        },
        showRemoveDialog: function (deletionRequestId) {
            var urlConfig = this.get('dialogConfig').views.remove.config;

            this.removeDialog.showView('remove', {
                model: new Y.usp.deletion.DeletionRequest({
                    url: L.sub(urlConfig.deleteUrl, {
                        deletionRequestId: deletionRequestId
                    }),
                    id: deletionRequestId
                }),
                otherData: {
                    subject: this.get('subject')
                }
            }, function (view) {
                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                //get rid of the cancel button - we have a no button instead
                this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
            });
        },
        handleRemoveInProgress: function () {
            this.showRemoveInProgressMessageDialog();
        },
        showRemoveInProgressMessageDialog: function () {
            this.removeDialog.showView('removeInProgress', {});
        },
        handleSave: function () {
            //refresh the view after deletion
            Y.fire('deletion:removeSaved');
        }
    }, {
        ATTRS: {
            type: {
                value: ''
            }
        }
    });
}, '0.0.1', {
    reqiures: ['yui-base',
        'view',
        'remove-dialogs',
        'usp-deletion-DeletionRequest',
        'usp-deletion-NewDeletionRequest'
    ]
});