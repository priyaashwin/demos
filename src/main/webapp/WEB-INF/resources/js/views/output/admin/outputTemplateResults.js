
YUI.add('output-template-results', function(Y) {
  var L = Y.Lang,
  	A = Y.Array,
    O = Y.Object,
    Micro = Y.Template.Micro,
    STATUS_TEMPLATE=Micro.compile('<a href="#" class="tool-tip" data-title="<%=this.title%>" data-content="<%=this.content%>" data-usemarkup="<%=this.useMarkup%>" aria-label="<%=this.title%> output state"><i class="icon-state <%=this.icon%>"></i></a>'),
    OUTPUT_TEMPLATE_STATE_DRAFT='DRAFT',
    OUTPUT_TEMPLATE_STATE_PUBLISHED='PUBLISHED',
    OUTPUT_TEMPLATE_STATE_DELETED='ARCHIVED',
  	USPFormatters = Y.usp.ColumnFormatters;
  	
  	var statusFormatter=function(o){
      var val=o.value;
      
      var data={
    		  title:'Output Template State',
    		  useMarkup:false
      };
      
      switch(val){
          case 'DRAFT':
        	  data=Y.merge(data, {
        		  content:OUTPUT_TEMPLATE_STATE_DRAFT,
        		  icon:'icon-state-draft'
        	  });
          break;
          case 'PUBLISHED':
        	  data=Y.merge(data, {
            	  content:OUTPUT_TEMPLATE_STATE_PUBLISHED,
            	  icon:'icon-state-published',
        	  });        	  
          break;
          case 'ARCHIVED':
        	  data=Y.merge(data, {
        		  content:OUTPUT_TEMPLATE_STATE_DELETED,
            	  icon:'icon-state-deleted',
        	  });
          break;         
      }

      return STATUS_TEMPLATE(data);
  }; 
  
  Y.namespace('app.admin.output').PaginatedFilteredOutputTemplateSearchResultList = Y.Base.create("paginatedFilteredOutputTemplateSearchResultList", Y.usp.output.PaginatedOutputTemplateList, [Y.usp.ResultsFilterURLMixin], {
      whiteList: ["name", "category", "status"],
      staticData:{
          useSoundex:false,
          appendWildcard:true
      }
  }, {
      ATTRS: {
          filterModel: {
              writeOnce: 'initOnly'
          },
          context:{
        	  value:'NONE'
          }
      }
  });
  
  Y.namespace('app.admin.output').OutputTemplateResults = Y.Base.create('outputTemplateResults', Y.usp.app.Results, [], {
      getResultsListModel:function(config){
          return new Y.app.admin.output.PaginatedFilteredOutputTemplateSearchResultList({
              url: config.url,
              //set the context - e.g. CHRONOLOGY, CASENOTES, FORMS
              context:config.context,
              //plug in the filter model
              filterModel: config.filterModel
          }); 
      },
      getColumnConfiguration:function(config){  
          var labels=config.labels||{},
              permissions=config.permissions;
          
          // declare the columns
         return ([{
            key: 'outputName',
            label: labels.name,
            sortable: true,
            width: '25%'
          }, {
              key: 'category',
              label: labels.outputCategory,
              formatter: USPFormatters.codedEntry,
              codedEntries: Y.uspCategory.output.outputCategory.category.codedEntries,
              width: '20%'
          }, {
	          key: 'description',
	          label: labels.description,
	          width: '30%'
	        }, {
	          key: 'useForSubjectAccessRequest',
	          label: labels.useForSubjectAccessRequest,
	          formatter: USPFormatters.yesNo,
	          width: '10%'
	        }, {
            key: 'status',
            label: labels.status,
            allowHTML: true,
            sortable: true,
            width: '5%',
            formatter: statusFormatter
          }, {
            label: labels.actions,
            className: 'pure-table-actions',
            width: '10%',
            formatter: USPFormatters.actions,
            items: [
                  {
                      clazz: 'view',
                      title: labels.viewTemplateTitle,
                      label: labels.viewTemplate,
                      enabled: permissions.canView
                  },
                  {
                      clazz: 'update',
                      title: labels.updateTemplateTitle,
                      label: labels.updateTemplate,
                      enabled: permissions.canUpdate,
                      visible:function(){
                          return (this.record.get('status') !== 'ARCHIVED');
                      }
                  },  
                  {
                      clazz: 'export',
                      title: labels.exportTemplateTitle,
                      label: labels.exportTemplate,
                      enabled: function() {
                          return permissions.canView;
                      }
                  },
                  {
                      clazz: 'publish',
                      title: labels.publishTemplateTitle,
                      label: labels.publishTemplate,
                      enabled: function() {
                          return permissions.canPublish;
                      },
                      visible:function(){
                          return (this.record.get('status') === 'DRAFT');
                      }
                  },
                  {
                      clazz: 'archive',
                      title: labels.archiveTemplateTitle,
                      label: labels.archiveTemplate,
                      enabled: function() {
                          return permissions.canArchive;
                      },
                      visible:function(){
                          return (this.record.get('status') !== 'ARCHIVED');
                      }
                  },
                  {
                      clazz: 'remove',
                      title: labels.removeTemplateTitle,
                      label: labels.removeTemplate,
                      visible: function() {
                          return permissions.canRemove;
                      }
                  }]
          }]);
      },
      rowClick:function(target, record, permissions){
        //click handler for table row
          if (target.hasClass('view')) {
              if (permissions.canView === true) {
                  //fire event for view
                  this.fire('viewTemplate', {
                      outputTemplate: record
                  });
              }
          }else if(target.hasClass('update')) {
              if (permissions.canUpdate === true) {
                  //fire event for update
                  this.fire('updateTemplate', {
                      outputTemplate: record
                  });
              }
          }else if(target.hasClass('publish')) {
              if (permissions.canPublish === true) {
                  //fire event for publish
                  this.fire('publishTemplate', {
                      outputTemplate: record
                  });
              }
          }else if(target.hasClass('archive')) {
              if (permissions.canArchive === true) {
                  //fire event for archive
                  this.fire('archiveTemplate', {
                      outputTemplate: record
                  });
              }
          }else if(target.hasClass('remove')) {
              if (permissions.canRemove === true) {
                  //fire event for remove
                  this.fire('removeTemplate', {
                      outputTemplate: record
                  });
              }
          }else if(target.hasClass('export')) {
              if (permissions.canView === true) {
                  //fire event for export
                  this.fire('exportTemplate', {
                      outputTemplate: record
                  });
              }
          }
      }
  });
  
  Y.namespace('app.admin.output').OutputTemplateFilteredResults = Y.Base.create('outputTemplateFilteredResults', Y.usp.app.FilteredResults, [], {
      filterType:Y.app.admin.output.OutputTemplateFilter,
      resultsType:Y.app.admin.output.OutputTemplateResults,
    /**
     * Re-loads the results AND syncs the results filter data
     *
     * @method refresh
     * @public         
     */ 
	refresh:function(){
	    var results=this.get('results'),
	        filter=this.get('filter');
		if(results){
			results.reload();
		}
		if(filter){
			filter.get('resultsFilter').syncFilterData();
		}
	},
  }, {
    ATTRS: {
    	searchConfig:{
    	    value:{
            	permissions:{
                	canUpdate:false,
                	canView:false,
                	canPublish:false,
                	canRemove:false,
                	canArchive:false
            	}
    	    }
    	}
    }
  });
}, '0.0.1', {
  requires: ['yui-base', 
             'node',
             'view',  
             'results-templates', 
             'results-formatters',
             'template-micro',
             'output-template-results-filter',
             'usp-output-OutputTemplate',
             'categories-output-component-OutputCategory',
             'app-filtered-results',
             'app-filter',
             'app-results']
});