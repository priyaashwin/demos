YUI.add('child-looked-after-output-admin', function(Y) {
    var L = Y.Lang,
        O = Y.Object;
    Y.namespace('app.admin.output').ChildLookedAfterOutputAdminTab = Y.Base.create('childLookedAfterOutputAdminTab', Y.app.admin.output.BaseOutputAdminTab, [], {
        context: 'CHILD_LOOKED_AFTER',
        getOutputTemplateDialog: function(config) {
            var dialogConfig=config.dialogConfig||{};
            
            //create output template dialog
            return new Y.app.admin.output.OutputTemplateDialog(Y.mix(dialogConfig,{
                views: {
                    addTemplate: {
                        type: Y.app.admin.output.ChildLookedAfterOutputTemplateAddView
                    },
                    viewTemplate: {
                        type: Y.app.admin.output.ChildLookedAfterOutputTemplateViewView
                    },
                    updateTemplate: {
                        type: Y.app.admin.output.ChildLookedAfterOutputTemplateUpdateView
                    },
                    generateTemplate: {
                        type: Y.app.admin.output.ChildLookedAfterOutputTemplateGenerateView
                    }
                }
            }, true, undefined, 0, true));
        },
        render: function() {
            //render the dialog
            this.outputTemplateDialog.render();
            //call into superclass
            Y.app.admin.output.ChildLookedAfterOutputAdminTab.superclass.render.call(this);
            return this;
        },
        showAdd: function() {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('addTemplate', {
                model: new Y.app.admin.output.NewChildLookedAfterOutputTemplateForm({
                    url: dialogConfig.views.addTemplate.config.url
                }),
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        showView: function(outputTemplate) {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('viewTemplate', {
                model: new Y.usp.output.OutputTemplate({
                    id: outputTemplate.get('id'),
                    url: dialogConfig.views.viewTemplate.config.url
                })
            }, {
                modelLoad: true
            });
        },
        showUpdate: function(outputTemplate) {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('updateTemplate', {
                model: new Y.app.admin.output.ChildLookedAfterOutputTemplateForm({
                    id: outputTemplate.get('id'),
                    url: dialogConfig.views.updateTemplate.config.url
                })
            }, {
                modelLoad: true
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        }
    }, {
        ATTRS: {
            dialogConfig: {
                value: {}
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'base-output-admin-tab',
        'child-looked-after-output-template-dialog-views',
        'output-template-dialog-views',
        'usp-output-OutputTemplate'
    ]
});