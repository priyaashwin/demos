YUI.add('base-output-admin-tab', function(Y) {
    var L = Y.Lang,
        O = Y.Object,
        A = Y.Array;
    Y.namespace('app.admin.output').BaseOutputAdminTab = Y.Base.create('baseOutputAdminTab', Y.View, [], {
        initializer: function(config) {
            //create a new instance of the output template results
            this.results = this.getOutputTemplateResults(config);
            // Add event targets
            this.results.addTarget(this);
            
            //event handlers
            this._evtHandlers = [
                //subscribe to events that we want to trigger a reload of the results
                this.on('*:saved', function(e) {
                    //reload results - access the results property of our results view
                    this.results.refresh();
                }, this)
         ];
            this.outputTemplateDialog = this.getOutputTemplateDialog(config);
            if (this.outputTemplateDialog) {
                // Add event targets
                this.outputTemplateDialog.addTarget(this);
            }
        },
        render: function() {
            var container = this.get('container'),
                //create a new document fragment
                contentNode = Y.one(Y.config.doc.createDocumentFragment());
            //render results
            contentNode.append(this.results.render().get('container'));
            //finally set the content into the container
            container.setHTML(contentNode);
            return this;
        },
        /**
         * Gets the output template dialog - must be overriden in the the extending class
         */
        getOutputTemplateDialog: function() {
            return null;
        },
        getOutputTemplateResults: function(config) {            
            var searchConfig=Y.merge(config.searchConfig||{},{
                context: this.context
            });
            
            var filterConfig=Y.merge(config.filterConfig||{}, {
                context: this.context
            });
            //create a new instance of the output template results
            return new Y.app.admin.output.OutputTemplateFilteredResults(Y.merge(config, {
                searchConfig:searchConfig,
                filterConfig:filterConfig
            }));
        },
        showPublish: function(outputTemplate) {
            this.showStaticDialog(outputTemplate, 'publishTemplate');
        },
        showArchive: function(outputTemplate) {
            this.showStaticDialog(outputTemplate, 'archiveTemplate');
        },
        showRemove: function(outputTemplate) {
            this.showStaticDialog(outputTemplate, 'removeTemplate');
        },
        showStaticDialog: function(outputTemplate, view) {
            if (this.outputTemplateDialog) {
                this.outputTemplateDialog.showView(view, {
                    model: outputTemplate
                }, function() {
                    //Get the button by name out of the footer and enable it.
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                });
            }
        },    
        showExport: function(outputTemplate){
            this.showStaticDialog(outputTemplate, 'exportTemplate');
        },
        showGenerate: function() {
            this.showStaticDialog(undefined, 'generateTemplate');
        },
        destructor: function() {
            //unbind event handles
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }
            if (this.results) {
                this.results.destroy();
                delete this.results;
            }
            //destroy the dialog
            if (this.outputTemplateDialog) {
                this.outputTemplateDialog.destroy();
                delete this.outputTemplateDialog;
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base', 'view', 'output-template-results']
});