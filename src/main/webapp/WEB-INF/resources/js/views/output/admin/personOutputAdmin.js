YUI.add('person-output-admin', function(Y) {
    var L = Y.Lang,
        O = Y.Object;
    Y.namespace('app.admin.output').PersonOutputAdminTab = Y.Base.create('personOutputAdminTab', Y.app.admin.output.BaseOutputAdminTab, [], {
        context: 'PERSON',
        getOutputTemplateDialog: function(config) {
            var dialogConfig=config.dialogConfig||{};
            
            //create output template dialog
            return new Y.app.admin.output.OutputTemplateDialog(Y.mix(dialogConfig,{
                views: {
                    addTemplate: {
                        type: Y.app.admin.output.PersonOutputTemplateAddView
                    },
                    viewTemplate: {
                        type: Y.app.admin.output.PersonOutputTemplateViewView
                    },
                    updateTemplate: {
                        type: Y.app.admin.output.PersonOutputTemplateUpdateView
                    },
                    generateTemplate: {
                        type: Y.app.admin.output.PersonOutputTemplateGenerateView
                    }
                }
            }, true, undefined, 0, true));
        },
        render: function() {
            //render the dialog
            this.outputTemplateDialog.render();
            //call into superclass
            Y.app.admin.output.PersonOutputAdminTab.superclass.render.call(this);
            return this;
        },
        showAdd: function() {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('addTemplate', {
                model: new Y.app.admin.output.NewPersonOutputTemplateForm({
                    url: dialogConfig.views.addTemplate.config.url
                }),
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        showView: function(outputTemplate) {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('viewTemplate', {
                model: new Y.usp.output.OutputTemplate({
                    id: outputTemplate.get('id'),
                    url: dialogConfig.views.viewTemplate.config.url
                })
            }, {
                modelLoad: true
            });
        },
        showUpdate: function(outputTemplate) {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('updateTemplate', {
                model: new Y.app.admin.output.PersonOutputTemplateForm({
                    id: outputTemplate.get('id'),
                    url: dialogConfig.views.updateTemplate.config.url
                })
            }, {
                modelLoad: true
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        }
    }, {
        ATTRS: {
            dialogConfig: {
                value: {}
            }
        }
    });
}, '0.0.1', {
    requires: [
        'yui-base',
        'base-output-admin-tab',
        'person-output-template-dialog-views',
        'output-template-dialog-views',
        'usp-output-OutputTemplate'
    ]
});