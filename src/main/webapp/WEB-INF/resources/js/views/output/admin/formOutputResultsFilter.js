YUI.add('form-output-template-results-filter', function(Y) {
    var DB = Y.usp.DataBindingUtil;

    Y.namespace('app.admin.output').FormOutputTemplateFilterModel = Y.Base.create('formOutputTemplateFilterModel', Y.app.admin.output.OutputTemplateFilterModel, [], {}, {
        ATTRS: {
            formDefinitionId: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            }
        }
    });
    Y.namespace('app.admin.output').FormOutputTemplateFilterView = Y.Base.create('formOutputTemplateFilterView', Y.app.admin.output.OutputTemplateFilterView, [], {
        template: Y.Handlebars.templates['formOutputResultsFilter'],
        _getFormDefinitions: function() {
            var model = new Y.usp.form.FormDefinitionList({
                url: this.get('formDefinitionListURL')
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },
        syncFilterData: function() {
            var container = this.get('container');

            //build a list of form definition names - this is as a result of an API call to find only applicable definitions
            this._getFormDefinitions().then(function(types) {
                //lookup the select - it is possible the select may no longer be present
                var select = container.one('#outputTemplateResultsFilter_formDefinitionId');
                //so we guard with a conditional
                if (select) {
                    definitions = types.toJSON().map(function(type) {
                        return ({
                            name: type.name + ' (version:' + type.version + ')',
                            version: type.version,
                            definitionName: type.name,
                            id: type.id
                        });
                    }).sort(function(a, b) {
                        //name sort
                        var ret = a.definitionName.localeCompare(b.definitionName, 'en', {
                            'sensitivity': 'base'
                        });
                        if (ret === 0) {
                            //version sort
                            ret = (b.version - a.version);
                        }
                        return ret;
                    });


                    Y.FUtil.setSelectOptions(select, definitions, null, null, false, true);

                    //now we have our select box populated - we need to ensure that any default selection is made
                    DB.setElementValue(select._node, this.get('model').get('formDefinitionId'));
                }
            }.bind(this));
            
            //call the parent syncFilterData method
            Y.app.admin.output.FormOutputTemplateFilterView.superclass.syncFilterData.call(this);
        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            /**
             * @attribute formDefinitionListURL
             * @description URL to get possible form definition ids to populate filter
             * @default null
             * @type string
             */
            formDefinitionListURL: {
                value: null
            }
        }
    });
    
    Y.namespace('app.admin.output').FormOutputTemplateFilter = Y.Base.create('formOutputTemplateFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.admin.output.FormOutputTemplateFilterModel,
        filterType: Y.app.admin.output.FormOutputTemplateFilterView,
        filterContextTemplate: Y.Handlebars.templates["formOutputActiveFilter"]
    });

}, '0.0.1', {
    requires: ['yui-base',
        'app-filter',
        'results-filter',
        'output-template-results-filter',
        'handlebars-helpers',
        'handlebars-output-templates',
        'usp-form-FormDefinition',
        'promise',
        'data-binding'
    ]
});