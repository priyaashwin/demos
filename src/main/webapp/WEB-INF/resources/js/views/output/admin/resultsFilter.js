YUI.add('output-template-results-filter', function(Y) {
    var DB = Y.usp.DataBindingUtil,
        L = Y.Lang;
    
    Y.namespace('app.admin.output').OutputTemplateFilterModel = Y.Base.create('outputTemplateFilterModel', Y.usp.ResultsFilterModel, [], {}, {
        ATTRS: {
            status: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                //default status list defined here
                value: ['DRAFT', 'PUBLISHED']
            },
            name: {
                USPType: 'String',
                value: ''
            },
            category: {
                USPType: 'String',
                setter: Y.usp.ResultsFilterModel.setListValue,
                getter: Y.usp.ResultsFilterModel.getListValue,
                value: null
            }
        }
    });
    
    Y.namespace('app.admin.output').OutputTemplateFilterView = Y.Base.create('OutputTemplateFilterView', Y.usp.app.ResultsFilter, [], {
        template: Y.Handlebars.templates['outputResultsFilter'],
        render:function(){
            //call into superclass to render
            Y.app.admin.output.OutputTemplateFilterView.superclass.render.call(this);

            //sync filter data
            this.syncFilterData();

            return this;
        },
        _getOutputCategories: function() {
            var model = new Y.usp.configuration.CodedEntryList({
                url: L.sub(this.get('outputCategoryUrl'), {context:this.get('context')})
            });
            return new Y.Promise(function(resolve, reject) {
                var data = model.load(function(err) {
                    if (err !== null) {
                        //failed for some reason so reject the promise
                        reject(err);
                    } else {
                        //success, so resolve the promise
                        resolve(data);
                    }
                });
            });
        },
        syncFilterData: function() {
            var container = this.get('container');

            //build a list of categories which are assigned
            this._getOutputCategories().then(function(categories) {
                var select = container.one('select[name="category"]');
                if (select) {
                    categories = categories.toJSON().sort(function(a, b) {
                        //name sort
                        return a.name.localeCompare(b.name, 'en', {
                            'sensitivity': 'base'
                        });
                    });
                    
                    Y.FUtil.setSelectOptions(select, categories, null, null, true, true, 'code');

                    //ensure any default selection is made
                    DB.setElementValue(select._node, this.get('model').get('category'));
                }
            }.bind(this));
        }
    }, {
        // Specify attributes and static properties for your View here.
        ATTRS: {
            otherData: {
                value: {
                    statusList: [{
                        code: "DRAFT",
                        name: "Draft"
                    }, {
                        code: "PUBLISHED",
                        name: "Published"
                    }, {
                        code: "ARCHIVED",
                        name: "Deleted"
                    }]
                }
            },
            context:{
                
            },
            outputCategoryUrl:{
                value:''
            }
        }
    });

    Y.namespace('app.admin.output').OutputTemplateFilter = Y.Base.create('outputTemplateFilter', Y.usp.app.Filter, [], {
        filterModelType: Y.app.admin.output.OutputTemplateFilterModel,
        filterType: Y.app.admin.output.OutputTemplateFilterView,
        filterContextTemplate: Y.Handlebars.templates["outputActiveFilter"]
    });
}, '0.0.1', {
    requires: ['yui-base',
        'app-filter',
        'results-filter',
        'handlebars-helpers',
        'handlebars-output-templates',
        'usp-configuration-CodedEntry',
        'promise',
        'data-binding'
    ]
});