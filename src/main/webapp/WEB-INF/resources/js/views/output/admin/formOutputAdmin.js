YUI.add('form-output-admin', function(Y) {
    var L = Y.Lang,
        O = Y.Object,
        E = Y.Escape;
    var formDefinitionTruncateFormatter = function(o) {
        var definitions = formDefinitionFormatter(o),
            ret = ['<ul class="no-style">'];
        if (definitions.length > 0) {
            ret.push('<li>' + E.html(definitions[0]) + '</li>');
        }
        if (definitions.length > 1) {
            ret.push('<li>... ' + (definitions.length - 1) + ' more ...</li>');
        }
        ret.push('</ul>');
        return ret.join('');
    };
    var formDefinitionFullFormatter = function(o) {
        var definitions = formDefinitionFormatter(o);
        return ('<ul class="no-style">' + definitions.map(function(value) {
            return ('<li>' + E.html(value) + '</li>');
        }).join('') + '</ul>');
    };
    var formDefinitionFormatter = function(o) {
        var val = o.value;
        var definitions = [];
        if (L.isArray(val)) {
            definitions = val.sort(function(a, b) {
                //version sort
                return (b.version || 0) - (a.version || 0);
            }).map(function(definition) {
                return definition.name + ' (version: ' + definition.version + ')';
            });
        }
        return definitions;
    };
    Y.namespace('app.admin.output').PaginatedFilteredFormOutputTemplateSearchResultList = Y.Base.create("PaginatedFilteredFormOutputTemplateSearchResultList", Y.usp.form.PaginatedFormOutputTemplateList, [Y.usp.ResultsFilterURLMixin], {
        whiteList: ["name", "status", "category", "formDefinitionId"],
        staticData: {
            useSoundex: false,
            appendWildcard: true
        }
    }, {
        ATTRS: {
            filterModel: {
                writeOnce: 'initOnly'
            },
            context: {
                value: 'NONE'
            }
        }
    });

    Y.namespace('app.admin.output').FormOutputTemplateResults = Y.Base.create('formOutputTemplateResults', Y.app.admin.output.OutputTemplateResults, [], {
        getResultsListModel: function(config) {
            return new Y.app.admin.output.PaginatedFilteredFormOutputTemplateSearchResultList({
                url: config.url,
                //set the context - e.g. CHRONOLOGY, CASENOTES, FORMS
                context: config.context,
                //plug in the filter model
                filterModel: config.filterModel
            });
        },
        getColumnConfiguration: function(config) {
            var labels = config.labels || {};
            //call superclass
            var columns = Y.app.admin.output.FormOutputTemplateResults.superclass.getColumnConfiguration.call(this, config);
            //now adjust the columns
            columns.splice(2, 0, {
                key: 'formDefinitions',
                label: labels.definitionName,
                width: '20%',
                allowHTML: true,
                expandedFormatter: formDefinitionFullFormatter,
                collapsedFormatter: formDefinitionTruncateFormatter
            });
            //update widths
            columns[0].width = '20%';
            columns[1].width = '20%';
            return columns;
        }
    }, {
        ATTRS: {
            resultsListPlugins: {
                writeOnce: 'initOnly',
                valueFn: function() {
                    return [{
                        fn: Y.Plugin.usp.ResultsTableExpandableCellsPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableMenuPlugin
                    }, {
                        fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin
                    }];
                }
            },
        }
    });

    /**
     * Extend the standard output results to include additional columns required for the form outputs
     */
    Y.namespace('app.admin.output').FormOutputTemplateFilteredResults = Y.Base.create('formOutputTemplateFilteredResults', Y.app.admin.output.OutputTemplateFilteredResults, [], {
        filterType: Y.app.admin.output.FormOutputTemplateFilter,
        resultsType: Y.app.admin.output.FormOutputTemplateResults
    });

    Y.namespace('app.admin.output').FormOutputAdminTab = Y.Base.create('formOutputAdminTab', Y.app.admin.output.BaseOutputAdminTab, [], {
        context: 'FORMS',
        getOutputTemplateDialog: function(config) {
            var dialogConfig=config.dialogConfig||{};
            
            //create output template dialog
            return new Y.app.admin.output.OutputTemplateDialog(Y.mix(dialogConfig,{
                views: {
                    addTemplate: {
                        type: Y.app.admin.output.FormOutputTemplateAddView
                    },
                    viewTemplate: {
                        type: Y.app.admin.output.FormOutputTemplateViewView
                    },
                    updateTemplate: {
                        type: Y.app.admin.output.FormOutputTemplateUpdateView
                    },
                    generateTemplate: {
                        type: Y.app.admin.output.FormOutputTemplateGenerateView
                    }
                }
            }, true, undefined, 0, true));
        },
        /**
         * Override the output template results
         */
        getOutputTemplateResults: function(config) {
            var searchConfig = Y.merge(config.searchConfig || {}, {
                context: this.context
            });

            var filterConfig=Y.merge(config.filterConfig||{}, {
                context: this.context
            });
            
            //create a new instance of the output template results
            return new Y.app.admin.output.FormOutputTemplateFilteredResults(Y.merge(config, {
                searchConfig: searchConfig,
                filterConfig:filterConfig
            }));
        },
        render: function() {
            //render the dialog
            this.outputTemplateDialog.render();
            //call into superclass
            Y.app.admin.output.FormOutputAdminTab.superclass.render.call(this);
            return this;
        },
        showAdd: function() {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('addTemplate', {
                model: new Y.app.admin.output.NewFormOutputTemplateForm({
                    url: dialogConfig.views.addTemplate.config.url
                })
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        },
        showView: function(outputTemplate) {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('viewTemplate', {
                model: new Y.usp.form.FormOutputTemplate({
                    id: outputTemplate.get('id'),
                    url: dialogConfig.views.viewTemplate.config.url
                })
            }, {
                modelLoad: true
            });
        },
        showUpdate: function(outputTemplate) {
            var dialogConfig = this.get('dialogConfig') || {};

            this.outputTemplateDialog.showView('updateTemplate', {

                model: new Y.app.admin.output.FormOutputTemplateForm({
                    id: outputTemplate.get('id'),
                    url: dialogConfig.views.updateTemplate.config.url
                })
            }, {
                modelLoad: true
            }, function() {
                //Get the button by name out of the footer and enable it.
                this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
            });
        }
    }, {
        ATTRS: {}
    });
}, '0.0.1', {
    requires: ['yui-base',
        'escape',
        'base-output-admin-tab',
        'usp-form-FormOutputTemplate',
        'output-template-results',
        'results-filter',
        'form-output-template-dialog-views',
        'form-output-template-results-filter',
        'usp-form-FormOutputTemplate',
        'results-table-expandable-cells-plugin'
    ]
});