YUI.add('output-tabs', function(Y) {
    var L = Y.Lang,
        O = Y.Object,
        A = Y.Array;
    Y.namespace('app.admin.output').OutputTabsView = Y.Base.create('outputTabsView', Y.app.tab.FilteredTabsView, [], {
        views: {
            /** Add new types to the output admin area here */
            person:{
                type: Y.app.admin.output.PersonOutputAdminTab
            },
            personLetter:{
                type: Y.app.admin.output.PersonLetterOutputAdminTab
            },
            casenote: {
                type: Y.app.admin.output.CaseNoteOutputAdminTab
            },
            chronology: {
                type: Y.app.admin.output.ChronologyOutputAdminTab
            },
            form: {
                type: Y.app.admin.output.FormOutputAdminTab
            },
            childLookedAfter: {
                type: Y.app.admin.output.ChildLookedAfterOutputAdminTab
            }
        },
        tabs: {
            person: {},
            personLetter: {},
            casenote: {},
            chronology: {},
            form: {},
            childLookedAfter: {}
        },        
        initializer: function(config) {
            //add an event handler to the filter node
            this._tabEvtHandlers = [
                                 //this.get('toolbarNode').delegate('click', this._handleButtonClick, '.pure-button', this),
                                 this.on('appToolbar:add', this.handleAddButton, this),
                                 this.on('appToolbar:generate', this.handleGenerateButton, this),
                                 this.on('*:viewTemplate', this._viewOutputTemplate, this),
                                 this.on('*:publishTemplate', this._publishOutputTemplate, this),
                                 this.on('*:archiveTemplate', this._archiveOutputTemplate, this),
                                 this.on('*:removeTemplate', this._removeOutputTemplate, this),
                                 this.on('*:updateTemplate', this._updateOutputTemplate, this),
                                 this.on('*:exportTemplate', this._exportOutputTemplate, this),
                                 this.after('*:activeViewChange', this._setToolbar, this),
                                 ];
        },
        destructor: function() {
            //unbind event handles
            if (this._tabEvtHandlers) {
                A.each(this._tabEvtHandlers, function(item) {
                    item.detach();
                });
                //null out
                this._tabEvtHandlers = null;
            }
        },
        /**
         * Handles the add event from the toolbar
         *
         * @method handleAddButton
         * @protected
         */
        handleAddButton: function(e) {
          //get the active view
          var activeView = this.get('activeView');
          if (activeView && activeView.showGenerate) {
            activeView.showAdd.call(activeView);
          }
        },
        /**
         * Handles the generate event from the toolbar
         *
         * @method handleGenerateButton
         * @protected
         */        
        handleGenerateButton: function(e) {
          //get the active view
          var activeView = this.get('activeView');
          if (activeView && activeView.showGenerate) {
            activeView.showGenerate.call(activeView);
          }
        },
        /**
         * Enables buttons when navigation is complete
         *
         * @method _setToolbar
         * @protected
         */
        _setToolbar: function() {
          var toolbar = this.get('toolbar');
  
          var permissions = this.get('permissions');
          
            //need to deal with the buttons individually here because the enablement is based on permissions
            if (permissions.canAdd) {
                //only re-enable the add button if the user has the permission to add
                this.enableButton('add');
                //only re-enable the generate button if the user has the permission to add
                this.enableButton('generate');
            }
        },
        /**
         * Handles view event from the results view
         *
         * @method _viewOutputTemplate
         * @protected
         */
        _viewOutputTemplate: function(e) {
            //get the active view
            var activeView = this.get('activeView');
            if (activeView) {
                //call into active view to show the dialog
                if (activeView.showView) {
                    activeView.showView.call(activeView, e.outputTemplate);
                }
            }
        },
        /**
         * Handles publish event from the results view
         *
         * @method _publishOutputTemplate
         * @protected
         */
        _publishOutputTemplate: function(e) {
            //get the active view
            var activeView = this.get('activeView');
            if (activeView) {
                //call into active view to show the dialog
                if (activeView.showPublish) {
                    activeView.showPublish.call(activeView, e.outputTemplate);
                }
            }
        },
        /**
         * Handles archive event from the results view
         *
         * @method _archiveOutputTemplate
         * @protected
         */
        _archiveOutputTemplate: function(e) {
            //get the active view
            var activeView = this.get('activeView');
            if (activeView) {
                //call into active view to show the dialog
                if (activeView.showArchive) {
                    activeView.showArchive.call(activeView, e.outputTemplate);
                }
            }
        },
        /**
         * Handles remove event from the results view
         *
         * @method _removeOutputTemplate
         * @protected
         */
        _removeOutputTemplate: function(e) {
            //get the active view
            var activeView = this.get('activeView');
            if (activeView) {
                //call into active view to show the dialog
                if (activeView.showRemove) {
                    activeView.showRemove.call(activeView, e.outputTemplate);
                }
            }
        },
        /**
         * Handles update event from the results view
         *
         * @method _updateOutputTemplate
         * @protected
         */
        _updateOutputTemplate: function(e) {
            //get the active view
            var activeView = this.get('activeView');
            if (activeView) {
                //call into active view to show the dialog
                if (activeView.showUpdate) {
                    activeView.showUpdate.call(activeView, e.outputTemplate);
                }
            }
        },
        /**
         * Handles export event from the results view
         *
         * @method _exportOutputTemplate
         * @protected
         */        
        _exportOutputTemplate: function(e){
            //get the active view
            var activeView = this.get('activeView');
            if (activeView) {
                //call into active view to show the dialog
                if (activeView.showExport) {
                    activeView.showExport.call(activeView, e.outputTemplate);
                }
            }
        }
    }, {
        ATTRS: {
            /**
             * @attribute permissions
             * @description An object representing the permissions to drive the
             * toolbar for the current user. Currently this is just the add permission.
             *
             * Permissions for the individual output template types are specified against
             * the individual tab view search config.
             * @default {
             *  	canAdd:false
             *	}
             * @type Object
             */
            permissions: {
                value: {
                    canAdd: false
                }
            }
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
               'app-toolbar',
               'filtered-tabs-view',
               'person-output-admin',
               'person-letter-output-admin',
               'casenote-output-admin',
               'chronology-output-admin',
               'form-output-admin',
               'child-looked-after-output-admin']
});