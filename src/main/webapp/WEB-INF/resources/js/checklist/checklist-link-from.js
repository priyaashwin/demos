/**
 * Processes session storage information to carry out 
 * View/Add of item linked to checklist
 * 
 */
YUI.add('checklist-link-from', function(Y) {
    var SS=Y.uspSessionStorage,
        parse=Y.JSON.parse;

        /**
         * Fired when a link from a checklist has been detected
         *
         * @event linkFrom
         * @param {String} action Indicator of the action required, `ADD`/`VIEW`/`PROMPT`
         * @param {String} targetEntityId the id of any target entity associated to the task
         * @param {Object} parameters configured parameters from the task 
         */
        LINK_FROM_EVENT = 'linkFrom';
    
    var ChecklistLinkFrom = Y.Base.create('checklistLinkFrom', Y.Base, [], {
        initializer:function(){           
            // publish events
            this.publish(LINK_FROM_EVENT, {
                emitFacade: true
            });
        },
        checkForLinkFromChecklist:function(){
            var type=this.get('type'),
                history,
                ch_si,
                payload={};

            
            if(type){
                //now we know we are capable of handling a specific type

                history = new Y.HistoryHash();
                
                if(history.get('ch_si')){
                    ch_si='checklist.'+history.get('ch_si');
                    
                    //now look in session storage for a checklist ID that matches
                    if(SS.getStoreValue(ch_si)){       
                        
                        //gather data into event payload
                        try{
                            //must try to JSON parse it
                            payload=parse(SS.getStoreValue(ch_si));
                        }catch(ignore){
                            //nope
                            payload=undefined;
                        }
                        
                        if(payload && payload.type===type){
                            this.fire(LINK_FROM_EVENT, {
                                checklistInstanceId:payload.checklistInstanceId,
                                targetEntityId: payload.targetEntityId,
                                action: (payload.action||'PROMPT').toUpperCase(),
                                taskState: payload.taskState,
                                parameters: payload.parameters
                            });
                        }
                        //clean up session storage
                        SS.removeStoreValue(ch_si);
                        
                        //remove the history so we don't attempt to trigger again
                        history.replaceValue('ch_si',undefined);
                    }
                }
            }
        }
    },{
        ATTRS:{
            /**
             * The type of object we can handle
             * e.g. 'FORM' or 'CASENOTE'
             *
             * @attribute type
             * @type {String}
             * @writeOnce
             * @default undefined
             */            
            type:{
                value:undefined,
                writeOnce: 'initOnly'
            }
        }
    });
    
    Y.namespace('app').ChecklistLinkFrom=ChecklistLinkFrom;
}, '0.0.1', {
    requires: ['yui-base', 
               'usp-session-storage',
               'history',
               'json-parse']
});