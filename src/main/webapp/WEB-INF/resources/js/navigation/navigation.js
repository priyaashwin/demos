/* global window */
YUI.add('usp-navigation', function(Y) {
    var TOKEN_REGEXP=RegExp("^{{.*}}$");
    var OPEN_TOKEN='{{';
    var CLOSE_TOKEN='}}';
    var setNodeURL = function(linkNode) {
        var currSubject = Y.currentSubject
            .getCurrentSubject();

        if (linkNode && !linkNode.getData('hrefTemplate')) {
            linkNode.setData('hrefTemplate', linkNode.get('href'));
        }
        var url = linkNode.getData('hrefTemplate') || linkNode.get('href');
        var newUrl = Y.Lang.sub(decodeURIComponent(url), {
            currentSubjectId: currSubject.id,
            currentSubjectType: currSubject.type
        });

        linkNode.set('href', newUrl);

    },
    getTokens = function(linkNode) {
      var tokens=[];
      if(linkNode){
        (linkNode.get('href').split("?")[1]||'').split("&").forEach(function(queryParameter){
          var params=queryParameter.split("=");
          var name=params[0];
          var value=params[1]||'';
          
          if(TOKEN_REGEXP.test(value)){
            tokens.push(value.replace(OPEN_TOKEN,'').replace(CLOSE_TOKEN,''));
          }
        });
      }
      return tokens;
    },
    isValidTokens=function(tokens, availableTokens){
      return (tokens||[]).every(function(token){
        return availableTokens.hasOwnProperty(token);
      });
    },
    substituteTokens=function(linkNode, tokens, availableTokens){
      var url=linkNode.get('href');
      //jump straight in and substitute
      tokens.forEach(function(token){
        url=url.replace(OPEN_TOKEN+token+CLOSE_TOKEN, availableTokens[token])  
      });
      
      linkNode.set('href', url);
    },    
    navigationReplace = {
        setUpNavigation: function(currSubject) {
            var toShow = [],
                toHide = [],
                _self = this;
            // todo: move this into the menu in the web resources facet so these
            // items start hidden, this will mean that the initial hide here is
            // unecessary
            var availableTokenData = Y.app.availableTokenData;

            Y.all('.main-menu li').each(function(node) {
                var userAllowedSubjectAccess = _self._isUserAllowedAccessToSubject(currSubject, node) === 'true';
                
                var showNode = _self._isNodeAccessable(node, currSubject, userAllowedSubjectAccess);

                if(node.hasClass('nav-customMenu')){
                  if(showNode){
                    //if the node is due to be shown - get the tokens used in the url
                    var tokens=getTokens(node.one('a'));
                    if(tokens.length>0){
                      if(!availableTokenData){
                        //if there is no token data - can't show the menu item
                        showNode=false;
                      }else{
                        //validate those tokens
                        showNode=isValidTokens(tokens, availableTokenData)
                        if(showNode){
                          //perform token substitution
                          substituteTokens(node.one('a'), tokens, availableTokenData);
                        }
                      }
                    }
                  }
                }

                if (showNode) {
                    toShow.push(node);
                }
                else {
                    toHide.push(node);
                }
                setNodeURL(node.one('a'));
            });
            Y.all(toShow).removeClass('hidden-nav');
            Y.all(toHide).addClass('hidden-nav');
        },
        /**
         * If the menu item node contains the class of
         * 'requires-subject-access', then the isSubjectAccessAllowed flag is
         * checked from the current subject. This method returns said flag or
         * 'true' if the node does not contain the class of
         * 'requires-subject-access'.
         * 
         * @method _isUserAllowedAccessToSubject
         * @param {Object}
         *            currSubject
         * @param {Node}
         *            node
         * @return 'true' or 'false'
         * @private
         */
        _isUserAllowedAccessToSubject: function(currSubject, node) {
            // if the menu requires the user to have access to the subject
            if (node.hasClass('requires-subject-access')) {
                return currSubject.isSubjectAccessAllowed;
            }
            return 'true';
        },
        /**
         * This method tests whether a class exists for a particular node and returns
         * true or false based on appropriate test conditions 
         * 
         * @method _isNodeAccessable
         * 
         * @param {Node}
         *            node
         * @param {Object}
         *            currSubject
         * @param {boolean}
         * 			  userAllowedSubjectAccess
         *
         * @return 'true' or 'false'
         * @private
         */
        _isNodeAccessable: function(node, currentSubject, userAllowedSubjectAccess){
        	var hasSubject = (currentSubject !== undefined && currentSubject.id !== null);
        	if(node.hasClass('depends-nothing')){
        		return true;
        	}
        	
        	if(hasSubject && currentSubject.type==='organisation'){
        		return node.hasClass('depends-organisation');
        	}
        	
        	return userAllowedSubjectAccess && ((hasSubject && node.hasClass('depends-subject')) || 
                    (currentSubject.type === 'person' && node.hasClass('depends-person')) || 
                    (currentSubject.type === 'group' && node.hasClass('depends-group')));
        	
         }
    };
    Y.namespace('app').navigationReplace = navigationReplace;
}, '0.0.1', {
    requires: ['yui-base','usp-current-subject']
});