YUI.add('checklist-definition-autocomplete-view', function(Y) {
  var A = Y.Array,
    Micro = Y.Template.Micro,
    ROW_TEMPLATE = Micro.compile('<div title="<%=this.name%>"><span><%=this.name%></span></div>'),
    SELECTED_ITEM_TEMPLATE = Micro.compile('<div class="text checklist-definition-selected-item">\
            <ul class="ac-single-selection">\
                <li class="data-item">\
                    <span><%=this.name%></span>\
                    <a href="#none" class="remove small change"><%=this.labels.change||"change"%></a>\
                </li>\
            </ul>\
        </div>');


  Y.namespace('app.checklist').ChecklistDefinitionAutoCompleteView = Y.Base.create('checklistDefinitionAutoCompleteView', Y.app.BaseAutoCompleteView, [], {
    getResultFormatter: function() {
      return Y.bind(this.resultFormatter, this);
    },
    resultFormatter: function(query, results) {
      return A.map(results, function(result) {
        return ROW_TEMPLATE(result.raw);
      }, this);
    }
  }, {
    ATTRS: {
      resultTextLocator: {
        valueFn: function() {
          return function(raw) {
            return raw.definitionId;
          };
        }
      },
      headers: {
        value: {
          'Accept': "application/vnd.olmgroup-usp.checklist.ChecklistDefinition+json"
        }
      }
    }
  });

  Y.namespace('app.checklist').ChecklistDefinitionAutoCompleteResultView = Y.Base.create('checklistDefinitionAutoCompleteResultView', Y.View, [], {
    events: {
      'a.change': {
        click: '_clearSelectedChecklistDefinition'
      }
    },
    initializer: function(config) {
      this.after('visibleChange', this._setVisibility, this);
      //create an instance of the checklist definition auto complete view
      this.autoCompleteView = new Y.app.checklist.ChecklistDefinitionAutoCompleteView(config);

      //add this as a bubble target
      this.autoCompleteView.addTarget(this);

      //handle selection from the autocomplete view
      this.on('*:select', function(e) {
        this.set('selectedChecklistDefinition', e.result.raw);
      });

      this._evtHandlers = [
        this.after('selectedChecklistDefinitionChange', this._renderSelectedChecklistDefinition, this),
        this.after('visibleChange', this._setVisibility, this)
      ];
    },
    render: function() {
      this.get('container').append(this.autoCompleteView.render().get('container'));

      this._renderSelectedChecklistDefinition();

      return this;
    },
    _clearSelectedChecklistDefinition: function() {
      this.set('selectedChecklistDefinition', null);
    },
    _renderSelectedChecklistDefinition: function() {
      var selectedChecklistDefinition = this.get('selectedChecklistDefinition'),
        container = this.get('container'),
        labels = this.get('labels'),
        displayNode;

      //remove the checklist definition display
      displayNode = container.one('.checklist-definition-selected-item');

      if (displayNode) {
        displayNode.remove(true);
      }

      if (selectedChecklistDefinition) {
        //hide the AC
        this.autoCompleteView.set('visible', false);

        var autocomplete = this.autoCompleteView.autocomplete;
        if (autocomplete) {
          autocomplete.get('inputNode').set('value', selectedChecklistDefinition.definitionId);
        }

        container.append(SELECTED_ITEM_TEMPLATE({
          name: selectedChecklistDefinition.name || '',
          labels: labels || {}
        }));
      } else {
        //show autocomplete
        this.autoCompleteView.set('visible', true);
      }
    },
    _setVisibility: function(e) {
      if (e.newVal === true) {
        this.autoCompleteView.set('visible', true);
      } else {
        this.autoCompleteView.set('visible', false);
      }
    },
    destructor: function() {
      if (this._evtHandlers) {
        A.each(this._evtHandlers, function(item) {
          item.detach();
        });
        //null out
        this._evtHandlers = null;
      }

      if (this.autoCompleteView) {
        //clean up autocomplete
        this.autoCompleteView.removeTarget(this);
        this.autoCompleteView.destroy();
      }
    }
  }, {
    ATTRS: {
      visible: {
        value: true
      },
      selectedChecklistDefinition: {},
      labels: {
        value: {
          value: {
            placeholder: 'Enter search criteria',
            noResults: 'No results found',
            change: 'change'
          }

        }
      }
    }
  });
}, '0.0.1', {
  requires: ['yui-base',
    'view',
    'template-micro',
    'base-autocomplete-view'
  ]
});