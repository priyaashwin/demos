YUI.add('security-profile-autocomplete-view', function(Y) {
    var A = Y.Array,
        Micro = Y.Template.Micro,
        ROW_TEMPLATE = Micro.compile('<div title="<%=this.name%>"><span><%=this.name%></span></div>'),
        SELECTED_ITEM_TEMPLATE = Micro.compile('<div class="text security-profile-selected-item">\
                                                   <ul class="ac-single-selection">\
                                                       <li class="data-item">\
                                                           <span><%=this.name%></span>\
                                                           <a href="#none" class="remove small change"><%=this.labels.change||"change"%></a>\
                                                       </li>\
                                                   </ul>\
                                               </div>');
    
    Y.namespace('app.admin.security.profile').SecurityProfileAutoCompleteView = Y.Base.create('securityProfileAutoCompleteView', Y.app.BaseAutoCompleteView, [], {
        getResultFormatter: function() {
            return Y.bind(this.resultFormatter,this);
        },
        resultFormatter: function(query, results) {
            return A.map(results, function(result) {
                return ROW_TEMPLATE(result.raw);
            }, this);
        }
    }, {
        ATTRS: {
            resultTextLocator:{
              valueFn:function(){
                  return function(raw){
                      return ''+raw.id
                  }
              }  
            },
            headers:{
                value: {
                    'Accept': "application/vnd.olmgroup-usp.securityextension.SecurityRecordTypeConfiguration+json"
                }
            }
        }
    });
    
    Y.namespace('app.admin.security.profile').SecurityProfileAutoCompleteResultView = Y.Base.create('securityProfileAutoCompleteResultView', Y.View, [], {
        events:{
            'a.change':{
                click:'_clearSelectedSecurityProfile'
            }
        },
        initializer: function(config) {
            //create an instance of the auto complete view
            this.autoCompleteView=new Y.app.admin.security.profile.SecurityProfileAutoCompleteView(config);
            
            //add this as a bubble target
            this.autoCompleteView.addTarget(this);
            
            //handle selection from the autocomplete view
            this.on('*:select', function(e) {
                this.set('selectedSecurityProfile', e.result.raw);
            });
            
            this.after('selectedSecurityProfileChange', this._renderSelectedSecurityProfile, this);
        },
        render: function() {
            this.get('container').append(this.autoCompleteView.render().get('container'));
            
            this._renderSelectedSecurityProfile();
            
            return this;
        },
        _clearSelectedSecurityProfile:function(){
          this.set('selectedSecurityProfile', null);
        },
        _renderSelectedSecurityProfile:function(){
            var selectedSecurityProfile=this.get('selectedSecurityProfile'),
                container=this.get('container'),
                labels=this.get('labels');
            
            //remove the security profile display
            displayNode=container.one('.security-profile-selected-item');
            
            if(displayNode){
                displayNode.remove(true);
            }
            
            if(selectedSecurityProfile){
                //hide the AC
                this.autoCompleteView.set('visible', false);
             
                var autocomplete=this.autoCompleteView.autocomplete;
                if(autocomplete){
                  autocomplete.get('inputNode').set('value', selectedSecurityProfile.id);
                }
                
                container.append(SELECTED_ITEM_TEMPLATE({
                    name:selectedSecurityProfile.name||'',
                    labels:labels||{}
                }));
            }else{
                //show autocomplete
                this.autoCompleteView.set('visible', true);
            }
        },
        destructor: function() {
            if(this.autoCompleteView){
                //clean up autocomplete
                this.autoCompleteView.removeTarget(this);
                this.autoCompleteView.destroy();
            }
        }
    },{
        ATTRS:{
          selectedSecurityProfile:{},
            labels:{
                value:{
                    value: {
                        placeholder: 'Enter search criteria',
                        noResults: 'No results found',
                        change:'change'
                    }
                    
                }
            }
        }
    });    
}, '0.0.1', {
    requires: ['yui-base',
               'view',
               'template-micro',
               'base-autocomplete-view'
              ]
});