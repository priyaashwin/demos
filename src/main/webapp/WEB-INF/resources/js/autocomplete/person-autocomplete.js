YUI.add('person-autocomplete-view', function (Y) {
    var A = Y.Array,
        Micro = Y.Template.Micro,
        //Note - the following fields in the template are considered safe as they should have been pre-escaped
        //genderAndDob, matchingField - if this changes - please update the template
        ROW_TEMPLATE_FULL = Micro.compile('<div class="<%=this.selectable?"":"unselectable"%>" title="<%=this.name%>">\
                                        <div <%=this.selectable?"":disabled="disabled"%><span><%=this.personIdentifier%> - <%=this.name%></span></div>\
                                        <div class="<%=this.selectable?"txt-color fl":"unselectable fl"%>"><span><%==this.genderAndDob.join(", ")%></span></div>\
                                        <%if(this.address){%>\
                                            <div class="<%=this.selectable?"txt-color":"unselectable"%>">\
                                                <strong>Address</strong> <span class="address"><%=this.address%></span>\
                                            </div>\
                                        <%}%>\
                                        <%==this.matchingField%>\
                                    </div>'),
        ROW_TEMPLATE_SIMPLE = Micro.compile('<div class="<%=this.selectable?"":"unselectable"%>" title="<%=this.name%>">\
                                            <div <%=this.selectable?"":disabled="disabled"%><span><%=this.personIdentifier%> - <%=this.name%></span></div>\
                                            <%==this.matchingField%>\
                                            </div>'),
        SELECTED_PERSON_TEMPLATE = Micro.compile('<div id="acPersonDisplay" class="text">\
                                                   <ul class="ac-single-selection">\
                                                       <li class="data-item">\
                                                           <span><%=this.personIdentifier%> - <%=this.personName%></span>\
                                                           <a href="#none" class="remove small change"><%=this.labels.change||"change"%></a>\
                                                       </li>\
                                                       <li class="data-item">\
                                                            <span class="address"><%=this.personAddress%></span>\
                                                       </li>\
                                                   </ul>\
                                               </div>');

    Y.namespace('app').PersonAutoCompleteView = Y.Base.create('personAutoCompleteView', Y.app.BaseAutoCompleteView, [], {
        initializer: function (config) {

            if (config.isPersonSelectable) {
                this.isPersonSelectable = config.isPersonSelectable;
            }

            this.on('*:select', function (e) {
                if (!this.isSelectable(e.result.raw)) {
                    //stop the event
                    e.preventDefault();
                    e.stopPropagation();
                }
            }, this);
        },
        getResultFormatter: function () {
            return Y.bind(this.resultFormatter, this);
        },
        isSelectable: function (person) {
            if (this.get('preventSecuredSelection') === true ? person.accessAllowed : true) {
                //hook to allow client implementation of selectable
                return this.isPersonSelectable(person);
            }

            return false;
        },
        isPersonSelectable: function () {
            return true;
        },
        resultFormatter: function (query, results) {
            var rowTemplateName = this.get('rowTemplate'),
                rowTemplate;
            switch (rowTemplateName) {
            case 'ROW_TEMPLATE_SIMPLE':
                rowTemplate = ROW_TEMPLATE_SIMPLE;
                break;
            default:
                rowTemplate = ROW_TEMPLATE_FULL;
            }
            return A.map(results, function (result) {
                var _rawAddress = result.raw.address || {},
                    location = _rawAddress.location || {},
                    locationDescription = location.location,

                    dobInfo = Y.app.ColumnFormatters.formatDateBasedOnLifeState(result.raw.lifeState, result.raw.dueDate, result.raw.dateOfBirth, result.raw.dateOfBirthEstimated, result.raw.age, true),
                    gender = Y.uspCategory.person.gender.category.codedEntries[result.raw.gender],
                    address = [],
                    genderAndDob = [];

                _rawAddress.roomDescription ? address.push('Room ' + _rawAddress.roomDescription) : '';
                _rawAddress.floorDescription ? address.push('Floor ' + _rawAddress.floorDescription) : '';
                locationDescription ? address.push(locationDescription) : '';
                address.join(', ');

                if (gender && (gender.name || '').length > 0) {
                    genderAndDob.push(gender.name);
                }

                if (dobInfo && dobInfo.length > 0) {
                    genderAndDob.push(dobInfo.join(' '));
                }

                return rowTemplate({
                    selectable: this.isSelectable(result.raw),
                    name: result.raw.name,
                    personIdentifier: result.raw.personIdentifier,
                    genderAndDob: genderAndDob,
                    address: address,
                    matchingField: Y.app.search.person.RenderMatchingField(result.raw, query)
                });
            }, this);
        }
    }, {
        ATTRS: {
            resultTextLocator: {
                valueFn: function () {
                    return function (raw) {
                        return '' + raw.id
                    }
                }
            },
            rowTemplate: {
                value: 'ROW_TEMPLATE_FULL'
            },
            'preventSecuredSelection': {
                value: true
            },
            headers: {
                value: {
                    'Accept': 'application/vnd.olmgroup-usp.person.PersonWithAddressAndSecurityAccessStatus+json'
                }
            }
        }
    });

    Y.namespace('app').PersonAutoCompleteResultView = Y.Base.create('personAutoCompleteResultView', Y.View, [], {
        events: {
            'a.change': {
                click: '_clearSelectedPerson'
            }
        },
        initializer: function (config) {
            //create an instance of the person auto complete view
            this.personAutoCompleteView = new Y.app.PersonAutoCompleteView(Y.mix(config, {
                isPersonSelectable: this.isPersonSelectable
            }));

            //add this as a bubble target
            this.personAutoCompleteView.addTarget(this);

            //handle selection from the autocomplete view
            this.on('*:select', function (e) {
                this.set('selectedPerson', e.result.raw);
            });

            this._evtHandlers = [
                this.after('selectedPersonChange', this._renderSelectedPerson, this),
                this.after('visibleChange', this._setVisibility, this),
                this.after('disabledChange', this._setDisabled, this)
            ]
        },
        isPersonSelectable: function () {
            return true;
        },
        render: function () {
            var selectedPerson = this.get('selectedPerson'),
                autocomplete;

            this.get('container').append(this.personAutoCompleteView.render().get('container'));

            if (selectedPerson) {
                autocomplete = this.personAutoCompleteView.autocomplete;
                if (autocomplete) {
                    //ensure a starting value is set for the inputNode if an initial selection has been made
                    autocomplete.get('inputNode').set('value', autocomplete.get('resultTextLocator').call(null, selectedPerson));
                }
            }

            this._renderSelectedPerson();

            return this;
        },
        _clearSelectedPerson: function () {
            this.set('selectedPerson', null);
        },
        _renderSelectedPerson: function () {
            var selectedPerson = this.get('selectedPerson'),
                container = this.get('container'),
                labels = this.get('labels');

            //remove the person display
            var displayNode = container.one('#acPersonDisplay');

            if (displayNode) {
                displayNode.remove(true);
            }

            if (selectedPerson) {
                //hide the AC
                this.personAutoCompleteView.set('visible', false);
                var address = '';
                if (typeof selectedPerson.address === 'object' && selectedPerson.address !== null) {
                    address = selectedPerson.address.location.location;
                }

                container.append(SELECTED_PERSON_TEMPLATE({
                    personIdentifier: selectedPerson.personIdentifier || '',
                    personName: selectedPerson.name || '',
                    personAddress: address || '',
                    labels: labels || {}
                }));
            } else {
                //show autocomplete
                this.personAutoCompleteView.set('visible', true);
            }
        },
        _setVisibility: function (e) {
            if (e.newVal === true) {
                this.personAutoCompleteView.set('visible', true);
            } else {
                this.personAutoCompleteView.set('visible', false);
            }
        },
        _setDisabled: function (e) {
            if (e.newVal === true) {
                this.personAutoCompleteView.set('disabled', true);
            } else {
                this.personAutoCompleteView.set('disabled', false);
            }
        },         
        destructor: function () {
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            if (this.personAutoCompleteView) {
                //clean up person autocomplete
                this.personAutoCompleteView.removeTarget(this);
                this.personAutoCompleteView.destroy();
            }
        }
    }, {
        ATTRS: {
            visible: {
                value: true
            },
            selectedPerson: {},
            labels: {
                value: {
                    value: {
                        placeholder: 'Enter search criteria',
                        noResults: 'No results found',
                        change: 'change'
                    }

                }
            },
            disabled:{}
        }
    });

    Y.namespace('app').ProfessionalPersonAutoCompleteResultView = Y.Base.create('professionalPersonAutoCompleteResultView', Y.app.PersonAutoCompleteResultView, [], {
        isPersonSelectable: function (person) {
            var personTypes = person.personTypes;
            var isClient = personTypes.includes('CLIENT');
            var isAdopter = personTypes.includes('ADOPTER');
            var isFosterCarer = personTypes.includes('FOSTER_CARER');
            var isProfessional = personTypes.includes('PROFESSIONAL');

            if (isClient && !isAdopter && !isFosterCarer && !isProfessional) {
                return false;
            }
            return true;
        }
    });

    Y.namespace('app').ProfessionalRolePersonAutoCompleteResultView = Y.Base.create('professionalRolePersonAutoCompleteResultView', Y.app.PersonAutoCompleteResultView, [], {
        isPersonSelectable: function (person) {
            var personTypes = person.personTypes;
            var isProfessional = personTypes.includes('PROFESSIONAL');
            return isProfessional;
        }
    });

    Y.namespace('app').ClientAutoCompleteResultView = Y.Base.create('clientAutoCompleteResultView', Y.app.PersonAutoCompleteResultView, [], {
        isPersonSelectable: function (person) {
            var personTypes = person.personTypes;
            var isClient = personTypes.includes('CLIENT');
            return isClient;
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'template-micro',
        'base-autocomplete-view',
        //TODO - see if we can break caserecording-results-formatters down so we don't need to pull in so many other dependencies
        //we only need the formatDateBasedOnLifeState function
        'caserecording-results-formatters',
        'categories-person-component-Gender',
        'person-name-matcher-highlighter'
    ]
});