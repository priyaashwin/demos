YUI.add('security-user-autocomplete-view', function (Y) {
	var A = Y.Array,
    Micro = Y.Template.Micro,
    
    ROW_TEMPLATE_FULL = Micro.compile('<div class="<%=this.selectable?"":"unselectable"%>" title="<%=this.securityUserName%>">\
                                    <div <%=this.selectable?"":disabled="disabled"%><span><%=this.securityUserIdentifier%> - <%=this.securityName%>   <%=this.personIdentifier%> - <%=this.personName%></span></div>\
                                    <div class="<%=this.selectable?"txt-color fl":"unselectable fl"%>"></div>\
                                    <%==this.matchingField%>\
                                </div>'),
    ROW_TEMPLATE_SIMPLE = Micro.compile('<div class="<%=this.selectable?"":"unselectable"%>" title="<%=this.securityUserName%>">\
                                        <div <%=this.selectable?"":disabled="disabled"%><span><%=this.securityUserIdentifier%> - <%=this.securityName%>   <%=this.personIdentifier%> - <%=this.personName%></span></div>\
                                        <%==this.matchingField%>\
                                        </div>'),
    SELECTED_PERSON_TEMPLATE = Micro.compile('<div id="acPersonDisplay" class="text">\
                                               <ul class="ac-single-selection">\
                                                   <li class="data-item">\
                                                       <span><%=this.userIdentifier%> - <%=this.userName%></span>\
                                                       <a href="#none" class="remove small change"><%=this.labels.change||"change"%></a>\
                                                   </li>\
                                                   <li class="data-item">\
    		                                            <span><%=this.personIdentifier%> - <%=this.personName%></span>\
                                                   </li>\
                                               </ul>\
                                           </div>');

    Y.namespace('app').SecurityUserAutoCompleteView = Y.Base.create('securityUserAutoCompleteView', Y.app.BaseAutoCompleteView, [], {
 
        getResultFormatter: function () {
            return Y.bind(this.resultFormatter, this);
        },
        
        resultFormatter: function (query, results) {
            var rowTemplateName = this.get('rowTemplate'),
                rowTemplate;
            switch (rowTemplateName) {
                case 'ROW_TEMPLATE_SIMPLE':
                    rowTemplate = ROW_TEMPLATE_SIMPLE;
                    break;
                default:
                rowTemplate = ROW_TEMPLATE_FULL;
            }
            return A.map(results, function (result) {
                return rowTemplate({
                    selectable: true,
                    securityName: result.raw.name,
                    securityUserName: result.raw.userName,
                    securityUserIdentifier: result.raw.userIdentifier,
                    personIdentifier: result.raw.person.personIdentifier,
                    personName: result.raw.person.name,
                    matchingField: Y.app.search.person.RenderMatchingField(result.raw, query)
                });
            }, this);
        }
    }, {
        ATTRS: {
            resultTextLocator: {
                valueFn: function () {
                    return function (raw) {
                        return '' + raw.id
                    }
                }
            },
            rowTemplate: {
                value: 'ROW_TEMPLATE_FULL'
            },
            headers: {
                value: {
                    'Accept': 'application/vnd.olmgroup-usp.person.SecurityUserWithRolesAndPerson+json'
                }
            }
        }
    });

    Y.namespace('app').SecurityUserAutoCompleteResultView = Y.Base.create('securityUserAutoCompleteResultView', Y.View, [], {
        events: {
            'a.change': {
                click: '_clearSelectedPerson'
            }
        },
        initializer: function (config) {
            //create an instance of the person auto complete view
            this.securityUserAutoCompleteView = new Y.app.SecurityUserAutoCompleteView(Y.mix(config, {
                isPersonSelectable: this.isPersonSelectable
            }));

            //add this as a bubble target
            this.securityUserAutoCompleteView.addTarget(this);

            //handle selection from the autocomplete view
            this.on('*:select', function (e) {
                this.set('selectedPerson', e.result.raw);
            });

            this._evtHandlers = [
                this.after('selectedPersonChange', this._renderSelectedPerson, this),
                this.after('visibleChange', this._setVisibility, this),
                this.after('disabledChange', this._setDisabled, this)
            ]
        },
        isPersonSelectable: function () {
            return true;
        },
        render: function () {
            var selectedPerson = this.get('selectedPerson'),
                autocomplete;

            this.get('container').append(this.securityUserAutoCompleteView.render().get('container'));

            if (selectedPerson) {
                autocomplete = this.securityUserAutoCompleteView.autocomplete;
                if (autocomplete) {
                    //ensure a starting value is set for the inputNode if an initial selection has been made
                    autocomplete.get('inputNode').set('value', autocomplete.get('resultTextLocator').call(null, selectedPerson));
                }
            }

            this._renderSelectedPerson();

            return this;
        },
        _clearSelectedPerson: function () {
            this.set('selectedPerson', null);
        },
        _renderSelectedPerson: function () {
            var selectedPerson = this.get('selectedPerson'),
                container = this.get('container'),
                labels = this.get('labels');

            //remove the person display
            var displayNode = container.one('#acPersonDisplay');

            if (displayNode) {
                displayNode.remove(true);
            }

            if (selectedPerson) {
                //hide the AC
                this.securityUserAutoCompleteView.set('visible', false);

                container.append(SELECTED_PERSON_TEMPLATE({
                    userIdentifier: selectedPerson.userIdentifier || '',
                    userName: selectedPerson.userName || '',
                    personIdentifier: selectedPerson.person.personIdentifier || '',
                    personName: selectedPerson.person.name || '',
                    labels: labels || {}
                }));
            } else {
                //show autocomplete
                this.securityUserAutoCompleteView.set('visible', true);
            }
        },
        _setVisibility: function (e) {
            if (e.newVal === true) {
                this.securityUserAutoCompleteView.set('visible', true);
            } else {
                this.securityUserAutoCompleteView.set('visible', false);
            }
        },
        _setDisabled: function (e) {
            if (e.newVal === true) {
                this.securityUserAutoCompleteView.set('disabled', true);
            } else {
                this.securityUserAutoCompleteView.set('disabled', false);
            }
        },         
        destructor: function () {
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            if (this.securityUserAutoCompleteView) {
                //clean up autocomplete
                this.securityUserAutoCompleteView.removeTarget(this);
                this.securityUserAutoCompleteView.destroy();
            }
        }
    }, {
        ATTRS: {
            visible: {
                value: true
            },
            selectedPerson: {},
            labels: {
                value: {
                    value: {
                        placeholder: 'Enter search criteria',
                        noResults: 'No results found',
                        change: 'change'
                    }
                }
            },
            disabled:{}
        }
    });

}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'template-micro',
        'base-autocomplete-view',  
        'person-name-matcher-highlighter'
    ]
});