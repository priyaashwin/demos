YUI.add('childlookedafter-autocomplete-view', function (Y) {

    'use-strict';

    var A = Y.Array,
        Micro = Y.Template.Micro,
        AC_INPUT = Micro.compile('<input name="<%=this.inputName%>" <%if(this.disabled){%> disabled="disabled"<%}%> class="pure-input-1" id="<%=this.formName%>_<%=this.inputName%>" placeholder="<%=this.placeholder%>" />'),
        SELECTED_PERSON_TEMPLATE = Micro.compile('<div id="acCarerDisplay" class="text">\
                       <ul class="ac-single-selection">\
                           <li class="data-item">\
                               <span><%=this.personIdentifier%> - <%=this.personName%></span>\
                               <a href="#none" class="remove small change"><%=this.labels.change||"change"%></a>\
                           </li>\
                           <li class="data-item">\
                                <span class="address"><%=this.personAddress%></span>\
                           </li>\
                       </ul>\
                   </div>');

    var AutoCompleteCarer = Y.Base.create('extendedPopupAutoComplete', Y.app.ExtendedPopupAutoComplete, [], {
        // TODO decide on date validation/field behaviour
        sendRequest: function (query, requestTemplate) {
            var date = Y.one('#startDate').get('value');
            if (this.isValidDate(date)) {
                AutoCompleteCarer.superclass.sendRequest.call(this, query, requestTemplate);
            }
        },

        isValidDate: function () {
            return true;
        }
    }, {
        CSS_PREFIX: Y.usp.PopupAutoComplete.CSS_PREFIX
    });

    /**
     * @class Y.app.childlookedafter.CarerAutoCompleteView
     * @inherits Y.app.PersonAutoCompleteView
     * @description Subclassed because we to resolve two params based on user input.  See requestTemplate below and AutoCompleteCarer above. 
     */
    Y.namespace('app.childlookedafter').CarerAutoCompleteView = Y.Base.create('carerAutoCompleteView', Y.app.PersonAutoCompleteView, [], {
        initializer:function(){
            this._carerAcEvtHandlers=[
                this.after('disabledChange', this.handleDisabledChange, this)
            ];
        },
        destructor:function(){

            this._carerAcEvtHandlers.forEach(function (handler) {
                handler.detach();
            });
            delete this._carerAcEvtHandlers;

        },
        handleDisabledChange:function(e){
            var inputNode=this.autocomplete.get('inputNode');
            if(inputNode){
                if(e.newVal){
                    inputNode.setAttribute('disabled', 'disabled');
                }else{
                    inputNode.removeAttribute('disabled');
                }
            }
        },
        render: function () {
            var container = this.get('container'),
                visible = this.get('visible'),
                labels = this.get('labels') || {};

            if (!this.autocomplete) {
                this.autocomplete = new AutoCompleteCarer({
                    inputNode: container.appendChild(AC_INPUT({
                        inputName: this.get('inputName'),
                        formName: this.get('formName'),
                        placeholder: labels.placeholder || '',
                        disabled: this.get('disabled')
                    })),
                    allowBrowserAutocomplete: false,
                    enableCache: false,
                    maxResults: this.get('maxResults'),
                    minQueryLength: 1,
                    source: this.get('source'),
                    scrollIntoView: true,
                    resultListLocator: function (response) {
                        return response.results || [];
                    },
                    requestTemplate: function (query) {
                        var node = Y.one(this.get('containingNodeSelector')),
                            date = Y.USPDate.parseDate(node.one('#startDate').get('value'));
                        return '&name=' + query + '&approvedOn=' + (date ? date.getTime() : '');
                    }.bind(this),
                    resultTextLocator: this.get('resultTextLocator'),
                    resultFormatter: this.getResultFormatter(),
                    headers: this.get('headers') || {}
                }).render();
                //Add event target
                this.autocomplete.addTarget(this);
            }

            this.autocomplete.get('inputNode').focus();

            if (!visible) {
                this.get('container').hide();
            }

            return this;
        }
    },{
        ATTRS:{
            disabled:{},
            preventSecuredSelection: {
                value: false
            }
        }
    });

    Y.namespace('app.childlookedafter').CarerAutoCompleteResultView = Y.Base.create('carerAutoCompleteResultView', Y.View, [], {
        events: {
            'a.change': {
                click: '_clearSelectedPerson'
            }
        },
        initializer: function (config) {
            //create an instance of the person auto complete view
            this.carerAutoCompleteView = new Y.app.childlookedafter.CarerAutoCompleteView(Y.mix(config, {
                isPersonSelectable: this.isPersonSelectable
            }));

            //add this as a bubble target
            this.carerAutoCompleteView.addTarget(this);

            //handle selection from the autocomplete view
            this.on('*:select', function (e) {
                this.set('selectedPerson', e.result.raw);
            });

            this._evtHandlers = [
                this.after('selectedPersonChange', this._renderSelectedPerson, this),
                this.after('visibleChange', this._setVisibility, this),
                this.after('disabledChange', this._setDisabled, this)
            ]
        },
        isPersonSelectable: function () {
            return true;
        },
        render: function () {
            this.get('container').append(this.carerAutoCompleteView.render().get('container'));
            this._renderSelectedPerson();
            return this;
        },
        _clearSelectedPerson: function () {
            this.set('selectedPerson', null);
        },
        _renderSelectedPerson: function () {
            var selectedPerson = this.get('selectedPerson'),
                container = this.get('container'),
                labels = this.get('labels');

            //remove the person display
            var displayNode = container.one('#acCarerDisplay');

            if (displayNode) {
                displayNode.remove(true);
            }

            if (selectedPerson) {
                //hide the AC
                this.carerAutoCompleteView.set('visible', false);
                var address = '';
                if (typeof selectedPerson.address === 'object' && selectedPerson.address !== null) {
                    address = selectedPerson.address.location.location;
                }

                container.append(SELECTED_PERSON_TEMPLATE({
                    personIdentifier: selectedPerson.personIdentifier || '',
                    personName: selectedPerson.name || '',
                    personAddress: address || '',
                    labels: labels || {}
                }));
            } else {
                //show autocomplete
                this.carerAutoCompleteView.set('visible', true);
            }
        },
        _setVisibility: function (e) {
            if (e.newVal === true) {
                this.carerAutoCompleteView.set('visible', true);
            } else {
                this.carerAutoCompleteView.set('visible', false);
            }
        },
        _setDisabled: function (e) {
            if (e.newVal === true) {
                this.carerAutoCompleteView.set('disabled', true);
            } else {
                this.carerAutoCompleteView.set('disabled', false);
            }
        },        
        destructor: function () {
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }
            if (this.carerAutoCompleteView) {
                this.carerAutoCompleteView.removeTarget(this);
                this.carerAutoCompleteView.destroy();
            }
        }
    }, {
        ATTRS: {
            visible: {
                value: true
            },
            selectedPerson: {},
            labels: {
                value: {
                    value: {
                        placeholder: 'Enter search criteria',
                        noResults: 'No results found',
                        change: 'change'
                    }
                }
            },
            disabled:{}
        }
    });

}, '0.0.1', {
    requires: [
        'yui-base',
        'view',
        'template-micro',
        'popup-autocomplete',
        'person-autocomplete-view'
    ]
});