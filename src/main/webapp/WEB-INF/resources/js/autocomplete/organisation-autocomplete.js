YUI.add('organisation-autocomplete-view', function (Y) {
    var A = Y.Array,
        Micro = Y.Template.Micro,
        //Note - the following fields in the template are considered safe as they should have been pre-escaped
        //matchingField - if this changes - please update the template
        ROW_TEMPLATE_FULL = Micro.compile('<div  title="<%=this.name%>">\
                                        <div><span><%=this.organisationIdentifier%> - <%=this.name%></span></div>\
                                        <%if(this.address){%>\
                                            <div class="txt-color">\
                                                <span class="address"><%=this.address%></span>\
                                            </div>\
                                        <%}%>\
                                        <%if(this.type){%>\
                                            <div class="txt-color">\
                                                <span class="type"><%=this.type%></span>\
                                            </div>\
                                        <%}%>\
                                        <%==this.matchingField%>\
                                    </div>'),
        ROW_TEMPLATE_SIMPLE = Micro.compile('<div title="<%=this.name%>">\
                                            <div><span><%=this.organisationIdentifier%> - <%=this.name%></span></div>\
                                            <%==this.matchingField%>\
                                            </div>'),
        SELECTED_ORGANISATION_TEMPLATE = Micro.compile('<div id="acOrganisationDisplay" class="text">\
                                                   <ul class="ac-single-selection">\
                                                       <li class="data-item">\
                                                           <span><%=this.organisationIdentifier%> - <%=this.organisationName%></span>\
                                                           <a href="#none" class="remove small change"><%=this.labels.change||"change"%></a>\
                                                       </li>\
                                                           <li class="data-item">\
    		                                                  <span class="address"><%=this.organisationAddress%></span>\
                                                           </li>\
                                                   </ul>\
                                               </div>');

    Y.namespace('app').OrganisationAutoCompleteView = Y.Base.create('organisationAutoCompleteView', Y.app.BaseAutoCompleteView, [], {
        getResultFormatter: function () {
            return Y.bind(this.resultFormatter, this);
        },
        resultFormatter: function (query, results) {
            var rowTemplateName = this.get('rowTemplate'),
                rowTemplate;
            switch (rowTemplateName) {
            case 'ROW_TEMPLATE_SIMPLE':
                rowTemplate = ROW_TEMPLATE_SIMPLE;
                break;
            default:
                rowTemplate = ROW_TEMPLATE_FULL;
            }
            return A.map(results, function (result) {
                var _rawAddress = result.raw.address || {},
                    location = _rawAddress.location || {},
                    address = location.location || '',
                    type = Y.uspCategory.organisation.organisationType.category.codedEntries[result.raw.type],
                    subType = Y.uspCategory.organisation.organisationSubType.category.codedEntries[result.raw.subType],
                    typeInfo = [];

                if (type && (type.name || '').length > 0) {
                    typeInfo.push(type.name);

                    if (subType && (subType.name || '').length > 0) {
                        typeInfo.push(subType.name);
                    }
                }

                return rowTemplate({
                    name: result.raw.name,
                    organisationIdentifier: result.raw.organisationIdentifier,
                    type: typeInfo.join(' - '),
                    matchingField: Y.app.search.organisation.RenderMatchingField(result.raw, query)
                });
            }, this);
        }
    }, {
        ATTRS: {
            resultTextLocator: {
                valueFn: function () {
                    return function (raw) {
                        return '' + raw.id
                    }
                }
            },
            rowTemplate: {
                value: 'ROW_TEMPLATE_FULL'
            },
            'preventSecuredSelection': {
                value: true
            },
            headers: {
                value: {
                    'Accept': 'application/vnd.olmgroup-usp.organisation.OrganisationWithAddressAndPreviousNames+json'
                }
            }
        }
    });

    Y.namespace('app').OrganisationAutoCompleteResultView = Y.Base.create('organisationAutoCompleteResultView', Y.View, [], {
        events: {
            'a.change': {
                click: '_clearSelectedOrganisation'
            }
        },
        initializer: function (config) {
            this.after('visibleChange', this._setVisibility, this)
            //create an instance of the organisation auto complete view
            this.organisationAutoCompleteView = new Y.app.OrganisationAutoCompleteView(config);

            //add this as a bubble target
            this.organisationAutoCompleteView.addTarget(this);

            //handle selection from the autocomplete view
            this.on('*:select', function (e) {
                this.set('selectedOrganisation', e.result.raw);
            });

            this._evtHandlers = [
                this.after('selectedOrganisationChange', this._renderSelectedOrganisation, this),
                this.after('visibleChange', this._setVisibility, this),
                this.after('disabledChange', this._setDisabled, this)
            ]
        },
        render: function () {
            this.get('container').append(this.organisationAutoCompleteView.render().get('container'));

            this._renderSelectedOrganisation();

            return this;
        },
        _clearSelectedOrganisation: function () {
            this.set('selectedOrganisation', null);
        },
        _renderSelectedOrganisation: function () {
            var selectedOrganisation = this.get('selectedOrganisation'),
                container = this.get('container'),
                labels = this.get('labels');

            //remove the organisation display
            var displayNode = container.one('#acOrganisationDisplay');

            if (displayNode) {
                displayNode.remove(true);
            }

            if (selectedOrganisation) {
                //hide the AC
                this.organisationAutoCompleteView.set('visible', false);
                var address = '';
                if (typeof selectedOrganisation.address === 'object' && selectedOrganisation.address !== null) {
                    address = selectedOrganisation.address.location.location;
                }

                container.append(SELECTED_ORGANISATION_TEMPLATE({
                    organisationIdentifier: selectedOrganisation.organisationIdentifier || '',
                    organisationName: selectedOrganisation.name || '',
                    organisationAddress: address || '',
                    labels: labels || {}
                }));
            } else {
                //show autocomplete
                this.organisationAutoCompleteView.set('visible', true);
            }
        },
        _setVisibility: function (e) {
            if (e.newVal === true) {
                this.organisationAutoCompleteView.set('visible', true);
            } else {
                this.organisationAutoCompleteView.set('visible', false);
            }
        },
        _setDisabled: function (e) {
            if (e.newVal === true) {
                this.organisationAutoCompleteView.set('disabled', true);
            } else {
                this.organisationAutoCompleteView.set('disabled', false);
            }
        },  
        destructor: function () {
            if (this._evtHandlers) {
                A.each(this._evtHandlers, function (item) {
                    item.detach();
                });
                //null out
                this._evtHandlers = null;
            }

            if (this.organisationAutoCompleteView) {
                //clean up organisation autocomplete
                this.organisationAutoCompleteView.removeTarget(this);
                this.organisationAutoCompleteView.destroy();
            }
        }
    }, {
        ATTRS: {
            visible: {
                value: true
            },
            selectedOrganisation: {},
            labels: {
                value: {
                    value: {
                        placeholder: 'Enter search criteria',
                        noResults: 'No results found',
                        change: 'change'
                    }

                }
            },
            disabled:{}
        }
    });
}, '0.0.1', {
    requires: ['yui-base',
        'view',
        'template-micro',
        'base-autocomplete-view',
        'categories-organisation-component-OrganisationType',
        'categories-organisation-component-OrganisationSubType',
        'organisation-name-matcher-highlighter'
    ]
});