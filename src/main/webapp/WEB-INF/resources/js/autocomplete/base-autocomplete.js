YUI.add('base-autocomplete-view', function(Y) {
    var A = Y.Array,
        Micro = Y.Template.Micro,
        AC_INPUT = Micro.compile('<input name="<%=this.inputName%>" <%if(this.disabled){%> disabled="disabled"<%}%> class="pure-input-1" id="<%=this.formName%>_<%=this.inputName%>" placeholder="<%=this.placeholder%>" />'),
        AC_NO_RESULTS = Micro.compile('<div class="no-search-results pure-info"><p class="estimatedInfo"><%=this.noResults%></p></div>'),
        _SOURCE_SUCCESS = '_sourceSuccess';

    var ExtendedPopupAutoComplete=Y.Base.create('extendedPopupAutoComplete', Y.usp.PopupAutoComplete, [], {
        /**
        Creates a DataSource-like object that uses `Y.io` as a source. See the
        `source` attribute for more details.
     
        @method _createIOSource
        @param {String} source URL.
        @return {Object} DataSource-like object.
        @protected
        @for AutoCompleteBase
        **/
        _createIOSource: function (source) {
            var ioSource = {type: 'io'},
                that     = this,
                ioRequest, lastRequest, loading;
     
            // Private internal _sendRequest method that will be assigned to
            // ioSource.sendRequest once io-base and json-parse are available.
            function _sendRequest(request) {
                var cacheKey = request.request;
     
                // Return immediately on a cached response.
                if (that._cache && cacheKey in that._cache) {
                    that[_SOURCE_SUCCESS](that._cache[cacheKey], request);
                    return;
                }
     
                // Cancel any outstanding requests.
                if (ioRequest && ioRequest.isInProgress()) {
                    ioRequest.abort();
                }
     
                ioRequest = Y.io(that._getXHRUrl(source, request), {
                    on: {
                        success: function (tid, response) {
                            var data;
     
                            try {
                                data = Y.JSON.parse(response.responseText);
                            } catch (ex) {
                                Y.error('JSON parse error', ex);
                            }
     
                            if (data) {
                                that._cache && (that._cache[cacheKey] = data);
                                that[_SOURCE_SUCCESS](data, request);
                            }
                        }
                    },
                    headers:that.get('headers')
                });
            }
     
            ioSource.sendRequest = function (request) {
                // Keep track of the most recent request in case there are multiple
                // requests while we're waiting for the IO module to load. Only the
                // most recent request will be sent.
                lastRequest = request;
     
                if (loading) { return; }
     
                loading = true;
     
                // Lazy-load the io-base and json-parse modules if necessary,
                // then overwrite the sendRequest method to bypass this check in
                // the future.
                Y.use('io-base', 'json-parse', function () {
                    ioSource.sendRequest = _sendRequest;
                    _sendRequest(lastRequest);
                });
            };
     
            return ioSource;
        }
    },{
        CSS_PREFIX: Y.usp.PopupAutoComplete.CSS_PREFIX,
        ATTRS:{
            headers:{}
        }
    });
    
    Y.namespace('app').BaseAutoCompleteView = Y.Base.create('baseAutoCompleteView', Y.View, [], {
        initializer: function() {
            this._acEvtBnds = [
                // we check returned results displaying an info message if none are returned 
                this.after('*:results', this._setResultsMessage, this),
                this.after('visibleChange', this._setACVisibility, this),
                this.after('disabledChange', this.handleDisabledChange, this)
            ];
        },
        _setACVisibility: function(e) {
            if (e.newVal === true) {
                this.autocomplete.get('inputNode').set('value','');
                this.get('container').show();
            } else {
                if (this.autocomplete) {
                    this.autocomplete.get('inputNode').blur();
                    this.autocomplete.hide();
                }
                this.get('container').hide();
            }
        },
        destructor: function() {
            //unbind event handles
            this._acEvtBnds.forEach(function (handler) {
                handler.detach();
            });
            delete this._acEvtBnds;
            
            if (this.autocomplete) {
                this.autocomplete.destroy();
                delete this.autocomplete;
            }
        },
        _setResultsMessage: function(e) {
            var container = this.get('container'),
                labels = this.get('labels') || {};
            if (e.results.length === 0) {
                if(!this._noResultsMessage){
                    this._noResultsMessage = container.appendChild (AC_NO_RESULTS({
                        noResults: labels.noResults || 'No results found'
                    }));
                }
            } else {
                if (this._noResultsMessage) {
                    this._noResultsMessage.remove(true);
                    delete this._noResultsMessage;
                }
            }
        },
        clearInput:function(){
          if(this.autocomplete){
            this.autocomplete.get('inputNode').set('value', '');
          }
        },
        handleDisabledChange:function(e){
            var inputNode=this.autocomplete.get('inputNode');
            if(inputNode){
                if(e.newVal){
                    inputNode.setAttribute('disabled', 'disabled');
                }else{
                    inputNode.removeAttribute('disabled');
                }
            }
        },
        render: function() {
            var container = this.get('container'),
                visible = this.get('visible'),
                labels = this.get('labels') || {};
                
            if (!this.autocomplete) {
                this.autocomplete = new ExtendedPopupAutoComplete({
                    inputNode: container.appendChild (AC_INPUT({
                        inputName: this.get('inputName'),
                        formName: this.get('formName'),
                        placeholder: labels.placeholder || '',
                        disabled: this.get('disabled')
                    })),
                    allowBrowserAutocomplete: false,
                    enableCache: false,
                    maxResults: this.get('maxResults'),
                    minQueryLength: 1,
                    source: this.get('source'),
                    scrollIntoView: true,
                    resultListLocator: function(response) {
                        return response.results || [];
                    },
                    resultTextLocator: this.get('resultTextLocator'),
                    resultFormatter: this.getResultFormatter(),
                    headers:this.get('headers')||{}
                }).render();
                //Add event target
                this.autocomplete.addTarget(this);
            }
            
            
            
            if(!visible){
                container.hide();
            }else{              
              //delay node focus to allow time for DOM update
              Y.later(100, this, function(){
                if(this.autocomplete){
                  this.autocomplete.get('inputNode').focus();
                }
              });
            }

            return this;
        },
        getResultFormatter: function() {
            return function() {
                return ('<div>Implement this in your subclass</div>');
            };
        }
    }, {
        ATTRS: {
            headers: {
                value:{
                    'Accept': 'application/json'
                }
            },
            container:{
                valueFn: function() {
                     return Y.Node.create('<div class="ac-view yui3-skin-sam"></div>');
                  }
            },
            source:{
                getter:function(){
                    return this.get('autocompleteURL');
                }
            },
            visible: {
                value: true
            },
            maxResults: {
                value: 50
            },
            autocompleteURL: {
                value: ''
            },
            resultTextLocator:{
                value:'_type'
            },
            labels: {
                value: {
                    placeholder: 'Enter search criteria',
                    noResults: 'No results found'
                }
            },
            inputName: {
                value: 'myInput'
            },
            formName: {
                value: 'myForm'
            }
        }
    });
    
    
    Y.namespace('app').ExtendedPopupAutoComplete = ExtendedPopupAutoComplete;
    
}, '0.0.1', {
    requires: ['yui-base', 
               'view',
               'yui-later',
               'template-micro',
               'popup-autocomplete'
               ]
});