/**
 Provides formatters for forms based results lists
 @module forms-results-formatters
 */
YUI.add('form-results-formatters', function(Y) {
  var E = Y.Escape,
    L = Y.Lang;

  var FORM_STATE_DRAFT = 'DRAFT',
    FORM_STATE_SUBMITTED = 'SUBMITTED',
    FORM_STATE_COMPLETE = 'COMPLETE',
    FORM_STATE_AUTHORISED = 'AUTHORISED',
    FORM_STATE_DELETED = 'ARCHIVED',
    FORM_TYPE_STATE_DRAFT = 'DRAFT',
    FORM_TYPE_STATE_PUBLISHED = 'PUBLISHED',
    FORM_TYPE_STATE_DELETED = 'ARCHIVED';

  var FORM_TYPE_PR = 'PlacementReport',
    FORM_TYPE_CPRA = 'CarePlanRiskAssessment',
    FORM_TYPE_INTAKE = 'IntakeForm',
    FORM_TYPE_PR_DISPLAY = 'PLACE',
    FORM_TYPE_CPRA_DISPLAY = 'CPRA',
    FORM_TYPE_INTAKE_DISPLAY = 'INTAKE',
    FORM_REVIEW_STATE_APPROVED = 'APPROVED',
    FORM_REVIEW_STATE_REJECTED = 'REJECTED';

  var review_template_href = '<a href="#" class="review-state" data-title="Review state" data-content="{stateType}" aria-label="{stateType} review state">',
    review_template_REJECTED = '<i class="icon-state icon-state-rejected"></i></a>',
    review_template_APPROVED = '<i class="icon-state icon-state-complete-authorised"></i></a>',
    type_template_href = '<a href="#" class="form-type-state" data-title="Form type state" data-content="{stateType}" aria-label="{stateType} form type state">',
    type_template_DRAFT = '<i class="icon-state icon-state-draft"></i></a>',
    type_template_PUBLISHED = '<i class="icon-state icon-state-published"></i></a>',
    type_template_DELETED = '<i class="icon-state icon-state-deleted"></i></a>';

  var template_href = '<a href="#" class="form-state" data-title="Form state" data-content="{stateType}" aria-label="{stateType} form state">',
    template_DRAFT = '<i class="icon-state icon-state-draft"></i></a>',
    template_SUBMITTED = '<i class="icon-state icon-state-submitted"></i></a>',
    template_COMPLETE = '<i class="icon-state icon-state-complete"></i></a>',
    template_COMPLETE_AUTHORISED = '<i class="icon-state icon-state-complete-authorised"></i></a>',
    template_REJECTED = '<i class="icon-state icon-state-rejected"></i></a>',
    template_AUTHORISED = '<i class="icon-state icon-state-authorised"></i></a>',
    template_DELETED = '<i class="icon-state icon-state-deleted"></i></a>';

  var subjectTypeTemplate = '<a href="#none" class="{infoClass}" data-title="{subjectTitle}" data-content="{subjectData}" aria-label="{subjectTitle} form state"><i class="icon-state {subjectIcon}"></i></a>';

  var Formatters = {
    formLastChangedBy: function(o) {
      var lastChangedBy,
        state = o.data['status'];
      if (state === FORM_STATE_DRAFT) {
        lastChangedBy = o.data['initiatorName'];
      } else if (state === FORM_STATE_SUBMITTED) {
        lastChangedBy = o.data['submitterName'];
      } else if (state === FORM_STATE_COMPLETE) {
        lastChangedBy = o.data['completerName'];
      } else if (state === FORM_STATE_DELETED) {
        lastChangedBy = o.data['archiverName'];
      }
      if (lastChangedBy === null) {
        lastChangedBy = '';
      }
      return lastChangedBy;
    },
    nameFormatter: function(o) {
    	return o.value===null?'':o.value
    },
    formState: function(o) {
      var state = o.data['status'],
        hasRejections = o.data['hasRejections'],
        isAuthorised = (o.data['dateSubmitted'] !== null && o.data['dateCompleted'] !== null);
      if (state === FORM_STATE_DRAFT && hasRejections) {
        return L.sub(template_href, {
          stateType: E.html(state + ' (REJECTED)')
        }) + template_REJECTED;
      } else if (state === FORM_STATE_DRAFT) {
        return L.sub(template_href, {
          stateType: E.html(state)
        }) + template_DRAFT;
      } else if (state === FORM_STATE_SUBMITTED) {
        return L.sub(template_href, {
          stateType: E.html(state)
        }) + template_SUBMITTED;
      } else if (state === FORM_STATE_COMPLETE && isAuthorised) {
        return L.sub(template_href, {
          stateType: E.html(state + ' (AUTHORISED)')
        }) + template_COMPLETE_AUTHORISED;
      } else if (state === FORM_STATE_COMPLETE) {
        return L.sub(template_href, {
          stateType: E.html(state)
        }) + template_COMPLETE;
      } else if (state === FORM_STATE_AUTHORISED) {
        return L.sub(template_href, {
          stateType: E.html(state)
        }) + template_AUTHORISED;
      } else if (state === FORM_STATE_DELETED) {
        return L.sub(template_href, {
          stateType: E.html('DELETED')
        }) + template_DELETED;
      }
      return '';
    },
    formType: function(o) {
      var formType = o.data['formType'];
      if (formType === FORM_TYPE_PR) {
        return FORM_TYPE_PR_DISPLAY;
      } else if (formType === FORM_TYPE_CPRA) {
        return FORM_TYPE_CPRA_DISPLAY;
      } else if (formType === FORM_TYPE_INTAKE) {
        return FORM_TYPE_INTAKE_DISPLAY;
      }
      return '';
    },
    formTypeState: function(o) {
      var state = o.data['status'];
      if (state === FORM_TYPE_STATE_DRAFT) {
        return L.sub(type_template_href, {
          stateType: E.html(state)
        }) + type_template_DRAFT;
      } else if (state === FORM_TYPE_STATE_PUBLISHED) {
        return L.sub(type_template_href, {
          stateType: E.html(state)
        }) + type_template_PUBLISHED;
      } else if (state === FORM_TYPE_STATE_DELETED) {
        return L.sub(type_template_href, {
          stateType: E.html('DELETED')
        }) + type_template_DELETED;
      }
      return '';
    },
    formReviewState: function(o) {
      var state = o.data['status'];
      if (state === FORM_REVIEW_STATE_APPROVED) {
        return L.sub(review_template_href, {
          stateType: 'AUTHORISED'
        }) + review_template_APPROVED;
      } else if (state === FORM_REVIEW_STATE_REJECTED) {
        return L.sub(review_template_href, {
          stateType: E.html(state)
        }) + review_template_REJECTED;
      }
      return '';
    },
    formName: function(o) {
      var name = [],
        subType;

      if (o) {
        name.push(o.value);
        subType = o.data.subType;
        if (subType && subType.trim().length > 0) {
          name.push(subType);
        }
      }

      return name.join(' - ');
    },
    formSubjectIcon: function(o) {
      var subjectType, subjectName,
        personIcon = 'fa fa-user secondary-icon',
        groupIcon = 'fa fa-users secondary-icon',
        personTitle = 'Person form',
        groupTitle = 'Group form',
        data = {
          infoClass:o.column.infoClass||'form-state'
        };
      if (o) {
        subjectType = o.data['subjectType'];
        subjectDisplay = o.data['subjectName']+' ('+o.data['subjectIdentifier']+')';
        switch (subjectType) {
          case 'GROUP':
            data.subjectIcon = groupIcon;
            data.subjectTitle = groupTitle;
            data.subjectData = E.html(subjectDisplay);
            break;
          case 'PERSON':
            data.subjectIcon = personIcon;
            data.subjectTitle = personTitle;
            data.subjectData = E.html(subjectDisplay);
            break;
        }

        return L.sub(subjectTypeTemplate, data);
      }
    },
    formSubject: function(o){
      return o.data['subjectName']+' ('+o.data['subjectIdentifier']+')';
    }

  };

  Y.namespace('app.form').ColumnFormatters = Formatters;

}, '0.0.1', {
  requires: ['yui-base',
               'escape',
               'app-utils-person',
               'usp-date',
               'results-formatters',
               'handlebars',
               'handlebars-helpers',
               'handlebars-person-templates',
               'handlebars-group-templates']
});