YUI.add('app-view-mode-plugin', function(Y) {

    var CSS_HIDDEN_CLASS = 'yui3-hide',
        STATE_SUMMARY = 'reading',
        STATE_DETAILED = 'initial';

    function ViewModePlugin(config) {
        ViewModePlugin.superclass.constructor.apply(this, arguments);
    }

    ViewModePlugin.NAME = 'viewModePlugin';
    ViewModePlugin.NS = 'viewModePlugin';


    /* ViewModePlugin extends the base Plugin.Base class */
    /* After including the plugin to results table: 
     * Set column property to { readingView: true } to make it visible in 'reading view'. 
     * Set column property to { readingView:'N%' } to show a column in reading view with different size.
     * Set column property to { startHidden: true } to ensure current orderBy implementation works correctly */

    Y.extend(ViewModePlugin, Y.Plugin.Base, {

        /**
         * @method initializer
         * @protected
         */

        initializer: function(config) {
            var host = this.get('host'),
                columns = host.get('columns');

            //add host as bubble target
            this.addTarget(host);

            this.augmentColumnConfiguration(columns);

            this._viewModeEvtHandles = [
                this.after('modeChange', this._handleAfterModeChange, this)
            ];

            //ensure the mode is set from the config
            this.set('mode', config.initialState || this.get('initialState'));
        },
        augmentColumnConfiguration: function(columns) {
            columns.forEach(function(column) {
                column.initialWidth = column.width;
                column.initialClassName = column.className;
            });
        },

        /**
         * Unbinds event handlers
         * @method destructor
         * @protected
         */

        destructor: function() {
            this.removeTarget(this.get('host'));

            this._viewModeEvtHandles.forEach(function(handle) {
                handle.detach();
            });

            delete this._viewModeEvtHandles;
        },
        /**
         * Sets the mode attribute
         * 
         * @method setMode
         * @public
         */

        setMode: function(mode) {
            this.set('mode', mode);
        },
        _handleAfterModeChange: function(e) {
            switch (e.newVal) {
                case STATE_SUMMARY:
                    this._setReadingMode();
                    break;
                default:
                    this._setInitialMode();
            }
        },
        /**
         * Sets 'initial' mode.
         * @method _setInitialMode
         * @private
         */

        _setInitialMode: function() {
            var host = this.get('host'),
                columns = host.get('columns');

            columns.forEach(function(column) {
                // ...reset column to initial width and initial class name
                column.width = column.initialWidth;
                // ...hide any column that starts hidden
                if (column.startHidden) {
                    column.className = CSS_HIDDEN_CLASS;
                } else {
                    column.className = column.initialClassName;
                }
            });
            
            // Finally figure out which 'startHidden' columns might now visible using sortBy state on myResults
            Y.Object.each(host.get('sortBy')[0], function(val, key) {
              var column=host.getColumn(key);
              if(column){
                column.className='';
              }
            }.bind(this)); 
            

            //update the columns attribute on the host
            host.set('columns', columns);
        },


        /**
         * Sets the 'reading' mode.
         * Saves the width and className to enable a return to 'initial' state.
         * @method _setReadingMode
         * @private
         */

        _setReadingMode: function() {
            var host = this.get('host'),
                columns = host.get('columns');

            columns.forEach(function(column) {
                // ...hide if it has no truthy readingView
                if (!column.readingView) {
                    column.className = CSS_HIDDEN_CLASS;
                } else {
                    column.width = (typeof column.readingView === 'string') ? column.readingView : column.initialWidth;
                }
            });

            //update the columns attribute on the host
            host.set('columns', columns);
        }
    }, {
        ATTRS: {

            /**
             * @attribute initialState
             * @description holds the starting mode value
             * @default 'reading'
             */
            initialState: {
                value: STATE_SUMMARY
            },

            /**
             * @attribute mode
             * @description holds current view mode value
             * @type {String}
             */
            mode: {
                value: null
            }
        }
    });

    Y.namespace('Plugin.app').ViewModePlugin = ViewModePlugin;

}, '0.0.1', {
    requires: [
        'plugin',
        'yui-base',
        'event-custom-base',
        'dom-base'
    ]
});