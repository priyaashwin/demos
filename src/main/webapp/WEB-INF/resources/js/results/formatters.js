/**
 provides a number of useful formatters for use in DataTables
 which are unique to Case Recoding
 @module caserecording-results-formatters
 @author Steve Pile
 @since 1.0.0
 */
YUI.add('caserecording-results-formatters', function(Y) {
  var L = Y.Lang,
    E = Y.Escape,
    A = Y.Array,
    UP = Y.app.utils.person,
    DEFAULT_PERSON_NAME_MAXLENGTH = 30,
    template_LINKABLE = '<a href="{url}" class="switch-focus" data-menu-action="{clazzName}" title="{title}">{value}</a>',
    template_NHS_CHI_NUMBERS = ' <span>{nhs}</span><br/><span>{chi}</span>',
    template_ESTIMATED_DATE = ' <span class="estimated" title="Estimated">{date}<span class="hidden">Estimated</span></span>',
    checklist_template_href = '<a href="#" class="tool-tip checklist-state" data-title="Work list state" data-content="{stateChecklist}" aria-label="{stateChecklist} work list state">',
    checklist_template_DRAFT = '<i class="icon-state icon-state-draft"></i></a>',
    checklist_template_PUBLISHED = '<i class="icon-state icon-state-published"></i></a>',
    checklist_template_DELETED = '<i class="icon-state icon-state-deleted"></i></a>',
    calendar_template_href = '<a href="#" class="tool-tip calendar-{column}" data-title="{tooltipTitle}" data-content="{content}" aria-label="{content} calendar {column}">',
    calendar_template_ACTIVE = '<i class="icon-state icon-state-active"></i></a>',
    calendar_template_IN_ACTIVE = '<i class="icon-state icon-state-in-active"></i></a>',
    calendar_template_TRUE = '<i class="icon-state icon-default-true"></i></a>',
    calendar_template_FALSE = '<i class="icon-state icon-default-false"></i></a>',
    rule_template_href = '<a href="#" class="tool-tip rule-state" data-title="Rule state" data-content="{stateRule}" aria-label="{stateRule} rule state">',
    rule_template_DRAFT = '<i class="icon-state icon-state-draft"></i></a>',
    rule_template_PUBLISHED = '<i class="icon-state icon-state-published"></i></a>',
    rule_template_DELETED = '<i class="icon-state icon-state-deleted"></i></a>',
    address_template_DND = '<div class="addressLocation-wrapper"><div class="pop-up-data" data-content="Do not disclose" data-align="bottom"><span class="do-not-disclose"> <i class="fa fa-ban" aria-hidden="true"></i> </span></div></div>',
    CHECKLIST_STATE_DRAFT = 'DRAFT',
    CHECKLIST_STATE_PUBLISHED = 'PUBLISHED',
    CHECKLIST_STATE_DELETED = 'ARCHIVED',
    CALENDAR_STATE_ACTIVE = 'ACTIVE',
    CALENDAR_STATE_IN_ACTIVE = 'IN_ACTIVE',
    CALENDAR_DEFAULT_CALENDAR_TRUE = true,
    CALENDAR_DEFAULT_CALENDAR_FALSE = false,
    RULE_STATE_DRAFT = 'DRAFT',
    RULE_STATE_PUBLISHED = 'PUBLISHED',
    RULE_STATE_DELETED = 'ARCHIVED',
    beautifyDescription = function(string) {
      string = string.toLowerCase().replace("_", " ");
      return string.charAt(0).toUpperCase() + string.slice(1);
    };

  var Formatters = {
    checklistState: function(o) {
      var state = o.data['status'];
      if (state === CHECKLIST_STATE_DRAFT) {
        return L.sub(checklist_template_href, {
          stateChecklist: E.html(state)
        }) + checklist_template_DRAFT;
      } else if (state === CHECKLIST_STATE_PUBLISHED) {
        return L.sub(checklist_template_href, {
          stateChecklist: E.html(state)
        }) + checklist_template_PUBLISHED;
      } else if (state === CHECKLIST_STATE_DELETED) {
        return L.sub(checklist_template_href, {
          stateChecklist: E.html('DELETED')
        }) + checklist_template_DELETED;
      }
      return '';
    },
    calendarState: function(o) {
      var state = o.data['status'],
        template = '';

      if (typeof state != "undefined") {
        template += L.sub(calendar_template_href, {
          tooltipTitle: 'Calendar State',
          column: 'state',
          content: E.html(beautifyDescription(state))
        });
      }

      if (state === CALENDAR_STATE_ACTIVE) {
        template += calendar_template_ACTIVE;
      } else if (state === CALENDAR_STATE_IN_ACTIVE) {
        template += calendar_template_IN_ACTIVE;
      }
      return template;
    },
    calendarDefault: function(o) {
      var isDefaultCalendar = o.data['defaultCalendar'],
        template = '';

      if (typeof isDefaultCalendar != "undefined") {
        template += L.sub(calendar_template_href, {
          tooltipTitle: 'Default Calendar?',
          column: 'default',
          content: E.html(beautifyDescription(isDefaultCalendar.toString()))
        });
      }

      if (isDefaultCalendar === CALENDAR_DEFAULT_CALENDAR_TRUE) {
        template += calendar_template_TRUE;
      } else if (isDefaultCalendar === CALENDAR_DEFAULT_CALENDAR_FALSE) {
        template += calendar_template_FALSE;
      }
      return template;
    },
    ruleState: function(o) {
      var state = o.data['status'];
      if (state === RULE_STATE_DRAFT) {
        return L.sub(rule_template_href, {
          stateRule: E.html(state)
        }) + rule_template_DRAFT;
      } else if (state === RULE_STATE_PUBLISHED) {
        return L.sub(rule_template_href, {
          stateRule: E.html(state)
        }) + rule_template_PUBLISHED;
      } else if (state === RULE_STATE_DELETED) {
        return L.sub(rule_template_href, {
          stateRule: E.html('DELETED')
        }) + rule_template_DELETED;
      }
      return '';
    },
    button: function(o) {
      var button = '',
        allowed, visible, buttonConfig = o.column.button;
      if (buttonConfig) {
        //turn on HTML
        o.column.allowHTML = true;

        //check if button visible
        visible = (typeof buttonConfig.visible === 'undefined') ? true : false;
        if (typeof buttonConfig.visible === 'function') {
          visible = buttonConfig.visible.call(o);
        } else if (typeof buttonConfig.visible === 'boolean') {
          visible = buttonConfig.visible;
        }
        if (visible) {
          //check if button enabled
          allowed = (typeof buttonConfig.enabled === 'undefined') ? true : false;
          if (typeof buttonConfig.enabled === 'function') {
            allowed = buttonConfig.enabled.call(o);
          } else if (typeof buttonConfig.enabled === 'boolean') {
            allowed = buttonConfig.enabled;
          }
          if (allowed) {
            button = L.sub('<button class="yui3-button pure-button-primary {className}" data-menu-action="{className}" title="{title}"><i class="{iconClassName}"></i> {label}</button>', {
              className: E.html(buttonConfig.clazz),
              iconClassName: E.html(buttonConfig.iconClazz),
              title: E.html(buttonConfig.title),
              label: E.html(buttonConfig.label)
            });
          } else {
            button = '<button class="yui3-button pure-button-disabled"><i class="' + E.html(buttonConfig.iconClazz) + '"></i> ' + E.html(buttonConfig.label) + '</button>';
          }
        }
      }
      return button;
    },
    formatDateBasedOnLifeState: function(lifeState, dueDate, dob, dobEstimated, age, isAutoComplete) {
      var dobInfo = [];
      var resolvedLifeState = UP.GetImpliedLifeState(lifeState);
      if (resolvedLifeState === 'UNBORN') {
        var formatedDueDate = Y.USPDate.formatDateValue(dueDate, null);
        if (formatedDueDate) {
          if (isAutoComplete) {
            dobInfo.push(' Due date');
          } else {
            dobInfo.push('<strong> Due date </strong>');
          }
          dobInfo.push(formatedDueDate);
          dobInfo.push('<span class="unborn unbornDisplay" title="Unborn"></span><span class="hidden">Unborn</span>');
          if (isAutoComplete) {
            dobInfo.push('<span> Unborn</span>');
          } else {
            dobInfo.push('(Unborn)');
          }

        }
      } else if (resolvedLifeState === 'NOT_CARRIED_TO_TERM') {
        if (isAutoComplete) {
          dobInfo.push('<span class="autocomplete-display"> Not carried </span>');
        } else {
          dobInfo.push('&nbsp; &nbsp;<span> Not carried </span>');
        }

      } else if (resolvedLifeState === 'ALIVE' || resolvedLifeState === 'DECEASED') {
        if (dob) {
          var formatedDob = Y.USPDate.formatDateValue(dob.calculatedDate, dob.fuzzyDateMask);
          if (formatedDob) {
            if (isAutoComplete) {
              dobInfo.push(' Born');
            } else {
              dobInfo.push('<strong> Born</strong>');
            }
            dobInfo.push(formatedDob);
            if (resolvedLifeState === 'DECEASED') {
              dobInfo.push('<span class="deceased autocomplete" title="Deceased"></span><span class="hidden">Deceased</span>');
            }
            if (dobEstimated === true) {
              dobInfo.push('<span class="estimated autocomplete" title="Estimated"></span><span class="hidden">Estimated</span>');
            }
            if (age > 1) {
              if (isAutoComplete) {
                dobInfo.push('<span class="autocomplete">' + ' ' + age + ' years</span>');
              } else {
                dobInfo.push('(' + age + ' years)');
              }
            } else if (age === 1 || age === 0) {
              if (isAutoComplete) {
                dobInfo.push('<span class="autocomplete">' + ' ' + age + ' year</span>');
              } else {
                dobInfo.push('(' + age + ' year)');
              }
            }
          }
        } else {
          if (resolvedLifeState === "DECEASED") {
            dobInfo.push('<span class="deceased autocomplete" title="Deceased"></span><span class="hidden">Deceased</span>');
          }
        }
      }
      return dobInfo;
    },
    lifeStateFormatter: function(o) {

      var value = o.value || o.data,
        dobEstimated = value.dateOfBirthEstimated,
        dob = value.dateOfBirth,
        age = value.age,
        dobInfo = [],
        formatedDueDate, formatedDob;
      var resolvedLifeState = UP.GetImpliedLifeState(value.lifeState);
      if (resolvedLifeState === "UNBORN") {
        formatedDueDate = Y.USPDate.formatDateValue(value.dueDate, null);
        if (formatedDueDate) {
          dobInfo.push(formatedDueDate);
          dobInfo.push('<span class="unborn" title="Unborn"></span><span class="hidden">Unborn</span>');
          dobInfo.push('(Unborn)');
        }
      } else if (resolvedLifeState === "NOT_CARRIED_TO_TERM") {
        dobInfo.push('<span>Not carried</span>');
      } else if (resolvedLifeState === "ALIVE" || resolvedLifeState === null || resolvedLifeState === "DECEASED") {
        if (dob) {

          formatedDob = Y.USPDate.formatDateValue(dob.calculatedDate, dob.fuzzyDateMask);

          if (formatedDob) {
            dobInfo.push(formatedDob);
            if (resolvedLifeState === "DECEASED") {
              dobInfo.push('<span class="deceased" title="Deceased"></span><span class="hidden">Deceased</span>');
            }
            if (dobEstimated === true) {
              dobInfo.push('<span class="estimated" title="Estimated"></span><span class="hidden">Estimated</span>');
            }
            if (age > 1) {
              dobInfo.push('(' + age + ' years)');
            } else if (age === 1 || age === 0) {
              dobInfo.push('(' + age + ' year)');
            }
          }
        } else {
          if (resolvedLifeState === "DECEASED") {
            dobInfo.push('<span class="deceased" title="Deceased"></span><span class="hidden">Deceased</span>');
          }
        }
      }
      return dobInfo.join(' ');
    },
    formattersWarnings: function(o) {
      var numberOfWarnings = o.record.get("numberOfWarnings");
      var name = Y.Escape.html(o.record.get("personVO!name"));
      if (name.length > 30) {
        name = name.slice(0, 30) + '...';
      }
      if (numberOfWarnings === 0) {
        return name;
      } else if (numberOfWarnings === 1) {
        return name + ' <span class="label warning label-small" title="This person has 1 warning" aria-label="This person has 1 warning"> <i class="fa fa-exclamation-triangle"></i> 1</span>';
      } else {
        return name + ' <span class="label warning label-small" title="This person has ' + numberOfWarnings + ' warnings" aria-label="This person has ' + numberOfWarnings + ' warnings"> <i class="fa fa-exclamation-triangle"></i> ' + numberOfWarnings + '</span>';
      }

    },


    estimatedDate: function(o) {

      var template;

      if (!o.record.get(o.column.estimatedKey || 'estimated' + o.column.key)) {
        template = '{date}';
      } else {
        template = template_ESTIMATED_DATE;
      }

      return L.sub(template, {
        date: Y.usp.ColumnFormatters.date({
          value: o.record.get(o.column.key)
        })
      });

    },


    /**
     * Note:
     * Results using nested vo's will need a local formatter that then calls this. 
     */
    _nameID: function(o) {
      var identifier = o.record.get('personIdentifier') ||
        o.record.get('organisationIdentifier') ||
        o.record.get('groupIdentifier') ||
        o.record.get('$identifier') || o.$identifier;

      return (o.value = L.sub('{name} ({identifier})', {
        name: o.record.get('name') || o.record.get('$name') || o.$name,
        identifier: identifier,
      })) && o.value;
    },

    linkablePopOverTeamName: function(o) {
      o.value = Y.app.ColumnFormatters._nameID(o);
      //passing true as last parameter will override the default escape functionality. We assume the data is pre-escaped
      return Y.app.ColumnFormatters.linkable(o, true);
    },
    personName: function(o) {

      var count, template, title, warnings, idAttrs, name;

      count = o.record.get("numberOfWarnings");
      name = o.record.get('name');
      if (typeof name==="undefined") {
        name = o.record.get("accessedPerson.name");
        if (typeof(name) !== "undefined") {
          name = name + ' (' + o.data.accessedPerson.personIdentifier + ')';
        } else {
          name = Y.usp.ColumnFormatters.truncate(o);
        }
      } else {
        idAttrs = o.record.getAttrs(['personPersonId', 'personIdentifier']);

        name = name + ' (' + (idAttrs.personPersonId || idAttrs.personIdentifier) + ')';
      }

      template = o.column.tmpl || Y.Handlebars.templates.personNameWithWarnings;
      title = 'This person has {count} warning{plural}';


      warnings = Y.app.utils.person.GetWarningsArray(o.record.toJSON());


      // allows us to use the truncate formatter on our name field later
      o.column.maxLength = o.column.maxLength || DEFAULT_PERSON_NAME_MAXLENGTH;
      o.column.allowHTML = true;

      if (!count) {
        title = '';
      } else if (count > 0) {
        title = Y.Lang.sub(title, {
          count: count,
          plural: (count > 1) ? 's' : ''
        });
      }


      return template({
        modelData: {
          count: count,
          name: name,
          title: title,
          warnings: warnings
        }
      });
    },



    linkable: function(o, suppressEsc) {

      var title = o.column.title || '',
        url = o.column.url || '',
        className = o.column.className || '',
        clazzName = o.column.clazz || '';

      o.column.allowHTML = true;
      return L.sub(template_LINKABLE, {
        title: E.html(L.sub(title, o.record.getAttrs())),
        url: url ? L.sub(url, o.record.getAttrs()) : '#none',
        linkClassName: className ? className : 'switch-focus',
        value: suppressEsc ? o.value : E.html(o.value),
        clazzName: clazzName
      });
    },


    linkableWithWarnings: function(o) {
      o.value = Y.app.ColumnFormatters.formattersWarnings(o);
      //passing true as last parameter will override the default escape functionality. We assume the data is pre-escaped
      return Y.app.ColumnFormatters.linkable(o, true);
    },


    linkablePopOverWarnings: function(o) {
      o.value = Y.app.ColumnFormatters.personName(o);
      //passing true as last parameter will override the default escape functionality. We assume the data is pre-escaped
      return Y.app.ColumnFormatters.linkable(o, true);
    },

    formatterNHSCHINumber: function(o) {
      if(o.record.get('nhsNumber') !== null && o.record.get('chiNumber') !== null) {
        o.column.allowHTML = true;
        return L.sub(template_NHS_CHI_NUMBERS, {
          nhs: o.record.get('nhsNumber'),
          chi: o.record.get('chiNumber')
        });
      } else {
        return o.record.get(o.column.key);
      }
    },

    formattersPersonName: function(o) {
      var numberOfWarnings = o.record.get("numberOfWarnings");
      var name = Y.Escape.html(o.record.get('name'));
      var truncatedText;
      if (numberOfWarnings === 0) {

        if (name.length > 30) {
          truncatedText = name.slice(0, 30) + '...';
          return truncatedText;
        } else {
          return name;
        }

      } else if (numberOfWarnings === 1) {
        if (name.length > 30) {
          truncatedText = name.slice(0, 30) + '...';
          return truncatedText + ' <span class="label warning label-small" title="This person has 1 warning" aria-label="This person has 1 warning"> <i class="fa fa-exclamation-triangle"></i> 1</span>';
        } else {
          return name + ' <span class="label warning label-small" title="This person has 1 warning" aria-label="This person has 1 warning"> <i class="fa fa-exclamation-triangle"></i> 1</span>';
        }
      } else {
        if (name.length > 30) {
          truncatedText = name.slice(0, 30) + '...';
          return truncatedText + ' <span class="label warning label-small" title="This person has ' + numberOfWarnings + ' warnings" aria-label="This person has ' + numberOfWarnings + ' warnings"> <i class="fa fa-exclamation-triangle"></i> ' + numberOfWarnings + '</span>';
        } else {
          return name + ' <span class="label warning label-small" title="This person has ' + numberOfWarnings + ' warnings" aria-label="This person has ' + numberOfWarnings + ' warnings"> <i class="fa fa-exclamation-triangle"></i> ' + numberOfWarnings + '</span>';
        }
      }
    },

    /**
     * Formatters
     * ----------
     * formatMembers
     * This returns members in a group e.g., 3 members example (per-id), example (per-id) and 1 other
     */
    formatMembers: function(o) {
      var data = o.record.toJSON();
      var membersArray = [],
        membersCount = data.membersCount,
        membersOnHover = [],
        moreMembersCount = 0,
        moreMembers = false,
        membersDetails, isSecuredGroup = data.isGroupSecured,
        memberTitle = 'member{plural}',
        OtherMemberTitle = 'Other member{plural}',
        otherTitle = 'other{plural}',
        securedGroupTitle = 'Group has restricted access',
        noMembersLabel = 'There are no members to display',
        template = Y.Handlebars.templates.groupMembersFormatter;
      if (data.members && data.members !== null && data.members.length > 0) {
        A.each(data.members, function(member) {
          var person = member.personSummary || member;

          membersArray.push(person.name + ' (' + person.personIdentifier + ')');
        });
      }

      if (membersCount > 2 && membersArray.length > 0) {
        if (membersCount > 3) {
          var splicingValue = membersCount - 2;
          membersOnHover = membersArray.splice(2, splicingValue);
        } else {
          membersOnHover = membersArray.splice(2, 1);
        }

        moreMembersCount = membersCount - 2;
        moreMembers = true;
      }

      if (!membersCount) {
        memberTitle = '';

      } else if (membersCount > 0) {
        if (!moreMembersCount) {
          OtherMemberTitle = '';
          otherTitle = '';
        }
        memberTitle = Y.Lang.sub(memberTitle, {
          plural: (membersCount > 1) ? 's' : ''
        });
        if (moreMembersCount > 0) {
          OtherMemberTitle = Y.Lang.sub(OtherMemberTitle, {
            plural: (moreMembersCount > 1) ? 's' : ''
          });
          otherTitle = Y.Lang.sub(otherTitle, {
            plural: (moreMembersCount > 1) ? 's' : ''
          });
        }
      }
      membersDetails = membersArray.join(', ');

      return template({
        modelData: {
          count: membersCount,
          members: membersArray,
          membersDetails: membersDetails,
          membersOnHover: membersOnHover,
          moreMembers: moreMembers,
          moreMembersCount: moreMembersCount,
          memberTitle: memberTitle,
          title: OtherMemberTitle,
          otherTitle: otherTitle,
          securedGroup: isSecuredGroup,
          securedGroupTitle: securedGroupTitle,
          noMembersLabel: noMembersLabel
        }
      });
    },
    /**
     * Formatters
     * ----------
     * formatNameAndIdentifier
     * This returns Name (Identifier)
     */
    formatNameAndIdentifier: function(o) {
      var nameAndId_TEMPLATE = '{name} ({identifier})',
        name = o.record.get('groupVO!name') || o.record.get('name'),
        identifierValue = o.record.get('groupVO!groupIdentifier') || o.record.get('groupIdentifier');
      return L.sub(nameAndId_TEMPLATE, {
        name: name || L.sub(name, o.record.getAttrs()),
        identifier: identifierValue,
      });
    },
    dateToTime: function(o) {
      return new Date(o.value).toUTCString().match(/(\d\d:\d\d)/)[0];
    },
    carerApprovalAgeRange: function(o) {
      var ageRange = o.record.get('lowerAgePermitted') + ' - ' + o.record.get('upperAgePermitted');
      return ageRange;
    },
    careTypeDescription: function(o) {
      var caretype = o.record.toJSON().typeOfCareAssignmentVO.codedEntryVO.name;

      return caretype;
    },
    summaryAddressFormat: function(o) {
      var record=o.record, address, location, roomDescription, floorDescription, rawAddress;

      rawAddress=record.get('address')||(record.get('addresses')||[])[0]||{};
      location=rawAddress.location;
      roomDescription = rawAddress.roomDescription? 'Room ' + rawAddress.roomDescription +', ':'';
      floorDescription = rawAddress.floorDescription ? 'Floor ' + rawAddress.floorDescription +', ':'';

      address = '';
      if(location){
        address = roomDescription + floorDescription + location.location;
      } else{
        var addresses=record.get('addresses');
        if(addresses && addresses[0] && addresses[0].unknownLocationType){
          address = Y.uspCategory.address.unknownLocation.category.codedEntries[addresses[0].unknownLocationType].name;
        }
      }
      address = address || 'None';
      var addressHTML = '<strong>Address</strong><span class="address"> {address}</span>';

      if (rawAddress.doNotDisclose) {
        addressHTML = '<strong>Address</strong> ' + address_template_DND + '<span class="address"> {address}</span>';
      }

      return ('<div>' + L.sub(addressHTML, {
        address: E.html(address)
      }) + '</div>');
    },
    fuzzyDateLinkForReadSummaryFormatter: function(o) {
        o.value = Y.usp.ColumnFormatters.fuzzyDate(o);
        if (o.data._securityMetaData) {
            if (o.column.permissions.canView && o.data._securityMetaData.readSummary && !o.data._securityMetaData.readDetail) {
                return o.value;
            }
        }
        o.column.clazz = 'view';
        return Formatters.linkable(o, o.column.permissions.canView);
    }
  };

  Y.namespace('app').ColumnFormatters = Formatters;

}, '0.0.1', {
  requires: ['yui-base',
               'escape',
               'app-utils-person',
               'usp-date',
               'results-formatters',
               'handlebars',
               'handlebars-helpers',
               'handlebars-person-templates',
               'handlebars-group-templates',
               'categories-address-component-UnknownLocation']
});