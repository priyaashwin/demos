/**
  Attachment based formatters
 @module attachment-results-formatters
 @since 1.0.0
 */
YUI.add('attachment-results-formatters', function(Y) {
  var L = Y.Lang;

  var ATTACHMENT_LINK = '<a href="#none" title="Manage Files" data-menu-action="manageFiles"><span><i class="fa fa-paperclip fa-icon-large"></i>{count}</span></a>',
    ATTACHMENT_COUNT = '<span><i class="fa fa-paperclip fa-icon-large"></i>{count}</span>';

  var Formatters = {

    attachment: function(o) {
      var template = ATTACHMENT_LINK;

      if (o.data._securityMetaData) {
        if (!o.data._securityMetaData.readDetail) {
          template = ATTACHMENT_COUNT;
        }
      }

      if (o.value === 0) {
        template = ATTACHMENT_COUNT;
      }

      return L.sub(template, {
        count: o.value
      });
    }
  };

  Y.namespace('app.attachment').ColumnFormatters = Formatters;

}, '0.0.1', {
  requires: ['yui-base']
});