//Page wide objects
Y.use('info-message' , function(Y){
    new Y.usp.InfoMessage({
        srcNode:'#headerMessage'
    }).render();    
});

Y.use('info-pop', function(Y){
    new Y.usp.InfoPop({
        selector:'.pop-up-data, .tool-tip',
        zIndex:9999999
     }).render();
});