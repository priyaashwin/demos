<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="version" ignore="false" />
<c:set var="verString" value="${version}" />

<c:if test="${(version == 'VERSION UNDEFINED') or (verString == '-1697335352')}">
	<c:set var="verString" value="${sessionScope.pseudoID}" />
</c:if>



<%-- YUI categories now  only served when authenticated, in order to satisfy OLM-20225 --%>
<sec:authorize access="isAuthenticated()">

/* ENUMERATION CONFIGURATION */

/*This is for matching searched query with person name results*/
"person-name-matcher-highlighter":'<s:url value="/js/search/person-name-matcher-highlighter.js?ver=${verString}" />',
"organisation-name-matcher-highlighter":'<s:url value="/js/search/organisation-name-matcher-highlighter.js?ver=${verString}" />',
/* Person warnings category is retrieved here
   This was not automatically done as the PersonWithWarningVO does not have any variable tagged with person warning category
   This may not be required in future if framework download's category for the indirect category reference
 */
"categories-person-component-Warnings":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.warnings/?sid=${verString}"/>',
"secured-categories-person-component-Warnings":'<s:url value="/rest/secured/yui-modules/category/com.olmgroup.usp.components.person.category.warnings/?sid=${verString}"/>',
"categories-person-component-Categoryofneedclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.categoryofneedclassifier/?sid=${verString}"/>',
"categories-person-component-Disabilityclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.disabilityclassifier/?sid=${verString}"/>',
"categories-person-component-Categoryofabuseclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.categoryofabuseclassifier/?sid=${verString}"/>',
"categories-person-component-Legalstatuschildrenclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.legalstatuschildrenclassifier/?sid=${verString}"/>',
"categories-person-component-Cinasylumstatusclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.cinasylumstatusclassifier/?sid=${verString}"/>',
"categories-person-component-Parentingcapacityissuesclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.parentingcapacityissuesclassifier/?sid=${verString}"/>',
"categories-person-component-Communicationneedclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.communicationneedclassifier/?sid=${verString}"/>',
"categories-person-component-Legalstatusmhclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.legalstatusmhclassifier/?sid=${verString}"/>',
"categories-person-component-Lacimmunisationsclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.lacimmunisationsclassifier/?sid=${verString}"/>',
"categories-person-component-Lacemploymentclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.lacemploymentclassifier/?sid=${verString}"/>',
"categories-person-component-Medicalconditionsclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.medicalconditionsclassifier/?sid=${verString}"/>',
"categories-person-component-Senclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.senclassifier/?sid=${verString}"/>',
"categories-person-component-Educationclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.educationclassifier/?sid=${verString}"/>',
"categories-person-component-Missingchildclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.missingchildclassifier/?sid=${verString}"/>',
"categories-person-component-Criminalconvictionsclassifier":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.criminalconvictionsclassifier/?sid=${verString}"/>',
"categories-person-component-Languages":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.languages/?sid=${verString}"/>',
"categories-person-component-NonVerbalLanguages":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.nonVerbalLanguages/?sid=${verString}"/>',
"categories-person-component-ProficiencyInOfficialLanguage":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.person.category.proficiencyInOfficialLanguage/?sid=${verString}"/>',
"categories-childlookedafter-component-TypeOfCare":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.childlookedafter.category.typeOfCare/?sid=${verString}"/>',
"categories-childlookedafter-component-TypeOfDisability":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.childlookedafter.category.typeOfDisability/?sid=${verString}"/>',
"categories-childlookedafter-component-Training":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.childlookedafter.category.training/?sid=${verString}"/>',
"address-category-type":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.address.category.type/?sid=${verString}"/>',
"address-category-usage":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.address.category.usage/?sid=${verString}"/>',
"address-category-countyCode":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.address.category.countyCode/?sid=${verString}"/>',
"categories-core-component-Source":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.facets.core.category.source/?sid=${verString}"/>',
"categories-core-component-SourceOrganisation":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.facets.core.category.sourceOrganisation/?sid=${verString}"/>',
"categories-casenote-component-EntryTypeSubjectTagging":'<s:url value="/rest/yui-modules/category/com.olmgroup.usp.components.casenote.category.entryTypeSubjectTagging/?sid=${verString}"/>',

</sec:authorize>
