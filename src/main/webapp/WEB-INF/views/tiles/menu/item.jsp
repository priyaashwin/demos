<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="label" ignore="false"/>
<tilesx:useAttribute name="path" ignore="false"/>
<tilesx:useAttribute name="icon" ignore="true"/>
<tilesx:useAttribute name="parameters" ignore="true"/>
<tilesx:useAttribute name="itemClass" ignore="true"/>
<tilesx:useAttribute name="title" ignore="true"/>
<tilesx:useAttribute name="newTab" ignore="true"/>
<tilesx:useAttribute name="contextualised" ignore="true" />

<tilesx:useAttribute name="secured" ignore="true"/>
<tilesx:useAttribute name="domainObject" ignore="true"/>
<tilesx:useAttribute name="permission" ignore="true"/>

<c:set var="domainObj" value="${domainObject}" />
<c:set var="perm" value="${permission}" />
<c:set var="sec" value="${secured}" />

<c:set var="accessAllowed" value="true" />

<c:set var="selectedLabel"><t:insertAttribute ignore="true" name="selected"/></c:set>
<c:set var="selectedSubLabel"><t:insertAttribute ignore="true" name="subSelected"/></c:set>
<c:if test="${selectedLabel eq label }">
	<c:set var="selectedStyle" scope="page" value="selected"/>
</c:if>
<c:if test="${selectedSubLabel eq label }">
	<c:set var="selectedSubStyle" scope="page" value="sub-selected"/>
</c:if>
<s:url value="${path }" var="itemUrl">
	<c:if test="${not empty  parameters}">
		<c:forEach items="${parameters}" var="parameter">
			<c:set var="bn" value="${parameter.objectName}"/>
			<c:set var="bp" value="${parameter.objectProperty}"/>
			<c:set var="ob" value="${requestScope[bn] }"/>

			<c:if test="${not empty ob }">
				<s:param name="${parameter.parameterName}" value="${ob[bp] }"/>
			</c:if>
			<c:remove var="ob"/>
			<c:remove var="bp"/>
			<c:remove var="bn"/>
		</c:forEach>
	</c:if>
</s:url>

<c:if test="${sec eq 'true'}">
	<sec:authorize access="hasPermission(null, '${domainObj}', '${perm}')" var="accessAllowed"/>
</c:if>

<c:if test="${accessAllowed}">
  <c:if test="${not empty contextualised}">
    <c:set var="accessAllowed" value="${contextualised}" />
  </c:if>
</c:if>

<c:if test="${accessAllowed}">
	<li class='<c:out value="${selectedStyle}"/> <c:out value="${selectedSubStyle}"/> <c:out value="${itemClass}"/>'><a href='<c:out value="${itemUrl }" escapeXml="false"/>'  title='<c:if test="${not empty title }"> <c:out value="${title}"/> </c:if>' target='<c:if test="${newTab eq 'true'}">_blank</c:if>' data-target= '<c:if test="${newTab eq 'true'}">true</c:if>' class="usp-fx-all"><c:if test="${not empty icon }"><i class='<c:out value="${icon }"/>'> <s:message code="${label}" text="${label }"/> </i></c:if><c:if test="${empty icon }"><s:message code="${label}" text="${label }"/></c:if></a></li>
</c:if>
