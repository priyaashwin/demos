<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="items" ignore="true"/>
<tilesx:useAttribute name="label" ignore="true"/>
<tilesx:useAttribute name="selected" ignore="true"/>
<tilesx:useAttribute name="subSelected" ignore="true"/>


<tilesx:useAttribute name="secured" ignore="true"/>
<tilesx:useAttribute name="domainObject" ignore="true"/>
<tilesx:useAttribute name="permission" ignore="true"/>

<c:set var="domainObj" value="${domainObject}" />
<c:set var="perm" value="${permission}" />
<c:set var="sec" value="${secured}" />

<c:set var="accessAllowed" value="true" />

	<c:if test="${not empty customItemDomainObject}">
			<c:set var="domainObj" value="${customItemDomainObject}" />
	</c:if>
 
	<c:if test="${not empty customItemPermission}">
			<c:set var="perm" value="${customItemPermission}" />
			<c:set var="sec"  value="true" />
	</c:if>

<c:if test="${sec eq 'true'}">
	<sec:authorize access="hasPermission(null, '${domainObj}', '${perm}')" var="accessAllowed"/>	
</c:if>
<c:if test="${accessAllowed}">

	<div class="menu-header">
		<h2 tabindex="0"><c:out value="${label }"/></h2>
	</div>
	<%-- insert menu items if not empty --%>
	<c:if test="${not empty items}">	
		<ul class="pure-group main-menu">
			<c:forEach items="${items}" var="item" varStatus="tIndex">
				<t:insertDefinition name="${item}" flush="false">
					<c:if test="${not empty selected }">
						<t:putAttribute name="selected" value="${selected }"/>
					</c:if>	
					<c:if test="${not empty subSelected }">
						<t:putAttribute name="subSelected" value="${subSelected }"/>
					</c:if>	
				</t:insertDefinition>
			 </c:forEach>
		 </ul>
	</c:if>
</c:if>	