<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import='com.olmgroup.usp.facets.search.SortSelection' %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<s:eval expression="@jacksonObjectMapper" var="objectMapper" />
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<tilesx:useAttribute name="menuSections" ignore="true"/>
<tilesx:useAttribute name="selected" ignore="true"/>
<tilesx:useAttribute name="subSelected" ignore="true"/>
<tilesx:useAttribute name="customMenuItemService" ignore="true"/>


<%-- insert menu sections if not empty --%>
<c:if test="${not empty menuSections}">

	<a href="#menu" title='<s:message code="slidemenu.toggle"/>' class="pure-menu-link slidepanel-reveal" aria-label="<s:message code="slidemenu.toggle"/>">
		<i class="icon-reorder fa fa-bars"></i>
	</a>

	<div id="menu" class="pure-u slidepanel usp-fx-left">
		<div class="nav-accent"></div>
		<a href="#" title='<s:message code="slidemenu.close"/>' class="slidepanel-close close"><s:message code="slidemenu.close"/></a>
		<div id="menuWrap">

			<c:forEach items="${menuSections}" var="menuSection" varStatus="mIndex">
				<t:insertDefinition name="${menuSection}" flush="false">
					<c:if test="${not empty selected }">
						<t:putAttribute name="selected" value="${selected }"/>
					</c:if>
					<c:if test="${not empty subSelected }">
						<t:putAttribute name="subSelected" value="${subSelected }"/>
					</c:if>
				</t:insertDefinition>
			</c:forEach>
			 <c:if test="${not empty customMenuItemService }">
			    <c:set var="pageSize" value="-1"/>
			    <c:set var="pageNumber" value="-1"/>
				  <c:catch var="exception">
				    <%
				         SortSelection sortSelection = new SortSelection().addAscendingOrderedExpression("label");

				         pageContext.setAttribute("sortSelection", sortSelection);
      			%>
				    <c:set var="customMenuItems" value="${customMenuItemService.findAll(pageNumber,pageSize,sortSelection).getResults()}"/>
				  </c:catch>
				 <c:catch var="custom-menu-exception">
					<c:set var="personId" value="${person.id}"/>
					 <c:set var="tokensValues" value="${customMenuItemService.getMenuItemTokensMapBySubjectTypeAndId('PERSON', pageContext.getAttribute('personId'))}"/>
				 </c:catch>
			 </c:if>

				<c:if test="${not empty customMenuItemService && exception == null}">
				    <ul class="pure-group main-menu">
						<c:forEach items="${customMenuItems}" var="custmMenuItem">
							<c:if test="${custmMenuItem.isActive() eq 'true'}">
				     		<t:insertDefinition name="base.menu.page.item" template="/WEB-INF/views/tiles/menu/item.jsp" flush="false">
								 		<t:putAttribute name="domainObject" value="CustomMenuItem"></t:putAttribute>
								 		<c:if test="${not empty custmMenuItem.getCustomMenuPermission()}">
								 			<t:putAttribute name="permission" value="${custmMenuItem.getCustomMenuPermission()}"></t:putAttribute>
								 		</c:if>
								 		<c:if test="${not empty custmMenuItem.getLabel()}">
								 			<t:putAttribute name="label" value="${custmMenuItem.getLabel()}"></t:putAttribute>
								 		</c:if>
								 		<c:if test="${not empty custmMenuItem.getMenuUrl()}">
								 			<t:putAttribute name="path" value="${custmMenuItem.getMenuUrl()}"></t:putAttribute>
								 		</c:if>
								 		<c:if test="${not empty custmMenuItem.getDescription()}">
								 			<t:putAttribute name="title" value="${custmMenuItem.getDescription()}"></t:putAttribute>
								 		</c:if>
										<c:if test="${not empty selected }">
											<t:putAttribute name="selected" value="${selected }"/>
										</c:if>
										<c:if test="${not empty subSelected }">
											<t:putAttribute name="subSelected" value="${subSelected }"/>
										</c:if>
										<c:if test="${not empty custmMenuItem.getShowInPersonContext() }">
											<t:putAttribute name="itemClass" value="nav-customMenu depends-person hidden-nav"></t:putAttribute>
										</c:if>
										<c:if test="${empty custmMenuItem.getShowInPersonContext() }">
											<t:putAttribute name="itemClass" value="nav-customMenu depends-nothing hidden-nav"></t:putAttribute>
										</c:if>
										<t:putAttribute name="secured" value="true"></t:putAttribute>
										<t:putAttribute name="newTab" value="true"></t:putAttribute>
								 </t:insertDefinition>
							</c:if>
				    </c:forEach>
				    </ul>
				</c:if>
		</div>
	</div>

  <script>
    Y.use('usp-current-subject', 'slidepanel', 'yui-base', 'json-parse', 'usp-navigation', function(Y) {
      <c:if test="${not empty securedSubjectContextVO}">
        <%-- if there is a subject in context, make sure the current subject is updated --%>
        Y.currentSubject.setIsSubjectAccessAllowed("${securedSubjectContextVO.subjectContextId}", "${securedSubjectContextVO.subjectContextType}", '${securedSubjectContextVO.isSubjectAccessAllowed}'==='true');
      </c:if>
      <c:if test="${not empty tokensValues}">
        Y.namespace('app').availableTokenData = Y.JSON.parse('${esc:escapeJavaScript(objectMapper.writeValueAsString(pageContext.getAttribute("tokensValues")))}');
      </c:if>
      //start-up the navigation menu
      Y.app.navigationReplace.setUpNavigation(Y.currentSubject.getCurrentSubject());
      });
</script>
</c:if>
