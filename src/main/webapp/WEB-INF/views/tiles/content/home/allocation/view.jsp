<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="hasPermission(null, 'PersonPersonRelationship','PersonPersonRelationship.View')" var="canViewPersonPersonRelationship"/>

<%-- Insert Person ADD Template  must be used in conjunction with the REACT component scripts--%>
<t:insertTemplate template="/WEB-INF/views/tiles/content/common/person/add.jsp" />
<%-- Insert Organisation ADD Template  must be used in conjunction with the REACT component scripts--%>
<t:insertTemplate template="/WEB-INF/views/tiles/content/common/organisation/add.jsp" />
<%-- Insert Team ADD Template  must be used in conjunction with the REACT component scripts--%>
<t:insertTemplate template="/WEB-INF/views/tiles/content/common/team/add.jsp" />
<!-- Insert group entry dialog -->
<t:insertTemplate template="/WEB-INF/views/tiles/content/groups/common/group-dialog.jsp" />  
<c:choose>
  <c:when test="${canViewPersonPersonRelationship eq true}">
  <div class="yui3-app" id="summary-widget-frame"></div>
    <div id="allocationView">			
      <t:insertTemplate template="/WEB-INF/views/tiles/content/home/allocation/sections/results.jsp" />
    </div>
    </c:when>
    <c:otherwise>
      <%-- Insert access denied tile --%>
      <t:insertDefinition name="access.denied" />
  </c:otherwise>
</c:choose>	