<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson"/>
<sec:authorize access="hasPermission(null, 'Group','Group.View')" var="canViewGroup"/>

<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.View')" var="canViewForm"/>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Reject')" var="canRejectForm"/>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Authorise')" var="canAuthoriseForm"/>
<sec:authorize access="hasPermission(null, 'FormInstanceAttachment','FormInstanceAttachment.ADD')" var="canUploadAttachment" />
<sec:authorize access="hasPermission(null, 'FormInstanceAttachment','FormInstanceAttachment.GET')" var="canViewAttachment" />
<sec:authorize access="hasPermission(null, 'FormInstanceAttachment','FormInstanceAttachment.DELETE')" var="canDeleteAttachment" />

<sec:authorize access="hasPermission(null, 'PersonWarningAssignment','PersonWarningAssignment.View')" var="canViewWarning" />
<sec:authorize access="hasPermission(null, 'CodedEntryAssignment','CodedEntryAssignment.Authorise')" var="canAuthoriseWarning" />

<div id="authorisationTabs">
  <div id="submittedFormsResults"></div>
  <div id="warningsResults"></div>
<div>

<s:url var="submittersURL" value="/rest/formInstance/submitters"/>
<s:url var="viewSubjectURL" value="/forms/{subjectType}/{subjectId}/view"/>
<s:url var="formsAwaitingAuthorisationURL" value="/rest/formInstance/submitted?s={sortBy}&pageNumber={page}&pageSize={pageSize}&accessLevel=READ_SUMMARY"/>
<s:url var="submittedFormInstanceTypesURL" value="/rest/formDefinition?&s={sortBy}&submittedOnly=true&pageSize=-1"/>
<s:url var="downloadAttachmentURL" value="/rest/formInstanceAttachment/{id}/content?formInstanceId={formInstanceId}"/>
<s:url var="formEditorURL" value="/forms/load?id={id}"/>
<s:url var="reviewHistoryURL" value="/forms/reviews?id={id}" />

<s:url var="viewWarningsSubjectURL" value="/person?id={subjectId}" />

<script>
Y.use('authorisation-controller-view', 'categories-person-component-Warnings', function(Y) {

  var formPermissions = {
    canView:'${canViewForm}'==='true',
    canViewSubject: '${canViewPerson}'==='true'||'${canViewGroup}'==='true',
    canViewPerson: '${canViewPerson}'==='true',
    canViewGroup: '${canViewGroup}'==='true',
    canAuthorise:'${canAuthoriseForm}'==='true',
    canReject:'${canRejectForm}'==='true',
    canViewAttachment: '${canViewAttachment}' === 'true'
  };

  var attachmentPermissions = {
      canView: '${canViewAttachment}' === 'true'
    }

  var searchConfig = {
    sortBy: [{
        dateSubmitted: 'desc'
    }],
    labels: {
      type:'<s:message code="forms.find.results.type" javaScriptEscape="true"/>',
      formId:'<s:message code="forms.find.results.formId" javaScriptEscape="true"/>',
      submittedDate:'<s:message code="forms.find.results.submittedDate" javaScriptEscape="true"/>',
      submitterName:'<s:message code="forms.find.results.submitterName" javaScriptEscape="true"/>',
      subject:'<s:message code="forms.find.results.subject" javaScriptEscape="true"/>',
      lastChangedBy:'<s:message code="forms.find.results.lastChangedBy" javaScriptEscape="true"/>',
      attachmentCount: '<s:message code="forms.find.results.files" javaScriptEscape="true"/>',
      actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
      viewSubject:'<s:message code="forms.find.results.viewSubject" javaScriptEscape="true"/>',
      viewForm:'<s:message code="forms.find.results.viewForm" javaScriptEscape="true"/>',
      viewFormTitle:'<s:message code="forms.find.results.viewFormTitle" javaScriptEscape="true"/>',
      rejectForm:'<s:message code="forms.find.results.rejectForm" javaScriptEscape="true"/>',
      rejectFormTitle:'<s:message code="forms.find.results.rejectFormTitle" javaScriptEscape="true"/>',
      approveForm:'<s:message code="forms.find.results.approveForm" javaScriptEscape="true"/>',
      approveFormTitle:'<s:message code="forms.find.results.approveFormTitle" javaScriptEscape="true"/>',
      reviewHistory:'<s:message code="forms.find.results.reviewHistory" javaScriptEscape="true"/>',
      reviewHistoryTitle:'<s:message code="forms.find.results.reviewHistoryTitle" javaScriptEscape="true"/>'
    },
    resultsCountNode:Y.one('#submittedFormsResultsCount'),
    noDataMessage:'<s:message code="home.forms.submitted.no.results" javaScriptEscape="true"/>',
    permissions: formPermissions,
    url:'${formsAwaitingAuthorisationURL}'
  };

  var filterContextConfig = {
      labels:{
          filter:'<s:message code="filter.active"/>',
          submitterIds:'<s:message code="forms.find.filter.submitterIds.active" javaScriptEscape="true"/>',
          removeSubmitterIds:'<s:message code="forms.find.filter.submitterIds.remove" javaScriptEscape="true"/>',
          formNames:'<s:message code="forms.find.filter.formNames.active" javaScriptEscape="true"/>',
          removeFormNames:'<s:message code="forms.find.filter.formNames.remove" javaScriptEscape="true"/>',
          submittedDate:'<s:message code="forms.find.filter.submittedDate.active" javaScriptEscape="true"/>',
          removeSubmittedDate:'<s:message code="forms.find.filter.submittedDate.remove" javaScriptEscape="true"/>',
          resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
          resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
      },
      container:'#authorisationFilterContext'
  };

  var filterConfig = {
    labels:{
        filterBy: '<s:message code="forms.find.filter.title" javaScriptEscape="true"/>',
        submitterIds: '<s:message code="forms.find.filter.submitterIdsFilter" javaScriptEscape="true"/>',
        formNames:'<s:message code="forms.find.filter.formNames.active" javaScriptEscape="true"/>',
        submittedDateFrom: '<s:message code="forms.find.filter.submittedDateFrom" javaScriptEscape="true"/>',
        submittedDateTo: '<s:message code="forms.find.filter.submittedDateTo" javaScriptEscape="true"/>',
        dateRangeLinks: '<s:message code="forms.find.filter.dateRangeLinks" javaScriptEscape="true"/>',
        dateRangeLastWeek: '<s:message code="forms.find.filter.dateRangeLastWeek" javaScriptEscape="true"/>',
        dateRangeLastMonth: '<s:message code="forms.find.filter.dateRangeLastMonth" javaScriptEscape="true"/>',
        dateRangeLastYear: '<s:message code="forms.find.filter.dateRangeLastYear" javaScriptEscape="true"/>',
        validation:{
          endBeforeSubmittedDate: '<s:message code="forms.find.filter.endBeforeSubmittedDate" javaScriptEscape="true"/>'
        },
        reset: '<s:message code="filter.reset" javaScriptEscape="true"/>'
    },
    permissions:formPermissions,
    submittersURL:'${submittersURL}',
    submittedFormInstanceTypesURL:'${submittedFormInstanceTypesURL}'
  };

  var attachmentLabels = {
      file: '<s:message code="file.upload.chooseFile" javaScriptEscape="true"/>',
      title: '<s:message code="file.upload.docTitle" javaScriptEscape="true"/>',
      description: '<s:message code="file.upload.docDescription" javaScriptEscape="true"/>',
      source: '<s:message code="common.source" javaScriptEscape="true"/>',
      otherSource: '<s:message code="common.source.other" javaScriptEscape="true"/>',
      sourceOrganisation: '<s:message code="common.sourceOrganisation" javaScriptEscape="true"/>',
      otherSourceOrganisation: '<s:message code="common.sourceOrganisation.other" javaScriptEscape="true"/>',
      sourceMustBeOtherOrNull: '<s:message code="common.sourceMustBeOtherOrNull" javaScriptEscape="true"/>'
    };

    var attachmentSearchConfig={
      sortBy:[{
        lastUploaded: 'desc'
      }],
      labels:{
        title: '<s:message code="forms.form.attachments.find.title" javaScriptEscape="true"/>',
        uploadedBy: '<s:message code="forms.form.attachments.find.uploadedBy" javaScriptEscape="true"/>',
        uploadedOn: '<s:message code="forms.form.attachments.find.uploadedOn" javaScriptEscape="true"/>',
        description: '<s:message code="forms.form.attachments.find.description" javaScriptEscape="true"/>',
        actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
        download: '<s:message code="file.download" javaScriptEscape="true"/>',
        downloadTitle: '<s:message code="file.downloadTitle" javaScriptEscape="true"/>',
        remove: '<s:message code="file.delete" javaScriptEscape="true"/>',
        removeTitle: '<s:message code="file.deleteTitle" javaScriptEscape="true"/>'
      },
      noDataMessage:'<s:message code="forms.form.attachments.find.no.results"/>',
      permissions: attachmentPermissions,
      url:'<s:url value="/rest/formInstance/{id}/attachment?inline=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
    };

    var formsIcon='fa fa-file-text';
    var narrativeDescription= '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';


    var attachmentDialogConfig = {
      views:{
        manageFiles:{
          header: {
            text: '<s:message code="forms.form.attachments.manage.header" javaScriptEscape="true"/>',
            icon: formsIcon
          },
          narrative: {
            summary: '<s:message code="forms.form.attachments.manage.summary" javaScriptEscape="true" />',
            description: narrativeDescription
          },
          config:{
            searchConfig:attachmentSearchConfig
          },
          labels:{}
        }
      }
    };


    var formDialogConfig = {
        views:{
          approveForm:{
            header: {
              text: '<s:message code="forms.form.approve.header" javaScriptEscape="true"/>',
              icon: formsIcon
            },
            narrative: {
              summary: '<s:message code="forms.form.approve.summary" javaScriptEscape="true" />',
              description: narrativeDescription
            },
            labels:{
              comments:'<s:message code="updateReviewFormInstanceVO.approval.comments" javaScriptEscape="true"/>'
            },
            config:{
              url:'<s:url value="/rest/formInstance/{id}/status?action={action}"/>'
            },
            successMessage: '<s:message code="forms.form.approve.success"/>'
          },
          rejectForm:{
            header: {
              text: '<s:message code="forms.form.reject.header" javaScriptEscape="true"/>',
              icon: formsIcon
            },
            narrative: {
              summary: '<s:message code="forms.form.reject.summary" javaScriptEscape="true" />',
              description: narrativeDescription
            },
            labels:{
              comments:'<s:message code="updateReviewFormInstanceVO.rejection.comments" javaScriptEscape="true"/>'
            },
            config:{
              url:'<s:url value="/rest/formInstance/{id}/status?action={action}"/>'
            },
            successMessage: '<s:message code="forms.form.reject.success"/>'
          }
        }
      };

  var formConfig = {
        container: '#submittedFormsResults',
        searchConfig: searchConfig,
        filterConfig: filterConfig,
        filterContextConfig: filterContextConfig,
        permissions: formPermissions,
        reviewHistoryURL:'<s:url value="/forms/reviews?id={id}" />',
        attachmentDialogConfig: attachmentDialogConfig,
        formDialogConfig: formDialogConfig,
        downloadAttachmentURL:'${downloadAttachmentURL}',
        viewSubjectURL:'${viewSubjectURL}',
        formEditorURL:'${formEditorURL}',
        reviewHistoryURL:'${reviewHistoryURL}'
      };

  var warningPermissions = {
    canView:'${canViewWarning}'==='true',
    canViewPerson: '${canViewPerson}'==='true',
    canAuthorise:'${canAuthoriseWarning}'==='true'
  };

  var warningSearchConfig = {
      sortBy: [{
          startDate: 'desc'
      }],
      labels: {
        actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
        viewSubject:'<s:message code="person.warning.authorise.results.viewSubject" javaScriptEscape="true"/>',
        warningCategory: '<s:message code="person.warning.authorise.results.warningCategory" javaScriptEscape="true"/>',
        subject: '<s:message code="person.warning.authorise.results.subject" javaScriptEscape="true"/>',
        startDate: '<s:message code="person.warning.authorise.results.startDate" javaScriptEscape="true"/>',
        submittedDate: '<s:message code="person.warning.authorise.results.submittedDate" javaScriptEscape="true"/>',
        submitterName: '<s:message code="person.warning.authorise.results.submitter" javaScriptEscape="true"/>',
        viewWarningTitle: '<s:message code="person.warning.authorise.results.viewWarningTitle" javaScriptEscape="true"/>',
        viewWarning: '<s:message code="person.warning.authorise.results.viewWarning" javaScriptEscape="true"/>',
        rejectWarningTitle: '<s:message code="person.warning.authorise.results.rejectWarningTitle" javaScriptEscape="true"/>',
        rejectWarning: '<s:message code="person.warning.authorise.results.rejectWarning" javaScriptEscape="true"/>',
        approveWarningTitle: '<s:message code="person.warning.authorise.results.approveWarningTitle" javaScriptEscape="true"/>',
        approveWarning: '<s:message code="person.warning.authorise.results.approveWarning" javaScriptEscape="true"/>'
      },
      resultsCountNode:Y.one('#warningsResultsCount'),
      noDataMessage:'<s:message code="person.warning.authorise.results.noData" javaScriptEscape="true"/>',
      permissions: warningPermissions,
      url:'<s:url value="/rest/personWarningAssignment/pendingForTeam?pageNumber={page}&pageSize={pageSize}&s={sortBy}&teamId=${currentUserTeamId}&accessLevel=WRITE" />'
  };

  var warningsIcon='fa fa-exclamation-triangle';

  var warningDialogConfig = {
      views:{
        viewWarning: {
          header: {
            text: '<s:message code="person.warning.authorise.dialog.view.header" javaScriptEscape="true"/>',
            icon: warningsIcon
          },
          narrative: {
            summary: '<s:message code="person.warning.authorise.dialog.view.summary" javaScriptEscape="true"/>',
            description: narrativeDescription
          },
          labels: {
            warningCategory: '<s:message code="person.warningCategory" javaScriptEscape="true"/>',
            warningNote: '<s:message code="person.warningNote" javaScriptEscape="true"/>',
            warningStartDate: '<s:message code="person.warning.startDate" javaScriptEscape="true"/>',
            warningEndDate: '<s:message code="person.warning.endDate" javaScriptEscape="true"/>'
          },
        },
        approveWarning: {
          header: {
            text: '<s:message code="person.warning.authorise.dialog.approve.header" javaScriptEscape="true"/>',
            icon: warningsIcon
          },
          narrative: {
            summary: '<s:message code="person.warning.authorise.dialog.approve.summary" javaScriptEscape="true"/>',
            description: narrativeDescription
          },
          labels: {
            message: '<s:message code="person.warning.authorise.dialog.approve.message" javaScriptEscape="true"/>'
          },
          config: {
            url:'<s:url value="/rest/codedEntryAssignment/{id}/status?action=authorised"/>'
          },
          successMessage: '<s:message code="person.warning.authorise.dialog.approve.successMessage" javaScriptEscape="true"/>'
        },
        rejectWarning: {
          header: {
            text: '<s:message code="person.warning.authorise.dialog.reject.header" javaScriptEscape="true"/>',
            icon: warningsIcon
          },
          narrative: {
            summary: '<s:message code="person.warning.authorise.dialog.reject.summary" javaScriptEscape="true"/>',
            description: narrativeDescription
          },
          labels: {
            message: '<s:message code="person.warning.authorise.dialog.reject.message" javaScriptEscape="true"/>'
          },
          config: {
            url:'<s:url value="/rest/codedEntryAssignment/{id}/status?action=rejectedAuthorisation"/>'
          },
          successMessage: '<s:message code="person.warning.authorise.dialog.reject.successMessage" javaScriptEscape="true"/>'
        }
      }
  };

  var warningsFilterContextConfig = {
      labels:{
          filter:'<s:message code="filter.active"/>',
          warningCodes:'<s:message code="person.warning.authorise.filter.types.active" javaScriptEscape="true"/>',
          removeTypes:'<s:message code="person.warning.authorise.filter.types.remove" javaScriptEscape="true"/>',
          resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
          resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
      },
      container:'#authorisationFilterContext'
  };

  var warningsFilterConfig = {
	sortBy: [{
	    startDate: 'desc'
	}],	  
    labels:{
        filterBy: '<s:message code="person.warning.authorise.filter.title" javaScriptEscape="true"/>',
        types: '<s:message code="person.warning.authorise.filter.typesFilter" javaScriptEscape="true"/>',
        reset: '<s:message code="filter.reset" javaScriptEscape="true"/>'
    },
    permissions: warningPermissions,
    warningCodesURL: '<s:url value="/rest/personWarningAssignment/pendingForTeam?pageNumber=1&pageSize=-1&teamId=${currentUserTeamId}&accessLevel=WRITE" />'
  };

  var warningConfig = {
      container: "#warningsResults",
      permissions: warningPermissions,
      searchConfig: warningSearchConfig,
      filterConfig: warningsFilterConfig,
      filterContextConfig: warningsFilterContextConfig,
      viewSubjectURL: '${viewWarningsSubjectURL}',
      warningDialogConfig: warningDialogConfig
  };

  var tabPermissions={
      canAuthoriseForm:'${canAuthoriseForm}'==='true',
      canViewForm:'${canViewForm}'==='true',
      canAuthoriseWarning:'${canAuthoriseWarning}'==='true',
      canViewWarning:'${canViewWarning}'==='true'
  };
  
  var config = {
      container: '#authorisationTabs',
      root:'<s:url value="/home/authorisation"/>',
      permissions: tabPermissions,
      tabs:{
        forms:{
          label: '<s:message code="home.authorisation.tabs.forms" javaScriptEscape="true"/>',
          config: formConfig
        },
        warnings:{
          label: '<s:message code="home.authorisation.tabs.warnings" javaScriptEscape="true"/>',
          config: warningConfig
        }
      }
  };

  var authorisationView = new Y.app.home.authorisation.AuthorisationControllerView(config).render();
});
</script>
