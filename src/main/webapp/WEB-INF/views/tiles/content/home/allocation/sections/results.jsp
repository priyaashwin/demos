<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson" />
<sec:authentication property="principal.securityProfile.securityTeam.teamId" var="teamId" />
<sec:authorize access="hasPermission(null,'SecuredPersonContext.GET','PersonWarning.Add')" var="canAddWarning" />
<sec:authorize access="hasPermission(null,'SecuredPersonContext.GET','MyTeamAllocationTab.View')" var="canViewTeamTab" />
<sec:authorize access="hasPermission(null,'SecuredPersonContext.GET','MyColleaguesAllocationTab.View')" var="canViewColleaguesTab" />
<sec:authorize access="hasPermission(null,'SecuredPersonContext.GET','PersonWarning.Update')" var="canUpdateWarning" />

<s:url var="allocationUrl" value="/rest/person/{userId}/accessPersonTarget?s={sortBy}&pageNumber={page}&pageSize={pageSize}">
    <s:eval expression="@propertyFactory.getProperty('usp.facet.googleMapLinkURL')" var="googleMapLinkURL" scope="page" />
    <s:param name="sortBy" value="[{\"accessDate\":\"desc\"}]" />
</s:url>
<s:url var="professionalRoleUrl" value="/rest/person/{userId}/personRelationship/professionalClients?s={sortBy}&teamId={teamId}&nameOrId={nameOrId}&gender={gender}&minAge={minAge}&maxAge={maxAge}&postCode={postCode}&houseStreetTown={houseStreetTown}&pageNumber={page}&pageSize={pageSize}&temporalStatusFilter=ACTIVE_OR_INACTIVE_PENDING">
    <s:param name="sortBy" value="[{\"surname\":\"asc\"},{\"forename\":\"asc\"}]" />
</s:url>
<s:url var="teamProfessionalClientsUrl" value="/rest/team/{teamId}/personTeamRelationship/teamClients?s={sortBy}&nameOrId={nameOrId}&gender={gender}&minAge={minAge}&maxAge={maxAge}&postCode={postCode}&houseStreetTown={houseStreetTown}&pageNumber={page}&pageSize={pageSize}">
    <s:param name="sortBy" value="[{\"surname\":\"asc\"},{\"forename\":\"asc\"}]" />
</s:url>
<s:url var="colleaguesProfessionalClientsUrl" value="/rest/team/{teamId}/personTeamRelationship/teamProfessionalClients?s={sortBy}&userId={userId}&nameOrId={nameOrId}&minAge={minAge}&maxAge={maxAge}&gender={gender}&postCode={postCode}&houseStreetTown={houseStreetTown}&selectedColleaguePersonIds={selectedColleaguePersonIds}&selectedTeamIds={selectedTeamIds}&pageNumber={page}&pageSize={pageSize}&temporalStatusFilter=ACTIVE_OR_INACTIVE_PENDING">
    <s:param name="sortBy" value="[{\"surname\":\"asc\"},{\"forename\":\"asc\"}]" />
</s:url>

<div id="allocationResultsContainer"></div>

<script>
    Y.use('allocation-controller-view', function (Y) {
        var currentUserId = '${esc:escapeJavaScript(currentUser.id)}';
        var currentTeamId = '${esc:escapeJavaScript(teamId)}';
        var permissions = {
            canAddWarning: '${canAddWarning}' === 'true',
            canUpdateWarning: '${canUpdateWarning}' === 'true',
            canViewTeamsTab: '${canViewTeamTab}' === 'true',
            canViewColleaguesTab: '${canViewColleaguesTab}' === 'true'
        };
        var commonLabels = {
            name: '<s:message code="home.allocations.nameId" javaScriptEscape="true"/>',
            dob: '<s:message code="home.allocations.dobAge" javaScriptEscape="true"/>',
            dateAccessed: '<s:message code="home.allocations.dateAccessed" javaScriptEscape="true"/>',
            contactNumber: '<s:message code="home.allocations.contactNumber" javaScriptEscape="true" />',
            address: '<s:message code="home.allocations.address" javaScriptEscape="true" />',
            gender: '<s:message code="home.allocations.gender" javaScriptEscape="true"/>',
            showPerson: '<s:message code="home.allocations.person.show" javaScriptEscape="true"/>',
            notAuthorised: '<s:message code="home.allocations.notAuthorised" javaScriptEscape="true" />',
            doNotDisclose: '<s:message code="home.allocations.doNotDisclose" javaScriptEscape="true" />',
            accessDenied: '<s:message code="home.allocations.accessDenied" javaScriptEscape="true" />',
            noRecords: '<s:message code="home.allocations.noRecords" javaScriptEscape="true" />'
        };
        var resultsFilterConfig = {
            labels: {
                filterBy: '<s:message code="allocation.find.filter.title" javaScriptEscape="true" />',
                status: '<s:message code="allocation.find.filter.status" javaScriptEscape="true" />',
                addressFilter: '<s:message code="allocation.find.filter.addressFilter" javaScriptEscape="true" />',
                nameOrIdFilter: '<s:message code="allocation.find.filter.nameOrIdFilter" javaScriptEscape="true" />',
                colleagueNameFilter: '<s:message code="allocation.find.filter.colleagueNameFilter" javaScriptEscape="true" />',
                colleaguesWithAllocations: '<s:message code="allocation.find.filter.colleagues" javaScriptEscape="true" />',
                teamsWithAllocations: '<s:message code="allocation.find.filter.teams" javaScriptEscape="true" />',
                genderFilter: '<s:message code="allocation.find.filter.genderFilter" javaScriptEscape="true" />',
                genderMale: '<s:message code="allocation.find.filter.genderMale" javaScriptEscape="true" />',
                genderFemale: '<s:message code="allocation.find.filter.genderFemale" javaScriptEscape="true" />',
                ageFilter: '<s:message code="allocation.find.filter.ageFilter" javaScriptEscape="true" />',
                ageBetween: '<s:message code="allocation.find.filter.ageBetween" javaScriptEscape="true" />',
                ageAnd: '<s:message code="allocation.find.filter.ageAnd" javaScriptEscape="true" />',
                postcode: '<s:message code="allocation.find.filter.postcode" javaScriptEscape="true" />',
                houseStreetTown: '<s:message code="allocation.find.filter.houseStreetTown" javaScriptEscape="true" />',
                genderIndeterminate: '<s:message code="allocation.find.filter.genderIndeterminate" javaScriptEscape="true" />',
                genderUnknown: '<s:message code="allocation.find.filter.genderUnknown" javaScriptEscape="true" />',
                nameOrIdTitle: '<s:message code="allocation.find.filter.nameOrId" javaScriptEscape="true" />',
                resetAll: '<s:message code="allocation.find.filter.resetAll" javaScriptEscape="true" />',
                activeFilters: '<s:message code="allocation.find.filter.activeFilter.title" javaScriptEscape="true" />',
                gender: '<s:message code="allocation.find.filter.activeFilter.gender" javaScriptEscape="true" />',
                nameOrId: '<s:message code="allocation.find.filter.activeFilter.nameOrId" javaScriptEscape="true" />',
                minAge: '<s:message code="allocation.find.filter.activeFilter.minAge" javaScriptEscape="true" />',
                maxAge: '<s:message code="allocation.find.filter.activeFilter.maxAge" javaScriptEscape="true" />',
                selectedTeamIds: '<s:message code="allocation.find.filter.activeFilter.selectedTeamIds" javaScriptEscape="true" />',
                selectOptionsError: '<s:message code="allocation.find.filter.colleagueName.selectOptionsError" javaScriptEscape="true" />',
                selectedColleaguePersonIds: '<s:message code="allocation.find.filter.activeFilter.selectedColleaguePersonIds" javaScriptEscape="true" />'
            }
        };
        var allocationsConfig = {
            container: '#allocationResultsContainer',
            commonLabels: '${commonLabels}',
            resultsFilterConfig: resultsFilterConfig,
            accessedClientsConfig: {
                url: '${allocationUrl}',
                permissions: permissions,
                tab: {
                    title: '<s:message code="home.allocation.tab.title.myClientsAllocation" javaScriptEscape="true"/>'
                },
                searchConfig: {
                    title: '<s:message code="home.recentRecords.title" javaScriptEscape="true"/>',
                    labels: commonLabels,
                    loading: 'loading...',
                    noDataMessage: '<s:message code="home.recentRecords.no.results" javaScriptEscape="true"/>',
                    accessAllowed: '<s:message code="home.recentRecords.accessAllowed" javaScriptEscape="true"/>',
                    showPerson: '<s:message code="home.person.show" javaScriptEscape="true"/>',
                    infoProtected: '<s:message code="home.person.infoProtected" javaScriptEscape="true"/>',
                    userId: currentUserId,
                    viewPersonUrl: '<s:url value="/summary/person?id={personId}" />'
                },
                googleMapLinkUrl: Eclipse.config.googleMapURL
            },
            teamAllocationConfig: {
                url: '${teamProfessionalClientsUrl}',
                permissions: permissions,
                tab: {
                    title: '<s:message code="home.allocation.tab.title.myTeamsAllocation" javaScriptEscape="true"/>'
                },
                searchConfig: {
                    title: '<s:message code="home.team.title" javaScriptEscape="true"/>',
                    labels: commonLabels,
                    noDataMessage: '<s:message code="home.teamRecords.no.results" javaScriptEscape="true"/>',
                    userId: currentUserId,
                    teamId: currentTeamId,
                    viewPersonUrl: '<s:url value="/summary/person?id={personId}"/>'
                },
                googleMapLinkUrl: Eclipse.config.googleMapURL
            },
            colleaguesAllocationConfig: {
                url: '${colleaguesProfessionalClientsUrl}',
                teamsUrl: '<s:url value="/rest/team/{teamId}/hierarchy" />',
                namesUrl: '<s:url value="/rest/team/{teamId}/hierarchy/professionalPerson?userId={userId}" />',
                permissions: permissions,
                tab: {
                    title: '<s:message code="home.allocation.tab.title.myColleaguesAllocation" javaScriptEscape="true"/>'
                },
                searchConfig: {
                    title: '<s:message code="home.colleagues.title" javaScriptEscape="true"/>',
                    labels: commonLabels,
                    noDataMessage: '<s:message code="home.colleaguesRecords.no.results" javaScriptEscape="true"/>',
                    userId: currentUserId,
                    teamId: currentTeamId,
                    viewPersonUrl: '<s:url value="/summary/person?id={personId}"/>'
                },
                googleMapLinkUrl: Eclipse.config.googleMapURL
            },
            allocationConfig: {
                url: '${professionalRoleUrl}',
                permissions: permissions,
                tab: {
                    title: '<s:message code="home.allocation.tab.title.myAllocation" javaScriptEscape="true"/>'
                },
                searchConfig: {
                    title: '<s:message code="home.caseLoad.title" javaScriptEscape="true"/>',
                    labels: commonLabels,
                    accessDenied: '<s:message code="home.person.accessDenied" javaScriptEscape="true"/>',
                    notAuthorised: '<s:message code="home.person.notAuthorised" javaScriptEscape="true"/>',
                    noDataMessage: '<s:message code="home.caseload.no.results" javaScriptEscape="true"/>',
                    userId: currentUserId,
                    teamId: currentTeamId,
                    viewPersonUrl: '<s:url value="/summary/person?id={personId}"/>'
                },
                googleMapLinkUrl: Eclipse.config.googleMapURL
            }
        };
        var allocationView = new Y.app.allocation.AllocationControllerView(allocationsConfig).render();
    });
</script>