<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
	<c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
	<c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
	<c:set var="lStyle" value="last" />
</c:if>

<%-- Button markup--%>
<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
	<a href='<s:url value="/home/allocation"/>' class="pure-button usp-fx-all ${aStyle} view-allocation"
		title='<s:message code="home.allocation.view.button.title"/> '
		aria-label='<s:message code="home.allocation.view.button.title"/>' tabindex="0"> 
		<i class="fa fa-list"></i>
		<span><s:message code="home.allocation.view.button" /></span>
	</a>
</li>
