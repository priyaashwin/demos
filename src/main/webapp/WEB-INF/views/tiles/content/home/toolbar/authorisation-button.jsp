<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Authorise')" var="canAuthoriseForm"/>
<sec:authorize access="hasPermission(null, 'CodedEntryAssignment','CodedEntryAssignment.Authorise')" var="canAuthoriseWarning"/>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.View')" var="canViewForm"/>
<sec:authorize access="hasPermission(null, 'PersonWarningAssignment','PersonWarningAssignment.View')" var="canViewWarning" />

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
	<c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
	<c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
	<c:set var="lStyle" value="last" />
</c:if>

<%--
Only show the forms button if the user has permission to authorise forms and warnings
(this may change in the future if more functionality is included authorise home page)
--%>
<c:if test="${(canAuthoriseForm eq true and canViewForm eq true) or (canAuthoriseWarning eq true and canViewWarning eq true )}">
  <%-- Button markup--%>
  <li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
  	<a href='<s:url value="/home/authorisation"/>' class="pure-button usp-fx-all ${aStyle}"
  		title='<s:message code="home.authorisation.view.button.title"/> '
  		aria-label='<s:message code="home.authorisation.view.button.title"/>' tabindex="0">
  		<span class="home-authorisation"><s:message code="home.authorisation.view.button.text" /></span>
  	</a>
  </li>
</c:if>
