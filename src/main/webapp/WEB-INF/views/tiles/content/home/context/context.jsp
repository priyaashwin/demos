<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<h1 tabindex="0" id="homeContext" class="iconic-title"></h1>

<script>
	Y.use('subject-context', function(Y){
		var subjectModel=new Y.app.PersonContextModel({
		        id:'<c:out value="${currentUser.id}"/>',
				personId:'${esc:escapeJavaScript(currentUser.personIdentifier)}',
				name:'${esc:escapeJavaScript(currentUser.name)}'
		});
	    
		//create and render the subjectContext object
		var subjectContent=new Y.app.SubjectContext({
		    container:Y.one('#homeContext'),
		    icon:'fa fa-home',
		    subTitle:'<s:message code="menu.home"/>',
		    model:subjectModel
		}).render();	    	
	});
</script>
