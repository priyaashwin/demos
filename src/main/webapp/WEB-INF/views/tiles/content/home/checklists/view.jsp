<%@page contentType="text/html;charset=UTF-8"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Archive')" var="canDelete"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Remove')" var="canRemove"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Update')" var="canUpdate"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Pause')" var="canPause"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Resume')" var="canResume"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.ResetEndDate')" var="canResetEndDate"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Start')" var="canStartEarly"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','Checklists.ViewTeamMemberChecklists')" var="canViewMembersChecklists"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Prioritise')" var="canPrioritise"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Escalate')" var="canEscalate"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Deescalate')" var="canDeescalate"/>

<sec:authentication property="principal.securityProfile.securityTeam.teamId" var="teamId"/>
    
<div id="scrollbar-menu">
    <div id="collapse-button" class="hide-on-search"></div>
</div>

<div id="checklistTabs" class="yui3-widget"></div>

<c:choose>
  <c:when test="${canView eq true}">
    <script>
    Y.use('checklist-home-tabs', 'event-delegate', function(Y) {
      var L = Y.Lang,
          canAdd = '${canAdd}' === 'true',
          canView = '${canView}' === 'true',
          canDelete = '${canDelete}' === 'true',
          canRemove = '${canRemove}' === 'true',
          canPause = '${canPause}' === 'true',
          canResume = '${canResume}' === 'true',
          canStartEarly = '${canStartEarly}' === 'true',
          //must be able to update to re-assign
          canReassign = '${canUpdate}' === 'true',
          canResetEndDate = '${canResetEndDate}' === 'true',
          canViewMembersChecklists = '${canViewMembersChecklists}' === 'true',
          canPrioritise = '${canPrioritise}' === 'true',
          canEscalate = '${canEscalate}' === 'true',
          canDeescalate = '${canDeescalate}' === 'true',
          reset = '${reset}' === 'true',
          currentUserId = '${esc:escapeJavaScript(currentUser.id)}';

      var permissions={
        canView: canView,
        canDelete: canDelete,
        canRemove: canRemove,
        canReassign: canReassign,
        canPause: canPause,
        canResume: canResume,
        canResetEndDate: canResetEndDate,
        canStartEarly: canStartEarly,
        canPrioritise: canPrioritise,
        canEscalate: canEscalate,
        canDeescalate: canDeescalate
      };
      var viewConfig={
        toolbarNode:'#sectionToolbar',
        currentPersonId: currentUserId,
        currentTeamId: '${currentUserTeamId}',
        personSummaryURL:'<s:url value="/summary/person?id={personId}&taskCard=true"/>',
        checklistURL: '<s:url value="/checklist/person/{personId}/view?checklistDefinitionId={checklistDefinitionId}&taskCard=true&reset=true"/>',
        searchConfig: {
            labels: {
                ownerName: '<s:message code="checklist.results.ownerName" javaScriptEscape="true"/>',
                dateDue: '<s:message code="checklist.results.due" javaScriptEscape="true"/>',
                dateStarted: '<s:message code="checklist.results.startDate" javaScriptEscape="true"/>',
                invalidList:'<s:message code="checklist.results.noFilterItems" javaScriptEscape="true"/>',
                indicator:{
                  checklist: '<s:message code="checklist.view.checklist" javaScriptEscape="true"/>',
                  task: '<s:message code="checklist.view.task" javaScriptEscape="true"/>'
                },
                checklist:{
                    state:{
                        inErrorTitle:'<s:message code="checklist.view.inError.title" javaScriptEscape="true"/>',
                        onTimeTitle:'<s:message code="checklist.view.onTime.title" javaScriptEscape="true"/>',
                        onTimeContent:'<s:message code="checklist.view.onTime.content" javaScriptEscape="true"/>',
                        lastReminderTitle:'<s:message code="checklist.view.lastReminder.title" javaScriptEscape="true"/>',
                        lastReminderContent:'<s:message code="checklist.view.lastReminder.content" javaScriptEscape="true"/>',
                        overdueTitle:'<s:message code="checklist.view.overdue.title" javaScriptEscape="true"/>',
                        overdueContent:'<s:message code="checklist.view.overdue.content" javaScriptEscape="true"/>',
                        pausedTitle:'<s:message code="checklist.view.paused.title" javaScriptEscape="true"/>',
                        pausedContent:'<s:message code="checklist.view.paused.content" javaScriptEscape="true"/>',
                        errorTitle:'<s:message code="checklist.view.error.title" javaScriptEscape="true"/>' 
                    },
                    clientTitle:'<s:message code="checklist.view.client.title" javaScriptEscape="true"/>',
                    teamTitle:'<s:message code="checklist.view.team.title" javaScriptEscape="true"/>'
                },
                header:{
                    overdue:'<s:message code="checklist.results.overdue" javaScriptEscape="true"/>',
                    lastReminder:'<s:message code="checklist.results.lastReminder" javaScriptEscape="true"/>',
                    onTime:'<s:message code="checklist.results.onTime" javaScriptEscape="true"/>',
                    inError:'<s:message code="checklist.results.inError" javaScriptEscape="true"/>',
                    paused:'<s:message code="checklist.results.paused" javaScriptEscape="true"/>',
                    multipleStatus:'<s:message code="checklist.results.multiple" javaScriptEscape="true"/>'
                },
                deleteChecklist: '<s:message code="checklist.results.deleteChecklist" javaScriptEscape="true"/>',
                removeChecklist: '<s:message code="checklist.results.removeChecklist" javaScriptEscape="true"/>',
                deleteChecklistTitle: '<s:message code="checklist.results.deleteChecklistTitle" javaScriptEscape="true"/>',
                removeChecklistTitle: '<s:message code="checklist.results.removeChecklistTitle" javaScriptEscape="true"/>',
                prioritiseChecklist: '<s:message code="checklist.results.prioritiseChecklist"/>',
                prioritiseChecklistTitle: '<s:message code="checklist.results.prioritiseChecklistTitle"/>',
                reassignChecklist: '<s:message code="checklist.results.reassignChecklist" javaScriptEscape="true"/>',
                reassignChecklistTitle: '<s:message code="checklist.results.reassignChecklistTitle" javaScriptEscape="true"/>',
                pauseChecklist: '<s:message code="checklist.results.pauseChecklist" javaScriptEscape="true"/>',
                pauseChecklistTitle: '<s:message code="checklist.results.pauseChecklistTitle" javaScriptEscape="true"/>',
                resumeChecklist: '<s:message code="checklist.results.resumeChecklist" javaScriptEscape="true"/>',
                resumeChecklistTitle: '<s:message code="checklist.results.resumeChecklistTitle" javaScriptEscape="true"/>',
                resetChecklistEndDate: '<s:message code="checklist.results.resetChecklistEndDate" javaScriptEscape="true"/>',
                resetChecklistEndDateTitle: '<s:message code="checklist.results.resetChecklistEndDateTitle" javaScriptEscape="true"/>',
                startChecklist: '<s:message code="checklist.results.startChecklist" javaScriptEscape="true"/>',
                startChecklistTitle: '<s:message code="checklist.results.startChecklistTitle" javaScriptEscape="true"/>',
                expandChecklistCardsTitle: '<s:message code="checklist.results.expandChecklistCardsTitle" javaScriptEscape="true"/>',
                collapseChecklistCardsTitle: '<s:message code="checklist.results.collapseChecklistCardsTitle" javaScriptEscape="true"/>',
                accessDeniedTitle: '<s:message code="access.denied.title" javaScriptEscape="true"/>',
                noAccess: '<s:message code="access.noAccess" javaScriptEscape="true"/>'
            },
            permissions: permissions,
            reset: reset,
            url: '<s:url value="/rest/taskInstance?ownerId={ownerId}&teamId={teamId}&s={sortBy}&pageNumber={page}&pageSize={pageSize}&t={t}" />',
            summaryUrl: '<s:url value="/rest/taskInstance/userSummary?ownerId={ownerId}&teamId={teamId}" />',
            noDataMessage: '<s:message code="home.checklist.tabs.summary.no.results" javaScriptEscape="true"/>'            	
        },
        reactChecklistDialogConfig: {
        	permissions: {
        		canEscalate: permissions.canEscalate,
        		canDeescalate: permissions.canDeescalate
        	},
            dialogConfig: {
              header: {
                icon: 'fa-user',
                text: '<s:message code="checklist.prioritise.header" javaScriptEscape="true"/>'
              },
              successMessage: '<s:message code="checklist.prioritise.success" javaScriptEscape="true"/>',
              narrative: {
                summary: '<s:message code="checklist.prioritise.summary" javaScriptEscape="true"/>',
                description: '{{subject.name}} ({{subject.identifier}})'
              }
            },
            labels: {
              priority: '<s:message code="checklist.prioritise.priority" javaScriptEscape="true"/>'
            },
            urls: {
             getChecklistUrl: '<s:url value="/rest/checklistInstance/{id}" />',
             setInitialChecklistPriorityUrl: '<s:url value="/rest/checklistInstance/{id}/status?action=prioritised" />',
             escalateChecklistInstancePriorityUrl: '<s:url value="/rest/checklistInstance/{id}/status?action=escalated" />',
             descalateChecklistInstancePriorityUrl: '<s:url value="/rest/checklistInstance/{id}/status?action=deescalated" />'
            }
        },
        filterContextConfig: {
            labels: {
                filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
                subject: '<s:message code="checklist.find.filter.subject.active" javaScriptEscape="true"/>',
                owner: '<s:message code="checklist.find.filter.owner.active" javaScriptEscape="true"/>',
                status: '<s:message code="checklist.find.filter.status.active" javaScriptEscape="true"/>',
                priority: '<s:message code="checklist.find.filter.priority.active" javaScriptEscape="true"/>',
                dateDue: '<s:message code="checklist.find.filter.dateDue.active" javaScriptEscape="true"/>',
                dateStarted: '<s:message code="checklist.find.filter.dateStarted.active" javaScriptEscape="true"/>',
                checklistDefinitionId: '<s:message code="checklist.find.home.filter.checklistDefinitionId.active" javaScriptEscape="true"/>',
                ownership: '<s:message code="checklist.find.filter.ownership.active" javaScriptEscape="true"/>'
            },
            container: '#checklistFilterContext'
        },
        filterConfig: {
            labels: {
                filterBy: '<s:message code="checklist.find.filter.title" javaScriptEscape="true"/>',
                checklistDefinitionId: '<s:message code="checklist.find.home.filter.checklistDefinitionId.active" javaScriptEscape="true"/>',
                dateFilter: '<s:message code="checklist.find.filter.dateFilter" javaScriptEscape="true"/>',
                subjectFilter: '<s:message code="checklist.find.filter.subjectFilter" javaScriptEscape="true"/>',
                ownerFilter: '<s:message code="checklist.find.filter.ownerFilter" javaScriptEscape="true"/>',
                typeStatusPriorityFilter: '<s:message code="checklist.find.home.filter.typeStatusPriorityFilter" javaScriptEscape="true"/>',
                dueDateFrom: '<s:message code="checklist.find.filter.dueDateFrom" javaScriptEscape="true"/>',
                dueDateTo: '<s:message code="checklist.find.filter.dueDateTo" javaScriptEscape="true"/>',
                startDateFrom: '<s:message code="checklist.find.filter.startDateFrom" javaScriptEscape="true"/>',
                startDateTo: '<s:message code="checklist.find.filter.startDateTo" javaScriptEscape="true"/>',
                dateRangeQuickDue: '<s:message code="checklist.find.filter.dateRangeQuickDue" javaScriptEscape="true"/>',
                dateRangeQuickStart: '<s:message code="checklist.find.filter.dateRangeQuickStart" javaScriptEscape="true"/>',
                dateRangeLastDay: '<s:message code="checklist.find.filter.dateRangeLastDay" javaScriptEscape="true"/>',
                dateRangeLastWeek: '<s:message code="checklist.find.filter.dateRangeLastWeek" javaScriptEscape="true"/>',
                dateRangeThisWeek: '<s:message code="checklist.find.filter.dateRangeThisWeek" javaScriptEscape="true"/>',
                dateRangeNextWeek: '<s:message code="checklist.find.filter.dateRangeNextWeek" javaScriptEscape="true"/>',
                dateRangeLastMonth: '<s:message code="checklist.find.filter.dateRangeLastMonth" javaScriptEscape="true"/>',
                dateRangeThisMonth: '<s:message code="checklist.find.filter.dateRangeThisMonth" javaScriptEscape="true"/>',
                dateRangeNextMonth: '<s:message code="checklist.find.filter.dateRangeNextMonth" javaScriptEscape="true"/>',
                and: '<s:message code="checklist.find.filter.conjunction" javaScriptEscape="true"/>',
                status: '<s:message code="checklist.find.filter.status" javaScriptEscape="true"/>',
                onTime: '<s:message code="checklist.find.filter.onTime" javaScriptEscape="true"/>',
                lastReminder: '<s:message code="checklist.find.filter.lastReminder" javaScriptEscape="true"/>',
                paused: '<s:message code="checklist.find.filter.paused" javaScriptEscape="true"/>',
                overdue: '<s:message code="checklist.find.filter.overdue" javaScriptEscape="true"/>',
                inError: '<s:message code="checklist.find.filter.error" javaScriptEscape="true"/>',
                priority: '<s:message code="checklist.find.filter.priority" javaScriptEscape="true"/>',
                none: '<s:message code="checklist.find.filter.none" javaScriptEscape="true"/>',
                low: '<s:message code="checklist.find.filter.low" javaScriptEscape="true"/>',
                medium: '<s:message code="checklist.find.filter.medium" javaScriptEscape="true"/>',
                high: '<s:message code="checklist.find.filter.high" javaScriptEscape="true"/>',
                noOwningTeam: '<s:message code="checklist.find.filter.noOwningTeam"/>',
                memberId: '<s:message code="checklist.find.filter.memberId" javaScriptEscape="true"/>',
                subjectId: '<s:message code="checklist.find.filter.subjectId" javaScriptEscape="true"/>',
                ownerId: '<s:message code="checklist.find.filter.ownerId" javaScriptEscape="true"/>',
                owner:{
                  team:'<s:message code="checklist.find.filter.owner.team" javaScriptEscape="true"/>',
                  member:'<s:message code="checklist.find.filter.owner.teamMembers" javaScriptEscape="true"/>'
                }
            },
            checklistDefinitionListURL: '<s:url value="/rest/taskInstanceOwnership/checklistDefinition?ownerId={ownerId}&teamId=${currentUserTeamId}&s={sortBy}&includeTeamOwners={includeTeamOwners}&includeTeamMembers={includeTeamMembers}" />',
            subjectListURL: '<s:url value="/rest/taskInstanceOwnership/personSubject?ownerId={ownerId}&teamId=${currentUserTeamId}&s={sortBy}&includeTeamOwners={includeTeamOwners}&includeTeamMembers={includeTeamMembers}" />',
            teamMembersListURL:'<s:url value="/rest/taskInstanceOwnership/teamMembers?ownerId={ownerId}&teamId=${currentUserTeamId}&s={sortBy}" />',
            owningTeamsListURL:'<s:url value="/rest/taskInstanceOwnership/owningTeams?ownerId={ownerId}&teamId=${currentUserTeamId}&includeTeamMembers={includeTeamMembers}&owningTeamsForPersonOwners={owningTeamsForPersonOwners}&s={sortBy}" />',
            //value to contextualise checklist results by current user's team
            teamIds: '${currentUserTeamId}',
            includeNoOwningTeam: '${includeNoOwningTeamByDefault}' === 'true'
        }
      };
      
      var checklistHeaderIcon='fa fa-check-square-o',
          narrativeDescription = '{{this.subject.name}} ({{this.subject.identifier}})';
          
      var dialogConfig={
        checklistStatusURL: '<s:url value="/rest/checklistInstance/{id}/status?action={action}"/>',
        checklistRemoveURL: '<s:url value="/rest/checklistInstance/{id}"/>',
        checklistInstanceURL: '<s:url value="/rest/checklistInstance/{id}"/>',
        checklistActionURL: '<s:url value="/rest/checklistInstance/{id}?action={action}"/>',
        views:{
          deleteChecklist: {
            header:{
              text:'<s:message code="checklist.delete.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary:'<s:message javaScriptEscape="true" code="checklist.delete.summary"/>',
              description: narrativeDescription
            },
            successMessage: '<s:message javaScriptEscape="true" code="checklist.delete.success"/>',
            config:{
              message: '<s:message code="checklist.delete.prompt"/>'
            }
          },
          removeChecklist: {
            header:{
              text:'<s:message code="checklist.remove.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary: '<s:message javaScriptEscape="true" code="checklist.remove.summary"/>',
              description: narrativeDescription
            },
            successMessage: '<s:message javaScriptEscape="true" code="checklist.remove.success"/>',
            config:{
              message: '<s:message code="checklist.remove.prompt"/>'
            }
          },
          reassignSuccessWithWarnings:{
            header:{
              text:'<s:message code="checklist.reassign.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary: '<s:message javaScriptEscape="true" code="checklist.reassign.successWithWarningsSummary"/>'
            },
            config:{
              message: '<s:message javaScriptEscape="true" code="checklist.reassign.successWithWarnings"/>'
            }
          },
          reassignChecklist: {
            header:{
              text:'<s:message code="checklist.reassign.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary: '<s:message javaScriptEscape="true" code="checklist.reassign.summary"/>',
              description: narrativeDescription
            },
            successMessage: '<s:message javaScriptEscape="true" code="checklist.reassign.success"/>',
            config:{
              reassignURL:'<s:url value="/rest/checklistInstance/{id}?action=reassign"/>',
              reassignBatchURL:'<s:url value="/rest/checklistInstance/{id}?action=reassign&reassignBatch=true"/>',
              personTeamsURL: '<s:url value="/rest/person/{id}/teamRelationship/professional?temporalStatusFilter=ACTIVE&sort=roleBOrganisation.name&pageSize=-1"/>',
              personAutocompleteURL: '<s:url value="/rest/person?nameOrUsername={query}&personType=professional&s={sortBy}&pageNumber=1&pageSize=50"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
              organisationAutocompleteURL:'<s:url value="/rest/organisation/active?escapeSpecialCharacters=true&appendWildcard=true&byOrganisationType=true&organisationTypes=TEAM&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
              owningTeamId: '${teamId}'
            },
            labels: {
                ownerId:'<s:message javaScriptEscape="true" code="newChecklistInstanceVO.ownerId"/>',
                ownerSubjectType:'<s:message javaScriptEscape="true" code="newChecklistInstanceVO.ownerSubjectType"/>',
                isGroupWorklist:'<s:message javaScriptEscape="true" code="checklist.reassign.isGroupWorklist"/>',
                reassignBatchInfo:'<s:message javaScriptEscape="true" code="checklist.reassign.reassignBatchInfo"/>',
                reassignBatch:'<s:message javaScriptEscape="true" code="checklist.reassign.reassignBatch"/>',
                ownerType:{
                    organisation:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation"/>',
                    person:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person"/>',
                    select:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.select"/>'
                },
                organisation:{
                    ownerId:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.ownerId"/>',
                    assignToOwnTeam:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.own"/>',
                    assignToOtherTeam:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.other"/>',
                    placeholder:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.placeholder"/>',
                    noResults:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.noResults"/>'
                },
                person:{
                    ownerId:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.ownerId"/>',
                    assignToSelf:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.self"/>',
                    assignToOther:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.other"/>',
                    placeholder:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.placeholder"/>',
                    noResults:'<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.noResults"/>',
                    teamSelect: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.teamSelect"/>'
              }
            }
          },
          pauseChecklist: {
            header:{
              text:'<s:message code="checklist.pause.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary: '<s:message javaScriptEscape="true" code="checklist.pause.summary"/>',
              description: narrativeDescription
            },
            successMessage: '<s:message javaScriptEscape="true" code="checklist.pause.success"/>',
            labels: {
                pauseReason: '<s:message javaScriptEscape="true" code="updateChecklistInstancePauseVO.pauseReason"/>'
            }
          },
          resumeChecklist: {
            header:{
              text:'<s:message code="checklist.resume.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary: '<s:message javaScriptEscape="true" code="checklist.resume.summary"/>',
              description: narrativeDescription
            },
            successMessage: '<s:message javaScriptEscape="true" code="checklist.resume.success"/>',
            config:{
              message: '<s:message code="checklist.resume.prompt"/>'  
            }
          },
          resetEndDate: {
            header:{
              text:'<s:message code="checklist.resetEndDate.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary: '<s:message javaScriptEscape="true" code="checklist.resetEndDate.summary"/>',
              description: narrativeDescription
            },
            successMessage: '<s:message javaScriptEscape="true" code="checklist.resetEndDate.success"/>',
            labels: {
                endDate: '<s:message javaScriptEscape="true" code="updateChecklistInstanceEndDateVO.endDate"/>'
            }
          },
          startChecklist: {
            header:{
              text:'<s:message code="checklist.start.header"/>',
              icon: checklistHeaderIcon
            },
            narrative:{
              summary: '<s:message javaScriptEscape="true" code="checklist.start.summary"/>',
              description: narrativeDescription
            },
            successMessage: '<s:message javaScriptEscape="true" code="checklist.start.success"/>',
            config:{
              message: '<s:message code="checklist.start.prompt"/>'
            }
          },
        }
      };

      var isMySummaryTabDisplayed = '${numberOfAssignedProfiles > 1}' === 'true';

      var checklistHomeTabs=new Y.app.checklist.HomeTabsView({
          root:'<s:url value="/home/checklists/"/>',
          viewURL: '<s:url value="/checklist/view?id={id}" />',
          taskURL: '<s:url value="/checklist/view?id={checklistId}&taskId={taskId}&taskCard=true"/>',
          container:'#checklistTabs',
          toolbarNode:'#sectionToolbar',
          contextHeaderNode: '#checklistContextHeader',
          canViewMembersChecklists:canViewMembersChecklists,
          tabs:{
              summary:{
                  label:'<s:message javaScriptEscape="true" code="home.checklist.tabs.summary"/>',
                  title:'<s:message javaScriptEscape="true" code="home.checklist.tabs.summary.title"/>',
                  config:Y.merge(viewConfig,{type:'SUMMARY'})
              },
              own:{
                  label:'<s:message javaScriptEscape="true" code="home.checklist.tabs.own"/>',
                  title:'<s:message javaScriptEscape="true" code="home.checklist.tabs.own.title"/>',
                  config:Y.merge(viewConfig,{type:'OWN'})
              },
              team:{
                  label:'<s:message javaScriptEscape="true" code="home.checklist.tabs.team"/>',
                  title:'<s:message javaScriptEscape="true" code="home.checklist.tabs.team.title"/>',
                  config:Y.merge(viewConfig,{type:'TEAM'})
              },
              members:{
                  label:'<s:message javaScriptEscape="true" code="home.checklist.tabs.members"/>',
                  title:'<s:message javaScriptEscape="true" code="home.checklist.tabs.members.title"/>',
                  config:Y.merge(viewConfig,{type:'MEMBERS'})
              }
          },
          dialogConfig:dialogConfig,
          currentUserId: currentUserId,
          isMySummaryTabDisplayed: isMySummaryTabDisplayed
      }).render();

      // Make sure to dispatch the current hash-based URL which was set by
      // the server to our route handlers.
      if (checklistHomeTabs.hasRoute(checklistHomeTabs.getPath())) {
          checklistHomeTabs.dispatch();
      } else if (isMySummaryTabDisplayed) {
          checklistHomeTabs.activateTab('summary');
      } else {
          checklistHomeTabs.activateTab('own');
      }

    });
    </script>
  </c:when>
  <c:otherwise>
    <%-- Insert access denied tile --%>
    <t:insertDefinition name="access.denied"/>
  </c:otherwise>
</c:choose>

