<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<s:eval expression="@appSettings.getDashboardType()" var="dashboardType" scope="page" />
<c:if test="${dashboardType eq 'google' }">
  <sec:authorize access="hasPermission(null, 'Dashboard','GoogleDashboard.View')" var="canViewDashboard" />
  <s:eval expression="@appSettings.getDashboardUrl()" var="dashboardUrl" scope="page" />
</c:if>
<c:if test="${dashboardType eq 'system' }">
  <sec:authorize access="hasPermission(null, 'Dashboard','Dashboard.View')" var="canViewDashboard" />
  <s:eval expression="@appSettings.getDashboardId()" var="dashboardId" scope="page" />
</c:if>

<div id="dashboardContainer"></div>
<c:choose>
  <c:when test="${canViewDashboard eq true}"> 
		<script>
		Y.use('dashboard-controller-view', function(Y) {
		
		  var dashboardPermissions = {
		    canViewDashboard:'${canViewDashboard}'==='true'
		  };
		  
		  var config = {
		      container: '#dashboardContainer',
		      permissions: dashboardPermissions,
		      dashboardConfig: {
		        type:'${dashboardType}',
		        google:{
		          //url is defined by the app setting
		          url:'${dashboardUrl}'
		        },
		        system:{
		          //base url is the eclipse root
		          baseUrl:'<s:url value="/"/>',
		          dashboardId:'${dashboardId}',
		          colours:[
		                   'rgb(124, 145, 196)',
		                   'rgb(241, 114, 158)',
		                   'rgb(0, 156, 165)',
		                   'rgb(176, 185, 29)',
		                   'rgb(169, 103, 170)',
		                   'rgb(215, 177, 97)',
		                   'rgb(244, 133, 128)'
		                  ],
		          borderColours:[
		                   'rgb(124, 145, 196)',
		                   'rgb(241, 114, 158)',
		                   'rgb(0, 156, 165)',
		                   'rgb(176, 185, 29)',
		                   'rgb(169, 103, 170)',
		                   'rgb(215, 177, 97)',
		                   'rgb(244, 133, 128)'
		          ]
		        }
		      }
		  };
		
		  var dashboardView = new Y.app.home.dashboard.DashboardControllerView(config).render();
		});
		</script>
</c:when>
  <c:otherwise>
    <%-- Insert access denied tile --%>
    <t:insertDefinition name="access.denied"/>
  </c:otherwise>
</c:choose>
