<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<s:eval expression="@appSettings.getDashboardType()" var="dashboardType" scope="page" />

<c:if test="${dashboardType eq 'google' }">
  <sec:authorize access="hasPermission(null, 'Dashboard','GoogleDashboard.View')" var="canView" />
</c:if>
<c:if test="${dashboardType eq 'system' }">
  <sec:authorize access="hasPermission(null, 'Dashboard','Dashboard.View')" var="canView" />
</c:if>

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
	<c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
	<c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
	<c:set var="lStyle" value="last" />
</c:if>

<c:choose>
  <c:when test="${canView eq true}">
    <c:set var="viewClass" value="" />
   <c:set var="tabIndex" value="0" />
   <c:set var="url" value="/home/dashboard" />
  </c:when>
  <c:otherwise>
    <c:set var="viewClass" value="pure-button-disabled" />
    <c:set var="tabIndex" value="-1"/>
    <c:set var="url" value="#none" />
    <c:set var="ariaDisabled" value="aria-disabled" />
  </c:otherwise>
</c:choose>

<%-- Only show the dashboard button if the user has permission --%>
<c:if test="${canView eq true}">
	<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
		<a href='<s:url value="${url }"/>' class="pure-button usp-fx-all ${viewClass}"
			title='<s:message code="home.dashboard.view.button.title"/> ' 
			${ariaDisabled}
			aria-label='<s:message code="home.dashboard.view.button.title"/>' tabindex="${tabIndex }"> 
			<i class="fa fa-bar-chart"></i>
			<span><s:message code="home.dashboard.view.button" /></span>
		</a>
	</li>
</c:if>