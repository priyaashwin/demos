<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />

<c:if test="${first eq true }">
  <c:set var="fStyle" value="first" />
</c:if>

<c:if test="${last eq true }">
  <c:set var="lStyle" value="last" />
</c:if>

<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
  <a href="#none" class="pure-button usp-fx-all filter pure-button-loading pure-button-disabled" title='<s:message code="home.forms.submitted.filter.button"/>' aria-disabled="true" aria-label='<s:message code="home.forms.submitted.filter.button"/>' tabindex="0">
    <i class="fa fa-filter"></i>
    <span><s:message code="button.filter" /></span>
  </a>
</li>