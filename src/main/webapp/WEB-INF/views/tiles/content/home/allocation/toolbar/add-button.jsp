<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'Person','Person.Add')" var="canAddPerson"/>
<sec:authorize access="hasPermission(null, 'Organisation','Organisation.ADD')" var="canAddOrganisation"/>
<sec:authorize access="hasPermission(null,'ProfessionalPersonTeamRelationship','ProfessionalPersonTeamRelationship.ADD')" var="canAddTeam"/>

<c:if test="${(canAddPerson eq true) || (canAddOrganisation eq true) || (canAddTeam eq true)}"  >

	<tilesx:useAttribute name="selected" ignore="true"/>
	<tilesx:useAttribute name="first" ignore="true"/>
	<tilesx:useAttribute name="last" ignore="true"/>
	
	<c:if test="${selected eq true }">
	  <c:set var="aStyle" value="pure-button-active"/>
	</c:if>
	<c:if test="${first eq true }">
	  <c:set var="fStyle" value="first"/>
	</c:if>
	<c:if test="${last eq true }">
	  <c:set var="lStyle" value="last"/>
	</c:if>
<c:choose>
	<c:when test="${(((canAddPerson eq true)?1:0)+((canAddOrganisation eq true)?1:0)+((canAddTeam eq true)?1:0))== 1}">
	<c:choose>
	<c:when test="${canAddPerson eq true}">
	<c:set var="bClass" value="addPersonButton"/>
	<c:set var="bTitle"><s:message code="home.add.person.button"/></c:set>
	</c:when>
	<c:when test="${canAddOrganisation eq true}">
	<c:set var="bClass" value="addOrganisationButton"/>
	<c:set var="bTitle"><s:message code="home.add.organisation.button"/></c:set>
	</c:when>
	<c:when test="${canAddTeam eq true}">
	<c:set var="bClass" value="addTeamButton"/>
	<c:set var="bTitle"><s:message code="home.add.team.button"/></c:set>
	</c:when>
	</c:choose>
	<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'><a id="home_button" href="#none" class='usp-fx-all pure-button <c:out value="${aStyle}"/> <c:out value="${bClass}"/>' title='<c:out value="${bTitle}"/>'><i class="fa fa-plus-circle"></i><span><c:out value="${bTitle}"/></span></a></li>
	</c:when>
	<c:otherwise>
	<li id="home_split_button" class="pure-menu pure-menu-open pure-menu-horizontal menu-plugin hide-on-search" role="menu">
		<ul class="pure-menu-children">
			<li class="pure-menu-can-have-children pure-menu-has-children <c:out value="${fStyle}"/> <c:out value="${lStyle}"/>">
				<a href="#none" class='pure-button usp-fx-all <c:out value="${ aStyle}"/>' title="Add - click to choose"><i class='fa fa-plus-circle'></i> <i class='fa fa-caret-down icon-white'></i><span><s:message code="button.add"/></span></a>
				<ul class="pure-menu pure-menu-children">
				<c:if test="${canAddPerson eq true}">
				<li class="pure-menu-item"><a href="#none" title='<s:message code="home.add.person.button"/>' class="addPersonButton usp-fx-all pure-button-loading" aria-disabled="true"><i class="fa fa-user"></i> person</a></li>
				</c:if>
				<c:if test="${canAddOrganisation eq true}">
				<li class="pure-menu-item"><a href="#none" title='<s:message code="home.add.organisation.button"/>' class="addOrganisationButton usp-fx-all pure-button-loading" aria-disabled="true"><i class="fa fa-building-o"></i> organisation</a></li>
				</c:if>
				<c:if test="${canAddTeam eq true}">
				<li class="pure-menu-item"><a href="#none" title='<s:message code="home.add.team.button"/>' class="addTeamButton usp-fx-all pure-button-loading" aria-disabled="true"><i class="eclipse-team"></i> team</a></li>
				</c:if>
				</ul>
			</li>
		</ul>
	</li>
	</c:otherwise>
</c:choose>
</c:if>
	<script>
	Y.use('yui-base','event-delegate','node','event-custom-base', 'split-button-plugin', function(Y){
		var button ='';
		//plug in button menu for graph style
		if(Y.one('#home_split_button')){
			Y.one('#home_split_button').plug(Y.Plugin.usp.SplitButtonPlugin);
			button = Y.one('#home_split_button');
		}else{
			button = Y.one('#home_button');
		}
		Y.delegate('click', function(e){
			
			var t = e.currentTarget;
			if(t.hasClass('addPersonButton')){
				e.preventDefault();
				Y.fire('personDialog:add');
			}
			else if(t.hasClass('addOrganisationButton')){
				e.preventDefault();
				Y.fire('organisationDialog:add');
			}
			else if(t.hasClass('addTeamButton')){
				e.preventDefault();
				Y.fire('teamDialog:add');
			}
		},button,'.pure-button, li.pure-menu-item a');
		
		});
	
	</script>
