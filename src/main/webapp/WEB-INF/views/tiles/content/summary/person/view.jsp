<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','CaseNoteEntry.View')" var="canViewCaseNote"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','ChronologyEntry.View')" var="canViewChronology"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonClassifierAssignment.GET')" var="canViewClassification"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.GET')" var="canViewRelationship"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','FormInstance.View')" var="canViewForm"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','AuditEventLogEntry.View')" var="canViewAudit"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','ChildCarerApproval.View')" var="canViewCarerApproval"/>
<sec:authorize access="hasPermission(null, 'PersonFinanceSummary','PersonFinanceSummary.View')" var="canViewPersonFinanceSummary"/>

<sec:authorize access="hasPermission(null, 'PersonFinanceSummary','PersonServiceAgreement.View')" var="canViewPersonServiceAgreement"/>
<sec:authorize access="hasPermission(null, 'PersonFinanceSummary','PersonPersonalBudgetItem.View')" var="canViewPersonPersonalBudgetItem"/>
<sec:authorize access="hasPermission(null, 'PersonFinanceSummary','PersonFinancialReferences.View')" var="canViewPersonFinancialReferences"/>
<sec:authorize access="hasPermission(null, 'PersonFinanceSummary','PersonFinancialRepresentativeRelationship.View')" var="canViewPersonFinancialRepresentativeRelationship"/>
<sec:authorize access="hasPermission(null, 'PersonFinanceSummary','PersonNonResidentialAssessment.View')" var="canViewPersonNonResidentialAssessment"/>
<sec:authorize access="hasPermission(null, 'PersonFinanceSummary','PersonResidentialContribution.View')" var="canViewPersonResidentialContribution"/>
<sec:authentication property="principal.securityProfile" var="currentUserTeam"/>
  
<sec:authorize access="hasPermission(null, 'PersonHealthSummary','PersonHealthSummary.View')" var="canViewPersonHealthTab"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonHealthSummary.View')" var="canViewPersonHealthSummary"/>

<sec:authorize access="hasPermission(null, 'PersonAddress.View','PersonAddress.View')" var="canViewAddress"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','ChildProtectionRelationships.GET')" var="canViewChildProtectionRelationships"/>

<c:set var="canViewChildProtection" value="${childProtectionSummaryEnabled and canViewChildProtectionRelationships}"/>

<s:eval expression="@appSettings.isChronologyImpactEnabled()" var="impactEnabled" scope="page" />

<jsp:useBean id="now" class="java.util.Date"/>

<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="isNamedPersonRelationshipAvailable" value="${userContextAttributes.getContext('named-person-relationship-available')}" />

<%-- access relationship allowed attribute --%>
<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="relationshipAttributesContext" value="${userContextAttributes.getContext('relationship-attribute')}" />
<c:set var="allowedAttributes" value="${relationshipAttributesContext.getAllowedAttributeValuesAsJson()}" />

<s:url var="timelineUrl" value="/rest/{subjectType}/{subjectId}/chronologyEntry?s={sortBy}&statusList=COMPLETE&statusList=DRAFT&pageNumber=1&pageSize=-1">
  <s:param name="sortBy" value="[{\"eventDate!calculatedDate\": \"desc\"}]" />
</s:url>

<s:url var="privateFosteringUrl" value="/rest/classificationAssignment?subjectType={subjectType}&subjectId={subjectId}&classificationCode=PRV_FOSTERED&classificationGroupCode=PRV_FOSTERING&page=1&pageSize=-1&s={sortBy}">
  <s:param name="sortBy" value="[{\"startDate\":\"desc\"}]" />
</s:url>

<s:url var="keyPersonPersonRelationshipsUrl" value="/rest/personPersonRelationship/keyRelationships?personId={subjectId}&date=${now}&relationshipSubTypes=SIBLING&relationshipSubTypes=PARENTCHILD&temporalStatusFilter=ACTIVE&pageNumber={page}&pageSize={pageSize}">
  <s:param name="relationshipAttributes" value="NEXT_OF_KIN" />
  <s:param name="relationshipAttributes" value="ALLOCATED_WORKER" />
  <c:if test="${isNamedPersonRelationshipAvailable eq true}">
    <s:param name="relationshipAttributes" value="NAMED_PERSON" />
  </c:if>
  <s:param name="relationshipAttributes" value="PARENTAL_RESPONSIBILITY" />
  <s:param name="relationshipAttributes" value="KEY_WORKER" />
  <s:param name="relationshipAttributes" value="NEAREST_RELATIVE" />
  <s:param name="relationshipAttributes" value="GUARDIAN" />
  <c:if test="${relationshipAttributesContext.isAllowed('GUARDIAN_WELFARE') eq true}">
  	<s:param name="relationshipAttributes" value="GUARDIAN_WELFARE" />
  </c:if>  
  <c:if test="${relationshipAttributesContext.isAllowed('GUARDIAN_FINANCIAL') eq true}">
    <s:param name="relationshipAttributes" value="GUARDIAN_FINANCIAL" />
  </c:if>  
  <c:if test="${relationshipAttributesContext.isAllowed('POWER_OF_ATTORNEY_HEALTH') eq true}">
    <s:param name="relationshipAttributes" value="POWER_OF_ATTORNEY_HEALTH" />
  </c:if>  
  <c:if test="${relationshipAttributesContext.isAllowed('POWER_OF_ATTORNEY_FINANCE') eq true}">
    <s:param name="relationshipAttributes" value="POWER_OF_ATTORNEY_FINANCE" />
  </c:if>  
  <s:param name="relationshipAttributes" value="EMERGENCY_CONTACT" />
  <s:param name="relationshipAttributes" value="KEY_HOLDER" /> 
  <s:param name="relationshipAttributes" value="DWP_APPOINTEE" />  
  <s:param name="relationshipAttributes" value="COURT_APPOINTED_DEPUTY_FINANCIAL" />
  <s:param name="relationshipAttributes" value="COURT_APPOINTED_DEPUTY" />
  <s:param name="relationshipAttributes" value="ADVOCATE" />
</s:url>

<s:url var="keyPersonOrganisationRelationshipsUrl" value="/rest/personOrganisationRelationship/keyRelationships?personId={subjectId}&date=${now}&temporalStatusFilter=ACTIVE&pageNumber={page}&pageSize={pageSize}">
  <s:param name="relationshipAttributes" value="PARENTAL_RESPONSIBILITY" />
  <s:param name="relationshipAttributes" value="RESPONSIBLE_LOCAL_AUTHORITY" />
  <s:param name="relationshipAttributes" value="EMERGENCY_CONTACT" />
  <s:param name="relationshipAttributes" value="KEY_HOLDER" />
  <s:param name="relationshipAttributes" value="DWP_APPOINTEE" />
  <s:param name="relationshipAttributes" value="COURT_APPOINTED_DEPUTY" />
</s:url>

<c:choose>
  <c:when test="${canViewPerson eq true}">

  <div class="yui3-app" id="summary-widget-frame"></div>
  <script>
  Y.use('categories-address-component-UnknownLocation',
      function(Y){
   var subject = {
        subjectType: 'PERSON',
        subjectId: '<c:out value="${person.id}"/>'
   };

   var impactEnabled='${impactEnabled}'==='true';
   
   var permissions={
     profile:{
       canView:'${canViewPerson}'==='true',
       canViewForm:'${canViewForm}'==='true',
       canViewCarerApproval : '${canViewCarerApproval}' === 'true',
       canViewPrivateFostering: '${canViewClassification}' === 'true'
     },
     caseNote:{
       canView:'${canViewCaseNote}'==='true'
     },
     chronology:{
       canView:'${canViewChronology}'==='true'
     },
     classification:{
       canView:'${canViewClassification}'==='true'
     },
     relationship:{
       canView:'${canViewRelationship}'==='true'
     },
     event:{
       canView:'${canViewAudit}'==='true'
     },
     finance:{
       canView:'${canViewPersonFinanceSummary}'==='true',
       serviceAgreements:{
         canView: '${canViewPersonServiceAgreement}' === 'true'
       },
       personalBudget:{
         canView: '${canViewPersonPersonalBudgetItem}' === 'true'
       },
       keyFinancialDetails:{
         canView: '${canViewPersonFinancialReferences}' === 'true' && '${canViewPersonFinancialRepresentativeRelationship}' === 'true'
       },
       charging:{
         canView: '${canViewPersonResidentialContribution}' === 'true' && '${canViewPersonNonResidentialAssessment}' === 'true'
       }
     },
     health:{
       canView:'${canViewPersonHealthTab}'==='true',
       healthSummary:{
         canView: '${canViewPersonHealthSummary}'==='true'
       }
     }
   }

   var timelineConfig={
     subject:subject,
     url: '${timelineUrl}',
     rangeEndpoint:{
       url:'<s:url value="/rest/{subjectType}/{subjectId}/chronology"/>',
       headers: {
         'X-Requested-With': 'XMLHttpRequest',
         'Accept': 'application/vnd.olmgroup-usp.chronology.personchronologywithentryrange+json'
       }
     },
     impactEnabled:impactEnabled,
     labels:{
       header:{
         title:'Chronology timeline:'
       },
       emptyTimeline:'There are no chronology entries.',
       selectPrompt:'Select a day to see entry details.',
       navigation:{
         first: 'first entry',
         next:'next entry',
         previous:'previous entry',
         last: 'last entry'
       },
       key:{
         collapse:'Hide key',
         expand:'Show key',
         noEntry:'No entry',
         positive:'Positive impact',
         neutral:'Neutral impact',
         negative:'Negative impact',
         unknown: 'Unknown impact',
       },
       control:{
         day: 'day',
         month: 'month',
         year: 'year'
       }
     }
   };


   var classificationsConfig={
     subject:subject,
     url: '<s:url value="/rest/classificationAssignment?subjectType={subjectType}&subjectId={subjectId}"/>',
     labels:{
       header:{
         title:'Classifications summary:'
       },
       daysAgo: 'days ago',
       dayAgo: 'day ago',
       currentDuration: 'current:',
       totalDuration: 'Total days',
       longestDuration: 'Longest:',
       episodes: 'Episodes:',
       collapse: 'Collapse classifications summary',
       expand: 'Expand classifications summary',
       noRecordsFound:'There are no classifications.'
     }
   };

   var caseNoteConfig={
     subject:subject,
     url: '<s:url value="/rest/{subjectType}/{subjectId}/caseNoteEntry?statusList=COMPLETE&statusList=DRAFT&excludeGroupEntries=0&useSoundex=0&appendWildcard=0&pageSize={pageSize}&page={page}&entryTypeList={entryType}"/>',
     configurableTypeUrl: '<s:url value="/rest/caseNoteConfigurableTypeReference?pageNumber={page}&pageSize={pageSize}"/>',
     labels:{
       header:{
         title: '<s:message code="personsummary.casenote.header.title" javaScriptEscape="true" />'
       },
       noRecordsFound:'<s:message code="personsummary.casenote.noRecordFound" javaScriptEscape="true" />'
     }
   };

    var profileConfig={
     subject:subject,
     url: '<s:url value="/rest/{subjectType}/{subjectId}/picture/content"/>',
     configurableTypeUrl: '<s:url value="/rest/formConfigurableTypeReference?teamId={teamId}"><s:param name="teamId" value="${currentUserTeam.securityTeam.teamId}" /></s:url>',
     currentUserTeamId: '${currentUserTeam.securityTeam.teamId}',
     formUrl: '<s:url value="/rest/formInstance/latest?personId={subjectId}&formDefinitionUID={formDefinitionUID}&status=DRAFT&status=COMPLETE&status=SUBMITTED"/>',
     launchFormURL:'<s:url value="/forms/load?id={id}"/>',
     carerApprovalSummaryUrl:'<s:url value="/rest/person/{subjectId}/childCarerApproval/carerApprovalSummary"/>',
     classificationsUrl: '${privateFosteringUrl}',
     fosterCarerRegistrationUrl:'<s:url value="/rest/person/{id}/fosterCarerRegistration?current"/>',
     labels:{
       forms:{
         header:{
           title:'Latest forms:'
         },
         empty: 'not present.'
       },
       fosterCarerApproval : {
   		   title:'Fostering:',
   		   details:{
   			   title:'Foster Carer Details',
   			   approvalsTitle:'Current Approvals',
   			   suspensionsTitle:'Current Availability',
   			   empty:'No current approval details.',
   			   ex_registration:{
   			      status:'ex_registered',
   			      title:'Deregistered: '
   			   }
   		   }
       },
       adopterApproval : {
   		   title:'Adoption:',
   		   details:{
   			   title:'Adoptive Carer Details',
   			   approvalsTitle:'Current Approvals',
   			   empty:'No current approval details.'
   		   }
       }
     }
   };

   var allowedAttributes = JSON.parse('${allowedAttributes}');
   
   var relationshipsConfig={
     subject:subject,
     labels:{
       header:{
         title:'<s:message code="personSummary.keyRelationships" javaScriptEscape="true"/>'
       }
     },
     personPersonRelationshipConfig:{ 
       url: '${keyPersonPersonRelationshipsUrl}',
       labels:{
         header:{
           title:'<s:message code="personSummary.personPersonRelationship.title" javaScriptEscape="true"/>'
         },
         icons:{
           nok:'<s:message code="personSummary.relationship.attributes.nok" javaScriptEscape="true"/>',
           aw:'<s:message code="personSummary.relationship.attributes.aw" javaScriptEscape="true"/>',
           np:'<s:message code="personSummary.relationship.attributes.np" javaScriptEscape="true"/>',
           pr:'<s:message code="personSummary.relationship.attributes.pr" javaScriptEscape="true"/>',
           kw:'<s:message code="personSummary.relationship.attributes.kw" javaScriptEscape="true"/>',
           cm:'<s:message code="personSummary.relationship.attributes.cm" javaScriptEscape="true"/>',
           fr:'<s:message code="personSummary.relationship.attributes.fr" javaScriptEscape="true"/>',
           lp:'<s:message code="personSummary.relationship.attributes.lp" javaScriptEscape="true"/>',
           c:'<s:message code="personSummary.relationship.attributes.c" javaScriptEscape="true"/>',
           pw:'<s:message code="personSummary.relationship.attributes.pw" javaScriptEscape="true"/>',
           rc:'<s:message code="personSummary.relationship.attributes.rc" javaScriptEscape="true"/>',
           nc:'<s:message code="personSummary.relationship.attributes.nc" javaScriptEscape="true"/>',
           ec:'<s:message code="personSummary.relationship.attributes.ec" javaScriptEscape="true"/>',
           kh:'<s:message code="personSummary.relationship.attributes.kh" javaScriptEscape="true"/>',
           dwp:'<s:message code="personSummary.relationship.attributes.dwp" javaScriptEscape="true"/>',
           g:'<s:message code="personSummary.relationship.attributes.g" javaScriptEscape="true"/>',
           gf:'<s:message code="personSummary.relationship.attributes.gf" javaScriptEscape="true"/>',
           cadf:'<s:message code="personSummary.relationship.attributes.cadf" javaScriptEscape="true"/>',
           cad:'<s:message code="personSummary.relationship.attributes.cad" javaScriptEscape="true"/>',
           poah:'<s:message code="personSummary.relationship.attributes.poah" javaScriptEscape="true"/>',
           poaf:'<s:message code="personSummary.relationship.attributes.poaf" javaScriptEscape="true"/>',
           nr:'<s:message code="personSummary.relationship.attributes.nr" javaScriptEscape="true"/>',
           ad:'<s:message code="personSummary.relationship.attributes.ad" javaScriptEscape="true"/>',
           gw:'<s:message code="personSummary.relationship.attributes.gw" javaScriptEscape="true"/>',
           
           nokopp:'<s:message code="personSummary.relationship.attributes.nokopp" javaScriptEscape="true"/>',
           awopp:'<s:message code="personSummary.relationship.attributes.awopp" javaScriptEscape="true"/>',
           npopp:'<s:message code="personSummary.relationship.attributes.npopp" javaScriptEscape="true"/>',
           propp:'<s:message code="personSummary.relationship.attributes.propp" javaScriptEscape="true"/>',
           kwopp:'<s:message code="personSummary.relationship.attributes.kwopp" javaScriptEscape="true"/>',
           cmopp:'<s:message code="personSummary.relationship.attributes.cmopp" javaScriptEscape="true"/>',
           fropp:'<s:message code="personSummary.relationship.attributes.fropp" javaScriptEscape="true"/>',
           lpopp:'<s:message code="personSummary.relationship.attributes.lpopp" javaScriptEscape="true"/>',
           copp:'<s:message code="personSummary.relationship.attributes.copp" javaScriptEscape="true"/>',
           pwopp:'<s:message code="personSummary.relationship.attributes.pwopp" javaScriptEscape="true"/>',
           rcopp:'<s:message code="personSummary.relationship.attributes.rcopp" javaScriptEscape="true"/>',
           ncopp:'<s:message code="personSummary.relationship.attributes.ncopp" javaScriptEscape="true"/>',
           ecopp:'<s:message code="personSummary.relationship.attributes.ecopp" javaScriptEscape="true"/>',
           khopp:'<s:message code="personSummary.relationship.attributes.khopp" javaScriptEscape="true"/>',
           dwpopp:'<s:message code="personSummary.relationship.attributes.dwpopp" javaScriptEscape="true"/>',
           gopp:'<s:message code="personSummary.relationship.attributes.gopp" javaScriptEscape="true"/>',
           gfopp:'<s:message code="personSummary.relationship.attributes.gfopp" javaScriptEscape="true"/>',           
           cadfopp:'<s:message code="personSummary.relationship.attributes.cadfopp" javaScriptEscape="true"/>',
           cadopp:'<s:message code="personSummary.relationship.attributes.cadopp" javaScriptEscape="true"/>',
           poahopp:'<s:message code="personSummary.relationship.attributes.poahopp" javaScriptEscape="true"/>',
           poafopp:'<s:message code="personSummary.relationship.attributes.poafopp" javaScriptEscape="true"/>',
           nropp:'<s:message code="personSummary.relationship.attributes.nropp" javaScriptEscape="true"/>',
           adopp:'<s:message code="personSummary.relationship.attributes.adopp" javaScriptEscape="true"/>',
           gwopp:'<s:message code="personSummary.relationship.attributes.gwopp" javaScriptEscape="true"/>'
         },
         noRecordsFound:'<s:message code="personSummary.personPersonRelationship.noResults" javaScriptEscape="true"/>'
       },
       summaryUrl:'<s:url value="/summary/person?id="/>',
       isNamedPersonRelationshipAvailable: '${isNamedPersonRelationshipAvailable}'==='true',
       allowedAttributes: allowedAttributes
     },
     personOrganisationRelationshipConfig:{
       url: '${keyPersonOrganisationRelationshipsUrl}',
       labels:{
         header:{
           title:'<s:message code="personSummary.personOrganisationRelationship.title" javaScriptEscape="true"/>'
         },
         icons:{
           pr:'<s:message code="personSummary.relationship.attributes.pr" javaScriptEscape="true"/>',
           rla:'<s:message code="personSummary.relationship.attributes.rla" javaScriptEscape="true"/>',
           ec:'<s:message code="personSummary.relationship.attributes.ec" javaScriptEscape="true"/>'
         },
         noRecordsFound:'<s:message code="personSummary.personOrganisationRelationship.noResults" javaScriptEscape="true"/>'
       },
       summaryUrl:'<s:url value="/organisation?id="/>',
       allowedAttributes: allowedAttributes
     },
     childProtectionRelationshipConfig:{
       permissions:{
         canViewAddress:'${canViewAddress}'==='true',
         canView:'${canViewChildProtection}'==='true'
       },
       url: '<s:url value="/rest/childProtectionRelationships?personId={subjectId}&pageNumber={page}&pageSize={pageSize}"/>',
       classificationGroupPath:'${childProtectionClassificationGroupPath}',
       classificationPathSeparator:'${childProtectionClassificationPathSeparator}',
       labels:{
         header:{
           title:'<s:message code="personSummary.childProtection.title" javaScriptEscape="true"/>'
         },
         noRecordsFound:'<s:message code="personSummary.childProtection.noResults" javaScriptEscape="true"/>',
         address:{
           noAddressPermission:'<s:message code="person.address.noPermission" javaScriptEscape="true"/>',
           address:'<s:message code="person.address" javaScriptEscape="true"/>',
           noAddress:'<s:message code="person.address.noRecordFound" javaScriptEscape="true"/>',
           isTemporary:'<s:message code="person.address.temporary" javaScriptEscape="true"/>',
           isPlacement:'<s:message code="person.address.placement" javaScriptEscape="true"/>',
           isDoNotDisclose:'<s:message code="person.address.doNotDisclose" javaScriptEscape="true"/>',
           current:'<s:message code="person.address.current" javaScriptEscape="true"/>',
           floor:'<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
           room:'<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>'
         }
       },
       codedEntries:{
         unknownLocation:Y.uspCategory.address.unknownLocation.category
       },
       summaryUrl:'<s:url value="/summary/person?id="/>',
       mapLinkUrl:Eclipse.config.googleMapURL
     },
     subjectPictureEndpoint: {
       url: '<s:url value="/rest/{subjectType}/{subjectId}/picture/content"/>'
     }
   };

   
   var eventsConfig={
     subject:subject,
     url: '<s:url value="/rest/auditEventLogEntry?subjectId={subjectId}&pageNumber={page}&pageSize={pageSize}"/>',
     labels:{
       header:{
         title:'Latest events:'
       },
       noRecordsFound:'There are no events.'
     }
   };
   
   var financeErrors={
       providerUnavailableTitle: '<s:message code="financeSummary.error.providerUnavailableTitle" javaScriptEscape="true" />',
       providerUnavailableMessage: '<s:message code="financeSummary.error.providerUnavailableMessage" javaScriptEscape="true" />',
       resourceNotFoundInProviderTitle: '<s:message code="financeSummary.error.resourceNotFoundInProviderTitle" javaScriptEscape="true" />',
       resourceNotFoundInProviderMessage: '<s:message code="financeSummary.error.resourceNotFoundInProviderMessage" javaScriptEscape="true" />',
       externalResourceNotFoundTitle: '<s:message code="financeSummary.error.externalResourceNotFoundTitle" javaScriptEscape="true" />',
       externalResourceNotFoundMessage: '<s:message code="financeSummary.error.externalResourceNotFoundMessage" javaScriptEscape="true" />'
   };
   
   var serviceAgreementsConfig={
      subject:subject,
      labels:{
        header:{
          title:'<s:message code="financeSummary.serviceAgreements.title" javaScriptEscape="true" />'
        },
        filter:{
          activeOnly:'<s:message code="financeSummary.filter.activeOnly" javaScriptEscape="true" />',
          showAll: '<s:message code="financeSummary.filter.showAll" javaScriptEscape="true" />',
        },
        columns: {
          partyName: '<s:message code="financeSummary.serviceAgreements.partyName" javaScriptEscape="true" />',
          service: '<s:message code="financeSummary.serviceAgreements.service" javaScriptEscape="true" />',
          type: '<s:message code="financeSummary.serviceAgreements.type" javaScriptEscape="true" />',
          active: '<s:message code="financeSummary.serviceAgreements.active" javaScriptEscape="true" />',
          weeklyCommitmentAmount:'<s:message code="financeSummary.serviceAgreements.weeklyCommitmentAmount" javaScriptEscape="true" />',
          yearlyCommitmentAmount:'<s:message code="financeSummary.serviceAgreements.yearlyCommitmentAmount" javaScriptEscape="true" />'
        },
        startDate: '<s:message code="financeSummary.serviceAgreements.startDate" javaScriptEscape="true" />',
        endDate: '<s:message code="financeSummary.serviceAgreements.endDate" javaScriptEscape="true" />',
        serviceId: '<s:message code="financeSummary.serviceAgreements.serviceId" javaScriptEscape="true" />',
        accountCode: '<s:message code="financeSummary.serviceAgreements.accountCode" javaScriptEscape="true" />',
        yearlyCommitmentTotal:'<s:message code="financeSummary.serviceAgreements.yearlyCommitmentTotal" javaScriptEscape="true" />',
        errors:financeErrors
      },
      searchConfig:{
        url: '<s:url value="/rest/{subjectType}/{subjectId}/serviceAgreement"/>'
      }
    };
     
   var personalBudgetConfig={
       subject:subject,
       labels:{
         header:{
           title:'<s:message code="financeSummary.personalBudget.title" javaScriptEscape="true" />'
         },
         filter:{
           activeOnly:'<s:message code="financeSummary.filter.activeOnly" javaScriptEscape="true" />',
           showAll: '<s:message code="financeSummary.filter.showAll" javaScriptEscape="true" />',
         },
         columns: {
           type: '<s:message code="financeSummary.personalBudget.type" javaScriptEscape="true" />',
           startDate: '<s:message code="financeSummary.personalBudget.startDate" javaScriptEscape="true" />',
           endDate: '<s:message code="financeSummary.personalBudget.endDate" javaScriptEscape="true" />',
           weeklyAmount: '<s:message code="financeSummary.personalBudget.weeklyAmount" javaScriptEscape="true" />',
           yearlyAmount:'<s:message code="financeSummary.personalBudget.yearlyAmount" javaScriptEscape="true" />'
         },
         errors:financeErrors
       },
       searchConfig:{
         url: '<s:url value="/rest/{subjectType}/{subjectId}/personalBudgetItem"/>'
       }
     };
   
   var keyFinancialDetailsConfig={
       subject:subject,
       labels:{
         header:{
           title:'<s:message code="financeSummary.keyDetails.title" javaScriptEscape="true" />'
         },
         filter:{
           activeOnly:'<s:message code="financeSummary.filter.activeOnly" javaScriptEscape="true" />',
           showAll: '<s:message code="financeSummary.filter.showAll" javaScriptEscape="true" />',
         },
         financialRepresentative:'<s:message code="financeSummary.representatives.title" javaScriptEscape="true" />',
         columns: {
           representativeId: '<s:message code="financeSummary.representatives.representativeId" javaScriptEscape="true" />',
           name: '<s:message code="financeSummary.representatives.name" javaScriptEscape="true" />',
           address: '<s:message code="financeSummary.representatives.address" javaScriptEscape="true" />',
           active: '<s:message code="financeSummary.representatives.active" javaScriptEscape="true" />'
         },
         creditorReference:'<s:message code="financeSummary.keyDetails.creditorReference" javaScriptEscape="true" />',
         debtorReference: '<s:message code="financeSummary.keyDetails.debtorReference" javaScriptEscape="true" />',
         relationshipType: '<s:message code="financeSummary.representatives.relationshipType" javaScriptEscape="true" />',
         startDate: '<s:message code="financeSummary.representatives.startDate" javaScriptEscape="true" />',
         endDate: '<s:message code="financeSummary.representatives.endDate" javaScriptEscape="true" />',
         errors:financeErrors
       },
       referencesConfig:{
         url: '<s:url value="/rest/{subjectType}/{subjectId}/financialReferences"/>'
       },
       searchConfig:{
         url: '<s:url value="/rest/{subjectType}/{subjectId}/financialRepresentativeRelationship"/>'
       }
     };

   var healthConfig={
       labels:{
         errorStatus:'<s:message code="healthSummary.errorStatus" javaScriptEscape="true" />',
         errorTitle: '<s:message code="healthSummary.errorTitle" javaScriptEscape="true" />',
         errorDetail: '<s:message code="healthSummary.errorDetail" javaScriptEscape="true" />',
         serviceUnavailable: '<s:message code="healthSummary.serviceUnavailable" javaScriptEscape="true" />',
       },
       subject:subject,
       healthTabConfig:{
         url: '<s:url value="/rest/{subjectType}/{subjectId}/personHealthSummary"/>'
       }
   };
   
   var chargingConfig={
       subject:subject,
       labels:{
         header:{
           title:'<s:message code="financeSummary.charging.title" javaScriptEscape="true" />'
         }
       },
       residentialContribution:{
         labels:{
           header:{
             title:'<s:message code="financeSummary.charging.residentialContribution.title" javaScriptEscape="true" />'
           },
           filter:{
             activeOnly:'<s:message code="financeSummary.filter.activeOnly" javaScriptEscape="true" />',
             showAll: '<s:message code="financeSummary.filter.showAll" javaScriptEscape="true" />',
           },
           columns: {
             id: '<s:message code="financeSummary.charging.residentialContribution.id" javaScriptEscape="true" />',
             type: '<s:message code="financeSummary.charging.residentialContribution.type" javaScriptEscape="true" />',
             startDate: '<s:message code="financeSummary.charging.residentialContribution.startDate" javaScriptEscape="true" />',
             endDate: '<s:message code="financeSummary.charging.residentialContribution.endDate" javaScriptEscape="true" />',
             weeklyContribution: '<s:message code="financeSummary.charging.residentialContribution.weeklyContribution" javaScriptEscape="true" />'
           },
           weeklyContributionTotal:'<s:message code="financeSummary.charging.residentialContribution.weeklyContributionTotal" javaScriptEscape="true" />',
           errors:financeErrors
         },
         searchConfig:{
           url: '<s:url value="/rest/{subjectType}/{subjectId}/residentialContribution"/>'
         }
       },
       nonResidentialAssessment:{
         labels:{
           header:{
             title:'<s:message code="financeSummary.charging.nonResidentialAssessment.title" javaScriptEscape="true" />'
           },
           filter:{
             activeOnly:'<s:message code="financeSummary.filter.activeOnly" javaScriptEscape="true" />',
             showAll: '<s:message code="financeSummary.filter.showAll" javaScriptEscape="true" />',
           },
           columns: {
             id: '<s:message code="financeSummary.charging.nonResidentialAssessment.id" javaScriptEscape="true" />',
             fromDate: '<s:message code="financeSummary.charging.nonResidentialAssessment.fromDate" javaScriptEscape="true" />',
             toDate: '<s:message code="financeSummary.charging.nonResidentialAssessment.toDate" javaScriptEscape="true" />',
             availableIncome: '<s:message code="financeSummary.charging.nonResidentialAssessment.availableIncome" javaScriptEscape="true" />',
             totalPayable: '<s:message code="financeSummary.charging.nonResidentialAssessment.totalPayable" javaScriptEscape="true" />',
             overridden: '<s:message code="financeSummary.charging.nonResidentialAssessment.overridden" javaScriptEscape="true" />'
           },
           reasonForOverriding:'<s:message code="financeSummary.charging.nonResidentialAssessment.reasonForOverriding" javaScriptEscape="true" />',
           notes: '<s:message code="financeSummary.charging.nonResidentialAssessment.notes" javaScriptEscape="true" />',
         },
         searchConfig:{
           url: '<s:url value="/rest/{subjectType}/{subjectId}/nonResidentialAssessment"/>'
         }
       }
     };
   
   ReactDOM.render(React.createElement(uspSubjectSummary.WidgetFrame, {
     labels: {
       accessDenied: '<s:message code="subjectSummary.accessDenied" javaScriptEscape="true" />'
     },
     //height of fixed page headers
     fixedContentHeight: 137,
     pages: [{
       permissions:{
         canView:true
       },
       title: '<s:message code="personSummary.title" javaScriptEscape="true" />',
       widgetPool: [{
         key: 'chronoTimeline',
         type: uspSubjectSummary.ChronologyTimelineView
       }, {
         key: 'classifications',
         type: uspSubjectSummary.ClassificationCardView
       }, {
         key: 'relationships',
         type: uspSubjectSummary.RelationshipView
       }, {
         key: 'casenote',
         type: uspSubjectSummary.CaseNoteCardView
       }, {
         key: 'profile',
         type: uspSubjectSummary.ProfileView
       }, {
         key: 'events',
         type: uspSubjectSummary.EventCardView
       }],
       layout: {
         columns: 3,
         widgets: [{
           column: 0,
           widget: {
             key: 'profile',
             configuration: profileConfig,
             height: 4,
             permissions: permissions.profile
           }
         }, {
           column: 0,
           widget: {
             key: 'relationships',
             configuration: relationshipsConfig,
             height: 8,
             permissions: permissions.relationship
           }
         }, {
           column: 1,
           widget: {
             key: 'casenote',
             configuration: caseNoteConfig,
             height: 4,
             permissions: permissions.caseNote
           }
         }, {
           column: 1,
           widget: {
             key: 'events',
             configuration: eventsConfig,
             height: 4,
             permissions: permissions.event
           }
         }, {
           column: 1,
           widget: {
             key: 'classifications',
             configuration: classificationsConfig,
             height: 4,
             permissions: permissions.classification
           }
         }, {
           column: 2,
           widget: {
             key: 'chronoTimeline',
             configuration: timelineConfig,
             height: 12,
             permissions: permissions.chronology
           }
         }]
       }
     }, {
       permissions:{
         canView:permissions.finance.canView
       },
       title: '<s:message code="financeSummary.title" javaScriptEscape="true" />',
       widgetPool: [{
         key: 'serviceAgreements',
         type: uspSubjectSummary.ServiceAgreements
       },{
         key: 'personalBudget',
         type: uspSubjectSummary.PersonalBudget
       },{
         key: 'keyFinancialDetails',
         type: uspSubjectSummary.KeyFinancialDetails
       },{
         key: 'charging',
         type: uspSubjectSummary.Charging
       }],
       layout: {
         columns: 2,
         widgets: [{
           column: 0,
           widget: {
             key: 'keyFinancialDetails',
             configuration: keyFinancialDetailsConfig,
             height: 6,
             permissions: permissions.finance.keyFinancialDetails
           }
         },{
           column: 1,
           widget: {
             key: 'serviceAgreements',
             configuration: serviceAgreementsConfig,
             height: 6,
             permissions: permissions.finance.serviceAgreements
           }
         },{
           column: 0,
           widget: {
             key: 'personalBudget',
             configuration: personalBudgetConfig,
             height: 6,
             permissions: permissions.finance.personalBudget
           }
         },{
           column: 1,
           widget:{
             key: 'charging',
             configuration:chargingConfig,
             height:6,
             permissions: permissions.finance.charging
           }
         }]
       }
     },
     {
       permissions:{
         canView:permissions.health.canView
       },
       title: '<s:message code="healthSummary.title" javaScriptEscape="true" />',
       widgetPool: [{
         key: 'healthSummary',
         type: uspSubjectSummary.HealthView
       }],
       layout: {
         columns: 1,
         widgets: [{
           column: 0,
           widget: {
             key: 'healthSummary',
             permissions: permissions.health.healthSummary,
             configuration: healthConfig
           }
         }]
        }
     },
    ]
    }), document.getElementById('summary-widget-frame'));
  });
</script>


  </c:when>
  <c:otherwise>
    <%-- Insert access denied tile --%>
    <t:insertDefinition name="access.denied" />
  </c:otherwise>
</c:choose>
