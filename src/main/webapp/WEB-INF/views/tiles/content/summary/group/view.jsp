<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'Group','Group.View')" var="canViewGroup"/>
<sec:authorize access="hasPermission(${group.id}, 'SecuredGroupContext.GET','CaseNoteEntry.View')" var="canViewCaseNote"/>
<sec:authorize access="hasPermission(${group.id}, 'SecuredGroupContext.GET','ChronologyEntry.View')" var="canViewChronology"/>
<sec:authorize access="hasPermission(${group.id}, 'SecuredGroupContext.GET','PersonPersonRelationship.GET')" var="canViewRelationship"/>
<sec:authorize access="hasPermission(${group.id}, 'SecuredGroupContext.GET','FormInstance.View')" var="canViewForm"/>

<s:eval expression="@appSettings.isChronologyImpactEnabled()" var="impactEnabled" scope="page" />

<jsp:useBean id="now" class="java.util.Date"/>

<c:choose>
  <c:when test="${canViewGroup eq true}">

  <div class="yui3-app" id="summary-widget-frame"></div>
  <script>
   var subject = {
        subjectType: 'GROUP',
        subjectId: '<c:out value="${group.id}"/>'
   };

   var impactEnabled='${impactEnabled}'==='true';
   
   var permissions={
     profile:{
       canView:'${canViewGroup}'==='true',
       canViewForm:'${canViewForm}'==='true'
     },
     caseNote:{
       canView:'${canViewCaseNote}'==='true'
     },
     chronology:{
       canView:'${canViewChronology}'==='true'
     },
     relationship:{
       canView:'${canViewRelationship}'==='true'
     }
   }

   var timelineConfig={
     subject:subject,
     url: '<s:url value="/rest/{subjectType}/{subjectId}/chronologyEntry?s={sortBy}&statusList=COMPLETE&statusList=DRAFT&includeMemberEntries=true&pageNumber=1&pageSize=-1"><s:param name="sortBy" value="[{\"eventDate!calculatedDate\": \"desc\"}]" /></s:url>',
     rangeEndpoint:{
       url:'<s:url value="/rest/{subjectType}/{subjectId}/chronology"/>',
       headers: {
         'X-Requested-With': 'XMLHttpRequest',
         'Accept': 'application/vnd.olmgroup-usp.chronology.groupchronologywithentryrange+json'
       }
     },
     impactEnabled:impactEnabled,
     labels:{
       header:{
         title:'Chronology timeline:'
       },
       emptyTimeline:'There are no chronology entries.',
       selectPrompt:'Select a day to see entry details.',
       navigation:{
         first: 'first entry',
         next:'next entry',
         previous:'previous entry',
         last: 'last entry'
       },
       key:{
         collapse:'Hide key',
         expand:'Show key',
         noEntry:'No entry',
         positive:'Positive impact',
         neutral:'Neutral impact',
         negative:'Negative impact',
         unknown: 'Unknown impact',
       },
       control:{
         day: 'day',
         month: 'month',
         year: 'year'
       }
     }
   };

   
   var caseNoteConfig={
     subject:subject,
     url: '<s:url value="/rest/{subjectType}/{subjectId}/caseNoteEntry?statusList=COMPLETE&statusList=DRAFT&excludeGroupEntries=0&useSoundex=0&appendWildcard=0&pageSize={pageSize}&page={page}&entryTypeList={entryType}"/>',
     configurableTypeUrl: '<s:url value="/rest/caseNoteConfigurableTypeReference?pageNumber={page}&pageSize={pageSize}"/>',
     labels:{
       header:{
         title: '<s:message code="personsummary.casenote.header.title" javaScriptEscape="true" />'
       },
       noRecordsFound:'<s:message code="personsummary.casenote.noRecordFound" javaScriptEscape="true" />'
     }
   };
   
   
   var membersConfig={
     subject:subject,
     url: '<s:url value="/rest/group/${group.id}/personMembership?temporalStatusFilter=ACTIVE&pageNumber={page}&pageSize={pageSize}"/>',
     labels:{
       header:{
         title:'Active group members:'
       },
       joinDate:'Date joined:',
       noRecordsFound:'There are no active group members.'
     },
     personSummaryUrl:'<s:url value="/summary/person?id="/>',
     subjectPictureEndpoint: {
       url: '<s:url value="/rest/{subjectType}/{subjectId}/picture/content"/>'
     }
   };

   var profileConfig={
     url: '<s:url value="/rest/group/{subjectId}"/>',
     configurableTypeUrl: '<s:url value="/rest/formConfigurableTypeReference?teamId={teamId}"><s:param name="teamId" value="${currentUserTeam.securityTeam.teamId}" /></s:url>',
     currentUserTeamId: '${currentUserTeam.securityTeam.teamId}',
     subject:subject,
     formUrl: '<s:url value="/rest/formInstance/latest?groupId={subjectId}&formDefinitionUID={formDefinitionUID}&status=DRAFT&status=COMPLETE&status=SUBMITTED"/>',
     launchFormURL:'<s:url value="/forms/load?id={id}"/>',
     labels:{
       forms:{
         header:{
           title:'Latest forms:'
         },
         empty: 'not present.'
       }
     }
   };
   
   ReactDOM.render(React.createElement(uspSubjectSummary.WidgetFrame, {
     labels: {
       accessDenied: 'Sorry, you do not have permission to view this information.'
     },
     //height of fixed page headers
     fixedContentHeight: 137,
     pages: [{
       permissions:{
         canView:true
       },
       title: '<s:message code="groupSummary.title" javaScriptEscape="true" />',
       widgetPool: [{
         key: 'chronoTimeline',
         type: uspSubjectSummary.ChronologyTimelineView
       }, {
         key: 'casenote',
         type: uspSubjectSummary.CaseNoteCardView
       }, {
         key: 'members',
         type: uspSubjectSummary.MemberCardView
       }, {
         key: 'profile',
         type: uspSubjectSummary.ProfileView
       }],
       layout: {
         columns: 3,
         widgets: [{
           column: 0,
           widget: {
             key: 'profile',
             configuration: profileConfig,
             height: 4,
             permissions: permissions.profile
           }
         }, {
           column: 0,
           widget: {
             key: 'members',
             configuration: membersConfig,
             height: 8,
             permissions: permissions.relationship
           }
         }, {
           column: 1,
           widget: {
             key: 'casenote',
             configuration: caseNoteConfig,
             height: 12,
             permissions: permissions.caseNote
           }
         }, {
           column: 2,
           widget: {
             key: 'chronoTimeline',
             configuration: timelineConfig,
             height: 12,
             permissions: permissions.chronology
           }
         }]
       }
     }]
   }), document.getElementById('summary-widget-frame'));
</script>


  </c:when>
  <c:otherwise>
    <%-- Insert access denied tile --%>
    <t:insertDefinition name="access.denied" />
  </c:otherwise>
</c:choose>
