<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- Define permissions --%> 
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.View')" var="canView"/>

<c:choose>
    <c:when test="${canView eq true}">         
        <script>
        Y.use('yui-base',
              'prform-print',
              'usp-resolveassessment-PlacementReport',
              function(Y){
            
            var model=new Y.usp.resolveassessment.PlacementReport({
				id:'${formId}',
				url: '<s:url value="/rest/placementReport/{id}"/>'
			}),
			viewLabels={
    		    background: {
    		        dateStarted: '<s:message code="placementReportVO.dateStarted"/>',
    		        name: '<s:message code="placementReportVO.subject.name"/>',
    		        dateOfBirth: '<s:message code="placementReportVO.subject.dateOfBirth"/>',
    		        placementAddress: '<s:message code="placementReportVO.placementAddress"/>',
    		        background: '<s:message code="placementReportVO.background"/>'
    		    },
    		    health: {
    		        health: '<s:message code="placementReportVO.health"/>',
    		        dietAndPhysicalHealth: '<s:message code="placementReportVO.dietAndPhysicalHealth"/>',
    		        mentalHealth: '<s:message code="placementReportVO.mentalHealth"/>'
    		    },
    		    medication: {
    		        medication: '<s:message code="placementReportVO.medication"/>',
    		        optician: '<s:message code="placementReportVO.optician"/>',
    		        dental: '<s:message code="placementReportVO.dental"/>'
    		    },
    		    gp: {
    		        healthPromotion: '<s:message code="placementReportVO.healthPromotion"/>',
    		        gpAppointmentsAndTelephoneContact: '<s:message code="placementReportVO.gpAppointmentsAndTelephoneContact"/>'
    		    },
    		    educationEmployment: {
    		        education: '<s:message code="placementReportVO.education"/>',
    		        employment: '<s:message code="placementReportVO.employment"/>'
    		    },
    		    social: {
    		        socialPresentationAndBehaviour: '<s:message code="placementReportVO.socialPresentationAndBehaviour"/>'
    		    },
    		    skills: {
    		        selfCareSkills: '<s:message code="placementReportVO.selfCareSkills"/>',
    		        independentLivingSkills: '<s:message code="placementReportVO.independentLivingSkills"/>'
    		    },
    		    identity: {
    		        identityAndContactWithFamily: '<s:message code="placementReportVO.identityAndContactWithFamily"/>',
    		        safeguarding: '<s:message code="placementReportVO.safeguarding"/>'
    		    },
    		    leisure: {
    		        leisureActivitiesCommunityAccess: '<s:message code="placementReportVO.leisureActivitiesCommunityAccess"/>',
    		        finance: '<s:message code="placementReportVO.finance"/>'
    		    },
    		    contacts: {
    		        forensicCommunityNurseContact: '<s:message code="placementReportVO.forensicCommunityNurseContact"/>',
    		        socialWorkerContact: '<s:message code="placementReportVO.socialWorkerContact"/>'
    		    },
    		    summary: {
    		        summaryAndAnalysis: '<s:message code="placementReportVO.summaryAndAnalysis"/>'
    		    },
    		    rejection: {
    		        rejectionReason: '<s:message code="rejectFormVO.rejectionReason"/>',
    		        rejectionDate: '<s:message code="rejectFormVO.rejectionDate"/>',
    		        rejectedBy: '<s:message code="rejectFormVO.rejector.name"/>',
                    noRejections:'<s:message code="resolve.view.form.rejection.noRejections"/>' 
    		    }
    		},
    		printView=new Y.app.PRFormPrintView({
                container:'body',
               	model:model,
               	labels:viewLabels
           });
           
           model.load(function(){
               printView.render();
           });
        });
		</script>
		
		<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />
		
    </c:when>
    <c:otherwise>
        <%-- Insert access denied tile --%>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>

