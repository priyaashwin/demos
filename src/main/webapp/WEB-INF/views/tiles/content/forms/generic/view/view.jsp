<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
 <sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Submit')" var="canSubmit"/>
 <sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Complete')" var="canComplete"/>
 <sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Authorise')" var="canAuthorise"/>
</c:if>

<%-- Read the max upload size from the properties file --%>
<s:eval expression="@appSettings.getMaxUploadSize()" var="maxUploadSize" scope="page" />
<s:eval expression="@appSettings.isNotificationEventHandlerEnabled()" var="notificationEventHandlerEnabled" scope="page" />
<s:eval expression="@appSettings.isCaseloadSubscriptionMatchersEnabled()" var="caseloadSubscriptionMatchersEnabled" scope="page" />

 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.View')" var="canView"/>
<c:choose>
	<c:when test="${canView eq true}">
		<div id="forms-engine"></div>
	
		<script>
		//Create a sandbox with the forms-engine available
		//We pre-load this sandbox with any dependencies required by the forms component
Y.use('info-message', 'template-micro',
    function(Y) {
         var MicroSyntax = {
          code: /\{\{%([\s\S]+?)%\}\}/g,
          escapedOutput: /\{\{(?!%)([\s\S]+?)\}\}/g,
          rawOutput: /\{\{\{([\s\S]+?)\}\}\}/g
        };
        var personSuggestionTemplate=Y.Template.Micro.compile('<div title="{{this.name}}"><div class="identifier"><span>{{this.name}} ({{this.personIdentifier}})</span></div></div>', MicroSyntax);
        var organisationSuggestionTemplate=Y.Template.Micro.compile('<div title="{{this.name}}"><div class="identifier"><span>{{this.name}} ({{this.organisationIdentifier}})</span></div></div>', MicroSyntax);
        
        var notificationEnabled='${notificationEventHandlerEnabled}'==='true' && '${caseloadSubscriptionMatchersEnabled}'==='true';
        
        var statusButtonPermissions = {
          canSubmit:'${canSubmit}'==='true',
          canComplete:'${canComplete}'==='true',
          canAuthorise:'${canAuthorise}'==='true'
        };

        //render the component
       uspFormEditor.renderEditor(document.getElementById('forms-engine'), {
                formId: '${formId}',
                formDefinitionId: '${formDefinitionId}',
                controlId: '${controlId}',
                memberId: '${memberId}',
                statusButtonPermissions: statusButtonPermissions,
                labels: {
                    //TODO - get these from a resource bundle
                    title: '<s:message code="form.editor.title" javaScriptEscape="true"/>',
                    name: '<s:message code="form.editor.name" javaScriptEscape="true"/>',
                    page: '<s:message code="form.editor.page" javaScriptEscape="true"/>',
                    section: '<s:message code="form.editor.section" javaScriptEscape="true"/>',
                    exit: '<s:message code="form.editor.exit" javaScriptEscape="true"/>',
                    exitAlt: '<s:message code="form.editor.exit.title" javaScriptEscape="true"/>',
                    submit: '<s:message code="form.editor.submit" javaScriptEscape="true"/>',
                    submitAlt: '<s:message code="form.editor.submit.title" javaScriptEscape="true"/>',
                    complete: '<s:message code="form.editor.complete" javaScriptEscape="true"/>',
                    completeAlt: '<s:message code="form.editor.complete.title" javaScriptEscape="true"/>',
                    authorise: '<s:message code="form.editor.authorise" javaScriptEscape="true"/>',
                    authoriseAlt: '<s:message code="form.editor.authorise.title" javaScriptEscape="true"/>',
                    next: '<s:message code="form.editor.next" javaScriptEscape="true"/>',
                    nextAlt: '<s:message code="form.editor.next.title" javaScriptEscape="true"/>',
                    save: '<s:message code="form.editor.save" javaScriptEscape="true"/>',
                    saveAlt: '<s:message code="form.editor.save.title" javaScriptEscape="true"/>',
                    cancel: '<s:message code="form.editor.cancel" javaScriptEscape="true"/>',
                    cancelAlt: '<s:message code="form.editor.cancel.title" javaScriptEscape="true"/>',
                    close: '<s:message code="form.editor.menu.close" javaScriptEscape="true"/>',
                    closeAlt: '<s:message code="form.editor.menu.close.title" javaScriptEscape="true"/>',
                    open: '<s:message code="form.editor.menu.open" javaScriptEscape="true"/>',
                    openAlt: '<s:message code="form.editor.menu.open.title" javaScriptEscape="true"/>',
                    previous: '<s:message code="form.editor.previous" javaScriptEscape="true"/>',
                    previousAlt: '<s:message code="form.editor.previous.title" javaScriptEscape="true"/>',
                    notificationTrigger:'<s:message code="form.editor.notificationTrigger" javaScriptEscape="true"/>',
                    notificationTriggerAlt:'<s:message code="form.editor.notificationTrigger.title" javaScriptEscape="true"/>',
                    contents: '<s:message code="form.editor.contents" javaScriptEscape="true"/>',
                    edit: {
                        section: '<s:message code="form.editor.edit.section" javaScriptEscape="true"/>'
                    },
                    exitDialog: {
                        title: '<s:message code="form.editor.exit.confirm.title" javaScriptEscape="true"/>',
                        confirm: '<s:message code="form.editor.exit.confirm.message" javaScriptEscape="true"/>',
                        confirmUnsaved: '<s:message code="form.editor.exit.confirm.unsaved.message" javaScriptEscape="true"/>',
                        ok: '<s:message code="button.yes" javaScriptEscape="true"/>',
                        cancel: '<s:message code="button.no" javaScriptEscape="true"/>'
                    },
                    submitDialog: {
                        title: '<s:message code="form.editor.submit.confirm.title" javaScriptEscape="true"/>',
                        confirm: '<s:message code="form.editor.submit.confirm.message" javaScriptEscape="true"/>',
                        confirmUnsaved: '<s:message code="form.editor.submit.confirm.unsaved.message" javaScriptEscape="true"/>',
                        ok: '<s:message code="form.editor.submit.confirm.button.submit" javaScriptEscape="true"/>',
                        cancel: '<s:message code="form.editor.submit.confirm.button.cancel" javaScriptEscape="true"/>'
                    },
                    completeDialog: {
                        title: '<s:message code="form.editor.complete.confirm.title" javaScriptEscape="true"/>',
                        confirm: '<s:message code="form.editor.complete.confirm.message" javaScriptEscape="true"/>',
                        confirmUnsaved: '<s:message code="form.editor.complete.confirm.unsaved.message" javaScriptEscape="true"/>',
                        ok: '<s:message code="form.editor.complete.confirm.button.complete" javaScriptEscape="true"/>',
                        cancel: '<s:message code="form.editor.complete.confirm.button.cancel" javaScriptEscape="true"/>'
                    },
                    authoriseDialog: {
                        title: '<s:message code="form.editor.authorise.confirm.title" javaScriptEscape="true"/>',
                        confirm: '<s:message code="form.editor.authorise.confirm.message" javaScriptEscape="true"/>',
                        confirmUnsaved: '<s:message code="form.editor.authorise.confirm.unsaved.message" javaScriptEscape="true"/>',
                        ok: '<s:message code="form.editor.authorise.confirm.button.authorise" javaScriptEscape="true"/>',
                        cancel: '<s:message code="form.editor.authorise.confirm.button.cancel" javaScriptEscape="true"/>'
                    },
                    notificationDialog:{
                      title: '<s:message code="notification.name" javaScriptEscape="true"/>',
                      ok: '<s:message code="button.send" javaScriptEscape="true"/>',
                      cancel: '<s:message code="button.cancel" javaScriptEscape="true"/>',
                      recipients:'<s:message code="notifications.recipients" javaScriptEscape="true"/>',
                      includeDirectTargets:'<s:message code="notifications.includeDirectTargets" javaScriptEscape="true"/>',
                      removeRecipient:'<s:message code="notifications.recipients.remove" javaScriptEscape="true"/>',
                      triggerChecklistInstance:'<s:message code="form.editor.triggerChecklistInstance" javaScriptEscape="true"/>',
                      triggerChecklistTitle:'<s:message code="form.editor.triggerChecklistTitle" javaScriptEscape="true"/>',
                      add:'<s:message code="notifications.recipient.add" javaScriptEscape="true"/>',
                      narrative:{
                        summary:'<s:message code="form.editor.notification.summary" javaScriptEscape="true"/>'  
                      },
                      personAutocomplete:{
                        placeholder: '<s:message code="notifications.recipients.user.autocomplete.placeholder" javaScriptEscape="true"/>',
                      },
                      organisationAutocomplete:{
                        placeholder: '<s:message code="notifications.recipients.team.autocomplete.placeholder" javaScriptEscape="true"/>'
                      },
                      notificationSent:'<s:message code="notification.sent" javaScriptEscape="true"/>'
                    }
                }
            }, {
                serverConfig: {
                    formInstanceEndpoint: {
                        url: '<s:url value="/rest/formInstance/{id}"/>'
                    },
                    formContentEndpoint: {
                        url: '<s:url value="/rest/formInstance/{id}/content"/>'
                    },
                    formContentChangesEndpoint: {
                        url: '<s:url value="/rest/formInstance/{id}/contentChanges"/>'
                    },
                    formDefinitionEndpoint: {
                        url: '<s:url value="/rest/formDefinition/{id}"/>'
                    },
                    formSectionPermissionsEndpoint: {
                        url: '<s:url value="/rest/formInstance/{id}/sectionPermissions"/>'
                    },
                    formControlContentEndpoint: {
                        url: '<s:url value="/rest/formInstance/{id}/controlContent?controlLocalId={controlLocalId}"/>'
                    },
                    formSectionContentEndpoint: {
                        url: '<s:url value="/rest/formInstance/{id}/content"/>'
                    }
                },
                mediaConfig: {
                    formInstanceId: '${formId}',
                    mediaURI: '<s:url value="/rest/formInstanceAttachment/{attachmentId}/content?formInstanceId={id}"/>',
                    uploadURI: '<s:url value="/rest/formInstance/{id}/attachment"/>',
                    maxUploadSize: Number('${maxUploadSize}')
                },
                searchControlConfig: {
                    searchControlEndpoint: {
                        url: '<s:url value="/rest/formSearchControlResult"/>',
                        params: {
                          searchControlType:'',
                          keyword:''
                        }
                    },
                    searchControlContentEndpoint: {
                      url: '<s:url value="/rest/formSearchControlResult/content"/>'
                    },
                    autocompleteConfig: {
                      queryParameter: 'keyword'
                    }
                },
                notificationConfig:{
                  notificationEnabled:notificationEnabled,
                  sendNotificationEndpoint:{
                    url: '<s:url value="/rest/formInstance/{id}/status"/>'
                  },
                  personAutocompleteConfig: {
                    queryParameter: 'nameOrUsername',
                    selectionFormatter: function(selection){
                      return personSuggestionTemplate(selection);
                    },
                    suggestionFormatter: function(suggestion){
                      return personSuggestionTemplate(suggestion);
                    },
                    suggestionsLocator: function(response){return (response||{}).results||[]}
                  },
                  organisationAutocompleteConfig: {
                    queryParameter: 'nameOrOrgIdOrPrevName',
                    selectionFormatter: function(selection){
                      return organisationSuggestionTemplate(selection);
                    },
                    suggestionFormatter: function(suggestion){
                      return organisationSuggestionTemplate(suggestion);
                    },
                    suggestionsLocator: function(response){return (response||{}).results||[]}
                  },
                  personAutocompleteEndpoint: {
                    url: '<s:url value="/rest/person?s={sortBy}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
                    params: {
                      personType: 'professional',
                      nameOrUsername:'',
                      pageSize:50
                    }
                  },
                  organisationAutocompleteEndpoint: {
                    url: '<s:url value="/rest/organisation"/>',
                    params: {
                      byOrganisationType:true,
                      organisationTypes:'TEAM',
                      nameOrOrgIdOrPrevName:'',
                      pageSize:50,
                      escapeSpecialCharacters:true,
                      appendWildcard:true
                    }
                  }
                }
            },
            //Pass configured YUI instance
            Y);

        new Y.usp.InfoMessage({
            //CSS selector to the node where messages are to be shown
            srcNode: '#contentMessagesPanel'
        }).render();


    });
		</script>

		<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />
	</c:when>
	<c:otherwise>
		<%-- Insert access denied tile --%>
		<t:insertDefinition name="access.denied"/>
	</c:otherwise>
</c:choose>
