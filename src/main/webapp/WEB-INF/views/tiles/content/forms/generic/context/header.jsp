<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<tilesx:useAttribute name="title" ignore="true" />
<h3 tabindex="0">
  <span class="context-title"><s:message code="${title}" text="${title}"/></span>
  <span id="formResultsCount" style="padding-left:0;"></span>
</h3>