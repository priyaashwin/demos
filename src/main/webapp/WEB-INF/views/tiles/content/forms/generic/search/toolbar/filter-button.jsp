<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="first" ignore="true"/>
<tilesx:useAttribute name="last" ignore="true"/>

<c:if test="${first eq true}">
  <c:set var="fStyle" value="first"/>
</c:if>

<c:if test="${last eq true}">
  <c:set var="lStyle" value="last"/>
</c:if>

<c:choose>
  <c:when test="${not empty group}">
    <c:set var="name" value="${group.name}"/>
  </c:when>
  <c:otherwise>
    <c:set var="name" value="${person.name}"/>
  </c:otherwise>
</c:choose>
  

<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
  <a href="#none" class="pure-button pure-button-disabled usp-fx-all filter pure-button-loading"
    title='<s:message code="forms.filter.button" arguments="${name}" htmlEscape="true" />' aria-disabled="true"
    aria-label='<s:message code="forms.filter.button" arguments="${name}" htmlEscape="true" />' tabindex="0">
    <i class="fa fa-filter"></i>
    <span><s:message code="button.filter" /></span>
  </a>
</li>