<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- Define permissions --%> 
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.Update')" var="canUpdate"/>
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.Reject')" var="canUpdateRejection"/>
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.Archive')" var="canDelete"/>
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.Authorise')" var="canApprove"/>
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.Reject')" var="canReject"/>
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.Complete')" var="canComplete"/>
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.Uncomplete')" var="canReopen"/>

<c:choose>
    <%-- if the subject context for the page is person --%>
    <c:when test="${!empty person}">
        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Add')" var="canAddWarning"/>
        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Update')" var="canUpdateWarning"/>
    </c:when>
    <%-- if the subject context for the page is group i.e. ${!empty group}"> --%>
    <c:otherwise>
        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Add')" var="canAddWarning"/>
        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Update')" var="canUpdateWarning"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${canView eq true}"> 
        <div id="formTabs" />
        
        <script>
    Y.use('yui-base',
            'form-content-context',
            'intakeform-tabs',
            'tabbed-page-contents-plugin',
            'usp-resolveassessment-IntakeFormFullDetails',
            'usp-resolveassessment-IntakeAppendixOneFullDetails',
            'usp-resolveassessment-IntakeAppendixTwoFullDetails',
            'usp-resolveassessment-IntakeAppendixThreeFullDetails',
            'form-status-buttons',
            'usp-person-PersonWithWarning',
            function(Y) {

                Y.namespace('app').IntakeForm = Y.Base.create("IntakeForm", Y.usp.resolveassessment.IntakeFormFullDetails, [], {}, {
                    ATTRS: {
                        canUpdatePermission: 'initOnly',
                        canUpdateRejectionPermission: 'initOnly',
                        canUpdate: {
                            value: false,
                            writeOnce: 'initOnly',
                            getter: function() {
                                return !this.get('canUpdatePermission') ? false : this.get('status') === STATE_DRAFT;
                            }
                        },
                        canUpdateRejection: {
                            value: false,
                            writeOnce: 'initOnly',
                            getter: function() {
                                return !this.get('canUpdateRejectionPermission') ? false : this.get('status') === STATE_DRAFT;
                            }
                        }
                    }
                });

                var STATE_DRAFT = 'DRAFT',
                    //load relationship types (familial)
                    familialRelationshipTypes = new Y.usp.relationship.PaginatedFamilialRelationshipTypeList({
                        url: '<s:url value="/rest/familialRelationshipType?pageNumber=1&pageSize=-1"/>'
                    }).load(),
                    //load relationship types (professional)
                    professionalRelationshipTypes = new Y.usp.relationship.PaginatedProfessionalRelationshipTypeList({
                        url: '<s:url value="/rest/professionalRelationshipType?pageNumber=1&pageSize=-1"/>'
                    }).load(),
                    formModel = new Y.app.IntakeForm({
                        id: '${formId}',
                        url: '<s:url value="/rest/intakeForm/{id}"/>',
                        canUpdatePermission: '${canUpdate}'==='true',
                        canUpdateRejectionPermission: '${canUpdateRejection}'==='true'
                    }),
                    appendix1Model = new Y.usp.resolveassessment.IntakeAppendixOneFullDetails({
                        url: '<s:url value="/rest/intakeForm/${formId}/intakeAppendixOne"/>'
                    }),
                    appendix2Model = new Y.usp.resolveassessment.IntakeAppendixTwoFullDetails({
                        url: '<s:url value="/rest/intakeForm/${formId}/intakeAppendixTwo"/>'
                    }),
                    appendix3Model = new Y.usp.resolveassessment.IntakeAppendixThreeFullDetails({
                        url: '<s:url value="/rest/intakeForm/${formId}/intakeAppendixThree"/>',
                    }),
                    context = new Y.app.FormContentContext({
                        container: Y.one('#formContext'),
                        model: formModel,
                        personModel: new Y.usp.person.PersonWithWarning({
                            id: '<c:out value="${person.id}"/>',
                            personIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                            name: '${esc:escapeJavaScript(person.name)}',
                            url: '<s:url value="/rest/person/{id}"/>'
                        }).load(),
                        labels: {
                           <c:choose>
                             <c:when test="${canAddWarning} or ${canUpdateWarning}">
                           warningTitle:'<s:message code="button.hovermanagewarnings" javaScriptEscape="true"/>',
                           </c:when>
                           <c:otherwise>
                           warningTitle:'<s:message code="button.hoverviewwarnings" javaScriptEscape="true"/>',
                             </c:otherwise>
                         </c:choose> 
                            formView: '<s:message code="resolve.view.context.title" javaScriptEscape="true"/>',
                            formType: '<s:message code="resolve.view.context.intakeform.type" javaScriptEscape="true"/>',
                            startDate: '<s:message code="resolve.view.context.startDate" javaScriptEscape="true"/>'
                        },
                        canAddWarning: '${canAddWarning}'==='true',
                        canUpdateWarning: '${canUpdateWarning}'==='true'
                    }),
                    viewLabels = {
                        referencedPerson: {
                            name: '<s:message code="intakeReferencedPersonVO.name" javaScriptEscape="true"/>',
                            relationship: '<s:message code="intakeReferencedPersonVO.relationship" javaScriptEscape="true"/>',
                            address: '<s:message code="intakeReferencedPersonVO.address" javaScriptEscape="true"/>',
                            contact: '<s:message code="intakeReferencedPersonVO.contact" javaScriptEscape="true"/>',
                            //buttons
                            editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                        },
                        client: {
                            information: {
                                name: '<s:message code="intakeFormFullDetailsVO.subject.name" javaScriptEscape="true"/>',
                                dateOfBirth: '<s:message code="intakeFormFullDetailsVO.subject.dateOfBirth" javaScriptEscape="true"/>',
                                nhsNumber: '<s:message code="intakeFormFullDetailsVO.subject.nhsNumber" javaScriptEscape="true"/>',
                                address: '<s:message code="intakeFormFullDetailsVO.placementAddress.location" javaScriptEscape="true"/>',
                                contact: '<s:message code="intakeFormFullDetailsVO.subject.contact" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            consent: {
                                consentToShare: '<s:message code="intakeFormFullDetailsVO.consent.consentToShare" javaScriptEscape="true"/>',
                                consentToShareDetails: '<s:message code="intakeFormFullDetailsVO.consent.consentToShareDetails" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        placement: {
                            briefHistory: {
                                description: '<s:message code="intakeFormFullDetailsVO.history.briefHistory" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            history: {
                                startDate: '<s:message code="intakePreviousPlacementVO.startDate" javaScriptEscape="true"/>',
                                endDate: '<s:message code="intakePreviousPlacementVO.endDate" javaScriptEscape="true"/>',
                                location: '<s:message code="intakePreviousPlacementVO.location" javaScriptEscape="true"/>',
                                type: '<s:message code="intakePreviousPlacementVO.type" javaScriptEscape="true"/>',
                                contact: '<s:message code="intakePreviousPlacementVO.contact" javaScriptEscape="true"/>',
                                endReason: '<s:message code="intakePreviousPlacementVO.endReason" javaScriptEscape="true"/>',
                                //buttons
                                remove: '<s:message code="intakePreviousPlacementVO.results.remove" javaScriptEscape="true"/>',
                                edit: '<s:message code="intakePreviousPlacementVO.results.edit" javaScriptEscape="true"/>',
                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>'
                            },
                            currentPlacement: {
                                startDate: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementStartDate" javaScriptEscape="true"/>',
                                endDate: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementEndDate" javaScriptEscape="true"/>',
                                location: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementLocation" javaScriptEscape="true"/>',
                                type: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementType" javaScriptEscape="true"/>',
                                contact: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementContact" javaScriptEscape="true"/>',
                                challenges: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementChallenges" javaScriptEscape="true"/>',
                                clientViews: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementClientViews" javaScriptEscape="true"/>',
                                assessmentPresentation: '<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementAssessmentPresentation" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            identifiedRisks: {
                                risk: '<s:message code="intakeRiskAssessmentVO.risk" javaScriptEscape="true"/>',
                                likelihood: '<s:message code="intakeRiskAssessmentVO.likelihood" javaScriptEscape="true"/>',
                                impact: '<s:message code="intakeRiskAssessmentVO.impact" javaScriptEscape="true"/>',
                                total: '<s:message code="intakeRiskAssessmentVO.total" javaScriptEscape="true"/>',
                                riskManagementActivity: '<s:message code="intakeRiskAssessmentVO.riskManagementActivity" javaScriptEscape="true"/>',
                                revisedLikelihood: '<s:message code="intakeRiskAssessmentVO.revisedLikelihood" javaScriptEscape="true"/>',
                                revisedImpact: '<s:message code="intakeRiskAssessmentVO.revisedImpact" javaScriptEscape="true"/>',
                                revisedTotal: '<s:message code="intakeRiskAssessmentVO.revisedTotal" javaScriptEscape="true"/>',
                                date: '<s:message code="intakeRiskAssessmentVO.riskDate" javaScriptEscape="true"/>',
                                //buttons
                                remove: '<s:message code="intakeRiskAssessmentVO.results.remove" javaScriptEscape="true"/>',
                                edit: '<s:message code="intakeRiskAssessmentVO.results.edit" javaScriptEscape="true"/>',
                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>'
                            }
                        },
                        referral: {
                            referrer: {
                                referrerName: '<s:message code="intakeFormFullDetailsVO.referral.referrerName" javaScriptEscape="true"/>',
                                referrerRole: '<s:message code="intakeFormFullDetailsVO.referral.referrerRole" javaScriptEscape="true"/>',
                                referralReason: '<s:message code="intakeFormFullDetailsVO.referral.referralReason" javaScriptEscape="true"/>',
                                referralDate: '<s:message code="intakeFormFullDetailsVO.referral.referralDate" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            future: {
                                clientPlan: '<s:message code="intakeFormFullDetailsVO.future.futurePlanOfClient" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            interventionAndSupportPlans: {
                                action: '<s:message code="intakeSupportPlanVO.interventionAndSupport" javaScriptEscape="true"/>',
                                desiredOutcome: '<s:message code="intakeSupportPlanVO.desiredOutcome" javaScriptEscape="true"/>',
                                //buttons
                                remove: '<s:message code="intakeSupportPlanVO.results.remove" javaScriptEscape="true"/>',
                                edit: '<s:message code="intakeSupportPlanVO.results.edit" javaScriptEscape="true"/>',
                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>'
                            }
                        },
                        health: {
                            health: {
                                height: '<s:message code="intakeFormFullDetailsVO.health.height" javaScriptEscape="true"/>',
                                weight: '<s:message code="intakeFormFullDetailsVO.health.weight" javaScriptEscape="true"/>',
                                lastAnnualHealthCheck: '<s:message code="intakeFormFullDetailsVO.health.lastAnnualHealthCheck" javaScriptEscape="true"/>',
                                medicalHistory: '<s:message code="intakeFormFullDetailsVO.health.medicalHistory" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            gp: {
                                name: '<s:message code="intakeFormFullDetailsVO.gp.name" javaScriptEscape="true"/>',
                                address: '<s:message code="intakeFormFullDetailsVO.gp.address" javaScriptEscape="true"/>',
                                contact: '<s:message code="intakeFormFullDetailsVO.gp.contact" javaScriptEscape="true"/>'
                            },
                            medications: {
                                name: '<s:message code="intakeMedicationsVO.name" javaScriptEscape="true"/>',
                                dosage: '<s:message code="intakeMedicationsVO.dosage" javaScriptEscape="true"/>',
                                form: '<s:message code="intakeMedicationsVO.form" javaScriptEscape="true"/>',
                                prescribedBy: '<s:message code="intakeMedicationsVO.prescribedBy" javaScriptEscape="true"/>',
                                usedFor: '<s:message code="intakeMedicationsVO.usedFor" javaScriptEscape="true"/>',
                                //buttons
                                remove: '<s:message code="intakeMedicationsVO.results.remove" javaScriptEscape="true"/>',
                                edit: '<s:message code="intakeMedicationsVO.results.edit" javaScriptEscape="true"/>',
                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>'
                            },
                            healthActionPlan: {
                                healthActionPlan: '<s:message code="intakeFormFullDetailsVO.medication.healthActionPlan" javaScriptEscape="true"/>',
                                recentChanges: '<s:message code="intakeFormFullDetailsVO.medication.recentChanges" javaScriptEscape="true"/>',
                                compliance: '<s:message code="intakeFormFullDetailsVO.medication.compliance" javaScriptEscape="true"/>',
                                supportRequired: '<s:message code="intakeFormFullDetailsVO.medication.supportRequired" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            lifestyle: {
                                alcoholConsumption: '<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleAlcoholConsumption" javaScriptEscape="true"/>',
                                alcoholDetails: '<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleAlcoholDetails" javaScriptEscape="true"/>',
                                smoking: '<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleSmoking" javaScriptEscape="true"/>',
                                smokingDetails: '<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleSmokingDetails" javaScriptEscape="true"/>',
                                drugs: '<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleDrugs" javaScriptEscape="true"/>',
                                drugsDetails: '<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleDrugsDetails" javaScriptEscape="true"/>',
                                supportRequired: '<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleSupportRequired" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        mentalHealth: {
                            mentalHealth: {
                                description: '<s:message code="intakeFormFullDetailsVO.mentalHealth.mentalHealthDescription" javaScriptEscape="true"/>',
                                sleepPattern: '<s:message code="intakeFormFullDetailsVO.mentalHealth.mentalHealthSleepPattern" javaScriptEscape="true"/>',
                                appetite: '<s:message code="intakeFormFullDetailsVO.mentalHealth.mentalHealthAppetite" javaScriptEscape="true"/>',
                                behaviour: '<s:message code="intakeFormFullDetailsVO.mentalHealth.behaviourDescription" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            psychology: {
                                benefitFromTherapy: '<s:message code="intakeFormFullDetailsVO.psychology.psychologyBenefitFromTherapy" javaScriptEscape="true"/>',
                                details: '<s:message code="intakeFormFullDetailsVO.psychology.psychologyDetails" javaScriptEscape="true"/>',
                                previousInput: '<s:message code="intakeFormFullDetailsVO.psychology.psychologyPreviousInput" javaScriptEscape="true"/>',
                                unresolvedAbuseIssues: '<s:message code="intakeFormFullDetailsVO.psychology.psychologyUnresolvedAbuseIssues" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        physio: {
                            mobility: {
                                recentIssues: '<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilityRecentIssues" javaScriptEscape="true"/>',
                                recentTripsOrFalls: '<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilityRecentTripsOrFalls" javaScriptEscape="true"/>',
                                tripsOrFallsMoreRegular: '<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilityTripsMoreRegular" javaScriptEscape="true"/>',
                                slipsAssessment: '<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilitySlipsAssessment" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            occupationalIssues: {
                                mobilityProblems: '<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otMobilityProblems" javaScriptEscape="true"/>',
                                utilityApplicationIssues: '<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otUtilityApplicationIssues" javaScriptEscape="true"/>',
                                aidsAndAdaptations: '<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otAidsAndAdaptations" javaScriptEscape="true"/>',
                                recentProblems: '<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otRecentProblems" javaScriptEscape="true"/>',

                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        speech: {
                            issues: {
                                speechCommunicationIssues: '<s:message code="intakeFormFullDetailsVO.speechAndLanguage.speechCommunicationIssues" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            eating: {
                                specialDiet: '<s:message code="intakeFormFullDetailsVO.speechAndLanguage.specialDiet" javaScriptEscape="true"/>',
                                problemsEating: '<s:message code="intakeFormFullDetailsVO.speechAndLanguage.problemsEating" javaScriptEscape="true"/>',
                                mealtimeIssues: '<s:message code="intakeFormFullDetailsVO.speechAndLanguage.mealtimeIssues" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        needs: {
                            personalCare: {
                                need: '<s:message code="intakePersonalCareNeedVO.need" javaScriptEscape="true"/>',
                                helpRequired: '<s:message code="intakePersonalCareNeedVO.helpRequired" javaScriptEscape="true"/>',
                                //buttons
                                remove: '<s:message code="intakePersonalCareNeedVO.results.remove" javaScriptEscape="true"/>',
                                edit: '<s:message code="intakePersonalCareNeedVO.results.edit" javaScriptEscape="true"/>',
                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>'
                            },
                            spiritualNeeds: {
                                needs: '<s:message code="intakeFormFullDetailsVO.spiritual.spiritualNeeds" javaScriptEscape="true"/>',
                                needsBeingMet: '<s:message code="intakeFormFullDetailsVO.spiritual.spiritualNeedsBeingMet" javaScriptEscape="true"/>',
                                needsSupportRequired: '<s:message code="intakeFormFullDetailsVO.spiritual.spiritualNeedsSupportRequired" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            social: {
                                monday: '<s:message code="resolve.view.intakeform.timetable.monday" javaScriptEscape="true"/>',
                                tuesday: '<s:message code="resolve.view.intakeform.timetable.tuesday" javaScriptEscape="true"/>',
                                wednesday: '<s:message code="resolve.view.intakeform.timetable.wednesday" javaScriptEscape="true"/>',
                                thursday: '<s:message code="resolve.view.intakeform.timetable.thursday" javaScriptEscape="true"/>',
                                friday: '<s:message code="resolve.view.intakeform.timetable.friday" javaScriptEscape="true"/>',
                                saturday: '<s:message code="resolve.view.intakeform.timetable.saturday" javaScriptEscape="true"/>',
                                sunday: '<s:message code="resolve.view.intakeform.timetable.sunday" javaScriptEscape="true"/>',
                                morning: '<s:message code="resolve.view.intakeform.timetable.morning" javaScriptEscape="true"/>',
                                afternoon: '<s:message code="resolve.view.intakeform.timetable.afternoon" javaScriptEscape="true"/>',
                                evening: '<s:message code="resolve.view.intakeform.timetable.evening" javaScriptEscape="true"/>',
                                currentArrangementsIssues: '<s:message code="intakeFormFullDetailsVO.social.socialCurrentArrangementsIssues" javaScriptEscape="true"/>',
                                supportRequired: '<s:message code="intakeFormFullDetailsVO.social.socialSupportRequired" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            socialSupport: {
                                support: '<s:message code="intakeSupportVO.support" javaScriptEscape="true"/>',
                                details: '<s:message code="intakeSupportVO.details" javaScriptEscape="true"/>',
                                //buttons
                                remove: '<s:message code="intakeSupportVO.results.remove" javaScriptEscape="true"/>',
                                edit: '<s:message code="intakeSupportVO.results.edit" javaScriptEscape="true"/>',
                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>'
                            }
                        },
                        app1Comms: {
                            healthCommunication: {
                                communicationVerbal: '<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationVerbal" javaScriptEscape="true"/>',
                                communicationSignsAndPictures: '<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationSignsAndPictures" javaScriptEscape="true"/>',
                                communicationGesturesAndNoises: '<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationGesturesAndNoises" javaScriptEscape="true"/>',
                                communicationEyePointing: '<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationEyePointing" javaScriptEscape="true"/>',
                                communicationOtherMethods: '<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationOtherMethods" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthFamily1: {
                                familyHistoryHeartProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryHeartProblems" javaScriptEscape="true"/>',
                                familyHistoryDiabetes: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryDiabetes" javaScriptEscape="true"/>',
                                familyHistoryAsthma: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryAsthma" javaScriptEscape="true"/>',
                                familyHistoryCancer: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryCancer" javaScriptEscape="true"/>',
                                familyHistoryGlaucoma: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryGlaucoma" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthFamily2: {
                                familyHistoryEpilepsy: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryEpilepsy" javaScriptEscape="true"/>',
                                familyHistoryMentalHealth: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryMentalHealth" javaScriptEscape="true"/>',
                                familyHistoryHighBloodPressure: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryHighBloodPressure" javaScriptEscape="true"/>',
                                familyHistoryOther: '<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryOther" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthReviews: {
                                lastMedicalReviewDate: '<s:message code="intakeFormFullDetailsVO.appendix1.reviews.lastMedicalReviewDate"/>',
                                lastMedicationReviewCheck: '<s:message code="intakeFormFullDetailsVO.appendix1.reviews.lastMedicationReviewCheck"/>',
                                requireBloodTests: '<s:message code="intakeFormFullDetailsVO.appendix1.reviews.requireBloodTests"/>',
                                bloodTestDetails: '<s:message code="intakeFormFullDetailsVO.appendix1.reviews.bloodTestDetails"/>',
                                lastBloodTestDate: '<s:message code="intakeFormFullDetailsVO.appendix1.reviews.lastBloodTestDate"/>',
                                bloodTestSupportRequired: '<s:message code="intakeFormFullDetailsVO.appendix1.reviews.bloodTestSupportRequired"/>',
                                bloodTestSupportDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.reviews.bloodTestSupportDescription"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthVision: {
                                eyesightProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.vision.eyesightProblems" javaScriptEscape="true"/>',
                                wearsGlassesOrLenses: '<s:message code="intakeFormFullDetailsVO.appendix1.vision.wearsGlassesOrLenses" javaScriptEscape="true"/>',
                                lastEyeTestDate: '<s:message code="intakeFormFullDetailsVO.appendix1.vision.lastEyeTestDate" javaScriptEscape="true"/>',
                                glassesLensesSupportRequired: '<s:message code="intakeFormFullDetailsVO.appendix1.vision.glassesLensesSupportRequired" javaScriptEscape="true"/>',
                                glassesLensesSupportDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.vision.glassesLensesSupportDescription" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthHearing: {
                                hearingProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingProblems" javaScriptEscape="true"/>',
                                hearingAidWorn: '<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingAidWorn" javaScriptEscape="true"/>',
                                lastHearingAssessmentDate: '<s:message code="intakeFormFullDetailsVO.appendix1.hearing.lastHearingAssessmentDate" javaScriptEscape="true"/>',
                                hearingSupportRequired: '<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingSupportRequired" javaScriptEscape="true"/>',
                                hearingSupportDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingSupportDescription" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthDental: {
                                dentalProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.dentalProblems" javaScriptEscape="true"/>',
                                visitsDentist: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.visitsDentist" javaScriptEscape="true"/>',
                                lastDentalAppointmentDate: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.lastDentalAppointmentDate" javaScriptEscape="true"/>',
                                hasDentures: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.hasDentures" javaScriptEscape="true"/>',
                                denturesWorn: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.denturesWorn" javaScriptEscape="true"/>',
                                reasonsDenturesNotWorn: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.reasonsDenturesNotWorn" javaScriptEscape="true"/>',
                                dentalSupportRequired: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.dentalSupportRequired" javaScriptEscape="true"/>',
                                dentalSupportDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.dental.dentalSupportDescription" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthFootcare: {
                                footProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.footProblems" javaScriptEscape="true"/>',
                                footProblemsDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.footProblemsDescription" javaScriptEscape="true"/>',
                                seesChiropodist: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.seesChiropodist" javaScriptEscape="true"/>',
                                lastChiropodyAppointmentDate: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.lastChiropodyAppointmentDate" javaScriptEscape="true"/>',
                                chiropodistName: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.chiropodistName" javaScriptEscape="true"/>',
                                chiropodistContactNumber: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.chiropodistContactNumber" javaScriptEscape="true"/>',
                                chiropodistAddress: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.chiropodistAddress" javaScriptEscape="true"/>',
                                feetSupportRequired: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.feetSupportRequired" javaScriptEscape="true"/>',
                                feetSupportDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.footcare.feetSupportDescription" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit"/>'
                            },
                            healthContinence: {
                                continenceProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceProblems"/>',
                                continenceProblemsDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceProblemsDescription"/>',
                                seenByContinenceAdvisor: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.seenByContinenceAdvisor"/>',
                                lastContinenceAppointmentDate: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.lastContinenceAppointmentDate"/>',
                                continenceAdvisorName: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceAdvisorName"/>',
                                continenceAdvisorContactNumber: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceAdvisorContactNumber"/>',
                                continenceAdvisorAddress: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceAdvisorAddress"/>',
                                suffersFromUrineInfections: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.suffersFromUrineInfections"/>',
                                urineInfectionDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.continence.urineInfectionDescription"/>',
                                //buttons
                                editButton: '<s:message code="button.edit"/>'
                            },
                            healthEpilepsy: {
                                suffersFromEpilepsy: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.suffersFromEpilepsy"/>',
                                seizureDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.seizureDescription"/>',
                                epilepsyMonitored: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsyMonitored"/>',
                                howOftenEpilepsyMonitored: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.howOftenEpilepsyMonitored"/>',
                                dateLastEpilepsyAppointment: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.dateLastEpilepsyAppointment"/>',
                                epilepsySpecialistName: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsySpecialistName"/>',
                                epilepsySpecialistContactNumber: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsySpecialistContactNumber"/>',
                                epilepsySpecialistAddress: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsySpecialistAddress"/>',
                                emergencyMedProtocol: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.emergencyMedProtocol"/>',
                                emergencyMedProtocolDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.emergencyMedProtocolDescription"/>',
                                //buttons
                                editButton: '<s:message code="button.edit"/>'
                            },
                            healthSkin: {
                                skinProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinProblems"/>',
                                seenBySkinSpecialist: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.seenBySkinSpecialist"/>',
                                dateLastSkinAppointment: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.dateLastSkinAppointment"/>',
                                skinSpecialistName: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSpecialistName"/>',
                                skinSpecialistContactNumber: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSpecialistContactNumber"/>',
                                skinSpecialistAddress: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSpecialistAddress"/>',
                                skinTreatmentDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinTreatmentDescription"/>',
                                skinSupportRequired: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSupportRequired"/>',
                                skinSupportDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSupportDescription"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthBreathingCirculation: {
                                suffersFromBreathlessness: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.suffersFromBreathlessness" javaScriptEscape="true"/>',
                                breathlessnessDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.breathlessnessDescription" javaScriptEscape="true"/>',
                                respiratoryHeartProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.respiratoryHeartProblems" javaScriptEscape="true"/>',
                                respiratoryHeartProblemsDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.respiratoryHeartProblemsDescription" javaScriptEscape="true"/>',
                                circulationProblems: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationProblems" javaScriptEscape="true"/>',
                                circulationProblemsDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationProblemsDescription" javaScriptEscape="true"/>',
                                breathingCirculationChecked: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.breathingCirculationChecked" javaScriptEscape="true"/>',
                                dateLastCirculationAppointment: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.dateLastCirculationAppointment" javaScriptEscape="true"/>',
                                circulationSpecialistName: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSpecialistName" javaScriptEscape="true"/>',
                                circulationSpecialistContactNumber: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSpecialistContactNumber" javaScriptEscape="true"/>',
                                circulationSpecialistAddress: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSpecialistAddress" javaScriptEscape="true"/>',
                                circulationSupportRequired: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSupportRequired" javaScriptEscape="true"/>',
                                circulationSupportDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSupportDescription" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            healthMensHealth: {
                                attendsWellManClinic: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.attendsWellManClinic" javaScriptEscape="true"/>',
                                attendsWellManClinicFrequency: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.attendsWellManClinicFrequency" javaScriptEscape="true"/>',
                                mensHealthSpecialistName: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.mensHealthSpecialistName" javaScriptEscape="true"/>',
                                mensHealthSpecialistContactNumber: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.mensHealthSpecialistContactNumber" javaScriptEscape="true"/>',
                                mensHealthSpecialistAddress: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.mensHealthSpecialistAddress" javaScriptEscape="true"/>',
                                carriesOutTesticularExaminations: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.carriesOutTesticularExaminations" javaScriptEscape="true"/>',
                                lastDateTesticularExamination: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.lastDateTesticularExamination" javaScriptEscape="true"/>',
                                otherMensHealthIssues: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.otherMensHealthIssues" javaScriptEscape="true"/>',
                                otherMensHealthIssuesDescription: '<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.otherMensHealthIssuesDescription" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        app2MentalHealth: {
                            context: {
                                dateTimeAssessment: '<s:message code="intakeFormFullDetailsVO.appendix2.context.dateTimeAssessment" javaScriptEscape="true"/>',
                                assessedBy: '<s:message code="intakeFormFullDetailsVO.appendix2.context.assessedBy" javaScriptEscape="true"/>',
                                assessedWhere: '<s:message code="intakeFormFullDetailsVO.appendix2.context.assessedWhere" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            mentalState1: {
                                mood: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.mood" javaScriptEscape="true"/>',
                                medication: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.medication" javaScriptEscape="true"/>',
                                moodWorseWhen: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.moodWorseWhen" javaScriptEscape="true"/>',
                                avoidingPeople: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.avoidingPeople" javaScriptEscape="true"/>',
                                futureSelf: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.futureSelf" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            mentalState2: {
                                thinkingOfHarming: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.thinkingOfHarming" javaScriptEscape="true"/>',
                                worries: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.worries" javaScriptEscape="true"/>',
                                feelingUseful: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.feelingUseful" javaScriptEscape="true"/>',
                                bodyControl: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.bodyControl" javaScriptEscape="true"/>',
                                hearingVoices: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.hearingVoices" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            mentalState3: {
                                talkingTV: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.talkingTV" javaScriptEscape="true"/>',
                                thoughtControl: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.thoughtControl" javaScriptEscape="true"/>',
                                seeingThings: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.seeingThings" javaScriptEscape="true"/>',
                                touch: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.touch" javaScriptEscape="true"/>',
                                specialPurpose: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.specialPurpose" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            placeOrientation: {
                                whereAmI: '<s:message code="intakeFormFullDetailsVO.appendix2.placeOrientation.whereAmI" javaScriptEscape="true"/>',
                                whichCountry: '<s:message code="intakeFormFullDetailsVO.appendix2.placeOrientation.whichCountry" javaScriptEscape="true"/>',
                                whereInCountry: '<s:message code="intakeFormFullDetailsVO.appendix2.placeOrientation.whereInCountry" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            timeOrientation: {
                                whichDay: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichDay" javaScriptEscape="true"/>',
                                whichMonth: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichMonth" javaScriptEscape="true"/>',
                                whichYear: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichYear" javaScriptEscape="true"/>',
                                whatTime: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whatTime" javaScriptEscape="true"/>',
                                whichPartOfDay: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichPartOfDay" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            sleepPattern: {
                                sleepPattern: '<s:message code="intakeFormFullDetailsVO.appendix2.sleepPattern.sleepPattern" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            appetite: {
                                appetiteLike: '<s:message code="intakeFormFullDetailsVO.appendix2.appetite.appetiteLike" javaScriptEscape="true"/>',
                                whenAreYouEating: '<s:message code="intakeFormFullDetailsVO.appendix2.appetite.whenAreYouEating" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            feelings: {
                                achesPains: '<s:message code="intakeFormFullDetailsVO.appendix2.feelings.achesPains" javaScriptEscape="true"/>',
                                enjoyingLife: '<s:message code="intakeFormFullDetailsVO.appendix2.feelings.enjoyingLife" javaScriptEscape="true"/>',
                                forgetful: '<s:message code="intakeFormFullDetailsVO.appendix2.feelings.forgetful" javaScriptEscape="true"/>',
                                energy: '<s:message code="intakeFormFullDetailsVO.appendix2.feelings.energy" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            other: {
                                generalHealth: '<s:message code="intakeFormFullDetailsVO.appendix2.other.generalHealth" javaScriptEscape="true"/>',
                                emotions: '<s:message code="intakeFormFullDetailsVO.appendix2.other.emotions" javaScriptEscape="true"/>',
                                anxiety: '<s:message code="intakeFormFullDetailsVO.appendix2.other.anxiety" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            outcome: {
                                outcomes: '<s:message code="intakeFormFullDetailsVO.appendix2.outcome.outcomes" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        app3Behaviour: {
                            context: {
                                assessmentDateTime: '<s:message code="intakeFormFullDetailsVO.appendix3.context.assessmentDateTime" javaScriptEscape="true"/>',
                                assessedBy: '<s:message code="intakeFormFullDetailsVO.appendix3.context.assessedBy" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            behaviour1: {
                                what: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.what" javaScriptEscape="true"/>',
                                where: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.where" javaScriptEscape="true"/>',
                                frequency: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.frequency" javaScriptEscape="true"/>',
                                duration: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.duration" javaScriptEscape="true"/>',
                                triggers: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.triggers" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            },
                            behaviour2: {
                                consequencesImpact: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.consequencesImpact" javaScriptEscape="true"/>',
                                responseToBehaviour: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.responseToBehaviour" javaScriptEscape="true"/>',
                                howLongBehaviourPresent: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.howLongBehaviourPresent" javaScriptEscape="true"/>',
                                desiredAlternatives: '<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.desiredAlternatives" javaScriptEscape="true"/>',
                                //buttons
                                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                            }
                        },
                        rejection: {
                            rejectionReason: '<s:message code="rejectFormVO.rejectionReason" javaScriptEscape="true"/>',
                            rejectionDate: '<s:message code="rejectFormVO.rejectionDate" javaScriptEscape="true"/>',
                            rejectedBy: '<s:message code="rejectFormVO.rejector.name" javaScriptEscape="true"/>',
                            noRejections: '<s:message code="resolve.view.form.rejection.noRejections" javaScriptEscape="true"/>',

                            //buttons
                            editButton: '<s:message code="button.edit" javaScriptEscape="true"/>'
                        }
                    },
                    formTabs = new Y.app.IntakeFormTabsView({
                        plugins: [{
                            fn: Y.Plugin.usp.TabbedPageContentsPlugin
                        }],
                        root: '<s:url value="/forms/resolve/intake/${formId}/"/>',
                        container: '#formTabs',
                        tabs: {
                            client: {
                                label: '<s:message code="resolve.view.intakeform.tabs.client" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'informationAccordion',
                                    label: '<s:message code="resolve.view.intakeform.clientInformation.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'nextOfKinAccordion',
                                    label: '<s:message code="resolve.view.intakeform.nextOfKin.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'mainCarerAccordion',
                                    label: '<s:message code="resolve.view.intakeform.mainCarer.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'consentAccordion',
                                    label: '<s:message code="resolve.view.intakeform.consent.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'peopleNotToBeContactedAccordion',
                                    label: '<s:message code="resolve.view.intakeform.peopleNotToBeContacted.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: formModel,
                                    informationConfig: {
                                        title: '<s:message code="resolve.view.intakeform.clientInformation.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.client.information
                                    },
                                    nextOfKinConfig: {
                                        title: '<s:message code="resolve.view.intakeform.nextOfKin.title" javaScriptEscape="true"/>',
                                        url: '<s:url value="/rest/intakeForm/{id}/referencedPerson/nextOfKin"/>',
                                        involvementName: '<s:message code="resolve.form.involvementName.nextOfKin" javaScriptEscape="true"/>',
                                        labels: viewLabels.referencedPerson
                                    },
                                    mainCarerConfig: {
                                        title: '<s:message code="resolve.view.intakeform.mainCarer.title" javaScriptEscape="true"/>',
                                        url: '<s:url value="/rest/intakeForm/{id}/referencedPerson/mainCarer"/>',
                                        involvementName: '<s:message code="resolve.form.involvementName.mainCarer" javaScriptEscape="true"/>',
                                        labels: viewLabels.referencedPerson
                                    },
                                    consentConfig: {
                                        title: '<s:message code="resolve.view.intakeform.consent.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.client.consent
                                    },
                                    peopleNotToBeContactedConfig: {
                                        title: '<s:message code="resolve.view.intakeform.peopleNotToBeContacted.title" javaScriptEscape="true"/>',
                                        referencedPeopleConfig: {
                                            labels: Y.merge(viewLabels.referencedPerson, {
                                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>',
                                                edit: '<s:message code="intakeReferencedPersonVO.results.edit" javaScriptEscape="true"/>',
                                                remove: '<s:message code="intakeReferencedPersonVO.results.remove" javaScriptEscape="true"/>'
                                            }),
                                            url: '<s:url value="/rest/intakeForm/{id}/referencedPerson/notToBeContacted"/>'
                                        },
                                        involvementName: '<s:message code="resolve.form.involvementName.peopleNotToBeContacted" javaScriptEscape="true"/>'
                                    }
                                }
                            },
                            placement: {
                                label: '<s:message code="resolve.view.intakeform.tabs.placement" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'briefHistoryAccordion',
                                    label: '<s:message code="resolve.view.intakeform.briefHistory.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'historyAccordion',
                                    label: '<s:message code="resolve.view.intakeform.history.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'currentPlacementAccordion',
                                    label: '<s:message code="resolve.view.intakeform.currentPlacement.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'identifiedRisksAccordion',
                                    label: '<s:message code="resolve.view.intakeform.risks.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: formModel,
                                    briefHistoryConfig: {
                                        title: '<s:message code="resolve.view.intakeform.briefHistory.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.placement.briefHistory
                                    },
                                    historyConfig: {
                                        title: '<s:message code="resolve.view.intakeform.history.title" javaScriptEscape="true"/>',
                                        previousPlacementConfig: {
                                            labels: viewLabels.placement.history,
                                            url: '<s:url value="/rest/intakeForm/{id}/intakePreviousPlacement"/>'
                                        }
                                    },
                                    currentPlacementConfig: {
                                        title: '<s:message code="resolve.view.intakeform.currentPlacement.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.placement.currentPlacement
                                    },
                                    identifiedRisksConfig: {
                                        title: '<s:message code="resolve.view.intakeform.risks.title" javaScriptEscape="true"/>',
                                        identifiedRiskConfig: {
                                            labels: viewLabels.placement.identifiedRisks,
                                            url: '<s:url value="/rest/intakeForm/{id}/intakeRiskAssessment"/>'
                                        }
                                    }
                                }
                            },
                            referral: {
                                label: '<s:message code="resolve.view.intakeform.tabs.referral" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'referrerAccordion',
                                    label: '<s:message code="resolve.view.intakeform.referrer.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'futureAccordion',
                                    label: '<s:message code="resolve.view.intakeform.future.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'interventionAndSupportPlansAccordion',
                                    label: '<s:message code="resolve.view.intakeform.interventionAndSupportPlans.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: formModel,
                                    referrerConfig: {
                                        title: '<s:message code="resolve.view.intakeform.referrer.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.referral.referrer
                                    },
                                    futureConfig: {
                                        title: '<s:message code="resolve.view.intakeform.future.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.referral.future
                                    },
                                    interventionAndSupportPlansConfig: {
                                        title: '<s:message code="resolve.view.intakeform.interventionAndSupportPlans.title" javaScriptEscape="true"/>',
                                        supportPlanConfig: {
                                            labels: viewLabels.referral.interventionAndSupportPlans,
                                            url: '<s:url value="/rest/intakeForm/{id}/intakeSupportPlan"/>'
                                        }
                                    }
                                }
                            },
                            health: {
                                label: '<s:message code="resolve.view.intakeform.tabs.health" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'healthAccordion',
                                    label: '<s:message code="resolve.view.intakeform.health.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'gpAccordion',
                                    label: '<s:message code="resolve.view.intakeform.gp.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'professionalsAccordion',
                                    label: '<s:message code="resolve.view.intakeform.professionals.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'medicationsAccordion',
                                    label: '<s:message code="resolve.view.intakeform.medications.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'healthActionPlanAccordion',
                                    label: '<s:message code="resolve.view.intakeform.healthActionPlan.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'intakeFormLifestyleAccordion',
                                    label: '<s:message code="resolve.view.intakeform.lifestyle.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: formModel,
                                    healthConfig: {
                                        title: '<s:message code="resolve.view.intakeform.health.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.health.health
                                    },
                                    gpConfig: {
                                        title: '<s:message code="resolve.view.intakeform.gp.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.health.gp
                                    },
                                    professionalsConfig: {
                                        title: '<s:message code="resolve.view.intakeform.professionals.title" javaScriptEscape="true"/>',
                                        referencedPeopleConfig: {
                                            labels: Y.merge(viewLabels.referencedPerson, {
                                                addButton: '<s:message code="button.add" javaScriptEscape="true"/>',
                                                edit: '<s:message code="intakeReferencedPersonVO.results.edit" javaScriptEscape="true"/>',
                                                remove: '<s:message code="intakeReferencedPersonVO.results.remove" javaScriptEscape="true"/>'
                                            }),
                                            url: '<s:url value="/rest/intakeForm/{id}/referencedPerson/professional"/>'
                                        },
                                        involvementName: '<s:message code="resolve.form.involvementName.professional" javaScriptEscape="true"/>'
                                    },
                                    medicationsConfig: {
                                        title: '<s:message code="resolve.view.intakeform.medications.title" javaScriptEscape="true"/>',
                                        medicationConfig: {
                                            labels: viewLabels.health.medications,
                                            url: '<s:url value="/rest/intakeForm/{id}/intakeMedication"/>'
                                        }
                                    },
                                    healthActionPlanConfig: {
                                        title: '<s:message code="resolve.view.intakeform.healthActionPlan.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.health.healthActionPlan
                                    },
                                    lifestyleConfig: {
                                        title: '<s:message code="resolve.view.intakeform.lifestyle.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.health.lifestyle
                                    }
                                }
                            },
                            mentalHealth: {
                                label: '<s:message code="resolve.view.intakeform.tabs.mentalHealth" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'mentalHealthAccordion',
                                    label: '<s:message code="resolve.view.intakeform.mentalHealth.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'psychologyAccordion',
                                    label: '<s:message code="resolve.view.intakeform.psychology.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: formModel,
                                    mentalHealthConfig: {
                                        title: '<s:message code="resolve.view.intakeform.mentalHealth.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.mentalHealth.mentalHealth
                                    },
                                    psychologyConfig: {
                                        title: '<s:message code="resolve.view.intakeform.psychology.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.mentalHealth.psychology
                                    }
                                }
                            },
                            physio: {
                                label: '<s:message code="resolve.view.intakeform.tabs.physio" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'physiotherapyAccordion',
                                    label: '<s:message code="resolve.view.intakeform.physiotherapy.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'mobilityAccordion',
                                    label: '<s:message code="resolve.view.intakeform.mobility.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'occupationalTherapistAccordion',
                                    label: '<s:message code="resolve.view.intakeform.occupationalTherapist.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'occupationalIssuesAccordion',
                                    label: '<s:message code="resolve.view.intakeform.occupationalIssues.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: formModel,
                                    physiotherapyConfig: {
                                        title: '<s:message code="resolve.view.intakeform.physiotherapy.title" javaScriptEscape="true"/>',
                                        url: '<s:url value="/rest/intakeForm/{id}/referencedPerson/physiotherapist"/>',
                                        labels: Y.merge(viewLabels.referencedPerson, {
                                            active: '<s:message code="intakeFormFullDetailsVO.physiotherapy.active" javaScriptEscape="true"/>'
                                        }),
                                        involvementName: '<s:message code="resolve.form.involvementName.physiotherapy" javaScriptEscape="true"/>'
                                    },
                                    mobilityConfig: {
                                        title: '<s:message code="resolve.view.intakeform.mobility.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.physio.mobility
                                    },
                                    occupationalTherapistConfig: {
                                        title: '<s:message code="resolve.view.intakeform.occupationalTherapist.title" javaScriptEscape="true"/>',
                                        url: '<s:url value="/rest/intakeForm/{id}/referencedPerson/occupationalTherapist"/>',
                                        labels: Y.merge(viewLabels.referencedPerson, {
                                            active: '<s:message code="intakeFormFullDetailsVO.occupationalTherapy.active" javaScriptEscape="true"/>'
                                        }),
                                        involvementName: '<s:message code="resolve.form.involvementName.occupationalTherapist" javaScriptEscape="true"/>'
                                    },
                                    occupationalIssuesConfig: {
                                        title: '<s:message code="resolve.view.intakeform.occupationalIssues.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.physio.occupationalIssues
                                    }
                                }
                            },
                            speech: {
                                label: '<s:message code="resolve.view.intakeform.tabs.speech" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'therapyAccordion',
                                    label: '<s:message code="resolve.view.intakeform.speechTherapy.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'issuesAccordion',
                                    label: '<s:message code="resolve.view.intakeform.communicationIssues.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'eatingAccordion',
                                    label: '<s:message code="resolve.view.intakeform.eating.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: formModel,
                                    therapyConfig: {
                                        title: '<s:message code="resolve.view.intakeform.speechTherapy.title" javaScriptEscape="true"/>',
                                        url: '<s:url value="/rest/intakeForm/{id}/referencedPerson/speechTherapist"/>',
                                        labels: Y.merge(viewLabels.referencedPerson, {
                                            active: '<s:message code="intakeFormFullDetailsVO.speechAndLanguage.active" javaScriptEscape="true"/>'
                                        }),
                                        involvementName: '<s:message code="resolve.form.involvementName.speechTherapy" javaScriptEscape="true"/>'
                                    },
                                    issuesConfig: {
                                        title: '<s:message code="resolve.view.intakeform.communicationIssues.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.speech.issues
                                    },
                                    eatingConfig: {
                                        title: '<s:message code="resolve.view.intakeform.eating.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.speech.eating
                                    }
                                }
                            },
                            needs: {
                                label: '<s:message code="resolve.view.intakeform.tabs.needs" javaScriptEscape="true"/>',
                                contents: [{
                                        id: 'personalCareNeedAccordion',
                                        label: '<s:message code="resolve.view.intakeform.personalCare.title" javaScriptEscape="true"/>'
                                    }, {
                                        id: 'spiritualNeedsAccordion',
                                        label: '<s:message code="resolve.view.intakeform.spiritualNeeds.title" javaScriptEscape="true"/>'
                                    }, {
                                        id: 'socialAccordion',
                                        label: '<s:message code="resolve.view.intakeform.social.title" javaScriptEscape="true"/>'
                                    }, {
                                        id: 'socialSupportAccordion',
                                        label: '<s:message code="resolve.view.intakeform.socialSupport.title" javaScriptEscape="true"/>'
                                    }

                                ],
                                config: {
                                    model: formModel,
                                    personalCareConfig: {
                                        title: '<s:message code="resolve.view.intakeform.personalCare.title" javaScriptEscape="true"/>',
                                        personalCareNeedConfig: {
                                            labels: viewLabels.needs.personalCare,
                                            url: '<s:url value="/rest/intakeForm/{id}/intakePersonalCareNeed"/>'
                                        }
                                    },
                                    spiritualNeedsConfig: {
                                        title: '<s:message code="resolve.view.intakeform.spiritualNeeds.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.needs.spiritualNeeds
                                    },
                                    socialConfig: {
                                        title: '<s:message code="resolve.view.intakeform.social.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.needs.social,
                                        url: '<s:url value="/rest/intakeForm/{id}/intakeTimetableEvent"/>'
                                    },
                                    socialSupportConfig: {
                                        title: '<s:message code="resolve.view.intakeform.socialSupport.title" javaScriptEscape="true"/>',
                                        supportConfig: {
                                            labels: viewLabels.needs.socialSupport,
                                            url: '<s:url value="/rest/intakeForm/{id}/intakeSupport"/>'
                                        }
                                    }
                                }
                            },
                            app1Comms: {
                                label: '<s:message code="resolve.view.intakeform.tabs.app1Comms" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'app1CommsAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Comms.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1Family1Accordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Family1.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1Family2Accordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Family2.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1ReviewsAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Reviews.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1VisionAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Vision.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1HearingAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Hearing.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1DentalAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Dental.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1FootcareAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Footcare.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1ContinenceAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Continence.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1EpilepsyAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Epilepsy.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1SkinAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1Skin.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1BreathingCirculationAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1BreathingCirculation.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app1MensHealthAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app1MensHealth.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: appendix1Model,
                                    formModel: formModel,
                                    app1CommsConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Comms.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthCommunication
                                    },
                                    app1Family1Config: {
                                        title: '<s:message code="resolve.view.intakeform.app1Family1.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthFamily1
                                    },
                                    app1Family2Config: {
                                        title: '<s:message code="resolve.view.intakeform.app1Family2.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthFamily2
                                    },
                                    app1ReviewsConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Reviews.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthReviews
                                    },
                                    app1VisionConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Vision.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthVision
                                    },
                                    app1HearingConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Hearing.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthHearing
                                    },
                                    app1DentalConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Dental.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthDental
                                    },
                                    app1FootcareConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Footcare.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthFootcare
                                    },
                                    app1ContinenceConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Continence.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthContinence
                                    },
                                    app1EpilepsyConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Epilepsy.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthEpilepsy
                                    },
                                    app1SkinConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1Skin.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthSkin
                                    },
                                    app1BreathingCirculationConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1BreathingCirculation.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthBreathingCirculation
                                    },
                                    app1MensHealthConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app1MensHealth.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app1Comms.healthMensHealth
                                    }
                                }
                            },
                            app2MentalHealth: {
                                label: '<s:message code="resolve.view.intakeform.tabs.app2MentalHealth" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'app2ContextAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2Context.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2MentalState1Accordion',
                                    label: '<s:message code="resolve.view.intakeform.app2MentalState1.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2MentalState2Accordion',
                                    label: '<s:message code="resolve.view.intakeform.app2MentalState2.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2MentalState3Accordion',
                                    label: '<s:message code="resolve.view.intakeform.app2MentalState3.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2PlaceOrientationAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2PlaceOrientation.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2TimeOrientationAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2TimeOrientation.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2SleepPatternAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2SleepPattern.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2AppetiteAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2Appetite.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2FeelingsAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2Feelings.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2OtherAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2Other.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app2OutcomeAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app2Outcome.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: appendix2Model,
                                    formModel: formModel,
                                    app2ContextConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2Context.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.context
                                    },
                                    app2MentalState1Config: {
                                        title: '<s:message code="resolve.view.intakeform.app2MentalState1.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.mentalState1
                                    },
                                    app2MentalState2Config: {
                                        title: '<s:message code="resolve.view.intakeform.app2MentalState2.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.mentalState2
                                    },
                                    app2MentalState3Config: {
                                        title: '<s:message code="resolve.view.intakeform.app2MentalState3.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.mentalState3
                                    },
                                    app2PlaceOrientationConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2PlaceOrientation.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.placeOrientation
                                    },
                                    app2TimeOrientationConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2TimeOrientation.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.timeOrientation
                                    },
                                    app2SleepPatternConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2SleepPattern.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.sleepPattern
                                    },
                                    app2AppetiteConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2Appetite.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.appetite
                                    },
                                    app2FeelingsConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2Feelings.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.feelings
                                    },
                                    app2OtherConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2Other.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.other
                                    },
                                    app2OutcomeConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app2Outcome.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app2MentalHealth.outcome
                                    }
                                }
                            },
                            app3Behaviour: {
                                label: '<s:message code="resolve.view.intakeform.tabs.app3Behaviour" javaScriptEscape="true"/>',
                                contents: [{
                                    id: 'app3ContextAccordion',
                                    label: '<s:message code="resolve.view.intakeform.app3Context.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app3Behaviour1Accordion',
                                    label: '<s:message code="resolve.view.intakeform.app3Behaviour1.title" javaScriptEscape="true"/>'
                                }, {
                                    id: 'app3Behaviour2Accordion',
                                    label: '<s:message code="resolve.view.intakeform.app3Behaviour2.title" javaScriptEscape="true"/>'
                                }],
                                config: {
                                    model: appendix3Model,
                                    formModel: formModel,
                                    app3ContextConfig: {
                                        title: '<s:message code="resolve.view.intakeform.app3Context.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app3Behaviour.context
                                    },
                                    app3Behaviour1Config: {
                                        title: '<s:message code="resolve.view.intakeform.app3Behaviour1.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app3Behaviour.behaviour1
                                    },
                                    app3Behaviour2Config: {
                                        title: '<s:message code="resolve.view.intakeform.app3Behaviour2.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.app3Behaviour.behaviour2
                                    }
                                }
                            },
                            rejection: {
                                label: '<s:message code="resolve.view.intakeform.tabs.rejection" javaScriptEscape="true"/>',
                                contents: [{
                                    label: '<s:message code="resolve.view.intakeform.rejection.title" javaScriptEscape="true"/>',
                                    id: 'rejectionView'
                                }],
                                config: {
                                    model: formModel,
                                    rejectionConfig: {
                                        title: '<s:message code="resolve.view.intakeform.rejection.title" javaScriptEscape="true"/>',
                                        labels: viewLabels.rejection
                                    }
                                }
                            }
                        },
                        dialogConfig: {
                            defaultHeader: 'loading...',
                            consentConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.consent.header"/></h3>',
                                    labels: {
                                        consentToShare: '<s:message code="updateIntakeConsentVO.consentToShare" javaScriptEscape="true"/>',
                                        consentToShareDetails: '<s:message code="updateIntakeConsentVO.consentToShareDetails" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            briefHistoryConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.briefHistory.header"/></h3>',
                                    labels: {
                                        description: '<s:message code="updateIntakeHistoryVO.briefHistory" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            currentPlacementConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.currentPlacement.header"/></h3>',
                                    labels: {
                                        startDate: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementStartDate" javaScriptEscape="true"/>',
                                        endDate: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementEndDate" javaScriptEscape="true"/>',
                                        location: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementLocation" javaScriptEscape="true"/>',
                                        type: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementType" javaScriptEscape="true"/>',
                                        contact: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementContact" javaScriptEscape="true"/>',
                                        challenges: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementChallenges" javaScriptEscape="true"/>',
                                        clientViews: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementClientViews" javaScriptEscape="true"/>',
                                        assessmentPresentation: '<s:message code="updateIntakeCurrentPlacementVO.currentPlacementAssessmentPresentation" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            lifestyleConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.lifestyle.header"/></h3>',
                                    labels: {
                                        alcoholConsumption: '<s:message code="updateIntakeLifestyleVO.lifestyleAlcoholConsumption" javaScriptEscape="true"/>',
                                        alcoholDetails: '<s:message code="updateIntakeLifestyleVO.lifestyleAlcoholDetails" javaScriptEscape="true"/>',
                                        smoking: '<s:message code="updateIntakeLifestyleVO.lifestyleSmoking" javaScriptEscape="true"/>',
                                        smokingDetails: '<s:message code="updateIntakeLifestyleVO.lifestyleSmokingDetails" javaScriptEscape="true"/>',
                                        drugs: '<s:message code="updateIntakeLifestyleVO.lifestyleDrugs" javaScriptEscape="true"/>',
                                        drugsDetails: '<s:message code="updateIntakeLifestyleVO.lifestyleDrugsDetails" javaScriptEscape="true"/>',
                                        supportRequired: '<s:message code="updateIntakeLifestyleVO.lifestyleSupportRequired" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            healthActionPlanConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.healthActionPlan.header"/></h3>',
                                    labels: {
                                        healthActionPlan: '<s:message code="updateIntakeMedicationVO.medicationHealthActionPlan" javaScriptEscape="true"/>',
                                        recentChanges: '<s:message code="updateIntakeMedicationVO.medicationRecentChanges" javaScriptEscape="true"/>',
                                        compliance: '<s:message code="updateIntakeMedicationVO.medicationCompliance" javaScriptEscape="true"/>',
                                        supportRequired: '<s:message code="updateIntakeMedicationVO.medicationSupportRequired" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            referrerConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.referrer.header"/></h3>',
                                    labels: {
                                        referrerName: '<s:message code="updateIntakeReferralVO.referrerName" javaScriptEscape="true"/>',
                                        referrerRole: '<s:message code="updateIntakeReferralVO.referrerRole" javaScriptEscape="true"/>',
                                        referralReason: '<s:message code="updateIntakeReferralVO.referralReason" javaScriptEscape="true"/>',
                                        referralDate: '<s:message code="updateIntakeReferralVO.referralDate" javaScriptEscape="true"/>',
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            futureConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.future.header"/></h3>',
                                    labels: {
                                        futurePlanOfClient: '<s:message code="updateIntakeFutureVO.futurePlanOfClient" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            eatingConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.eating.header"/></h3>',
                                    labels: {
                                        specialDiet: '<s:message code="updateIntakeSpeechVO.specialDiet" javaScriptEscape="true"/>',
                                        problemsEating: '<s:message code="updateIntakeSpeechVO.problemsEating" javaScriptEscape="true"/>',
                                        mealtimeIssues: '<s:message code="updateIntakeSpeechVO.mealtimeIssues" javaScriptEscape="true"/>',
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            socialConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.social.header"/></h3>',
                                    labels: {
                                        monday: '<s:message code="resolve.view.intakeform.timetable.monday" javaScriptEscape="true"/>',
                                        tuesday: '<s:message code="resolve.view.intakeform.timetable.tuesday" javaScriptEscape="true"/>',
                                        wednesday: '<s:message code="resolve.view.intakeform.timetable.wednesday" javaScriptEscape="true"/>',
                                        thursday: '<s:message code="resolve.view.intakeform.timetable.thursday" javaScriptEscape="true"/>',
                                        friday: '<s:message code="resolve.view.intakeform.timetable.friday" javaScriptEscape="true"/>',
                                        saturday: '<s:message code="resolve.view.intakeform.timetable.saturday" javaScriptEscape="true"/>',
                                        sunday: '<s:message code="resolve.view.intakeform.timetable.sunday" javaScriptEscape="true"/>',
                                        morning: '<s:message code="resolve.view.intakeform.timetable.morning" javaScriptEscape="true"/>',
                                        afternoon: '<s:message code="resolve.view.intakeform.timetable.afternoon" javaScriptEscape="true"/>',
                                        evening: '<s:message code="resolve.view.intakeform.timetable.evening" javaScriptEscape="true"/>',
                                        currentArrangementsIssues: '<s:message code="updateIntakeSocialVO.socialCurrentArrangementsIssues" javaScriptEscape="true"/>',
                                        supportRequired: '<s:message code="updateIntakeSocialVO.socialSupportRequired" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            communicationIssuesConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.speechCommunication.header"/></h3>',
                                    labels: {
                                        speechCommunicationIssues: '<s:message code="updateIntakeSpeechVO.speechCommunicationIssues" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            healthConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.health.header"/></h3>',
                                    labels: {
                                        height: '<s:message code="updateIntakeHealthVO.height" javaScriptEscape="true"/>',
                                        weight: '<s:message code="updateIntakeHealthVO.weight" javaScriptEscape="true"/>',
                                        lastAnnualHealthCheck: '<s:message code="updateIntakeHealthVO.lastAnnualHealthCheck" javaScriptEscape="true"/>',
                                        medicalHistory: '<s:message code="updateIntakeHealthVO.medicalHistory" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            mentalHealthConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.mentalHealth.header"/></h3>',
                                    labels: {
                                        mentalHealthAppetite: '<s:message code="updateIntakeMentalHealthVO.mentalHealthAppetite" javaScriptEscape="true"/>',
                                        mentalHealthDescription: '<s:message code="updateIntakeMentalHealthVO.mentalHealthDescription" javaScriptEscape="true"/>',
                                        mentalHealthSleepPattern: '<s:message code="updateIntakeMentalHealthVO.mentalHealthSleepPattern" javaScriptEscape="true"/>',
                                        behaviourDescription: '<s:message code="updateIntakeMentalHealthVO.behaviourDescription" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            psychologyConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.psychology.header"/></h3>',
                                    labels: {
                                        psychologyBenefitFromTherapy: '<s:message code="updateIntakePsychologyVO.psychologyBenefitFromTherapy" javaScriptEscape="true"/>',
                                        psychologyDetails: '<s:message code="updateIntakePsychologyVO.psychologyDetails" javaScriptEscape="true"/>',
                                        psychologyPreviousInput: '<s:message code="updateIntakePsychologyVO.psychologyPreviousInput" javaScriptEscape="true"/>',
                                        psychologyUnresolvedAbuseIssues: '<s:message code="updateIntakePsychologyVO.psychologyUnresolvedAbuseIssues" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            occupationalIssuesConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.occupationalIssues.header"/></h3>',
                                    labels: {
                                        otMobilityProblems: '<s:message code="updateIntakePhysiotherapyVO.otMobilityProblems" javaScriptEscape="true"/>',
                                        otUtilityApplicationIssues: '<s:message code="updateIntakePhysiotherapyVO.otUtilityApplicationIssues" javaScriptEscape="true"/>',
                                        otAidsAndAdaptations: '<s:message code="updateIntakePhysiotherapyVO.otAidsAndAdaptations" javaScriptEscape="true"/>',
                                        otRecentProblems: '<s:message code="updateIntakePhysiotherapyVO.otRecentProblems" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            spiritualNeedsConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.spiritualNeeds.header"/></h3>',
                                    labels: {
                                        needs: '<s:message code="updateIntakeSpiritualVO.spiritualNeeds" javaScriptEscape="true"/>',
                                        needsBeingMet: '<s:message code="updateIntakeSpiritualVO.spiritualNeedsBeingMet" javaScriptEscape="true"/>',
                                        needsSupportRequired: '<s:message code="updateIntakeSpiritualVO.spiritualNeedsSupportRequired" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            mobilityConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.mobility.header"/></h3>',
                                    labels: {
                                        mobilityRecentIssues: '<s:message code="updateIntakePhysiotherapyVO.mobilityRecentIssues" javaScriptEscape="true"/>',
                                        mobilityRecentTripsOrFalls: '<s:message code="updateIntakePhysiotherapyVO.mobilityRecentTripsOrFalls" javaScriptEscape="true"/>',
                                        mobilityTripsMoreRegular: '<s:message code="updateIntakePhysiotherapyVO.mobilityTripsMoreRegular" javaScriptEscape="true"/>',
                                        mobilitySlipsAssessment: '<s:message code="updateIntakePhysiotherapyVO.mobilitySlipsAssessment" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}"/>'
                                }
                            },
                            app1CommunicationConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Communication.header"/></h3>',
                                    labels: {
                                        communicationVerbal: '<s:message code="updateIntakeAppendixOneCommunicationVO.communicationVerbal" javaScriptEscape="true"/>',
                                        communicationSignsAndPictures: '<s:message code="updateIntakeAppendixOneCommunicationVO.communicationSignsAndPictures" javaScriptEscape="true"/>',
                                        communicationGesturesAndNoises: '<s:message code="updateIntakeAppendixOneCommunicationVO.communicationGesturesAndNoises" javaScriptEscape="true"/>',
                                        communicationEyePointing: '<s:message code="updateIntakeAppendixOneCommunicationVO.communicationEyePointing" javaScriptEscape="true"/>',
                                        communicationOtherMethods: '<s:message code="updateIntakeAppendixOneCommunicationVO.communicationOtherMethods" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneCommunicationDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/communication"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1FamilyHistory1Config: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1FamilyHistory1.header"/></h3>',
                                    labels: {
                                        familyHistoryHeartProblems: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryHeartProblems" javaScriptEscape="true"/>',
                                        familyHistoryDiabetes: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryDiabetes" javaScriptEscape="true"/>',
                                        familyHistoryAsthma: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryAsthma" javaScriptEscape="true"/>',
                                        familyHistoryCancer: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryCancer" javaScriptEscape="true"/>',
                                        familyHistoryGlaucoma: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryGlaucoma" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneFamilyHistory1Dialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/familyHistory"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1FamilyHistory2Config: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1FamilyHistory2.header"/></h3>',
                                    labels: {
                                        familyHistoryEpilepsy: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryEpilepsy" javaScriptEscape="true"/>',
                                        familyHistoryMentalHealth: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryMentalHealth" javaScriptEscape="true"/>',
                                        familyHistoryHighBloodPressure: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryHighBloodPressure" javaScriptEscape="true"/>',
                                        familyHistoryOther: '<s:message code="updateIntakeAppendixOneFamilyHistoryVO.familyHistoryOther" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneFamilyHistory2Dialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/familyHistory"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1ReviewConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Review.header"/></h3>',
                                    labels: {
                                        lastMedicalReviewDate: '<s:message code="updateIntakeAppendixOneReviewVO.lastMedicalReviewDate" javaScriptEscape="true"/>',
                                        lastMedicationReviewCheck: '<s:message code="updateIntakeAppendixOneReviewVO.lastMedicationReviewCheck" javaScriptEscape="true"/>',
                                        requireBloodTests: '<s:message code="updateIntakeAppendixOneReviewVO.requireBloodTests" javaScriptEscape="true"/>',
                                        lastBloodTestDate: '<s:message code="updateIntakeAppendixOneReviewVO.lastBloodTestDate" javaScriptEscape="true"/>',
                                        bloodTestDetails: '<s:message code="updateIntakeAppendixOneReviewVO.bloodTestDetails" javaScriptEscape="true"/>',
                                        bloodTestSupportRequired: '<s:message code="updateIntakeAppendixOneReviewVO.bloodTestSupportRequired" javaScriptEscape="true"/>',
                                        bloodTestSupportDescription: '<s:message code="updateIntakeAppendixOneReviewVO.bloodTestSupportDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneReviewDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/review"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1VisionConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Vision.header"/></h3>',
                                    labels: {
                                        eyesightProblems: '<s:message code="updateIntakeAppendixOneProblemsVO.eyesightProblems" javaScriptEscape="true"/>',
                                        wearsGlassesOrLenses: '<s:message code="updateIntakeAppendixOneProblemsVO.wearsGlassesOrLenses" javaScriptEscape="true"/>',
                                        lastEyeTestDate: '<s:message code="updateIntakeAppendixOneProblemsVO.lastEyeTestDate" javaScriptEscape="true"/>',
                                        glassesLensesSupportRequired: '<s:message code="updateIntakeAppendixOneProblemsVO.glassesLensesSupportRequired" javaScriptEscape="true"/>',
                                        glassesLensesSupportDescription: '<s:message code="updateIntakeAppendixOneProblemsVO.glassesLensesSupportDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneVisionDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/problems"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1HearingConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Hearing.header"/></h3>',
                                    labels: {
                                        hearingProblems: '<s:message code="updateIntakeAppendixOneProblemsVO.hearingProblems" javaScriptEscape="true"/>',
                                        hearingAidWorn: '<s:message code="updateIntakeAppendixOneProblemsVO.hearingAidWorn" javaScriptEscape="true"/>',
                                        lastHearingAssessmentDate: '<s:message code="updateIntakeAppendixOneProblemsVO.lastHearingAssessmentDate" javaScriptEscape="true"/>',
                                        hearingSupportRequired: '<s:message code="updateIntakeAppendixOneProblemsVO.hearingSupportRequired" javaScriptEscape="true"/>',
                                        hearingSupportDescription: '<s:message code="updateIntakeAppendixOneProblemsVO.hearingSupportDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneHearingDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/problems"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1DentalConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Dental.header"/></h3>',
                                    labels: {
                                        dentalProblems: '<s:message code="updateIntakeAppendixOneProblemsVO.dentalProblems" javaScriptEscape="true"/>',
                                        visitsDentist: '<s:message code="updateIntakeAppendixOneProblemsVO.visitsDentist" javaScriptEscape="true"/>',
                                        lastDentalAppointmentDate: '<s:message code="updateIntakeAppendixOneProblemsVO.lastDentalAppointmentDate" javaScriptEscape="true"/>',
                                        denturesWorn: '<s:message code="updateIntakeAppendixOneProblemsVO.denturesWorn" javaScriptEscape="true"/>',
                                        hasDentures: '<s:message code="updateIntakeAppendixOneProblemsVO.hasDentures" javaScriptEscape="true"/>',
                                        reasonsDenturesNotWorn: '<s:message code="updateIntakeAppendixOneProblemsVO.reasonsDenturesNotWorn" javaScriptEscape="true"/>',
                                        dentalSupportRequired: '<s:message code="updateIntakeAppendixOneProblemsVO.dentalSupportRequired" javaScriptEscape="true"/>',
                                        dentalSupportDescription: '<s:message code="updateIntakeAppendixOneProblemsVO.dentalSupportDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneDentalDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/problems"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1FootCareConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1FootCare.header"/></h3>',
                                    labels: {
                                        footProblems: '<s:message code="updateIntakeAppendixOneFootCareVO.footProblems" javaScriptEscape="true"/>',
                                        footProblemsDescription: '<s:message code="updateIntakeAppendixOneFootCareVO.footProblemsDescription" javaScriptEscape="true"/>',
                                        seesChiropodist: '<s:message code="updateIntakeAppendixOneFootCareVO.seesChiropodist" javaScriptEscape="true"/>',
                                        lastChiropodyAppointmentDate: '<s:message code="updateIntakeAppendixOneFootCareVO.lastChiropodyAppointmentDate" javaScriptEscape="true"/>',
                                        chiropodistName: '<s:message code="updateIntakeAppendixOneFootCareVO.chiropodistName" javaScriptEscape="true"/>',
                                        chiropodistAddress: '<s:message code="updateIntakeAppendixOneFootCareVO.chiropodistAddress" javaScriptEscape="true"/>',
                                        chiropodistContactNumber: '<s:message code="updateIntakeAppendixOneFootCareVO.chiropodistContactNumber" javaScriptEscape="true"/>',
                                        feetSupportRequired: '<s:message code="updateIntakeAppendixOneFootCareVO.feetSupportRequired" javaScriptEscape="true"/>',
                                        feetSupportDescription: '<s:message code="updateIntakeAppendixOneFootCareVO.feetSupportDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneFootCareDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/footCare"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1ContinenceConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Continence.header"/></h3>',
                                    labels: {
                                        continenceProblems: '<s:message code="updateIntakeAppendixOneContinenceVO.continenceProblems" javaScriptEscape="true"/>',
                                        continenceProblemsDescription: '<s:message code="updateIntakeAppendixOneContinenceVO.continenceProblemsDescription" javaScriptEscape="true"/>',
                                        seenByContinenceAdvisor: '<s:message code="updateIntakeAppendixOneContinenceVO.seenByContinenceAdvisor" javaScriptEscape="true"/>',
                                        lastContinenceAppointmentDate: '<s:message code="updateIntakeAppendixOneContinenceVO.lastContinenceAppointmentDate" javaScriptEscape="true"/>',
                                        continenceAdvisorName: '<s:message code="updateIntakeAppendixOneContinenceVO.continenceAdvisorName" javaScriptEscape="true"/>',
                                        continenceAdvisorAddress: '<s:message code="updateIntakeAppendixOneContinenceVO.continenceAdvisorAddress" javaScriptEscape="true"/>',
                                        continenceAdvisorContactNumber: '<s:message code="updateIntakeAppendixOneContinenceVO.continenceAdvisorContactNumber" javaScriptEscape="true"/>',
                                        suffersFromUrineInfections: '<s:message code="updateIntakeAppendixOneContinenceVO.suffersFromUrineInfections" javaScriptEscape="true"/>',
                                        urineInfectionDescription: '<s:message code="updateIntakeAppendixOneContinenceVO.urineInfectionDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneContinenceDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/continence"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1EpilepsyConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Epilepsy.header"/></h3>',
                                    labels: {
                                        suffersFromEpilepsy: '<s:message code="updateIntakeAppendixOneEpilepsyVO.suffersFromEpilepsy" javaScriptEscape="true"/>',
                                        seizureDescription: '<s:message code="updateIntakeAppendixOneEpilepsyVO.seizureDescription" javaScriptEscape="true"/>',
                                        epilepsyMonitored: '<s:message code="updateIntakeAppendixOneEpilepsyVO.epilepsyMonitored" javaScriptEscape="true"/>',
                                        howOftenEpilepsyMonitored: '<s:message code="updateIntakeAppendixOneEpilepsyVO.howOftenEpilepsyMonitored" javaScriptEscape="true"/>',
                                        dateLastEpilepsyAppointment: '<s:message code="updateIntakeAppendixOneEpilepsyVO.dateLastEpilepsyAppointment" javaScriptEscape="true"/>',
                                        epilepsySpecialistName: '<s:message code="updateIntakeAppendixOneEpilepsyVO.epilepsySpecialistName" javaScriptEscape="true"/>',
                                        epilepsySpecialistAddress: '<s:message code="updateIntakeAppendixOneEpilepsyVO.epilepsySpecialistAddress" javaScriptEscape="true"/>',
                                        epilepsySpecialistContactNumber: '<s:message code="updateIntakeAppendixOneEpilepsyVO.epilepsySpecialistContactNumber" javaScriptEscape="true"/>',
                                        emergencyMedProtocol: '<s:message code="updateIntakeAppendixOneEpilepsyVO.emergencyMedProtocol" javaScriptEscape="true"/>',
                                        emergencyMedProtocolDescription: '<s:message code="updateIntakeAppendixOneEpilepsyVO.emergencyMedProtocolDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneEpilepsyDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/epilepsy"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1SkinConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1Skin.header"/></h3>',
                                    labels: {
                                        skinProblems: '<s:message code="updateIntakeAppendixOneSkinVO.skinProblems" javaScriptEscape="true"/>',
                                        dateLastSkinAppointment: '<s:message code="updateIntakeAppendixOneSkinVO.dateLastSkinAppointment" javaScriptEscape="true"/>',
                                        seenBySkinSpecialist: '<s:message code="updateIntakeAppendixOneSkinVO.seenBySkinSpecialist" javaScriptEscape="true"/>',
                                        skinSpecialistName: '<s:message code="updateIntakeAppendixOneSkinVO.skinSpecialistName" javaScriptEscape="true"/>',
                                        skinSpecialistAddress: '<s:message code="updateIntakeAppendixOneSkinVO.skinSpecialistAddress" javaScriptEscape="true"/>',
                                        skinSpecialistContactNumber: '<s:message code="updateIntakeAppendixOneSkinVO.skinSpecialistContactNumber" javaScriptEscape="true"/>',
                                        skinTreatmentDescription: '<s:message code="updateIntakeAppendixOneSkinVO.skinTreatmentDescription" javaScriptEscape="true"/>',
                                        skinSupportRequired: '<s:message code="updateIntakeAppendixOneSkinVO.skinSupportRequired" javaScriptEscape="true"/>',
                                        emergencyMedProtocol: '<s:message code="updateIntakeAppendixOneEpilepsyVO.emergencyMedProtocol" javaScriptEscape="true"/>',
                                        skinSupportDescription: '<s:message code="updateIntakeAppendixOneSkinVO.skinSupportDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneSkinDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/skin"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1BreathingCirculationConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1BreathingCirculation.header"/></h3>',
                                    labels: {
                                        suffersFromBreathlessness: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.suffersFromBreathlessness" javaScriptEscape="true"/>',
                                        breathlessnessDescription: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.breathlessnessDescription" javaScriptEscape="true"/>',
                                        respiratoryHeartProblems: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.respiratoryHeartProblems" javaScriptEscape="true"/>',
                                        respiratoryHeartProblemsDescription: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.respiratoryHeartProblemsDescription" javaScriptEscape="true"/>',
                                        circulationProblems: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.circulationProblems" javaScriptEscape="true"/>',
                                        circulationProblemsDescription: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.circulationProblemsDescription" javaScriptEscape="true"/>',
                                        breathingCirculationChecked: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.breathingCirculationChecked" javaScriptEscape="true"/>',
                                        dateLastCirculationAppointment: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.dateLastCirculationAppointment" javaScriptEscape="true"/>',
                                        circulationSpecialistName: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.circulationSpecialistName" javaScriptEscape="true"/>',
                                        circulationSpecialistAddress: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.circulationSpecialistAddress" javaScriptEscape="true"/>',
                                        circulationSpecialistContactNumber: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.circulationSpecialistContactNumber" javaScriptEscape="true"/>',
                                        circulationSupportRequired: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.circulationSupportRequired" javaScriptEscape="true"/>',
                                        circulationSupportDescription: '<s:message code="updateIntakeAppendixOneBreathingCirculationVO.circulationSupportDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneBreathingCirculationDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/breathingCirculation"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app1MensHealthConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app1MensHealth.header"/></h3>',
                                    labels: {
                                        attendsWellManClinic: '<s:message code="updateIntakeAppendixOneMensHealthVO.attendsWellManClinic" javaScriptEscape="true"/>',
                                        attendsWellManClinicFrequency: '<s:message code="updateIntakeAppendixOneMensHealthVO.attendsWellManClinicFrequency" javaScriptEscape="true"/>',
                                        mensHealthSpecialistName: '<s:message code="updateIntakeAppendixOneMensHealthVO.mensHealthSpecialistName" javaScriptEscape="true"/>',
                                        mensHealthSpecialistAddress: '<s:message code="updateIntakeAppendixOneMensHealthVO.mensHealthSpecialistAddress" javaScriptEscape="true"/>',
                                        mensHealthSpecialistContactNumber: '<s:message code="updateIntakeAppendixOneMensHealthVO.mensHealthSpecialistContactNumber" javaScriptEscape="true"/>',
                                        carriesOutTesticularExaminations: '<s:message code="updateIntakeAppendixOneMensHealthVO.carriesOutTesticularExaminations" javaScriptEscape="true"/>',
                                        lastDateTesticularExamination: '<s:message code="updateIntakeAppendixOneMensHealthVO.lastDateTesticularExamination" javaScriptEscape="true"/>',
                                        otherMensHealthIssues: '<s:message code="updateIntakeAppendixOneMensHealthVO.otherMensHealthIssues" javaScriptEscape="true"/>',
                                        otherMensHealthIssuesDescription: '<s:message code="updateIntakeAppendixOneMensHealthVO.otherMensHealthIssuesDescription" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixOneMensHealthDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixOne"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixOne/{id}/mensHealth"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2ContextConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2Context.header"/></h3>',
                                    labels: {
                                        dateAndTimeOfAssessment: '<s:message code="updateIntakeAppendixTwoContextVO.dateAndTimeOfAssessment" javaScriptEscape="true"/>',
                                        assessor: '<s:message code="updateIntakeAppendixTwoContextVO.assessor" javaScriptEscape="true"/>',
                                        assessmentLocation: '<s:message code="updateIntakeAppendixTwoContextVO.assessmentLocation" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoContextDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/context"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2MentalState1Config: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2MentalState1.header"/></h3>',
                                    labels: {
                                        mentalStateMood: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateMood" javaScriptEscape="true"/>',
                                        mentalStateMedication: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateMedication" javaScriptEscape="true"/>',
                                        mentalStateTimeWorse: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateTimeWorse" javaScriptEscape="true"/>',
                                        mentalStateAvoidingPeople: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateAvoidingPeople" javaScriptEscape="true"/>',
                                        mentalStateFuture: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateFuture" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoMentalState1Dialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/mentalState"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2MentalState2Config: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2MentalState2.header"/></h3>',
                                    labels: {
                                        mentalStateHarm: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateHarm" javaScriptEscape="true"/>',
                                        mentalStateWorries: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateWorries" javaScriptEscape="true"/>',
                                        mentalStateUseful: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateUseful" javaScriptEscape="true"/>',
                                        mentalStateControl: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateControl" javaScriptEscape="true"/>',
                                        mentalStateVoices: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateVoices" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoMentalState2Dialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/mentalState"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2MentalState3Config: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2MentalState3.header"/></h3>',
                                    labels: {
                                        mentalStateTalkingTV: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateTalkingTV" javaScriptEscape="true"/>',
                                        mentalStateThoughts: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateThoughts" javaScriptEscape="true"/>',
                                        mentalStateVisions: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateVisions" javaScriptEscape="true"/>',
                                        mentalStateSensations: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateSensations" javaScriptEscape="true"/>',
                                        mentalStateSpecialReason: '<s:message code="updateIntakeAppendixTwoMentalStateVO.mentalStateSpecialReason" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoMentalState3Dialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/mentalState"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2PlaceOrientationConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2PlaceOrientation.header"/></h3>',
                                    labels: {
                                        placeWhere: '<s:message code="updateIntakeAppendixTwoOrientationVO.placeWhere" javaScriptEscape="true"/>',
                                        placeCountry: '<s:message code="updateIntakeAppendixTwoOrientationVO.placeCountry" javaScriptEscape="true"/>',
                                        placePartOfCountry: '<s:message code="updateIntakeAppendixTwoOrientationVO.placePartOfCountry" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoPlaceOrientationDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/orientation"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2TimeOrientationConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2TimeOrientation.header"/></h3>',
                                    labels: {
                                        timeDay: '<s:message code="updateIntakeAppendixTwoOrientationVO.timeDay" javaScriptEscape="true"/>',
                                        timeMonth: '<s:message code="updateIntakeAppendixTwoOrientationVO.timeMonth" javaScriptEscape="true"/>',
                                        timeYear: '<s:message code="updateIntakeAppendixTwoOrientationVO.timeYear" javaScriptEscape="true"/>',
                                        timeTime: '<s:message code="updateIntakeAppendixTwoOrientationVO.timeTime" javaScriptEscape="true"/>',
                                        timePartOfDay: '<s:message code="updateIntakeAppendixTwoOrientationVO.timePartOfDay" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoTimeOrientationDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/orientation"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2SleepPatternConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2SleepPattern.header"/></h3>',
                                    labels: {
                                        sleepPattern: '<s:message code="updateIntakeAppendixTwoWellbeingVO.sleepPattern" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoSleepPatternDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/wellbeing"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2AppetiteConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2Appetite.header"/></h3>',
                                    labels: {
                                        appetiteDescription: '<s:message code="updateIntakeAppendixTwoWellbeingVO.appetiteDescription" javaScriptEscape="true"/>',
                                        appetite: '<s:message code="updateIntakeAppendixTwoWellbeingVO.appetite" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoAppetiteDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/wellbeing"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2FeelingsConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2Feelings.header"/></h3>',
                                    labels: {
                                        feelingsAchesAndPains: '<s:message code="updateIntakeAppendixTwoWellbeingVO.feelingsAchesAndPains" javaScriptEscape="true"/>',
                                        feelingsEnjoyingLife: '<s:message code="updateIntakeAppendixTwoWellbeingVO.feelingsEnjoyingLife" javaScriptEscape="true"/>',
                                        feelingsConcentration: '<s:message code="updateIntakeAppendixTwoWellbeingVO.feelingsConcentration" javaScriptEscape="true"/>',
                                        feelingsEnergy: '<s:message code="updateIntakeAppendixTwoWellbeingVO.feelingsEnergy" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoFeelingsDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/wellbeing"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2OtherConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2Other.header"/></h3>',
                                    labels: {
                                        otherGeneralHealth: '<s:message code="updateIntakeAppendixTwoWellbeingVO.otherGeneralHealth" javaScriptEscape="true"/>',
                                        otherFeelings: '<s:message code="updateIntakeAppendixTwoWellbeingVO.otherFeelings" javaScriptEscape="true"/>',
                                        anxiety: '<s:message code="updateIntakeAppendixTwoWellbeingVO.anxiety" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoOtherDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/wellbeing"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app2OutcomeConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app2Outcome.header"/></h3>',
                                    labels: {
                                        outcomes: '<s:message code="updateIntakeAppendixTwoOutcomeVO.outcomes" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixTwoOutcomeDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixTwo"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixTwo/{id}/outcome"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app3ContextConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app3Context.header"/></h3>',
                                    labels: {
                                        dateAndTimeOfAssessment: '<s:message code="updateIntakeAppendixThreeContextVO.dateAndTimeOfAssessment" javaScriptEscape="true"/>',
                                        assessor: '<s:message code="updateIntakeAppendixThreeContextVO.assessor" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixThreeContextDialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixThree"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixThree/{id}/context"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app3Behaviour1Config: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app3Behaviour1.header"/></h3>',
                                    labels: {
                                        behaviourWhat: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourWhat" javaScriptEscape="true"/>',
                                        behaviourWhere: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourWhere" javaScriptEscape="true"/>',
                                        behaviourFrequency: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourFrequency" javaScriptEscape="true"/>',
                                        behaviourDuration: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourDuration" javaScriptEscape="true"/>',
                                        behaviourTriggers: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourTriggers" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixThreeBehaviour1Dialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixThree"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixThree/{id}/behaviour"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            app3Behaviour2Config: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.app3Behaviour2.header"/></h3>',
                                    labels: {
                                        behaviourConsequences: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourConsequences" javaScriptEscape="true"/>',
                                        behaviourResponse: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourResponse" javaScriptEscape="true"/>',
                                        behaviourHowLongPresent: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourHowLongPresent" javaScriptEscape="true"/>',
                                        behaviourAspirations: '<s:message code="updateIntakeAppendixThreeBehaviourVO.behaviourAspirations" javaScriptEscape="true"/>'
                                    },
                                    template: 'intakeFormEditAppendixThreeBehaviour2Dialog',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeAppendixThree"/>',
                                    saveUrl: '<s:url value="/rest/intakeAppendixThree/{id}/behaviour"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>'
                                }
                            },
                            rejectionConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.rejection.header"/></h3>',
                                    labels: {
                                        rejectionReason: '<s:message code="updateRejectFormVO.rejectionReason" javaScriptEscape="true"/>',
                                        rejectionDate: '<s:message code="rejectFormVO.rejectionDate" javaScriptEscape="true"/>',
                                        rejectedBy: '<s:message code="rejectFormVO.rejector.name" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.rejection.summary"/>',
                                    url: '<s:url value="/rest/form/{id}/rejection/mostRecent"/>',
                                    saveUrl: '<s:url value="/rest/formRejection/{id}"/>'
                                }
                            },
                            previousPlacementConfig: {
                                add: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.previousPlacement.header"/></h3>',
                                    labels: {
                                        startDate: '<s:message code="newIntakePreviousPlacementVO.startDate" javaScriptEscape="true"/>',
                                        endDate: '<s:message code="newIntakePreviousPlacementVO.endDate" javaScriptEscape="true"/>',
                                        location: '<s:message code="newIntakePreviousPlacementVO.location" javaScriptEscape="true"/>',
                                        type: '<s:message code="newIntakePreviousPlacementVO.type" javaScriptEscape="true"/>',
                                        contact: '<s:message code="newIntakePreviousPlacementVO.contact" javaScriptEscape="true"/>',
                                        endReason: '<s:message code="newIntakePreviousPlacementVO.endReason" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.add.previousPlacement.summary"/>',
                                    successMessage: '<s:message code="resolve.form.previousPlacement.add.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakePreviousPlacement"/>'
                                },
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.previousPlacement.header"/></h3>',
                                    labels: {
                                        startDate: '<s:message code="updateIntakePreviousPlacementVO.startDate" javaScriptEscape="true"/>',
                                        endDate: '<s:message code="updateIntakePreviousPlacementVO.endDate" javaScriptEscape="true"/>',
                                        location: '<s:message code="updateIntakePreviousPlacementVO.location" javaScriptEscape="true"/>',
                                        type: '<s:message code="updateIntakePreviousPlacementVO.type" javaScriptEscape="true"/>',
                                        contact: '<s:message code="updateIntakePreviousPlacementVO.contact" javaScriptEscape="true"/>',
                                        endReason: '<s:message code="updateIntakePreviousPlacementVO.endReason" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.previousPlacement.summary"/>',
                                    successMessage: '<s:message code="resolve.form.previousPlacement.edit.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakePreviousPlacement/{id}"/>'
                                },
                                remove: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.previousPlacement.header"/></h3>',
                                    labels: {},
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.remove.previousPlacement.summary"/>',
                                    message: '<s:message code="resolve.dialog.remove.previousPlacement.confirm" javaScriptEscape="true"/>',
                                    successMessage: '<s:message code="resolve.form.previousPlacement.remove.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakePreviousPlacement/{id}"/>'
                                }
                            },
                            identifiedRisksConfig: {
                                add: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.identifiedRisk.header"/></h3>',
                                    labels: {
                                        risk: '<s:message code="newIntakeRiskAssessmentVO.risk" javaScriptEscape="true"/>',
                                        likelihood: '<s:message code="newIntakeRiskAssessmentVO.likelihood" javaScriptEscape="true"/>',
                                        impact: '<s:message code="newIntakeRiskAssessmentVO.impact" javaScriptEscape="true"/>',
                                        total: '<s:message code="newIntakeRiskAssessmentVO.total" javaScriptEscape="true"/>',
                                        riskManagementActivity: '<s:message code="newIntakeRiskAssessmentVO.riskManagementActivity" javaScriptEscape="true"/>',
                                        revisedLikelihood: '<s:message code="newIntakeRiskAssessmentVO.revisedLikelihood" javaScriptEscape="true"/>',
                                        revisedImpact: '<s:message code="newIntakeRiskAssessmentVO.revisedImpact" javaScriptEscape="true"/>',
                                        revisedTotal: '<s:message code="newIntakeRiskAssessmentVO.revisedTotal" javaScriptEscape="true"/>',
                                        date: '<s:message code="newIntakeRiskAssessmentVO.riskDate" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="intakeIdentifiedRisk.dialog.add.summary"/>',
                                    successMessage: '<s:message code="resolve.form.risk.add.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeRiskAssessment"/>'
                                },
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.identifiedRisk.header"/></h3>',
                                    labels: {
                                        risk: '<s:message code="updateIntakeRiskAssessmentVO.risk" javaScriptEscape="true"/>',
                                        likelihood: '<s:message code="updateIntakeRiskAssessmentVO.likelihood" javaScriptEscape="true"/>',
                                        impact: '<s:message code="updateIntakeRiskAssessmentVO.impact" javaScriptEscape="true"/>',
                                        total: '<s:message code="updateIntakeRiskAssessmentVO.total" javaScriptEscape="true"/>',
                                        riskManagementActivity: '<s:message code="updateIntakeRiskAssessmentVO.riskManagementActivity" javaScriptEscape="true"/>',
                                        revisedLikelihood: '<s:message code="updateIntakeRiskAssessmentVO.revisedLikelihood" javaScriptEscape="true"/>',
                                        revisedImpact: '<s:message code="updateIntakeRiskAssessmentVO.revisedImpact" javaScriptEscape="true"/>',
                                        revisedTotal: '<s:message code="updateIntakeRiskAssessmentVO.revisedTotal" javaScriptEscape="true"/>',
                                        date: '<s:message code="updateIntakeRiskAssessmentVO.riskDate" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="intakeIdentifiedRisk.dialog.edit.summary"/>',
                                    successMessage: '<s:message code="resolve.form.risk.edit.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeRiskAssessment/{id}"/>'
                                },
                                remove: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.identifiedRisk.header"/></h3>',
                                    labels: {},
                                    narrative: '<s:message javaScriptEscape="true" code="intakeIdentifiedRisk.dialog.remove.summary"/>',
                                    message: '<s:message code="intakeRiskAssessmentVO.results.confirm" javaScriptEscape="true"/>',
                                    successMessage: '<s:message code="resolve.form.risk.remove.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeRiskAssessment/{id}"/>'
                                }
                            },
                            personalCareNeedConfig: {
                                add: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.personalCareNeed.header"/></h3>',
                                    labels: {
                                        need: '<s:message code="newIntakePersonalCareNeedVO.need" javaScriptEscape="true"/>',
                                        helpRequired: '<s:message code="newIntakePersonalCareNeedVO.helpRequired" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.add.personalCareNeed.summary"/>',
                                    successMessage: '<s:message code="resolve.form.personalCareNeed.add.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakePersonalCareNeed"/>'
                                },
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.personalCareNeed.header"/></h3>',
                                    labels: {
                                        need: '<s:message code="updateIntakePersonalCareNeedVO.need" javaScriptEscape="true"/>',
                                        helpRequired: '<s:message code="updateIntakePersonalCareNeedVO.helpRequired" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.personalCareNeed.summary"/>',
                                    successMessage: '<s:message code="resolve.form.personalCareNeed.edit.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakePersonalCareNeed/{id}"/>'
                                },
                                remove: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.personalCareNeed.header"/></h3>',
                                    labels: {},
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.remove.personalCareNeed.summary"/>',
                                    message: '<s:message code="resolve.dialog.remove.personalCareNeed.confirm" javaScriptEscape="true"/>',
                                    successMessage: '<s:message code="resolve.form.personalCareNeed.remove.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakePersonalCareNeed/{id}"/>'
                                }
                            },
                            socialSupportConfig: {
                                add: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.socialSupport.header"/></h3>',
                                    labels: {
                                        support: '<s:message code="newIntakeSupportVO.support" javaScriptEscape="true"/>',
                                        details: '<s:message code="newIntakeSupportVO.details" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.add.socialSupport.summary"/>',
                                    successMessage: '<s:message code="resolve.form.socialSupport.add.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeSupport"/>'
                                },
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.socialSupport.header"/></h3>',
                                    labels: {
                                        support: '<s:message code="updateIntakeSupportVO.support" javaScriptEscape="true"/>',
                                        details: '<s:message code="updateIntakeSupportVO.details" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.socialSupport.summary"/>',
                                    successMessage: '<s:message code="resolve.form.socialSupport.edit.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeSupport/{id}"/>'
                                },
                                remove: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.socialSupport.header"/></h3>',
                                    labels: {},
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.remove.socialSupport.summary"/>',
                                    message: '<s:message code="resolve.dialog.remove.socialSupport.confirm" javaScriptEscape="true"/>',
                                    successMessage: '<s:message code="resolve.form.socialSupport.remove.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeSupport/{id}"/>'
                                }
                            },
                            supportPlanConfig: {
                                add: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.supportPlan.header"/></h3>',
                                    labels: {
                                        action: '<s:message code="newIntakeSupportPlanVO.interventionAndSupport" javaScriptEscape="true"/>',
                                        desiredOutcome: '<s:message code="newIntakeSupportPlanVO.desiredOutcome" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.add.supportPlan.summary"/>',
                                    successMessage: '<s:message code="resolve.form.supportPlan.add.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeSupportPlan"/>'
                                },
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.supportPlan.header"/></h3>',
                                    labels: {
                                        action: '<s:message code="updateIntakeSupportPlanVO.interventionAndSupport" javaScriptEscape="true"/>',
                                        desiredOutcome: '<s:message code="updateIntakeSupportPlanVO.desiredOutcome" javaScriptEscape="true"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.supportPlan.summary"/>',
                                    successMessage: '<s:message code="resolve.form.supportPlan.edit.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeSupportPlan/{id}"/>'
                                },
                                remove: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.supportPlan.header"/></h3>',
                                    labels: {},
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.remove.supportPlan.summary"/>',
                                    message: '<s:message code="resolve.dialog.remove.supportPlan.confirm" javaScriptEscape="true"/>',
                                    successMessage: '<s:message code="resolve.form.supportPlan.remove.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeSupportPlan/{id}"/>'
                                }
                            },
                            medicationConfig: {
                                add: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.medication.header"/></h3>',
                                    labels: {
                                        name: '<s:message code="newIntakeMedicationsVO.name" javaScriptEscape="true"/>',
                                        dosage: '<s:message code="newIntakeMedicationsVO.dosage" javaScriptEscape="true"/>',
                                        form: '<s:message code="newIntakeMedicationsVO.form" javaScriptEscape="true"/>',
                                        prescribedBy: '<s:message code="newIntakeMedicationsVO.prescribedBy" javaScriptEscape="true"/>',
                                        usedFor: '<s:message code="newIntakeMedicationsVO.usedFor" javaScriptEscape="true"/>',
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.add.medication.summary"/>',
                                    successMessage: '<s:message code="resolve.form.medication.add.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeMedication"/>'
                                },
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.medication.header"/></h3>',
                                    labels: {
                                        name: '<s:message code="updateIntakeMedicationsVO.name" javaScriptEscape="true"/>',
                                        dosage: '<s:message code="updateIntakeMedicationsVO.dosage" javaScriptEscape="true"/>',
                                        form: '<s:message code="updateIntakeMedicationsVO.form" javaScriptEscape="true"/>',
                                        prescribedBy: '<s:message code="updateIntakeMedicationsVO.prescribedBy" javaScriptEscape="true"/>',
                                        usedFor: '<s:message code="updateIntakeMedicationsVO.usedFor" javaScriptEscape="true"/>',
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.medication.summary"/>',
                                    successMessage: '<s:message code="resolve.form.medication.edit.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeMedication/{id}"/>'
                                },
                                remove: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.medication.header"/></h3>',
                                    labels: {},
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.remove.medication.summary"/>',
                                    message: '<s:message code="resolve.dialog.remove.medication.confirm" javaScriptEscape="true"/>',
                                    successMessage: '<s:message code="resolve.form.medication.remove.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeMedication/{id}"/>'
                                }
                            },
                            referencedPersonConfig: {
                                add: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.referencedPerson.header"/></h3>',
                                    labels: {
                                        name: '<s:message code="newIntakeReferencedPersonVO.name" javaScriptEscape="true"/>',
                                        relationshipTypeId: '<s:message code="newIntakeReferencedPersonVO.relationshipTypeId" javaScriptEscape="true"/>',
                                        address: '<s:message code="newIntakeReferencedPersonVO.address" javaScriptEscape="true"/>',
                                        contact: '<s:message code="newIntakeReferencedPersonVO.contact" javaScriptEscape="true"/>',
                                        active: '<s:message code="newIntakeReferencedPersonVO.active" javaScriptEscape="true"/>'
                                    },
                                    url: '<s:url value="/rest/intakeForm/{id}/intakeReferencedPerson"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.add.referencedPerson.summary"/>',
                                    successMessage: '<s:message code="resolve.form.referencedPerson.add.success" javaScriptEscape="true"/>',
                                    familialRelationshipTypes: familialRelationshipTypes,
                                    professionalRelationshipTypes: professionalRelationshipTypes
                                },
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.referencedPerson.header"/></h3>',
                                    labels: {
                                        name: '<s:message code="updateIntakeReferencedPersonVO.name" javaScriptEscape="true"/>',
                                        relationshipTypeId: '<s:message code="updateIntakeReferencedPersonVO.relationshipTypeId" javaScriptEscape="true"/>',
                                        address: '<s:message code="updateIntakeReferencedPersonVO.address" javaScriptEscape="true"/>',
                                        contact: '<s:message code="updateIntakeReferencedPersonVO.contact" javaScriptEscape="true"/>',
                                        active: '<s:message code="updateIntakeReferencedPersonVO.active" javaScriptEscape="true"/>'
                                    },
                                    url: '<s:url value="/rest/intakeReferencedPerson/{id}"/>',
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.referencedPerson.summary"/>',
                                    successMessage: '<s:message code="resolve.form.referencedPerson.edit.success" javaScriptEscape="true"/>',
                                    familialRelationshipTypes: familialRelationshipTypes,
                                    professionalRelationshipTypes: professionalRelationshipTypes
                                },
                                remove: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.referencedPerson.header"/></h3>',
                                    labels: {},
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.remove.referencedPerson.summary"/>',
                                    message: '<s:message code="resolve.dialog.remove.referencedPerson.confirm" javaScriptEscape="true"/>',
                                    successMessage: '<s:message code="resolve.form.referencedPerson.remove.success" javaScriptEscape="true"/>',
                                    url: '<s:url value="/rest/intakeReferencedPerson/{id}"/>'
                                }
                            },
                            formName: '<s:message code="form.intake" javaScriptEscape="true"/>',
                            successMessage: '<s:message code="resolve.form.edit.success" javaScriptEscape="true"/>'
                        }
                    }),
                    formStatusButtons = new Y.app.FormStatusButtons({
                        container: '#sectionToolbar .pure-button-group',
                        model: formModel,
                        buttonsConfig: {
                            complete: {
                                template: '<a href="#none" class="pure-button first usp-fx-all complete-form" title="<s:message code="resolve.view.form.complete.button"/> " aria-label="<s:message code="resolve.view.form.complete.button"/> "><i class="fa fa-check-square"></i><span><s:message code="resolve.view.form.complete.button" /></span></a>',
                                statusDraft: true,
                                isPermitted: '${canComplete}' === 'true'
                            },
                            reopen: {
                                template: '<a href="#none" class="pure-button usp-fx-all reopen-form" title="<s:message code="resolve.view.form.reopen.button"/>" aria-label="<s:message code="resolve.view.form.reopen.button"/> "><i class="fa fa-file"></i><span><s:message code="resolve.view.form.reopen.button" /></span></a>',
                                statusComplete: true,
                                isPermitted: '${canReopen}' === 'true'
                            },
                            reject: {
                                template: '<a href="#none" class="pure-button usp-fx-all reject-form" title="<s:message code="resolve.view.form.reject.button"/>" aria-label="<s:message code="resolve.view.form.reject.button"/> "><i class="fa fa-thumbs-down"></i><span><s:message code="resolve.view.form.reject.button" /></span></a>',
                                statusComplete: true,
                                isPermitted: '${canReject}' === 'true'
                            },
                            approve: {
                                template: '<a href="#none" class="pure-button usp-fx-all approve-form" title="<s:message code="resolve.view.form.approve.button"/>" aria-label="<s:message code="resolve.view.form.approve.button"/> "><i class="fa fa-thumbs-up"></i><span><s:message code="resolve.view.form.approve.button" /></span></a>',
                                statusComplete: true,
                                isPermitted: '${canApprove}' === 'true'
                            },
                            remove: {
                                template: '<a href="#none" class="pure-button usp-fx-all delete-form" title="<s:message code="resolve.view.form.delete.button"/>" aria-label="<s:message code="resolve.view.form.delete.button"/> "><i class="fa fa-trash"></i><span><s:message code="resolve.view.form.delete.button" /></span></a>',
                                statusDraft: true,
                                statusComplete: true,
                                isPermitted: '${canDelete}' === 'true'
                            }
                        },
                        dialogConfig: {
                            complete: {
                                message: '<s:message javaScriptEscape="true" code="resolve.form.complete.prompt"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.complete.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.complete.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.complete.header"/></h3>'
                            },
                            reopen: {
                                message: '<s:message code="resolve.form.reopen.prompt" javaScriptEscape="true"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.reopen.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.reopen.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reopen.header"/></h3>'
                            },
                            reject: {
                                message: '<s:message code="resolve.form.reject.prompt" javaScriptEscape="true"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.reject.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.reject.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reject.header"/></h3>',
                                labels: {
                                    rejectionReason: '<s:message code="updateRejectFormVO.rejectionReason" javaScriptEscape="true"/>'
                                }
                            },
                            approve: {
                                message: '<s:message code="resolve.form.approve.prompt" javaScriptEscape="true"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.approve.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.approve.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.approve.header"/></h3>'
                            },
                            remove: {
                                message: '<s:message code="resolve.form.delete.prompt" javaScriptEscape="true"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.remove.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.remove.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.delete.header"/></h3>'
                            },
                            placementReport: {
                                label: '<s:message code="resolve.view.context.prform.type" javaScriptEscape="true"/>'
                            },
                            carePlanRiskAssessment: {
                                label: '<s:message code="resolve.view.context.cpraform.type" javaScriptEscape="true"/>'
                            },
                            intakeForm: {
                                label: '<s:message code="resolve.view.context.intakeform.type" javaScriptEscape="true"/>'
                            },
                            formName: '<s:message code="form.intake" javaScriptEscape="true"/>',
                            url: '<s:url value="/rest/{formType}/{id}/status?action={actionParam}"/>'
                        }
                    });

                //listen for saved event - savedSocial is included here as it
                //contains a mix of  REST and intake form attributes
                formTabs.on(['*:saved', '*:savedSocial'], function() {
                    //reload the model
                    this.load();
                }, formModel);

                //listen for saved appendix event            
                formTabs.on(['*:savedAppendix'], function(e) {
                    //reload the appropriate appendix model
                    switch (e.appendixType) {
                        case 'IntakeAppendixOneFullDetails':
                            appendix1Model.set('id', formModel.get('id'));
                            appendix1Model.load();
                            break;
                        case 'IntakeAppendixTwoFullDetails':
                            appendix2Model.set('id', formModel.get('id'));
                            appendix2Model.load();
                            break;
                        case 'IntakeAppendixThreeFullDetails':
                            appendix3Model.set('id', formModel.get('id'));
                            appendix3Model.load();
                            break;
                    }
                }, formModel);

                formStatusButtons.on('*:saved', function() {
                    //reload the model
                    this.load();
                }, formModel);

                //load the model which will trigger a render
                formModel.load(function() {
                    // Make sure to dispatch the current hash-based URL which was set by
                    // the server to our route handlers.
                    formTabs.render().dispatch();

                    //render our buttons
                    formStatusButtons.render();

                    //update style of toolbar buttons to indicate enablement
                    Y.all('#sectionToolbar .pure-button-loading').removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
                });

                //delegate on toolbar button click - ONLY for print button - other button clicks are handled by statusButtons.js
                Y.delegate('click', function(e) {
                    e.preventDefault();

                    var myWin = window.open('<s:url value="/forms/resolve/intake/${formId}/?print=true"/>');
                }, '#sectionToolbar', '.pure-button-group a.print');
                
                context.render();
            });
        </script>
        
		<%-- Insert Person Warning Dialog Template --%>
		<t:insertTemplate template="/WEB-INF/views/tiles/content/person/panel/personWarningDialog.jsp" />        
    </c:when>
    <c:otherwise>
        <%-- Insert access denied tile --%>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>

