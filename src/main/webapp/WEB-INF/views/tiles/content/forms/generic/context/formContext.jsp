<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>

<h1 tabindex="0" id="formContext" class="iconic-title"></h1>

<script>
	Y.use('form-instance-context','usp-form-FormInstance', function(Y){
		var formId='<c:out value="${formId}"/>',
		    formModel=new Y.usp.form.FormInstance({
				id:'${formId}',
				url: '<s:url value="/rest/formInstance/{id}"/>'
		    }).load();

		//create and render the formContext object
		var formContent=new Y.app.FormInstanceContext({
		    container:Y.one('#formContext'),
		    title:'<s:message code="formsReviews.history"/>',
		    model:formModel,
		    labels:{
		        form:'<s:message code="forms.context.title"/>',
		        startDate:'<s:message code="forms.context.startDate" />'
		    }
		}).render();	    	
	});
</script>
