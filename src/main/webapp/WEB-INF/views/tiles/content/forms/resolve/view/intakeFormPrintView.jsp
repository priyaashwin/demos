<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- Define permissions --%> 
<sec:authorize access="hasPermission(null, 'IntakeForm','IntakeForm.View')" var="canView"/>

<c:choose>
    <c:when test="${canView eq true}">         
        <script>
        Y.use('yui-base',
              'intakeform-print',
              'usp-resolveassessment-IntakeFormFullDetailsEx',
              'resolveassessment-component-enumerations',
              'categories-resolveassessment-component-Need',
              'categories-resolveassessment-component-Support',

              function(Y){
            
            var model=new Y.usp.resolveassessment.IntakeFormFullDetailsEx({
				id:'${formId}',
				url: '<s:url value="/rest/intakeForm/{id}"/>'
			}),
			viewLabels={
   		        referencedPerson:{
   		        	name:'<s:message code="intakeReferencedPersonVO.name"/>',
   		        	relationship:'<s:message code="intakeReferencedPersonVO.relationship"/>',
   		        	address:'<s:message code="intakeReferencedPersonVO.address"/>',
   		        	contact:'<s:message code="intakeReferencedPersonVO.contact"/>'           		        	
   		        },        	    
        	    client: {
       		        information:{
       		        	title: '<s:message code="resolve.view.intakeform.clientInformation.title"/>',
       		        	name:'<s:message code="intakeFormFullDetailsVO.subject.name"/>',
       		        	dateOfBirth:'<s:message code="intakeFormFullDetailsVO.subject.dateOfBirth"/>',
       		        	nhsNumber:'<s:message code="intakeFormFullDetailsVO.subject.nhsNumber"/>',
       		        	address:'<s:message code="intakeFormFullDetailsVO.placementAddress.location"/>',
       		        	contact:'<s:message code="intakeFormFullDetailsVO.subject.contact"/>'
       		        },
       		        consent:{
       		        	title: '<s:message code="resolve.view.intakeform.consent.title"/>',
       		        	consentToShare:'<s:message code="intakeFormFullDetailsVO.consent.consentToShare"/>',
       		        	consentToShareDetails:'<s:message code="intakeFormFullDetailsVO.consent.consentToShareDetails"/>'
       		        },
    	    		mainCarer:{
    	    		    title:'<s:message code="resolve.view.intakeform.mainCarer.title"/>'
    	    		},
    	    		nextOfKin:{
    	    		    title:'<s:message code="resolve.view.intakeform.nextOfKin.title"/>'
    	    		},
    	    		peopleNotToBeContacted:{
    	    		    title:'<s:message code="resolve.view.intakeform.peopleNotToBeContacted.title"/>'
    	            }
       		    },
	            placement:{
	   		        briefHistory:{
    	    		    title:'<s:message code="resolve.view.intakeform.briefHistory.title"/>',
	   		            description:'<s:message code="intakeFormFullDetailsVO.history.briefHistory"/>'
	   		        },
       		        history:{           		            
    	    		    title:'<s:message code="resolve.view.intakeform.history.title"/>',
               			startDate:'<s:message code="intakePreviousPlacementVO.startDate"/>',
               			endDate:'<s:message code="intakePreviousPlacementVO.endDate"/>',
               			location:'<s:message code="intakePreviousPlacementVO.location"/>',
               			type:'<s:message code="intakePreviousPlacementVO.type"/>',
               			contact:'<s:message code="intakePreviousPlacementVO.contact"/>',
               			endReason:'<s:message code="intakePreviousPlacementVO.endReason"/>'
       		        },
       		        currentPlacement:{
    	    		    title:'<s:message code="resolve.view.intakeform.currentPlacement.title"/>',
       		         	startDate:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementStartDate"/>',
       		         	endDate:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementEndDate"/>',
       		         	location:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementLocation"/>',
       		      		type:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementType"/>',
       		      		contact:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementContact"/>',
       		      		challenges:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementChallenges"/>',
       		      		clientViews:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementClientViews"/>',
       		      		assessmentPresentation:'<s:message code="intakeFormFullDetailsVO.currentPlacement.currentPlacementAssessmentPresentation"/>'
       		        },
       		        identifiedRisks:{
    	    		    title:'<s:message code="resolve.view.intakeform.risks.title"/>',
    		            risk:'<s:message code="intakeRiskAssessmentVO.risk"/>',
    		            likelihood:'<s:message code="intakeRiskAssessmentVO.likelihood"/>',
    		            impact:'<s:message code="intakeRiskAssessmentVO.impact"/>',
    		            total:'<s:message code="intakeRiskAssessmentVO.total"/>',
    		            riskManagementActivity:'<s:message code="intakeRiskAssessmentVO.riskManagementActivity"/>',
    		            revisedLikelihood:'<s:message code="intakeRiskAssessmentVO.revisedLikelihood"/>',
    		            revisedImpact:'<s:message code="intakeRiskAssessmentVO.revisedImpact"/>',
    		            revisedTotal:'<s:message code="intakeRiskAssessmentVO.revisedTotal"/>',
    		            date:'<s:message code="intakeRiskAssessmentVO.riskDate"/>'
       		        }	   		        
	            },
       		    referral:{
       		        referrer:{
    	    		    title:'<s:message code="resolve.view.intakeform.referrer.title"/>',
       		         	referrerName:'<s:message code="intakeFormFullDetailsVO.referral.referrerName"/>',
       		 			referrerRole:'<s:message code="intakeFormFullDetailsVO.referral.referrerRole"/>',
       		 			referralReason:'<s:message code="intakeFormFullDetailsVO.referral.referralReason"/>',
       		 			referralDate:'<s:message code="intakeFormFullDetailsVO.referral.referralDate"/>'
       		        },
       		        future:{
    	    		    title:'<s:message code="resolve.view.intakeform.future.title"/>',
       		            clientPlan:"<s:message code="intakeFormFullDetailsVO.future.futurePlanOfClient"/>"
       		        },
       		     	interventionAndSupportPlans:{
    	    		    title:'<s:message code="resolve.view.intakeform.interventionAndSupportPlans.title"/>',
       		         	action:'<s:message code="intakeSupportPlanVO.interventionAndSupport"/>',
       		         	desiredOutcome:'<s:message code="intakeSupportPlanVO.desiredOutcome"/>'
       		        }
       		    },
       		    health:{
       		        health:{
    	    		    title:'<s:message code="resolve.view.intakeform.health.title"/>',
       		            height:'<s:message code="intakeFormFullDetailsVO.health.height"/>',
       		            weight:'<s:message code="intakeFormFullDetailsVO.health.weight"/>',
       		            lastAnnualHealthCheck:'<s:message code="intakeFormFullDetailsVO.health.lastAnnualHealthCheck"/>',
       		            medicalHistory:'<s:message code="intakeFormFullDetailsVO.health.medicalHistory"/>'
       		        },
       		        gp:{
    	    		    title:'<s:message code="resolve.view.intakeform.gp.title"/>',
       		            name:'<s:message code="intakeFormFullDetailsVO.gp.name"/>',
       		            address:'<s:message code="intakeFormFullDetailsVO.gp.address"/>',
       		            contact:'<s:message code="intakeFormFullDetailsVO.gp.contact"/>'
       		        },
       		        professionals:{
    	    		    title:'<s:message code="resolve.view.intakeform.professionals.title"/>'
       		        },
       		        medications:{
    	    		    title:'<s:message code="resolve.view.intakeform.medications.title"/>',
       		         	name:'<s:message code="intakeMedicationsVO.name"/>',
       		         	dosage:'<s:message code="intakeMedicationsVO.dosage"/>',
       		         	form:'<s:message code="intakeMedicationsVO.form"/>',
       		         	prescribedBy:'<s:message code="intakeMedicationsVO.prescribedBy"/>',
       		         	usedFor:'<s:message code="intakeMedicationsVO.usedFor"/>'
       		        },
       		        healthActionPlan:{
    	    		    title:'<s:message code="resolve.view.intakeform.healthActionPlan.title"/>',
       		            healthActionPlan:'<s:message code="intakeFormFullDetailsVO.medication.healthActionPlan"/>',
       		            recentChanges:'<s:message code="intakeFormFullDetailsVO.medication.recentChanges"/>',
       		      		compliance:'<s:message code="intakeFormFullDetailsVO.medication.compliance"/>',
       		   			supportRequired:'<s:message code="intakeFormFullDetailsVO.medication.supportRequired"/>'
       		        },
       		        lifestyle:{
    	    		    title:'<s:message code="resolve.view.intakeform.lifestyle.title"/>',
       		         	alcoholConsumption:'<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleAlcoholConsumption"/>',
       		         	alcoholDetails:'<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleAlcoholDetails"/>',
       		         	smoking:'<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleSmoking"/>',
       		         	smokingDetails:'<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleSmokingDetails"/>',
       		         	drugs:'<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleDrugs"/>',
       		         	drugsDetails:'<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleDrugsDetails"/>',
       		         	supportRequired:'<s:message code="intakeFormFullDetailsVO.lifestyle.lifestyleSupportRequired"/>'
       		        }
       		    },
       		    mentalHealth:{
       		        mentalHealth:{
    	    		    title:'<s:message code="resolve.view.intakeform.mentalHealth.title"/>',
       		            description:'<s:message code="intakeFormFullDetailsVO.mentalHealth.mentalHealthDescription"/>',
       		            sleepPattern:'<s:message code="intakeFormFullDetailsVO.mentalHealth.mentalHealthSleepPattern"/>',
       		            appetite:'<s:message code="intakeFormFullDetailsVO.mentalHealth.mentalHealthAppetite"/>',
       		            behaviour:'<s:message code="intakeFormFullDetailsVO.mentalHealth.behaviourDescription"/>'
       		      	},
       		      	psychology:{
    	    		    title:'<s:message code="resolve.view.intakeform.psychology.title"/>',
       		      	    benefitFromTherapy:'<s:message code="intakeFormFullDetailsVO.psychology.psychologyBenefitFromTherapy"/>', 
       		      	    details:'<s:message code="intakeFormFullDetailsVO.psychology.psychologyDetails"/>',
       		      	    previousInput:'<s:message code="intakeFormFullDetailsVO.psychology.psychologyPreviousInput"/>',
       		      	    unresolvedAbuseIssues:'<s:message code="intakeFormFullDetailsVO.psychology.psychologyUnresolvedAbuseIssues"/>' 
       		      	}
       		    },
       		    physio:{
       		    	physiotherapy:{
    	    		    title:'<s:message code="resolve.view.intakeform.physiotherapy.title"/>',
    	    		    active:'<s:message code="intakeFormFullDetailsVO.physiotherapy.active"/>'
       		    	},
       		     	mobility:{
    	    		    title:'<s:message code="resolve.view.intakeform.mobility.title"/>',
       		     		recentIssues:'<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilityRecentIssues"/>', 
       		     		recentTripsOrFalls:'<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilityRecentTripsOrFalls"/>',
       		     		tripsOrFallsMoreRegular:'<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilityTripsMoreRegular"/>',
       		     		slipsAssessment:'<s:message code="intakeFormFullDetailsVO.physiotherapy.mobilitySlipsAssessment"/>'
       		      	},
       		      	occupationalTherapist:{
    	    		    title:'<s:message code="resolve.view.intakeform.occupationalTherapist.title"/>',
    	    		    active:'<s:message code="intakeFormFullDetailsVO.occupationalTherapy.active"/>'
       		      	},
       		     	occupationalIssues:{
    	    		    title:'<s:message code="resolve.view.intakeform.occupationalIssues.title"/>',
       		     		mobilityProblems:'<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otMobilityProblems"/>', 
       		     		utilityApplicationIssues:'<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otUtilityApplicationIssues"/>',
       		  			aidsAndAdaptations:'<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otAidsAndAdaptations"/>',
       					recentProblems:'<s:message code="intakeFormFullDetailsVO.occupationalTherapy.otRecentProblems"/>'
       		      	}
       		    },
       		    speech: {
					therapy:{
    	    		    title:'<s:message code="resolve.view.intakeform.speechTherapy.title"/>',						
        	    		active:'<s:message code="intakeFormFullDetailsVO.speechAndLanguage.active"/>'
					},
       		    	issues:{
    	    		    title:'<s:message code="resolve.view.intakeform.communicationIssues.title"/>',
       		        	speechCommunicationIssues:'<s:message code="intakeFormFullDetailsVO.speechAndLanguage.speechCommunicationIssues"/>'
       		        },
       		        eating:{
    	    		    title:'<s:message code="resolve.view.intakeform.eating.title"/>',
       		         	specialDiet:'<s:message code="intakeFormFullDetailsVO.speechAndLanguage.specialDiet"/>',
       		         	problemsEating:'<s:message code="intakeFormFullDetailsVO.speechAndLanguage.problemsEating"/>',
       		         	mealtimeIssues:'<s:message code="intakeFormFullDetailsVO.speechAndLanguage.mealtimeIssues"/>'
       		        }
       		    },
       		    needs: {
       		        personalCare:{
    	    		    title:'<s:message code="resolve.view.intakeform.personalCare.title"/>',
       		         	need:'<s:message code="intakePersonalCareNeedVO.need"/>',
       		         	helpRequired:'<s:message code="intakePersonalCareNeedVO.helpRequired"/>'
       		        },
       		     	spiritualNeeds:{
    	    		    title:'<s:message code="resolve.view.intakeform.spiritualNeeds.title"/>',
       		         	needs:'<s:message code="intakeFormFullDetailsVO.spiritual.spiritualNeeds"/>',
       		         	needsBeingMet:'<s:message code="intakeFormFullDetailsVO.spiritual.spiritualNeedsBeingMet"/>',
       		         	needsSupportRequired:'<s:message code="intakeFormFullDetailsVO.spiritual.spiritualNeedsSupportRequired"/>'
       		        },
       		     	social:{
    	    		    title:'<s:message code="resolve.view.intakeform.social.title"/>',
       		         	monday:'<s:message code="resolve.view.intakeform.timetable.monday"/>',
       		         	tuesday:'<s:message code="resolve.view.intakeform.timetable.tuesday"/>',
       		      		wednesday:'<s:message code="resolve.view.intakeform.timetable.wednesday"/>',
       		   			thursday:'<s:message code="resolve.view.intakeform.timetable.thursday"/>',
       					friday:'<s:message code="resolve.view.intakeform.timetable.friday"/>',
      					saturday:'<s:message code="resolve.view.intakeform.timetable.saturday"/>',
       					sunday:'<s:message code="resolve.view.intakeform.timetable.sunday"/>',
       		         	morning:'<s:message code="resolve.view.intakeform.timetable.morning"/>',
       		         	afternoon:'<s:message code="resolve.view.intakeform.timetable.afternoon"/>',
       		         	evening:'<s:message code="resolve.view.intakeform.timetable.evening"/>',
       		         	currentArrangementsIssues:'<s:message code="intakeFormFullDetailsVO.social.socialCurrentArrangementsIssues"/>',
       		         	supportRequired:'<s:message code="intakeFormFullDetailsVO.social.socialSupportRequired"/>'
       		        },
       		        socialSupport:{
    	    		    title:'<s:message code="resolve.view.intakeform.socialSupport.title"/>',
       		            supportType:'<s:message code="intakeSupportVO.support"/>',
       		            details:'<s:message code="intakeSupportVO.details"/>'
       		        }
       		    },
       		 	app1Comms: {
	    		    title:'<s:message code="resolve.view.intakeform.tabs.app1Comms"/>',
    		        healthCommunication:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Comms.title"/>',
    		        	communicationVerbal:'<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationVerbal"/>',
    		        	communicationSignsAndPictures:'<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationSignsAndPictures"/>',
    		        	communicationGesturesAndNoises:'<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationGesturesAndNoises"/>',
    		        	communicationEyePointing:'<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationEyePointing"/>',
    		        	communicationOtherMethods:'<s:message code="intakeFormFullDetailsVO.appendix1.communication.communicationOtherMethods"/>',
    		        },
    		        healthFamily1:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Family1.title"/>',
    		        	familyHistoryHeartProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryHeartProblems"/>',
    		        	familyHistoryDiabetes:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryDiabetes"/>',
    		        	familyHistoryAsthma:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryAsthma"/>',
    		        	familyHistoryCancer:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryCancer"/>',
    		        	familyHistoryGlaucoma:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryGlaucoma"/>',
    		        },
    		        healthFamily2:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Family2.title"/>',
    		        	familyHistoryEpilepsy:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryEpilepsy"/>',
    		        	familyHistoryMentalHealth:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryMentalHealth"/>',
    		        	familyHistoryHighBloodPressure:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryHighBloodPressure"/>',
    		        	familyHistoryOther:'<s:message code="intakeFormFullDetailsVO.appendix1.familyHistory.familyHistoryOther"/>',
    		        },
    		        healthReviews:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Reviews.title"/>',
    		        	lastMedicalReviewDate:'<s:message code="intakeFormFullDetailsVO.appendix1.reviews.lastMedicalReviewDate"/>',
    		        	lastMedicationReviewCheck:'<s:message code="intakeFormFullDetailsVO.appendix1.reviews.lastMedicationReviewCheck"/>',
    		        	requireBloodTests:'<s:message code="intakeFormFullDetailsVO.appendix1.reviews.requireBloodTests"/>',
    		        	bloodTestDetails:'<s:message code="intakeFormFullDetailsVO.appendix1.reviews.bloodTestDetails"/>',
    		        	lastBloodTestDate:'<s:message code="intakeFormFullDetailsVO.appendix1.reviews.lastBloodTestDate"/>',
    		        	bloodTestSupportRequired:'<s:message code="intakeFormFullDetailsVO.appendix1.reviews.bloodTestSupportRequired"/>',
    		        	bloodTestSupportDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.reviews.bloodTestSupportDescription"/>',
    		        },
    		        healthVision:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Vision.title"/>',
    		        	eyesightProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.vision.eyesightProblems"/>',
    		        	wearsGlassesOrLenses:'<s:message code="intakeFormFullDetailsVO.appendix1.vision.wearsGlassesOrLenses"/>',
    		        	lastEyeTestDate:'<s:message code="intakeFormFullDetailsVO.appendix1.vision.lastEyeTestDate"/>',
    		        	glassesLensesSupportRequired:'<s:message code="intakeFormFullDetailsVO.appendix1.vision.glassesLensesSupportRequired"/>',
    		        	glassesLensesSupportDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.vision.glassesLensesSupportDescription"/>',
    		        },
    		        healthHearing:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Hearing.title"/>',
    		        	hearingProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingProblems"/>',
    		        	hearingAidWorn:'<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingAidWorn"/>',
    		        	lastHearingAssessmentDate:'<s:message code="intakeFormFullDetailsVO.appendix1.hearing.lastHearingAssessmentDate"/>',
    		        	hearingSupportRequired:'<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingSupportRequired"/>',
    		        	hearingSupportDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.hearing.hearingSupportDescription"/>',
    		        },
    		        healthDental:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Dental.title"/>',
    		        	dentalProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.dentalProblems"/>',
    		        	visitsDentist:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.visitsDentist"/>',
    		        	lastDentalAppointmentDate:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.lastDentalAppointmentDate"/>',
    		        	hasDentures:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.hasDentures"/>',
    		        	denturesWorn:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.denturesWorn"/>',
    		        	reasonsDenturesNotWorn:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.reasonsDenturesNotWorn"/>',
    		        	dentalSupportRequired:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.dentalSupportRequired"/>',
    		        	dentalSupportDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.dental.dentalSupportDescription"/>',
    		        },
    		        healthFootcare:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Footcare.title"/>',
    		        	footProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.footProblems"/>',
    		        	footProblemsDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.footProblemsDescription"/>',
    		        	seesChiropodist:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.seesChiropodist"/>',
    		        	lastChiropodyAppointmentDate:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.lastChiropodyAppointmentDate"/>',
    		        	chiropodistName:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.chiropodistName"/>',
    		        	chiropodistContactNumber:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.chiropodistContactNumber"/>',
    		        	chiropodistAddress:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.chiropodistAddress"/>',
    		        	feetSupportRequired:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.feetSupportRequired"/>',
    		        	feetSupportDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.footcare.feetSupportDescription"/>',
    		        },
    		        healthContinence:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Continence.title"/>',
    		        	continenceProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceProblems"/>',
    		        	continenceProblemsDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceProblemsDescription"/>',
    		        	seenByContinenceAdvisor:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.seenByContinenceAdvisor"/>',
    		        	lastContinenceAppointmentDate:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.lastContinenceAppointmentDate"/>',
    		        	continenceAdvisorName:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceAdvisorName"/>',
    		        	continenceAdvisorContactNumber:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceAdvisorContactNumber"/>',
    		        	continenceAdvisorAddress:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.continenceAdvisorAddress"/>',
    		        	suffersFromUrineInfections:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.suffersFromUrineInfections"/>',
    		        	urineInfectionDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.continence.urineInfectionDescription"/>',
    		        },
    		        healthEpilepsy:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Epilepsy.title"/>',
    		        	suffersFromEpilepsy:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.suffersFromEpilepsy"/>',
    		        	seizureDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.seizureDescription"/>',
    		        	epilepsyMonitored:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsyMonitored"/>',
    		        	howOftenEpilepsyMonitored:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.howOftenEpilepsyMonitored"/>',
    		        	dateLastEpilepsyAppointment:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.dateLastEpilepsyAppointment"/>',
    		        	epilepsySpecialistName:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsySpecialistName"/>',
    		        	epilepsySpecialistContactNumber:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsySpecialistContactNumber"/>',
    		        	epilepsySpecialistAddress:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.epilepsySpecialistAddress"/>',
    		        	emergencyMedProtocol:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.emergencyMedProtocol"/>',
    		        	emergencyMedProtocolDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.epilepsy.emergencyMedProtocolDescription"/>',
    		        },
    		        healthSkin:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1Skin.title"/>',
    		        	skinProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinProblems"/>',
    		        	seenBySkinSpecialist:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.seenBySkinSpecialist"/>',
    		        	dateLastSkinAppointment:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.dateLastSkinAppointment"/>',
    		        	skinSpecialistName:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSpecialistName"/>',
    		        	skinSpecialistContactNumber:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSpecialistContactNumber"/>',
    		        	skinSpecialistAddress:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSpecialistAddress"/>',
    		        	skinTreatmentDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinTreatmentDescription"/>',
    		        	skinSupportRequired:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSupportRequired"/>',
    		        	skinSupportDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.skin.skinSupportDescription"/>',
    		        },
    		        healthBreathingCirculation:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1BreathingCirculation.title"/>',
    		        	suffersFromBreathlessness:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.suffersFromBreathlessness"/>',
    		        	breathlessnessDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.breathlessnessDescription"/>',
    		        	respiratoryHeartProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.respiratoryHeartProblems"/>',
    		        	respiratoryHeartProblemsDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.respiratoryHeartProblemsDescription"/>',
    		        	circulationProblems:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationProblems"/>',
    		        	circulationProblemsDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationProblemsDescription"/>',
    		        	breathingCirculationChecked:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.breathingCirculationChecked"/>',
    		        	dateLastCirculationAppointment:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.dateLastCirculationAppointment"/>',
    		        	circulationSpecialistName:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSpecialistName"/>',
    		        	circulationSpecialistContactNumber:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSpecialistContactNumber"/>',
    		        	circulationSpecialistAddress:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSpecialistAddress"/>',
    		        	circulationSupportRequired:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSupportRequired"/>',
    		        	circulationSupportDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.breathingCirculation.circulationSupportDescription"/>',
    		        },
    		        healthMensHealth:{
    	    		    title:'<s:message code="resolve.view.intakeform.app1MensHealth.title"/>',
    		        	attendsWellManClinic:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.attendsWellManClinic"/>',
    		        	attendsWellManClinicFrequency:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.attendsWellManClinicFrequency"/>',
    		        	mensHealthSpecialistName:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.mensHealthSpecialistName"/>',
         		      	mensHealthSpecialistContactNumber:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.mensHealthSpecialistContactNumber"/>',
    		        	mensHealthSpecialistAddress:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.mensHealthSpecialistAddress"/>',
    		        	carriesOutTesticularExaminations:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.carriesOutTesticularExaminations"/>',
    		        	lastDateTesticularExamination:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.lastDateTesticularExamination"/>',
    		        	otherMensHealthIssues:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.otherMensHealthIssues"/>',
    		        	otherMensHealthIssuesDescription:'<s:message code="intakeFormFullDetailsVO.appendix1.mensHealth.otherMensHealthIssuesDescription"/>',
    		        }
    			},       		    
                app2MentalHealth: {
	    		    title:'<s:message code="resolve.view.intakeform.tabs.app2MentalHealth"/>',
       		 		context:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2Context.title"/>',
       		 			dateTimeAssessment: '<s:message code="intakeFormFullDetailsVO.appendix2.context.dateTimeAssessment"/>',
       		 			assessedBy: '<s:message code="intakeFormFullDetailsVO.appendix2.context.assessedBy"/>',
       		 			assessedWhere: '<s:message code="intakeFormFullDetailsVO.appendix2.context.assessedWhere"/>',
       		 		},
       		 		mentalState1:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2MentalState1.title"/>',
       		 			mood: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.mood"/>',
            			medication: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.medication"/>',
            			moodWorseWhen: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.moodWorseWhen"/>',
            			avoidingPeople:	'<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.avoidingPeople"/>',
            			futureSelf: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState1.futureSelf"/>',
       		 		},
       		 		mentalState2:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2MentalState2.title"/>',
       		 			thinkingOfHarming: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.thinkingOfHarming"/>',
       		 			worries: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.worries"/>',
       		 			feelingUseful: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.feelingUseful"/>',
       		 			bodyControl: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.bodyControl"/>',
       		 			hearingVoices: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState2.hearingVoices"/>',
       		 		},
       		 		mentalState3:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2MentalState3.title"/>',
       		 			talkingTV: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.talkingTV"/>',
       		 			thoughtControl: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.thoughtControl"/>',
       		 			seeingThings: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.seeingThings"/>',
       		 			touch: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.touch"/>',
       		 			specialPurpose: '<s:message code="intakeFormFullDetailsVO.appendix2.mentalState3.specialPurpose"/>',
       		 		},
       		 		placeOrientation:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2PlaceOrientation.title"/>',
       		 			whereAmI: '<s:message code="intakeFormFullDetailsVO.appendix2.placeOrientation.whereAmI"/>',
       		 			whichCountry: '<s:message code="intakeFormFullDetailsVO.appendix2.placeOrientation.whichCountry"/>',
       		 			whereInCountry: '<s:message code="intakeFormFullDetailsVO.appendix2.placeOrientation.whereInCountry"/>',
       		 		},
       		 		timeOrientation:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2TimeOrientation.title"/>',
           		 		whichDay: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichDay"/>',
       		 			whichMonth: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichMonth"/>',
   		 				whichYear: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichYear"/>',
	 					whatTime: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whatTime"/>',
						whichPartOfDay: '<s:message code="intakeFormFullDetailsVO.appendix2.timeOrientation.whichPartOfDay"/>',
       		 		},
       		 		sleepPattern:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2SleepPattern.title"/>',
       		 			sleepPattern:'<s:message code="intakeFormFullDetailsVO.appendix2.sleepPattern.sleepPattern"/>',
       		 		},
       		 		appetite:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2Appetite.title"/>',
       		 			appetiteLike:'<s:message code="intakeFormFullDetailsVO.appendix2.appetite.appetiteLike"/>',
       		 			whenAreYouEating:'<s:message code="intakeFormFullDetailsVO.appendix2.appetite.whenAreYouEating"/>',
       		 		},
       		 		feelings:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2Feelings.title"/>',
       		 			achesPains:'<s:message code="intakeFormFullDetailsVO.appendix2.feelings.achesPains"/>',
       		 			enjoyingLife:'<s:message code="intakeFormFullDetailsVO.appendix2.feelings.enjoyingLife"/>',
       		 			forgetful:'<s:message code="intakeFormFullDetailsVO.appendix2.feelings.forgetful"/>',
       		 			energy:'<s:message code="intakeFormFullDetailsVO.appendix2.feelings.energy"/>',
       		 		},
       		 		other:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2Other.title"/>',
       		 			generalHealth:'<s:message code="intakeFormFullDetailsVO.appendix2.other.generalHealth"/>',
       		 			emotions:'<s:message code="intakeFormFullDetailsVO.appendix2.other.emotions"/>',
       		 			anxiety:'<s:message code="intakeFormFullDetailsVO.appendix2.other.anxiety"/>',
       		 		},
       		 		outcome:{
    	    		    title:'<s:message code="resolve.view.intakeform.app2Outcome.title"/>',
       		 			outcomes:'<s:message code="intakeFormFullDetailsVO.appendix2.outcome.outcomes"/>',
       		 		}
       		 	},       		    
               	app3Behaviour: {
	    		    title:'<s:message code="resolve.view.intakeform.tabs.app3Behaviour"/>',
               		context:{
    	    		    title:'<s:message code="resolve.view.intakeform.app3Context.title"/>',
       		 			assessmentDateTime:'<s:message code="intakeFormFullDetailsVO.appendix3.context.assessmentDateTime"/>',
       		 			assessedBy:'<s:message code="intakeFormFullDetailsVO.appendix3.context.assessedBy"/>',
       		 		},
       		 		behaviour1:{
    	    		    title:'<s:message code="resolve.view.intakeform.app3Behaviour1.title"/>',
           		 		what:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.what"/>',
          		 		where:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.where"/>',
           		 		frequency:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.frequency"/>',
           		 		duration:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.duration"/>',
           		 		triggers:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour1.triggers"/>',
       		 		},
       		 		behaviour2:{
    	    		    title:'<s:message code="resolve.view.intakeform.app3Behaviour2.title"/>',
           		 		consequencesImpact:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.consequencesImpact"/>',
          		 		responseToBehaviour:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.responseToBehaviour"/>',
           		 		howLongBehaviourPresent:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.howLongBehaviourPresent"/>',
           		 		desiredAlternatives:'<s:message code="intakeFormFullDetailsVO.appendix3.behaviour2.desiredAlternatives"/>',
       		 		}
       		 	},
    		    rejection: {
    		        rejectionReason: '<s:message code="rejectFormVO.rejectionReason"/>',
    		        rejectionDate: '<s:message code="rejectFormVO.rejectionDate"/>',
    		        rejectedBy: '<s:message code="rejectFormVO.rejector.name"/>',
                    noRejections:'<s:message code="resolve.view.form.rejection.noRejections"/>', 
    		    }
    		},
    		printView=new Y.app.IntakeFormPrintView({
                container:'body',
               	model:model,
               	labels:viewLabels,
               	otherData:{
	                enumTypes: {
                       likelihood: Y.usp.resolveassessment.enum.Likelihood.values,
                       impact: Y.usp.resolveassessment.enum.Impact.values,
                       appetite: Y.usp.resolveassessment.enum.Appetite.values,
                       anxiety: Y.usp.resolveassessment.enum.Anxiety.values,
                       dayPart: Y.usp.resolveassessment.enum.DayPart.values
    	            }
    			},
        	    codedEntries: {
                    needTypes: Y.uspCategory.resolveassessment.need.category.codedEntries,
                    supportTypes: Y.uspCategory.resolveassessment.support.category.codedEntries
                }
           });
           
           model.load(function(){
               printView.render();
           });
        });
		</script>
		
		<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />
		
    </c:when>
    <c:otherwise>
        <%-- Insert access denied tile --%>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>

