<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- Define permissions --%> 
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.Update')" var="canUpdate"/>
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.Reject')" var="canUpdateRejection"/>
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.Archive')" var="canDelete"/>
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.Authorise')" var="canApprove"/>
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.Reject')" var="canReject"/>
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.Complete')" var="canComplete"/>
<sec:authorize access="hasPermission(null, 'PlacementReport','PlacementReport.Uncomplete')" var="canReopen"/>

<c:choose>
    <%-- if the subject context for the page is person --%>
    <c:when test="${!empty person}">
        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Add')" var="canAddWarning"/>
        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Update')" var="canUpdateWarning"/>
    </c:when>
    <%-- if the subject context for the page is group i.e. ${!empty group}"> --%>
    <c:otherwise>
        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Add')" var="canAddWarning"/>
        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Update')" var="canUpdateWarning"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${canView eq true}"> 
        <div id="formTabs" />
        
    <script>
    Y.use('yui-base',
            'form-content-context',
            'prform-tabs',
            'tabbed-page-contents-plugin',
            'usp-resolveassessment-PlacementReport',
            'form-status-buttons',
            'usp-person-PersonWithWarning',
            function(Y) {
    
                //The Placement Report Form model
                Y.namespace('app').PlacementReport = Y.Base.create("placementReport", Y.usp.resolveassessment.PlacementReport, [], {}, {
                    ATTRS: {
                        canUpdatePermission: 'initOnly',
                        canUpdateRejectionPermission: 'initOnly',
                        canUpdate: {
                            value: false,
                            writeOnce: 'initOnly',
                            getter: function() {
                                return !this.get('canUpdatePermission') ? false : this.get('status') === STATE_DRAFT;
                            }
                        },
                        canUpdateRejection: {
                            value: false,
                            writeOnce: 'initOnly',
                            getter: function() {
                                return !this.get('canUpdateRejectionPermission') ? false : this.get('status') === STATE_DRAFT;
                            }
                        }
                    }
                });
                var STATE_DRAFT = 'DRAFT',
                    formModel = new Y.app.PlacementReport({
                        id: '${formId}',
                        url: '<s:url value="/rest/placementReport/{id}"/>',
                        canUpdatePermission: '${canUpdate}' === 'true',
                        canUpdateRejectionPermission: '${canUpdateRejection}' === 'true'
                    }),
                    context = new Y.app.FormContentContext({
                        container: Y.one('#formContext'),
                        model: formModel,
                        personModel: new Y.usp.person.PersonWithWarning({
                            id: '<c:out value="${person.id}"/>',
                            personIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                            name: '${esc:escapeJavaScript(person.name)}',
                            url: '<s:url value="/rest/person/{id}"/>'
                        }).load(),
                        labels: {
                          <c:choose>
                          <c:when test="${canAddWarning} or ${canUpdateWarning}">
                          warningTitle:'<s:message code="button.hovermanagewarnings"/>',
                          </c:when>
                          <c:otherwise>
                          warningTitle:'<s:message code="button.hoverviewwarnings"/>',
                          </c:otherwise>
                          </c:choose>   
                            formView: '<s:message code="resolve.view.context.title"/>',
                            formType: '<s:message code="resolve.view.context.prform.type"/>',
                            startDate: '<s:message code="resolve.view.context.startDate" />'
                        },
                        canAddWarning: '${canAddWarning}' === 'true',
                        canUpdateWarning: '${canUpdateWarning}' === 'true'
                    }),
                    viewLabels = {
                        background: {
                            dateStarted: '<s:message code="placementReportVO.dateStarted"/>',
                            name: '<s:message code="placementReportVO.subject.name"/>',
                            dateOfBirth: '<s:message code="placementReportVO.subject.dateOfBirth"/>',
                            placementAddress: '<s:message code="placementReportVO.placementAddress"/>',
                            background: '<s:message code="placementReportVO.background"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        health: {
                            health: '<s:message code="placementReportVO.health"/>',
                            dietAndPhysicalHealth: '<s:message code="placementReportVO.dietAndPhysicalHealth"/>',
                            mentalHealth: '<s:message code="placementReportVO.mentalHealth"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        medication: {
                            medication: '<s:message code="placementReportVO.medication"/>',
                            optician: '<s:message code="placementReportVO.optician"/>',
                            dental: '<s:message code="placementReportVO.dental"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        gp: {
                            healthPromotion: '<s:message code="placementReportVO.healthPromotion"/>',
                            gpAppointmentsAndTelephoneContact: '<s:message code="placementReportVO.gpAppointmentsAndTelephoneContact"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        educationEmployment: {
                            education: '<s:message code="placementReportVO.education"/>',
                            employment: '<s:message code="placementReportVO.employment"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        social: {
                            socialPresentationAndBehaviour: '<s:message code="placementReportVO.socialPresentationAndBehaviour"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        skills: {
                            selfCareSkills: '<s:message code="placementReportVO.selfCareSkills"/>',
                            independentLivingSkills: '<s:message code="placementReportVO.independentLivingSkills"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        identity: {
                            identityAndContactWithFamily: '<s:message code="placementReportVO.identityAndContactWithFamily"/>',
                            safeguarding: '<s:message code="placementReportVO.safeguarding"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        leisure: {
                            leisureActivitiesCommunityAccess: '<s:message code="placementReportVO.leisureActivitiesCommunityAccess"/>',
                            finance: '<s:message code="placementReportVO.finance"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        contacts: {
                            forensicCommunityNurseContact: '<s:message code="placementReportVO.forensicCommunityNurseContact"/>',
                            socialWorkerContact: '<s:message code="placementReportVO.socialWorkerContact"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        summary: {
                            summaryAndAnalysis: '<s:message code="placementReportVO.summaryAndAnalysis"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        },
                        rejection: {
                            rejectionReason: '<s:message code="rejectFormVO.rejectionReason"/>',
                            rejectionDate: '<s:message code="rejectFormVO.rejectionDate"/>',
                            rejectedBy: '<s:message code="rejectFormVO.rejector.name"/>',
                            noRejections: '<s:message code="resolve.view.form.rejection.noRejections"/>',
    
                            //buttons
                            editButton: '<s:message code="button.edit"/>'
                        }
                    },
                    formTabs = new Y.app.PRFormTabsView({
                        plugins: [{
                            fn: Y.Plugin.usp.TabbedPageContentsPlugin
                        }],
                        root: '<s:url value="/forms/resolve/place/${formId}/"/>',
                        container: '#formTabs',
                        tabs: {
                            background: {
                                label: '<s:message code="resolve.view.prform.tabs.background"/>',
                                contents: [{
                                    id: 'backgroundView',
                                    label: '<s:message code="resolve.view.prform.background.title"/>'
                                }],
                                config: {
                                    model: formModel,
                                    backgroundConfig: {
                                        title: '<s:message code="resolve.view.prform.background.title"/>',
                                        labels: viewLabels.background
                                    }
                                }
                            },
                            health: {
                                label: '<s:message code="resolve.view.prform.tabs.health"/>',
                                contents: [{
                                    id: 'healthView',
                                    label: '<s:message code="resolve.view.prform.health.title"/>'
                                }, {
                                    id: 'medicationView',
                                    label: '<s:message code="resolve.view.prform.medication.title"/>'
                                }, {
                                    id: 'gpView',
                                    label: '<s:message code="resolve.view.prform.gp.title"/>'
                                }],
                                config: {
                                    model: formModel,
                                    healthConfig: {
                                        title: '<s:message code="resolve.view.prform.health.title"/>',
                                        labels: viewLabels.health
                                    },
                                    medicationConfig: {
                                        title: '<s:message code="resolve.view.prform.medication.title"/>',
                                        labels: viewLabels.medication
                                    },
                                    gpConfig: {
                                        title: '<s:message code="resolve.view.prform.gp.title"/>',
                                        labels: viewLabels.gp
                                    }
                                }
                            },
                            educationemployment: {
                                label: '<s:message code="resolve.view.prform.tabs.educationemployment"/>',
                                contents: [{
                                    id: 'educationemploymentView',
                                    label: '<s:message code="resolve.view.prform.educationemployment.title"/>'
                                }, {
                                    id: 'socialView',
                                    label: '<s:message code="resolve.view.prform.social.title"/>'
                                }],
                                config: {
                                    model: formModel,
                                    educationemploymentConfig: {
                                        title: '<s:message code="resolve.view.prform.educationemployment.title"/>',
                                        labels: viewLabels.educationEmployment
                                    },
                                    socialConfig: {
                                        title: '<s:message code="resolve.view.prform.social.title"/>',
                                        labels: viewLabels.social
                                    }
                                }
                            },
                            skills: {
                                label: '<s:message code="resolve.view.prform.tabs.skills"/>',
                                contents: [{
                                    id: 'skillsView',
                                    label: '<s:message code="resolve.view.prform.skills.title"/>'
                                }, {
                                    id: 'identityView',
                                    label: '<s:message code="resolve.view.prform.identity.title"/>'
                                }, {
                                    id: 'leisureView',
                                    label: '<s:message code="resolve.view.prform.leisure.title"/>'
                                }],
                                config: {
                                    model: formModel,
                                    skillsConfig: {
                                        title: '<s:message code="resolve.view.prform.skills.title"/>',
                                        labels: viewLabels.skills
                                    },
                                    identityConfig: {
                                        title: '<s:message code="resolve.view.prform.identity.title"/>',
                                        labels: viewLabels.identity
                                    },
                                    leisureConfig: {
                                        title: '<s:message code="resolve.view.prform.leisure.title"/>',
                                        labels: viewLabels.leisure
                                    }
                                }
                            },
                            contacts: {
                                label: '<s:message code="resolve.view.prform.tabs.contacts"/>',
                                contents: [{
                                    id: 'contactsView',
                                    label: '<s:message code="resolve.view.prform.contacts.title"/>'
                                }],
                                config: {
                                    model: formModel,
                                    contactsConfig: {
                                        title: '<s:message code="resolve.view.prform.contacts.title"/>',
                                        labels: viewLabels.contacts
                                    }
                                }
                            },
                            summary: {
                                label: '<s:message code="resolve.view.prform.tabs.summary"/>',
                                contents: [{
                                    id: 'summaryView',
                                    label: '<s:message code="resolve.view.prform.summary.title"/>'
                                }],
                                config: {
                                    model: formModel,
                                    summaryConfig: {
                                        title: '<s:message code="resolve.view.prform.summary.title"/>',
                                        labels: viewLabels.summary
                                    }
                                }
                            },
                            rejection: {
                                label: '<s:message code="resolve.view.prform.tabs.rejection"/>',
                                contents: [{
                                    id: 'rejectionView',
                                    label: '<s:message code="resolve.view.prform.rejection.title"/>'
                                }],
                                config: {
                                    model: formModel,
                                    rejectionConfig: {
                                        title: '<s:message code="resolve.view.prform.rejection.title"/>',
                                        labels: viewLabels.rejection
                                    }
                                }
                            }
                        },
                        dialogConfig: {
                            defaultHeader: 'loading...',
                            backgroundConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.background.header"/></h3>',
                                    labels: {
                                        dateStarted: '<s:message code="updatePlacementReportVO.dateStarted"/>',
                                        name: '<s:message code="updatePlacementReportVO.subject.name"/>',
                                        dateOfBirth: '<s:message code="updatePlacementReportVO.subject.dateOfBirth"/>',
                                        placementAddress: '<s:message code="updatePlacementReportVO.placementAddress"/>',
                                        background: '<s:message code="updatePlacementReportVO.background"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            healthConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.health.header"/></h3>',
                                    labels: {
                                        health: '<s:message code="updatePlacementReportVO.health"/>',
                                        dietAndPhysicalHealth: '<s:message code="updatePlacementReportVO.dietAndPhysicalHealth"/>',
                                        mentalHealth: '<s:message code="updatePlacementReportVO.mentalHealth"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            medicationConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.medication.header"/></h3>',
                                    labels: {
                                        medication: '<s:message code="updatePlacementReportVO.medication"/>',
                                        optician: '<s:message code="updatePlacementReportVO.optician"/>',
                                        dental: '<s:message code="updatePlacementReportVO.dental"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            gpConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.gp.header"/></h3>',
                                    labels: {
                                        healthPromotion: '<s:message code="updatePlacementReportVO.healthPromotion"/>',
                                        gpAppointmentsAndTelephoneContact: '<s:message code="updatePlacementReportVO.gpAppointmentsAndTelephoneContact"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            educationemploymentConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.educationEmployment.header"/></h3>',
                                    labels: {
                                        education: '<s:message code="updatePlacementReportVO.education"/>',
                                        employment: '<s:message code="updatePlacementReportVO.employment"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            socialConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.social.header"/></h3>',
                                    labels: {
                                        socialPresentationAndBehaviour: '<s:message code="updatePlacementReportVO.socialPresentationAndBehaviour"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            skillsConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.skills.header"/></h3>',
                                    labels: {
                                        selfCareSkills: '<s:message code="updatePlacementReportVO.selfCareSkills"/>',
                                        independentLivingSkills: '<s:message code="updatePlacementReportVO.independentLivingSkills"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            identityConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.identity.header"/></h3>',
                                    labels: {
                                        identityAndContactWithFamily: '<s:message code="updatePlacementReportVO.identityAndContactWithFamily"/>',
                                        safeguarding: '<s:message code="updatePlacementReportVO.safeguarding"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            leisureConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.leisure.header"/></h3>',
                                    labels: {
                                        leisureActivitiesCommunityAccess: '<s:message code="updatePlacementReportVO.leisureActivitiesCommunityAccess"/>',
                                        finance: '<s:message code="updatePlacementReportVO.finance"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            contactsConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.contacts.header"/></h3>',
                                    labels: {
                                        forensicCommunityNurseContact: '<s:message code="updatePlacementReportVO.forensicCommunityNurseContact"/>',
                                        socialWorkerContact: '<s:message code="updatePlacementReportVO.socialWorkerContact"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            summaryConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.summary.header"/></h3>',
                                    labels: {
                                        summaryAndAnalysis: '<s:message code="updatePlacementReportVO.summaryAndAnalysis"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                    url: '<s:url value="/rest/placementReport/{id}"/>'
                                }
                            },
                            rejectionConfig: {
                                edit: {
                                    headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.rejection.header"/></h3>',
                                    labels: {
                                        rejectionReason: '<s:message code="updateRejectFormVO.rejectionReason"/>',
                                        rejectionDate: '<s:message code="rejectFormVO.rejectionDate"/>',
                                        rejectedBy: '<s:message code="rejectFormVO.rejector.name"/>'
                                    },
                                    narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.rejection.summary"/>',
                                    url: '<s:url value="/rest/form/{id}/rejection/mostRecent"/>',
                                    saveUrl: '<s:url value="/rest/formRejection/{id}"/>'
                                }
                            },
                            formName: '<s:message code="form.pr"/>',
                            successMessage: '<s:message code="resolve.form.edit.success"/>'
                        }
                    }),
                    formStatusButtons = new Y.app.FormStatusButtons({
                        container: '#sectionToolbar .pure-button-group',
                        model: formModel,
                        buttonsConfig: {
                            complete: {
                                template: '<a href="#none" class="pure-button first usp-fx-all complete-form" title="<s:message code="resolve.view.form.complete.button"/> " aria-label="<s:message code="resolve.view.form.complete.button"/> "><i class="fa fa-check-square"></i><span><s:message code="resolve.view.form.complete.button" /></span></a>',
                                statusDraft: true,
                                isPermitted: '${canComplete}' === 'true'
                            },
                            reopen: {
                                template: '<a href="#none" class="pure-button usp-fx-all reopen-form" title="<s:message code="resolve.view.form.reopen.button"/>" aria-label="<s:message code="resolve.view.form.reopen.button"/> "><i class="fa fa-file"></i><span><s:message code="resolve.view.form.reopen.button" /></span></a>',
                                statusComplete: true,
                                isPermitted: '${canReopen}' === 'true'
                            },
                            reject: {
                                template: '<a href="#none" class="pure-button usp-fx-all reject-form" title="<s:message code="resolve.view.form.reject.button"/>" aria-label="<s:message code="resolve.view.form.reject.button"/> "><i class="fa fa-thumbs-down"></i><span><s:message code="resolve.view.form.reject.button" /></span></a>',
                                statusComplete: true,
                                isPermitted: '${canReject}' === 'true'
                            },
                            approve: {
                                template: '<a href="#none" class="pure-button usp-fx-all approve-form" title="<s:message code="resolve.view.form.approve.button"/>" aria-label="<s:message code="resolve.view.form.approve.button"/> "><i class="fa fa-thumbs-up"></i><span><s:message code="resolve.view.form.approve.button" /></span></a>',
                                statusComplete: true,
                                isPermitted: '${canApprove}' === 'true'
                            },
                            remove: {
                                template: '<a href="#none" class="pure-button usp-fx-all delete-form" title="<s:message code="resolve.view.form.delete.button"/>" aria-label="<s:message code="resolve.view.form.delete.button"/> "><i class="fa fa-trash"></i><span><s:message code="resolve.view.form.delete.button" /></span></a>',
                                statusDraft: true,
                                statusComplete: true,
                                isPermitted: '${canDelete}' === 'true'
                            }
                        },
                        dialogConfig: {
                            complete: {
                                message: '<s:message javaScriptEscape="true" code="resolve.form.complete.prompt"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.complete.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.complete.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.complete.header"/></h3>'
                            },
                            reopen: {
                                message: '<s:message code="resolve.form.reopen.prompt"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.reopen.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.reopen.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reopen.header"/></h3>'
                            },
                            reject: {
                                message: '<s:message code="resolve.form.reject.prompt"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.reject.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.reject.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reject.header"/></h3>',
                                labels: {
                                    rejectionReason: '<s:message code="updateRejectFormVO.rejectionReason"/>'
                                }
                            },
                            approve: {
                                message: '<s:message code="resolve.form.approve.prompt"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.approve.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.approve.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.approve.header"/></h3>'
                            },
                            remove: {
                                message: '<s:message code="resolve.form.delete.prompt"/>',
                                narrative: '<s:message javaScriptEscape="true" code="resolve.form.remove.summary"/>',
                                successMessage: '<s:message javaScriptEscape="true" code="resolve.form.remove.success"/>',
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.delete.header"/></h3>'
                            },
                            placementReport: {
                                label: '<s:message code="resolve.view.context.prform.type"/>'
                            },
                            carePlanRiskAssessment: {
                                label: '<s:message code="resolve.view.context.cpraform.type"/>'
                            },
                            intakeForm: {
                                label: '<s:message code="resolve.view.context.intakeform.type"/>'
                            },
                            formName: '<s:message code="form.pr"/>',
                            url: '<s:url value="/rest/{formType}/{id}/status?action={actionParam}"/>'
                        }
                    });
    
                //listen for saved event            
                formTabs.on('*:saved', function() {
                    //reload the model
                    this.load();
                }, formModel);
    
                formStatusButtons.on('*:saved', function() {
                    //reload the model
                    this.load();
                }, formModel);
    
                //load the model which will trigger a render
                formModel.load(function() {
                    // Make sure to dispatch the current hash-based URL which was set by
                    // the server to our route handlers.
                    formTabs.render().dispatch();
    
                    //render our buttons
                    formStatusButtons.render();
    
                    //update style of toolbar buttons to indicate enablement
                    Y.all('#sectionToolbar .pure-button-loading').removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
                });
    
                //delegate on toolbar button click - ONLY for print button - other button clicks are handled by statusButtons.js
                Y.delegate('click', function(e) {
                    e.preventDefault();
    
                    var myWin = window.open('<s:url value="/forms/resolve/place/${formId}/?print=true"/>');
                }, '#sectionToolbar', '.pure-button-group a.print');
            });    
    </script>
        
        
		<%-- Insert Person Warning Dialog Template --%>
		<t:insertTemplate template="/WEB-INF/views/tiles/content/person/panel/personWarningDialog.jsp" />        
    </c:when>
    <c:otherwise>
        <%-- Insert access denied tile --%>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>

