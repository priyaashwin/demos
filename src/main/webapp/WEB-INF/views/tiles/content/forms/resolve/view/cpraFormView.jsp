<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.Update')" var="canUpdate"/>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.Reject')" var="canUpdateRejection"/>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.Archive')" var="canDelete"/>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.Authorise')" var="canApprove"/>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.Reject')" var="canReject"/>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.Complete')" var="canComplete"/>
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.Uncomplete')" var="canReopen"/>

<c:choose>
    <%-- if the subject context for the page is person --%>
    <c:when test="${!empty person}">
        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Add')" var="canAddWarning"/>
        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Update')" var="canUpdateWarning"/>
    </c:when>
    <%-- if the subject context for the page is group i.e. ${!empty group}"> --%>
    <c:otherwise>
        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Add')" var="canAddWarning"/>
        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Update')" var="canUpdateWarning"/>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${canView eq true}"> 
        <div id="formTabs" />
        
<script>
Y.use('yui-base',
        'form-content-context',
        'cpraform-tabs',
        'tabbed-page-contents-plugin',
        'usp-resolveassessment-CarePlanRiskAssessment',
        'form-status-buttons',
        'usp-person-PersonWithWarning',
        function(Y) {
            //The CPRA Form model
            Y.namespace('app').CarePlanRiskAssessment = Y.Base.create("CarePlanRiskAssessment", Y.usp.resolveassessment.CarePlanRiskAssessment, [], {}, {
                ATTRS: {
                    canUpdatePermission: 'initOnly',
                    canUpdateRejectionPermission: 'initOnly',
                    canUpdate: {
                        value: false,
                        writeOnce: 'initOnly',
                        getter: function() {
                            return !this.get('canUpdatePermission') ? false : this.get('status') === STATE_DRAFT;
                        }
                    },
                    canUpdateRejection: {
                        value: false,
                        writeOnce: 'initOnly',
                        getter: function() {
                            return !this.get('canUpdateRejectionPermission') ? false : this.get('status') === STATE_DRAFT;
                        }
                    },
                    subjectIsFemale: {
                        writeOnce: 'initOnly',
                        getter: function() {
                            return this.get('subject') !== undefined && this.get('subject').gender === 'FEMALE';
                        }
                    }
                }
            });

            var STATE_DRAFT = 'DRAFT',
                formModel = new Y.app.CarePlanRiskAssessment({
                    id: '${formId}',
                    url: '<s:url value="/rest/carePlanRiskAssessment/{id}"/>',
                    canUpdatePermission: '${canUpdate}'==='true',
                    canUpdateRejectionPermission: '${canUpdateRejection}'==='true'
                }),
                viewLabels = {
                    background: {
                        name: '<s:message code="carePlanRiskAssessmentVO.subject.name"/>',
                        dateOfBirth: '<s:message code="carePlanRiskAssessmentVO.subject.dateOfBirth"/>',
                        gp: '<s:message code="carePlanRiskAssessmentVO.gp"/>',
                        keyWorker: '<s:message code="carePlanRiskAssessmentVO.keyWorker"/>',
                        dateStarted: '<s:message code="carePlanRiskAssessmentVO.dateStarted"/>',
                        placementAddress: '<s:message code="carePlanRiskAssessmentVO.placementAddress"/>',

                        //buttons
                        editButton: '<s:message code="button.edit"/>'
                    },
                    history: {
                        subjectAuthorisationDates: '<s:message code="carePlanRiskAssessmentVO.subjectAuthorisationDates"/>',
                        name: '<s:message code="carePlanRiskAssessmentVO.initiatorName"/>',

                        //buttons
                        editButton: '<s:message code="button.edit"/>'
                    },
                    risks: {
                        specificCommunicationNeeds: '<s:message code="carePlanRiskAssessmentVO.specificCommunicationNeeds"/>',
                        historyOfSpecificRisks: '<s:message code="carePlanRiskAssessmentVO.historyOfSpecificRisks"/>',
                        historicalRiskLevel: '<s:message code="carePlanRiskAssessmentVO.historicalRiskLevel"/>',
                        historicalRiskLevelLow: '<s:message code="historicalRiskLevel.low"/>',
                        historicalRiskLevelMedium: '<s:message code="historicalRiskLevel.medium"/>',
                        historicalRiskLevelHigh: '<s:message code="historicalRiskLevel.high"/>',

                        //buttons
                        editButton: '<s:message code="button.edit"/>'
                    },
                    progress: {
                        planType: '<s:message code="progressReportVO.planType"/>',
                        minimalProgressSummary: '<s:message code="progressReportVO.minimalProgressSummary"/>',
                        fairProgressSummary: '<s:message code="progressReportVO.fairProgressSummary"/>',
                        goodProgressSummary: '<s:message code="progressReportVO.goodProgressSummary"/>',
                        majorProgressSummary: '<s:message code="progressReportVO.majorProgressSummary"/>',
                        reviewDate: '<s:message code="progressReportVO.reviewDate"/>',

                        //buttons
                        edit: '<s:message code="progressReportVO.results.edit"/>'
                    },
                    views: {
                        serviceUserComments: '<s:message code="carePlanRiskAssessmentVO.serviceUserComments"/>',

                        //buttons
                        editButton: '<s:message code="button.edit"/>'
                    },
                    relative: {
                        relativeAdvocateComments: '<s:message code="carePlanRiskAssessmentVO.relativeAdvocateComments"/>',

                        //buttons
                        editButton: '<s:message code="button.edit"/>'
                    },
                    carers: {
                        carersPerspective: '<s:message code="carePlanRiskAssessmentVO.carersPerspective"/>',

                        //buttons
                        editButton: '<s:message code="button.edit"/>'
                    },
                    supportPlan: {
                        actions: '<s:message code="keyAreaVO.results.actions"/>',
                        keyAreaIdentifier: '<s:message code="keyAreaVO.results.keyAreaIdentifier"/>',
                        requiredPlanOfSupport: '<s:message code="keyAreaVO.results.requiredPlanOfSupport"/>',
                        progressOutcome: '<s:message code="keyAreaVO.results.progressOutcome"/>',
                        edit: '<s:message code="keyAreaVO.results.edit"/>'
                    },
                    identifiedRisks: {
                        risk: '<s:message code="identifiedRiskVO.risk"/>',
                        likelihood: '<s:message code="identifiedRiskVO.likelihood"/>',
                        impact: '<s:message code="identifiedRiskVO.impact"/>',
                        total: '<s:message code="identifiedRiskVO.total"/>',
                        riskManagementActivity: '<s:message code="identifiedRiskVO.riskManagementActivity"/>',
                        revisedLikelihood: '<s:message code="identifiedRiskVO.revisedLikelihood"/>',
                        revisedImpact: '<s:message code="identifiedRiskVO.revisedImpact"/>',
                        revisedTotal: '<s:message code="identifiedRiskVO.revisedTotal"/>',
                        date: '<s:message code="identifiedRiskVO.date"/>',
                        actions: '<s:message code="identifiedRiskVO.results.actions"/>',
                        remove: '<s:message code="identifiedRiskVO.results.remove"/>',
                        edit: '<s:message code="identifiedRiskVO.results.edit"/>',
                        part1: {
                            title: '<s:message code="resolve.view.cpraform.risk.part1"/>',
                            narrativeRiskType: '<s:message code="resolve.view.cpraform.risk.part1.type"/>',
                            addButtonLabel: '<s:message code="resolve.view.cpraform.risk.part1.add"/>'
                        },
                        part2: {
                            title: '<s:message code="resolve.view.cpraform.risk.part2"/>',
                            narrativeRiskType: '<s:message code="resolve.view.cpraform.risk.part2.type"/>',
                            addButtonLabel: '<s:message code="resolve.view.cpraform.risk.part2.add"/>'
                        },
                        part3: {
                            title: '<s:message code="resolve.view.cpraform.risk.part3"/>',
                            narrativeRiskType: '<s:message code="resolve.view.cpraform.risk.part3.type"/>',
                            addButtonLabel: '<s:message code="resolve.view.cpraform.risk.part3.add"/>'
                        },
                        part4: {
                            title: '<s:message code="resolve.view.cpraform.risk.part4"/>',
                            narrativeRiskType: '<s:message code="resolve.view.cpraform.risk.part4.type"/>',
                            addButtonLabel: '<s:message code="resolve.view.cpraform.risk.part4.add"/>'
                        }
                    },
                    rejection: {
                        rejectionReason: '<s:message code="rejectFormVO.rejectionReason"/>',
                        rejectionDate: '<s:message code="rejectFormVO.rejectionDate"/>',
                        rejectedBy: '<s:message code="rejectFormVO.rejector.name"/>',
                        noRejections: '<s:message code="resolve.view.form.rejection.noRejections"/>',

                        //buttons
                        editButton: '<s:message code="button.edit"/>'
                    }
                },
                supportPlanConfig = {
                    title: '<s:message code="keyAreas.title"/>',
                    labels: viewLabels.supportPlan,
                    canUpdate: '${canUpdate}'==='true',
                    url: '<s:url value="/rest/carePlanRiskAssessment/{id}/keyArea?planType={planType}"/>'
                },
                identifiedRisksConfig = {
                    title: '<s:message code="identifiedRisk.title"/>',
                    labels: viewLabels.identifiedRisks,
                    canUpdate: '${canUpdate}'==='true',
                    url: '<s:url value="/rest/carePlanRiskAssessment/{id}/identifiedRisk?planType={planType}&riskType={riskType}"/>'
                },
                context = new Y.app.FormContentContext({
                    container: Y.one('#formContext'),
                    model: formModel,
                    personModel: new Y.usp.person.PersonWithWarning({
                        id: '<c:out value="${person.id}"/>',
                        personIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                        name: '${esc:escapeJavaScript(person.name)}',
                        url: '<s:url value="/rest/person/{id}"/>'
                    }).load(),
                    labels: {
                      <c:choose>
                      <c:when test="${canAddWarning} or ${canUpdateWarning}">
                      warningTitle:'<s:message code="button.hovermanagewarnings"/>',
                      </c:when>
                      <c:otherwise>
                      warningTitle:'<s:message code="button.hoverviewwarnings"/>',
                      </c:otherwise>
                    </c:choose> 
                        formView: '<s:message code="resolve.view.context.title"/>',
                        formType: '<s:message code="resolve.view.context.cpraform.type"/>',
                        startDate: '<s:message code="resolve.view.context.startDate" />'
                    },
                    canAddWarning: '${canAddWarning}' === 'true',
                    canUpdateWarning: '${canUpdateWarning}' === 'true'
                }),
                formTabs = new Y.app.CPRAFormTabsView({
                    plugins: [{
                        fn: Y.Plugin.usp.TabbedPageContentsPlugin
                    }],
                    root: '<s:url value="/forms/resolve/cpra/${formId}/"/>',
                    container: '#formTabs',
                    tabs: {
                        background: {
                            label: '<s:message code="resolve.view.cpraform.tabs.background"/>',
                            contents: [{
                                label: '<s:message code="resolve.view.cpraform.introduction.title"/>',
                                id: 'introductionView'
                            }, {
                                label: '<s:message code="resolve.view.cpraform.background.title"/>',
                                id: 'backgroundView'
                            }, {
                                label: '<s:message code="resolve.view.cpraform.history.title"/>',
                                id: 'historyView'
                            }, {
                                label: '<s:message code="resolve.view.cpraform.risks.title"/>',
                                id: 'risksView'
                            }],
                            config: {
                                model: formModel,
                                introductionConfig: {
                                    title: '<s:message code="resolve.view.cpraform.introduction.title"/>',
                                    labels: {
                                        //buttons
                                        editButton: '<s:message code="button.edit"/>'
                                    }
                                },
                                backgroundConfig: {
                                    title: '<s:message code="resolve.view.cpraform.background.title"/>',
                                    labels: viewLabels.background
                                },
                                historyConfig: {
                                    title: '<s:message code="resolve.view.cpraform.history.title"/>',
                                    labels: viewLabels.history
                                },
                                risksConfig: {
                                    title: '<s:message code="resolve.view.cpraform.risks.title"/>',
                                    labels: viewLabels.risks
                                }
                            }
                        },
                        health: {
                            label: '<s:message code="resolve.view.cpraform.tabs.health"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.HEALTH_AND_MEDICATION.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.HEALTH_AND_MEDICATION.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        emotional: {
                            label: '<s:message code="resolve.view.cpraform.tabs.emotional"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.EMOTIONAL_AND_BEHAVIOURAL.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.EMOTIONAL_AND_BEHAVIOURAL.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        identity: {
                            label: '<s:message code="resolve.view.cpraform.tabs.identity"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.IDENTITY_FAMILY_AND_SOCIAL_RELATIONSHIPS.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.IDENTITY_FAMILY_AND_SOCIAL_RELATIONSHIPS.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        selfcare: {
                            label: '<s:message code="resolve.view.cpraform.tabs.selfcare"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.SOCIAL_PRESENTATION_AND_SELF_CARE_SKILLS.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.SOCIAL_PRESENTATION_AND_SELF_CARE_SKILLS.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        ils: {
                            label: '<s:message code="resolve.view.cpraform.tabs.ils"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.INDEPENDENT_LIVING_SKILLS.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.INDEPENDENT_LIVING_SKILLS.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        daycare: {
                            label: '<s:message code="resolve.view.cpraform.tabs.daycare"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.DAY_CARE_PLAN.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.DAY_CARE_PLAN.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        finance: {
                            label: '<s:message code="resolve.view.cpraform.tabs.finance"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.FINANCE.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.FINANCE.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        consultation: {
                            label: '<s:message code="resolve.view.cpraform.tabs.consultation"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.MANAGER_STAFF_CONSULTATION.title"/>',
                                id: 'planAccordion'
                            }],
                            config: Y.merge({
                                model: formModel
                            }, supportPlanConfig)
                        },
                        specialists: {
                            label: '<s:message code="resolve.view.cpraform.tabs.specialists"/>',
                            contents: [{
                                label: '<s:message code="keyAreas.contents.SPECIALIST_SERVICES.title"/>',
                                id: 'planAccordion'
                            }, {
                                label: '<s:message code="identifiedRisk.contents.SPECIALIST_SERVICES.title"/>',
                                id: 'riskSetAccordion'
                            }],
                            config: {
                                model: formModel,
                                supportPlan: supportPlanConfig,
                                identifiedRisks: identifiedRisksConfig
                            }
                        },
                        progress: {
                            label: '<s:message code="resolve.view.cpraform.tabs.progress"/>',
                            contents: [{
                                label: '<s:message code="resolve.view.cpraform.progress.title"/>',
                                id: 'progressReportAccordion'
                            }],
                            config: {
                                model: formModel,
                                progressConfig: {
                                    title: '<s:message code="resolve.view.cpraform.progress.title"/>',
                                    progressReportConfig: {
                                        labels: viewLabels.progress,
                                        url: '<s:url value="/rest/carePlanRiskAssessment/{id}/progressReport"/>'
                                    }
                                }
                            }
                        },
                        views: {
                            label: '<s:message code="resolve.view.cpraform.tabs.views"/>',
                            contents: [{
                                label: '<s:message code="resolve.view.cpraform.views.title"/>',
                                id: 'serviceUserView'
                            }, {
                                label: '<s:message code="resolve.view.cpraform.relativeViews.title"/>',
                                id: 'relativeView'
                            }, {
                                label: '<s:message code="resolve.view.cpraform.carersPerspective.title"/>',
                                id: 'carersView'
                            }],
                            config: {
                                model: formModel,
                                viewsConfig: {
                                    title: '<s:message code="resolve.view.cpraform.views.title"/>',
                                    labels: viewLabels.views
                                },
                                relativeConfig: {
                                    title: '<s:message code="resolve.view.cpraform.relativeViews.title"/>',
                                    labels: viewLabels.relative
                                },
                                carersConfig: {
                                    title: '<s:message code="resolve.view.cpraform.carersPerspective.title"/>',
                                    labels: viewLabels.carers
                                }
                            }
                        },
                        rejection: {
                            label: '<s:message code="resolve.view.cpraform.tabs.rejection"/>',
                            contents: [{
                                label: '<s:message code="resolve.view.prform.rejection.title"/>',
                                id: 'rejectionView'
                            }],
                            config: {
                                model: formModel,
                                rejectionConfig: {
                                    title: '<s:message code="resolve.view.prform.rejection.title"/>',
                                    labels: viewLabels.rejection
                                }
                            }
                        }
                    },
                    dialogConfig: {
                        defaultHeader: 'loading...',
                        commsAndRisksConfig: {
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.commsAndRisks.header"/></h3>',
                                labels: {
                                    specificCommunicationNeeds: '<s:message code="carePlanRiskAssessmentVO.specificCommunicationNeeds"/>',
                                    historyOfSpecificRisks: '<s:message code="carePlanRiskAssessmentVO.historyOfSpecificRisks"/>',
                                    historicalRiskLevel: '<s:message code="carePlanRiskAssessmentVO.historicalRiskLevel"/>',
                                    historicalRiskLevelLow: '<s:message code="historicalRiskLevel.low"/>',
                                    historicalRiskLevelMedium: '<s:message code="historicalRiskLevel.medium"/>',
                                    historicalRiskLevelHigh: '<s:message code="historicalRiskLevel.high"/>'
                                },
                                narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                url: '<s:url value="/rest/carePlanRiskAssessment/{id}"/>'
                            }
                        },
                        viewsConfig: {
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.serviceUser.header"/></h3>',
                                labels: {
                                    serviceUserComments: '<s:message code="carePlanRiskAssessmentVO.serviceUserComments"/>',
                                },
                                narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                url: '<s:url value="/rest/carePlanRiskAssessment/{id}"/>'
                            }
                        },
                        relativeConfig: {
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.relative.header"/></h3>',
                                labels: {
                                    relativeAdvocateComments: '<s:message code="carePlanRiskAssessmentVO.relativeAdvocateComments"/>',
                                },
                                narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                url: '<s:url value="/rest/carePlanRiskAssessment/{id}"/>'
                            }
                        },
                        carersPerspectiveConfig: {
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.carersPerspective.header"/></h3>',
                                labels: {
                                    carersPerspective: '<s:message code="carePlanRiskAssessmentVO.carersPerspective"/>',
                                },
                                narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.summary"/>',
                                url: '<s:url value="/rest/carePlanRiskAssessment/{id}"/>'
                            }
                        },

                        identifierRiskConfig: {
                            add: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.add.identifiedRisk.header"/></h3>',
                                labels: {
                                    risk: '<s:message code="newIdentifiedRiskVO.risk"/>',
                                    likelihood: '<s:message code="newIdentifiedRiskVO.likelihood"/>',
                                    impact: '<s:message code="newIdentifiedRiskVO.impact"/>',
                                    total: '<s:message code="newIdentifiedRiskVO.total"/>',
                                    riskManagementActivity: '<s:message code="newIdentifiedRiskVO.riskManagementActivity"/>',
                                    revisedLikelihood: '<s:message code="newIdentifiedRiskVO.revisedLikelihood"/>',
                                    revisedImpact: '<s:message code="newIdentifiedRiskVO.revisedImpact"/>',
                                    revisedTotal: '<s:message code="newIdentifiedRiskVO.revisedTotal"/>',
                                    date: '<s:message code="newIdentifiedRiskVO.date"/>'
                                },
                                narrative: '<s:message javaScriptEscape="true" code="identifiedRisk.dialog.add.summary"/>',
                                successMessage: '<s:message code="resolve.form.risk.add.success"/>',
                                url: '<s:url value="/rest/carePlanRiskAssessment/{id}/identifiedRisk"/>'
                            },
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.identifiedRisk.header"/></h3>',
                                labels: {
                                    risk: '<s:message code="updateIdentifiedRiskVO.risk"/>',
                                    likelihood: '<s:message code="updateIdentifiedRiskVO.likelihood"/>',
                                    impact: '<s:message code="updateIdentifiedRiskVO.impact"/>',
                                    total: '<s:message code="updateIdentifiedRiskVO.total"/>',
                                    riskManagementActivity: '<s:message code="updateIdentifiedRiskVO.riskManagementActivity"/>',
                                    revisedLikelihood: '<s:message code="updateIdentifiedRiskVO.revisedLikelihood"/>',
                                    revisedImpact: '<s:message code="updateIdentifiedRiskVO.revisedImpact"/>',
                                    revisedTotal: '<s:message code="updateIdentifiedRiskVO.revisedTotal"/>',
                                    date: '<s:message code="updateIdentifiedRiskVO.date"/>'
                                },
                                narrative: '<s:message javaScriptEscape="true" code="identifiedRisk.dialog.edit.summary"/>',
                                successMessage: '<s:message code="resolve.form.risk.edit.success"/>',
                                url: '<s:url value="/rest/identifiedRisk/{id}"/>'
                            },
                            remove: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.remove.identifiedRisk.header"/></h3>',
                                labels: {},
                                narrative: '<s:message javaScriptEscape="true" code="identifiedRisk.dialog.remove.summary"/>',
                                message: '<s:message code="identifiedRiskVO.results.confirm"/>',
                                successMessage: '<s:message code="resolve.form.risk.remove.success"/>',
                                url: '<s:url value="/rest/identifiedRisk/{id}"/>'
                            }
                        },
                        supportPlanConfig: {
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.keyArea.header"/></h3>',
                                labels: {
                                    keyAreaIdentifier: '<s:message code="updateKeyAreaVO.keyAreaIdentifier"/>',
                                    requiredPlanOfSupport: '<s:message code="updateKeyAreaVO.requiredPlanOfSupport"/>',
                                    progressOutcome: '<s:message code="updateKeyAreaVO.progressOutcome"/>'
                                },
                                narrative: '<s:message javaScriptEscape="true" code="keyArea.dialog.edit.summary"/>',
                                successMessage: '<s:message code="resolve.form.keyArea.edit.success"/>',
                                url: '<s:url value="/rest/keyArea/{id}"/>'
                            }
                        },
                        rejectionConfig: {
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.rejection.header"/></h3>',
                                labels: {
                                    rejectionReason: '<s:message code="updateRejectFormVO.rejectionReason"/>',
                                    rejectionDate: '<s:message code="rejectFormVO.rejectionDate"/>',
                                    rejectedBy: '<s:message code="rejectFormVO.rejector.name"/>'
                                },
                                narrative: '<s:message javaScriptEscape="true" code="resolve.dialog.edit.rejection.summary"/>',
                                url: '<s:url value="/rest/form/{id}/rejection/mostRecent"/>',
                                saveUrl: '<s:url value="/rest/formRejection/{id}"/>'
                            }
                        },
                        progressReportConfig: {
                            edit: {
                                headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.dialog.edit.progressreport.header"/></h3>',
                                labels: {
                                    planType: '<s:message code="updateProgressReportVO.planType"/>',
                                    reviewDate: '<s:message code="updateProgressReportVO.reviewDate"/>',
                                    minimalProgressSummary: '<s:message code="updateProgressReportVO.minimalProgressSummary"/>',
                                    fairProgressSummary: '<s:message code="updateProgressReportVO.fairProgressSummary"/>',
                                    goodProgressSummary: '<s:message code="updateProgressReportVO.goodProgressSummary"/>',
                                    majorProgressSummary: '<s:message code="updateProgressReportVO.majorProgressSummary"/>'
                                },
                                narrative: '<s:message javaScriptEscape="true" code="progressReport.dialog.edit.summary"/>',
                                successMessage: '<s:message code="resolve.form.progressReport.edit.success"/>',
                                url: '<s:url value="/rest/progressReport/{id}"/>'
                            }
                        },
                        formName: '<s:message code="form.cpra"/>',
                        successMessage: '<s:message code="resolve.form.edit.success"/>'
                    }
                }),
                formStatusButtons = new Y.app.FormStatusButtons({
                    container: '#sectionToolbar .pure-button-group',
                    model: formModel,
                    buttonsConfig: {
                        complete: {
                            template: '<a href="#none" class="pure-button first usp-fx-all complete-form" title="<s:message code="resolve.view.form.complete.button"/> " aria-label="<s:message code="resolve.view.form.complete.button"/> "><i class="fa fa-check-square"></i><span><s:message code="resolve.view.form.complete.button" /></span></a>',
                            statusDraft: true,
                            isPermitted: '${canComplete}' === 'true'
                        },
                        reopen: {
                            template: '<a href="#none" class="pure-button usp-fx-all reopen-form" title="<s:message code="resolve.view.form.reopen.button"/>" aria-label="<s:message code="resolve.view.form.reopen.button"/> "><i class="fa fa-file"></i><span><s:message code="resolve.view.form.reopen.button" /></span></a>',
                            statusComplete: true,
                            isPermitted: '${canReopen}' === 'true'
                        },
                        reject: {
                            template: '<a href="#none" class="pure-button usp-fx-all reject-form" title="<s:message code="resolve.view.form.reject.button"/>" aria-label="<s:message code="resolve.view.form.reject.button"/> "><i class="fa fa-thumbs-down"></i><span><s:message code="resolve.view.form.reject.button" /></span></a>',
                            statusComplete: true,
                            isPermitted: '${canReject}' === 'true'
                        },
                        approve: {
                            template: '<a href="#none" class="pure-button usp-fx-all approve-form" title="<s:message code="resolve.view.form.approve.button"/>" aria-label="<s:message code="resolve.view.form.approve.button"/> "><i class="fa fa-thumbs-up"></i><span><s:message code="resolve.view.form.approve.button" /></span></a>',
                            statusComplete: true,
                            isPermitted: '${canApprove}' === 'true'
                        },
                        remove: {
                            template: '<a href="#none" class="pure-button usp-fx-all delete-form" title="<s:message code="resolve.view.form.delete.button"/>" aria-label="<s:message code="resolve.view.form.delete.button"/> "><i class="fa fa-trash"></i><span><s:message code="resolve.view.form.delete.button" /></span></a>',
                            statusDraft: true,
                            statusComplete: true,
                            isPermitted: '${canDelete}' === 'true'
                        }
                    },
                    dialogConfig: {
                        complete: {
                            message: '<s:message javaScriptEscape="true" code="resolve.form.complete.prompt"/>',
                            narrative: '<s:message javaScriptEscape="true" code="resolve.form.complete.summary"/>',
                            successMessage: '<s:message javaScriptEscape="true" code="resolve.form.complete.success"/>',
                            headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.complete.header"/></h3>'
                        },
                        reopen: {
                            message: '<s:message code="resolve.form.reopen.prompt"/>',
                            narrative: '<s:message javaScriptEscape="true" code="resolve.form.reopen.summary"/>',
                            successMessage: '<s:message javaScriptEscape="true" code="resolve.form.reopen.success"/>',
                            headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reopen.header"/></h3>'
                        },
                        reject: {
                            message: '<s:message code="resolve.form.reject.prompt"/>',
                            narrative: '<s:message javaScriptEscape="true" code="resolve.form.reject.summary"/>',
                            successMessage: '<s:message javaScriptEscape="true" code="resolve.form.reject.success"/>',
                            headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reject.header"/></h3>',
                            labels: {
                                rejectionReason: '<s:message code="updateRejectFormVO.rejectionReason"/>'
                            }
                        },
                        approve: {
                            message: '<s:message code="resolve.form.approve.prompt"/>',
                            narrative: '<s:message javaScriptEscape="true" code="resolve.form.approve.summary"/>',
                            successMessage: '<s:message javaScriptEscape="true" code="resolve.form.approve.success"/>',
                            headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.approve.header"/></h3>'
                        },
                        remove: {
                            message: '<s:message code="resolve.form.delete.prompt"/>',
                            narrative: '<s:message javaScriptEscape="true" code="resolve.form.remove.summary"/>',
                            successMessage: '<s:message javaScriptEscape="true" code="resolve.form.remove.success"/>',
                            headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.delete.header"/></h3>'
                        },
                        placementReport: {
                            label: '<s:message code="resolve.view.context.prform.type"/>'
                        },
                        carePlanRiskAssessment: {
                            label: '<s:message code="resolve.view.context.cpraform.type"/>'
                        },
                        intakeForm: {
                            label: '<s:message code="resolve.view.context.intakeform.type"/>'
                        },
                        formName: '<s:message code="form.cpra"/>',
                        url: '<s:url value="/rest/{formType}/{id}/status?action={actionParam}"/>'
                    }
                });

            //listen for saved event            
            formTabs.on('*:saved', function() {
                //reload the model
                this.load();
            }, formModel);

            formStatusButtons.on('*:saved', function() {
                //reload the model
                this.load();
            }, formModel);

            //load the model which will trigger a render
            formModel.load(function() {
                // Make sure to dispatch the current hash-based URL which was set by
                // the server to our route handlers.
                formTabs.render().dispatch();

                //render our buttons
                formStatusButtons.render();

                //update style of toolbar buttons to indicate enablement
                Y.all('#sectionToolbar .pure-button-loading').removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
            });

            //delegate on toolbar button click - ONLY for print button - other button clicks are handled by statusButtons.js
            Y.delegate('click', function(e) {
                e.preventDefault();

                var myWin = window.open('<s:url value="/forms/resolve/cpra/${formId}/?print=true"/>');
            }, '#sectionToolbar', '.pure-button-group a.print');

            context.render();
          });
</script>
		<%-- Insert Person Warning Dialog Template --%>
		<t:insertTemplate template="/WEB-INF/views/tiles/content/person/panel/personWarningDialog.jsp" />        
    </c:when>
    <c:otherwise>
        <%-- Insert access denied tile --%>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>

