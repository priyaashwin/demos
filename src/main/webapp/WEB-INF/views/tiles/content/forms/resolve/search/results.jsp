<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
 
 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'Form','Form.Add')" var="canAdd"/>
<sec:authorize access="hasPermission(null, 'Form','Form.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'Form','Form.Archive')" var="canDelete"/>
<sec:authorize access="hasPermission(null, 'Form','Form.Authorise')" var="canApprove"/>
<sec:authorize access="hasPermission(null, 'Form','Form.Reject')" var="canReject"/>
<sec:authorize access="hasPermission(null, 'Form','Form.Complete')" var="canComplete"/>
<sec:authorize access="hasPermission(null, 'Form','Form.Uncomplete')" var="canReopen"/>
<sec:authorize access="hasPermission(null, 'Form','Forms.ViewArchived')" var="canViewDeleteDeleted"/>
<%-- <sec:authorize access="hasRole('Resolve_Manager_Role')" var="canViewArchived"/> --%>
<sec:authorize access="hasPermission(null, 'Form','Forms.ViewArchived')" var="canViewArchived"/>

<div id="formResults"/>

<script>
Y.use('form-search-view',
      'form-add-buttons',
      'usp-relationshipsrecording-CanAddForms',
	  'event-custom-base',
	  function(Y){
        
    var L=Y.Lang,
    canAdd=${canAdd},
    canView=${canView},
    canDelete=${canDelete},
    canApprove=${canApprove},
    canReject=${canReject},
    canComplete=${canComplete},
    canReopen=${canReopen},
    canViewArchived=${canViewArchived},
    
    formAddButtons=new Y.app.FormAddButtons({
        container:'#sectionToolbar .pure-button-group',
        model: new Y.usp.relationshipsrecording.CanAddForms({
            url: '<s:url value="/rest/forms/resolve/canAdd?personId=${person.id}" />',
        }),
        buttonsConfig:{
            addPR:{
                template:'<a href="#none" class="pure-button usp-fx-all view-pr-form pure-button-disabled" title="<s:message code="form.pr.button"/> " aria-label="<s:message code="form.pr.button"/>"><i class="fa fa-plus-circle"></i><span><s:message code="form.pr.button" /></span></a>',
                canAdd: ${canAdd}
            },
            addCPRA:{                      
                template:'<a href="#none" class="pure-button usp-fx-all view-cpra-form pure-button-disabled" title="<s:message code="form.cpra.button"/> " aria-label="<s:message code="form.cpra.button"/>"><i class="fa fa-plus-circle"></i><span><s:message code="form.cpra.button" /></span></a>',
                canAdd: ${canAdd}
            },
            addIntake:{
                template:'<a href="#none" class="pure-button usp-fx-all view-intake-form pure-button-disabled" title="<s:message code="form.intake.button"/> " aria-label="<s:message code="form.intake.button"/>"><i class="fa fa-plus-circle"></i><span><s:message code="form.intake.button" /></span></a>',
                canAdd: ${canAdd}                
            }
        }
    }).render(),

    searchView=new Y.app.FormSearchView({
		container:'#formResults',
		searchConfig:{
			labels:{				
				startDate:'<s:message code="resolve.find.results.startDate"/>',
                completedDate:'<s:message code="resolve.find.results.completedDate"/>',
                authorisedDate:'<s:message code="resolve.find.results.authorisedDate"/>',
				editDate:'<s:message code="resolve.find.results.editDate"/>',
                type:'<s:message code="resolve.find.results.type"/>',
                user:'<s:message code="resolve.find.results.user"/>', 
                state:'<s:message code="resolve.find.results.state"/>',
                actions:'<s:message code="resolve.find.results.actions"/>',
                viewForm:'<s:message code="resolve.find.results.viewForm"/>',
                completeForm:'<s:message code="resolve.find.results.completeForm"/>',
                rejectForm:'<s:message code="resolve.find.results.rejectForm"/>',
                approveForm:'<s:message code="resolve.find.results.approveForm"/>',
                reopenForm:'<s:message code="resolve.find.results.reopenForm"/>',
                deleteForm:'<s:message code="resolve.find.results.deleteForm"/>',
                viewFormTitle:'<s:message code="resolve.find.results.viewFormTitle"/>',
                completeFormTitle:'<s:message code="resolve.find.results.completeFormTitle"/>',
                rejectFormTitle:'<s:message code="resolve.find.results.rejectFormTitle"/>',
                approveFormTitle:'<s:message code="resolve.find.results.approveFormTitle"/>',
                reopenFormTitle:'<s:message code="resolve.find.results.reopenFormTitle"/>',
                deleteFormTitle:'<s:message code="resolve.find.results.deleteFormTitle"/>'
			},
			noDataMessage:'<s:message code="resolve.find.no.results"/>',
			permissions:{
				canView:canView,
				canDelete:canDelete,
				canApprove:canApprove,
				canReject:canReject,
				canComplete:canComplete,
				canReopen:canReopen,
				canViewArchived:canViewArchived
		    },			
			url:'<s:url value="/rest/form?personId=${person.id}&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
		    prFormURL:'<s:url value="/forms/resolve/place/{id}/background" />',
		    cpraFormURL:'<s:url value="/forms/resolve/cpra/{id}/background" />',
		    intakeFormURL:'<s:url value="/forms/resolve/intake/{id}/client" />',
		},
		filterContextConfig:{
			labels:{
                filter:'<s:message code="filter.active"/>',                       
                status:'<s:message code="resolve.find.filter.status.active"/>'
            },
            container:'#formFilterContext'
		},
		filterConfig:{
			labels:{
                filterBy: '<s:message code="resolve.find.filter.title"/>',
                status: '<s:message code="resolve.find.filter.status"/>'
            },
			canViewArchived:canViewArchived
		},
		statusDialogConfig:{
		    prFormType:'<s:message code="resolve.view.context.prform.type"/>',
		    cpraFormType:'<s:message code="resolve.view.context.cpraform.type"/>',
		    intakeFormType:'<s:message code="resolve.view.context.intakeform.type"/>',
		    prFormURL:'<s:url value="/rest/placementReport/{id}/status?action={action}"/>',
		    cpraFormURL:'<s:url value="/rest/carePlanRiskAssessment/{id}/status?action={action}"/>',
		    intakeFormURL:'<s:url value="/rest/intakeForm/{id}/status?action={action}"/>',
			completeConfig:{
			    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.complete.header"/></h3>',
			    message:'<s:message code="resolve.form.complete.prompt"/>',
			    narrative:'<s:message javaScriptEscape="true" code="resolve.form.complete.summary"/>',
			    successMessage:'<s:message javaScriptEscape="true" code="resolve.form.complete.success"/>'
			},
			reopenConfig:{
			    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reopen.header"/></h3>',
			    message:'<s:message code="resolve.form.reopen.prompt"/>',
			    narrative:'<s:message javaScriptEscape="true" code="resolve.form.reopen.summary"/>',
			    successMessage:'<s:message javaScriptEscape="true" code="resolve.form.reopen.success"/>'
			},
			rejectConfig:{
			    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.reject.header"/></h3>',
			    message:'<s:message code="resolve.form.reject.prompt"/>',
			    narrative:'<s:message javaScriptEscape="true" code="resolve.form.reject.summary"/>',
			    successMessage:'<s:message javaScriptEscape="true" code="resolve.form.reject.success"/>',
    			labels:{
    				rejectionReason:'<s:message code="updateRejectFormVO.rejectionReason"/>',
				}
			},
			approveConfig:{
			    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.approve.header"/></h3>',
			    message:'<s:message code="resolve.form.approve.prompt"/>',
			    narrative:'<s:message javaScriptEscape="true" code="resolve.form.approve.summary"/>',
			    successMessage:'<s:message javaScriptEscape="true" code="resolve.form.approve.success"/>'
			},
			deleteConfig:{
			    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-file-text fa-inverse fa-stack-1x"></i></span> <s:message code="resolve.form.delete.header"/></h3>',
			    message:'<s:message code="resolve.form.delete.prompt"/>',
			    narrative:'<s:message javaScriptEscape="true" code="resolve.form.remove.summary"/>',
			    successMessage:'<s:message javaScriptEscape="true" code="resolve.form.remove.success"/>'
			}
		},
		prFormName:'<s:message code="form.pr"/>',
		cpraFormName:'<s:message code="form.cpra"/>',
        intakeFormName:'<s:message code="form.intake"/>',
		subject:{
		    name: '${esc:escapeJavaScript(person.name)}',
		    personIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}'
		}
	}).render();

    formAddButtons.get('model').load();
    
	//update the filter button style based on visibility of filter
    searchView.after('formResultsFilter:visibleChange',function(e){
        if(e.newVal===true){
            this.addClass('pure-button-active');
        }else{
            this.removeClass('pure-button-active');
        }
    },Y.one('#sectionToolbar a.filter'));
    
	//On change in model.....
    searchView.on('*:saved',function(){
    	formAddButtons.get('model').load();    	
    });            

    //attach delegate to toolbar
    Y.delegate('click', function(e){
         var t=e.currentTarget,
         results=searchView.results,
         resultsFilter=searchView.resultsFilter;
         
         //stop default action
         e.preventDefault();
         
       if(t.hasClass('filter')){             
             //filter button has been clicked
             if(resultsFilter.get('visible')===false){
                 resultsFilter.showFilter();                 
             }else{
                 resultsFilter.hideFilter();
             }
         }
       
       if(!t.hasClass('pure-button-disabled')) {
           if(t.hasClass('view-pr-form')){
               //ensure all buttons disabled
    		   formAddButtons.disableButtons();
               //go to the pr form view URL
    		   window.location='<s:url value="/forms/resolve/place?personId=${person.id}" />';
           }else if(t.hasClass('view-cpra-form')){
               //ensure all buttons disabled
               formAddButtons.disableButtons();
               //go to the cpra form view URL
          	 window.location='<s:url value="/forms/resolve/cpra?personId=${person.id}" />';
           }else if(t.hasClass('view-intake-form')){
               //ensure all buttons disabled
               formAddButtons.disableButtons();
               //go to the intake form view URL
               window.location='<s:url value="/forms/resolve/intake?personId=${person.id}" />';
           }
       }
       
    },'#sectionToolbar','.pure-button,li.pure-menu-item a', searchView);   
	
    
    //update style of toolbar buttons to indicate enablement
    Y.all('#sectionToolbar .pure-button-loading').removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
});

</script>
