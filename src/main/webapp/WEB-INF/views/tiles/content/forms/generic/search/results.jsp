<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
 
<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'FormInstance','Forms.ViewArchived')" var="canViewArchived"/>
<sec:authorize access="hasPermission(null, 'FormInstance','Forms.ViewAutomatedAdd')" var="canViewAutomatedAdd"/>
<sec:authorize access="hasPermission(null, 'FormInstanceAttachment','FormInstanceAttachment.View')" var="canViewAttachment" />

 <%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Add')" var="canAdd"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Submit')" var="canSubmit"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Withdraw')" var="canWithdraw"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Complete')" var="canComplete"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Uncomplete')" var="canUncomplete"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Reject')" var="canReject"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Authorise')" var="canAuthorise"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Archive')" var="canDelete"/>
	<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.Remove')" var="canRemove"/>
	<sec:authorize access="hasPermission(null, 'FormInstanceAttachment','FormInstanceAttachment.Add')" var="canUploadAttachment" />
	<sec:authorize access="hasPermission(null, 'FormInstanceAttachment','FormInstanceAttachment.Delete')" var="canDeleteAttachment" />
	<sec:authorize access="hasPermission(null, 'FormInstanceAttachment','FormInstanceAttachment.AddCompleted')" var="canUploadAttachmentOnCompleteInstance" />
</c:if>

<%-- Ability to output is based on the Form Instance View permission --%>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.View')" var="canOutput"/>

<div id="formResults"></div>

<s:url var="formInstanceTypesURL" value="/rest/formDefinition?subjectId={subjectId}&subjectType={subjectType}&s={sortBy}&pageSize=-1">
    <c:forEach items="${initialGroupIds}" var="groupId">
      <s:param name="groupId" value="${groupId}" />
    </c:forEach>
</s:url>

<s:url var="formInstanceResultsURL" value="/rest/formInstance?subjectId={subjectId}&subjectType={subjectType}&s={sortBy}&pageNumber={page}&pageSize={pageSize}">
    <c:forEach items="${initialGroupIds}" var="groupId">
      <s:param name="groupId" value="${groupId}" />
    </c:forEach>
</s:url>

<s:url var="existingFormInstanceTypesURL" value="/rest/formInstance?status=DRAFT&status=SUBMITTED&subjectId={subjectId}&subjectType={subjectType}&formDefinitionId={formTypeId}&s={sortBy}&pageSize=-1">
    <c:forEach items="${initialGroupIds}" var="groupId">
      <s:param name="groupId" value="${groupId}" />
    </c:forEach>
</s:url>

<script>
Y.use('form-controller', 'yui-base', function(Y){
<c:choose>
  <c:when test="${not empty group}">
  var subject = {
    subjectType: 'group',
    subjectName: '${esc:escapeJavaScript(group.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(group.groupIdentifier)}',
    subjectId: '<c:out value="${group.id}"/>'
  };
  </c:when>
  <c:otherwise>
  var subject = {
    subjectType: 'person',
    subjectName: '${esc:escapeJavaScript(person.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
    subjectId: '<c:out value="${person.id}"/>',
  };
  </c:otherwise>
</c:choose>

var permissions = {
  canAdd:'${canAdd}'==='true',
  canViewAutomatedAdd: '${canViewAutomatedAdd}' === 'true',
  canView:'${canView}'==='true',
  canSoftDelete:'${canDelete}'==='true',
  canHardDelete:'${canRemove}'==='true',
  canOutput:'${canOutput}'==='true',
  canAuthorise:'${canAuthorise}'==='true',
  canReject:'${canReject}'==='true',
  canSubmit:'${canSubmit}'==='true',
  canWithdraw:'${canWithdraw}'==='true',
  canComplete:'${canComplete}'==='true',
  canUncomplete:'${canUncomplete}'==='true',
  canViewArchived:'${canViewArchived}'==='true',
  canViewAttachment: '${canViewAttachment}' === 'true',
  canUploadAttachment: '${canUploadAttachment}' === 'true',
  canDeleteAttachment: '${canDeleteAttachment}' === 'true',
  canUploadAttachmentOnCompleteInstance: '${canUploadAttachmentOnCompleteInstance}' === 'true',
  canReadingView: '${canView}'==='true'
};

var attachmentPermissions = {
  canView: '${canViewAttachment}' === 'true',
  canRemove: '${canDeleteAttachment}' === 'true'
}

  var resultsSearchConfig = {
    sortBy: [ //{status: 'desc'}, // MarkD says he wants only to sort by Start date (desc)
      {
          activityDate: 'desc'
      }, {
          dateStarted: 'desc'
      }
    ],
    labels: {
      type:'<s:message code="forms.find.results.type" javaScriptEscape="true"/>',
      formId:'<s:message code="forms.find.results.formId" javaScriptEscape="true"/>',
      startDate:'<s:message code="forms.find.results.startDate" javaScriptEscape="true"/>',
      completedDate:'<s:message code="forms.find.results.completedDate" javaScriptEscape="true"/>',
      completedBy:'<s:message code="forms.find.results.completedBy" javaScriptEscape="true"/>',
      submittedBy:'<s:message code="forms.find.results.submittedBy" javaScriptEscape="true"/>',
      submittedDate:'<s:message code="forms.find.results.submittedDate" javaScriptEscape="true"/>',
      activityDate: '<s:message code="forms.find.results.activityDate" javaScriptEscape="true"/>',
      lastChangedBy:'<s:message code="forms.find.results.lastChangedBy" javaScriptEscape="true"/>',
      state:'<s:message code="forms.find.results.state" javaScriptEscape="true"/>',
      subjectType:'<s:message code="forms.find.results.subjectType" javaScriptEscape="true"/>',
      attachmentCount: '<s:message code="forms.find.results.files" javaScriptEscape="true"/>',
      actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
      viewForm:'<s:message code="forms.find.results.viewForm" javaScriptEscape="true"/>',
      viewFormTitle:'<s:message code="forms.find.results.viewFormTitle" javaScriptEscape="true"/>',
      submitForm:'<s:message code="forms.find.results.submitForm" javaScriptEscape="true"/>',
      submitFormTitle:'<s:message code="forms.find.results.submitFormTitle" javaScriptEscape="true"/>',
      withdrawForm:'<s:message code="forms.find.results.withdrawForm" javaScriptEscape="true"/>',
      withdrawFormTitle:'<s:message code="forms.find.results.withdrawFormTitle" javaScriptEscape="true"/>',
      completeForm:'<s:message code="forms.find.results.completeForm" javaScriptEscape="true"/>',
      completeFormTitle:'<s:message code="forms.find.results.completeFormTitle" javaScriptEscape="true"/>',
      uncompleteForm:'<s:message code="forms.find.results.uncompleteForm" javaScriptEscape="true"/>',
      uncompleteFormTitle:'<s:message code="forms.find.results.uncompleteFormTitle" javaScriptEscape="true"/>',
      rejectForm:'<s:message code="forms.find.results.rejectForm" javaScriptEscape="true"/>',
      rejectFormTitle:'<s:message code="forms.find.results.rejectFormTitle" javaScriptEscape="true"/>',
      approveForm:'<s:message code="forms.find.results.approveForm" javaScriptEscape="true"/>',
      approveFormTitle:'<s:message code="forms.find.results.approveFormTitle" javaScriptEscape="true"/>',
      deleteForm:'<s:message code="forms.find.results.deleteForm" javaScriptEscape="true"/>',
      deleteFormTitle:'<s:message code="forms.find.results.deleteFormTitle" javaScriptEscape="true"/>',
      removeForm:'<s:message code="forms.find.results.removeForm" javaScriptEscape="true"/>',
      removeFormTitle:'<s:message code="forms.find.results.removeFormTitle" javaScriptEscape="true"/>',
      outputForm:'<s:message code="forms.find.results.outputForm" javaScriptEscape="true"/>',
      outputFormTitle:'<s:message code="forms.find.results.outputFormTitle" javaScriptEscape="true"/>',
      reviewHistory:'<s:message code="forms.find.results.reviewHistory" javaScriptEscape="true"/>',
      reviewHistoryTitle:'<s:message code="forms.find.results.reviewHistoryTitle" javaScriptEscape="true"/>',
      uploadFile: '<s:message code="forms.find.results.uploadFile" javaScriptEscape="true"/>',
      uploadFileTitle: '<s:message code="forms.find.results.uploadFileTitle" javaScriptEscape="true"/>'
    },
    resultsCountNode:Y.one('#formResultsCount'),
    noDataMessage:'<s:message code="forms.find.no.results"/>',
    permissions: permissions,
    url:'${formInstanceResultsURL}'
  };
  
  var resultsFilterContextConfig = {
      labels:{
          filter:'<s:message code="filter.active"/>',
          status:'<s:message code="forms.find.filter.status.active"/>',
          type:'<s:message code="forms.find.filter.type.active"/>',
          mostRecentOnly:'<s:message code="forms.find.filter.mostRecentOnly.active"/>',
          startDate:'<s:message code="forms.find.filter.startDate.active"/>',
          submittedDate:'<s:message code="forms.find.filter.submittedDate.active"/>',
          completionDate:'<s:message code="forms.find.filter.completionDate.active"/>',
          attachmentSource:'<s:message code="attachment.filter.source.active" javaScriptEscape="true"/>',
          attachmentSourceOrganisation:'<s:message code="attachment.filter.sourceOrganisation.active" javaScriptEscape="true"/>',
          attachmentTitle: '<s:message code="attachment.filter.title.active" javaScriptEscape="true"/>',
          hasAttachments: '<s:message code="attachment.filter.hasAttachments.active" javaScriptEscape="true"/>',
          resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
          resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
      },
      container:'#formFilterContext'
  };

  var resultsQuickFiltersConfig = {
      labels: {
        title:'<s:message code="forms.filter.quick.title"/>',
        filters: {
          mostRecentOnly:'<s:message code="forms.filter.quick.checkbox.mostRecentOnly"/>'
        }
      },
      container: '#formQuickFilters',
      localStorageNaming: 'formsQuickFilters',
      externallyManagedAttrs: ['mostRecentOnly']
  };

  var resultsFilterConfig = {
    labels:{
        filterBy: '<s:message code="forms.find.filter.title"/>',
        status: '<s:message code="forms.find.filter.status"/>',
        type: '<s:message code="forms.find.filter.type"/>',
        mostRecentOnly:'<s:message code="forms.find.filter.mostRecentOnly"/>',
        startDateFilter: '<s:message code="forms.find.filter.startDateFilter"/>',
        submittedDateFilter: '<s:message code="forms.find.filter.submittedDateFilter"/>',
        completionDateFilter: '<s:message code="forms.find.filter.completionDateFilter"/>',
        typeFilter: '<s:message code="forms.find.filter.typeFilter"/>',
        dateFrom: '<s:message code="forms.find.filter.dateFrom"/>',
        dateTo: '<s:message code="forms.find.filter.dateTo"/>',
        dateRangeLinks: '<s:message code="forms.find.filter.dateRangeLinks"/>',
        dateRangeLastWeek: '<s:message code="forms.find.filter.dateRangeLastWeek"/>',
        dateRangeLastMonth: '<s:message code="forms.find.filter.dateRangeLastMonth"/>',
        dateRangeLastYear: '<s:message code="forms.find.filter.dateRangeLastYear"/>',
        validation:{
          endBeforeStartDate: '<s:message code="forms.find.filter.endBeforeStartDate"/>',
          endBeforeCompletionDate: '<s:message code="forms.find.filter.endBeforeCompletionDate"/>',
          endBeforeSubmittedDate: '<s:message code="forms.find.filter.endBeforeSubmittedDate"/>'
        },
        attachmentFilter:'<s:message code="forms.find.filter.attachmentFilter" javaScriptEscape="true"/>',
        hasAttachments:{
          title:'<s:message code = "attachment.filter.hasAttachments.title" javaScriptEscape="true"/>',
          attachmentTitle:'<s:message code = "attachment.filter.title" javaScriptEscape="true"/>',
          withAttachments:'<s:message code = "attachment.filter.hasAttachments.withAttachments" javaScriptEscape="true"/>',
          withoutAttachment:'<s:message code = "attachment.filter.hasAttachments.withoutAttachments" javaScriptEscape="true"/>',
          attachmentSource:'<s:message code = "attachment.filter.source" javaScriptEscape="true"/>',
          attachmentOtherSource:'<s:message code = "attachment.filter.otherSource" javaScriptEscape="true"/>',
          attachmentSourceOrganisation:'<s:message code = "attachment.filter.sourceOrganisation" javaScriptEscape="true"/>',
          attachmentOtherSourceOrganisation:'<s:message code = "attachment.filter.otherSourceOrganisation" javaScriptEscape="true"/>'
        }
    },
    permissions:permissions,
    formInstanceTypesURL:'${formInstanceTypesURL}'
  };
    
  var formsIcon='fa fa-file-text';
  var formNarrativeDescription= '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

  var statusDialogConfig = {
    statusChangeURL:'<s:url value="/rest/formInstance/{id}/status?action={action}&subTypeId={subTypeId}"/>',
    views:{
      submitForm:{
        header: {
          text: '<s:message code="forms.form.submit.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.submit.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          message:'<s:message code="forms.form.submit.prompt" javaScriptEscape="true" />',
          subTypeId:'<s:message code="forms.statusChange.subTypeId" javaScriptEscape="true" />'
        },
        config:{
          //used in the errors plugin to link to the form content
          formEditorURL:'<s:url value="/forms/load?id={id}"/>'
        },        
        successMessage: '<s:message code="forms.form.submit.success"/>'
      },
      withdrawForm:{
        header: {
          text: '<s:message code="forms.form.withdraw.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.withdraw.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          message:'<s:message code="forms.form.withdraw.prompt" javaScriptEscape="true" />'
        },
        successMessage: '<s:message code="forms.form.withdraw.success"/>'
      },
      completeForm:{
        header: {
          text: '<s:message code="forms.form.complete.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.complete.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          message:'<s:message code="forms.form.complete.prompt" javaScriptEscape="true" />',
          subTypeId:'<s:message code="forms.statusChange.subTypeId" javaScriptEscape="true" />'
        },
        config:{
          //used in the errors plugin to link to the form content
          formEditorURL:'<s:url value="/forms/load?id={id}"/>'
        },
        successMessage: '<s:message code="forms.form.complete.success"/>'
      },
      uncompleteForm:{
        header: {
          text: '<s:message code="forms.form.uncomplete.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.uncomplete.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          message:'<s:message code="forms.form.uncomplete.prompt" javaScriptEscape="true" />'
        },
        successMessage: '<s:message code="forms.form.uncomplete.success"/>'
      },
      deleteForm:{
        header: {
          text: '<s:message code="forms.form.delete.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.delete.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          message:'<s:message code="forms.form.delete.prompt" javaScriptEscape="true" />'
        },
        successMessage: '<s:message code="forms.form.delete.success"/>'
      },
      removeForm:{
        header: {
          text: '<s:message code="forms.form.remove.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.remove.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          message:'<s:message code="forms.form.remove.prompt" javaScriptEscape="true" />'
        },
        config:{
          url:'<s:url value="/rest/formInstance/{id}"/>'
        },
        successMessage: '<s:message code="forms.form.remove.success"/>'
      },
      approveForm:{
        header: {
          text: '<s:message code="forms.form.approve.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.approve.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          comments:'<s:message code="updateReviewFormInstanceVO.approval.comments" javaScriptEscape="true"/>'
        },
        config:{
          url:'<s:url value="/rest/formInstance/{id}/status?action={action}"/>'
        },
        successMessage: '<s:message code="forms.form.approve.success"/>'
      },
      rejectForm:{
        header: {
          text: '<s:message code="forms.form.reject.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.reject.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          comments:'<s:message code="updateReviewFormInstanceVO.rejection.comments" javaScriptEscape="true"/>'
        },
        config:{
          url:'<s:url value="/rest/formInstance/{id}/status?action={action}"/>'
        },
        successMessage: '<s:message code="forms.form.reject.success"/>'
      }
    }
  };

  var attachmentLabels = {
    file: '<s:message code="file.upload.chooseFile" javaScriptEscape="true"/>',
    title: '<s:message code="file.upload.docTitle" javaScriptEscape="true"/>',
    description: '<s:message code="file.upload.docDescription" javaScriptEscape="true"/>',
    source: '<s:message code="common.source" javaScriptEscape="true"/>',
    otherSource: '<s:message code="common.source.other" javaScriptEscape="true"/>',
    sourceOrganisation: '<s:message code="common.sourceOrganisation" javaScriptEscape="true"/>',
    otherSourceOrganisation: '<s:message code="common.sourceOrganisation.other" javaScriptEscape="true"/>',
    sourceMustBeOtherOrNull: '<s:message code="common.sourceMustBeOtherOrNull" javaScriptEscape="true"/>'
  };
  
  var attachmentSearchConfig={
    sortBy:[{
      lastUploaded: 'desc'
    }],
    labels:{
      title: '<s:message code="forms.form.attachments.find.title" javaScriptEscape="true"/>',
      uploadedBy: '<s:message code="forms.form.attachments.find.uploadedBy" javaScriptEscape="true"/>',
      uploadedOn: '<s:message code="forms.form.attachments.find.uploadedOn" javaScriptEscape="true"/>',
      description: '<s:message code="forms.form.attachments.find.description" javaScriptEscape="true"/>',
      actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
      download: '<s:message code="file.download" javaScriptEscape="true"/>',
      downloadTitle: '<s:message code="file.downloadTitle" javaScriptEscape="true"/>',
      remove: '<s:message code="file.delete" javaScriptEscape="true"/>',
      removeTitle: '<s:message code="file.deleteTitle" javaScriptEscape="true"/>'
    },
    noDataMessage:'<s:message code="forms.form.attachments.find.no.results"/>',
    permissions: attachmentPermissions,
    url:'<s:url value="/rest/formInstance/{id}/attachment?inline=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
  };
  
  var attachmentDialogConfig = {
    views:{
      uploadFile:{
        header: {
          text: '<s:message code="file.upload.uploadFile" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        config: {
          url: '<s:url value="/rest/formInstance/{formInstanceId}/attachment"/>',
          urlComplete: '<s:url value="/rest/formInstance/{formInstanceId}/attachment?completedFormInstance=true"/>',
          validationLabels:{
            otherSource:'<s:message code="NotBlank.source.other" javaScriptEscape="true" />',
            otherSourceOrganisation:'<s:message code="NotBlank.sourceOrganisation.other" javaScriptEscape="true" />'
          }
        },
        narrative: {
          summary: '<s:message code="forms.form.attachment.upload.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        successMessage: '<s:message code="file.upload.success" javaScriptEscape="true"/>',
        labels:attachmentLabels
      },
      manageFiles:{
        header: {
          text: '<s:message code="forms.form.attachments.manage.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        narrative: {
          summary: '<s:message code="forms.form.attachments.manage.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        config:{
          searchConfig:attachmentSearchConfig
        },
        labels:{}
      },
      removeFile:{
        header: {
          text: '<s:message code="forms.form.attachments.remove.header" javaScriptEscape="true"/>',
          icon: formsIcon
        },
        config: {
          url: '<s:url value="/rest/formInstanceAttachment/{id}"/>'
        },        
        narrative: {
          summary: '<s:message code="forms.form.attachments.remove.summary" javaScriptEscape="true" />',
          description: formNarrativeDescription
        },
        labels:{
          message:'<s:message code="forms.form.attachments.remove.prompt" javaScriptEscape="true" />'
        },
        successMessage: '<s:message code="forms.form.attachments.remove.success"/>'
      }
    }
  };
  
  var outputNarrativeDescription= '{{this.name}}';

  var outputDialogConfig = {
    views:{      
      produceDocument:{
        type:'app.form.FormOutputDocumentView',
        header: {
          text: '<s:message code="forms.form.output.header" javaScriptEscape="true"/>',
          icon: 'fa fa-file'
        },
        narrative: {
          summary: '<s:message code="produceDocument.dialog.summary" javaScriptEscape="true" />',
          description: outputNarrativeDescription
        },
        labels:{
            outputCategory: '<s:message code="produceDocument.dialog.category" javaScriptEscape="true"/>',
            outputCategoryPrompt: '<s:message code="produceDocument.dialog.category.select.blank" javaScriptEscape="true"/>',
            availableTemplates: '<s:message code="produceDocument.dialog.availableTemplates" javaScriptEscape="true"/>',
            outputFormat: '<s:message code="produceDocument.dialog.outputFormat" javaScriptEscape="true"/>'
        },
        config:{
          templateListURL: '<s:url value="/rest/outputTemplate?pageNumber=-1&status=PUBLISHED&context=FORMS"/>',
          generateURL:'<s:url value="/rest/documentRequest"/>',
          formDocumentType: 'FORM',
          subjectId: subject.subjectId,
          subjectType: subject.subjectType,
          successMessage: '<s:message code="produceDocument.dialog.success.generateInProgress" javaScriptEscape="true" />',
          failureMessage: '<s:message code="produceDocument.dialog.failure.generateFailed" javaScriptEscape="true" />'
        }
      }
    }
  };
  
  var resultsConfig = {
      title:'<s:message javaScriptEscape="true" code="forms.find.title"/>',
      searchConfig: resultsSearchConfig,
      filterContextConfig: resultsFilterContextConfig,
      quickFiltersConfig: resultsQuickFiltersConfig,
      filterConfig: resultsFilterConfig,
      dialogConfig: statusDialogConfig,
      permissions: permissions,
      attachmentDialogConfig: attachmentDialogConfig,
      outputDialogConfig: outputDialogConfig,
      readingCfg: {
        triggerNode: '#readingButton',
        active: {
          className: 'pure-button-active',
          title: '<s:message code="button.view.active" />',
          ariaLabel: '<s:message code="button.view.active" />'
        },
        inactive: {
          title: '<s:message code="button.view.inactive" />',
          ariaLabel: '<s:message code="button.view.inactive" />'
        }
      },
      formEditorURL:'<s:url value="/forms/load?id={id}"/>',
      formInstanceURL:'<s:url value="/rest/formInstance/{id}"/>',
      formDefinitionURL:'<s:url value="/rest/formDefinition/{id}"/>',
      reviewHistoryURL:'<s:url value="/forms/reviews?id={id}" />',
      downloadAttachmentURL:'<s:url value="/rest/formInstanceAttachment/{id}/content?formInstanceId={formInstanceId}"/>'
    };
  
  var addSearchConfig = {
      sortBy: [{
        name: 'asc'
    }],
    labels: {
        name: '<s:message code="formTypes.find.results.name" javaScriptEscape="true"/>',
        version: '<s:message code="formTypes.find.results.version" javaScriptEscape="true"/>',
        actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
        addForm: '<s:message code="formTypes.find.results.addForm" javaScriptEscape="true"/>',
        addFormTitle: '<s:message code="formTypes.find.results.addFormTitle" javaScriptEscape="true"/>'
    },
    resultsCountNode: Y.one('#formResultsCount'),
    noDataMessage: '<s:message code="formTypes.find.no.results" javaScriptEscape="true"/>',
    permissions: permissions,
    url: '<s:url value="/rest/formDefinition?status=PUBLISHED&s={sortBy}&pageNumber={page}&pageSize={pageSize}&preFilterAccessLevel=WRITE&excludePreferAutomatedAdd={excludePreferAutomatedAdd}"/>'
   };
   var addFilterContextConfig = {
       labels: {
         filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
         name: '<s:message code="forms.add.filter.name.active" javaScriptEscape="true"/>',
         resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
         resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
     },
     container: '#formFilterContext'
   };

   var addQuickFiltersConfig = {
      container: '#formQuickFilters'
   };

   var addFilterConfig = {
       labels: {
         filterBy: '<s:message code="filter.by"/>',
         name: '<s:message code="forms.add.filter.name"/>'
     }
   };
    
   var addDialogConfig = {
      views: {
        addFormSelectMapping: {
            header: {
                text: '<s:message code="forms.form.addSelectMapping.header" javaScriptEscape="true"/>',
                icon: formsIcon
            },
            narrative: {
                summary: '<s:message code="forms.form.addSelectMapping.summary" javaScriptEscape="true" />',
                description: formNarrativeDescription
            },
            labels: {
                formMappingId: '<s:message code="forms.form.add.mapping.select" javaScriptEscape="true" />',
                emptyFormMappingId: '<s:message code="forms.form.add.mapping.mappingId.empty" javaScriptEscape="true" />'
            }
        },
        addFormSelectSubType: {
          header: {
              text: '<s:message code="forms.form.addSelectSubType.header" javaScriptEscape="true"/>',
              icon: formsIcon
          },
          narrative: {
              summary: '<s:message code="forms.form.addSelectSubType.summary" javaScriptEscape="true" />',
              description: formNarrativeDescription
          },
          labels: {
              subTypeId: '<s:message code="forms.form.add.subType.select" javaScriptEscape="true" />',
              emptySubTypeId: '<s:message code="forms.form.add.subType.subTypeId.empty" javaScriptEscape="true" />',
              message: '<s:message code="forms.form.add.subType.message" javaScriptEscape="true" />'
          }
        },
        addWhereInstanceExistsForGroup: {
            header: {
                text: '<s:message code="forms.form.addWhereInstanceExistsForGroup.header" javaScriptEscape="true"/>',
                icon: formsIcon
            },
            narrative: {
                summary: '<s:message code="forms.form.addWhereInstanceExistsForGroup.summary" javaScriptEscape="true" />',
                description: formNarrativeDescription
            },
            labels: {},
            config:{
              warning: '<s:message code="forms.form.addWhereInstanceExistsForGroup.warning" javaScriptEscape="false" />',
              message: '<s:message code="forms.form.addWhereInstanceExistsForGroup.prompt" javaScriptEscape="false" />'
            }
        },

        showExistingInstance: {
            header: {
                text: '<s:message code="forms.form.showExistingInstance.header" javaScriptEscape="true"/>',
                icon: formsIcon
            },
            narrative: {
                summary: '<s:message code="forms.form.showExistingInstance.summary" javaScriptEscape="true" />',
                description: formNarrativeDescription
            },
            config:{
              message: '<s:message code="forms.form.showExistingInstance.prompt" javaScriptEscape="false" />',
              restrictedMessage: '<s:message code="forms.form.showExistingInstance.worklist.restricted.prompt" javaScriptEscape="false" />'
            }
        },
        addMultipleInstance: {
            header: {
                text: '<s:message code="forms.form.addMultipleInstance.header" javaScriptEscape="true"/>',
                icon: formsIcon
            },
            narrative: {
                summary: '<s:message code="forms.form.addMultipleInstance.summary" javaScriptEscape="true" />',
                description: formNarrativeDescription
            },
            config:{
              message: '<s:message code="forms.form.addMultipleInstance.prompt" javaScriptEscape="false" />'
            }
        }
     }
   };
   
   var errorsConfig={
       validationError: {
         title: '<s:message code="form.add.validationError.title" javaScriptEscape="false" />',
         message: '<s:message code="form.add.validationError.message" javaScriptEscape="false" />'
     },
     staleDataError: {
         title: '<s:message code="form.add.staleDataError.title" javaScriptEscape="false" />',
         message: '<s:message code="form.add.staleDataError.message" javaScriptEscape="false" />'
     },
     serverError: {
         title: '<s:message code="form.add.serverError.title" javaScriptEscape="false" />',
         message: '<s:message code="form.add.serverError.message" javaScriptEscape="false" />'
     },
     notFoundError: {
         title: '<s:message code="form.add.notFoundError.title" javaScriptEscape="false" />',
         message: '<s:message code="form.add.notFoundError.message" javaScriptEscape="false" />'
     },
     accessDenied:{
         message:'<s:message code="form.add.accessDenied.message" javaScriptEscape="false" />'
     }
   };
   
   var addConfig = {
       title:'<s:message javaScriptEscape="true" code="forms.add.title"/>',
       searchConfig: addSearchConfig,
       filterContextConfig: addFilterContextConfig,
       quickFiltersConfig: addQuickFiltersConfig,
       filterConfig: addFilterConfig,
       dialogConfig: addDialogConfig,
       permissions: permissions,
       viewFormURL: '<s:url value="/forms/load?id={id}"/>',
       existingFormInstanceTypesURL: '${existingFormInstanceTypesURL}',
       applicableFormMappingsURL: '<s:url value="/rest/formMapping?formDefinitionId={id}&subjectType={subjectType}&subjectId={subjectId}"/>',
       addFormInstanceURL: '<s:url value="/rest/{subjectType}/{id}/formInstance"/>',
       definitionURL: '<s:url value="/rest/formDefinition?formDefinitionId={id}&latestOnly=true"/>',
       labels: {
           formDefinitionStateInvalid: '<s:message code="forms.form.invalidDefintionState" javaScriptEscape="false" />',
           successMessage: '<s:message javaScriptEscape="true" code="forms.form.add.success"/>'
       },
       errorsConfig:errorsConfig
     };

   var config = {
       root: Y.Lang.sub('<s:url value="/forms/{subjectType}/{subjectId}/"/>', subject),
       container: '#formResults',
       permissions: permissions,
       subject: subject,
       resultsConfig: resultsConfig,
       addConfig: addConfig,
       addRoute: Y.Lang.sub('<s:url value="/forms/{subjectType}/{subjectId}/add"/>', subject),
       viewRoute: Y.Lang.sub('<s:url value="/forms/{subjectType}/{subjectId}/view"/>', subject),
       linkFromConfig: {
         labels: {
             checklistLinkFromTemplate: '<s:message javaScriptEscape="true" code="forms.add.checklistLinkFromTemplate"/>'
         },
         permissions: permissions,
         formInstanceURL:'<s:url value="/rest/formInstance/{id}"/>'
       }
     };

   var formControllerView = new Y.app.form.FormControllerView(config).render();

   // Make sure to dispatch the current hash-based URL which was set by
   // the server to our route handlers.            
   if (formControllerView.hasRoute(formControllerView.getPath())) {
     formControllerView.dispatch();
   } else {
     //dispatch to starting page which is results
     formControllerView.navigate(Y.Lang.sub('<s:url value="/form/{subjectType}/view"/>', subject));
   }
});
</script>
