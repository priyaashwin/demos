<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 
 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'FormInstance','FormInstance.View')" var="canView"/>

<div id="formReviewResults"></div>

<script>
Y.use('form-reviews-view', function(Y){       
    var L=Y.Lang,
    	canView='${canView}',   
	    // create list of reviews
	    reviewListView=new Y.app.FormReviewListView({
			container:'#formReviewResults',
			searchConfig:{
				labels:{				
	   			    comments:'<s:message code="formsReviews.find.results.comments"/>',
	   				reviewedStatus:'<s:message code="formsReviews.find.results.status"/>',
	   				reviewedBy:'<s:message code="formsReviews.find.results.reviewer.name"/>',
	   				reviewDate:'<s:message code="formsReviews.find.results.reviewDate"/>',
	                viewReview:'<s:message code="formsReviews.find.results.viewReview"/>'
				},
				noDataMessage:'<s:message code="formsReviews.find.no.results"/>',
				permissions:{
					canView:canView==='true'
			    },			
			    url:L.sub('<s:url value="/rest/formInstance/{id}/review?s={sortBy}&pageNumber={page}&pageSize={pageSize}" />', {id:'<c:out value="${formId}"/>'})
			}
		}).render();     
});
</script>
