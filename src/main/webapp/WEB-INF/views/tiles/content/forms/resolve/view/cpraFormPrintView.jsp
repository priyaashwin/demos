<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- Define permissions --%> 
<sec:authorize access="hasPermission(null, 'CarePlanRiskAssessment','CarePlanRiskAssessment.View')" var="canView"/>

<c:choose>
    <c:when test="${canView eq true}">         
        <script> 
        Y.use('yui-base',
              'cpraform-print',
              'usp-resolveassessment-CarePlanRiskAssessmentFullDetails',
              'resolveassessment-component-enumerations',
            function(Y){
            
			Y.namespace('app').CarePlanRiskAssessmentFullDetails=Y.Base.create("CarePlanRiskAssessmentFullDetails", Y.usp.resolveassessment.CarePlanRiskAssessmentFullDetails,[],{
			    getKeyAreasByPlanType: function(planType){
                    var keyAreas=[];    
                    Y.Array.each(this.get('keyAreas'), function (keyArea) {
                        if (keyArea.planType===planType){
                            keyAreas.push(keyArea);
                        }
                    }); 
                	return keyAreas;
			    },
			    getProgressReportByPlanType: function(planType){
                    var progressReport=null;    
                    Y.Array.each(this.get('progressReports'), function (o) {
                        if (o.planType===planType){
                            progressReport=o;
                        }
                    }); 
                	return progressReport;
			    },
			    getIdentifiedRisksByPlanType: function(planType){
                    var identifiedRisks=[],      
                        identifiedRisksByType=[[],[],[],[]],
                        riskTypeTitles=[
                            '<s:message code="resolve.view.cpraform.risk.part1"/>',
              		        '<s:message code="resolve.view.cpraform.risk.part2"/>',
            		        '<s:message code="resolve.view.cpraform.risk.part3"/>',
            		        '<s:message code="resolve.view.cpraform.risk.part4"/>'
                        ];
                    Y.Array.each(this.get('identifiedRisks'), function (identifiedRisk) {
                        if (identifiedRisk.planType===planType){
                            if (identifiedRisk.type===Y.usp.resolveassessment.enum.RiskType.values[0].enumValue){
                                identifiedRisksByType[0].push(identifiedRisk);
                            }
                            else if (identifiedRisk.type===Y.usp.resolveassessment.enum.RiskType.values[1].enumValue){
                                identifiedRisksByType[1].push(identifiedRisk);
                            }
                            else if (identifiedRisk.type===Y.usp.resolveassessment.enum.RiskType.values[2].enumValue){
                                identifiedRisksByType[2].push(identifiedRisk);
                            }
                            else if (identifiedRisk.type===Y.usp.resolveassessment.enum.RiskType.values[3].enumValue){
                                identifiedRisksByType[3].push(identifiedRisk);
                            }
                        }
                    });              
                    Y.Array.each(Y.usp.resolveassessment.enum.RiskType.values, function(o, i) {
                        identifiedRisks.push({riskType:o.enumValue, title:riskTypeTitles[i], risks:identifiedRisksByType[i]});
                    }); 
                	return identifiedRisks;
			    }
          	},{
          		ATTRS:{
          		    carePlans: {
                        getter: function () {
                            var carePlans = [];                        
                            Y.Array.each(Y.usp.resolveassessment.enum.CarePlanType.values, function(o) {
                                if (o.enumName !== 'MANAGER_STAFF_CONSULTATION') {
                                    carePlans.push({planType:o.name,keyAreas:this.getKeyAreasByPlanType(o.enumName),identifiedRisks:this.getIdentifiedRisksByPlanType(o.enumName)});
                                }
                                else {
                                    carePlans.push({planType:o.name,keyAreas:this.getKeyAreasByPlanType(o.enumName),identifiedRisks:[]});
                                }
                            }, this);
                        	return carePlans;
                        }
                    },
                    orderedProgressReports: {
    	                getter: function () {
    	                    var progressReports = [];                        
    	                    Y.Array.each(Y.usp.resolveassessment.enum.CarePlanType.values, function(o) {
    	                        progressReports.push(this.getProgressReportByPlanType(o.enumName));
    	                    }, this);
    	                	return progressReports;
    	                }
    	            },
                    subjectIsFemale: {
                        writeOnce: 'initOnly',
                        getter: function () {
                        	return this.get('subject') !== undefined && this.get('subject').gender === 'FEMALE'; 
                        }
                    }
          		}
          	});
			
            var model=new Y.app.CarePlanRiskAssessmentFullDetails({
				id:'${formId}',
				url: '<s:url value="/rest/carePlanRiskAssessment/{id}"/>'
			}),
			viewLabels={
       		    background: {
       		        title:'<s:message code="resolve.view.cpraform.background.title"/>',
       				name:'<s:message code="carePlanRiskAssessmentVO.subject.name"/>',
       				dateOfBirth:'<s:message code="carePlanRiskAssessmentVO.subject.dateOfBirth"/>',
       				gp:'<s:message code="carePlanRiskAssessmentVO.gp"/>',
       				keyWorker:'<s:message code="carePlanRiskAssessmentVO.keyWorker"/>',
       				dateStarted:'<s:message code="carePlanRiskAssessmentVO.dateStarted"/>',
       				placementAddress:'<s:message code="carePlanRiskAssessmentVO.placementAddress"/>'
       		    },
       			history:{
       		        title:'<s:message code="resolve.view.cpraform.history.title"/>',
       				subjectAuthorisationDates:'<s:message code="carePlanRiskAssessmentVO.subjectAuthorisationDates"/>',
       				name:'<s:message code="carePlanRiskAssessmentVO.initiatorName"/>'
       			},
       			risks:{
   		        	title:'<s:message code="resolve.view.cpraform.risks.title"/>',
       				specificCommunicationNeeds:'<s:message code="carePlanRiskAssessmentVO.specificCommunicationNeeds"/>',
       				historyOfSpecificRisks:'<s:message code="carePlanRiskAssessmentVO.historyOfSpecificRisks"/>',
       				historicalRiskLevel:'<s:message code="carePlanRiskAssessmentVO.historicalRiskLevel"/>'
       			},
       			progress:{
       			    title:'<s:message code="resolve.view.cpraform.progress.title"/>',
       			    planType:'<s:message code="progressReportVO.planType"/>',
       			    minimalProgressSummary:'<s:message code="progressReportVO.minimalProgressSummary"/>',
       			    fairProgressSummary:'<s:message code="progressReportVO.fairProgressSummary"/>',
       			    goodProgressSummary:'<s:message code="progressReportVO.goodProgressSummary"/>',
       			    majorProgressSummary:'<s:message code="progressReportVO.majorProgressSummary"/>',
       			    reviewDate:'<s:message code="progressReportVO.reviewDate"/>'
       			},
       			views:{
       				serviceUserComments:'<s:message code="resolve.view.cpraform.views.title"/>',
   					relativeAdvocateComments:'<s:message code="resolve.view.cpraform.relativeViews.title"/>',
       				carersPerspective:'<s:message code="resolve.view.cpraform.carersPerspective.title"/>'
       			},
       			supportPlan:{
       			    title:'<s:message code="keyAreas.print.title"/>',
    			    actions:'<s:message code="keyAreaVO.results.actions"/>',
    	            keyAreaIdentifier:'<s:message code="keyAreaVO.results.keyAreaIdentifier"/>',
    	            requiredPlanOfSupport:'<s:message code="keyAreaVO.results.requiredPlanOfSupport"/>',
    	            progressOutcome:'<s:message code="keyAreaVO.results.progressOutcome"/>'
       			},
       			identifiedRisks:{
       			    title:'<s:message code="identifiedRisk.print.title"/>',
		            risk:'<s:message code="identifiedRiskVO.risk"/>',
		            likelihood:'<s:message code="identifiedRiskVO.likelihood"/>',
		            impact:'<s:message code="identifiedRiskVO.impact"/>',
		            total:'<s:message code="identifiedRiskVO.total"/>',
		            riskManagementActivity:'<s:message code="identifiedRiskVO.riskManagementActivity"/>',
		            revisedLikelihood:'<s:message code="identifiedRiskVO.revisedLikelihood"/>',
		            revisedImpact:'<s:message code="identifiedRiskVO.revisedImpact"/>',
		            revisedTotal:'<s:message code="identifiedRiskVO.revisedTotal"/>',
		            date:'<s:message code="identifiedRiskVO.date"/>'
    			},
       			rejection:{
                   	rejectionReason:'<s:message code="rejectFormVO.rejectionReason"/>',
                   	rejectionDate:'<s:message code="rejectFormVO.rejectionDate"/>',  
                    rejectedBy:'<s:message code="rejectFormVO.rejector.name"/>',
                    noRejections:'<s:message code="resolve.view.form.rejection.noRejections"/>' 
   				}
           	},  
    		printView=new Y.app.CPRAFormPrintView({
                container:'body',
               	model:model,
               	labels:viewLabels,
               	otherData: {
	                enumTypes: {
                        impact: Y.usp.resolveassessment.enum.Impact.values,
                        likelihood: Y.usp.resolveassessment.enum.Likelihood.values,
                        planType: Y.usp.resolveassessment.enum.CarePlanType.values,
                        historicalRiskLevels:Y.usp.resolveassessment.enum.HistoricalRiskLevel.values
	                }
               	}
            });
           
            model.load(function(){
                printView.render();
            });
        });
		</script>
		
		<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />
		
    </c:when>
    <c:otherwise>
        <%-- Insert access denied tile --%>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>
