<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>

<sec:authorize access="hasPermission(null, 'ChildCarerApproval','ChildCarerApproval.View')" var="canViewCarerApproval" />
<sec:authorize access="hasPermission(null, 'AdoptiveCarerApproval','AdoptiveCarerApproval.GET')" var="canViewAdoptiveCarerApproval" />
<sec:authorize access="hasPermission(null, 'FosterCarerExemption','FosterCarerExemption.GET')" var="canViewFosterExemption" />
<sec:authorize access="hasPermission(null, 'ChildCarerSuspension','ChildCarerSuspension.View')" var="canViewCarerSuspension" />
<sec:authorize access="hasPermission(null, 'SpecificChildApproval','SpecificChildApproval.View')" var="canViewSpecificChildApproval" />
<sec:authorize access="hasPermission(null, 'FosterCarerInvestigation','ChildCarerSuspension.View')" var="canViewFosterCarerInvestigation" />
<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson" />
<sec:authorize access="hasPermission(null, 'FosterCarerApproval','ApprovalsFosterCarer.View')" var="canViewFosterCarerApprovalAccordion" />
<sec:authorize access="hasPermission(null, 'SpecificChildApproval','ApprovalsSpecificChild.View')" var="canViewSpecificChildApprovalAccordion" />
<sec:authorize access="hasPermission(null, 'FosterCarerExemption','ApprovalsExemptions.View')" var="canViewFosterExemptionAccordion" />
<sec:authorize access="hasPermission(null, 'AdoptiveCarerApproval','AdopterApprovals.View')" var="canViewAdoptiveCarerApprovalAccordion" />
<sec:authorize access="hasPermission(null, 'Person','HouseholdCurrent.View')" var="canViewCurrentHouseholdAccordion" />
<sec:authorize access="hasPermission(null, 'Person','HouseholdPlacements.View')" var="canViewPlacementAccordion" />
<sec:authorize access="hasPermission(null, 'ChildCarerTraining','Training.View')" var="canViewCarerTraining" />
<sec:authorize access="hasPermission(null, 'ChildCarerTraining','ChildCarerTraining.Delete')" var="canDeleteChildCarerTraining" />
<sec:authorize access="hasPermission(null, 'FosterCarerTSDStandards','FosterCarerTSDStandards.View')" var="canViewFosterCarerTSDStandards" />
<sec:authorize access="hasPermission(null, 'FosterCarerSuspension','AvailabilityCarerUnavailable.View')" var="canViewFosterCarerSuspensionAccordion" />
<sec:authorize access="hasPermission(null, 'FosterCarerInvestigation','AvailabilityCarerUnderInvestigation.View')" var="canViewFosterCarerInvestigationAccordion" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
    <sec:authorize access="hasPermission(null, 'ChildCarerApproval','ChildCarerApproval.Add')" var="canAddCarerApproval" />
    <sec:authorize access="hasPermission(null, 'ChildCarerApproval','ChildCarerApproval.Update')" var="canUpdateCarerApproval" />
    <sec:authorize access="hasPermission(null, 'FosterCarerApproval','FosterCarerApproval.Remove')" var="canRemoveCarerApproval" />
    <sec:authorize access="hasPermission(null, 'DeletionRequest','DeletionRequest.GET')" var="canRequestDeletion" />
    <sec:authorize access="hasPermission(null, 'AdoptiveCarerApproval','AdoptiveCarerApproval.ADD')" var="canAddAdoptiveCarerApproval" />
    <sec:authorize access="hasPermission(null, 'AdoptiveCarerApproval','AdoptiveCarerApproval.DELETE')" var="canDeleteAdoptiveCarerApproval" />
    <%-- Back end is checking wrong permission - so we must do the same in the front end - that's on the next line
	<sec:authorize access="hasPermission(null, 'AdoptiveCarerApproval','AdoptiveCarerApproval.UPDATE')" var="canUpdateAdoptiveCarerApproval" />
	 --%>
    <sec:authorize access="hasPermission(null, 'ChildCarerApproval','ChildCarerApproval.Update')" var="canUpdateAdoptiveCarerApproval" />
    <%--The line above will need to be updated to the correct permission if the backend is ever fixed --%>
    <sec:authorize access="hasPermission(null, 'ChildCarerExemption','ChildCarerExemption.Add')" var="canAddCarerExemption" />
    <sec:authorize access="hasPermission(null, 'ChildCarerExemption','ChildCarerExemption.Update')" var="canUpdateCarerExemption" />
    <sec:authorize access="hasPermission(null, 'ChildCarerExemption','ChildCarerExemption.Delete')" var="canDeleteCarerExemption" />
    <sec:authorize access="hasPermission(null, 'ChildCarerSuspension','ChildCarerSuspension.Add')" var="canAddCarerSuspension" />
    <sec:authorize access="hasPermission(null, 'ChildCarerTraining','ChildCarerTraining.ADD')" var="canAddCarerTraining" />
    <sec:authorize access="hasPermission(null, 'FosterCarerTSDStandards','FosterCarerTSDStandards.ADD')" var="canAddFosterCarerTSDStandards" />
    <sec:authorize access="hasPermission(null, 'SpecificChildApproval','SpecificChildApproval.Add')" var="canAddSpecificChildApproval" />
    <sec:authorize access="hasPermission(null, 'SpecificChildApproval','SpecificChildApproval.Delete')" var="canDeleteSpecificChildApproval" />
    <sec:authorize access="hasPermission(null, 'FosterCarerInvestigation','ChildCarerSuspension.Add')" var="canAddFosterCarerInvestigation" />
    <sec:authorize access="hasPermission(null, 'FosterCarerInvestigation','FosterCarerUnderInvestigation.Update')" var="canUpdateFosterCarerInvestigation" />
    <sec:authorize access="hasPermission(null, 'FosterCarerInvestigation','FosterCarerUnderInvestigation.End')" var="canEndFosterCarerInvestigation" />
    <sec:authorize access="hasPermission(null, 'FosterCarerSuspension','FosterCarerSuspension.End')" var="canEndFosterCarerSuspension" />
    <sec:authorize access="hasPermission(null, 'FosterCarerRegistration','FosterCarerRegistration.End')" var="canEndFosterCarerRegistration" />
    <sec:authorize access="hasPermission(null, 'FosterCarerRegistration','FosterCarerRegistration.Update')" var="canEditFosterCarerRegistration" />
    <sec:authorize access="hasPermission(null, 'FosterCarerRegistration','FosterCarerRegistration.Remove')" var="canRemoveFosterCarerRegistration" />
    <sec:authorize access="hasPermission(null, 'ChildCarerApproval','ChildCarerApproval.UPDATE_DATES')" var="canEditApprovalDates" />
    <sec:authorize access="hasPermission(null, 'FosterCarerApproval','FosterCarerApproval.UPDATE_DATA')" var="canEditFosterApprovalData" />
</c:if>

<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="adopterApprovalContext" value="${userContextAttributes.getContext('adopter-approval')}" />
<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="fosterCarerApprovalContext" value="${userContextAttributes.getContext('foster-carer-approval')}" />

<c:set var="currentSubjectId" value="${person.id}" />

<div id="approvalTabs"></div>

<script>
    Y.use('approvals-controller-view', function (Y) {
        var permissions = {
            canViewCarerApproval: '${canViewCarerApproval}' === 'true',
            canAddCarerApproval: '${canAddCarerApproval}' === 'true',
            canEndFosterCarerRegistration: '${canEndFosterCarerRegistration}' === 'true',
            canUpdateCarerApproval: '${canUpdateCarerApproval}' === 'true',
            canRemoveCarerApproval: '${canRemoveCarerApproval}' === 'true' && '${canRequestDeletion}' === 'true',
            canViewFosterExemption: '${canViewFosterExemption}' === 'true',
            canAddCarerExemption: '${canAddCarerExemption}' === 'true',
            canUpdateCarerExemption: '${canUpdateCarerExemption}' === 'true',
            canDeleteCarerExemption: '${canDeleteCarerExemption}' === 'true',
            canViewCarerSuspension: '${canViewCarerSuspension}' === 'true',
            canAddCarerSuspension: '${canAddCarerSuspension}' === 'true',
            canEndFosterCarerSuspension: '${canEndFosterCarerSuspension}' === 'true',
            canAddCarerTraining: '${canAddCarerTraining}' === 'true',
            canDeleteChildCarerTraining: '${canDeleteChildCarerTraining}' === 'true',
            canAddFosterCarerTSDStandards: '${canAddFosterCarerTSDStandards}' === 'true',
            canAddSpecificChildApproval: '${canAddSpecificChildApproval}' === 'true',
            canDeleteSpecificChildApproval: '${canDeleteSpecificChildApproval}' === 'true',
            canAddAdoptiveCarerApproval: '${canAddAdoptiveCarerApproval}' === 'true',
            canDeleteAdoptiveCarerApproval: '${canDeleteAdoptiveCarerApproval}' === 'true',
            canViewAdoptiveCarerApproval: '${canViewAdoptiveCarerApproval}' === 'true',
            canUpdateAdoptiveCarerApproval: '${canUpdateAdoptiveCarerApproval}' === 'true',
            canAddFosterCarerInvestigation: '${canAddFosterCarerInvestigation}' === 'true',
            canViewFosterCarerInvestigation: '${canViewFosterCarerInvestigation}' === 'true',
            canUpdateFosterCarerInvestigation: '${canUpdateFosterCarerInvestigation}' === 'true',
            canEndFosterCarerInvestigation: '${canEndFosterCarerInvestigation}' === 'true',
            canViewPartnerDetails: '${canViewPerson}' === 'true',
            canViewFosterCarerApprovalAccordion: '${canViewFosterCarerApprovalAccordion}' === 'true',
            canViewSpecificChildApprovalAccordion: '${canViewSpecificChildApprovalAccordion}' === 'true',
            canViewFosterExemptionAccordion: '${canViewFosterExemptionAccordion}' === 'true',
            canViewAdoptiveCarerApprovalAccordion: '${canViewAdoptiveCarerApprovalAccordion}' === 'true',
            canViewCurrentHouseholdAccordion: '${canViewCurrentHouseholdAccordion}' === 'true',
            canViewPlacementAccordion: '${canViewPlacementAccordion}' === 'true',
            canViewCarerTraining: '${canViewCarerTraining}' === 'true',
            canViewFosterCarerTSDStandards: '${canViewFosterCarerTSDStandards}' === 'true',
            canViewFosterCarerSuspensionAccordion: '${canViewFosterCarerSuspensionAccordion}' === 'true',
            canViewFosterCarerInvestigationAccordion: '${canViewFosterCarerInvestigationAccordion}' === 'true',
            canEditApprovalDates: '${canEditApprovalDates}' === 'true',
            canEditFosterApprovalData: '${canEditFosterApprovalData}' === 'true',
            canEditFosterCarerRegistration: '${canEditFosterCarerRegistration}' === 'true',
            canRemoveFosterCarerRegistration: '${canRemoveFosterCarerRegistration}' === 'true'
        };

        var currentSubjectId = '${currentSubjectId}';

        var subject = {
            subjectId: currentSubjectId,
            subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
            subjectName: '${esc:escapeJavaScript(person.name)}',
            personTypes: '${esc:escapeJavaScript(person.personTypes)}',
            subjectType: 'person',
            hasActivePartner: '${hasActivePartner}' === 'true',
            activePartnerIdentifier: '${esc:escapeJavaScript(activePartnerIdentifier)}',
            activePartnerName: '${esc:escapeJavaScript(activePartnerName)}',
            activePartnerOrganisationId: '${activePartnerOrganisationId}'
        };

        var subjectNarrative = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

        var subjectNarrativeDescription = subjectNarrative + '{{%if (this.organisation) {%}} <s:message code="approvals.details.foster.dialog.add.extended.summary" javaScriptEscape="true" /> {{this.organisation.name}} ({{this.organisation.organisationIdentifier}}){{%}%}}';

        var caIcon = 'fa eclipse-childlookedafter';

        var approvalsConfig = {
            permissions: permissions,
            registrationsConfig: {
                title: 'Foster approval registrations and approvals',
                registration: {
                    url: '<s:url value="/rest/{subjectType}/{subjectId}/fosterCarerRegistration"/>',
                    subject: subject,
                    labels: {
                        missingDemographicShort: '<s:message code="approvals.details.missingDemographicShort" javaScriptEscape="true" />',
                        missingDemographicLong: '<s:message code="approvals.details.missingDemographicLong" javaScriptEscape="true" />',
                        estimatedDOBExceptionShort: '<s:message code="approvals.details.estimatedDOBExceptionShort" javaScriptEscape="true" />',
                        estimatedDOBExceptionLong: '<s:message code="approvals.details.estimatedDOBExceptionLong" javaScriptEscape="true" />',
                        addApprovalRegistration: '<s:message code="approvals.addRegistration" javaScriptEscape="true" />',
                        removeApprovalRegistration: '<s:message code="approvals.removeRegistration" javaScriptEscape="true" />',
                        addApproval: '<s:message code="approvals.add" javaScriptEscape="true" />',
                        registrationTable: {
                            startDate: '<s:message code="approval.registrations.startDate" javaScriptEscape="true" />',
                            endDate: '<s:message code="approval.registrations.endDate" javaScriptEscape="true" />',
                            endReason: '<s:message code="approval.registrations.endReason" javaScriptEscape="true" />',
                            approvingOrganisation: '<s:message code="approval.registrations.approvingOrganisation" javaScriptEscape="true" />',
                            maxPlaces: '<s:message code="approval.registrations.maxPlaces" javaScriptEscape="true" />',
                            maxRooms: '<s:message code="approval.registrations.maxRooms" javaScriptEscape="true" />',
                            actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',

                            emptyMessage: '<s:message code="approval.registrations.noResults" javaScriptEscape="true" />',
                            endFosterCarerRegistrationTitle: '<s:message code="approvals.end.registration.title" javaScriptEscape="true" />',
                            endFosterCarerRegistration: '<s:message code="approvals.end.registration.label" javaScriptEscape="true" />',
                            editFosterCarerRegistrationTitle: '<s:message code="approvals.edit.registration.title" javaScriptEscape="true" />',
                            editFosterCarerRegistration: '<s:message code="approvals.edit.registration.label" javaScriptEscape="true" />'
                        },
                        approvalsTable: {
                            startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
                            endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                            genderPermitted: '<s:message code="approvals.details.genderPermitted" javaScriptEscape="true" />',
                            careType: '<s:message code="approvals.details.careType" javaScriptEscape="true" />',
                            maxRelatedPlacement: '<s:message code="approvals.details.maxRelatedPlacement" javaScriptEscape="true" />',
                            maxUnrelatedPlacement: '<s:message code="approvals.details.maxUnrelatedPlacement" javaScriptEscape="true" />',
                            disabilityPermitted: '<s:message code="approvals.details.disabilityPermitted" javaScriptEscape="true" />',
                            numberOfRooms: '<s:message code="approvals.details.numberOfRooms" javaScriptEscape="true" />',
                            ageRangePermitted: '<s:message code="approvals.details.ageRangePermitted" javaScriptEscape="true" />',
                            notes: '<s:message code="approvals.details.notes" javaScriptEscape="true" />',
                            synchronisedPartner: '<s:message code="approvals.details.synchronisedPartner" javaScriptEscape="true" />',
                            actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',

                            viewApprovalTitle: '<s:message code="approvals.actions.view.title" javaScriptEscape="true" />',
                            viewApproval: '<s:message code="approvals.actions.view" javaScriptEscape="true" />',
                            editApprovalTitle: '<s:message code="approvals.actions.edit.title" javaScriptEscape="true" />',
                            editApproval: '<s:message code="approvals.actions.edit" javaScriptEscape="true" />',
                            removeApprovalTitle: '<s:message code="approvals.actions.remove.title" javaScriptEscape="true" />',
                            removeApproval: '<s:message code="approvals.actions.remove" javaScriptEscape="true" />',
                            editFosterApprovalDataTitle: '<s:message code="approvals.actions.editFosterApprovalData.title" javaScriptEscape="true" />',
                            editFosterApprovalData: '<s:message code="approvals.actions.editFosterApprovalData" javaScriptEscape="true" />'
                        }
                    }
                }
            },
            specificChildApprovalConfig: {
                searchConfig: {
                    sortBy: [],
                    labels: {
                        id: '<s:message code="approvals.specificChildApproval.results.id" javaScriptEscape="true" />',
                        name: '<s:message code="approvals.specificChildApproval.results.name" javaScriptEscape="true" />',
                        actions: '<s:message code="approvals.specificChildApproval.results.actions" javaScriptEscape="true" />'
                    },
                    initialMessage: '<s:message code="approvals.specificChildApproval.results.initialMessage" javaScriptEscape="true" />',
                    noDataMessage: '<s:message code="approvals.specificChildApproval.no.results" javaScriptEscape="true" />',
                    url: '<s:url value="/rest/fosterCarerApproval/{id}/specificChildApproval?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                },
                tablePanelConfig: {
                    title: '<s:message code="approvals.specificChildApproval.accordion.title" javaScriptEscape="true" />'
                },
                subject: subject,
                labels: {
                    add: '<s:message code="button.add" javaScriptEscape="true" />',
                    addApproval: '<s:message code="approvals.specificChildApproval.add" javaScriptEscape="true" />',
                    addApprovalAriaLabel: '<s:message code="approvals.specificChildApproval.add.label" javaScriptEscape="true" />'
                }
            },
            exemptionsConfig: {
                searchConfig: {
                    sortBy: [{
                        'startDate': 'desc'
                    }],
                    labels: {
                        actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        exemptionType: '<s:message code="approvals.exemption.type" javaScriptEscape="true" />'
                    },
                    noDataMessage: '<s:message code="approvals.exemptions.no.results" javaScriptEscape="true" />',
                    url: '<s:url value="/rest/person/${currentSubjectId}/fosterCarerExemption?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                },
                subject: subject,
                labels: {
                    add: '<s:message code="button.add" javaScriptEscape="true" />',
                    addCarerExemption: '<s:message code="approvals.exemptions.add" javaScriptEscape="true" />',
                    addCarerExemptionAriaLabel: '<s:message code="approvals.exemptions.add.label" javaScriptEscape="true" />'
                },
                tablePanelConfig: {
                    title: '<s:message code="approvals.exemptions.accordion.title" javaScriptEscape="true" />'
                }
            }
        };

        var adoptiveCarerApprovalConfig = {
            permissions: permissions,
            adoptiveCarerApprovalDetailsConfig: {
                searchConfig: {
                    sortBy: [{
                        'startDate': 'desc'
                    }],
                    labels: {
                        viewApprovals: '<s:message code="approvals.actions.view" javaScriptEscape="true" />',
                        actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.adoptive.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        maxRelatedPlacement: '<s:message code="approvals.details.maxRelatedPlacement" javaScriptEscape="true" />',
                        maxUnrelatedPlacement: '<s:message code="approvals.details.maxUnrelatedPlacement" javaScriptEscape="true" />',
                        disabilityPermitted: '<s:message code="approvals.details.disabilityPermitted" javaScriptEscape="true" />',
                        genderPermitted: '<s:message code="approvals.details.genderPermitted" javaScriptEscape="true" />',
                        numberOfRooms: '<s:message code="approvals.details.numberOfRooms" javaScriptEscape="true" />',
                        ageRangePermitted: '<s:message code="approvals.details.ageRangePermitted" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.details.notes" javaScriptEscape="true" />',
                        childrenPlaced: '<s:message code="approvals.details.childrenPlaced" javaScriptEscape="true" />'
                    },
                    noDataMessage: '<s:message code="approvals.no.results" javaScriptEscape="true" />',
                    url: '<s:url value="/rest/person/${currentSubjectId}/adoptiveCarerApproval?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                },
                labels: {
                    add: '<s:message code="button.add" javaScriptEscape="true" />',
                    addApproval: '<s:message code="approvals.adoptive.add" javaScriptEscape="true" />',
                    addApprovalAriaLabel: '<s:message code="approvals.adoptive.add.label" javaScriptEscape="true" />',
                    missingDemographicShort: '<s:message code="approvals.details.missingDemographicShort" javaScriptEscape="true" />',
                    missingDemographicLong: '<s:message code="approvals.details.missingDemographicLong" javaScriptEscape="true" />',
                    estimatedDOBExceptionShort: '<s:message code="approvals.details.estimatedDOBExceptionShort" javaScriptEscape="true" />',
                    estimatedDOBExceptionLong: '<s:message code="approvals.details.estimatedDOBExceptionLong" javaScriptEscape="true" />'
                },
                subject: subject,
                tablePanelConfig: {
                    title: '<s:message code="approvals.adoptive.details.accordion.title" javaScriptEscape="true" />'
                }
            }
        };

        var carerTrainingConfig = {
            permissions: permissions,
            trainingConfig: {
                searchConfig: {
                    sortBy: [{
                        'startDate': 'desc'
                    }],
                    labels: {
                        startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        attendedWith: '<s:message code="approvals.training.attendedWith" javaScriptEscape="true" />',
                        trainingCourse: '<s:message code="approvals.training.course" javaScriptEscape="true" />',
                        actions: '<s:message code="approvals.training.actions" javaScriptEscape="true" />',
                        careType: '<s:message code="approvals.details.careType" javaScriptEscape="true" />',
                        deleteCarerTrainingTitle: '<s:message code="approvals.training.delete.label" javaScriptEscape="true" />',
                        deleteCarerTraining: '<s:message code="approvals.training.delete.title.label" javaScriptEscape="true" />'
                    },
                    noDataMessage: '<s:message code="approvals.training.no.results" javaScriptEscape="true" />',
                    url: '<s:url value="/rest/person/${currentSubjectId}/fosterCarerTraining?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                },
                labels: {
                    add: '<s:message code="button.add" javaScriptEscape="true" />',
                    addCarerTraining: '<s:message code="approvals.training.add" javaScriptEscape="true" />',
                    addCarerTrainingAriaLabel: '<s:message code="approvals.training.add.label" javaScriptEscape="true" />'
                },
                subject: subject,
                tablePanelConfig: {
                    title: '<s:message code="approvals.training.title" javaScriptEscape="true" />'
                }
            },
            tsdstandardsConfig: {
                searchConfig: {
                    sortBy: [{
                        'statusDate': 'desc'
                    }],
                    labels: {
                        statusDate: '<s:message code="approvals.tsdstandards.statusDate" javaScriptEscape="true" />',
                        statusCode: '<s:message code="approvals.tsdstandards.statusCode" javaScriptEscape="true" />',
                        actions: '<s:message code="approvals.tsdstandards.actions" javaScriptEscape="true" />',
                        progressTitle: '<s:message code="approvals.tsdstandards.progressTitle" javaScriptEscape="true" />',
                        progress: '<s:message code="approvals.tsdstandards.progress" javaScriptEscape="true" />',
                        viewTitle: '<s:message code="approvals.tsdstandards.viewTitle" javaScriptEscape="true" />',
                        view: '<s:message code="approvals.tsdstandards.view" javaScriptEscape="true" />',
                        viewHistoryTitle: '<s:message code="approvals.tsdstandards.viewHistoryTitle" javaScriptEscape="true" />',
                        viewHistory: '<s:message code="approvals.tsdstandards.viewHistory" javaScriptEscape="true" />'
                    },
                    noDataMessage: '<s:message code="approvals.tsdstandards.no.results" javaScriptEscape="true" />',
                    url: '<s:url value="/rest/person/${currentSubjectId}/fosterCarerTSDStandards?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                },
                labels: {
                    add: '<s:message code="button.add" javaScriptEscape="true" />',
                    addFosterCarerTSDStandards: '<s:message code="approvals.tsdstandards.add" javaScriptEscape="true" />',
                    addFosterCarerTSDStandardsAreaLabel: '<s:message code="approvals.tsdstandards.add.label" javaScriptEscape="true" />'
                },
                subject: subject,
                tablePanelConfig: {
                    title: '<s:message code="approvals.tsdstandards.title" javaScriptEscape="true" />'
                }
            }
        };

        var availabilityConfig = {
            permissions: permissions,
            fosterCarerSuspensionAccordionConfig: {
                searchConfig: {
                    sortBy: [{
                        'startDate': 'desc'
                    }, {
                        'endDate': 'desc'
                    }],
                    labels: {
                        startDate: '<s:message code="approvals.availability.suspension.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.suspension.endDate" javaScriptEscape="true" />',
                        reason: '<s:message code="approvals.availability.suspension.reason" javaScriptEscape="true" />',
                        placesUnavailable: '<s:message code="approvals.availability.suspension.placesUnavailable" javaScriptEscape="true" />',
                        actions: '<s:message code="approvals.availability.suspension.actions" javaScriptEscape="true" />'
                    },
                    noDataMessage: '<s:message code="approvals.availability.suspension.no.results" javaScriptEscape="true" />',
                    url: '<s:url value="/rest/person/${currentSubjectId}/fosterCarerSuspension?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                },
                labels: {
                    add: '<s:message code="button.add" javaScriptEscape="true" />',
                    addCarerTraining: '<s:message code="approvals.training.add" javaScriptEscape="true" />',
                    addCarerTrainingAriaLabel: '<s:message code="approvals.training.add.label" javaScriptEscape="true" />',
                },
                subject: subject,
                tablePanelConfig: {
                    title: '<s:message code="approvals.availability.suspension.accordion.title" javaScriptEscape="true" />'
                }
            },
            fosterCarerUnderInvestigationAccordionConfig: {
                searchConfig: {
                    sortBy: [{
                        'startDate': 'desc'
                    }],
                    labels: {
                        edit: '<s:message code="approvals.availability.underInvestigation.edit" javaScriptEscape="true" />',
                        end: '<s:message code="approvals.availability.underInvestigation.end" javaScriptEscape="true" />',
                        view: '<s:message code="approvals.availability.underInvestigation.view" javaScriptEscape="true" />',
                        actions: '<s:message code="approvals.availability.underInvestigation.actions" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.availability.underInvestigation.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.underInvestigation.endDate" javaScriptEscape="true" />',
                        allegedAbuse: '<s:message code="approvals.availability.underInvestigation.allegedAbuse" javaScriptEscape="true" />',
                        outcome: '<s:message code="approvals.availability.underInvestigation.outcome" javaScriptEscape="true" />'
                    },
                    noDataMessage: '<s:message code="approvals.availability.underInvestigation.no.results" javaScriptEscape="true" />',
                    url: '<s:url value="/rest/person/${currentSubjectId}/fosterCarerUnderInvestigation?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                },
                labels: {
                    add: '<s:message code="button.add" javaScriptEscape="true" />',
                    addCarerTraining: '<s:message code="approvals.availability.underInvestigation.add" javaScriptEscape="true" />',
                    addCarerTrainingAriaLabel: '<s:message code="approvals.availability.underInvestigation.add.label" javaScriptEscape="true" />',
                },
                subject: subject,
                tablePanelConfig: {
                    title: '<s:message code="approvals.availability.underInvestigation.accordion.title" javaScriptEscape="true" />'
                }
            }
        };

        var householdResultLabels = {
            personName: '<s:message code="approvals.current.household.personName" javaScriptEscape="true" />',
            relationshipName: '<s:message code="approvals.current.household.relationshipName" javaScriptEscape="true" />',
            relationshipType: '<s:message code="approvals.current.household.relationshipType" javaScriptEscape="true" />',
            age: '<s:message code="approvals.current.household.age" javaScriptEscape="true" />',
            address: '<s:message code="approvals.current.household.address" javaScriptEscape="true" />',
            dateOfBirth: '<s:message code="approvals.current.household.dateOfBirth" javaScriptEscape="true" />'
        };

        var placementLabels = {
            personName: '<s:message code="approvals.household.placement.results.name" javaScriptEscape="true" />',
            dateOfBirth: '<s:message code="approvals.household.placement.results.dob" javaScriptEscape="true" />',
            startDate: '<s:message code="approvals.household.placement.results.startDate" javaScriptEscape="true" />',
            endDate: '<s:message code="approvals.household.placement.results.endDate" javaScriptEscape="true" />',
            placementType: '<s:message code="approvals.household.placement.results.placementType" javaScriptEscape="true" />',
            captionCurrentPlacement: '<s:message code="approvals.household.placement.caption.current" javaScriptEscape="true" />',
            captionHistoricPlacement: '<s:message code="approvals.household.placement.caption.historic" javaScriptEscape="true" />'
        };

        var householdConfig = {
            permissions: {
                canViewCurrentHouseholdAccordion: permissions.canViewCurrentHouseholdAccordion,
                canViewPlacementAccordion: permissions.canViewPlacementAccordion
            },
            currentHouseholdConfig: {
                sameLocationAddressResultsConfig: {
                    searchConfig: {
                        sortBy: [],
                        labels: householdResultLabels,
                        noDataMessage: '<s:message code="approvals.current.household.no.results" javaScriptEscape="true" />',
                        url: '<s:url value="/rest/person/${currentSubjectId}/personRelationship/sharedLocationResidents?type=HOME&usage=PERMANENT&onlyRelatedMembers=true"/>',
                    },
                    tablePanelConfig: {
                        title: '<s:message code="approvals.current.household.caption" javaScriptEscape="true" />'
                    }
                },
                widerFamilyAddressResultsConfig: {
                    searchConfig: {
                        sortBy: [],
                        labels: householdResultLabels,
                        noDataMessage: '<s:message code="approvals.different.household.no.results" javaScriptEscape="true" />',
                        url: '<s:url value="/rest/person/${currentSubjectId}/personRelationship/familyNotSharingLocation?type=HOME&usage=PERMANENT"/>'
                    },
                    tablePanelConfig: {
                        title: '<s:message code="approvals.different.household.caption" javaScriptEscape="true" />'
                    }
                },
                title: '<s:message code="approvals.current.household.accordion.title" javaScriptEscape="true" />'
            },
            placementConfig: {
                currentPlacementResultsConfig: {
                    searchConfig: {
                        sortBy: [],
                        labels: placementLabels,
                        noDataMessage: '<s:message code="approvals.household.placement.current.no.results" javaScriptEscape="true" />',
                        url: '<s:url value="/rest/placement/current?personCarerId=${currentSubjectId}" />'
                    },
                    tablePanelConfig: {
                        title: '<s:message code="approvals.household.placement.caption.current" javaScriptEscape="true" />'
                    }
                },
                historicPlacementResultsConfig: {
                    searchConfig: {
                        sortBy: [],
                        labels: placementLabels,
                        noDataMessage: '<s:message code="approvals.household.placement.historic.no.results" javaScriptEscape="true" />',
                        url: '<s:url value="/rest/placement/historic?personCarerId=${currentSubjectId}" />'
                    },
                    tablePanelConfig: {
                        title: '<s:message code="approvals.household.placement.caption.historic" javaScriptEscape="true" />'
                    }
                },
                title: '<s:message code="approvals.household.placement.accordion.title" javaScriptEscape="true" />'
            }
        };

        var commonApprovalDialogLabels = {
            ageRange: '<s:message code="approvals.details.ageRangePermitted" javaScriptEscape="true" />',
            ageRangeMax: '<s:message code="approvals.details.dialog.ageRangeMax" javaScriptEscape="true" />',
            ageRangeMin: '<s:message code="approvals.details.dialog.ageRangeMin" javaScriptEscape="true" />',
            maxAvailablePlaces: '<s:message code="approvals.details.dialog.maxAvailablePlaces" javaScriptEscape="true" />',
            maxAvailableRooms: '<s:message code="approvals.details.dialog.maxAvailableRooms" javaScriptEscape="true" />',
            disabilityPermitted: '<s:message code="approvals.details.disabilityPermitted" javaScriptEscape="true" />',
            genderOfChildren: '<s:message code="approvals.details.dialog.genderPermitted" javaScriptEscape="true" />',
            maxRelated: '<s:message code="approvals.details.dialog.maxRelatedPlacement" javaScriptEscape="true" />',
            maxUnrelated: '<s:message code="approvals.details.dialog.maxUnrelatedPlacement" javaScriptEscape="true" />',
            numberOfRooms: '<s:message code="approvals.details.dialog.numberOfRooms" javaScriptEscape="true" />',
            startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
            endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
            typesOfCare: '<s:message code="approvals.details.dialog.careType" javaScriptEscape="true" />',
            notes: '<s:message code="approvals.details.notes" javaScriptEscape="true" />',
            organisation: '<s:message code="approvals.details.organisation" javaScriptEscape="true" />',
            organisationAddress: '<s:message code="approvals.details.organisationAddress" javaScriptEscape="true" />',
            partner: '<s:message code="approvals.details.appliedToPartner" javaScriptEscape="true" />',
            isTempApproval: '<s:message code="approvals.details.isTempApproval" javaScriptEscape="true" />',
            isTempApprovalYes: '<s:message code="approvals.details.dialog.isTempApproval.yes" javaScriptEscape="true" />',
            isTempApprovalNo: '<s:message code="approvals.details.dialog.isTempApproval.no" javaScriptEscape="true" />',
            typeOfDisability: '<s:message code="approvals.details.typeOfDisability" javaScriptEscape="true" />'
        };

        var dialogConfig = {
            views: {
                addFosterApproval: {
                    header: {
                        text: '<s:message code="approvals.details.foster.add.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.foster.dialog.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.details.add.details.success.message" javaScriptEscape="true" />',
                    labels: Y.mix(commonApprovalDialogLabels, {
                        shortTerm: '<s:message code="approvals.details.dialog.shortTerm" javaScriptEscape="true" />',
                        longTerm: '<s:message code="approvals.details.dialog.longTerm" javaScriptEscape="true" />',
                        respite: '<s:message code="approvals.details.dialog.respite" javaScriptEscape="true" />',
                        emergency: '<s:message code="approvals.details.dialog.emergency" javaScriptEscape="true" />',
                        synchroniseWithPartner: '<s:message code="approvals.details.dialog.synchroniseWithPartner" javaScriptEscape="true" />',
                        supportForDisability: '<s:message code="approvals.details.dialog.supportForDisability" javaScriptEscape="true" />',
                        primaryCarer: '<s:message code="approvals.details.dialog.primaryCarer" javaScriptEscape="true" />',
                        primaryCarerYes: '<s:message code="approvals.details.dialog.primaryCarer.yes" javaScriptEscape="true" />',
                        primaryCarerNo: '<s:message code="approvals.details.dialog.primaryCarer.no" javaScriptEscape="true" />'
                    }),
                    validationLabels: {
                        maxRelatedPlacements: '<s:message code="approvals.details.dialog.maxRelatedPlacementsErrorMessage" javaScriptEscape="true" />',
                        maxUnrelatedPlacements: '<s:message code="approvals.details.dialog.maxUnrelatedPlacementsErrorMessage" javaScriptEscape="true" />',
                        numberOfRooms: '<s:message code="approvals.details.dialog.numberOfRoomsErrorMessage" javaScriptEscape="true" />',
                        upperAgePermitted: '<s:message code="approvals.details.dialog.upperAgePermittedErrorMessage" javaScriptEscape="true" />',
                        lowerAgePermitted: '<s:message code="approvals.details.dialog.lowerAgePermittedErrorMessage" javaScriptEscape="true" />',
                        disabilitySupport: '<s:message code="approvals.details.dialog.disabilitySupportErrorMessage" javaScriptEscape="true" />',
                        primaryCarer: '<s:message code="approvals.details.dialog.primaryCarerErrorMessage" javaScriptEscape="true" />',
                        typeOfCareId: '<s:message code="approvals.details.dialog.typeOfCareIdErrorMessage" javaScriptEscape="true" />',
                        organisationId: '<s:message code="approvals.details.dialog.organisationIdErrorMessage" javaScriptEscape="true" />'
                    },
                    width: 800,
                    config: {
                        url: '<s:url value="/rest/fosterCarerApproval/"/>',
                        fosterCarerApprovalContext: '${fosterCarerApprovalContext}',
                        autocomplete: {
                            autocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
                            labels: {
                                placeholder: '<s:message code="approvals.details.dialog.organisationSearchPlaceholder" javaScriptEscape="true"/>'
                            }
                        }
                    }
                },
                endFosterCarerRegistrationError: {
                    header: {
                        text: '<s:message code="approvals.details.foster.registration.end.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.foster.registration.end.summary" javaScriptEscape="true" />',
                        description: subjectNarrative
                    },
                    labels: {
                        message: '<s:message code="approvals.details.foster.registration.end.message" javaScriptEscape="true" />',
                        approvalsCount: '<s:message code="approvals.details.foster.registration.end.approvals.message" javaScriptEscape="true" />',
                        underInvestigationCount: '<s:message code="approvals.details.foster.registration.end.investigation.message" javaScriptEscape="true" />',
                        suspensionCount: '<s:message code="approvals.details.foster.registration.end.suspension.message" javaScriptEscape="true" />',
                        exemptionCount: '<s:message code="approvals.details.foster.registration.end.exemption.message" javaScriptEscape="true" />'
                    },
                    width: 500,
                    config: {
                        url: '<s:url value="/rest/fosterCarerRegistration/{id}/validation?personId={subjectId}"/>'
                    }
                },
                endFosterCarerRegistrationForm: {
                    header: {
                        text: '<s:message code="approvals.details.foster.registration.end.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.foster.registration.end.summary" javaScriptEscape="true" />',
                        description: subjectNarrative
                    },
                    labels: {
                        endDate: '<s:message code="common.column.date.end" javaScriptEscape="true" />',
                        endReason: '<s:message code="common.column.reason" javaScriptEscape="true" />',
                    },
                    width: 500,
                    config: {
                        submitUrl: '<s:url value="/rest/fosterCarerRegistration/{id}/status?action=ended"/>'
                    }
                },
                editFosterCarerRegistration: {
                    header: {
                        text: '<s:message code="approvals.details.foster.registration.edit.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.foster.registration.edit.summary" javaScriptEscape="true" />',
                        description: subjectNarrative
                    },
                    successMessage: '<s:message code="approvals.details.foster.registration.edit.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="updateFosterCarerRegistrationVO.startDate" javaScriptEscape="true" />',
                        maxAvailablePlaces: '<s:message code="updateFosterCarerRegistrationVO.maxAvailablePlaces" javaScriptEscape="true" />',
                        maxAvailableRooms: '<s:message code="updateFosterCarerRegistrationVO.maxAvailableRooms" javaScriptEscape="true" />',
                        primaryCarer: '<s:message code="approvals.details.dialog.primaryCarer" javaScriptEscape="true" />',
                        primaryCarerYes: '<s:message code="approvals.details.dialog.primaryCarer.yes" javaScriptEscape="true" />',
                        primaryCarerNo: '<s:message code="approvals.details.dialog.primaryCarer.no" javaScriptEscape="true" />'
                    },
                    validationLabels: {
                        startDateMandatory: '<s:message code="approvals.details.foster.registration.edit.startDate.mandatory" javaScriptEscape="true" />',
                        maxAvailablePlacesMandatory: '<s:message code="approvals.details.foster.registration.edit.maxAvailablePlaces.mandatory" javaScriptEscape="true" />',
                        maxAvailablePlacesIsNumeric: '<s:message code="approvals.details.foster.registration.edit.maxAvailablePlaces.isNumeric" javaScriptEscape="true" />',
                        maxAvailableRoomsMandatory: '<s:message code="approvals.details.foster.registration.edit.maxAvailableRooms.mandatory" javaScriptEscape="true" />',
                        maxAvailableRoomsIsNumeric: '<s:message code="approvals.details.foster.registration.edit.maxAvailableRooms.isNumeric" javaScriptEscape="true" />',
                        primaryCarer: '<s:message code="approvals.details.dialog.primaryCarerErrorMessage" javaScriptEscape="true" />',
                    },
                    config: {
                        url: '<s:url value="/rest/fosterCarerRegistration/{id}"/>'
                    }
                },
                viewFosterApproval: {
                    header: {
                        text: '<s:message code="approvals.details.foster.view.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.foster.dialog.view.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription,
                    },
                    labels: Y.mix(commonApprovalDialogLabels, {
                        viewCarerDetailsSummary: '<s:message code="approvals.details.foster.dialog.view.summary" javaScriptEscape="true" />'
                    }),
                    config: {
                        url: '<s:url value="/rest/childCarerApproval/{id}/"/>',
                        fosterCarerApprovalContext: '${fosterCarerApprovalContext}'
                    }
                },
                editFosterApproval: {
                    header: {
                        text: '<s:message code="approvals.details.foster.edit.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.foster.dialog.edit.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription,
                    },
                    successMessage: '<s:message code="approvals.details.edit.details.success.message" javaScriptEscape="true" />',
                    labels: Y.mix(commonApprovalDialogLabels, {
                        viewCarerDetailsSummary: '<s:message code="approvals.details.foster.dialog.view.summary" javaScriptEscape="true" />'
                    }),
                    config: {
                        fosterCarerApprovalContext: '${fosterCarerApprovalContext}',
                        url: '<s:url value="/rest/childCarerApproval/{id}/"/>'
                    }
                },
                removeLastFosterApprovalWarning: {
                    header: {
                        text: '<s:message code="approvals.foster.remove.dialog.warning.header" javaScriptEscape="true"/>',
                        icon: 'fa fa-wrench'
                    },
                    labels: {
                        message: '<s:message code="approvals.foster.remove.dialog.warning.lastapproval" javaScriptEscape="true" />'
                    }
                },
                addSpecificChildApproval: {
                    header: {
                        text: '<s:message code="approvals.specificChildApproval.dialog.add.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.specificChildApproval.dialog.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.specificChildApproval.add.success.message" javaScriptEscape="true" />',
                    labels: {
                        personSearch: '<s:message code="approvals.specificChildApproval.dialog.add.personSearch" javaScriptEscape="true" />',
                        autoCompletePlaceholder: '<s:message code="approvals.specificChildApproval.dialog.add.autoCompletePlaceholder" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/specificChildApproval/"/>',
                        personAutocompleteURL: '<s:url value="/rest/person?nameOrId={query}&pageSize={maxResults}&appendWildcard=true"/>'
                    }
                },
                deleteSpecificChildApproval: {
                    header: {
                        text: '<s:message code="approvals.specificChildApproval.dialog.delete.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.specificChildApproval.dialog.delete.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.specificChildApproval.delete.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.specificChildApproval.dialog.delete.deleteConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/specificChildApproval/{id}/"/>'
                    }
                },
                addCarerExemption: {
                    header: {
                        text: '<s:message code="approvals.exemption.add.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.exemption.dialog.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.exemptions.add.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        details: '<s:message code="approvals.exemptions.details" javaScriptEscape="true" />',
                        type: '<s:message code="approvals.exemptions.type" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerExemption/"/>',
                    }
                },
                editCarerExemption: {
                    header: {
                        text: '<s:message code="approvals.exemption.edit.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.exemption.dialog.edit.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.exemptions.edit.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        details: '<s:message code="approvals.exemptions.details" javaScriptEscape="true" />',
                        type: '<s:message code="approvals.exemptions.type" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerExemption/{id}/"/>'
                    }
                },
                deleteCarerExemption: {
                    header: {
                        text: '<s:message code="approvals.exemption.delete.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.exemption.dialog.delete.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.exemptions.delete.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        details: '<s:message code="approvals.exemptions.details" javaScriptEscape="true" />',
                        type: '<s:message code="approvals.exemptions.type" javaScriptEscape="true" />',
                        deleteExemptionConfirm: '<s:message code="childCarerExemption.deleteConfirm" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerExemption/{id}/"/>'
                    }
                },
                deleteChildCarerTraining: {
                    header: {
                        text: '<s:message code="approvals.training.delete.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.training.dialog.delete.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.training.delete.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.training.delete.confirm" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.training.delete.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.training.delete.endDate" javaScriptEscape="true" />',
                        course: '<s:message code="approvals.training.delete.course" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerTraining/{id}/"/>'
                    }
                },
                addAdoptiveApproval: {
                    header: {
                        text: '<s:message code="approvals.details.adoptive.add.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.adoptive.dialog.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.details.add.details.success.message" javaScriptEscape="true" />',
                    labels: {
                        ageRangeMax: '<s:message code="approvals.details.dialog.ageRangeMax" javaScriptEscape="true" />',
                        ageRangeMin: '<s:message code="approvals.details.dialog.ageRangeMin" javaScriptEscape="true" />',
                        disabilityPermitted: '<s:message code="approvals.details.dialog.disabilityPermitted" javaScriptEscape="true" />',
                        genderOfChildren: '<s:message code="approvals.details.dialog.genderPermitted" javaScriptEscape="true" />',
                        preferences: '<s:message code="approvals.details.preferences" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.adoptive.startDate" javaScriptEscape="true" />',
                        organisation: '<s:message code="approvals.details.organisation" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.details.notes" javaScriptEscape="true" />',
                        adoptionType: '<s:message code="approvals.details.dialog.adoptionType" javaScriptEscape="true" />',
                        adoptionTypeInterCountry: '<s:message code="approvals.details.dialog.adoptionTypeInterCountry" javaScriptEscape="true" />',
                        adoptionTypeDomestic: '<s:message code="approvals.details.dialog.adoptionTypeDomestic" javaScriptEscape="true" />'
                    },
                    validationLabels: {
                        maxRelatedPlacements: '<s:message code="approvals.details.dialog.maxRelatedPlacementsErrorMessage" javaScriptEscape="true" />',
                        maxUnrelatedPlacements: '<s:message code="approvals.details.dialog.maxUnrelatedPlacementsErrorMessage" javaScriptEscape="true" />',
                        numberOfRooms: '<s:message code="approvals.details.dialog.numberOfRoomsErrorMessage" javaScriptEscape="true" />',
                        upperAgePermitted: '<s:message code="approvals.details.dialog.upperAgePermittedErrorMessage" javaScriptEscape="true" />',
                        lowerAgePermitted: '<s:message code="approvals.details.dialog.lowerAgePermittedErrorMessage" javaScriptEscape="true" />',
                        disabilitySupport: '<s:message code="approvals.details.dialog.disabilitySupportErrorMessage" javaScriptEscape="true" />',
                        organisationId: '<s:message code="approvals.details.dialog.organisationIdErrorMessage" javaScriptEscape="true" />'
                    },
                    width: 800,
                    config: {
                        url: '<s:url value="/rest/adoptiveCarerApproval/"/>',
                        adopterApprovalContext: '${adopterApprovalContext}',
                        autocomplete: {
                            autocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
                            labels: {
                                placeholder: '<s:message code="approvals.details.dialog.organisationSearchPlaceholder" javaScriptEscape="true"/>'
                            }
                        },
                        autocompleteChild: {
                            autocompleteURL: '<s:url value="/rest/person?nameOrId={query}&personType=client&appendWildcard=true&pageSize={maxResults}"></s:url>',
                            labels: {
                                addChild: '<s:message code="approvals.details.dialog.addChild" javaScriptEscape="true" />',
                                placeholder: '<s:message code="approvals.details.dialog.addChildSearchPlaceholder" javaScriptEscape="true"/>'
                            }
                        }
                    }
                },
                viewAdoptiveApproval: {
                    header: {
                        text: '<s:message code="approvals.details.adoptive.view.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.adoptive.dialog.view.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    labels: {
                        ageRangeMax: '<s:message code="approvals.details.dialog.ageRangeMax" javaScriptEscape="true" />',
                        ageRangeMin: '<s:message code="approvals.details.dialog.ageRangeMin" javaScriptEscape="true" />',
                        disabilityPermitted: '<s:message code="approvals.details.dialog.disabilityPermitted" javaScriptEscape="true" />',
                        genderOfChildren: '<s:message code="approvals.details.dialog.genderPermitted" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.adoptive.startDate" javaScriptEscape="true" />',
                        organisation: '<s:message code="approvals.details.organisation" javaScriptEscape="true" />',
                        childrenPlaced: '<s:message code="approvals.details.childrenPlaced" javaScriptEscape="true" />',
                        organisationAddress: '<s:message code="approvals.details.dialog.organisationAddress" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.details.notes" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        childBeenPlaced: '<s:message code="approvals.details.dialog.childBeenPlaced" javaScriptEscape="true" />',
                        endReason: '<s:message code="approvals.details.dialog.endReason" javaScriptEscape="true" />',
                        adoptionType: '<s:message code="approvals.details.dialog.adoptionType" javaScriptEscape="true" />',
                        domesticAdoptionSubheading: '<s:message code="approvals.details.dialog.domesticAdoptionSubheading" javaScriptEscape="true" />',
                        originallyFosteringChild: '<s:message code="approvals.details.dialog.originallyFosteringChild" javaScriptEscape="true" />',
                        fosteringForAdoption: '<s:message code="approvals.details.dialog.fosteringForAdoption" javaScriptEscape="true" />',
                        childLocalAuthority: '<s:message code="approvals.details.dialog.childLocalAuthority" javaScriptEscape="true" />',
                        interCountryAdoptionSubheading: '<s:message code="approvals.details.dialog.interCountryAdoptionSubheading" javaScriptEscape="true" />',
                        dateMatched: '<s:message code="approvals.details.dialog.dateMatched" javaScriptEscape="true" />',
                        dateChildPlaced: '<s:message code="approvals.details.dialog.dateChildPlaced" javaScriptEscape="true" />',
                        dateOrderGranted: '<s:message code="approvals.details.dialog.dateOrderGranted" javaScriptEscape="true" />',
                        preferences: '<s:message code="approvals.details.preferences" javaScriptEscape="true" />'
                    },
                    width: 800,
                    config: {
                        url: '<s:url value="/rest/childCarerApproval/{id}/"/>',
                        adopterApprovalContext: '${adopterApprovalContext}',
                        addChildLabels: {
                            addChild: '<s:message code="approvals.details.dialog.addChild" javaScriptEscape="true" />'
                        }
                    }
                },
                editAdoptiveApproval: {
                    header: {
                        text: '<s:message code="approvals.details.adoptive.edit.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.adoptive.dialog.edit.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.details.edit.details.success.message" javaScriptEscape="true" />',
                    labels: {
                        ageRangeMax: '<s:message code="approvals.details.dialog.ageRangeMax" javaScriptEscape="true" />',
                        ageRangeMin: '<s:message code="approvals.details.dialog.ageRangeMin" javaScriptEscape="true" />',
                        disabilityPermitted: '<s:message code="approvals.details.dialog.disabilityPermitted" javaScriptEscape="true" />',
                        genderOfChildren: '<s:message code="approvals.details.dialog.genderPermitted" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.adoptive.startDate" javaScriptEscape="true" />',
                        organisation: '<s:message code="approvals.details.organisation" javaScriptEscape="true" />',
                        childrenPlaced: '<s:message code="approvals.details.childrenPlaced" javaScriptEscape="true" />',
                        organisationAddress: '<s:message code="approvals.details.dialog.organisationAddress" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.details.notes" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        childBeenPlaced: '<s:message code="approvals.details.dialog.childBeenPlaced" javaScriptEscape="true" />',
                        endReason: '<s:message code="approvals.details.dialog.endReason" javaScriptEscape="true" />',
                        adoptionType: '<s:message code="approvals.details.dialog.adoptionType" javaScriptEscape="true" />',
                        adoptionTypeInterCountry: '<s:message code="approvals.details.dialog.adoptionTypeInterCountry" javaScriptEscape="true" />',
                        adoptionTypeDomestic: '<s:message code="approvals.details.dialog.adoptionTypeDomestic" javaScriptEscape="true" />',
                        domesticAdoptionSubheading: '<s:message code="approvals.details.dialog.domesticAdoptionSubheading" javaScriptEscape="true" />',
                        originallyFosteringChild: '<s:message code="approvals.details.dialog.originallyFosteringChild" javaScriptEscape="true" />',
                        fosteringForAdoption: '<s:message code="approvals.details.dialog.fosteringForAdoption" javaScriptEscape="true" />',
                        childLocalAuthority: '<s:message code="approvals.details.dialog.childLocalAuthority" javaScriptEscape="true" />',
                        interCountryAdoptionSubheading: '<s:message code="approvals.details.dialog.interCountryAdoptionSubheading" javaScriptEscape="true" />',
                        dateMatched: '<s:message code="approvals.details.dialog.dateMatched" javaScriptEscape="true" />',
                        dateChildPlaced: '<s:message code="approvals.details.dialog.dateChildPlaced" javaScriptEscape="true" />',
                        dateOrderGranted: '<s:message code="approvals.details.dialog.dateOrderGranted" javaScriptEscape="true" />',
                        preferences: '<s:message code="approvals.details.preferences" javaScriptEscape="true" />'
                    },
                    width: 800,
                    config: {
                        url: '<s:url value="/rest/childCarerApproval/{id}/"/>',
                        adopterApprovalContext: '${adopterApprovalContext}',
                        autocomplete: {
                            autocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
                            labels: {
                                placeholder: '<s:message code="approvals.details.dialog.organisationSearchPlaceholder" javaScriptEscape="true"/>'
                            }
                        },
                        autocompleteChild: {
                            autocompleteURL: '<s:url value="/rest/person?nameOrId={query}&personType=client&pageSize={maxResults}"></s:url>',
                            labels: {
                                addChild: '<s:message code="approvals.details.dialog.addChild" javaScriptEscape="true" />',
                                placeholder: '<s:message code="approvals.details.dialog.addChildSearchPlaceholder" javaScriptEscape="true"/>'
                            }
                        }
                    }
                },
                addCarerTraining: {
                    header: {
                        text: '<s:message code="approvals.training.add.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },

                    narrative: {
                        summary: '<s:message code="approvals.training.dialog.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.training.add.details.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.details.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.details.endDate" javaScriptEscape="true" />',
                        applyToPartner: '<s:message code="approvals.details.applyToPartner" javaScriptEscape="true" />',
                        startDatePartner: '<s:message code="approvals.details.startDate.partner" javaScriptEscape="true" />',
                        endDatePartner: '<s:message code="approvals.details.endDate.partner" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerTraining/"/>'
                    }
                },
                addFosterCarerTSDStandards: {
                    header: {
                        text: '<s:message code="approvals.tsdstandards.add.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },

                    narrative: {
                        summary: '<s:message code="approvals.tsdstandards.dialog.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.tsdstandards.add.details.success.message" javaScriptEscape="true" />',
                    labels: {
                        statusDate: '<s:message code="approvals.tsdstandards.statusDate" javaScriptEscape="true" />',
                        statusCode: '<s:message code="approvals.tsdstandards.statusCode" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/fosterCarerTSDStandards/"/>'
                    }
                },
                viewFosterCarerTSDStandards: {
                    header: {
                        text: '<s:message code="approvals.tsdstandards.view.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.tsdstandards.view.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    labels: {
                        statusDate: '<s:message code="approvals.tsdstandards.statusDate" javaScriptEscape="true" />',
                        statusCode: '<s:message code="approvals.tsdstandards.statusCode" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/fosterCarerTSDStandards/{id}/"/>'
                    }
                },
                viewFosterCarerTSDStandardsHistory: {
                    header: {
                        text: '<s:message code="approvals.tsdstandards.viewHistory.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.tsdstandards.viewHistory.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    labels: {
                        statusDate: '<s:message code="approvals.tsdstandards.statusDate" javaScriptEscape="true" />',
                        statusCode: '<s:message code="approvals.tsdstandards.statusCode" javaScriptEscape="true" />'
                    }
                },
                addCarerSuspension: {
                    header: {
                        text: '<s:message code="approvals.availability.suspension.add.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.availability.suspension.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.availability.suspension.add.successMessage" javaScriptEscape="true" />',
                    labels: {
                        endDate: '<s:message code="approvals.availability.suspension.add.endDate" javaScriptEscape="true" />',
                        reason: '<s:message code="approvals.availability.suspension.add.reason" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.availability.suspension.add.startDate" javaScriptEscape="true" />',
                        placesUnavailable: '<s:message code="approvals.availability.suspension.add.placesUnavailable" javaScriptEscape="true" />',
                        reasonMandatory: '<s:message code="approvals.availability.suspension.add.reason.mandatory" javaScriptEscape="true" />',
                        startDateMandatory: '<s:message code="approvals.availability.suspension.add.startDate.mandatory" javaScriptEscape="true" />',
                        placesUnavailableMandatory: '<s:message code="approvals.availability.suspension.add.placesUnavailable.mandatory" javaScriptEscape="true" />',
                        placesUnavailableIsNumeric: '<s:message code="approvals.availability.suspension.add.placesUnavailable.isNumeric" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerSuspension/"/>'
                    }
                },
                viewCarerSuspension: {
                    header: {
                        text: '<s:message code="approvals.availability.suspension.view.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.availability.suspension.view.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription,
                    },
                    labels: {
                        viewCarerDetailsSummary: '<s:message code="approvals.availability.suspension.view.summary" javaScriptEscape="true" />',
                        startDate: '<s:message code="approvals.availability.suspension.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.suspension.endDate" javaScriptEscape="true" />',
                        reason: '<s:message code="approvals.availability.suspension.reason" javaScriptEscape="true" />',
                        reasonDetails: '<s:message code="approvals.availability.suspension.reasonDetails" javaScriptEscape="true" />',
                        placesUnavailable: '<s:message code="approvals.availability.suspension.placesUnavailable" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerSuspension/{id}/"/>'
                    }
                },
                editCarerSuspension: {
                    header: {
                        text: '<s:message code="approvals.availability.suspension.edit.header" javaScriptEscape="true" />',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="approvals.availability.suspension.edit.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.availability.suspension.edit.successMessage" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.availability.suspension.edit.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.suspension.edit.endDate" javaScriptEscape="true" />',
                        reason: '<s:message code="approvals.availability.suspension.edit.reason" javaScriptEscape="true" />',
                        placesUnavailable: '<s:message code="approvals.availability.suspension.edit.placesUnavailable" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerSuspension/{id}/"/>',
                        submitUrl: '<s:url value="/rest/fosterCarerSuspension/{id}/status?action=ended"/>'
                    }
                },
                addCarerUnderInvestigation: {
                    header: {
                        text: '<s:message code="approvals.availability.underInvestigation.add.header" javaScriptEscape="true" />',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="approvals.availability.underInvestigation.add.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    permissions: {
                        canAddCarerSuspension: permissions.canAddCarerSuspension
                    },
                    successMessage: '<s:message code="approvals.availability.underInvestigation.add.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.availability.underInvestigation.add.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.underInvestigation.add.endDate" javaScriptEscape="true" />',
                        allegedAbuse: '<s:message code="approvals.availability.underInvestigation.add.allegedAbuse" javaScriptEscape="true" />',
                        outcome: '<s:message code="approvals.availability.underInvestigation.add.outcome" javaScriptEscape="true" />',
                        monitoringStartDate: '<s:message code="approvals.availability.underInvestigation.add.monitoringStartDate" javaScriptEscape="true" />',
                        monitoringEndDate: '<s:message code="approvals.availability.underInvestigation.add.monitoringEndDate" javaScriptEscape="true" />',
                        allegationMadeBy: '<s:message code="approvals.availability.underInvestigation.add.allegationMadeBy" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.availability.underInvestigation.add.notes" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerSuspension/"/>',
                        fosterChildRelationshipsUrl: '<s:url value="/rest/person/${person.id}/personRelationship?relationshipTypeIdsRoleB=${relationshipType.id}&s=roleBPerson"/>'
                    }
                },
                editCarerInvestigation: {
                    header: {
                        text: '<s:message code="approvals.availability.underInvestigation.update.header" javaScriptEscape="true" />',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="approvals.availability.underInvestigation.update.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.availability.underInvestigation.update.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.availability.underInvestigation.update.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.underInvestigation.update.endDate" javaScriptEscape="true" />',
                        allegedAbuse: '<s:message code="approvals.availability.underInvestigation.update.allegedAbuse" javaScriptEscape="true" />',
                        outcome: '<s:message code="approvals.availability.underInvestigation.update.outcome" javaScriptEscape="true" />',
                        monitoringStartDate: '<s:message code="approvals.availability.underInvestigation.update.monitoringStartDate" javaScriptEscape="true" />',
                        monitoringEndDate: '<s:message code="approvals.availability.underInvestigation.update.monitoringEndDate" javaScriptEscape="true" />',
                        allegationMadeBy: '<s:message code="approvals.availability.underInvestigation.update.allegationMadeBy" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.availability.underInvestigation.update.notes" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerSuspension/{id}"/>',
                        fosterChildRelationshipsUrl: '<s:url value="/rest/person/${person.id}/personRelationship?relationshipTypeIdsRoleB=${relationshipType.id}&s=roleBPerson"/>',
                        submitUrl: '<s:url value="/rest/fosterCarerUnderInvestigation/{id}"/>'
                    }
                },
                endCarerInvestigation: {
                    header: {
                        text: '<s:message code="approvals.availability.underInvestigation.end.header" javaScriptEscape="true" />',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="approvals.availability.underInvestigation.end.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.availability.underInvestigation.end.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.availability.underInvestigation.end.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.underInvestigation.end.endDate" javaScriptEscape="true" />',
                        allegedAbuse: '<s:message code="approvals.availability.underInvestigation.end.allegedAbuse" javaScriptEscape="true" />',
                        outcome: '<s:message code="approvals.availability.underInvestigation.end.outcome" javaScriptEscape="true" />',
                        monitoringStartDate: '<s:message code="approvals.availability.underInvestigation.end.monitoringStartDate" javaScriptEscape="true" />',
                        monitoringEndDate: '<s:message code="approvals.availability.underInvestigation.end.monitoringEndDate" javaScriptEscape="true" />',
                        allegationMadeBy: '<s:message code="approvals.availability.underInvestigation.end.allegationMadeBy" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.availability.underInvestigation.end.notes" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerSuspension/{id}"/>',
                        fosterChildRelationshipsUrl: '<s:url value="/rest/person/${person.id}/personRelationship?relationshipTypeIdsRoleB=${relationshipType.id}&s=roleBPerson"/>',
                        submitUrl: '<s:url value="/rest/fosterCarerUnderInvestigation/{id}/status?action=ended"/>'
                    }
                },
                viewCarerInvestigation: {
                    header: {
                        text: '<s:message code="approvals.availability.underInvestigation.view.header" javaScriptEscape="true" />',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="approvals.availability.underInvestigation.view.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.availability.underInvestigation.view.success.message" javaScriptEscape="true" />',
                    labels: {
                        startDate: '<s:message code="approvals.availability.underInvestigation.view.startDate" javaScriptEscape="true" />',
                        endDate: '<s:message code="approvals.availability.underInvestigation.view.endDate" javaScriptEscape="true" />',
                        allegedAbuse: '<s:message code="approvals.availability.underInvestigation.view.allegedAbuse" javaScriptEscape="true" />',
                        outcome: '<s:message code="approvals.availability.underInvestigation.view.outcome" javaScriptEscape="true" />',
                        monitoringStartDate: '<s:message code="approvals.availability.underInvestigation.view.monitoringStartDate" javaScriptEscape="true" />',
                        monitoringEndDate: '<s:message code="approvals.availability.underInvestigation.view.monitoringEndDate" javaScriptEscape="true" />',
                        allegationMadeBy: '<s:message code="approvals.availability.underInvestigation.view.allegationMadeBy" javaScriptEscape="true" />',
                        notes: '<s:message code="approvals.availability.underInvestigation.view.notes" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerSuspension/{id}"/>',
                        fosterChildRelationshipsUrl: '<s:url value="/rest/person/${person.id}/personRelationship?relationshipTypeIdsRoleB=${relationshipType.id}&s=roleBPerson"/>'
                    }
                },
                updateFosterCarerApprovalData: {
                    header: {
                        text: '<s:message code="approvals.details.foster.approval.editData.header" javaScriptEscape="true" />',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.details.foster.approval.editData.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.details.foster.approval.editData.success.message" javaScriptEscape="true" />',
                    labels: {
                        lowerAgePermitted: '<s:message code="updateDataFosterCarerApprovalVO.lowerAgePermitted" javaScriptEscape="true" />',
                        upperAgePermitted: '<s:message code="updateDataFosterCarerApprovalVO.upperAgePermitted" javaScriptEscape="true" />',
                        maxRelatedPlacements: '<s:message code="updateDataFosterCarerApprovalVO.maxRelatedPlacements" javaScriptEscape="true" />',
                        maxUnrelatedPlacements: '<s:message code="updateDataFosterCarerApprovalVO.maxUnrelatedPlacements" javaScriptEscape="true" />',
                        genderPermitted: '<s:message code="updateDataFosterCarerApprovalVO.genderPermitted" javaScriptEscape="true" />',
                        disabilityPermitted: '<s:message code="updateDataFosterCarerApprovalVO.disabilityPermitted" javaScriptEscape="true" />',
                        typeOfDisability: '<s:message code="approvals.details.typeOfDisability" javaScriptEscape="true" />'
                    },
                    validationLabels: {
                        lowerAgePermittedMandatory: '<s:message code="approvals.details.foster.approval.editData.lowerAgePermitted.mandatory" javaScriptEscape="true" />',
                        lowerAgePermittedIsNumeric: '<s:message code="approvals.details.foster.approval.editData.lowerAgePermitted.isNumeric" javaScriptEscape="true" />',
                        upperAgePermittedMandatory: '<s:message code="approvals.details.foster.approval.editData.upperAgePermitted.mandatory" javaScriptEscape="true" />',
                        upperAgePermittedIsNumeric: '<s:message code="approvals.details.foster.approval.editData.upperAgePermitted.isNumeric" javaScriptEscape="true" />',
                        maxRelatedPlacementsMandatory: '<s:message code="approvals.details.foster.approval.editData.maxRelatedPlacements.mandatory" javaScriptEscape="true" />',
                        maxRelatedPlacementsIsNumeric: '<s:message code="approvals.details.foster.approval.editData.maxRelatedPlacements.isNumeric" javaScriptEscape="true" />',
                        maxUnrelatedPlacementsMandatory: '<s:message code="approvals.details.foster.approval.editData.maxUnrelatedPlacements.mandatory" javaScriptEscape="true" />',
                        maxUnrelatedPlacementsIsNumeric: '<s:message code="approvals.details.foster.approval.editData.maxUnrelatedPlacements.isNumeric" javaScriptEscape="true" />'
                    },
                    config: {
                        url: '<s:url value="/rest/childCarerApproval/{id}/"/>',
                        submitUrl: '<s:url value="/rest/fosterCarerApproval/{id}/"/>',
                    }
                },
            }
        };

        var removeFosterCarerApprovalDialogConfig = {
            views: {
                prepareRemove: {
                    header: {
                        text: '<s:message code="approvals.foster.remove.prepare.dialog.header" javaScriptEscape="true"/>',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.foster.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.foster.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.foster.remove.prepare.dialog.removalConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        prepareUrl: '<s:url value="/rest/fosterCarerApproval/{id}/deletionRequest" />'
                    }
                },
                remove: {
                    header: {
                        text: '<s:message code="approvals.foster.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.foster.remove.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.foster.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.foster.remove.dialog.removalConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        deleteUrl: '<s:url value="/rest/fosterCarerApproval?deletionRequestId={deletionRequestId}" />',
                        resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
                    }
                },
                removeInProgress: {
                    header: {
                        text: '<s:message code="approvals.foster.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: 'fa fa-wrench'
                    },
                    labels: {
                        message: '<s:message code="approvals.foster.remove.dialog.inProgress" javaScriptEscape="true" />'
                    }
                }
            }
        };

        var removeRegistrationDialogConfig = {
            views: {
                prepareRemove: {
                    header: {
                        text: '<s:message code="approvals.foster.registration.remove.prepare.dialog.header" javaScriptEscape="true"/>',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.foster.registration.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.foster.registration.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.foster.registration.remove.prepare.dialog.removalConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        prepareUrl: '<s:url value="/rest/fosterCarerRegistration/{id}/deletionRequest" />'
                    }
                },
                remove: {
                    header: {
                        text: '<s:message code="approvals.foster.registration.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.foster.registration.remove.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.foster.registration.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.foster.registration.remove.dialog.removalConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        deleteUrl: '<s:url value="/rest/fosterCarerRegistration?deletionRequestId={deletionRequestId}" />',
                        resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
                    }
                },
                removeInProgress: {
                    header: {
                        text: '<s:message code="approvals.foster.registration.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: 'fa fa-wrench'
                    },
                    labels: {
                        message: '<s:message code="approvals.foster.remove.dialog.inProgress" javaScriptEscape="true" />'
                    }
                }
            }
        };

        var removeAdopterApprovalDialogConfig = {
            views: {
                prepareRemove: {
                    header: {
                        text: '<s:message code="approvals.adoptive.remove.prepare.dialog.header" javaScriptEscape="true"/>',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.adoptive.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.adoptive.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.adoptive.remove.prepare.dialog.removalConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        prepareUrl: '<s:url value="/rest/adoptiveCarerApproval/{id}/deletionRequest" />'
                    }
                },
                remove: {
                    header: {
                        text: '<s:message code="approvals.adoptive.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: caIcon
                    },
                    narrative: {
                        summary: '<s:message code="approvals.adoptive.remove.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.adoptive.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="approvals.adoptive.remove.dialog.removalConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        deleteUrl: '<s:url value="/rest/adoptiveCarerApproval?deletionRequestId={deletionRequestId}" />',
                        resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
                    }
                },
                removeInProgress: {
                    header: {
                        text: '<s:message code="approvals.adoptive.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: 'fa fa-wrench'
                    },
                    labels: {
                        message: '<s:message code="approvals.adoptive.remove.dialog.inProgress" javaScriptEscape="true" />'
                    }
                }
            }
        };

        var removeConfig = {
            removeFosterCarerApprovalConfig: {
                type: 'fosterCarerApproval',
                subject: subject,
                dialogConfig: removeFosterCarerApprovalDialogConfig
            },
            removeRegistrationConfig: {
                type: 'fosterCarerRegistration',
                subject: subject,
                dialogConfig: removeRegistrationDialogConfig
            },
            removeAdoptiveCarerApprovalConfig: {
                type: 'adoptiveCarerApproval',
                subject: subject,
                dialogConfig: removeAdopterApprovalDialogConfig
            }
        };

        var config = {
            container: '#approvalTabs',
            root: '<s:url value="/approvals/person/${currentSubjectId}/"/>',
            permissions: permissions,
            subject: subject,
            tabs: {
                approvals: {
                    label: '<s:message code="approvals.approvals.title" javaScriptEscape="true"/>',
                    config: approvalsConfig
                },
                adoptiveCarerApproval: {
                    label: '<s:message code="approvals.adoptive.carer.approval.title" javaScriptEscape="true"/>',
                    config: adoptiveCarerApprovalConfig
                },
                household: {
                    label: '<s:message code="approvals.household.title" javaScriptEscape="true"/>',
                    config: householdConfig
                },
                training: {
                    label: '<s:message code="approvals.training.title" javaScriptEscape="true"/>',
                    config: carerTrainingConfig
                },
                availability: {
                    label: '<s:message code="approvals.availability.title" javaScriptEscape="true"/>',
                    config: availabilityConfig
                }
            },
            dialogConfig: dialogConfig,
            removeConfig: removeConfig,
            carerTrainingConfig: carerTrainingConfig,
            personDetailsURL: '<s:url value="/rest/person/{id}"/>',
            currentFosterCarerRegistrationUrl: '<s:url value="/rest/person/{id}/currentFosterCarerRegistration"/>',
            changeContextURL: '<s:url value="/person?id={id}" />'
        };

        var approvalsView = new Y.app.approvals.ApprovalsControllerView(config).render();
    });
</script>