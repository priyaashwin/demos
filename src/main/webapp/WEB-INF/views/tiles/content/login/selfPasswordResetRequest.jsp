<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script>
Y.use('yui-base',
	'placeholder',
	'info-pop',
	'info-message',
function(Y){
	// Show pop up messages
	new Y.usp.InfoPop({
		selector:'.pop-up-data',
		zIndex:9999999
	}).render();
	
	new Y.usp.Placeholder({
		form: '#passwordResetRequestVO'
	});
	
	var infoMessage = new Y.usp.InfoMessage({
		srcNode:'#headerMessage'
	});
	// in case the user was redirected back to this page because they did not successfully reset their password
	//infoMessage.reset();
	
	infoMessage.render();
	
	/* Y.fire('infoMessage:message',{
		message:'<s:message code="user.selfPasswordReset.successMessage" javaScriptEscape="true" />',
		delayed:true
	}); */
});
</script>


<form:form modelAttribute="passwordResetRequestVO" method="POST" class="pure-form pure-form-stacked pure-form-block usp-curves-sml">
	<h1>Eclipse</h1> 
	<h2><s:message code="user.selfPasswordReset.title"/></h2>
	<label for="userName" class="hiddenLabel"><s:message code="passwordResetRequestVO.userName"/></label>
	<input type="text" id="userName" name="userName" placeholder="<s:message code="passwordResetRequestVO.userName"/>" />
	
	<label for="emailAddress" class="hiddenLabel"><s:message code="passwordResetRequestVO.emailAddress"/></label>
	<input type="text" id="emailAddress" name="emailAddress" placeholder="<s:message code="passwordResetRequestVO.emailAddress"/>" />
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
	
	<button type="submit" class="pure-button pure-button-primary pure-button-login"><s:message code="user.selfPasswordReset.saveButtonText"/></button>
</form:form>