<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script>
Y.use('yui-base',
	'info-message',
function(Y){
	var infoMessage = new Y.usp.InfoMessage({
		srcNode:'#headerMessage'
	});
	// in case the user was redirected back to this page because their last submission was unsuccessful
	infoMessage.reset();

	infoMessage.render();
});
</script>


<form:form modelAttribute="securityChallengeVerificationVO" method="POST" class="securityChallengeVerificationVO pure-form pure-form-stacked usp-curves-sml">
	<h1>Eclipse</h1>
	<h2><s:message code="authentication.secondaryPassword.validation.title"/></h2>
	<div class="pure-g-r validateSecondaryPasswordInputs" >
		<label for="firstReqChar" class="sr-only">Enter character ${requiredChar_1}</label>
		<input class="pure-u-1-6 l-box" type="text" id="firstReqChar" name="challengeEntry['${requiredChar_1}']" maxlength="1" placeholder="${requiredChar_1}" />
		<label for="secondReqChar" class="sr-only">Enter character ${requiredChar_2}</label>
		<input class="pure-u-1-6 l-box" type="text" id="secondReqChar" name="challengeEntry['${requiredChar_2}']" maxlength="1" placeholder="${requiredChar_2}" />
		<label for="thirdReqChar" class="sr-only">Enter character ${requiredChar_3}</label>
		<input class="pure-u-1-6 l-box" type="text" id="thirdReqChar" name="challengeEntry['${requiredChar_3}']" maxlength="1" placeholder="${requiredChar_3}" />
	</div>
        <c:if test="${_csrf}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>

	<button type="submit" class="pure-button pure-button-primary pure-button-login validateSecondaryPasswordSubmit"><s:message code="authentication.secondaryPassword.validation.saveButton"/></button>
</form:form>
<form action='<s:url value="/relationshipsrecording_logout"/>' method='post' id="backToMainLoginForm" class="backToMainLoginForm">
        <c:if test="${_csrf}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
	<button type='submit' class='btn backToMainLoginFormSubmit pure-button' title='<s:message code="user.login.generic.backToMainLogin" />'><s:message code="user.login.generic.backToMainLogin" /></button>
</form>
