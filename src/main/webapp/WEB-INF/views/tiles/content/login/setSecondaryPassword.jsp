<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script>
Y.use('yui-base',
	'placeholder',
	'info-pop',
	'info-message',
function(Y){
	// Show pop up messages
	new Y.usp.InfoPop({
		selector:'.pop-up-data',
		zIndex:9999999
	}).render();
	
	new Y.usp.Placeholder({
		form: '#secondaryPasswordVO'
	});
	
	var infoMessage = new Y.usp.InfoMessage({
		srcNode:'#headerMessage'
	});
	// in case the user was redirected back to this page because their last submission was unsuccessful
	infoMessage.reset();
	
	infoMessage.render();
	
	Y.fire('infoMessage:message',{
		message:'<s:message code="authentication.secondaryPassword.configure.successMessage" javaScriptEscape="true" />',
		delayed:true
	});
});
</script>


<form:form modelAttribute="secondaryPasswordVO" method="POST" class="secondaryPasswordVO pure-form pure-form-stacked pure-form-block usp-curves-sml">
	<h1>Eclipse</h1> 
	<h2><s:message code="authentication.secondaryPassword.configure.title"/></h2>
	<a href="#" class="pop-up-data" 
		data-title="<s:message code="authentication.secondaryPassword.configure.help.title"/>" 
		data-content="<s:message code="authentication.secondaryPassword.configure.help.content" argumentSeparator=",,"
			arguments="${passwordPolicy.minChars},,${passwordPolicy.minUppercaseChars},,${passwordPolicy.minDigits},,${passwordPolicy.minNonAlphanumericChars}"/>">Help</a>

	<label for="password" class="hiddenLabel"><s:message code="secondaryPasswordVO.password"/></label>
	<input type="password" id="password" name="password" placeholder="<s:message code="secondaryPasswordVO.password"/>" />
	
	<label for="confirmPassword" class="hiddenLabel"><s:message code="secondaryPasswordVO.confirmPassword"/></label>
	<input type="password" id="confirmPassword" name="confirmPassword" placeholder="<s:message code="secondaryPasswordVO.confirmPassword"/>" />
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
	
	<button type="submit" class="pure-button pure-button-primary pure-button-login"><s:message code="authentication.secondaryPassword.configure.saveButton"/></button>
</form:form>
<form action='<s:url value="/relationshipsrecording_logout"/>' method='post' id="backToMainLoginForm" class="backToMainLoginForm">
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
	<button type='submit' class='btn backToMainLoginFormSubmit pure-button' title='<s:message code="user.login.generic.backToMainLogin" />'><s:message code="user.login.generic.backToMainLogin" /></button>
</form>