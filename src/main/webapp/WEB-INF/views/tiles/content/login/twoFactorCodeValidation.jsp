<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script>
Y.use('yui-base',
	'placeholder',
	'info-pop',
	'info-message',
function(Y){
	// Show pop up messages
	new Y.usp.InfoPop({
		selector:'.pop-up-data',
		zIndex:9999999
	}).render();
	
	new Y.usp.Placeholder({
		form: '#twoFactorSmsVerificationVO'
	});
	
	var infoMessage = new Y.usp.InfoMessage({
		srcNode:'#headerMessage'
	});
	// in case the user was redirected back to this page because they did not successfully reset their password
	infoMessage.reset();
	
	infoMessage.render();
	
	Y.fire('infoMessage:message',{
		message:'<s:message code="user.twoFactorCodeValidation.successMessage" javaScriptEscape="true" />',
		delayed:true
	});

	Y.one('#twoFactorSmsVerificationVO').on('submit',function(){
        var twoFactorTokenValue = Y.one('#twoFactorToken').get('value');
        twoFactorTokenValue = twoFactorTokenValue.toUpperCase();
        Y.one('#twoFactorToken').set('value',twoFactorTokenValue);
 });

});
</script>


<form:form modelAttribute="twoFactorSmsVerificationVO" method="POST" class="twoFactorSmsVerificationVO pure-form pure-form-stacked pure-form-block usp-curves-sml">
	<h1>Eclipse</h1> 
	<h2><s:message code="user.twoFactorCodeValidation.title"/></h2>
	
	<input type="text" id="twoFactorToken" name="token" autofocus autocomplete=off />
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
	
	<button type="submit" class="pure-button pure-button-primary pure-button-login"><s:message code="user.twoFactorCodeValidation.saveButtonText"/></button>
	<a class="requestNewCode" href="<c:url value='/login/twoFactorSms/newToken' />"><span><s:message code="user.twoFactorCodeValidation.requestNewCode"/></span></a>
</form:form>
<form action='<s:url value="/relationshipsrecording_logout"/>' method='post' id="backToMainLoginForm" class="backToMainLoginForm">
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
	<button type='submit' class='btn backToMainLoginFormSubmit pure-button' title='<s:message code="user.login.generic.backToMainLogin" />'><s:message code="user.login.generic.backToMainLogin" /></button>
</form>