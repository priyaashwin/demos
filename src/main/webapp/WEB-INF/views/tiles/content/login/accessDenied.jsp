<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="pure-u-1" id="main">
	<form id="login-form" class="pure-form pure-form-block" action="<c:url value='/relationshipsrecording_logout' />" method="post">
		<h1>Eclipse</h1>
		<p class="dark-panel">Please go to the homepage, or log out.</p>	
		<button type="button" class="pure-button pure-button-active pure-button-login" onclick="javascript:location.href = '<c:url value="/index"/>';">Home</button>
		<button type="submit" class="pure-button pure-button-primary pure-button-login">Log out</button>
	</form>
</div>