<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script>
//TODO: Replace with YUI-based equivalent
window.onload = function () {
	  //retain profile ID in sessionStorage for session re-establishment
	  document.getElementById("selectedProfile").addEventListener("submit",function(e){
	  	var selectedItem=document.getElementById("profileId").selectedOptions[0];
	  	if(selectedItem){
		  	 sessionStorage.setItem("profileId",selectedItem.value);
 		  	 sessionStorage.setItem("profileName",selectedItem.text);
	  	}
		 
	  });
}
</script>

<div id="headerMessage"></div>

<script>
Y.use('info-message',
function(Y){
	new Y.usp.InfoMessage({
		srcNode:'#headerMessage'
	}).render();
});
</script>

<form:form modelAttribute="selectedProfile" method="POST" class="selectedProfile pure-form pure-form-stacked pure-form-block usp-curves-sml">
	<h1>Eclipse</h1> 
    <h2>Select profile</h2>
    <form:select path="profileId" items="${assignedProfiles}" />
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
    <button type="submit" class="pure-button pure-button-primary pure-button-login">Select</button>     
</form:form>
<form action='<s:url value="/relationshipsrecording_logout"/>' method='post' id="backToMainLoginForm" class="backToMainLoginForm">
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
	<button type='submit' class='btn backToMainLoginFormSubmit pure-button' title='<s:message code="user.login.generic.backToMainLogin" />'><s:message code="user.login.generic.backToMainLogin" /></button>
</form>
