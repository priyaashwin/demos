<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="headerMessage"></div>

<script>
Y.use('info-message',
function(Y){
  localStorage.removeItem('uspNotifications');
	new Y.usp.InfoMessage({
		srcNode:'#headerMessage'
	}).render();
	
	if(Y.one('#termsAndConditionsEnabled')){
		Y.on('click',function(){
			if(Y.one('#termsAndConditionsEnabled').get('checked')){
				Y.one('#loginbutton').removeAttribute('disabled').removeAttribute('readonly');
			}else{
				Y.one('#loginbutton').set('disabled','disabled').setAttribute('readonly','readonly');
			}
		});
	}
});




</script>


<script>

function initOverLabels () {
	
	  if (!document.getElementById) return;      

	  var labels, id, field;

	  // Set focus and blur handlers to hide and show 
	  // labels with 'overlabel' class names.
	  labels = document.getElementsByTagName('label');
	  for (var i = 0; i < labels.length; i++) {

	    if (labels[i].className == 'overlabel') {

	      // Skip labels that do not have a named association
	      // with another field.
	      id = labels[i].htmlFor || labels[i].getAttribute ('for');
	      if (!id || !(field = document.getElementById(id))) {
	        continue;
	      } 

	      // Change the applied class to hover the label 
	      // over the form field.
	      labels[i].className = 'overlabel-apply';

	      // Hide any fields having an initial value.
	      if (field.value !== '') {
	        hideLabel(field.getAttribute('id'), true);
	      }

	      // Set handlers to show and hide labels.
	      field.onfocus = function () {
	        hideLabel(this.getAttribute('id'), true);
	        checkAndHide();
	      };
	      
	      /* 	IE10 - If user selects username from autocomplete dropdown
	      		this event is fired and we can hide labels
	      */ 
	      field.onpropertychange = function () {
	    	  checkAndHide();
	      };
			
	      /* Same as above, but for Firefox */			
	      field.oninput = function () {
	    	  setTimeout(function() {
			   		checkAndHide();
					},50);  
	      };
			
		
	      field.onblur = function () {
		    	// fix to stop IE10 chopping bits of the password off
		    	// when the password control loses focus.  Seems to be
		    	// an IE 10 bug, similar to the one described here:
		    	// http://stackoverflow.com/questions/19291044/ie-10-cuts-off-text-in-text-box-after-100-px
	    	
		    	if (this.getAttribute('id')==='password'){
		    		this.value=this.value;	
		      }
	    	
	        if (this.value === '') {
	          hideLabel(this.getAttribute('id'), false);
	        }
	        
	      };
	      
	      // Handle clicks to label elements (for Safari).
	      labels[i].onclick = function () {
	        var id, field;
	        id = this.getAttribute('for');
	        if (id && (field = document.getElementById(id))) {
	          field.focus();
	        }
	      };

	    }
	  }
		
	  /* 	fix to stop IE10 chopping bits of the password off
  		 	when the login button is clicked.  Caused by a bug 
  		 	in IE10 on the ms-reveal button.  This and the fix
  		 	further up can also be fixed in app.css with:
  				input::-ms-reveal {
  					display: none;
  				}
  			But then we lose the reveal button.
	  	*/
		document.getElementById('loginbutton').onmousedown=function (e) {
			e.preventDefault();
		};
  			
		var isChromeAutofill=function(elem){
		  return getComputedStyle(elem).backgroundColor==='rgb(250, 255, 189)';
		}
		/*
		  Fix to hide password label in Chrome if username has a value.
			Ideally, we would check if password field has a value, but
			on logging out of the application and returning to the login 
			screen, before the user interacts
			with the form, the password value is always '' even when
			Chrome has autofilled a password.  Here we are going to assume
			that if there is a username autofilled on load, there will also
			be a password autofilled.
		*/
		var username=document.getElementById('username');
		if (username.value.length||isChromeAutofill(username)) {
			hideLabel('username',true);
			hideLabel('password',true);
		}
		
	};
	
	function hideLabel(field_id, hide) {
	  var field_for;
	  var labels = document.getElementsByTagName('label');
	  for (var i = 0; i < labels.length; i++) {
	    field_for = labels[i].htmlFor || labels[i].getAttribute('for');
	    if (field_for == field_id) {
	      labels[i].style.textIndent = (hide) ? '-10000px' : '0px';
	      return true;
	    }
	  }
	}
	
	function checkAndHide() {
		if (document.getElementById('password').value!==''){
			hideLabel('password', true);
		}
		if (document.getElementById('username').value!==''){
			hideLabel('username', true);
		}
	}

	window.onload = function () {
		
		var hasLocalStorage, hasSessionStorage, isSupported,
		
		LOG_NO_STORAGE = "DOM Storage not enabled for this browser which prevents SessionStorage and LocalStorage working.  Please ensure DOM storage is enabled.",
		
		MSG_NO_STORAGE = "<p>We can't run Eclipse in your browser at the moment as certain features need to be enabled (LocalStorage)</p><p>Please contact your support desk for assistance - we've logged the issues for them in the browser console.</p>",
		
		MSG_NOT_SUPPORTED = "<p>We can't run Eclipse in your browser as it isn't supported</p><p>Please contact your support desk for assistance</p>";
		
	
		// Feature test localStorage...
		hasLocalStorage=(function() {
			try {
				localStorage.setItem("localStorageTest", "test");
				localStorage.removeItem("localStorageTest");
				return true;
			} catch(e) {
				return false;
			}  
		})();
		
		// ...and sessionStrorage...
		// See https://mathiasbynens.be/notes/localstorage-pattern
		// and https://github.com/Modernizr/Modernizr/blob/c56fb8b09515f629806ca44742932902ac145302/modernizr.js#L696-731
		hasSessionStorage=(function() {
			try {
				sessionStorage.setItem("sessionStorageTest", "test");
				sessionStorage.removeItem("sessionStorageTest");
				return true;
			} catch(e) {
				return false;
			}	
		})();
		
		// ...and finish with some rudimentary UA sniffing to ward off IE8 and below
		isSupported=(function() {		
			var ua=navigator.userAgent;
			if(ua.indexOf('MSIE')!==-1 && Y.UA.ie < 9) {
				return false;
			}
			return true;
		})();
		
		
		if(!isSupported) {
			document.getElementById('msg-feeedback').innerHTML = MSG_NOT_SUPPORTED;
			document.getElementById('feature-message').style.display = 'block';
			document.getElementById('login-fields').style.display = 'none';
		} else if(!hasLocalStorage || !hasSessionStorage) {
			document.getElementById('msg-feeedback').innerHTML = MSG_NO_STORAGE;	
			document.getElementById('feature-message').style.display = 'block';
			document.getElementById('login-fields').style.display = 'none';
		  if(localStorage && console) {
			  console.log(LOG_NO_STORAGE);
		  }
		} else {
			setTimeout(initOverLabels, 50);
			
			//clear all session storage
			sessionStorage.clear();
		}
		
	};
</script>

<form id="login-form" action="<c:url value='/relationshipsrecording_login' />" method="post" class="login-pure-form pure-form pure-form-stacked pure-form-block usp-curves-sml">
	<h1>
		Eclipse
	</h1>
	
	<div id="feature-message" class="pure-alert pure-alert-block pure-alert-info" style="display:none">
    <h4>Browser Issue Detected</h4>
    <span id="msg-feeedback"></span>
  </div>
  
	<fieldset id="login-fields">	   
		<label for="username" class="overlabel">Username</label>
		<input id="username" type="text" name="j_username" class="pure-input-1-3">
		<label for="password" class="overlabel">Password</label>   
		<input id="password" type="password" name="j_password" class="pure-input-1-3">
		<c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</c:if>
		<c:choose>
			<c:when test="${termsAndConditionsEnabled eq true}">
				<label for="termsAndConditionsEnabled" class="pure-checkbox termsAndConditions">
					<input id="termsAndConditionsEnabled" type="checkbox" name="termsAndConditionsEnabled" class="termsAndConditionsEnabled"></input>
					<span><s:message code="user.loginPage.iAgree"/> <a <c:choose><c:when test="${termsAndConditionsUrl != null}"> href="${termsAndConditionsUrl}"  target="_blank"</c:when><c:otherwise>href="#"</c:otherwise></c:choose> class="termsOfUse"><s:message code="user.loginPage.terms"/></a></span>
				</label>
				
			</c:when>
		</c:choose>
		<button type="submit" id="loginbutton" class="pure-button pure-button-primary pure-button-login" <c:if test="${termsAndConditionsEnabled eq true}">disabled="disabled" readonly="readonly" </c:if> >Log in</button>
	</fieldset>
	<c:choose>
		<c:when test="${forgotPasswordEnabled eq true}">
			<a class="forgottenPassword" href="<c:url value='/login/forgottenPassword' />"><span><s:message code="user.loginPage.forgottenPassword"/></span></a>
		</c:when>
	</c:choose>
</form>