<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>

<sec:authorize access="hasPermission(null, 'SecurityGroupOfRights','SecurityGroupOfRights.GET')" var="canView" />
<sec:authorize access="hasPermission(null, 'SecurityGroupOfRights','SecurityGroupOfRights.UPDATE')" var="canUpdate" />

<div id="groupOfRightsResults"></div>
 
<script>
Y.use('yui-base',
	'event-delegate',
	'event-custom-base',
	'groupofrights-search-view',
	'groupofrights-dialog-views',
function(Y){
	var canView = '${canView}'==='true',
		canUpdate = '${canUpdate}'==='true',
		groupOfRightsSearch=new Y.app.GroupOfRightsSearchView({
		container:'#groupOfRightsResults',
		searchConfig:{
			title:'<s:message code="groupOfRights.find.results" javaScriptEscape="true"/>',
			labels:{
				code:'<s:message code="groupOfRights.find.results.code" javaScriptEscape="true"/>',
				name:'<s:message code="groupOfRights.find.results.name" javaScriptEscape="true"/>',
				description:'<s:message code="groupOfRights.find.results.description" javaScriptEscape="true"/>',
				rights:'<s:message code="groupOfRights.find.results.rights" javaScriptEscape="true"/>',
				active:'<s:message code="groupOfRights.find.results.active" javaScriptEscape="true"/>',
				viewGroupOfRights:'<s:message code="groupOfRights.find.results.view" javaScriptEscape="true"/>',
				editGroupOfRights:'<s:message code="groupOfRights.find.results.edit" javaScriptEscape="true"/>',
				actions:'<s:message code="groupOfRights.find.results.actions" javaScriptEscape="true"/>'
			},
			resultsCountNode:Y.one('#groupOfRightsResultsCount'),
			noDataMessage:'<s:message code="groupOfRights.find.no.results" javaScriptEscape="true"/>',
			canView: canView,
			canUpdate: canUpdate,
			url:'<s:url value="/rest/securityGroupOfRights?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
		},
		addConfig:{
			labels:{
				code:'<s:message code="newSecurityGroupOfRightsVO.code" javaScriptEscape="true"/>',
				name:'<s:message code="newSecurityGroupOfRightsVO.name" javaScriptEscape="true"/>',
				description:'<s:message code="newSecurityGroupOfRightsVO.description" javaScriptEscape="true"/>',
				rights:'<s:message code="newSecurityGroupOfRightsVO.rightIds" javaScriptEscape="true"/>',
				active:'<s:message code="newSecurityGroupOfRightsVO.active" javaScriptEscape="true"/>',		
				addGroupOfRightsSuccessMessage:'<s:message code="groupOfRights.add.success.message" javaScriptEscape="true"/>',
				addGroupOfRightsSummary:'<s:message code="securityGroupOfRights.add.summary" javaScriptEscape="true" />'		
			},
			headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
				class="fa fa-circle fa-stack-1x"></i><i \
				class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> \
				<s:message code="groupOfRights.dialog.add.header" javaScriptEscape="true"/></h3>',
			rightsURL:'<s:url value="/rest/securityRight?active=true&s=name&pageNumber=-1&pageSize=-1"/>',
			gorURL:'<s:url value="/rest/securityGroupOfRights"/>' 
		},
		editConfig:{
			labels:{
				code:'<s:message code="updateSecurityGroupOfRightsWithRightsVO.code" javaScriptEscape="true"/>',
				name:'<s:message code="updateSecurityGroupOfRightsWithRightsVO.name" javaScriptEscape="true"/>',
				description:'<s:message code="updateSecurityGroupOfRightsWithRightsVO.description" javaScriptEscape="true"/>',
				rights:'<s:message code="updateSecurityGroupOfRightsWithRightsVO.rights" javaScriptEscape="true"/>',
				active:'<s:message code="updateSecurityGroupOfRightsWithRightsVO.active" javaScriptEscape="true"/>',
				editGroupOfRightsSummary:'<s:message code="groupOfRights.edit.summary" javaScriptEscape="true" />',
				editGroupOfRightsSuccessMessage:'<s:message code="groupOfRights.edit.success.message" javaScriptEscape="true"/>'
			},
			headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
				class="fa fa-circle fa-stack-1x"></i><i \
				class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> \
				<s:message code="groupOfRights.dialog.edit.header"/></h3>',
			groupOfrightsURL:'<s:url value="/rest/securityGroupOfRights/{id}"/>',
			updateURL:'<s:url value="/rest/securityGroupOfRights/{id}?updateAssociatedRights=true"/>',
			rightsURL:'<s:url value="/rest/securityRight?active=true&s=name&pageNumber=-1&pageSize=-1"/>'
		}
	});
	 
	//render the results
	groupOfRightsSearch.render();
});
</script>	