<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>

<sec:authorize access="hasPermission(null, 'SecurityUser','SecurityUser.GET')" var="canView"/>
<sec:authorize access="hasPermission(null, 'SecurityUser','SecurityUser.UPDATE')" var="canUpdate"/>
<sec:authorize access="hasPermission(null,'ProfessionalPersonTeamRelationship','ProfessionalPersonTeamRelationship.ADD')" var="canAdd"/>

<sec:authorize access="hasPermission(null,'SecurityDomain','SecurityDomain.UPDATE')" var="canUpdateSecurityDomain"/>
<sec:authorize access="hasPermission(null,'SecurityDomain','SecurityDomain.ADD')" var="canAddSecurityDomain"/>
<sec:authorize access="hasPermission(null,'SecurityDomain','SecurityDomain.DELETE')" var="canRemoveSecurityDomain"/>

<sec:authorize access="hasPermission(null,'SecurityRecordTypeConfiguration','SecurityRecordTypeConfiguration.ADD')" var="canAddSecurityProfile"/>
<sec:authorize access="hasPermission(null,'SecurityRecordTypeConfiguration','SecurityRecordTypeConfiguration.GET')" var="canViewSecurityProfile"/>
<sec:authorize access="hasPermission(null,'SecurityRecordTypeConfiguration','SecurityRecordTypeConfiguration.UPDATE')" var="canEditSecurityProfile"/>
<sec:authorize access="hasPermission(null,'SecurityRecordTypeConfiguration','SecurityRecordTypeConfiguration.DELETE')" var="canRemoveSecurityProfile"/>
<sec:authorize access="hasPermission(null,'SecurityRecordTypeConfiguration','SecurityRecordTypeConfiguration.COPY')" var="canDuplicateSecurityProfile"/>

<div id="securityAdminTabs" class="yui3-widget"></div>

<script>
Y.use('team-controller-view', function(Y) {
  var permissions = {
      canAddTeam: '${canAdd}' === 'true',
      canView: '${canView}' === 'true',
      canViewOrganisation: '${canView}' === 'true',
      canUpdate: '${canUpdate}' === 'true',
      canAddSecurityDomain: '${canAddSecurityDomain}'==='true',
      canEditSecurityDomain: '${canUpdateSecurityDomain}'==='true',
      canFillSecurityDomain:'${canUpdateSecurityDomain}'==='true',
      canRemoveSecurityDomain: '${canRemoveSecurityDomain}'==='true',
      canAddSecurityProfile: '${canAddSecurityProfile}'==='true',
      canEditSecurityProfile: '${canEditSecurityProfile}'==='true',
      canDuplicateSecurityProfile: '${canDuplicateSecurityProfile}'==='true',
      canRemoveSecurityProfile: '${canRemoveSecurityProfile}' === 'true',
      canViewAccessLevels:'${canViewSecurityProfile}' === 'true',
      canEditAccessLevels:'${canEditSecurityProfile}' === 'true'
  };
  var searchConfig={
    sortBy: [{
        name: 'asc'
    }],
    labels: {
        name: '<s:message code="common.column.name" javaScriptEscape="true" />',
        description: '<s:message code="common.column.description" javaScriptEscape="true" />',
        actions: '<s:message code="common.column.actions" javaScriptEscape="true" />'
    },
    permissions: permissions,
    resultsCountNode: '#teamAdminResultsCount'
  };
  
  var filterContextConfig={
    labels: {
        filter: '<s:message code="filter.active" javaScriptEscape="true" />',
        resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
        resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
    },
    container: '#teamAdminFilterContext'
  };
  
  var filterConfig={
    labels: {
        filterBy: '<s:message code="filter.by" javaScriptEscape="true" />'
    }
  };
  
  var teamListConfig={
    searchConfig: Y.merge(searchConfig, {
      labels: Y.merge(searchConfig.labels, {
        securityRecordTypeConfiguration:'<s:message code="team.result.securityRecordTypeConfiguration" javaScriptEscape="true" />',
        viewTeam: '<s:message code="team.result.action.view" javaScriptEscape="true" />',
        viewTeamTitle: '<s:message code="team.result.action.hover.view" javaScriptEscape="true" />',
        editTeam: '<s:message code="team.result.action.edit" javaScriptEscape="true" />',
        editTeamTitle: '<s:message code="team.result.action.hover.edit" javaScriptEscape="true" />',
        viewOrganisation: '<s:message code="team.result.action.viewAsOrganisation" javaScriptEscape="true" />',
        viewOrganisationTitle: '<s:message code="team.result.action.hover.viewAsOrganisation" javaScriptEscape="true" />'
      }),
      noDataMessage: '<s:message code="team.find.no.results" javaScriptEscape="true" />',
      url: '<s:url value="/rest/team?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
    }),
    filterContextConfig:Y.merge(filterContextConfig, {
        labels: Y.merge(filterContextConfig.labels, {name: '<s:message code="team.find.filter.name.active" javaScriptEscape="true" />'})
    }),
    filterConfig:Y.merge(filterConfig, Y.merge, {
        labels: Y.merge(filterConfig.labels, {name: '<s:message code="team.find.filter.name" javaScriptEscape="true" />'})
    }),
    toolbar:{
      addTitle:'<s:message code="team.add.button" javaScriptEscape="true" />',
      addAriaLabel:'<s:message code="team.add.button" javaScriptEscape="true" />'
    },
    permissions:permissions
  };

  var profileListConfig={
    searchConfig:Y.merge(searchConfig, {
      labels: Y.merge(searchConfig.labels, {
        defaultAccessLevel:'<s:message code="securityProfile.result.defaultAccessLevel" javaScriptEscape="true" />',
        editProfile: '<s:message code="securityProfile.result.action.edit" javaScriptEscape="true" />',
        editProfileTitle: '<s:message code="securityProfile.result.action.editTitle" javaScriptEscape="true" />',
        removeProfile: '<s:message code="securityProfile.result.action.remove" javaScriptEscape="true" />',
        removeProfileTitle: '<s:message code="securityProfile.result.action.removeTitle" javaScriptEscape="true" />',
        viewAccessLevels:'<s:message code="securityProfile.result.action.viewAccessLevels" javaScriptEscape="true" />',
        viewAccessLevelsTitle:'<s:message code="securityProfile.result.action.viewAccessLevelsTitle" javaScriptEscape="true" />',
        duplicateProfile:'<s:message code="securityProfile.result.action.duplicate" javaScriptEscape="true" />',
        duplicateProfileTitle:'<s:message code="securityProfile.result.action.duplicateTitle" javaScriptEscape="true" />'
      }),
      noDataMessage: '<s:message code="securityProfile.find.no.results" javaScriptEscape="true" />',
      url: '<s:url value="/rest/securityRecordTypeConfiguration?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
    }),
    filterContextConfig:Y.merge(filterContextConfig, {
      labels: Y.merge(filterContextConfig.labels, {name: '<s:message code="securityProfile.find.filter.name.active" javaScriptEscape="true" />'})
    }),
    filterConfig:Y.merge(filterConfig, Y.merge, {
        labels: Y.merge(filterConfig.labels, {name: '<s:message code="securityProfile.find.filter.name" javaScriptEscape="true" />'})
    }),
    toolbar:{
      addTitle:'<s:message code="securityProfile.add.button" javaScriptEscape="true" />',
      addAriaLabel:'<s:message code="securityProfile.add.button" javaScriptEscape="true" />'
    },
    permissions:permissions
  };
  
  var domainListConfig={
    searchConfig:Y.merge(searchConfig, {
      labels: Y.merge(searchConfig.labels, {
        editDomainTitle:'<s:message code="securityDomain.result.action.editTitle" javaScriptEscape="true" />',
        editDomain:'<s:message code="securityDomain.result.action.edit" javaScriptEscape="true" />',
        removeDomain: '<s:message code="securityDomain.result.action.remove" javaScriptEscape="true" />',
        removeDomainTitle: '<s:message code="securityDomain.result.action.removeTitle" javaScriptEscape="true" />',
        fillDomainTitle:'<s:message code="securityDomain.result.action.fillTitle" javaScriptEscape="true" />',
        fillDomain:'<s:message code="securityDomain.result.action.fill" javaScriptEscape="true" />'
      }),
      defaultSecurityDomainCode:'${defaultSecurityDomainCode}',
      noDataMessage: '<s:message code="securityDomain.find.no.results" javaScriptEscape="true" />',
      url: '<s:url value="/rest/securityDomain?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
    }),
    filterContextConfig:Y.merge(filterContextConfig, {
      labels: Y.merge(filterContextConfig.labels, {name: '<s:message code="securityDomain.find.filter.name.active" javaScriptEscape="true" />'})
    }),
    filterConfig:Y.merge(filterConfig, Y.merge, {
        labels: Y.merge(filterConfig.labels, {name: '<s:message code="securityDomain.find.filter.name" javaScriptEscape="true" />'})
    }),
    toolbar:{
      addTitle:'<s:message code="securityDomain.add.button" javaScriptEscape="true" />',
      addAriaLabel:'<s:message code="securityDomain.add.button" javaScriptEscape="true" />'
    },
    permissions:permissions
  };

  var teamAddLabels = {
      name: '<s:message javaScriptEscape="true" code="newTeamAndProfessionalRelationshipsVO.name"/>',
      description: '<s:message javaScriptEscape="true" code="newTeamAndProfessionalRelationshipsVO.description"/>',
      placeholder: '<s:message javaScriptEscape="true" code="team.personSearchPlaceholder"/>',
      noResults: '<s:message javaScriptEscape="true" code="team.personSearch.no.results"/>',
      securityProfilePlaceholder:'<s:message javaScriptEscape="true" code="team.securityProfileSearchPlaceholder"/>',
      securityProfile:'<s:message javaScriptEscape="true" code="newTeamAndProfessionalRelationshipsVO.securityRecordTypeConfigurationId"/>',
      noEntryAvailable:'<s:message javaScriptEscape="true" code="common.label.subType.non.available"/>',
      noEntrySelected:'<s:message javaScriptEscape="true" code="common.label.subType.non.selected"/>',
      region:'<s:message javaScriptEscape="true" code="team.result.region"/>',
      sector:'<s:message javaScriptEscape="true" code="team.result.sector"/>',
      sectorSubType:'<s:message javaScriptEscape="true" code="team.result.sectorSubType"/>',
      btns: {
          remove: '<s:message javaScriptEscape="true" code="button.remove"/>'
      }
  };
  var teamViewLabels = {
      name: '<s:message javaScriptEscape="true" code="TeamWithSecurityConfigurationVO.name"/>',
      description: '<s:message javaScriptEscape="true" code="TeamWithSecurityConfigurationVO.description"/>',
      securityProfile:'<s:message javaScriptEscape="true" code="TeamWithSecurityConfigurationVO.securityRecordTypeConfigurationId"/>',
      region:'<s:message javaScriptEscape="true" code="TeamWithSecurityConfigurationVO.region"/>',
      sector:'<s:message javaScriptEscape="true" code="team.result.sector"/>',
      sectorSubType:'<s:message javaScriptEscape="true" code="team.result.sectorSubType"/>'
  };

  var viewSearchConfigLabels = {
      comboNameID: 'Name (ID)',
      role: 'Role',
      noResults: 'No team members found'
  };

  var teamEditLabels = {
      name: '<s:message javaScriptEscape="true" code="updateTeamAndProfessionalRelationshipsVO.name"/>',
      description: '<s:message javaScriptEscape="true" code="updateTeamAndProfessionalRelationshipsVO.description"/>',
      securityProfilePlaceholder:'<s:message javaScriptEscape="true" code="team.securityProfileSearchPlaceholder"/>',
      securityProfile:'<s:message javaScriptEscape="true" code="updateTeamAndProfessionalRelationshipsVO.securityRecordTypeConfigurationId"/>',
      placeholder: '<s:message javaScriptEscape="true" code="team.personSearchPlaceholder"/>',
      noResults: '<s:message javaScriptEscape="true" code="team.personSearch.no.results"/>',
      noEntryAvailable:'<s:message javaScriptEscape="true" code="common.label.subType.non.available"/>',
      noEntrySelected:'<s:message javaScriptEscape="true" code="common.label.subType.non.selected"/>',
      region:'<s:message javaScriptEscape="true" code="team.result.region"/>',
      sector:'<s:message javaScriptEscape="true" code="team.result.sector"/>',
      sectorSubType:'<s:message javaScriptEscape="true" code="team.result.sectorSubType"/>',
      btns: {
          remove: '<s:message javaScriptEscape="true" code="button.remove"/>'
      }
  };

  var validationLabels={
    securityRecordTypeConfigurationIdValidationErrorMessage:'<s:message javaScriptEscape="true" code="securityRecordTypeConfigurationId.validationError"/>'
  };
  
  var teamHeaderIcon = 'fa eclipse-team',
      teamNarrativeDescription = '{{this.name}} ({{this.organisationIdentifier}})';
      
  var personAutocompleteURL = '<s:url value="/rest/person?nameOrUsername={query}&personType=professional&s={sortBy}&pageSize={maxResults}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>';
  var securityProfileAutocompleteURL = '<s:url value="/rest/securityRecordTypeConfiguration?name={query}&s={sortBy}&pageSize={maxResults}&useSoundex=true&appendWildcard=true"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>';

  var teamDialogConfig = {
      professionalTeamRelationshipTypeUrl: '<s:url value="/rest/professionalTeamRelationshipType"/>',
      views: {
          addTeam: {
              header: {
                  text: 'Add team',
                  icon: teamHeaderIcon
              },
              narrative: {
                  summary: 'You\'re adding a new team'
              },
              successMessage: '<s:message javaScriptEscape="true" code="team.message.add.success"/>',
              labels: teamAddLabels,
              config: {
                  validationLabels:validationLabels,
                  url: '<s:url value="/rest/team?withProfessionalRelationships=true"/>',
                  personAutocompleteURL: personAutocompleteURL,
                  teamViewPageURL: '<s:url value="/organisation?id={organisationId}"/>',
                  securityProfileAutocompleteURL:securityProfileAutocompleteURL
              }
          },
          viewTeam: {
              header: {
                  text: 'View team',
                  icon: teamHeaderIcon
              },
              narrative: {
                  summary: 'You\'re viewing the details of the team',
                  description: teamNarrativeDescription
              },
              labels: teamViewLabels,
              config: {
                  searchConfig: {
                	  sortBy: [{
                		  'po.roleAPerson.forename': 'asc',
                		  'po.roleAPerson.surname': 'asc'
                	  }],
                      url: '<s:url value="/rest/team/{id}/personTeamRelationship/professional?s={sortBy}&&temporalStatusFilter=ACTIVE&pageNumber={page}&pageSize={pageSize}"/>',
                      labels: viewSearchConfigLabels
                  }
              }
          },
          editTeam: {
              header: {
                  text: 'Manage team members',
                  icon: teamHeaderIcon
              },
              narrative: {
                  summary: 'You\'re managing the details of the team',
                  description: teamNarrativeDescription
              },
              successMessage: '<s:message javaScriptEscape="true" code="team.message.edit.success"/>',
              labels: teamEditLabels,
              config: {
                validationLabels:validationLabels,
                url: '<s:url value="/rest/team/{id}?withProfessionalRelationships=true"/>',
                personAutocompleteURL: personAutocompleteURL,
                securityProfileAutocompleteURL:securityProfileAutocompleteURL
              }
          }
      }
  };
  
  
  var securityDomainAddUpdateLabels={
    name:'<s:message javaScriptEscape="true" code="newSecurityDomainVO.name"/>',
    description:'<s:message javaScriptEscape="true" code="newSecurityDomainVO.description"/>'
  };
  
  var teamSecurityDomainIcon = 'fa fa-briefcase';
  
  var securityProfileAddUpdateLabels={
    name:'<s:message javaScriptEscape="true" code="newSecurityRecordTypeConfigurationVO.name"/>',
    description:'<s:message javaScriptEscape="true" code="newSecurityRecordTypeConfigurationVO.description"/>',
    defaultAccessLevel:'<s:message javaScriptEscape="true" code="newSecurityRecordTypeConfigurationVO.defaultAccessLevel"/>'
  };
  
  var teamSecurityProfileIcon = 'fa fa-lock';
  
  var teamSecurityDialogConfig={
    views: {
      addSecurityDomain: {
          header: {
              text: '<s:message javaScriptEscape="true" code="securityDomain.dialog.add.header"/>',
              icon: teamSecurityDomainIcon
          },
          narrative: {
              summary: '<s:message javaScriptEscape="true" code="securityDomain.dialog.add.narrative"/>'
          },
          successMessage: '<s:message javaScriptEscape="true" code="securityDomain.dialog.add.successMessage"/>',
          labels: securityDomainAddUpdateLabels,
          config: {
              url: '<s:url value="/rest/securityDomain"/>'
          }
      },
      updateSecurityDomain: {
        header: {
            text: '<s:message javaScriptEscape="true" code="securityDomain.dialog.update.header"/>',
            icon: teamSecurityDomainIcon
        },
        narrative: {
            summary: '<s:message javaScriptEscape="true" code="securityDomain.dialog.update.narrative"/>'
        },
        successMessage: '<s:message javaScriptEscape="true" code="securityDomain.dialog.update.successMessage"/>',
        labels: securityDomainAddUpdateLabels,
        config: {
            url: '<s:url value="/rest/securityDomain/{id}"/>'
        }
      },
      removeSecurityDomain: {
        header: {
          text: '<s:message javaScriptEscape="true" code="securityDomain.dialog.remove.header"/>',
          icon: teamSecurityDomainIcon
        },
        narrative: {
            summary: '<s:message javaScriptEscape="true" code="securityDomain.dialog.remove.narrative"/>'
        },
        successMessage: '<s:message javaScriptEscape="true" code="securityDomain.dialog.remove.successMessage"/>',
        labels: {
          message:'<s:message javaScriptEscape="true" code="securityDomain.dialog.remove.message"/>'
        },
        config: {
            url: '<s:url value="/rest/securityDomain/{id}"/>'
        }
      },      
      addSecurityProfile: {
        header: {
          text: '<s:message javaScriptEscape="true" code="securityProfile.dialog.add.header"/>',
          icon: teamSecurityProfileIcon
        },
        narrative: {
            summary: '<s:message javaScriptEscape="true" code="securityProfile.dialog.add.narrative"/>'
        },
        successMessage: '<s:message javaScriptEscape="true" code="securityDomain.dialog.add.successMessage"/>',
        labels: securityProfileAddUpdateLabels,
        config: {
            url: '<s:url value="/rest/securityRecordTypeConfiguration"/>'
        }
      },
      updateSecurityProfile: {
        header: {
          text: '<s:message javaScriptEscape="true" code="securityProfile.dialog.update.header"/>',
          icon: teamSecurityProfileIcon
        },
        narrative: {
            summary: '<s:message javaScriptEscape="true" code="securityProfile.dialog.update.narrative"/>'
        },
        successMessage: '<s:message javaScriptEscape="true" code="securityDomain.dialog.update.successMessage"/>',
        labels: securityProfileAddUpdateLabels,
        config: {
            url: '<s:url value="/rest/securityRecordTypeConfiguration/{id}"/>'
        }
      },
      removeSecurityProfile: {
        header: {
          text: '<s:message javaScriptEscape="true" code="securityProfile.dialog.remove.header"/>',
          icon: teamSecurityProfileIcon
        },
        narrative: {
            summary: '<s:message javaScriptEscape="true" code="securityProfile.dialog.remove.narrative"/>'
        },
        successMessage: '<s:message javaScriptEscape="true" code="securityProfile.dialog.remove.successMessage"/>',
        labels: {
          message:'<s:message javaScriptEscape="true" code="securityProfile.dialog.remove.message"/>'
        },
        config: {
            url: '<s:url value="/rest/securityRecordTypeConfiguration/{id}"/>'
        }
      },
      duplicateSecurityProfile: {
        header: {
          text: '<s:message javaScriptEscape="true" code="securityProfile.dialog.duplicate.header"/>',
          icon: teamSecurityProfileIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="securityProfile.dialog.duplicate.narrative"/>'
        },
        labels: {
          message:'<s:message javaScriptEscape="true" code="securityProfile.dialog.duplicate.message"/>'
        },
        config: {
            url: '<s:url value="/rest/securityRecordTypeConfiguration?copy=true"/>'
        }

      }
    }
  };

  var config = {
      container: '#securityAdminTabs',
      root:'<s:url value="/admin/team/list/"/>',
      contextTitleNode: '#teamAdminContextTitle',
      organisationViewURL:'<s:url value="/organisation?id={id}&reset=true"/>',
      permissions:permissions,
      teamDialogConfig: teamDialogConfig,
      teamSecurityDialogConfig:teamSecurityDialogConfig,
      tabs:{
        teams:{
          label:'<s:message code="security.admin.tabs.teams" javaScriptEscape="true"/>',
          config:teamListConfig
        },
        profiles:{
          label:'<s:message code="security.admin.tabs.profiles" javaScriptEscape="true"/>',
          config:profileListConfig
        },
        domains:{
          label:'<s:message code="security.admin.tabs.domains" javaScriptEscape="true"/>',
          config:domainListConfig
        }
      },
      accessLevelsConfig:{
        url:'<s:url value="/rest/securityRecordTypeConfiguration/{id}"/>',
        updateAccessLevelsUrl:'<s:url value="/rest/securityRecordTypeConfiguration/{id}/accessLevels"/>',
        securityAccessUrl:'<s:url value="/rest/securityRecordTypeConfiguration/{id}/securityAccessElement"/>',
        labels:{
          edit:'<s:message code="button.edit" javaScriptEscape="true" />',
          editTitle:'<s:message code="securityProfile.accessLevel.edit.button.title" javaScriptEscape="true" />',
          editAriaLabel:'<s:message code="securityProfile.accessLevel.edit.button.title.aria" javaScriptEscape="true" />',
          cancel:'<s:message code="button.cancel" javaScriptEscape="true" />',
          cancelTitle:'<s:message code="securityProfile.accessLevel.cancel.button.title" javaScriptEscape="true" />',
          cancelAriaLabel:'<s:message code="securityProfile.accessLevel.cancel.button.title.aria" javaScriptEscape="true" />',
          save:'<s:message code="button.save" javaScriptEscape="true" />',
          saveTitle:'<s:message code="securityProfile.accessLevel.save.button.title" javaScriptEscape="true" />',
          saveAriaLabel:'<s:message code="securityProfile.accessLevel.save.button.title.aria" javaScriptEscape="true" />'
        }
      },
      domainContentsConfig:{
        url:'<s:url value="/rest/securityDomain/{id}"/>',
        defaultSecurityDomainUrl:'<s:url value="/rest/securityDomain?securityDomainCode={securityDomainCode}"/>',
        defaultSecurityDomainCode:'${defaultSecurityDomainCode}',
        updateDomainAssignmentsUrl:'<s:url value="/rest/securityDomain/{id}"/>',
        successMessage: '<s:message javaScriptEscape="true" code="securityDomain.assignments.message.update.success"/>',
        labels:{
          prompt:'<s:message code="securityDomain.assignments.info" javaScriptEscape="true" />',
          cancel:'<s:message code="button.cancel" javaScriptEscape="true" />',
          cancelTitle:'<s:message code="securityDomain.contents.cancel.button.title" javaScriptEscape="true" />',
          cancelAriaLabel:'<s:message code="securityDomain.contents.cancel.button.title.aria" javaScriptEscape="true" />',
          save:'<s:message code="button.save" javaScriptEscape="true" />',
          saveTitle:'<s:message code="securityDomain.contents.save.button.title" javaScriptEscape="true" />',
          saveAriaLabel:'<s:message code="securityDomain.contents.save.button.title.aria" javaScriptEscape="true" />'
        }
      },      
      labels:{
        teamAdminTitle:'<s:message javaScriptEscape="true" code="menu.teamadministration.sectiontitle"/>',
        securityDomainTitle:'<s:message javaScriptEscape="true" code="security.admin.domains.title"/>',
        securityProfileTitle:'<s:message javaScriptEscape="true" code="security.admin.profiles.title"/>',
        accessLevelTitle:'<s:message javaScriptEscape="true" code="security.admin.accessLevels.title"/>',
        domainContentsTitle:'<s:message javaScriptEscape="true" code="security.admin.domainContents.title"/>'
      },
      filterContext:{
          shared:true,
          container:'#teamAdminFilterContext'
      } 
  };
  
  var teamView = new Y.app.admin.team.TeamControllerView(config).render();
});
</script>