<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'SecurityUser','SecurityUser.ADD')" var="canAdd" />
<sec:authorize access="hasPermission(null, 'SecurityUser','SecurityUser.GET')" var="canView"/>
<sec:authorize access="hasPermission(null, 'SecurityUser','SecurityUser.UPDATE')" var="canUpdate"/>
<sec:authorize access="hasPermission(null, 'SecurityUser','SecurityUser.UPDATE')" var="canChangePassword"/>
<sec:authentication property="principal.userSummary.id" var="userid"/>
<div id="userResults"></div> 

<c:set var="passwordPolicyTooltipArgs"  value="${passwordPolicy.minChars},,${passwordPolicy.minUppercaseChars},,${passwordPolicy.minDigits},,${passwordPolicy.minNonAlphanumericChars}" />
 
<script>
Y.use('yui-base', 'event-delegate', 'event-custom-base', 'user-search-view', function(Y) {
  var permissions = {
      canView: '${canView}' === 'true',
      canUpdate: '${canUpdate}' === 'true',
      canChangePassword: '${canChangePassword}' === 'true',
      canAddUser: '${canAdd}' === 'true'
  };
  var searchConfig = {
      title: '<s:message code="user.find.results"/>',
      sortBy: [{name: 'asc'}],
      labels: {
          id: '<s:message code="user.find.results.id"/>',
          name: '<s:message code="user.find.results.name"/>',
          userName: '<s:message code="user.find.results.userName"/>',
          roles: '<s:message code="user.find.results.roles"/>',
          noRoles: '<s:message code="user.view.noRoles"/>',
          person: '<s:message code="user.find.results.person"/>',
          expiryDate: '<s:message code="user.find.results.expiryDate"/>',
          enabled: '<s:message code="user.find.results.enabled"/>',
          viewUser: '<s:message code="user.find.results.view"/>',
          editUser: '<s:message code="user.find.results.edit"/>',
          changeUserPassword: '<s:message code="user.find.results.changeUserPassword"/>',
          enableUser: '<s:message code="user.find.results.enable"/>',
          disableUser: '<s:message code="user.find.results.disable"/>',
          actions: '<s:message code="user.find.results.actions"/>'
      },
      resultsCountNode: Y.one('#userResultsCount'),
      noDataMessage: '<s:message code="user.find.no.results"/>',
      permissions: permissions,
      loggedInUserId: '${esc:escapeJavaScript(userid)}',
      url: '<s:url value="/rest/securityUser/?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
  };
  var filterContextConfig = {
      labels: {
          filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
          roles: '<s:message code="user.find.filter.roles.active" javaScriptEscape="true"/>',
          name: '<s:message code="user.find.results.name" javaScriptEscape="true"/>',
          username: '<s:message code="user.find.results.userName" javaScriptEscape="true"/>'
      },
      container: '#userFilterContext'
  };
  var filterConfig = {
      securityRoleListURL: '<s:url value="/rest/securityRole?s=description%22%3A%22asc%22&pageNumber=-1&pageSize=-1"/>',
      labels: {
          filterBy: '<s:message code="outputTemplate.find.filter.title" javaScriptEscape="true"/>',
          name: '<s:message code="user.find.results.name" javaScriptEscape="true"/>',
          username: '<s:message code="user.find.results.userName" javaScriptEscape="true"/>',
          roles: '<s:message code="user.find.results.filter.roles" javaScriptEscape="true"/>'
      }
  };
  var workContactURLs = {
      workMobileContactsURL: '<s:url value="/rest/person/{personId}/mobileContact/preferredCurrentWork"/>',
      workEmailContactsURL: '<s:url value="/rest/person/{personId}/emailContact/preferredCurrentWork"/>' 
  };
  //labels
  var addUserLabels = {
      person: '<s:message code="newSecurityUserVO.person"/>',
      name: '<s:message code="newSecurityUserVO.name"/>',
      userName: '<s:message code="newSecurityUserVO.userName"/>',
      fillOutLdapOrUserPwd: '<s:message code="newSecurityUserVO.fillOutLdapOrUserPwd"/>',
      ldapUserName: '<s:message code="newSecurityUserVO.ldapUserName"/>',
      password: '<s:message code="newSecurityUserVO.password"/>',
      confirmPassword: '<s:message code="newSecurityUserVO.confirmPassword"/>',
      passwordExpiryDate: '<s:message code="newSecurityUserVO.passwordExpiryDate"/>',
      passwordReset: '<s:message code="newSecurityUserVO.passwordReset"/>',
      enabled: '<s:message code="newSecurityUserVO.enabled"/>',
      locked: '<s:message code="newSecurityUserVO.locked"/>',
      expiryDate: '<s:message code="newSecurityUserVO.expiryDate"/>',
      roles: '<s:message code="newSecurityUserVO.roles"/>',
      personNotBlank: '<s:message code="NotBlank.newSecurityUserVO.person" javaScriptEscape="true"/>',
      changeUserPersonAssociation: '<s:message code="user.add.changeUserPersonAssociation"/>',
      passwordPopupMessageTitle: '<s:message code="user.add.passwordPopup.title"/>',
      passwordPopupMessageContent: '<s:message code="user.add.passwordPopup.content" argumentSeparator=",," arguments="${passwordPolicyTooltipArgs}"/>',
      confirmPasswordPopupMessageTitle: '<s:message code="user.add.confirmPasswordPopup.title"/>',
      confirmPasswordPopupMessageContent: '<s:message code="user.add.confirmPasswordPopup.content"/>',      
      rolesMandatory: '<s:message code="user.add.roles.mandatory" javaScriptEscape="true"/>',
      authenticationFactor: '<s:message code="user.edit.authenticationFactor" javaScriptEscape="true"/>',
      authPasswordOnly: '<s:message code="user.edit.authPasswordOnly" javaScriptEscape="true"/>',
      authOneAndAHalfFactor: '<s:message code="user.edit.authOneAndAHalfFactor" javaScriptEscape="true"/>',
      authTwoFactor: '<s:message code="user.edit.authTwoFactor" javaScriptEscape="true"/>',
      workMobileContactLabel: '<s:message code="user.contacts.workMobileContact" javaScriptEscape="true"/>',
      workEmailContactLabel: '<s:message code="user.contacts.workEmailContact" javaScriptEscape="true"/>',
      contactsWarningLabel: '<s:message code="user.contacts.dialog.warning" javaScriptEscape="true"/>',
      addUserAndSendLoginCredentials: '<s:message code="user.add.success.sendLoginDetails" javaScriptEscape="true"/>',
      addUserWarningForNoEmail: '<s:message code="user.add.success.warningSendLoginDetails" javaScriptEscape="true"/>'
  };
  var viewUserLabels = {
      name: '<s:message code="viewUserVO.name"/>',
      userName: '<s:message code="viewUserVO.userName"/>',
      ldapUserName: '<s:message code="viewUserVO.ldapUserName"/>',
      passwordExpiryDate: '<s:message code="viewUserVO.passwordExpiryDate"/>',
      passwordReset: '<s:message code="viewUserVO.passwordReset"/>',
      enabled: '<s:message code="viewUserVO.enabled"/>',
      locked: '<s:message code="viewUserVO.locked"/>',
      expiryDate: '<s:message code="viewUserVO.expiryDate"/>',
      person: '<s:message code="viewUserVO.person"/>',
      roles: '<s:message code="viewUserVO.roles"/>',
      userType: '<s:message code="viewUserVO.userType"/>',
      trusted: '<s:message code="viewUserVO.trusted"/>',
      noRoles: '<s:message code="user.view.noRoles"/>',
      changePassword: '<s:message code="user.view.changePassword"/>',
      authenticationFactor: '<s:message code="user.edit.authenticationFactor" javaScriptEscape="true"/>',
      authPasswordOnly: '<s:message code="user.edit.authPasswordOnly" javaScriptEscape="true"/>',
      authOneAndAHalfFactor: '<s:message code="user.edit.authOneAndAHalfFactor" javaScriptEscape="true"/>',
      authTwoFactor: '<s:message code="user.edit.authTwoFactor" javaScriptEscape="true"/>',
      workMobileContactLabel: '<s:message code="user.contacts.workMobileContact" javaScriptEscape="true"/>',
      workEmailContactLabel: '<s:message code="user.contacts.workEmailContact" javaScriptEscape="true"/>',
      contactsWarningLabel: '<s:message code="user.contacts.dialog.warning" javaScriptEscape="true"/>'
  };
  var editUserLabels = {
      name: '<s:message code="updateUserVO.name"/>',
      ldapUserName: '<s:message code="updateUserVO.ldapUserName"/>',
      enabled: '<s:message code="updateUserVO.enabled"/>',
      locked: '<s:message code="updateUserVO.locked"/>',
      resetSecondaryPassword: '<s:message code="updateUserVO.resetSecondaryPassword"/>',
      passwordReset: '<s:message code="updateUserVO.passwordReset"/>',
      expiryDate: '<s:message code="updateUserVO.expiryDate"/>',
      roles: '<s:message code="updateUserVO.roles"/>',
      passwordExpiryDate: '<s:message code="updateUserVO.passwordExpiryDate"/>',
      changePassword: '<s:message code="user.edit.changePassword"/>',
      rolesMandatory: '<s:message code="user.edit.roles.mandatory" javaScriptEscape="true"/>',
      authenticationFactor: '<s:message code="user.edit.authenticationFactor" javaScriptEscape="true"/>',
      authPasswordOnly: '<s:message code="user.edit.authPasswordOnly" javaScriptEscape="true"/>',
      authOneAndAHalfFactor: '<s:message code="user.edit.authOneAndAHalfFactor" javaScriptEscape="true"/>',
      authTwoFactor: '<s:message code="user.edit.authTwoFactor" javaScriptEscape="true"/>',
      workMobileContactLabel: '<s:message code="user.contacts.workMobileContact" javaScriptEscape="true"/>',
      workEmailContactLabel: '<s:message code="user.contacts.workEmailContact" javaScriptEscape="true"/>',
      contactsWarningLabel: '<s:message code="user.contacts.dialog.warning" javaScriptEscape="true"/>',
  };
  var changeUserPasswordLabels = {
      password: '<s:message code="updateUserPasswordVO.password"/>',
      confirmPassword: '<s:message code="updateUserPasswordVO.confirmPassword"/>',
      passwordPopupMessageTitle: '<s:message code="user.changePassword.passwordPopup.title"/>',
      passwordPopupMessageContent: '<s:message code="user.changePassword.passwordPopup.content" argumentSeparator=",," arguments="${passwordPolicyTooltipArgs}"/>',
      confirmPasswordPopupMessageTitle: '<s:message code="user.changePassword.confirmPasswordPopup.title"/>',
      confirmPasswordPopupMessageContent: '<s:message code="user.changePassword.confirmPasswordPopup.content"/>'
  };
  var disableUserLabels = {
      confirm: '<s:message code="user.disable.disableConfirm" javaScriptEscape="true" />'
  };
  var enableUserLabels = {
      confirm: '<s:message code="user.enable.confirm" javaScriptEscape="true" />'
  };
  //common narrative
  var userNarrativeDescription = '{{this.name}} ({{this.userIdentifier}})',
      userHeaderIcon = 'fa fa-wrench';
  var dialogConfig = {
      views: {
          addUser: {
              header: {
                  text: '<s:message code="user.dialog.add.header" javaScriptEscape="true"/>',
                  icon: userHeaderIcon
              },
              narrative: {
                  summary: '<s:message code="user.add.summary" javaScriptEscape="true" />'
              },
              successMessage: '<s:message code="user.add.success.message" javaScriptEscape="true"/>',
              labels: addUserLabels,
              config: {
                  url: '<s:url value="/rest/securityUser"/>',                  
                  personTeamURL: '<s:url value="/rest/person/{personId}/teamRelationship/professional?temporalStatusFilter=ACTIVE&pageNumber=1&pageSize=-1"/>',
                  appLoginURL: '${esc:escapeJavaScript(appLoginURL)}',
                  automaticEmailLoginDetailsURL: '<s:url value="/rest/securityUser/{personId}/sendEmail"/>',
                  defaultPasswordExpiryDuration: '${passwordPolicy.expiryDateDuration}',
                  restUrlsForWorkContacts: workContactURLs,
                  autocomplete:{
                    autocompleteURL: '<s:url value="/rest/person?nameOrId={query}&appendWildcard=true"/>',
                    labels:{
                      placeholder: '<s:message code="user.add.personSearchPlaceholder" javaScriptEscape="true"/>'  
                    }
                  }
              }
          },
          viewUser: {
              header: {
                  text: '<s:message code="user.dialog.view.header" javaScriptEscape="true"/>',
                  icon: userHeaderIcon
              },
              narrative: {
                  summary: '<s:message code="user.view.summary" javaScriptEscape="true" />',
                  description: userNarrativeDescription
              },
              labels: viewUserLabels,
              config: {
                  url: '<s:url value="/rest/securityUser/{id}"/>',
                  systemConfigDetailsURL: '<s:url value="/rest/securityConfiguration"/>',
                  restUrlsForWorkContacts: workContactURLs
              }
          },
          editUser: {
              header: {
                  text: '<s:message code="user.dialog.edit.header" javaScriptEscape="true"/>',
                  icon: userHeaderIcon
              },
              narrative: {
                  summary: '<s:message code="user.edit.summary" javaScriptEscape="true" />',
                  description: userNarrativeDescription
              },
              successMessage: '<s:message code="user.edit.success.message" javaScriptEscape="true"/>',
              labels: editUserLabels,
              config: {
                  url: '<s:url value="/rest/securityUser/{id}"/>',
                  personTeamURL: '<s:url value="/rest/person/{personId}/teamRelationship/professional?temporalStatusFilter=ACTIVE&pageNumber=1&pageSize=-1"/>',
                  restUrlsForWorkContacts: workContactURLs
              }
          },
          changeUserPassword: {
              header: {
                  text: '<s:message code="user.dialog.changePassword.header" javaScriptEscape="true"/>',
                  icon: userHeaderIcon
              },
              narrative: {
                  summary: '<s:message code="user.changePassword.summary" javaScriptEscape="true" />',
                  description: '{{this.targetName}} ({{this.targetIdentifier}})'
              },
              successMessage: '<s:message code="user.changePassword.success.message" javaScriptEscape="true"/>',
              labels: changeUserPasswordLabels,
              config: {
                  url: '<s:url value="/rest/securityUser/{id}?action=updateCredentials"/>'
              }
          },
          disableUser: {
              header: {
                  text: '<s:message code="user.dialog.disable.header" javaScriptEscape="true"/>',
                  icon: userHeaderIcon
              },
              narrative: {
                  summary: '<s:message code="user.disable.summary" javaScriptEscape="true" />',
                  description: userNarrativeDescription
              },
              successMessage: '<s:message code="user.disable.success.message" javaScriptEscape="true"/>',
              labels: disableUserLabels,
              config: {
                  url: '<s:url value="/rest/securityUser/{id}"/>',
                  updateURL: '<s:url value="/rest/securityUser/{id}/status?action=enabled&enabled=false" />'
              }
          },
          enableUser: {
              header: {
                  text: '<s:message code="user.dialog.enable.header" javaScriptEscape="true"/>',
                  icon: userHeaderIcon
              },
              narrative: {
                  summary: '<s:message code="user.enable.summary" javaScriptEscape="true" />',
                  description: userNarrativeDescription
              },
              successMessage: '<s:message code="user.enable.success.message" javaScriptEscape="true"/>',
              labels: enableUserLabels,
              config: {
                  url: '<s:url value="/rest/securityUser/{id}"/>',
                  updateURL: '<s:url value="/rest/securityUser/{id}/status?action=enabled&enabled=true" />'
              }
          }
      },
      permissions: permissions
  };
  var userResults = new Y.app.admin.user.UserFilteredResults({
      container: '#userResults',
      securityRoleListURL: '<s:url value="/rest/securityRole?s=description%22%3A%22asc%22&pageNumber=-1&pageSize=-1"/>',
      systemConfigURL: '<s:url value="/rest/securityConfiguration"/>',
      searchConfig: searchConfig,
      filterConfig: filterConfig,
      filterContextConfig: filterContextConfig,
      dialogConfig: dialogConfig
  }).render();
});
</script>	