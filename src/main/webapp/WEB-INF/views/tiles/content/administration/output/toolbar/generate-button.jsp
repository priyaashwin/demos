<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'OutputTemplate','OutputTemplate.ADD')" var="canAddOutput" />
<sec:authorize access="hasPermission(null, 'FormOutputTemplate','FormOutputTemplate.ADD')" var="canAddFormOutput" />

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
	<c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
	<c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
	<c:set var="lStyle" value="last" />
</c:if>

<%-- CSS classes and tabIndex to apply if user has ADD permission --%>
<c:choose>
	<c:when test="${canAddOutput eq true and canAddFormOutput eq true}">
		<c:set var="generateClass" value="generate" />
		<c:set var="tabIndex" value="0" />
	</c:when>
	<c:otherwise>
		<c:set var="tabIndex" value="-1"/>
	</c:otherwise>
</c:choose>

<%-- Button markup--%>
<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
	<a href="#none" data-button-action="${generateClass}"  class="pure-button pure-button-disabled usp-fx-all ${activeStyle} ${generateClass}"
		title='<s:message code="outputTemplate.generate.button"/> ' aria-disabled="true"
		aria-label='<s:message code="outputTemplate.generate.button"/>' tabindex="${tabIndex }"> 
		<i class="fa fa-bolt"></i>
		<span><s:message code="button.template" /></span>
	</a>
</li>