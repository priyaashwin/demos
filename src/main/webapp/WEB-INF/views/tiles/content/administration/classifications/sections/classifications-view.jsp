<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasPermission(null, 'ClassificationGroup','ClassificationGroup.GET')" var="canGetClassificationGroup"/>
<sec:authorize access="hasPermission(null, 'Classification','Classification.GET')" var="canGetClassification"/>
<sec:authorize access="hasPermission(null, 'ClassificationGroup','ClassificationGroup.ARCHIVED')" var="canDeleteClassificationGroup" />
<sec:authorize access="hasPermission(null, 'Classification','Classification.ARCHIVED')" var="canDeleteClassification" />

<c:if test="${(canDeleteClassificationGroup && canDeleteClassification) eq true}"> 
    <div id="showClassificationDeletedContainer" class="hide-on-search pure-u-1">
        <label for="classificationTreeFilter_statusDeleted" class="pure-checkbox">
            <span id = "classificationTreeFilter_statusDeletedSpan" >	
                <input id="classificationEntryFilter_statusDeleted" type="checkbox" name="status" value="ARCHIVED" />
                <s:message code="classification.show.deleted.input"/>
            </span>
        </label>
    </div>
</c:if> 
<div id="classicationTreePage" class="hide-on-search pure-u-1">
Classification Page
</div>

<script>
    Y.use('yui-base', 'event-custom-base', 'admin-system-config-screen', 'node', 'event', 'usp-classification-ClassificationGroupWithChildren', function(Y) {
        var CLASSIFICATION_MULTIPANEL_DIALOG_ACTION = 'classificationConfig:showDialog';
        var classificationTreeModel = new Y.usp.classification.ClassificationGroupWithChildren({
            url: '<s:url value="/rest/classificationGroup/hierarchy?includeArchived=false" />',
            excludeDeleteURL: '<s:url value="/rest/classificationGroup/hierarchy?includeArchived=false" />',
            includeDeleteURL: '<s:url value="/rest/classificationGroup/hierarchy?includeArchived=true" />'
        });

        var node = Y.one('#classicationTreePage');

        Y.namespace('app.admin.classification').ClassificationTreeView = Y.Base.create('classificationTreeView', Y.View, [], {
            initializer: function() {
                var createFactory = function(type) {
                    return React.createElement.bind(null, type);
                };

                //setup a React factory for the classificationTreeView
                this.classificationTreeViewFactory = createFactory(uspClassification.ClassificationTreeView);
            },
            render: function() {
                var self = this;
                this.get('model').load(Y.bind(function(err, res) {
                    ReactDOM.render(self.classificationTreeViewFactory({
                        tree: Y.JSON.parse(res.response),
                        clickHandler: self._handleActionMenu.bind(self),
                        labels: self.get('labels')
                    }), node._node);
                }));
            },
            destructor: function() {
                //clean up identification view
                ReactDOM.unmountComponentAtNode(node._node);
            },
            _handleActionMenu: function(data) {
                var target = data.target;
                var targetType = target.dataset.type.toUpperCase();
                var targetElement = Y.one(target);
                var action = 'NONE';

                //Need to decide the action type based on the input data
                if (targetElement.hasClass('classificationView') && 'CLASSIFICATIONGROUP' === targetType) {
                    action = 'viewClassificationGroup';
                } else if (targetElement.hasClass('classificationView') && 'CLASSIFICATION' === targetType) {
                    action = 'viewClassification';
                } else if (targetElement.hasClass('classificationAdd') && 'CLASSIFICATIONGROUP' === targetType) {
                    action = 'addClassificationProceed';
                } else if (targetElement.hasClass('classificationAdd') && 'CLASSIFICATION' === targetType) {
                    action = 'addClassificationProceed';
                } else if (targetElement.hasClass('classificationEdit') && 'CLASSIFICATIONGROUP' === targetType) {
                    action = 'editClassificationGroup';
                } else if (targetElement.hasClass('classificationEdit') && 'CLASSIFICATION' === targetType) {
                    action = 'editClassification';
                } else if (targetElement.hasClass('classificationDelete') && 'CLASSIFICATION' === targetType) {
                    action = 'deleteClassification';
                } else if (targetElement.hasClass('classificationDelete') && 'CLASSIFICATIONGROUP' === targetType) {
                    action = 'deleteClassificationGroup';
                } 
                // Special case: restore selected, no pop-up required
                else if (targetElement.hasClass('classificationRestore')) {
                    this._handleRestoreClassification({
                        id: target.dataset.id,
                        type: targetType
                    });
                    return;
                }

                Y.fire(CLASSIFICATION_MULTIPANEL_DIALOG_ACTION, {
                    action: action,
                    type: targetType,
                    id: target.dataset.id,
                    parentGroupId: target.dataset.id, //While adding group/classification parent group will be the node where add action is selected
                    codePath: data.codePath
                });
            },
            _handleRestoreClassification: function(data) {
                if ('CLASSIFICATION' === data.type) {
                    var url = '<s:url value="/rest/classification/{id}/status?action=active" />';
                }
                else if ('CLASSIFICATIONGROUP' === data.type) {
                    var url = '<s:url value="/rest/classificationGroup/{id}/status?action=active" />';
                }
                else {
                    throw Error('Unknown classification type');
                }
                
                url = Y.Lang.sub(url, { id: data.id });
                
                Y.io(url, {
                    method: 'PUT',
                    on: {
                        success: function() {
                            Y.fire('infoMessage:message', {
                                message: '<s:message code="classification.config.messages.restore.success"/>'
                            });
                            this.render();
                        }.bind(this),
                        failure: function() {
                            var responseBody = Y.JSON.parse(response.responseText);
                            alert("Sorry, this operation failed:\n " + responseBody.message);
                        }
                    }
                });
            }
        }, {
            ATTRS: {}
        });

        var commonLabels = {
            iconI: '<s:message code="classification.tree.icons.i"/>',
            iconR: '<s:message code="classification.tree.icons.r"/>',
            iconP: '<s:message code="classification.tree.icons.p"/>',
            iconITitle: '<s:message code="classification.tree.icons.title.i"/>',
            iconRTitle: '<s:message code="classification.tree.icons.title.r"/>',
            iconPTitle: '<s:message code="classification.tree.icons.title.p"/>',
        }
        var ClassificationTreeView = new Y.app.admin.classification.ClassificationTreeView({
            model: classificationTreeModel,
            labels: commonLabels
        });
        ClassificationTreeView.render();
        Y.on('classificationsTree:dataChanged', function(e) {
            ClassificationTreeView.render();
        });

        Y.one('#showClassificationDeletedContainer').delegate('click', function(e) {
            classificationTreeModel
            if (e.currentTarget.get('checked')) {
                classificationTreeModel.url = classificationTreeModel.get('includeDeleteURL');
            } else {
                classificationTreeModel.url = classificationTreeModel.get('excludeDeleteURL');
            }
            ClassificationTreeView.set('model', classificationTreeModel);
            ClassificationTreeView.render();
        }, 'input');
    });
</script>