<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasPermission(null, 'ChecklistDefinition','ChecklistDefinition.View')" var="canView" />

<c:choose>
	<c:when test="${canView eq true}">
		<div id="checklist-designer"></div>
	
		<script>
	    	//render the component
	    	uspChecklistDesigner.renderDesigner(document.getElementById('checklist-designer'), {
	    	    id:'${id}',
	    	    labels:{
	    	        //TODO - source these from the resource bundle when development is complete	    
	    	        ok:'ok',
	    	        cancel:'cancel',
	    	        taskType:'Task type',
	    	        outcomeType:'Outcome type'
	    	    }
	    	}, {
	    	    serverConfig:{
	    	        loadEndpoint:{
	    	            url:'<s:url value="/rest/checklistDefinition/{id}/design"/>'
	    	        },
	    	     	saveEndpoint:{
	    	     	    url:'<s:url value="/rest/checklistDefinition"/>'
	    	     	},
	    	     	taskTypeConfigurationEndpoint:{
	    	     	    url:'<s:url value="/rest/taskTypeDefinition"/>'
	    	     	},
	    	     	outcomeTypeConfigurationEndpoint:{
	    	     	    url:'<s:url value="/rest/outcomeTypeDefinition"/>'
	    	     	},
	    	     	dynamicSourceConfigurationEndpoint:{
	    	     	   url:'<s:url value="/rest/dynamicSourceConfiguration"/>'
	    	     	},
	    	     	ruleConfigurationEndpoint:{
	    	     	    url:'<s:url value="/rest/ruleDefinition"/>'
	    	     	},	    	     	
	    	     	latestVersionEndpoint:{
	    	     	   url:'<s:url value="/rest/checklistDefinition"/>'
	    	     	}
	    	    }
	    	});
		</script>
		<script>
		 window.onbeforeunload = function(event)
		    {
		     	if(window.opener && window.opener.Y){
		     	   window.opener.Y.fire('refreshList')
		     	}
		    };
		    
		 	// Listen for the event.
		    document.addEventListener('saveEvent', function (e) {
		     	if(window.opener && window.opener.Y){
		     	   window.opener.Y.fire('refreshList')
		     	}
		    }, false);
		</script>
			        
		<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />
	</c:when>
	<c:otherwise>
		<%-- Insert access denied tile --%>
		<t:insertDefinition name="access.denied"/>
	</c:otherwise>
</c:choose>
