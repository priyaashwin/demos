<%@ page import="java.util.Locale" %>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<script>

  Y.use('yui-base',
      'event-custom',
      'classification-configuration-dialog-views',
      'classification-configuration-models',
      'model-transmogrify',
    
  function(Y) {

	// callback when dialog is open	 
		var showViewFn = function(view) {

			switch(view.name) {
			case 'addClassificationProceedForm':
				   this.getButton('proceedButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					 break;
			
			case 'addClassificationForm':
			   this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				 break;
			case 'addClassificationGroupForm':
				   this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					 break;
			 		 
			case 'editClassificationForm':
				 this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				 break;
				 
			case 'editClassificationGroupForm':
				   this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					 break;
			case 'deleteClassificationForm':
				 this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				 break;	
			case 'deleteClassificationGroupForm':
				 this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				 break;	
			
			}	
	  }
	  
    var commonLabels = {  
    		classification: '<s:message code="classification.config.classification"/>',
    		classificationGroup: '<s:message code="classification.config.classification.group"/>',
    		hierarachyPath:'<s:message code="classification.config.hierarchyPath"/>',
			  name:'<s:message code="classification.config.name"/>',
			  code:'<s:message code="classification.config.code"/>',
			  acronym:'<s:message code="classification.config.acronym"/>',
			  showIconOnHeader:'<s:message code="classification.config.showIconOnHeader"/>',
			  iconPersist:'<s:message code="classification.config.iconPersist"/>',
			  iconPersistPopupMessageTitle:'<s:message code="classification.config.iconPersistPopupMessageTitle"/>',
			  iconPersistPopupMessageContent:'<s:message code="classification.config.iconPersistPopupMessageContent"/>',
			  addToHeaderList:'<s:message code="classification.config.addToHeaderList"/>',
			  restrictMultipleInstances:'<s:message code="classification.config.restrictMultipleInstances"/>',
			  notManuallyAdded:'<s:message code="classification.config.notManuallyAdded"/>',
			  endReasonMandatory:'<s:message code="classification.config.endReasonMandatory"/>',
			  system:'<s:message code="classification.config.system"/>',
			  statusLocked:'<s:message code="classification.config.statusLocked"/>',
			  activateClassification:'<s:message code="classification.config.activateLabel"/>',
    		proceedClassificationNarrativeMessage:'<s:message code="classification.config.proceed.narrative" javaScriptEscape="true"/>', 
    		addClassificationNarrativeMessage:'<s:message code="classification.config.add.dialog.narrative" javaScriptEscape="true"/>', 
    		addClassificationGroupNarrativeMessage:'<s:message code="classification.group.config.add.dialog.narrative" javaScriptEscape="true"/>', 
    		updateClassificationNarrativeMessage:'<s:message code="classification.config.update.dialog.narrative" javaScriptEscape="true"/>', 
    		updateClassificationGroupNarrativeMessage:'<s:message code="classification.group.config.update.dialog.narrative" javaScriptEscape="true"/>',
    		deleteClassificationNarrativeMessage:'<s:message code="classification.config.delete.dialog.narrative" javaScriptEscape="true"/>', 
    		deleteClassificationGroupNarrativeMessage:'<s:message code="classification.group.config.delete.dialog.narrative" javaScriptEscape="true"/>',
    		viewClassificationNarrativeMessage:'<s:message code="classification.config.view.dialog.narrative" javaScriptEscape="true"/>',
    		viewClassificationGroupNarrativeMessage:'<s:message code="classification.group.config.view.dialog.narrative" javaScriptEscape="true"/>',
        deleteClassificationConfirm: '<s:message code="classification.config.delete.dialog.confirm.message" javaScriptEscape="true"/>',
        deleteClassificationGroupConfirm: '<s:message code="classification.group.config.delete.dialog.confirm.message" javaScriptEscape="true"/>',
        //messages
        msgAddClassificationGroupOnSuccess: '<s:message code="classification.group.config.messages.add.success" javaScriptEscape="true"/>',
        msgAddClassificationOnSuccess: '<s:message code="classification.config.messages.add.success" javaScriptEscape="true"/>',
        msgUpdateClassificationGroupOnSuccess: '<s:message code="classification.group.config.messages.update.success" javaScriptEscape="true"/>',
        msgUpdateClassificationOnSuccess: '<s:message code="classification.config.messages.update.success" javaScriptEscape="true"/>',
        msgDeleteClassificationGroupOnSuccess: '<s:message code="classification.group.config.messages.delete.success" javaScriptEscape="true"/>',
        msgDeleteClassificationOnSuccess: '<s:message code="classification.config.messages.delete.success" javaScriptEscape="true"/>',
        
    },
    
    dialogTitles = {
    		addClassificationProceed:'<s:message code="classification.config.proceed.header"/>',
    		addClassification:'<s:message code="classification.config.add.header"/>',
    		addClassificationGroup:'<s:message code="classification.group.config.add.header"/>',
    		editClassification:'<s:message code="classification.config.update.header"/>',
    		editClassificationGroup:'<s:message code="classification.group.config.update.header"/>',
    		deleteClassification:'<s:message code="classification.config.delete.header"/>',
    		deleteClassificationGroup:'<s:message code="classification.group.config.delete.header"/>',
    		viewClassification:'<s:message code="classification.config.view.header"/>',
    		viewClassificationGroup:'<s:message code="classification.group.config.view.header"/>',
	  },
    
    onSuccessMessages =  {       
    		    addClassificationGroup: '<s:message code="classification.group.config.messages.add.success" javaScriptEscape="true"/>',
    		    addClassification: '<s:message code="classification.config.messages.add.success" javaScriptEscape="true"/>',
    		    editClassificationGroup: '<s:message code="classification.group.config.messages.update.success" javaScriptEscape="true"/>',
            editClassification: '<s:message code="classification.config.messages.update.success" javaScriptEscape="true"/>',
            deleteClassification: '<s:message code="classification.config.messages.delete.success" javaScriptEscape="true"/>',
            deleteClassificationGroup: '<s:message code="classification.group.config.messages.delete.success" javaScriptEscape="true"/>'
  	  };

    var classificationConfigurationDialog = new Y.app.classification.configuration.MultiPanelPopUp({
    	commonLabels : commonLabels,
    	dialogTitles : dialogTitles,
    	onSuccessMessages:onSuccessMessages
    });
    
    classificationConfigurationDialog.render();
    
    
    Y.on('classificationConfig:showDialog', function(e){
      
    	switch(e.action){ 
    	case 'addClassificationProceed':       
	    	  this.showView('addClassificationProceed', {
	          model: new Y.Model(),
	    		  labels: commonLabels,
	    		  otherData: {
	    			  parentGroupId: e.parentGroupId,
	    			  codePath: e.codePath
	    		  }
	        }, showViewFn);    
	         break;
	    	case 'addClassificationGroup':       
	    	  this.showView('addClassificationGroup', {
	          model: new Y.app.classification.configuration.AddClassificationGroupModel(),
	    		  labels: commonLabels,
	    		  otherData: {
	    			  codePath: e.codePath,
	    			  parentGroupId: e.parentGroupId
	    		  }
	        }, showViewFn);    
	         break;
	        
	    	case 'addClassification':       
	      	  this.showView('addClassification', {
	            model:new Y.app.classification.configuration.AddClassificationModel(),
	      		  labels: commonLabels,
	      		  otherData: {
	      			 codePath: e.codePath,
	    			   parentGroupId: e.parentGroupId
	    		  }
	          }, showViewFn);    
	          break;
	          
	    	case 'editClassificationGroup':       
	       	  this.showView('editClassificationGroup', {
	             model: new Y.app.classification.configuration.EditClassificationGroupModel(),
	       		   labels: commonLabels,
	           }, {modelLoad:true, payload:{id:e.id}}, showViewFn);    
	          break;
	    	case 'editClassification':       
	       	  this.showView('editClassification', {
	             model: new Y.app.classification.configuration.EditClassificationModel(),
	       		  labels: commonLabels
	           }, {modelLoad:true, payload:{id:e.id}}, showViewFn);    
	         break;
	    	case 'deleteClassificationGroup':       
	       	  this.showView('deleteClassificationGroup', {
	             model: new Y.app.classification.configuration.DeleteClassificationGroupModel(),
	       		  labels: commonLabels,
	       		  id:e.id
	           }, {modelLoad:true, payload:{id:e.id}}, showViewFn);    
	         break;
	    	case 'deleteClassification':       
	       	  this.showView('deleteClassification', {
	             model: new Y.app.classification.configuration.DeleteClassificationModel(),
	       		  labels: commonLabels,
	       		  id:e.id
	           }, {modelLoad:true, payload:{id:e.id}}, showViewFn);    
	          break;
	    	case 'viewClassification':
	    	    this.showView('viewClassification', {
	    	        model: new Y.app.classification.configuration.ViewClassificationModel(),
	    	        labels: commonLabels,
	    	        id: e.id
	    	    }, { modelLoad: true, payload: { id: e.id } }, showViewFn);
	    	    break;
	    	case 'viewClassificationGroup':
	    	    this.showView('viewClassificationGroup', {
	    	        model: new Y.app.classification.configuration.ViewClassificationGroupModel(),
	    	        labels: commonLabels,
	    	        id: e.id
	    	    }, { modelLoad: true, payload: { id: e.id } }, showViewFn);
	    	    break;
	    	default :
	    	    break;
      
    	};
    	
    }, classificationConfigurationDialog);
  });

</script>