<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasPermission(null, 'FormDefinition','FormDefinition.View')" var="canView" />
<sec:authorize access="hasPermission(null, 'FormDefinition','FormDefinition.Add')" var="canAdd" />
<sec:authorize access="hasPermission(null, 'FormDefinition','FormDefinition.Update')" var="canUpdate" />
<sec:authorize access="hasPermission(null, 'FormDefinition','FormDefinition.Publish')" var="canPublish" />
<sec:authorize access="hasPermission(null, 'FormDefinition','FormDefinition.Archive')" var="canArchive" />
<sec:authorize access="hasPermission(null, 'FormDefinition','FormDefinition.Remove')" var="canRemove" />
<sec:authorize access="hasPermission(null, 'FormMapping','FormMapping.View')" var="canViewMappings" />

<div id="formDefinitionView"></div>

<s:url var="checklistDefinitionAutocompleteUrl" value="/rest/checklistDefinition?name={query}&status=PUBLISHED&s={sortBy}&pageSize={maxResults}&accessLevel=WRITE">
  <s:param name="sortBy" value="[{\"name\":\"asc\"}]" />
  <s:param name="useSoundex" value="false"/>
  <s:param name="appendWildcard" value="true"/>
</s:url>
 
<script>
Y.use('form-definition-controller-view', 'event-custom-base',  function(Y){
  var permissions={
    canAddFormDesign: '${canAdd}'=='true',
    canView: '${canView}'=='true',
    canEdit: '${canUpdate}'=='true',
    canEditDesign: '${canUpdate}'=='true',
    canViewMappings: '${canViewMappings}'=='true',
    canPublish:'${canPublish}'=='true',
    canArchive: '${canArchive}'=='true',
    canRemove:'${canRemove}'=='true'
  };

  var searchConfig={
    sortBy: [{
            name: 'asc'
        }, {
            version: 'desc'
    }],
    labels:{
      name:'<s:message code="formTypes.find.results.name" javaScriptEscape="true" />',
      securityDomain:'<s:message code="formTypes.find.results.securityDomain" javaScriptEscape="true" />',
      version:'<s:message code="formTypes.find.results.version" javaScriptEscape="true" />',
      state:'<s:message code="formTypes.find.results.state" javaScriptEscape="true" />',
      mappings:'<s:message code="formTypes.find.results.mappings" javaScriptEscape="true" />',
      actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',
      viewFormType:'<s:message code="formTypes.find.results.viewFormType" javaScriptEscape="true" />',
      viewFormTypeTitle:'<s:message code="formTypes.find.results.viewFormTypeTitle" javaScriptEscape="true" />',
      editFormType:'<s:message code="formTypes.find.results.editFormType" javaScriptEscape="true" />',
      editFormTypeTitle:'<s:message code="formTypes.find.results.editFormTypeTitle" javaScriptEscape="true" />',
      publishFormType:'<s:message code="formTypes.find.results.publishFormType" javaScriptEscape="true" />',
      publishFormTypeTitle:'<s:message code="formTypes.find.results.publishFormTypeTitle" javaScriptEscape="true" />',
      deleteFormType:'<s:message code="formTypes.find.results.deleteFormType" javaScriptEscape="true" />',
      deleteFormTypeTitle:'<s:message code="formTypes.find.results.deleteFormTypeTitle" javaScriptEscape="true" />',
      removeFormType:'<s:message code="formTypes.find.results.removeFormType" javaScriptEscape="true" />',
      removeFormTypeTitle:'<s:message code="formTypes.find.results.removeFormTypeTitle" javaScriptEscape="true" />',
      viewMappings:'<s:message code="formTypes.find.results.viewMappings" javaScriptEscape="true" />',
      viewMappingsTitle:'<s:message code="formTypes.find.results.viewMappingsTitle" javaScriptEscape="true" />',
      editFormDefinition:'<s:message code="formTypes.find.results.editFormDefinition" javaScriptEscape="true" />',
      editFormDefinitionTitle:'<s:message code="formTypes.find.results.editFormDefinitionTitle" javaScriptEscape="true" />'
    },
    resultsCountNode: Y.one('#formTypeResultsCount'),
    noDataMessage: '<s:message code="formTypes.find.no.results"/>',
    url:'<s:url value="/rest/formDefinition?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
    permissions: permissions
  };

  var filterContextConfig={
          labels: {
              filter: '<s:message code="filter.active" javaScriptEscape="true" />',
              name:'<s:message code="formTypes.find.filter.name.active" javaScriptEscape="true" />',
              status:'<s:message code="formTypes.find.filter.status.active" javaScriptEscape="true" />',
              securityDomain:'<s:message code="formTypes.find.filter.securityDomain.active" javaScriptEscape="true" />',
              resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
              resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
          }
      };
        
  var filterConfig={
    labels: {
        filterBy: '<s:message code="filter.by" javaScriptEscape="true" />',
        name:'<s:message code="formTypes.find.filter.name" javaScriptEscape="true" />',
        status: '<s:message code="formTypes.find.filter.status" javaScriptEscape="true" />',
        securityDomain:'<s:message code="formTypes.find.filter.securityDomain" javaScriptEscape="true" />'
    }
  };

  var viewLabels={
    name:'<s:message code="formDefinitionVO.name" javaScriptEscape="true" />',
    preferAutomatedAdd:'<s:message code="formDefinitionVO.preferAutomatedAdd" javaScriptEscape="true" />',
    securityDomain:'<s:message code="formDefinitionVO.securityDomain" javaScriptEscape="true" />',
    cardinality:'<s:message code="formDefinitionVO.cardinality" javaScriptEscape="true" />',
    subTypes:'<s:message code="formDefinitionVO.subTypes" javaScriptEscape="true" />',
    instancesRequireAuthorisation:'<s:message code="formDefinitionVO.instancesRequireAuthorisation" javaScriptEscape="true" />',
    reviewGeneratesCaseNote:'<s:message code="formDefinitionVO.reviewGeneratesCaseNote" javaScriptEscape="true" />',
    reviewCaseNoteEntryType:'<s:message code="formDefinitionVO.reviewCaseNoteEntryType" javaScriptEscape="true" />',
    outputTemplates:'<s:message code="formDefinitionVO.outputTemplates" javaScriptEscape="true" />',
    relatedChecklistDefinitionId:'<s:message code="updateFormDefinitionVO.relatedChecklistDefinitionId" javaScriptEscape="true" />',
    activityDateControl:'<s:message code="updateFormDefinitionVO.activityDateControl" javaScriptEscape="true" />'
  };
  
  var editLabels={
    name:'<s:message code="updateFormDefinitionVO.name" javaScriptEscape="true" />',
    preferAutomatedAdd:'<s:message code="updateFormDefinitionVO.preferAutomatedAdd" javaScriptEscape="true" />',
    securityDomain:'<s:message code="updateFormDefinitionVO.securityDomain" javaScriptEscape="true" />',
    cardinality:'<s:message code="updateFormDefinitionVO.cardinality" javaScriptEscape="true" />',
    instancesRequireAuthorisation:'<s:message code="updateFormDefinitionVO.instancesRequireAuthorisation" javaScriptEscape="true" />',
    reviewGeneratesCaseNote:'<s:message code="updateFormDefinitionVO.reviewGeneratesCaseNote" javaScriptEscape="true" />',
    reviewCaseNoteEntryType:'<s:message code="updateFormDefinitionVO.reviewCaseNoteEntryType" javaScriptEscape="true" />',
    subTypes:'<s:message code="updateFormDefinitionVO.subTypes" javaScriptEscape="true" />',
    relatedChecklistDefinitionId:'<s:message code="updateFormDefinitionVO.relatedChecklistDefinitionId" javaScriptEscape="true" />',
    checklistDefinitionPlaceholder:'<s:message code="relatedChecklistDefinitionId.autoCompletePlaceholder" javaScriptEscape="true" />',
    activityDateControl:'<s:message code="updateFormDefinitionVO.activityDateControl" javaScriptEscape="true" />'
  };
  
  var headerIcon = 'eclipse-forms',
      narrativeDescription = '{{this.name}}';
      
  var formDefinitionUrl='<s:url value="/rest/formDefinition/{id}"/>',
      formDefinitionStatusUrl='<s:url value="/rest/formDefinition/{id}/status?action={action}"/>';
  
  var dialogConfig={
      views: {
        view: {
          header: {
            text: '<s:message code="formTypes.formType.view.header" javaScriptEscape="true"/>',
            icon: headerIcon
          },
          narrative: {
              summary: '<s:message javaScriptEscape="true" code="formTypes.formType.view.summary"/>',
              description: narrativeDescription
          },
          labels: viewLabels,
          config:{
            url:formDefinitionUrl
          }
        },
        edit:{
          header: {
            text: '<s:message code="formTypes.formType.edit.header" javaScriptEscape="true"/>',
            icon: headerIcon
          },
          narrative: {
              summary: '<s:message javaScriptEscape="true" code="formTypes.formType.edit.summary"/>',
              description: narrativeDescription
          },
          successMessage: '<s:message javaScriptEscape="true" code="formTypes.formType.edit.success"/>',
          labels: editLabels,
          config:{
            url:formDefinitionUrl,            
            validationLabels:{
              securityDomainValidationErrorMessage:'<s:message code="updateFormDefinitionVO.securityDomain.mandatory" javaScriptEscape="true" />',
            },
            checklistDefinitionAutocompleteURL:'${checklistDefinitionAutocompleteUrl}'
          }
        },
        publish:{
          header: {
            text: '<s:message code="formTypes.formType.publish.header" javaScriptEscape="true"/>',
            icon: headerIcon
          },
          narrative: {
              summary: '<s:message javaScriptEscape="true" code="formTypes.formType.publish.summary"/>',
              description: narrativeDescription
          },
          successMessage: '<s:message javaScriptEscape="true" code="formTypes.formType.publish.success"/>',
          labels: {
            message:'<s:message code="formTypes.formType.publish.prompt" javaScriptEscape="true"/>',
            previousAttachedTemplates:'<s:message code="formTypes.previous.outputTemplates" javaScriptEscape="true"/>'
          },
          config:{
            formDefinitionVersionsListUrl:'<s:url value="/rest/formDefinition?formDefinitionId={formDefinitionId}"/>',
            url:formDefinitionStatusUrl,
            action:'published'
          }
        },
        archive:{
          header: {
            text: '<s:message code="formTypes.formType.delete.header" javaScriptEscape="true"/>',
            icon: headerIcon
          },
          narrative: {
              summary: '<s:message javaScriptEscape="true" code="formTypes.formType.delete.summary"/>',
              description: narrativeDescription
          },
          successMessage: '<s:message javaScriptEscape="true" code="formTypes.formType.delete.success"/>',
          labels: {
            message:'<s:message code="formTypes.formType.delete.prompt" javaScriptEscape="true"/>'
          },
          config:{
            formDefinitionVersionsListUrl:'<s:url value="/rest/formDefinition?formDefinitionId={formDefinitionId}"/>',
            url:formDefinitionStatusUrl,
            action:'archived'
          }
        },
        remove:{
          header: {
            text: '<s:message code="formTypes.formType.remove.header" javaScriptEscape="true"/>',
            icon: headerIcon
          },
          narrative: {
              summary: '<s:message javaScriptEscape="true" code="formTypes.formType.remove.summary"/>',
              description: narrativeDescription
          },
          successMessage: '<s:message javaScriptEscape="true" code="formTypes.formType.remove.success"/>',
          labels: {
            message:'<s:message code="formTypes.formType.remove.prompt" javaScriptEscape="true"/>'
          },
          config:{
            formDefinitionVersionsListUrl:'<s:url value="/rest/formDefinition?formDefinitionId={formDefinitionId}"/>',
            url:formDefinitionUrl
          }
        }
      }
  };

  var designerConfig={
    designerURL:'<s:url value="/form/designer"/>'
  };
  
  var config = {
      container: '#formDefinitionView',
      permissions:permissions,
      dialogConfig: dialogConfig,
      searchConfig:searchConfig,
      filterConfig:filterConfig,
      filterContextConfig:filterContextConfig,
      designerConfig:designerConfig,
      dateControlFullDetailsUrl:'<s:url value="/rest/formDefinition/{id}/dateControl?groupScope=true&mandatory=MANDATORY_ON_SAVE&subTypes=DATE,PAST_DATE"/>',
      securityDomainUrl:'<s:url value="/rest/securityDomain?page=1&pageSize=-1&s={sortBy}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
      formMappingsUrl:'<s:url value="/admin/forms/mappings?formDefinitionId={formDefinitionId}" />'
  };
  
  Y.publish('refreshList', { broadcast: 2 });
  
  var formDefinitionView = new Y.app.admin.form.FormDefinitionControllerView(config).render();

  });

	</script>
