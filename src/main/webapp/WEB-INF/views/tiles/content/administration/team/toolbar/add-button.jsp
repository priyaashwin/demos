<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="first" ignore="true"/>
<tilesx:useAttribute name="last" ignore="true"/>

<c:if test="${first eq true }">
  <c:set var="fStyle" value="first"/>
</c:if>

<c:if test="${last eq true }">
  <c:set var="lStyle" value="last"/>
</c:if>

<c:choose>
  <c:when test="${canAdd eq true}">
    <c:set var="addClass" value="pure-button-loading" />
   <c:set var="tabIndex" value="0" />
  </c:when>
  <c:otherwise>
    <c:set var="tabIndex" value="-1"/>
  </c:otherwise>
</c:choose>
<%-- Button markup--%>
<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
  <a href="#none" data-button-action="addTeam" class="pure-button-loading pure-button pure-button-disabled usp-fx-all add-button" aria-disabled="true" tabindex="${tabIndex }">
    <i class="fa fa-plus-circle"></i>
    <span><s:message code="button.add" /></span>
  </a>
</li>

