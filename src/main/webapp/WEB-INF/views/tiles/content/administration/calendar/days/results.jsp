<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%> 

 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'WorkingDay','WorkingDay.View')" var="canViewWD" />
<sec:authorize access="hasPermission(null, 'WorkingDay','WorkingDay.Add')" var="canAddWD" />
<sec:authorize access="hasPermission(null, 'WorkingDay','WorkingDay.Update')" var="canUpdateWD" />
<sec:authorize access="hasPermission(null, 'CalendarCalendarEventBundle','CalendarCalendarEventBundle.View')" var="canViewNWD" />
<sec:authorize access="hasPermission(null, 'CalendarCalendarEventBundle','CalendarCalendarEventBundle.Add')" var="canAddNWD" />
<div id="calendarDaysTabs"></div>

<script>
Y.use('calendar-days-search-view',
	  'calendar-nw-days-search-view',
      'tabbed-page',
      function(Y) {
	var calendarDetails = {
		id: '${calendarId}',
		name: '${esc:escapeJavaScript(calendarName)}'
	};
	
	var workingDaysSearchConfig = {
		labels: {
			day: '<s:message code="calendarDays.find.results.day"/>',
			startTime: '<s:message code="calendarDays.find.results.startTime"/>',
			endTime: '<s:message code="calendarDays.find.results.endTime"/>',
			actions: '<s:message code="calendarDays.find.results.actions"/>',
			editCalendarDay: '<s:message code="calendarDays.find.results.editCalendarDay"/>',
			editCalendarDayTitle: '<s:message code="calendarDays.find.results.editCalendarDayTitle"/>',
			deleteCalendarDay: '<s:message code="calendarDays.find.results.deleteCalendarDay"/>',
			deleteCalendarDayTitle: '<s:message code="calendarDays.find.results.deleteCalendarDayTitle"/>'
		},
		noDataMessage: '<s:message code="calendarDays.find.no.results"/>',
		calendar: calendarDetails,
		url: '<s:url value="/rest/workingDay?calendarId={calendarId}&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
		resultsCountNode:Y.one('#calendarResultsCount'),
		titleNode:Y.one('#calendarHeaderTitle'),
		permissions: {
			canView: '${canViewWD}'==='true',
			canAdd: '${canAddWD}'==='true',
			canUpdate: '${canUpdateWD}'==='true'
		}
	};
	
	var nonWorkingDaysSearchConfig = {
		labels: {
			name: '<s:message code="calendarNWDays.find.results.name"/>',
			description: '<s:message code="calendarNWDays.find.results.description"/>',
			actions: '<s:message code="calendarNWDays.find.results.actions"/>',
			deleteCalendarNWDay: '<s:message code="calendarNWDays.find.results.deleteCalendarNWDay"/>',
			deleteCalendarNWDayTitle: '<s:message code="calendarNWDays.find.results.deleteCalendarNWDayTitle"/>'
		},
		noDataMessage: '<s:message code="calendarNWDays.find.no.results"/>',
		calendar: calendarDetails,
		url: '<s:url value="/rest/calendar/{calendarId}/calendarEventBundle?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
		resultsCountNode:Y.one('#calendarResultsCount'),
		titleNode:Y.one('#calendarHeaderTitle'),
		permissions: {
			canView: '${canViewNWD}'==='true',
			canAdd: '${canAddNWD}'==='true'
		}
	};
	
	var workingDaysDialogConfig = {
		defaultHeader: '<s:message code="calendarDays.dialog.loading"/>',
		addCalendarDaysConfig: {
			headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendarDays.addDialog.title"/></h3>',
			successMessage:'<s:message code="calendarDays.add.success"/>',
			url: '<s:url value="/rest/calendar/{id}/workingDay"/>',
			labels: {
				day: '<s:message code="newWorkingDayVO.day"/>',
				startTime: '<s:message code="newWorkingDayVO.startTime"/>',
				endTime: '<s:message code="newWorkingDayVO.endTime"/>',
				narrative: "<s:message code='calendarDays.add.narrative'/>"
			}
		},
		editCalendarDaysConfig: {
			headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendarDays.editDialog.title"/></h3>',
			successMessage:'<s:message code="calendarDays.edit.success"/>',
			url: '<s:url value="/rest/workingDay/{id}"/>',
			labels: {
				day: '<s:message code="updateWorkingDayVO.day"/>',
				startTime: '<s:message code="updateWorkingDayVO.startTime"/>',
				endTime: '<s:message code="updateWorkingDayVO.endTime"/>',
				narrative: "<s:message code='calendarDays.edit.narrative'/>"
			}
		},
		deleteCalendarDaysConfig: {
			headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendarDays.deleteDialog.title"/></h3>',
			successMessage:'<s:message code="calendarDays.delete.success"/>',
			url: '<s:url value="/rest/workingDay/{id}"/>',
			labels: {
				narrative: "<s:message code='calendarDays.delete.narrative'/>"
			}
		}
	};
	
	var nonWorkingDaysDialogConfig = {
		defaultHeader: '<s:message code="calendarNWDays.dialog.loading"/>',
		addCalendarNWDaysConfig: {
			headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendarNWDays.addDialog.title"/></h3>',
			successMessage:'<s:message code="calendarNWDays.add.success"/>',
			url: '<s:url value="/rest/calendarCalendarEventBundle"/>',
			calendarEventBundleListURL: '<s:url value="/rest/calendarEventBundle?useSoundex=false&appendWildcard=false"/>',
			calendarCalendarEventBundleListURL: '<s:url value="/rest/calendar/{calendarId}/calendarEventBundle"/>',
			labels: {
				eventBundle: '<s:message code="newCalendarCalendarEventBundleVO.calendarEventBundleId"/>',
				narrative: "<s:message code='calendarNWDays.add.narrative'/>"
			}
		},
		deleteCalendarNWDaysConfig: {
			headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendarNWDays.deleteDialog.title"/></h3>',
			successMessage:'<s:message code="calendarNWDays.delete.success"/>',
			url: '<s:url value="/rest/calendarCalendarEventBundle/{id}"/>',
			labels: {
				narrative: "<s:message code='calendarNWDays.delete.narrative'/>"
			}
		}
	};
	
	var workingDayAddTitle = '<s:message code="calendarDays.button.add"/>';
	var nonWorkingDayAddTitle = '<s:message code="calendarNWDays.button.add"/>';
	
	var calendarDaysTabs = new Y.usp.TabbedPage({
	  serverRouting: false,
	  container: '#calendarDaysTabs',
	  views: {
		  calendarWorkingDays: {
			  preserve: true,
			  type: Y.Base.create('calendarWorkingDays', Y.View, [], {
				  render: function() {
					  var calendarWorkingDaysSearch = new Y.app.CalendarDaysSearchView({
							container: this.get('container'),
							searchConfig: workingDaysSearchConfig,
							calendarDaysDialogConfig: workingDaysDialogConfig
					  }).render();
		  
					  Y.delegate('click', function(e) {
						var t = e.currentTarget,
						  enabledDayAdd = !t.hasClass('pure-button-disabled') && t.hasClass('calendar-days-add'),
						  isWorkingDayButton = t.get('title') === workingDayAddTitle;
						e.preventDefault();
						
						if (enabledDayAdd & isWorkingDayButton) {
						  Y.fire('calendarDays:showDialog', { action: 'add' });
						}
					  }, '#sectionToolbar', '.pure-button,li.pure-menu-item a', calendarWorkingDaysSearch);
				  }
			  })
		  },
		  calendarNonWorkingDays: {
			  preserve: true,
			  type: Y.Base.create('calendarNonWorkingDays', Y.View, [], {
				  render: function() {
					  var calendarNonWorkingDaysSearch = new Y.app.CalendarNWDaysSearchView({
							container: this.get('container'),
							searchConfig: nonWorkingDaysSearchConfig,
							calendarNWDaysDialogConfig: nonWorkingDaysDialogConfig
					  }).render();
					  
					  Y.delegate('click', function(e) {
						var t = e.currentTarget,
						  enabledDayAdd = !t.hasClass('pure-button-disabled') && t.hasClass('calendar-days-add'),
						  isNonWorkingDayButton = t.get('title') === nonWorkingDayAddTitle;
						e.preventDefault();
						
						if (enabledDayAdd && isNonWorkingDayButton) {
						  Y.fire('calendarNWDays:showDialog', { action: 'add' });
						}
					  }, '#sectionToolbar', '.pure-button,li.pure-menu-item a', calendarNonWorkingDaysSearch);
				  }
			  })
		  }
	  },
	  tabs: {
		  calendarWorkingDays: {
			  label: 'Working Days',
			  callback: function (req, res, next) {
				  calendarDaysTabs.showView("calendarWorkingDays");
				  Y.fire('calendarDays:tabActive');
				  Y.one('#sectionToolbar .calendar-days-add')
				    .set('title', workingDayAddTitle)
				    .set('aria-lable', workingDayAddTitle);
			  }
		  },
		  calendarNonWorkingDays: {
			  label: 'Non Working Days',
			  callback: function (req, res, next) {
				  calendarDaysTabs.showView("calendarNonWorkingDays");
				  Y.fire('calendarNWDays:tabActive');
				  Y.one('#sectionToolbar .calendar-days-add')
				    .set('title', nonWorkingDayAddTitle)
				    .set('aria-lable', nonWorkingDayAddTitle);
			  }
		  }
	  }
	}).render();
	calendarDaysTabs.activateTab('calendarWorkingDays');
	
	Y.delegate('click', function(e) {
		var t = e.currentTarget,
		  enabledBack = !t.hasClass('pure-button-disabled') && t.hasClass('calendar-days-back');
		e.preventDefault();

		if (enabledBack) {
			window.location = '<s:url value="/admin/calendar"/>';
		}
	}, '#sectionToolbar', '.pure-button,li.pure-menu-item a');  

	Y.all('#sectionToolbar .pure-button-loading')
	  .removeAttribute('aria-disabled')
	  .removeAttribute('disabled')
	  .removeClass('pure-button-disabled')
	  .removeClass('pure-button-loading');
});
</script>