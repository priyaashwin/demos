<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<h1 tabindex="0" class="iconic-title">
	<span class="fa-stack"> 
		<i class="fa fa-circle fa-stack-1x"></i>
		<i class="fa fa-wrench fa-inverse fa-stack-1x"></i>
	</span>
	<span class="no-context">Administration</span>	
</h1>