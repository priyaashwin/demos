<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<sec:authorize access="hasPermission(null, 'SecurityConfiguration','SecurityConfiguration.View')" var="canViewSecurityConfiguration" />

<c:choose>
  <c:when test="${canViewSecurityConfiguration eq true}">
<div id="systemConfigDetails">
	<t:insertTemplate template="/WEB-INF/views/tiles/content/administration/system/config/sections/toggle-accordions.jsp" />
	<%--Inserting dialog for Custom menu item dialog --%>
	<t:insertTemplate template="/WEB-INF/views/tiles/content/administration/system/config/dialogs/custom-menu-items-dialog.jsp" />
	<%-- Insert Administration system config section --%>
	<t:insertTemplate template="/WEB-INF/views/tiles/content/administration/system/config/sections/config-general.jsp" />
</div>
    </c:when>
    <c:otherwise>
      <%-- Insert access denied tile --%>
      <t:insertDefinition name="access.denied" />
  </c:otherwise>
</c:choose>