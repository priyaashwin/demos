<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'CalendarEventBundle','CalendarEventBundle.View')" var="canView" />

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
	<c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
	<c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
	<c:set var="lStyle" value="last" />
</c:if>

<%-- CSS classes and tabIndex to apply if user has ADD permission --%>
<c:choose>
	<c:when test="${canView eq true}">
		<c:set var="holidaysClass" value="pure-button-loading calendar-holidays" />
		<c:set var="tabIndex" value="0" />
	</c:when>
	<c:otherwise>
		<c:set var="tabIndex" value="-1"/>
	</c:otherwise>
</c:choose>

<%-- Button markup--%>
<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
	<a href="#none" class="pure-button pure-button-disabled usp-fx-all ${aStyle} ${holidaysClass}"
		title='<s:message code="calendar.button.holidays"/>' 
		aria-disabled="true"
		aria-label='<s:message code="calendar.button.holidays"/>'
		tabindex="${tabIndex }"> 
		<i class="fa fa-globe"></i>
		<span>
			<s:message code="calendar.button.holidays" />
		</span>
	</a>
</li>
