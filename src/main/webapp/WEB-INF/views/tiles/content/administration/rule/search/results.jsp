<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasPermission(null, 'RuleDefinition','RuleDefinition.View')" var="canView" />
<sec:authorize access="hasPermission(null, 'RuleDefinition','RuleDefinition.Add')" var="canAdd" />
<sec:authorize access="hasPermission(null, 'RuleDefinition','RuleDefinition.Publish')" var="canPublish" />
<sec:authorize access="hasPermission(null, 'RuleDefinition','RuleDefinition.Archive')" var="canArchive" />
<sec:authorize access="hasPermission(null, 'RuleDefinition','RuleDefinition.Remove')" var="canRemove" />

<s:eval expression="@appSettings.isRuleVersionEnabled()" var="versionEnabled" scope="page" />

<div id="ruleResults"></div>

<script>
Y.use('rule-definition-search-view',
  	  'event-custom-base',
  	  function(Y){

    //There is no such thing as an "update" to a rule - we use canAdd for this permission
	var canUpdate = '${canAdd}'==='true',
		canPublish = '${canPublish}'==='true',
		canArchive = '${canArchive}'==='true',
		canRemove = '${canRemove}'==='true',
		versionEnabled='${versionEnabled}'==='true',	

		    
	    searchView=new Y.app.RuleDefinitionSearchView({
			container:'#ruleResults',
			searchConfig:{
				labels:{				
	                name:'<s:message code="ruleDefinition.find.results.name"/>',
	                version:'<s:message code="ruleDefinition.find.results.version"/>',
	                description:'<s:message code="ruleDefinition.find.results.description"/>',
	                state:'<s:message code="ruleDefinition.find.results.state"/>',
	                validationStatus:'<s:message code="ruleDefinition.find.results.validationStatus"/>',
	                actions:'<s:message code="ruleDefinition.find.results.actions"/>',
	                editRule:'<s:message code="ruleDefinition.find.results.editRule"/>',
	                editRuleTitle:'<s:message code="ruleDefinition.find.results.editRuleTitle"/>',
	                publishRule:'<s:message code="ruleDefinition.find.results.publishRule"/>',
	                publishRuleTitle:'<s:message code="ruleDefinition.find.results.publishRuleTitle"/>',
	                deleteRule:'<s:message code="ruleDefinition.find.results.deleteRule"/>',
	                deleteRuleTitle:'<s:message code="ruleDefinition.find.results.deleteRuleTitle"/>',
	                removeRule:'<s:message code="ruleDefinition.find.results.removeRule"/>',
	                removeRuleTitle:'<s:message code="ruleDefinition.find.results.removeRuleTitle"/>'
				},
				noDataMessage:'<s:message code="ruleDefinition.find.no.results"/>',
				versionEnabled:versionEnabled,
				permissions:{
					canUpdate:canUpdate,
					canPublish:canPublish,
					canArchive:canArchive,
					canRemove:canRemove
			    },	
			    resultsCountNode:Y.one('#ruleResultsCount'),
				url:'<s:url value="/rest/ruleDefinition?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
			},
			filterContextConfig:{
				labels:{
	                filter:'<s:message code="filter.active"/>',
	                name:'<s:message code="ruleDefinition.find.filter.name.active"/>',
	                status:'<s:message code="ruleDefinition.find.filter.status.active"/>',
	                	validationStatus: '<s:message code="ruleDefinition.find.filter.validationStatus.active"/>'
	            },
	            container:'#ruleFilterContext'
			},
			filterConfig:{
				labels:{
	                filterBy: '<s:message code="ruleDefinition.find.filter.title"/>',
	                name:'<s:message code="ruleDefinition.find.filter.name"/>',
	                status: '<s:message code="ruleDefinition.find.filter.status"/>',
	                validationStatus: '<s:message code="ruleDefinition.find.filter.validationStatus"/>'
	            }
			},
			ruleDefinitionDialogConfig:{
			    ruleDefinitionStatusURL:'<s:url value="/rest/ruleDefinition/{id}/status?action={action}"/>',
			    ruleDefinitionURL:'<s:url value="/rest/ruleDefinition/{id}"/>',				
				publishConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-balance-scale fa-inverse fa-stack-1x"></i></span> <s:message code="ruleDefinition.publish.header"/></h3>',
				    message:'<s:message code="ruleDefinition.publish.prompt"/>',
				    narrative:'<s:message javaScriptEscape="true" code="ruleDefinition.publish.summary"/>',
				    successMessage:'<s:message javaScriptEscape="true" code="ruleDefinition.publish.success"/>'
				},
				deleteConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-balance-scale fa-inverse fa-stack-1x"></i></span> <s:message code="ruleDefinition.delete.header"/></h3>',
				    message:'<s:message code="ruleDefinition.delete.prompt"/>',
				    narrative:'<s:message javaScriptEscape="true" code="ruleDefinition.delete.summary"/>',
				    successMessage:'<s:message javaScriptEscape="true" code="ruleDefinition.delete.success"/>'
				},
				removeConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-balance-scale fa-inverse fa-stack-1x"></i></span> <s:message code="ruleDefinition.remove.header"/></h3>',
				    message:'<s:message code="ruleDefinition.remove.prompt"/>',
				    narrative:'<s:message javaScriptEscape="true" code="ruleDefinition.remove.summary"/>',
				    successMessage:'<s:message javaScriptEscape="true" code="ruleDefinition.remove.success"/>'
				}
			},
			designerConfig:{
			    designerURL:'<s:url value="/rule/designer"/>'
			}
		}).render();
	    
		//update the filter button style based on visibility of filter
	    searchView.after('ruleDefinitionResultsFilter:visibleChange',function(e){
	        if(e.newVal===true){
	            this.addClass('pure-button-active');
	        }else{
	            this.removeClass('pure-button-active');
	        }
	    },Y.one('#sectionToolbar a.filter'));

	    //attach delegate to toolbar
	    Y.delegate('click', function(e){
	         var t=e.currentTarget,
	         results=searchView.results,
	         resultsFilter=searchView.resultsFilter;
	         
	         //stop default action
	         e.preventDefault();
	         
	       if(t.hasClass('filter')){             
	             //filter button has been clicked
	             if(resultsFilter.get('visible')===false){
	                 resultsFilter.showFilter();                 
	             }else{
	                 resultsFilter.hideFilter();
	             }
	         }
	       
	       if(!t.hasClass('pure-button-disabled')) {
	           if(t.hasClass('rule-designer')){
	               //launch a new window and load the rule designer
	               searchView.launchDesigner();
	           }
	       }
	       
	    },'#sectionToolbar','.pure-button,li.pure-menu-item a', searchView);  
	    
	    Y.publish('refreshList', { broadcast: 2 });
	    
	    Y.Global.on('refreshList', function(e){	        
	    	this.results.reload();    
	    }, searchView);
	    
	    //update style of toolbar buttons to indicate enablement
	    Y.all('#sectionToolbar .pure-button-loading').removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
	});

</script>