<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<h3>Version Details: ${relationshipsrecordingAppVersion.buildVersion} (${relationshipsrecordingAppVersion.buildTime})</h3>	

<div id="versionDetails">
	<ul>
		<li>Environment: <strong>${buildVersionEnvInfo.environment}</strong></li>
		<li>Application dependencies:
			<ul>
				<c:forEach var="dependency"
					items="${buildVersionEnvInfo.dependencyVersions}">
					<li>${dependency.buildTitle} (${dependency.buildTime}): <strong>${dependency.buildVersion}</strong></li>
				</c:forEach>
			</ul>
		</li>
	</ul>
</div>
