<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>

<sec:authorize access="hasPermission(null, 'SecurityRole','SecurityRole.GET')" var="canView"/>
<sec:authorize access="hasPermission(null, 'SecurityRole','SecurityRole.UPDATE')" var="canUpdate"/>


<%-- include web facet picklist template--%>
<t:insertTemplate template="/webjars/facets/webresources/jsp/picklist_template.jsp" flush=""/>

<div id="roleResults"></div>

<script>
Y.use('yui-base',
	'event-delegate',
	'event-custom-base',
	'security-role-search-view', //provided by security-component
	'security-role-dialog-views', //provided by security-component
function(Y){
	var canView = '${canView}'==='true',
		canUpdate = '${canUpdate}'==='true',
		roleSearch=new Y.usp.security.ui.RoleSearchView({
			container:'#roleResults',
			searchConfig:{
				title:'<s:message code="role.find.results" javaScriptEscape="true"/>',
				labels:{
					name:'<s:message code="role.find.results.name" javaScriptEscape="true"/>',
					description:'<s:message code="role.find.results.description" javaScriptEscape="true"/>',
					groupOfRights:'<s:message code="role.find.results.groupOfRights" javaScriptEscape="true"/>',
					active:'<s:message code="role.find.results.active" javaScriptEscape="true"/>',
					viewRole:'<s:message code="role.find.results.view" javaScriptEscape="true"/>',
					editRole:'<s:message code="role.find.results.edit" javaScriptEscape="true"/>',
					activeRole:'<s:message code="role.find.results.activation" javaScriptEscape="true"/>',
					deActiveRole:'<s:message code="role.find.results.deActivation" javaScriptEscape="true"/>',
					actions:'<s:message code="role.find.results.actions" javaScriptEscape="true"/>',
					activeRoleSuccessMessage:'<s:message code="role.active.success.message" javaScriptEscape="true"/>',
					activeRoleFailMessage:'<s:message code="role.active.fail.message" javaScriptEscape="true"/>'
				},
				resultsCountNode:Y.one('#roleResultsCount'),
				noDataMessage:'<s:message code="role.find.no.results" javaScriptEscape="true"/>',
				canView: canView,
				canUpdate: canUpdate,
				url:'<s:url value="/rest/securityRole/?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
			},
			viewConfig:{
				labels:{
					name:'<s:message code="role.view.name" javaScriptEscape="true"/>',
					description:'<s:message code="role.view.description" javaScriptEscape="true"/>',
					groupOfRights:'<s:message code="role.view.groupOfRights" javaScriptEscape="true"/>',
					active:'<s:message code="role.view.active" javaScriptEscape="true"/>',
					system:'<s:message code="role.view.system" javaScriptEscape="true"/>',
					groupOfRights:'<s:message code="role.view.groupOfRights" javaScriptEscape="true"/>',
					noGroupOfRights:'<s:message code="role.view.noGroupOfRights" javaScriptEscape="true"/>',
					viewRoleSummary:'<s:message code="role.view.summary" javaScriptEscape="true"/>'
				},
				headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
					class="fa fa-circle fa-stack-1x"></i><i \
					class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> \
					<s:message code="role.dialog.view.header"/></h3>',
				viewURL:'<s:url value="/rest/securityRole/{id}"/>',
				canUpdate:canUpdate
			},
			editConfig:{
				labels:{
					name:'<s:message code="updateSecurityRoleVO.name" javaScriptEscape="true"/>',
					description:'<s:message code="updateSecurityRoleVO.description" javaScriptEscape="true"/>',
					groupOfRights:'<s:message code="updateSecurityRoleVO.groupOfRights" javaScriptEscape="true"/>',
					active:'<s:message code="updateSecurityRoleVO.active" javaScriptEscape="true"/>',
					editRoleSummary:'<s:message code="role.edit.summary" javaScriptEscape="true" />',
					editRoleSuccessMessage:'<s:message code="role.edit.success.message" javaScriptEscape="true"/>'
				},
				headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
					class="fa fa-circle fa-stack-1x"></i><i \
					class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> \
					<s:message code="role.dialog.edit.header"/></h3>',
				roleURL:'<s:url value="/rest/securityRole/{id}"/>',
				updateURL:'<s:url value="/rest/securityRole/{id}?updateAssociatedGroupOfRights=true"/>',
				gorURL:'<s:url value="/rest/securityGroupOfRights?active=true&s=name&pageNumber=-1&pageSize=-1"/>'
			},
			addConfig:{
				labels:{
					name:'<s:message code="newSecurityRoleVO.name" javaScriptEscape="true"/>',
					description:'<s:message code="newSecurityRoleVO.description" javaScriptEscape="true"/>',
					groupOfRights:'<s:message code="newSecurityRoleVO.groupOfRights" javaScriptEscape="true"/>',
					active:'<s:message code="newSecurityRoleVO.active" javaScriptEscape="true"/>',		
					addRoleSuccessMessage:'<s:message code="role.add.success.message" javaScriptEscape="true"/>',
					addRoleSummary:'<s:message code="securityRole.add.summary" javaScriptEscape="true" />'		
				},
				headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
					class="fa fa-circle fa-stack-1x"></i><i \
					class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> \
					<s:message code="role.dialog.add.header" javaScriptEscape="true"/></h3>',
				gorURL:'<s:url value="/rest/securityGroupOfRights?active=true&s=name&pageNumber=-1&pageSize=-1"/>',
				roleURL:'<s:url value="/rest/securityRole"/>' 
			},
			deActiveConfig:{
				labels:{
					deActiveRoleSummary:'<s:message code="role.deActive.summary" javaScriptEscape="true" />',
					deActiveRoleConfirm:'<s:message code="role.deActive.deActiveConfirm" javaScriptEscape="true" />',
					deActiveRoleSuccessMessage:'<s:message code="role.deActive.success.message" javaScriptEscape="true"/>'	
				},
				headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
					class="fa fa-circle fa-stack-1x"></i><i \
					class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> \
					<s:message code="role.dialog.deActive.header" javaScriptEscape="true"/></h3>',
				deActiveURL:'<s:url value="/rest/securityRole/{id}/status?action=active&active=false" />',
				roleURL:'<s:url value="/rest/securityRole/{id}"/>'
			},
			activeConfig:{
				activeURL:'<s:url value="/rest/securityRole/{id}/status?action=active&active=true" />'
			}
		});
	
	roleSearch.render();
	
});
 
</script>	