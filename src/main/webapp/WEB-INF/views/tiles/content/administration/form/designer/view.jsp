<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasPermission(null, 'FormDefinition','FormDefinition.View')" var="canView" />

<c:choose>
	<c:when test="${canView eq true}">
		<div id="form-designer"></div>
	
		<script>
		  	//render the component
	    	uspFormDesigner.renderDesigner(document.getElementById('form-designer'), {
	    	    id:'${id}',
	    	    labels:{
	    	        //TODO - source these from the resource bundle when development is complete
	    	        rule:{
	        	        name:'Rule name',
		    	        description:'Rule description',
		    	        part:{
		    	            addNewPart:'Add a new part to this expression',
		    	            pleaseChoose:'Please choose',
		    	            blank:'Click here',
		    	            empty:'Empty string',
		    	            save:'Save',
		    	            ok:'OK',
		    	            operandType:'Operand type',
		    	            typeSelectLabel:'Select {type}',
		    	            subTypeSelectLabel:'Select {subType}',
		    	            noSubTypes:'There are no available items, please choose a different type',
		    	            noAttributes:'No items to display.'
		    	        }
	    	        }
	    	    }
	    	}, {
	    	    serverConfig:{
	    	        loadEndpoint:{
	    	            url:'<s:url value="/rest/formDefinition/{id}/design"/>'
	    	        },
	    	     	saveEndpoint:{
	    	     	    url:'<s:url value="/rest/formDefinition"/>'
	    	     	},
	    	     	latestVersionEndpoint:{
	    	     	   url:'<s:url value="/rest/formDefinition"/>'
	    	     	},
	    	     	operandTypeConfigurationEndpoint:{
	    	     	   url:'<s:url value="/rest/ruleConfiguration"/>'
	    	     	},
	    	     	relationshipTypesConfigurationEndpoint:{
	    	     	 url:'<s:url value="/rest/formDesignConfiguration"/>'
	    	     	}
	    	    }
	    	});
		</script>
		
		<script>
		 window.onbeforeunload = function(event)
		    {
		     	if(window.opener && window.opener.Y){
		     	   window.opener.Y.fire('refreshList')
		     	}
		    };
		    
		 	// Listen for the event.
		    document.addEventListener('saveEvent', function (e) {
		     	if(window.opener && window.opener.Y){
		     	   window.opener.Y.fire('refreshList')
		     	}
		    }, false);
		</script>
			        
		<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />
	</c:when>
	<c:otherwise>
		<%-- Insert access denied tile --%>
		<t:insertDefinition name="access.denied"/>
	</c:otherwise>
</c:choose>
