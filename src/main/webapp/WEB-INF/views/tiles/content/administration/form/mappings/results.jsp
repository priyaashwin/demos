<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'FormMapping','FormMapping.View')" var="canView" />
<sec:authorize access="hasPermission(null, 'FormMapping','FormMapping.Update')" var="canEdit"/>
<sec:authorize access="hasPermission(null, 'FormMapping','FormMapping.Add')" var="canAdd"/>
<sec:authorize access="hasPermission(null, 'FormMapping','FormMapping.Delete')" var="canRemove" />

<div id="formMappingResults"></div>
 
<script>
Y.use(  'model',
        'form-mappings-search-view',      
  	  	function(Y){
    
    var canView = '${canView}'==='true',
    	canEdit = '${canEdit}'==='true',
    	canAdd = '${canAdd}'==='true',
    	canRemove='${canRemove}'==='true',
        searchView=new Y.app.FormMappingsSearchView({
			container:'#formMappingResults',
			searchConfig:{
				labels:{				
	                name:'<s:message code="formMappings.find.results.name"/>',
	                direction:'<s:message code="formMappings.find.results.direction"/>',
	                actions:'<s:message code="formMappings.find.results.actions"/>',
	                view:'<s:message code="formMappings.find.results.view"/>',
	                viewTitle:'<s:message code="formMappings.find.results.viewTitle"/>',
	                edit:'<s:message code="formMappings.find.results.edit"/>',
	                editTitle:'<s:message code="formMappings.find.results.editTitle"/>',
	                setPreferred:'<s:message code="formMappings.find.results.setPreferred"/>',
	                setPreferredTitle:'<s:message code="formMappings.find.results.setPreferredTitle"/>',
	                clearPreferred:'<s:message code="formMappings.find.results.clearPreferred"/>',
	                clearPreferredTitle:'<s:message code="formMappings.find.results.clearPreferredTitle"/>',	                
	                remove:'<s:message code="formMappings.find.results.remove"/>',
	                removeTitle:'<s:message code="formMappings.find.results.removeTitle"/>',
	                focus:'<s:message code="formMappings.find.results.focus"/>',
	                focusTitle:'<s:message code="formMappings.find.results.focusTitle"/>'	                

				},
				resultsCountNode:Y.one('#formMappingResultsCount'),
				noDataMessage:'<s:message code="formMappings.find.no.results"/>',
				permissions:{
					canView:canView,
					canEdit:canEdit,
					canAdd:canAdd,
					canRemove:canRemove
			    },
				url:'<s:url value="/rest/formMapping?formDefinitionId={formDefinitionId}&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
			},
			buttonsConfig:{
			    add:{
			        container:'#addFormMappingButton',
			        template:'<a href="#none" class="pure-button usp-fx-all" title="<s:message code="formMappings.add.button.title"/>" aria-label="<s:message code="formMappings.add.button.title.aria"/>"> <i class="fa fa-plus-circle"></i><span><s:message code="button.add" /></span></a>'
			    }
			},
			contextConfig:{
			    container:'#adminContext',
			    template:'{name}',
				//setup Model with initial data from JSP
				//model will then be updated by events where necessary
			    model:new Y.Model({
			        name:'${esc:escapeJavaScript(formDefinition.name)}',
			        formDefinitionId:<c:out value="${formDefinition.id}"/>
			    })			   
			},
			formMappingDialogConfig:{
			    definitionsURL:'<s:url value="/rest/formDefinition"/>',
			    definitionURL:'<s:url value="/rest/formDefinition/{id}"/>',
			    addConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="eclipse-forms fa-inverse fa-stack-1x"></i></span> <s:message code="formMapping.add.header"/></h3>',
				    narrative:'<s:message javaScriptEscape="true" code="formMapping.add.summary"/>',
				    successMessage:'<s:message javaScriptEscape="true" code="formMapping.add.success"/>',
				    url:'<s:url value="/rest/formDefinition/{id}/formMapping"/>',
	    			labels:{
	    			    name:'Name'
					}
				},
				editConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="eclipse-forms fa-inverse fa-stack-1x"></i></span> <s:message code="formMapping.edit.header"/></h3>',
				    narrative:'<s:message javaScriptEscape="true" code="formMapping.edit.summary"/>',
				    successMessage:'<s:message javaScriptEscape="true" code="formMapping.edit.success"/>',
				    url:'<s:url value="/rest/formMapping/{id}"/>',
	    			labels:{
	    			    name:'Name'
					}
				},
				viewConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="eclipse-forms fa-inverse fa-stack-1x"></i></span> <s:message code="formMapping.view.header"/></h3>',
				    narrative:'<s:message javaScriptEscape="true" code="formMapping.view.summary"/>',
				    url:'<s:url value="/rest/formMapping/{id}"/>',
	    			labels:{
	    			    name:'Name'
					}
				},				
				removeConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="eclipse-forms fa-inverse fa-stack-1x"></i></span> <s:message code="formMapping.remove.header"/></h3>',
				    narrative:'<s:message javaScriptEscape="true" code="formMapping.remove.summary"/>',
				    message:'<s:message code="formMapping.remove.confirm"/>',
				    successMessage:'<s:message javaScriptEscape="true" code="formMapping.remove.success"/>',
				    url:'<s:url value="/rest/formMapping/{id}"/>',
	    			labels:{
					}				    
				},
				preferredConfig:{
				    headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="eclipse-forms fa-inverse fa-stack-1x"></i></span> <s:message code="formMapping.preferred.header"/></h3>',
				    successMessage:'<s:message javaScriptEscape="true" code="formMapping.preferred.success"/>',
				    set:{
					    narrative:'<s:message javaScriptEscape="true" code="formMapping.preferred.set.summary"/>',
					    message:'<s:message code="formMapping.preferred.set.confirm"/>',
					    url:'<s:url value="/rest/formMapping/{id}"/>'
  
				    },
				    clear:{
					    narrative:'<s:message javaScriptEscape="true" code="formMapping.preferred.clear.summary"/>',
					    message:'<s:message code="formMapping.preferred.clear.confirm"/>'
				    },
	    			labels:{
					},
				    url:'<s:url value="/rest/formMapping/{id}?preferred={preferred}"/>'
				}
			}
	}).render();    
});
</script>
