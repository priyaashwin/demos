<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
    <c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
    <c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
    <c:set var="lStyle" value="last" />
</c:if>

<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'><a href="#" data-button-action="filter" class="filter pure-button-loading pure-button pure-button-disabled usp-fx-all ${aStyle}" title='<s:message code="filter.button"/> ' aria-disabled="true"  aria-label='<s:message code="filter.button"/>'><i class="fa fa-filter"></i><span><s:message code="button.filter" /></span></a></li>


