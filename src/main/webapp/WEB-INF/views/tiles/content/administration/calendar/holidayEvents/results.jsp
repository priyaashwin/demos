<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%> 

 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'CalendarEvent','CalendarEvent.View')" var="canView" />
<sec:authorize access="hasPermission(null, 'CalendarEvent','CalendarEvent.Add')" var="canAdd" />
<sec:authorize access="hasPermission(null, 'CalendarEvent','CalendarEvent.Update')" var="canUpdate" />
<sec:authorize access="hasPermission(null, 'CalendarEvent','CalendarEvent.Remove')" var="canDelete" />
<div id="holidayEventResults"></div>

<script>
Y.use('holiday-event-controller', function(Y) {

  var permissions = {
    canView: '${canView}' === 'true',
    canAdd: '${canAdd}' === 'true',
    canDelete: '${canDelete}' === 'true',
    canUpdate: '${canUpdate}' === 'true'
  };


  var searchConfig = {
    sortBy: [{
      startDate: 'desc'
    }],
    labels: {
      name: '<s:message code="holidayEvent.find.results.name"/>',
      description: '<s:message code="holidayEvent.find.results.description"/>',
      date: '<s:message code="holidayEvent.find.results.date"/>',
      actions: '<s:message code="common.column.actions"/>',
      editHolidayEventTitle: '<s:message code="holidayEvent.find.results.editHolidayEventTitle"/>',
      editHolidayEvent: '<s:message code="holidayEvent.find.results.editHolidayEvent"/>',
      deleteHolidayEventTitle: '<s:message code="holidayEvent.find.results.deleteHolidayEventTitle"/>',
      deleteHolidayEvent: '<s:message code="holidayEvent.find.results.deleteHolidayEvent"/>'
    },
    resultsCountNode: Y.one('#calendarResultsCount'),
    noDataMessage: '<s:message code="holidayEvent.find.no.results"/>',
    url: '<s:url value="/rest/calendarEventBundle/{holidayId}/calendarEvent?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
    permissions: permissions
  };

  var headerIcon = 'fa fa-wrench';
  var narrativeDescription = '{{this.holiday.name}}';

  var dialogConfig = {
    views: {
      addHolidayEvent: {
        header: {
          text: '<s:message code="holidayEvent.addDialog.title" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="holidayEvent.add.narrative" javaScriptEscape="true" />',
          description: narrativeDescription
        },
        successMessage: '<s:message code="holidayEvent.add.success" javaScriptEscape="true"/>',
        labels: {
          name: '<s:message code="newCalendarEventVO.name"/>',
          description: '<s:message code="newCalendarEventVO.description"/>',
          startDate: '<s:message code="newCalendarEventVO.startDate"/>'
        },
        config: {
          url: '<s:url value="/rest/calendarEventBundle/{holidayId}/calendarEvent"/>',
          validationLabels:{
            date:'<s:message code="holidayEvent.date.required" javaScriptEscape="true" />',
            name:'<s:message code="holidayEvent.name.required" javaScriptEscape="true" />'
          }
        }
      },
      updateHolidayEvent: {
        header: {
          text: '<s:message code="holidayEvent.editDialog.title" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="holidayEvent.edit.narrative" javaScriptEscape="true" />',
          description: narrativeDescription
        },
        successMessage: '<s:message code="holidayEvent.edit.success" javaScriptEscape="true"/>',
        labels: {
          name: '<s:message code="updateCalendarEventVO.name"/>',
          description: '<s:message code="updateCalendarEventVO.description"/>',
          startDate: '<s:message code="updateCalendarEventVO.startDate"/>'
        },
        config: {
          url: '<s:url value="/rest/calendarEvent/{id}"/>',
          validationLabels:{
            date:'<s:message code="holidayEvent.date.required" javaScriptEscape="true" />',
            name:'<s:message code="holidayEvent.name.required" javaScriptEscape="true" />'
          }
        }
      },
      deleteHolidayEvent: {
        header: {
          text: '<s:message code="holidayEvent.deleteDialog.title" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="holidayEvent.delete.narrative" javaScriptEscape="true" />',
          description: narrativeDescription
        },
        successMessage: '<s:message code="holidayEvent.delete.success" javaScriptEscape="true"/>',
        labels: {
          message:'<s:message code="holidayEvent.delete.confirm" javaScriptEscape="true"/>',
        },
        config: {
          url: '<s:url value="/rest/calendarEvent/{id}"/>'
        }
      }
    }
  };

  var holiday = {
    id: '${holidayId}',
    name: '${esc:escapeJavaScript(holidayName)}'
  };


  var config = {
    container: '#holidayEventResults',
    searchConfig: searchConfig,
    permissions: permissions,
    dialogConfig: dialogConfig,
    holiday: holiday
  };

  var holidayEventController = new Y.app.admin.calendar.HolidayEventControllerView(config).render();
});
</script>