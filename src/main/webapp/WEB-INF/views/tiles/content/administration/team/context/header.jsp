<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h3 tabindex="0">
	<span id="teamAdminContextTitle" class="context-title"><s:message code="menu.teamadministration.sectiontitle"/></span>
	<span id="teamAdminResultsCount" class="context-count"></span>
</h3>