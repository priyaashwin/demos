<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasPermission(null, 'ChecklistDefinition','ChecklistDefinition.Add')" var="canAdd" />
<sec:authorize access="hasPermission(null, 'ChecklistDefinition','ChecklistDefinition.View')" var="canView" />
<sec:authorize access="hasPermission(null, 'ChecklistDefinition','ChecklistDefinition.Update')" var="canUpdate" />
<sec:authorize access="hasPermission(null, 'ChecklistDefinition','ChecklistDefinition.Publish')" var="canPublish" />
<sec:authorize access="hasPermission(null, 'ChecklistDefinition','ChecklistDefinition.Archive')" var="canArchive" />
<sec:authorize access="hasPermission(null, 'ChecklistDefinition','ChecklistDefinition.Remove')" var="canRemove" />

<div id="checklistDefinitionView"></div>
 
<script>
Y.use('checklist-definition-controller-view', 'event-custom-base', function(Y) {
  var permissions = {
      canAddChecklistDesign: '${canAdd}' === 'true',
      canView: '${canView}' === 'true',
      canEdit: '${canUpdate}'=='true',
      canEditDesign: '${canUpdate}'=='true',
      canPublish: '${canPublish}' === 'true',
      canArchive: '${canArchive}' === 'true',
      canRemove: '${canRemove}' === 'true'
  };
  
  var searchConfig = {
      sortBy: [{
          name: 'asc'
      }, {
          version: 'desc'
      }],
      labels: {
          name: '<s:message code="checklistDefinition.find.results.name"/>',
          securityDomain:'<s:message code="checklistDefinition.find.results.securityDomain" javaScriptEscape="true" />',
          version: '<s:message code="checklistDefinition.find.results.version"/>',
          state: '<s:message code="checklistDefinition.find.results.state"/>',
          actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',
          viewChecklist: '<s:message code="checklistDefinition.find.results.viewChecklist"/>',
          viewChecklistTitle: '<s:message code="checklistDefinition.find.results.viewChecklistTitle"/>',
          editChecklist: '<s:message code="checklistDefinition.find.results.editChecklist"/>',
          editChecklistTitle: '<s:message code="checklistDefinition.find.results.editChecklistTitle"/>',
          publishChecklist: '<s:message code="checklistDefinition.find.results.publishChecklist"/>',
          publishChecklistTitle: '<s:message code="checklistDefinition.find.results.publishChecklistTitle"/>',
          deleteChecklist: '<s:message code="checklistDefinition.find.results.deleteChecklist"/>',
          deleteChecklistTitle: '<s:message code="checklistDefinition.find.results.deleteChecklistTitle"/>',
          removeChecklist: '<s:message code="checklistDefinition.find.results.removeChecklist"/>',
          removeChecklistTitle: '<s:message code="checklistDefinition.find.results.removeChecklistTitle"/>',
          editChecklistDefinition: '<s:message code="checklistDefinition.find.results.editChecklistDefinition"/>',
          editChecklistDefinitionTitle: '<s:message code="checklistDefinition.find.results.editChecklistDefinitionTitle"/>',
          editOwnership: '<s:message code="checklistDefinition.find.results.editOwnership"/>',
          editOwnershipTitle: '<s:message code="checklistDefinition.find.results.editOwnershipTitle"/>'
      },
      resultsCountNode: Y.one('#checklistResultsCount'),
      noDataMessage: '<s:message code="checklistDefinition.find.no.results"/>',      
      url: '<s:url value="/rest/checklistDefinition?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
      permissions: permissions
  };
  
  var filterContextConfig = {
      labels: {
          filter: '<s:message code="filter.active"/>',
          name: '<s:message code="checklistDefinition.find.filter.name.active"/>',
          securityDomain:'<s:message code="checklistDefinition.find.filter.securityDomain.active" javaScriptEscape="true" />',
          status: '<s:message code="checklistDefinition.find.filter.status.active"/>',
          resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
          resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
      }
  };
  
  var filterConfig = {
      labels: {
          filterBy: '<s:message code="checklistDefinition.find.filter.title"/>',
          name: '<s:message code="checklistDefinition.find.filter.name"/>',
          status: '<s:message code="checklistDefinition.find.filter.status"/>',
          securityDomain:'<s:message code="checklistDefinition.find.filter.securityDomain" javaScriptEscape="true" />'
      }
  };
  
  var viewLabels = {
      name: '<s:message javaScriptEscape="true" code="checklistDefinitionVO.name"/>',
      securityDomain:'<s:message code="checklistDefinitionVO.securityDomain" javaScriptEscape="true" />',
      calculationMode: '<s:message javaScriptEscape="true" code="checklistDefinitionVO.calculationMode"/>',
      calculationModeOptions: {
          fromStart: '<s:message javaScriptEscape="true" code="checklistDefinition.calculationModeOptions.fromStart"/>',
          toEnd: '<s:message javaScriptEscape="true" code="checklistDefinition.calculationModeOptions.toEnd"/>'
      },
      canPause: '<s:message javaScriptEscape="true" code="checklistDefinitionVO.canPause"/>',
      canBackdate: '<s:message javaScriptEscape="true" code="checklistDefinitionVO.canBackdate"/>',
      preferAutomatedAdd:'<s:message code="checklistDefinitionVO.preferAutomatedAdd" javaScriptEscape="true" />',
      cardinality: '<s:message javaScriptEscape="true" code="checklistDefinitionVO.cardinality"/>'
  };
  
  var editLabels = {
      securityDomain:'<s:message code="updateChecklistDefinitionVO.securityDomain" javaScriptEscape="true" />',
      calculationMode: '<s:message javaScriptEscape="true" code="updateChecklistDefinitionVO.calculationMode"/>',
      calculationModeOptions: {
          fromStart: '<s:message javaScriptEscape="true" code="checklistDefinition.calculationModeOptions.fromStart"/>',
          toEnd: '<s:message javaScriptEscape="true" code="checklistDefinition.calculationModeOptions.toEnd"/>'
      },
      canPause: '<s:message javaScriptEscape="true" code="updateChecklistDefinitionVO.canPause"/>',
      canBackdate: '<s:message javaScriptEscape="true" code="updateChecklistDefinitionVO.canBackdate"/>',
      cardinality: '<s:message javaScriptEscape="true" code="updateChecklistDefinitionVO.cardinality"/>',
      preferAutomatedAdd: '<s:message javaScriptEscape="true" code="updateChecklistDefinitionVO.preferAutomatedAdd"/>', 
      multipleInstancesWarning: '<s:message javaScriptEscape="true" code="checklistDefinition.supportsMultiplePerSubject.warning"/>'
  };
  
  var deleteLabels = {
      message: '<s:message code="checklistDefinition.delete.prompt"/>'
  };
  
  var removeLabels = {
      message: '<s:message code="checklistDefinition.remove.prompt"/>'
  };
  
  var publishLabels = {
      message: '<s:message code="checklistDefinition.publish.prompt"/>'
  };
  
  var editOwnershipLabels = {
      addingTeamSearchPlaceholder: '<s:message javaScriptEscape="true" code="checklistDefinition.ownership.addingTeamSearchPlaceholder"/>',
      owningTeamSearchPlaceholder: '<s:message javaScriptEscape="true" code="checklistDefinition.ownership.owningTeamSearchPlaceholder"/>',
      addingTeam: {
          type: '<s:message javaScriptEscape="true" code="checklistDefinition.ownership.addingTeamType"/>',
          any: '<s:message javaScriptEscape="true" code="checklistDefinition.ownership.addingTeam.any"/>',
          specific: '<s:message javaScriptEscape="true" code="checklistDefinition.ownership.addingTeam.specific"/>'
      },
      addingTeamId: '<s:message javaScriptEscape="true" code="checklistTeamOwnershipConfigurationVO.addingTeamId"/>',
      owningTeamId: '<s:message javaScriptEscape="true" code="checklistTeamOwnershipConfigurationVO.owningTeamId"/>',
      teamRelationshipTypeId: '<s:message javaScriptEscape="true" code="checklistTeamOwnershipConfigurationVO.teamRelationshipTypeId"/>'
  };

  var headerIcon = 'fa fa-check-square-o',
      checklistNarrativeDescription='{{this.name}}';
  
  var checklistDefinitionUrl= '<s:url value="/rest/checklistDefinition/{id}"/>',
      checklistDefinitionStatusUrl='<s:url value="/rest/checklistDefinition/{id}/status?action={action}"/>';
      
  var dialogConfig = {
      views: {
          view: {
              header: {
                  text: '<s:message code="checklistDefinition.view.header"/>',
                  icon: headerIcon
              },
              narrative: {
                  summary: '<s:message javaScriptEscape="true" code="checklistDefinition.view.summary"/>',
                  description: checklistNarrativeDescription
              },
              labels: viewLabels,
              config:{
                url:checklistDefinitionUrl
              }
          },
          edit: {
              header: {
                  text: '<s:message code="checklistDefinition.edit.header"/>',
                  icon: headerIcon
              },
              narrative: {
                  summary: '<s:message javaScriptEscape="true" code="checklistDefinition.edit.summary"/>',
                  description: checklistNarrativeDescription
              },
              successMessage: '<s:message javaScriptEscape="true" code="checklistDefinition.edit.success"/>',
              labels: editLabels,
              config:{
                url:checklistDefinitionUrl,
                validationLabels:{
                  securityDomainValidationErrorMessage:'<s:message code="updateChecklistDefinitionVO.securityDomain.mandatory" javaScriptEscape="true" />',
                }
              }
          },
          archive: {
              header: {
                  text: '<s:message code="checklistDefinition.delete.header"/>',
                  icon: headerIcon
              },
              narrative: {
                  summary: '<s:message javaScriptEscape="true" code="checklistDefinition.delete.summary"/>',
                  description: checklistNarrativeDescription
              },
              successMessage: '<s:message javaScriptEscape="true" code="checklistDefinition.delete.success"/>',
              labels: deleteLabels,
              config:{
                url:checklistDefinitionStatusUrl,
                action:'archived'
              }
          },
          remove: {
              header: {
                  text: '<s:message code="checklistDefinition.remove.header"/>',
                  icon: headerIcon
              },
              narrative: {
                  summary: '<s:message javaScriptEscape="true" code="checklistDefinition.remove.summary"/>',
                  description: checklistNarrativeDescription
              },
              successMessage: '<s:message javaScriptEscape="true" code="checklistDefinition.remove.success"/>',
              labels: removeLabels,
              config:{
                url:checklistDefinitionUrl
              }
          },
          publish: {
              header: {
                  text: '<s:message code="checklistDefinition.publish.header"/>',
                  icon: headerIcon
              },
              narrative: {
                  summary: '<s:message javaScriptEscape="true" code="checklistDefinition.publish.summary"/>',
                  description: checklistNarrativeDescription
              },
              successMessage: '<s:message javaScriptEscape="true" code="checklistDefinition.publish.success"/>',
              labels: publishLabels,
              config:{
                url:checklistDefinitionStatusUrl,
                action:'published'
              }
          },
          editOwnership: {
              header: {
                  text: '<s:message code="checklistDefinition.ownership.header"/>',
                  icon: headerIcon
              },
              narrative: {
                  summary: '<s:message javaScriptEscape="true" code="checklistDefinition.ownership.summary"/>',
                  description: checklistNarrativeDescription
              },
              successMessage: '<s:message javaScriptEscape="true" code="checklistDefinition.publish.success"/>',
              labels: editOwnershipLabels,
              config: {
                  autocompleteURL: '<s:url value="/rest/organisation?byOrganisationType=true&organisationTypes=TEAM&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
                  professionalTeamRelationshipTypeURL: '<s:url value="/rest/professionalTeamRelationshipType"/>',
                  url: '<s:url value="/rest/checklistDefinition/{id}?action=teamOwnershipConfiguration"/>'
              }
          }
      }
  };
  
  var designerConfig={
          designerURL:'<s:url value="/checklist/designer"/>'
  };
  
  var config={
    container: '#checklistDefinitionView',
    permissions:permissions,
    dialogConfig: dialogConfig,
    searchConfig: searchConfig,
    filterConfig: filterConfig,
    filterContextConfig: filterContextConfig,
    designerConfig:designerConfig,
    securityDomainUrl:'<s:url value="/rest/securityDomain?page=1&pageSize=-1&s={sortBy}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
  };
  
  var checklistDefinitionView = new Y.app.admin.checklist.ChecklistDefinitionControllerView(config).render();
  
  Y.publish('refreshList', {
      broadcast: 2
  });
});
</script>
