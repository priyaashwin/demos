<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null,'SecurityGroupOfRights','SecurityGroupOfRights.ADD')" var="canAdd"/>

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
	<c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
	<c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
	<c:set var="lStyle" value="last" />
</c:if>

<c:choose>
	<c:when test="${canAdd eq true}">
		<c:set var="addClass" value="pure-button-loading add-groupofrights" />
		
		<%-- Button markup--%>
		<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
			<a href="#none" class="pure-button pure-button-disabled usp-fx-all ${activeStyle} ${addClass}"
				title='<s:message code="groupOfRights.add.button"/>' aria-disabled="true"
				aria-label='<s:message code="groupOfRights.add.button"/>' tabindex=0>
				<i class="fa fa-plus-circle"></i>
				<span><s:message code="button.add" /></span>
			</a>
		</li>
	</c:when>
</c:choose>

