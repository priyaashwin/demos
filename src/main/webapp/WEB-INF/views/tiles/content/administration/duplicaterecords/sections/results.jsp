<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'PersonDuplicate','PersonDuplicate.FindDuplicates')" var="canViewPersonDuplicate" />
<sec:authorize access="hasPermission(null, 'PersonDuplicate','PersonDuplicate.Update')" var="canUpdatePersonDuplicate" />

<div id="duplicateRecordsContainer"></div>

<script>
Y.use('yui-base', 'json-parse', 'duplicaterecords-controller', function(Y) {
     var permissions = {
    	canViewPersonDuplicate: '${canViewPersonDuplicate}' === 'true',
    	canLinkDuplicateRecords: '${canUpdatePersonDuplicate}' === 'true'
     };
     
     new Y.app.admin.duplicaterecords.DuplicateRecordsControllerView({
        container: '#duplicateRecordsContainer',
        permissions: permissions,
        dialogConfig: {
        	views:{
		        link: {
		          header: {
		            text: '<s:message code="duplicaterecords.dialog.link.header" javaScriptEscape="true"/>',
		            icon: 'fa fa fa-wrench'
		          },
		          narrative: {
		            summary: '<s:message code="duplicaterecords.dialog.link.summary" javaScriptEscape="true" />'
		          },
		          successMessage: '<s:message code="duplicaterecords.dialog.link.successMessage" javaScriptEscape="true"/>',
		          labels: {
		        		linkConfirmation: '<s:message code="duplicaterecords.dialog.link.confirmation"/>'
		        	},
		          config: {
		           url: '<s:url value="/rest/person/{id}/duplicates"/>'
		         }
		       },
           unlink: {
		          header: {
			          text: '<s:message code="duplicaterecords.dialog.unlink.header" javaScriptEscape="true"/>',
			          icon: 'fa fa fa-wrench'
			        },
			        narrative: {
			          summary: '<s:message code="duplicaterecords.dialog.unlink.summary" javaScriptEscape="true" />'
			        },
			        successMessage: '<s:message code="duplicaterecords.dialog.unlink.successMessage" javaScriptEscape="true"/>',
			        labels: {
			          unlinkConfirmation: '<s:message code="duplicaterecords.dialog.unlink.confirmation"/>'
			        },
			        config: {
			          url: '<s:url value="/rest/person/{id}/master"/>'
			        }
          }
        }
      },
      duplicateRecordSetsConfig: {
            searchConfig:{
              sortBy: [],
              labels: {
                forename: '<s:message code="duplicaterecords.results.forename"/>',
                surname: '<s:message code="duplicaterecords.results.surname"/>',
                gender: '<s:message code="duplicaterecords.results.gender"/>',
                dateOfBirth: '<s:message code="duplicaterecords.results.dateOfBirth"/>'
              },
              noDataMessage: '<s:message code="duplicaterecords.duplicaterecordsets.results.noData"/>',
              url: '<s:url value="/rest/person/potentialDuplicates?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
            },
            tablePanelConfig:{
                title: '<s:message code="duplicaterecords.duplicaterecordsets.accordion.title" javaScriptEscape="true" />' 
              },
            filterContextConfig: {
            	labels: {
            	  filter: '<s:message code="duplicaterecords.filter.active"/>',
            	  nameOrId: '<s:message code="duplicaterecords.filter.nameOrId"/>'
            	},
            	container: '#duplicaterecordsFilterContext'
            },
            filterConfig: {
            	labels: {
            	  filterBy: '<s:message code="duplicaterecords.filter.title"/>',
            	  name: '<s:message code="duplicaterecords.filter.name" javaScriptEscape="true"/>',
            	  id: '<s:message code="duplicaterecords.filter.id" javaScriptEscape="true"/>',
            	  status: '<s:message code="duplicaterecords.filter.status"/>'
            	}
            },
            toolbarNode:'#sectionToolbar'
          },
        duplicateRecordsConfig: {
        	  searchConfig:{
              sortBy: [
                {id: 'asc'}
              ],
              labels: {
                forename: '<s:message code="duplicaterecords.results.forename"/>',
                surname: '<s:message code="duplicaterecords.results.surname"/>',
                gender: '<s:message code="duplicaterecords.results.gender"/>',
                dateOfBirth: '<s:message code="duplicaterecords.results.dateOfBirth"/>',
                actions: '<s:message code="duplicaterecords.results.actions" javaScriptEscape="true" />',
                linkButtonTitle: '<s:message code="duplicaterecords.link.button"/>',
                linkButtonLabel: '<s:message code="button.link"/>'
              },
              initialMessage: '<s:message code="duplicaterecords.duplicaterecords.results.initialMessage" javaScriptEscape="true" />',
              noDataMessage: '<s:message code="duplicaterecords.duplicaterecords.results.noData"/>',
              url: '<s:url value="/rest/person/{id}/potentialDuplicates?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
            },
            tablePanelConfig:{
                title: '<s:message code="duplicaterecords.duplicaterecords.accordion.title" javaScriptEscape="true" />'
              },
        }
     }).render();

});
</script>