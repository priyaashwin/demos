<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 
<%-- Define permissions --%>
<sec:authorize access="hasPermission(null,'Configuration','CodedEntry.Add')" var="canAdd"/>
<sec:authorize access="hasPermission(null,'Configuration','CodedEntry.Update')" var="canUpdate"/>
<sec:authorize access="hasPermission(null,'CodedEntry','CodedEntry.ACTIVE')" var="canActivate"/>
<sec:authorize access="hasPermission(null,'CodedEntry','CodedEntry.INACTIVE')" var="canDeactivate"/>

<s:url var="checklistDefinitionAutocompleteUrl" value="/rest/checklistDefinition?name={query}&status=PUBLISHED&s={sortBy}&pageSize={maxResults}&accessLevel=WRITE">
  <s:param name="sortBy" value="[{\"name\":\"asc\"}]" />
  <s:param name="useSoundex" value="false"/>
  <s:param name="appendWildcard" value="true"/>
</s:url>

<c:choose>
  <c:when test="${canUpdate eq true}"> 
    <div id="settingsTabs"></div>
    <script>
       Y.use('yui-base', 'settings-coded-entry-view', function(Y) {
        var labels = {
            code: '<s:message code="settings.types.results.code"/>',
            name: '<s:message code="settings.types.results.name"/>',
            description: '<s:message code="settings.types.results.description"/>',
            system: '<s:message code="settings.types.results.system"/>',
            active: '<s:message code="settings.types.results.active"/>',
            actions: '<s:message code="settings.types.results.actions"/>',
            edit: '<s:message code="settings.types.results.edit"/>',
            view: '<s:message code="settings.types.results.view"/>',
            activate: '<s:message code="settings.types.results.activate"/>',
            deactivate: '<s:message code="settings.types.results.deactivate"/>',
            requiresAuthorisation: '<s:message code="settings.types.results.requiresAuthorisation" />',
            addAuthorisation: '<s:message code="settings.types.results.addAuthorisation" />',
            removeAuthorisation:'<s:message code="settings.types.results.removeAuthorisation" />'
          },
          addLabels = {
            code: '<s:message code="newCodedEntryVO.code"/>',
            name: '<s:message code="newCodedEntryVO.name"/>',
            description: '<s:message code="newCodedEntryVO.description"/>',
            system: '<s:message code="newCodedEntryVO.system"/>',
            active: '<s:message code="newCodedEntryVO.active"/>',
            requiresAuthorisation: '<s:message code="newCodedEntryVO.requiresAuthorisation" />'
          },
          viewLabels = {
            code: '<s:message code="viewCodedEntryVO.code"/>',
            name: '<s:message code="viewCodedEntryVO.name"/>',
            description: '<s:message code="viewCodedEntryVO.description"/>',
            system: '<s:message code="viewCodedEntryVO.system"/>',
            active: '<s:message code="viewCodedEntryVO.active"/>',
            statusLocked: '<s:message code="viewCodedEntryVO.statusModifiable"/>',
            descriptiveLock: '<s:message code="viewCodedEntryVO.descriptiveLock"/>',
            requiresAuthorisation: '<s:message code="viewCodedEntryVO.requiresAuthorisation"/>'
          },
          editLabels = {
            code: '<s:message code="updateCodedEntryVO.code"/>',
            name: '<s:message code="updateCodedEntryVO.name"/>',
            description: '<s:message code="updateCodedEntryVO.description"/>',
            system: '<s:message code="updateCodedEntryVO.system"/>',
            active: '<s:message code="updateCodedEntryVO.active"/>',
            statusLocked: '<s:message code="viewCodedEntryVO.statusModifiable"/>',
            descriptiveLock: '<s:message code="viewCodedEntryVO.descriptiveLock"/>',
            requiresAuthorisation: '<s:message code="viewCodedEntryVO.requiresAuthorisation" />'
          },
          activateLabels = {
            confirm: '<s:message code="settings.entries.activation.confirm"/>'
          },
          deactivateLabels = {
            confirm: '<s:message code="settings.entries.deactivation.confirm"/>'
          },
          addAuthorisationLabels = {
            confirm: '<s:message code="settings.entries.addAuthorisation.confirm"/>'
          },
          removeAuthorisationLabels = {
            confirm: '<s:message code="settings.entries.removeAuthorisation.confirm"/>'
          };

        var codedEntryConfig = {
          labels: labels,
          url: '<s:url value="/rest/codedEntry?categoryCode={categoryCode}&activeOnly=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
          addButtonLabel: '<s:message code="settings.codedEntry.add.button"/>'
        };

        var codedSubEntryConfig = {
          labels: labels,
          url: '<s:url value="/rest/codedEntry/{id}/child?activeOnly=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
          addButtonLabel: '<s:message code="settings.codedEntry.add.button"/>'
        };

        var dialogConfig = {
          defaultHeader: 'loading...',
          views: {
            addCodedEntry: {
              labels: addLabels,
              url: '<s:url value="/rest/category/{category}/codedEntry"/>',
              successMessage: '<s:message code="settings.codedEntry.add.success" javaScriptEscape="true"/>',
              narrative: {
                summary: '<s:message code="settings.codedEntry.add.summary" javaScriptEscape="true"/>'
              }
            },
            addCodedSubEntry: {
              labels: addLabels,
              url: '<s:url value="/rest/codedEntry/{parentCodeId}/codedSubentry"/>',
              successMessage: '<s:message code="settings.codedEntry.add.success" javaScriptEscape="true"/>',
              narrative: {
                summary: '<s:message code="settings.codedSubEntry.add.summary" javaScriptEscape="true"/>'
              }
            },
            editCodedEntry: {
              labels: editLabels,
              url: '<s:url value="/rest/codedEntry/{id}"/>',
              successMessage: '<s:message code="settings.codedEntry.edit.success" javaScriptEscape="true"/>',
              narrative: {
                summary: '<s:message code="settings.codedEntry.edit.summary" javaScriptEscape="true"/>'
              }
            },
            viewCodedEntry: {
              labels: viewLabels,
              url: '<s:url value="/rest/codedEntry/{id}"/>',
              narrative: {
                summary: '<s:message code="settings.codedEntry.view.summary" javaScriptEscape="true"/>'
              }
            },
            activateCodedEntry: {
              labels: activateLabels,
              url: '<s:url value="/rest/codedEntry/{id}/status?action=active"/>',
              successMessage: '<s:message code="settings.codedEntry.activate.success" javaScriptEscape="true"/>',
              narrative: {
                summary: '<s:message code="settings.codedEntry.activate.summary" javaScriptEscape="true"/>'
              }
            },
            deactivateCodedEntry: {
              labels: deactivateLabels,
              url: '<s:url value="/rest/codedEntry/{id}/status?action=inactive"/>',
              successMessage: '<s:message code="settings.codedEntry.deactivate.success" javaScriptEscape="true"/>',
              narrative: {
                summary: '<s:message code="settings.codedEntry.deactivate.summary" javaScriptEscape="true"/>'
              }
            },
            addAuthorisation: {
               labels: addAuthorisationLabels,
               url: '<s:url value="/rest/codedEntry/{id}?requireAuthorisation=true"/>',
               successMessage: '<s:message code="settings.codedEntry.addAuthorisation.success" javaScriptEscape="true"/>',
               narrative: {
                 summary: '<s:message code="settings.codedEntry.addAuthorisation.summary" javaScriptEscape="true"/>'
               }
            },
            removeAuthorisation: {
               labels: removeAuthorisationLabels,
               url: '<s:url value="/rest/codedEntry/{id}?requireAuthorisation=false"/>',
               successMessage: '<s:message code="settings.codedEntry.removeAuthorisation.success" javaScriptEscape="true"/>',
               narrative: {
                 summary: '<s:message code="settings.codedEntry.removeAuthorisation.summary" javaScriptEscape="true"/>'
               }
            }
          }
        };
        var permissions = {
          canView: true,
          canAdd: false,
          canUpdate: '${canUpdate}' === 'true',
          canActivate: '${canActivate}' === 'true',
          canDeactivate: '${canDeactivate}' === 'true'
        };
        
        var settingsTabs = new Y.app.SettingsCodedEntryTabsView({
          root: '<s:url value="/admin/"/>',
          container: '#settingsTabs',
          tabs: {
            caseNoteEntryType: {
              label: '<s:message code="settings.casenote.tabs.type"/>',
              config: {
                category: '<c:out value="${caseNoteEntryTypeCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${caseNoteEntryTypeCategoryCode}"/>',
                  title: '<s:message code="settings.casenote.type.title"/>',
                  noDataMessage: '<s:message code="settings.casenote.type.find.no.results"/>',
                  supportsAuthorisation: 'true' === '${caseNoteEntryTypeSupportsAuthorisation}'
                }),
                codedSubEntryConfig: Y.merge(codedSubEntryConfig, {
                  initialMessage: '<s:message code="settings.casenote.subtype.initialMessage"/>',
                  noDataMessage: '<s:message code="settings.casenote.subtype.find.no.results"/>',
                  title: '<s:message code="settings.casenote.subtype.title"/>',
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && caseNoteEntryTypeAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            chronologyEntryType: {
              label: '<s:message code="settings.tabs.chronology"/>',
              config: {
                category: '<c:out value="${chronologyEntryTypeCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${chronologyEntryTypeCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.chronology.find.no.results"/>',
                  title: '<s:message code="settings.chronology.title"/>',
                  supportsAuthorisation: 'true' === '${chronologyEntryTypeSupportsAuthorisation}'
                }),
                codedSubEntryConfig: Y.merge(codedSubEntryConfig, {
                  initialMessage: '<s:message code="settings.chronology.subtype.initialMessage"/>',
                  noDataMessage: '<s:message code="settings.chronology.subtype.find.no.results"/>',
                  title: '<s:message code="settings.chronology.subtype.title"/>',
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && chronologyEntryTypeAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            personReferenceType: {
              label: '<s:message code="settings.tabs.personref"/>',
              config: {
                category: '<c:out value="${personReferenceTypeCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${personReferenceTypeCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.personref.find.no.results"/>',
                  title: '<s:message code="settings.personref.title"/>',
                  supportsAuthorisation: 'true' === '${personReferenceTypeSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && personReferenceTypeAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            personWarnings: {
              label: '<s:message code="settings.tabs.personwarnings"/>',
              config: {
                category: '<c:out value="${personWarningsCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${personWarningsCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.personwarnings.find.no.results"/>',
                  title: '<s:message code="settings.personwarnings.title"/>',
                  supportsAuthorisation: 'true' === '${personWarningsSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && personWarningsAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig,
                warningRelatedChecklistDefinitionConfig: {
                  dialogConfig: {
                    views: {
                      edit: {
                        header: {
                          text:'<s:message code="settings.personwarnings.warningRelatedChecklist.header" javaScriptEscape="true"/>',
                          icon:'fa fa-wrench'
                        },
                        successMessage: '<s:message code="settings.personwarnings.warningRelatedChecklist.success" javaScriptEscape="true"/>',
                        narrative: {
                          summary: '<s:message code="settings.personwarnings.warningRelatedChecklist.summary" javaScriptEscape="true"/>'
                        },
                        labels: {
                            title: '<s:message code="settings.personwarnings.warningRelatedChecklist.title" javaScriptEscape="true" />',
                            editWarningRelatedChecklistDefinitionSummary: '<s:message code="settings.personwarnings.warningRelatedChecklist.summary" javaScriptEscape="true" />',
                            placeholder: '<s:message code="settings.personwarnings.warningRelatedChecklist.placeholder" javaScriptEscape="true" />'
                          },
                        getUrl: '<s:url value="/rest/warningRelatedChecklistDefinition"/>',
                        setUrl: '<s:url value="/rest/warningRelatedChecklistDefinition?definitionId={definitionId}"/>',
                        listUrl: '<s:url value="/rest/checklistDefinition?status=PUBLISHED"/>',
                        checklistDefinitionAutocompleteURL:'${checklistDefinitionAutocompleteUrl}',                        		
                      }
                    }
                  },
                  labels: {
                   title: '<s:message code="settings.personwarnings.warningRelatedChecklist.title" javaScriptEscape="true" />',
                    worklist: '<s:message code="settings.personwarnings.warningRelatedChecklist.worklist"/>',
                    edit: '<s:message code="settings.personwarnings.warningRelatedChecklist.edit"/>'
                  },
                  title: '<s:message code="settings.personwarnings.warningRelatedChecklist.title" javaScriptEscape="true" />'
                }
              }
            },
            personAlerts: {
              label: '<s:message code="settings.tabs.personAlerts"/>',
              config: {
                category: '<c:out value="${personAlertsCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${personAlertsCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.personAlerts.find.no.results"/>',
                  title: '<s:message code="settings.personAlerts.title"/>',
                  supportsAuthorisation: 'true' === '${personAlertsSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && personAlertsAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            checklistPauseReason: {
              label: '<s:message code="settings.tabs.checklistPauseReason"/>',
              config: {
                category: '<c:out value="${checklistPauseReasonCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${checklistPauseReasonCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.checklistPauseReason.find.no.results"/>',
                  title: '<s:message code="settings.checklistPauseReason.title"/>',
                  supportsAuthorisation: 'true' === '${checklistPauseReasonSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && checklistPauseReasonAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            personProfessionalTitle: {
              label: '<s:message code="settings.tabs.professionalTitle"/>',
              config: {
                category: '<c:out value="${personProfessionalTitleCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${personProfessionalTitleCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.professionalTitle.find.no.results"/>',
                  title: '<s:message code="settings.professionalTitle.title"/>',
                  supportsAuthorisation: 'true' === '${personProfessionalSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && personProfessionalTitleAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            trainingCourseCode: {
              label: '<s:message code="settings.tabs.trainingCourseCode"/>',
              config: {
                category: '<c:out value="${trainingCourseCodeCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${trainingCourseCodeCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.trainingCourseCode.find.no.results"/>',
                  title: '<s:message code="settings.trainingCourseTitle.title"/>',
                  supportsAuthorisation: 'true' === '${trainingCourseCodeSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && trainingCourseCodeAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            typeOfCare: {
              label: '<s:message code="settings.tabs.typeOfCare"/>',
              config: {
                category: '<c:out value="${typeOfCareCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${typeOfCareCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.typeOfCare.find.no.results"/>',
                  title: '<s:message code="settings.typeOfCare.title"/>',
                  supportsAuthorisation: 'true' === '${typeOfCareSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && typeOfCareAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            reasonForUnavailability: {
              label: '<s:message code="settings.tabs.reasonForUnavailability"/>',
              config: {
                category: '<c:out value="${reasonForUnavailabilityCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${reasonForUnavailabilityCategoryCode}"/>',
                  noDataMessage: '<s:message code="settings.reasonForUnavailability.find.no.results"/>',
                  title: '<s:message code="settings.reasonForUnavailability.title"/>',
                  supportsAuthorisation: 'true' === '${reasonForUnavailabilitySupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && reasonForUnavailabilityAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            outputCategory: {
              label: '<s:message code="settings.tabs.outputCategory"/>',
              config: {
                category: '<c:out value="${outputCategoryCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${outputCategoryCategoryCode}"/>',
                  title: '<s:message code="settings.outputCategory.title"/>',
                  noDataMessage: '<s:message code="settings.outputCategory.find.no.results"/>',
                  supportsAuthorisation: 'true' === '${outputCategorySupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && outputCategoryAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            sourceOrganisation: {
              label: '<s:message code="settings.tabs.sourceOrganisation"/>',
              config: {
                category: '<c:out value="${sourceOrganisationCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${sourceOrganisationCategoryCode}"/>',
                  title: '<s:message code="settings.sourceOrganisation.title"/>',
                  noDataMessage: '<s:message code="settings.sourceOrganisation.find.no.results"/>',
		              supportsAuthorisation: 'true' === '${sourceOrganisationSupportsAuthorisation}'
                }),
                codedSubEntryConfig: Y.merge(codedSubEntryConfig, {
                  initialMessage: '<s:message code="settings.source.initialMessage"/>',
                  noDataMessage: '<s:message code="settings.source.find.no.results"/>',
                  title: '<s:message code="settings.source.title"/>',
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && sourceOrganisationAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            nonVerbalLanguages: {
              label: '<s:message code="settings.tabs.nonVerbalLanguages"/>',
              config: {
                category: '<c:out value="${nonVerbalLanguageCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${nonVerbalLanguageCategoryCode}"/>',
                  title: '<s:message code="settings.nonVerbalLanguages.title"/>',
                  noDataMessage: '<s:message code="settings.nonVerbalLanguages.find.no.results"/>',
                  supportsAuthorisation: 'true' === '${nonVerbalLanguageSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && nonVerbalLanguageAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            belongingsType: {
              label: '<s:message code="settings.tabs.belongingsType"/>',
              config: {
                category: '<c:out value="${belongingsTypeCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${belongingsTypeCategoryCode}"/>',
                  title: '<s:message code="settings.belongingsType.title"/>',
                  noDataMessage: '<s:message code="settings.belongingsType.find.no.results"/>',
                  supportsAuthorisation: 'true' === '${belongingsTypeSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && belongingsTypeAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            belongingsLocation: {
              label: '<s:message code="settings.tabs.belongingsLocation"/>',
              config: {
                category: '<c:out value="${belongingsLocationCategoryId}"/>',
                codedEntryConfig: Y.merge(codedEntryConfig, {
                  categoryCode: '<c:out value="${belongingsLocationCategoryCode}"/>',
                  title: '<s:message code="settings.belongingsLocation.title"/>',
                  noDataMessage: '<s:message code="settings.belongingsLocation.find.no.results"/>',
                  supportsAuthorisation: 'true' === '${belongingsLocationSupportsAuthorisation}'
                }),
                permissions: Y.merge(permissions, {
                  canAdd: '${canAdd && belongingsLocationAdditionsAllowed}' === 'true'
                }),
                dialogConfig: dialogConfig
              }
            },
            classificationEndReason: {
                label: '<s:message code="settings.tabs.classificationEndReason"/>',
                config: {
                  category: '<c:out value="${classificationEndReasonCategoryId}"/>',
                  codedEntryConfig: Y.merge(codedEntryConfig, {
                    categoryCode: '<c:out value="${classificationEndReasonCategoryCode}"/>',
                    title: '<s:message code="settings.classificationEndReason.title"/>',

                    noDataMessage: '<s:message code="settings.classificationEndReason.find.no.results"/>'
                  }),
                  permissions: Y.merge(permissions, {
                    canAdd: '${canAdd && classificationEndReasonAdditionsAllowed}' === 'true'
                  }),
                  dialogConfig: dialogConfig
                }
              },
              typeOfDisability: {
                  label: '<s:message code="settings.tabs.typeOfDisability"/>',
                  config: {
                    category: '<c:out value="${typeOfDisabilityCategoryId}"/>',
                    codedEntryConfig: Y.merge(codedEntryConfig, {
                      categoryCode: '<c:out value="${typeOfDisabilityCategoryCode}"/>',
                      noDataMessage: '<s:message code="settings.typeOfDisability.find.no.results"/>',
                      title: '<s:message code="settings.typeOfDisability.title"/>',
                      supportsAuthorisation: 'true' === '${typeOfDisabilitySupportsAuthorisation}'
                    }),
                    permissions: Y.merge(permissions, {
                      canAdd: '${canAdd && typeOfDisabilityAdditionsAllowed}' === 'true'
                    }),
                    dialogConfig: dialogConfig
                  }
                }
          }
        }).render();

        
        // Make sure to dispatch the current hash-based URL which was set by
        // the server to our route handlers.
        if (settingsTabs.hasRoute(settingsTabs.getPath())) {
          settingsTabs.dispatch();
        } else {
          settingsTabs.activateTab('caseNoteEntryType');
        }
      });
  </script>
  </c:when>
  <c:otherwise>
    <%-- Insert access denied tile --%>
    <t:insertDefinition name="access.denied"/>
  </c:otherwise>
</c:choose>

