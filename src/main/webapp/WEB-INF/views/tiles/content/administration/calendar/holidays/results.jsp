<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'CalendarEventBundle','CalendarEventBundle.View')" var="canView" />
<sec:authorize access="hasPermission(null, 'CalendarEventBundle','CalendarEventBundle.Update')" var="canUpdate" />
<sec:authorize access="hasPermission(null, 'CalendarEventBundle','CalendarEventBundle.Add')" var="canAdd" />
<sec:authorize access="hasPermission(null, 'CalendarEventBundle','CalendarEventBundle.Activate')" var="canActivate" />
<sec:authorize access="hasPermission(null, 'CalendarEventBundle','CalendarEventBundle.Remove')" var="canDelete" />
<sec:authorize access="hasPermission(null, 'Calendar','Calendar.View')" var="canViewCalendar" />
<div id="calendarHolidaysResults"></div>

<script>
Y.use('holiday-controller', function(Y) {
  var permissions = {
    canView: '${canView}' === 'true',
    canViewCalendar: '${canViewCalendar}' === 'true',
    canAdd: '${canAdd}' === 'true',
    canActivate: '${canActivate}' === 'true',
    canDelete: '${canDelete}' === 'true',
    canUpdate: '${canUpdate}' === 'true'
  };

  var searchConfig = {
    sortBy: [{
      name: 'asc'
    }],
    labels: {
      name: '<s:message code="calendarHolidays.find.results.name"/>',
      description: '<s:message code="calendarHolidays.find.results.description"/>',
      status: '<s:message code="calendarHolidays.find.results.state"/>',
      actions: '<s:message code="common.column.actions"/>',
      viewHoliday: '<s:message code="calendarHolidays.find.results.viewHoliday"/>',
      viewHolidayTitle: '<s:message code="calendarHolidays.find.results.viewHolidayTitle"/>',
      editHoliday: '<s:message code="calendarHolidays.find.results.editHoliday"/>',
      editHolidayTitle: '<s:message code="calendarHolidays.find.results.editHolidayTitle"/>',
      activateHoliday: '<s:message code="calendarHolidays.find.results.activateHoliday"/>',
      activateHolidayTitle: '<s:message code="calendarHolidays.find.results.activateHolidayTitle"/>',
      deleteHoliday: '<s:message code="calendarHolidays.find.results.deleteHoliday"/>',
      deleteHolidayTitle: '<s:message code="calendarHolidays.find.results.deleteHolidayTitle"/>'
    },
    resultsCountNode: Y.one('#calendarResultsCount'),
    noDataMessage: '<s:message code="calendarHolidays.find.no.results"/>',
    url: '<s:url value="/rest/calendarEventBundle?useSoundex=false&appendWildcard=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
    permissions: permissions
  };

  var headerIcon = 'fa fa-wrench';
  var narrativeDescription = '{{this.name}}';


  var dialogConfig = {
    views: {
      addHoliday: {
        header: {
          text: '<s:message code="calendarHolidays.addDialog.title" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="calendarHolidays.add.narrative" javaScriptEscape="true" />'
        },
        successMessage: '<s:message code="calendarHolidays.add.success" javaScriptEscape="true"/>',
        labels: {
          name: '<s:message code="newCalendarEventBundleVO.name"/>',
          description: '<s:message code="newCalendarEventBundleVO.description"/>'
        },
        config: {
          url: '<s:url value="/rest/calendarEventBundle"/>'
        }
      },
      updateHoliday: {
        header: {
          text: '<s:message code="calendarHolidays.editDialog.title" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="calendarHolidays.edit.narrative" javaScriptEscape="true" />',
          description: narrativeDescription
        },
        successMessage: '<s:message code="calendarHolidays.edit.success" javaScriptEscape="true"/>',
        labels: {
          name: '<s:message code="updateCalendarEventBundleVO.name"/>',
          description: '<s:message code="updateCalendarEventBundleVO.description"/>'
        },
        config: {
          url: '<s:url value="/rest/calendarEventBundle/{id}"/>'
        }
      },
      activateHoliday: {
        header: {
          text: '<s:message code="calendarHolidays.activateDialog.title" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="calendarHolidays.activate.narrative" javaScriptEscape="true" />',
          description: narrativeDescription
        },
        successMessage: '<s:message code="calendarHolidays.activate.success" javaScriptEscape="true"/>',
        labels: {
          message: '<s:message code="calendarHolidays.activate.confirm" javaScriptEscape="true"/>',
        },
        config: {
          url: '<s:url value="/rest/calendarEventBundle/{id}"/>',
          updateUrl: '<s:url value="/rest/calendarEventBundle/{id}/status?action=active"/>'
        }
      },
      deleteHoliday: {
        header: {
          text: '<s:message code="calendarHolidays.deleteDialog.title" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="calendarHolidays.delete.narrative" javaScriptEscape="true" />',
          description: narrativeDescription
        },
        successMessage: '<s:message code="calendarHolidays.delete.success" javaScriptEscape="true"/>',
        labels: {
          message: '<s:message code="calendarHolidays.delete.confirm" javaScriptEscape="true"/>',
        },
        config: {
          url: '<s:url value="/rest/calendarEventBundle/{id}"/>'
        }
      }
    }
  };

  var config = {
    container: '#calendarHolidaysResults',
    searchConfig: searchConfig,
    permissions: permissions,
    dialogConfig: dialogConfig,
    viewURL: '<s:url value="/admin/calendar/holidays/events?holidayId={id}"/>',
    viewCalendarURL: '<s:url value="/admin/calendar"/>',
  };


  var holidayController = new Y.app.admin.calendar.HolidayControllerView(config).render();
});
</script>
