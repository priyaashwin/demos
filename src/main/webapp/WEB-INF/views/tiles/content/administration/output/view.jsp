<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'OutputTemplate','OutputTemplate.Get')" var="canViewOutput"/>
<sec:authorize access="hasPermission(null, 'FormOutputTemplate','FormOutputTemplate.View')" var="canViewFormOutput"/>
<sec:authorize access="hasPermission(null, 'OutputTemplate','OutputTemplate.Add')" var="canAddOutput"/>
<sec:authorize access="hasPermission(null, 'FormOutputTemplate','FormOutputTemplate.Add')" var="canAddFormOutput"/>
<sec:authorize access="hasPermission(null, 'OutputTemplate','OutputTemplate.Update')" var="canUpdateOutput"/>
<sec:authorize access="hasPermission(null, 'FormOutputTemplate','FormOutputTemplate.Update')" var="canUpdateFormOutput"/>

<%--  Publish permission requires - can  update  --%>
<sec:authorize access="hasPermission(null, 'OutputTemplate','OutputTemplate.Update')" var="canPublishOutput" />
<sec:authorize access="hasPermission(null, 'FormOutputTemplate','FormOutputTemplate.Update')" var="canPublishFormOutput" />
<%--  Archive permission requires - can  update  --%>
<sec:authorize access="hasPermission(null, 'OutputTemplate','OutputTemplate.Update')" var="canArchiveOutput" />
<sec:authorize access="hasPermission(null, 'FormOutputTemplate','FormOutputTemplate.Update')" var="canArchiveFormOutput" />
<%--  Remove permission required - can delete  --%>
<sec:authorize access="hasPermission(null, 'OutputTemplate','OutputTemplate.Delete')" var="canRemove" />
<c:choose>
    <c:when test="${canViewOutput eq true and canViewFormOutput eq true}">
     <div id="outputTabs"></div>

     <script>
     Y.use('output-tabs', function(Y) {
         var outputTemplateIcon = 'fa fa-file';

         //Common configuration - we will clone/mix into this as required
         var outputFormats = {
                 PDF: '<s:message code="outputTemplate.supportedFormats.PDF"/>',
                 DOC: '<s:message code="outputTemplate.supportedFormats.DOC"/>',
                 ODT: '<s:message code="outputTemplate.supportedFormats.ODT"/>'
             },
             commonConfig = {
                 //selector to the toolbar containing filter, generate and add buttons
                 toolbarNode: '#sectionToolbar',
                 searchConfig: {
                     sortBy: [{
                         outputName: 'desc'
                     }],
                     labels: {
                         name: '<s:message code="outputTemplate.results.name"/>',
                         description: '<s:message code="outputTemplate.results.description"/>',
                         status: '<s:message code="outputTemplate.results.status"/>',
                         outputCategory:'<s:message code="outputTemplate.results.category"/>',
                         useForSubjectAccessRequest:'<s:message code="outputTemplate.results.useForSubjectAccessRequest"/>',
                         actions: '<s:message code="outputTemplate.results.actions"/>',
                         viewTemplate: '<s:message code="outputTemplate.results.viewTemplate"/>',
                         viewTemplateTitle: '<s:message code="outputTemplate.results.viewTemplateTitle"/>',
                         updateTemplate: '<s:message code="outputTemplate.results.updateTemplate"/>',
                         updateTemplateTitle: '<s:message code="outputTemplate.results.updateTemplateTitle"/>',
                         publishTemplate: '<s:message code="outputTemplate.results.publishTemplate"/>',
                         publishTemplateTitle: '<s:message code="outputTemplate.results.publishTemplateTitle"/>',
                         archiveTemplate: '<s:message code="outputTemplate.results.archiveTemplate"/>',
                         archiveTemplateTitle: '<s:message code="outputTemplate.results.archiveTemplateTitle"/>',
                         removeTemplate: '<s:message code="outputTemplate.results.removeTemplate"/>',
                         removeTemplateTitle: '<s:message code="outputTemplate.results.removeTemplateTitle"/>',
                         exportTemplate: '<s:message code="outputTemplate.results.exportTemplate"/>',
                         exportTemplateTitle: '<s:message code="outputTemplate.results.exportTemplateTitle"/>'
                     },
                     permissions: {
                         //TODO - check different permission for form templates
                         //for now we assume the permissions are in sync
                         canView: '${canViewOutput}' === 'true' && '${canViewFormOutput}' === 'true',
                         canUpdate: '${canUpdateOutput}' === 'true' && '${canUpdateFormOutput}' === 'true',
                         canPublish: '${canPublishOutput}' === 'true' && '${canPublishFormOutput}' === 'true',
                         canArchive: '${canArchiveOutput}' === 'true' && '${canArchiveFormOutput}' === 'true',
                         canRemove: '${canRemove}' === 'true'
                     },
                     noDataMessage: '<s:message code="outputTemplate.no.results"/>',
                     url: '<s:url value="/rest/outputTemplate?context={context}&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                 },
                 filterContextConfig: {
                     labels: {
                         filter: '<s:message code="filter.active"/>',
                         name: '<s:message code="outputTemplate.find.filter.name.active"/>',
                         status: '<s:message code="outputTemplate.find.filter.status.active"/>',
                         outputCategory:'<s:message code="outputTemplate.find.filter.category.active"/>'
                     },
                     container: '#outputFilterContext'
                 },
                 filterConfig: {
                     labels: {
                         filterBy: '<s:message code="outputTemplate.find.filter.title"/>',
                         name: '<s:message code="outputTemplate.find.filter.name"/>',
                         status: '<s:message code="outputTemplate.find.filter.status"/>',
                         outputCategory:'<s:message code="outputTemplate.find.filter.category"/>',
                     },
                     outputCategoryUrl:'<s:url value="/rest/outputTemplate/assignedOutputCategories?context={context}"/>'
                 },
                 dialogConfig: {
                     views:{
                       addTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           labels: {
                               outputTemplateName: '<s:message code="newOutputTemplateWithVersionVO.outputTemplateName"/>',
                               outputCategory: '<s:message code="newOutputTemplateWithVersionVO.category"/>',
                               description: '<s:message code="newOutputTemplateWithVersionVO.description"/>',
                               filename: '<s:message code="newOutputTemplateWithVersionVO.filename"/>',
                               supportedFormats: '<s:message code="newOutputTemplateWithVersionVO.supportedFormats"/>',
                               useForSubjectAccessRequest: '<s:message code="newOutputTemplateWithVersionVO.useForSubjectAccessRequest"/>',
                               formats: outputFormats
                           }
                       },
                       viewTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           narrative: {
                               summary: '<s:message code="outputTemplate.view.summary" javaScriptEscape="true"/>'
                           },
                           labels: {
                               outputName: '<s:message code="outputTemplateVO.outputName"/>',
                               outputCategory: '<s:message code="outputTemplateVO.category"/>',
                               description: '<s:message code="outputTemplateVO.description"/>',
                               supportedFormats: '<s:message code="outputTemplateVO.supportedFormats"/>',
                               useForSubjectAccessRequest: '<s:message code="outputTemplateVO.useForSubjectAccessRequest"/>'
                           }
                       },
                       updateTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           narrative: {
                               summary: '<s:message code="outputTemplate.update.summary" javaScriptEscape="true"/>',
                           },
                           successMessage: '<s:message javaScriptEscape="true" code="outputTemplate.update.success"/>',
                           labels: {
                               outputName: '<s:message code="updateOutputTemplateVO.outputName"/>',
                               outputCategory: '<s:message code="updateOutputTemplateVO.category"/>',
                               description: '<s:message code="updateOutputTemplateVO.description"/>',
                               supportedFormats: '<s:message code="updateOutputTemplateVO.supportedFormats"/>',
                               useForSubjectAccessRequest: '<s:message code="updateOutputTemplateVO.useForSubjectAccessRequest"/>',
                               formats: outputFormats
                           }
                       },
                       publishTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           narrative: {
                               summary: '<s:message javaScriptEscape="true" code="outputTemplate.publish.summary"/>'
                           },
                           successMessage: '<s:message javaScriptEscape="true" code="outputTemplate.publish.success"/>',
                           config:{
                               message: '<s:message javaScriptEscape="true" code="outputTemplate.publish.prompt"/>',
                               url: '<s:url value="/rest/outputTemplate/{id}/status?action=published"/>'
                           }
                       },
                       archiveTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           narrative: {
                               summary: '<s:message javaScriptEscape="true" code="outputTemplate.archive.summary"/>'
                           },
                           successMessage: '<s:message javaScriptEscape="true" code="outputTemplate.archive.success"/>',
                           config:{
                               url: '<s:url value="/rest/outputTemplate/{id}/status?action=archived"/>',
                               message: '<s:message javaScriptEscape="true" code="outputTemplate.archive.prompt"/>'
                           }
                       },
                       removeTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           narrative: {
                               summary: '<s:message javaScriptEscape="true" code="outputTemplate.remove.summary"/>'
                           },
                           successMessage: '<s:message javaScriptEscape="true" code="outputTemplate.remove.success"/>',
                           config:{
                               url: '<s:url value="/rest/outputTemplate/{id}"/>',
                               message: '<s:message javaScriptEscape="true" code="outputTemplate.remove.prompt"/>'
                           }
                       },
                       generateTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           config:{
                               message: '<s:message javaScriptEscape="true" code="outputTemplate.generate.prompt"/>'
                           }

                       },
                       exportTemplate: {
                           header:{
                               icon: outputTemplateIcon
                           },
                           narrative: {
                               summary: '<s:message javaScriptEscape="true" code="outputTemplate.export.summary"/>',
                           },
                           config:{
                               url: '<s:url value="/rest/outputTemplate/{id}?download=true"/>',
                               message: '<s:message javaScriptEscape="true" code="outputTemplate.export.prompt"/>'
                           }
                       }
                   }
                 }
             };

         var outputTabs = new Y.app.admin.output.OutputTabsView({
             root: '<s:url value="/admin/outputs"/>',
             container: '#outputTabs',
             //selector to the toolbar containing filter, generate and add buttons
             toolbarNode: '#sectionToolbar',
             permissions: {
                 canAdd: '${canAddOutput}' === 'true' && '${canAddFormOutput}' === 'true',
                 canGenerate: '${canAddOutput}' === 'true' && '${canAddFormOutput}' === 'true'
             },
             tabs: {
                 person: {
                     label: '<s:message code="outputTemplate.tabs.person"/>',
                     config: Y.mix(Y.clone(commonConfig), {
                         dialogConfig: {
                             views:{
                               addTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.add.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.person.add.summary" javaScriptEscape="true"/>'
                                   },
                                   successMessage: '<s:message code="outputTemplate.person.add.success" javaScriptEscape="true"/>',
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate?outputTemplateWithVersion=true"/>'
                                   }
                               },
                               viewTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.view.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }

                               },
                               updateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.update.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }
                               },
                               publishTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.publish.header" javaScriptEscape="true"/>'
                                   }
                               },
                               archiveTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.archive.header" javaScriptEscape="true"/>'
                                   }
                               },
                               removeTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.remove.header" javaScriptEscape="true"/>'
                                   }
                               },
                               generateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.generate.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.person.generate.summary" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/person/template"/>'
                                   }
                               },
                               exportTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.export.header" javaScriptEscape="true"/>'
                                   }
                               }
                             }
                         }
                     }, true, undefined, 0, true)
                 },
                 personLetter: {
                     label: '<s:message code="outputTemplate.tabs.personLetter"/>',
                     config: Y.mix(Y.clone(commonConfig), {
                         dialogConfig: {
                             views:{
                               addTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.personLetter.add.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.personLetter.add.summary" javaScriptEscape="true"/>'
                                   },
                                   successMessage: '<s:message code="outputTemplate.personLetter.add.success" javaScriptEscape="true"/>',
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate?outputTemplateWithVersion=true"/>'
                                   }
                               },
                               viewTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.personLetter.view.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }

                               },
                               updateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.personLetter.update.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }
                               },
                               publishTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.personLetter.publish.header" javaScriptEscape="true"/>'
                                   }
                               },
                               archiveTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.personLetter.archive.header" javaScriptEscape="true"/>'
                                   }
                               },
                               removeTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.personLetter.remove.header" javaScriptEscape="true"/>'
                                   }
                               },
                               generateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.personLetter.generate.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.personLetter.generate.summary" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/person/namedRelationTemplate"/>'
                                   }
                               },
                               exportTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.person.export.header" javaScriptEscape="true"/>'
                                   }
                               }
                             }
                         }
                     }, true, undefined, 0, true)
                 },
                 casenote: {
                     label: '<s:message code="outputTemplate.tabs.caseNote"/>',
                     config: Y.mix(Y.clone(commonConfig), {
                         dialogConfig: {
                             views:{
                               addTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.caseNote.add.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.caseNote.add.summary" javaScriptEscape="true"/>'
                                   },
                                   successMessage: '<s:message code="outputTemplate.caseNote.add.success" javaScriptEscape="true"/>',
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate?outputTemplateWithVersion=true"/>'
                                   }
                               },
                               viewTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.caseNote.view.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }
                               },
                               updateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.caseNote.update.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }
                               },
                               publishTemplate: {
                                   header:{
                                       text: '<s:message code="outputTemplate.caseNote.publish.header" javaScriptEscape="true"/>'
                                   }
                               },
                               archiveTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.caseNote.archive.header" javaScriptEscape="true"/>',
                                   }
                               },
                               removeTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.caseNote.remove.header" javaScriptEscape="true"/>'
                                   }
                               },
                               generateTemplate: {
                                   header:{
                                       text: '<s:message code="outputTemplate.caseNote.generate.header" javaScriptEscape="true"/>',
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.caseNote.generate.summary" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/caseNote/template"/>'
                                   }
                               },
                               exportTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.caseNote.export.header" javaScriptEscape="true"/>'
                                   }
                               }
                             }
                         }
                     }, true, undefined, 0, true)
                 },
                 chronology: {
                     label: '<s:message code="outputTemplate.tabs.chronology"/>',
                     config: Y.mix(Y.clone(commonConfig), {
                         dialogConfig: {
                             views:{
                               addTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.chronology.add.header" javaScriptEscape="true"/>',
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.chronology.add.summary" javaScriptEscape="true"/>'
                                   },
                                   successMessage: '<s:message code="outputTemplate.chronology.add.success" javaScriptEscape="true"/>',
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate?outputTemplateWithVersion=true"/>'
                                   }
                               },
                               viewTemplate: {
                                   header:{
                                       text: '<s:message code="outputTemplate.chronology.view.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }
                               },
                               updateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.chronology.update.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }
                               },
                               publishTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.chronology.publish.header" javaScriptEscape="true"/>'
                                   }
                               },
                               archiveTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.chronology.archive.header" javaScriptEscape="true"/>'
                                   }
                               },
                               removeTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.chronology.remove.header" javaScriptEscape="true"/>'
                                   }
                               },
                               generateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.chronology.generate.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.chronology.generate.summary" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/chronology/template"/>'
                                   }
                               },
                               exportTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.chronology.export.header" javaScriptEscape="true"/>'
                                   }
                               }
                             }
                         }
                     }, true, undefined, 0, true)
                 },
                 form: {
                     label: '<s:message code="outputTemplate.tabs.form"/>',
                     config: Y.mix(Y.clone(commonConfig), {
                         searchConfig: {
                             labels: {
                                 definitionName: '<s:message code="outputTemplate.results.form.definitionName"/>'
                             },
                             url: '<s:url value="/rest/formOutputTemplate?context={context}&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                         },
                         filterContextConfig: {
                             labels: {
                                 formDefinitionId: '<s:message code="outputTemplate.find.filter.formDefinitionId.active"/>'
                             }
                         },
                         filterConfig: {
                             //URL to load form definitions to populate filter
                             formDefinitionListURL: '<s:url value="/rest/formDefinition?hasOutputTemplates=true"/>',
                             labels: {
                                 formDefinitionId: '<s:message code="outputTemplate.find.filter.formDefinitionId"/>'
                             },
                             outputCategoryUrl:'<s:url value="/rest/outputTemplate/assignedOutputCategories?context={context}"/>'
                         },
                         dialogConfig: {
                             views:{
                               addTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.add.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.form.add.summary" javaScriptEscape="true"/>'
                                   },
                                   successMessage: '<s:message code="outputTemplate.form.add.success" javaScriptEscape="true"/>',
                                   labels: {
                                       formDefinitionIds: '<s:message code="newFormOutputTemplateWithVersionVO.formDefinitionIds"/>',
                                       formDefinition: '<s:message code="formOutputTemplate.formDefinition"/>',
                                       formDefinitionVersions: '<s:message code="formOutputTemplate.formDefinitionVersions"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/formOutputTemplate?outputTemplateWithVersion=true"/>',
                                       formDefinitionListURL: '<s:url value="/rest/formDefinition?includeHistoric=true&includeDraft=false"/>'
                                   }
                               },
                               viewTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.view.header" javaScriptEscape="true"/>'
                                   },
                                   labels: {
                                       formDefinitionName: '<s:message code="formOutputTemplate.formDefinition"/>',
                                       formDefinitionVersion: '<s:message code="formOutputTemplate.formDefinitionVersions"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/formOutputTemplate/{id}"/>'
                                   }
                               },
                               updateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.update.header" javaScriptEscape="true"/>'
                                   },
                                   labels: {
                                       formDefinitionIds: '<s:message code="updateFormOutputTemplateVO.formDefinitionIds"/>',
                                       formDefinition: '<s:message code="formOutputTemplate.formDefinition"/>',
                                       formDefinitionVersions: '<s:message code="formOutputTemplate.formDefinitionVersions"/>'
                                   },
                                   config:{
                                     url: '<s:url value="/rest/formOutputTemplate/{id}"/>',
                                     formDefinitionVersionsListURL: '<s:url value="/rest/formDefinition?formDefinitionId={formDefinitionId}"/>'
                                   }
                               },
                               publishTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.publish.header" javaScriptEscape="true"/>'
                                   }
                               },
                               archiveTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.archive.header" javaScriptEscape="true"/>'
                                   }
                               },
                               removeTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.remove.header" javaScriptEscape="true"/>'
                                   }
                               },
                               generateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.generate.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.form.generate.summary" javaScriptEscape="true"/>'
                                   },
                                   labels: {
                                       formDefinitionId: '<s:message code="outputTemplate.form.generate.formDefinitionId"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/formDefinition/{formDefinitionId}/template"/>',
                                       formDefinitionListURL: '<s:url value="/rest/formDefinition?includeHistoric=true"/>'
                                   }
                               },
                               exportTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.form.export.header" javaScriptEscape="true"/>'
                                   }
                               }
                             }
                         }
                     }, true, undefined, 0, true)
                 },
                 childLookedAfter: {
                     label: '<s:message code="outputTemplate.tabs.childLookedAfter"/>',
                     config: Y.mix(Y.clone(commonConfig), {
                         dialogConfig: {
                             views:{
                               addTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.add.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.childLookedAfter.add.summary" javaScriptEscape="true"/>'
                                   },
                                   successMessage: '<s:message code="outputTemplate.childLookedAfter.add.success" javaScriptEscape="true"/>',
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate?outputTemplateWithVersion=true"/>'
                                   }
                               },
                               viewTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.view.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }

                               },
                               updateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.update.header" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/outputTemplate/{id}"/>'
                                   }
                               },
                               publishTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.publish.header" javaScriptEscape="true"/>'
                                   }
                               },
                               archiveTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.archive.header" javaScriptEscape="true"/>'
                                   }
                               },
                               removeTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.remove.header" javaScriptEscape="true"/>'
                                   }
                               },
                               generateTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.generate.header" javaScriptEscape="true"/>'
                                   },
                                   narrative: {
                                       summary: '<s:message code="outputTemplate.childLookedAfter.generate.summary" javaScriptEscape="true"/>'
                                   },
                                   config:{
                                       url: '<s:url value="/rest/childLookedAfter/template"/>'
                                   }
                               },
                               exportTemplate: {
                                   header: {
                                       text: '<s:message code="outputTemplate.childLookedAfter.export.header" javaScriptEscape="true"/>'
                                   }
                               }
                             }
                         }
                     }, true, undefined, 0, true)
                 }
             }
         }).render();


         // Make sure to dispatch the current hash-based URL which was set by
         // the server to our route handlers.
         if (outputTabs.hasRoute(outputTabs.getPath())) {
             outputTabs.dispatch();
         } else {
             outputTabs.activateTab('person');
         }
     });
     </script>
    </c:when>
    <c:otherwise>
        <%-- Insert access denied tile --%>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>
