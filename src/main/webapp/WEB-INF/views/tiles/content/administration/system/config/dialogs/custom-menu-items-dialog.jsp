<%@ page import="java.util.Locale"%>
<%@ page import="org.json.JSONObject"%>
<%@ page import="java.util.Map"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<s:eval expression="@jacksonObjectMapper" var="objectMapper" />
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<c:set var="availableTokens" value="${customMenuItemService.getAvailableTokensBySubjectType('PERSON')}"/>

<script>

	Y.use('yui-base',
      'json-parse',
      'event-custom',
      'custommenu-dialog-views',
      'model-transmogrify',
      'usp-custommenu-CustomMenuItem',
      'usp-custommenu-UpdateCustomMenuItem',
    
  function(Y) {

	var EVENT_INFO_MESSAGE = 'infoMessage:message';

	var availableTokenData = Y.JSON.parse('${esc:escapeJavaScript(objectMapper.writeValueAsString(pageContext.getAttribute("availableTokens")))}');
	  
    var commonLabels = {       
			customMenuLinkLabel:'<s:message code="administrator.system.config.customMenuLinks.customMenuLinkLabel"/>',
			customMenuItemKeyLabel: '<s:message code="common.label.key"/>',
			customMenuItemValueLabel: '<s:message code="common.label.value"/>',
    		customMenuLinkTooltip: '<s:message code="administrator.system.config.customMenuLinks.customMenuLinkTooltip"/>',        
    		customMenuLinkUrl: '<s:message code="administrator.system.config.customMenuLinks.customMenuLinkUrl"/>',
    		customMenuItemActiveLabel: '<s:message code="administrator.system.config.customMenuLinks.customMenuActiveLabel"/>',
			customMenuItemContextLabel: '<s:message code="administrator.system.config.customMenuLinks.customMenuContextLabel"/>',
			editSuccessMessage : '<s:message code="administrator.system.config.customMenuLinks.editSuccessMessage"/>',
    		editCustomMenuItemNarrativeSummary: '<s:message code="administrator.system.config.customMenuLinks.editCustomMenuItemNarrativeSummary"/>'
    		
    };

    var myCustomMenuItemDialog = new Y.app.custommenu.MultiPanelPopUp();
    
    myCustomMenuItemDialog.render();
    
    
    var UpdateCustomMenuItemModel = Y.Base.create("updateCustomMenuItemModel", Y.usp.custommenu.CustomMenuItem, [Y.usp.ModelFormLink,Y.usp.ModelTransmogrify], { 
  		form: '#customMenuItemEditForm',
  		url: '<s:url value="/rest/customMenuItem/{id}"/>'
  	});
    
    
    Y.on('customMenuItem:showDialog', function(e){
      
    	switch(e.action){     
    	case 'editCustomMenuItem':       
    	  this.showView('editCustomMenuItemForm', {
	    		  model: new UpdateCustomMenuItemModel(),
	    		  labels: commonLabels,
	    		  availableTokenData: availableTokenData,
	    		  SYSTEM_CONFIG_URL : '<s:url value="/admin/system/config"/>'
           }, {
							modelLoad:true,
							payload:{
								id:e.id
					    }
				 });
        break;
    	};
    	
    }, myCustomMenuItemDialog);
    
    myCustomMenuItemDialog.on('*:saved', function(e){
			// display the success message
			var typeOfMessage = e.typeOfMessage ? e.typeOfMessage : 'success';
				Y.fire(EVENT_INFO_MESSAGE, {
					message: e.successMessage,
					type: typeOfMessage,
					delayed: true,
				});
		
		}, myCustomMenuItemDialog);	
    
    myCustomMenuItemDialog.after('*:saveEnabledChange', function(e){
      if(e.newVal===true) {          
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
      } else {
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',true);
      }
    }, myCustomMenuItemDialog); 
    
  });
         


</script>