<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 
 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'FormMapping','FormMapping.View')" var="canViewMappings" />

<c:choose>
	<c:when test="${canViewMappings eq true}">
		<div>
			<t:insertTemplate template="/WEB-INF/views/tiles/content/administration/form/mappings/results.jsp" />
		</div>	
	</c:when>
	<c:otherwise>
		<%-- Insert access denied tile --%>
		<t:insertDefinition name="access.denied"/>
	</c:otherwise>
</c:choose>