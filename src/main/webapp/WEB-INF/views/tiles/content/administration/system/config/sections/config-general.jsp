<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%-- SecurityConfiguration permissions --%>
<sec:authorize access="hasPermission(null, 'SecurityConfiguration','SecurityConfiguration.Update')" var="canUpdateSecurityConfiguration" />



<div id="systemConfig" class="hide-on-search">
	<div id="configGeneral"></div>
</div>


<script>
	Y.use('yui-base','admin-system-config-screen', function(Y){
		var systemConfigDetailsModel = new Y.app.security.SystemConfigGeneralModel({
			url: '<s:url value="/rest/securityConfiguration"/>'
		});
		
		  
		systemConfigDetailsModel.load();
		
		Y.on('security:systemConfigurationChanged', function(e){
			systemConfigDetailsModel.load();
		});
		
		var systemConfigurationPropertiesForSMS = {
			 isEnabled : '${smsConfig.isEnabled()}',
			 smsProvider : '${smsConfig.getSmsProvider()}',
			 defaultFromNumber : '${smsConfig.getDefaultFromNumber()}',
			 connectionTimeout : '${smsConfig.getConnectionTimeout()}',
			 accountCredentails : '${smsConfig.getAccountCredentials()}',
			 accountSid : '${smsConfig.getAccountSid()}'
			 
		};
		
		var systemConfigurationPropertiesForSMTP = {
				 isEnabled : '${smtpConfig.isEnabled()}',
				 host : '${smtpConfig.getHost()}',
				 port : '${smtpConfig.getPort()}',
				 username : '${smtpConfig.getUsername()}',
				 auth : '${smtpConfig.isAuth()}',
				 connectiontimeout : '${smtpConfig.getConnectionTimeout()}',
				 starttlsEnable : '${smtpConfig.isStarttlsEnable()}',
				 sslTrust : '${smtpConfig.getSslTrust()}'
				 
			};
		
		var systemConfigurationPropertiesForLDAP = {
				 isEnabled:'${ldapConfig.isEnabled()}',
				 url : '${ldapConfig.getUrl()}',
				 rootDN : '${ldapConfig.getRootDN()}',
				 principalDN : '${ldapConfig.getPrincipalDN()}',
				 userBase : '${ldapConfig.getUserBase()}',
				 userFilter : '${ldapConfig.getUserFilter()}',
				 groupBase : '${ldapConfig.getGroupBase()}',
				 groupFilter : '${ldapConfig.getGroupFilter()}'
				 
			};
		
		var systemConfigView = new Y.app.security.SystemConfigView({
			  container:'#configGeneral',
				model:systemConfigDetailsModel,
	   		systemGeneralConfig: {
      			title:'<s:message code="administrator.system.config.general.title"/>',
	   			  labels:{
	      			accLockOutSectionLabel:'<s:message code="administrator.system.config.general.accLockOutSectionLabel"/>',
	      			enableAccountLockOutCheckboxLabel:'<s:message code="administrator.system.config.general.enableAccountLockOutCheckboxLabel"/>',
	      			failedLoginAttemptsLimit:'<s:message code="administrator.system.config.general.failedLoginAttemptsLimit"/>',
			        timeToUnlock:'<s:message code="administrator.system.config.general.timeToUnlock"/>',
			        securityConfigurationDetailsSuccessMessage: '<s:message code="administrator.system.config.general.securityConfigurationDetailsSuccessMessage"/>',
			        multiLoginPeriod:'<s:message code="administrator.system.config.general.multiLoginPeriod"/>',
			        legendMultipleLoginTimeout:'<s:message code="administrator.system.config.general.legendMultipleLoginTimeout"/>',			        
			        multipleLoginTimeout:'<s:message code="administrator.system.config.general.multipleLoginTimeout"/>',
			        legendBottomMultipleLoginTimeout:'<s:message code="administrator.system.config.general.legendBottomMultipleLoginTimeout"/>',
			        smsTokenTTL:'<s:message code="administrator.system.config.general.smsTokenTTL"/>',
			        legendAuthenticationFactor:'<s:message code="administrator.system.config.general.legendAuthenticationFactor"/>',
			        authenticationFactor:'<s:message code="administrator.system.config.general.authenticationFactor"/>',
			        selfServicePasswordReset:'<s:message code="administrator.system.config.general.selfServicePasswordReset"/>',
			        selfServicePasswordResetOff:'<s:message code="administrator.system.config.general.selfServicePasswordResetOff"/>',
			        selfServicePasswordResetOn:'<s:message code="administrator.system.config.general.selfServicePasswordResetOn"/>',
			        legendSelfServicePasswordReset:'<s:message code="administrator.system.config.general.legendSelfServicePasswordReset"/>',
			        autoSendLoginDetails:'<s:message code="administrator.system.config.general.autoSendLoginDetails"/>',
			        autoSendLoginDetailsOff:'<s:message code="administrator.system.config.general.autoSendLoginDetailsOff"/>',
			        autoSendLoginDetailsOn:'<s:message code="administrator.system.config.general.autoSendLoginDetailsOn"/>',
			        legendAutoSendLoginDetails:'<s:message code="administrator.system.config.general.legendAutoSendLoginDetails"/>',
			        generalConfigFormSave:'<s:message code="administrator.system.config.general.generalConfigFormSave"/>',
			        generalConfigFormCancel:'<s:message code="administrator.system.config.general.generalConfigFormCancel"/>',
			        termsAndConditions:'<s:message code="administrator.system.config.general.termsAndConditions"/>',
			        termsAndConditionsEnabled:'<s:message code="administrator.system.config.general.termsAndConditionsEnabled"/>'
				    },
				    closed:false
	   		},
	   		systemForgottenPswdEmailConfig: {
      			title:'<s:message code="administrator.system.config.forgottenPswdEmail.title"/>',
	   			labels:{
	   				fromAddress: '<s:message code="administrator.system.config.forgottenPswdEmail.fromAddress"/>',
	   				subjectLine: '<s:message code="administrator.system.config.forgottenPswdEmail.subjectLine"/>',
	   				timeToLive: '<s:message code="administrator.system.config.forgottenPswdEmail.timeToLive"/>',
	   				content: '<s:message code="administrator.system.config.forgottenPswdEmail.content"/>',
	   				forgottenPasswordEmailDetailsSuccessMessage: '<s:message code="administrator.system.config.general.securityConfigurationDetailsSuccessMessage"/>'
				   },
				   closed:true
	   		},
	   		systemSmsConfig: {
      			title:'<s:message code="administrator.system.config.sms.title"/>',
	   			labels:{	   				
	   				timeToLive: '<s:message code="administrator.system.config.sms.timeToLive"/>',
	   				smsContent: '<s:message code="administrator.system.config.sms.smsContent"/>',
	   				twoFactorSmsSuccessMessage: '<s:message code="administrator.system.config.general.securityConfigurationDetailsSuccessMessage"/>'
				   },
				   closed:true
	   		},
	   		systemLoginDetailsEmailsConfig: {
      			title:'<s:message code="administrator.system.config.loginDetailsEmails.title"/>',
	   			labels:{
	   				fromAddress: '<s:message code="administrator.system.config.loginDetailsEmails.fromAddress"/>',
	   				userSubject: '<s:message code="administrator.system.config.loginDetailsEmails.userSubject"/>',
	   				passwordSubject: '<s:message code="administrator.system.config.loginDetailsEmails.passwordSubject"/>',
	   				emailWithUsername: '<s:message code="administrator.system.config.loginDetailsEmails.emailWithUsername"/>',
	   				emailWithPassword: '<s:message code="administrator.system.config.loginDetailsEmails.emailWithPassword"/>',
	   				userContent: '<s:message code="administrator.system.config.loginDetailsEmails.userContent"/>',
	   				passwordContent: '<s:message code="administrator.system.config.loginDetailsEmails.passwordContent"/>',
	   				loginDetailsEmailsSuccessMessage: '<s:message code="administrator.system.config.general.securityConfigurationDetailsSuccessMessage"/>'
				  },
				   closed:true
	   		},
	   		systemTermsAndConditionsConfig: {
      			title:'<s:message code="administrator.system.config.termsAndConditions.title"/>',
		   			labels:{
		   				url: '<s:message code="administrator.system.config.termsAndConditions.url"/>',
		   				termsAndConditionsSuccessMessage: '<s:message code="administrator.system.config.general.securityConfigurationDetailsSuccessMessage"/>'
					  },
				    closed:true
	   		},
	   		systemCustomMenuLinksConfig: {
      			title:'<s:message code="administrator.system.config.customMenuLinks.title"/>',
		   			labels:{
		   				customMenuItemActiveLabel: '<s:message code="administrator.system.config.customMenuLinks.customMenuActiveLabel"/>',
						customMenuItemContextLabel: '<s:message code="administrator.system.config.customMenuLinks.customMenuContextLabel"/>',
						customMenuItemKeyLabel: '<s:message code="common.label.key"/>',
						customMenuItemValueLabel: '<s:message code="common.label.value"/>',
		   				customMenuItemSectionTitle: '<s:message code="administrator.system.config.customMenuLinks.customMenuSectionTitle"/>',
		   				customMenuLinkLabel:'<s:message code="administrator.system.config.customMenuLinks.customMenuLinkLabel"/>',
		   				customMenuLinkTooltip: '<s:message code="administrator.system.config.customMenuLinks.customMenuLinkTooltip"/>',
		   				customMenuLinkUrl: '<s:message code="administrator.system.config.customMenuLinks.customMenuLinkUrl"/>',
		   				editCustomMenuLinkLabel: '<s:message code="administrator.system.config.customMenuLinks.customMenuEditLinkUrl"/>',
		   				customMenuLinkRightLabel: '<s:message code="administrator.system.config.customMenuLinks.customMenuLinkRightLabel"/>'
					 },
					 customMenuListUrl : '<s:url value="/rest/customMenuItem?s={sortBy}&pageNumber=-1&pageSize=-1"><s:param name="sortBy" value="[{\"label\":\"asc\"}]" /></s:url>',
				   closed:true
	   		},
	   		systemConfigurationPropertiesDisplay: {
      			title:'<s:message code="administrator.system.config.configurationPropertiesDisplay.title"/>',
		   			labels:{
		   				ldapConfiguration: '<s:message code="administrator.system.config.configurationPropertiesDisplay.ldapConfiguration"/>',
		   				smtpConfiguration: '<s:message code="administrator.system.config.configurationPropertiesDisplay.smtpConfiguration"/>',
		   				smsConfiguration: '<s:message code="administrator.system.config.configurationPropertiesDisplay.smsConfiguration"/>',
		   				isEnabled : '<s:message code="administrator.system.config.configurationPropertiesDisplay.isEnabled"/>',
		   				connectiontimeout : '<s:message code="administrator.system.config.configurationPropertiesDisplay.connectiontimeout"/>',
		   				
		   				smsProvider : '<s:message code="administrator.system.config.smsConfiguration.smsProvider"/>',
		   				defaultFromNumber : '<s:message code="administrator.system.config.smsConfiguration.defaultFromNumber"/>',
		   				accountCredentails : '<s:message code="administrator.system.config.smsConfiguration.accountCredentails"/>',
		   				accountSid : '<s:message code="administrator.system.config.smsConfiguration.accountSid"/>',
						  
						  
						  host : '<s:message code="administrator.system.config.smtpConfiguration.host"/>',
						  defaultFromNumber : '<s:message code="administrator.system.config.smtpConfiguration.defaultFromNumber"/>',
						  port : '<s:message code="administrator.system.config.smtpConfiguration.port"/>',
						  username : '<s:message code="administrator.system.config.smtpConfiguration.username"/>',
						  auth : '<s:message code="administrator.system.config.smtpConfiguration.auth"/>',
						  starttlsEnable : '<s:message code="administrator.system.config.smtpConfiguration.starttlsEnable"/>',
						  sslTrust : '<s:message code="administrator.system.config.smtpConfiguration.sslTrust"/>',
						 	
						 	url : '<s:message code="administrator.system.config.ldapConfiguration.url"/>',
						 	rootDN : '<s:message code="administrator.system.config.ldapConfiguration.rootDN"/>',
						 	principalDN : '<s:message code="administrator.system.config.ldapConfiguration.principalDN"/>',
						 	userBase : '<s:message code="administrator.system.config.ldapConfiguration.userBase"/>',
						 	userFilter : '<s:message code="administrator.system.config.ldapConfiguration.userFilter"/>',
						 	groupBase : '<s:message code="administrator.system.config.ldapConfiguration.groupBase"/>',
						  groupFilter : '<s:message code="administrator.system.config.ldapConfiguration.groupFilter"/>'
						},
							smtpConfigProperties : systemConfigurationPropertiesForSMTP,
							smsConfigProperties : systemConfigurationPropertiesForSMS,
							ldapConfigProperties : systemConfigurationPropertiesForLDAP,
							
						closed:true
	   		},
	   		systemEnvironmentDetailsConfig: {
      		title:'<s:message code="administrator.system.config.systemEnvironmentDetails.title"/>',
	   			labels:{
	   				environment: '<s:message code="administrator.system.config.systemEnvironmentDetails.environment"/>',
	   				version: '<s:message code="administrator.system.config.systemEnvironmentDetails.version"/>'
				  },
				  environmentProperties: {
					  environment: '${buildVersionEnvInfo.environment}',
					  version: '${relationshipsrecordingAppVersion.buildVersion}'
				  },
			    closed:true
   		  }
		});
	
		systemConfigView.render();
				
	});	

</script>

