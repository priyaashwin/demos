<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="title" ignore="true" />
<h3 tabindex="0">
  <span id="calendarHeaderTitle" class="context-title">
  <c:choose> 
    <c:when test="${not empty title }">
        <c:out value="${title }"/>
    </c:when>
    <c:otherwise>
        <s:message code="menu.calendaradministration.sectiontitle" />
    </c:otherwise>
  </c:choose>
  </span> 
  <span id="calendarResultsCount" style="padding-left: 0;" />
</h3>
