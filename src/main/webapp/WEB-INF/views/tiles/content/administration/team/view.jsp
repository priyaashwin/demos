<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 
 <%-- Define permissions --%>
 <%-- TODO Need to change the permissions once the service apis are created --%>
<sec:authorize access="hasPermission(null, 'SecurityGroupOfRights','SecurityGroupOfRights.GET')" var="canView" />
<c:choose>
	<c:when test="${canView eq true}">
		<div>
			<t:insertTemplate template="/WEB-INF/views/tiles/content/administration/team/results.jsp" />
			<%-- Insert Team ADD Template  must be used in conjunction with the REACT component scripts--%>
			<t:insertTemplate template="/WEB-INF/views/tiles/content/common/team/add.jsp" >
				<t:putAttribute name="preventPageLoad" value="true"/>
			</t:insertTemplate>
		</div>	
	</c:when>
	<c:otherwise>
		<%-- Insert access denied tile --%>
		<t:insertDefinition name="access.denied"/>
	</c:otherwise>
</c:choose>