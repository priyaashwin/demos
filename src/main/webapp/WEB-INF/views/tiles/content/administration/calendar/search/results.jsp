<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

 <%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'Calendar','Calendar.View')" var="canView" />
<sec:authorize access="hasPermission(null, 'Calendar','Calendar.Update')" var="canUpdate" />
<sec:authorize access="hasPermission(null, 'Calendar','Calendar.Deactivate')" var="canDeactivate" />
<sec:authorize access="hasPermission(null, 'Calendar','Calendar.Activate')" var="canActivate" />
<div id="calendarResults"></div>

<script>
Y.use('calendar-search-view',
      'event-custom-base',
      function(Y) {
  var canView = '${canView}'==='true',
    canUpdate = '${canUpdate}'==='true',
    canActivate = '${canActivate}'==='true',
    canDeactivate = '${canDeactivate}'==='true',
    calendarSearch = new Y.app.CalendarSearchView({
		container:'#calendarResults',
		searchConfig: {
			labels: {
				name: '<s:message code="calendar.find.results.name"/>',
				description: '<s:message code="calendar.find.results.description"/>',
				state: '<s:message code="calendar.find.results.state"/>',
				defaultCalendar: '<s:message code="calendar.find.results.defaultCalendar"/>',
				actions: '<s:message code="calendar.find.results.actions"/>',
				viewCalendar: '<s:message code="calendar.find.results.viewCalendar"/>',
				viewCalendarTitle: '<s:message code="calendar.find.results.viewCalendarTitle"/>',
				editCalendar: '<s:message code="calendar.find.results.editCalendar"/>',
				editCalendarTitle: '<s:message code="calendar.find.results.editCalendarTitle"/>',
				activateCalendar: '<s:message code="calendar.find.results.activateCalendar"/>',
				activateCalendarTitle: '<s:message code="calendar.find.results.activateCalendarTitle"/>',
				deactivateCalendar: '<s:message code="calendar.find.results.deactivateCalendar"/>',
				deactivateCalendarTitle: '<s:message code="calendar.find.results.deactivateCalendarTitle"/>',
				setDefaultCalendar: '<s:message code="calendar.find.results.setDefaultCalendar"/>',
				setDefaultCalendarTitle: '<s:message code="calendar.find.results.setDefaultCalendarTitle"/>'
			},
			noDataMessage: '<s:message code="calendar.find.no.results"/>',
			url: '<s:url value="/rest/calendar?useSoundex=false&appendWildcard=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
			viewURL: '<s:url value="/admin/calendar/days?calendarId={id}"/>',
			resultsCountNode:Y.one('#calendarResultsCount'),
			permissions: {
				canView: canView,
				canUpdate: canUpdate,
				canActivate: canActivate,
				canDeactivate: canDeactivate
			}
		},
		calendarDialogConfig: {
			defaultHeader: '<s:message code="calendar.dialog.loading"/>',
			addCalendarConfig: {
				headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendar.addDialog.title"/></h3>',
				successMessage:'<s:message javaScriptEscape="true" code="calendar.add.success"/>',
				url: '<s:url value="/rest/calendar"/>',
				labels: {
					name: '<s:message code="newCalendarAndWorkingDaysVO.name"/>',
					firstDayOfWeek: '<s:message code="newCalendarAndWorkingDaysVO.firstDayOfWeek"/>',
					description: '<s:message code="newCalendarAndWorkingDaysVO.description"/>',
					narrative: "<s:message code='calendar.add.narrative'/>"
				}
			},
			editCalendarConfig: {
				headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendar.editDialog.title"/></h3>',
				successMessage:'<s:message javaScriptEscape="true" code="calendar.edit.success"/>',
				url: '<s:url value="/rest/calendar/{id}"/>',
				labels: {
					name: '<s:message code="updateCalendarVO.name"/>',
					firstDayOfWeek: '<s:message code="updateCalendarVO.firstDayOfWeek"/>',
					description: '<s:message code="updateCalendarVO.description"/>',
				    narrative:'<s:message javaScriptEscape="true" code="calendar.edit.narrative"/>'
				}
			},
			activateCalendarConfig: {
				headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendar.activateDialog.title"/></h3>',
				successMessage:'<s:message javaScriptEscape="true" code="calendar.activate.success"/>',
			    url:'<s:url value="/rest/calendar/{id}/status?action={action}"/>',
				labels: {
				    narrative:'<s:message javaScriptEscape="true" code="calendar.activate.narrative"/>'
				}
			},
			deactivateCalendarConfig: {
				headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendar.deactivateDialog.title"/></h3>',
				successMessage:'<s:message javaScriptEscape="true" code="calendar.deactivate.success"/>',
			    url:'<s:url value="/rest/calendar/{id}/status?action={action}"/>',
				labels: {
				    narrative:'<s:message javaScriptEscape="true" code="calendar.deactivate.narrative"/>'
				}
			},
			setDefaultCalendarConfig: {
				headerTemplate: '<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span><s:message code="calendar.setDefaultDialog.title"/></h3>',
				successMessage:'<s:message javaScriptEscape="true" code="calendar.setDefault.success"/>',
			    url:'<s:url value="/rest/calendar/{id}?default={default}"/>',
				labels: {
				    narrative:'<s:message javaScriptEscape="true" code="calendar.setDefault.narrative"/>'
				}
			}
		}
	  }).render();	
 
  Y.delegate('click', function(e) {
    var t = e.currentTarget,
      enabledCalendarAdd = !t.hasClass('pure-button-disabled') && t.hasClass('calendar-add'),
      enabledCalendarHolidays = !t.hasClass('pure-button-disabled') && t.hasClass('calendar-holidays');
    e.preventDefault();

    if (enabledCalendarAdd) {
      Y.fire('calendar:showDialog', {
          action: 'add'
      });
    }
    
    if (enabledCalendarHolidays) {
		window.location = '<s:url value="/admin/calendar/holidays"/>';
    }
  }, '#sectionToolbar', '.pure-button,li.pure-menu-item a', calendarSearch);  

  Y.all('#sectionToolbar .pure-button-loading')
    .removeAttribute('aria-disabled')
    .removeAttribute('disabled')
    .removeClass('pure-button-disabled')
    .removeClass('pure-button-loading');
});
</script>
