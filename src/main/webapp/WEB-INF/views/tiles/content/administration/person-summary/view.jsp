<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasPermission(null,'ConfigurableTypeReference','ConfigurableTypeReference.Update')" var="canUpdate"/>
<sec:authorize access="hasPermission(null,'FormConfigurableTypeReference','FormConfigurableTypeReference.Update')" var="canUpdateForm"/>
<sec:authorize access="hasPermission(null,'FormConfigurableTypeReference','FormConfigurableTypeReference.Delete')" var="canDeleteForm"/>
<sec:authorize access="hasPermission(null,'CaseNoteConfigurableTypeReference','CaseNoteConfigurableTypeReference.Update')" var="canUpdateCaseNote"/>

<c:choose>
    <c:when test="${canUpdate eq true}"> 
        <div id="personSummaryAdministrationTabs"></div>
        <script>
        Y.use('yui-base', 'person-summary-administration-view', function(Y) {      	
        	var personSummaryAdminTabs = new Y.app.PersonSummaryAdminTabsView({
        		root: '<s:url value="/admin/personsummary"/>',
        		container: '#personSummaryAdministrationTabs',
        		tabs: {
        			casenote: {
        				label: '<s:message code="person.summary.admin.casenote.title" javaScriptEscape="true"/>',
        				config: {
        					title:'<s:message code="person.summary.admin.casenote.title" javaScriptEscape="true"/>',
        					labels: {
            					name: '<s:message code="person.summary.admin.casenote.name" javaScriptEscape="true"/>',
            					identifier: '<s:message code="person.summary.admin.casenote.identifier" javaScriptEscape="true"/>',
            					caseNoteEntryType:'<s:message code="person.summary.admin.casenote.entrytype" javaScriptEscape="true"/>',
            					update: '<s:message code="person.summary.admin.casenote.update" javaScriptEscape="true"/>',
            				},
            				permissions : {
            					canUpdate: '${canUpdateCaseNote}'
            				},
        					url: '<s:url value="/rest/caseNoteConfigurableTypeReference?&pageNumber={page}&pageSize={pageSize}"/>',
        					dialogConfig: {
            					update: {
            						headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> <s:message code="person.summary.admin.casenote.dialog.header"/></h3>',
									labels: {
            							successMessage: '<s:message code="person.summary.admin.casenote.dialog.successMessage" javaScriptEscape="true"/>',
            							narrative: '<s:message code="person.summary.admin.casenote.dialog.narrative" javaScriptEscape="true"/>',
            							caseNote: '<s:message code="person.summary.admin.casenote.entrytype" javaScriptEscape="true"/>',
            							identifier: '<s:message code="person.summary.admin.casenote.identifier" javaScriptEscape="true"/>'
            						},
            						url: '<s:url value="/rest/caseNoteConfigurableTypeReference/{id}"/>'
            					}
            			}
        				}
        			}, 
        			form: {
        				label: '<s:message code="person.summary.admin.forms.title" javaScriptEscape="true"/>',
								config: {
									title:'<s:message code="person.summary.admin.forms.title" javaScriptEscape="true"/>',
									labels: {
		            		name: '<s:message code="person.summary.admin.forms.name" javaScriptEscape="true"/>',
		           			identifier: '<s:message code="person.summary.admin.forms.identifier" javaScriptEscape="true"/>',
							form:'<s:message code="person.summary.admin.forms.form" javaScriptEscape="true"/>',
							add: '<s:message code="person.summary.admin.forms.add" javaScriptEscape="true"/>',
							update: '<s:message code="person.summary.admin.forms.update" javaScriptEscape="true"/>',
							delete: '<s:message code="person.summary.admin.forms.delete" javaScriptEscape="true"/>',
							associatedTeams: '<s:message code="person.summary.admin.forms.associatedTeams" javaScriptEscape="true"/>'
		            	},
		            	permissions : {
								canUpdate: '${canUpdateForm}',
								canDelete: '${canDeleteForm}',
        					},
							url: '<s:url value="/rest/formConfigurableTypeReference?&pageNumber={page}&pageSize={pageSize}"/>',
							dialogConfig: {
								views: {
			        				updateFormConfigurableTypeReferenceView: {
			        					headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> <s:message code="person.summary.admin.forms.dialog.header"/></h3>',
										labels: {
			        						form:'<s:message code="person.summary.admin.forms.form" javaScriptEscape="true"/>',
											identifier: '<s:message code="person.summary.admin.forms.identifier" javaScriptEscape="true"/>',
											associatedTeams: '<s:message code="person.summary.admin.forms.associatedTeams" javaScriptEscape="true"/>'
										},
										narrative:{
        									summary: '<s:message code="person.summary.admin.forms.dialog.update.narrative" javaScriptEscape="true"/>'
										},
										successMessage: '<s:message code="person.summary.admin.forms.dialog.update.successMessage" javaScriptEscape="true"/>',
			        					url: '<s:url value="/rest/formConfigurableTypeReference/{id}"/>',
										formDefinitionUrl: '<s:url value="/rest/formDefinition?includeHistoric=false&s={sortBy}&pageNumber=-1&pageSize=-1"/>',
										teamUrl: '<s:url value="/rest/team?pageNumber=1&pageSize=-1&s={sortBy}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>' 
									},
									addFormConfigurableTypeReferenceView: {
			        					headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> <s:message code="person.summary.admin.forms.dialog.header"/></h3>',
										labels: {
			        						form:'<s:message code="person.summary.admin.forms.form" javaScriptEscape="true"/>',
											identifier: '<s:message code="person.summary.admin.forms.identifier" javaScriptEscape="true"/>',
											associatedTeams: '<s:message code="person.summary.admin.forms.associatedTeams" javaScriptEscape="true"/>'
										},
										narrative:{
        									summary: '<s:message code="person.summary.admin.forms.dialog.add.narrative" javaScriptEscape="true"/>'
										},
										successMessage: '<s:message code="person.summary.admin.forms.dialog.add.successMessage" javaScriptEscape="true"/>',
			        					url: '<s:url value="/rest/formConfigurableTypeReference"/>',
										formDefinitionUrl: '<s:url value="/rest/formDefinition?includeHistoric=false&s={sortBy}&pageNumber=-1&pageSize=-1"/>',
										teamUrl: '<s:url value="/rest/team?pageNumber=1&pageSize=-1&s={sortBy}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>' 
									},
									deleteFormConfigurableTypeReferenceView: {
			        					headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-wrench fa-inverse fa-stack-1x"></i></span> <s:message code="person.summary.admin.forms.dialog.header"/></h3>',
										narrative:{
        									summary: '<s:message code="person.summary.admin.forms.dialog.delete.narrative" javaScriptEscape="true"/>'
										},
										successMessage: '<s:message code="person.summary.admin.forms.dialog.delete.successMessage" javaScriptEscape="true"/>',
			        					url: '<s:url value="/rest/formConfigurableTypeReference/{id}"/>'
									}
								}
			        		}
        				}
        			}
        		}
        	}).render();
        	
        	// Make sure to dispatch the current hash-based URL which was set by
            // the server to our route handlers.            
            if (personSummaryAdminTabs.hasRoute(personSummaryAdminTabs.getPath())) {
            	personSummaryAdminTabs.dispatch();
            } else {
            	personSummaryAdminTabs.activateTab('casenote');
            }
        	
        });
        </script>
    </c:when>
    <c:otherwise>
        <t:insertDefinition name="access.denied"/>
    </c:otherwise>
</c:choose>