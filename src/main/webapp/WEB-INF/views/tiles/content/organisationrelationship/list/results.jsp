<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson"/>
<sec:authorize access="hasPermission(null, 'Organisation','Organisation.View')" var="canViewOrganisation"/>


<sec:authorize access="hasPermission(null, 'PersonOrganisationRelationship','PersonOrganisationRelationship.View')" var="canViewOrganisationRelationship"/>
<sec:authorize access="hasPermission(null, 'PersonOrganisationRelationship','PersonOrganisationRelationship.Update')" var="canEditOrganisationRelationship"/>
<sec:authorize access="hasPermission(null, 'PersonOrganisationRelationship','PersonOrganisationRelationship.Remove')" var="canRemoveOrganisationRelationship"/>
<sec:authorize access="hasPermission(null, 'PersonOrganisationRelationship','PersonOrganisationRelationship.Close')" var="canCloseOrganisationRelationship"/>


<div id="organisationRelationshipResults"></div>

<script>
Y.use('organisation-relationship-controller-view','yui-base', function(Y) {
  var subject = {
    subjectType: 'organisation',
    subjectName: '${esc:escapeJavaScript(organisation.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(organisation.organisationIdentifier)}',
    subjectId: '<c:out value="${organisation.id}"/>',
    //Needed by the relationship dialog - will be removed when that is updated to use the subject
    id : '<c:out value="${organisation.id}"/>',
    name:'${esc:escapeJavaScript(organisation.name)}',
    identifier:'${esc:escapeJavaScript(organisation.organisationIdentifier)}'
  };
  
  var permissions = {
      canView: '${canViewOrganisationRelationship}' === 'true',
      canViewList: '${canViewOrganisationRelationship}' === 'true',
      canViewRelationships: '${canViewOrganisation}'==='true', 
      canViewOrganisationRelationship: '${canViewOrganisationRelationship}' === 'true'
  };
  
  var searchConfig={
          permissions:permissions,
  };
  
  
  
  var personOrganisationSearchConfig = Y.merge(searchConfig,{
      sortBy: [
               {'personVO!name':'asc'},
        {startDate:'desc'}
      ],
      labels: {
        id:'<s:message code="person.organisation.relationships.general.results.id" javaScriptEscape="true"/>',
        name:'<s:message code="person.organisation.relationships.general.results.name" javaScriptEscape="true"/>',
        relationship:'<s:message code="person.organisation.relationships.general.results.type" javaScriptEscape="true"/>',
        startDate:'<s:message code="person.organisation.relationships.general.results.startDate" javaScriptEscape="true"/>',
        closeDate:'<s:message code="person.organisation.relationships.general.results.closeDate" javaScriptEscape="true"/>',
        actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>'
      },
      noDataMessage: '<s:message code="person.organisation.relationships.general.no.results" javaScriptEscape="true"/>',
      url:'<s:url value="/rest/{subjectType}/{id}/personRelationship?relationshipClass=EMPLOYEREMPLOYEE&s={sortBy}&pageNumber={page}&pageSize={pageSize}&relationshipsAndDates=true"/>'
  });
  
  
  var config = {
    container: '#organisationRelationshipResults',
    
    personOrganisationConfig:{
      searchConfig: personOrganisationSearchConfig
    },
    permissions: permissions,
    subject: subject || {},
    viewPersonURL:'<s:url value="/person?id={personId}" />',
  };

  var relationshipView = new Y.app.relationship.OrganisationRelationshipControllerView(config).render();
});
</script>