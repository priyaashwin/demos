<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<sec:authorize access="hasPermission(null, 'PersonOrganisationRelationship','PersonOrganisationRelationship.GET')" var="canView"/>

<c:choose>
  <c:when test="${canView eq true}">
    <t:insertTemplate template="/WEB-INF/views/tiles/content/organisationrelationship/list/results.jsp" />
  </c:when>
  <c:otherwise>
    <%-- Insert access denied tile --%>
    <t:insertDefinition name="access.denied" />
  </c:otherwise>
</c:choose>
