<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'AdoptionInformation','AdoptionInformation.GET')" var="canView" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'AdoptionInformation','AdoptionInformation.ADD')" var="canAdd" />
	<sec:authorize access="hasPermission(null, 'AdoptionInformation','AdoptionInformation.UPDATE')" var="canUpdate" />
</c:if>

<div id="adoptionInformationResults"></div>

<script>
Y.use('adoptinformation-controller-view', function(Y) {
    var permissions = {
        canUpdateAdoptionInformation: '${canUpdate}' === 'true',
        canViewAdoptionInformation: '${canView}' === 'true',
        canAddAdoptionInformation: '${canAdd}' === 'true'
    };

    var subjectNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

    var subject = {
        subjectId: '<c:out value="${person.id}"/>',
        subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
        subjectName: '${esc:escapeJavaScript(person.name)}',
        subjectType: 'person'
    };

    var labels = {
        adoptionInformationView: {
            accordionTitle: '<s:message code="adoptioninformation.table.header" javaScriptEscape="true"/>',
            adoptionInformationTable: {
            	header: '<s:message code="adoptioninformation.table.header" javaScriptEscape="true"/>',
            	dateShouldBePlaced: '<s:message code="adoptioninformation.table.dateShouldBePlaced" javaScriptEscape="true"/>',
            	dateMatched: '<s:message code="adoptioninformation.table.dateMatched" javaScriptEscape="true"/>',
            	datePlaced: '<s:message code="adoptioninformation.table.datePlaced" javaScriptEscape="true"/>',
            	legalStatusOfAdopters: '<s:message code="adoptioninformation.table.legalStatusOfAdopters" javaScriptEscape="true"/>',
            	noData: '<s:message code="adoptioninformation.table.noData" javaScriptEscape="true"/>',
            	edit: '<s:message code="adoptioninformation.table.edit" javaScriptEscape="true"/>',
                view: '<s:message code="adoptioninformation.table.view" javaScriptEscape="true"/>',
                action: '<s:message code="common.column.actions" javaScriptEscape="true"/>'
            },
            addAdoptionInformation: '<s:message code="adoptioninformation.add.button" javaScriptEscape="true"/>'
        },
        
        addAdoptionInformation: {
        	dateShouldBePlaced: '<s:message code="adoptioninformation.dialog.add.dateShouldBePlaced" javaScriptEscape="true"/>',
        	dateMatched: '<s:message code="adoptioninformation.dialog.add.dateMatched" javaScriptEscape="true"/>',
        	datePlaced: '<s:message code="adoptioninformation.dialog.add.datePlaced" javaScriptEscape="true"/>',
        	numberOfAdopters: '<s:message code="adoptioninformation.dialog.add.numberOfAdopters" javaScriptEscape="true"/>',
        	genderOfAdopters: '<s:message code="adoptioninformation.dialog.add.genderOfAdopters" javaScriptEscape="true"/>',
        	formerFosterCarers: '<s:message code="adoptioninformation.dialog.add.formerFosterCarers" javaScriptEscape="true"/>',
        	legalStatusOfAdopters: '<s:message code="adoptioninformation.dialog.add.legalStatusOfAdopters" javaScriptEscape="true"/>',
        	disruptionDate: '<s:message code="adoptioninformation.dialog.add.disruptionDate" javaScriptEscape="true"/>',
        	familyFindingActivity: '<s:message code="adoptioninformation.dialog.add.familyFindingActivity" javaScriptEscape="true"/>',
        	otherDisruptionReason: '<s:message code="adoptioninformation.dialog.add.otherDisruptionReason" javaScriptEscape="true"/>',
        	dateNoLongerPlanned: '<s:message code="adoptioninformation.dialog.add.dateNoLongerPlanned" javaScriptEscape="true"/>',
        	reasonNoLongerPlanned: '<s:message code="adoptioninformation.dialog.add.reasonNoLongerPlanned" javaScriptEscape="true"/>',
        	adoptionReversedDisrupted: '<s:message code="adoptioninformation.dialog.add.adoptionReversedDisrupted" javaScriptEscape="true"/>'
        },
        viewAdoptionInformation: {
        	dateShouldBePlaced: '<s:message code="adoptioninformation.dialog.view.dateShouldBePlaced" javaScriptEscape="true"/>',
        	dateMatched: '<s:message code="adoptioninformation.dialog.view.dateMatched" javaScriptEscape="true"/>',
        	datePlaced: '<s:message code="adoptioninformation.dialog.view.datePlaced" javaScriptEscape="true"/>',
        	numberOfAdopters: '<s:message code="adoptioninformation.dialog.view.numberOfAdopters" javaScriptEscape="true"/>',
        	genderOfAdopters: '<s:message code="adoptioninformation.dialog.view.genderOfAdopters" javaScriptEscape="true"/>',
        	formerFosterCarers: '<s:message code="adoptioninformation.dialog.view.formerFosterCarers" javaScriptEscape="true"/>',
        	legalStatusOfAdopters: '<s:message code="adoptioninformation.dialog.view.legalStatusOfAdopters" javaScriptEscape="true"/>',
        	disruptionDate: '<s:message code="adoptioninformation.dialog.view.disruptionDate" javaScriptEscape="true"/>',
        	familyFindingActivity: '<s:message code="adoptioninformation.dialog.view.familyFindingActivity" javaScriptEscape="true"/>',
        	otherDisruptionReason: '<s:message code="adoptioninformation.dialog.view.otherDisruptionReason" javaScriptEscape="true"/>',
        	dateNoLongerPlanned: '<s:message code="adoptioninformation.dialog.view.dateNoLongerPlanned" javaScriptEscape="true"/>',
        	reasonNoLongerPlanned: '<s:message code="adoptioninformation.dialog.view.reasonNoLongerPlanned" javaScriptEscape="true"/>',
        	adoptionReversedDisrupted: '<s:message code="adoptioninformation.dialog.view.adoptionReversedDisrupted" javaScriptEscape="true"/>'
        },
        editAdoptionInformation: {
        	dateShouldBePlaced: '<s:message code="adoptioninformation.dialog.edit.dateShouldBePlaced" javaScriptEscape="true"/>',
        	dateMatched: '<s:message code="adoptioninformation.dialog.edit.dateMatched" javaScriptEscape="true"/>',
        	datePlaced: '<s:message code="adoptioninformation.dialog.edit.datePlaced" javaScriptEscape="true"/>',
        	numberOfAdopters: '<s:message code="adoptioninformation.dialog.edit.numberOfAdopters" javaScriptEscape="true"/>',
        	genderOfAdopters: '<s:message code="adoptioninformation.dialog.edit.genderOfAdopters" javaScriptEscape="true"/>',
        	formerFosterCarers: '<s:message code="adoptioninformation.dialog.edit.formerFosterCarers" javaScriptEscape="true"/>',
        	legalStatusOfAdopters: '<s:message code="adoptioninformation.dialog.edit.legalStatusOfAdopters" javaScriptEscape="true"/>',
        	disruptionDate: '<s:message code="adoptioninformation.dialog.edit.disruptionDate" javaScriptEscape="true"/>',
        	familyFindingActivity: '<s:message code="adoptioninformation.dialog.edit.familyFindingActivity" javaScriptEscape="true"/>',
        	otherDisruptionReason: '<s:message code="adoptioninformation.dialog.edit.otherDisruptionReason" javaScriptEscape="true"/>',
        	dateNoLongerPlanned: '<s:message code="adoptioninformation.dialog.edit.dateNoLongerPlanned" javaScriptEscape="true"/>',
        	reasonNoLongerPlanned: '<s:message code="adoptioninformation.dialog.edit.reasonNoLongerPlanned" javaScriptEscape="true"/>',
        	adoptionReversedDisrupted: '<s:message code="adoptioninformation.dialog.edit.adoptionReversedDisrupted" javaScriptEscape="true"/>'
        }
    };

    new Y.app.adoptioninformation.AdoptionInformationControllerView({
        container: '#adoptionInformationResults',
        labels: labels,
        permissions: permissions,
        subject: subject,
        url: '<s:url value="/rest/person/{subjectId}/adoptionInformation?pageNumber=-1&pageSize=-1&s={sortBy}"><s:param name="sortBy" value="[{\"dateShouldBePlaced\":\"desc\"}]" /></s:url>',
        pictureUrl: '<s:url value="/rest/{subjectType}/{subjectId}/picture/content"/>',
        dialogConfig: {
            adoptionInformation: {
                personDetailsModelURL : '<s:url value="/rest/person/${person.id}"/>',
            	views: {
            		add: {
            			header: {
            				text: '<s:message code="adoptioninformation.dialog.add.header" javaScriptEscape="true"/>',
            	        	icon: 'fa eclipse-childlookedafter'
            			},
            			narrative: {
            				summary: '<s:message code="adoptioninformation.dialog.add.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
            			successMessage: '<s:message code="adoptioninformation.dialog.add.successMessage" javaScriptEscape="true"/>',
        	 			  labels: labels.addAdoptionInformation,
            			config: {
            				url: '<s:url value="/rest/adoptionInformation" />'
                  }
            		},
                edit: {
                  header: {
                        text: '<s:message code="adoptioninformation.dialog.edit.header" javaScriptEscape="true"/>',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="adoptioninformation.dialog.edit.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="adoptioninformation.dialog.edit.successMessage" javaScriptEscape="true"/>',
                    labels: labels.editAdoptionInformation,
                    config: {
                        url: '<s:url value="/rest/adoptionInformation/{id}"/>'
                    }
                },
                view: {
                    header: {
                        text: '<s:message code="adoptioninformation.dialog.view.header" javaScriptEscape="true"/>',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="adoptioninformation.dialog.view.summary" javaScriptEscape="true" />',
                        description: subjectNarrativeDescription
                    },
                    labels: labels.viewAdoptionInformation,
                    permissions: permissions,
                    config: {
                        url: '<s:url value="/rest/adoptionInformation/{id}"/>'
                    }
                }
              }
            }
        }
    }).render();
});

</script>
