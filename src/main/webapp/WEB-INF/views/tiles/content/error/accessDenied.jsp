<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<p><s:message code="access.denied"/></p>               
<p>Please <a href ='<s:url value="/index"/>'>click here</a> to return to the home page.