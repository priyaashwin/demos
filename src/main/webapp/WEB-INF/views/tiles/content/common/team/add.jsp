<%@ page import="java.util.Locale"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<div id="addTeamDialog"></div>
<tilesx:useAttribute name="preventPageLoad" ignore="true" />
<script>
//todo - move this into a YUI module
Y.use('yui-base',
    'event-custom-base',
    'categories-organisation-component-TeamRegion',
    'categories-organisation-component-TeamSector',
    'categories-organisation-component-TeamSectorSubType',
    'categories-organisation-component-OrganisationType',
    'categories-organisation-component-OrganisationSubType',
    'categories-person-component-ProfessionalTitle',
    'categories-person-component-Gender',
    function(Y) {
  if (typeof uspAddOrganisation !== 'undefined') {
    var teamDialog;
    var dialogRef=function(ref){
      teamDialog=ref;
    };
    ReactDOM.render(React.createElement(uspAddOrganisation.AddTeamDialog, {
      ref:dialogRef,
      dialogConfig: {
        header: {
          icon: 'eclipse-team',
          text: '<s:message code="team.add.header" javaScriptEscape="true"/>'
        },
        successMessage: '<s:message code="team.message.add.success" javaScriptEscape="true"/>',
        narrative: {
           summary: '<s:message code="team.add.narrative" javaScriptEscape="true"/>'
         },
        delayMessage:'${preventPageLoad}'!=='true'
      },
      labels: {
        name: '<s:message code="newChildTeamAndProfessionalRelationshipsVO.name" javaScriptEscape="true"/>',
        description: '<s:message code="newChildTeamAndProfessionalRelationshipsVO.description" javaScriptEscape="true"/>',
        securityRecordTypeConfigurationId: '<s:message code="newChildTeamAndProfessionalRelationshipsVO.securityRecordTypeConfigurationId" javaScriptEscape="true"/>',
        startDate: '<s:message code="organisation.add.startDate" javaScriptEscape="true"/>',
        effectiveDate: '<s:message code="organisation.add.effectiveDate" javaScriptEscape="true"/>',
        effectiveDateInvalid:'<s:message code="organisation.add.effectiveDate.invalid" javaScriptEscape="true"/>',
        securityProfile:{
          securityRecordTypeConfigurationId: '<s:message code="newChildTeamAndProfessionalRelationshipsVO.securityRecordTypeConfigurationId" javaScriptEscape="true"/>',
          placeholder: '<s:message code="team.securityProfileSearchPlaceholder" javaScriptEscape="true"/>'
        },
        parentOrganisation:{
          parentOrganisationId:'<s:message code="organisation.parentOrganisation" javaScriptEscape="true"/>',
          placeholder: '<s:message code="organisation.organisationSearchPlaceholder" javaScriptEscape="true"/>'
        },
        region:'<s:message code="newChildTeamAndProfessionalRelationshipsVO.region" javaScriptEscape="true"/>',
        sector:'<s:message code="newChildTeamAndProfessionalRelationshipsVO.sector" javaScriptEscape="true"/>',
        emptySector:'<s:message code="team.sector.empty" javaScriptEscape="true"/>',
        subSector:'<s:message code="newChildTeamAndProfessionalRelationshipsVO.subSector" javaScriptEscape="true"/>',
        emptySubSector:'<s:message javaScriptEscape="true" code="team.subSector.empty"/>',
        members:{
          title:'<s:message code="team.members" javaScriptEscape="true"/>',
          placeholder: '<s:message code="team.personSearchPlaceholder" javaScriptEscape="true"/>'
        },
        validation:{
          nameMandatory: '<s:message code="common.error.name" javaScriptEscape="true"/>',
          effectiveDateInvalidDate:'<s:message code="organisation.add.effectiveDate.invalid" javaScriptEscape="true"/>',
          parentOrganisationIdRequired:'<s:message code="organisation.add.parentOrganisationId.required" javaScriptEscape="true"/>',
          effectiveDateRequired:'<s:message code="organisation.add.effectiveDate.required" javaScriptEscape="true"/>',
          securityRecordTypeConfigurationIdValidationErrorMessage:'<s:message code="securityRecordTypeConfigurationId.validationError" javaScriptEscape="true"/>',
          startDateInvalid:'<s:message code="organisation.add.startDate.invalid"/>'
        }
      },
      codedEntries: {
        teamSectorCategory: Y.uspCategory.organisation.teamSector.category,
        teamSubSectorCategory: Y.uspCategory.organisation.teamSectorSubType.category,
        teamRegionCategory: Y.uspCategory.organisation.teamRegion.category,
        organisationTypeCategory: Y.uspCategory.organisation.organisationType.category,
        organisationSubTypeCategory: Y.uspCategory.organisation.organisationSubType.category,
        professionalTitleCategory: Y.uspCategory.person.professionalTitle.category,
        genderCategory: Y.uspCategory.person.gender.category.codedEntries
      },
      endpoints: {
        organisationAutocompleteEndpoint: '<s:url value="/rest/organisation" />',
        securityProfileAutocompleteEndpoint:'<s:url value="/rest/securityRecordTypeConfiguration"/>',
        newTeamEndpoint:'<s:url value="/rest/team?withProfessionalRelationships=true" />',
        newOwnedTeamEndpoint:'<s:url value="/rest/organisation/{parentOrganisationId}/ownedTeam" />',
        professionalTeamRelationshipTypesEndpoint:'<s:url value="/rest/professionalTeamRelationshipType"/>',
        personAutocompleteEndpoint: '<s:url value="/rest/person"/>',
      }
    }), document.getElementById('addTeamDialog'));
  
  }
 
  
  Y.on('teamDialog:add', function(e){
    if(teamDialog){
      teamDialog.showDialog();
    }
  });
});
</script>  
  
<c:if test="${preventPageLoad ne true}">
  <script>
  var teamUrl = '<s:url value="/organisation?id={id}"/>';
  document.addEventListener('team:saved', function(e){
    var detail=e.detail||{};
    if(detail.teamId){
      window.location = Y.Lang.sub(teamUrl, {
        id: detail.teamId
        });
      }
  });
  </script>
  </c:if>
