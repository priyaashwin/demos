<%@ page import="java.util.Locale"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<div id="addOrganisationDialog"></div>

<script>
//todo - move this into a YUI module
Y.use('yui-base',
    'event-custom-base',
    'categories-organisation-component-OrganisationType',
    'categories-organisation-component-OrganisationSubType',
    function(Y) {
  if (typeof uspAddOrganisation !== 'undefined') {
    var organisationDialog;
    var dialogRef=function(ref){
      organisationDialog=ref;
    };
    ReactDOM.render(React.createElement(uspAddOrganisation.AddOrganisationDialog, {
      ref:dialogRef,
      dialogConfig: {
        header: {
          icon: 'fa-building-o',
          text: '<s:message code="organisation.main.header" javaScriptEscape="true"/>'
        },
        successMessage: '<s:message code="organisation.add.success.message" javaScriptEscape="true"/>',
        narrative: {
           summary: '<s:message code="organisation.add.narrative" javaScriptEscape="true"/>'
         }
      },
      labels: {
        name: '<s:message code="organisation.add.orgName"/>',
        type:'<s:message code="organisation.add.type"/>',
        emptyType:'<s:message javaScriptEscape="true" code="organisation.type.empty"/>',
        subType:'<s:message code="organisation.add.subType"/>',
        emptySubType:'<s:message javaScriptEscape="true" code="organisation.subType.empty"/>',
        startDate:'<s:message javaScriptEscape="true" code="organisation.add.startDate"/>',
        effectiveDate: '<s:message code="organisation.add.effectiveDate"/>',
        effectiveDateInvalid:'<s:message code="organisation.add.effectiveDate.invalid"/>',
        parentOrganisation:{
          parentOrganisationId:'<s:message code="organisation.parentOrganisation"/>',
          placeholder: '<s:message code="organisation.organisationSearchPlaceholder"/>'
        },
        validation:{
          nameMandatory: '<s:message code="common.error.name"/>',
          effectiveDateInvalidDate:'<s:message code="organisation.add.effectiveDate.invalid"/>',
          parentOrganisationIdRequired:'<s:message code="organisation.add.parentOrganisationId.required"/>',
          effectiveDateRequired:'<s:message code="organisation.add.effectiveDate.required"/>',
          startDateInvalid:'<s:message code="organisation.add.startDate.invalid"/>'
        }
      },
      codedEntries: {
        organisationTypeCategory: Y.uspCategory.organisation.organisationType.category,
        organisationSubTypeCategory: Y.uspCategory.organisation.organisationSubType.category
      },
      endpoints: {
        organisationAutocompleteEndpoint: '<s:url value="/rest/organisation" />',
        newOrganisationEndpoint:'<s:url value="/rest/organisation" />',
        newOwnedOrganisationEndpoint:'<s:url value="/rest/organisation/{parentOrganisationId}/owned" />'
      }
    }), document.getElementById('addOrganisationDialog'));
  
  }
  
  var organisationUrl = '<s:url value="/organisation?id={id}"/>';
  
  document.addEventListener('organisation:saved', function(e){
    var detail=e.detail||{};
    if(detail.organisationId){
      window.location = Y.Lang.sub(organisationUrl, {
        id: detail.organisationId
      });
    }
  });
  
  
  Y.on('organisationDialog:add', function(e){
    if(organisationDialog){
      organisationDialog.showDialog();
    }
  });
});
</script>
