<%@ page import="java.util.Locale"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc"
	uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize
	access="hasPermission(null, 'Security','Security.Open_Access')"
	var="hasOpenAccess" />

<div id="addPersonDialog"></div>

<script>
//todo - move this into a YUI module
Y.use('yui-base', 
    'categories-person-component-Title', 
    'categories-person-component-PersonType', 
    'categories-person-component-Ethnicity', 
    'categories-person-component-Gender', 
    'categories-person-component-ProfessionalTitle',
    'json-parse', 
    'event-custom-base',
    function(Y) {
  if (typeof uspAddPerson !== 'undefined') {
    var professionalRelationshipRoles = Y.JSON.parse('${esc:escapeJavaScript(professionalRelationshipRoles)}');
    var personDialog;
    var dialogRef=function(ref){
      personDialog=ref;
    };
    ReactDOM.render(React.createElement(uspAddPerson.AddPersonDialog, {
      ref:dialogRef,
      currentUser: {
        id: '${currentUser.id}',
        personIdentifier: '${esc:escapeJavaScript(currentUser.personIdentifier)}',
        name: '${esc:escapeJavaScript(currentUser.name)}',
        forename: '${esc:escapeJavaScript(currentUser.forename)}',
        surname: '${esc:escapeJavaScript(currentUser.surname)}',
        professionalTitle: '${esc:escapeJavaScript(currentUser.professionalTitle)}'
      },
      isOpenAccess: '${hasOpenAccess}' === 'true',
      dialogConfig: {
        header: {
          icon: 'fa-user',
          text: '<s:message code="person.add.header" javaScriptEscape="true"/>'
        },
        successMessage: '<s:message code="person.add.success.message" javaScriptEscape="true"/>',
        narrative: {
          demographics: {
            summary: '<s:message code="person.add.summary" javaScriptEscape="true"/>',
            description: '<s:message code="person.add.description" javaScriptEscape="true"/>'
          },
          duplicateCheck: {
            summary: '<s:message code="person.potentialDuplicates.summary" javaScriptEscape="true"/>',
            description: '<s:message code="person.potentialDuplicates.description" javaScriptEscape="true"/>'
          },
          additionalDetails: {
            summary: '<s:message code="person.additionalDetails.summary" javaScriptEscape="true"/>',
            description: '<s:message code="person.additionalDetails.description" javaScriptEscape="true"/>'
          }
        }
      },
      labels: {
        forename: '<s:message code="newPersonVO.forename" javaScriptEscape="true"/>',
        surname: '<s:message code="newPersonVO.surname" javaScriptEscape="true"/>',
        personType: '<s:message code="newPersonVO.personTypes" javaScriptEscape="true"/>',
        title: '<s:message code="newPersonVO.title" javaScriptEscape="true"/>',
        dateOfBirth: '<s:message code="person.dateOfBirth" javaScriptEscape="true"/>',
        gender: '<s:message code="newPersonVO.gender" javaScriptEscape="true"/>',
        ethnicity: '<s:message code="newPersonVO.ethnicity" javaScriptEscape="true"/>',
        age: '<s:message code="person.age" javaScriptEscape="true"/>',
        dateOfBirthMessage: '<s:message code="person.add.dateOfBirthPopup.content" javaScriptEscape="true"/>',
        dateOfBirthEstimatedMessage: '<s:message code="person.add.dobEstimateMessage" javaScriptEscape="true"/>',
        dateOfBirthEstimated: '<s:message code="person.add.dobExact" javaScriptEscape="true"/>',
        dueDateMessage: '<s:message code="person.add.dueDatePopup.content" javaScriptEscape="true"/>',
        dueDate: '<s:message code="person.add.dueDate" javaScriptEscape="true"/>',
        deceasedDateOfBirthMessage: '<s:message code="person.add.deceasedBornDiedDatePopup.content" javaScriptEscape="true"/>',
        diedDateEstimatedMessage: '<s:message code="person.add.deceasedEstimateMessage" javaScriptEscape="true"/>',
        diedDateEstimated: '<s:message code="person.add.dobExact" javaScriptEscape="true"/>',
        died: '<s:message code="person.died" javaScriptEscape="true"/>',
        ageAtDeath: '<s:message code="person.ageAtDeath" javaScriptEscape="true"/>',
        unborn: '<s:message code="person.unborn" javaScriptEscape="true"/>',
        deceased: '<s:message code="person.deceased" javaScriptEscape="true"/>',
        unBornDefaultValue: '<s:message code="person.add.forename.unBornDefaultValue" javaScriptEscape="true"/>',
        number: '<s:message code="person.number" javaScriptEscape="true"/>',
        nhsNumber: '<s:message code="person.nhsNumber" javaScriptEscape="true"/>',
        nhsNumberFirstPlaceholder: '<s:message code="person.add.nhsNumberFirstPlaceholder" javaScriptEscape="true"/>',
        nhsNumberSecondPlaceholder: '<s:message code="person.add.nhsNumberSecondPlaceholder" javaScriptEscape="true"/>',
        nhsNumberThirdPlaceholder: '<s:message code="person.add.nhsNumberThirdPlaceholder" javaScriptEscape="true"/>',
        chiNumber: '<s:message code="person.chiNumber" javaScriptEscape="true"/>',
        address: '<s:message code="person.address" javaScriptEscape="true"/>',
        organisationName: '<s:message code="person.professional.organisation" javaScriptEscape="true"/>',
        professionalTitle: '<s:message code="person.professional.title" javaScriptEscape="true"/>',
        allocatedWorker: '<s:message code="person.allocatedWorker" javaScriptEscape="true"/>',
        namedPerson: '<s:message code="person.namedPerson" javaScriptEscape="true"/>',
        doNotDisclose:'<s:message code="person.address.doNotDisclose" javaScriptEscape="true"/>',
        validation: {
          chiGenderDigitInvalid: '<s:message code="person.chiGenderDigitInvalid" javaScriptEscape="true"/>',
          chiEstimatedDateOfBirth: '<s:message code="person.chiEstimatedDateOfBirth" javaScriptEscape="true"/>',
          chiDateOfBirthInvalid: '<s:message code="person.chiDateOfBirth" javaScriptEscape="true"/>',
          forenameMandatory: '<s:message code="person.validation.mandatory.forename" javaScriptEscape="true"/>',
          surnameMandatory: '<s:message code="person.validation.mandatory.surname" javaScriptEscape="true"/>',
          personTypeMandatory: '<s:message code="person.validation.mandatory.personType" javaScriptEscape="true"/>',
          ethnicityMandatory: '<s:message code="person.validation.mandatory.ethnicity" javaScriptEscape="true"/>',
          genderMandatory: '<s:message code="person.validation.mandatory.gender" javaScriptEscape="true"/>',
          dateOfBirthMandatory: '<s:message code="person.validation.mandatory.dob" javaScriptEscape="true"/>',
          nhsNumberInvalid: '<s:message code="person.validation.invalid.nhsNumber" javaScriptEscape="true"/>',
          chiNumberInvalid: '<s:message code="person.validation.invalid.chiNumber" javaScriptEscape="true"/>',
          dueDateInvalidDate: '<s:message code="typeMismatch.newPersonVO.dueDate" javaScriptEscape="true"/>',
          dueDateInvalid: '<s:message code="BeforeDate.newPersonVO.dueDate" javaScriptEscape="true"/>',
          dueDateMandatory: '<s:message code="person.validation.mandatory.dueDate" javaScriptEscape="true"/>',
          diedDateAfterToday: '<s:message code="TodayOrEarlier.newPersonVO.diedDate" javaScriptEscape="true"/>',
          diedDateBeforeDateOfBirth: '<s:message code="DiedDateBeforeDateOfBirthException.newPersonVO.diedDate" javaScriptEscape="true"/>',
          organisationNameMandatory: '<s:message code="person.validation.mandatory.organisationName"  javaScriptEscape="true"/>',
          professionalTitleMandatory: '<s:message code="person.validation.mandatory.professionalTitle" javaScriptEscape="true"/>',
          relatedPersonMandatory: '<s:message code="relationship.validation.mandatory.relatedPerson" javaScriptEscape="true"/>',
          relationshipTypeMandatory: '<s:message code="relationship.validation.mandatory.relationshipType" javaScriptEscape="true"/>'          
        },
        duplicateLoading: '<s:message code="person.potentialDuplicates.loading" javaScriptEscape="true"/>',
        duplicatesFound: '<s:message code="person.potentialDuplicates.number" javaScriptEscape="true"/>',
        relatedPerson: {
          autoRelatedPersonId: '<s:message code="relationship.relatedPersonSearch" javaScriptEscape="true"/>',
          autoRelationshipTypeId: '<s:message code="relationship.role" javaScriptEscape="true"/>',
          placeholder: '<s:message code="relationship.autoCompletePlaceholder" javaScriptEscape="true"/>',
          openAccessWarning: '<s:message code="relationship.add.openaccess.warning" javaScriptEscape="true"/>',
          skipAutoRelationship:'<s:message code="relationship.add.openaccess.skip" javaScriptEscape="true"/>'
        },
        relatedTeam:{
          autoRelatedTeamId: '<s:message code="relationship.relatedTeamSearch" javaScriptEscape="true"/>',
          openAccessWarning: '<s:message code="relationship.addProfessional.openaccess.warning" javaScriptEscape="true"/>',
          skipAutoRelationship: '<s:message code="relationship.addProfessional.openaccess.skip" javaScriptEscape="true"/>',
          placeholder: '<s:message code="relationship.addProfessional.autoCompletePlaceholder" javaScriptEscape="true"/>'
        },
        detailsNotRequired: '<s:message code="person.additionalDetails.noDetailsRequired" javaScriptEscape="true"/>',
  		invalidFosterAge: '<s:message code="person.validation.invalidFosterAge" javaScriptEscape="true"/>',
  	    invalidAdopterAge: '<s:message code="person.validation.invalidAdopterAge" javaScriptEscape="true"/>'

      },
      codedEntries: {
        personTypeCategory: Y.uspCategory.person.personType.category,
        titleCategory: Y.uspCategory.person.title.category,
        ethnicityCategory: Y.uspCategory.person.ethnicity.category,
        genderCategory: Y.uspCategory.person.gender.category,
        professionalTitleCategory: Y.uspCategory.person.professionalTitle.category
      },
      relationshipRoles: {
        professional: professionalRelationshipRoles
      },
      endpoints: {
        duplicateSearchEndpoint: '<s:url value="/rest/person?pageNumber={page}&pageSize={pageSize}" />',
        relationshipAutocompleteEndpoint: '<s:url value="/rest/person?personType=PROFESSIONAL"/>',
        newPersonEndpoint: '<s:url value="/rest/person?checkNhsNumber=true"/>',
        teamAutocompleteEndpoint: '<s:url value="/rest/organisation/active?escapeSpecialCharacters=true&byOrganisationType=true&organisationTypes=TEAM"/>'
      }
    }), document.getElementById('addPersonDialog'));
  }
  
  var personSummaryUrl = '<s:url value="/summary/person?id={id}"/>';
  var personUrl = '<s:url value="/person?id={id}"/>';
  document.addEventListener('duplicatePerson:view', function(e){
    var detail=e.detail||{};
    if(detail.personId){
      window.location = Y.Lang.sub(personSummaryUrl, {
        id: detail.personId
      });
    }
  });
  
  document.addEventListener('person:saved', function(e){
    var detail=e.detail||{};
    if(detail.personId){
      window.location = Y.Lang.sub(personUrl, {
        id: detail.personId
      });
    }
  });
  
  
  Y.on('personDialog:add', function(e){
    if(personDialog){
      personDialog.showDialog();
    }
  });
});
</script>
