<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'CareLeaver','CareLeaver.GET')" var="canView" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
    <sec:authorize access="hasPermission(null, 'CareLeaver','CareLeaver.ADD')" var="canAdd" />
    <sec:authorize access="hasPermission(null, 'CareLeaver','CareLeaver.DELETE')" var="canRemove" />
    <sec:authorize access="hasPermission(null, 'DeletionRequest','DeletionRequest.GET')" var="canRequestDeletion" />

</c:if>

<div id="careLeaverResultsContainer"></div>


<script>
    Y.use('chronology-controller-view', 'yui-base', 'json-parse', 'careleaver-controller-view', function (Y) {
        var permissions = {
            canView: '${canView}' === 'true',
            canAddCareLeaver: '${canAdd}' === 'true' && '${canAddCareLeaverDetails}' === 'true',
            canRemoveCareLeaver: '${canRemove}' === 'true' && '${canRequestDeletion}' === 'true'
        };
        var subject = {
            subjectId: '<c:out value="${person.id}"/>',
            subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
            subjectName: '${esc:escapeJavaScript(person.name)}',
        };

        var subjectNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

        var showWelshView = '${careLeaverWelsh}' === 'true';

        var labels = {
            addCareLeaver: {
                successMessage: '<s:message code="careleaver.dialog.add.successMessage" javaScriptEscape="true"/>',
                careLeaverEligibility: '<s:message code="careleaver.dialog.add.careLeaverEligibility" javaScriptEscape="true"/>',
                reasonQualifying: '<s:message code="careleaver.dialog.add.reasonQualifying" javaScriptEscape="true"/>',
                contactDate: '<s:message code="careleaver.dialog.add.contactDate" javaScriptEscape="true"/>',
                inTouch: '<s:message code="careleaver.dialog.add.inTouch" javaScriptEscape="true"/>',
                mainActivity: '<s:message code="careleaver.dialog.add.mainActivity" javaScriptEscape="true"/>',
                accommodation: '<s:message code="careleaver.dialog.add.accommodation" javaScriptEscape="true"/>',
                accommodationSuitability: '<s:message code="careleaver.dialog.add.accommodationSuitability" javaScriptEscape="true"/>',
                multipleOccupancy: '<s:message code="careleaver.dialog.add.multipleOccupancy" javaScriptEscape="true"/>'
            },
            addCareLeaverWelsh: {
                successMessage: '<s:message code="careleaver.dialog.addWelsh.successMessage" javaScriptEscape="true"/>',
                careLeaverEligibility: '<s:message code="careleaver.dialog.add.careLeaverEligibility" javaScriptEscape="true"/>',
                contactDate: '<s:message code="careleaver.dialog.add.contactDate" javaScriptEscape="true"/>',
                inTouch: '<s:message code="careleaver.dialog.add.inTouch" javaScriptEscape="true"/>',
                mainActivity: '<s:message code="careleaver.dialog.addWelsh.mainActivity" javaScriptEscape="true"/>',
                accommodation: '<s:message code="careleaver.dialog.add.accommodation" javaScriptEscape="true"/>',
                accommodationSuitability: '<s:message code="careleaver.dialog.add.accommodationSuitability" javaScriptEscape="true"/>'
            },
            viewCareLeaver: {
                header: '<s:message code="careleaver.dialog.view.header" javaScriptEscape="true"/>',
                summary: '<s:message code="careleaver.dialog.view.summary" javaScriptEscape="true"/>',
                careLeaverEligibility: '<s:message code="careleaver.dialog.view.careLeaverEligibility" javaScriptEscape="true"/>',
                reasonQualifying: '<s:message code="careleaver.dialog.view.reasonQualifying" javaScriptEscape="true"/>',
                contactDate: '<s:message code="careleaver.dialog.view.contactDate" javaScriptEscape="true"/>',
                inTouch: '<s:message code="careleaver.dialog.view.inTouch" javaScriptEscape="true"/>',
                mainActivity: '<s:message code="careleaver.dialog.view.mainActivity" javaScriptEscape="true"/>',
                accommodation: '<s:message code="careleaver.dialog.view.accommodation" javaScriptEscape="true"/>',
                accommodationSuitability: '<s:message code="careleaver.dialog.view.accommodationSuitability" javaScriptEscape="true"/>',
                multipleOccupancy: '<s:message code="careleaver.dialog.view.multipleOccupancy" javaScriptEscape="true"/>'
            },
            viewCareLeaverWelsh: {
                header: '<s:message code="careleaver.dialog.viewWelsh.header" javaScriptEscape="true"/>',
                summary: '<s:message code="careleaver.dialog.viewWelsh.summary" javaScriptEscape="true"/>',
                careLeaverEligibility: '<s:message code="careleaver.dialog.view.careLeaverEligibility" javaScriptEscape="true"/>',
                contactDate: '<s:message code="careleaver.dialog.view.contactDate" javaScriptEscape="true"/>',
                inTouch: '<s:message code="careleaver.dialog.view.inTouch" javaScriptEscape="true"/>',
                mainActivity: '<s:message code="careleaver.dialog.viewWelsh.mainActivity" javaScriptEscape="true"/>',
                accommodation: '<s:message code="careleaver.dialog.view.accommodation" javaScriptEscape="true"/>',
                accommodationSuitability: '<s:message code="careleaver.dialog.view.accommodationSuitability" javaScriptEscape="true"/>'
            },
            results: {
                inTouch: '<s:message code="careleaver.results.columns.inTouch" javaScriptEscape="true"/>',
                contactDate: '<s:message code="careleaver.results.columns.contactDate" javaScriptEscape="true"/>',
                eligibility: '<s:message code="careleaver.results.columns.eligibility" javaScriptEscape="true"/>',
                reasonQualifying: '<s:message code="careleaver.results.columns.reasonQualifying" javaScriptEscape="true"/>',
                mainActivity: '<s:message code="careleaver.results.columns.mainActivity" javaScriptEscape="true"/>',
                options: {
                    view: {
                        title: '<s:message code="careleaver.results.options.view.title" javaScriptEscape="true"/>',
                        label: '<s:message code="careleaver.results.options.view.label" javaScriptEscape="true"/>'
                    },
                    remove: {
                        title: '<s:message code="careleaver.results.options.remove.title" javaScriptEscape="true"/>',
                        label: '<s:message code="careleaver.results.options.remove.label" javaScriptEscape="true"/>'
                    }
                },
                actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>'

            }
        };

        var removeCareLeaverConfig = {
            views: {
                prepareRemove: {
                    header: {
                        text: '<s:message code="careleaver.remove.prepare.dialog.header" javaScriptEscape="true"/>',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="careleaver.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="approvals.foster.registration.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="careleaver.remove.prepare.dialog.removalConfirmation" javaScriptEscape="true" />'
                    },
                    config: {
                        prepareUrl: '<s:url value="/rest/careLeaver/{id}/deletionRequest" />'
                    }
                },
                remove: {
                    header: {
                        text: '<s:message code="careleaver.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: 'fa eclipse-childlookedafter'
                    },
                    narrative: {
                        summary: '<s:message code="careleaver.remove.dialog.summary" javaScriptEscape="true"/>',
                        description: subjectNarrativeDescription
                    },
                    successMessage: '<s:message code="careleaver.remove.success.message" javaScriptEscape="true" />',
                    labels: {
                        deleteConfirmation: '<s:message code="careleaver.remove.dialog.removalConfirmation" javaScriptEscape="true" />',
                        deleteConfirmationWithWarnings: '<s:message code="careleaver.remove.dialog.removalConfirmationWithWarnings" javaScriptEscape="true" />'
                    },
                    config: {
                        deleteUrl: '<s:url value="/rest/careLeaver?deletionRequestId={deletionRequestId}" />',
                        resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
                    }
                },
                removeInProgress: {
                    header: {
                        text: '<s:message code="careleaver.remove.dialog.header" javaScriptEscape="true"/>',
                        icon: 'fa fa-wrench'
                    },
                    labels: {
                        message: '<s:message code="careleaver.remove.dialog.inProgress" javaScriptEscape="true" />'
                    }
                }
            }
        };

        var removeConfig = {
            type: 'careLeaver',
            subject: subject,
            dialogConfig: removeCareLeaverConfig
        };

        new Y.app.careleaver.CareLeaverControllerView({
            container: '#careLeaverResultsContainer',
            labels: labels,
            subject: subject,
            permissions: permissions,
            showWelshView: showWelshView,
            dialogConfig: {
                views: {
                    add: {
                        header: {
                            text: '<s:message code="careleaver.dialog.add.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="careleaver.dialog.add.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        successMessage: '<s:message code="careleaver.dialog.add.successMessage" javaScriptEscape="true"/>',
                        labels: labels.addCareLeaver,
                        config: {
                            url: '<s:url value="/rest/careLeaver"/>'
                        }
                    },
                    view: {
                        header: {
                            text: '<s:message code="careleaver.dialog.view.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="careleaver.dialog.view.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.viewCareLeaver,
                        config: {
                            url: '<s:url value="/rest/careLeaver/{id}"/>'
                        }

                    },
                    addWelsh: {
                        header: {
                            text: '<s:message code="careleaver.dialog.addWelsh.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="careleaver.dialog.addWelsh.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        successMessage: '<s:message code="careleaver.dialog.addWelsh.successMessage" javaScriptEscape="true"/>',
                        labels: labels.addCareLeaverWelsh,
                        config: {
                            url: '<s:url value="/rest/careLeaver"/>'
                        }
                    },
                    viewWelsh: {
                        header: {
                            text: '<s:message code="careleaver.dialog.viewWelsh.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="careleaver.dialog.viewWelsh.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.viewCareLeaverWelsh,
                        config: {
                            url: '<s:url value="/rest/careLeaver/{id}"/>'
                        }

                    }
                }
            },
            resultsConfig: {
                labels: labels,
                subject: subject,
                permissions: permissions,
                showWelshView: showWelshView,
                url: '<s:url value="/rest/person/{subjectId}/careLeaver?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
                sortBy: [{ contactDate: 'desc' }]
            },
            removeConfig: removeConfig
        }).render();

    });
</script>