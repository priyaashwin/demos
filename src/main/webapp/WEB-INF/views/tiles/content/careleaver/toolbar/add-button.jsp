<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'CareLeaver','CareLeaver.ADD')" var="canAdd"/>
</c:if>

<tilesx:useAttribute name="selected" ignore="true" />
<tilesx:useAttribute name="first" ignore="true" />
<tilesx:useAttribute name="last" ignore="true" />
<c:if test="${selected eq true }">
	<c:set var="aStyle" value="pure-button-active" />
</c:if>
<c:if test="${first eq true }">
	<c:set var="fStyle" value="first" />
</c:if>
<c:if test="${last eq true }">
	<c:set var="lStyle" value="last" />
</c:if>


<%-- Only show the add button if the user has permission --%>
<c:if test="${canAdd eq true and canAddCareLeaverDetails eq true}">
	
	<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
	  <a href="#none" 
	    id="addCareLeaver"
	    data-button-action="addCareLeaver"
	    class="pure-button pure-button-loading pure-button-disabled usp-fx-all ${activeStyle}"
	    title='<s:message code="careleaver.add.button"/>'
	    aria-disabled="true"
	    aria-label='<s:message code="careleaver.add.button"/>' 
	    tabindex="${tabIndex }">
	      <i class="fa fa-plus-circle"></i>
	      <span><s:message code="button.add" /></span>
	  </a>
	</li>

</c:if>

