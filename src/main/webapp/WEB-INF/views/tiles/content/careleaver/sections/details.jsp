<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'CareLeaver','CareLeaver.GET')" var="canView" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'CareLeaver','CareLeaver.ADD')" var="canAdd" />
</c:if>

<div id="careLeaverDetails"></div>

<script>
	Y.use('careleaver-controller-view', function(Y) {
	    var permissions = {
	        canView: '${canView}' === 'true',
	        canAddCareLeaver: '${canAdd}' === 'true' && '${canAddCareLeaverDetails}' === 'true'
	    };
	    var subject = {
	          subjectId: '<c:out value="${person.id}"/>',
	          subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
	          subjectName: '${esc:escapeJavaScript(person.name)}',
	        };
	    
	    var subjectNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';
	
	    var labels = {
	        detailsView: {
	            title: '<s:message code="careleaver.title" javaScriptEscape="true"/>',
	            accordionTitle: '<s:message code="careleaver.details.header" javaScriptEscape="true"/>',
	            careLeaverEligibility: '<s:message code="careleaver.details.careLeaverEligibility" javaScriptEscape="true"/>',
		          reasonQualifying: '<s:message code="careleaver.details.reasonQualifying" javaScriptEscape="true"/>',
		          contactDate: '<s:message code="careleaver.details.contactDate" javaScriptEscape="true"/>',
		          inTouch: '<s:message code="careleaver.details.inTouch" javaScriptEscape="true"/>',
		          mainActivity: '<s:message code="careleaver.details.mainActivity" javaScriptEscape="true"/>',
		          accommodation: '<s:message code="careleaver.details.accommodation" javaScriptEscape="true"/>',
		          accommodationSuitability: '<s:message code="careleaver.details.accommodationSuitability" javaScriptEscape="true"/>',
		          multipleOccupancy: '<s:message code="careleaver.details.multipleOccupancy" javaScriptEscape="true"/>'
	        },
	        addCareLeaver: {
	          successMessage: '<s:message code="careleaver.dialog.add.successMessage" javaScriptEscape="true"/>',
	          careLeaverEligibility: '<s:message code="careleaver.dialog.add.careLeaverEligibility" javaScriptEscape="true"/>',
	          reasonQualifying: '<s:message code="careleaver.dialog.add.reasonQualifying" javaScriptEscape="true"/>',
	          contactDate: '<s:message code="careleaver.dialog.add.contactDate" javaScriptEscape="true"/>',
	          inTouch: '<s:message code="careleaver.dialog.add.inTouch" javaScriptEscape="true"/>',
	          mainActivity: '<s:message code="careleaver.dialog.add.mainActivity" javaScriptEscape="true"/>',
	          accommodation: '<s:message code="careleaver.dialog.add.accommodation" javaScriptEscape="true"/>',
	          accommodationSuitability: '<s:message code="careleaver.dialog.add.accommodationSuitability" javaScriptEscape="true"/>',
	          multipleOccupancy: '<s:message code="careleaver.dialog.add.multipleOccupancy" javaScriptEscape="true"/>'
	        }
	    };
	
	    new Y.app.careleaver.CareLeaverControllerView({
	        container: '#careLeaverDetails',
	        labels: labels,
	        subject: subject,
	        permissions: permissions,
	        url: '<s:url value="/rest/person/{subjectId}/careLeaver"/>',
	        dialogConfig: {
	          views:{
			        add: {
			          header: {
			            text: '<s:message code="careleaver.dialog.add.header" javaScriptEscape="true"/>',
			            icon: 'fa eclipse-childlookedafter'
			          },
			          narrative: {
			            summary: '<s:message code="careleaver.dialog.add.summary" javaScriptEscape="true" />',
			            description: subjectNarrativeDescription
			          },
			          successMessage: '<s:message code="careleaver.dialog.add.successMessage" javaScriptEscape="true"/>',
			          labels: labels.addCareLeaver,
			          config: {
			           url: '<s:url value="/rest/careLeaver"/>'
			         }
			       }
	          }
	       }
	     }).render();
	});

</script>
