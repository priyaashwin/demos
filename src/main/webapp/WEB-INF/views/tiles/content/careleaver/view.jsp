<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="hasPermission(null, 'CareLeaver','CareLeaver.View')" var="canViewCareLeaver"/>

<c:choose>
  <c:when test="${canViewCareLeaver eq true}">
      <%-- <t:insertTemplate template="/WEB-INF/views/tiles/content/careleaver/sections/details.jsp" />  --%>
      <t:insertTemplate template="/WEB-INF/views/tiles/content/careleaver/sections/results.jsp" />
  </c:when>
  <c:otherwise>
    <%-- Insert access denied tile --%>
    <t:insertDefinition name="access.denied" />
  </c:otherwise>
</c:choose>