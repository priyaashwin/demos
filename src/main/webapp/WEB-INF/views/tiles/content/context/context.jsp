<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'Person','Person.View')"
  var="canViewPerson" />
<sec:authorize access="hasPermission(null, 'Group','Group.View')"
  var="canViewGroup" />

<tilesx:useAttribute name="icon" />
<tilesx:useAttribute name="title" />

<c:if test="${(canViewPerson eq true) || (canViewGroup eq true)}">
  <h1 tabindex="0" id="appContext" class="iconic-title"></h1>
  <script>
      Y.use('subject-context', function(Y) {
        var groupId = '${esc:escapeJavaScript(group.groupIdentifier)}',
            personId = '${esc:escapeJavaScript(person.personIdentifier)}', 
            subjectModel;
            
        if (groupId) {
          //create the subject model
          subjectModel = new Y.app.GroupContextModel({
            id: '<c:out value="${group.id}"/>',
            groupId: groupId,
            name: '${esc:escapeJavaScript(group.name)}'
          });
        } else if (personId) {
          //create the subject model
          subjectModel = new Y.app.PersonContextModel({
            id: '<c:out value="${person.id}"/>',
            personId: personId,
            name: '${esc:escapeJavaScript(person.name)}'
          });
        }

        //create and render the subjectContext object
        var subjectContent = new Y.app.SubjectContext({
          container: Y.one('#appContext'),
          icon: '<c:out value="${icon}"/>',
          subTitle: '<s:message code="${title}" text="${title}"/>',
          model: subjectModel
        }).render();
      });
    </script>
</c:if>