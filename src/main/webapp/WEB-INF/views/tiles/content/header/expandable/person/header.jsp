<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="id"/>
<tilesx:useAttribute name="name"/>
<tilesx:useAttribute name="identifier"/>
<tilesx:useAttribute name="icon" ignore="true" />

<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canView"/>
<sec:authorize access="hasPermission(${id}, 'SecuredPersonContext.GET','PersonAlert.View')" var="canViewAlert"/>
<sec:authorize access="hasPermission(${id}, 'SecuredPersonContext.GET','Person.View')" var="canViewLock"/>
<sec:authorize access="hasPermission(${id}, 'SecuredPersonContext.GET','PersonAddress.Get')" var="canViewAddress"/>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(${id}, 'SecuredPersonContext.GET','PersonWarning.Add')" var="canAddWarning"/>
	<sec:authorize access="hasPermission(${id}, 'SecuredPersonContext.GET','PersonWarning.Update')" var="canUpdateWarning"/>
	<sec:authorize access="hasPermission(${id}, 'SecuredPersonContext.GET','PersonAlertAssignment.Update')" var="canUpdateAlert"/>
</c:if>

<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="primaryReferenceNumberContext" value="${userContextAttributes.getContext('primary-reference-number')}" />
<c:set var="isNamedPersonRelationshipAvailable" value="${userContextAttributes.getContext('named-person-relationship-available')}" />

<c:if test="${canView eq true}">
  <div id="fatHeader" class="header-person loading"></div>
  
  <script>  
  Y.use('categories-person-component-Gender',
        'secured-categories-person-component-Warnings',
        'categories-person-component-Alerts',
        'categories-contact-component-ContactType',
        'categories-address-component-UnknownLocation',
        'event-custom',
        function(Y){
    
    var header;
    
    ReactDOM.render(React.createElement(uspContextHeader.ContextHeader, {
      subjectId:'${id}',
      icon:'${icon}',
      name:'${name}',
      identifier: '${identifier}',
      primaryReferenceNumberContext: '${primaryReferenceNumberContext}',
      isNamedPersonRelationshipAvailable: '${isNamedPersonRelationshipAvailable}'==='true',
      labels:{
        dueDate: '<s:message code="person.dueDate" javaScriptEscape="true"/>',
        notCarried: '<s:message code="person.notCarried" javaScriptEscape="true"/>',
        born: '<s:message code="person.born" javaScriptEscape="true"/>',
        died: '<s:message code="person.died" javaScriptEscape="true"/>',
        gender: '<s:message code="person.gender" javaScriptEscape="true"/>',
        nhsNumber: '<s:message code="person.nhsNumber" javaScriptEscape="true"/>',
        chiNumber: '<s:message code="person.chiNumber" javaScriptEscape="true"/>',
        lock:{
          locked: '<s:message code="person.locked" javaScriptEscape="true"/>',
          expand:'<s:message code="showDetails" javaScriptEscape="true"/>',
          collapse:'<s:message code="hideDetails" javaScriptEscape="true"/>',
          accessAllowed:{
            title:'<s:message code="person.locked.accessAllowed" javaScriptEscape="true"/>'
          },
          accessDenied:{
            title:'<s:message code="person.locked.accessDenied" javaScriptEscape="true"/>'
          },
          loadFailed:'<s:message code="person.locked.loadFailed" javaScriptEscape="true"/>'
        },
        warnings:{
          related: '<s:message code="button.hoverrelatedwarnings" javaScriptEscape="true"/>',
          manage: '<s:message code="button.hovermanagewarnings" javaScriptEscape="true"/>',
          view: '<s:message code="button.hoverviewwarnings" javaScriptEscape="true"/>'
        },
        alerts:{
            manage: '<s:message code="person.alerts.manage.title" javaScriptEscape="true"/>',
            view: '<s:message code="person.alerts.view.title" javaScriptEscape="true"/>'
        },
        classifications:{
        },
        duplicateStatus: {
            masterDuplicateStatus: '<s:message code="duplicateRecords.masterDuplicateStatus" javaScriptEscape="true"/>',
            duplicateDuplicateStatus: '<s:message code="duplicateRecords.duplicateDuplicateStatus" javaScriptEscape="true"/>'
        },
        infoBar:{
          expandTitle:'<s:message code="infoPanel.expand.title" javaScriptEscape="true"/>',
          expand:'<s:message code="infoPanel.expand" javaScriptEscape="true"/>',
          collapse:'<s:message code="infoPanel.collapse" javaScriptEscape="true"/>',
          collapseTitle:'<s:message code="infoPanel.collapse.title" javaScriptEscape="true"/>',
          allocatedWorker:'<s:message code="person.allocatedWorker" javaScriptEscape="true"/>',
	        address:{
            address:'<s:message code="person.address" javaScriptEscape="true"/>',
            noAddress:'<s:message code="person.address.noRecordFound" javaScriptEscape="true"/>',
            isTemporary:'<s:message code="person.address.temporary" javaScriptEscape="true"/>',
            isPlacement:'<s:message code="person.address.placement" javaScriptEscape="true"/>',
            isDoNotDisclose:'<s:message code="person.address.doNotDisclose" javaScriptEscape="true"/>',
            current:'<s:message code="person.address.current" javaScriptEscape="true"/>',
            floor:'<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
            room:'<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>'
          },
          allocated:{
            allocatedWorker:'<s:message code="person.allocatedWorker" javaScriptEscape="true"/>',
            allocatedTeam:'<s:message code="person.allocatedTeam" javaScriptEscape="true"/>'
          },
          named: {
            namedPerson:'<s:message code="person.namedPerson" javaScriptEscape="true"/>'
          },
          contact:{
            contact:'<s:message code="person.contact" javaScriptEscape="true"/>',
            isTemporary:'<s:message code="person.contact.temporary" javaScriptEscape="true"/>'
          },
          classifications:{
            classifications:'<s:message code="person.classifications" javaScriptEscape="true"/>'
          }
        }
      },
      permissions:{
        canAddWarning:'${canAddWarning}'==='true',
        canUpdateWarning:'${canUpdateWarning}'==='true',
        canViewAlert:'${canViewAlert}'==='true',
        canUpdateAlert:'${canUpdateAlert}'==='true',
        canViewLock:'${canViewLock}'==='true',
        canViewAddress: '${canViewAddress}' === 'true',
        isSubjectAccessAllowed:'${securedSubjectContextVO.isSubjectAccessAllowed}' ==='true'
      },
      subjectDetailsEndpoint: {
        url: '<s:url value="/rest/person/{id}"/>',
      },
      subjectLockInfoEndpoint: {
        url: '<s:url value="/rest/person/{id}/securityAccessEntries"/>'
      },
      codedEntries:{
       gender:Y.uspCategory.person.gender.category,
       warnings:Y.secured.uspCategory.person.warnings.category,
       alerts:Y.uspCategory.person.alerts.category,
       contactType:Y.uspCategory.contact.contactType.category,
       unknownLocation:Y.uspCategory.address.unknownLocation.category
      },
      type:'person',
      subjectPictureEndpoint: {
        url: '<s:url value="/rest/{subjectType}/{subjectId}/picture/content"/>'
      },
      mapLinkUrl:Eclipse.config.googleMapURL
    }), document.getElementById('fatHeader'), function(){
      header=this;
    });
    

    Y.on(['person:personDetailChanged',
          'allocatedWorkers:dataChanged',
          'namedPersons:dataChanged',
          'allocatedTeam:dataChanged',
          'person:addressChanged',
          'classification:dataChanged',
          'relationship:dataChanged',
          'contentContext:update'], function(e){
      //force a refresh of the header because data has changed
      header.refresh();
    });
    
    document.addEventListener('addEditPersonWarning', function(e){
      var detail=e.detail||{};
      if(detail.personId){
        Y.fire('warnings:showDialog', {
          id: detail.personId,
          action: 'addEditPersonWarning'
        });
      }
    });
    
    document.addEventListener('viewPersonWarning', function(e){
      var detail=e.detail||{};
      if(detail.personId){
        Y.fire('warnings:showDialog', {
          id: detail.personId,
          action: 'viewPersonWarning'
        });
      }
    });
    
    document.addEventListener('viewPersonAlerts', function(){
        Y.fire('viewAlerts');
      });

    document.addEventListener('managePersonAlerts', function(){
        Y.fire('viewAlerts');
      });

    document.addEventListener('viewSummary', function(e){
      var detail=e.detail||{};
      if (window.location.toString().indexOf('/summary/person?') < 0) {
        if(detail.subjectId){
          window.location = Y.Lang.sub('<s:url value="/summary/person?id={id}"/>',{
            id:detail.subjectId
          });
        }
      }else{
        if(detail.subjectId){
          window.location = Y.Lang.sub('<s:url value="/person?id={id}"/>',{
            id:detail.subjectId
          });
        }
      }
    });
    
    document.addEventListener('headerUpdated', function(e){
      //force a resize of the header
      Y.fire('header:resize');
    });
    
    Y.one('#fatHeader').removeClass('loading');
  });
  </script>
</c:if>
