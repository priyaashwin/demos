<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div id="logout">
	<form action='<s:url value="/relationshipsrecording_logout"/>' method='post'>
        <c:if test="${_csrf != null}">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </c:if>
		<button type='submit' class='btn' title='<s:message code="toolbar.logout" />'><s:message code="toolbar.logout"/></button>
	</form>
</div>