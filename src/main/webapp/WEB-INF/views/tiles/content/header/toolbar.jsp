<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"  uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${FeedbackSettings.isEnable != null}">
  <s:eval expression="@FeedbackSettings.isEnable()" var="settingsEnabled" />
</c:if>

<div id="mob-search-launch">
	<a href="#"><i class="fa fa-search"></i></a>
</div>

<div id="page-tools">
  <a id='recent-records' style="display:none" href="#" title='<s:message code="recentrecords.name"/>'><i class="eclipse-historic"></i><span><s:message code="toolbar.recentrecords"/></span></a>
  <a id='notifications' style="display:none" href="#" title='<s:message code="notifications.name"/>'>
    <div class="fa-stack">
      <i class="notification-icon fa fa-exclamation-circle fa-stack-1x"></i>
      <div class="fa-stack-2x new-notifications-badge"><strong>0</strong></div>
      <span><s:message code="toolbar.notifications"/></span>
    </div>
  </a>
  <ul id="notifications-panel"><li><strong>Notifications</strong></li></ul>
</div>

<%-- Feedback button handling code below--%>
<c:if test="${settingsEnabled==true}">
	<a href="#" id="feedback-button" title='<s:message code="feedback.title"/>'>${FeedbackSettings.url}<i class="fa fa-quote-left"></i><span><s:message code="feedback.button"/></span></a>
	
	<script>
	Y.use('node','feedback-dialog-views','multi-panel-popup',function(Y){
		//pull relavent settings from Spring bean into JS
		var feedbackSettings={
				url: '<s:eval expression="@FeedbackSettings.getUrl()" javaScriptEscape="true" />',
				width: <s:eval expression="@FeedbackSettings.getWidth()"  javaScriptEscape="true"/>,
				height: <s:eval expression="@FeedbackSettings.getHeight()"  javaScriptEscape="true"/>,
				launch: '<s:eval expression="@FeedbackSettings.getLaunch()"  javaScriptEscape="true"/>'
		},myPopup;
		
		//If configured to do so, just launch a new window
		if(feedbackSettings.launch==='window'){
			Y.on('feedback:launch',function(e){
				window.open(e.url,'_blank','width='+(feedbackSettings.width||'')+',height='+(feedbackSettings.height||''));				
			});		
		}else{ //otherwise launch in a dialog
			myPopup=new Y.usp.MultiPanelPopUp({
				width:feedbackSettings.width,
				buttons:[{
		            	name: 'doneButton',
		                labelHTML: '<i class="fa fa-times"></i> <s:message code="feedback.dialog.close" />',
		                classNames: 'pure-button-secondary',
		                action: function(e) {
		                    e.preventDefault();
		                    window.onbeforeunload=null;
		                    this.hide();
		                },
		                section: Y.WidgetStdMod.FOOTER
				}],
				//Plugin Model Error handling
				plugins:[{fn:Y.Plugin.usp.MultiPanelModelErrorsPlugin}],			
				views: {
					 feedback: {
		               	type:Y.usp.FeedbackView,
		            	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-quote-left fa-inverse fa-stack-1x"></i></span> Feedback</h3>'
					}
		                                   
		        }
			});	
			
			myPopup.render();		
			Y.on('feedback:launch',function(e){
				myPopup.showView('feedback',{
					url:e.url
				});				
			});
		
		}
		
		var url=feedbackSettings.url;
		url=Y.Lang.sub(url,{
			href:encodeURIComponent(window.location.href),
			user:encodeURIComponent('<sec:authentication property="principal.userSummary.name" htmlEscape="false"/>'),
			environent:encodeURIComponent('${buildVersionEnvInfo.environment}'),
			version:encodeURIComponent('${relationshipsrecordingAppVersion.buildVersion}'),
			ua:encodeURIComponent(navigator.userAgent)
		});
	
		
		Y.one('#feedback-button').on('click',function(){
			Y.fire('feedback:launch',{
				url:url
			});
		});
		
	});
	</script>
</c:if>

	
  
