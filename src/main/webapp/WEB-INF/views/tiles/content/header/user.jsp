<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>


<sec:authentication property="principal.userSummary.workEmailAddress" var="userEmailAddress"/>
<sec:authentication property="principal.userSummary.workMobileContactNumber" var="userMobileNumber"/>
<sec:authentication property="principal.userSummary.id" var="userId"/>

<c:set var="passwordPolicyTooltipArgs"  value="${passwordPolicy.minChars},,${passwordPolicy.minUppercaseChars},,${passwordPolicy.minDigits},,${passwordPolicy.minNonAlphanumericChars}" />

<div id="logged-in-user" class="pure-menu pure-menu-open pure-menu-horizontal" style="display:inline">
    <ul class="pure-menu-children">
        <li class="pure-menu-can-have-children pure-menu-has-children">
            <a href='<s:url value="/index"/>' id="profile">
            <i class="fa fa-user"></i><span class="username pure-menu-label"><sec:authentication property="principal.userSummary.name"/></span><i class="fa fa-caret-down"></i></a>
            <ul class="pure-menu pure-menu-children" role="menu">
                <li class="pure-menu-heading headingProfile">Profile: <span class="headingProfileDescription"><sec:authentication property="principal.securityProfile.profileDescription"/></span>
				<c:if test="${not empty userEmailAddress}">
						<span style='font-size:80%;display:block;margin-top:-.5em;'>e: ${userEmailAddress}</span>
				</c:if>
				<c:if test="${not empty userMobileNumber}">
						<span style='font-size:80%;display:block;margin-top:-.5em;'>m: ${userMobileNumber}</span>
				</c:if>
				</li>
                <li class="pure-menu-item"><a class="changePassword" href="#" role="menuitem">Change password</a></li>
                <li class="pure-menu-item"><a class="changeSecondaryPassword" href="#" role="menuitem">Change secondary password</a></li>
                <li class="pure-menu-item"><a class="changeNotificationSettings" href="#" role="menuitem">Change notification settings</a></li>
            </ul>
        </li>
    </ul>
</div>

<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />

<script>
Y.use('yui-base',
	'event-delegate',
	'event-custom-base',
	'user-selfCredentialChange-dialog-views',
	'split-button-plugin',
function(Y){
	
	//plug in button menu for graph style
	Y.one('#logged-in-user').plug(Y.Plugin.usp.SplitButtonPlugin);
	
	var updateSelfPasswordChangeLabels={
		password:'<s:message code="user.updateSelfPasswordChange.password"/>',
		confirmPassword:'<s:message code="user.updateSelfPasswordChange.confirmPassword"/>',
		passwordPopupMessageTitle: '<s:message code="user.updateSelfPasswordChange.passwordPopup.title"/>',
		passwordPopupMessageContent: '<s:message code="user.updateSelfPasswordChange.passwordPopup.content" argumentSeparator=",," arguments="${passwordPolicyTooltipArgs}"/>',
		confirmPasswordPopupMessageTitle: '<s:message code="user.updateSelfPasswordChange.confirmPasswordPopup.title"/>',
		confirmPasswordPopupMessageContent: '<s:message code="user.updateSelfPasswordChange.confirmPasswordPopup.content"/>',
		updateSelfPasswordChangeSummary:'<s:message code="user.updateSelfPasswordChange.summary" javaScriptEscape="true" />',
		updateSelfPasswordChangeSuccessMessage:'<s:message code="user.updateSelfPasswordChange.success.message" javaScriptEscape="true"/>',
		currentPassword: '<s:message code="user.updateSelfPasswordChange.currentPassword"/>'
	},
	updateSelfSecondaryPasswordChangeLabels={
		secondaryPassword:'<s:message code="user.updateSelfSecondaryPasswordChange.secondaryPassword"/>',
		confirmSecondaryPassword:'<s:message code="user.updateSelfSecondaryPasswordChange.confirmSecondaryPassword"/>',
		secondaryPasswordPopupMessageTitle: '<s:message code="user.updateSelfSecondaryPasswordChange.secondaryPasswordPopup.title"/>',
		secondaryPasswordPopupMessageContent: '<s:message code="user.updateSelfSecondaryPasswordChange.secondaryPasswordPopup.content" argumentSeparator=",," arguments="${passwordPolicyTooltipArgs}"/>',
		confirmSecondaryPasswordPopupMessageTitle: '<s:message code="user.updateSelfSecondaryPasswordChange.confirmSecondaryPasswordPopup.title"/>',
		confirmSecondaryPasswordPopupMessageContent: '<s:message code="user.updateSelfSecondaryPasswordChange.confirmSecondaryPasswordPopup.content"/>',
		updateSelfSecondaryPasswordChangeSummary:'<s:message code="user.updateSelfSecondaryPasswordChange.summary" javaScriptEscape="true" />',
		updateSelfSecondaryPasswordChangeSuccessMessage:'<s:message code="user.updateSelfSecondaryPasswordChange.success.message" javaScriptEscape="true"/>',
		currentPrimaryPassword: '<s:message code="user.updateSelfSecondaryPasswordChange.currentPrimaryPassword"/>'
	},
	updateNotificationSettingsLabels={
	    summary: '<s:message code="user.dialog.updateNotificationSettings.summary" javaScriptEscape="true"/>',
	    notificationTypesTitle: '<s:message code="user.dialog.updateNotificationSettings.notificationTypesTitle" javaScriptEscape="true"/>',
	    notificationOccuranceTitle: '<s:message code="user.dialog.updateNotificationSettings.notificationOccurenceTitle" javaScriptEscape="true"/>',
	    notifiableEventsTitle: '<s:message code="user.dialog.updateNotificationSettings.notifiableEventsTitle" javaScriptEscape="true"/>',
	    notifyMeTitle: '<s:message code="user.dialog.updateNotificationSettings.notifyMeTitle" javaScriptEscape="true"/>',
	    SMS: '<s:message code="user.dialog.updateNotificationSettings.SMS" javaScriptEscape="true"/>',
	    email: '<s:message code="user.dialog.updateNotificationSettings.email" javaScriptEscape="true"/>',
	    eclipse: '<s:message code="user.dialog.updateNotificationSettings.Eclipse" javaScriptEscape="true"/>',
	    inMyCases: '<s:message code="user.dialog.updateNotificationSettings.inMyCases" javaScriptEscape="true"/>',
	    inMyAndTeamCases: '<s:message code="user.dialog.updateNotificationSettings.inMyAndTeamCases" javaScriptEscape="true"/>',
	    successMessage: '<s:message code="user.dialog.updateNotificationSettings.successMessage" javaScriptEscape="true"/>',
	    browserNotificationsBlockedMessage: '<s:message code="user.dialog.updateNotificationSettings.browserNotificationsBlockedMessage" javaScriptEscape="true"/>'
	}
	userDialog=new Y.app.UserDialog({
		headerContent:'Loading...',
		//View config mixed into existing config
		views: {
			updateSelfPasswordChange:{
				headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
					class="fa fa-circle fa-stack-1x"></i><i \
					class="fa fa-user fa-inverse fa-stack-1x"></i></span> \
					<s:message code="user.dialog.updateSelfPasswordChange.header"/></h3>'
			},
			updateSelfSecondaryPasswordChange:{
				headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
					class="fa fa-circle fa-stack-1x"></i><i \
					class="fa fa-user fa-inverse fa-stack-1x"></i></span> \
					<s:message code="user.dialog.updateSelfSecondaryPasswordChange.header"/></h3>'
			},
			updateNotificationSettings:{
        headerTemplate:'<h3 class="iconic-title" tabindex="0"><span class="fa-stack"><i \
          class="fa fa-circle fa-stack-1x"></i><i \
          class="fa fa-user fa-inverse fa-stack-1x"></i></span> \
          <s:message code="user.dialog.updateNotificationSettings.header"/></h3>'
      }
		}
	}).render();
	
	//attach delegate to toolbar
	Y.delegate('click', function(e){
		var t=e.currentTarget;
		//stop default action
		e.preventDefault();
		if(t.hasClass('changePassword')){
			Y.fire('user:showDialog', {
				action: 'updateSelfPasswordChange'
			});
		}else if(t.hasClass('changeSecondaryPassword')){
			Y.fire('user:showDialog', {
				action: 'updateSelfSecondaryPasswordChange',
			});
		} else if(t.hasClass('changeNotificationSettings')) {
		  Y.fire('user:showDialog', {
		    action: 'updateNotificationSettings'
		  })
		}
	},'#logged-in-user','ul li.pure-menu-item a');
	
	var showUpdateSelfPasswordChangeDialog = function(e) {
		userDialog.showView('updateSelfPasswordChange', {
			labels:updateSelfPasswordChangeLabels,
			model:new Y.app.UpdateSelfPasswordChangeForm({
				url:'<s:url value="/rest/securityUser/{securityUserId}/?action=updateCredentials&selfService"/>',
				securityUserId:${userId}
			}),
			targetName:e.name,
			targetIdentifier:e.userIdentifier
		},
		function(view){
			//Get the save button by name out of the footer and enable it.
			userDialog.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
		});
	},
	showUpdateSelfSecondaryPasswordChangeDialog = function(e) {
		userDialog.showView('updateSelfSecondaryPasswordChange', {
			labels:updateSelfSecondaryPasswordChangeLabels,
			model:new Y.app.UpdateSelfSecondaryPasswordChangeForm({
				url:'<s:url value="/rest/securityUser/{securityUserId}/?action=updateCredentials&selfService"/>',
				securityUserId:${userId}
			}),
			targetName:e.name,
			targetIdentifier:e.userIdentifier
		},
		function(view){
			//Get the save button by name out of the footer and enable it.
			userDialog.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
		});
	},
	showUpdateNotificationSettingsDialog = function(e) {
	  userDialog.showView('updateNotificationSettings', {
      labels: updateNotificationSettingsLabels,
      updateURL: '<s:url value="/rest/userNotificationSubscription/{id}?selfService"/>',
      createURL: '<s:url value="/rest/userNotificationSubscription?selfService"/>',
      realmsURL: '<s:url value="/rest/notificationRealm"/>',
      viewURL: '<s:url value="/rest/userNotificationSubscription?selfService"/>'
    },
    function(view){
      //Get the save button by name out of the footer and enable it.
      userDialog.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
    });
	};
	
	//respond to custom events
	Y.on('user:showDialog', function(e){
		switch(e.action){
			case 'updateSelfPasswordChange': showUpdateSelfPasswordChangeDialog(e);
				break;
			case 'updateSelfSecondaryPasswordChange': showUpdateSelfSecondaryPasswordChangeDialog(e);
				break;
			case 'updateNotificationSettings': showUpdateNotificationSettingsDialog(e);
			  break;
			default:
		}
	}, userDialog);
	
	//listen for save event
	userDialog.on('*:saved', function(e){
		// display the success message
		Y.fire('infoMessage:message', {
			message: e.successMessage,
			type: 'success'
		});
		
		this.results.reload();
	}, this);	
});
</script>
