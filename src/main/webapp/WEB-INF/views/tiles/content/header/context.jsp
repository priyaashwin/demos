<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<h1><a href='<s:url value="/index"/>' title="Home"><span>Eclipse</span></a></h1>
<div id="headerMessage"></div>

<c:choose>
    <%-- if the subject context for the page is person --%>
    <c:when test="${!empty person}">
        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Person.View')" var="recordUnlocked"/>
    </c:when>
    <%-- if the subject context for the page is group --%>
    <c:when test="${!empty group}">
        <sec:authorize access="hasPermission(${group.id}, 'SecuredGroupContext.GET','Group.View')" var="recordUnlocked"/>
    </c:when>
    <%-- some page may don't have a subject like home, so set a default as true --%>
   <c:otherwise>
        <sec:authorize access="true" var="recordUnlocked"/>
    </c:otherwise>
</c:choose>


<c:choose>
    <%-- Subject is locked, show 'locked by...' message --%>
    <c:when test="${securedSubjectContextVO.isSubjectRestrictedViaLock()}">
        <c:choose>
            <c:when test="${not empty securedSubjectContextVO.getSubjectLockOwner()}">
                <c:set var="accessDeniedMessage">
                    <s:message code="access.recordLockedByKnown" javaScriptEscape="true">
                        <s:argument value="${securedSubjectContextVO.getSubjectLockOwner()}"/>
                    </s:message>
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="accessDeniedMessage">
                    <s:message code="access.recordLockedByUnknown" javaScriptEscape="true"/>
                </c:set>
            </c:otherwise>
        </c:choose>
    </c:when>
    <%-- If unlocked show the default relational security message --%>
    <c:otherwise>
        <c:set var="accessDeniedMessage">
            <s:message code="access.recordRestricted" javaScriptEscape="true"/>
        </c:set>
    </c:otherwise>
</c:choose>


<script>
    var recordUnlocked=${recordUnlocked};

    if (!recordUnlocked) {
        //Ensure there is an event handler for our info-message before firing
        var pollCount = 0,
            iMCheck = Y.later('500', this, function(msg) {
                if (Y.getEvent('infoMessage:message', true)) {
                    //cancel the poll
                    iMCheck.cancel();

                    Y.fire('infoMessage:message', {
                        message: msg,
                        type: 'error'
                    });
                }

                pollCount++;

                if (pollCount > 100) {
                    iMCheck.cancel();
                    Y.log('Gave up looking for infoMessage:message handler ', 'debug');
                }
            }, '${accessDeniedMessage}', true);
    }
 </script>