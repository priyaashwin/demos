<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div id="learn-how-container" style="display: none;"></div>

<script>
  var retries = 0;
  var timerId = setInterval (function() {
    if (window.inlineManualPlayerData) {
      document.getElementById('learn-how-container').style.display = "block";
      clearInterval(timerId);
    } else if (retries > 100) {
      clearTimeout(timerId);
    }
    retries++;
  }, 50);
</script>
