<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ page import="com.olmgroup.usp.apps.relationshipsrecording.service.context.AdvancedSearchContext" %>

<sec:authorize access="hasPermission(null, 'Person','Person.GET')" var="canViewPerson"/>
<sec:authorize access="hasPermission(null, 'Group','Group.GET')" var="canViewGroup"/>
<sec:authorize access="hasPermission(null, 'Organisation','Organisation.GET')" var="canViewOrganisation" />
<sec:authorize access="hasPermission(null, 'RelationshipsRecording.SEARCH','RelationshipsRecording.SEARCH')" var="canSearch" />
<s:eval expression="@appSettings.getAutoCompleteMaxResults()" var="maxResults" scope="page" />

<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="advancedSearchContext" value="${userContextAttributes.getContext('advanced-search')}" />

<c:if test="${canSearch eq true}">
	<div id="search">
		<form action="#none" id="searchForm" class="pure-form  header-search">			
			<div class="search-wrapper yui3-skin-sam">
			<div class="input-group input-prepend-append">
				<label for="searchSelector">Choose to search</label>
				<span>
				  		<select id="searchSelector" >
					  		<c:if test="${canViewPerson eq true}">
					  			<option selected value='PERSON'>Person</option>
					  		</c:if>
					  		<c:if test="${canViewGroup eq true}">
					  			<option value='GROUP'>Group</option>
					  		</c:if>
					  		<c:if test="${canViewOrganisation eq true}">
					  			<option value='ORGANISATION'>Org.</option>
					  		</c:if>
				  		</select>
  				</span>	    
  				<label for="subjectSearch">Subject search</label>
				<input id="subjectSearch" class="pure-input-1" name="subjectSearch" maxlength="100" type="text" placeholder='<s:message code="relationship.subjectSearchPlaceholder"/>'/>
				<span>
					<a href="#" class="pure-button pure-button-active pure-button-cal" aria-label="Submit search" id="searchButton"><i class="fa fa-search"></i></a>
				</span>
				<span>
						<a href="#"  title="Advanced search" id="advancedSearchLink">Advanced</a>
				</span>
			</div>
				<a class="close" href="#" style="display:none;" id="mob-search-closer">Close</a>
			</div>		
		</form>
		<div id="applicationSearch"></div>
		<div id="advancedSearch"></div>


  <script>    
    Y.use('history',
            'usp-session-storage',
            'person-quick-search',
            'person-app-search',
            'group-quick-search',
            'group-app-search',
            'organisation-quick-search',
            'organisation-app-search',
            'app-recent-records-dialog',
            'usp-person-Person',
            function(Y) {

      var isScottishRegion='${advancedSearchContext}'==='<%=AdvancedSearchContext.SCOTTISH%>';
      
      var personAdvancedSearchContext = {
            chiNumberVisible:isScottishRegion,
            nhsNumberVisible: isScottishRegion||!isScottishRegion,
        };
      
                var maxResults = '${maxResults}',
                    quickSearch = null,
                    applicationSearch = null,
                    currentSubjectType = null;
                var history = new Y.HistoryHash();

                var emptyPersonCriteria={
                    nameOrId: '',
                    houseStreetTown: '',
                    postCode: '',
                    minAge: '',
                    maxAge: '',
                    gender: '',
                    refNo: '',
                    nhsNo: '',
                    dayOfBirth: '',
                    monthOfBirth: '',
                    yearOfBirth: '',
                    chiNumber: ''
                };

                var emptyOrganisationCriteria={
                    nameOrId: '',
                    type: '',
                    subType: '',
                    externalReferenceNumber: '',
                    postcode: '',
                    houseStreetTown: ''
                };
                
                var _initiateSearch = function(e) {
                    e.preventDefault();

                    //access the input value
                    var input = Y.one("#subjectSearch"),
                        value = input.get('value');

                    if (value && applicationSearch) {

                        setTimeout(function() {
                            if (currentSubjectType === 'ORGANISATION') {
                                applicationSearch.executeQuery(Y.merge(emptyOrganisationCriteria, {
                                  nameOrId: value,
                                }));
                            } else {
                                applicationSearch.executeQuery(Y.merge(emptyPersonCriteria, {
                                  nameOrId: value,
                                }));
                            }
                        }, 300);
                    }
                };

                var _advancedSearch = function(e) {
                    e.preventDefault();

                    //access the input value
                    var input = Y.one("#subjectSearch"),
                        value = input.get('value');

                        if (currentSubjectType === 'ORGANISATION') {
                            applicationSearch.executeQuery(Y.merge(emptyOrganisationCriteria, {
                                nameOrId: value,
                            }));
                        } else {
                            applicationSearch.executeQuery(Y.merge(emptyPersonCriteria, {
                                nameOrId: value,
                            }));
                        }
                };

                Y.one("#searchButton").on("click", _initiateSearch);
                Y.one("#searchForm").on("submit", _initiateSearch);
                Y.one("#advancedSearchLink").on("click", _advancedSearch);


                //our current person model - we want to load only once
                var currentPersonModel = new Y.usp.person.Person({
                    root: '<s:url value="/rest/person/current "/>'
                });

                //load the model
                currentPersonModel.load();


                var recentRecordsDialog = new Y.app.RecentRecordsDialog({}).render();

                //Ensure the recent records dialog remains aligned on the screen
                recentRecordsDialog.after('*:load', function(e) {
                    this.align();
                }, recentRecordsDialog);

                //reveal the link
                Y.on('contentready', function() {
                    Y.one('#recent-records').show();
                }, '#recent-records');

                recentEventHandler = Y.delegate('click', function(e) {
                    e.preventDefault();
                    this.showView('recentPeopleView', {
                        personUrl: '<s:url value="/summary/person?id={personId}&reset=true" />',
                        model: currentPersonModel,
                        searchConfig: {
                            labels: {
                                name: '<s:message code="home.recentRecords.nameId" javaScriptEscape="true"/>',
                                dob: '<s:message code="home.recentRecords.dobAge" javaScriptEscape="true"/>',
                                gender: '<s:message code="home.recentRecords.gender" javaScriptEscape="true"/>',
                                showPerson: '<s:message code="home.person.show" javaScriptEscape="true"/>'
                            },
                            noDataMessage: '<s:message code="home.recentRecords.no.results" javaScriptEscape="true"/>',
                            url: '<s:url value="/rest/person/{userId}/accessPersonTarget?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
                        },
                        otherData: {
                            labels: {
                                recentRecords: '<s:message code="recentrecords.name" javaScriptEscape="true"/>'
                            }
                        }
                    });
                }, '#recent-records', 'a', recentRecordsDialog);
                
                var searchHeader={
                  text: '<s:message code="app.search.results" javaScriptEscape="true"/>',
                  icon: 'fa fa-search',
                  showAll:  '<s:message code="app.search.showAll" javaScriptEscape="true"/>'
                };
                
                function optionSelected(selection) {
                    if (quickSearch) {
                        quickSearch.destroy();
                    }
                    if (applicationSearch) {
                        applicationSearch.destroy();
                    }
                    currentSubjectType = selection;

                    switch (selection) {
                        case 'PERSON':
                            Y.one('#advancedSearchLink').show();
                            quickSearch = new Y.app.PersonQuickSearchView({
                                viewConfig:{
                                    header:searchHeader
                                },
                                inputNode: '#subjectSearch',
                                maxResults: maxResults,
                                url: '<s:url value="/rest/person?appendWildcard=true&nameOrId=" />',
                                personUrl: '<s:url value="/summary/person?id={personId}&reset=true" />'
                            });
                            quickSearch.render();

                            var input = Y.one("#subjectSearch"),
                                value = input.get('value');

                            applicationSearch = new Y.app.PersonAppSearchView({
                                container: '#applicationSearch',
                                viewConfig:{
                                    width: 1116,
                                    type: 'app.search.PersonCriteriaView',
                                    header:Y.merge(searchHeader,{
                                        text:'<s:message code="app.search.advanced" javaScriptEscape="true"/>'
                                    })
                                },
                                filterConfig:{
                                  labels:{
                                    main:'<s:message code="app.search.main" javaScriptEscape="true"/>',
                                    secondary:'<s:message code="app.search.secondary" javaScriptEscape="true"/>',
                                    gender: '<s:message code="person.gender" javaScriptEscape="true"/>',
                                    nameOrId: '<s:message code="person.nameId" javaScriptEscape="true"/>',
                                    born: '<s:message code="person.born" javaScriptEscape="true"/>',
                                    agedBetween: '<s:message code="person.aged_between" javaScriptEscape="true"/>',
                                    rangeBetween: '<s:message code="range.between" javaScriptEscape="true"/>',
                                    rangeAnd: '<s:message code="range.and" javaScriptEscape="true"/>',
                                    postCode: '<s:message code="person.address.add.postcode" javaScriptEscape="true"/>',
                                    houseStreetTown: '<s:message code="person.address.add.houseStreetTown" javaScriptEscape="true"/>',
                                    nhsNumber: '<s:message code="person.nhsNumber" javaScriptEscape="true"/>',
                                    chiNumber: '<s:message code="person.chiNumber" javaScriptEscape="true"/>',
                                    refNo: '<s:message code="person.refNo" javaScriptEscape="true"/>',
                                    address: '<s:message code="person.contact.detail" javaScriptEscape="true"/>',
                                    fuzzyDateLabels:{
                                      date:'<s:message code="person.born" javaScriptEscape="true"/>'
                                    }
                                  },
                                  searchContext:personAdvancedSearchContext
                                },
                                filterContextConfig:{
                                  labels:{
                                    nameOrId: '<s:message code="person.nameId" javaScriptEscape="true"/>',
                                    address:  '<s:message code="person.contact.detail" javaScriptEscape="true"/>',
                                    aged:  '<s:message code="person.aged_between" javaScriptEscape="true"/>',
                                    gender: '<s:message code="person.gender" javaScriptEscape="true"/>',
                                    refNo: '<s:message code="person.refNo" javaScriptEscape="true"/>',
                                    nhsNo: '<s:message code="person.nhsNumber" javaScriptEscape="true"/>',
                                    chiNumber: '<s:message code="person.chiNumber" javaScriptEscape="true"/>',
                                    dob: '<s:message code="newPersonVO.dateOfBirth" javaScriptEscape="true"/>',
                                    resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
                                    resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
                                  }
                                },
                                searchConfig: {
                                    isScottishRegion: isScottishRegion,
                                    labels: {
                                        searchResults: '<s:message code="app.search.results" javaScriptEscape="true"/>',
                                        personName: '<s:message code="person.name" javaScriptEscape="true"/>',
                                        personIdentifier: '<s:message code="person.id" javaScriptEscape="true"/>',
                                        noDataMessage: '<s:message code="person.no.results" javaScriptEscape="true"/>',
                                        address: '<s:message code="person.contact.detail" javaScriptEscape="true"/>'
                                    },
                                    url: '<s:url value="/rest/person?s={sortBy}&pageNumber={page}&pageSize={pageSize}&appendWildcard=true&nameOrId={nameOrId}&houseStreetTown={houseStreetTown}&postCode={postCode}&minAge={minAge}&maxAge={maxAge}&gender={gender}&refNo={refNo}&nhsNo={nhsNo}&dayOfBirth={dayOfBirth}&monthOfBirth={monthOfBirth}&yearOfBirth={yearOfBirth}&chiNumber={chiNumber}"/>'
                                },
                                otherData: {
                                    nameOrId: value
                                },
                                personUrl: '<s:url value="/summary/person?id={personId}&reset=true" />'
                            }).render();
                            break; // break for case person

                        case 'GROUP':
                            Y.one('#advancedSearchLink').hide();
                            quickSearch = new Y.app.GroupQuickSearchView({
                                viewConfig:{
                                    header:searchHeader
                                },
                                inputNode: '#subjectSearch',
                                maxResults: maxResults,
                                url: '<s:url value="/rest/group?useSoundex=false&escapeSpecialCharacters=true&appendWildcard=true&nameOrId=" />',
                                groupUrl: '<s:url value="/groups/group?id={groupId}&reset=true" />'
                            }).render();

                            applicationSearch = new Y.app.GroupAppSearchView({
                                container: '#applicationSearch',
                                viewConfig:{
                                  header:searchHeader,  
                                },
                                searchConfig: {
                                    labels: {
                                        searchResults: '<s:message code="app.search.results"/>',
                                        groupIdentifier: '<s:message code="person.group.general.results.id"/>',
                                        name: '<s:message code="person.group.general.results.name"/>',
                                        groupType: '<s:message code="person.group.general.results.groupType"/>',
                                        startDate: '<s:message code="person.group.general.results.startDate"/>',
                                        closeDate: '<s:message code="person.group.general.results.closeDate"/>',
                                        noDataMessage: '<s:message code="person.group.general.no.results"/>'
                                    },
                                    url: '<s:url value="/rest/group?s={sortBy}&useSoundex=false&escapeSpecialCharacters=true&appendWildcard=true&pageNumber={page}&pageSize={pageSize}&nameOrId={nameOrId}&temporalStatusFilter=ALL"/>'
                                },
                                groupUrl: '<s:url value="/groups/group?id={groupId}&reset=true" />'
                            }).render();

                            break; // break for case group

                        case 'ORGANISATION':
                            Y.one('#advancedSearchLink').show();
                            quickSearch = new Y.app.OrganisationQuickSearchView({
                                viewConfig:{
                                    header:searchHeader
                                },
                                inputNode: '#subjectSearch',
                                maxResults: maxResults,
                                url: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrId="/>',
                                organisationUrl: '<s:url value="/organisation?id={organisationId}&reset=true" />'
                            }).render();

                            var input = Y.one("#subjectSearch"),
                            value = input.get('value');

                            applicationSearch = new Y.app.OrganisationAppSearchView({
                                container: '#applicationSearch',
                                viewConfig:{
                                    type:'app.search.OrganisationCriteriaView',
                                    header:Y.merge(searchHeader,{
                                        text:'<s:message code="app.search.advanced" javaScriptEscape="true"/>',
                                    }),
                                },
                                filterConfig:{
                                    labels:{
                                        main:'<s:message code="app.search.main" javaScriptEscape="true"/>',
                                        secondary:'<s:message code="app.search.secondary" javaScriptEscape="true"/>',
                                        nameOrId: '<s:message code="organisation.nameId" javaScriptEscape="true"/>',
                                        postCode: '<s:message code="organisation.address.postcode" javaScriptEscape="true"/>',
                                        houseStreetTown: '<s:message code="organisation.address.houseStreetTown" javaScriptEscape="true"/>',
                                        extRefNo: '<s:message code="organisation.extRefNo" javaScriptEscape="true"/>',
                                        address: '<s:message code="organisation.address" javaScriptEscape="true"/>',
                                        type: '<s:message code="organisation.type" javaScriptEscape="true"/>',
                                        subType: '<s:message code="organisation.subType" javaScriptEscape="true"/>',
                                    }
                                },
                                filterContextConfig:{
                                    labels:{
                                        nameOrId: '<s:message code="organisation.nameId" javaScriptEscape="true"/>',
                                        address:  '<s:message code="organisation.address" javaScriptEscape="true"/>',
                                        extRefNo: '<s:message code="organisation.extRefNo" javaScriptEscape="true"/>',
                                        type: '<s:message code="organisation.type" javaScriptEscape="true"/>',
                                        subType: '<s:message code="organisation.subType" javaScriptEscape="true"/>',
                                        resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
                                        resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
                                    }
                                },
                                searchConfig: {
                                    labels: {
                                        searchResults: '<s:message code="app.search.results" javaScriptEscape="true"/>',
                                        organisationName: '<s:message code="organisation.name" javaScriptEscape="true"/>',
                                        organisationIdentifier: '<s:message code="organisation.label.id" javaScriptEscape="true"/>',
                                        organisationType: '<s:message code="organisation.type"/>',
                                        organisationSubtype: '<s:message code="organisation.subtype"/>',
                                        noDataMessage: '<s:message code="organisation.no.results" javaScriptEscape="true"/>',
                                        address: '<s:message code="organisation.address" javaScriptEscape="true"/>'
                                    },
                                    url :'<s:url value="/rest/organisation?s={sortBy}&pageNumber={page}&pageSize={pageSize}&appendWildcard=true&nameOrId={nameOrId}&type={type}&subType={subType}&externalReferenceNumber={externalReferenceNumber}&postcode={postcode}&houseStreetTown={houseStreetTown}&escapeSpecialCharacters=true&useSoundex=true"/>'
                                },
                                otherData: {
                                    nameOrId: value
                                },
                                organisationUrl: '<s:url value="/organisation?id={organisationId}&reset=true" />'
                            }).render();

                            break;

                    } // End of switch	    	 
                }

                /**
                 * @function optionSelector
                 * @description Select option based on session value
                 */
                function optionSelector() {
                    //sessionStorage.setItem
                    switch (Y.uspSessionStorage.getStoreValue('optionSource')) {
                        case 'GROUP':
                            optionSelected('GROUP');
                            Y.one('#advancedSearchLink').hide();
                            break;
                        case 'ORGANISATION':
                            optionSelected('ORGANISATION');
                            Y.one('#advancedSearchLink').show();
                            break;
                        default:
                            optionSelected('PERSON');
		                        Y.one('#advancedSearchLink').show();
	                        break;
                    }
                }
                Y.one('#searchSelector').on('change', function(e) {
                    optionSelected(e.currentTarget.get('value'));
                });
                if (history.get('g_s')) {
                    optionSelector();
                } else {
                    optionSelected('PERSON');
                }
            });
  </script>

  <script>
    Y.use('node', 'event-resize', 'event-custom-base', function(Y) {
      Y.on('domready', function () {
        var $menuItems = Y.all('#page-tools, #logged-in-user, #logout');
        var $btnHideSearch = Y.one('#mob-search-closer');
        var $btnShowSearch = Y.one('#mob-search-launch');
        var $searchWrapper = Y.one('.search-wrapper');
        var $search = Y.one('#search');
        var $header = Y.one('header');

        var showMobileSearch = function(e) {
            e.preventDefault();
            $menuItems.hide();
            $search.addClass('mob-search').setStyle('display', 'block');
            $searchWrapper.addClass('mob-search');
            $btnHideSearch.show();
            $btnShowSearch.hide();
            $header.addClass('search-mode');
        };

        var hideMobileSearch = function(e) {
            e.preventDefault();
            $menuItems.show();
            $search.removeClass('mob-search').setStyle('display', 'none');
            $searchWrapper.removeClass('mob-search');
            $btnHideSearch.hide();
            $btnShowSearch.show();
            $header.removeClass('search-mode');
        };

        var resetResponsiveSearch = function(e) {
            //Test if device is mobile or tablet - if it is a mobile device then we dont want to invoke reset on resize of window, as resize event  interferes with search box input on mobile devices.
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) == false) {
                transformSearch();
            }
        };

        var resetOnOrientationchange = function(e) {
            transformSearch();
        };

        function transformSearch() {
            var width = 0;

            if (document.body && document.body.offsetWidth) {
                width = document.body.offsetWidth;
            }

            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                width = document.documentElement.offsetWidth;
            }

            if (window.innerWidth && window.innerHeight) {
                width = window.innerWidth;
            }

            $searchWrapper.removeClass('mob-search');
            $btnHideSearch.hide();
            $menuItems.show();
            $search.show();
            $header.removeClass('search-mode');

            // we only want this showing for mobile.
            if (width < 768) {
                $btnShowSearch.show();
            }

        };

        Y.delegate('click', showMobileSearch, 'body', '#mob-search-launch');
        Y.delegate('click', hideMobileSearch, 'body', '#mob-search-closer');
        Y.on('windowresize', resetResponsiveSearch);
        Y.on('orientationchange', resetOnOrientationchange);
      });
    });
  </script>
  </div>
</c:if>