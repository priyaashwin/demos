<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<!-- Session Timeout monitor -->
<sec:authentication property="principal.username" var="username" />
<sec:authentication property="principal.securityProfile.profileDescription" var="profile"/>
<sec:authentication property="principal.securityProfile.profileId" var="profileId"/>
<%-- assignedProfiles.size() not currently working - returns 0 --%>
<sec:authentication property="principal.assignedProfiles" var="assignedProfiles"/> 


<script>
// save session information, for use in tracking session timeout and display of login dialog
Y.use('authentication-controller', function(Y) {
    var sessionInfo = {
        username: '${esc:escapeJavaScript(username)}',
        profileId: '${profileId}',
        profileDesc: '${esc:escapeJavaScript(profile)}',
        profileCount: '${assignedProfiles.size()}'
    };
    var headerIcon = 'fa fa-key';
    var dialogConfig = {
        labels:{
            lockedException: '<s:message code="authentication.error.user" javaScriptEscape="true"/>'
        },
        views: {
            loginForm: {
                header: {
                    text: 'Login',
                    icon: headerIcon
                },
                messageTimeout: '<s:message code="session.authentication.timeout" javaScriptEscape="true"/>',
                messageLogout: '<s:message code="session.authentication.logout" javaScriptEscape="true"/>',
                successMessage: '<s:message code="session.authentication.success" javaScriptEscape="true"/>',
                authenticateURL: '<s:url value="/apisecurity/authenticate"/>'
            },
            sessionSwitchMessage: {
                header: {
                  text: '<s:message code="session.switched.title" javaScriptEscape="true"/>',
                    icon: headerIcon
                },
                narrative:{
                  summary:'<s:message code="session.switched.narrative" javaScriptEscape="true"/>'
                }
            },
            incompleteAuthMessage: {
                header: {
                    text: '<s:message code="session.incompleteLogin.title" javaScriptEscape="true"/>',
                    icon: headerIcon
                },
                narrative:{
                  summary:'<s:message code="session.incompleteLogin.narrative" javaScriptEscape="true"/>'
                },
                config: {
                    loginURL: '<s:url value="/" />'
                }
            },
            secondaryPassword: {
              header: {
                text: '<s:message code="session.secondaryPassword.title" javaScriptEscape="true"/>',
                icon: headerIcon
               },
               narrative:{
                 summary:'<s:message code="session.secondaryPassword.narrative" javaScriptEscape="true"/>'
               },
               secondaryPasswordURL: '<s:url value="/apisecurity/authenticateSecondaryPassword" />',
               successMessage: '<s:message code="session.authentication.success" javaScriptEscape="true"/>'
            },
            accountExpired: {
              header: {
                text: '<s:message code="session.accountExpired.title" javaScriptEscape="true"/>',
                icon: headerIcon
               },
               narrative:{
                 summary:'<s:message code="session.accountExpired.narrative" javaScriptEscape="true"/>'
               },
               labels:{
                 accountExpired:'<s:message code="session.accountExpired" javaScriptEscape="true"/>',
                 closeWindow:'<s:message code="session.closeWindow" javaScriptEscape="true"/>'
               }
            }
        }
    };
    var config = {
        sessionDuration: Number('${pageContext.session.maxInactiveInterval}'),
        keepAliveURL: '<s:url value="/rest/session/extend" />',
        sessionInfo: sessionInfo,
        dialogConfig: dialogConfig
    };
    var authenticationHandler = new Y.app.login.AuthenticationControllerView(config).render();
});
</script>