<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="icon" ignore="true" />
<c:choose>
    <%-- if the subject context for the page is person --%>
    <c:when test='${subjectType eq "person"}'>
        <t:insertTemplate template="/WEB-INF/views/tiles/content/header/expandable/person/header.jsp" flush="false">
          <t:putAttribute name="icon" value="${icon}"/>
          <t:putAttribute name="id" value="${person.id}"/>
          <t:putAttribute name="name" value="${esc:escapeJavaScript(person.name)}"/>
          <t:putAttribute name="identifier" value="${esc:escapeJavaScript(person.personIdentifier)}"/>
        </t:insertTemplate>
        
        <%-- Insert Person Warning Dialog Template --%>
        <t:insertTemplate template="/WEB-INF/views/tiles/content/person/panel/personWarningDialog.jsp" />
        
        <%-- Insert Person Alert Dialog Template --%>
        <t:insertTemplate template="/WEB-INF/views/tiles/content/person/panel/personAlertDialog.jsp" />
    </c:when>
    <c:when test='${subjectType eq "group"}'>
        <t:insertTemplate template="/WEB-INF/views/tiles/content/header/expandable/group/header.jsp" flush="false">
          <t:putAttribute name="icon" value="${icon}"/>
          <t:putAttribute name="id" value="${group.id}"/>
          <t:putAttribute name="name" value="${esc:escapeJavaScript(group.name)}"/>
          <t:putAttribute name="identifier" value="${esc:escapeJavaScript(group.groupIdentifier)}"/>
        </t:insertTemplate>
    </c:when>
    <c:when test='${subjectType eq "organisation"}'>
        <t:insertTemplate template="/WEB-INF/views/tiles/content/header/expandable/organisation/header.jsp" flush="false">
            <t:putAttribute name="icon" value="${icon}"/>
            <t:putAttribute name="id" value="${organisation.id}"/>
            <t:putAttribute name="name" value="${esc:escapeJavaScript(organisation.name)}"/>
            <t:putAttribute name="identifier" value="${esc:escapeJavaScript(organisation.organisationIdentifier)}"/>
        </t:insertTemplate>
    </c:when>
</c:choose>