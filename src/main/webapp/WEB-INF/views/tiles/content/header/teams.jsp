<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authentication property="principal.securityProfile.profileDescription" var="selectedProfileDescription"/>
<sec:authentication property="principal.securityProfile.profileId" var="selectedProfileId"/>

<div id="team-select-wrapper"></div>

<script>
Y.use('yui-base', 'change-team-view', function(Y) {
  var config = {
    container: '#team-select-wrapper',
    selectedProfileDescription: '${esc:escapeJavaScript(selectedProfileDescription)}',
    selectedProfileId: '${selectedProfileId}',
    getTeamsURL: '<s:url value="/rest/profile/assigned"/>',
    switchTeamURL: '<s:url value="/rest/profile/switch?profileId={profileId}"/>',
    rootURL: '<s:url value="/" />'
  };

  var changeTeamView = new Y.app.home.ChangeTeamControllerView(config).render();
});
</script>
