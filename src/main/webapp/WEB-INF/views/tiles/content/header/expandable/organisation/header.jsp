<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="id"/>
<tilesx:useAttribute name="name"/>
<tilesx:useAttribute name="identifier"/>
<tilesx:useAttribute name="icon" ignore="true" />

<sec:authorize access="hasPermission(null, 'Organisation','Organisation.View')" var="canView"/>

<c:if test="${canView eq true}">
  <div id="fatHeader" class="header-organisation loading"></div>

  <script>
    Y.use( 'event-custom',
          function(Y){

      var header;

      ReactDOM.render(React.createElement(uspContextHeader.ContextHeader, {
        subjectId:'${id}',
        icon:'${icon}',
        name:'${name}',
        identifier: '${identifier}',
        labels:{},
        codedEntries:{},
        permissions:{},
        subjectDetailsEndpoint: {
          url: '<s:url value="/rest/organisation/{id}"/>',
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/vnd.olmgroup-usp.relationshipsrecording.organisationdetailswithprofessionalwarninglist+json'
          }
        },
        subjectLockInfoEndpoint:{},
        type:'organisation',
        subjectPictureEndpoint: {
          url: '<s:url value="/rest/{subjectType}/{subjectId}/picture/content"/>'
        },
        mapLinkUrl:Eclipse.config.googleMapURL,
        isTitleClickable:false
      }), document.getElementById('fatHeader'), function(){
        header=this;
      });


      Y.on(['contentContext:update','organisation:dataChanged'], function(e){
        //force a refresh of the header because data has changed
        header.refresh();
      });

      document.addEventListener('headerUpdated', function(e){
        //force a resize of the header
        Y.fire('header:resize');
      });

      document.addEventListener('viewSummary', function(e){
        var detail=e.detail||{};
        if(detail.subjectId){
          window.location = Y.Lang.sub('<s:url value="/organisation?id={id}"/>',{
            id:detail.subjectId
          });
        }
      });

      document.addEventListener('viewSubject', function(e){
        var detail=e.detail||{};

        if(detail.subjectId && detail.subjectType){
          window.location = Y.Lang.sub('<s:url value="/summary/{subjectType}?id={id}"/>',{
            id:detail.subjectId,
            subjectType: detail.subjectType
          });
        }
      });
      
      Y.one('#fatHeader').removeClass('loading');
    });
  </script>
</c:if>
