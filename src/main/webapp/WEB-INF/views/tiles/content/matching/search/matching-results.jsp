<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'SecurityUser','SecurityUser.ADD')" var="canAdd" />
	
<div id="matchingResults"></div>

<script>
Y.use('matching-controller-view', function(Y) {
	
	var permissions = {
	    canView: true,
	    canUpdate: '${canUpdate}' === 'true',
	    canAdd: '${canAdd}' === 'true'
	};

	var searchConfig = {
		  permissions: permissions,
	    title: '<s:message code="matching.find.results" javaScriptEscape="true"/>',
	    noDataMessage: '<s:message code="matching.find.no.results" javaScriptEscape="true"/>',
	    url: '<s:url value="/rest/fosterCarerMatches?pageNumber={page}&pageSize={pageSize}&approvedOnly=true"/>',
	    summaryUrl: '<s:url value="/summary/person?id={id}"/>',
	    labels: {
	        carer: '<s:message code="matching.find.results.carer" javaScriptEscape="true"/>',
	        types: '<s:message code="matching.find.results.types" javaScriptEscape="true"/>',
	        maxAvailablePlaces: '<s:message code="matching.find.results.maxAvailablePlaces" javaScriptEscape="true" />',
	        maxAvailableRooms: '<s:message code="matching.find.results.maxAvailableRooms" javaScriptEscape="true" />'
	    }
	};

	var filterContextConfig = {
			container: '#resultFilterContext',
	    labels: {
	        filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
	        resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>',
	        resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
	        type: '<s:message code="matching.find.filter.type.active" javaScriptEscape="true"/>',
	        resetType: '<s:message code="matching.find.filter.type.active.reset" javaScriptEscape="true"/>',
	        age: '<s:message code="matching.find.filter.age" javaScriptEscape="true"/>',
	        disability: '<s:message code="matching.find.filter.disability" javaScriptEscape="true"/>',
	        gender: '<s:message code="matching.find.filter.gender" javaScriptEscape="true"/>',
	        type: '<s:message code="matching.find.filter.type" javaScriptEscape="true"/>',
	    }
	};
	
	var filterConfig = {
	    labels: {
	        filterBy: '<s:message code="filter.by" javaScriptEscape="true"/>',
	        reset: '<s:message code="filter.reset" javaScriptEscape="true"/>',
	        resetTitle: '<s:message code="filter.reset" javaScriptEscape="true"/>',
	        age: '<s:message code="matching.find.filter.age" javaScriptEscape="true"/>',
	        disability: '<s:message code="matching.find.filter.disability" javaScriptEscape="true"/>',
	        gender: '<s:message code="matching.find.filter.gender" javaScriptEscape="true"/>',
	        type: '<s:message code="matching.find.filter.type" javaScriptEscape="true"/>',
	    }
	};
	
	new Y.app.matching.MatchingControllerView({
	    container: '#matchingResults',
	    searchConfig: searchConfig,
	    filterConfig: filterConfig,
	    filterContextConfig: filterContextConfig,
	    permissions: permissions
	}).render();
	
});
</script>