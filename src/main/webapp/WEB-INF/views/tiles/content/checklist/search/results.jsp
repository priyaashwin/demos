<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
 
<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstanceResults.ViewArchived')" var="canViewArchived"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','Checklists.ViewAutomatedAdd')" var="canViewAutomatedAdd"/>
<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstanceResults.ViewCompleted')" var="canViewCompleted"/>

<sec:authentication property="principal.securityProfile.securityTeam.teamId" var="teamId"/>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Add')" var="canAdd"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Archive')" var="canDelete"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Remove')" var="canRemove"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Update')" var="canUpdate"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Pause')" var="canPause"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Resume')" var="canResume"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.ResetEndDate')" var="canResetEndDate"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Start')" var="canStartEarly"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Prioritise')" var="canPrioritise"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Escalate')" var="canEscalate"/>
	<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.Deescalate')" var="canDeescalate"/>
</c:if>

<s:url value="/checklist/view?id={id}" var="viewChecklistURl">
    <c:if test="${not empty group}">
        <s:param name="isGroupContext" value="true"/>
    </c:if>
</s:url>

<div id="checklistResults"></div>

<script>
Y.use('checklist-controller',
      'yui-base', 
      'categories-person-component-Title', 
      'categories-person-component-PersonType', 
      'categories-person-component-Ethnicity', 
      'categories-person-component-Gender', 
      'categories-person-component-ProfessionalTitle',
      function(Y){
<c:choose>
  <c:when test="${not empty group}">
  var subject = {
    subjectType: 'group',
    subjectName: '${esc:escapeJavaScript(group.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(group.groupIdentifier)}',
    subjectId: '<c:out value="${group.id}"/>'
  };
  </c:when>
  <c:otherwise>
  var subject = {
    subjectType: 'person',
    subjectName: '${esc:escapeJavaScript(person.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
    subjectId: '<c:out value="${person.id}"/>',
  };
  </c:otherwise>
</c:choose>

  var permissions = {
    canAdd: '${canAdd}' === 'true',
    canView: '${canView}' === 'true',
    canViewErrors: '${canView}' === 'true',
    canDelete: '${canDelete}' === 'true',
    canRemove: '${canRemove}' === 'true',
    canViewArchived: '${canViewArchived}' == 'true',
    canViewCompleted: '${canViewCompleted}' === 'true',
    canPause: '${canPause}' === 'true',
    canResume: '${canResume}' === 'true',
    canStartEarly: '${canStartEarly}' === 'true',
    canPrioritise: '${canPrioritise}' === 'true',
    canEscalate: '${canEscalate}' === 'true',
    canDeescalate: '${canDeescalate}' === 'true',
    //must be able to update to re-assign
    canReassign: '${canUpdate}' === 'true',
    canResetEndDate: '${canResetEndDate}' === 'true',
    canViewAutomatedAdd: '${canViewAutomatedAdd}' === 'true'
  };

  var resultsSearchConfig = {
    sortBy: [{
      dateDue: 'desc'
    }],
    labels: {
      name: '<s:message code="checklist.results.name"/>',
      dateDue: '<s:message code="checklist.results.dateDue"/>',
      priority: '<s:message code="checklist.results.priority"/>',
      percentComplete: '<s:message code="checklist.results.percentComplete"/>',
      ownerName: '<s:message code="checklist.results.ownerName"/>',
      actions: '<s:message code="checklist.results.actions"/>',
      description: '<s:message code="checklist.results.description"/>',
      dateStarted: '<s:message code="checklist.results.dateStarted"/>',
      dateStartScheduled: '<s:message code="checklist.results.dateStartScheduled"/>',
      dateCompleted: '<s:message code="checklist.results.dateCompleted"/>',
      subjectName: '<s:message code="checklist.results.subjectName"/>',
      viewChecklist: '<s:message code="checklist.results.viewChecklist"/>',
      prioritiseChecklist: '<s:message code="checklist.results.prioritiseChecklist"/>',
      deleteChecklist: '<s:message code="checklist.results.deleteChecklist"/>',
      removeChecklist: '<s:message code="checklist.results.removeChecklist"/>',
      viewChecklistTitle: '<s:message code="checklist.results.viewChecklistTitle"/>',
      prioritiseChecklistTitle: '<s:message code="checklist.results.prioritiseChecklistTitle"/>',
      deleteChecklistTitle: '<s:message code="checklist.results.deleteChecklistTitle"/>',
      removeChecklistTitle: '<s:message code="checklist.results.removeChecklistTitle"/>',
      reassignChecklist: '<s:message code="checklist.results.reassignChecklist"/>',
      reassignChecklistTitle: '<s:message code="checklist.results.reassignChecklistTitle"/>',
      reassignTask: '<s:message code="checklist.results.reassignTask"/>',
      reassignTaskTitle: '<s:message code="checklist.results.reassignTaskTitle"/>',      
      pauseChecklist: '<s:message code="checklist.results.pauseChecklist"/>',
      pauseChecklistTitle: '<s:message code="checklist.results.pauseChecklistTitle"/>',
      resumeChecklist: '<s:message code="checklist.results.resumeChecklist"/>',
      resumeChecklistTitle: '<s:message code="checklist.results.resumeChecklistTitle"/>',
      resetChecklistEndDate: '<s:message code="checklist.results.resetChecklistEndDate"/>',
      resetChecklistEndDateTitle: '<s:message code="checklist.results.resetChecklistEndDateTitle"/>',
      startChecklist: '<s:message code="checklist.results.startChecklist"/>',
      startChecklistTitle: '<s:message code="checklist.results.startChecklistTitle"/>',
      viewError: '<s:message code="checklist.results.viewError"/>',
      viewErrorTitle: '<s:message code="checklist.results.viewErrorTitle"/>'
    },
    noDataMessage: '<s:message code="checklist.find.no.results"/>',
    permissions: permissions,
    resultsCountNode: Y.one('#checklistResultsCount'),
    url: '<s:url value="/rest/checklistInstance?subjectId={subjectId}&subjectType={subjectType}&s={sortBy}&pageNumber={page}&pageSize={pageSize}" />',
    viewURL: '${viewChecklistURl}',
    currentPersonId: '${currentPersonId}'
  };
  var resultsFilterContextConfig = {
    labels: {
      filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
      subject: '<s:message code="checklist.find.filter.subject.active" javaScriptEscape="true"/>',
      status: '<s:message code="checklist.find.filter.status.active" javaScriptEscape="true"/>',
      priority: '<s:message code="checklist.find.filter.priority.active" javaScriptEscape="true"/>',
      dateDue: '<s:message code="checklist.find.filter.dateDue.active" javaScriptEscape="true"/>',
      checklistDefinitionId: '<s:message code="checklist.find.filter.checklistDefinitionId.active" javaScriptEscape="true"/>'
    },
    container: '#checklistFilterContext'
  };
  
  var resultsQuickFiltersConfig = {
	      labels: {
	        title:'<s:message code="checklist.filter.quick.title" javaScriptEscape="true"/>',
	        filters: {
	          status:'<s:message code="checklist.filter.quick.checkbox.status" javaScriptEscape="true"/>'
	        }
	      },
	      container: '#checklistQuickFilters',
	      localStorageNaming: 'checklistQuickFilters',
	      externallyManagedAttrs: ['status']
	  };
  

  var resultsFilterConfig = {
    labels: {
      filterBy: '<s:message code="checklist.find.filter.title" javaScriptEscape="true"/>',
      checklistDefinitionId: '<s:message code="checklist.find.filter.checklistDefinitionId.active" javaScriptEscape="true"/>',
      dateFilter: '<s:message code="checklist.find.filter.dateFilter" javaScriptEscape="true"/>',
      subjectFilter: '<s:message code="checklist.find.filter.subjectFilter" javaScriptEscape="true"/>',
      typeStatusPriorityFilter: '<s:message code="checklist.find.filter.typeStatusPriorityFilter" javaScriptEscape="true"/>',
      dueDateFrom: '<s:message code="checklist.find.filter.dueDateFrom" javaScriptEscape="true"/>',
      dueDateTo: '<s:message code="checklist.find.filter.dueDateTo" javaScriptEscape="true"/>',
      dateRangeLinks: '<s:message code="checklist.find.filter.dateRangeLinks" javaScriptEscape="true"/>',
      dateRangeLastWeek: '<s:message code="checklist.find.filter.dateRangeLastWeek" javaScriptEscape="true"/>',
      dateRangeThisWeek: '<s:message code="checklist.find.filter.dateRangeThisWeek" javaScriptEscape="true"/>',
      dateRangeNextWeek: '<s:message code="checklist.find.filter.dateRangeNextWeek" javaScriptEscape="true"/>',
      dateRangeLastMonth: '<s:message code="checklist.find.filter.dateRangeLastMonth" javaScriptEscape="true"/>',
      dateRangeThisMonth: '<s:message code="checklist.find.filter.dateRangeThisMonth" javaScriptEscape="true"/>',
      dateRangeNextMonth: '<s:message code="checklist.find.filter.dateRangeNextMonth" javaScriptEscape="true"/>',
      status: '<s:message code="checklist.find.filter.status" javaScriptEscape="true"/>',
      notStarted: '<s:message code="checklist.find.filter.notStarted" javaScriptEscape="true"/>',
      onTime: '<s:message code="checklist.find.filter.onTime" javaScriptEscape="true"/>',
      lastReminder: '<s:message code="checklist.find.filter.lastReminder" javaScriptEscape="true"/>',
      paused: '<s:message code="checklist.find.filter.paused" javaScriptEscape="true"/>',
      overdue: '<s:message code="checklist.find.filter.overdue" javaScriptEscape="true"/>',
      complete: '<s:message code="checklist.find.filter.complete" javaScriptEscape="true"/>',
      archived: '<s:message code="checklist.find.filter.archived" javaScriptEscape="true"/>',
      inError: '<s:message code="checklist.find.filter.error" javaScriptEscape="true"/>',
      priority: '<s:message code="checklist.find.filter.priority" javaScriptEscape="true"/>',
      none: '<s:message code="checklist.find.filter.none" javaScriptEscape="true"/>',
      low: '<s:message code="checklist.find.filter.low" javaScriptEscape="true"/>',
      medium: '<s:message code="checklist.find.filter.medium" javaScriptEscape="true"/>',
      high: '<s:message code="checklist.find.filter.high" javaScriptEscape="true"/>',
      memberId: '<s:message code="checklist.find.filter.memberId" javaScriptEscape="true"/>',
      subjectId: '<s:message code="checklist.find.filter.subjectId" javaScriptEscape="true"/>',
    },
    checklistDefinitionListURL: '<s:url value="/rest/checklistDefinition?subjectId={subjectId}&subjectType={subjectType}&s={sortBy}" />',
    groupMemberListURL: '<s:url value="/rest/group/{subjectId}/personMembership?temporalStatusFilter=ACTIVE&s={sortBy}&pageNumber=1&pageSize=-1" />'
  };

  var checklistHeaderIcon = 'fa fa-check-square-o',
    narrativeDescription = '{{this.subject.subjectName||this.subject.name}} ({{this.subject.subjectIdentifier||this.subject.identifier}})';

  var statusDialogConfig = {
    checklistStatusURL: '<s:url value="/rest/checklistInstance/{id}/status?action={action}"/>',
    checklistRemoveURL: '<s:url value="/rest/checklistInstance/{id}"/>',
    checklistInstanceURL: '<s:url value="/rest/checklistInstance/{id}"/>',
    checklistActionURL: '<s:url value="/rest/checklistInstance/{id}?action={action}"/>',
    views: {
      deleteChecklist: {
        header: {
          text: '<s:message code="checklist.delete.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.delete.summary"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message javaScriptEscape="true" code="checklist.delete.success"/>',
        config: {
          message: '<s:message code="checklist.delete.prompt"/>'
        }
      },
      prioritiseChecklist: {
    	permissions: {
    		canEscalate: permissions.canEscalate,
    		canDeescalate: permissions.canDeescalate
    	},
        dialogConfig: {
          header: {
            icon: 'fa-user',
            text: '<s:message code="checklist.prioritise.header" javaScriptEscape="true"/>'
          },
          successMessage: '<s:message code="checklist.prioritise.success" javaScriptEscape="true"/>',
          narrative: {
            summary: '<s:message code="checklist.prioritise.summary" javaScriptEscape="true"/>',
            description: '{{subject.name}} ({{subject.identifier}})'
          }
        },
        labels: {
          priority: '<s:message code="checklist.prioritise.priority" javaScriptEscape="true"/>'
        },
        urls: {
          getChecklistUrl: '<s:url value="/rest/checklistInstance/{id}" />',
          setInitialChecklistPriorityUrl: '<s:url value="/rest/checklistInstance/{id}/status?action=prioritised" />',
          escalateChecklistInstancePriorityUrl: '<s:url value="/rest/checklistInstance/{id}/status?action=escalated" />',
          descalateChecklistInstancePriorityUrl: '<s:url value="/rest/checklistInstance/{id}/status?action=deescalated" />'
        }
      },
      reassignTask: {
    	  permissions: {
    	  },
        dialogConfig: {
          header: {
            icon: checklistHeaderIcon,
            text: '<s:message code="checklist.reassignTask.header"/>'
          },          
          narrative: {
              summary: '<s:message code="checklist.reassignTask.summary" javaScriptEscape="true"/>',
              description: '{{subject.name}} ({{subject.identifier}})'
          },
          successMessage: '<s:message javaScriptEscape="true" code="checklist.reassignTask.success"/>'
        },
        searchConfig: {
          labels: {
            name: '<s:message code="checklist.reassignTask.task"/>',
            overdue: '<s:message code="checklist.reassignTask.overdue"/>',
            startDate: '<s:message code="checklist.reassignTask.dateStarted"/>',
            dueDate: '<s:message code="checklist.reassignTask.dateDue"/>',
            owner: '<s:message code="checklist.reassignTask.ownerName"/>'            	
          },          
          currentPersonId: '${currentPersonId}',
          currentPersonTeamId: '${currentPersonTeamId}',
          codedEntries: {
            personTypeCategory: Y.uspCategory.person.personType.category,
            titleCategory: Y.uspCategory.person.title.category,
            ethnicityCategory: Y.uspCategory.person.ethnicity.category,
            genderCategory: Y.uspCategory.person.gender.category,
            professionalTitleCategory: Y.uspCategory.person.professionalTitle.category
          },
        },
        labels: {        	  
      	  buttons: {
              reassignSubmitTitle: '<s:message code="checklist.reassignTask.buttons.reassignSubmitTitle" javaScriptEscape="true"/>',
          	  reassignSubmitText: '<s:message code="checklist.reassignTask.buttons.reassignSubmitText" javaScriptEscape="true"/>',
          	  reassignCancel: '<s:message code="checklist.reassignTask.buttons.reassignCancel" javaScriptEscape="true"/>'
      	  },      	  
      	  radioGroups: {
          	  typeOfOwner: '<s:message code="checklist.reassignTask.radioGroups.typeOfOwner" javaScriptEscape="true"/>',
          	  individualOwner: '<s:message code="checklist.reassignTask.radioGroups.individualOwner" javaScriptEscape="true"/>',
          	  teamOwner: '<s:message code="checklist.reassignTask.radioGroups.teamOwner" javaScriptEscape="true"/>',
          	  
          	  individualOwnerHeader: '<s:message code="checklist.reassignTask.radioGroups.individualOwnerHeader" javaScriptEscape="true"/>',
          	  individualOwnerSelf: '<s:message code="checklist.reassignTask.radioGroups.individualOwnerSelf" javaScriptEscape="true"/>',
          	  individualOwnerOther: '<s:message code="checklist.reassignTask.radioGroups.individualOwnerOther" javaScriptEscape="true"/>',
              
          	  teamOwnerHeader: '<s:message code="checklist.reassignTask.radioGroups.teamOwnerHeader" javaScriptEscape="true"/>',
              teamOwnerSelf: '<s:message code="checklist.reassignTask.radioGroups.teamOwnerSelf" javaScriptEscape="true"/>',
              teamOwnerOther: '<s:message code="checklist.reassignTask.radioGroups.teamOwnerOther" javaScriptEscape="true"/>'
      	  },         
          personSelectPlaceholder: '<s:message code="checklist.reassignTask.placeholder.personSelect" javaScriptEscape="true"/>',
          teamSelectPlaceholder: '<s:message code="checklist.reassignTask.placeholder.teamSelect" javaScriptEscape="true"/>',
          otherPersonTeamSelect: '<s:message code="checklist.reassignTask.otherPersonTeamSelect" javaScriptEscape="true"/>'
        },        
        urls: {
          getTasksUrl: '<s:url value="/rest/checklistInstance/{id}" />',
          getTeamsUrl: '<s:url value="/rest/person/{id}/teamRelationship/professional?temporalStatusFilter=ACTIVE&sort=roleBOrganisation.name&pageSize=-1" />',
          getPersonAutoCompleteUrl:  '<s:url value="/rest/person?personType=PROFESSIONAL" />',
          getTeamAutoCompleteUrl: '<s:url value="/rest/organisation/active?escapeSpecialCharacters=true&byOrganisationType=true&organisationTypes=TEAM"/>',
          updateTaskOwnershipUrL: '<s:url value="/rest/checklistInstance/{id}" />',
        }
      },
      removeChecklist: {
        header: {
          text: '<s:message code="checklist.remove.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.remove.summary"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message javaScriptEscape="true" code="checklist.remove.success"/>',
        config: {
          message: '<s:message code="checklist.remove.prompt"/>'
        }
      },
      reassignSuccessWithWarnings: {
        header: {
          text: '<s:message code="checklist.reassign.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.reassign.successWithWarningsSummary"/>'
        },
        config: {
          message: '<s:message javaScriptEscape="true" code="checklist.reassign.successWithWarnings"/>'
        }
      },
      reassignChecklist: {
        header: {
          text: '<s:message code="checklist.reassign.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.reassign.summary"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message javaScriptEscape="true" code="checklist.reassign.success"/>',
        config: {
          reassignURL: '<s:url value="/rest/checklistInstance/{id}?action=reassign"/>',
          reassignBatchURL: '<s:url value="/rest/checklistInstance/{id}?action=reassign&reassignBatch=true"/>',
          personTeamsURL: '<s:url value="/rest/person/{id}/teamRelationship/professional?temporalStatusFilter=ACTIVE&sort=roleBOrganisation.name&pageSize=-1"/>',
          personAutocompleteURL: '<s:url value="/rest/person?nameOrUsername={query}&personType=professional&s={sortBy}&pageNumber=1&pageSize=50"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
          organisationAutocompleteURL: '<s:url value="/rest/organisation/active?escapeSpecialCharacters=true&appendWildcard=true&byOrganisationType=true&organisationTypes=TEAM&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
          owningTeamId: '${teamId}'
        },
        labels: {
          ownerId: '<s:message javaScriptEscape="true" code="newChecklistInstanceVO.ownerId"/>',
          ownerSubjectType: '<s:message javaScriptEscape="true" code="newChecklistInstanceVO.ownerSubjectType"/>',
          isGroupWorklist: '<s:message javaScriptEscape="true" code="checklist.reassign.isGroupWorklist"/>',
          reassignBatchInfo: '<s:message javaScriptEscape="true" code="checklist.reassign.reassignBatchInfo"/>',
          reassignBatch: '<s:message javaScriptEscape="true" code="checklist.reassign.reassignBatch"/>',
          ownerType: {
            organisation: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation"/>',
            person: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person"/>',
            select: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.select"/>'
          },
          organisation: {
            ownerId: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.ownerId"/>',
            assignToOwnTeam: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.own"/>',
            assignToOtherTeam: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.other"/>',
            placeholder: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.placeholder"/>',
            noResults: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.organisation.noResults"/>'
          },
          person: {
            ownerId: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.ownerId"/>',
            assignToSelf: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.self"/>',
            assignToOther: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.other"/>',
            placeholder: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.placeholder"/>',
            noResults: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.noResults"/>',
            teamSelect: '<s:message javaScriptEscape="true" code="checklist.reassign.ownerType.person.teamSelect"/>'
          }
        }
      },
      pauseChecklist: {
        header: {
          text: '<s:message code="checklist.pause.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.pause.summary"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message javaScriptEscape="true" code="checklist.pause.success"/>',
        labels: {
          pauseReason: '<s:message javaScriptEscape="true" code="updateChecklistInstancePauseVO.pauseReason"/>'
        }
      },
      resumeChecklist: {
        header: {
          text: '<s:message code="checklist.resume.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.resume.summary"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message javaScriptEscape="true" code="checklist.resume.success"/>',
        config: {
          message: '<s:message code="checklist.resume.prompt"/>'
        }
      },
      resetEndDate: {
        header: {
          text: '<s:message code="checklist.resetEndDate.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.resetEndDate.summary"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message javaScriptEscape="true" code="checklist.resetEndDate.success"/>',
        labels: {
          endDate: '<s:message javaScriptEscape="true" code="updateChecklistInstanceEndDateVO.endDate"/>'
        }
      },
      startChecklist: {
        header: {
          text: '<s:message code="checklist.start.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.start.summary"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message javaScriptEscape="true" code="checklist.start.success"/>',
        config: {
          message: '<s:message code="checklist.start.prompt"/>'
        }
      },
      viewErrors: {
        header: {
          text: '<s:message code="checklist.errors.header"/>',
          icon: checklistHeaderIcon
        },
        narrative: {
          summary: '<s:message javaScriptEscape="true" code="checklist.errors.summary"/>',
          description: narrativeDescription
        },
        labels: {
          canIgnore: '<s:message javaScriptEscape="true" code="checklist.errors.canIgnore"/>',
          canRetry: '<s:message javaScriptEscape="true" code="checklist.errors.canRetry"/>'
        },
        config: {
          url: '<s:url value="/rest/checklistInstance/{id}"/>',
          recoveryUrl: '<s:url value="/rest/checklistInstance/{id}/status?action=recoveredFromError&recoveryMode={mode}"/>'
        }
      }
    }
  };

  var resultsConfig = {
    title:'<s:message javaScriptEscape="true" code="checklist.title"/>',
    searchConfig: resultsSearchConfig,
    filterContextConfig: resultsFilterContextConfig,
    quickFiltersConfig: resultsQuickFiltersConfig,
    filterConfig: resultsFilterConfig,
    dialogConfig: statusDialogConfig,
    permissions: permissions
  };

  var addSearchConfig = {
    sortBy: [{
      name: 'asc'
    }],
    labels: {
      name: '<s:message code="checklist.add.results.name"/>',
      version: '<s:message code="checklist.add.results.version"/>',
      actions: '<s:message code="common.column.actions"/>',
      addChecklist: '<s:message code="checklist.add.results.addChecklist"/>',
      addChecklistTitle: '<s:message code="checklist.add.results.addChecklistTitle"/>'
    },
    noDataMessage: '<s:message code="checklist.add.no.results"/>',
    permissions: permissions,
    resultsCountNode: Y.one('#checklistResultsCount'),
    url: '<s:url value="/rest/checklistDefinition?status=PUBLISHED&s={sortBy}&pageNumber={page}&pageSize={pageSize}&preFilterAccessLevel=WRITE&excludePreferAutomatedAdd={excludePreferAutomatedAdd}"/>'
  };
  var addFilterContextConfig = {
    labels: {
      filter: '<s:message code="filter.active"/>',
      name: '<s:message code="checklist.add.filter.name.active"/>'
    },
    container: '#checklistFilterContext'
  };

  var addFilterConfig = {
    labels: {
      filterBy: '<s:message code="filter.by"/>',
      name: '<s:message code="checklist.add.filter.name"/>'
    }
  };
  
  var addQuickFiltersConfig = {
	      container: '#checklistQuickFilters'
  };

  var addDialogConfig = {
    addChecklistInstanceURL: Y.Lang.sub('<s:url value="/rest/{subjectType}/{id}/checklistInstance"/>', subject),
    existingChecklistInstanceURL: '<s:url value="/rest/checklistInstance?subjectId={subjectId}&subjectType=PERSON&checklistDefinitionId={checklistDefinitionId}&status=ON_TIME,LAST_REMINDER,OVERDUE,PAUSED"/>',
    personTeamsURL: '<s:url value="/rest/person/{id}/teamRelationship/professional?temporalStatusFilter=ACTIVE&sort=roleBOrganisation.name&pageSize=-1"/>',
    //URL to load checklist definition
    checklistDefinitionURL: '<s:url value="/rest/checklistDefinition/{id}"/>',
    potentialOwnersURL: '<s:url value="/rest/checklistDefinition/{id}/potentialOwners"/>',
    successMessage: '<s:message javaScriptEscape="true" code="checklistDefinition.add.success"/>',
    groupSuccessMessage: '<s:message javaScriptEscape="true" code="checklistInstance.addForGroup.success"/>',
    views:{
      showExistingInstance: {
        header: {
          text: '<s:message code="checklistInstance.showExistingInstance.header"/>',
          icon: checklistHeaderIcon
        },
        narrative:{
          summary:'<s:message javaScriptEscape="true" code="checklistInstance.showExistingInstance.summary"/>',
          description: narrativeDescription
        },
        config:{
          message: '<s:message code="checklistInstance.showExistingInstance.prompt"/>'  
        }
      },
      showExistingGroupChecklistInstances: {
        header: {
          text: '<s:message code="checklistInstance.existingGroupChecklistInstances.header"/>',
          icon: checklistHeaderIcon
        },
        narrative:{
          summary:'<s:message javaScriptEscape="true" code="checklistInstance.existingGroupChecklistInstances.summary"/>',
          description: narrativeDescription
        },
        config:{
          message: '<s:message code="checklistInstance.existingGroupChecklistInstances.prompt"/>'
        }
      },
      addChecklistInstance: {
        header: {
          text: '<s:message code="checklistInstance.addChecklistInstances.header"/>',
          icon: checklistHeaderIcon
        },
        narrative:{
          description: narrativeDescription,
          requireStartDate:{
            summary:'<s:message javaScriptEscape="true" code="checklistInstance.backdateChecklistInstance.summary"/>'
          },
          requireEndDate:{
            summary:'<s:message javaScriptEscape="true" code="checklistInstance.fixedEndDateChecklistInstance.summary"/>'
          },
          requireOwnership:{
            summary:'<s:message javaScriptEscape="true" code="checklistInstance.ownershipChecklistInstance.summary"/>',
            requireStartDate:{
              summary:'<s:message javaScriptEscape="true" code="checklistInstance.ownershipAndBackdateChecklistInstance.summary"/>'
            },
            requireEndDate:{
              summary:'<s:message javaScriptEscape="true" code="checklistInstance.ownershipAndFixedEndDateChecklistInstance.summary"/>'  
            }
          }
        },
        config:{
          checklistInstanceBackdateViewConfig: { 
            labels: {
              startDate: '<s:message javaScriptEscape="true" code="newChecklistInstanceVO.startDate"/>'
            }
          },
          checklistInstanceFixedEndDateViewConfig: {
            labels: {
              endDate: '<s:message javaScriptEscape="true" code="newChecklistInstanceVO.endDate"/>'
            }
          },
          checklistInstanceOwnershipViewConfig: {
            labels: {
              ownerId: '<s:message javaScriptEscape="true" code="newChecklistInstanceVO.ownerId"/>',
              ownerSubjectType: '<s:message javaScriptEscape="true" code="newChecklistInstanceVO.ownerSubjectType"/>',
              ownerType: {
                organisation: '<s:message javaScriptEscape="true" code="checklist.add.ownerType.organisation"/>',
                person: '<s:message javaScriptEscape="true" code="checklist.add.ownerType.person"/>',
                select: '<s:message javaScriptEscape="true" code="checklist.add.ownerType.select"/>'
              },
              warningMessage: '<s:message javaScriptEscape="true" code="checklist.add.ownerType.warningMessage"/>'
            }
          }
        }
      }
    }
  };
  
  var errorsConfig={
      validationError: {
        title: '<s:message javaScriptEscape="true" code="checklist.add.error.title"/>',
        message: '<s:message javaScriptEscape="true" code="checklist.add.validationError.messsage"/>'
      },
      serverError: {
        title: '<s:message javaScriptEscape="true" code="checklist.add.error.title"/>',
        message: '<s:message javaScriptEscape="true" code="checklist.add.error.messsage"/>'
      },
      notFoundError: {
        title: '<s:message javaScriptEscape="true" code="checklist.add.error.title"/>',
        message: '<s:message javaScriptEscape="true" code="checklist.add.definitionNotFound.message"/>'
      }
  };

  var addConfig = {
    title:'<s:message javaScriptEscape="true" code="checklist.add.title"/>',
    searchConfig: addSearchConfig,
    filterContextConfig: addFilterContextConfig,
    quickFiltersConfig: addQuickFiltersConfig,
    filterConfig: addFilterConfig,
    dialogConfig: addDialogConfig,
    permissions: permissions,
    url: Y.Lang.sub('<s:url value="/checklist/{subjectType}/{subjectId}/add" />', subject),
    viewURL: '<s:url value="/checklist/view?id={id}" />',
    currentPersonId: '${currentPersonId}',
    currentPersonTeamId: '${currentPersonTeamId}',
    errorsConfig:errorsConfig
  };

  var config = {
    root: Y.Lang.sub('<s:url value="/checklist/{subjectType}/{subjectId}/"/>', subject),
    container: '#checklistResults',
    permissions: permissions,
    subject: subject,
    resultsConfig: resultsConfig,
    addConfig: addConfig,
    addRoute: Y.Lang.sub('<s:url value="/checklist/{subjectType}/{subjectId}/add"/>', subject),
    viewRoute: Y.Lang.sub('<s:url value="/checklist/{subjectType}/{subjectId}/view"/>', subject)
  };

  var checklistControllerView = new Y.app.checklist.ChecklistControllerView(config).render();

  // Make sure to dispatch the current hash-based URL which was set by
  // the server to our route handlers.            
  if (checklistControllerView.hasRoute(checklistControllerView.getPath())) {
    checklistControllerView.dispatch();
  } else {
    //dispatch to starting page which is results
    checklistControllerView.navigate(Y.Lang.sub('<s:url value="/checklist/{subjectType}/view"/>', subject));
  }
});
</script>