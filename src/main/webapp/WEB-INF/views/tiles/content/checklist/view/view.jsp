<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<sec:authorize access="hasPermission(null, 'ChecklistInstance','ChecklistInstance.View')" var="canView" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'TaskInstance','TaskInstance.DECLINED')" var="canDecline" />
	<sec:authorize access="hasPermission(null, 'TaskInstance','TaskInstance.COMPLETED')" var="canComplete" />
</c:if>

<c:choose>
	<c:when test="${not empty group}">
		<c:set var="currentSubjectType" value="group"/>
		<c:set var="currentSubjectId" value="${group.id}"/>
	</c:when>
	<c:otherwise>
		<c:set var="currentSubjectType" value="person"/>
		<c:set var="currentSubjectId" value="${person.id}"/>
	</c:otherwise>
</c:choose>
			
<c:choose>
	<c:when test="${canView eq true}">
		<div id="checklist-instance" class="yui3-app"></div>
	
		<script>
    Y.use('yui-base', 'escape', function(Y) {
      var L=Y.Lang, E=Y.Escape;
      
      //render the component
      uspChecklistInstance.render(document.getElementById('checklist-instance'), {
          id: '${checklistInstanceId}',
          labels: {
              checklist: '<s:message code="checklistInstance.title" javaScriptEscape="true"/>',
              percentComplete: '<s:message code="checklistInstance.percentComplete" javaScriptEscape="true"/>',
              emptyContainer: '<s:message code="checklistInstance.container.empty" javaScriptEscape="true"/>',
              dateStarted: '<s:message code="checklistInstance.dateStarted" javaScriptEscape="true"/>',
              dateDue: '<s:message code="checklistInstance.dateDue" javaScriptEscape="true"/>',
              dateCompleted: '<s:message code="checklistInstance.dateCompleted" javaScriptEscape="true"/>',
              error: '<s:message code="checklistInstance.inError" javaScriptEscape="true"/>',
              tasks: {
                  task: '<s:message code="checklistInstance.task.title" javaScriptEscape="true"/>',
                  dateStarted: '<s:message code="checklistInstance.task.dateStarted" javaScriptEscape="true"/>',
                  dateDue: '<s:message code="checklistInstance.task.dateDue" javaScriptEscape="true"/>',
                  owner: '<s:message code="checklistInstance.task.owner" javaScriptEscape="true"/>',
                  complete: '<s:message code="checklistInstance.task.complete" javaScriptEscape="true"/>',
                  taskType: '<s:message code="checklistInstance.task.taskType" javaScriptEscape="true"/>',
                  state: '<s:message code="checklistInstance.task.state" javaScriptEscape="true"/>',
                  lastReminderSent: '<s:message code="checklistInstance.task.lastReminder" javaScriptEscape="true"/>',
                  expectedStart: '<s:message code="checklistInstance.task.dateStartExpected" javaScriptEscape="true"/>',
                  dateCompleted: '<s:message code="checklistInstance.task.dateCompleted" javaScriptEscape="true"/>',
                  dateCompletedActual: '<s:message code="checklistInstance.task.dateCompletedActual" javaScriptEscape="true"/>',
                  description: '<s:message code="checklistInstance.task.description" javaScriptEscape="true"/>',
                  showAllDetailTitle: '<s:message code="checklistInstance.task.showAll.title" javaScriptEscape="true"/>',
                  hideAllDetailTitle: '<s:message code="checklistInstance.task.hideAll.title" javaScriptEscape="true"/>',
                  showDetailTitle: '<s:message code="checklistInstance.task.show.title" javaScriptEscape="true"/>',
                  hideDetailTitle: '<s:message code="checklistInstance.task.hide.title" javaScriptEscape="true"/>',
                  allCompleteTasksHidden: '<s:message code="checklistInstance.task.allComplete" javaScriptEscape="true"/>',
                  goToType: '<s:message code="checklistInstance.task.goToType" javaScriptEscape="true"/>',
                  addType: '<s:message code="checklistInstance.task.addType" javaScriptEscape="true"/>',
                  viewType: '<s:message code="checklistInstance.task.viewType" javaScriptEscape="true"/>',
                  targetRemoved: '<s:message code="checklistInstance.task.targetRemoved" javaScriptEscape="true"/>',
                  error: '<s:message code="checklistInstance.task.inError" javaScriptEscape="true"/>',
                  taskState: {
                      skipped: '<s:message code="checklistInstance.task.state.skipped" javaScriptEscape="true"/>',
                      inactive: '<s:message code="checklistInstance.task.state.inactive" javaScriptEscape="true"/>',
                      completeDone: '<s:message code="checklistInstance.task.state.completeDone" javaScriptEscape="true"/>',
                      completeDeclined: '<s:message code="checklistInstance.task.state.completeDeclined" javaScriptEscape="true"/>',
                      active: '<s:message code="checklistInstance.task.state.active" javaScriptEscape="true"/>',
                      activeOverdue: '<s:message code="checklistInstance.task.state.activeOverdue" javaScriptEscape="true"/>',
                      activeLastReminder: '<s:message code="checklistInstance.task.state.activeLastReminder" javaScriptEscape="true"/>',
                      error: '<s:message code="checklistInstance.task.state.error" javaScriptEscape="true"/>',
                      errorIgnored: '<s:message code="checklistInstance.task.state.errorIgnored" javaScriptEscape="true"/>',
                      paused: '<s:message code="checklistInstance.task.state.paused" javaScriptEscape="true"/>',
                      deleted: '<s:message code="checklistInstance.task.state.deleted" javaScriptEscape="true"/>'
                  },
                  condition: {
                      mandatory: '<s:message code="checklistInstance.task.condition.mandatory" javaScriptEscape="true"/>',
                      optional: '<s:message code="checklistInstance.task.condition.optional" javaScriptEscape="true"/>',
                      skip: '<s:message code="checklistInstance.task.condition.skip" javaScriptEscape="true"/>'
                  },
                  actions: {
                      decline: '<s:message code="checklistInstance.task.actions.decline" javaScriptEscape="true"/>',
                      declineTitle: '<s:message code="checklistInstance.task.actions.declineTitle" javaScriptEscape="true"/>',
                      declineLabel: '<s:message code="checklistInstance.task.actions.declineLabel" javaScriptEscape="true"/>',
                      declineConfirm: '<s:message code="checklistInstance.task.actions.declineConfirm" javaScriptEscape="true"/>',
                      complete: '<s:message code="checklistInstance.task.actions.complete" javaScriptEscape="true"/>',
                      completeTitle: '<s:message code="checklistInstance.task.actions.completeTitle" javaScriptEscape="true"/>',
                      completeLabel: '<s:message code="checklistInstance.task.actions.completeLabel" javaScriptEscape="true"/>',
                      completeConfirm: '<s:message code="checklistInstance.task.actions.completeConfirm" javaScriptEscape="true"/>'
                  }
              },
              taskGroups: {
                  todoTask: '<s:message code="checklistInstance.taskGroup.todo.task.count" javaScriptEscape="true"/>',
                  todoTasks: '<s:message code="checklistInstance.taskGroup.todo.tasks.count" javaScriptEscape="true"/>',
                  todo: '<s:message code="checklistInstance.taskGroup.todo" javaScriptEscape="true"/>',
                  overdueTask: '<s:message code="checklistInstance.taskGroup.overdue.task.count" javaScriptEscape="true"/>',
                  overdueTasks: '<s:message code="checklistInstance.taskGroup.overdue.tasks.count" javaScriptEscape="true"/>',
                  late: '<s:message code="checklistInstance.taskGroup.late" javaScriptEscape="true"/>',
                  lastReminderTask: '<s:message code="checklistInstance.taskGroup.lastReminder.task.count" javaScriptEscape="true"/>',
                  lastReminderTasks: '<s:message code="checklistInstance.taskGroup.lastReminder.tasks.count" javaScriptEscape="true"/>',
                  lastReminder: '<s:message code="checklistInstance.taskGroup.lastReminder" javaScriptEscape="true"/>',
                  completeTasks: '<s:message code="checklistInstance.taskGroup.complete.all" javaScriptEscape="true"/>',
                  complete: '<s:message code="checklistInstance.taskGroup.complete" javaScriptEscape="true"/>',
                  error: '<s:message code="checklistInstance.taskGroup.error" javaScriptEscape="true"/>'
              },
              candidateItems:{
                title: '<s:message code="checklistInstance.candidateItems.title" javaScriptEscape="true"/>'
              },
              linkSubject:{
                groupPrompt:'<s:message code="checklistInstance.link.group" javaScriptEscape="true"/>',
                addToGroup:'<s:message code="checklistInstance.link.group.add" javaScriptEscape="true"/>',
                addToIndividual:'<s:message code="checklistInstance.link.individual.add" javaScriptEscape="true"/>'
              }
          },
          permissions: {
              canView: '${canView}',
              canDecline: '${canDecline}',
              canComplete: '${canComplete}'
          }
      }, {
          serverConfig: {
              loadEndpoint: {
                  url: '<s:url value="/rest/checklistInstance/{id}"/>'
              },
              taskTypeConfigurationEndpoint: {
                  url: '<s:url value="/rest/taskTypeDefinition"/>'
              },
              taskDeclineEndpoint: {
                  url: '<s:url value="/rest/taskInstance/{id}/status?action=declined"/>'
              },
              taskCompleteEndpoint: {
                  url: '<s:url value="/rest/taskInstance/{id}/status?action=completed"/>'
              }
          },
          taskURLConfig: {
              FORM: {
                  view: {
                      url: '<s:url value="/forms/{subjectType}/{subjectId}/view?reset=true" />',
                      find:{
                        //basic url
                        url: '<s:url value="/rest/formInstance/{id}" />',
                        Accept: 'application/vnd.olmgroup-usp.form.FormInstance+json',
                        candidateFormatter: [{
                          label: '<s:message code="checklistInstance.taskType.candidate.form.name" javaScriptEscape="true"/>',
                          getter: function(candidate) {
                            var name=candidate.formDefinitionName||candidate.name||'Unknown';
                            var subType=candidate.subType;
                            return E.html(name+(subType?' ('+subType+')':''));
                          }
                        },{
                          label: '<s:message code="checklistInstance.taskType.candidate.form.identifer" javaScriptEscape="true"/>',
                            getter: function(candidate) {
                              return E.html(candidate.formIdentifier||'');
                            }
                        },{
                            label: '<s:message code="checklistInstance.taskType.candidate.form.dateStarted" javaScriptEscape="true"/>',
                            getter: function(candidate) {
                                return Y.USPDate.formatDateValue(candidate.dateStarted);
                            }
                        }, {
                          label: '<s:message code="checklistInstance.taskType.candidate.form.status" javaScriptEscape="true"/>',
                          getter: function(candidate) {
                              var status = candidate.status,
                                  hasRejections = candidate.hasRejections,
                                  isAuthorised = (candidate.dateSubmitted!=null && candidate.dateCompleted!=null);
                              
                              var FORM_STATE_DRAFT='DRAFT',
                                  FORM_STATE_SUBMITTED='SUBMITTED',
                                  FORM_STATE_COMPLETE='COMPLETE',
                                  FORM_STATE_DELETED='ARCHIVED';
                              
                              var template='<span class="pop-up-data" data-title="Form state" data-content="{content}"><i class="icon-state icon-state-{state}"></i></span>';
                              
                              var content='';
                              switch(status){
                                case FORM_STATE_DRAFT:
                                  content=L.sub(template, {
                                    content:'Draft'+(hasRejections?' (Rejected)':''),
                                    state:hasRejections?'rejected':'draft'
                                });
                                break;
                                case FORM_STATE_SUBMITTED:
                                  content=L.sub(template, {
                                    content:'Submitted',
                                    state:'submitted'
                                  });
                                break;
                                case FORM_STATE_COMPLETE:
                                  content=L.sub(template, {
                                    content:'Complete'+(isAuthorised?' (Authorised)':''),
                                    state:'complete'+(isAuthorised?'-authorised':'')
                                  });
                                break;
                                case FORM_STATE_DELETED:
                                  content=L.sub(template, {
                                    content:'Deleted',
                                    state:'deleted'
                                  });
                                break;
                              }
                              return content;
                          }
                        }]
                      }
                  },
                  add: {
                      url: '<s:url value="/forms/{subjectType}/{subjectId}/add?reset=true" />'
                  }
              },
              CASENOTE: {
                  view: {
                      url: '<s:url value="/casenote/{subjectType}?id={subjectId}&reset=true" />'
                  },
                  add: {
                    url: '<s:url value="/casenote/{subjectType}?id={subjectId}&reset=true" />'
                }
              },
              CHRONOLOGY: {
                  view: {
                      url: '<s:url value="/chronology/{subjectType}?id={subjectId}&reset=true"/>'
                  },
                  add: {
                    url: '<s:url value="/chronology/{subjectType}?id={subjectId}&reset=true" />'
                }
              }
          }
      },
      //Pass configured YUI instance
      Y);
  });
		</script>
	</c:when>
	<c:otherwise>
		<%-- Insert access denied tile --%>
		<t:insertDefinition name="access.denied"/>
	</c:otherwise>
</c:choose>
