<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'PeriodOfCare','PeriodOfCare.GET')" var="canView" />
<sec:authorize access="hasPermission(null, 'AdoptionInformation','AdoptionInformation.GET')"
    var="canViewAdoptionInformation" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
    <sec:authorize access="hasPermission(null, 'PeriodOfCare','PeriodOfCare.ADD')" var="canAdd" />
    <sec:authorize access="hasPermission(null, 'PeriodOfCare','PeriodOfCare.DELETE')" var="canDelete" />
    <sec:authorize access="hasPermission(null, 'EpisodeOfCare','EpisodeOfCare.DELETE')" var="canDeleteEpisode" />
    <sec:authorize access="hasPermission(null, 'AbsenceFromPlacement','AbsenceFromPlacement.ARCHIVE')"
        var="canArchiveAbsenceFromPlacement" />
    <sec:authorize access="hasPermission(null, 'AbsenceFromPlacement','AbsenceFromPlacement.UPDATE')"
        var="canUpdateAbsenceFromPlacement" />
    <sec:authorize access="hasPermission(null, 'AdoptionInformation','AdoptionInformation.UPDATE')"
        var="canUpdateAdoptionInformation" />
    <sec:authorize access="hasPermission(null, 'Placement','Placement.DELETE')" var="canDeleteTemporaryPlacement" />
    <sec:authorize access="hasPermission(null, 'Placement','Placement.UPDATE')" var="canUpdatePlacement" />
</c:if>

<%-- access CLA allowed attribute --%>
<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="claAttributesContext" value="${userContextAttributes.getContext('cla-episodes')}" />
<c:set var="isShowAdmissionReasonApplicable" value="${claAttributesContext.isShowAdmissionReasonApplicable()}" />
<c:set var="isShowProviderTypeFieldApplicable" value="${claAttributesContext.isShowProviderTypeFieldApplicable()}" />
<c:set var="isShowPlacementProvisionApplicable" value="${claAttributesContext.isShowPlacementProvisionApplicable()}" />
<c:set var="isShowPreviouslyLookedAfterApplicable" value="${claAttributesContext.isShowPreviouslyLookedAfterApplicable()}" />
<c:set var="isShowSiblingsApplicable" value="${claAttributesContext.isShowSiblingsApplicable()}" />
<c:set var="isShowPlannedEndOfPlacementApplicable" value="${claAttributesContext.isShowPlannedEndOfPlacementApplicable()}" />
<c:set var="isShowEndLabelApplicable" value="${claAttributesContext.isShowEndLabelApplicable()}" />
<c:set var="isShowScottishView" value="${claAttributesContext.isShowScottishView()}" />
<c:set var="isShowWelshView" value="${claAttributesContext.isShowWelshView()}" />
<c:set var="isSupportMutipleLegalStatusEntriesApplicable" value="${claAttributesContext.isSupportMutipleLegalStatusEntriesApplicable()}" />

<%-- Define permissions --%>
<div id="childLookedAfterResults"></div>

<script>
    Y.use('childlookedafter-controller-view', function (Y) {
        var permissions = {
            canView: '${canView}' === 'true',
            canDelete: '${canDelete}' === 'true',
            canOutput: '${canView}' === 'true',
            canAddPeriodOfCare: '${canAdd}' === 'true',
            canDeleteEpisode: '${canDeleteEpisode}' === 'true',
            canArchiveAbsenceFromPlacement: '${canArchiveAbsenceFromPlacement}' === 'true',
            canUpdateAbsenceFromPlacement: '${canUpdateAbsenceFromPlacement}' === 'true',
            canUpdateAdoptionInformation: '${canUpdateAdoptionInformation}' === 'true',
            canViewAdoptionInformation: '${canViewAdoptionInformation}' === 'true',
            canDeleteTemporaryPlacement: '${canDeleteTemporaryPlacement}' === 'true',
            canUpdatePlacement: '${canUpdatePlacement}' === 'true'
        };

        var subjectNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

        var subject = {
            subjectId: '<c:out value="${person.id}"/>',
            subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
            subjectName: '${esc:escapeJavaScript(person.name)}',
            subjectType: 'person',
            dateOfBirth: '${esc:escapeJavaScript(person.dateOfBirth.calculatedDate)}'
        };

        var outputNarrativeDescription = '{{this.subjectName}} ({{this.subjectIdentifier}})';

        var outputDialogConfig;

        //Handlebar needs a boolean value.
        var showProviderTypeField = '${isShowProviderTypeFieldApplicable}'==='true';
        var showAdmissionReason = '${isShowAdmissionReasonApplicable}'==='true';
        var showPlacementProvision = '${isShowPlacementProvisionApplicable}'==='true';
        var showPreviouslyLookedAfter = '${isShowPreviouslyLookedAfter}'==='true';
        var showSiblings = '${isShowSiblingsApplicable}'==='true';
        var showPlannedEndOfPlacement = '${isShowPlannedEndOfPlacementApplicable}'==='true';
        var showEndLabel = '${isShowEndLabelApplicable}'==='true';
   
        if (subject.subjectType === 'person') {
            outputDialogConfig = {
                views: {
                    produceDocument: {
                        type: 'app.output.BaseOutputDocumentView',
                        header: {
                            text: '<s:message code="childlookedafter.output.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-file'
                        },
                        narrative: {
                            summary: '<s:message code="produceDocument.dialog.summary" javaScriptEscape="true" />',
                            description: outputNarrativeDescription
                        },
                        labels: {
                            outputCategory: '<s:message code="produceDocument.dialog.category" javaScriptEscape="true"/>',
                            outputCategoryPrompt: '<s:message code="produceDocument.dialog.category.select.blank" javaScriptEscape="true"/>',
                            availableTemplates: '<s:message code="produceDocument.dialog.availableTemplates" javaScriptEscape="true"/>',
                            outputFormat: '<s:message code="produceDocument.dialog.outputFormat" javaScriptEscape="true"/>'
                        },
                        config: {
                            templateListURL: '<s:url value="/rest/outputTemplate?status=PUBLISHED&context=CHILD_LOOKED_AFTER&asList=true"/>',
                            generateURL: '<s:url value="/rest/documentRequest"/>',
                            documentType: 'CHILD_LOOKED_AFTER',
                            successMessage: '<s:message code="produceDocument.dialog.success.generateInProgress" javaScriptEscape="true" />',
                            failureMessage: '<s:message code="produceDocument.dialog.failure.generateFailed" javaScriptEscape="true" />'
                        }
                    }
                }
            };
        }

        var fuzzyDateLabels = {
            date: '<s:message code="childlookedafter.periodofcare.dialog.add.permanencePlacementDate" javaScriptEscape="true"/>',
            year: '<s:message code="fuzzyDate.year" javaScriptEscape="true"/>',
            month: '<s:message code="fuzzyDate.month" javaScriptEscape="true"/>',
            day: '<s:message code="fuzzyDate.day" javaScriptEscape="true"/>',
            hour: '<s:message code="fuzzyDate.hour" javaScriptEscape="true"/>',
            minute: '<s:message code="fuzzyDate.minute" javaScriptEscape="true"/>',
            second: '<s:message code="fuzzyDate.second" javaScriptEscape="true"/>'
        };

        var scottishLabels = {
            periodsOfCareView: {
                title: '<s:message code="childlookedafter.periodofcare.scottishCLA.title" javaScriptEscape="true"/>',
                accordionTitle: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.add.header" javaScriptEscape="true"/>',
                noResultsMain: '<s:message code="childlookedafter.episodeofcare.noresults" javaScriptEscape="true"/>',
                episodeOfCareTable: {
                    header: '<s:message code="childlookedafter.episodeofcare.scottishCLA.table.header" javaScriptEscape="true"/>',
                    edit: '<s:message code="childlookedafter.episodeofcare.scottishCLA.table.edit" javaScriptEscape="true"/>',
                    view: '<s:message code="childlookedafter.episodeofcare.scottishCLA.table.view" javaScriptEscape="true"/>',
                    deleteEpisodeOfCare: '<s:message code="childlookedafter.episodeofcare.scottishCLA.table.delete" javaScriptEscape="true"/>',
                },
                addEpisodeOfCare: '<s:message code="childlookedafter.episodeofcare.scottishCLA.add.button" javaScriptEscape="true"/>',
                endPeriodOfCare: '<s:message code="childlookedafter.periodofcare.scottishCLA.end.button" javaScriptEscape="true"/>',
                deletePeriodOfCare: '<s:message code="childlookedafter.periodofcare.scottishCLA.delete.button" javaScriptEscape="true"/>',
            }
        }

        var labels = {
            periodsOfCareView: {
                title: '<s:message code="childlookedafter.periodofcare.title" javaScriptEscape="true"/>',
                accordionTitle: '<s:message code="childlookedafter.periodofcare.dialog.add.header" javaScriptEscape="true"/>',
                accordionHistoricTitle: '<s:message code="childlookedafter.periodofcare.dialog.add.historical.header" javaScriptEscape="true"/>',
                noResultsMain: '<s:message code="childlookedafter.periodofcare.noresults" javaScriptEscape="true"/>',
                accordionCounts: {
                    absences: 'Absences',
                    episodes: 'Episodes'
                },
                periodOfCareSummaryTable: {
                    startDate: '<s:message code="childlookedafter.periodofcare.table.startDate" javaScriptEscape="true"/>',
                    endDate: '<s:message code="childlookedafter.periodofcare.table.endDate" javaScriptEscape="true"/>',
                    endDateReason: '<s:message code="childlookedafter.periodofcare.table.endDate.reason" javaScriptEscape="true"/>',
                    categoryOfNeed: '<s:message code="childlookedafter.periodofcare.table.categoryOfNeed" javaScriptEscape="true"/>',
                    careAndSupportNeed: '<s:message code="childlookedafter.periodofcare.dialog.add.careAndSupportNeed" javaScriptEscape="true"/>',
                    admissionReason: '<s:message code="childlookedafter.periodofcare.table.admissionReason" javaScriptEscape="true"/>',
                    episodes: '<s:message code="childlookedafter.periodofcare.table.episodes" javaScriptEscape="true"/>',
                    absences: '<s:message code="childlookedafter.periodofcare.table.absences" javaScriptEscape="true"/>',
                    edit: '<s:message code="childlookedafter.periodofcare.table.edit" javaScriptEscape="true"/>',
                    remove: '<s:message code="childlookedafter.periodofcare.table.remove" javaScriptEscape="true"/>'
                },
                episodeOfCareTable: {
                    header: '<s:message code="childlookedafter.episodeofcare.table.header" javaScriptEscape="true"/>',
                    absences: '<s:message code="childlookedafter.episodeofcare.table.absences" javaScriptEscape="true"/>',
                    startDate: '<s:message code="childlookedafter.episodeofcare.table.startDate" javaScriptEscape="true"/>',
                    endDate: '<s:message code="childlookedafter.episodeofcare.table.endDate" javaScriptEscape="true"/>',
                    legalStatus: '<s:message code="childlookedafter.episodeofcare.table.legalStatus" javaScriptEscape="true"/>',
                    placementDetail: '<s:message code="childlookedafter.episodeofcare.table.placementDetail" javaScriptEscape="true"/>',
                    placementType: '<s:message code="childlookedafter.episodeofcare.table.placementType" javaScriptEscape="true"/>',
                    placedWith: '<s:message code="childlookedafter.episodeofcare.table.placedWith" javaScriptEscape="true"/>',
                    dataContentHeader: '<s:message code="childlookedafter.episodeofcare.table.dataContentHeader" javaScriptEscape="true"/>',
                    noOrgDetails: '<s:message code="childlookedafter.episodeofcare.table.noOrgDetails" javaScriptEscape="true"/>',
                    noRegOrg: '<s:message code="childlookedafter.episodeofcare.table.noRegOrg" javaScriptEscape="true"/>',
                    address: '<s:message code="childlookedafter.episodeofcare.table.address" javaScriptEscape="true"/>',
                    additionalPlacements: '<s:message code="childlookedafter.episodeofcare.table.additionalPlacements" javaScriptEscape="true"/>',
                    category: '<s:message code="childlookedafter.episodeofcare.table.category" javaScriptEscape="true"/>',
                    carerAddress: '<s:message code="childlookedafter.episodeofcare.table.placedWith.address" javaScriptEscape="true"/>',
                    edit: '<s:message code="childlookedafter.episodeofcare.table.edit" javaScriptEscape="true"/>',
                    view: '<s:message code="childlookedafter.episodeofcare.table.view" javaScriptEscape="true"/>',
                    deleteEpisodeOfCare: '<s:message code="childlookedafter.episodeofcare.table.delete" javaScriptEscape="true"/>',
                    editTempPlacement: '<s:message code="childlookedafter.episodeofcare.table.temporary.placement.edit" javaScriptEscape="true"/>',
                    viewTempPlacement: '<s:message code="childlookedafter.episodeofcare.table.temporary.placement.view" javaScriptEscape="true"/>',
                    deleteTempPlacement: '<s:message code="childlookedafter.episodeofcare.table.temporary.placement.delete" javaScriptEscape="true"/>',
                    remove: '<s:message code="childlookedafter.episodeofcare.table.remove" javaScriptEscape="true"/>',
                    noData: '<s:message code="childlookedafter.episodeofcare.table.noData" javaScriptEscape="true"/>'
                },
                absenceFromPlacementTable: {
                    header: '<s:message code="childlookedafter.absencefromplacement.table.header" javaScriptEscape="true"/>',
                    startDate: '<s:message code="childlookedafter.absencefromplacement.table.startDate" javaScriptEscape="true"/>',
                    endDate: '<s:message code="childlookedafter.absencefromplacement.table.endDate" javaScriptEscape="true"/>',
                    absenceType: '<s:message code="childlookedafter.absencefromplacement.table.absenceType" javaScriptEscape="true"/>',
                    absenceNote: '<s:message code="childlookedafter.absencefromplacement.table.absenceNote" javaScriptEscape="true"/>',
                    absentWith: '<s:message code="childlookedafter.absencefromplacement.table.absentWith" javaScriptEscape="true"/>',
                    edit: '<s:message code="childlookedafter.absencefromplacement.table.edit" javaScriptEscape="true"/>',
                    archive: '<s:message code="childlookedafter.absencefromplacement.table.archive" javaScriptEscape="true"/>',
                    noData: '<s:message code="childlookedafter.absencefromplacement.table.noData" javaScriptEscape="true"/>'
                },
                adoptionInformationTable: {
                    header: '<s:message code="childlookedafter.adoptioninformation.table.header" javaScriptEscape="true"/>',
                    dateShouldBePlaced: '<s:message code="childlookedafter.adoptioninformation.table.dateShouldBePlaced" javaScriptEscape="true"/>',
                    dateMatched: '<s:message code="childlookedafter.adoptioninformation.table.dateMatched" javaScriptEscape="true"/>',
                    datePlaced: '<s:message code="childlookedafter.adoptioninformation.table.datePlaced" javaScriptEscape="true"/>',
                    legalStatusOfAdopters: '<s:message code="childlookedafter.adoptioninformation.table.legalStatusOfAdopters" javaScriptEscape="true"/>',
                    noData: '<s:message code="childlookedafter.adoptioninformation.table.noData" javaScriptEscape="true"/>',
                    edit: '<s:message code="childlookedafter.adoptioninformation.table.edit" javaScriptEscape="true"/>',
                    view: '<s:message code="childlookedafter.adoptioninformation.table.view" javaScriptEscape="true"/>',
                    action: '<s:message code="common.column.actions" javaScriptEscape="true"/>'
                },
                addEpisodeOfCare: '<s:message code="childlookedafter.episodeofcare.add.button" javaScriptEscape="true"/>',
                addAdoptionInformation: '<s:message code="childlookedafter.adoptioninformation.add.button" javaScriptEscape="true"/>',
                noteAbsence: '<s:message code="childlookedafter.absence.add.button" javaScriptEscape="true"/>',
                endPeriodOfCare: '<s:message code="childlookedafter.periodofcare.end.button" javaScriptEscape="true"/>',
                deletePeriodOfCare: '<s:message code="childlookedafter.periodofcare.delete.button" javaScriptEscape="true"/>',
                addTemporaryPlacement: '<s:message code="childlookedafter.temporaryPlacement.add.button" javaScriptEscape="true"/>',
                deleteTempPlacement: '<s:message code="childlookedafter.periodofcare.delete.button" javaScriptEscape="true"/>'
            },
            addPeriodOfCare: {
                startDate: '<s:message code="childlookedafter.periodofcare.dialog.add.startDate" javaScriptEscape="true"/>',
                admissionReason: '<s:message code="childlookedafter.periodofcare.dialog.add.admissionReason" javaScriptEscape="true"/>',
                previouslyLookedAfter: '<s:message code="childlookedafter.periodofcare.dialog.add.previouslyLookedAfter" javaScriptEscape="true"/>',
                previousPermanenceOption: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceOption" javaScriptEscape="true"/>',
                previousPermanenceLocalAuthority: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceLocalAuthority" javaScriptEscape="true"/>',
                previousPermanenceRegion: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceRegion" javaScriptEscape="true"/>',
                categoryOfNeed: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeed" javaScriptEscape="true"/>',
                categoryOfNeedMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeedMandatory" javaScriptEscape="true"/>',
                categoryOfNeedInvalid: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeedInvalid" javaScriptEscape="true"/>',
                careAndSupportNeed: '<s:message code="childlookedafter.periodofcare.dialog.add.careAndSupportNeed" javaScriptEscape="true"/>',
                siblingGroup: '<s:message code="childlookedafter.periodofcare.dialog.add.siblingGroup" javaScriptEscape="true"/>',
                legalStatus: '<s:message code="childlookedafter.periodofcare.dialog.add.legalStatus" javaScriptEscape="true"/>',
                siblingsAssessed: '<s:message code="childlookedafter.periodofcare.dialog.add.siblingsAssessed" javaScriptEscape="true"/>',
                placementType: '<s:message code="childlookedafter.periodofcare.dialog.add.placementType" javaScriptEscape="true"/>',
                placementProvision: '<s:message code="childlookedafter.periodofcare.dialog.add.placementProvision" javaScriptEscape="true"/>',
                carerSubjectType: '<s:message code="childlookedafter.periodofcare.dialog.add.carerSubjectType" javaScriptEscape="true"/>',
                typeOfCare: '<s:message code="childlookedafter.periodofcare.dialog.add.typeOfCare" javaScriptEscape="true"/>',
                startDateMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.startDateMandatory" javaScriptEscape="true"/>',
                placementTypeMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.placementTypeMandatory" javaScriptEscape="true"/>',
                legalStatusMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.legalStatusMandatory" javaScriptEscape="true"/>',
                carerSubjectMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.carerSubjectMandatory" javaScriptEscape="true"/>',
                previousPermanenceOptionMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceOptionMandatory" javaScriptEscape="true"/>',
                previousPermanenceLAOrRegionMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceLAOrRegionMandatory" javaScriptEscape="true"/>',
                categoryOfNeedMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeedMandatory" javaScriptEscape="true"/>',
                placementProvisionMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.placementProvisionMandatory" javaScriptEscape="true"/>',
                typeOfCareMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.typeOfCareMandatory" javaScriptEscape="true"/>',
                admissionReasonMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.admissionReasonMandatory" javaScriptEscape="true"/>',
                startDateFormat: '<s:message code="childlookedafter.periodofcare.dialog.add.startDateFormat" javaScriptEscape="true"/>',
                parent: '<s:message code="childlookedafter.periodofcare.dialog.add.parent" javaScriptEscape="true"/>',
                address: '<s:message code="childlookedafter.periodofcare.dialog.add.address" javaScriptEscape="true"/>',
                warnings: {
                    noParentExists: '<s:message code="childlookedafter.periodofcare.dialog.add.warnings.noParentExists" javaScriptEscape="true"/>',
                    noActiveHomeAddress: '<s:message code="childlookedafter.periodofcare.dialog.add.warnings.noActiveHomeAddress" javaScriptEscape="true"/>'
                },
                location: '<s:message code="childlookedafter.dialog.add.location" javaScriptEscape="true"/>',
                addLocation: '<s:message code="childlookedafter.dialog.add.addLocation" javaScriptEscape="true"/>',
                postcode: '<s:message code="childlookedafter.dialog.add.postcode" javaScriptEscape="true"/>',
                houseNameOrNumber: '<s:message code="childlookedafter.dialog.add.houseNameOrNumber" javaScriptEscape="true"/>',
                roomDescription: '<s:message code="childlookedafter.dialog.add.roomDescription" javaScriptEscape="true"/>',
                floorDescription: '<s:message code="childlookedafter.dialog.add.floorDescription" javaScriptEscape="true"/>',
                overMaxResults: '<s:message code="location.overMaxResults" javaScriptEscape="true" />',
                addressNote: '<s:message code="location.addressNote" javaScriptEscape="true" />',
                addALocation: '<s:message code="location.addALocation" javaScriptEscape="true" />',
                noResults: '<s:message code="location.noResults" javaScriptEscape="true" />',
                locationMandatory: '<s:message code="AtLeastOneNotNull.newPersonAddressVO.locationId" javaScriptEscape="true" />'
            },
            addEpisodeOfCare: {
                startReason: '<s:message code="childlookedafter.episodeofcare.dialog.add.startReason" javaScriptEscape="true"/>',
                startDate: '<s:message code="childlookedafter.episodeofcare.dialog.add.startDate" javaScriptEscape="true"/>',
                temporaryPlacement: '<s:message code="childlookedafter.episodeofcare.dialog.add.temporaryPlacement" javaScriptEscape="true"/>',
                legalStatus: '<s:message code="childlookedafter.episodeofcare.dialog.add.legalStatus" javaScriptEscape="true"/>',
                placementType: '<s:message code="childlookedafter.episodeofcare.dialog.add.placementType" javaScriptEscape="true"/>',
                placementProvision: '<s:message code="childlookedafter.periodofcare.dialog.add.placementProvision" javaScriptEscape="true"/>',
                placementChangeReason: '<s:message code="childlookedafter.episodeofcare.dialog.add.placementChangeReason" javaScriptEscape="true"/>',
                plannedEndOfPlacement: '<s:message code="childlookedafter.episodeofcare.dialog.add.plannedEndOfPlacement" javaScriptEscape="true"/>',
                carerSubjectType: '<s:message code="childlookedafter.episodeofcare.dialog.add.carerSubjectType" javaScriptEscape="true"/>',
                typeOfCare: '<s:message code="childlookedafter.episodeofcare.dialog.add.typeOfCare" javaScriptEscape="true"/>',
                parent: '<s:message code="childlookedafter.periodofcare.dialog.add.parent" javaScriptEscape="true"/>',
                address: '<s:message code="childlookedafter.episodeofcare.dialog.add.address" javaScriptEscape="true"/>',
                warnings: {
                    incorrectPreviousPlacement: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.incorrectPreviousPlacement" javaScriptEscape="true"/>',
                    overageHome: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.overageHome" javaScriptEscape="true"/>',
                    underageForYouthJustice: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.underageForYouthJustice" javaScriptEscape="true"/>',
                    underageForIndependentLiving: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.underageForIndependentLiving" javaScriptEscape="true"/>',
                    underageForHostels: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.underageForHostels" javaScriptEscape="true"/>',
                    underageForYouthPrison: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.underageForYouthPrison" javaScriptEscape="true"/>',
                    underageForResidentialSchool: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.underageForResidentialSchool" javaScriptEscape="true"/>',
                    policeProtectionDuration: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.policeProtectionDuration" javaScriptEscape="true"/>',
                    emergencyProtectionDuration: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.emergencyProtectionDuration" javaScriptEscape="true"/>',
                    assessmentOrderDuration: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.assessmentOrderDuration" javaScriptEscape="true"/>',
                    noParentExists: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.noParentExists" javaScriptEscape="true"/>',
                    noActiveHomeAddress: '<s:message code="childlookedafter.episodeofcare.dialog.add.warnings.noActiveHomeAddress" javaScriptEscape="true"/>'
                },
                location: '<s:message code="childlookedafter.dialog.add.location" javaScriptEscape="true"/>',
                addLocation: '<s:message code="childlookedafter.dialog.add.addLocation" javaScriptEscape="true"/>',
                postcode: '<s:message code="childlookedafter.dialog.add.postcode" javaScriptEscape="true"/>',
                houseNameOrNumber: '<s:message code="childlookedafter.dialog.add.houseNameOrNumber" javaScriptEscape="true"/>',
                roomDescription: '<s:message code="childlookedafter.dialog.add.roomDescription" javaScriptEscape="true"/>',
                floorDescription: '<s:message code="childlookedafter.dialog.add.floorDescription" javaScriptEscape="true"/>',
                overMaxResults: '<s:message code="location.overMaxResults" javaScriptEscape="true" />',
                addressNote: '<s:message code="location.addressNote" javaScriptEscape="true" />',
                addALocation: '<s:message code="location.addALocation" javaScriptEscape="true" />',
                noResults: '<s:message code="location.noResults" javaScriptEscape="true" />',
                locationMandatory: '<s:message code="AtLeastOneNotNull.newPersonAddressVO.locationId" javaScriptEscape="true" />'
            },
            viewEpisodeOfCare: {
                startReason: '<s:message code="childlookedafter.episodeofcare.dialog.view.startReason" javaScriptEscape="true"/>',
                admissionReason: '<s:message code="childlookedafter.episodeofcare.dialog.view.admissionReason" javaScriptEscape="true"/>',
                startDate: '<s:message code="childlookedafter.episodeofcare.dialog.view.startDate" javaScriptEscape="true"/>',
                endReason: '<s:message code="childlookedafter.episodeofcare.dialog.view.endReason" javaScriptEscape="true"/>',
                dischargeDate: '<s:message code="childlookedafter.episodeofcare.dialog.view.dischargeDate" javaScriptEscape="true"/>',
                dischargeReason: '<s:message code="childlookedafter.episodeofcare.dialog.view.dischargeReason" javaScriptEscape="true"/>',
                endDate: '<s:message code="childlookedafter.episodeofcare.dialog.view.endDate" javaScriptEscape="true"/>',
                temporaryPlacement: '<s:message code="childlookedafter.episodeofcare.dialog.view.temporaryPlacement" javaScriptEscape="true"/>',
                legalStatus: '<s:message code="childlookedafter.episodeofcare.dialog.view.legalStatus" javaScriptEscape="true"/>',
                placementType: '<s:message code="childlookedafter.episodeofcare.dialog.view.placementType" javaScriptEscape="true"/>',
                placementProvision: '<s:message code="childlookedafter.episodeofcare.dialog.view.placementProvision" javaScriptEscape="true"/>',
                placementChangeReason: '<s:message code="childlookedafter.episodeofcare.dialog.view.placementChangeReason" javaScriptEscape="true"/>',
                carerSubjectType: '<s:message code="childlookedafter.episodeofcare.dialog.view.carerSubjectType" javaScriptEscape="true"/>',
                fosterCarerRegistration: '<s:message code="childlookedafter.episodeofcare.dialog.view.fosterCarerRegistration" javaScriptEscape="true"/>',
                previouslyLookedAfter: '<s:message code="childlookedafter.episodeofcare.dialog.view.previouslyLookedAfter" javaScriptEscape="true"/>',
                siblingGroup: '<s:message code="childlookedafter.episodeofcare.dialog.view.siblingGroup" javaScriptEscape="true"/>',
                siblingsAssessed: '<s:message code="childlookedafter.episodeofcare.dialog.view.siblingsAssessed" javaScriptEscape="true"/>',
                typeOfCare: '<s:message code="childlookedafter.episodeofcare.dialog.view.typeOfCare" javaScriptEscape="true"/>',
                plannedEndOfPlacement: '<s:message code="childlookedafter.episodeofcare.dialog.view.plannedEndOfPlacement" javaScriptEscape="true"/>'
            },
            editEpisodeOfCare: {
                startReason: '<s:message code="childlookedafter.episodeofcare.dialog.edit.startReason" javaScriptEscape="true"/>',
                admissionReason: '<s:message code="childlookedafter.episodeofcare.dialog.edit.admissionReason" javaScriptEscape="true"/>',
                startDate: '<s:message code="childlookedafter.episodeofcare.dialog.edit.startDate" javaScriptEscape="true"/>',
                endReason: '<s:message code="childlookedafter.episodeofcare.dialog.edit.endReason" javaScriptEscape="true"/>',
                dischargeDate: '<s:message code="childlookedafter.episodeofcare.dialog.edit.dischargeDate" javaScriptEscape="true"/>',
                dischargeReason: '<s:message code="childlookedafter.episodeofcare.dialog.edit.dischargeReason" javaScriptEscape="true"/>',
                endDate: '<s:message code="childlookedafter.episodeofcare.dialog.edit.endDate" javaScriptEscape="true"/>',
                temporaryPlacement: '<s:message code="childlookedafter.episodeofcare.dialog.edit.temporaryPlacement" javaScriptEscape="true"/>',
                legalStatus: '<s:message code="childlookedafter.episodeofcare.dialog.edit.legalStatus" javaScriptEscape="true"/>',
                placementType: '<s:message code="childlookedafter.episodeofcare.dialog.edit.placementType" javaScriptEscape="true"/>',
                placementProvision: '<s:message code="childlookedafter.episodeofcare.dialog.edit.placementProvision" javaScriptEscape="true"/>',
                placementChangeReason: '<s:message code="childlookedafter.episodeofcare.dialog.edit.placementChangeReason" javaScriptEscape="true"/>',
                carerSubjectType: '<s:message code="childlookedafter.episodeofcare.dialog.edit.carerSubjectType" javaScriptEscape="true"/>',
                currentCarer: '<s:message code="childlookedafter.episodeofcare.dialog.edit.currentCarer" javaScriptEscape="true"/>',
                currentTypeOfCare: '<s:message code="childlookedafter.episodeofcare.dialog.edit.currentTypeOfCare" javaScriptEscape="true"/>',
                fosterCarerRegistration: '<s:message code="childlookedafter.episodeofcare.dialog.view.fosterCarerRegistration" javaScriptEscape="true"/>',
                previouslyLookedAfter: '<s:message code="childlookedafter.episodeofcare.dialog.view.previouslyLookedAfter" javaScriptEscape="true"/>',
                siblingGroup: '<s:message code="childlookedafter.episodeofcare.dialog.view.siblingGroup" javaScriptEscape="true"/>',
                siblingsAssessed: '<s:message code="childlookedafter.episodeofcare.dialog.view.siblingsAssessed" javaScriptEscape="true"/>',
                typeOfCare: '<s:message code="childlookedafter.episodeofcare.dialog.view.typeOfCare" javaScriptEscape="true"/>',
                plannedEndOfPlacement: '<s:message code="childlookedafter.episodeofcare.dialog.view.plannedEndOfPlacement" javaScriptEscape="true"/>'
            },
            deleteEpisodeOfCare: {
                successMessage: '<s:message code="childlookedafter.episodeofcare.dialog.delete.successMessage" javaScriptEscape="true"/>',
                deleteConfirm: '<s:message code="childlookedafter.episodeofcare.dialog.delete.confirmDeleteMessage" javaScriptEscape="true"/>'
            },
            prepareDeleteEpisodeOfCare: {
                successMessage: '<s:message code="childlookedafter.episodeofcare.dialog.delete.successMessage" javaScriptEscape="true"/>',
                deleteConfirm: '<s:message code="childlookedafter.episodeofcare.dialog.delete.prepare.confirmDeleteMessage" javaScriptEscape="true"/>'
            },
            addAbsenceFromPlacement: {
                startDate: '<s:message code="childlookedafter.absencefromplacement.dialog.add.startDate" javaScriptEscape="true"/>',
                startDateFormat: '<s:message code="childlookedafter.absencefromplacement.dialog.add.startDateFormat" javaScriptEscape="true"/>',
                startDateMandatory: '<s:message code="childlookedafter.absencefromplacement.dialog.add.startDateMandatory" javaScriptEscape="true"/>',
                endDate: '<s:message code="childlookedafter.absencefromplacement.dialog.add.endDate" javaScriptEscape="true"/>',
                endDateFormat: '<s:message code="childlookedafter.absencefromplacement.dialog.add.endDateFormat" javaScriptEscape="true"/>',
                absenceType: '<s:message code="childlookedafter.absencefromplacement.dialog.add.absenceType" javaScriptEscape="true"/>',
                reasonMissing: '<s:message code="childlookedafter.absencefromplacement.dialog.add.reasonMissing" javaScriptEscape="true"/>',
                childOfferedReturnInterview: '<s:message code="childlookedafter.absencefromplacement.dialog.add.childOfferedReturnInterview" javaScriptEscape="true"/>',
                childAcceptedReturnInterview: '<s:message code="childlookedafter.absencefromplacement.dialog.add.childAcceptedReturnInterview" javaScriptEscape="true"/>',
                absenceNote: '<s:message code="childlookedafter.absencefromplacement.dialog.add.absenceNote" javaScriptEscape="true"/>',
                absenceTypeMandatory: '<s:message code="childlookedafter.absencefromplacement.dialog.add.absenceTypeMandatory" javaScriptEscape="true"/>'
            },
            editAbsenceFromPlacement: {
                startDate: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.startDate" javaScriptEscape="true"/>',
                endDate: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.endDate" javaScriptEscape="true"/>',
                endDateFormat: '<s:message code="childlookedafter.absencefromplacement.dialog.add.endDateFormat" javaScriptEscape="true"/>',
                absenceType: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.absenceType" javaScriptEscape="true"/>',
                reasonMissing: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.reasonMissing" javaScriptEscape="true"/>',
                childOfferedReturnInterview: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.childOfferedReturnInterview" javaScriptEscape="true"/>',
                childAcceptedReturnInterview: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.childAcceptedReturnInterview" javaScriptEscape="true"/>',
                absenceNote: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.absenceNote" javaScriptEscape="true"/>',
                absenceTypeMandatory: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.absenceTypeMandatory" javaScriptEscape="true"/>'
            },
            viewAbsenceFromPlacement: {
                startDate: '<s:message code="childlookedafter.absencefromplacement.dialog.view.startDate" javaScriptEscape="true"/>',
                endDate: '<s:message code="childlookedafter.absencefromplacement.dialog.view.endDate" javaScriptEscape="true"/>',
                absenceType: '<s:message code="childlookedafter.absencefromplacement.dialog.view.absenceType" javaScriptEscape="true"/>',
                absenceNote: '<s:message code="childlookedafter.absencefromplacement.dialog.view.absenceNote" javaScriptEscape="true"/>',
                reasonMissing: '<s:message code="childlookedafter.absencefromplacement.dialog.view.reasonMissing" javaScriptEscape="true"/>',
                childOfferedReturnInterview: '<s:message code="childlookedafter.absencefromplacement.dialog.view.childOfferedReturnInterview" javaScriptEscape="true"/>',
                childAcceptedReturnInterview: '<s:message code="childlookedafter.absencefromplacement.dialog.view.childAcceptedReturnInterview" javaScriptEscape="true"/>'
            },
            endPeriodOfCare: {
                endDate: '<s:message code="childlookedafter.periodofcare.dialog.end.endDate" javaScriptEscape="true"/>',
                endReason: '<s:message code="childlookedafter.periodofcare.dialog.end.endReason" javaScriptEscape="true"/>',
                endButton: '<s:message code="childlookedafter.periodofcare.dialog.end.button" javaScriptEscape="true"/>',
                adoptionWarning: '<s:message code="childlookedafter.periodofcare.dialog.end.adoptionWarning" javaScriptEscape="true"/>',
                ageWarning: '<s:message code="childlookedafter.periodofcare.dialog.end.ageWarning" javaScriptEscape="true"/>'
            },
            deletePeriodOfCare: {
                successMessage: '<s:message code="childlookedafter.periodofcare.dialog.delete.successMessage" javaScriptEscape="true"/>',
                deleteConfirm: '<s:message code="childlookedafter.periodofcare.dialog.delete.confirmDeleteMessage" javaScriptEscape="true"/>'
            },
            prepareDeletePeriodOfCare: {
                successMessage: '<s:message code="childlookedafter.periodofcare.dialog.delete.successMessage" javaScriptEscape="true"/>',
                deleteConfirm: '<s:message code="childlookedafter.periodofcare.dialog.delete.prepare.confirmDeleteMessage" javaScriptEscape="true"/>'
            },
            temporaryPlacement: {
                startDate: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.startDate" javaScriptEscape="true"/>',
                placementType: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.placementType" javaScriptEscape="true"/>',
                endDate: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.endDate" javaScriptEscape="true"/>',
                carer: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.carer" javaScriptEscape="true"/>',
                carerType: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.carerType" javaScriptEscape="true"/>',
                careOrganisation: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.careOrganisation" javaScriptEscape="true"/>',
                carerTypePerson: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.carertype.person" javaScriptEscape="true"/>',
                carerTypeOrganisation: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.carertype.organisation" javaScriptEscape="true"/>',
                placementProvision: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.placementProvision" javaScriptEscape="true"/>',
                typeOfCare: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.typeOfCare" javaScriptEscape="true"/>',
                hospitalDuration: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.warnings.hospitalDuration" javaScriptEscape="true"/>',
                otherDuration: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.warnings.otherDuration" javaScriptEscape="true"/>',
                overlapping: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.warnings.overlapping" javaScriptEscape="true"/>'
            },
            deleteTempPlacement: {
                deleteConfirm: '<s:message code="childlookedafter.temporaryPlacement.dialog.delete.confirmDeleteMessage" javaScriptEscape="true"/>'
            },
        };

        var periodOfCareViewsAddConfig = {
            fuzzyDateLabels: fuzzyDateLabels,
            url: '<s:url value="/rest/periodOfCare"/>',
            fosterCarerAutocompleteURL: '<s:url value="/rest/fosterCarerApproval/person?pageNumber=1&pageSize={maxResults}" />',
            personAutocompleteURL: '<s:url value="/rest/person?appendWildcard=true&nameOrId={query}"/>',
            adopterAutocompleteURL: '<s:url value="/rest/adoptiveCarerApproval/child?childId={childId}&pageNumber=1&pageSize={maxResults}&escapeSpecialCharacters=true&appendWildcard=true&useSoundex=true" />',
            adopterChildAutocompleteURL: '<s:url value="/rest/adoptiveCarerApproval/child?childId={childId}&pageNumber=1&pageSize={maxResults}&escapeSpecialCharacters=true&appendWildcard=true&useSoundex=true" />',
            organisationAutocompleteURL: '<s:url value="/rest/organisation?nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
            localAuthorityAutocompleteURL: '<s:url value="/rest/organisation?nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}&byType=true&types=LOCALAUTHORITY"/>',
            categoryOfNeedsURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CIN_CAT" />',
            legalStatusesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CLA_LGL_STAT" />',
            placementTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CLA_PLC" />',
            activeCategoryOfNeedURL: '<s:url value="/rest/classificationAssignment?subjectId=${person.id}&subjectType=PERSON&active=true&pageNumber=1&pageSize=-1"/>',
            latestPeriodOfCareURL: '<s:url value="/rest/periodOfCare/latest?personId=${person.id}"/>',
            typeOfCareURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval"/>',
            parentalRelationshipURL: '<s:url value="/rest/personPersonRelationship/keyRelationships?personId=${person.id}&relationshipAttributes=PARENTAL_RESPONSIBILITY&temporalStatusFilter=ACTIVE&pageNumber=-1&pageSize=-1" />',
            personAddressURL: '<s:url value="/rest/person/${person.id}/address/active?type=HOME"/>',
            locationUrl: '<s:url value="/rest/location"/>',
            locationQueryParamsUrl: '<s:url value="?nameOrNumber={nameOrNumber}&postcode={postcode}&pageSize={pageSize}"/>'
        };

        var periodOfCareViewsEndConfig = {
            url: '<s:url value="/rest/periodOfCare/{id}/status?action=ended"/>',
            latestPlacementURL: '<s:url value="/rest/placement/latest?periodOfCareId={id}&latest=true" />',
            getPersonURL: '<s:url value="/rest/person/${person.id}" />',
        };

        var periodOfCareViewsDeletePeriodConfig = {
            url: '<s:url value="/rest/deletionRequest?entityId={periodOfCareId}&entityType=PeriodOfCare" />',
            prepareUrl: '<s:url value="/rest/periodOfCare/{periodOfCareId}/prepareForDeletion" />',
            deleteUrl: '<s:url value="/rest/periodOfCare/{periodOfCareId}/deletionRequest/{deletionRequestId}" />'
        };

        var periodOfCareViewsPrepareDeletePeriodConfig = {
            url: '<s:url value="/rest/periodOfCare/{periodOfCareId}" />',
            prepareUrl: '<s:url value="/rest/periodOfCare/{periodOfCareId}/prepareForDeletion" />',
            deleteUrl: '<s:url value="/rest/periodOfCare/{periodOfCareId}/deletionRequest/{deletionRequestId}" />'
        };

        var episodeOfCareViewsAddConfig = {
            url: '<s:url value="/rest/episodeOfCare"/>',
            fosterCarerAutocompleteURL: '<s:url value="/rest/fosterCarerApproval/person?pageNumber=1&pageSize={maxResults}" />',
            personAutocompleteURL: '<s:url value="/rest/person?appendWildcard=true&nameOrId={query}"/>',
            adopterAutocompleteURL: '<s:url value="/rest/adoptiveCarerApproval/child?childId={childId}&pageNumber=1&pageSize={maxResults}&escapeSpecialCharacters=true&appendWildcard=true&useSoundex=true" />',
            adopterChildAutocompleteURL: '<s:url value="/rest/adoptiveCarerApproval/child?childId={childId}&pageNumber=1&pageSize={maxResults}&escapeSpecialCharacters=true&appendWildcard=true&useSoundex=true" />',
            organisationAutocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
            legalStatusesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CLA_LGL_STAT" />',
            placementTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CLA_PLC" />',
            latestEpisodeOfCareURL: '<s:url value="/rest/episodeOfCare/latest?periodOfCareId={periodOfCareId}" />',
            subject: subject,
            typeOfCareURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval"/>',
            typeOfCareByDateURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval?date={date}"/>',
            parentalRelationshipURL: '<s:url value="/rest/personPersonRelationship/keyRelationships?personId=${person.id}&relationshipAttributes=PARENTAL_RESPONSIBILITY&temporalStatusFilter=ACTIVE&pageNumber=-1&pageSize=-1" />',
            personAddressURL: '<s:url value="/rest/person/${person.id}/address/active?type=HOME"/>',
            supportsMultipleLegalStatusEntries: '${isSupportMutipleLegalStatusEntriesApplicable}' === 'true',
            locationUrl: '<s:url value="/rest/location"/>',
            locationQueryParamsUrl: '<s:url value="?nameOrNumber={nameOrNumber}&postcode={postcode}&pageSize={pageSize}"/>',
            personPlacementAddressUrl: '<s:url value="/rest/person/${person.id}/address/active?type=PLACEMENT&usage=PERMANENT"/>'
        };

        var episodeOfCareViewsAddAdmittance = {
            header: {
                text: '<s:message code="childlookedafter.episodeofcare.dialog.add.header" javaScriptEscape="true"/>',
                icon: 'fa eclipse-childlookedafter'
            },
            narrative: {
                summary: '<s:message code="childlookedafter.periodofcare.dialog.add.summary" javaScriptEscape="true" />',
                description: subjectNarrativeDescription
            },
            successMessage: '<s:message code="childlookedafter.periodofcare.dialog.add.successMessage" javaScriptEscape="true"/>',
            labels: {
                startDate: '<s:message code="childlookedafter.periodofcare.dialog.add.startDate" javaScriptEscape="true"/>',
                admissionReason: '<s:message code="childlookedafter.periodofcare.dialog.add.admissionReason" javaScriptEscape="true"/>',
                previouslyLookedAfter: '<s:message code="childlookedafter.periodofcare.dialog.add.previouslyLookedAfter" javaScriptEscape="true"/>',
                previousPermanenceOption: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceOption" javaScriptEscape="true"/>',
                previousPermanenceLocalAuthority: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceLocalAuthority" javaScriptEscape="true"/>',
                previousPermanenceRegion: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceRegion" javaScriptEscape="true"/>',
                categoryOfNeed: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeed" javaScriptEscape="true"/>',
                categoryOfNeedMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeedMandatory" javaScriptEscape="true"/>',
                categoryOfNeedInvalid: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeedInvalid" javaScriptEscape="true"/>',
                siblingGroup: '<s:message code="childlookedafter.periodofcare.dialog.add.siblingGroup" javaScriptEscape="true"/>',
                legalStatus: '<s:message code="childlookedafter.periodofcare.dialog.add.legalStatus" javaScriptEscape="true"/>',
                siblingsAssessed: '<s:message code="childlookedafter.periodofcare.dialog.add.siblingsAssessed" javaScriptEscape="true"/>',
                placementType: '<s:message code="childlookedafter.periodofcare.dialog.add.placementType" javaScriptEscape="true"/>',
                placementProvision: '<s:message code="childlookedafter.periodofcare.dialog.add.placementProvision" javaScriptEscape="true"/>',
                carerSubjectType: '<s:message code="childlookedafter.periodofcare.dialog.add.carerSubjectType" javaScriptEscape="true"/>',
                typeOfCare: '<s:message code="childlookedafter.periodofcare.dialog.add.typeOfCare" javaScriptEscape="true"/>',
                startDateMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.startDateMandatory" javaScriptEscape="true"/>',
                placementTypeMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.placementTypeMandatory" javaScriptEscape="true"/>',
                legalStatusMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.legalStatusMandatory" javaScriptEscape="true"/>',
                carerSubjectMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.carerSubjectMandatory" javaScriptEscape="true"/>',
                previousPermanenceOptionMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceOptionMandatory" javaScriptEscape="true"/>',
                previousPermanenceLAOrRegionMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.previousPermanenceLAOrRegionMandatory" javaScriptEscape="true"/>',
                categoryOfNeedMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.categoryOfNeedMandatory" javaScriptEscape="true"/>',
                placementProvisionMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.placementProvisionMandatory" javaScriptEscape="true"/>',
                typeOfCareMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.typeOfCareMandatory" javaScriptEscape="true"/>',
                admissionReasonMandatory: '<s:message code="childlookedafter.periodofcare.dialog.add.admissionReasonMandatory" javaScriptEscape="true"/>',
                startDateFormat: '<s:message code="childlookedafter.periodofcare.dialog.add.startDateFormat" javaScriptEscape="true"/>',
                parent: '<s:message code="childlookedafter.periodofcare.dialog.add.parent" javaScriptEscape="true"/>',
                address: '<s:message code="childlookedafter.periodofcare.dialog.add.address" javaScriptEscape="true"/>',
                warnings: {
                    noParentExists: '<s:message code="childlookedafter.periodofcare.dialog.add.warnings.noParentExists" javaScriptEscape="true"/>',
                    noActiveHomeAddress: '<s:message code="childlookedafter.periodofcare.dialog.add.warnings.noActiveHomeAddress" javaScriptEscape="true"/>'
                },
                location: '<s:message code="childlookedafter.dialog.add.location" javaScriptEscape="true"/>',
                addLocation: '<s:message code="childlookedafter.dialog.add.addLocation" javaScriptEscape="true"/>',
                postcode: '<s:message code="childlookedafter.dialog.add.postcode" javaScriptEscape="true"/>',
                houseNameOrNumber: '<s:message code="childlookedafter.dialog.add.houseNameOrNumber" javaScriptEscape="true"/>',
                roomDescription: '<s:message code="childlookedafter.dialog.add.roomDescription" javaScriptEscape="true"/>',
                floorDescription: '<s:message code="childlookedafter.dialog.add.floorDescription" javaScriptEscape="true"/>',
                overMaxResults: '<s:message code="location.overMaxResults" javaScriptEscape="true" />',
                addressNote: '<s:message code="location.addressNote" javaScriptEscape="true" />',
                addALocation: '<s:message code="location.addALocation" javaScriptEscape="true" />',
                noResults: '<s:message code="location.noResults" javaScriptEscape="true" />',
                locationMandatory: '<s:message code="AtLeastOneNotNull.newPersonAddressVO.locationId" javaScriptEscape="true" />',
            },
            config: {
                fuzzyDateLabels: fuzzyDateLabels,
                url: '<s:url value="/rest/episodeOfCare"/>',
                fosterCarerAutocompleteURL: '<s:url value="/rest/fosterCarerApproval/person?pageNumber=1&pageSize={maxResults}" />',
                personAutocompleteURL: '<s:url value="/rest/person?appendWildcard=true&nameOrId={query}"/>',
                adopterAutocompleteURL: '<s:url value="/rest/adoptiveCarerApproval/child?childId={childId}&pageNumber=1&pageSize={maxResults}&escapeSpecialCharacters=true&appendWildcard=true&useSoundex=true" />',
                adopterChildAutocompleteURL: '<s:url value="/rest/adoptiveCarerApproval/child?childId={childId}&pageNumber=1&pageSize={maxResults}&escapeSpecialCharacters=true&appendWildcard=true&useSoundex=true" />',
                organisationAutocompleteURL: '<s:url value="/rest/organisation?nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
                localAuthorityAutocompleteURL: '<s:url value="/rest/organisation?nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}&byType=true&types=LOCALAUTHORITY"/>',
                categoryOfNeedsURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CIN_CAT" />',
                legalStatusesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CLA_LGL_STAT" />',
                placementTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=CLA_PLC" />',
                activeCategoryOfNeedURL: '<s:url value="/rest/classificationAssignment?subjectId=${person.id}&subjectType=PERSON&active=true&pageNumber=1&pageSize=-1"/>',
                latestPeriodOfCareURL: '<s:url value="/rest/periodOfCare/latest?personId=${person.id}"/>',
                typeOfCareURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval"/>',
                parentalRelationshipURL: '<s:url value="/rest/personPersonRelationship/keyRelationships?personId=${person.id}&relationshipAttributes=PARENTAL_RESPONSIBILITY&temporalStatusFilter=ACTIVE&pageNumber=-1&pageSize=-1" />',
                personAddressURL: '<s:url value="/rest/person/${person.id}/address/active?type=HOME"/>',
                locationUrl: '<s:url value="/rest/location"/>',
                locationQueryParamsUrl: '<s:url value="?nameOrNumber={nameOrNumber}&postcode={postcode}&pageSize={pageSize}"/>'
            }
        };

        var episodeOfCareViewsViewConfig = {
            url: '<s:url value="/rest/episodeOfCare/{id}"/>',
            subject: subject
        };

        var episodeOfCareViewsViewDisplayProperties = {
            showAdmissionReason: showAdmissionReason,
            showPlacementProvision: showPlacementProvision,
            showPreviouslyLookedAfter: showPreviouslyLookedAfter,
            showSiblings: showSiblings,
            showPlannedEndOfPlacement: showPlannedEndOfPlacement,
            showEndLabel: showEndLabel
        };

        var episodeOfCareViewsEditConfig = {
            url: '<s:url value="/rest/episodeOfCare/{id}"/>',
            placementUrl: '<s:url value="/rest/placement/{id}/carer"/>',
            fosterCarerAutocompleteURL: '<s:url value="/rest/fosterCarerApproval/person?pageNumber=1&pageSize={maxResults}" />',
            adopterAutocompleteURL: '<s:url value="/rest/adoptiveCarerApproval/child?childId={childId}&pageNumber=1&pageSize={maxResults}&escapeSpecialCharacters=true&appendWildcard=true&useSoundex=true" />',            
            personAutocompleteURL: '<s:url value="/rest/person?appendWildcard=true&nameOrId={query}"/>',
            organisationAutocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
            typeOfCareURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval"/>',
            subject: subject
        };

        var episodeOfCareViewsEditDisplayProperties = {
            showAdmissionReason: showAdmissionReason,
            showPlacementProvision: showPlacementProvision,
            showPreviouslyLookedAfter: showPreviouslyLookedAfter,
            showSiblings: showSiblings,
            showPlannedEndOfPlacement: showPlannedEndOfPlacement,
            showEndLabel: showEndLabel
        };

        var episodeOfCareViewsDeleteEpisodeConfig = {
            url: '<s:url value="/rest/deletionRequest?entityId={episodeOfCareId}&entityType=EpisodeOfCare" />',
            prepareUrl: '<s:url value="/rest/episodeOfCare/{episodeOfCareId}/prepareForDeletion" />',
            deleteUrl: '<s:url value="/rest/episodeOfCare/{episodeOfCareId}/deletionRequest/{deletionRequestId}" />'
        };

        var episodeOfCareViewsPrepareDeleteEpisodeConfig = {
            url: '<s:url value="/rest/episodeOfCare/{episodeOfCareId}" />',
            prepareUrl: '<s:url value="/rest/episodeOfCare/{episodeOfCareId}/prepareForDeletion" />',
            deleteUrl: '<s:url value="/rest/episodeOfCare/{episodeOfCareId}/deletionRequest/{deletionRequestId}" />'
        };

        var episodeOfCareViewsViewFirstMigClaEpisode = {
            header: {
                text: '<s:message code="childlookedafter.episodeofcare.dialog.view.header" javaScriptEscape="true"/>',
                icon: 'fa eclipse-childlookedafter'
            },
            narrative: {
                summary: '<s:message code="childlookedafter.episodeofcare.dialog.view.summary" javaScriptEscape="true" />',
                description: subjectNarrativeDescription
            },
            labels: {
                reasonForAdmission: '<s:message code="childlookedafter.episodeofcare.dialog.migView.reasonForAdmission" javaScriptEscape="true"/>',
                providerType: '<s:message code="childlookedafter.episodeofcare.dialog.migView.providerType" javaScriptEscape="true"/>',
                previouslyLookedAfter: '<s:message code="childlookedafter.episodeofcare.dialog.migView.previouslyLookedAfter" javaScriptEscape="true"/>',
                localAuthority: '<s:message code="childlookedafter.episodeofcare.dialog.migView.localAuthorityName" javaScriptEscape="true"/>',
                date: '<s:message code="common.label.date" javaScriptEscape="true"/>'
            },
            width: 800,
            config: {
                subject: subject
            }
        };

        var episodeOfCareViewsViewOtherMigClaEpisode = {
            header: {
                text: '<s:message code="childlookedafter.episodeofcare.dialog.view.header" javaScriptEscape="true"/>',
                icon: 'fa eclipse-childlookedafter'
            },
            narrative: {
                summary: '<s:message code="childlookedafter.episodeofcare.dialog.view.summary" javaScriptEscape="true" />',
                description: subjectNarrativeDescription
            },
            labels: labels.viewEpisodeOfCare,
            width: 800,
            config: {
                subject: subject
            }
        };

        var scottishDialogConfig = {
            periodOfCare: {
                views: {
                    addScottish: {
                        header: {
                            text: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.add.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.add.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        successMessage: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.add.successMessage" javaScriptEscape="true"/>',
                        labels: labels.addPeriodOfCare,
                        config: periodOfCareViewsAddConfig
                    },
                    end: {
                        header: {
                            text: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.end.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.end.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        successMessage: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.end.successMessage" javaScriptEscape="true"/>',
                        labels: labels.endPeriodOfCare,
                        config: periodOfCareViewsEndConfig
                    },
                    deletePeriod: {
                        header: {
                            text: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.delete.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.delete.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.deletePeriodOfCare,
                        config: periodOfCareViewsDeletePeriodConfig
                    },
                    deleteInProgress: {
                        header: {
                            text: '<s:message code="childlookedafter.periodofcare.dialog.scottishCLA.delete.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        labels: {
                            message: '<s:message code="childlookedafter.periodofcare.dialog.delete.inProgress" javaScriptEscape="true" />',
                        }
                    },
                    prepareDeletePeriod: {
                        header: {
                            text: '<s:message code="childlookedafter.periodofcare.dialog.delete.scottishCLA.prepare.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.periodofcare.dialog.delete.scottishCLA.prepare.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.prepareDeletePeriodOfCare,
                        config: periodOfCareViewsPrepareDeletePeriodConfig
                    }
                },
                personDetailsModelURL: '<s:url value="/rest/person/${person.id}"/>',
                latestEpisodeOfCareURL: '<s:url value="/rest/episodeOfCare/latest?periodOfCareId={periodOfCareId}" />',
            },
            episodeOfCare: {
                views: {
                    add: {
                        header: {
                            text: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.add.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.add.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        successMessage: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.add.successMessage" javaScriptEscape="true"/>',
                        labels: labels.addEpisodeOfCare,
                        config: episodeOfCareViewsAddConfig
                    },
                    addAdmittance: episodeOfCareViewsAddAdmittance,
                    view: {
                        header: {
                            text: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.view.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.view.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.viewEpisodeOfCare,
                        config: episodeOfCareViewsViewConfig,
                        displayProperties: episodeOfCareViewsViewDisplayProperties
                    },
                    edit: {
                        header: {
                            text: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.edit.header" javaScriptEscape="true"/>',
                            icon: 'fa eclipse-childlookedafter'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.edit.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.editEpisodeOfCare,
                        config: episodeOfCareViewsEditConfig,
                        displayProperties: episodeOfCareViewsEditDisplayProperties
                    },
                    deleteEpisode: {
                        header: {
                            text: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.delete.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.delete.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.deleteEpisodeOfCare,
                        config: episodeOfCareViewsDeleteEpisodeConfig
                    },
                    deleteLastEpisodeWarning: {
                        header: {
                            text: '<s:message code="childlookedafter.episodeofcare.dialog.delete.lastepisode.scottishCLA.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        labels: {
                            message: '<s:message code="childlookedafter.episodeofcare.dialog.delete.lastepisode.scottishCLA.warning" javaScriptEscape="true" />'
                        }
                    },
                    deleteInProgress: {
                        header: {
                            text: '<s:message code="childlookedafter.episodeofcare.dialog.scottishCLA.delete.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        labels: {
                            message: '<s:message code="childlookedafter.episodeofcare.dialog.delete.inProgress" javaScriptEscape="true" />',
                        }
                    },
                    prepareDeleteEpisode: {
                        header: {
                            text: '<s:message code="childlookedafter.episodeofcare.dialog.delete.scottishCLA.prepare.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.episodeofcare.dialog.delete.scottishCLA.prepare.summary" javaScriptEscape="true" />',
                            description: subjectNarrativeDescription
                        },
                        labels: labels.prepareDeleteEpisodeOfCare,
                        config: episodeOfCareViewsPrepareDeleteEpisodeConfig
                    },
                    viewFirstMigClaEpisode: episodeOfCareViewsViewFirstMigClaEpisode,
                    viewOtherMigClaEpisode: episodeOfCareViewsViewOtherMigClaEpisode
                }
            }
        };

        new Y.app.childlookedafter.ChildLookedAfterControllerView({
            container: '#childLookedAfterResults',
            labels: labels,
            permissions: permissions,
            outputDialogConfig: outputDialogConfig,
            subject: subject,
            scottishLabels: scottishLabels,
            scottishDialogConfig: scottishDialogConfig,
            showScottishView: '${isShowScottishView}'==='true',
            showWelshView: '${isShowWelshView}' ==='true',
            url: '<s:url value="/rest/person/{subjectId}/periodOfCare?pageNumber={page}&pageSize={pageSize}&s={sortBy}"><s:param name="sortBy" value="[{\"startDate\":\"desc\"}]" /></s:url>',
            migClaPeriodUrl: '<s:url value="/rest/person/{subjectId}/migClaPeriod?pageNumber={page}&pageSize={pageSize}&s={sortBy}"><s:param name="sortBy" value="[{\"startDate\":\"desc\"}]" /></s:url>',
            pictureUrl: '<s:url value="/rest/{subjectType}/{subjectId}/picture/content"/>',
            dialogConfig: {
                periodOfCare: {
                    views: {
                        add: {
                            header: {
                                text: '<s:message code="childlookedafter.periodofcare.dialog.add.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.periodofcare.dialog.add.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.periodofcare.dialog.add.successMessage" javaScriptEscape="true"/>',
                            labels: labels.addPeriodOfCare,
                            config: periodOfCareViewsAddConfig
                        },
                        addWelsh: {
                            header: {
                                text: '<s:message code="childlookedafter.periodofcare.dialog.add.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.periodofcare.dialog.add.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.periodofcare.dialog.add.successMessage" javaScriptEscape="true"/>',
                            labels: labels.addPeriodOfCare,
                            config: periodOfCareViewsAddConfig
                        },
                        end: {
                            header: {
                                text: '<s:message code="childlookedafter.periodofcare.dialog.end.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.periodofcare.dialog.end.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.periodofcare.dialog.end.successMessage" javaScriptEscape="true"/>',
                            labels: labels.endPeriodOfCare,
                            config: periodOfCareViewsEndConfig
                        },
                        deletePeriod: {
                            header: {
                                text: '<s:message code="childlookedafter.periodofcare.dialog.delete.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.periodofcare.dialog.delete.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.deletePeriodOfCare,
                            config: periodOfCareViewsDeletePeriodConfig
                        },
                        deleteInProgress: {
                            header: {
                                text: '<s:message code="childlookedafter.periodofcare.dialog.delete.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            labels: {
                                message: '<s:message code="childlookedafter.periodofcare.dialog.delete.inProgress" javaScriptEscape="true" />',
                            }
                        },
                        prepareDeletePeriod: {
                            header: {
                                text: '<s:message code="childlookedafter.periodofcare.dialog.delete.prepare.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.periodofcare.dialog.delete.prepare.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.prepareDeletePeriodOfCare,
                            config: periodOfCareViewsPrepareDeletePeriodConfig
                        }
                    },
                    personDetailsModelURL: '<s:url value="/rest/person/${person.id}"/>',
                    latestEpisodeOfCareURL: '<s:url value="/rest/episodeOfCare/latest?periodOfCareId={periodOfCareId}" />',
                },
                episodeOfCare: {
                    views: {
                        add: {
                            header: {
                                text: '<s:message code="childlookedafter.episodeofcare.dialog.add.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.episodeofcare.dialog.add.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.episodeofcare.dialog.add.successMessage" javaScriptEscape="true"/>',
                            labels: labels.addEpisodeOfCare,
                            config: episodeOfCareViewsAddConfig
                        },
                        addAdmittance: episodeOfCareViewsAddAdmittance,
                        view: {
                            header: {
                                text: '<s:message code="childlookedafter.episodeofcare.dialog.view.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.episodeofcare.dialog.view.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.viewEpisodeOfCare,
                            config: episodeOfCareViewsViewConfig,
                            displayProperties: episodeOfCareViewsViewDisplayProperties
                        },
                        edit: {
                            header: {
                                text: '<s:message code="childlookedafter.episodeofcare.dialog.edit.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.episodeofcare.dialog.edit.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.editEpisodeOfCare,
                            config: episodeOfCareViewsEditConfig,
                            displayProperties: episodeOfCareViewsEditDisplayProperties
                        },
                        deleteEpisode: {
                            header: {
                                text: '<s:message code="childlookedafter.episodeofcare.dialog.delete.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.episodeofcare.dialog.delete.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.deleteEpisodeOfCare,
                            config: episodeOfCareViewsDeleteEpisodeConfig
                        },
                        deleteLastEpisodeWarning: {
                            header: {
                                text: '<s:message code="childlookedafter.episodeofcare.dialog.delete.lastepisode.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            labels: {
                                message: '<s:message code="childlookedafter.episodeofcare.dialog.delete.lastepisode.warning" javaScriptEscape="true" />'
                            }
                        },
                        deleteInProgress: {
                            header: {
                                text: '<s:message code="childlookedafter.episodeofcare.dialog.delete.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            labels: {
                                message: '<s:message code="childlookedafter.episodeofcare.dialog.delete.inProgress" javaScriptEscape="true" />',
                            }
                        },
                        prepareDeleteEpisode: {
                            header: {
                                text: '<s:message code="childlookedafter.episodeofcare.dialog.delete.prepare.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.episodeofcare.dialog.delete.prepare.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.prepareDeleteEpisodeOfCare,
                            config: episodeOfCareViewsPrepareDeleteEpisodeConfig
                        },
                        viewFirstMigClaEpisode: episodeOfCareViewsViewFirstMigClaEpisode,
                        viewOtherMigClaEpisode: episodeOfCareViewsViewOtherMigClaEpisode
                    }
                },
                absenceFromPlacement: {
                    views: {
                        add: {
                            header: {
                                text: '<s:message code="childlookedafter.absencefromplacement.dialog.add.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.absencefromplacement.dialog.add.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.absencefromplacement.dialog.add.successMessage" javaScriptEscape="true"/>',
                            labels: labels.addAbsenceFromPlacement,
                            config: {
                                url: '<s:url value="/rest/absenceFromPlacement"/>',
                                absenceTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=MISSING_CHILD" />'
                            }
                        },
                        edit: {
                            header: {
                                text: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.successMessage" javaScriptEscape="true"/>',
                            labels: labels.editAbsenceFromPlacement,
                            config: {
                                url: '<s:url value="/rest/absenceFromPlacement/{id}"/>',
                                absenceTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=MISSING_CHILD" />'
                            }
                        },
                        view: {
                            header: {
                                text: '<s:message code="childlookedafter.absencefromplacement.dialog.edit.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.absencefromplacement.dialog.view.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.viewAbsenceFromPlacement,
                            config: {
                                url: '<s:url value="/rest/absenceFromPlacement/{id}"/>',
                                absenceTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=MISSING_CHILD" />'
                            }
                        },
                        archive: {
                            header: {
                                text: '<s:message code="childlookedafter.absencefromplacement.dialog.archive.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.absencefromplacement.dialog.archive.summary" javaScriptEscape="true"/>',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.absencefromplacement.delete.success.message" javaScriptEscape="true"/>',
                            config: {
                                url: '<s:url value="/rest/absenceFromPlacement/{id}/status?action=archived"/>'
                            },
                            labels: {
                                absenceDeleteConfirm: '<s:message code="childlookedafter.absencefromplacement.deleteConfirm" javaScriptEscape="true"/>'
                            }
                        }
                    }
                },
                temporaryPlacement: {
                    views: {
                        add: {
                            header: {
                                text: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.successMessage" javaScriptEscape="true"/>',
                            labels: labels.temporaryPlacement,
                            validationLabels: {
                                startDateMandatory: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.startDateMandatory" javaScriptEscape="true"/>',
                                placementTypeMandatory: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.placementTypeMandatory" javaScriptEscape="true"/>',
                                placementProvisionMandatory: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.placementProvisionMandatory" javaScriptEscape="true"/>',
                                carerMandatory: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.carerMandatory" javaScriptEscape="true"/>'
                            },
                            config: {
                                url: '<s:url value="/rest/placement" />',
                                latestEpisodeOfCareURL: '<s:url value="/rest/episodeOfCare/latest?periodOfCareId={periodOfCareId}" />',
                                temporaryPlacementTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=TMP_PLC" />',
                                carerAutocompleteURL: '<s:url value="/rest/fosterCarerApproval/person?pageNumber=1&pageSize={maxResults}" />',
                                personAutocompleteURL: '<s:url value="/rest/person?pageNumber=1&pageSize={maxResults}&nameOrId={query}" />',
                                organisationAutocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
                                typeOfCareURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval"/>',
                                fosterCarerHolidaysURL: '<s:url value="/rest/episodeOfCare/{episodeOfCareId}/fosterCarerHoliday/fosterCarerHolidayTotals?startDate={placementStartDate}&endDate={placementEndDate}"/>'
                            },
                            displayProperties: {
                                showProviderTypeField: showProviderTypeField
                            }
                        },
                        edit: {
                            header: {
                                text: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.temporaryPlacement.dialog.edit.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.temporaryPlacement.dialog.edit.successMessage" javaScriptEscape="true"/>',
                            labels: labels.temporaryPlacement,
                            width: 800,
                            config: {
                                url: '<s:url value="/rest/placement/{id}" />',
                                updateUrl: '<s:url value="/rest/placement/{id}/status?action=close" />',
                                latestEpisodeOfCareURL: '<s:url value="/rest/episodeOfCare/latest?periodOfCareId={periodOfCareId}" />',
                                temporaryPlacementTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=TMP_PLC" />',
                                fosterCarerHolidaysURL: '<s:url value="/rest/episodeOfCare/{episodeOfCareId}/fosterCarerHoliday/fosterCarerHolidayTotals?startDate={placementStartDate}&endDate={placementEndDate}"/>',
                                typeOfCareURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval"/>'
                            },
                            displayProperties: {
                                showProviderTypeField: showProviderTypeField
                            }
                        },
                        view: {
                            header: {
                                text: '<s:message code="childlookedafter.temporaryPlacement.dialog.add.header" javaScriptEscape="true"/>',
                                icon: 'fa eclipse-childlookedafter'
                            },
                            narrative: {
                                summary: 'You are viewing a temporary placement',
                                description: subjectNarrativeDescription
                            },
                            labels: labels.temporaryPlacement,
                            width: 800,
                            config: {
                                temporaryPlacementTypesURL: '<s:url value="/rest/classification/classifications?status=active&classificationGroupCode=TMP_PLC" />',
                                typeOfCareURL: '<s:url value="/rest/person/{subjectId}/fosterCarerApproval"/>'
                            },
                            displayProperties: {
                                showProviderTypeField: showProviderTypeField
                            }
                        },
                        deleteTempPlacement: {
                            header: {
                                text: '<s:message code="childlookedafter.temporaryPlacement.dialog.delete.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-wrench'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.temporaryPlacement.dialog.delete.summary" javaScriptEscape="true" />',
                                description: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.temporaryPlacement.dialog.delete.successMessage" javaScriptEscape="true"/>',
                            labels: labels.deleteTempPlacement,
                            config: {
                                url: '<s:url value="/rest/placement/{tempPlacementId}" />',
                            }
                        }
                    }
                },
                location: {
                    views: {
                        add: {
                            header: {
                                text: '<s:message code="childlookedafter.dialog.add.location.header" javaScriptEscape="true"/>',
                                icon: 'fa fa-envelope fa-inverse fa-stack-1x'
                            },
                            narrative: {
                                summary: '<s:message code="childlookedafter.dialog.add.location.narrative" javaScriptEscape="true" />',
                                narrative: subjectNarrativeDescription
                            },
                            successMessage: '<s:message code="childlookedafter.dialog.add.location.successMessage" javaScriptEscape="true" />',
                            labels: {
                                primaryNameOrNumber: '<s:message code="location.primaryNameOrNumber" javaScriptEscape="true" />',
                                street: '<s:message code="location.street" javaScriptEscape="true" />',
                                locality: '<s:message code="location.locality" javaScriptEscape="true" />',
                                town: '<s:message code="location.town" javaScriptEscape="true" />',
                                county: '<s:message code="location.county" javaScriptEscape="true" />',
                                country: '<s:message code="location.country" javaScriptEscape="true" />',
                                countrySubdivision: '<s:message code="location.countrySubdivision" javaScriptEscape="true" />',
                                postcode: '<s:message code="location.postcode" javaScriptEscape="true" />',
                            },
                            config: {
                                url: '<s:url value="/rest/location"/>'
                            }
                        }
                    }
                }
            }
        }).render();
    });
</script>