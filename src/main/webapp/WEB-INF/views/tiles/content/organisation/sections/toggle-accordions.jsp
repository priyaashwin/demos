<%@page contentType="text/html;charset=UTF-8"%>

<div id="scrollbar-menu">
    <div id="collapse-button" class="hide-on-search"></div>
</div>
    
<script>
    Y.use('yui-base', 'app-button-model', 'app-button-trigger', 'template-toggle-expand-collapse', function(Y){

      var btnExpandCollapseAll = new Y.app.views.ButtonTrigger({            
    	  template: Y.app.templates.TOGGLE_EXPAND_COLLAPSE,              
    	  container:'#collapse-button',            
    	  allowToggle: true,            
    	  activeClass: 'closed',             
    	  isActive: true,             
    	  model: new Y.app.ButtonModel({                                                                                                                                                                    
    		  cssButton: 'pure-button expand-collapse-all',                                                                  
    		  cssIconOpen: 'fa fa-chevron-circle-up',               
    		  cssIconClosed: 'fa fa-chevron-circle-down',                                
    		  title: 'Open all accordions'              
    	  })                                         
      });
         
         
      btnExpandCollapseAll.render();

   });
</script>