<%@ page import="java.util.Locale" %>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<script>

  Y.use('yui-base',
      'event-custom',
      'reference-number-dialog-views',
      'usp-organisation-NewOrganisationReferenceNumber',
      'usp-organisation-OrganisationReferenceNumber',
      'usp-organisation-UpdateOrganisationReferenceNumber',
    
  function(Y) {

    var commonLabels = {       
          		       
    		number:'<s:message code="person.referenceNumber.number"/>',
    		referenceNumberType: '<s:message code="person.referenceNumber.referenceNumberType"/>', 
    		referenceNumberAddSuccessMessage: '<s:message code="organisation.referenceNumber.referenceNumberAddSuccessMessage"/>',
    		editReferenceNumberNarrative: '<s:message code="organisation.referenceNumber.edit.narrative" javaScriptEscape="true"/>',
    		//Edit Success message
    		referenceNumberSingleEditSuccessMessage:'<s:message code="organisation.referenceNumber.single.edit.success.message" javaScriptEscape="true"/>',
	    	referenceNumberSingleRemoveSuccessMessage:'<s:message code="organisation.referenceNumber.single.remove.success.message" javaScriptEscape="true"/>',
	    	referenceNumberMultipleEditSuccessMessage:'<s:message code="organisation.referenceNumber.multiple.edit.success.message" javaScriptEscape="true"/>',
	    	referenceNumberMultipleRemoveSuccessMessage:'<s:message code="organisation.referenceNumber.multiple.remove.success.message" javaScriptEscape="true"/>',
	    	referenceNumberMultipleEditRemoveSuccessMessage:'<s:message code="person.referenceNumber.multiple.edit.remove.success.message" javaScriptEscape="true"/>',
    		
    		//messages
        msgAddOnSuccess: '<s:message code="person.referenceNumber.add.success.message" javaScriptEscape="true"/>',
        
        //narrative
        narrativeSummary: '<s:message code="person.referenceNumber.add.narrative" javaScriptEscape="true"/>',
        narrativeDescription: '',
        
    };

    var referenceNumberDialog = new Y.app.referenceNumber.MultiPanelPopUp();
    
    referenceNumberDialog.render();
    
    var AddModel = Y.Base.create("addReferenceNumberModel", Y.usp.organisation.NewOrganisationReferenceNumber, [Y.usp.ModelFormLink], { 
        form: '#referenceNumberAddForm',
        url: '<s:url value="/rest/organisation/${organisation.id}/referenceNumber"/>'
    });
    
    var EditModel = Y.Base.create("editReferenceNumberModel",Y.usp.organisation.PaginatedOrganisationReferenceNumberList, [Y.usp.ModelFormLink], { 
        form: '#updateRemoveReferenceNumberForm',
        url: '<s:url value="/rest/organisation/{id}/referenceNumber?s=type:asc&pageSize=100"/>'
    });
    
    
    Y.on('referenceNumber:showDialog', function(e){
      
      switch(e.action){     
      
      case 'addReferenceNumber':       
        this.showView('addReferenceNumber', {
          model: new AddModel(),
          contextId: e.contextId,
          contextName: e.contextName,
          contextType: e.contextType,         
          labels: commonLabels,
          activeReferenceNumbers: e.activeReferenceNumbers
        });    
        break;
      
      case 'editRemoveReferenceNumber':                   
        this.showView('editRemoveReferenceNumber', {
          model: new EditModel(),
          contextId: e.contextId,               
          contextName: e.contextName,               
          contextType: e.contextType,
          labels: commonLabels,
          activeReferenceNumbers: e.activeReferenceNumbers,
          url:'<s:url value="/rest/organisationReferenceNumber/invoke?action=combinedUpdateDelete"/>'
        },{
			modelLoad:true, 
			payload:{
				id:e.id
				}
		},function(){   		        		
     		// FIXME - This is a temporary solution, as we are using a paginated list here
     		// and there is no 'change' event associated with ModelList to call the PersonReferenceNumbers view render
			this.get('activeView').render();
			//Get the button by name out of the footer and enable it.
     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);

		});                
        break; 
        
      };

      
    }, referenceNumberDialog);   
    
  });
         


</script>