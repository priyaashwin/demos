<%@ page import="java.util.Locale" %>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

    <script>
    Y.use('yui-base',
        'event-custom',
        'address-dialog-views',
        'model-transmogrify',
        'usp-organisation-NewOrganisationAddress',
        'usp-organisation-OrganisationAddress',
        'usp-organisation-UpdateOrganisationAddressStartEnd',
        'handlebars-address-templates',
        'usp-relationship-ResidentDetailsAndAddresses',
        'usp-organisation-UpdateOrganisationAddress',
        'usp-person-UpdatePersonAddressResidents',

        function (Y) {

            var commonLabels = {
                name: '<s:message code="organisation.add.orgName"/>',
                type: '<s:message code="organisation.add.type"/>',
                subType: '<s:message code="organisation.add.subType"/>',
                addAddressHeader: '<s:message code="organisation.address.add.header"/>',
                endAddressHeader: '<s:message code="organisation.address.end.header"/>',
                addOrganisationMessage: '<s:message code="organisation.add.narrative" javaScriptEscape="true"/>',
                addOrganisationSuccessMessage: '<s:message code="organisation.add.success.message" javaScriptEscape="true"/>',
                location: '<s:message code="organisation.address.end.location" javaScriptEscape="true"/>',
                roomDescription: '<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>',
                floorDescription: '<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
                usage: '<s:message code="person.address.add.usage" javaScriptEscape="true"/>',
                addressStartDate: '<s:message code="person.address.add.startDate" javaScriptEscape="true"/>',
                addressEndDate: '<s:message code="organisation.address.end.endDate" javaScriptEscape="true"/>',
                endReason: '<s:message code="organisation.address.end.endReason" javaScriptEscape="true"/>',
                endAddressEndConfirm: '<s:message code="person.address.end.endConfirm" javaScriptEscape="true"/>',
                primaryNameOrNumber: '<s:message code="location.primaryNameOrNumber" javaScriptEscape="true"/>',
                street: '<s:message code="location.street" javaScriptEscape="true"/>',
                locality: '<s:message code="location.locality" javaScriptEscape="true"/>',
                town: '<s:message code="location.town" javaScriptEscape="true"/>',
                county: '<s:message code="location.county" javaScriptEscape="true"/>',
                country: '<s:message code="location.country" javaScriptEscape="true"/>',
                countrySubdivision: '<s:message code="location.countrySubdivision" javaScriptEscape="true"/>',
                postcode: '<s:message code="location.postcode" javaScriptEscape="true"/>',
                removeAddressConfirm: '<s:message code="person.address.remove.removeConfirm" javaScriptEscape="true"/>',

                reopenAddressNarrative: '<s:message code="person.address.reopen.narrativeSummary" javaScriptEscape="true"/>',
                reopenAddressConfirm: '<s:message code="person.address.reopen.reopenConfirm" javaScriptEscape="true"/>',
                updateAddressDatesNarrative: '<s:message code="person.address.date.update.narrativeSummary" javaScriptEscape="true"/>',
                updateAddressDatesConfirm: '<s:message code="person.address.date.updateDateConfirm" javaScriptEscape="true"/>',
                updateAddressDates: '<s:message code="person.address.find.results.updateAddressStartEnd" javaScriptEscape="true"/>',
                updateAddressReopen: '<s:message code="person.address.find.results.updateAddressReopen" javaScriptEscape="true"/>',

                //messages
                msgAddAddressOnSuccess: '<s:message code="address.messages.add.success" javaScriptEscape="true"/>',
                msgEndAddressOnSuccess: '<s:message code="address.messages.end.success" javaScriptEscape="true"/>',
                msgRemoveAddressOnSuccess: '<s:message code="address.messages.remove.success" javaScriptEscape="true"/>',
                updateAddressDatesSuccessMessage: '<s:message code="person.address.date.update.success.message" javaScriptEscape="true"/>',
                reopenAddressSuccessMessage: '<s:message code="person.address.reopen.success.message" javaScriptEscape="true"/>',
                msgUpdateAddressOnSuccess: '<s:message code="address.messages.add.success" javaScriptEscape="true"/>',
                location: '<s:message code="childlookedafter.dialog.add.location" javaScriptEscape="true"/>',
                addLocation: '<s:message code="childlookedafter.dialog.add.addLocation" javaScriptEscape="true"/>',
                postcode: '<s:message code="childlookedafter.dialog.add.postcode" javaScriptEscape="true"/>',
                houseNameOrNumber: '<s:message code="childlookedafter.dialog.add.houseNameOrNumber" javaScriptEscape="true"/>',
                roomDescription: '<s:message code="childlookedafter.dialog.add.roomDescription" javaScriptEscape="true"/>',
                floorDescription: '<s:message code="childlookedafter.dialog.add.floorDescription" javaScriptEscape="true"/>',
                overMaxResults: '<s:message code="location.overMaxResults" javaScriptEscape="true" />',
                addressNote: '<s:message code="location.addressNote" javaScriptEscape="true" />',
                addALocation: '<s:message code="location.addALocation" javaScriptEscape="true" />',
                noResults: '<s:message code="location.noResults" javaScriptEscape="true" />',
                locationMandatory: '<s:message code="AtLeastOneNotNull.newPersonAddressVO.locationId" javaScriptEscape="true" />',
                updateAddressLocationNarrative: '<s:message code="person.address.location.update.narrativeSummary" javaScriptEscape="true"/>',
                updateAddressLocationConfirm: '<s:message code="person.address.location.update.confirm" javaScriptEscape="true"/>'
            };
            urls = {
                getUrl: '<s:url value="/rest/organisationAddress/{id}"/>',
                reopenUrl: '<s:url value="/rest/organisationAddress/{id}/reopen"/>',
                updateStartEndUrl: '<s:url value="/rest/organisationAddress/{id}/startAndEndDate"/>'
            };
            var subjectNarrativeDescription = '{{this.organisation.name}} ({{this.organisation.organisationIdentifier}})';

            var locationConfig = {
                views: {
                    add: {
                        header: {
                            text: '<s:message code="childlookedafter.dialog.add.location.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-envelope fa-inverse fa-stack-1x'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.dialog.add.location.narrative" javaScriptEscape="true" />',
                            narrative: this.subjectNarrativeDescription
                        },
                        successMessage: '<s:message code="childlookedafter.dialog.add.location.successMessage" javaScriptEscape="true" />',
                        labels: {
                            primaryNameOrNumber: '<s:message code="location.primaryNameOrNumber" javaScriptEscape="true" />',
                            street: '<s:message code="location.street" javaScriptEscape="true" />',
                            locality: '<s:message code="location.locality" javaScriptEscape="true" />',
                            town: '<s:message code="location.town" javaScriptEscape="true" />',
                            county: '<s:message code="location.county" javaScriptEscape="true" />',
                            country: '<s:message code="location.country" javaScriptEscape="true" />',
                            countrySubdivision: '<s:message code="location.countrySubdivision" javaScriptEscape="true" />',
                            postcode: '<s:message code="location.postcode" javaScriptEscape="true" />',
                        },
                        config: {
                            url: '<s:url value="/rest/location"/>'
                        }
                    }
                }
            };

            var addressConfig = {
                views: {
                    update: {
                        config: {
                            residentUrl: '<s:url value="/rest/personOrganisationRelationship/sharedLocationResidents?organisationId={orgId}&locationId={locationId}&organisationAddressId={orgAddressId}"/>',
                            updateAddressUrl: '<s:url value="/rest/organisationAddress/{id}?allowOverlappingAddresses=true"/>',
                            locationUrl: '<s:url value="/rest/location"/>',
                            locationQueryParamsUrl: '<s:url value="?nameOrNumber={nameOrNumber}&postcode={postcode}&pageSize={pageSize}"/>'
                        }
                    },
                    updateResident: {
                        labels: {
                            residentsCloseAddSuccessMessage: '<s:message code="person.address.move.success.message" javaScriptEscape="true"/>',
                            personName: '<s:message code="person.address.move.personName" javaScriptEscape="true"/>',
                            relationshipName: '<s:message code="person.address.move.relationshipName" javaScriptEscape="true"/>',
                            update: '<s:message code="person.address.move.update" javaScriptEscape="true"/>',
                            narrativeSummaryResidents: '<s:message code="address.location.update.narrativeSummaryResidents" javaScriptEscape="true"/>'
                        },
                        config: {
                            addressUrl: '<s:url value="/rest/personAddress/{id}/residents"/>'
                        }
                    }
                }
            }

            var myAddressDialog = new Y.app.address.MultiPanelPopUp();

            myAddressDialog.render();

            var AddAddressModel = Y.Base.create("addAddressModel", Y.usp.organisation.NewOrganisationAddress, [Y.usp.ModelFormLink], {
                form: '#addAddressForm',
                url: '<s:url value="/rest/organisation/${organisation.id}/address "/>'
            });

            var EndAddressModel = Y.Base.create("endAddressModel", Y.usp.organisation.OrganisationAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
                form: '#endPersonAddressForm',
                url: '<s:url value="/rest/organisationAddress/{id}"/>',
                getURL: function (action, options) {
                    if (action === 'update') {
                        return this._substituteURL('<s:url value="/rest/organisationAddress/{id}/status?action=close&checkInactive=true"/>', Y.merge(this.getAttrs(), options));
                    }
                    //return standard operation
                    return EndAddressModel.superclass.getURL.apply(this, [action, options]);
                }
            });

            var RemoveAddressModel = Y.Base.create("removeAddressModel", Y.usp.organisation.OrganisationAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
                url: '<s:url value="/rest/organisationAddress/{id}"/>',
                template: Y.Handlebars.templates["removePersonAddress"],
                getURL: function (action, options) {

                    if (action === 'delete') {
                        return this._substituteURL('<s:url value="/rest/organisationAddress/{id}"/>', Y.merge(this.getAttrs(), options));
                    }

                    //return standard operation
                    return RemoveAddressModel.superclass.getURL.apply(this, [action, options]);
                }
            });

            var UpdateStartEndOrganisationAddressForm = Y.Base.create("updateStartEndOrganisationAddressForm", Y.usp.organisation.OrganisationAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
                form: '#updateOrganisationAddressDatesForm'
            });

            var ReopenOrganisationAddressForm = Y.Base.create("reopenOrganisationAddressForm", Y.usp.organisation.OrganisationAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
                form: '#reopenOrganisationAddressForm'
            });

            var UpdateOrganisationLocationForm = Y.Base.create("updateOrganisationLocationForm", Y.usp.organisation.OrganisationAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], {
                form: '#updateAddressLocationForm',
                url: '<s:url value="/rest/organisationAddress/{id}"/>',
                customValidation: function (attrs) {
                    var errors = {};
                    if (this.isEmpty(attrs.locationId)) {
                        errors.locationId = 'Location is mandatory.';
                    }
                    return errors;
                },
                isEmpty: function (v) {
                    return v === null || v === '';
                }
            });

            Y.on('address:showDialog', function (e) {

                switch (e.action) {
                case 'addAddress':
                    this.showView('addAddressForm', {
                        model: new AddAddressModel(),
                        contextId: e.contextId,
                        contextName: e.contextName,
                        contextType: e.contextType,
                        labels: commonLabels,
                        panel: myAddressDialog,
                        locationUrl: '<s:url value="/rest/location"/>'
                    });
                    break;

                case 'endAddress':
                    this.showView('endAddressForm', {
                        model: new EndAddressModel(),
                        contextId: e.contextId,
                        contextName: e.contextName,
                        contextType: e.contextType,
                        labels: commonLabels
                    }, { modelLoad: true, payload: { id: e.addressId } });
                    break;

                case 'removeAddress':
                    this.showView('removeAddressForm', {
                        model: new RemoveAddressModel(),
                        contextId: e.contextId,
                        contextName: e.contextName,
                        contextType: e.contextType,
                        labels: commonLabels
                    }, { modelLoad: true, payload: { id: e.addressId } });
                    break;

                case 'updateStartEndAddressDates':
                    this.showView('updateStartEndAddressDatesForm', {
                        model: new UpdateStartEndOrganisationAddressForm({
                            url: Y.Lang.sub(urls.getUrl, {
                                id: e.addressId
                            })
                        }),
                        contextId: e.contextId,
                        contextName: e.contextName,
                        contextType: e.contextType,
                        labels: commonLabels,
                        urls: urls
                    }, {
                        modelLoad: true
                    }, function () {
                        //Get the button by name out of the footer and enable it.
                        this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
                    break;

                case 'updateAddressReopen':
                    this.showView('reopenAddressForm', {
                        model: new ReopenOrganisationAddressForm({
                            url: Y.Lang.sub(urls.getUrl, {
                                id: e.addressId
                            })
                        }),
                        contextId: e.contextId,
                        contextName: e.contextName,
                        contextType: e.contextType,
                        labels: commonLabels,
                        urls: urls
                    }, {
                        modelLoad: true
                    }, function () {
                        //Get the button by name out of the footer and enable it.
                        this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                    });
                    break;

                case 'moveAddressLocation':
                    this.showView('moveAddressLocation', {
                        model: new UpdateOrganisationLocationForm(),
                        transmogrify: Y.usp.organisation.UpdateOrganisationAddress,
                        targetSubjectId: e.contextId,
                        targetSubjectName: e.contextName,
                        targetSubjectPrefix: 'ORG',
                        labels: commonLabels,
                        urls: {
                            locationUrl: addressConfig.views.update.config.locationUrl,
                            locationQueryParamsUrl: addressConfig.views.update.config.locationQueryParamsUrl,
                            updateAddressUrl: Y.Lang.sub(addressConfig.views.update.config.updateAddressUrl, {
                                id: e.addressId
                            })
                        },
                        panel: myAddressDialog,
                        locationConfig: locationConfig,
                        residentModel: new Y.usp.relationship.PaginatedResidentDetailsAndAddressesList({
                            url: Y.Lang.sub(addressConfig.views.update.config.residentUrl, {
                                orgId: e.contextId,
                                locationId: e.locationId,
                                orgAddressId: e.addressId
                            })
                        }),
                        addressId: e.addressId,
                    }, { modelLoad: true, payload: { id: e.addressId } });
                    break;

                case 'moveResidentPersonAddressLocation':
                    this.showView('moveResidentPersonAddressLocation', {
                        labels: addressConfig.views.updateResident.labels,
                        residentRecords: e.residentRecords,
                        newLocationId: e.newLocationId,
                        addressId: e.addressId,
                        model: new Y.usp.person.UpdatePersonAddressResidents({
                            url: Y.Lang.sub(addressConfig.views.updateResident.config.addressUrl, {
                                id: e.addressId
                            })
                        }),
                        dialog: myAddressDialog
                    }, { modelLoad: false });
                    break;

                };

            }, myAddressDialog);

            myAddressDialog.after('*:saveEnabledChange', function (e) {
                if (e.newVal === true) {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
                } else {
                    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', true);
                }
            }, myAddressDialog);

        }); 
    </script>