<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'Organisation','Organisation.ADD')" var="canAddOrganisation"/>
<sec:authorize access="hasPermission(null,'ProfessionalPersonTeamRelationship','ProfessionalPersonTeamRelationship.ADD')" var="canAddTeam"/>
<c:if test="${(canAddOrganisation eq true) || (canAddTeam eq true)}">

	<tilesx:useAttribute name="selected" ignore="true"/>
	<tilesx:useAttribute name="first" ignore="true"/>
	<tilesx:useAttribute name="last" ignore="true"/>
	
	<c:if test="${selected eq true }">
	  <c:set var="aStyle" value="pure-button-active"/>
	</c:if>
	<c:if test="${first eq true }">
	  <c:set var="fStyle" value="first"/>
	</c:if>
	<c:if test="${last eq true }">
	  <c:set var="lStyle" value="last"/>
	</c:if>
<c:choose>
<c:when test="${(canAddOrganisation eq true) && (canAddTeam eq true)}">
	<li id="organisation_team_button" class="pure-menu pure-menu-open pure-menu-horizontal menu-plugin hide-on-search" role="menu">
		<ul class="pure-menu-children">
			<li class="pure-menu-can-have-children pure-menu-has-children <c:out value="${fStyle}"/> <c:out value="${lStyle}"/>">
				<a href="#none" class='pure-button usp-fx-all <c:out value="${ aStyle}"/>' title="Add - click to choose"><i class='fa fa-plus-circle'></i> <i class='fa fa-caret-down icon-white'></i><span><s:message code="button.add"/></span></a>
				<ul class="pure-menu pure-menu-children">
					<li class="pure-menu-item"><a href="#none" title='<s:message code="organisation.add.button"/>' class="addOrganisation usp-fx-all"><i class="fa fa-building-o"></i> organisation</a></li>
					<li class="pure-menu-item"><a href="#none" title='<s:message code="team.add.button"/>' class="addTeam usp-fx-all"><i class="eclipse-team"></i> team</a></li>
				</ul>
			</li>
		</ul>
	</li>
	</c:when>
	<c:otherwise>
	<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'><a id="organisation_button" href="#none" class='addOrganisation usp-fx-all pure-button <c:out value="aStyle"/>' title='<s:message code="button.add"/>'><i class="fa fa-plus-circle"></i><span><s:message code="button.add" /></span></a></li>
	</c:otherwise>
</c:choose>
</c:if>

	<script>
		Y.use('yui-base','event-delegate','node','event-custom-base', 'split-button-plugin', function(Y){       
			var button ='#organisation_team_button';
			if(Y.one('#organisation_team_button')){
				Y.one('#organisation_team_button').plug(Y.Plugin.usp.SplitButtonPlugin);
				button =Y.one('#organisation_team_button');
			} else{
				button =Y.one('#organisation_button');
			}
			Y.delegate('click', function(e){				
				var t = e.currentTarget;
				if(t.hasClass('addOrganisation')){
					e.preventDefault();
					Y.fire('organisationDialog:add');
				}	else if(t.hasClass('addTeam')){
					e.preventDefault();
					Y.fire('teamDialog:add');
				}
			},button,'.pure-button, li.pure-menu-item a');		
		});
	</script>
