<%@page contentType="text/html;charset=UTF-8"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
 
<div id="organisationView">
    <%-- Insert Organisation ADD Template  must be used in conjunction with the REACT component scripts--%>
    <t:insertTemplate template="/WEB-INF/views/tiles/content/common/organisation/add.jsp" />
    <%-- Insert Team ADD Template  must be used in conjunction with the REACT component scripts--%>
    <t:insertTemplate template="/WEB-INF/views/tiles/content/common/team/add.jsp" />
    
    <t:insertTemplate template="/WEB-INF/views/tiles/content/organisation/panel/organisationDialog.jsp" />
    <t:insertTemplate template="/WEB-INF/views/tiles/content/organisation/panel/address-dialog.jsp" />
    <t:insertTemplate template="/WEB-INF/views/tiles/content/organisation/panel/reference-number-dialog.jsp" />
    <t:insertDefinition name="organisation.section" />
</div>