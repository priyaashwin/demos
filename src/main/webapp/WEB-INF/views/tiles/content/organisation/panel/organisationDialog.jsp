<%@ page import="java.util.Locale" %>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<script>

	Y.use(	'yui-base',
			'event-custom',
			'handlebars-helpers',
			'handlebars-organisation-templates',
			'multi-panel-popup',
			'multi-panel-model-errors-plugin', 
			'model-form-link',
			'model-transmogrify',
			'organisation-dialog-views',			
			'usp-organisation-Organisation',
			'usp-organisation-OrganisationOwnership',
			'usp-organisation-NewOrganisationOwnership',
		 	'usp-relationshipsrecording-UpdateDeleteOrganisationContacts',
		 	'usp-organisation-UpdateTeam',
		 	'handlebars-address-templates',
		
	function(Y){
		var L=Y.Lang,O=Y.Object;
		
		
		var ChangeOrganisationNameModel = Y.Base.create("ChangeOrganisationNameModel", Y.usp.organisation.Organisation, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
			url:'<s:url value="/rest/organisation/{id}"/>',
			form: '#changeOrganisationNameForm',
			//Provide our own implementation of getURL to update URL on save
			getURL:function(action, options){
				if(action==='update'){
					if(this.getAttrs().organisationType ==="TEAM"){
						return this._substituteURL('<s:url value="/rest/team/{id}?isNewMainName=true"/>', Y.merge(this.getAttrs(), options));	
					}
					return this._substituteURL('<s:url value="/rest/organisation/{id}?isNewMainName=true"/>', Y.merge(this.getAttrs(), options));
				}
				//return standard operation
				return ChangeOrganisationNameModel.superclass.getURL.apply(this, [action, options]);
			}
		});
		
		var NewContactReadModel = Y.Base.create("NewContactForm", Y.usp.contact.NewContact,[],{
			url:'<s:url value="/rest/organisation/'+'${organisation.id}'+'/contact"/>',			
		});
		
		var UpdateRemoveContactReadModel = Y.Base.create("UpdateRemoveContactReadModel", Y.usp.contact.ContactList,[],{
			url:'<s:url value="/rest/organisation/'+'${organisation.id}'+'/contact"/>'						
		});	
		
		var NewContactModel=Y.Base.create("NewContactModel", Y.usp.contact.NewContact,[],{
			url: '<s:url value="/rest/organisation/'+'${organisation.id}'+'/contact"/>',

			validate: function(attributes, callback){
				var errs={};						
				
				if (attributes._type=='NewEmailContact' && attributes.address=="") {
					errs['contactDetail-input']='Email address is mandatory';
				} else if (attributes._type=='NewWebContact' && attributes.address=="") {
				  	errs['contactDetail-input']='Web address is mandatory';                
				} else if(attributes._type=='NewEmailContact') {
					var atPart =  (attributes.address).indexOf("@")+2;
					var subaddress = (attributes.address).substring(atPart);
					var dotPosition = subaddress.indexOf(".");
						
					if (dotPosition==-1) {
						errs['contactDetail-input']='Please enter a valid Email address';
					}					
				}
				
				if(O.keys(errs).length>0){
					return callback({
						code:400,
						msg:{validationErrors:errs}
					});
				}
				
				// Data in form is correct.Now pass model to the server for any additional validation
				return NewContactModel.superclass.validate.apply(this, arguments);				
			}
		});
		
		var UpdateRemoveContactModel=Y.Base.create("UpdateRemoveContactModel", Y.usp.relationshipsrecording.UpdateDeleteOrganisationContacts,[],{
			url: '<s:url value="/rest/contact/invoke?action=combinedUpdateDeleteOrganisation&organisationId='+'${organisation.id}'+'"/>',
					
			validate: function(attributes, callback){
				var errs={},panelLabel='';				
				attributes.updateContactVOs.forEach(function(contact){									
					if((contact._type=='UpdateTelephoneContact' && contact.number=="") ||
				   		(contact._type=='UpdateSocialMediaContact' && contact.identifier=="") ||
				   		(contact._type=='UpdateEmailContact' && contact.address=="") ||
				   		(contact._type=='UpdateWebContact' && contact.address=="")
				  	  ) {
						
						for(var i=0; i<panelLabels.length; i++) {
							if (panelLabels[i][0] == contact.id){
								panelLabel = panelLabels[i][1];
							}
						}
						
						errs['contact_'+contact.id]=Y.one('#contactLabel_'+contact.id).getHTML() + ' within ' + panelLabel + ' section cannot be empty';
					}
					else if (contact._type=='UpdateEmailContact'  && contact.address!="") {						
						
						/* var atPart =  (contact.address).indexOf("@")+2;
						var subaddress = (contact.address).substring(atPart);
						var dotPosition = subaddress.indexOf(".");
						
						if (dotPosition==-1) {
							errs['contact_'+contact.id]='Please enter a valid Email address';
						} */
						
						var validEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						if (!validEmail.test(contact.address)) {					    	
					    	errs['contact_'+contact.id]='Please enter a valid Email address';
					   	}
					}
				});
				
				if(O.keys(errs).length>0){
					return callback({
						code:400,
						msg:{validationErrors:errs}
					});
				}
				
				// Data in form is correct.Now pass model to the server for any additional validation
				return UpdateRemoveContactModel.superclass.validate.apply(this, arguments);
				//return callback();
			}				
		});			
		var commonLabels = {
				name: '<s:message code="organisation.add.orgName"/>',
        	    type:'<s:message code="organisation.add.type"/>',
				subType:'<s:message code="organisation.add.subType"/>',
				endDate: '<s:message code="organisation.end.endDate"/>',
				openRelationshipsWarning: '<s:message code="organisation.end.openRelationshipsWarning" javaScriptEscape="true"/>',
        	    organisationDetails:'<s:message code="organisation.main.header" javaScriptEscape="true"/>',
				endOrganisation: '<s:message code="organisation.end.header" javaScriptEscape="true"/>',
				changeParent: '<s:message code="organisation.parent.change.header" javaScriptEscape="true"/>',
				editOrganisation:'<s:message code="organisation.edit.header" javaScriptEscape="true"/>',
				addOrganisationOwnership:'<s:message code="organisation.parent.change.header" javaScriptEscape="true"/>',
        	    changeName: '<s:message code="organisation.identificationPanel.changeName" />',
        	    changeNameLink: '<s:message code="organisation.identificationPanel.changeNameLink" />',
    	        changeNameSummaryBeginning: '<s:message code="organisation.identificationPanel.changeName.summary.beginning" javaScriptEscape="true"/>',
    	        changeNameSummaryEnding: '<s:message code="organisation.identificationPanel.changeName.summary.ending" />',
    	        changeNameSuccessMessage:'<s:message code="organisation.identificationPanel.changeName.success.message" javaScriptEscape="true"/>',
        	    editReferenceNumberNarrative: '<s:message code="person.referenceNumber.edit.narrative" javaScriptEscape="true"/>',
				addContactSuccessMessage : '<s:message code="person.contact.add.successmessage" javaScriptEscape="true"/>',
				endOrganisationMessage: '<s:message code="organisation.end.message" javaScriptEscape="true"/>',
				endTeamMessage: '<s:message code="team.end.message" javaScriptEscape="true"/>',
				endTeamHeader: '<s:message code="team.end.header" javaScriptEscape="true"/>',
				endOrganisationSuccessMessage : '<s:message code="organisation.end.successmessage" javaScriptEscape="true"/>',
				endTeamSuccessMessage : '<s:message code="team.end.successmessage" javaScriptEscape="true"/>',
				endOrganisationHeader: '<s:message code="organisation.end.header" javaScriptEscape="true"/>',
        		addContactHeader: '<s:message code="person.contact.add.header" javaScriptEscape="true"/>',
        		addContactNarrative: '<s:message code="person.contact.add.narrative" javaScriptEscape="true"/>',   	
        		editContactHeader: '<s:message code="person.contact.edit.header" javaScriptEscape="true"/>',
           	    editContactNarrative: '<s:message code="person.contact.edit.narrative" javaScriptEscape="true"/>',
           	    contactSingleEditSuccessMessage:'<s:message code="person.contact.single.edit.success.message" javaScriptEscape="true"/>',
           	 	contactSingleRemoveSuccessMessage:'<s:message code="person.contact.single.remove.success.message" javaScriptEscape="true"/>', 	
           	 	contactMultipleEditSuccessMessage:'<s:message code="person.contact.multiple.edit.success.message" javaScriptEscape="true"/>',
    	    	contactMultipleRemoveSuccessMessage:'<s:message code="person.contact.multiple.remove.success.message" javaScriptEscape="true"/>',           	    
           	 	contactMultipleEditRemoveSuccessMessage:'<s:message code="person.contact.multiple.edit.remove.success.message" javaScriptEscape="true"/>',
        		contactFixedType: '<s:message code="person.contact.type" javaScriptEscape="true"/>',
        	    contactType: '<s:message code="person.contact.contactType" javaScriptEscape="true"/>',
        	    contactUsage: '<s:message code="person.contact.contactUsage" javaScriptEscape="true"/>',
                contactVerified:'<s:message code="person.contact.contactVerified" javaScriptEscape="true"/>',
                contactVerifiedDate: '<s:message code="person.contact.contactVerifiedDate" javaScriptEscape="true"/>',
        	    socialMediaType: '<s:message code="person.contact.socialMediaType" javaScriptEscape="true"/>',  	    	
    	    	telephoneType: '<s:message code="person.contact.telephoneType" javaScriptEscape="true"/>',
    	    	contactNumber: '<s:message code="person.contact.number" javaScriptEscape="true"/>',
    	    	contactIdentifier: '<s:message code="person.contact.identifier" javaScriptEscape="true"/>',    	    	
    	    	contactDetail: '<s:message code="person.contact.detail" javaScriptEscape="true"/>',
    	    	addContactSuccessMessage : '<s:message code="person.contact.add.successmessage" javaScriptEscape="true"/>' ,   	    		
    	    	addContactSuccessMessage : '<s:message code="person.contact.add.successmessage" javaScriptEscape="true"/>',    	               	    		            
    	    	editOrganisationSuccessMessage:'<s:message code="organisation.edit.success.message" javaScriptEscape="true"/>',
    	    	narrativeEditOrganisation: '<s:message code="organisation.edit.narrative" javaScriptEscape="true"/>',  
    	    	narrativeAddOrganisationOwnership: '<s:message code="organisationOwnership.add.narrative" javaScriptEscape="true"/>',  
    	    	phoneNumber:'<s:message code="person.contactPanel.phoneNumber"/>',
		        mobileNumber:'<s:message code="person.contactPanel.mobileNumber"/>',
		        website:'<s:message code="person.contactPanel.website"/>',
		        email:'<s:message code="person.contactPanel.email"/>',
		        socialMedia:'<s:message code="person.contactPanel.socialMedia"/>',
		        fax:'<s:message code="person.contactPanel.fax"/>',
		        parentOrganisation:'<s:message code="organisation.parentOrganisation"/>',
		        organisationSearchPlaceholder: '<s:message code="organisation.organisationSearchPlaceholder"/>',
		        effectiveDate: '<s:message code="organisation.add.effectiveDate"/>',
		        startDate: '<s:message code="organisation.add.startDate"/>'
		};
		
		// Cancel button action
    	var cancelDialog = function(e) {
       		e.preventDefault();
       		organisationDialog.hide();
    	}; 

        var addOrganisationOwnershipSubmit = function(e){
            var self = this,
            view = this.get('activeView'),
            labels = view.get('labels'),
            model = view.get('model');

            model.save(function(err, res){
                if(!err){
                    self.hideDialogMask();
                    organisationDialog.hide();  
                    Y.fire('infoMessage:message', {
                        message: labels.editOrganisationSuccessMessage,
                    });
                    Y.fire('organisationDetailsChanged');
                }
            })
        }
        var editOrganisationSubmit = function(e) {
        	       	 
        	var self = this;
        	var view = this.get('activeView');        	  
        	var model = view.get('model');   	  
        	var labels = view.get('labels');
        	var transmogrify = Y.usp.organisation.UpdateOrganisation;         
        	if(model.get('organisationType')==='TEAM'){
        		transmogrify = Y.usp.organisation.UpdateTeam;
        		model.url = '<s:url value="/rest/team/{id}"/>';
        	}
        	model.save({ transmogrify: transmogrify}, function(err, res){
        	            	                   		             		  
        		if(!err){
        	                   			  
        			self.hideDialogMask();
        			organisationDialog.hide();  
        			Y.fire('infoMessage:message', {       	                    				  
        				message: labels.editOrganisationSuccessMessage,        	                    				  
        			});
        			Y.fire('organisationDetailsChanged');
  					Y.fire('contentContext:update',{
						data:{
							id:model.get('id'),
							name:model.get('name'),
							personId:model.get('organisationIdentifier'),
						}
					});         	                			  
        	                        			  
        			//window.location=L.sub('<s:url value="/organisation?id={organisationId}" />',{
        			//	organisationId: model.get('id')      				
        			//});
       	                		  
        		}

        	});
        
        };

        //This will create new organisation name and previous name if all
        //criteria matched
        var changeOrganisationNameSubmit = function(e) {
	       	 
        	var self = this;
        	var view = this.get('activeView');        	  
        	var model = view.get('model');   	  
        	var labels = view.get('labels');
        	
        	var transmogrify = Y.usp.organisation.UpdateOrganisation ;         
        	if(model.get('organisationType')==='TEAM'){
        		transmogrify = Y.usp.organisation.UpdateTeam;
        	}               	  
        	model.save({ transmogrify: transmogrify}, function(err, res){
        	            	                   		             		  
        		if(!err){
        	                   			  
        			self.hideDialogMask();
        			organisationDialog.hide();  
        			Y.fire('infoMessage:message', {       	                    				  
        				message: labels.changeNameSuccessMessage,        	                    				  
        			});
        			Y.fire('organisationDetailsChanged');
  					Y.fire('contentContext:update',{
						data:{
							id:model.get('id'),
							name:model.get('name'),
							personId:model.get('organisationIdentifier'),
						}
					});         	                			  
       	                		  
        		}

        	});
        
        };
        
        // save button action
        var addContactSubmit = function(e){
            
            var _instance = this;
            var view = this.get('activeView');
            var contactFixedType = view.get('fixedContactType');
            var contactType = view.get('container').one('#contactType-input').get('value');
            var contactUsage = view.get('container').one('#contactUsage-input').get('value');
            var contactDetail = view.get('container').one('#contactDetail-input').get('value');
            var contactNumber = view.get('container').one('#number-input').get('value');
            var contactIdentifier = view.get('container').one('#identifier-input').get('value');
            var isVerified = view.get('container').one('#contactVerified-input').get('checked');
            var contactVerifiedDate = view.get('container').one('#contactVerifiedDate-input').get('value');
            var verifiedDateAsNumber = Y.USPDate.parseDate(contactVerifiedDate);
           
            e.preventDefault();       
            
            this.showDialogMask();    //show the mask

            switch(contactFixedType) {
            case 'EMAIL':
            newContactModel = new NewContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(),
                  "preferred" : "false", "address" : contactDetail, "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber,
                  "_type": 'NewEmailContact'
                }
            );  
                break;
            case 'SOCIALMEDIA':
            var socialMediaType = view.get('container').one('#socialMediaType-input').get('value');
            newContactModel = new NewContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(), 
                  "preferred" : "false", "socialMediaType" : socialMediaType , "identifier" : contactIdentifier,
                  "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber, "_type": 'NewSocialMediaContact'
                }
            );
                break;
            case 'TELEPHONE':
            var telephoneType = view.get('container').one('#telephoneType-input').get('value');
            newContactModel = new NewContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(),
                  "preferred" : "false", "telephoneType" : telephoneType, "number" : contactNumber,             
                  "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber, "_type": 'NewTelephoneContact'
                }
            );
                break;
            case 'WEB':
            newContactModel = new NewContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(),
                  "preferred" : "false", "address" : contactDetail, "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber,
                  "_type": 'NewWebContact'
                }
            );            
                break;
            default:
              break ;
              }     
          
                     

            // Add this model to the organisationDialog target allows us to use the multipanel error message
            newContactModel.addTarget(organisationDialog);
          
            // Do the save   
            newContactModel.save(function(err, res){              
             
                  _instance.hideDialogMask();             //hide the mask - prevent duplicat save on double click 
                  
                  if(err) {
                	  
                    if(err.code !== 400){
                      Y.log(err.code + err.msg);
                    }
                    
                  } else {
                	  organisationDialog.hide();
                  	Y.fire('contactChanged');
                  	Y.fire('infoMessage:message', {message:commonLabels.addContactSuccessMessage}); 
                }
              });
           
        };
        
     // Array to store contact ids and its corresponding panel labels
		var panelLabels = [[]];
		 var editRemoveContactSubmit = function(e) {
			 
				// prevent duplicate save on double click
			  	var _instance = this;		 
				e.preventDefault();       
				this.showDialogMask();		//show the mask
				
			    contactEdited=0, 
				contactDeleted=0;
				
				var deleteContNums = [], updateContNums = [], model, view, contactInput, removeContact, eContacts, wContacts, updateContact, subType;
								
				
				model = this.get('activeView').get('model').toJSON();
				view = this.get('activeView').get('container');	
				
				// get sub entities			
				eContacts = this.get('activeView').get('emailContacts');
				wContacts = this.get('activeView').get('webContacts');
				
				model.forEach(function(contact)
				    {	
					    contactInput = view.getById('contact_'+contact.id);
					    isVerified = view.getById('contactVerified-input_'+contact.id).get('checked');
						removeContact = contactInput.getAttribute('data-remove-num');					
						var panelLabel=[];
							
						if(removeContact){
							// add contacts to delete to the array
							deleteContNums.push(contact.id);
							contactDeleted = contactDeleted + 1;
						}
						else {
							currentValue = contactInput.get('value');						
							if (isVerified) {
								verifiedDate = view.getById('contactVerifiedDate-input_'+contact.id).get('value');
								verifiedDate = Y.USPDate.parseDate(verifiedDate);
							} else {
								verifiedDate = null;
							}
					 		if(contact.number){
					 			if(currentValue!==contact.number){
					 				contactEdited = contactEdited + 1;
								}
				 				subType='UpdateTelephoneContact';
				 				if(contact.telephoneType == 'LANDLINE') {
				 					panelLabel = [contact.id,"Phone number"];
				 				} else if(contact.telephoneType == 'MOBILE') {
				 					panelLabel = [contact.id,"Mobile number"];
				 				} else if(contact.telephoneType == 'FAX') {
				 					panelLabel = [contact.id,"Fax number"];
				 				}				 								 				
				 				panelLabels.push(panelLabel);
				 				
								updateContact = {"number":currentValue,"verified":isVerified,"verifiedDate":verifiedDate,"id":contact.id,"objectVersion":contact.objectVersion,"_type":subType};
					  		}else if(contact.identifier){
					 			if(currentValue!==contact.identifier){
					 				contactEdited = contactEdited + 1;
					 			}
				 				subType='UpdateSocialMediaContact';
				 				
				 				panelLabel = [contact.id,"Social media"];
				 				panelLabels.push(panelLabel);
				 				
				 				updateContact = {"identifier":currentValue,"verified":isVerified,"verifiedDate":verifiedDate,"id":contact.id,"objectVersion":contact.objectVersion,"_type":subType};
					  		} else if (contact.address) {
								 if(currentValue!==contact.address){
									contactEdited = contactEdited + 1;
								 }	
									//Check for subTypes
								 	for(var i=0; i<eContacts.length; i++){
										if(eContacts[i].id==contact.id){						
											subType='UpdateEmailContact';											
											break;
										}		
								 	}							 	
									for(var i=0; i<wContacts.length; i++){
										if(wContacts[i].id==contact.id){						
											subType='UpdateWebContact';
											break;
										}
									}
									
					 				if (subType == 'UpdateEmailContact') {
										panelLabel = [contact.id,"Email address"];						 										 			
					 				} else if(subType == 'UpdateWebContact') {
										panelLabel = [contact.id,"Website"];	
					 				}
					 				
					 				panelLabels.push(panelLabel);
					 				updateContact = {"address":currentValue,"verified":isVerified, "verifiedDate":verifiedDate, "id":contact.id,"objectVersion":contact.objectVersion,"_type":subType};
							 }
					 		//store the VO in the large array				 		
					 		updateContNums.push(updateContact);
						}
					});	
				
				updateRemoveContactModel = new UpdateRemoveContactModel(
					{
						"_type":"UpdateDeleteOrganisationContacts",
						"objectVersion":0,
						"updateContactVOs":updateContNums,
						"deleteContactIds":deleteContNums
					}
				);
				
				// Adding this model to the organisationDialog targes allows us to use the multipanel error message
				updateRemoveContactModel.addTarget(organisationDialog);
				
				updateRemoveContactModel.save(function(err, res){
					_instance.hideDialogMask();		  				//hide the mask -	prevent duplicat save on double click 
	        		if (err){
						if(err.code !== 400){
							Y.log(err.code + err.msg);
						}
	        		}else{
	        			
	        			Y.fire('contactChanged');
	        			organisationDialog.hide();
	        			 //Show success message, even if no changes have been made, consistent with rest of app
	        			if(contactEdited<2 && contactDeleted===0){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactSingleEditSuccessMessage});	    						
	    				}
	        			//Multiple edits only
	        			else if(contactEdited>1 && contactDeleted===0){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactMultipleEditSuccessMessage});	        						
	        			}
	        			//Single remove only
	        			else if(contactEdited===0 && contactDeleted===1){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactSingleRemoveSuccessMessage});	        						
	        			}
	        			//Multiple remove only
	        			else if(contactEdited===0 && contactDeleted>1){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactMultipleRemoveSuccessMessage});	        						
	        			}
	        			// Edit(s) and remove(s)
	        			else if(contactEdited>0 && contactDeleted>0){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactMultipleEditRemoveSuccessMessage});	    						
	    				}					      		
	               		        		
	        		//hide afterwards
	              	//setTimeout(function(){
	              	//	Y.one('#headerMessage').hide('fadeOut');
	              	//},5000);
	        	}
	        });
		};
	
		var organisationDialog = new Y.usp.MultiPanelPopUp({
			headerContent:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-building-o fa-inverse fa-stack-1x"></i></span> '+ commonLabels.organisationDetails+'</h3>',
			width:700,
			//Plugin Model Error handling
			plugins:[{fn:Y.Plugin.usp.MultiPanelModelErrorsPlugin}],			
			
			// Views that this dialog supports
			views: {				
				editOrganisation: {
					type: Y.app.organisation.EditOrganisationView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-building-o fa-inverse fa-stack-1x"></i></span>'+commonLabels.editOrganisation+ '</h3>',			          
					buttons:[{                  			            
						action:   editOrganisationSubmit,                  			            
						section:  Y.WidgetStdMod.FOOTER,			            
						name:      'saveButton',			            
						labelHTML: '<i class="fa fa-check"></i> save',			            
						classNames:'pure-button-primary'	          
					},{                 			            
						action:     cancelDialog,                 			            
						section:    Y.WidgetStdMod.FOOTER,                  			            
						name:       'cancelButton',                   			            
						labelHTML:  '<i class="fa fa-times"></i> cancel',                 		            
						classNames: 'pure-button-secondary'			          
					}]
				},				
				addOrganisationOwnership: {
					type: Y.app.organisation.AddOrganisationOwnershipView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-share-alt fa-inverse fa-stack-1x"></i></span>'+commonLabels.addOrganisationOwnership+ '</h3>',
					buttons:[{
						action:   addOrganisationOwnershipSubmit,
						section:  Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> save',
						classNames:'pure-button-primary'
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:       'cancelButton',
						labelHTML:  '<i class="fa fa-times"></i> cancel',
						classNames: 'pure-button-secondary'
					}]
				},
				
				changeOrganisationName: {
					type: Y.app.organisation.ChangeOrganisationNameView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-building-o fa-inverse fa-stack-1x"></i></span>'+commonLabels.changeName+ '</h3>',			          
					buttons:[{                  			            
						action:  changeOrganisationNameSubmit,                  			            
						section:  Y.WidgetStdMod.FOOTER,			            
						name:      'saveButton',			            
						labelHTML: '<i class="fa fa-check"></i> save',			            
						classNames:'pure-button-primary'	          
					},{                 			            
						action:     cancelDialog,                 			            
						section:    Y.WidgetStdMod.FOOTER,                  			            
						name:       'cancelButton',                   			            
						labelHTML:  '<i class="fa fa-times"></i> cancel',                 		            
						classNames: 'pure-button-secondary'			          
					}]
				},

				addContact: {
					type:Y.app.organisation.NewContactView,		
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa eclipse-contact fa-inverse fa-stack-1x"></i></span>'+commonLabels.addContactHeader+'</h3>',
				 	buttons:[{
						action:    addContactSubmit,		
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> cancel',
						classNames:'pure-button-secondary'
					}]
				},
				// Edit/Remove Contact dialog
				 editRemoveContact: {
					type:Y.app.organisation.EditRemoveContactView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa eclipse-contact fa-inverse fa-stack-1x"></i></span>'+commonLabels.editContactHeader+'</h3>',				 	
				 	buttons:[{
						action:    editRemoveContactSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> cancel',
						classNames:'pure-button-secondary'
					}]
				},
			}
		});
		
		//render the popup
		organisationDialog.render();
		
		var self = this;
		this.endOrganisationDialog = new Y.app.organisation.EndOrganisationDialog({
			views: {
				endOrganisation: {
        			header: {
        				text: commonLabels.endOrganisationHeader,
						icon: 'fa fa-building-o'
        			},
        			narrative: {
						summary: commonLabels.endOrganisationMessage 
    				},
        			successMessage: commonLabels.endOrganisationSuccessMessage,
				},
    			endTeam: {
        			header: {
        				text: commonLabels.endTeamHeader,
        				icon: 'fa fa-building-o'
        			},
        			narrative: {
						summary: commonLabels.endTeamMessage
        			},
        			successMessage: commonLabels.endTeamSuccessMessage,
				}
			}      
		});
		
		this.endOrganisationDialog.render();

		//respond to custom events
		Y.on('organisation:showDialog', function(e){
			switch(e.action){
			case 'endOrganisation': 
				if(e.isTeam) {
					self.endOrganisationDialog.showView('endTeam',{
						model: new Y.app.organisation.EndOrganisationModel({
							url:'<s:url value="/rest/organisation/{id}"/>'
						}),
						labels: commonLabels,
						organisationUrl: '<s:url value="/rest/team/{id}/status?action=close"/>',
						transmogrify: Y.usp.organisation.UpdateEndTeam
					}, {
						modelLoad: true, 
						payload: { id:e.id }
					});
				} else {
					self.endOrganisationDialog.showView('endOrganisation',{
						model: new Y.app.organisation.EndOrganisationModel({
							url:'<s:url value="/rest/organisation/{id}"/>'
						}),
						labels: commonLabels,
						organisationUrl: '<s:url value="/rest/organisation/{id}/status?action=close"/>',
						transmogrify: Y.usp.organisation.UpdateEndOrganisation
					}, {
						modelLoad: true, 
						payload: { id:e.id }
					});
				}
			break;
			case 'addOrganisationOwnership':
				this.showView('addOrganisationOwnership', {
					model: new Y.app.organisation.AddOrganisationOwnershipForm({
						url:'<s:url value="/rest/organisationOwnership"/>',
						childOrganisationId:e.id,
					}),
					organisationAutocompleteURL: '<s:url value="/rest/organisation?nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
					labels: commonLabels
				}, {
					modelLoad: false
				});
				break;
			break;
			case 'addOrganisation':
				this.showView('addOrganisation',{
					model: new Y.app.organisation.AddOrganisationForm({
						url:'<s:url value="/rest/organisation"/>',
					}),
					labels: commonLabels,
					autocompleteURL:'<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&pageSize={maxResults}"/>',
				},
				function(){
				this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				var typeVal=Y.one('#mainTypeOrganisation-input').one('option');
				if(!typeVal.get('value')){
					typeVal.setStyle('display', 'none').set('text', 'Please choose');
				}
				var subTypeVal=Y.one('#subTypeOrganisation-input').one('option');
				if(!subTypeVal.get('value')){
					subTypeVal.setStyle('display', 'none').set('text', 'Please choose');
				}
				});
			break;
			
			case 'editOrganisation':				
				this.showView('editOrganisation', {			         
					model: new Y.app.organisation.EditOrganisationForm({
						url: '<s:url value="/rest/organisation/{id}"/>'
					}),	
					labels: commonLabels			        
				}, {
					modelLoad: true, 
					payload: { id:e.id }
				});				
				break;
			case 'changeOrganisationName':				
				this.showView('changeOrganisationName', {			         
					model: new ChangeOrganisationNameModel(),			          
					labels: commonLabels			        
				}, {
					modelLoad: true, 
					payload: { id:e.id }
				});				
				break;
				
			case 'addContact':					
				this.showView('addContact',{			
					model: new NewContactReadModel(),
					labels: commonLabels,							  
					//set targetOrganisation attributes	
					contextId: e.id,
					contextName: e.name},
				function(){		        		
					//Get the button by name out of the footer and enable it.
					this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',true);
					
					var firstOption ;
					firstOption=Y.one('#contactType-input').one('option');
					if(!firstOption.get('value')){
						firstOption.setStyle('display', 'none').set('text', 'Please choose');
					} 
					firstOption=Y.one('#contactUsage-input').one('option');
					if(!firstOption.get('value')){
						firstOption.setStyle('display', 'none').set('text', 'Please choose');
					}
					firstOption=Y.one('#socialMediaType-input').one('option');
					if(!firstOption.get('value')){
						firstOption.setStyle('display', 'none').set('text', 'Please choose');
					}
					firstOption=Y.one('#telephoneType-input').one('option');
					if(!firstOption.get('value')){
						firstOption.setStyle('display', 'none').set('text', 'Please choose');
					}						
				});
				break;	
			 case 'editRemoveContact':					
				 this.showView('editRemoveContact',{
					model: new UpdateRemoveContactReadModel().load(), 
					labels: commonLabels,
					contextId: e.id,
					contextName: e.name,	
					canRemoveContact: e.canRemoveContact,
					codedEntries:{
						contactType:Y.uspCategory.contact.contactType.category.codedEntries,
						contactUsage:Y.uspCategory.contact.contactUsage.category.codedEntries			    			
					}							
			},{
				modelLoad:true, 
				payload:{
					id:e.id							
					}
			},function(){
				this.get('activeView').render();
				//Get the button by name out of the footer and enable it.
	     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			});
			break;	
			default:
			}Y.one("body").delegate('submit',function(e){e.preventDefault();},'form[action="#none"]');	
		},organisationDialog);
		
		organisationDialog.after('*:saveEnabledChange', function(e){
			// enable/disable the save button
			if (e.newVal===true) {			    
			    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			} else {
			    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',true);
			}
		}, organisationDialog);	
		
	});
         


</script>