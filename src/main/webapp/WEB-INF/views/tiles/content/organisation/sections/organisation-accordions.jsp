<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<s:eval expression="@jacksonObjectMapper" var="objectMapper" />
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'Organisation','Organisation.ADD_CONTACT')" var="canAddContact"/>
<sec:authorize access="hasPermission(null, 'Organisation','Organisation.UPDATE')" var="canEditOrganisation"/>
<sec:authorize access="hasPermission(null, 'Organisation','Organisation.CLOSE')" var="canEndOrganisation"/>
<sec:authorize access="hasPermission(null, 'Organisation','Team.CLOSE')" var="canEndTeam"/>
<sec:authorize access="hasPermission(null, 'OrganisationAddress','OrganisationAddress.ADD')" var="canAddAddress"/>
<sec:authorize access="hasPermission(null, 'OrganisationAddress','OrganisationAddress.Close')" var="canCloseAddress"/>
<sec:authorize access="hasPermission(null, 'OrganisationAddress','OrganisationAddress.Remove')" var="canRemoveAddress"/>
<sec:authorize access="hasPermission(null, 'OrganisationAddress','OrganisationAddress.UPDATE_START_END')" var="canUpdateAddressStartEnd"/>
<sec:authorize access="hasPermission(null, 'OrganisationAddress','OrganisationAddress.UPDATE_REOPEN')" var="canUpdateAddressReopen"/>
<sec:authorize access="hasPermission(null, 'OrganisationAddress','OrganisationAddress.Update')" var="canUpdateAddress"/>
<sec:authorize access="hasPermission(null, 'PersonAddress','PersonAddress.UpdateResidents')" var="canUpdateResidentLocation"/>
<sec:authorize access="hasPermission(null, 'Organisation','OrganisationReferenceNumber.ADD')" var="canAddReferenceNumber"/>
<sec:authorize access="hasPermission(null, 'Organisation','OrganisationReferenceNumber.UPDATE')" var="canUpdateReferenceNumber"/>
<sec:authorize access="hasPermission(null, 'Organisation','OrganisationContact.Update')" var="canEditContact"/>
<sec:authorize access="hasPermission(null, 'Organisation','OrganisationContact.Remove')" var="canRemoveContact"/>
<sec:authorize access="hasPermission(null, 'OrganisationOwnership','OrganisationOwnership.ADD')" var="canAddOrganisationOwnership"/>
<sec:authorize access="hasPermission(null, 'OrganisationName','OrganisationName.ADD')" var="canChangeOrganisationName"/>
<sec:authorize access="hasPermission(null, 'Address.Disclose','Address.Disclose')" var="canSetAddressDisclose"/>
<sec:authorize access="hasPermission(null, 'Address.DoNotDisclose','Address.DoNotDisclose')" var="canSetAddressDoNotDisclose"/>


<t:insertTemplate template="/WEB-INF/views/tiles/content/organisation/sections/toggle-accordions.jsp" />

<div id="organisationSearchResults" class="pure-table pure-table-striped pure-table-hover"></div>
<div id="organisationSearchResultsPaginator"></div>

<div id="organisationDetails">
	<div id="identification"></div>
</div>

<script>
Y.use('yui-base','node','event-custom-base','usp-organisation-Organisation','app-organisation-accordions','usp-contact-Contact','usp-organisation-OrganisationReferenceNumber', 'usp-organisation-OrganisationWithPreviousNamesAndParent','paginated-results-table', function(Y) {
	var canAddAddress = ${canAddAddress};
	var canCloseAddress = ${canCloseAddress};
	var canRemoveAddress = ${canRemoveAddress};
	var canUpdateAddress = ${canUpdateAddress};
	var canSetAddressDoNotDisclose = '${canSetAddressDoNotDisclose}' === 'true';
	var canSetAddressDisclose = '${canSetAddressDisclose}' === 'true';
	var canUpdateResidentLocation = ${canUpdateResidentLocation};
	var canAddContact = ${canAddContact};
	var canEditContact = ${canEditContact};
	var canRemoveContact = ${canRemoveContact};
	var canEditOrganisation = ${canEditOrganisation};
	var canEndOrganisation = ${canEndOrganisation};
	var canAddOrganisationOwnership = ${canAddOrganisationOwnership};
	var canEndTeam = ${canEndTeam};
	var canAddReferenceNumber = ${canAddReferenceNumber};
	var canUpdateReferenceNumber = ${canUpdateReferenceNumber};
	var canRemoveReferenceNumber = true;
	var canChangeOrganisationName = ${canChangeOrganisationName},
	    canUpdateAddressReopen = ${canUpdateAddressReopen},
	    canUpdateAddressStartEnd = ${canUpdateAddressStartEnd};

	var organisationModel = new Y.usp.organisation.OrganisationWithPreviousNamesAndParent({
		id: '${organisation.id}',
		root: '<s:url value="/rest/organisation"/>'
	}).load();

	var dataContacts = ${objectMapper.writeValueAsString(organisationContacts)};
	var dataReferenceNumbers = ${objectMapper.writeValueAsString(organisationReferences)};

	var contactsModelList = new Y.usp.contact.ContactList({
		items: dataContacts
	});

	contactsModelList.url = '<s:url value="/rest/organisation/${organisation.id}/contact"/>';

	var referenceModelList = new Y.usp.organisation.PaginatedOrganisationReferenceNumberList({
		items: dataReferenceNumbers
	});

	referenceModelList.url= '<s:url value="/rest/organisation/${organisation.id}/referenceNumber?s=type:asc&pageSize=100"/>';

	var organisationDetailsView = new Y.app.views.OrganisationAccordions({
		toolbarNode:'#sectionToolbar',
		container: '#identification',
		model: organisationModel,
		identificationDetailsConfig:{
			canEditOrganisation: canEditOrganisation,
			canEndOrganisation: canEndOrganisation,
			canAddOrganisationOwnership: canAddOrganisationOwnership,
			canEndTeam: canEndTeam,
			canChangeOrganisationName: canChangeOrganisationName,
			closed: false,
			title: '<s:message code="organisation.identificationPanel.title"/>',
			labels: {
				organisationId:'<s:message code="organisation.label.id"/>',
				name: '<s:message code="organisation.label.name"/>',
				type: '<s:message code="organisation.label.organisationType"/>',
				subType: '<s:message code="organisation.label.organisationSubType"/>',
				startDate: '<s:message javaScriptEscape="true" code="common.column.date.start"/>',
				endDate: '<s:message javaScriptEscape="true" code="organisation.end.endDate"/>',
				editButtonLabel: '<s:message code="button.edit"/>',
				endButtonLabel:'<s:message code="organisation.end.header"/>',
				changeParentLabel:'<s:message code="organisation.parent.change.header"/>',
				previousNames:'<s:message code="organisation.identificationPanel.previousNames"/>',
				changeName: '<s:message code="organisation.identificationPanel.changeName" />',
				changeNameLink : '<s:message code="organisation.identificationPanel.changeNameLink" />',
				changeNamePopupTitle:'<s:message code="organisation.identificationPanel.changeName.popup.title" />',
				changeNamePopupContent :'<s:message code="organisation.identificationPanel.changeName.popup.content" javaScriptEscape="true"/>',
				parentOrganisation: '<s:message code="organisation.parentOrganisation"/>',
				parentOganisationOwnershipStartDate: '<s:message code="organisation.parentOganisationOwnershipStartDate"/>'
			}
		},
		organisationTreeConfig:{
			labels:{
				accordionTitle:'<s:message code="organisation.hierarchy.title"/>'
			},
			properties:{
				endpoints:{
					organisationEndpoint:'<s:url value="/rest/organisation/{id}"/>'
				}
			},
			viewOrganisationURL:'<s:url value="/organisation?id={id}"/>'
		},
		contactDetailsConfig: {
			canAddContact: canAddContact,
			canEditContact: canEditContact,
			canRemoveContact: canRemoveContact,
			title: '<s:message code="person.contactPanel.title"/>',
			modelList: contactsModelList,
			closed: true,
			contextId: '${esc:escapeJavaScript(organisation.organisationIdentifier)}',
			contextType: 'organisation',
			contextName: '${esc:escapeJavaScript(organisation.name)}',
			labels: {
				email:'<s:message code="person.contactPanel.email" javaScriptEscape="true"/>',
				fax: '<s:message code="person.contactPanel.fax" javaScriptEscape="true"/>',
				mobileNumber:'<s:message code="person.contactPanel.mobileNumber" javaScriptEscape="true"/>',
				phoneNumber:'<s:message code="person.contactPanel.phoneNumber" javaScriptEscape="true"/>',
				socialMedia:'<s:message code="person.contactPanel.socialMedia" javaScriptEscape="true"/>',
				website:'<s:message code="person.contactPanel.website" javaScriptEscape="true"/>',
				contactVerified:'<s:message code="person.contact.contactVerified" javaScriptEscape="true"/>',
				contactVerifiedDate: '<s:message code="person.contact.contactVerifiedDate" javaScriptEscape="true"/>'
			}
		},
		addressesConfig:{
			canAddAddress: canAddAddress,
			canEndAddress: canCloseAddress,
			doNotDisclosePemissions: {
				canSetAddressDoNotDisclose: canSetAddressDoNotDisclose,
				canSetAddressDisclose: canSetAddressDisclose
			},
			canUpdateAddressReopen: canUpdateAddressReopen,
			canRemoveAddress: canRemoveAddress,
			canUpdateAddressStartEnd: canUpdateAddressStartEnd,
			canUpdateResidentLocation: canUpdateResidentLocation,
			canUpdateAddress: canUpdateAddress,
			closed: true,
			contextId: '<c:out value="${organisation.id}"/>',
			contextType: 'organisation',
			contextName: '${esc:escapeJavaScript(organisation.name)}',
			title: '<s:message code="address.address" javaScriptEscape="true"/>',
			noDataMessage:'<s:message code="address.no.results" javaScriptEscape="true"/>',
			url: '<s:url value="/rest/organisation/${organisation.id}/address?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
			doNotDiscloseURLs: {
				setAddressDoNotDiscloseURL: '<s:url value="/rest/address/{id}/status?action=doNotDisclose"/>',
				setAddressDiscloseURL: '<s:url value="/rest/address/{id}/status?action=disclose"/>'
			},
			labels: {
				endAction: '<s:message code="address.actions.end" javaScriptEscape="true"/>',
				removeAction: '<s:message code="address.actions.remove" javaScriptEscape="true"/>',
				moveAddressLocation: '<s:message code="address.actions.moveAddressLocation" javaScriptEscape="true"/>',
				addButtonLabel:'<s:message code="button.add" javaScriptEscape="true"/>',
				address:'<s:message code="address.address" javaScriptEscape="true"/>',
				addressResultsView:'<s:message code="address.results.view" javaScriptEscape="true"/>',
				addressResultsActions:'<s:message code="address.results.actions" javaScriptEscape="true"/>',
				roomDescription:'<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>',
				floorDescription:'<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
				startDate:'<s:message code="address.startdate" javaScriptEscape="true"/>',
				endDate:'<s:message code="address.enddate" javaScriptEscape="true"/>',
				type:'<s:message code="address.type" javaScriptEscape="true"/>',
				usage:'<s:message code="address.usage" javaScriptEscape="true"/>',
				updateStartEndAddressDates:'<s:message code="person.address.date.update.header" javaScriptEscape="true"/>',
				updateAddressReopen: '<s:message code="person.address.find.results.updateAddressReopen" javaScriptEscape="true"/>',
				doNotDisclose: '<s:message code="person.address.doNotDisclose" javaScriptEscape="true"/>'
			}
		},
		referenceNumbersConfig: {
			closed: true,
			contextId: '<c:out value="${organisation.id}"/>',
			contextType: 'organisation',
			contextName: '${esc:escapeJavaScript(organisation.name)}',
			canAddReferenceNumber: canAddReferenceNumber,
			canUpdateReferenceNumber: canUpdateReferenceNumber,
			canRemoveReferenceNumber: canRemoveReferenceNumber,
			modelList: referenceModelList,
			title: '<s:message code="person.referenceNumber.view.title"/>',
			labels: {
				nationalInsurance:'<s:message code="person.referenceNumber.view.nationalInsurance"/>',
				driversLicence:'<s:message code="person.referenceNumber.view.driversLicence"/>',
				passport:'<s:message code="person.referenceNumber.view.passport"/>',
				addButton:'<s:message code="button.add"/>',
				editButton:'<s:message code="button.edit"/>',
				referenceNumberType: '<s:message code="person.referenceNumber.referenceNumberType"/>',
				number:'<s:message code="person.referenceNumber.number"/>',
				addReferenceNumberMessage: '<s:message code="person.referenceNumber.add.narrative" javaScriptEscape="true"/>',
				addReferenceNumberHeader: '<s:message code="person.referenceNumber.add.header" javaScriptEscape="true"/>'
			}
		}
	});

	organisationDetailsView.render();

	// Please add events below here
	Y.delegate('click', function() {
		Y.fire('organisation:showDialog', {
			action: 'addOrganisation'
		});
	}, 'body', '#add-organisation');

	Y.on('organisationDetailsChanged', function(e) {
		organisationModel.load();
	});

	Y.on('contactChanged', function(e) {
		contactsModelList.load();
	});

	Y.on('referenceNumberChanged', function(e) {
		referenceModelList.load();
	});
});
</script>
