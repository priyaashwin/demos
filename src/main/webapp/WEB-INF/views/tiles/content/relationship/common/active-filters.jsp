<%@page contentType="text/html;charset=UTF-8"%>

<div id="relationshipValidationError" class="pure-alert pure-alert-block pure-alert-error hide-on-search" style="display:none">
 		<a href="#" class="close" title="Close">Close</a>
 		<h4 tabindex="0"></h4>
</div>
<div id="relationshipUserInfo" class="pure-alert pure-alert-block pure-alert-info hide-on-search" style="display:none">
 		<a href="#" class="close" title="Close">Close</a>
 		<h4 tabindex="0"></h4>
</div>
<div id="relationshipFilterContext" class="filter-context"></div>
