<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'PersonPersonRelationship','PersonPersonRelationship.View')" var="canView"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Genogram.View')" var="canViewGenogram"/>

<c:if test="${canView eq true}">
	<tilesx:useAttribute name="selected" ignore="true"/>
	<tilesx:useAttribute name="first" ignore="true"/>
	<tilesx:useAttribute name="last" ignore="true"/>
	<c:if test="${selected eq true }">
		<c:set var="aStyle" value="pure-button-active"/>
	</c:if>
	<c:if test="${first eq true }">
		<c:set var="fStyle" value="first"/>
	</c:if>
	<c:if test="${last eq true }">
		<c:set var="lStyle" value="last"/>
	</c:if>
	<li id="graph_style_button" class="pure-menu pure-menu-open pure-menu-horizontal menu-plugin hide-on-search" role="menu">
		<ul class="pure-menu-children">
			<li class="pure-menu-can-have-children pure-menu-has-children <c:out value="${fStyle}"/> <c:out value="${lStyle}"/>">
				<a href="#none" class='pure-button <c:out value="${ aStyle}"/>' title="Change diagram style"><i class='fa fa-sitemap'></i> <i class='fa fa-caret-down icon-white'></i><span><s:message code="button.graph"/></span></a>
				<ul class="pure-menu pure-menu-children">
					<li class="pure-menu-item"><a href="#none" title="Generation diagram" class="swimlane usp-fx-all"><i class="fa fa-columns"></i> Generation layout</a></li>
					<li class="pure-menu-item"><a href="#none" title="Hub and spoke diagram" class="hub_n_spoke usp-fx-all"><i class="fa fa-crosshairs"></i> Hub and spoke layout</a></li>
          <c:if test="${canViewGenogram eq true}">
            <li class="pure-menu-item"><a href="#none" title="Genogram" class="genogram usp-fx-all"><i class="fa fa-group"></i> Genogram</a></li>
          </c:if>
				</ul>
			</li>
		</ul>
	</li>
	<script>
		Y.use('yui-base', 'split-button-plugin', function(Y){
			//plug in button menu for graph style
			Y.one('#graph_style_button').plug(Y.Plugin.usp.SplitButtonPlugin);
		});
	</script>
</c:if>
