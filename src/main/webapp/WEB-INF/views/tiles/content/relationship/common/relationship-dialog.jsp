<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>  

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.Update')" var="canEdit"/>
</c:if>

<%-- Dig out what we need from the user context --%>
<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="isNamedPersonRelationshipAvailable" value="${userContextAttributes.getContext('named-person-relationship-available')}" />

<%-- access relationship allowed attribute --%>
<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="relationshipAttributesContext" value="${userContextAttributes.getContext('relationship-attribute')}" />
<c:set var="allowedAttributes" value="${relationshipAttributesContext.getAllowedAttributeValuesAsJson()}" />

<sec:authentication property="principal.securityProfile.securityTeam.teamId" var="teamId"/>
<sec:authentication property="principal.securityProfile.securityTeam.teamName" var="teamName"/>
<sec:authentication property="principal.securitySubject.subjectId" var="currentUserPersonId"/>

<script>
	

	Y.use('yui-base','event-custom','model','handlebars-helpers',
	      'handlebars-relationship-templates','multi-panel-popup',
	      'multi-panel-model-errors-plugin','usp-relationship-PersonPersonRelationship',
	      'usp-relationshipsrecording-AsymmetricPersonPersonRelationshipDetail',
	      'usp-relationship-PersonPersonRelationshipDetails',
	      'usp-relationship-SuggestedPersonPersonRelationships',
	      'model-form-link','model-transmogrify','relationship-dialog-views',
	      'json-parse','usp-relationship-helper',
	      'usp-date','escape',
	      'app-relationship-suggestions-list','array-extras', function(Y){
		var L=Y.Lang,A=Y.Array,E=Y.Escape,		
		familialRelationshipRoles=A(Y.JSON.parse('${esc:escapeJavaScript(familialRelationshipRoles)}')),
    	socialRelationshipRoles=A(Y.JSON.parse('${esc:escapeJavaScript(socialRelationshipRoles)}')),
    	professionalRelationshipRoles=A(Y.JSON.parse('${esc:escapeJavaScript(professionalRelationshipRoles)}')),
    	
    	  //create new nominatedPersonModel
        nominatedPersonModel=new Y.Model({
            id:'<c:out value="${person.id}"/>',
            personId:'${esc:escapeJavaScript(person.personIdentifier)}',
            forename:'${esc:escapeJavaScript(person.forename)}',
            surname:'${esc:escapeJavaScript(person.surname)}',
            name:'${esc:escapeJavaScript(person.name)}',
            gender:'${esc:escapeJavaScript(person.gender)}',
            dob:'<c:out value="${person.dateOfBirth.getCalculatedDate()}"/>',
            dateOfBirthEstimated:'<c:out value="${person.dateOfBirthEstimated}"/>',
            age: '${esc:escapeJavaScript(person.age)}',
            lifeState: '${esc:escapeJavaScript(person.lifeState)}',
            dueDate:'<c:out value="${person.dueDate.getTime()}"/>'
        }),
		
        //action after a cancel button is clicked
        cancelDialog = function(e) {
           e.preventDefault();
           myPopup.hide();
           // Ensure touch devices return to top of page
           // This is important for the graph in particular since the panning tool
           // is required to navigate.
           window.scrollTo(0,0);  
        },
        
        //action after a proceed button is clicked
        proceedDialog = function(e) {
          
          var view = this.get('activeView');

          e.preventDefault();
     
          if(view.get('relationshipType')==='OrganisationRelationshipType'){
        	  this.hide();
        	  Y.fire('personOrganisationRelationship:showDialog', {
                  action: 'addPersonOrganisationRelationship',
                  subject: view.get('subject'),
                  targetPersonId: nominatedPersonModel.get('id'),
                  targetPersonName:nominatedPersonModel.get('name')
              });              
          }else if(view.get('relationshipType')==='ClientTeamRelationshipType'){
        	  this.hide();
        	  Y.fire('personOrganisationRelationship:showDialog', {
                  action: 'addPersonTeamRelationship',
                  clientTeamId: view.get('clientTeamId'),
                  subject: view.get('subject'),
                  targetPersonId: nominatedPersonModel.get('id'),
                  targetPersonName:nominatedPersonModel.get('name'),
              });
        	  
          }else {

          Y.fire('relationship:showDialog', {
              action: 'add',
              subject: view.get('subject'),
              targetPersonId: view.get('targetPersonId'),
              targetPersonName: view.get('targetPersonName'),
              personRoles:view.get('targetPersonTypes'),
              relatedPerson: view.get('relatedPerson'),
              relatedPersonRoles:view.get('relatedPersonRoles'),
              relationshipType: view.get('relationshipType') || 'FamilialRelationshipType',
              relationshipTypes: {
                FamilialRelationshipType:'familial',
                SocialRelationshipType:'social',
                ProfessionalRelationshipType:'professional'
              }
          });   
          
          }
        },
        
        NewPersonPersonRelationshipTypeForm = Y.Base.create("NewPersonPersonRelationshipTypeForm", Y.usp.relationship.NewPersonPersonRelationship, [Y.usp.ModelFormLink], { 
            url: '<s:url value="/rest/personPersonRelationship"/>',
            form:'#relationshipAddRelationshipTypeForm'
        }),
        
   
        //Mix in ModelFormLink to model where necessary
        NewPersonPersonRelationshipForm=Y.Base.create("NewPersonPersonRelationshipForm", Y.usp.relationship.NewPersonPersonRelationship, [Y.usp.ModelFormLink],{
			url:'<s:url value="/rest/personPersonRelationship"/>',
			form:'#relationshipAddForm'
        }),
        //This model will be transformed into a UpdatePersonRelationship model when it is submitted back to the server
        UpdatePersonPersonRelationshipForm=Y.Base.create("UpdatePersonPersonRelationshipForm", Y.usp.relationshipsrecording.AsymmetricPersonPersonRelationshipDetail, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
			url:'<s:url value="/rest/personPersonRelationship/{id}?checkInactive=false&nominatedPersonId={personId}"/>',
			form:'#relationshipEditForm'
        }),
      	//This model will be transformed into a UpdateClosePersonPersonRelationship model when it is submitted back to the server
        ClosePersonPersonRelationshipForm=Y.Base.create("ClosePersonPersonRelationshipForm", Y.usp.relationshipsrecording.AsymmetricPersonPersonRelationshipDetail, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
			//The URL will be updated during the save operation
			url:'<s:url value="/rest/personPersonRelationship/{id}?checkInactive=false&nominatedPersonId={personId}"/>',
			form:'#relationshipCloseForm',
			//Provide our own implementation of getURL to update URL on save
			getURL:function(action, options){
				if(action==='update'){
					return this._substituteURL('<s:url value="/rest/personPersonRelationship/{id}/status?checkInactive=false&action=close"/>', Y.merge(this.getAttrs(), options));
				}
				//return standard operation
				return ClosePersonPersonRelationshipForm.superclass.getURL.apply(this, [action, options]);
			}
        }),
        
        commonLabels={
        	type: '<s:message code="relationship.type"/>',
        	role: '<s:message code="relationship.role"/>',
        	qualifier:'<s:message code="relationship.qualifier"/>',
        	relationship: '<s:message code="relationship.relationship"/>',
        	relatedPersonName: '<s:message code="relationship.relatedPersonName"/>',
        	inLaw: '<s:message code="relationship.inLaw"/>',
        	adoptive: '<s:message code="relationship.adoptive"/>',
        	half: '<s:message code="relationship.half"/>',
        	step: '<s:message code="relationship.step"/>',
        	multipleBirth: '<s:message code="relationship.multipleBirth" javaScriptEscape="true"/>',
        	startDate: '<s:message code="relationship.startDate"/>',
        	startDateEstimated: '<s:message code="relationship.startDateEstimated"/>',
        	relatedProfessionalTeamSelect: '<s:message code="relationship.relatedProfessionalTeamSelect"/>',
        	closeDate: '<s:message code="relationship.closeDate"/>',
        	closeReason: '<s:message code="relationship.closeReason"/>',
        	endConfirm: '<s:message code="relationship.endConfirm"/>',
        	deleteConfirm: '<s:message code="relationship.deleteConfirm"/>',   			
        	viewRelationshipMessage: "<s:message code='relationship.viewRelationshipMessage'/>",
        	editRelationshipMessage: "<s:message code='relationship.editRelationshipMessage'/>",
        	closeRelationshipMessage: "<s:message code='relationship.closeRelationshipMessage'/>",
        	deleteRelationshipMessage: "<s:message code='relationship.deleteRelationshipMessage'/>",
        	relatedTeamSearchPlaceholder: "<s:message code='relationship.relatedTeamSearchPlaceholder'/>",
        	OrganisationSearchPlaceholder: "<s:message code='organisation.organisationSearchPlaceholder'/>"
        	
        },
       	myPopup=new Y.usp.MultiPanelPopUp({
			headerContent:'<h3 tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span> Relationship</h3>',
			width:560,
			//Plugin Model Error handling
			plugins:[{fn:Y.Plugin.usp.MultiPanelModelErrorsPlugin}],
			//Views that this dialog supports
			views: {
				
				viewRelationship: {
					type:Y.app.relationships.ViewRelationshipDetailView,
					headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> View relationship</h3>',
					buttons:[{
	                        section: Y.WidgetStdMod.FOOTER,
	                        name: 'editButton',
	                        labelHTML: '<i class="fa fa-pencil-square-o"></i> Edit',
	                        classNames: 'pure-button-active',
	                        action: function(e) {
	                            e.preventDefault();
	                            var model=this.get('activeView').get('model');
	                            Y.fire('relationship:showDialog', {
	            		            id: model.get('id'),
	            		            personId: model.get('roleAPersonId')!==model.get('personId')?model.get('roleAPersonId'):model.get('roleBPersonId'),
	            		            action: 'edit'
	            		        });	                            
	                        },
	                        disabled: true
	                    },
	                    /*{
	                    	name: 'moreButton',
	                    	name:       'moreButton',
							labelHTML:  '<i class="fa fa-ellipsis-h"></i> more',
							classNames: 'pure-button-active',  
	    	                action: function(e) {
	    	                    e.preventDefault();
	    	                    this.hide();
	    	                },
	    	                section: Y.WidgetStdMod.FOOTER
	                    }, 
	                    */
	                    {
	                    	action:     cancelDialog,
	                    	section:    Y.WidgetStdMod.FOOTER,
	                    	name:       'cancelButton',
	                    	labelHTML:  '<i class="fa fa-times"></i> Cancel',
	                    	classNames: 'pure-button-secondary'
	                    }]
                },
                editRelationship: {
                	//this view will be automatically created/destroyed as necessary as it has complex DOM elements
                	type:Y.app.relationships.UpdateRelationshipView,
            		headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Edit relationship</h3>',
            		buttons:[{
	                        section: Y.WidgetStdMod.FOOTER,
	                        name: 'saveButton',
	                        labelHTML: '<i class="fa fa-check"></i> Save',
	                        classNames: 'pure-button-primary',
	                        action: function(e) {
	                            // Get the view and the model
	                            var view=this.get('activeView'),
	                            model=view.get('model'),     
	                            relationshipType=model.get('typeDiscriminator');
	                            var _instance = this;
	                  			
	                            e.preventDefault();
	                  			
	                            //show the mask
	                            _instance.showDialogMask();
	                            
	                            model.setAttrs(view.attributesPanel.getValues('Update'), { 
	                            	silent: true                             	
	                            });

                               // If the dropdown #professional-teams-select is hidden (start date when a professional is not a team member)
                               // or professionalTeamId is not present on the model (where professional has no team - None Recorded) , don't map professionalTeamId.
                               var professionalTeamsNode = view.get('container').one('#professional-teams-select');
                               if(professionalTeamsNode && professionalTeamsNode.get('hidden') || !model.get('professionalTeamId')) {
                                    model.removeAttr('professionalTeamId');
                               }

	                            model.save({ transmogrify:Y.usp.relationship.UpdatePersonPersonRelationship }, function(err, response){
	                            	  
	                            		//hide the mask
	                  		          _instance.hideDialogMask();
	                            		if (err===null){
                                            myPopup.hide();
                                            window.scrollTo(0,0); 
                            		        Y.fire('relationship:dataChanged', {
                            		            relationshipType: relationshipType,
                            		            relationshipRole: model.get('relationship'),
                            		            action: 'edit'
                            		        });
                            		        Y.fire('infoMessage:message', {message:'Relationship successfully updated'});
                            		        Y.fire('allocatedWorkers:dataChanged', {});
                            		        Y.fire('namedPersons:dataChanged', {});
                                        }
                                    })
	                        },
	                        disabled: true
	                    },
	                    {
	                    	action:     cancelDialog,
	                    	section:    Y.WidgetStdMod.FOOTER,
	                    	name:       'cancelButton',
	                    	labelHTML:  '<i class="fa fa-times"></i> Cancel',
	                    	classNames: 'pure-button-secondary'
	                    }]
                },
                closeRelationship: {
                	//this view will be automatically created/destroyed as necessary as it has complex DOM elements
                	type:Y.app.relationships.CloseRelationshipView,
            		headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> End relationship</h3>',
            		buttons:[{
	                        section: Y.WidgetStdMod.FOOTER,
	                        name: 'yesButton',
	                        labelHTML: '<i class="fa fa-check"></i> yes',
	                        classNames:'pure-button-primary',		
	                        action: function(e) {
	                            // Get the view and the model
	                            var view=this.get('activeView'),
	                            model=view.get('model'),     
	                            relationshipType=model.get('typeDiscriminator');
                                var _instance = this;
                                e.preventDefault();
	                  			//show the mask
	                            _instance.showDialogMask();
	                  			
	                            model.save({
                            			transmogrify:Y.usp.relationship.UpdateClosePersonPersonRelationship
                            		},function(err, response){
                            			//hide the mask
                      		          _instance.hideDialogMask();
                            			if (err===null){
                                            myPopup.hide();
                            		        Y.fire('relationship:dataChanged', {
                            		            relationshipType: relationshipType,
                            		            relationshipRole: model.get('relationship'),
                            		            action: 'close'
                            		        });
                            		        
                            		        Y.fire('infoMessage:message', {message:'Relationship successfully ended'});
                            		        Y.fire('allocatedWorkers:dataChanged', {});
                                        Y.fire('namedPersons:dataChanged', {});
                                        }                                       
	                            	});
	                        },
	                        disabled: true
	                    },
	                    {
	                    	name: 'noButton',
	    	                labelHTML: '<i class="fa fa-times"></i> No',
	    	                classNames: 'pure-button-secondary',
	    	                action: function(e) {
	    	                    e.preventDefault();
	    	                    this.hide();
	    	                },
	    	                section: Y.WidgetStdMod.FOOTER
	    			}]
                },
                deleteRelationship: {
                	//this view will be automatically created/destroyed as necessary as the model will be destroyed by the delete
                	type:Y.app.relationships.DeleteRelationshipDetailView,
            		headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Remove relationship</h3>',
            		buttons:[{
	                        section: Y.WidgetStdMod.FOOTER,
	                        name: 'yesButton',
	                        labelHTML: '<i class="fa fa-check"></i> yes',
							classNames:'pure-button-primary',
	                        action: function(e) {
	                            // Get the view and the model
	                            var view=this.get('activeView'),
	                            model=view.get('model'),     
	                            relationshipType=model.get('typeDiscriminator');
	                            
                                var _instance = this;
                                e.preventDefault();
	                  			//show the mask
	                            _instance.showDialogMask();
	                            
	                            // do the delete
	                            model.destroy({remove:true}, 
	                            function(err, response){
	                            	_instance.hideDialogMask();   
	                            	if (err===null){
	                                            myPopup.hide();
	                            		        Y.fire('relationship:dataChanged', {
	                            		            relationshipType: relationshipType,
	                            		            relationshipRole: model.get('relationship'),
	                            		            action: 'delete'
	                            		        });

	                                Y.fire('infoMessage:message', {message:'Relationship successfully removed'});
	                                Y.fire('allocatedWorkers:dataChanged', {});
                                  Y.fire('namedPersons:dataChanged', {});
	                              }
	                            });
	                        },
	                        disabled: true
	                    },
	                    {
	                        section: Y.WidgetStdMod.FOOTER,
	                        name: 'noButton',
	                        labelHTML: '<i class="fa fa-times"></i> No',
	                        classNames: 'pure-button-secondary',
	    	                action: function(e) {
	    	                    e.preventDefault();
	    	                    this.hide();
	    	                },
	    	                disabled: true
	    			}]
                },
                suggestionsView: {
                	//this view will be automatically created/destroyed as necessary as it has complex DOM elements
                	type:Y.app.relationships.RelationshipSuggestionsView,
            		headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Relationship suggestions</h3>',
            		buttons:[
					{
					    section: Y.WidgetStdMod.FOOTER,
					    name: 'goToGraph',
					    labelHTML: '<i class="fa fa-sitemap"></i> Show on Graph',
					    classNames: 'pure-button-active',
					    action: function(e) {
					        e.preventDefault();
					        this.hide();
					        
					        var view=this.get('activeView');
					        model=view.get('model');
					        var nominatedPersonId = model.get("id");
					        var newRelationshipId = view.get("newRelationshipId");
					        
					        Y.fire('relationshipFilterModel:reset');
					        if(Y.one('.usp-graph-content')){    	                    	
    	                    	Y.fire('relationship:dataChanged', {    	            		           
    	            		            action: 'add',
    	            		            newRelationshipId: newRelationshipId
    	            		        });
    	                    }else{
    	                    	window.location=L.sub('<s:url value="/relationships/persondiagram?id={personId}&suggestionsRelationshipId={suggestionsRelationshipId}" />',{personId:nominatedPersonId, suggestionsRelationshipId:newRelationshipId});	
    	                    }
					    },
					    disabled: true
					},
                    {
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'noButton',
                        labelHTML: '<i class="fa fa-times"></i> Cancel',
                        classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
                        disabled: true
                    }
                    ]
                },
                saveSuggestedRelationship: {                	
                	type:Y.app.relationships.SaveSuggestedRelationshipView,
            		headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Save suggestion </h3>',
            		buttons:[{
	                        section: Y.WidgetStdMod.FOOTER,
	                        name: 'saveButton',
	                        labelHTML: '<i class="fa fa-check"></i> Save',
	                        classNames: 'pure-button-primary',
	                        action: function(e) {
	                        	e.preventDefault();
	                        	// Get the view and the model
	                            var view=this.get('activeView');	                        	
	                            model=view.get('model');
                                var _instance = this;
	                  			//show the mask
	                            _instance.showDialogMask();

	                            var roleAPersonId = null;
	                            var roleBPersonId = null;
	                            
	                            var relationshipTypeId = view.get("relationshipTypeId");
	                            var qualifier = view.get("qualifier");
	                            var role = view.get("role");
	                            var half = null;
	                            var step = null;
	                            var inLaw = null;
	                            var adoptive = null;
	
	                            var suggestionRelationshipId = view.get("suggestionRelationshipId");
	                            
	                            var relationshipType = view.get("relationshipTypeDescriptor");
	                            
	                            var professionalTeamId = view.get("professionalTeamId");
	                            
	                            var professionalTeamName = view.get("professionalTeamName");
	                            
	                            if(qualifier == 'HALF'){
	                            	half = true;
	                            }else if(qualifier == 'STEP'){
	                            	step = true;
	                            }else if(qualifier == 'IN_LAW'){
	                            	inLaw = true;
	                            }else if(qualifier == 'ADOPTIVE'){
	                            	adoptive = true;
	                            }
	                            
	                            if(role == 'ROLE_A'){
	                            	roleAPersonId = view.get("roleAPersonId");
	                            	roleBPersonId = view.get("roleBPersonId");
	                            }else{
	                            	roleAPersonId = view.get("roleBPersonId");
	                            	roleBPersonId = view.get("roleAPersonId");
	                            }
	                            
	                            model.setAttrs({
            	            		personPersonRelationshipTypeId:relationshipTypeId,
            	            		roleAPersonId: roleAPersonId,
									roleBPersonId: roleBPersonId,
									half: half,
									step: step,
									inLaw: inLaw,
									adoptive: adoptive,
									saveAutomaticRelationships: true,
									relationshipType: relationshipTypeId,
									selectedPerson: roleBPersonId,
									professionalTeamId: professionalTeamId,
									professionalTeamName: professionalTeamName
            	            	},{
            	            		//don't trigger the onchange listener as we will re-render after save
            	            		silent:true
            	            	});
	                            
	                            model.save(function(err, response){
	                            	
	                               //hide the mask
	              		          _instance.hideDialogMask();
	                            	if (err===null){
	                            		 	myPopup.hide();
	                            		 	window.scrollTo(0,0);
	                            		 	if(Y.one('.usp-graph-content')){
	                            		 			                    	
	                            		 		  var responseData = Y.JSON.parse(response.responseText);
	                            		 		  var AUTO_GENERATED_ID,                                       // Constant holding an ID used by session storage
	                                          automaticRelationships = [],                             // an array of new automatic relationships
	                                          automaticRelationshipsList = [],                         // an array of existing automatic relationships
	                                          Storage;                                                 // Session storage object
	                                             
	                                            
	                                         automaticRelationships = responseData.secondaryResourceURIs; // Our automatic relationship id's begin life as url's
	                                         AUTO_GENERATED_ID = 'automaticRelationshipsList';
	                                         Storage = Y.uspSessionStorage;
	                                                
	                                         if(Storage.hasStoreValue(AUTO_GENERATED_ID)) {
	                                           automaticRelationshipsList = Y.JSON.parse(Storage.getStoreValue(AUTO_GENERATED_ID));
	                                           automaticRelationships.forEach(function(url) {         // extract id from url
	                                             var id = url.split('/').slice(-1).toString();
	                                             automaticRelationshipsList.push(id);
	                                           });
	                                         }
	                                         // ..store automatic relationships
	                                         Storage.setStoreValue(AUTO_GENERATED_ID, Y.JSON.stringify(automaticRelationshipsList));
	                            		 		
	                            		 			//check if the suggestions button is enabled
	                            		 			// if enabled then refresh the graph with person suggestions
	                            		 			// else with new relationship id
	                            		 			
	                            		 			if(Y.one('#suggestionsToggle').hasClass('active')){	                            		 				
	                            		 				Y.fire('relationship:dataChanged', {    	            		           
	                            				            action: 'add',
	                            				            includePersonSuggestions: true
	                            				  });	                            		 				
	                            		 			}else{
		                            		 			Y.fire('relationship:dataChanged', {    	            		           
		                    	            		            action: 'add',
		                    	            		            newRelationshipId: suggestionRelationshipId
		                    	            		     });
	                            		 			}
	                    	                   
	             	                    	}
	                            		 	
	                            		 	var personId = nominatedPersonModel.get('id');
	                            		 	
	                            		 	// show infom message if the saved suggestion may not be visible because of filter options & secondary button(off)
	                        		 		var relationshipStartDate = Y.USPDate.parseDate(view.getInput('startDate').get('value'));
	                        		 		RelationshipHelper = Y.uspRelationshipHelper; 
	                        		 		var filterMessage = RelationshipHelper.getFilterMessage(personId,roleAPersonId,roleBPersonId,relationshipStartDate,relationshipType);
	 	
	                            		 	if(filterMessage){
	                            		 		filterMessage = filterMessage+' to view the saved suggestion';
	                            		 	}
	                            		 	
	                            		 	// show message if automatic relations are created
	                            		 	var personName = nominatedPersonModel.get('forename')+' '+nominatedPersonModel.get('surname');
	                            		 	
							     			var infomessage = '';             			
							     									
	     									if(automaticRelationships.length == 0){
	    										if(filterMessage){
	    											var message = 'Relationship successfully added';
	    											var messageInfo= message+'<br>'+filterMessage+'</br>';
	    											Y.fire('infoMessage:message', {message:messageInfo});
		    										
	    										}else{
	    											Y.fire('infoMessage:message', {message:'Relationship successfully added'});
	    										
	    										}
	    	                               		
	    									}else{
	    										
	    										//nominatedPersonId
	    										var automaticRelationshipIdsArray = new Array();
	    										automaticRelationships.forEach(function(automaticRelationship){											
	    											var lastSlash = automaticRelationship.lastIndexOf("/");
	    											automaticRelationshipIdsArray.push(automaticRelationship.substring(lastSlash+1));											
	    										});
	    										var queryString = '';
	    										automaticRelationshipIdsArray.forEach(function(automaticRelationshipId){	
	    											queryString = queryString + '&relationshipIds='+automaticRelationshipId;
	    										});
	    										
	    										var url = '<s:url value="/rest/person/"/>';
	    										
	    										var firstAmp = queryString.indexOf("&");
	    										
	    										url = url+personId+'/personRelationship?'+queryString.substring(firstAmp+1);
	    										
	    										var totalNumberOfNewRelationships = automaticRelationships.length;
	    										var relationshipsInNetwork = 0; 
	    										//{sync: true}, {
	    										Y.io(url, 
	    											 { 
	    											sync: true,
	    											headers: {
	    										        'Accept': 'application/json',
	    										    },
	    										    on: {
	    										        success: function(id, e) {
	    										            //Y.log('******** response: '+e.responseText);
	    										            relationshipsInNetwork =  Y.JSON.parse(e.responseText);
	    										        }
	    										    }
	    										});
	    										
	    										//call ajax/rest cal to get no. new relationships in the nominated person's network
	    										
	    										var relationshipsOutsideNetwork = totalNumberOfNewRelationships - relationshipsInNetwork;
	    										var message = 'Relationship successfully added';	
	    										
	    										infomessage = 'The application also created '+totalNumberOfNewRelationships+' new relationship(s)';										
	    										if(relationshipsInNetwork > 0 && relationshipsOutsideNetwork > 0){
	    											infomessage = infomessage + ' - '+relationshipsInNetwork+' within and '+relationshipsOutsideNetwork+' outside '+personName+"'s network.";
	    										}else if(relationshipsInNetwork > 0){
	    											infomessage = infomessage + ' within '+personName+"'s network.";
	    										}else if(relationshipsOutsideNetwork > 0){
	    											infomessage = infomessage + ' outside '+personName+"'s network.";
	    										}
	    										
	    										var messageInfo= message+'<br>'+filterMessage+'</br>'+infomessage;
	    										var messageInfo1=message+'<br>'+infomessage+'</br>';
	    										
	    										if(filterMessage){
	                                		       
	                                		        Y.fire('infoMessage:message', {message:messageInfo});
	                                		        Y.fire('relationship:changeTab', {tabIndex:0}); 
	    										}else{
	                                		        Y.fire('infoMessage:message', {message:messageInfo1});
	                                		        Y.fire('relationship:changeTab', {tabIndex:0}); 

	    										}	
	    									}
	                                   		
	                            	 }
	                            });
	                           
	                        },
	                        disabled: true
	                    }]
                },
                
                /**
                 * Add relationship type
                 * =====================
                 */
                 addRelationshipType: {
                   type: Y.app.relationships.NewRelationshipTypeView,
                   headerTemplate: '<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span>Add relationship</h3>',
                   buttons: [
                     {
                       action:     proceedDialog,
                       section:    Y.WidgetStdMod.FOOTER,
                       name:       'proceedButton',
                       labelHTML:  '<i class="fa fa-check"></i> Proceed',
                       classNames: 'pure-button-primary'
                     },
                     {
                    	 action:     cancelDialog,
                    	 section:    Y.WidgetStdMod.FOOTER,
                    	 name:       'cancelButton',
                    	 labelHTML:  '<i class="fa fa-times"></i> Cancel',
                    	 classNames: 'pure-button-secondary'
                     }
                   ] 
                 },
                
                addRelationship: {
                	//this view will be automatically created/destroyed as necessary as it has complex DOM elements
                	type:Y.app.relationships.NewRelationshipView,
                	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Add relationship</h3>',            		buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'saveButton',
                        labelHTML: '<i class="fa fa-check"></i> Save',
                        classNames: 'pure-button-primary',
                        action: function(e) {
                        	var _instance = this;
                        	e.preventDefault();
                                                        
                            // Get the view and the model
                            var view=this.get('activeView'),
                            model=view.get('model'),
                            
                            // Use the selected values of the relationship type and role 
                            // inputs to determine the relationshipTypeId and on which 
                            // ends of the relationship each person belongs
                            relationshipTypeId=null,
                            roleEnd=null,
                            role=null,
                            roleTokens=null,
                            relationshipType=view.get('relationshipType'),
                            relatedPersonId= view.get('relatedPersonId'),
                            relationshipRole=view.getInput('relationshipRole').get("options").item(view.getInput('relationshipRole').get('selectedIndex')).get('value');                            
                            var findRole=function(roles, id){
                            	return A.find(roles||[], function(role){
                            		return role.id===id;
                            	});
                            };
                            
                            //find the role
                            if (relationshipType==="FamilialRelationshipType") {
                                role=findRole(view.get('familialRelationshipRoles'),relationshipRole);
                            } else if(relationshipType==="SocialRelationshipType") {
                            	role=findRole(view.get('socialRelationshipRoles'),relationshipRole);
                            } else if(relationshipType==="ProfessionalRelationshipType") {
                            	role=findRole(view.get('professionalRelationshipRoles'),relationshipRole);
                            }

          					// Check if we are adding an underage full parent.  If so warn the user and
          					// ask for confirmation.
          					if (!view.getInput('adoptive').get('checked') && 
          						!view.getInput('half').get('checked') &&
          						!view.getInput('step').get('checked') &&
          						!view.getInput('inLaw').get('checked')) {
		          				
		          				var minAncestorAge;
		          				var dueOrBorn ='Born';
		          				var isAncestorRole=false;
		          				if(role && role.minAncestorAge){
                                    minAncestorAge=role.minAncestorAge;
                                    isAncestorRole=role.isAncestorRole;
		          				}
		          				if (minAncestorAge) {
		          					if (isAncestorRole) {
		          						var childDob=nominatedPersonModel.get('dob');
		          						if(!childDob &&  nominatedPersonModel.get('lifeState')==='UNBORN'){
		          							childDob=(nominatedPersonModel.get('dueDate'))?Y.Date.format(Y.Date.parse(nominatedPersonModel.get('dueDate')),{format:"%F"}): null
		          							dueOrBorn = 'Due';
		          						}
		          						var parentDob=view.get('relatedPersonDoB');
		          						var parentName=view.get('relatedPersonName');
		          						var childName=nominatedPersonModel.get('forename')+' '+nominatedPersonModel.get('surname');
		          					} else {
		          						var parentDob=nominatedPersonModel.get('dob');
		          						var childDob=view.get('relatedPersonDoB');
		          						if(!childDob &&  view.get('relatedPersonLifeState')==='UNBORN'){
		          							childDob=view.get('relatedPersonDueDate')
		          							dueOrBorn = 'Due';
		          						}
		          						var childName=view.get('relatedPersonName');
		          						var parentName=nominatedPersonModel.get('forename')+' '+nominatedPersonModel.get('surname');
		          					}
		          					var ageDiff=getAgeDifference(parentDob,childDob);
		          					if (ageDiff <= minAncestorAge && ageDiff >= 0) {
		          						var aOrAn='a';
		          						if (ageDiff===8 || ageDiff===11) {
		          							aOrAn='an';	
		          						}

		          						
		          						var tempView = Y.Base.create('tempView', Y.View,[],{
		          			            	render: function() {
		          			            		var labels = this.get("labels");                		
		          			            		this.get("container")
		          			            		.setHTML('<div class="pure-g-r"><div class="pure-u-1 l-box"><div id="underageWarning" class="pure-alert pure-alert-block pure-alert-warning"><h4>Are you sure '+E.html(parentName)+' (Born: '+Y.USPDate.formatDateValue(parentDob)+') is the underage parent of '+E.html(childName)+' ('+dueOrBorn+': '+Y.USPDate.formatDateValue(childDob)+')?  There is '+aOrAn+' '+ageDiff+' year age difference.</h4></div></div></div>');
		          			            	}
		          						}); 
		          						myConfirm=new Y.usp.MultiPanelPopUp({
		          							width:560,
		          							buttons:[{
		          			            		name: 'noButton',
		          			                	labelHTML: '<i class="fa fa-times"></i> No',
		          			                	classNames: 'pure-button-secondary',
		          			                	action: function(e) {
		          			                    	e.preventDefault();
		          			                    	this.hide();
		          			                	},
		          			                	section: Y.WidgetStdMod.FOOTER
		          							},'close'],
		          							//Plugin Model Error handling
		          							plugins:[],			
		          							views: {
		          						 		confirmSave: {
		          			               			type:tempView,
		          			            			headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-sitemap fa-inverse fa-stack-1x"></i></span> Add relationship</h3>',
		          			        				buttons:[{
		          			                    		section: Y.WidgetStdMod.FOOTER,
		          			                    		name: 'saveButton',
		          			                    		labelHTML: '<i class="fa fa-check"></i> Yes', 
		          			                    		classNames:'pure-button-primary',
		          			                    		action: saveAddRelationshipAfterConfirm,
		          			                    		disabled: false
		          			                		}]
		          								}
		          							}
		          						});
		          						myConfirm.set('zIndex',1000);
		          						myConfirm.render();
		          						myConfirm.showView("confirmSave",
		          			        		{labels:commonLabels}, // attributes for View
		          			            	{modelLoad:false, payload:{}} //options for MultiPanel
		          			        	);
		          						return;
	          						}
		          				
	         					}
                        	}
	          				_instance.showDialogMask();
	          				
	          				function getAgeDifference(parentDob, childDob) {
	          				    var parentDate = new Date(parentDob);
	          				    var childDate = new Date(childDob);
	          				    var age = childDate.getFullYear() - parentDate.getFullYear();
	          				    var m = childDate.getMonth() - parentDate.getMonth();
	          				    var dt = childDate.getDate() -  parentDate.getDate();
	          				    if (age > 0 && (m < 0 || (m === 0 && dt < 0))) {
	          				      
	          				    	age--;
	          				    }
	          				    return age;
	          				}
	          				
	          				function saveAddRelationshipAfterConfirm() {
	          					myConfirm.hide();
	          					saveAddRelationship();
	          				}
	          				
	          				function saveAddRelationship () {
	          					
	          					model.setAttrs({
									saveAutomaticRelationships: true
	        	            	},{
	        	            		//don't trigger the onchange listener as we will re-render after save
	        	            		silent:true
	        	            	});
								
								if (role && role.roleClass==='UNION'){
									var unionVal=Y.one('input[name=unionType]:checked').get('value');
								    var unionRole;
								    
		                            if (relationshipType==="FamilialRelationshipType") {
		                            	unionRole=findRole(view.get('familialRelationshipRoles'),unionVal);
		                            } else if(relationshipType==="SocialRelationshipType") {
		                            	unionRole=findRole(view.get('socialRelationshipRoles'),unionVal);
		                            } else if(relationshipType==="ProfessionalRelationshipType") {
		                            	unionRole=findRole(view.get('professionalRelationshipRoles'),unionVal);
		                            }
		                            if(unionRole){
		                            	role=unionRole;   
		                            }
	            	            }

								if(role){
                                    roleTokens=role.id.split('_');
                                    if (roleTokens.length==2){
                                        relationshipTypeId = roleTokens[0];
                                        roleEnd = roleTokens[1];
                                    }
                                }
								
								model.setAttrs({        	            		
									relationshipType: relationshipTypeId,
									selectedPerson: view.get('relatedPersonId')
	        	            	},{
	        	            		//don't trigger the onchange listener as we will re-render after save
	        	            		silent:true
	        	            	});
								var roleAPersonId,roleBPersonId;
	               	            if (relationshipTypeId!=null && roleEnd!=null){
	               	            	roleAPersonId = roleEnd=='A'?view.get('relatedPersonId'):view.get('targetPersonId');
	               	            	roleBPersonId = roleEnd=='B'?view.get('relatedPersonId'):view.get('targetPersonId')
	            	            	model.setAttrs({
	            	            		personPersonRelationshipTypeId:Number(relationshipTypeId),
	            	            		roleAPersonId: roleAPersonId,
										roleBPersonId: roleBPersonId
	            	            	},{
	            	            		//don't trigger the onchange listener as we will re-render after save
	            	            		silent:true
	            	            	});
	            	            }
	            	            else {
	            	            	roleAPersonId = view.get('targetPersonId');
	            	            	roleBPersonId = view.get('relatedPersonId');
	            	            	//for other relationship type we don't care on which end the relationship each person goes
	            	            	model.setAttrs({
	            	            		roleAPersonId:roleAPersonId ,
										roleBPersonId:roleBPersonId 
	            	            	},{
	            	            		//don't trigger the onchange listener as we will re-render after save
	            	            		silent:true
	            	            	});            	            	
	            	           	}
	               	            
	               	            //save our arribute values silently
	               	            var attribs = view.attributesPanel.getValues('Update');
	               	            	
	               	            	if(!Y.Object.isEmpty(attribs)) {                              
	               	            		model.setAttrs(attribs, {                               
	                                  silent:true                               
	                                }); 
	                                
	               	            	}
	            
	               	           
	            	            // Do the save            	 
	            	            model.save(function(err, response){
	                            	
	                            	_instance.hideDialogMask();
	                            	var $relationshipRoleError;
	                            	var $selectedPersonError;
	                            	var $startDateError;
	                            	
	                            	if(err != null){
	                            	
	                            		$startDateError = _instance.bodyNode.one('a[href="#startDate"]');
	                                    $relationshipRoleError = _instance.bodyNode.one('a[href="#relationshipType"]'); // note the type not role - possible change
	                            		$selectedPersonError = _instance.bodyNode.one('a[href="#selectedPerson"]'); 
	                            		
	                            		if($relationshipRoleError) {
	                                		view.getInput('relationshipRole').addClass("error");
	                                		$relationshipRoleError.on('click', function(e) {
	                                			view.getInput('relationshipRole').focus();
	                                		});
	                                	} 
	                            		
	                            		if($startDateError && view.getInput('startDate').get('value')) {
	                            			if($startDateError.get('text')!=='Invalid date') {
	                            			$startDateError.set('text', 'Invalid date');	
	                            			}
	                                	}
	                               	
	                            		if($selectedPersonError){
	                                		view.getInput('relatedPersonSearch').addClass("error");
	                                		$selectedPersonError.on('click', function(e) {
	                                   			view.getInput('relatedPersonSearch').focus();
	                                		});
	                                	}                                	
	                                }else if (err===null){                   
	                                    
	                                        //Start - check for any suggestions and display the suggestions overlay
	                                     var newResourceID = model.get('id');
	                                        
	                                     //End -check for any suggestions and display the suggestions overlay
										  var responseData = Y.JSON.parse(response.responseText);
										  var infomessage = '';
										
										  var AUTO_GENERATED_ID,                                       // Constant holding an ID used by session storage
										  automaticRelationships = [],                             // an array of new automatic relationships
										  automaticRelationshipsList = [],                         // an array of existing automatic relationships
									      Storage;                                                 // Session storage object
									                  
								          automaticRelationships = responseData.secondaryResourceURIs; // Our automatic relationship id's begin life as url's
								          AUTO_GENERATED_ID = 'automaticRelationshipsList';
						              	  Storage = Y.uspSessionStorage;
						                                    
						                  if(Storage.hasStoreValue(AUTO_GENERATED_ID)) {
						                    automaticRelationshipsList = Y.JSON.parse(Storage.getStoreValue(AUTO_GENERATED_ID));
						                  }
								                  
						                  automaticRelationships.forEach(function(url) {         // extract id from url
						                    var id = url.split('/').slice(-1);
						                    automaticRelationshipsList.push(id);
						                  });
						
								          Storage.setStoreValue(AUTO_GENERATED_ID, Y.JSON.stringify(automaticRelationshipsList));
								              
						                  Y.fire('relationship:dataChanged', {
						                      relationshipType: relationshipType,
						                      relationshipRole: relationshipRole,
						                      action: 'add'
						                  });
						                  
						                  window.scrollTo(0,0);	
										  
									     // checking filters  
										// show infom message if the saved suggestion may not be visible because of filter options & secondary button(off)
			                       		 	
		                       		 	var nominatedPersonId = nominatedPersonModel.get('id');
		                       		    var roleAPersonId = model.get('roleAPersonId');
		                       		    var roleBPersonId = model.get('roleBPersonId');
		                       		    var relationshipStartDate = Y.USPDate.parseDate(view.getInput('startDate').get('value'));
	                    		 		
	                    		 		RelationshipHelper = Y.uspRelationshipHelper; 
	                    		 		var filterMessage = RelationshipHelper.getFilterMessage(nominatedPersonId,roleAPersonId,roleBPersonId,relationshipStartDate,relationshipType);

		                       		 	if(filterMessage){
		                       		 		filterMessage = filterMessage+' to view the saved relationship.';
		                       		 	}
										  
										 // checking filters ends
										if(automaticRelationships.length == 0){
											
											if(filterMessage){											
												var message = 'Relationship successfully added';
												var messageForFilter= message+'<br>'+filterMessage+'</br>';
								
												Y.fire('infoMessage:message', {message:messageForFilter});
												Y.fire('relationship:changeTab', {tabIndex:0}); 
												Y.fire('allocatedWorkers:dataChanged', {});
                        Y.fire('namedPersons:dataChanged', {});
																						
											}else{
	                            		        Y.fire('infoMessage:message', {message:'Relationship successfully added'});
	                            		        Y.fire('relationship:changeTab', {tabIndex:0}); 
	                            		        Y.fire('allocatedWorkers:dataChanged', {});
											}
										}else{
											
											//nominatedPersonId
											var automaticRelationshipIdsArray = new Array();
											automaticRelationships.forEach(function(automaticRelationship){											
												var lastSlash = automaticRelationship.lastIndexOf("/");
												automaticRelationshipIdsArray.push(automaticRelationship.substring(lastSlash+1));											
											});
											
											var queryString = '';
											automaticRelationshipIdsArray.forEach(function(automaticRelationshipId){	
												queryString = queryString + '&relationshipIds='+automaticRelationshipId;
											});
											
											var url = '<s:url value="/rest/person/"/>';
											
											var firstAmp = queryString.indexOf("&");
											
											url = url+nominatedPersonModel.get('id')+'/personRelationship?'+queryString.substring(firstAmp+1);
											
											var totalNumberOfNewRelationships = automaticRelationships.length;
											var relationshipsInNetwork = 0; 
											//{sync: true}, {
											Y.io(url, 
												 { 
												sync: true,
											    on: {
											        success: function(id, e) {										           
											            relationshipsInNetwork =  Y.JSON.parse(e.responseText);
											        }
											    }
											});
											
											//call ajax/rest cal to get no. new relationships in the nominated person's network
											
											var relationshipsOutsideNetwork = totalNumberOfNewRelationships - relationshipsInNetwork;
											var message = 'Relationship successfully added';										
											var personName = nominatedPersonModel.get('forename')+' '+nominatedPersonModel.get('surname');
											
											infomessage = 'The application also created '+totalNumberOfNewRelationships+' new relationship(s)';										
											if(relationshipsInNetwork > 0 && relationshipsOutsideNetwork > 0){
												infomessage = infomessage + ' - '+relationshipsInNetwork+' within and '+relationshipsOutsideNetwork+' outside '+personName+"'s network.";
											}else if(relationshipsInNetwork > 0){
												infomessage = infomessage + ' within '+personName+"'s network.";
											}else if(relationshipsOutsideNetwork > 0){
												infomessage = infomessage + ' outside '+personName+"'s network.";
											}
											
											if(filterMessage){
	                            		      
											   var messageInfo= message+'<br>'+filterMessage+'</br>'+infomessage;
											   Y.fire('infoMessage:message', {message:messageInfo});

											}else{
											  var messageInfo2= message+'<br>'+infomessage+'</br>';	
											  Y.fire('infoMessage:message', {message:messageInfo2});
											}
											
										}
											
											if(newResourceID != null) {
												var relationshipSuggestionsList = new Y.app.RelationshipSuggestionsList({
													url: '<s:url value="/rest/personPersonRelationship/'+newResourceID+'/suggestions?relationshipSuggestionsFilter=SUGGESTED_RELATIONS"/>'													
												}); 
	                        		        
	                        		        //Mix in ModelFormLink to model where necessary
	                        		        SuggestedPersonPersonRelationshipForm=Y.Base.create("SuggestedPersonPersonRelationshipForm", Y.usp.relationship.NewPersonPersonRelationship, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],
	                        		        {
	                        					url:'<s:url value="/rest/personPersonRelationship"/>',
	                        					form:'#SuggestedPersonPersonRelationshipForm_28'
	                        		        },	
	                        		        {
	                        		        	ATTRS: {
			                        						roleAForename: {},
			                        						roleASurname: {},
			                        						roleAName: {},
			                        						roleBForename: {},
			                        						roleBSurname: {},
			                        						roleBName: {},
			                        						relationship: {},						
			                        						isHalf: {},
			                        						isStep: {},
			                        						isAdoptive: {},
			                        						isInLaw: {},
			                        						relationshipRole: {},
			                        						hasAnyQualifiers: {},
			                        						roleAGender: {},
			                        						roleBGender: {},
			                        						notContainsOnlyFullQualifier: {},
			                        						relationshipType: {},
			                        						firstPersonRole: {},
			                        						startDateEstimated: {},
			                        						relationshipStartDate: {}
	                        		        	},
	                        		        	panel: {
	                        		          		value: ''
	                        		          	}
	                        		        });   

	                        		        relationshipSuggestionsList.on("error",function(e){ 
	                        		        	Y.log(' no suggestions ');
	                        		        	// close the existing 'add relationship' overlay
	                        		        	myPopup.hide();
	                        		        });
	                        		        
	                        		        relationshipSuggestionsList.on("load",function(e){
	                        		        	
	                        		        	
	                        		        	
	                        		        	RelationshipHelper = Y.uspRelationshipHelper; // Global?
	                        		        	
	                        		        	var suggestionsArray = new Array();
	                        		        	var suggestionsClientIds = new Array();
	                        		        	var nominatedPersonId = view.get('targetPersonId');
	                        		        	var secondaryPersonId = relatedPersonId;
	                        		        	var nominatedPerson =  null;                    	            	
	                        		        	var secondaryPerson =  null;                 	            
	                        		        	var nominatedPersonSuggestions = new Array(); 	                    	            		
	                        		        	var secondaryPersonSuggestions = new Array(); 
	                        		        	
	                        		        	if(this.size()<1) {
	                        		        		Y.log('no suggestions');
	                        		        		myPopup.hide();
	                        		        		return;
	                        		        	}
	                        	            	
	                        	            	
	                        		        	relationshipSuggestionsList.map(function(model,index){                        	            		                   	            		
	                        		        		var firstPerson =  model.get('firstPerson');
	                        		        		var secondPerson = model.get('secondPerson');                        	            		
	                        	            		
	                        		        		if(nominatedPerson == null){  			
	                        		        			if(nominatedPersonId == firstPerson.id){                        	            			
	                        		        				nominatedPerson = firstPerson;                        	            				
	                        		        			} 
	                            	            		
	                        		        			if(nominatedPersonId == secondPerson.id){		
	                        		        				nominatedPerson = secondPerson;	
	                        		        			}
	                        		        		}                        	            		
		                        	            	
	                        		        		if(secondaryPerson == null){
	                        		        			if(secondaryPersonId == firstPerson.id){                        	            		
	                        		        				secondaryPerson = firstPerson;                        	            			
	                        		        			} 
		                        	            		
	                        		        			if(secondaryPersonId == secondPerson.id){
	                        		        				secondaryPerson = secondPerson;
	                        		        			}
	                        		        		}                        	            		
	                        	            	
	                        		        	});
	                        	            	
	                        	            	relationshipSuggestionsList.map(function(model,index){
	                        	            		
	                        	            		var firstPerson =  model.get('firstPerson');
	                        	            		var secondPerson = model.get('secondPerson');
	                        	            		var suggestions = model.get('suggestions');                        	            		
	                        	            		
	                        	            		var roleAPerson = null;
	                        	            		var roleBPerson = null;
	                        	            		
	                        	            		if(nominatedPersonId == firstPerson.id){
	                        	            			roleAPerson = secondPerson;
	                        	            			roleBPerson = firstPerson;
	                        	            		}else if(nominatedPersonId == secondPerson.id){
	                        	            			roleAPerson = firstPerson;
	                        	            			roleBPerson = secondPerson;
	                        	            		}else if(secondaryPersonId == firstPerson.id){
	                        	            			roleAPerson = secondPerson;
	                        	            			roleBPerson = firstPerson;
	                        	            		}else if(secondaryPersonId == secondPerson.id){
	                        	            			roleAPerson = firstPerson;
	                        	            			roleBPerson = secondPerson;
	                        	            		}
	                        	            		
	                        	            		var unionSuggestions = new Array(); 
	                        	            		
	                        	            		suggestions.forEach(function(suggestion){
	                        	            			
	                        	            			if(suggestion.personPersonRelationshipType != null && suggestion.personPersonRelationshipType.description != 'UNION'){
	                        	            				
	                        	            				var suggestionForm = new SuggestedPersonPersonRelationshipForm();
	                            		            		
	                            		            		if(suggestion.personPersonRelationshipType != null){
	                            		            		
	                            		            			suggestionForm.set('roleAForename', roleAPerson.forename);
	                            			            		suggestionForm.set('roleASurname', roleAPerson.surname);
	                            			            		suggestionForm.set('roleAName', roleAPerson.name);
	                            			            		suggestionForm.set('roleBForename', roleBPerson.forename);
	                            			            		suggestionForm.set('roleBSurname', roleBPerson.surname);
	                            			            		suggestionForm.set('roleBName', roleBPerson.name);
	                            			            		suggestionForm.set('relationshipType', suggestion.personPersonRelationshipType._type);    
	                            			            		suggestionForm.set('professionalTeamId', suggestion.professionalTeamId);
	                            			            		suggestionForm.set('professionalTeamName', suggestion.professionalTeamName);
	                            			            		
	                            			            		var relationshipClass = suggestion.personPersonRelationshipType.relationshipClass;                        			            		
	                            			            		var qualifiers = suggestion.permittedQualifiers; 
	                            			            		
	                            			            		var firstPersonRole = null;
	                        			            			if(firstPerson.id == roleAPerson.id){
	                        			            				firstPersonRole = suggestion.firstPersonRole;
	                        			            			}else{
	                        			            				if(suggestion.firstPersonRole == 'ROLE_A'){
	                        			            					firstPersonRole = 'ROLE_B';
	                        			            				}else{
	                        			            					firstPersonRole = 'ROLE_A';
	                        			            				}
	                        			            			}
	                        			            			
	                        			            			suggestionForm.set('firstPersonRole', firstPersonRole); 
	                            		            		    
	                            			            		if(suggestion.personPersonRelationshipType._type != null && 
	                            			            				suggestion.personPersonRelationshipType._type == 'ProfessionalRelationshipType'){                        			            			
	                            			            			if(firstPersonRole == 'ROLE_A'){
	                            			            				suggestionForm.set('relationship', suggestion.personPersonRelationshipType.roleAName);
	                            			            			}else{
	                            			            				suggestionForm.set('relationship', suggestion.personPersonRelationshipType.roleBName);
	                            			            			}
	                            			            		}else{  
	                            			            			
	                            			            			
	                               						            var genderedRelationshipRole = RelationshipHelper.getGenderedRelationshipRole(roleAPerson, roleBPerson, suggestion, firstPersonRole);
	                            			            			                                			            		
	                               						         	suggestionForm.set('relationship', genderedRelationshipRole);
	                               						         	
	                               						         	// set the Youngest person dob as the relationship startdate	                               						         	
	                               						         	if(suggestion.personPersonRelationshipType._type == 'FamilialRelationshipType'
	                               						         			&& suggestion.personPersonRelationshipType.startDateGuidance==="YOUNGEST_PERSON_DATE_OF_BIRTH"){
	                               						         		
	                               						         		var relationshipStartDate = null;
	                               						         	    var startDateEstimated = null;	                               						         		
	                               						         		var roleAPersonDOB = null;
	                               						         		var roleAPersonFormattedDOB = null; 
		                               						         	var roleBPersonDOB = null;
		                               						        	var roleBPersonFormattedDOB = null;
	                               						         		
	                               						         		
	                               						         		if(roleAPerson.dateOfBirth) {
	                               						         			roleAPersonDOB = roleAPerson.dateOfBirth.calculatedDate;
	                               						         			roleAPersonFormattedDOB = Y.USPDate.formatDateValue(roleAPerson.dateOfBirth.calculatedDate);
	                               						         		}
	                               						         		
	                               						         		var roleAPersonDOBEstimated = String(roleAPerson.dateOfBirthEstimated);		
	                               						         		
	                               						         		if(roleBPerson.dateOfBirth) {
	                               						         			roleBPersonDOB =  roleBPerson.dateOfBirth.calculatedDate;
	                               						         			roleBPersonFormattedDOB = Y.USPDate.formatDateValue(roleBPerson.dateOfBirth.calculatedDate)
	                               						         		}
	                               						         		
	                               						         		var roleBPersonDOBEstimated = String(roleBPerson.dateOfBirthEstimated);
	                               						         		
	                               						         		
	                               						         		if(roleAPersonDOB > roleBPersonDOB){
		                               						         		relationshipStartDate = roleAPersonFormattedDOB;
		                               						         		if(roleAPersonDOBEstimated===String(true)){
		                               						         			startDateEstimated = true;
		                               						         		}
		                               						         	 }else if(roleAPersonDOB < roleBPersonDOB){
		                               						         		relationshipStartDate = roleBPersonFormattedDOB;
			                               						         	if(roleBPersonDOBEstimated===String(true)){
		                               						         			startDateEstimated = true;
		                               						         		}
		                               						         	 }else if(roleAPersonDOB===roleBPersonDOB){
		                               						         		relationshipStartDate = roleAPersonFormattedDOB;
		                               						         	 	if(roleAPersonDOBEstimated===String(true ) || roleBPersonDOBEstimated ===String(true)){
		                               						         	 		startDateEstimated = true;
		                               									    }
		                               						         	 }
	                               						         	 
		                               						         	 if(relationshipStartDate){
		                               						         		suggestionForm.set('relationshipStartDate', relationshipStartDate);
		                               						         	 }
		                               						         	if(startDateEstimated){
		                               						         		suggestionForm.set('startDateEstimated', startDateEstimated);
		                               						         	 }
	                               						         	 
	                               						         	}
	                               						         	
	                               						         	
	                               						         	//end
	                               						            
	                                   						        if(qualifiers != null && qualifiers.length > 0){		            		    	
	                                 			            			
	                                 		            		    	suggestionForm.set('hasAnyQualifiers', true);	                         		            		    	
	                                 		            		    	suggestionForm.set('notContainsOnlyFullQualifier', false);
	                                 		            		    	
	                                 		            		    	qualifiers.forEach(function(qualifier){                        		            		    		
	                                 		            		    		if(qualifier === 'HALF'){
	                                 		            		    			suggestionForm.set('isHalf', true);	
	                                 		            		    			suggestionForm.set('notContainsOnlyFullQualifier', true);
	                                 		            		    		}else if(qualifier === 'STEP'){
	                                 		            		    			suggestionForm.set('isStep', true);	
	                                 		            		    			suggestionForm.set('notContainsOnlyFullQualifier', true);
	                                 		            		    		}else if(qualifier === 'IN-LAW'){
	                                 		            		    			suggestionForm.set('isInLaw', true);
	                                 		            		    			suggestionForm.set('notContainsOnlyFullQualifier', true);
	                                 		            		    		}else if(qualifier === 'ADOPTIVE'){
	                                 		            		    			suggestionForm.set('isAdoptive', true);	
	                                 		            		    			suggestionForm.set('notContainsOnlyFullQualifier', true);
	                                 		            		    		}		            		    		
	                                 		            		    	});
	                                 		            		    
	                                 		            		    	var qualifiersText = '';
	                                			            			
	                                			            			if(A.indexOf(qualifiers, 'FULL', 0) > -1){
	                                			            				qualifiersText = qualifiersText + 'full/';
	                                			            			}
	                                			            			if(A.indexOf(qualifiers, 'HALF', 0) > -1){
	                                			            				qualifiersText = qualifiersText + 'half/';			            				
	                                			            			}
	                                			            			if(A.indexOf(qualifiers, 'STEP', 0) > -1){
	                                			            				qualifiersText = qualifiersText + 'step/';			            				
	                                			            			}
	                                			            			if(A.indexOf(qualifiers, 'IN-LAW', 0) > -1){
	                                			            				qualifiersText = qualifiersText + 'in-law/';			            				
	                                			            			}
	                                			            			if(A.indexOf(qualifiers, 'ADOPTIVE', 0) > -1){
	                                			            				qualifiersText = qualifiersText + 'adoptive/';
	                                			            				
	                                			            			}
	                                			            			qualifiersText = qualifiersText.substring(0,qualifiersText.length-1);	                        			            			
	                                			            			qualifiersText = qualifiersText + ' '+genderedRelationshipRole;	                        			            			
	                                			            			suggestionForm.set('relationship', qualifiersText);	                        			            			
	                                			            			suggestionForm.set('relationshipRole', genderedRelationshipRole);
	                                			            			
	                                   						        }
	                            			            		}                       			            		
	                            			            		
	                            		            		}
	                            		            		
	                            		            		// fix for professional relationships which have 'of' or 'for' in the middle of the relationship role
	                        			            		//ex Mark Gaston is Cared for by George Clooney
	                        			            		//Mark Gaston is Client of social worker George Clooney
	                        			            		// we are going to add 'of' for all roles except for roles having 'of' or 'for by'
	                        			            		
	                        			            		var suggestedRelationshipRole = suggestionForm.get('relationship');
	                        			            		if(!(suggestedRelationshipRole.indexOf(" of ") > -1)  &&  !(suggestedRelationshipRole.indexOf(" for by") > -1)){
	                        			            			suggestedRelationshipRole = suggestedRelationshipRole + ' of';
	                        			            			suggestionForm.set('relationship', suggestedRelationshipRole);
	                        			            		}
	                        			            		
	    													suggestedRelationshipRole = suggestionForm.get('relationshipRole');
	                        			            		if(suggestedRelationshipRole != null && !(suggestedRelationshipRole.indexOf(" of ") > -1)  &&  !(suggestedRelationshipRole.indexOf(" for by") > -1)){
	                        			            			suggestedRelationshipRole = suggestedRelationshipRole + ' of';
	                        			            			suggestionForm.set('relationshipRole', suggestedRelationshipRole);
	                        			            		}
	                        			            		
	                        			            		
	                        			            		//this roleAPersonId and roleBPersonId should be assigned properly for parent child relationships
	                        			            		// for remaining relationsships it does not matter
	                        			            		suggestionForm.set('roleAPersonId', roleAPerson.id);
	                        			            		suggestionForm.set('roleBPersonId', roleBPerson.id);
	                        			            		suggestionForm.set('personPersonRelationshipTypeId', suggestion.personPersonRelationshipType.id);
	                        			            		
	                        			            		suggestionForm.set('roleAGender', roleAPerson.gender);
	                        			            		suggestionForm.set('roleBGender', roleBPerson.gender);                        			            		
	                        		            			
	                        			            		var suggestionJson = suggestionForm.toJSON();
	                        			            		var clientId = suggestionForm.get('clientId');
	                        			 				    suggestionJson.clientId = clientId;
	                        			 				    suggestionsClientIds.push(clientId);
	                        			            		
	                        			            		if(nominatedPersonId == roleAPerson.id || nominatedPersonId == roleBPerson.id){
	                        			            			nominatedPersonSuggestions.push(suggestionJson);	
	                        			            		}else if(secondaryPersonId == roleAPerson.id || secondaryPersonId == roleBPerson.id){
	                        			            			secondaryPersonSuggestions.push(suggestionJson);
	                        			            		}
	                        	            			}else{
	                        	            				
	                        	            				unionSuggestions.push(suggestion);                        	            				
	                        	            			}
	                    			            		
	                        	            		});
	                        	            		
	                        	            		// handle union type suggestions
	                        	            		if(unionSuggestions.length > 0){
	                        	            			
	                        	            			var suggestionForm = new SuggestedPersonPersonRelationshipForm();
	                        	            			suggestionForm.set('roleAForename', roleAPerson.forename);
	                    			            		suggestionForm.set('roleASurname', roleAPerson.surname);
	                    			            		suggestionForm.set('roleAName', roleAPerson.name);
	                    			            		suggestionForm.set('roleBForename', roleBPerson.forename);
	                    			            		suggestionForm.set('roleBSurname', roleBPerson.surname);
	                    			            		suggestionForm.set('roleBName', roleBPerson.name);
	                    			            		
	                    			            		// for union type relationships role does not have significance - they can be any side
	                    			            		var firstPersonRole = 'ROLE_A';  
	                			            			suggestionForm.set('firstPersonRole', firstPersonRole); 
	                			            			suggestionForm.set('relationship', 'union with');
	                			            			
	                			            			suggestionForm.set('roleAPersonId', roleAPerson.id);
	                    			            		suggestionForm.set('roleBPersonId', roleBPerson.id);
	                    			            		//suggestionForm.set('personPersonRelationshipTypeId', suggestion.personPersonRelationshipType.id);
	                    			            		
	                    			            		suggestionForm.set('roleAGender', roleAPerson.gender);
	                    			            		suggestionForm.set('roleBGender', roleBPerson.gender);                        			            		
	                    		            			
	                    			            		
	                    			            		
	                    			 				   var unionRelationshipRoles = new Array();
	                    			 				    
	                    			 				   unionSuggestions.forEach(function(unionSuggestion){
	                           	            			
	                    			 					  var genderedRelationshipRole = RelationshipHelper.getGenderedRelationshipRole(roleAPerson, roleBPerson, unionSuggestion, firstPersonRole);
	                    			 					  var unionRelationshipRole = {}; 
	                    			 					  // unionRelationshipRole.set('genderedRelationshipRole', genderedRelationshipRole);
	                    			 					  unionRelationshipRole.genderedRelationshipRole = genderedRelationshipRole;
	                    			 					  unionRelationshipRole.personPersonRelationshipTypeId = unionSuggestion.personPersonRelationshipType.id;
	                    			 					  unionRelationshipRole.relationshipType = unionSuggestion.personPersonRelationshipType._type;  
	                    			 					  unionRelationshipRoles.push(unionRelationshipRole);
	                    			 					  
	                    			 				   });
	                    			 				   
	                    			 				   suggestionForm.set('unionRelationshipRoles', unionRelationshipRoles);
	                    			 				   var suggestionJson = suggestionForm.toJSON();
		                  			            	   var clientId = suggestionForm.get('clientId');
		                  			 				   suggestionJson.clientId = clientId;
		                  			 				   suggestionsClientIds.push(clientId);
	                    			 				    
	                    			            		if(nominatedPersonId == roleAPerson.id || nominatedPersonId == roleBPerson.id){
	                    			            			nominatedPersonSuggestions.push(suggestionJson);	
	                    			            		}else if(secondaryPersonId == roleAPerson.id || secondaryPersonId == roleBPerson.id){
	                    			            			secondaryPersonSuggestions.push(suggestionJson);
	                    			            		}
	                        	            		}
	                        	            		
	                        	            		
	                        	            	});
	                        	            	
	                        	            	if(nominatedPerson != null){
	                        	            		nominatedPersonDetails = new Y.Model({
	                        	            			header: 'yes',
	                        	            			forename: nominatedPerson.forename,
	                        	            			surname: nominatedPerson.surname,
	                        	            			name: nominatedPerson.name,
	                        	            			personid: nominatedPerson.personIdentifier,
	                        	            			gender: nominatedPerson.gender,
	                        	            			age: nominatedPerson.age,
	                        	            			personSuggestions: nominatedPersonSuggestions
	                        		            	});
	                        	            		suggestionsArray.push(nominatedPersonDetails.toJSON());
	                        	            	}
	                        	            	
	                        	            	if(secondaryPerson != null){
	                        	            		secondaryPersonDetails = new Y.Model({
	                        	            			header: 'yes',
	                        	            			forename: secondaryPerson.forename,
	                        	            			surname: secondaryPerson.surname,
	                        	            			name: secondaryPerson.name,
	                        	            			personid: secondaryPerson.personIdentifier,
	                        	            			gender: secondaryPerson.gender,
	                        	            			age: secondaryPerson.age,
	                        	            			personSuggestions: secondaryPersonSuggestions
	                        		            	});
	                        	            		suggestionsArray.push(secondaryPersonDetails.toJSON());
	                        	            	}
	                        	            	
	                        	            	suggestionsModel = new Y.Model({
	                        	            		suggestions: suggestionsArray,
	                        	            		suggestionsClientIds: suggestionsClientIds,
	                        	            		id: nominatedPersonId
	                        	            	});
	                        	            	
	                        	            	var nominatedPersonName = nominatedPersonModel.get('forename')+' '+nominatedPersonModel.get('surname');	
	                        	            	
	                        	        	    Y.fire('relationship:showDialog', {
	                        			            id: nominatedPersonId,
	                        			            personName: nominatedPersonName,
	                        			            personId: secondaryPersonId,
	                        			            suggestions: suggestionsModel,
	                        			            infomessage: infomessage,
	                        			            filtermessage: filterMessage,
	                        			            action: 'suggestions',
	                        			            panelPopup: myPopup,
	                        			            newRelationshipId: newResourceID
	                        			        });
	                        		            		
	                        		        });
	                        		        
	                        		        relationshipSuggestionsList.load();
	                                        	
	                                      }                                       
	                                       
	                                    }
	                            	});
							}
	          				
	          				saveAddRelationship();
	          				
                    },
                        disabled: true
                    },
                    {
                    	action:     cancelDialog,
                    	section:    Y.WidgetStdMod.FOOTER,
                    	name:       'cancelButton',
                    	labelHTML:  '<i class="fa fa-times"></i> Cancel',
                    	classNames: 'pure-button-secondary'
                    }]
                }
            }
		});
		
		//render that baby
		myPopup.render();
		
		Y.on('contentContext:update', Y.bind(function(e){
            this.setAttrs(e.data);
        },nominatedPersonModel), this);
		
		var canEdit=${canEdit};
		//respond to custom events
		Y.on('relationship:showDialog', function(e){

			var allowedAttributes = JSON.parse('${allowedAttributes}');

			if(e.action=='view'){
				model=new Y.usp.relationshipsrecording.AsymmetricPersonPersonRelationshipDetail({
       				url:'<s:url value="/rest/personPersonRelationship/{id}?nominatedPersonId={personId}"/>'});
				
				this.showView('viewRelationship', {
           			model:model,
           			template:Y.Handlebars.templates["relationshipDialog"],
           			labels:commonLabels
				}, {modelLoad:true, payload:{id:e.id, personId:e.personId}}, function(view){
					
					// Hide the edit button if not permitted
					if (!canEdit || !e.accessAllowed)
					{ 
						this.getButton('editButton', Y.WidgetStdMod.FOOTER).hide();
					} else if(model.get('temporalStatus')=="INACTIVE_HISTORIC"){
						// Hide the edit button if the relationship is inactiveHistoric
					   this.getButton('editButton', Y.WidgetStdMod.FOOTER).hide();
					}else{
						//Get the button by name out of the footer and enable it.
					      this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					}
					
					// Hide the close button if the relationship is inactiveHistoric
					if(model.get('temporalStatus')=="INACTIVE_HISTORIC"){
					  // this.getButton('closeButton', Y.WidgetStdMod.FOOTER).set('hidden',true);
					}else{
						//Get the button by name out of the footer and enable it.
						//this.getButton('closeButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					}
								
				});
			}
			
			else if(e.action=='edit'){
				this.showView('editRelationship', {
					model:new UpdatePersonPersonRelationshipForm(),
					relationshipType: e.relationshipType,
					targetEntity: e.targetEntity,					
					labels:commonLabels,
					familialRelationshipRoles: familialRelationshipRoles,
					socialRelationshipRoles: socialRelationshipRoles,
					professionalRelationshipRoles: professionalRelationshipRoles,
					nominatedPersonAge: nominatedPersonModel.get('age'),
          //Control display of named person relationship attribute in dialog
          hideNamedPersonRelationship: '${isNamedPersonRelationshipAvailable}'==='false',
					allowedAttributes: allowedAttributes,
					professionalTeamId: e.professionalTeamId,
					professionalTeamName: e.professionalTeamName,
					personTeamsURL:'<s:url value="/rest/person/{personId}/teamRelationship/professional?s=startDate&pageNumber=1&pageSize=-1"/>',
					professionalTeamId: '${teamId}',
					professionalTeamName: '${esc:escapeJavaScript(teamName)}'
				}, {
					modelLoad:true, 
					payload:{ 
						id: e.id, 
						personId: e.personId 
					}
			  }, function(view){
					//Get the button by name out of the footer and enable it.
					this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				});	
			}
			
			else if(e.action=='close'){
				this.showView('closeRelationship', {
					model: new ClosePersonPersonRelationshipForm(),    // create new model instance setting some attributes
					labels: commonLabels                              // labels for template
				}, { 
					modelLoad: true, 
					payload: {
						id: e.id, 
						personId: e.personId
					}
				}, function(view){
					//Get the button by name out of the footer and enable it.
					this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);	
					this.getButton('noButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				});				
			}
			
			else if(e.action=='add'){		
				var relatedPersonId;
				
				if (e.relatedPerson) {
					relatedPersonId=e.relatedPerson.personId;
				}
				var nominatedPerson = nominatedPersonModel.toJSON();
				var personName = nominatedPerson.name,
				    personDOB =nominatedPerson.dob;
	    	        personDOBEstimated=nominatedPerson.dateOfBirthEstimated;
				this.showView('addRelationship', {
					addPersonUrl:'<s:url value="/rest/person" />',
					model: new NewPersonPersonRelationshipForm(), // create new model instance
					otherData: nominatedPersonModel.toJSON(),
					labels: {
						// relationship labels
						role: '<s:message code="relationship.role" javaScriptEscape="true"/>',
						qualifier:'<s:message code="relationship.qualifier" javaScriptEscape="true"/>',
						unionqualifier:'<s:message code="relationship.unionqualifier" javaScriptEscape="true"/>',
						inLaw: '<s:message code="relationship.inLaw" javaScriptEscape="true"/>',
						adoptive: '<s:message code="relationship.adoptive" javaScriptEscape="true"/>',
						half: '<s:message code="relationship.half" javaScriptEscape="true"/>',
 						step: '<s:message code="relationship.step" javaScriptEscape="true"/>',
						multipleBirth: '<s:message code="relationship.multipleBirth" javaScriptEscape="true"/>',
						relatedPersonSearch: '<s:message code="relationship.relatedPersonSearch" javaScriptEscape="true"/>',
						relatedPersonSearchPlaceholder: '<s:message code="relationship.relatedPersonSearchPlaceholder" javaScriptEscape="true"/>',
						startDate: '<s:message code="relationshipAdd.startDate" javaScriptEscape="true"/>',
						startDateEstimated: '<s:message code="relationship.startDateEstimated" javaScriptEscape="true"/>',
						relatedProfessionalTeamSelect: '<s:message code="relationship.relatedProfessionalTeamSelect" javaScriptEscape="true"/>',
						// Person labels
	                    forename: '<s:message code="person.forename" javaScriptEscape="true"/>',
	                    surname: '<s:message code="person.surname" javaScriptEscape="true"/>',
	                    ethnicity: '<s:message code="person.ethnicity" javaScriptEscape="true"/>',
	                    dateOfBirth: '<s:message code="person.dateOfBirth" javaScriptEscape="true"/>',
	                    age:'<s:message code="person.age" javaScriptEscape="true"/>',
	                    // DoB messages
	                    dobEstimateMessage: '<s:message code="person.add.dobEstimateMessage" javaScriptEscape="true"/>',
	                    dobCheckboxMessage: '<s:message code="person.add.dobExact" javaScriptEscape="true"/>',
	                    // Popover 
	                    dobPopupMessageTitle: '<s:message code="person.add.dateOfBirthPopup.title" javaScriptEscape="true"/>',
                        dobPopupMessageContent: '<s:message code="person.add.dateOfBirthPopup.content" javaScriptEscape="true"/>',                       
                        dateOfBirthValidationErrorMessage : '<s:message code="person.addeditperson.dateofbirth.invalidMessage" javaScriptEscape="true"/>'
					},
					//Control display of named person relationship attribute in dialog
					hideNamedPersonRelationship: '${isNamedPersonRelationshipAvailable}'==='false',
					allowedAttributes: allowedAttributes,
					//Options for select boxes
					familialRelationshipRoles: familialRelationshipRoles,
					socialRelationshipRoles: socialRelationshipRoles,
					professionalRelationshipRoles: professionalRelationshipRoles,
					//set targetPerson attribute
					targetPersonId:e.targetPersonId,
					personRoles:e.personRoles,
					// Message to show after succesful add person save
					messageAddPersonSuccess: '<s:message code="person.add.success.message" javaScriptEscape="true"/>',

					//TODO: remove awful hack and use a Model or sommit					
					targetPersonName: personName || e.targetPersonName,
					targetPersonDOB:e.targetPersonDOB || personDOB,
					targetPersonDoBEstimated:e.targetPersonDoBEstimated ||personDOBEstimated,
					relatedPersonId:relatedPersonId,
					relatedPerson: e.relatedPerson,
					relatedPersonRoles:e.relatedPersonRoles,
					relationshipTypes:e.relationshipTypes,
					relationshipType: e.relationshipType,
					panel: myPopup,
					//specify autocomplete URL
					autocompleteURL:'<s:url value="/rest/person?appendWildcard=true&nameOrId="/>',  //{query}
					autocompleteProfessionalURL:'<s:url value="/rest/person?appendWildcard=true&personType=PROFESSIONAL&nameOrIdentifier="/>',
					personTeamsURL:'<s:url value="/rest/person/{personId}/teamRelationship/professional?s=startDate&pageNumber=1&pageSize=-1"/>',
					professionalTeamId: '${teamId}',
					professionalTeamName: '${esc:escapeJavaScript(teamName)}',
					currentUserPersonId: '${currentUserPersonId}'
				}, function(view){
					//Get the button by name out of the footer and enable it.
					this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled', false);	 	
				});		
			}else if(e.action=='addRelationshipType') {
                
				var relatedPersonId;
				if (e.relatedPerson) {
					relatedPersonId=e.relatedPerson.personId;
				}
				
				var isOrganisationRelationship=e.isOrganisationRelationship;
				
				  var NAME = '${esc:escapeJavaScript(person.name)}',
			      	ID = '<c:out value="${person.id}"/>',
			     	IDFIER = '${esc:escapeJavaScript(person.personIdentifier)}',
			     	GENDER = '${esc:escapeJavaScript(person.gender)}',
			      	TYPES = '${esc:escapeJavaScript(person.personTypes)}',
			      	SUBJECT = {
			    		  id: ID,
			    		  identifier: IDFIER,
			    		  name: NAME, 	  
			    		  gender: GENDER,
			    		  personTypes: TYPES,
			    		  type: 'person'
			      };
			      	
				this.showView('addRelationshipType', {
					model: new NewPersonPersonRelationshipTypeForm({
					  relationshipType:isOrganisationRelationship?'OrganisationRelationshipType':'FamilialRelationshipType'
				  }),
					subject: e.subject || SUBJECT,
					targetPersonId: e.personId,
					targetPersonName: e.personName,
					onlyProfessional: e.onlyProfessional,
					targetPersonTypes:e.personRoles,
					relatedPersonId:relatedPersonId,
					relatedPerson: e.relatedPerson,
					relatedPersonRoles:e.relatedPersonRoles,
					clientTeamId: e.clientTeamId,
					labels: {
					  type: '<s:message code="relationship.type"/>'
					}
				});
			}else if(e.action=='delete'){				
				this.showView('deleteRelationship', {
           			model:new Y.usp.relationshipsrecording.AsymmetricPersonPersonRelationshipDetail({
           				url:'<s:url value="/rest/personPersonRelationship/{id}?nominatedPersonId={personId}"/>'
           			}),
           			template:Y.Handlebars.templates["relationshipDeleteDialog"],
           			labels: commonLabels                              // labels for template
				}, {modelLoad:true, payload:{id:e.id, personId:e.personId}}, function(view){
					
					//Get the button by name out of the footer and enable it.
					this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);	
					this.getButton('noButton', Y.WidgetStdMod.FOOTER).set('disabled',false);	
				});	
			}
			else if(e.action=='saveSuggestion'){				
				this.showView('saveSuggestedRelationship', {
					model:new NewPersonPersonRelationshipForm(),
           			template:Y.Handlebars.templates["suggestionSaveDialog"],
           			labels:{
           					startDate: '<s:message code="relationship.startDate" javaScriptEscape="true"/>',
           					startDateEstimated: '<s:message code="relationship.suggestion.startDateEstimated" javaScriptEscape="true"/>' 
                       },
                    suggestionRelationshipId: e.suggestionRelationshipId,
                    rootPersonId: e.rootPersonId,
                    roleAPersonId: e.roleAPersonId,
                    roleBPersonId: e.roleBPersonId,
                    roleAPersonIdentifier: e.roleAPersonIdentifier,
                    roleBPersonIdentifier: e.roleBPersonIdentifier,
                    roleAPersonName: e.roleAPersonName,
                    roleBPersonName: e.roleBPersonName,
                    relationshipTypeId: e.relationshipTypeId,
                    relationshipTypeDescriptor: e.relationshipTypeDescriptor,
                    relationshipLabel: e.relationshipLabel,
                    qualifier: e.qualifier,
                    relationshipStartDate: e.relationshipStartDate,
		            startDateEstimated: e.startDateEstimated,
                    role: e.role,
                    professionalTeamId: e.professionalTeamId,
                    professionalTeamName: e.professionalTeamName,
                    panel:myPopup
				},
			function(view){											
					//Get the button by name out of the footer and enable it.
					this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);	
					
					//Show the cancel button
					//this.getButton('cancelButton', Y.WidgetStdMod.FOOTER).show();	
				});	
			}else if(e.action=='suggestions'){				
				this.showView('suggestionsView', {
           			addSuggestRelationshipUrl: '<s:url value="/rest/personPersonRelationship" />',
           			model: e.suggestions, 
           			panelPopup: e.panelPopup,
           			newRelationshipId: e.newRelationshipId,
           			infomessage: e.infomessage,
           			filtermessage: e.filtermessage,
           			personName: e.personName,
           			template:Y.Handlebars.templates["relationshipSuggestionsDialog"],           			
           			//template:Y.Handlebars.compile(Y.one('#TempSuggestionsOverlay').getHTML()),
           			labels:{                      
                        inLaw: '<s:message code="relationship.inLaw" javaScriptEscape="true"/>',
                        adoptive: '<s:message code="relationship.adoptive" javaScriptEscape="true"/>',
                        half: '<s:message code="relationship.half" javaScriptEscape="true"/>',
                        step: '<s:message code="relationship.step" javaScriptEscape="true"/>',
                        suggestionsHeader: '<s:message code="relationship.suggestionsHeader" javaScriptEscape="true"/>',
                        suggestionsChoose: '<s:message code="relationship.suggestionsChoose" javaScriptEscape="true"/>',
                        suggestionsNoQualifiers: '<s:message code="relationship.suggestionsNoQualifiers" javaScriptEscape="true"/>',
                        startDate: '<s:message code="relationship.startDate" javaScriptEscape="true"/>',
                        suggestionsfilterAlertMessage: '<s:message code="relationship.suggestionsfilterAlertMessage" javaScriptEscape="true"/>',
                        startDateEstimated: '<s:message code="relationship.suggestion.startDateEstimated" javaScriptEscape="true"/>',
                        suggestionsNarrative: "<s:message code='relationship.suggestionsNarrative' javaScriptEscape="true"/>"
                        
                    }
					
				}, function(view){					
			        Y.one('.RelationshipSuggestionsView').setStyle('width','750px');	
			        var infomessage = view.get('infomessage');
			        var filtermessage = view.get('filtermessage');

                    if(infomessage.length > 10 || filtermessage != null){
			        	
			        	var message = '';
			        	if(infomessage.length > 10 && filtermessage != null){
			        		message = message+'<br>'+filtermessage+'</br>'+infomessage;
			        	}else{
			        		if(filtermessage){
			        			message = message+'<br>'+filtermessage+'</br>';
			        		}else if(infomessage.length > 10){
			        			message = message+'<br>'+infomessage+'</br>';
			        		}
			        	}
			        	
			          var messageInfo='Relationship successfully added' +message;
			           Y.fire('infoMessage:message', {message:messageInfo});
				       	
			        }else{
			        	Y.fire('infoMessage:message', {message:'Relationship successfully added'});

			        }
			      
			        this.getButton('noButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			        this.getButton('goToGraph', Y.WidgetStdMod.FOOTER).set('disabled',false);	
					//Show the cancel button
					//this.getButton('cancelButton', Y.WidgetStdMod.FOOTER).show();			    	
				});				
			}Y.one("body").delegate('submit',function(e){e.preventDefault();},'form[action="#none"]');	
		}, myPopup);
	});
</script>
