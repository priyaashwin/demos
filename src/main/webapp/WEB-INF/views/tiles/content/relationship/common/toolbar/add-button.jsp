<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.ADD')" var="canAdd"/>
</c:if>

<tilesx:useAttribute name="first" ignore="true"/>
<tilesx:useAttribute name="last" ignore="true"/>

<c:if test="${first eq true}">
  <c:set var="fStyle" value="first"/>
</c:if>

<c:if test="${last eq true}">
  <c:set var="lStyle" value="last"/>
</c:if>

<c:choose>
  <c:when test="${not empty person}">
    <c:set var="name" value="${person.name}"/>

    <%-- CSS classes and tabIndex to apply if user has ADD permission --%>
    <c:choose>
      <c:when test="${canAdd eq true}">
        <c:set var="addClass" value="pure-button-loading" />
        <c:set var="addAction" value="addRelationship" />
       <c:set var="tabIndex" value="0" />
      </c:when>
      <c:otherwise>
        <c:set var="tabIndex" value="-1"/>
      </c:otherwise>
    </c:choose>
    
    <%-- Button markup--%>
    <li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
      <a href="#none" data-button-action="${addAction}" class="pure-button pure-button-disabled usp-fx-all ${addClass}"
        title='<s:message code="relationship.add.button" arguments="${name}" htmlEscape="true" />' aria-disabled="true"
        aria-label='<s:message code="relationship.add.button" arguments="${name}" htmlEscape="true" />' tabindex="${tabIndex}">
        <i class="fa fa-plus-circle" aria-hidden="true"></i>
        <span><s:message code="button.add" /></span>
      </a>
    </li>
  </c:when>
</c:choose>