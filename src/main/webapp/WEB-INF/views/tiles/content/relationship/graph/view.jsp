<%@page contentType="text/html;charset=UTF-8"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
      <%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
        <%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
          <%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

            <sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson" />
            <sec:authorize access="hasPermission(null, 'PersonPersonRelationship','PersonPersonRelationship.View')" var="canView" />
			<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Genogram.View')" var="canViewGenogram"/>
			<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','GenogramLayout.Update')" var="canSaveGenogramLayout"/>
			<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.Add')" var="canAdd"/>
			<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.Update')" var="canEdit"/>
			<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.Close')" var="canClose"/>
			<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.Remove')" var="canRemove"/>


            <%-- Insert relationship dialog --%>
              <t:insertTemplate template="/WEB-INF/views/tiles/content/relationship/common/relationship-dialog.jsp" />

              <%-- Insert person-organisation relationship dialog --%>
                <t:insertTemplate template="/WEB-INF/views/tiles/content/relationship/person-organisation/person-org-relationship-dialog.jsp" />

                <c:choose>
                  <c:when test="${canView eq true}">
                    <div id="relationshipView"></div>
                    <div id="diagram"></div>
                    <div id="genogram"></div>

                    <script>
                      //set mxGraphBase (Global variable) - required by mxGraph
                      var mxBasePath = '<s:url value="/js/mxGraph" />';

                      Y.use('relationship-graph-controller-view', function (Y) {

                        var permissions = {
                          canView: '${canView}' === 'true',
                          canViewList: '${canView}' === 'true',
                          canViewGenogram: '${canViewGenogram}' === 'true', //nothing configured for canViewGenogram so won't display.
                          canAdd: '${canAdd}' === 'true',
                          canAddRelationship: '${canAdd}' === 'true',
                          canEdit: '${canEdit}' === 'true',
                          canClose: '${canClose}' === 'true',
                          canRemove: '${canRemove}' === 'true',
                          canViewPerson: '${canViewPerson}' === 'true',
                          canSaveGenogramLayout: '${canSaveGenogramLayout}' === 'true' 
                        };

                        var subject = {
                          subjectType: 'person',
                          subjectName: '${esc:escapeJavaScript(person.name)}',
                          subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                          subjectId: '<c:out value="${person.id}"/>',
                          //Needed by the relationship dialog - will be removed when that is updated to use the subject
                          id: '<c:out value="${person.id}"/>',
                          personTypes: '${esc:escapeJavaScript(person.personTypes)}',
                          forename: '${esc:escapeJavaScript(person.forename)}',
                          surname: '${esc:escapeJavaScript(person.surname)}',
                          name: '${esc:escapeJavaScript(person.name)}',
                          identifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                          gender: '${esc:escapeJavaScript(person.gender)}',
                          age: '${esc:escapeJavaScript(person.age)}'
                        };

                        var filterContextConfig = {
                          labels: {
                            filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
                            resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
                            resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>',
                            type: '<s:message code="person.relationship.find.filter.relationshipTypes.active" javaScriptEscape="true"/>',
                            state: '<s:message code="person.relationship.find.filter.temporalStatusFilter.active" javaScriptEscape="true"/>',
                            date: '<s:message code="person.relationship.find.filter.date.active" javaScriptEscape="true"/>',
                            stateActiveMessage: '<s:message code="person.relationship.filter.stateFilterSelectedInfo" javaScriptEscape="true"/>',
                            dateActiveMessage: '<s:message code="person.relationship.filter.dateFilterSelectedInfo" javaScriptEscape="true"/>'
                          }
                        };

                        var filterConfig = {
                          labels: {
                            filterBy: '<s:message code="filter.by" javaScriptEscape="true"/>',
                            relationshipTypes: {
                              familial: '<s:message code="person.relationship.find.filter.relationshipType.familial" javaScriptEscape="true"/>',
                              social: '<s:message code="person.relationship.find.filter.relationshipType.social" javaScriptEscape="true"/>',
                              professional: '<s:message code="person.relationship.find.filter.relationshipType.professional" javaScriptEscape="true"/>',
                            },
                            state: {
                              active: '<s:message code="person.relationship.find.filter.state.active" javaScriptEscape="true"/>',
                              inactiveHistoric: '<s:message code="person.relationship.find.filter.state.inactiveHistoric" javaScriptEscape="true"/>',
                              inactivePending: '<s:message code="person.relationship.find.filter.state.inactivePending" javaScriptEscape="true"/>'
                            },
                            dateFrom: '<s:message code="person.relationship.find.filter.entryDateRangeStart" javaScriptEscape="true"/>',
                            dateTo: '<s:message code="person.relationship.find.filter.entryDateRangeEnd" javaScriptEscape="true"/>',
                            resetTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
                            reset: '<s:message code="filter.reset" javaScriptEscape="true"/>',
                            validation: {
                              dateTo: '<s:message code="person.relationship.find.filter.endBeforeStartDate" javaScriptEscape="true"/>'
                            }
                          }
                        };

                        var graphConfig = {
                          container: '#diagram',
                          url: '<s:url value="/rest/relationships/person/{personId}/all"/>',
                          diagramUrl: '<s:url value="/relationships/persondiagram?id={personId}" />',
                          labels: {
                            msgServerErrorLine1: '<s:message code="server.error.line1" javaScriptEscape="true"/>',
                            msgServerErrorLine2: '<s:message code="server.error.line2" javaScriptEscape="true"/>',
                            hideSuggestions: '<s:message code="button.hideSuggestions" javaScriptEscape="true"/>',
                            showSuggestions: '<s:message code="button.showSuggestions" javaScriptEscape="true"/>',
                            buttonHideKey: '<s:message code="button.hideKey" javaScriptEscape="true"/>',
                            buttonShowKey: '<s:message code="button.showKey" javaScriptEscape="true"/>',
                            buttonHideSecondary: '<s:message code="button.hideSecondary" javaScriptEscape="true"/>',
                            buttonShowSecondary: '<s:message code="button.showSecondary" javaScriptEscape="true"/>'
                          },
                          client_team_id: '${relationshipClassClientTeamId}',
                          loadCallback: function (data) {
                            // display no suggestion messages.
                            if (data.toJSON().numberOfSuggestions === 0 && Y.one('#relationshipUserInfo') && Y.one('#suggestionsToggle') && Y.one('#suggestionsToggle').hasClass('active')) {

                              Y.one('#relationshipUserInfo h4').setHTML('<span id="suggestionInfo"><s:message code="relationship.noSuggestionMessage" javaScriptEscape="true"/></span>');
                              Y.one('#relationshipUserInfo').show('fadeIn');
                              Y.one('#suggestionsToggle').removeClass('active').addClass('inactive');
                              Y.fire('header:resize');
                            }
                          }
                        };

                        var genogramConfig = {
                          container: '#genogram',
                          permissions: permissions,
                          subject: subject,
                          routes: {
                            data: '<s:url value="/rest/person/{id}/graphingPersonRelationships/relationships"/>',
                            page: '<s:url value="/relationships/persondiagram?id={id}"/>',
                          },
                          filterConfig: {
                            url: '<s:url value="/rest/person/{subjectId}/personRelationship?pageNumber={page}&pageSize={pageSize}&s={sortBy}&differentialLevel=3&relationshipTypes=FamilialRelationshipType&dateFrom={dateFrom}"><s:param name="sortBy" value="[{\"name\":1},{\"personPersonRelationshipType\":-1},{\"startDate\":-1}]" /></s:url>',
                            labels: {
                              whyGreyedOut: '<s:message code="person.relationship.genogram.filter.grey.reason" javaScriptEscape="true"/>'
                            }
                          },
                          dialogConfig: {
                            views: {
                              print: {
                                header: {
                                  text: '<s:message code="genogram.print.header" javaScriptEscape="true"/>',
                                  icon: 'fa fa-print'
                                },
                                narrative: {
                                  summary: '<s:message code="genogram.print.summary" javaScriptEscape="true"/>'
                                }
                              }
                            }
                          },
                          getLayoutUrl : '<s:url value="/rest/person/{id}/genogramLayout"/>',
          				        saveLayoutUrl : '<s:url value="/rest/person/{personId}/genogramLayout?updateGenogramLayoutVO={layout}"/>'  
                        };

                        var relationshipGraphView = new Y.app.relationship.RelationshipGraphControllerView({
                          container: '#relationshipView',
                          filterConfig: filterConfig,
                          filterContextConfig: filterContextConfig,
                          permissions: permissions,
                          subject: subject || {},
                          genogramConfig: genogramConfig,
                          graphConfig: graphConfig,
                          viewListUrl: '<s:url value="/relationships/{subjectType}?id={subjectId}" />',
                        }).render();

                        Y.one('#relationshipUserInfo').delegate('click', function () {
                          Y.one('#relationshipUserInfo').hide('fadeOut');
                        }, 'a.close');
                      });
                    </script>
                  </c:when>
                  <c:otherwise>
                    <%-- Insert access denied tile --%>
                      <t:insertDefinition name="access.denied" />
                  </c:otherwise>
                </c:choose>