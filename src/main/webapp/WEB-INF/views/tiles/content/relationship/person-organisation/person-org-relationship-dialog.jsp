<%@ page import="java.util.Locale" %>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- access relationship allowed attribute --%>
<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="relationshipAttributesContext" value="${userContextAttributes.getContext('relationship-attribute')}" />
<c:set var="allowedAttributes" value="${relationshipAttributesContext.getAllowedAttributeValuesAsJson()}" />

<script>
  Y.use('yui-base',
      'event-custom',
      'person-organisation-relationship-dialog-views',
      'model-transmogrify',
      'usp-relationship-NewPersonOrganisationRelationship',
      'usp-relationship-PersonOrganisationRelationship',
      'usp-relationship-UpdatePersonOrganisationRelationship', function(Y) {

    var allowedAttributes = JSON.parse('${allowedAttributes}');
    
	  var placeholders = {
		    OrganisationRelationshipType: '<s:message code="relationship.relatedOrganisationSearchPlaceholder" javaScriptEscape="true"/>',
		    TeamRelationshipType: '<s:message code="relationship.relatedTeamSearchPlaceholder" javaScriptEscape="true"/>',
	  },
	  
   	  //Get Age of nominated person (Target person)
	  age = '${esc:escapeJavaScript(person.age)}',
      
	  narratives = {          
		    OrganisationRelationshipType: 'You\'re adding an organisational relationship for',          
		    TeamRelationshipType: 'You\'re adding a team relationship for'      
	  },
	  
	  //For relationship type
	  REL_TYPE_ORGANISATIONAL = 'OrganisationalRelationshipType',
	  REL_TYPE_TEAM = 'ClientTeamRelationshipType',
	   
	  commonLabels = {       
  		    relationship: '<s:message code="relationship.relationship"/>',
        	organisationName: '<s:message code="relationship.organisationName"/>',
        	organisationType: '<s:message code="relationship.organistionType"/>',
        	organistionSubtype: '<s:message code="relationship.organistionSubtype"/>',
        	startDate: '<s:message code="relationship.startDate"/>',
        	addStartDateLabel: '<s:message code="relationshipAdd.startDate"/>',
        	startDateEstimated: '<s:message code="relationship.startDateEstimated"/>',
        	closeDate: '<s:message code="relationship.closeDate"/>',
        	closeReason: '<s:message code="relationship.closeReason"/>',
        	endConfirm: '<s:message code="relationship.endConfirm"/>',
        	deleteConfirm: '<s:message code="relationship.deleteConfirm"/>',   			
        	viewRelationshipMessage: "<s:message code='relationship.viewPerOrgRelationshipMessage'/>",
        	editPerOrgRelationshipMessage: "<s:message code='relationship.editPerOrgRelationshipMessage'/>",
        	endPerOrgRelationshipMessage: "<s:message code='relationship.endPerOrgRelationshipMessage'/>",
        	deleteRelationshipMessage: "<s:message code='relationship.deleteRelationshipMessage'/>",
        	relatedOrganisationSearch :'<s:message code="relationship.relatedOrganisationSearch" javaScriptEscape="true"/>',					
        	addRelationshipSuccessMessage: '<s:message code="relationship.addSuccess" javaScriptEscape="true"/>',
        	employeeOrOther: '<s:message code="relationship.employeeOrOther" javaScriptEscape="true"/>',
        	employee:'<s:message code="relationship.employee" javaScriptEscape="true"/>',
        	other:'<s:message code="relationship.other" javaScriptEscape="true"/>',
	    };

    var relationshipDialog = new Y.app.relationship.MultiPanelPopUp();
    
    relationshipDialog.render();
    
    var AddPersonOrganisationRelationshipModel = Y.Base.create("AddPersonOrganisationRelationshipModel", Y.usp.relationship.NewPersonOrganisationRelationship, [Y.usp.ModelFormLink], { 
        form: '#newPersonOrganisationRelationshipForm'      
    });
    
    var AddPersonTeamRelationshipModel = Y.Base.create("AddPersonTeamRelationshipModel", Y.usp.relationship.NewPersonOrganisationRelationship, [Y.usp.ModelFormLink], { 
        form: '#newPersonOrganisationRelationshipForm'
    });
    
     var ViewPersonOrganisationRelationshipModel = Y.Base.create("ViewPersonOrganisationRelationshipModel", Y.usp.relationship.PersonOrganisationRelationship, [Y.usp.ModelFormLink], { 
        form: '#viewPersonOrganisationRelationshipForm',
        url: '<s:url value="/rest/personOrganisationRelationship/{id}/"/>'
    });
    
    var EditPersonOrganisationRelationshipModel = Y.Base.create("EditPersonOrganisationRelationshipModel", Y.usp.relationship.PersonOrganisationRelationship, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], { 
        form: '#editPersonOrganisationRelationshipForm',
        getURL:function(action, options){		
        	if(action==='update'){
        		
        			if(options.organisationType==='TEAM')
        				this.url = '<s:url value="/rest/clientPersonTeamRelationship/{id}?checkInactive=false"/>';
        		
        		return this._substituteURL(this.url, Y.merge(this.getAttrs(), options));			
        	}			
        	//return standard operation			
        	return EditPersonOrganisationRelationshipModel.superclass.getURL.apply(this, [action, options]);		
        }
    });
    
    var EndPersonOrganisationRelationshipModel = Y.Base.create("EndPersonOrganisationRelationshipModel", Y.usp.relationship.PersonOrganisationRelationship, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], { 
        form: '#relationshipOrganisationCloseForm',
        url: '<s:url value="/rest/personOrganisationRelationship/{id}?checkInactive=false"/>',
        getURL:function(action, options){
			if(action==='update'){
				if(this.getAttrs().relationshipType === REL_TYPE_TEAM){
					return this._substituteURL('<s:url value="/rest/clientPersonTeamRelationship/{id}/status?checkInactive=false&action=close"/>', Y.merge(this.getAttrs(), options));
				}else{
					return this._substituteURL('<s:url value="/rest/personOrganisationRelationship/{id}/status?checkInactive=false&action=close"/>', Y.merge(this.getAttrs(), options));
				}
			}
			//return standard operation
			return EndPersonOrganisationRelationshipModel.superclass.getURL.apply(this, [action, options]);
		}
    });
    
    var DeletePersonOrganisationRelationshipModel = Y.Base.create("DeletePersonOrganisationRelationshipModel", Y.usp.relationship.PersonOrganisationRelationship, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], { 
        form: '#viewPersonOrganisationRelationshipForm',
        url: '<s:url value="/rest/personOrganisationRelationship/{id}/"/>',
        getURL:function(action, options){
			if(action==='delete'){
				if(this.getAttrs().relationshipType === REL_TYPE_TEAM){
					return this._substituteURL('<s:url value="/rest/clientPersonTeamRelationship/{id}"/>', Y.merge(this.getAttrs(), options));
				}else{
					return this._substituteURL('<s:url value="/rest/personOrganisationRelationship/{id}"/>', Y.merge(this.getAttrs(), options));
				}
			}
			//return standard operation
			return DeletePersonOrganisationRelationshipModel.superclass.getURL.apply(this, [action, options]);
		}
    });
    
    Y.on('personOrganisationRelationship:showDialog', function(e){
      
    	switch(e.action){        	
    	case 'addPersonTeamRelationship':      
    		
    		commonLabels.placeholderRelatedSeach = placeholders.TeamRelationshipType;
    		commonLabels.narrative = narratives.TeamRelationshipType;
    		
    		this.showView('addPersonTeamRelationship', {                   
            labels: commonLabels,
            relationshipType: 'ClientTeamRelationshipType',
            subject: e.subject,
            clientTeamId: e.clientTeamId,
            model: new AddPersonTeamRelationshipModel({
                url: '<s:url value="/rest/clientPersonTeamRelationship/ "/>'
            }),
    		});             
    		break;
    		
    	case 'addPersonOrganisationRelationship':    		
    		
    		commonLabels.placeholderRelatedSeach = placeholders.OrganisationRelationshipType;
    		commonLabels.narrative = narratives.OrganisationRelationshipType;

    		this.showView('addPersonOrganisationRelationshipView', {
   			  labels: commonLabels,
          allowedAttributes: allowedAttributes,
   			  relationshipType: 'OrganisationalRelationshipType',
   			  subject: e.subject,
   			  otherData: {
    				employerEmployeeRelationshipTypeId:'${esc:escapeJavaScript(employerEmployeeRelationshipTypeId)}',
    				responsibleLocalAuthorityRelationshipTypeId:'${esc:escapeJavaScript(responsibleLocalAuthorityRelationshipTypeId)}',
    				age: age
	    		  },
    			model: new AddPersonOrganisationRelationshipModel({
   				 url: '<s:url value="/rest/personOrganisationRelationship/ "/>'
   			})
      	},function(view){
      		var con = view.get('container');
      		con.one('#employeeOrOther').show();
      	});    
    		
        break;
                
    	case 'viewPersonOrganisationRelationship':
    		var config = {
    			canEditClientPersonTeamRelationship : e.canEditClientPersonTeamRelationship,
    			canEditOrganisationRelationship : e.canEditOrganisationRelationship
    			};
    		this.showView('viewPersonOrganisationRelationshipView', {
    			model: new ViewPersonOrganisationRelationshipModel(),
    			labels: commonLabels
    			
    		},{modelLoad:true, payload:{id:e.id}}, function(view){
    			var model = view.get('model'); 
    			var container = view.get('container');
    			// defaults the button to inactive
    			this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled',true);
    			
    			
    			// checks if record is inactive and person organisation relationship type is not professional
    		    if(model && !(model.get('temporalStatus')==="INACTIVE_HISTORIC")
    		    		&& (model.get('personOrganisationRelationshipTypeVO')?(!(model.get('personOrganisationRelationshipTypeVO')._type === "ProfessionalTeamRelationshipType")) :true)){
    		    	
    		    	// checks if  organisation type is 'TEAM' and has permissions for team or 
    		    	// organisation type does not exist and has permissions to view organisation
    		    	if((model.get('organisationVO').organisationType==='TEAM' && config.canEditClientPersonTeamRelationship)||
    		    			(!(model.get('organisationVO').organisationType) && config.canEditOrganisationRelationship)){
    		    		
    		    		this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
    		    		
    				}
    		    	
    		    }
    		 
    		});
    	break;
        
    	case 'deletePersonOrganisationRelationship':
    		var relationshipType, url;
        	if(e.organisationType==='TEAM'){
    			relationshipType =  REL_TYPE_TEAM
    		} else {
    			relationshipType =  REL_TYPE_ORGANISATIONAL
    		}
    		this.showView('deletePersonOrganisationRelationshipView', {
    			model: new DeletePersonOrganisationRelationshipModel({
    				relationshipType: relationshipType
    			}),
    			labels: commonLabels,
    			deleteRelationship:true
    		},{modelLoad:true, payload:{id:e.id}}, function(view){
    			this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
    		});
    	break;

    	case 'endPersonOrganisationRelationship':
        	var relationshipType, url;
        	if(e.organisationType==='TEAM'){
    			relationshipType =  REL_TYPE_TEAM
    		} else {
    			relationshipType =  REL_TYPE_ORGANISATIONAL
    		}
    		this.showView('endPersonOrganisationRelationshipView', {
    			model: new EndPersonOrganisationRelationshipModel({
    				relationshipType: relationshipType
    			}),
    			labels: commonLabels,
    			endRelationship:true,
    			relationshipType: relationshipType
    			
    		},{modelLoad:true, payload:{id:e.id}}, 
    		function(view){
    			this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
    			var typeVal=Y.one('#personOrganisationRelationship-input').one('option');
    			var model = view.get('model');
    			var container = view.get('container');
    			if(!typeVal.get('value')){
    				typeVal.setStyle('display', 'none').set('text', 'Please choose');
    			}
    			
    		});
    	break;
    	case 'editPersonOrganisationRelationship':
    		 var relationshipType;
    		 
    		if(e.organisationType==='TEAM'){
    			relationshipType =  'ClientTeamRelationshipType'
    			
    		} else {
    			relationshipType =  'OrganisationalRelationshipType'
    		}
    		
    		this.showView('editPersonOrganisationRelationshipView', {
    			labels: commonLabels,
          allowedAttributes: allowedAttributes,
    			relationshipType: relationshipType,
    			otherData: {
    				employerEmployeeRelationshipTypeId:'${esc:escapeJavaScript(employerEmployeeRelationshipTypeId)}',
    				responsibleLocalAuthorityRelationshipTypeId:'${esc:escapeJavaScript(responsibleLocalAuthorityRelationshipTypeId)}'
    			},
    			model: new EditPersonOrganisationRelationshipModel({
    				url: '<s:url value="/rest/personOrganisationRelationship/{id}?checkInactive=false"/>',
    			}) 
    		},{modelLoad:true, payload:{id:e.id}},
    		function(view){
    			var model=view.get('model');
    			var container=view.get('container');
    			this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
    		});
    	break;
		
		}
    }, relationshipDialog);
    
    relationshipDialog.after('*:saveEnabledChange', function(e){
      if(e.newVal===true) {          
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
      } else {
          this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',true);
      }
    }, relationshipDialog); 
    
  });
         


</script>