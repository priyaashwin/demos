<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson"/>
<sec:authorize access="hasPermission(null, 'PersonPersonRelationship','PersonPersonRelationship.View')" var="canViewPersonRelationship"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPersonRelationship.View')" var="accessAllowed"/>
<c:set var="isProfessional" value="${personTypesHelper.isProfessional()}" />
<c:set var="isProfessionalOnly" value="${personTypesHelper.isProfessionalOnly()}" />

<sec:authorize access="hasPermission(null, 'PersonOrganisationRelationship','PersonOrganisationRelationship.View')" var="canViewOrganisationRelationship"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','GraphingPersonRelationships.View')" var="canGenerateGedcom"/>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<%-- if the subject context for the page is person --%>
	<sec:authorize access="hasPermission(null, 'PersonPersonRelationship.Add','PersonPersonRelationship.Add')" var="canAddPersonRelationshipPerm" />
	<sec:authorize access="hasPermission(null, 'PersonPersonRelationship.Update','PersonPersonRelationship.Update')" var="canEditPersonRelationshipPerm" />
	<sec:authorize access="hasPermission(null, 'PersonPersonRelationship.Close','PersonPersonRelationship.Close')" var="canClosePersonRelationshipPerm" />
	<sec:authorize access="hasPermission(null, 'PersonPersonRelationship.Remove','PersonPersonRelationship.Remove')" var="canRemovePersonRelationshipPerm" />
	<!-- Professional relationships for professionals (solely) can be modified even with no relational access in place. -->
	<c:set var="canAddPersonRelationship" value="${canAddPersonRelationshipPerm == 'true' && (accessAllowed == 'true' || isProfessionalOnly == 'true')}" />
	<c:set var="canEditPersonRelationship" value="${canEditPersonRelationshipPerm == 'true' && (accessAllowed == 'true' || isProfessionalOnly == 'true')}" />
	<c:set var="canClosePersonRelationship" value="${canClosePersonRelationshipPerm == 'true' && (accessAllowed == 'true' || isProfessionalOnly == 'true')}" />
	<c:set var="canRemovePersonRelationship" value="${canRemovePersonRelationshipPerm == 'true' && (accessAllowed == 'true' || isProfessionalOnly == 'true')}" />
	
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonOrganisationRelationship.Update')" var="canEditOrganisationRelationship"/>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonOrganisationRelationship.Remove')" var="canRemoveOrganisationRelationship"/>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonOrganisationRelationship.Close')" var="canCloseOrganisationRelationship"/>
	
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','ClientPersonTeamRelationship.Update')" var="canEditClientPersonTeamRelationship"/>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','ClientPersonTeamRelationship.Close')" var="canEndClientPersonTeamRelationship"/>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','ClientPersonTeamRelationship.Remove')" var="canRemoveClientPersonTeamRelationship"/>
</c:if>

<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="isNamedPersonRelationshipAvailable" value="${userContextAttributes.getContext('named-person-relationship-available')}" />

<%-- access relationship allowed attribute --%>
<sec:authentication property="principal.contextAttributes" var="userContextAttributes" />
<c:set var="relationshipAttributesContext" value="${userContextAttributes.getContext('relationship-attribute')}" />
<c:set var="allowedAttributes" value="${relationshipAttributesContext.getAllowedAttributeValuesAsJson()}" />

<div id="relationshipResults"></div>

<script>
Y.use('relationship-controller-view','yui-base', function(Y) {

	var allowedAttributes = JSON.parse('${allowedAttributes}');

  var subject = {
    subjectType: 'person',
    subjectName: '${esc:escapeJavaScript(person.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
    subjectId: '<c:out value="${person.id}"/>',
    //Needed by the relationship dialog - will be removed when that is updated to use the subject
    id : '<c:out value="${person.id}"/>',
    personTypes : '${esc:escapeJavaScript(person.personTypes)}',
    forename : '${esc:escapeJavaScript(person.forename)}',
    surname : '${esc:escapeJavaScript(person.surname)}',
    name:'${esc:escapeJavaScript(person.name)}',
    identifier:'${esc:escapeJavaScript(person.personIdentifier)}',
    personTypeArray:'${objectMapper.writeValueAsString(person.personTypes)}'
  };

  var permissions = {
      canAddRelationship: '${canAddPersonRelationship}' === 'true',
      canView: '${canViewPersonRelationship}' === 'true'||'${canViewOrganisationRelationship}' === 'true',
      canViewList: '${canViewPersonRelationship}' === 'true' ||'${canViewOrganisationRelationship}' === 'true',
      canViewGraph:'${canViewPersonRelationship}' === 'true'||'${canViewOrganisationRelationship}' === 'true',
      canViewRelationships: '${canViewPerson}'==='true',
      canAddPersonRelationship: '${canAddPersonRelationship}' === 'true',
      canViewPersonRelationship: '${canViewPersonRelationship}' === 'true',
      canEditPersonRelationship: '${canEditPersonRelationship}' === 'true',
      canClosePersonRelationship: '${canClosePersonRelationship}' === 'true' ,
      canRemovePersonRelationship: '${canRemovePersonRelationship}' === 'true',
      canViewOrganisationRelationship: '${canViewOrganisationRelationship}' === 'true',
      canRemoveOrganisationRelationship:'${canRemoveOrganisationRelationship}'==='true',
      canEditOrganisationRelationship: '${canEditOrganisationRelationship}' === 'true',
      canCloseOrganisationRelationship: '${canCloseOrganisationRelationship}'==='true',
      canEditClientPersonTeamRelationship: '${canEditClientPersonTeamRelationship}' ==='true',
      canEndClientPersonTeamRelationship: '${canEndClientPersonTeamRelationship}' ==='true',
      canRemoveClientPersonTeamRelationship: '${canRemoveClientPersonTeamRelationship}' ==='true',
      canGenerateGedcom:'${canGenerateGedcom}' === 'true'
  };

  
  var searchConfig={
          permissions:permissions,
  };
  
  var personPersonSearchConfig = Y.merge(searchConfig,{
    sortBy: [{personPersonRelationshipType:'desc'},
      {startDate:'desc'},
      {name:'asc'}
    ],
    labels: {
      id:'<s:message code="person.relationships.general.results.id" javaScriptEscape="true"/>',
      name:'<s:message code="person.relationships.general.results.name" javaScriptEscape="true"/>',
      relationship:'<s:message code="person.relationships.general.results.relationship" javaScriptEscape="true"/>',
      startDate:'<s:message code="person.relationships.general.results.startDate" javaScriptEscape="true"/>',
      closeDate:'<s:message code="person.relationships.general.results.closeDate" javaScriptEscape="true"/>',
      viewRelationship:'View relationships for {name}',
      relationshipView:'<s:message code="relationship.view" javaScriptEscape="true"/>',
      actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
      view: 'View relationship',
      viewTitle: 'View this relationship',
      add: 'Add relationship',
      addTitle: 'Add relationship',
      edit:'Edit relationship',
      editTitle:'Edit this relationship',
      close: 'End relationship',
      closeTitle:'End relationship',
      remove: 'Remove relationship',
      removeTitle: 'Remove this relationship'
    },
    noDataMessage: '<s:message code="person.relationships.general.no.results" javaScriptEscape="true"/>',
    permissions:permissions,
    url:'<s:url value="/rest/{subjectType}/{id}/personRelationship?s={sortBy}&pageNumber={page}&pageSize={pageSize}&differentialLevel=3"/>',
    hideNamedPersonRelationship: '${isNamedPersonRelationshipAvailable}'==='false',
		allowedAttributes: allowedAttributes
  });
  
  var personOrganisationSearchConfig = Y.merge(searchConfig,{
      permissions: permissions,
      sortBy: [{'organisationVO!name':'asc'},
        {startDate:'desc'}
      ],
      labels: {
        id:'<s:message code="person.organisation.relationships.general.results.id" javaScriptEscape="true"/>',
        name:'<s:message code="person.organisation.relationships.general.results.name" javaScriptEscape="true"/>',
        relationship:'<s:message code="person.organisation.relationships.general.results.type" javaScriptEscape="true"/>',
        startDate:'<s:message code="person.organisation.relationships.general.results.startDate" javaScriptEscape="true"/>',
        closeDate:'<s:message code="person.organisation.relationships.general.results.closeDate" javaScriptEscape="true"/>',
        viewOrganisation:'View organisation {organisationVO!name}',
                
        viewRelationship:'View relationships for {organisationVO!name}',
        relationshipView:'<s:message code="relationship.view" javaScriptEscape="true"/>',
        actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
        view: 'View relationship',
        viewTitle: 'View this relationship',
        add: 'Add relationship',
        addTitle: 'Add relationship',
        edit:'Edit relationship',
        editTitle:'Edit this relationship',
        close: 'End relationship',
        closeTitle:'End relationship',
        remove: 'Remove relationship',
        removeTitle: 'Remove this relationship'
      },
      noDataMessage: '<s:message code="person.organisation.relationships.general.no.results" javaScriptEscape="true"/>',
      url:'<s:url value="/rest/{subjectType}/{id}/organisationRelationship?s={sortBy}&pageNumber={page}&pageSize={pageSize}&relationshipsAndDates=true"/>',
			allowedAttributes: allowedAttributes
  });
  
  var filterContextConfig = {
    labels: {
      filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
      resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
      resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>',
      type:'<s:message code="person.relationship.find.filter.relationshipTypes.active" javaScriptEscape="true"/>',
      state:'<s:message code="person.relationship.find.filter.temporalStatusFilter.active" javaScriptEscape="true"/>',
      date:'<s:message code="person.relationship.find.filter.date.active" javaScriptEscape="true"/>',
      stateActiveMessage:'<s:message code="person.relationship.filter.stateFilterSelectedInfo" javaScriptEscape="true"/>',
      dateActiveMessage:'<s:message code="person.relationship.filter.dateFilterSelectedInfo" javaScriptEscape="true"/>'
    },
    container:'#relationshipFilterContext'
  };
  
  var filterConfig = {
    labels: {
      filterBy: '<s:message code="filter.by" javaScriptEscape="true"/>',
      relationshipTypes:{
        familial:'<s:message code="person.relationship.find.filter.relationshipType.familial" javaScriptEscape="true"/>',
        social:'<s:message code="person.relationship.find.filter.relationshipType.social" javaScriptEscape="true"/>',
        professional:'<s:message code="person.relationship.find.filter.relationshipType.professional" javaScriptEscape="true"/>',
      },
      state:{
        active:'<s:message code="person.relationship.find.filter.state.active" javaScriptEscape="true"/>',
        inactiveHistoric:'<s:message code="person.relationship.find.filter.state.inactiveHistoric" javaScriptEscape="true"/>',
        inactivePending:'<s:message code="person.relationship.find.filter.state.inactivePending" javaScriptEscape="true"/>'
      },
      dateFrom:'<s:message code="person.relationship.find.filter.entryDateRangeStart" javaScriptEscape="true"/>',
      dateTo:'<s:message code="person.relationship.find.filter.entryDateRangeEnd" javaScriptEscape="true"/>',
      resetTitle:'<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
      reset:'<s:message code="filter.reset" javaScriptEscape="true"/>',
      validation:{
        dateTo:'<s:message code="person.relationship.find.filter.endBeforeStartDate" javaScriptEscape="true"/>'
      }
    }
  };

  var downloadDialogConfig={
    views:{      
      generateGedcom:{
        header: {
          text: '<s:message code="relationship.generateGedcom.header" javaScriptEscape="true"/>',
          icon: 'fa fa-file-code-o'
        },
        narrative: {
          summary: '<s:message code="relationship.generateGedcom.dialog.summary" javaScriptEscape="true" />',
          description: '{{this.subjectName}} ({{this.subjectIdentifier}})'
        },
        labels:{
          generateComplete:'<s:message code="relationship.generateGedcom.dialog.generateComplete" javaScriptEscape="true"/>',
          downloadPrompt:'<s:message code="relationship.generateGedcom.dialog.downloadPrompt" javaScriptEscape="true"/>',
          download:'<s:message code="relationship.generateGedcom.dialog.download" javaScriptEscape="true"/>'
        },
        config:{
          downloadURL:'<s:url value="/rest/graphingPersonRelationships/{id}/content"/>'
        }
      },
      download:{
        header: {
          text: '<s:message code="relationship.generateGedcom.download.header" javaScriptEscape="true"/>',
          icon: 'fa fa-file-code-o'
        },
        labels:{
          message: '<s:message code="relationship.generateGedcom.dialog.download" javaScriptEscape="true"/>'
        }
      }
    },
    generateURL:'<s:url value="/rest/graphingPersonRelationships"/>'
  };
  var config = {
    container: '#relationshipResults',
    personPersonConfig:{
      searchConfig: personPersonSearchConfig,
      filterConfig: filterConfig,
      filterContextConfig: filterContextConfig,
    },
    personOrganisationConfig:{
      searchConfig: personOrganisationSearchConfig
    },
    permissions: permissions,
    subject: subject || {},
    tabs:{
      personRelationships:{
        label:'People'
      },
      organisationRelationships:{
        label:'Organisations'
      }
    },
    downloadDialogConfig:downloadDialogConfig,
    viewRelationshipsURL:'<s:url value="/relationships/person?id={personId}" />',
    viewOrganisationURL:'<s:url value="/organisation?id={organisationId}" />',
    viewGraphURL:'<s:url value="/relationships/persondiagram?id={subjectId}" />',
    client_team_id:'${relationshipClassClientTeamId}'
  };

  var relationshipView = new Y.app.relationship.RelationshipControllerView(config).render();
});
</script>