    <%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
  <sec:authorize access="hasPermission(null, 'ChildProtectionEnquiry','ChildProtectionEnquiry.Add')" var="canAddCPEnquiry" />
  <sec:authorize access="hasPermission(null, 'ChildProtectionEnquiry','ChildProtectionEnquiry.View')" var="canViewCPEnquiry" />
  <sec:authorize access="hasPermission(null, 'ChildProtectionEnquiry','ChildProtectionEnquiry.Delete')" var="canDeleteCPEnquiry" />

<div id="childProtectionAccordions"></div>

<script>
Y.use('childprotection-controller-view', function(Y) {
    var subject = {
        subjectId: '<c:out value="${person.id}"/>',
        subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
        subjectName: '${esc:escapeJavaScript(person.name)}',
        subjectType: 'person'
    };

    var subjectNarrative = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

    var cpEquiryPermissions={
        canAddCPEnquiry: '${canAddCPEnquiry}' === 'true',
        canViewCPEnquiry: '${canViewCPEnquiry}' === 'true',
        canDeleteCPEnquiry: '${canDeleteCPEnquiry}' === 'true'
    };

    var commonDialogLabels={
        autoCompletePlaceholder: '<s:message code="common.label.ac.placeholder" javaScriptEscape="true" />',
        subjectName: '<s:message code="childprotection.enquiry.label.subjectName" javaScriptEscape="true"/>',
        enquiryReason: '<s:message code="common.column.reason" javaScriptEscape="true"/>',
        enquirerTeam: '<s:message code="childprotection.enquiry.label.enquirerTeam" javaScriptEscape="true"/>',
        enquirerAddress: '<s:message code="childprotection.enquiry.label.address" javaScriptEscape="true"/>',
        enquiryRequestDate: '<s:message code="childprotection.enquiry.label.enquiryRequestDate" javaScriptEscape="true"/>'
    };

    var cpEquirySearchConfig = {
        title: '<s:message code="common.find.results" javaScriptEscape="true"/>',
        sortBy: [{enquiryRequestDate: 'desc'}],
        labels: {
            enquiryReason: '<s:message code="common.column.reason" javaScriptEscape="true"/>',
            enquiryRequestDate: '<s:message code="childprotection.enquiry.label.enquiryRequestDate" javaScriptEscape="true"/>',
            subjectName: '<s:message code="childprotection.enquiry.label.subjectName" javaScriptEscape="true"/>',
            viewEnquiry: '<s:message code="common.find.results.view" javaScriptEscape="true"/>',
            removeEnquiry: '<s:message code="common.find.results.remove" javaScriptEscape="true"/>',
            updateEnquiry: '<s:message code="common.find.results.update" javaScriptEscape="true"/>',
            actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>'
        },
        noDataMessage: '<s:message code="childprotection.enquiry.find.no.results" javaScriptEscape="true"/>',
        permissions: cpEquiryPermissions,
        url: '<s:url value="/rest/person/{subjectId}/childProtectionEnquiry?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
        subject: subject
    };

    var cpEquiryDialogConfig = {
        views: {
            addChildProtectionEnquiry: {
                header: {
                    text: '<s:message code="childprotection.enquiry.dialog.add.header" javaScriptEscape="true" />',
                    icon: ''
                },
                narrative: {
                    summary: '<s:message code="childprotection.enquiry.dialog.add.summary" javaScriptEscape="true" />',
                    description: subjectNarrative
                },
                labels: Y.merge(commonDialogLabels, {
                    subjectType: {
                        person:'<s:message code="common.subjectType.person" javaScriptEscape="true" />',
                        organisation:'<s:message code="common.subjectType.organisation" javaScriptEscape="true" />'
                    },
                    person:{
                        placeholder: '<s:message code="common.label.ac.person.placeholder" javaScriptEscape="true" />'
                    },
                    organisation:{
                        placeholder: '<s:message code="common.label.ac.organisation.placeholder" javaScriptEscape="true" />'
                    }
                }),
                validationLabels: {
                    enquiryRequestDateMandatory: '<s:message code="childprotection.enquiry.dialog.enquiryRequestDateMandatory" javaScriptEscape="true"/>',
                    enquirerSubjectMandatory: '<s:message code="childprotection.enquiry.dialog.enquirerSubjectMandatory" javaScriptEscape="true"/>',
                    enquiryReasonMandatory: '<s:message code="childprotection.enquiry.dialog.enquiryReasonMandatory" javaScriptEscape="true"/>'
                },
                width: 500,
                config: {
                    url: '<s:url value="/rest/childProtectionEnquiry/"/>',
                    enquirerTeamListURL: '<s:url value="/rest/person/{id}/teamRelationship/professional?temporalStatusFilter=ACTIVE&sort={sortBy}&pageSize=-1"><s:param name="sortBy" value="[{\"roleBOrganisation.name\":\"asc\"}]" /></s:url>',
                    enquirerTeamAddressURL: '<s:url value="/rest/organisation/{id}/address?pageNumber=-1&pageSize=-1"/>',
                    personAutocompleteURL: '<s:url value="/rest/person?personType=PROFESSIONAL&nameOrIdentifier={query}&pageSize={maxResults}" />',
                    organisationAutocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>'
                },
                successMessage: '<s:message code="childprotection.enquiry.dialog.add.details.success.message" javaScriptEscape="true" />'
            },
            viewChildProtectionEnquiry: {
                header: {
                    text: '<s:message code="childprotection.enquiry.dialog.view.header" javaScriptEscape="true" />',
                    icon: ''
                },
                narrative: {
                    summary: '<s:message code="childprotection.enquiry.dialog.view.summary" javaScriptEscape="true" />',
                    description: subjectNarrative
                },
                labels: commonDialogLabels,
                width: 500,
                config: {
                    url: '<s:url value="/rest/childProtectionEnquiry/{id}"/>'
                }
            },
            deleteChildProtectionEnquiry: {
                header: {
                    text: '<s:message code="childprotection.enquiry.dialog.remove.header" javaScriptEscape="true"/>',
                    icon: ''
                },
                narrative: {
                    summary: '<s:message code="childprotection.enquiry.dialog.remove.summary" javaScriptEscape="true" />',
                    description: subjectNarrative
                },
                labels:{
                    deleteConfirmation: '<s:message code="childprotection.enquiry.dialog.remove.prompt" javaScriptEscape="true" />'
                },
                config:{
                    url:'<s:url value="/rest/childProtectionEnquiry/{id}"/>'
                },
                successMessage: '<s:message code="childprotection.enquiry.dialog.remove.success"/>'
            }
        }
    };

    var config = {
      container: '#childProtectionAccordions',
      subject: subject,
      childProtectionEnquiryAccordionConfig: {
        permissions:cpEquiryPermissions,
        searchConfig: cpEquirySearchConfig,
        dialogConfig: cpEquiryDialogConfig,
        tablePanelConfig:{
            title: '<s:message code="childprotection.enquiry.page.title" javaScriptEscape="true"/>',
            closed: false
        },
        addButtonConfig: {
            icon:'fa-plus-circle',
            title:'<s:message code="childprotection.enquiry.label.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
            ariaLabel:'<s:message code="childprotection.enquiry.label.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
            label:'<s:message code="button.add" javaScriptEscape="true"/>'
        }
      }
    };

    var childProtectionView = new Y.app.childprotection.ChildProtectionControllerView(config);

    childProtectionView.render();
});
</script>
