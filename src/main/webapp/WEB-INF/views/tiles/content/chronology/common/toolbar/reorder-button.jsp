<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.UPDATE')" var="canReorder"/>
</c:if>

<c:if test="${canReorder eq true}">

  <tilesx:useAttribute name="first" ignore="true" />
  <tilesx:useAttribute name="last" ignore="true" />

  <c:if test="${first eq true}">
    <c:set var="fStyle" value="first" />
  </c:if>
  <c:if test="${last eq true}">
    <c:set var="lStyle" value="last" />
  </c:if>


  <c:choose>
    <c:when test="${not empty group}">
      <c:set var="name" value="${group.name}"/>
    </c:when>
    <c:otherwise>
      <c:set var="name" value="${person.name}"/>
    </c:otherwise>
  </c:choose>

  <%-- Button markup--%>
<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
  <a id="reorderButton" href="#none" data-button-action="reorder" class="pure-button pure-button-disabled usp-fx-all pure-button-loading"
    title='<s:message code="chronology.reorder.button" arguments="${name}" htmlEscape="true" />' aria-disabled="true"
    aria-label='<s:message code="chronology.reorder.button" arguments="${name}" htmlEscape="true" />' tabindex="0">
    <i class="fa fa-bars"></i>
    <span><s:message code="button.order" /></span>
  </a>
</li>

</c:if>
