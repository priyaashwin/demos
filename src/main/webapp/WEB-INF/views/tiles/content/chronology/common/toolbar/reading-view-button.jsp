<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.View')" var="canView"/>

<c:if test="${canView eq true}">
  <tilesx:useAttribute name="first" ignore="true"/>
  <tilesx:useAttribute name="last" ignore="true"/>
  <c:if test="${first eq true }">
    <c:set var="fStyle" value="first"/>
  </c:if>
  <c:if test="${last eq true }">
    <c:set var="lStyle" value="last"/>
  </c:if>
  
<%-- Button markup--%>
<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
  <a id="readingButton" href="#none" data-button-action="readingView" class="pure-button pure-button-disabled usp-fx-all pure-button-loading"
    title='<s:message code="button.view.inactive" /> ' aria-disabled="true"
    aria-label='<s:message code="button.view.inactive"/>' tabindex="0"> 
    <i class="fa fa-book"></i>
    <span><s:message code="button.view" /></span>
  </a>
</li>
</c:if>