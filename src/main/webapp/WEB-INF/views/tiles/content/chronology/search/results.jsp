<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.View')" var="canView" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.Add')" var="canAdd" />
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.Update')" var="canUpdate" />
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.Remove')" var="canRemove" />
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.COMPLETE')" var="canComplete" />
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.UNCOMPLETE')" var="canUncomplete" />
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.Delete')" var="canDelete" />
	<sec:authorize access="hasPermission(null, 'ChronologyEntry','ChronologyEntry.UPDATE')" var="canReorder" />
</c:if>

<s:eval expression="@appSettings.isChronologyImpactEnabled()" var="impactEnabled" scope="page" />

<div id="chronologyResults"></div>

<script>
Y.use('chronology-controller-view','yui-base', 'json-parse', function(Y) {
<c:choose>
  <c:when test="${not empty groupList}">
    //The list of valid groups that are used in the filter
    var groupList =  Y.Array(Y.JSON.parse('${esc:escapeJavaScript(groupList)}'));
  </c:when>
  <c:otherwise>
    var groupList=[];
  </c:otherwise>    
</c:choose>
  
  var impactEnabled='${impactEnabled}'==='true';
  
  var practitioner = {
    personName: '${esc:escapeJavaScript(loginPersonName)}',
    personId: '${esc:escapeJavaScript(loginPersonId)}',
    organisationName: '${esc:escapeJavaScript(loginPersonOrganisationName)}'
  };
  
<c:choose>
  <c:when test="${not empty group}">
  var subject = {
    subjectType: 'group',
    subjectName: '${esc:escapeJavaScript(group.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(group.groupIdentifier)}',
    subjectId: '<c:out value="${group.id}"/>'
  };
  </c:when>
  <c:otherwise>
  var subject = {
    subjectType: 'person',
    subjectName: '${esc:escapeJavaScript(person.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
    subjectId: '<c:out value="${person.id}"/>',
  };
  </c:otherwise>
</c:choose> 

 var permissions = {
    canView: '${canView}' === 'true',
    canPrint: '${canView}' === 'true',
    canOutput: '${canView}' === 'true',
    canUpdate: '${canUpdate}' === 'true',
    canAdd: '${canAdd}' === 'true',
    canRemove: '${canRemove}' === 'true',
    canDelete: '${canDelete}' === 'true',
    canComplete: '${canComplete}' === 'true',
    canUncomplete: '${canUncomplete}' === 'true',
    canReorder: '${canReorder}' === 'true',
    canReadingView: '${canView}' === 'true'
  };
  
  var commonLabels = {
    event: '<s:message code="chronologyEntry.event" javaScriptEscape="true"/>',
    eventDate: '<s:message code="chronologyEntry.eventDate" javaScriptEscape="true"/>',
    entryType: '<s:message code="chronologyEntry.entryType" javaScriptEscape="true"/>',
    entrySubType: '<s:message code="chronologyEntry.entrySubType" javaScriptEscape="true"/>',
    action: '<s:message code="chronologyEntry.action" javaScriptEscape="true"/>',
    impact: '<s:message code="chronologyEntry.impact" javaScriptEscape="true"/>',
    impactPositive: '<s:message code="chronologyEntry.impact.positive" javaScriptEscape="true"/>',
    impactNegative: '<s:message code="chronologyEntry.impact.negative" javaScriptEscape="true"/>',
    impactNeutral: '<s:message code="chronologyEntry.impact.neutral" javaScriptEscape="true"/>',
    impactUnknown: '<s:message code="chronologyEntry.impact.unknown" javaScriptEscape="true"/>',
    source: '<s:message code="common.source" javaScriptEscape="true"/>',
    otherSource: '<s:message code="common.source.other" javaScriptEscape="true"/>',
    sourceOrganisation: '<s:message code="common.sourceOrganisation" javaScriptEscape="true"/>',
    otherSourceOrganisation: '<s:message code="common.sourceOrganisation.other" javaScriptEscape="true"/>',
    sourceMustBeOtherOrNull: '<s:message code="common.sourceMustBeOtherOrNull" javaScriptEscape="true"/>',
    practitioner: '<s:message code="chronologyEntry.practitioner" javaScriptEscape="true"/>',
    practitionerOrganisation: '<s:message code="chronologyEntry.practitionerOrganisation" javaScriptEscape="true"/>',
    recordedDate: '<s:message code="chronologyEntry.recordedDate" javaScriptEscape="true"/>',
    editedDate: '<s:message code="chronologyEntry.editedDate" javaScriptEscape="true"/>',
    completeConfirm: '<s:message code="chronologyEntry.completeConfirm" javaScriptEscape="true"/>',
    chronologyTabName: '<s:message code="chronology.tabs.chronology"/>',
    visibilityTabName: '<s:message code="chronology.tabs.visibility"/>',
    groupMembers: '<s:message code="chronologyEntry.group.members" javaScriptEscape="true"/>',
    visibleToAll: '<s:message code="chronologyEntry.visibleToAll" javaScriptEscape="true"/>',
    selectAllMembers: '<s:message code="chronologyEntry.selectAllMembers" javaScriptEscape="true"/>',
    visibleToAllTitle: '<s:message code="chronologyEntry.visibleToAllCheckboxLabelPopupMessageTitle" javaScriptEscape="true"/>',
    visibleToAllContent: '<s:message code="chronologyEntry.visibleToAllCheckboxLabelPopupMessageContent" javaScriptEscape="true"/>'

  };

  var fuzzyDateLabels = {
    date: '<s:message code="chronologyEntry.eventDate" javaScriptEscape="true"/>',
    year: '<s:message code="fuzzyDate.year" javaScriptEscape="true"/>',
    month: '<s:message code="fuzzyDate.month" javaScriptEscape="true"/>',
    day: '<s:message code="fuzzyDate.day" javaScriptEscape="true"/>',
    hour: '<s:message code="fuzzyDate.hour" javaScriptEscape="true"/>',
    minute: '<s:message code="fuzzyDate.minute" javaScriptEscape="true"/>',
    second: '<s:message code="fuzzyDate.second" javaScriptEscape="true"/>'
  };
  
  var searchConfig = {
          sortBy: [{
            'eventDate!calculatedDate': 'desc'
          }],
          labels: {
            recordedDate: '<s:message code="chronology.results.recordedDate" javaScriptEscape="true"/>',
            editedDate: '<s:message code="chronology.results.editedDate" javaScriptEscape="true"/>',
            calculatedDate: '<s:message code="chronology.results.eventDate" javaScriptEscape="true"/>',
            event: '<s:message code="chronology.results.event" javaScriptEscape="true"/>',
            action: '<s:message code="chronology.results.actionTaken" javaScriptEscape="true"/>',
            entryType: '<s:message code="chronology.results.entryType" javaScriptEscape="true"/>',
            entrySubType: '<s:message code="chronology.results.entrySubType" javaScriptEscape="true"/>',
            impact: '<s:message code="chronology.results.impact" javaScriptEscape="true"/>',
            source: '<s:message code="chronology.results.source" javaScriptEscape="true"/>',
            practitioner: '<s:message code="chronology.results.practitioner" javaScriptEscape="true"/>',
            status: '<s:message code="chronology.results.status" javaScriptEscape="true"/>',
            actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
            
            view: '<s:message code="chronology.results.view" javaScriptEscape="true"/>',
            viewTitle: '<s:message code="chronology.results.viewTitle" javaScriptEscape="true"/>',
            complete: '<s:message code="chronology.results.complete" javaScriptEscape="true"/>',
            completeTitle: '<s:message code="chronology.results.completeTitle" javaScriptEscape="true"/>',
            uncomplete: '<s:message code="chronology.results.uncomplete" javaScriptEscape="true"/>',
            uncompleteTitle: '<s:message code="chronology.results.uncompleteTitle" javaScriptEscape="true"/>',
            deleteEntry: '<s:message code="chronology.results.delete" javaScriptEscape="true"/>',
            deleteEntryTitle: '<s:message code="chronology.results.deleteTitle" javaScriptEscape="true"/>',
            remove: '<s:message code="chronology.results.remove" javaScriptEscape="true"/>',
            removeTitle: '<s:message code="chronology.results.removeTitle" javaScriptEscape="true"/>'
          },
          initialState: 'initial',
          resultsCountNode: Y.one('#chronologyResultsCount'),
          noDataMessage: '<s:message code="chronology.find.no.results" javaScriptEscape="true"/>',
          permissions: permissions,
          url: '<s:url value="/rest/{subjectType}/{id}/chronologyEntry?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
          urlForView: '<s:url value="/rest/chronologyEntry/{id}"/>'
        };
  
  var filterContextConfig = {
          labels: {
            filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
            date: '<s:message code="chronology.find.filter.date.active" javaScriptEscape="true"/>',
            recordedDate: '<s:message code="chronology.find.filter.recordedDate.active" javaScriptEscape="true"/>',
            editedDate: '<s:message code="chronology.find.filter.editedDate.active" javaScriptEscape="true"/>',
            practitioner: '<s:message code="chronology.find.filter.practitioner.active" javaScriptEscape="true"/>',
            source: '<s:message code="chronology.find.filter.source.active" javaScriptEscape="true"/>',
            impact: '<s:message code="chronology.find.filter.impact.active" javaScriptEscape="true"/>',
            status: '<s:message code="chronology.find.filter.status.active" javaScriptEscape="true"/>',
            practitionerOrganisation: '<s:message code="chronology.find.filter.practitionerOrganisation.active" javaScriptEscape="true"/>',
            entryType: '<s:message code="chronology.find.filter.entryType.active" javaScriptEscape="true"/>',
            sourceOrganisation: '<s:message code="chronology.find.filter.sourceOrganisation.active" javaScriptEscape="true"/>',
            group: '<s:message code="chronology.find.filter.group.active" javaScriptEscape="true"/>',
            filterDisabled: '<s:message code="chronology.find.filter.disabled" javaScriptEscape="true"/>',
            resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
            resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>'
          }
        };
  
  var filterConfig = {
          labels: {
            filterBy: '<s:message code="filter.by" javaScriptEscape="true"/>',
            dateFilter: '<s:message code="chronology.find.filter.dateFilter" javaScriptEscape="true"/>',
            entryStartDate: '<s:message code="chronology.find.filter.eventDateRangeStart" javaScriptEscape="true"/>',
            entryEndDate: '<s:message code="chronology.find.filter.eventDateRangeEnd" javaScriptEscape="true"/>',
            recordedStartDate: '<s:message code="chronology.find.filter.recordedDateRangeStart" javaScriptEscape="true"/>',
            recordedEndDate: '<s:message code="chronology.find.filter.recordedDateRangeEnd" javaScriptEscape="true"/>',
            editedStartDate: '<s:message code="chronology.find.filter.editedDateRangeStart" javaScriptEscape="true"/>',
            editedEndDate: '<s:message code="chronology.find.filter.editedDateRangeEnd" javaScriptEscape="true"/>',
            dateRangeLinks: '<s:message code="chronology.find.filter.dateRangeLinks" javaScriptEscape="true"/>',
            dateRangeLastWeek: '<s:message code="chronology.find.filter.entryDateRangeLastWeek" javaScriptEscape="true"/>',
            dateRangeLastMonth: '<s:message code="chronology.find.filter.entryDateRangeLastMonth" javaScriptEscape="true"/>',
            dateRangeLastYear: '<s:message code="chronology.find.filter.entryDateRangeLastYear" javaScriptEscape="true"/>',
            peopleFilter: '<s:message code="chronology.find.filter.peopleFilter" javaScriptEscape="true"/>',
            practitioner: '<s:message code="chronology.find.filter.practitioner" javaScriptEscape="true"/>',
            practitionerOrganisation: '<s:message code="chronology.find.filter.practitionerOrganisation" javaScriptEscape="true"/>',
            source: '<s:message code="chronology.find.filter.source" javaScriptEscape="true"/>',
            otherSource:'<s:message code = "chronology.find.filter.otherSource" javaScriptEscape="true"/>',
            sourceOrganisation: '<s:message code="chronology.find.filter.sourceOrganisation" javaScriptEscape="true"/>',
            otherSourceOrganisation:'<s:message code = "chronology.find.filter.otherSourceOrganisation" javaScriptEscape="true"/>',
            typeFilter: '<s:message code="chronology.find.filter.typeFilter" javaScriptEscape="true"/>',
            entryType: '<s:message code="chronology.find.filter.entryType" javaScriptEscape="true"/>',
            impact: '<s:message code="chronology.find.filter.impact" javaScriptEscape="true"/>',
            impactPositive: '<s:message code="chronology.find.filter.impact.positive" javaScriptEscape="true"/>',
            impactNegative: '<s:message code="chronology.find.filter.impact.negative" javaScriptEscape="true"/>',
            impactNeutral: '<s:message code="chronology.find.filter.impact.neutral" javaScriptEscape="true"/>',
            impactUnknown: '<s:message code="chronology.find.filter.impact.unknown" javaScriptEscape="true"/>',
            status:'<s:message code="chronology.find.filter.status" javaScriptEscape="true"/>',
            includeDraft:'<s:message code="chronology.find.filter.status.draft" javaScriptEscape="true"/>',
            includeComplete:'<s:message code="chronology.find.filter.status.complete" javaScriptEscape="true"/>',
            includeDeleted:'<s:message code="chronology.find.filter.status.deleted" javaScriptEscape="true"/>',
            groupFilter: '<s:message code="chronology.find.filter.groupFilter" javaScriptEscape="true"/>',
            excludeAllGroups: '<s:message code="chronology.find.filter.group.excludeAllGroups" javaScriptEscape="true"/>',
            validation:{
              startDateBeforeEndDate : '<s:message code = "chronology.find.filter.endBeforeStartDate" javaScriptEscape="true"/>',
              startDateBeforeEndRecordedDate : '<s:message code = "chronology.find.filter.endBeforeStartRecordedDate" javaScriptEscape="true"/>',
              startDateBeforeEndEditedDate : '<s:message code = "chronology.find.filter.endBeforeStartEditedDate" javaScriptEscape="true"/>'
            }
          },
          permissions:{
            canDelete:permissions.canDelete
          },
          subject:subject,
          groupList:groupList,          
          filterItemsUrl:'<s:url value="/rest/chronology/{id}/filterItems"/>',
          recordId:'${chronology.id}',
          chronologyRecordUrl:'<s:url value="/rest/{subjectType}/{id}/chronology"/>'
        };
  

  var headerIcon = 'eclipse-chronology',
    narrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

  var dialogConfig = {
    views: {
      add: {
        header: {
          text: '<s:message code="chronology.dialog.add.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.addChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="chronologyEntry.add.success.message" javaScriptEscape="true"/>',
        labels: commonLabels,
        validationLabels: {
          eventDateValidationErrorMessage: '<s:message code="chronologyEntry.invalidEventDate.message" javaScriptEscape="true"/>',
          entryValidationErrorMessage: '<s:message code="chronologyEntry.mandatoryEntry.message" javaScriptEscape="true"/>',
          entryTypeValidationErrorMessage: '<s:message code="chronologyEntry.mandatoryEntryType.message" javaScriptEscape="true"/>',
          otherSource:'<s:message code="NotBlank.source.other" javaScriptEscape="true" />',
          otherSourceOrganisation:'<s:message code="NotBlank.sourceOrganisation.other" javaScriptEscape="true" />'
        },
        config: {
          url: '<s:url value="/rest/{subjectType}/{subjectId}/chronologyEntry"/>',
          fuzzyDateLabels: fuzzyDateLabels
        }
      },
      view: {
        header: {
          text: '<s:message code="chronology.dialog.view.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.viewChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        config: {
          url: '<s:url value="/rest/chronologyEntry/{id}"/>'
        },
        labels: commonLabels
      },
      edit: {
        header: {
          text: '<s:message code="chronology.dialog.edit.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.viewChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="chronologyEntry.edit.success.message" javaScriptEscape="true"/>',
        labels: commonLabels,
        validationLabels: {
          otherSource:'<s:message code="NotBlank.source.other" javaScriptEscape="true" />',
          otherSourceOrganisation:'<s:message code="NotBlank.sourceOrganisation.other" javaScriptEscape="true" />'
        },
        config: {
          completeURL: '<s:url value="/rest/chronologyEntry/{id}/status?action=complete"/>',
          url: '<s:url value="/rest/chronologyEntry/{id}"/>',
          fuzzyDateLabels: fuzzyDateLabels
        }
      },
      editComplete: {
        header: {
          text: '<s:message code="chronology.dialog.edit.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.viewChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="chronologyEntry.edit.success.message" javaScriptEscape="true"/>',
        labels: commonLabels,
        validationLabels: {
          otherSource:'<s:message code="NotBlank.source.other" javaScriptEscape="true" />',
          otherSourceOrganisation:'<s:message code="NotBlank.sourceOrganisation.other" javaScriptEscape="true" />'
        },
        config: {
          url: '<s:url value="/rest/chronologyEntry/{id}"/>',
          fuzzyDateLabels: fuzzyDateLabels
        }
      },
      hardDelete: {
        header: {
          text: '<s:message code="chronology.dialog.remove.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.removeChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="chronologyEntry.remove.success.message" javaScriptEscape="true"/>',
        config: {
          url: '<s:url value="/rest/chronologyEntry/{id}"/>'
        },
        labels: Y.merge(commonLabels, {
          personRemoveConfirm: '<s:message code="chronologyEntry.removeConfirm" javaScriptEscape="true"/>',
          groupRemoveConfirm: '<s:message code="group.chronologyEntry.removeConfirm" javaScriptEscape="true"/>',
        })
      },
      softDelete: {
        header: {
          text: '<s:message code="chronology.dialog.delete.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.deleteChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="chronologyEntry.delete.success.message" javaScriptEscape="true"/>',
        config: {
          url: '<s:url value="/rest/chronologyEntry/{id}/status?action=deleted"/>'
        },
        labels: Y.merge(commonLabels, {
          personDeleteConfirm: '<s:message code="chronologyEntry.deleteConfirm" javaScriptEscape="true"/>',
          groupDeleteConfirm: '<s:message code="group.chronologyEntry.deleteConfirm" javaScriptEscape="true"/>',
        })
      },
      complete: {
        header: {
          text: '<s:message code="chronology.dialog.complete.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.completeChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        labels: Y.merge(commonLabels, {
          completeConfirm: '<s:message code="chronologyEntry.completeConfirm" javaScriptEscape="true"/>'
        }),
        config: {
          completeURL: '<s:url value="/rest/chronologyEntry/{id}/status?action=complete"/>',
          url: '<s:url value="/rest/chronologyEntry/{id}"/>'
        },
        successMessage: '<s:message code="chronologyEntry.complete.success.message" javaScriptEscape="true"/>'
      },
      uncomplete: {
        header: {
          text: '<s:message code="chronology.dialog.uncomplete.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="chronologyEntry.uncompleteChronologyEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        labels: Y.merge(commonLabels, {
          uncompleteConfirm: '<s:message code="chronologyEntry.uncompleteConfirm" javaScriptEscape="true"/>'
        }),
        config: {
          uncompleteURL: '<s:url value="/rest/chronologyEntry/{id}/status?action=draft" />',
          url: '<s:url value="/rest/chronologyEntry/{id}"/>'
        },
        successMessage: '<s:message code="chronologyEntry.uncomplete.success.message" javaScriptEscape="true"/>'
      }
    },
    //config for nested confirm complete dialog view
    confirmComplete:{
      header: {
        text: '<s:message code="chronology.dialog.complete.header" javaScriptEscape="true"/>',
        icon: headerIcon
      },
    },
    activeGroupMembersURL: '<s:url value="/rest/group/{id}/personMembership?temporalStatusFilter=ACTIVE&pageNumber=-1&pageSize=-1"/>'
  };

  
  var outputNarrativeDescription= '{{this.subjectName}} ({{this.subjectIdentifier}})';
  
  var outputDialogConfig;
  
  if(subject.subjectType==='person'){
    outputDialogConfig={
      views:{      
        produceDocument:{
          type:'app.ChronologyOutputDocumentView',
          header: {
            text: '<s:message code="chronology.output.header" javaScriptEscape="true"/>',
            icon: 'fa fa-file'
          },
          narrative: {
            summary: '<s:message code="produceDocument.dialog.summary" javaScriptEscape="true" />',
            description: outputNarrativeDescription
          },
          labels:{
            outputCategory: '<s:message code="produceDocument.dialog.category" javaScriptEscape="true"/>',
            outputCategoryPrompt: '<s:message code="produceDocument.dialog.category.select.blank" javaScriptEscape="true"/>',
            availableTemplates: '<s:message code="produceDocument.dialog.availableTemplates" javaScriptEscape="true"/>',
            outputFormat: '<s:message code="produceDocument.dialog.outputFormat" javaScriptEscape="true"/>'
          },
          config:{
            templateListURL: '<s:url value="/rest/outputTemplate?status=PUBLISHED&context=CHRONOLOGY&asList=true"/>',
            generateURL:'<s:url value="/rest/documentRequest"/>',
            documentType: 'CHRONOLOGY',
            successMessage: '<s:message code="produceDocument.dialog.success.generateInProgress" javaScriptEscape="true" />',
            failureMessage: '<s:message code="produceDocument.dialog.failure.generateFailed" javaScriptEscape="true" />'
          }
        }
      }
    };
  }
  

  var config = {
    container: '#chronologyResults',
    impactEnabled: impactEnabled,
    searchConfig: searchConfig,
    filterConfig: filterConfig,
    filterContextConfig: filterContextConfig,
    permissions: permissions,
    subject: subject || {},
    practitioner: practitioner,
    readingCfg: {
      triggerNode: '#readingButton',
      active: {
        className: 'pure-button-active',
        title: '<s:message code="button.view.active" />'
      },
      inactive: {
        title: '<s:message code="button.view.inactive" />'
      }
    },
    reorderCfg: {
      triggerNode: '#reorderButton',
      active: {
        className: 'pure-button-primary',
        icon: 'fa fa-check-square',
        label: '<s:message code="button.finish" />'
      },
      inactive: {
        icon: 'fa fa-bars',
        label: '<s:message code="button.order" />'
      }
    },
    orderByCfg: {
      triggerNode: '#order-by'
    },
    labels:{
      checklistLinkFrom:'The work list task is waiting for a {entryType} chronology entry in a {entryState} state.'
    },
    dialogConfig: dialogConfig,
    outputDialogConfig: outputDialogConfig,
    updateRowUrl: '<s:url value="/rest/chronologyEntry/{id}/neighbour?position={position}&neighbourId={neighbourId}"/>',
    printURL:'<s:url value="/chronology/{contextType}?id={id}&print=true" />'
  };

  var chronologyView = new Y.app.chronology.ChronologyControllerView(config).render();
});
</script>

