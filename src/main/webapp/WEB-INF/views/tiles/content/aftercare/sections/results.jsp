<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'AfterCare','AfterCare.GET')" var="canView" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'AfterCare','AfterCare.ADD')" var="canAdd" />
	<sec:authorize access="hasPermission(null, 'AfterCareProvision','AfterCareProvision.ADD')" var="canAddProvision" />
	<sec:authorize access="hasPermission(null, 'AfterCare','AfterCare.CLOSE')" var="canClose" />
	<sec:authorize access="hasPermission(null, 'AfterCare','AfterCareProvision.CLOSE')" var="canCloseProvision" />
</c:if>

<div id="afterCareResultsContainer"></div>


<script>
Y.use('aftercare-controller-view', function(Y) {
    var permissions = {
        canView: '${canView}' === 'true',
        canAddAfterCare: '${canAdd}' === 'true' && '${canAddAfterCareDetails}' === 'true',
        canAddProvision: '${canAddProvision}' === 'true' && '${canAddAfterCareDetails}' === 'true',
        canEndRecord: '${canClose}' === 'true',
        canEditProvision: '${canCloseProvision}' === 'true'
    };

    var subject = {
          subjectId: '<c:out value="${person.id}"/>',
          subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
          subjectName: '${esc:escapeJavaScript(person.name)}',
          eligibility: '<c:out value="${afterCareEligibility}"/>'
        };

    var subjectNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

    var labels = {
	      addAfterCare: {
	          successMessage: '<s:message code="aftercare.dialog.add.successMessage" javaScriptEscape="true"/>',
	          receivingAfterCare: '<s:message code="aftercare.dialog.add.receivingAfterCare" javaScriptEscape="true"/>',
	          eligibility: '<s:message code="aftercare.dialog.add.eligibility" javaScriptEscape="true"/>',
	          eligibilityDesc: {
	            compulsory: '<s:message code="aftercare.dialog.add.eligibility.compulsory" javaScriptEscape="true"/>',
	            discretionary: '<s:message code="aftercare.dialog.add.eligibility.discretionary" javaScriptEscape="true"/>',
	          },
	          contactDate: '<s:message code="aftercare.dialog.add.contactDate" javaScriptEscape="true"/>',
	          economicActivity: '<s:message code="aftercare.dialog.add.economicActivity" javaScriptEscape="true"/>',
	          accommodation: '<s:message code="aftercare.dialog.add.accommodation" javaScriptEscape="true"/>',
	          servicesEndReason: '<s:message code="aftercare.dialog.add.serviceEndReason" javaScriptEscape="true"/>'
	      },
	      viewAfterCare: {
   	        header: '<s:message code="aftercare.dialog.view.header" javaScriptEscape="true"/>',
   	        summary: '<s:message code="aftercare.dialog.view.summary" javaScriptEscape="true"/>',
	        	eligibility: '<s:message code="aftercare.dialog.view.eligibility" javaScriptEscape="true"/>',
	        	eligibilityDesc: {
              compulsory: '<s:message code="aftercare.dialog.view.eligibility.compulsory" javaScriptEscape="true"/>',
            	discretionary: '<s:message code="aftercare.dialog.view.eligibility.discretionary" javaScriptEscape="true"/>',
            	transitioned: '<s:message code="aftercare.dialog.view.eligibility.transitioned" javaScriptEscape="true"/>',
            	notknown: '<s:message code="aftercare.dialog.view.eligibility.notknown" javaScriptEscape="true"/>'
            },
	        	receivingAfterCare: '<s:message code="aftercare.dialog.view.receivingAfterCare" javaScriptEscape="true"/>',
				    initialContactDate: '<s:message code="aftercare.dialog.view.initialContactDate" javaScriptEscape="true"/>',
				    contacts: '<s:message code="aftercare.dialog.view.contacts" javaScriptEscape="true"/>',
				    contactDate: '<s:message code="aftercare.dialog.view.contactDate" javaScriptEscape="true"/>',
            economicActivity: '<s:message code="aftercare.dialog.view.economicActivity" javaScriptEscape="true"/>',
            accommodation: '<s:message code="aftercare.dialog.view.accommodation" javaScriptEscape="true"/>',
            serviceEndReason: '<s:message code="aftercare.dialog.view.serviceEndReason" javaScriptEscape="true"/>',
            serviceEndDate: '<s:message code="aftercare.dialog.view.serviceEndDate" javaScriptEscape="true"/>',
            accordionTitle: '<s:message code="aftercare.dialog.view.title" javaScriptEscape="true"/>',
            endRecord: '<s:message code="aftercare.dialog.view.endRecord" javaScriptEscape="true"/>',
            addContact: '<s:message code="aftercare.dialog.view.addContact" javaScriptEscape="true"/>',
            addProvision: '<s:message code="aftercare.results.addProvision" javaScriptEscape="true"/>',
            provision: {
            	header: '<s:message code="aftercare.provision.results.header" javaScriptEscape="true"/>',
            	provisionType: '<s:message code="aftercare.provision.results.provisionType" javaScriptEscape="true"/>',
		    			startDate: '<s:message code="aftercare.provision.results.startDate" javaScriptEscape="true"/>',
		    			endDate: '<s:message code="aftercare.provision.results.endDate" javaScriptEscape="true"/>',
		    			carer: '<s:message code="aftercare.provision.results.carer" javaScriptEscape="true"/>',
		    			actions: '<s:message code="aftercare.provision.results.actions" javaScriptEscape="true"/>',
		    			view: '<s:message code="aftercare.provision.results.view" javaScriptEscape="true"/>',
		    			edit: '<s:message code="aftercare.provision.results.edit" javaScriptEscape="true"/>'
            }
		    },
		    endAfterCare: {
		       endReason: '<s:message code="aftercare.dialog.end.serviceEndReason" javaScriptEscape="true"/>',
           endDate: '<s:message code="aftercare.dialog.end.serviceEndDate" javaScriptEscape="true"/>',
           endAftercareWarning: '<s:message code="aftercare.dialog.end.endAftercareWarning" javaScriptEscape="true"/>'
		    },
		    addAfterCareContact: {
		    	 contactDate: '<s:message code="aftercare.dialog.addcontact.contactDate" javaScriptEscape="true"/>',
        	 economicActivity: '<s:message code="aftercare.dialog.addcontact.economicActivity" javaScriptEscape="true"/>',
        	 accommodation: '<s:message code="aftercare.dialog.addcontact.accommodation" javaScriptEscape="true"/>',
		    },
		    addAfterCareProvision: {
		    	header: '<s:message code="aftercare.dialog.addProvision.header" javaScriptEscape="true"/>',
		    	summary: '<s:message code="aftercare.dialog.addProvision.summary" javaScriptEscape="true"/>',
		    	successMessage: '<s:message code="aftercare.dialog.addProvision.successMessage" javaScriptEscape="true"/>',
		    	provisionType: '<s:message code="aftercare.dialog.addProvision.provisionType" javaScriptEscape="true"/>',
		    	startDate: '<s:message code="aftercare.dialog.addProvision.startDate" javaScriptEscape="true"/>',
		    	endDate: '<s:message code="aftercare.dialog.addProvision.endDate" javaScriptEscape="true"/>',
		    	carer: '<s:message code="aftercare.dialog.addProvision.carer" javaScriptEscape="true"/>'
		    },
		    viewAfterCareProvision: {
		    	header: '<s:message code="aftercare.dialog.viewProvision.header" javaScriptEscape="true"/>',
		    	summary: '<s:message code="aftercare.dialog.viewProvision.summary" javaScriptEscape="true"/>',
		    	provisionType: '<s:message code="aftercare.dialog.viewProvision.provisionType" javaScriptEscape="true"/>',
		    	startDate: '<s:message code="aftercare.dialog.viewProvision.startDate" javaScriptEscape="true"/>',
		    	endDate: '<s:message code="aftercare.dialog.viewProvision.endDate" javaScriptEscape="true"/>',
		    	carer: '<s:message code="aftercare.dialog.viewProvision.carer" javaScriptEscape="true"/>'
		    },
		    editAfterCareProvision: {
		    	header: '<s:message code="aftercare.dialog.editProvision.header" javaScriptEscape="true"/>',
		    	summary: '<s:message code="aftercare.dialog.editProvision.summary" javaScriptEscape="true"/>',
		    	successMessage: '<s:message code="aftercare.dialog.editProvision.successMessage" javaScriptEscape="true"/>',
		    	provisionType: '<s:message code="aftercare.dialog.editProvision.provisionType" javaScriptEscape="true"/>',
		    	startDate: '<s:message code="aftercare.dialog.editProvision.startDate" javaScriptEscape="true"/>',
		    	endDate: '<s:message code="aftercare.dialog.editProvision.endDate" javaScriptEscape="true"/>',
		    	carer: '<s:message code="aftercare.dialog.editProvision.carer" javaScriptEscape="true"/>'
		    }
	    };
    
    new Y.app.aftercare.AfterCareControllerView({
        container: '#afterCareResultsContainer',
        labels: labels,
        subject: subject,
        permissions: permissions,
        dialogConfig: {
          views:{
		        add: {
		          header: {
		            text: '<s:message code="aftercare.dialog.add.header" javaScriptEscape="true"/>',
		            icon: 'fa eclipse-childlookedafter'
		          },
		          narrative: {
		            summary: '<s:message code="aftercare.dialog.add.summary" javaScriptEscape="true" />',
		            description: subjectNarrativeDescription
		          },
		          successMessage: '<s:message code="aftercare.dialog.add.successMessage" javaScriptEscape="true"/>',
		          labels: labels.addAfterCare,
		          config: {
		           url: '<s:url value="/rest/afterCare"/>',
		           subject: subject
		          }
						},
		        end: {
              header: {
                text: '<s:message code="aftercare.dialog.end.header" javaScriptEscape="true"/>',
                icon: 'fa eclipse-childlookedafter'
              },
              narrative: {
                summary: '<s:message code="aftercare.dialog.end.summary" javaScriptEscape="true" />',
                description: subjectNarrativeDescription
              },
              successMessage: '<s:message code="aftercare.dialog.end.successMessage" javaScriptEscape="true"/>',
              labels: labels.endAfterCare,
              config: {
               url: '<s:url value="/rest/afterCare/{id}/status?action=close&aftercareEndDate={endDate}&aftercareEndReason={endReason}"/>',
							 subject: subject
              }
            },
            addContact: {
              header: {
                text: '<s:message code="aftercare.dialog.addcontact.header" javaScriptEscape="true"/>',
                icon: 'fa eclipse-childlookedafter'
              },
              narrative: {
                summary: '<s:message code="aftercare.dialog.addcontact.summary" javaScriptEscape="true" />',
                description: subjectNarrativeDescription
              },
              successMessage: '<s:message code="aftercare.dialog.addcontact.successMessage" javaScriptEscape="true"/>',
              labels: labels.addAfterCareContact,
              config: {
               url: '<s:url value="/rest/afterCare/{id}/contact"/>',
               subject: subject
              }
            },
           addProvision: {
		          header: {
		            text: '<s:message code="aftercare.dialog.addProvision.header" javaScriptEscape="true"/>',
		            icon: 'fa eclipse-childlookedafter'
		          },
		          narrative: {
		            summary: '<s:message code="aftercare.dialog.addProvision.summary" javaScriptEscape="true" />',
		            description: subjectNarrativeDescription
		          },
		          successMessage: '<s:message code="aftercare.dialog.addProvision.successMessage" javaScriptEscape="true"/>',
		          labels: labels.addAfterCareProvision,
		          config: {
		           url: '<s:url value="/rest/afterCareProvision"/>',
		           personCarerAutocompleteURL: '<s:url value="/rest/person?escapeSpecialCharacters=true&appendWildcard=true&nameOrIdentifier={query}&escapeSpecialCharacters=true&appendWildcard=true&personType=FOSTER_CARER&pageSize={maxResults}" />',
               organisationCarerAutocompleteURL: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
		           subject: subject
		          }
		        },
            viewProvision: {
		          header: {
		            text: '<s:message code="aftercare.dialog.viewProvision.header" javaScriptEscape="true"/>',
		            icon: 'fa eclipse-childlookedafter'
		          },
		          narrative: {
		            summary: '<s:message code="aftercare.dialog.viewProvision.summary" javaScriptEscape="true" />',
		            description: subjectNarrativeDescription
		          },
		          labels: labels.viewAfterCareProvision,
		          config: {
		           url: '<s:url value="/rest/afterCareProvision/{id}"/>',
		           subject: subject
		          },
		          permissions: permissions
		        },
		        editProvision: {
			          header: {
			            text: '<s:message code="aftercare.dialog.editProvision.header" javaScriptEscape="true"/>',
			            icon: 'fa eclipse-childlookedafter'
			          },
			          narrative: {
			            summary: '<s:message code="aftercare.dialog.editProvision.summary" javaScriptEscape="true" />',
			            description: subjectNarrativeDescription
			          },
			          successMessage: '<s:message code="aftercare.dialog.editProvision.successMessage" javaScriptEscape="true"/>',
			          labels: labels.editAfterCareProvision,
			          config: {
			          url: '<s:url value="/rest/afterCareProvision/{id}"/>',
			        	submitUrl: '<s:url value="/rest/afterCareProvision/{id}/status?action=close"/>',
			           subject: subject
			          }
			        }
          }
       },
       resultsConfig: {
    	   labels: labels,
           subject: subject,
           permissions: permissions,
           url: '<s:url value="/rest/person/{subjectId}/afterCare?s={sortBy}&pageNumber={page}&pageSize={pageSize}"><s:param name="sortBy" value="[{\"contactDate\":\"desc\"}]" /></s:url>'
       }
     }).render();



});
</script>