<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.View')" var="canView"/>
<sec:authorize access="hasPermission(null, 'CaseNoteEntryAttachment','CaseNoteEntryAttachment.GET')" var="canViewAttachment" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.Add')" var="canAdd"/>
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.Update')" var="canUpdate"/>
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.Remove')" var="canRemove"/>
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.COMPLETE')" var="canComplete"/>
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.UNCOMPLETE')" var="canUncomplete" />
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.Delete')" var="canDelete" />
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.UPDATE')" var="canReorder"/>
	<sec:authorize access="hasPermission(null, 'CaseNoteEntry','CaseNoteEntry.SEND_NOTIFICATION')" var="canSendNotification"/>
	<sec:authorize access="hasPermission(null, 'CaseNoteEntryAttachment','CaseNoteEntryAttachment.ADD')" var="canUpload" />
	<sec:authorize access="hasPermission(null, 'CaseNoteEntryAttachment','CaseNoteEntryAttachment.DELETE')" var="canDeleteAttachment" />
	<sec:authorize access="hasPermission(null, 'CaseNoteEntryAttachment','CaseNoteEntryAttachment.AddCompleted')" var="canUploadAttachmentOnCompleteInstance" /> 
</c:if>

<s:eval expression="@appSettings.isCaseNoteImpactEnabled()" var="impactEnabled" scope="page" />
<s:eval expression="@appSettings.isNotificationEventHandlerEnabled()" var="notificationEventHandlerEnabled" scope="page" />
<s:eval expression="@appSettings.isCaseloadSubscriptionMatchersEnabled()" var="caseloadSubscriptionMatchersEnabled" scope="page" />

<div id="casenoteResults"></div>

<script>
Y.use('casenote-controller-view','yui-base', 'json-parse', function(Y) {
<c:choose>
  <c:when test="${not empty groupList}">
    //The list of valid groups that are used in the filter
    var groupList =  Y.Array(Y.JSON.parse('${esc:escapeJavaScript(groupList)}'));
  </c:when>
  <c:otherwise>
    var groupList=[];
  </c:otherwise>    
</c:choose>
  
  var impactEnabled='${impactEnabled}'==='true';
  var notificationEnabled='${notificationEventHandlerEnabled}'==='true' && '${caseloadSubscriptionMatchersEnabled}'==='true';

  var practitioner = {
    personName: '${esc:escapeJavaScript(loginPersonName)}',
    personId: '${esc:escapeJavaScript(loginPersonId)}',
    organisationName: '${esc:escapeJavaScript(loginPersonOrganisationName)}'
  };

<c:choose>
  <c:when test="${not empty group}">
  var subject = {
    subjectType: 'group',
    subjectName: '${esc:escapeJavaScript(group.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(group.groupIdentifier)}',
    subjectId: '<c:out value="${group.id}"/>'
  };
  </c:when>
  <c:when test="${not empty organisation}">
  var subject = {
    subjectType: 'organisation',
    subjectName: '${esc:escapeJavaScript(organisation.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(organisation.organisationIdentifier)}',
    subjectId: '<c:out value="${organisation.id}"/>'
  };
  </c:when>
  <c:otherwise>
  var subject = {
    subjectType: 'person',
    subjectName: '${esc:escapeJavaScript(person.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
    subjectId: '<c:out value="${person.id}"/>',
  };
  </c:otherwise>
</c:choose>

  var permissions = {
    canView: '${canView}' === 'true',
    canPrint: '${canView}' === 'true',
    canOutput: '${canView}' === 'true',
    canUpdate: '${canUpdate}' === 'true',
    canAdd: '${canAdd}' === 'true',
    canRemove: '${canRemove}' === 'true',
    canDelete: '${canDelete}' === 'true',
    canComplete: '${canComplete}' === 'true',
    canUncomplete: '${canUncomplete}' === 'true',
    canViewAttachment: '${canViewAttachment}' === 'true',
    canUploadAttachment: '${canUpload}' === 'true',
    canUpload: '${canUpload}' === 'true',
    canReorder: '${canReorder}' === 'true',
    canReadingView: '${canView}' === 'true',
    canSendNotification: '${canSendNotification}' === 'true',
    canDeleteAttachment: '${canDeleteAttachment}' === 'true',
    canUploadAttachmentOnCompleteInstance: '${canUploadAttachmentOnCompleteInstance}' === 'true'
  };

  var commonLabels = {
    event: '<s:message code="caseNoteEntry.event" javaScriptEscape="true"/>',
    eventDate: '<s:message code="caseNoteEntry.eventDate" javaScriptEscape="true"/>',
    entryType: '<s:message code="caseNoteEntry.entryType" javaScriptEscape="true"/>',
    entrySubType: '<s:message code="caseNoteEntry.entrySubType" javaScriptEscape="true"/>',
    impact: '<s:message code="caseNoteEntry.impact" javaScriptEscape="true"/>',
    impactPositive: '<s:message code="caseNoteEntry.impact.positive" javaScriptEscape="true"/>',
    impactNegative: '<s:message code="caseNoteEntry.impact.negative" javaScriptEscape="true"/>',
    impactNeutral: '<s:message code="caseNoteEntry.impact.neutral" javaScriptEscape="true"/>',
    impactUnknown: '<s:message code="caseNoteEntry.impact.unknown" javaScriptEscape="true"/>',
    source: '<s:message code="common.source" javaScriptEscape="true"/>',
    otherSource: '<s:message code="common.source.other" javaScriptEscape="true"/>',
    sourceOrganisation: '<s:message code="common.sourceOrganisation" javaScriptEscape="true"/>',
    otherSourceOrganisation: '<s:message code="common.sourceOrganisation.other" javaScriptEscape="true"/>',
    sourceMustBeOtherOrNull: '<s:message code="common.sourceMustBeOtherOrNull" javaScriptEscape="true"/>',
    practitioner: '<s:message code="caseNoteEntry.practitioner" javaScriptEscape="true"/>',
    practitionerOrganisation: '<s:message code="caseNoteEntry.practitionerOrganisation" javaScriptEscape="true"/>',
    recordedDate: '<s:message code="caseNoteEntry.recordedDate" javaScriptEscape="true"/>',
    editedDate: '<s:message code="caseNoteEntry.editedDate" javaScriptEscape="true"/>',
    casenoteTabName: '<s:message code="attachment.casenote.tabview.casenoteTab.label"/>',
    attachmentTabName: '<s:message code="attachment.casenote.tabview.attachmentTab.label"/>',
    notificationsTabName: '<s:message code="caseNoteEntry.tabview.notificationTab.label"/>',
    completeConfirm: '<s:message code="caseNoteEntry.completeConfirm" javaScriptEscape="true"/>',
    groupEntryAddForm: '<s:message code="caseNoteEntry.groupEntryAddForm" javaScriptEscape="true"/>',
    groupEntryMembers: '<s:message code="caseNoteEntry.groupEntryMembers" javaScriptEscape="true"/>',
    viewEntryVisibleMembersTabLabel: '<s:message code="caseNoteEntry.viewEntryVisibleMembersTabLabel" javaScriptEscape="true"/>',
    viewPeopleSeenTabLabel: '<s:message code="caseNoteEntry.viewPeopleSeenTabLabel" javaScriptEscape="true"/>',
    visibleToAll: '<s:message code="caseNoteEntry.visibleToAll" javaScriptEscape="true"/>',
    selectAllMembers: '<s:message code="caseNoteEntry.selectAllMembers" javaScriptEscape="true"/>',
    visibleToAllCheckboxLabelPopupMessageTitle: '<s:message code="caseNoteEntry.visibleToAllCheckboxLabelPopupMessageTitle" javaScriptEscape="true"/>',
    visibleToAllCheckboxLabelPopupMessageContent: '<s:message code="caseNoteEntry.visibleToAllCheckboxLabelPopupMessageContent" javaScriptEscape="true"/>'
  };

  var fuzzyDateLabels = {
    date: '<s:message code="caseNoteEntry.eventDate" javaScriptEscape="true"/>',
    year: '<s:message code="fuzzyDate.year" javaScriptEscape="true"/>',
    month: '<s:message code="fuzzyDate.month" javaScriptEscape="true"/>',
    day: '<s:message code="fuzzyDate.day" javaScriptEscape="true"/>',
    hour: '<s:message code="fuzzyDate.hour" javaScriptEscape="true"/>',
    minute: '<s:message code="fuzzyDate.minute" javaScriptEscape="true"/>',
    second: '<s:message code="fuzzyDate.second" javaScriptEscape="true"/>'
  };

  var searchConfig = {
    sortBy: [{
      'eventDate!calculatedDate': 'desc'
    }],
    labels: {
      recordedDate: '<s:message code="casenote.results.recordedDate" javaScriptEscape="true"/>',
      editedDate: '<s:message code="casenote.results.editedDate" javaScriptEscape="true"/>',
      calculatedDate: '<s:message code="casenote.results.eventDate" javaScriptEscape="true"/>',
      event: '<s:message code="casenote.results.event" javaScriptEscape="true"/>',
      entryType: '<s:message code="casenote.results.entryType" javaScriptEscape="true"/>',
      entrySubType: '<s:message code="casenote.results.entrySubType" javaScriptEscape="true"/>',
      impact: '<s:message code="casenote.results.impact" javaScriptEscape="true"/>',
      source: '<s:message code="casenote.results.source" javaScriptEscape="true"/>',
      practitioner: '<s:message code="casenote.results.practitioner" javaScriptEscape="true"/>',
      status: '<s:message code="casenote.results.status" javaScriptEscape="true"/>',
      attachmentCount: '<s:message code="casenote.results.files" javaScriptEscape="true"/>',
      peopleSeen: '<s:message code="casenote.results.peopleSeen" javaScriptEscape="true"/>',
      actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
      view: '<s:message code="casenote.results.view" javaScriptEscape="true"/>',
      viewTitle: '<s:message code="casenote.results.viewTitle" javaScriptEscape="true"/>',
      complete: '<s:message code="casenote.results.complete" javaScriptEscape="true"/>',
      completeTitle: '<s:message code="casenote.results.completeTitle" javaScriptEscape="true"/>',
      uncomplete: '<s:message code="casenote.results.uncomplete" javaScriptEscape="true"/>',
      uncompleteTitle: '<s:message code="casenote.results.uncompleteTitle" javaScriptEscape="true"/>',
      deleteEntry: '<s:message code="casenote.results.delete" javaScriptEscape="true"/>',
      deleteEntryTitle: '<s:message code="casenote.results.deleteTitle" javaScriptEscape="true"/>',
      remove: '<s:message code="casenote.results.remove" javaScriptEscape="true"/>',
      removeTitle: '<s:message code="casenote.results.removeTitle" javaScriptEscape="true"/>',
      uploadFile: '<s:message code="casenote.results.uploadFile" javaScriptEscape="true"/>',
      uploadFileTitle: '<s:message code="casenote.results.uploadFileTitle" javaScriptEscape="true"/>',
      sendNotification:'<s:message code="casenote.results.sendNotification" javaScriptEscape="true"/>',
      sendNotificationTitle:'<s:message code="casenote.results.sendNotificationTitle" javaScriptEscape="true"/>',
      subjectSeen: '<s:message code="casenote.results.peopleSeen" javaScriptEscape="true"/>',
    },
    initialState: 'initial',
    notificationEnabled:notificationEnabled,
    resultsCountNode: Y.one('#casenoteResultsCount'),
    noDataMessage: '<s:message code="casenote.find.no.results" javaScriptEscape="true"/>',
    permissions: permissions,
    url: '<s:url value="/rest/{subjectType}/{id}/caseNoteEntry?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
    urlForView: '<s:url value="/rest/caseNoteEntry/{id}"/>'
  };

  var filterContextConfig = {
    labels: {
      filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
      date: '<s:message code="casenote.find.filter.date.active" javaScriptEscape="true"/>',
      recordedDate: '<s:message code="casenote.find.filter.recordedDate.active" javaScriptEscape="true"/>',
      editedDate: '<s:message code="casenote.find.filter.editedDate.active" javaScriptEscape="true"/>',
      practitioner: '<s:message code="casenote.find.filter.practitioner.active" javaScriptEscape="true"/>',
      source: '<s:message code="casenote.find.filter.source.active" javaScriptEscape="true"/>',
      impact: '<s:message code="casenote.find.filter.impact.active" javaScriptEscape="true"/>',
      status: '<s:message code="casenote.find.filter.status.active" javaScriptEscape="true"/>',
      practitionerOrganisation: '<s:message code="casenote.find.filter.practitionerOrganisation.active" javaScriptEscape="true"/>',
      entryType: '<s:message code="casenote.find.filter.entryType.active" javaScriptEscape="true"/>',
      sourceOrganisation: '<s:message code="casenote.find.filter.sourceOrganisation.active" javaScriptEscape="true"/>',
      group: '<s:message code="casenote.find.filter.group.active" javaScriptEscape="true"/>',
      attachmentSource:'<s:message code="attachment.filter.source.active" javaScriptEscape="true"/>',
      attachmentSourceOrganisation:'<s:message code="attachment.filter.sourceOrganisation.active" javaScriptEscape="true"/>',
      attachmentTitle: '<s:message code="attachment.filter.title.active" javaScriptEscape="true"/>',
      hasAttachments: '<s:message code="attachment.filter.hasAttachments.active" javaScriptEscape="true"/>',
      filterDisabled: '<s:message code="casenote.find.filter.disabled" javaScriptEscape="true"/>',
      resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
      resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>',
      subjectSeen: '<s:message code="casenote.results.peopleSeen" javaScriptEscape="true"/>',
    }
  };

  var filterConfig = {
    labels: {
      filterBy: '<s:message code="filter.by" javaScriptEscape="true"/>',
      dateFilter: '<s:message code="casenote.find.filter.dateFilter" javaScriptEscape="true"/>',
      entryStartDate: '<s:message code="casenote.find.filter.entryDateRangeStart" javaScriptEscape="true"/>',
      entryEndDate: '<s:message code="casenote.find.filter.entryDateRangeEnd" javaScriptEscape="true"/>',
      recordedStartDate: '<s:message code="casenote.find.filter.recordedDateRangeStart" javaScriptEscape="true"/>',
      recordedEndDate: '<s:message code="casenote.find.filter.recordedDateRangeEnd" javaScriptEscape="true"/>',
      editedStartDate: '<s:message code="casenote.find.filter.editedDateRangeStart" javaScriptEscape="true"/>',
      editedEndDate: '<s:message code="casenote.find.filter.editedDateRangeEnd" javaScriptEscape="true"/>',
      dateRangeLinks: '<s:message code="casenote.find.filter.dateRangeLinks" javaScriptEscape="true"/>',
      dateRangeLastWeek: '<s:message code="casenote.find.filter.entryDateRangeLastWeek" javaScriptEscape="true"/>',
      dateRangeLastMonth: '<s:message code="casenote.find.filter.entryDateRangeLastMonth" javaScriptEscape="true"/>',
      dateRangeLastYear: '<s:message code="casenote.find.filter.entryDateRangeLastYear" javaScriptEscape="true"/>',
      peopleFilter: '<s:message code="casenote.find.filter.peopleFilter" javaScriptEscape="true"/>',
      practitioner: '<s:message code="casenote.find.filter.practitioner" javaScriptEscape="true"/>',
      practitionerOrganisation: '<s:message code="casenote.find.filter.practitionerOrganisation" javaScriptEscape="true"/>',
      source: '<s:message code="casenote.find.filter.source" javaScriptEscape="true"/>',
      otherSource:'<s:message code = "casenote.find.filter.otherSource" javaScriptEscape="true"/>',
      sourceOrganisation: '<s:message code="casenote.find.filter.sourceOrganisation" javaScriptEscape="true"/>',
      otherSourceOrganisation:'<s:message code = "casenote.find.filter.otherSourceOrganisation" javaScriptEscape="true"/>',
      typeFilter: '<s:message code="casenote.find.filter.typeFilter" javaScriptEscape="true"/>',
      entryType: '<s:message code="casenote.find.filter.entryType" javaScriptEscape="true"/>',
      impact: '<s:message code="casenote.find.filter.impact" javaScriptEscape="true"/>',
      impactPositive: '<s:message code="casenote.find.filter.impact.positive" javaScriptEscape="true"/>',
      impactNegative: '<s:message code="casenote.find.filter.impact.negative" javaScriptEscape="true"/>',
      impactNeutral: '<s:message code="casenote.find.filter.impact.neutral" javaScriptEscape="true"/>',
      impactUnknown: '<s:message code="casenote.find.filter.impact.unknown" javaScriptEscape="true"/>',
      status:'<s:message code="casenote.find.filter.status" javaScriptEscape="true"/>',
      includeDraft:'<s:message code="casenote.find.filter.status.draft" javaScriptEscape="true"/>',
      includeComplete:'<s:message code="casenote.find.filter.status.complete" javaScriptEscape="true"/>',
      includeDeleted:'<s:message code="casenote.find.filter.status.deleted" javaScriptEscape="true"/>',
      groupFilter: '<s:message code="casenote.find.filter.groupFilter" javaScriptEscape="true"/>',
      excludeAllGroups: '<s:message code="casenote.find.filter.group.excludeAllGroups" javaScriptEscape="true"/>',
      attachmentFilter:'<s:message code="casenote.find.filter.attachmentFilter" javaScriptEscape="true"/>',
      subjectSeen: '<s:message code="casenote.results.peopleSeen" javaScriptEscape="true"/>',
      subjectSeenOption: '<s:message code="casenote.find.filter.subjectSeen.Seen" javaScriptEscape="true"/>',
      subjectNotSeenOption: '<s:message code="casenote.find.filter.subjectSeen.NotSeen" javaScriptEscape="true"/>',
      subjectSeenAloneOption: '<s:message code="casenote.find.filter.subjectSeen.Alone" javaScriptEscape="true"/>',
      validation:{
        startDateBeforeEndDate : '<s:message code = "casenote.find.filter.endBeforeStartDate" javaScriptEscape="true"/>',
        startDateBeforeEndRecordedDate : '<s:message code = "casenote.find.filter.endBeforeStartRecordedDate" javaScriptEscape="true"/>',
        startDateBeforeEndEditedDate : '<s:message code = "casenote.find.filter.endBeforeStartEditedDate" javaScriptEscape="true"/>'
      },
      hasAttachments:{
        title:'<s:message code = "attachment.filter.hasAttachments.title" javaScriptEscape="true"/>',
        attachmentTitle:'<s:message code = "attachment.filter.title" javaScriptEscape="true"/>',
        withAttachments:'<s:message code = "attachment.filter.hasAttachments.withAttachments" javaScriptEscape="true"/>',
        withoutAttachment:'<s:message code = "attachment.filter.hasAttachments.withoutAttachments" javaScriptEscape="true"/>',
        attachmentSource:'<s:message code = "attachment.filter.source" javaScriptEscape="true"/>',
        attachmentOtherSource:'<s:message code = "attachment.filter.otherSource" javaScriptEscape="true"/>',
        attachmentSourceOrganisation:'<s:message code = "attachment.filter.sourceOrganisation" javaScriptEscape="true"/>',
        attachmentOtherSourceOrganisation:'<s:message code = "attachment.filter.otherSourceOrganisation" javaScriptEscape="true"/>'
      }
    },
    permissions:{
      canDelete:permissions.canDelete
    },
    subject:subject,
    groupList:groupList,    
    filterItemsUrl:'<s:url value="/rest/casenote/{id}/filterItems"/>',
    recordId:'${casenote.id}',
    caseNoteRecordUrl:'<s:url value="/rest/{subjectType}/{id}/caseNoteRecord"/>'
  };

  var attachmentsConfig = {
    labels: {
      title: '<s:message code="attachment.casenote.table.title" javaScriptEscape="true"/>',
      uploadedBy: '<s:message code="attachment.casenote.table.uploadedBy" javaScriptEscape="true"/>',
      uploadedOn: '<s:message code="attachment.casenote.table.uploadedOn" javaScriptEscape="true"/>',
      description: '<s:message code="attachment.casenote.table.description" javaScriptEscape="true"/>',
      actions: '<s:message code="attachment.casenote.table.tools" javaScriptEscape="true"/>',
      noResults: '<s:message code="attachment.casenote.table.noResults" javaScriptEscape="true"/>',
      fileDownload: '<s:message code="attachment.casenote.download" javaScriptEscape="true"/>',
      fileRemove: '<s:message code="attachment.casenote.delete" javaScriptEscape="true"/>',
      viewFile: '<s:message code="attachment.casenote.view" javaScriptEscape="true"/>'
    },
    permissions: permissions
  };
  
  var notificationLabels={
      enableNotification:'<s:message code="caseNoteEntry.sendNotification" javaScriptEscape="true"/>',
      add:'<s:message code="notifications.recipient.add" javaScriptEscape="true"/>',
      includeDirectTargets:'<s:message code="notifications.includeDirectTargets" javaScriptEscape="true"/>',
      recipients:'<s:message code="notifications.recipients" javaScriptEscape="true"/>',
      personAutocomplete:{
        placeholder: '<s:message code="notifications.recipients.user.autocomplete.placeholder" javaScriptEscape="true"/>',
      },
      organisationAutocomplete:{
        placeholder: '<s:message code="notifications.recipients.team.autocomplete.placeholder" javaScriptEscape="true"/>'
      }
  };
  
  var sendNotificationLabels={
	      enableNotification:'<s:message code="caseNoteEntry.notify.sendNotification" javaScriptEscape="true"/>',
	      add:'<s:message code="notifications.recipient.add" javaScriptEscape="true"/>',
	      includeDirectTargets:'<s:message code="notifications.includeDirectTargets" javaScriptEscape="true"/>',
	      recipients:'<s:message code="notifications.recipients" javaScriptEscape="true"/>',
	      personAutocomplete:{
	        placeholder: '<s:message code="notifications.recipients.user.autocomplete.placeholder" javaScriptEscape="true"/>',
	      },
	      organisationAutocomplete:{
	        placeholder: '<s:message code="notifications.recipients.team.autocomplete.placeholder" javaScriptEscape="true"/>'
	      }
	  };

  var headerIcon = 'eclipse-caseNotes',
    narrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

  var dialogConfig = {
    views: {
      add: {
        header: {
          text: '<s:message code="casenote.dialog.add.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.addCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="caseNoteEntry.add.success.message" javaScriptEscape="true"/>',
        labels: commonLabels,
        validationLabels: {
          eventDateValidationErrorMessage: '<s:message code="caseNoteEntry.invalidEventDate.message" javaScriptEscape="true"/>',
          entryValidationErrorMessage: '<s:message code="caseNoteEntry.mandatoryEntry.message" javaScriptEscape="true"/>',
          entryTypeValidationErrorMessage: '<s:message code="caseNoteEntry.mandatoryEntryType.message" javaScriptEscape="true"/>',
          otherSource:'<s:message code="NotBlank.source.other" javaScriptEscape="true" />',
          otherSourceOrganisation:'<s:message code="NotBlank.sourceOrganisation.other" javaScriptEscape="true" />'
        },
        config: {
          url: '<s:url value="/rest/{subjectType}/{subjectId}/caseNoteEntry"/>',
          imageUploadURL: Y.Lang.sub('<s:url value="/rest/subject/{subjectId}/tempAttachment?_iFrameUpload=true" />',subject),
          fuzzyDateLabels: fuzzyDateLabels,
          notificationsConfig:{
            personAutocompleteURL: '<s:url value="/rest/person?nameOrUsername={query}&personType=professional&s={sortBy}&pageSize={maxResults}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
            organisationAutocompleteURL:'<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&byOrganisationType=true&organisationTypes=TEAM&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}" />',
                labels:notificationLabels
          },         
          attachmentsConfig: Y.merge(attachmentsConfig, {
            isTemporaryAttachments: true,
            downloadUrl: '<s:url value="/rest/temporaryAttachment/{id}/content"/>',
            attachmentListUrl: '<s:url value="/rest/temporaryAttachment?temporaryAttachmentIds={temporaryAttachmentIds}&s={sortBy}&pageNumber={page}&pageSize={pageSize}" />'
          })
        }
      },
      view: {
        header: {
          text: '<s:message code="casenote.dialog.view.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.viewCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        config: {
          url: '<s:url value="/rest/caseNoteEntry/{id}"/>',
          attachmentsConfig: Y.merge(attachmentsConfig, {
            isTemporaryAttachments: false,
            downloadUrl: '<s:url value="/rest/caseNoteEntryAttachment/{id}/content"/>',
            attachmentListUrl: '<s:url value="/rest/caseNoteEntry/{id}/attachment?isInline=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}" />'
          })
        },
        labels: commonLabels
      },
      edit: {
        header: {
          text: '<s:message code="casenote.dialog.edit.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.viewCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="caseNoteEntry.edit.success.message" javaScriptEscape="true"/>',
        labels: commonLabels,
        validationLabels: {
          otherSource:'<s:message code="NotBlank.source.other" javaScriptEscape="true" />',
          otherSourceOrganisation:'<s:message code="NotBlank.sourceOrganisation.other" javaScriptEscape="true" />'
        },
        config: {
          imageUploadURL: '<s:url value="/rest/caseNoteEntry/{id}/caseNoteEntryAttachment?_iFrameUpload=true" />',
          completeURL: '<s:url value="/rest/caseNoteEntry/{id}/status?action=complete"/>',
          url: '<s:url value="/rest/caseNoteEntry/{id}"/>',
          fuzzyDateLabels: fuzzyDateLabels,
          attachmentsConfig: Y.merge(attachmentsConfig, {
            isTemporaryAttachments: false,
            downloadUrl: '<s:url value="/rest/caseNoteEntryAttachment/{id}/content"/>',
            attachmentListUrl: '<s:url value="/rest/caseNoteEntry/{id}/attachment?isInline=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}" />'
          })
        }
      },
      editComplete: {
        header: {
          text: '<s:message code="casenote.dialog.edit.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.viewCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="caseNoteEntry.editComplete.success.message" javaScriptEscape="true"/>',
        config: {
          url: '<s:url value="/rest/caseNoteEntry/{id}"/>',
          fuzzyDateLabels: fuzzyDateLabels,
          attachmentsConfig: Y.merge(attachmentsConfig, {
            isTemporaryAttachments: false,
            downloadUrl: '<s:url value="/rest/caseNoteEntryAttachment/{id}/content"/>',
            attachmentListUrl: '<s:url value="/rest/caseNoteEntry/{id}/attachment?isInline=false&s={sortBy}&pageNumber={page}&pageSize={pageSize}" />'
          })
        },
        labels: commonLabels
      },
      hardDelete: {
        header: {
          text: '<s:message code="casenote.dialog.remove.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.removeCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="caseNoteEntry.remove.success.message" javaScriptEscape="true"/>',
        config: {
          url: '<s:url value="/rest/caseNoteEntry/{id}"/>'
        },
        labels: Y.merge(commonLabels, {
          personRemoveConfirm: '<s:message code="caseNoteEntry.removeConfirm" javaScriptEscape="true"/>',
          groupRemoveConfirm: '<s:message code="group.caseNoteEntry.removeConfirm" javaScriptEscape="true"/>',
        })
      },
      softDelete: {
        header: {
          text: '<s:message code="casenote.dialog.delete.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.deleteCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="caseNoteEntry.delete.success.message" javaScriptEscape="true"/>',
        config: {
          url: '<s:url value="/rest/caseNoteEntry/{id}/status?action=deleted"/>'
        },
        labels: Y.merge(commonLabels, {
          personDeleteConfirm: '<s:message code="caseNoteEntry.deleteConfirm" javaScriptEscape="true"/>',
          groupDeleteConfirm: '<s:message code="group.caseNoteEntry.deleteConfirm" javaScriptEscape="true"/>',
        })
      },
      complete: {
        header: {
          text: '<s:message code="casenote.dialog.complete.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.completeCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        labels: Y.merge(commonLabels, {
          completeConfirm: '<s:message code="caseNoteEntry.completeConfirm" javaScriptEscape="true"/>'
        }),
        config: {
          completeURL: '<s:url value="/rest/caseNoteEntry/{id}/status?action=complete"/>',
          url: '<s:url value="/rest/caseNoteEntry/{id}"/>'
        },
        successMessage: '<s:message code="caseNoteEntry.complete.success.message" javaScriptEscape="true"/>'
      },
      uncomplete: {
        header: {
          text: '<s:message code="casenote.dialog.uncomplete.header" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.uncompleteCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        labels: Y.merge(commonLabels, {
          uncompleteConfirm: '<s:message code="caseNoteEntry.uncompleteConfirm" javaScriptEscape="true"/>'
        }),
        config: {
          uncompleteURL: '<s:url value="/rest/caseNoteEntry/{id}/status?action=draft" />',
          url: '<s:url value="/rest/caseNoteEntry/{id}"/>'
        },
        successMessage: '<s:message code="caseNoteEntry.uncomplete.success.message" javaScriptEscape="true"/>'
      },
      sendNotification: {
    	  header: {
              text: '<s:message code="casenote.dialog.notification.header" javaScriptEscape="true"/>',
              icon: headerIcon
    	  },
    	  narrative: {
              summary: '<s:message code="caseNoteEntry.notifyCaseNoteEntrySummary" javaScriptEscape="true"/>',
              description: narrativeDescription
    	  },
          successMessage: '<s:message code="casenote.notification.success.message" javaScriptEscape="true"/>',
    	  config:{
              url: '<s:url value="/rest/caseNoteEntry/{id}/status?action=notify"/>',
              notificationsConfig:{
                  personAutocompleteURL: '<s:url value="/rest/person?nameOrUsername={query}&personType=professional&s={sortBy}&pageSize={maxResults}"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
                  organisationAutocompleteURL:'<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&byOrganisationType=true&organisationTypes=TEAM&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}" />',
                  labels:sendNotificationLabels
                }
    	  }, 
      }
    },
    //config for nested confirm complete dialog view
    confirmComplete:{
      header: {
        text: '<s:message code="casenote.dialog.complete.header" javaScriptEscape="true"/>',
        icon: headerIcon
      },
    },    
    activeGroupMembersURL: '<s:url value="/rest/group/{id}/personMembership?temporalStatusFilter=ACTIVE&pageNumber=-1&pageSize=-1"/>'
  };

  var attachmentLabels = {
    chooseFile: '<s:message code="file.upload.chooseFile" javaScriptEscape="true"/>',
    docTitle: '<s:message code="file.upload.docTitle" javaScriptEscape="true"/>',
    removeFile: '<s:message code="file.upload.removeFile" javaScriptEscape="true"/>',
    docDescription: '<s:message code="file.upload.docDescription" javaScriptEscape="true"/>',
    source: '<s:message code="common.source" javaScriptEscape="true"/>',
    otherSource: '<s:message code="common.source.other" javaScriptEscape="true"/>',
    sourceOrganisation: '<s:message code="common.sourceOrganisation" javaScriptEscape="true"/>',
    otherSourceOrganisation: '<s:message code="common.sourceOrganisation.other" javaScriptEscape="true"/>',
    sourceMustBeOtherOrNull: '<s:message code="common.sourceMustBeOtherOrNull" javaScriptEscape="true"/>'
  };
  
  var imageDialogConfig={
	 views: {
		 uploadFile: {
			 header: {
		          text: '<s:message code="image.upload.title" javaScriptEscape="true"/>',
		          icon: headerIcon
		        },
		        successMessage: '<s:message code="file.upload.success" javaScriptEscape="true"/>',
		        labels:{
		        	image:'<s:message code="image.upload.file" javaScriptEscape="true"/>'
		        },
		        config:{
					 url: '<s:url value="/rest/subject/{subjectId}/tempAttachment" />',
					 imageUrl: '<s:url value="/rest/temporaryAttachment/{id}/content" />'
		        }
		 }	
	 }
  };
  
  var attachmentDialogConfig = {
    views: {
      uploadFile: {
        header: {
          text: '<s:message code="file.upload.uploadFile" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        config: {
          url: '<s:url value="/rest/caseNoteEntry/{id}/attachment"/>',
          temporarySubjectAttachmentUrl: '<s:url value="/rest/subject/{id}/temporaryAttachment"/>',
          validationLabels:{
            otherSource:'<s:message code="NotBlank.source.other" javaScriptEscape="true" />',
            otherSourceOrganisation:'<s:message code="NotBlank.sourceOrganisation.other" javaScriptEscape="true"/>'
          }
        },
        narrative: {
          summary: '<s:message code="caseNoteEntry.upload.uploadAttachmentCaseNoteEntrySummary" javaScriptEscape="true"/>',
          description: narrativeDescription
        },
        successMessage: '<s:message code="file.upload.success" javaScriptEscape="true"/>',
        labels: attachmentLabels
      },
      viewAttachment: {
        header: {
          text: '<s:message code="file.viewTitle" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        config: {
          url: '<s:url value="/rest/caseNoteEntryAttachment/{id}"/>',
          downloadUrl: '<s:url value="/rest/caseNoteEntryAttachment/{id}/content"/>'
        },
        labels: attachmentLabels
      },
      deleteAttachment: {
        header: {
          text: '<s:message code="file.deleteTitle" javaScriptEscape="true"/>',
          icon: headerIcon
        },
        config: {
          url: '<s:url value="/rest/caseNoteEntryAttachment/{id}"/>'
        },
        successMessage: '<s:message code="caseNoteEntry.deleteAttachment.deleteAttachmentCaseNoteEntrySuccessMessage"/>',
        labels: {
          deleteConfirm: '<s:message code="caseNoteEntry.deleteAttachment.deleteConfirm" javaScriptEscape="true"/>'
        }
      }
    }
  };
  
  var outputNarrativeDescription= '{{this.subjectName}} ({{this.subjectIdentifier}})';
  
  var outputDialogConfig;
  
  if(subject.subjectType==='person'){
    outputDialogConfig={
      views:{      
        produceDocument:{
          type:'app.CaseNoteOutputDocumentView',
          header: {
            text: '<s:message code="casenote.output.header" javaScriptEscape="true"/>',
            icon: 'fa fa-file'
          },
          narrative: {
            summary: '<s:message code="produceDocument.dialog.summary" javaScriptEscape="true" />',
            description: outputNarrativeDescription
          },
          labels:{
            outputCategory: '<s:message code="produceDocument.dialog.category" javaScriptEscape="true"/>',
            outputCategoryPrompt: '<s:message code="produceDocument.dialog.category.select.blank" javaScriptEscape="true"/>',
            availableTemplates: '<s:message code="produceDocument.dialog.availableTemplates" javaScriptEscape="true"/>',
            outputFormat: '<s:message code="produceDocument.dialog.outputFormat" javaScriptEscape="true"/>'
          },
          config:{
            templateListURL: '<s:url value="/rest/outputTemplate?status=PUBLISHED&context=CASENOTES&asList=true"/>',
            generateURL:'<s:url value="/rest/documentRequest"/>',
            documentType: 'CASENOTE',
            successMessage: '<s:message code="produceDocument.dialog.success.generateInProgress" javaScriptEscape="true" />',
            failureMessage: '<s:message code="produceDocument.dialog.failure.generateFailed" javaScriptEscape="true" />'
          }
        }
      }
    };
  }

  var config = {
    container: '#casenoteResults',
    impactEnabled: impactEnabled,
    notificationEnabled:notificationEnabled,
    searchConfig: searchConfig,
    filterConfig: filterConfig,
    filterContextConfig: filterContextConfig,
    permissions: permissions,
    subject: subject || {},
    practitioner: practitioner,
    readingCfg: {
      triggerNode: '#readingButton',
      active: {
        className: 'pure-button-active',
        title: '<s:message code="button.view.active" />'
      },
      inactive: {
        title: '<s:message code="button.view.inactive" />'
      }
    },
    reorderCfg: {
      triggerNode: '#reorderButton',
      active: {
        className: 'pure-button-primary',
        icon: 'fa fa-check-square',
        label: '<s:message code="button.finish" />'
      },
      inactive: {
        icon: 'fa fa-bars',
        label: '<s:message code="button.order" />'
      }
    },
    orderByCfg: {
      triggerNode: '#order-by'
    },
    labels:{
      checklistLinkFrom:'The work list task is waiting for a {entryType} case note in a {entryState} state.'
    },
    dialogConfig: dialogConfig,
    attachmentDialogConfig: attachmentDialogConfig,
    imageDialogConfig: imageDialogConfig,
    outputDialogConfig: outputDialogConfig,
    updateRowUrl: '<s:url value="/rest/caseNoteEntry/{id}/neighbour?position={position}&neighbourId={neighbourId}"/>',
    printURL:'<s:url value="/casenote/{contextType}?id={id}&print=true" />' 		 		
  };

  var casenoteView = new Y.app.casenote.CasenoteControllerView(config).render();
});
</script>
