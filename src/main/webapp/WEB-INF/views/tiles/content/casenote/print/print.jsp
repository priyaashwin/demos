<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<s:eval expression="@appSettings.isCaseNoteImpactEnabled()" var="impactEnabled" scope="page" />

<div id="casenoteResultsHeader"></div>
<div id="casenoteResultsTable" class="print-view"></div>

<script>
Y.use('yui-base',
        'model',
        'model-errors-plugin',
        'print-header-view',
        'handlebars-casenote-templates',
    function(Y) {
        var config, header, model;
        
        var isGroup = !!'${esc:escapeJavaScript(group.groupIdentifier)}';
        var isPerson = !!'${esc:escapeJavaScript(person.personIdentifier)}';

        config = {
            id: '<c:out value="${group.id}"/>' || '<c:out value="${person.id}"/>',
            fullId: '${esc:escapeJavaScript(group.groupIdentifier)}' || '${esc:escapeJavaScript(person.personIdentifier)}',
            name: '${esc:escapeJavaScript(group.name)}' || '${esc:escapeJavaScript(person.name)}',
            image: 'logo-print-header.png'
        };

        if (isGroup) {
            config.context = 'group';
        } else if (isPerson) {
            config.context = 'person';
            config.dateOfBirth = {
                calculatedDate: '<c:out value="${person.dateOfBirth.calculatedDate}"/>'
            };
        }



        model = new Y.Model({
            id: config.id,
            image: config.image,
            fullId: config.fullId,
            name: config.name,
            dateOfBirth: config.dateOfBirth
        });



        header = new Y.app.PrintHeaderView({
            container: Y.one('#casenoteResultsHeader'),
            template: Y.Handlebars.templates.caseNotePrintHeader,
            templateId: '#caseNotePrintHeader',
            model: model
        });

        header.render();

    });
       
    Y.use('casenote-results-print',
  	      function(Y) {
        	    
          <c:choose>
          <c:when test="${not empty group}">
          var subject = {
            subjectType: 'group',
            subjectName: '${esc:escapeJavaScript(group.name)}',
            subjectIdentifier: '${esc:escapeJavaScript(group.groupIdentifier)}',
            subjectId: '<c:out value="${group.id}"/>'
          };
          </c:when>
          <c:otherwise>
          var subject = {
            subjectType: 'person',
            subjectName: '${esc:escapeJavaScript(person.name)}',
            subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
            subjectId: '<c:out value="${person.id}"/>',
          };
          </c:otherwise>
          </c:choose>
      
          var impactEnabled='${impactEnabled}'==='true';
          
          var searchConfig = {
              sortBy: [{
                'eventDate!calculatedDate': 'desc'
              }],
              labels: {
                recordedDate: '<s:message code="casenote.results.recordedDate" javaScriptEscape="true"/>',
                editedDate: '<s:message code="casenote.results.editedDate" javaScriptEscape="true"/>',
                calculatedDate: '<s:message code="casenote.results.eventDate" javaScriptEscape="true"/>',
                event: '<s:message code="casenote.results.event" javaScriptEscape="true"/>',
                entryType: '<s:message code="casenote.results.entryType" javaScriptEscape="true"/>',
                impact: '<s:message code="casenote.results.impact" javaScriptEscape="true"/>',
                source: '<s:message code="casenote.results.source" javaScriptEscape="true"/>',
                practitioner: '<s:message code="casenote.results.practitioner" javaScriptEscape="true"/>',
                status: '<s:message code="casenote.results.status" javaScriptEscape="true"/>',
                subjectSeen: '<s:message code="casenote.results.peopleSeen" javaScriptEscape="true"/>'
              },
              initialState: 'initial',
              resultsCountNode: Y.one('#casenoteResultsCount'),
              noDataMessage: '<s:message code="casenote.find.no.results" javaScriptEscape="true"/>',
              permissions: {},
              url: '<s:url value="/rest/{subjectType}/{id}/caseNoteEntry?s={sortBy}&pageNumber=-1&pageSize=-1"/>',
              impactEnabled: impactEnabled
            };
          
          var casenotePrintView = new Y.app.casenote.CasenotePrintView({
            container:'#casenoteResultsTable',
            searchConfig:searchConfig,
            subject:subject
          }).render();
    	  });
       

</script>

<t:insertTemplate template="/WEB-INF/views/tiles/content/header/session.jsp" />