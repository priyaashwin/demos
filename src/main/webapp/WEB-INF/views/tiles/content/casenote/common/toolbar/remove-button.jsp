<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null,'CaseNoteRecord','CaseNoteRecord.Remove')" var="canRemove"/>

<c:if test="${canRemove eq true}">

	<tilesx:useAttribute name="selected" ignore="true"/>
	<tilesx:useAttribute name="first" ignore="true"/>
	<tilesx:useAttribute name="last" ignore="true"/>

	<c:if test="${selected eq true }">
	  <c:set var="aStyle" value="pure-button-active"/>
	</c:if>
	<c:if test="${first eq true }">
	  <c:set var="fStyle" value="first"/>
	</c:if>
	<c:if test="${last eq true }">
	  <c:set var="lStyle" value="last"/>
	</c:if>

	<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
		<a id="removeCasenoteButton" href="#none" class='usp-fx-all pure-button pure-button-disabled pure-button-loading <c:out value="aStyle"/>' title='<s:message code="button.removeAll"/>'>
			<i class="fa fa-minus-circle"></i>
			<span><s:message code="button.removeAll" /></span>
		</a>
	</li>

	<script>
		Y.use('yui-base', 'event-custom', 'node', 'remove-controller-view', function(Y) {
			var button = Y.one('#removeCasenoteButton');

			button.on('click', function(e){
				e.preventDefault();
				Y.fire('removeDialog:prepareForCaseNotesRemove');
			});

			var subject = {
				subjectType: 'person',
				subjectName: '${esc:escapeJavaScript(person.name)}',
				subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
				subjectId: '<c:out value="${person.id}"/>'
			};

			var personIcon = 'fa fa-user';
			var personNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

			var dialogConfig = {
				views: {
					prepareRemove: {
						header: {
							text: '<s:message code="casenote.remove.prepare.dialog.header" javaScriptEscape="true"/>',
							icon: personIcon
						},
						narrative: {
							summary: '<s:message code="casenote.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
							description: personNarrativeDescription
						},
						successMessage: '<s:message code="casenote.remove.prepare.success.message" javaScriptEscape="true" />',
						labels: {
							deleteConfirmation: '<s:message code="casenote.remove.prepare.dialog.deletionConfirmation" javaScriptEscape="true" />'
						},
						config: {
							prepareUrl: '<s:url value="/rest/caseNoteRecord/{id}/deletionRequest" />',
							url: '<s:url value="/rest/person/${person.id}/caseNoteRecord" />'
						}
					},
					remove: {
						header: {
							text: '<s:message code="casenote.remove.dialog.header" javaScriptEscape="true"/>',
							icon: personIcon
						},
						narrative: {
							summary: '<s:message code="casenote.remove.dialog.summary" javaScriptEscape="true"/>',
							description: personNarrativeDescription
						},
						successMessage: '<s:message code="casenote.remove.success.message" javaScriptEscape="true" />',
						labels: {
							deleteConfirmation: '<s:message code="casenote.remove.dialog.deletionConfirmation" javaScriptEscape="true" />'
						},
						config: {
							deleteUrl: '<s:url value="/rest/caseNoteRecord?deletionRequestId={deletionRequestId}" />',
							resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
						}
					},
					removeInProgress: {
						header: {
							text: '<s:message code="casenote.remove.dialog.header" javaScriptEscape="true"/>',
							icon: 'fa fa-wrench'
						},
						labels: {
							message: '<s:message code="casenote.remove.dialog.inProgress" javaScriptEscape="true" />',
						}
					}
				}
			};

			var config = {
				type: 'caseNotes',
				subject: subject,
				dialogConfig: dialogConfig
			};

			var casenoteRemoveController = new Y.app.remove.RemoveControllerView(config).render();
		});
	</script>

</c:if>
