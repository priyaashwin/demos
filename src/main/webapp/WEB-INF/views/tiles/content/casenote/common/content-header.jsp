<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="pure-g-r">
  <div class="pure-u-1-2">
    <h3 tabindex="0">
      <span class="context-title"><s:message code="menu.casenote"/></span>
      <span class="context-count" id="casenoteResultsCount"></span>
    </h3>
    </div>
    <div class="pure-u-1-2">
     <div id="sort-menu">
      <label class="menu-title" for="order-by"><s:message code="button.orderby"/></label>
      <select id="order-by">
        <option value="event-date"><s:message code="casenote.results.eventDate"/></option>
        <option value="recorded-date"><s:message code="casenote.results.recordedDate"/></option>
        <option value="last-edited-date"><s:message code="casenote.results.editedDate"/></option>
    </select>
  </div>
  </div>
</div>
