<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonAlertAssignment.Update')" var="canUpdate"/>
</c:if>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonAlert.View')" var="canView"/>

<script>
  Y.use('yui-base', 'person-alert-controller-view', function(Y){
      var subject = {
        subjectType: 'person',
        subjectName: '${esc:escapeJavaScript(person.name)}',
        subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
        subjectId: '<c:out value="${person.id}"/>',
      };
      
      var permissions = {
        canUpdateAlerts: '${canUpdate}' === 'true',
        canViewAlerts: '${canView}' === 'true'
      };
      
      var personIcon = 'fa fa-user';
      var personNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

      var commonLabels={
              noAlerts:'<s:message code="person.alerts.no.results" javaScriptEscape="true" />',
              alerts:'<s:message code="person.alerts.results" javaScriptEscape="true" />',
              alert:'<s:message code="person.alert" javaScriptEscape="true" />',
              startDate:'<s:message code="person.alertStartDate" javaScriptEscape="true" />',
              endDate:'<s:message code="person.alertEndDate" javaScriptEscape="true" />',
              category:'<s:message code="person.alertCategory" javaScriptEscape="true" />',
              note:'<s:message code="person.alertNote" javaScriptEscape="true" />'
      };
      
      var dialogConfig={
        views:{
          manageAlerts: {
              header: {
                  text: '<s:message code="person.alerts.manage.header" javaScriptEscape="true"/>',
                  icon: personIcon
              },
              narrative: {
                  summary: '<s:message code="person.alerts.manage.summary" javaScriptEscape="true" />',
                  description: personNarrativeDescription
              },
              labels: Y.merge(commonLabels,{
                  addAlert:'<s:message code="person.alert.add" javaScriptEscape="true" />'
              }),
              config: {
                  url: '<s:url value="/rest/person/{subjectId}"/>',
                  updateUrl:'<s:url value="/rest/person/{subjectId}?updateAssociatedAlerts=true"/>',
                  validationLabels:{
                      alertCategoryErrorMessage:'<s:message code="person.alert.validation.alertCategory" javaScriptEscape="true" />',
                      alertNoteErrorMessage:'<s:message code="person.alert.validation.alertNote" javaScriptEscape="true" />',
                      alertStartDateErrorMessage:'<s:message code="person.alert.validation.alertStartDate" javaScriptEscape="true" />',
                      alertEndDateErrorMessage:'<s:message code="person.alert.validation.alertEndDate" javaScriptEscape="true" />'
                  }
              }
          },
          viewAlerts: {
              header: {
                  text: '<s:message code="person.alerts.view.header" javaScriptEscape="true"/>',
                  icon: personIcon
              },
              narrative: {
                  summary: '<s:message code="person.alerts.view.summary" javaScriptEscape="true" />',
                  description: personNarrativeDescription
              },
              labels: commonLabels,
              config: {
                  url: '<s:url value="/rest/person/{subjectId}"/>'
              }
          }
        }
      };
      
      var config = {
              dialogConfig:dialogConfig,
              permissions:permissions,
              subject:subject
      };
      
      var personAlertController = new Y.app.person.alert.PersonAlertControllerView(config).render();
  });
</script>