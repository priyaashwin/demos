<%@page contentType="text/html;charset=UTF-8"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
 
<div id="peopleView">
    <t:insertTemplate template="/WEB-INF/views/tiles/content/person/sections/toggle-accordions.jsp" />
    
    <%-- Insert Person ADD Template  must be used in conjunction with the REACT component scripts--%>
    <t:insertTemplate template="/WEB-INF/views/tiles/content/common/person/add.jsp" />

    <%-- Insert People Dialog Template --%>
    <t:insertTemplate template="/WEB-INF/views/tiles/content/person/panel/personDialog.jsp" />	
    <%-- Insert person view section --%>
    <t:insertDefinition name="person.section" />
</div>