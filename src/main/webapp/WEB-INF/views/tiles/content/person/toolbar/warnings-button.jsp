<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson"/>

<c:if test="${readOnly ne true}">
	<c:choose>
	    <%-- if the subject context for the page is person --%>
	    <c:when test="${!empty person}">
	        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Add')" var="canAddWarning"/>
	        <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonWarning.Update')" var="canUpdateWarning"/>
	    </c:when>
	    <%-- if the subject context for the page is group i.e. ${!empty group}"> --%>
	    <c:otherwise>
	        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Add')" var="canAddWarning"/>
	        <sec:authorize access="hasPermission(null, 'Person','PersonWarning.Update')" var="canUpdateWarning"/>
	    </c:otherwise>
	</c:choose>
</c:if>
  
<c:if test="${canViewPerson eq true && securedSubjectContextVO.isSubjectAccessAllowed eq true}">
	<tilesx:useAttribute name="selected" ignore="true"/>
	<tilesx:useAttribute name="first" ignore="true"/>
	<tilesx:useAttribute name="last" ignore="true"/>

	<c:if test="${selected eq true}">
		<c:set var="aStyle" value="pure-button-active"/>
	</c:if>
	<c:if test="${first eq true }">
		<c:set var="fStyle" value="first"/>
	</c:if>
	<c:if test="${last eq true }">
		<c:set var="lStyle" value="last"/>
	</c:if>

	<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
		<c:choose>
			<c:when test="${(canAddWarning||canUpdateWarning)}">
				<a id="viewAddEditWarningButton" href="#none" class='usp-fx-all pure-button <c:out value="aStyle"/> pure-button-disabled pure-button-loading' title='<s:message code="button.hovermanagewarnings"/>'><i class="fa fa-exclamation-triangle"></i><span><s:message code="button.warnings" /></span></a>
			</c:when>
			<c:otherwise>
				<a id="viewAddEditWarningButton" href="#none" class='usp-fx-all pure-button <c:out value="aStyle"/> pure-button-disabled pure-button-loading' title='<s:message code="button.hoverviewwarnings"/>'><i class="fa fa-exclamation-triangle"></i><span><s:message code="button.warnings" /></span></a>
			</c:otherwise>
		</c:choose>
	</li>
	<script>
		Y.use('yui-base','event-custom','node',function(Y){
			var canAddWarning='${canAddWarning}'==='true',
				canUpdateWarning='${canUpdateWarning}'==='true';
			var button = Y.one('#viewAddEditWarningButton');

			button.on('click', function(e){
				e.preventDefault();

				if(canAddWarning || canUpdateWarning){
					Y.fire('warnings:showDialog',{
						id: '<c:out value="${person.id}"/>',
						action:'addEditPersonWarning'
					});
				}
				else {
					Y.fire('warnings:showDialog',{
						id: '<c:out value="${person.id}"/>',
						action:'viewPersonWarning'
					});
				}
			});

			//remove disabled style
			button.removeAttribute('aria-disabled').removeAttribute('disabled').removeClass('pure-button-disabled').removeClass('pure-button-loading');
		});
	</script>
</c:if>
