<%@ page import="java.util.Locale"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc"
	uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'Citizenship','Citizenship.Add')" var="canAddCitizenship" />
	<sec:authorize access="hasPermission(null, 'Citizenship','Citizenship.Update')" var="canUpdateCitizenship" />
	<sec:authorize access="hasPermission(null, 'Citizenship','Citizenship.Remove')" var="canRemoveCitizenship" />
</c:if>

<c:set var="canUpdate" value="false" />

<sec:authorize access="hasPermission(null, 'Citizenship','Citizenship.View')" var="canViewCitizenship" />

<sec:authorize access="hasPermission(null, 'Security','Security.Open_Access')" var="hasOpenAccess" />

<%
  //Get the jvm locale language
			String lang = Locale.getDefault().getLanguage();
%>

<%-- polyfill for missing canvas functionality in IE --%>
<script src='<s:url value="/js/cropper/canvas.js"/>'></script>
<script>	
	var localLanguageCode='<%out.print(lang);%>';

	<%-- Person popup - to be used to add, edit or view --%>
	Y.use('yui-base',
			'event-custom',
			'handlebars-helpers',
			'handlebars-person-templates',
			'person-dialog-views',
			'usp-person-Person',
			'usp-person-PersonWithCitizenships',
			'usp-contact-Contact',
			'usp-contact-NewContact',
			'usp-contact-NewEmailContact',
			'usp-contact-NewWebContact',
			'usp-contact-NewSocialMediaContact',
			'usp-contact-NewTelephoneContact',	
			'usp-person-NewCitizenship',
			'usp-person-UpdateCitizenship',
			'usp-person-PersonReferenceNumber',
			'usp-person-NewPersonReferenceNumber',
			'usp-person-NewPersonAddress',
			'usp-person-UpdateClosePersonAddress',
			'usp-person-PersonAddress',
			'usp-person-PersonName',
			'usp-person-UpdatePersonAddress',
			'usp-person-UpdatePersonAddressResidents',
			'usp-address-UpdateEndAddress',
			'usp-person-UpdatePersonAddressStartEnd',
			'usp-person-UpdatePersonLanguage',
			'multi-panel-popup',
			'multi-panel-model-errors-plugin', 
			'model-form-link',
			'usp-session-storage',
			'model-transmogrify',
			'usp-relationshipsrecording-UpdateDeletePersonReferenceNumbers',
			'usp-relationshipsrecording-UpdateDeletePersonContacts',
			'usp-address-Location',
			'usp-address-NewLocation',
			'usp-person-PersonWithLanguages',
			'app-utils-person',
			'usp-person-UpdatePersonNotCarriedToTerm',
			'usp-person-UpdateNotCarriedPersonBorn', 
			'usp-person-UpdateNotCarriedPersonUnborn',
			'usp-person-UpdatePersonDeceasedToAlive',
			'usp-person-UpdatePersonDeceased',
			'usp-person-PersonPicture',
			'usp-person-NewPersonPicture',
			'json-parse',
			'file-html5',
			'person-picture-dialog-views',
			'usp-person-UpdateCloseAddPersonAddresses',
			'relationship-dialog-views',
			'usp-relationship-PersonPersonRelationship',
			'usp-relationship-helper',
			'usp-relationship-PersonPersonRelationshipDetails',
			'usp-relationship-SuggestedPersonPersonRelationships',
			'usp-relationshipsrecording-AsymmetricPersonPersonRelationshipDetail',
			'usp-relationship-PersonAddressRelationshipName',
			'reference-number-dialog-views',
			'usp-relationshipsrecording-PersonOrganisationRelationshipsResult',
	function(Y){
		var L=Y.Lang,O=Y.Object,A=Y.Array,N = Y.Number,
			referenceNumberEdited=false, 
			referenceNumberDeleted=false,
			UP= Y.app.utils.person,
			isEmpty = function (v) { return v === null || v === '' };
		
		var currentUser = new Y.Model({
				id:'<c:out value="${currentUser.id}"/>',
				personIdentifier:'${esc:escapeJavaScript(currentUser.personIdentifier)}',
				forename:'${esc:escapeJavaScript(currentUser.forename)}',
				surname:'${esc:escapeJavaScript(currentUser.surname)}',
				name:'${esc:escapeJavaScript(currentUser.name)}',
				gender:'${esc:escapeJavaScript(currentUser.gender)}',
				dob:'<c:out value="${currentUser.dateOfBirth.getCalculatedDate()}"/>',
				dateOfBirthEstimated:'<c:out value="${currentUser.dateOfBirthEstimated}"/>',
				lifeState: '${esc:escapeJavaScript(currentUser.lifeState)}',
				dueDate:'<c:out value="${currentUser.dueDate.getTime()}"/>',
				personTypes: '<c:out value="${currentUser.getPersonTypes()}"/>'
		});

		var proceedEditDialog = function(e) {
				var view = this.get('activeView');
				var roles = view.get('personRoles');
				var existingRoles = view.get('existingPersonRoles');
				e.preventDefault();

				if(existingRoles && roles.length == existingRoles.length) {
						var e={};
						e.code=400;
						e.msg={
								validationErrors:{PersonType: commonLabels.personRoleSelectionRequired}
						}
						view.fire('error',{error:e});
						return;
				}

				Y.fire('person:showDialog', {
						action: 'editPerson',
						personRoles: view.get('personRoles'),
						id:view.get('id')
				});
		};


		var ViewPersonDialog=Y.Base.create("ViewPersonForm", Y.usp.person.Person, [],{
				url:'<s:url value="/rest/person/{id}"/>'
		});
		
		
		var isAttrNull = function(attr) {
				if (L.isNull(attr) || L.isUndefined(attr) || attr === "") {
						return true;
				}

				return false;
		}


		var PersonRoleSelectForm = Y.Base.create("PersonRoleSelectForm",  Y.usp.person.NewPerson, [Y.usp.ModelFormLink], {
				url:'<s:url value="/rest/person?checkNhsNumber=true"/>',
				form:'#personRoleSelectForm'
		});

		//This model will be transformed into a UpdatePerson model when it is submitted back to the server
		var UpdatePersonForm=Y.Base.create("UpdatePersonForm", Y.usp.person.PersonWithCitizenships, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
				url:'<s:url value="/rest/person/{id}?checkNhsNumber=true"/>',
				form:'#personEditForm'
		});

		var PersonPreviousNameForm = Y.Base.create('personPreviousNameForm', Y.usp.person.PersonName, {
				url:'<s:url value="/rest/personName/{id}"/>',
				form: '#personPreviousNameForm'
		});

		var RemovePreviousNameForm = Y.Base.create('removePreviousNameForm', Y.usp.person.Person, {
				url:'<s:url value="/rest/person/{id}"/>',
				form: '#removePreviousNameForm'
		});

		var ChangePersonNameForm = Y.Base.create("ChangePersonNameForm", Y.usp.person.Person, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
				url:'<s:url value="/rest/person/{id}"/>',
				form: '#changePersonNameForm',
				//Provide our own implementation of getURL to update URL on save
				getURL: function(action, options) {
						if (action==='update') {
								return this._substituteURL('<s:url value="/rest/person/{id}?checkNhsNumber=false&isNewMainName=true"/>', Y.merge(this.getAttrs(), options));
						}
						//return standard operation
						return ChangePersonNameForm.superclass.getURL.apply(this, [action, options]);
				}
		});
		
		// UpdateModel ->  Y.usp.person.Person -> Y.Model
		var ChangeUnbornModel = Y.Base.create("ChangeUnbornModel", Y.usp.person.Person, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
				url:'<s:url value="/rest/person/{id}"/>',
				form: '#changeUnbornForm',
				getURL: function(action, options) {
						if (action==='update') {
								if (this.getAttrs().notCarriedToTermType)
										return this._substituteURL('<s:url value="/rest/person/{id}/status?action=notCarriedToTerm"/>', Y.merge(this.getAttrs(), options));
								else
										return this._substituteURL('<s:url value="/rest/person/{id}/status?action=born&checkNhsNumber=true"/>', Y.merge(this.getAttrs(), options));
						}
						//return standard operation
						return ChangeUnbornModel.superclass.getURL.apply(this, [action, options]);
				}
		});

		// UpdateModel ->  Y.usp.person.Person -> Y.Model
		var ChangeDeceasedToAliveAdministrativelyModel = Y.Base.create("ChangeDeceasedToAliveAdministrativelyModel", Y.usp.person.Person, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
				url:'<s:url value="/rest/person/{id}"/>',
				form: '#changeDeceasedToAliveForm',
				getURL: function(action, options) {
						if (action==='update') {
								return this._substituteURL('<s:url value="/rest/person/{id}/status?action=deceasedToAliveAdministratively"/>', Y.merge(this.getAttrs(), options));
						}
						//return standard operation
						return ChangeDeceasedToAliveAdministrativelyModel.superclass.getURL.apply(this, [action, options]);
				}
		});

		//TODO comment required
		var ChangeNotCarriedModel = Y.Base.create("ChangeNotCarriedModel", Y.usp.person.Person, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
			url:'<s:url value="/rest/person/{id}"/>',
			form: '#changeNotCarriedForm',
			getURL:function(action, options){
				if(action==='update'){
					if(this.getAttrs().outcome === 'UNBORN')
						return this._substituteURL('<s:url value="/rest/person/{id}/status?action=notCarriedToUnborn"/>', Y.merge(this.getAttrs(), options));
					else if(this.getAttrs().outcome === 'BORN')
						return this._substituteURL('<s:url value="/rest/person/{id}/status?action=notCarriedToAlive&checkNhsNumber=true"/>', Y.merge(this.getAttrs(), options));
					
				}
				//return standard operation
				return ChangeNotCarriedModel.superclass.getURL.apply(this, [action, options]);
			}
			
		});
		
		var ChangeAliveModel = Y.Base.create("ChangeAliveModel", Y.usp.person.Person, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
			url:'<s:url value="/rest/person/{id}"/>',
			form: '#changeAliveForm',
			getURL:function(action, options){
				if(action==='update'){
					return this._substituteURL('<s:url value="/rest/person/{id}/status?action=deceased"/>', Y.merge(this.getAttrs(), options));
				}
				//return standard operation
				return ChangeAliveModel.superclass.getURL.apply(this, [action, options]);
			}
		});
				
		var UpdatePersonLanguageForm=Y.Base.create("UpdatePersonLanguageForm", Y.usp.person.UpdatePersonLanguage, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
			url:'<s:url value="/rest/person/'+'${person.id}'+'?updateAssociatedLanguages=true"/>',	
			form: '#personLanguageForm',
			validate: function(attributes, callback){
				var errs={};				
				var localLanguageId;
				if (!attributes.primaryLanguageId.length) {
					errs['firstLanguage-input']=commonLabels.firstLanguage+' is mandatory.';
				}
				if (typeof Y.uspCategory.person.languages.category.codedEntries[localLanguageCode] != 'undefined') {
					localLanguageId=Y.uspCategory.person.languages.category.codedEntries[localLanguageCode].id;
				} else if(typeof Y.uspCategory.person.languages.category.codedEntries['en'] != 'undefined'){
					// default to id of English coded entry
					localLanguageId=Y.uspCategory.person.languages.category.codedEntries['en'].id;
				}
				
				if (attributes.primaryLanguageId!=localLanguageId) {
					if (!attributes.proficiencyInOfficialLanguage) {
						errs['proficiencyInOfficialLanguage-input']=commonLabels.proficiencyInOfficialLanguage+' is mandatory.';
					}
				}
				if(O.keys(errs).length>0){
					return callback({
						code:400,
						msg:{validationErrors:errs}
					});
				}
				// TO DO:
				// Need to look at this for these reasons:
				// 1) This validate function is being fired twice
				// 2) The second time it is called:
				// 		-- if interpreterRequired has been checked it is set to 'on' instead of true
				// 		-- if interpreterRequired has not been checked it is set to null instead of false
				// Setting the values here is a workaround 
				// Barny is going to look at this issue in the framework at some point
				if (this.get('interpreterRequired')==='on'){
					this.set('interpreterRequired',true)
				}
				if (this.get('interpreterRequired')===null){
					this.set('interpreterRequired',false)
				}
				
				// Data in form is correct.Now pass model to the server for any additional validation
				return UpdatePersonLanguageForm.superclass.validate.apply(this, arguments);				
			}
		});
		
		var UpdateRemovePersonReferenceNumberForm = Y.Base.create("UpdateRemovePersonReferenceNumberForm", Y.usp.person.PaginatedPersonReferenceNumberList,[],{
			url:'<s:url value="/rest/person/{id}/referenceNumber?s=type:asc&pageSize=-1"/>'
		});
		
		var UpdatePersonAddressResidentsModel = Y.Base.create("UpdatePersonAddressResidentsModel", Y.usp.person.UpdatePersonAddressResidents, [],{
			url: L.sub('<s:url value = "/rest/personAddress/{personId}/residents"/>', {personId:'<c:out value="${person.id}"/>'})
		});
		
		var UpdateCloseAddPersonAddressesModel = Y.Base.create("UpdateCloseAddPersonAddressesModel", Y.usp.person.UpdateCloseAddPersonAddresses, [],{			
			url: '<s:url value = "/rest/personAddress/invoke?action=combinedCloseAdd&checkInactive=true"/>',
				
			validate: function(attributes, callback){
				var errs={};
				var residents={};
				var startDate;
				var self = this;
				
				this.residents = this.residentsDetails;
				
				this.residents.forEach(function(details){
					// check if end date is before the start date of new address to be added
					if (details.startDate > self.endDate) {
						if(O.keys(errs).length<1){
							 errs['resident_0'] = 'The following resident(s) need to be processed individually:';
						} 
						errs['resident_'+details.id]=details.personName;
					}
				});
				if(O.keys(errs).length>0){
					return callback({
						code:400,
						msg:{validationErrors:errs}
					});
				}
				// Now call validate
				return UpdateCloseAddPersonAddressesModel.superclass.validate.apply(this, arguments);
			}					
		});
		
		var UpdateRemoveReferenceNumberModel=Y.Base.create("UpdateRemoveReferenceNumberModel", Y.usp.relationshipsrecording.UpdateDeletePersonReferenceNumbers,[],{
			url: '<s:url value="/rest/personReferenceNumber/invoke?action=combinedUpdateDelete"/>',
			
			validate: function(attributes, callback){
				var errs={};
				attributes.updateReferenceNumberVOs.forEach(function(referenceNumber){
					if(referenceNumber.referenceNumber===""){
						// No access to reference number type in the model hence the Y.one lookup to get the name of the reference number in error
						errs['referenceNumber_'+referenceNumber.id]=Y.one('#referenceNumberLabel_'+referenceNumber.id).getHTML()+' cannot be empty';
					}
				});

				if(O.keys(errs).length>0){
					return callback({
						code:400,
						msg:{validationErrors:errs}
					});
				}
				
				// Now call any validate in the class we extended UpdateDeletePersonReferenceNumbers
				return UpdateRemoveReferenceNumberModel.superclass.validate.apply(this, arguments);
			}
		});
		
		var UpdateRemoveContactForm = Y.Base.create("UpdateRemoveContactForm", Y.usp.contact.ContactList,[],{
			url:'<s:url value="/rest/person/'+'${person.id}'+'/contact"/>'						
		});	
				
		var UpdateRemoveContactModel=Y.Base.create("UpdateRemoveContactModel", Y.usp.relationshipsrecording.UpdateDeletePersonContacts,[],{
			url: '<s:url value="/rest/contact/invoke?action=combinedUpdateDeletePerson&personId='+'${person.id}'+'"/>',
					
			validate: function(attributes, callback){
				var errs={},panelLabel='';				
				attributes.updateContactVOs.forEach(function(contact){									
					if((contact._type=='UpdateTelephoneContact' && contact.number=="") ||
				   		(contact._type=='UpdateSocialMediaContact' && contact.identifier=="") ||
				   		(contact._type=='UpdateEmailContact' && contact.address=="") ||
				   		(contact._type=='UpdateWebContact' && contact.address=="")
				  	  ) {
						
						for(var i=0; i<panelLabels.length; i++) {
							if (panelLabels[i][0] == contact.id){
								panelLabel = panelLabels[i][1];
							}
						}
						
						errs['contact_'+contact.id]=Y.one('#contactLabel_'+contact.id).getHTML() + ' within ' + panelLabel + ' section cannot be empty';
					}
					else if (contact._type=='UpdateEmailContact'  && contact.address!="") {						
						
						/* var atPart =  (contact.address).indexOf("@")+2;
						var subaddress = (contact.address).substring(atPart);
						var dotPosition = subaddress.indexOf(".");
						
						if (dotPosition==-1) {
							errs['contact_'+contact.id]='Please enter a valid Email address';
						} */
						
						var validEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
						if (!validEmail.test(contact.address)) {					    	
					    	errs['contact_'+contact.id]='Please enter a valid Email address';
					   	}
					}
				});
				
				if(O.keys(errs).length>0){
					return callback({
						code:400,
						msg:{validationErrors:errs}
					});
				}
				
				// Data in form is correct.Now pass model to the server for any additional validation
				return UpdateRemoveContactModel.superclass.validate.apply(this, arguments);
				//return callback();
			}				
		});		
		
		var AddPersonAddressForm = Y.Base.create("AddPersonAddressForm", Y.usp.person.NewPersonAddress, [Y.usp.ModelFormLink], { 
      		form: '#addPersonAddressForm',
      		url: '<s:url value="/rest/person/{personId}/address"/>'
    	});
		
		var ReopenPersonAddressForm = Y.Base.create("ReopenPersonAddressForm", Y.usp.person.PersonAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], { 
      		form: '#reopenPersonAddressForm'
    	});
		
		var UpdatePersonLocationForm = Y.Base.create("UpdatePersonLocationForm", Y.usp.person.PersonAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], { 
      		form: '#updatePersonLocationForm',
      		customValidation: function (attrs) {
                var errors = {};
                if (isEmpty(attrs.locationId)) {
                    errors.locationId = 'Location is mandatory.';
                }
                return errors;
            }
    	});
		
		var UpdateStartEndPersonAddressForm = Y.Base.create("UpdateStartEndPersonAddressForm", Y.usp.person.PersonAddress, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify], { 
      		form: '#updateStartEndPersonAddressForm'
    	});
		
		var EndPersonAddressForm = Y.Base.create("EndPersonAddressForm", Y.usp.person.PersonAddress, [Y.usp.ModelFormLink,Y.usp.ModelTransmogrify], { 
      		form: '#endPersonAddressForm',
      		url: '<s:url value="/rest/personAddress/{id}"/>',
      		getURL:function(action, options){
				if(action==='update'){
					return this._substituteURL('<s:url value="/rest/personAddress/{id}/status?action=close&checkInactive=true"/>', Y.merge(this.getAttrs(), options));
				}
				//return standard operation
				return EndPersonAddressForm.superclass.getURL.apply(this, [action, options]);
      		}	
      	});
		
		var RemovePersonAddressForm = Y.Base.create("RemovePersonAddressForm", Y.usp.person.PersonAddress, [Y.usp.ModelFormLink,Y.usp.ModelTransmogrify], { 
      		url: '<s:url value="/rest/personAddress/{id}"/>',
      		template:Y.Handlebars.templates["removePersonAddress"],
      		getURL:function(action, options){
      			
				if(action==='delete'){
					return this._substituteURL('<s:url value="/rest/personAddress/{id}"/>', Y.merge(this.getAttrs(), options));
				}
				
				//return standard operation
				return RemovePersonAddressForm.superclass.getURL.apply(this, [action, options]);
      		}	
      	});
		
		var UpdatePersonPictureForm = Y.Base.create("UpdatePersonPictureForm", Y.usp.person.NewPersonPicture, [Y.usp.ModelFormLink],{
			url: '<s:url value="/rest/personPicture"/>',
			form: '#editPersonPictureForm'
		});
		
		var RemovePersonPictureForm = Y.Base.create("RemovePersonAddressForm", Y.usp.person.PersonPicture, [Y.usp.ModelFormLink], { 
			url: '<s:url value="/rest/person/${person.id}/picture"/>',
			removeUrl: '<s:url value="/rest/personPicture/{pictureId}"/>'
    });
		
		var NewPersonContactForm = Y.Base.create("NewPersonContactForm", Y.usp.contact.NewContact,[],{
			form: '#personContactAddForm'
		});
		
		var NewPersonContactModel=Y.Base.create("NewPersonContactModel", Y.usp.contact.NewContact,[],{
			url: '<s:url value="/rest/person/'+'${person.id}'+'/contact"/>',

			validate: function(attributes, callback){
				var errs={};						
				
				if (attributes._type=='NewEmailContact' && attributes.address=="") {
					errs['contactDetail-input']='Email address is mandatory';
				} else if (attributes._type=='NewWebContact' && attributes.address=="") {
				  	errs['contactDetail-input']='Web address is mandatory';                
				} else if(attributes._type=='NewEmailContact') {
					var atPart =  (attributes.address).indexOf("@")+2;
					var subaddress = (attributes.address).substring(atPart);
					var dotPosition = subaddress.indexOf(".");
						
					if (dotPosition==-1) {
						errs['contactDetail-input']='Please enter a valid Email address';
					}					
				}
				
				if(O.keys(errs).length>0){
					return callback({
						code:400,
						msg:{validationErrors:errs}
					});
				}
				
				// Data in form is correct.Now pass model to the server for any additional validation
				return NewPersonContactModel.superclass.validate.apply(this, arguments);				
			}
		});

		var subjectNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

	    var subject = {
	        subjectId: '<c:out value="${person.id}"/>',
	        subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
	        subjectName: '${esc:escapeJavaScript(person.name)}',
	        subjectType: 'person'
	    };
	    
		var permissions = {
				canViewCitizenship: '${canViewCitizenship}'==='true',
				canAddCitizenship: '${canAddCitizenship}'==='true',
				canUpdateCitizenship: '${canUpdateCitizenship}'==='true',
				canRemoveCitizenship: '${canRemoveCitizenship}'==='true',
				hasOpenAccess: '${hasOpenAccess}'==='true'
		};

		var commonLabels = {
				personId:'<s:message code="person.personId"/>',
				unborn:'<s:message code="person.Unborn"/>',
				deceased:'<s:message code="person.deceased"/>',
				dueDate:'<s:message code="person.add.dueDate"/>',
				notCarriedToTermDate:'<s:message code="person.add.notCarriedToTermDate"/>',
				notCarriedToTerm:'<s:message code="person.add.notCarriedToTerm"/>',
				dueDatePopupMessageTitle: '<s:message code="person.add.dueDatePopup.title"/>',
				notCarriedToTermDatePopupMessageTitle: '<s:message code="person.add.notCarriedToTermDatePopup.title"/>',
				dueDatePopupMessageContent: '<s:message code="person.add.dueDatePopup.content" javaScriptEscape="true"/>',
				notCarriedToTermDatePopupMessageContent: '<s:message code="person.add.notCarriedToTermDatePopup.content" javaScriptEscape="true"/>',
				unBornDefaultValue: '<s:message code="person.add.forename.unBornDefaultValue"/>',
				title: '<s:message code="person.title"/>',
				forename: '<s:message code="person.forename"/>',
				middleNames: '<s:message code="person.middleNames"/>',
				surname: '<s:message code="person.surname"/>',
				knownAs: '<s:message code="person.knownAs"/>',
				knownAsPopupMessageTitle: '<s:message code="person.add.knownAsPopup.title"/>',
				knownAsPopupMessageContent: '<s:message code="person.add.knownAsPopup.content"/>',
				alternateNames: '<s:message code="person.alternateNames"/>',
				alternateNamesPopupMessageTitle: '<s:message code="person.add.alternateNamesPopup.title"/>',
				alternateNamesPopupMessageContent: '<s:message code="person.add.alternateNamesPopup.content"/>',
				name: '<s:message code="person.name"/>',
				gender: '<s:message code="person.gender"/>',
				genderIdentity: '<s:message code="person.genderIdentity"/>',
				sexualOrientation: '<s:message code="person.sexualOrientation"/>',
				genderIdentitySelfDefined: '<s:message code="person.genderIdentitySelfDefined"/>',
				ethnicity: '<s:message code="person.ethnicity"/>',
				religion: '<s:message code="person.religion"/>',
				countryOfBirth: '<s:message code="person.countryOfBirth"/>',
				citizenship: '<s:message code="person.citizenships.citizenship"/>',
				startDate: '<s:message code="person.citizenships.startDate"/>',
				endDate: '<s:message code="person.citizenships.endDate"/>',
				dateOfBirth: '<s:message code="person.dateOfBirth"/>',
				born:'<s:message code="person.born"/>',
				died:'<s:message code="person.died"/>',
				age:'<s:message code="person.age"/>',
				ageAtDeath:'<s:message code="person.ageAtDeath"/>',
				chiNumber: '<s:message code="person.chiNumber"/>',
				nhsNumber: '<s:message code="person.nhsNumber"/>',
				nhsNumberFirstPlaceholder: '<s:message code="person.add.nhsNumberFirstPlaceholder"/>',
				nhsNumberSecondPlaceholder: '<s:message code="person.add.nhsNumberSecondPlaceholder"/>',
				nhsNumberThirdPlaceholder: '<s:message code="person.add.nhsNumberThirdPlaceholder"/>',
				dobPopupMessageTitle: '<s:message code="person.add.dateOfBirthPopup.title"/>',
				dobPopupMessageContent: '<s:message code="person.add.dateOfBirthPopup.content" javaScriptEscape="true"/>',
				diedDatePopupMessageTitle: '<s:message code="person.add.diedDatePopup.title"/>',
				deceasedBornDiedDatePopup: '<s:message code="person.add.deceasedBornDiedDatePopup.content" javaScriptEscape="true"/>',
				personOtherNames: '<s:message code="person.otherNames"/>',
				dobEstimateMessage: '<s:message code="person.add.dobEstimateMessage" javaScriptEscape="true"/>',
				dobEstimateMessageWithChiNumber:'<s:message code="person.dobEstimateMessage.chiNumber" javaScriptEscape="true"/>',
				genderDigitMismatchWithChiNumber:'<s:message code="person.genderDigitMismatchWithChiNumber" javaScriptEscape="true"/>',
				bornAndDiedEstimateMessage: '<s:message code="person.add.bornAndDiedEstimateMessage" javaScriptEscape="true"/>',
				dobCheckboxMessage: '<s:message code="person.add.dobExact"/>',
				deceasedEstimatedCheckMessage:'<s:message code="person.add.deceasedEstimateCheckboxLabel"/>',
				dob_either: '<s:message code="person.add.either"/>',
				dob_or: '<s:message code="person.add.or"/>',
				viewPersonHeader: '<s:message code="person.view.header"/>',
				changeUnbornStatus: '<s:message code="person.identificationPanel.changeUnbornStatus" />',
				changeUnbornStatusLink: '<s:message code="person.identificationPanel.changeUnbornStatusLink" />',
				changeUnbornStatusSummaryBeginning: '<s:message code="person.identificationPanel.changeUnbornStatus.summary.beginning" javaScriptEscape="true"/>',
				changeUnbornStatusSummaryEnding: '<s:message code="person.identificationPanel.changeUnbornStatus.summary.ending" />',
				changeUnbornStatusSuccessMessage:'<s:message code="person.identificationPanel.changeUnbornStatus.success.message" javaScriptEscape="true"/>',
				changeNotCarriedStatus: '<s:message code="person.identificationPanel.changeNotCarriedStatus" />',
				changeNotCarriedStatusLink: '<s:message code="person.identificationPanel.changeNotCarriedStatusLink" />',
				changeNotCarriedStatusSummaryBeginning: '<s:message code="person.identificationPanel.changeNotCarriedStatus.summary.beginning" javaScriptEscape="true"/>',
				changeNotCarriedStatusSummaryEnding: '<s:message code="person.identificationPanel.changeNotCarriedStatus.summary.ending" />',
				changeNotCarriedStatusSuccessMessage:'<s:message code="person.identificationPanel.changeNotCarriedStatus.success.message" javaScriptEscape="true"/>',
				changeNotCarriedStatusPopupTitle:'<s:message code="person.identificationPanel.changeNotCarriedStatus.popup.title" javaScriptEscape="true"/>',
				changeNotCarriedStatusPopupContent:'<s:message code="person.identificationPanel.changeNotCarriedStatus.popup.content" javaScriptEscape="true"/>',
				changeAliveStatus: '<s:message code="person.identificationPanel.changeAliveStatus" />',
				changeAliveStatusLink: '<s:message code="person.identificationPanel.changeAliveStatusLink" />',
				changeAliveStatusSummaryBeginning: '<s:message code="person.identificationPanel.changeAliveStatus.summary.beginning" javaScriptEscape="true"/>',
				changeAliveStatusSummaryEnding: '<s:message code="person.identificationPanel.changeAliveStatus.summary.ending" />',
				changeAliveStatusSuccessMessage:'<s:message code="person.identificationPanel.changeAliveStatus.success.message" javaScriptEscape="true"/>',
				changeAliveStatusPopupTitle:'<s:message code="person.identificationPanel.changeAliveStatus.popup.title" javaScriptEscape="true"/>',
				changeAliveStatusPopupContent:'<s:message code="person.identificationPanel.changeAliveStatus.popup.content" javaScriptEscape="true"/>',
				changeName: '<s:message code="person.identificationPanel.changeName" />',
				changeNameLink: '<s:message code="person.identificationPanel.changeNameLink" />',
				changeNameSummaryBeginning: '<s:message code="person.identificationPanel.changeName.summary.beginning" javaScriptEscape="true"/>',
				changeNameSummaryEnding: '<s:message code="person.identificationPanel.changeName.summary.ending" />',
				changeNameSuccessMessage:'<s:message code="person.identificationPanel.changeName.success.message" javaScriptEscape="true"/>',
				previousNameRemoveLink: '<s:message code="person.identificationPanel.previousNameLink" javaScriptEscape="true"/>',
				previousNamePopupTitle: '<s:message code="person.identificationPanel.previousName.popup.title" javaScriptEscape="true"/>',
				previousNamePopupContent: '<s:message code="person.identificationPanel.previousName.popup.content" javaScriptEscape="true"/>',
				previousNameRemoveConfirm: '<s:message code="person.identificationPanel.previousName.removeConfirm" javaScriptEscape="true"/>',
				previousNameSummary: '<s:message code="person.identificationPanel.previousName.summary" javaScriptEscape="true" />',
				previousNameSuccessMessage: '<s:message code="person.identificationPanel.previousName.success.message" javaScriptEscape="true" />',
				editPersonHeader:'<s:message code="person.edit.header"/>',
				changeUnbornHeader:'<s:message code="person.change.unborn.header"/>',
				changeDeceasedHeader: '<s:message code="person.change.deceasedtoalive.header"/>',
				changeUnbornPersonSummaryHeader:'<s:message code="person.change.unborn.personHeader"/>',
				changeUnbornPregnancyInfo: '<s:message code="person.change.unborn.pregnancyInfo"/>',
				changeNotcarriedHeader:'<s:message code="person.change.notcarried.header"/>',
				changeNotcarriedPersonSummaryHeader:'<s:message code="person.change.unborn.personHeader"/>',
				changeNotcarriedPregnancyInfo: '<s:message code="person.change.notcarried.pregnancyInfo"/>',
				changeAliveHeader:'<s:message code="person.change.alive.header"/>',
				changeAlivePersonSummaryHeaderBeginning:'<s:message code="person.change.alive.personHeader.beginning"/>',
				changeAlivePersonSummaryHeaderEnding:'<s:message code="person.change.alive.personHeader.ending"/>',
				changeAlivePregnancyInfo: '<s:message code="person.change.alive.pregnancyInfo"/>',
				changeAliveAlertInfo: '<s:message code="person.change.alive.alertInfo"/>',
				editPersonSummary:'<s:message code="person.edit.narrative" javaScriptEscape="true"/>',
				editPersonSuccessMessage:'<s:message code="person.edit.success.message" javaScriptEscape="true"/>',
				referenceNumberType: '<s:message code="person.referenceNumber.referenceNumberType"/>',
				number:'<s:message code="person.referenceNumber.number"/>',
				addReferenceNumberMessage: '<s:message code="person.referenceNumber.add.narrative" javaScriptEscape="true"/>',
				languageHeader: '<s:message code="person.language.update.header"/>',
				languageAddHeader: '<s:message code="person.language.add.header"/>',
				firstLanguage: '<s:message code="person.language.update.firstLanguage"/>',
				otherLanguage: '<s:message code="person.language.update.otherLanguage"/>',
				nonVerbalLanguage: '<s:message code="person.language.update.nonVerbalLanguage"/>',
				proficiencyInOfficialLanguage: '<s:message code="person.language.update.proficiencyInOfficialLanguage"/>',
				interpreterRequired: '<s:message code="person.language.update.interpreterRequired"/>',
				updateLanguageMessage: '<s:message code="person.language.update.narrative" javaScriptEscape="true"/>',
				addLanguageMessage: '<s:message code="person.language.add.narrative" javaScriptEscape="true"/>',
				languageUpdateSuccessMessage:'<s:message code="person.language.update.successMessage" javaScriptEscape="true"/>',
				languageAddSuccessMessage:'<s:message code="person.language.add.successMessage" javaScriptEscape="true"/>',
				addReferenceNumberHeader: '<s:message code="person.referenceNumber.add.header" javaScriptEscape="true"/>',
				editReferenceNumberHeader: '<s:message code="person.referenceNumber.edit.header" javaScriptEscape="true"/>',
				editReferenceNumberNarrative: '<s:message code="person.referenceNumber.edit.narrative" javaScriptEscape="true"/>',
				referenceNumberAddSuccessMessage:  '<s:message code="person.referenceNumber.add.success.message" javaScriptEscape="true"/>',
				referenceNumberSingleEditSuccessMessage:'<s:message code="person.referenceNumber.single.edit.success.message" javaScriptEscape="true"/>',
				referenceNumberSingleRemoveSuccessMessage:'<s:message code="person.referenceNumber.single.remove.success.message" javaScriptEscape="true"/>',
				referenceNumberMultipleEditSuccessMessage:'<s:message code="person.referenceNumber.multiple.edit.success.message" javaScriptEscape="true"/>',
				referenceNumberMultipleRemoveSuccessMessage:'<s:message code="person.referenceNumber.multiple.remove.success.message" javaScriptEscape="true"/>',
				referenceNumberMultipleEditRemoveSuccessMessage:'<s:message code="person.referenceNumber.multiple.edit.remove.success.message" javaScriptEscape="true"/>',
				isMandatoryMessage: '<s:message code="label.mandatory" javaScriptEscape="true"/>',
				addPersonAddressHdr: '<s:message code="person.address.add.header" javaScriptEscape="true"/>',
				addPersonLocationSuccessMessage: '<s:message code="location.add.success.message" javaScriptEscape="true"/>',
				addPersonAddressSuccessMessage:'<s:message code="person.address.add.success.message" javaScriptEscape="true"/>',
				isMandatoryMessage: '<s:message code="label.mandatory" javaScriptEscape="true"/>',
				endPersonAddressHdr: '<s:message code="person.address.end.header" javaScriptEscape="true"/>',
				reopenPersonAddressHdr: '<s:message code="person.address.reopen.header" javaScriptEscape="true"/>',
				reopenPersonAddressSuccessMessage: '<s:message code="person.address.reopen.success.message" javaScriptEscape="true"/>',
				reopenPersonAddressNarrative: '<s:message code="person.address.reopen.narrativeSummary" javaScriptEscape="true"/>',
				reopenAddressConfirm:'<s:message code="person.address.reopen.reopenConfirm" javaScriptEscape="true"/>',
				updatePersonAddressDatesHdr: '<s:message code="person.address.date.update.header" javaScriptEscape="true"/>',
				updatePersonAddressDatesSuccessMessage: '<s:message code="person.address.date.update.success.message" javaScriptEscape="true"/>',
				updatePersonAddressDatesNarrative: '<s:message code="person.address.date.update.narrativeSummary" javaScriptEscape="true"/>',
				updatePersonAddressDatesConfirm:'<s:message code="person.address.date.updateDateConfirm" javaScriptEscape="true"/>',
				updatePersonAddressLocationHdr: '<s:message code="person.address.location.update.header" javaScriptEscape="true"/>',
				updatePersonAddressLocationSuccessMessage: '<s:message code="person.address.location.update.success.message" javaScriptEscape="true"/>',
				updatePersonAddressLocationNarrative: '<s:message code="person.address.location.update.narrativeSummary" javaScriptEscape="true"/>',
				updatePersonAddressLocationConfirm:'<s:message code="person.address.location.update.confirm" javaScriptEscape="true"/>',
				movePersonAddressHdr: '<s:message code="person.address.move.header" javaScriptEscape="true"/>',
				personName: '<s:message code="person.address.move.personName" javaScriptEscape="true"/>',			
				update: '<s:message code="person.address.move.update" javaScriptEscape="true"/>',
				moveResidents: '<s:message code="person.address.move.moveResidents" javaScriptEscape="true"/>',
				narrativeSummaryResidentsLocationUpdate: '<s:message code="person.address.location.update.narrativeSummaryResidents" javaScriptEscape="true"/>',
				narrativeSummaryResidents: '<s:message code="person.address.move.narrativeSummaryResidents" javaScriptEscape="true"/>',
				narrativeDescriptionReason: '<s:message code="person.address.move.narrativeDescriptionReason" javaScriptEscape="true"/>',
				narrativeDescriptionStart:  '<s:message code="person.address.move.narrativeDescriptionStart" javaScriptEscape="true"/>',
				narrativeDescriptionEnd:  '<s:message code="person.address.move.narrativeDescriptionEnd" javaScriptEscape="true"/>',
				residentsCloseAddSuccessMessage: '<s:message code="person.address.move.success.message" javaScriptEscape="true"/>',
				endPersonAddressSuccessMessage:'<s:message code="person.address.end.success.message" javaScriptEscape="true"/>',
				removePersonAddressHdr: '<s:message code="person.address.remove.header" javaScriptEscape="true"/>',
				removeAddressConfirm: '<s:message code="person.address.remove.removeConfirm" javaScriptEscape="true"/>',
				removeAddressSuccessMessage: '<s:message code="person.address.remove.success.message" javaScriptEscape="true"/>',
				usage:'<s:message code="person.address.add.usage" javaScriptEscape="true"/>',
				type:'<s:message code="person.address.add.type" javaScriptEscape="true"/>',
				roomDescription:'<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>',
				floorDescription:'<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
				location:'<s:message code="person.address.add.location" javaScriptEscape="true"/>',
				currentLocation:'<s:message code="person.address.current.location" javaScriptEscape="true"/>',
				addLocation: '<s:message code="childlookedafter.dialog.add.addLocation" javaScriptEscape="true"/>',
				postcode: '<s:message code="childlookedafter.dialog.add.postcode" javaScriptEscape="true"/>',
				houseNameOrNumber: '<s:message code="childlookedafter.dialog.add.houseNameOrNumber" javaScriptEscape="true"/>',
				overMaxResults: '<s:message code="location.overMaxResults" javaScriptEscape="true" />',
				addressNote: '<s:message code="location.addressNote" javaScriptEscape="true" />',
				addALocation: '<s:message code="location.addALocation" javaScriptEscape="true" />',
				noResults: '<s:message code="location.noResults" javaScriptEscape="true" />',
				locationMandatory: '<s:message code="AtLeastOneNotNull.newPersonAddressVO.locationId" javaScriptEscape="true" />',
				addressStartDate:'<s:message code="person.address.add.startDate" javaScriptEscape="true"/>',
				addressEndDate:'<s:message code="person.address.end.endDate" javaScriptEscape="true"/>',
				endReason:'<s:message code="person.address.end.endReason" javaScriptEscape="true"/>',
				endAddressEndConfirm:'<s:message code="person.address.end.endConfirm" javaScriptEscape="true"/>',
				primaryNameOrNumber: '<s:message code="location.primaryNameOrNumber" javaScriptEscape="true"/>',
				street:'<s:message code="location.street" javaScriptEscape="true"/>',
				locality:'<s:message code="location.locality" javaScriptEscape="true"/>',
				town: '<s:message code="location.town" javaScriptEscape="true"/>',
				county: '<s:message code="location.county" javaScriptEscape="true"/>',
				postcode: '<s:message code="location.postcode" javaScriptEscape="true"/>',
				phoneNumber:'<s:message code="person.contactPanel.phoneNumber"/>',
				mobileNumber:'<s:message code="person.contactPanel.mobileNumber"/>',
				website:'<s:message code="person.contactPanel.website"/>',
				email:'<s:message code="person.contactPanel.email"/>',
				socialMedia:'<s:message code="person.contactPanel.socialMedia"/>',
				fax:'<s:message code="person.contactPanel.fax"/>',
				editContactHeader: '<s:message code="person.contact.edit.header" javaScriptEscape="true"/>',
				editContactNarrative: '<s:message code="person.contact.edit.narrative" javaScriptEscape="true"/>',
				contactSingleEditSuccessMessage:'<s:message code="person.contact.single.edit.success.message" javaScriptEscape="true"/>',
				contactSingleRemoveSuccessMessage:'<s:message code="person.contact.single.remove.success.message" javaScriptEscape="true"/>',
				contactMultipleEditSuccessMessage:'<s:message code="person.contact.multiple.edit.success.message" javaScriptEscape="true"/>',
				contactMultipleRemoveSuccessMessage:'<s:message code="person.contact.multiple.remove.success.message" javaScriptEscape="true"/>',
				contactMultipleEditRemoveSuccessMessage:'<s:message code="person.contact.multiple.edit.remove.success.message" javaScriptEscape="true"/>',
				contactFixedType: '<s:message code="person.contact.type" javaScriptEscape="true"/>',
				contactType: '<s:message code="person.contact.contactType" javaScriptEscape="true"/>',
				contactUsage: '<s:message code="person.contact.contactUsage" javaScriptEscape="true"/>',
				socialMediaType: '<s:message code="person.contact.socialMediaType" javaScriptEscape="true"/>',
				telephoneType: '<s:message code="person.contact.telephoneType" javaScriptEscape="true"/>',
				contactVerified: '<s:message code="person.contact.contactVerified" javaScriptEscape="true"/>',
				contactVerifiedDate: '<s:message code="person.contact.contactVerifiedDate" javaScriptEscape="true"/>',
				contactNumber: '<s:message code="person.contact.number" javaScriptEscape="true"/>',
				contactIdentifier: '<s:message code="person.contact.identifier" javaScriptEscape="true"/>',
				contactDetail: '<s:message code="person.contact.detail" javaScriptEscape="true"/>',
				addContactNarrative: '<s:message code="person.contact.add.narrative" javaScriptEscape="true"/>',
				addContactHeader: '<s:message code="person.contact.add.header" javaScriptEscape="true"/>',
				addContactSuccessMessage : '<s:message code="person.contact.add.successmessage" javaScriptEscape="true"/>',
				dateValidationErrorMessage : '<s:message code="form.date.invalid.message" javaScriptEscape="true"/>',
				dateOfBirthValidationErrorMessage : '<s:message code="person.addeditperson.dateofbirth.invalidMessage" javaScriptEscape="true"/>',
				diedDateValidationErrorMessage : '<s:message code="person.addeditperson.diedDate.invalidMessage" javaScriptEscape="true"/>',
				dueDateValidationErrorMessage : '<s:message code="person.addeditperson.duedate.invalidMessage" javaScriptEscape="true"/>',
				notCarriedToTermDateValidationErrorMessage : '<s:message code="person.addeditperson.notCarriedToTermDate.invalidMessage" javaScriptEscape="true"/>',
				viewPersonMessage: '<s:message code="person.viewPersonMessage" javaScriptEscape="true"/>',
				personRoleSelectionRequired: '<s:message code="person.addeditperson.persontype.requiredMessage" javaScriptEscape="true"/>',
				organisation: '<s:message code="person.professional.organisation" javaScriptEscape="true"/>',
				professionalTitle: '<s:message code="person.professional.title" javaScriptEscape="true"/>',
				addRolesNarrative:'<s:message code="person.role.add.summary" javaScriptEscape="true"/>',
				addPersonRolesNarrative:'<s:message code="person.add.roleSelect.summary" javaScriptEscape="true"/>',
				removeDeceasedPersonSummary:'<s:message code="person.change.deceasedtoalive.summary" javaScriptEscape="true"/>',
				removePersonPictureHeader: '<s:message code="person.remove.picture.header" javaScriptEscape="true"/>',
				removePersonPictureSummary: '<s:message code="person.remove.picture.summary" javaScriptEscape="true"/>',
				removePersonPictureConfirmation: '<s:message code="person.remove.picture.confirmation" javaScriptEscape="true"/>',
				removePersonPictureSuccess: '<s:message code="person.remove.picture.success" javaScriptEscape="true"/>',
				editPersonPictureHeader: '<s:message code="person.edit.picture.header" javaScriptEscape="true"/>',
				editPersonPictureSummary: '<s:message code="person.edit.picture.summary" javaScriptEscape="true"/>',
				editPersonPictureSuccess: '<s:message code="person.edit.picture.success" javaScriptEscape="true"/>',
				personRoleProspective: '<s:message code="person.role.prospective" />',
				locationUnknown: '<s:message code="person.address.unknownLocation" javaScriptEscape="true" />',
				validationLabels: {
						forenameMandatory: '<s:message code="person.validation.mandatory.forename" />',
						surnameMandatory: '<s:message code="person.validation.mandatory.surname" />',
						ethnicityMandatory: '<s:message code="person.validation.mandatory.ethnicity" />',
						genderMandatory: '<s:message code="person.validation.mandatory.gender" />',
						dobMandatory: '<s:message code="person.validation.mandatory.dob" />',
						organisationNameMandatory: '<s:message code="person.validation.mandatory.organisationName" />',
						professionalTitleMandatory: '<s:message code="person.validation.mandatory.professionalTitle" />',
						dueDateMandatory: '<s:message code="person.validation.mandatory.dueDate" />',
						nhsNumberInvalid: '<s:message code="person.validation.invalid.nhsNumber" />',
						invalidDob: '<s:message code="person.validation.invalidDob" />',
						typeMandatory: '<s:message code="personReferenceNumber.validation.mandatory.type" />',
						referenceNumberMandatory: '<s:message code="personReferenceNumber.validation.mandatory.referenceNumber" />'
				},
				potentialDuplicates: '<s:message code="person.potentialDuplicates" javaScriptEscape="true" />'
      	};

		var urls = {
				personNameUrl: '<s:url value="/rest/personName/{id}"/>',
				personAddressUrl: '<s:url value="/rest/personAddress/{id}"/>',
				reopenUrl: '<s:url value="/rest/personAddress/{id}/reOpen"/>',
				updateStartEndUrl: '<s:url value="/rest/personAddress/{id}/startAndEndDate"/>',
				locationUrl: '<s:url value="/rest/location"/>',
				locationQueryParamsUrl: '<s:url value="?nameOrNumber={nameOrNumber}&postcode={postcode}&pageSize={pageSize}"/>',
				updateAddressUrl: '<s:url value="/rest/personAddress/{id}?allowOverlappingAddresses=false"/>'
		};

		var locationConfig = {
				views: {
						add: {
								header: {
										text: '<s:message code="childlookedafter.dialog.add.location.header" javaScriptEscape="true"/>',
										icon: 'fa fa-envelope fa-inverse fa-stack-1x'
								},
								narrative: {
										summary: '<s:message code="childlookedafter.dialog.add.location.narrative" javaScriptEscape="true" />',
										narrative: this.subjectNarrativeDescription
								},
								successMessage: '<s:message code="childlookedafter.dialog.add.location.successMessage" javaScriptEscape="true" />',
								labels: {
										primaryNameOrNumber: '<s:message code="location.primaryNameOrNumber" javaScriptEscape="true" />',
										street: '<s:message code="location.street" javaScriptEscape="true" />',
										locality: '<s:message code="location.locality" javaScriptEscape="true" />',
										town: '<s:message code="location.town" javaScriptEscape="true" />',
										county: '<s:message code="location.county" javaScriptEscape="true" />',
										country: '<s:message code="location.country" javaScriptEscape="true" />',
										countrySubdivision: '<s:message code="location.countrySubdivision" javaScriptEscape="true" />',
										postcode: '<s:message code="location.postcode" javaScriptEscape="true" />',
								},
								config: {
										url: '<s:url value="/rest/location"/>'
								}
						}
				}
		};

	 	/**
		*  Actions that can be performed on a person
		*/		
		
		var addPersonAddressSubmit = function(e) {
			  var view = this.get('activeView');
			  var model = view.get('model');
			  var _instance = this;
			  var residentStartDate;
			  var self = this;
			  e.preventDefault();
			  
			  // checking if the fuzzy date is valid
			  if(!view.fuzzyStartDate.isValid()) {				  
				  var e={};
                  e.code=400;
                  e.msg={                       
                       validationErrors:{startDate: commonLabels.dateValidationErrorMessage}
                  }
                  view.fire('error',{error:e});
                  
                  // add the link to the year field
                  
                  var $startDateValidation = _instance.bodyNode.one('.message-error a[href="#startDate"]'),
                      $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
				  if($startDateValidation){
						view.getWrapper('fuzzyStartDate').addClass('error');
						view.getWrapper('fuzzyStartDate').removeClass('focus');
						$startDateValidation.on('click', function(e) {
							view.getWrapper('fuzzyStartDate').addClass('focus');
							_instance.bodyNode.one('#fuzzyStartYEAR').focus();
            			});
						_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, view.getWrapper('fuzzyStartDate'));
				  }else{
					 view.getWrapper('fuzzyStartDate').removeClass('error');
   				  }
				  return;
			  }
			  
			  // prevent duplicate save on double click
			  //show the mask
			  this.showDialogMask();

			  //set non model-form-link model properties
			  model.setAttrs({
				  personId:   view.get('targetPersonId'),
				  locationId: view.get('locationId'),
				  startDate:  view.getStartDate()
			  }, { silent: true });
			  
			  model.save(function(err, res) {
				  var responseData = "";
				  //hide the mask -	prevent duplicate save on double click 
			      _instance.hideDialogMask();
				  if(err) {
					  
					 //manully add validation indictor
					 responseData = Y.JSON.parse(res.responseText);
					 if(responseData.validationErrors.locationId){
	                   view.getInput('postcodeSearch').addClass('error');
	                   Y.one('a[href="#locationId"]').on('click', function(e) {
	         			    view.getInput('postcodeSearch').focus();
	      		           });
	                  }
					 var $startDateValidation = _instance.bodyNode.one('.message-error a[href="#startDate"]'),
					     $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
					 if($startDateValidation){
							view.getWrapper('fuzzyStartDate').addClass('error');
							view.getWrapper('fuzzyStartDate').removeClass('focus');
							$startDateValidation.on('click', function(e) {
								view.getWrapper('fuzzyStartDate').addClass('focus');
								_instance.bodyNode.one('#fuzzyStartYEAR').focus();
	            			});
							var $clearTheErrorOnStartDate = _instance.bodyNode.one('.message-error a.close');
							_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, view.getWrapper('fuzzyStartDate'));
							
					 }else{
						 view.getWrapper('fuzzyStartDate').removeClass('error');
	   				 }
					 // log any other untrapped errors
					 if(err.code !== 400){
						    Y.log(err.code + err.msg);
					 }
					  
				  } else {
					  // resident personAddress processing below
					  if ((view.get('sourceViewName') == 'movePersonAddress')&& (model.get('type') == 'HOME')
							  && (model.get('usage') == 'PERMANENT')) {
						  
						  self.modelStartDate = model.get('startDate');
						  Y.io(res.getResponseHeader('Location'), {	
								
		                        headers: {
		                            'Accept': 'application/json',
		                        },
		                        on: {
		                            success: function(tx, r) {
		                                var parsedResponse = {};
		                                var _instance = this;
		                                try {
		                                    _instance.residentStartDate = Y.JSON.parse(r.responseText).startDate.calculatedDate;
		                                  
		            					  	var checkResidentModel = new Y.usp.person.UpdateCloseAddPersonAddresses({			
		                        				url: L.sub('<s:url value="/rest/person/{personId}/personRelationship/sharedLocation?locationId={locationId}&usage=PERMANENT&type=HOME"/>',{personId:view.get('targetPersonId'), locationId: view.get('endPersonAddressDetails').endLocationId})              						
		                        			});
		                        			checkResidentModel.load(function(err, res){
		                        				if ((err===null) && ((Y.JSON.parse(res.responseText).length) > 0)){
		            					  		  	// residents exists display dialog
			                        				  personDialog.hide();
			  		                                  Y.fire('person:addressChanged');
			  		            					  Y.fire('infoMessage:message', {message:commonLabels.addPersonAddressSuccessMessage});
		  		            					
		    	        					    	  Y.fire('person:showDialog',{
		        	    									action:'moveResidentPersonAddress',
		            										locationId: view.get('locationId'),
		            										startDate:  _instance.residentStartDate,
		            										startDateSFD: self.modelStartDate,
		            										endPersonAddressDetails: view.get('endPersonAddressDetails'),
		            										targetPersonId: view.get('targetPersonId')
		            								 });
		                        				 } else {  // "HOME" and "PERMANENT" but no residents to process
		                   						  	personDialog.hide();
			                   						  Y.fire('person:addressChanged');
			                   						  Y.fire('infoMessage:message', {message:commonLabels.addPersonAddressSuccessMessage});
			                   				    }
		                             	});
		                             } catch (e) {
		                                    return;
		                             }
		                          }
		                        }
		                   
							});
					  } else { // not "HOME" and "PERMANENT" so no resident personAddress processing
						  
						  personDialog.hide();
						  Y.fire('person:addressChanged');
						  Y.fire('infoMessage:message', {message:commonLabels.addPersonAddressSuccessMessage});
					  }
				  }
			  });
    	};
    	var reopenPersonAddressSubmit = function (e) {
            var view = this.get('activeView'),
                model = view.get('model'),
                _instance = this;
		  	 e.preventDefault();
		  	this.showDialogMask();
            
			model.url = view.get('reopenUrl');
			// The model here is not actually required by the API but we use the model.save function to invoke the API.
			model.save({
				transmogrify:Y.usp.address.UpdateEndAddress
    		},function(err, response){
	        	_instance.hideDialogMask();
                if (err===null){
                	personDialog.hide();
					Y.fire('person:addressChanged');
					Y.fire('infoMessage:message', {message:commonLabels.reopenPersonAddressSuccessMessage});
                }                                       
        	});
			
        };
        
        var updatePersonAddressLocationSubmit = function (e) {
            var view = this.get('activeView'),
                model = view.get('model'),
                _instance = this,
                roomDescription = view.get('container').one('#roomDescription-input').get('value'),
                floorDescription = view.get('container').one('#floorDescription-input').get('value'),
                newLocationId = view.placementAddressView.get('selectedLocation'),
                personAddressId = view.get('id'),
                currentLocationId = model.get('location').id;
            
		  	 e.preventDefault();
		  	this.showDialogMask();
		  	
		  	model.setAttrs({
		  		roomDescription: roomDescription,
		  		floorDescription:  floorDescription,
		  		locationId: newLocationId
		  	},{
		  		silent:true
		  	});
            
			model.url = view.get('urls').updateAddressUrl;
			model.save({
				transmogrify:Y.usp.person.UpdatePersonAddress
    		},function(err, response){
	        	_instance.hideDialogMask();
                if (err===null){
                	var checkResidentModel = new Y.usp.relationship.PersonAddressRelationshipName({
        				url: L.sub('<s:url value="/rest/personPersonRelationship/sharedLocationResidents?personAddressId={personAddressId}&locationId={locationId}&onlyRelatedMembers=false&personId={personId}&usage=PERMANENT&type=HOME"/>',
        						{personId: view.get('targetPersonId'), personAddressId: personAddressId, locationId: currentLocationId})
					});
					
					checkResidentModel.load(function(err, res){
						var responseText = Y.JSON.parse(res.responseText),
						    preSelectedRecords;
						
						if ((err===null) && ((responseText.totalSize) > 0)){
				  		  	// residents exists display dialog
            				personDialog.hide();
				  		  	Y.fire('person:addressChanged');
				  		    Y.fire('infoMessage:message', {message:commonLabels.updatePersonAddressLocationSuccessMessage});
				  		  
				  		    // get all records which are going to be preselected
				  		    preSelectedRecords = responseText.results.filter(function(record) {
				  			return record.relationshipName !== 'No relation'});
				  		    
				  		    Y.fire('person:showDialog',{
				  		    	action:'moveResidentPersonAddressLocation',
				  		    	newLocationId: newLocationId,
				  		    	currentLocationId: currentLocationId,
				  		    	preSelectedRecords: preSelectedRecords,
				  		    	personAddressId: view.get('id'),
								targetPersonId: view.get('targetPersonId')
							 }); 
        				 } else {  // "HOME" and "PERMANENT" but no residents to process
   						  	personDialog.hide();
       						  Y.fire('person:addressChanged');
       						  Y.fire('infoMessage:message', {message:commonLabels.addPersonAddressSuccessMessage});
       				    }
             	});
                }                                       
        	});
			
        };
        
        var updatePersonAddressDatesSubmit = function (e) {
            var view = this.get('activeView'),
                model = view.get('model'),
                _instance = this;
		  	 e.preventDefault();
		  	this.showDialogMask();
    
            model.setAttrs({
            	startDate: view.getStartDate(),
            	endDate:  view.getEndDate()
            },{
            	silent:true
            });
            
			model.url = view.get('updateStartEndUrl');
			model.save({
				transmogrify:Y.usp.person.UpdatePersonAddressStartEnd
    		},function(err, response){
	        	_instance.hideDialogMask();
                if (err===null){
					  		personDialog.hide();
					  		Y.fire('person:addressChanged');
					  		Y.fire('infoMessage:message', {message:commonLabels.updatePersonAddressDatesSuccessMessage});
                }                                       
        	});
			
        };	
	    
		var endPersonAddressSubmit = function(e) {
			
			// Get the view and the model
            var view=this.get('activeView'),
            model=view.get('model');  
		  	var _instance = this;
		  	 e.preventDefault();
		  	 
		   // checking if the fuzzy date is valid
		   if(!view.fuzzyEndDate.isValid()){				  
			  var e={};
               e.code=400;
               e.msg={                   
                    validationErrors:{endDate: commonLabels.dateValidationErrorMessage}
               }
               view.fire('error',{error:e});
               
               // add the link to the year field
               
               var $endDateValidation = _instance.bodyNode.one('.message-error a[href="#endDate"]'),
                   $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
			   if($endDateValidation){
					view.getWrapper('fuzzyEndDate').addClass('error');
					view.getWrapper('fuzzyEndDate').removeClass('focus');
					$endDateValidation.on('click', function(e) {
						view.getWrapper('fuzzyEndDate').addClass('focus');
						_instance.bodyNode.one('#fuzzyEndYEAR').focus();
         			});
					_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, view.getWrapper('fuzzyEndDate'));
			  }else{
				 view.getWrapper('fuzzyEndDate').removeClass('error');
				  }
			  return;
		    }
			
		// prevent duplicate save on double click 
		   //show the mask
		  	this.showDialogMask();
            model.setAttrs({
            	endDate:  view.getEndDate()
            },{
        		//don't trigger the onchange listener as we will re-render after save
        		silent:true
        	});
           
            model.save({
        			transmogrify:Y.usp.person.UpdateClosePersonAddress
        		},function(err, response){
        			//hide the mask -	prevent duplicate save on double click 
		        	_instance.hideDialogMask();
        			
		        	 var $endDateValidation = _instance.bodyNode.one('.message-error a[href="#endDate"]'),
		        	     $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
					 if($endDateValidation){
							view.getWrapper('fuzzyEndDate').addClass('error');
							view.getWrapper('fuzzyEndDate').removeClass('focus');
							$endDateValidation.on('click', function(e) {
								view.getWrapper('fuzzyEndDate').addClass('focus');
								_instance.bodyNode.one('#fuzzyEndYEAR').focus();
	            			});
							_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, view.getWrapper('fuzzyEndDate'));
					 }else{
						 view.getWrapper('fuzzyEndDate').removeClass('error');
	   				 }
        			
                    if (err===null){
                    	if ((view.get('sourceViewName') == 'movePersonAddress')&& (view.get('model').get('type') == 'HOME')
									&& (view.get('model').get('usage') == 'PERMANENT')) {
                    		
                    		// resident personAddress processing
  					  			var residentEndDate;
                    			var url = '<s:url value="/rest/address/{id}" />';
                    			url = L.sub(url,{id: view.get('model').get('id')});
                    			
                           		Y.io(url, {
  					  				headers: {
                        	  			'Content-Type': 'application/json'
                          			}, 
                          			on: {
                        	  			complete: function(tx, r , arg) {
                                  			var parsedResponse = {};
                                  			try {
                                      		 	this.residentEndDate = Y.JSON.parse(r.responseText).endDate.calculatedDate;
                                      		
                  					  		 	personDialog.hide();
                  					  		 	Y.fire('person:addressChanged');
                  					  		 	Y.fire('infoMessage:message', {message:commonLabels.endPersonAddressSuccessMessage});
                                      
            								 	Y.fire('person:showDialog',{
            										action:'addMovePersonAddress',
            										targetPerson:{
            											id: view.get('targetPersonId'),
            											name : view.get('targetPersonName')
            											},
            										endLocationId: view.get('model').get('location').id,
            										endReason: view.get('model').get('endReason'),          									
          											endDateSDF: view.get('model').get('endDate'),
          											endDate: this.residentEndDate,
          											addressType: view.get('model').get('type'),
          			                    			addressUsage: view.get('model').get('usage'),
          			                    			sourceViewName: view.get('sourceViewName'),
          			                    			addressId: view.get('model').get('id'),
          			                    			objectVersion: view.get('model').get('objectVersion')
            									});
  									
                                  			} catch (e) {
                                      			return;
                                  			}
                              	    	}
                          			}
                      		});
  					  
                    	}else {  // not resident processing
  					  		personDialog.hide();
  					  		Y.fire('person:addressChanged');
  					  		Y.fire('infoMessage:message', {message:commonLabels.endPersonAddressSuccessMessage});
                    	}							
                    }                                       
            	});
        	};
		var removePersonAddressSubmit = function(e) {
            // Get the view and the model
            var view=this.get('activeView'),
            model=view.get('model');     
            
            e.preventDefault();
           
            model.destroy({remove:true}, 
            		function(err, response){
                    if (err===null){
						personDialog.hide();
						Y.fire('person:addressChanged');
   					 	Y.fire('infoMessage:message', {message:commonLabels.removeAddressSuccessMessage});   	  				
                    }                                       
            	});
        	};	
        	
		// save button action
		  var addPersonReferenceNumberSubmit = function(e){
			
			  	// prevent duplicate save on double click
			  	var _instance = this;
			    e.preventDefault(); 
				//show the mask
				this.showDialogMask();
				
		        // Get the view and the model
		        var view=this.get('activeView'),
		        	model=view.get('model');		        			        	
		            
		        model.setAttrs({
		        	
		    	},{
		    		//don't trigger the onchange listener as we will re-render after save
		    		silent:true
		    	});		        

		     	// Do the save   
		        model.save(function(err, res){
		        		//hide the mask -	prevent duplicate save on double click 
		        		_instance.hideDialogMask();	
		        		if (err){
							if(err.code !== 400){
								Y.log(err.code + err.msg);
							}
		        		}else{
		        			personDialog.hide();
		  					Y.fire('person:personDetailChanged');
		  					Y.fire('infoMessage:message', {message:commonLabels.referenceNumberAddSuccessMessage});
		        	}
		        });
		     
			};			
	
				var updatePersonLanguageSubmit = function(e){	
					// prevent duplicate save on double click
					var _instance = this;
					e.preventDefault(); 
					//show the mask
					this.showDialogMask();
					
					// Get the view and the model
					var view=this.get('activeView'),
					model=view.get('model');
					
					// Set attributes in update model
					updateModel=new UpdatePersonLanguageForm();
					updateModel.setAttrs({
						_type:'UpdatePersonLanguage',
						primaryLanguageId:view.getSelectedFirstLanguageCode(),
						otherLanguageIds:view.getSelectedOtherLanguageCodes(),
						nonVerbalLanguageIds:view.getSelectedNonVerbalLanguageIds(),
						//dateOfBirth:fuzzyDateJSON
						id:model.get('id'),
						objectVersion:model.get('objectVersion')
					},{
		        		//don't trigger the onchange listener as we will re-render after save
		        		silent:true
					});
					updateModel.addTarget(personDialog);
					updateModel.save(function(err, res){
						//hide the mask -	prevent duplicate save on double click 
						_instance.hideDialogMask();	
						
						if(err != null){
							// do something on error?
						}else{
							personDialog.hide();
							Y.fire('person:personDetailChanged');
							var message;
							if (model.get('personLanguageAssignmentVOs').length){
								message=commonLabels.languageUpdateSuccessMessage;
							} else {
								message=commonLabels.languageAddSuccessMessage;
							}
							Y.fire('infoMessage:message', {message:message});
						}
					});
				};			
			
		var editRemovePersonReferenceNumberSubmit = function(e) {  
			referenceNumberEdited=0, 
			referenceNumberDeleted=0;
			// prevent duplicate save on double click
		  	var _instance = this;
			e.preventDefault();
			this.showDialogMask();		//show the mask
			
			var deleteRefNums = [], updateRefNums = [], model, view,referenceNumInput,removeReferenceNum;
			model = this.get('activeView').get('model').toJSON();
			view = this.get('activeView').get('container');
						
			model.forEach(function(referenceNumber){
				referenceNumInput = view.getById('referenceNumber_'+referenceNumber.id);
				removeReferenceNum = referenceNumInput.getAttribute('data-remove-num');
				if(removeReferenceNum){
					// add reference numbers to delete to the array
					deleteRefNums.push(referenceNumber.id);
					referenceNumberDeleted = referenceNumberDeleted+1;
				}
				else{
					currentValue = referenceNumInput.get('value');
					if(currentValue!==referenceNumber.referenceNumber){
						referenceNumberEdited = referenceNumberEdited+1;
					}
						updateReferenceNumber = {"referenceNumber":currentValue,"id":referenceNumber.id,"startDate":referenceNumber.startDate,"objectVersion":referenceNumber.objectVersion,"setId":true ,"_type":"UpdatePersonReferenceNumber" };
						updateRefNums.push(updateReferenceNumber);
				}
			});
			
			updateRemoveReferenceNumberModel = new UpdateRemoveReferenceNumberModel(
				{
					"_type":"UpdateDeletePersonReferenceNumbers",
					"objectVersion":0,
					"updateReferenceNumberVOs":updateRefNums,
					"deletePersonReferenceNumberIds":deleteRefNums
				}
			);
			
			// Adding this model to the personDialog targes allows us to use the multipanel
			// error message
			updateRemoveReferenceNumberModel.addTarget(personDialog);
			
			updateRemoveReferenceNumberModel.save(function(err, res){
				//hide the mask -	prevent duplicate save on double click 
	        	_instance.hideDialogMask();
				if (err){
					if(err.code !== 400){
						Y.log(err.code + err.msg);
					}
        		}else{
        			personDialog.hide();
  					Y.fire('person:personDetailChanged');
        			//Show success message, even if no changes have been made, consistent with rest of app
        			if(referenceNumberEdited<2 && referenceNumberDeleted===0){
        				Y.fire('infoMessage:message', {message:commonLabels.referenceNumberSingleEditSuccessMessage});	
    				}
        			//Multiple edits only
        			else if(referenceNumberEdited>1 && referenceNumberDeleted===0){
        				Y.fire('infoMessage:message', {message:commonLabels.referenceNumberMultipleEditSuccessMessage});		
        			}
        			//Single remove only
        			else if(referenceNumberEdited===0 && referenceNumberDeleted===1){
        				Y.fire('infoMessage:message', {message:commonLabels.referenceNumberSingleRemoveSuccessMessage});	
        			}
        			//Multiple remove only
        			else if(referenceNumberEdited===0 && referenceNumberDeleted>1){
        				Y.fire('infoMessage:message', {message:commonLabels.referenceNumberMultipleRemoveSuccessMessage});	
        			}
        			// Edit(s) and remove(s)
        			else if(referenceNumberEdited>0 && referenceNumberDeleted>0){
        				Y.fire('infoMessage:message', {message:commonLabels.referenceNumberMultipleEditRemoveSuccessMessage});	
    				}
        	}
        });
		};
		
		// Array to store contact ids and its corresponding panel labels
		var panelLabels = [[]];
		
		 var editRemoveContactSubmit = function(e) {

				// prevent duplicate save on double click
			  	var _instance = this;		 
				e.preventDefault();       
				this.showDialogMask();		//show the mask
				
			   	contactEdited=0, 
				contactDeleted=0;
				
				var deleteContNums = [], updateContNums = [], model, view, contactInput, removeContact, eContacts, wContacts, updateContact, subType;
								
				
				model = this.get('activeView').get('model').toJSON();
				view = this.get('activeView').get('container');	
				
				// get sub entities			
				eContacts = this.get('activeView').get('emailContacts');
				wContacts = this.get('activeView').get('webContacts');
				
				model.forEach(function(contact)
				    {	
					    contactInput = view.getById('contact_'+contact.id);
						removeContact = contactInput.getAttribute('data-remove-num');					
						var panelLabel=[];
							
						if(removeContact){
							// add contacts to delete to the array
							deleteContNums.push(contact.id);
							contactDeleted = contactDeleted + 1;
						}
						else {
							currentValue = contactInput.get('value');						
							isVerified = view.getById('contactVerified-input_'+contact.id).get('checked');
							if (isVerified) {
								verifiedDate = view.getById('contactVerifiedDate-input_'+contact.id).get('value');
								verifiedDate = Y.USPDate.parseDate(verifiedDate);
							} else {
								verifiedDate = null;
							}
							if(contact.number){
					 			if(currentValue!==contact.number){
					 				contactEdited = contactEdited + 1;
								}
				 				subType='UpdateTelephoneContact';
				 				if(contact.telephoneType == 'LANDLINE') {
				 					panelLabel = [contact.id,"Phone number"];
				 				} else if(contact.telephoneType == 'MOBILE') {
				 					panelLabel = [contact.id,"Mobile number"];
				 				} else if(contact.telephoneType == 'FAX') {
				 					panelLabel = [contact.id,"Fax number"];
				 				}				 								 				
				 				panelLabels.push(panelLabel);
				 				
								updateContact = {"number":currentValue,"verified":isVerified,"verifiedDate":verifiedDate,"id":contact.id,"objectVersion":contact.objectVersion,"_type":subType};
					  		}else if(contact.identifier){
					 			if(currentValue!==contact.identifier){
					 				contactEdited = contactEdited + 1;
					 			}
				 				subType='UpdateSocialMediaContact';
				 				
				 				panelLabel = [contact.id,"Social media"];
				 				panelLabels.push(panelLabel);
				 				
				 				updateContact = {"identifier":currentValue,"verified":isVerified,"verifiedDate":verifiedDate,"id":contact.id,"objectVersion":contact.objectVersion,"_type":subType};
							} else if (contact.address) {
								 if(currentValue!==contact.address){
									contactEdited = contactEdited + 1;
								 }	
									//Check for subTypes
								 	for(var i=0; i<eContacts.length; i++){
										if(eContacts[i].id==contact.id){						
											subType='UpdateEmailContact';											
											break;
										}		
								 	}							 	
									for(var i=0; i<wContacts.length; i++){
										if(wContacts[i].id==contact.id){						
											subType='UpdateWebContact';
											break;
										}
									}
									
					 				if (subType == 'UpdateEmailContact') {
										panelLabel = [contact.id,"Email address"];						 										 			
					 				} else if(subType == 'UpdateWebContact') {
										panelLabel = [contact.id,"Website"];	
					 				}
					 				
					 				panelLabels.push(panelLabel);
					 				updateContact = {"address":currentValue,"verified":isVerified, "verifiedDate":verifiedDate, "id":contact.id,"objectVersion":contact.objectVersion,"_type":subType};
							 }
					 		//store the VO in the large array				 		
					 		updateContNums.push(updateContact);
						}
					});	
				updateRemoveContactModel = new UpdateRemoveContactModel(
					{
						"_type":"UpdateDeletePersonContacts",
						"objectVersion":0,
						"updateContactVOs":updateContNums,
						"deleteContactIds":deleteContNums
					}
				);
				
				// Adding this model to the personDialog targes allows us to use the multipanel error message
				updateRemoveContactModel.addTarget(personDialog);
				
				updateRemoveContactModel.save(function(err, res){
					_instance.hideDialogMask();		  				//hide the mask -	prevent duplicat save on double click 
	        		if (err){
						if(err.code !== 400){
							Y.log(err.code + err.msg);
						}
	        		}else{
	        			personDialog.hide();
	  					Y.fire('person:personDetailChanged');
	  					
	        			 //Show success message, even if no changes have been made, consistent with rest of app
	        			if(contactEdited<2 && contactDeleted===0){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactSingleEditSuccessMessage});	    						
	    				}
	        			//Multiple edits only
	        			else if(contactEdited>1 && contactDeleted===0){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactMultipleEditSuccessMessage});	        						
	        			}
	        			//Single remove only
	        			else if(contactEdited===0 && contactDeleted===1){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactSingleRemoveSuccessMessage});	        						
	        			}
	        			//Multiple remove only
	        			else if(contactEdited===0 && contactDeleted>1){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactMultipleRemoveSuccessMessage});	        						
	        			}
	        			// Edit(s) and remove(s)
	        			else if(contactEdited>0 && contactDeleted>0){
	        				Y.fire('infoMessage:message', {message:commonLabels.contactMultipleEditRemoveSuccessMessage});	    						
	    				}					      		
	               		        		
	        		//hide afterwards
	              	//setTimeout(function(){
	              	//	Y.one('#headerMessage').hide('fadeOut');
	              	//},5000);
	        	}
	        });
			};
					
		var moveResidentPersonAddressSubmit = function(e) {
			var _instance = this;
		    
			e.preventDefault();
		    
		    var view = this.get('activeView'),
		        residentsDetails = view.combinedEndAddPersonAddressResults.get('residentDetails');
		    		    
		    // carry out processing only if records are selected on screen
		    if ((residentsDetails) && (residentsDetails.length) > 0){
		    	this.showDialogMask();
		    	
		    	if (view.get('sourceView') === 'moveResidentPersonAddressLocation') {
		    		var updatePersonAddressVOs =[];
		    		
		    		residentsDetails.forEach(function(residentDetail) {
		    			var updatePersonAddressVO ={};
		    			
		    			updatePersonAddressVO = {"_type": "UpdatePersonAddress",
		    					"id": residentDetail.id,
		    					"objectVersion":residentDetail.objectVersion,
		    					"locationId": view.get('locationId')
		    		    };
		    			updatePersonAddressVOs.push(updatePersonAddressVO);
		    	    });
		    		
		    		updatePersonAddressResidentsModel = new UpdatePersonAddressResidentsModel(
		    			{
		    				"_type": "UpdatePersonAddressResidents",
		    				"updatePersonAddressList": updatePersonAddressVOs,
		    				"id": view.get('oldLocationId')
		    			}
		    	    );
		    		
		    		updatePersonAddressResidentsModel.addTarget(personDialog);
		    		updatePersonAddressResidentsModel.save(function(err,res){		    			
		    			//hide the mask -	prevent duplicat save on double click
		    			_instance.hideDialogMask();
		    			
		    			if (err){
		    				if(err.code !== 400){
		    					Y.log(err.code + err.msg);
							}
		    			}else{
		    				personDialog.hide();
		    				Y.fire('infoMessage:message', {message:_instance.get('activeView').get('labels').residentsCloseAddSuccessMessage});
        				}
					});
		    } else {
		    	var updateClosePersonAddressVOs = [];
		 	
		 		residentsDetails.forEach(function(residentDetail){
		 			var updateClosePersonAddressVO = {};
		 		
		 			updateClosePersonAddressVO = {"_type": "UpdateClosePersonAddress","id":residentDetail.id,"objectVersion":residentDetail.objectVersion,
		 				"endDate": view.get('endPersonAddressDetails').endDateSDF, "endReason" : view.get('endPersonAddressDetails').endReason};
		 			updateClosePersonAddressVOs.push(updateClosePersonAddressVO);
		 		});
		 	
		 		updateCloseAddPersonAddressesModel = new UpdateCloseAddPersonAddressesModel(
					{
						"locationId": view.get('locationId'),
						"newAddressStartDate": view.get('startDateSFD'),
						"updateClosePersonAddressVOs": updateClosePersonAddressVOs,
						"_type": "UpdateCloseAddPersonAddresses", 
						"type": "HOME",
						"usage": "PERMANENT"
					}
		 		);
		 		updateCloseAddPersonAddressesModel.residentsDetails = residentsDetails;
		 		updateCloseAddPersonAddressesModel.endDate = view.get('endPersonAddressDetails').endDate;
		 	
		 		// Adding this model to the personDialog targes allows us to use the multipanel error message
				updateCloseAddPersonAddressesModel.addTarget(personDialog);
		 	
				updateCloseAddPersonAddressesModel.save(function(err,res){
					//hide the mask -	prevent duplicat save on double click 
					_instance.hideDialogMask();
				
					if (err){
						if(err.code !== 400){
							Y.log(err.code + err.msg);
						}
        			}else{
        				personDialog.hide(); 
        				Y.fire('infoMessage:message', {message:_instance.get('activeView').get('labels').residentsCloseAddSuccessMessage});
        			}
				}); 
		    }	
		   }			
		};
	
		
			
		// Cancel button action
		var cancelDialog = function(e) {
	   		e.preventDefault();
	   		personDialog.hide();
  		};
       
        var addPersonContactSubmit = function(e){
            var _instance = this;
            var view = this.get('activeView');
            var container = view.get('container');
            var contactFixedType = view.get('fixedContactType');
            var contactType = container.one('#contactType-input').get('value');
            var contactUsage = container.one('#contactUsage-input').get('value');
            var contactDetail = container.one('#contactDetail-input').get('value');
            var contactNumber = container.one('#number-input').get('value');
            var contactIdentifier = container.one('#identifier-input').get('value');
            var isVerified = container.one('#contactVerified-input').get('checked');
            var contactVerifiedDate = container.one('#contactVerifiedDate-input').get('value');
            var verifiedDateAsNumber = Y.USPDate.parseDate(contactVerifiedDate);
            e.preventDefault();

            _instance.showDialogMask();    //show the mask

            switch(contactFixedType) {
            case 'EMAIL':
            newPersonContactModel = new NewPersonContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(),
                  "preferred" : "false", "address" : contactDetail, "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber, 
                  "_type": 'NewEmailContact'
                }
            );  
                break;
            case 'SOCIALMEDIA':
            var socialMediaType = view.get('container').one('#socialMediaType-input').get('value');
            newPersonContactModel = new NewPersonContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(), 
                  "preferred" : "false", "socialMediaType" : socialMediaType , "identifier" : contactIdentifier,
                  "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber, "_type": 'NewSocialMediaContact'
                }
            );
                break;
            case 'TELEPHONE':
            var telephoneType = view.get('container').one('#telephoneType-input').get('value');
            newPersonContactModel = new NewPersonContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(),
                  "preferred" : "false", "telephoneType" : telephoneType, "number" : contactNumber,             
                  "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber, "_type": 'NewTelephoneContact'
                }
            );
                break;
            case 'WEB':
            newPersonContactModel = new NewPersonContactModel(
                { "contactType" : contactType, "contactUsage" : contactUsage, "startDate" : new Date(),
                  "preferred" : "false", "address" : contactDetail, "verified" : isVerified, "verifiedDate" : verifiedDateAsNumber,
                  "_type": 'NewWebContact'
                }
            );            
                break;
            default:
              break ;
              }     
          
                     

            // Add this model to the personDialog target allows us to use the multipanel error message
            newPersonContactModel.addTarget(personDialog);
          
            // Do the save   
            newPersonContactModel.save(function(err, res){              
             
            	_instance.hideDialogMask(); //hide the mask - prevent duplicat save on double click 
                  
                  if(err) {
                	  
                    if(err.code !== 400){
                      Y.log(err.code + err.msg);
                    }
                    
                  } else {
                    personDialog.hide();
                  	Y.fire('person:personDetailChanged');
                  	Y.fire('infoMessage:message', {message:commonLabels.addContactSuccessMessage}); 
                }
              });
           
        };
		var _validateDob = function(view){
			 // checking if the fuzzy date is valid
		    if(!view.fuzzyDOBWidget.isValid()){				  
			  var e={};
               e.code=400;
               e.msg={                       
                    validationErrors:{dateOfBirth: commonLabels.dateOfBirthValidationErrorMessage}
               }
               view.fire('error',{error:e});
               
               // add the link to the year field
               
               var $dobValidation = _instance.bodyNode.one('.message-error a[href="#dateOfBirth"]'),
                   $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
				if($dobValidation){
					view.getWrapper('dateOfBirth').addClass('error');
					view.getWrapper('dateOfBirth').removeClass('focus');
					$dobValidation.on('click', function(e) {
						view.getWrapper('dateOfBirth').addClass('focus');
						_instance.bodyNode.one('#fuzzyDOBYEAR').focus();
           		    });
					_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, view.getWrapper('dateOfBirth'));
				}else{
					view.getWrapper('dateOfBirth').removeClass('error');
				}
			  return;
		    }
  			
  			
		}; 	 
        var changeUnbornSubmit = function(e){

			var _instance = this;
			// Get the view and the model
  	        var view = _instance.get('activeView');
  	        var model = view.get('model');
  	        var outcome = view.get('outcome');
  	        var transmogrify, notCarriedToTermType;
  			
  	        e.preventDefault();
  	        
  	        if(outcome==='BORN') {
  	        	
  	        	transmogrify = Y.usp.person.UpdatePersonBorn;
  	        } else {
  	            transmogrify = Y.usp.person.UpdatePersonNotCarriedToTerm; 
  	            notCarriedToTermType = outcome;

  	        }
  		
  			_validateDob(view);
  			
  			//show the mask
            this.showDialogMask();
  			
         	// Set attrs depending on roles
            var attrs={
            			dateOfBirth: view.getFuzzyDOB(),
            			nhsNumber: view.getNhsNumber(),
            			notCarriedToTermType : notCarriedToTermType
            			
            		  };
		    
        	model.setAttrs(
        		attrs
        	,{
        		//don't trigger the onchange listener as we will re-render after save
        		silent:true
        	});
  			
  			// Save
  			model.save({transmogrify:transmogrify},
  				function(err,response){
  				//hide the mask
		        _instance.hideDialogMask();
  				if(err===null){
  					personDialog.hide();			      			    
  					Y.fire('person:personDetailChanged');
					Y.fire('infoMessage:message', {message:commonLabels.editPersonSuccessMessage});
				}else{
  	  				
  					//manully add validation indictor for NHS number
					var $nhsValidation = _instance.bodyNode.one('.message-error a[href="#nhsNumber"]'); 					
  					if($nhsValidation){
             				view.getInput('nhsFirstThreeDigits').addClass('error');
             				view.getInput('nhsSecondThreeDigits').addClass('error');
             				view.getInput('nhsLastFourDigits').addClass('error');
             				
             				$nhsValidation.on('click', function(e) {
                       			view.getInput('nhsFirstThreeDigits').focus().select();
                    		});
          			}
  					
  					var $dobValidation = _instance.bodyNode.one('.message-error a[href="#dateOfBirth"]'),
  					     $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
   					if($dobValidation){
   						view.getWrapper('dateOfBirth').addClass('error');
   						view.getWrapper('dateOfBirth').removeClass('focus');
   						$dobValidation.on('click', function(e) {
   							view.getWrapper('dateOfBirth').addClass('focus');
   							_instance.bodyNode.one('#fuzzyDOBYEAR').focus();
                 		});
   						_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, view.getWrapper('dateOfBirth'));
   					}else{
   						view.getWrapper('dateOfBirth').removeClass('error');
   					}
  				}
  			});
        };
        
        var changeDeceasedToAliveAdministrativelySubmit = function(e){
        
        	var _instance = this;
			 // Get the view and the model
 	        var view= _instance.get('activeView');
 	        var model=view.get('model');
 	   		e.preventDefault();
 	        model.save({transmogrify:Y.usp.person.UpdatePersonDeceasedToAlive},
 	  				function(err,response){
 	    	  if(err===null){
					personDialog.hide();			      			    
					Y.fire('person:personDetailChanged');
					Y.fire('infoMessage:message', {message:commonLabels.editPersonSuccessMessage});
						}
 	  				
 	  			});
        	
        };
	    var changeAliveSubmit = function(e) {
	          var _instance = this;
	          // Get the view and the model
		      var view=this.get('activeView');
		      var model=view.get('model');
		      var lifeState = UP.GetImpliedLifeState(view.get('lifeState'));
		            e.preventDefault();
	            
	       	  // checking if the fuzzy date is valid
	          if(!view.fuzzyDOBWidget.isValid() || !view.fuzzyDeceasedDOBWidget.isValid()){                        
	              var e={};
			      e.code=400;
			      e.msg={                       
			           validationErrors:{dateOfBirth: commonLabels.dateOfBirthValidationErrorMessage}
			      }
			      view.fire('error',{error:e});
			      
			      // add the link to the year field
			      errorHighlightFuzzyDateWidgets(view,_instance);
			              return;
	         	}
	            
	            //show the mask
	   			this.showDialogMask();
	            
	     		// Set attrs depending on roles
	   			var attrs={}, fuzzyDOB,fuzzyDiedDate;
				fuzzyDOB = view.getFuzzyDeceasedDOB() || {};
				fuzzyDiedDate = view.getFuzzyDeceasedDiedDate() || {};
			    	attrs={
							dateOfBirth: typeof fuzzyDOB.year !== 'undefined' && fuzzyDOB.year !== null ? fuzzyDOB : null,
							diedDate: typeof fuzzyDiedDate.year !== 'undefined' && fuzzyDiedDate.year !== null ? fuzzyDiedDate : null,
							personTypes: view.get('personRoles')
						};
	         
	         	attrs.lifeState = lifeState;
	         
	     		model.setAttrs(
	            attrs
	    			,{
	           		//don't trigger the onchange listener as we will re-render after save
	           		silent:true
	    		 });
	     	
	     		// Save
	            model.save({transmogrify:Y.usp.person.UpdatePersonDeceased},
	                  function(err,response){
	                  //hide the mask
	             		_instance.hideDialogMask();
	                  	if(err===null){
	                         personDialog.hide();
	                         Y.fire('person:personDetailChanged');
	                         Y.fire('infoMessage:message', {message:commonLabels.editPersonSuccessMessage});
	                  	  }else{
	                         errorHighlightFuzzyDateWidgets(view,_instance);
	                  	}
	           	 });
	     };

        
        var changeNotcarriedSubmit = function(e){

			var _instance = this;
			 // Get the view and the model
  	        var view= _instance.get('activeView');
  	        var model=view.get('model');
  	      	var outcome = view.get('outcome');
  			e.preventDefault();
  			
  			_validateDob(view);
  			
  			//show the mask
            this.showDialogMask();
  			var transmogrify;
			if(outcome==='BORN') {
				transmogrify = Y.usp.person.UpdateNotCarriedPersonBorn;
  	        } else if(outcome==='UNBORN'){
  	        	transmogrify = Y.usp.person.UpdateNotCarriedPersonUnborn; 

  	        }
  			
         	// Set attrs depending on roles
            var attrs={
            			dateOfBirth: view.getFuzzyDOB(),
            			nhsNumber: view.getNhsNumber(),
            			outcome: view.get('outcome')
            		  };
			model.setAttrs(
        		attrs
        	,{
        		//don't trigger the onchange listener as we will re-render after save
        		silent:true
        	});
			
  			// Save
  			model.save({transmogrify:transmogrify},
  				function(err,response){
  				//hide the mask
		        _instance.hideDialogMask();
  				if(err===null){
  					personDialog.hide();			      			    
  					Y.fire('person:personDetailChanged');
					Y.fire('infoMessage:message', {message:commonLabels.editPersonSuccessMessage});
				}else{
  	  				
  					//manully add validation indictor for NHS number
					var $nhsValidation = _instance.bodyNode.one('.message-error a[href="#nhsNumber"]'); 					
  					if($nhsValidation){
             				view.getInput('nhsFirstThreeDigits').addClass('error');
             				view.getInput('nhsSecondThreeDigits').addClass('error');
             				view.getInput('nhsLastFourDigits').addClass('error');
             				
             				$nhsValidation.on('click', function(e) {
                       			view.getInput('nhsFirstThreeDigits').focus().select();
                    		});
          			}
  					
  					var $dobValidation = _instance.bodyNode.one('.message-error a[href="#dateOfBirth"]'),
  					     $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
   					if($dobValidation){
   						view.getWrapper('dateOfBirth').addClass('error');
   						view.getWrapper('dateOfBirth').removeClass('focus');
   						$dobValidation.on('click', function(e) {
   							view.getWrapper('dateOfBirth').addClass('focus');
   							_instance.bodyNode.one('#fuzzyDOBYEAR').focus();
                 		});
   						_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, view.getWrapper('dateOfBirth'));
   					}else{
   						view.getWrapper('dateOfBirth').removeClass('error');
   					}
  				}
  			});
        };
        //method for highlighting error messages for dob and died date
        var errorHighlightFuzzyDateWidgets = function(view,_instance){
        	var $dobValidation = _instance.bodyNode.one('.message-error a[href="#dateOfBirth"]'),
            $diedDateValidation = _instance.bodyNode.one('.message-error a[href="#diedDate"]'),
            $clearTheErrorOnDateFields = _instance.bodyNode.one('.message-error a.close');
            
            
				if($dobValidation){
					if(view.getWrapper('dateOfBirth')){
						view.getWrapper('dateOfBirth').addClass('error');
						view.getWrapper('dateOfBirth').removeClass('focus');
						$dobValidation.on('click', function(e) {
							view.getWrapper('dateOfBirth').addClass('focus');
							_instance.bodyNode.one('#fuzzyDOBYEAR').focus();
						});
						//Error border needs to be removed on the filed 
						//once the close is clicked on the error banner
						_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields, 
																		view.getWrapper('dateOfBirth'));
						
					} else if(view.getWrapper('deceased-dateOfBirth')){
						view.getWrapper('deceased-dateOfBirth').addClass('error');
						view.getWrapper('deceased-dateOfBirth').removeClass('focus');
						$dobValidation.on('click', function(e) {
							view.getWrapper('deceased-dateOfBirth').addClass('focus');
							_instance.bodyNode.one('#fuzzyDeceasedDOBYEAR').focus();
							});
						//Error border needs to be removed on the filed 
						//once the close is clicked on the error banner
						_removeErrorHighlightAroundFuzzydateWidget($clearTheErrorOnDateFields,
								 										view.getWrapper('deceased-dateOfBirth'));
					}
					
				} else {
					if(view.getWrapper('dateOfBirth')){
						view.getWrapper('dateOfBirth').removeClass('error');
					} else if(view.getWrapper('deceased-dateOfBirth')){
						view.getWrapper('deceased-dateOfBirth').removeClass('error');
					}
				}
				if( $diedDateValidation){
					if(view.getWrapper('diedDate')){
						view.getWrapper('diedDate').addClass('error');
						view.getWrapper('diedDate').removeClass('focus');
						$diedDateValidation.on('click', function(e) {
							view.getWrapper('diedDate').addClass('focus');
							_instance.bodyNode.one('#fuzzyDiedDateYEAR').focus();});
					} else {
						view.getWrapper('diedDate').removeClass('error');
					}
				}
        }
        
        var _removeErrorHighlightAroundFuzzydateWidget = function($errorBannerClose, $fuzzyDateWrapper) {
        	$errorBannerClose.on('click', function(e) {
        		$fuzzyDateWrapper.removeClass('error');
			});
        };
        
    
		
  		var editPersonSubmit = function(e) {
			var _instance = this;
			 // Get the view and the model
  	        var view=this.get('activeView');
  	        var model=view.get('model');
  	        var outcome = view.get('outcome');
  	        var lifeState = UP.GetImpliedLifeState(view.get('lifeState'));
  			e.preventDefault();
  			
  		  // checking if the fuzzy date is valid
		    if(!view.fuzzyDOBWidget.isValid() || !view.fuzzyDeceasedDOBWidget.isValid()){				  
			  var e={};
               e.code=400;
               e.msg={                       
                    validationErrors:{dateOfBirth: commonLabels.dateOfBirthValidationErrorMessage}
               }
               view.fire('error',{error:e});
               
               // add the link to the year field
               errorHighlightFuzzyDateWidgets(view,_instance);
			  return;
		    }
  			
  			//show the mask
            this.showDialogMask();
  			
         	// Set attrs depending on roles
            var attrs={},
            isClient = view.get('personRoles').indexOf('CLIENT') > -1,
            isOther = view.get('personRoles').indexOf('OTHER') > -1,
            isFosterCarer = view.get('personRoles').indexOf('FOSTER_CARER') > -1,
            isAdopter = view.get('personRoles').indexOf('ADOPTER') > -1,
            isAliveOrDeceasedClientOrCarer = (isClient || isFosterCarer || isAdopter) && (lifeState === 'ALIVE' || lifeState === 'DECEASED');
            
		    if (isClient && lifeState === 'ALIVE'){
            	attrs={
            		dateOfBirth: view.getFuzzyDOB(),
            		personTypes: view.get('personRoles')
            	};
		    } else  if(isClient && lifeState === 'DECEASED'){
	    			var fuzzyDeceasedDOB = view.getFuzzyDeceasedDOB() || {};
			    	attrs={
							dateOfBirth: typeof fuzzyDeceasedDOB.year !== 'undefined' && fuzzyDeceasedDOB.year !== null ? fuzzyDeceasedDOB : null,
							personTypes: view.get('personRoles')
						};
	    	}else{
		    	if(isOther || isFosterCarer || isAdopter){
		    		var fuzzyDOB;
		    		if(lifeState === 'ALIVE') {
						fuzzyDOB = view.getFuzzyDOB() || {};
					} else if(lifeState === 'DECEASED'){
						fuzzyDOB = view.getFuzzyDeceasedDOB() || {};
					}
					
				attrs={
					dateOfBirth: typeof fuzzyDOB.year !== 'undefined' && fuzzyDOB.year !== null ? fuzzyDOB : null,
					personTypes: view.get('personRoles')
					};
		    	} 
		    	else {
		    		attrs={
		    			// If a person is being switched from Other to Professional, no need to preserve DoB,
		    			// ethnicity or gender
		    			dateOfBirth: null,
		    			gender:null,
		    			genderIdentity:null,
		    			sexualOrientation:null,
		    			genderIdentitySelfDefined:null,
		    			ethnicity:null,
	            		personTypes: view.get('personRoles'),
	            		countryOfBirth: null,
	            		religion: null
	            	};
		    	}
        }
		    
		    if(isAliveOrDeceasedClientOrCarer) {
		      attrs.nhsNumber = view.getNhsNumber();
		    }
		    
		    attrs.lifeState = lifeState;
			attrs.newCitizenships = view.getCitizenships('NewCitizenship');
			attrs.citizenshipsToUpdate = view.getCitizenships('UpdateCitizenship');
			attrs.citizenshipIdsToDelete = view.getCitizenships('DeleteCitizenship');
			attrs.notCarriedToTermType = outcome;
        	model.setAttrs(
        		attrs
        	,{
        		//don't trigger the onchange listener as we will re-render after save
        		silent:true
        	});
        	//if Due Date field is not present donot map.
        	if(!view.get('container').one('#dueDate-wrapper')) {
        		model.removeAttr('dueDate');
        	}
        	//if Not carried to term date field is not present do not map.
        	if(!view.get('container').one('#notCarriedToTermDate-wrapper')) {
        		model.removeAttr('notCarriedToTermDate');
        		model.removeAttr('notCarriedToTermType');
        	}
  			
  			// Save
  			model.save({transmogrify:Y.usp.person.UpdatePerson},
  				function(err,response){
  				//hide the mask
		        _instance.hideDialogMask();
  				if(err===null){
  					personDialog.hide();
  					Y.fire('person:personDetailChanged');
					Y.fire('infoMessage:message', {message:commonLabels.editPersonSuccessMessage});
				}else{
  	  				
  					//manully add validation indictor for NHS number
					var $nhsValidation = _instance.bodyNode.one('.message-error a[href="#nhsNumber"]'); 					
  					if($nhsValidation){
             				view.getInput('nhsFirstThreeDigits').addClass('error');
             				view.getInput('nhsSecondThreeDigits').addClass('error');
             				view.getInput('nhsLastFourDigits').addClass('error');
             				
             				$nhsValidation.on('click', function(e) {
                       			view.getInput('nhsFirstThreeDigits').focus().select();
                    		});
          			}
  					errorHighlightFuzzyDateWidgets(view,_instance);
  				}
  			});
  		};

  		var setSimpleFuzzyDate = function(actualFuzzyDate){
  			var simpleFuzzyDate = {
  					_type: "SimpleFuzzyDate", 
					year: null, 
					month: null, 
					day: null
				};
			if 	(actualFuzzyDate && actualFuzzyDate.calculatedDate){
			var actualFuzzyDateArray = actualFuzzyDate.calculatedDate.split("-");
			var fuzzyDateMask = actualFuzzyDate.fuzzyDateMask;
			
			switch(fuzzyDateMask){
			case "YEAR":
				simpleFuzzyDate.year=actualFuzzyDateArray[0];
				break;
			case "MONTH":
				simpleFuzzyDate.year=actualFuzzyDateArray[0];
				// Month is zero-based, so subtract 1
				simpleFuzzyDate.month=actualFuzzyDateArray[1]-1;
				break;
			case "DAY":
				simpleFuzzyDate.year=actualFuzzyDateArray[0];
				// Month is zero-based, so subtract 1
				simpleFuzzyDate.month=actualFuzzyDateArray[1]-1;
				simpleFuzzyDate.day=actualFuzzyDateArray[2];
				break;
			}
			}
			return 	simpleFuzzyDate;
		};
			
  		// Save change person name button action
		var changePersonNameSave = function(e){
  			e.preventDefault();
  			
  			// Get the view and the model
  			var view=this.get('activeView'),
  				model=view.get('model');
  			if(view.get('model').get('dateOfBirth')){
  				delete view.get('model').get('dateOfBirth').allowedRange;
  			}
  			model.removeAttr('dueDate');
  			model.removeAttr('notCarriedToTermDate');
  			model.removeAttr('notCarriedToTermType');
  			
  			model.save({transmogrify:Y.usp.person.UpdatePerson},
  				function(err,response){
  				if(err===null){
  					personDialog.hide();  			      			    
  					Y.fire('person:personDetailChanged');
					Y.fire('infoMessage:message', {message:commonLabels.changeNameSuccessMessage});
  				}
  			});
  		};
  		
  	   // Remove person previous name ok button action
		var removePersonPreviousNameSave = function(e){
  			e.preventDefault();
  			
  			// Get the view and the model
  			var view=this.get('activeView'),
  				model=view.get('model');
  			
  			
  			 model.destroy({
  				remove: true
  			 },
  				function(err,response){
  				if(err===null){
  					personDialog.hide();  			      			    
  					Y.fire('person:personDetailChanged');
					Y.fire('infoMessage:message', {message:commonLabels.previousNameSuccessMessage});
  				}
  			}); 
  		};
  		
  		
		var personDialog=new Y.usp.MultiPanelPopUp({
			headerContent:'<h3><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-user fa-inverse fa-stack-1x"></i></span> Person details</h3>',
			width:700,
			//Plugin Model Error handling
			plugins:[{fn:Y.Plugin.usp.MultiPanelModelErrorsPlugin}],
			
			destructor: function() {
				this.unplug(Y.Plugin.usp.MultiPanelModelErrorsPlugin);
			},
			
			// Views that this dialog supports
			views: {
				// View Person dialog
				viewPerson: {
					type:Y.app.person.ViewPersonView,
					headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span> '+commonLabels.viewPersonHeader+'</h3>',
					buttons:[{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:       'cancelButton', 
						labelHTML:  '<i class="fa fa-times"></i> Cancel',
						classNames: 'pure-button-secondary'
	        		}]
	            },
				updatePersonRoleSelect:{
					type:Y.app.person.AddPersonRolesView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.editPersonHeader+'</h3>',
					buttons:[ {
	                       action:     proceedEditDialog,
	                       section:    Y.WidgetStdMod.FOOTER,
	                       name:       'proceedButton',
	                       labelHTML:  '<i class="fa fa-check"></i> Proceed',
	                       classNames: 'pure-button-primary'
	                     },
	                     {
	                    	 action:     cancelDialog,
	                    	 section:    Y.WidgetStdMod.FOOTER,
	                    	 name:       'cancelButton',
	                    	 labelHTML:  '<i class="fa fa-times"></i> Cancel',
	                    	 classNames: 'pure-button-secondary'
	                     }]
				},
				changePersonRole:{
					type:Y.app.person.PersonRoleChangeView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.editPersonHeader+'</h3>',
					buttons:[ {
	                       action:     proceedEditDialog,
	                       section:    Y.WidgetStdMod.FOOTER,
	                       name:       'proceedButton',
	                       labelHTML:  '<i class="fa fa-check"></i> Proceed',
	                       classNames: 'pure-button-primary'
	                     },
	                     {
	                    	 action:     cancelDialog,
	                    	 section:    Y.WidgetStdMod.FOOTER,
	                    	 name:       'cancelButton',
	                    	 labelHTML:  '<i class="fa fa-times"></i> Cancel',
	                    	 classNames: 'pure-button-secondary'
	                     }]
				},
                
				// edit Person dialog
				editPerson:{
					type:Y.app.person.UpdatePersonView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.editPersonHeader+'</h3>',
					buttons:[{
		        	   	action: editPersonSubmit,
		        	   	section: Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary', 
		             	disabled: true
					},{
				            action:     cancelDialog,
				            section:    Y.WidgetStdMod.FOOTER,
				            name:       'cancelButton', 
				            labelHTML:  '<i class="fa fa-times"></i> Cancel',
				            classNames: 'pure-button-secondary'
					}]
				},
					
				changePersonName:{
					type:Y.app.person.ChangePersonNameView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.changeName+'</h3>',
					buttons:[{
		        	   	action: changePersonNameSave,
		        	   	section: Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary', 
		             	disabled: true
					},{
			            action:     cancelDialog,
			            section:    Y.WidgetStdMod.FOOTER,
			            name:       'cancelButton', 
			            labelHTML:  '<i class="fa fa-times"></i> Cancel',
			            classNames: 'pure-button-secondary'
					}]
				},
				
				removePersonName:{
					type:Y.app.person.RemovePreviousNameView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.previousNamePopupTitle+'</h3>',
					buttons:[{
		        	   	action: removePersonPreviousNameSave,
						name:      'yesButton',
						labelHTML: '<i class="fa fa-check"></i> Yes',
		             	disabled: true
					},{
			            action:     cancelDialog,
			            name:       'noButton', 
			            labelHTML:  '<i class="fa fa-times"></i> No',
			            classNames: 'pure-button-primary',
					}]
				},
				
				
				
				addPersonAddress: {
					type: Y.app.person.AddPersonAddressView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.addPersonAddressHdr+'</h3>',
					buttons: [{
						action:    addPersonAddressSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary', 
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:       'cancelButton', 
						labelHTML:  '<i class="fa fa-times"></i> Cancel',
						classNames: 'pure-button-secondary'
					}]
				},
				addMovePersonAddress: {
					type: Y.app.person.AddPersonAddressView,
					headerTemplate:'<h3 id="labelledby" aria-describedby="describedby" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.movePersonAddressHdr+'</h3>',
					buttons: [{
						action:    addPersonAddressSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary', 
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:       'cancelButton', 
						labelHTML:  '<i class="fa fa-times"></i> Cancel',
						classNames: 'pure-button-secondary'
					}]
				},
				
				
				moveResidentPersonAddress: {
					type: Y.app.person.CombinedEndAddPersonAddressView,
					headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-user fa-inverse fa-stack-1x"></i></span> '+commonLabels.moveResidents+'</h3>',
					buttons:[{
						action:    moveResidentPersonAddressSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:       'cancelButton', 
						labelHTML:  '<i class="fa fa-times"></i> Cancel',
						classNames: 'pure-button-secondary'
	        		}]
	            },
	            moveResidentPersonAddressLocation: {
					type: Y.app.person.UpdatePersonAddressLocationView,
					headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-user fa-inverse fa-stack-1x"></i></span> '+commonLabels.moveResidents+'</h3>',
					buttons:[{
						action:    moveResidentPersonAddressSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:       'cancelButton', 
						labelHTML:  '<i class="fa fa-times"></i> Cancel',
						classNames: 'pure-button-secondary'
	        		}]
	            },
	            
				// Add Reference Number dialog
				addPersonReferenceNumber: {
					type:Y.app.person.NewPersonReferenceNumberView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-hashtag fa-inverse fa-stack-1x"></i></span>'+commonLabels.addReferenceNumberHeader+'</h3>',
				 	buttons:[{
						action:    addPersonReferenceNumberSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> Cancel',
						classNames:'pure-button-secondary'
						}]
					},	
				// Update language dialog
				updatePersonLanguage: {
					type:Y.app.person.UpdatePersonLanguageView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-language fa-inverse fa-stack-1x"></i></span><span id="languageHeader">'+commonLabels.languageHeader+'</span></h3>',
				 	buttons:[{
						action:    updatePersonLanguageSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> Cancel',
						classNames:'pure-button-secondary'
						}]
				},
				// Edit/Remove Reference Number dialog
				editRemovePersonReferenceNumber: {
					type:Y.app.person.EditRemovePersonReferenceNumberView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-hashtag fa-inverse fa-stack-1x"></i></span>'+commonLabels.editReferenceNumberHeader+'</h3>',
				 	buttons:[{
						action:    editRemovePersonReferenceNumberSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> Cancel',
						classNames:'pure-button-secondary'
					}]
				},
				endPersonAddress: {
					type:Y.app.person.EndPersonAddressView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.endPersonAddressHdr+'</h3>',
				 	buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'yesButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
                        classNames:'pure-button-primary',		
                        action: endPersonAddressSubmit,
                        disabled: false
                    },
                    {
                    	name: 'noButton',
    	                labelHTML: '<i class="fa fa-times"></i> No',
    	                classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
    	                section: Y.WidgetStdMod.FOOTER
    			}]
				},				
				updatePersonLocation: {
					type:Y.app.person.UpdatePersonLocationView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.updatePersonAddressLocationHdr+'</h3>',
				 	buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'yesButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
                        classNames:'pure-button-primary',		
                        action: updatePersonAddressLocationSubmit,
                        disabled: false
                    },
                    {
                    	name: 'noButton',
    	                labelHTML: '<i class="fa fa-times"></i> No',
    	                classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
    	                section: Y.WidgetStdMod.FOOTER
    			}]
				},
				reopenPersonAddress: {
					type:Y.app.person.ReopenPersonAddressView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.reopenPersonAddressHdr+'</h3>',
				 	buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'yesButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
                        classNames:'pure-button-primary',		
                        action: reopenPersonAddressSubmit,
                        disabled: false
                    },
                    {
                    	name: 'noButton',
    	                labelHTML: '<i class="fa fa-times"></i> No',
    	                classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
    	                section: Y.WidgetStdMod.FOOTER
    			}]
				},
				updateStartEndPersonAddress: {
					type:Y.app.person.UpdateStartEndPersonAddressView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.updatePersonAddressDatesHdr+'</h3>',
				 	buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'yesButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
                        classNames:'pure-button-primary',		
                        action: updatePersonAddressDatesSubmit,
                        disabled: false
                    },
                    {
                    	name: 'noButton',
    	                labelHTML: '<i class="fa fa-times"></i> No',
    	                classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
    	                section: Y.WidgetStdMod.FOOTER
    			}]
				},
				endMovePersonAddress: {
					type:Y.app.person.EndPersonAddressView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.movePersonAddressHdr+'</h3>',
				 	buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'yesButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
                        classNames:'pure-button-primary',		
                        action: endPersonAddressSubmit,
                        disabled: false
                    },
                    {
                    	name: 'noButton',
    	                labelHTML: '<i class="fa fa-times"></i> No',
    	                classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
    	                section: Y.WidgetStdMod.FOOTER
    			}]
				},
				
				// Edit/Remove Contact dialog
				 editRemoveContact: {
					type:Y.app.person.EditRemoveContactView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa eclipse-contact fa-inverse fa-stack-1x"></i></span>'+commonLabels.editContactHeader+'</h3>',				 	
				 	buttons:[{
						action:    editRemoveContactSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> Cancel',
						classNames:'pure-button-secondary'
					}]
				},
				
				removePersonAddress: {
					type:Y.app.person.RemovePersonAddressView,
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-envelope fa-inverse fa-stack-1x"></i></span>'+commonLabels.removePersonAddressHdr+'</h3>',
				 	buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'yesButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
						classNames:'pure-button-primary',		
                        action: removePersonAddressSubmit,
                        disabled: false
                    },
                    {
                    	name: 'noButton',
    	                labelHTML: '<i class="fa fa-times"></i> No',
    	                classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
    	                section: Y.WidgetStdMod.FOOTER
    			}]
				},
				
				// Add Person Contact dialog
				addPersonContactView: {
					type:Y.app.person.NewPersonContactView,		
				 	headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa eclipse-contact fa-inverse fa-stack-1x"></i></span>'+commonLabels.addContactHeader+'</h3>',
				 	buttons:[{
						action:    addPersonContactSubmit,		
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> Cancel',
						classNames:'pure-button-secondary'
					}]
				},
				
				// Change unborn dialog
				changeUnbornView :{
					type: Y.app.person.ChangeUnbornView,
					headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.changeUnbornHeader+'</h3>',
					buttons:[{
						action:    changeUnbornSubmit,		
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> Cancel',
						classNames:'pure-button-secondary'
					}]
				},
				// Change Deceased to alive dialog
				changeDeceasedToAliveAdministrativelyView :{
					type: Y.app.person.ChangeDeceasedToAliveView,
					headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.changeDeceasedHeader+'</h3>',
					buttons:[{
						action:    changeDeceasedToAliveAdministrativelySubmit,		
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Yes',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> No',
						classNames:'pure-button-secondary'
					}]
				},
				
				
				//change notcarried dialog
				changeNotCarried :{
					type: Y.app.person.ChangeNotCarriedView,
					headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.changeNotcarriedHeader+'</h3>',
					buttons:[{
						action:    changeNotcarriedSubmit,		
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},{
						action:     cancelDialog,
						section:    Y.WidgetStdMod.FOOTER,
						name:      'cancelButton', 
						labelHTML: '<i class="fa fa-times"></i> Cancel',
						classNames:'pure-button-secondary'
					}]
				},
				
				// Change unborn dialog
				changeAliveView :{
					type: Y.app.person.ChangeAliveView,
					headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-person fa-inverse fa-stack-1x"></i></span>'+commonLabels.changeAliveHeader+'</h3>',
					buttons:[{
                        section: Y.WidgetStdMod.FOOTER,
                        name: 'yesButton',
                        labelHTML: '<i class="fa fa-check"></i> Yes',
						classNames:'pure-button-primary',		
                        action: changeAliveSubmit,
                        disabled: false
                    },
                    {
                    	name: 'noButton',
    	                labelHTML: '<i class="fa fa-times"></i> No',
    	                classNames: 'pure-button-secondary',
    	                action: function(e) {
    	                    e.preventDefault();
    	                    this.hide();
    	                },
    	                section: Y.WidgetStdMod.FOOTER
    				}]
				},
       		}
		});
		
		//render the popup
		personDialog.render();
		
		//respond to custom events
		Y.on('person:showDialog', function(e){
			switch(e.action){
				case 'viewPerson':
					this.showView('viewPerson', {
						model:new ViewPersonDialog(),	
		        		labels:commonLabels
					},
					{
						modelLoad:true,
						payload:{id:e.id}
					},
					function(){
				    	//Get the button by name out of the footer and enable it.
				    	this.getButton('cancelButton', Y.WidgetStdMod.FOOTER).set('disabled',false);							
					});
					break;			
					// Role select for changing an existing person's role
					case 'changePersonRoles':
						this.showView('changePersonRole',{
							model:new PersonRoleSelectForm(),
							labels:commonLabels,
							id:e.id,
							personRoles: e.personRoles
						});
						break;	
					// Role select for adding extra roles to a person
					case 'addPersonRoles':
						this.showView('updatePersonRoleSelect',{
							model:new PersonRoleSelectForm(),
							labels:commonLabels,
							id:e.id,
							personRoles: e.personRoles,
							existingPersonRoles: e.personRoles.slice(0),
							action:'addPersonRoles'
						});
						break;	
				case 'editPerson':
					this.showView('editPerson',{
						model:new UpdatePersonForm(),
						labels:commonLabels,
						personRoles: e.personRoles,
						lifeState:e.lifeState,
						permissions: permissions,
					},{modelLoad:true, payload:{id:e.id}},function(view){
			     		//Get the button by name out of the footer and enable it.
			     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			     		
					});
					break;					
				case 'changePersonName':
					this.showView('changePersonName',{
							model:new ChangePersonNameForm(),
							labels:commonLabels
						},
						{modelLoad:true, payload:{id:e.id}},
					function(){
			     		//Get the button by name out of the footer and enable it.
			     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false); 
					});
					break;
				case 'removePersonName':
					this.showView('removePersonName', {
		                model:  new PersonPreviousNameForm({
		                	url: Y.Lang.sub(urls.personNameUrl, {
		                        id: e.id
		                    })
		                }),
		                labels:commonLabels,
		                otherData: {
		                	previousPersonName: e.name
		                }
		            }, function() {
		                this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled', false);
		            });					
					break;
				case 'changeUnborn':
					this.showView('changeUnbornView',{
						model:new ChangeUnbornModel(),
						labels:commonLabels
					},
					{modelLoad:true, payload:{id:e.id}},
					function(){
						this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
				break;
				case 'changeDeceasedToAliveAdministratively':
					this.showView('changeDeceasedToAliveAdministrativelyView',{
						model:new ChangeDeceasedToAliveAdministrativelyModel(),
						labels:commonLabels
					},
					{modelLoad:true, payload:{id:e.id}},
					function(){
						this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
				break;
				case 'changeNotCarried':
					this.showView('changeNotCarried',{
						model:new ChangeNotCarriedModel(),
						labels:commonLabels
					},
					{modelLoad:true, payload:{id:e.id}},
					function(){
						this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
				break;
				case 'changeAlive':
					this.showView('changeAliveView',{
						model:new ChangeAliveModel(),
						labels:commonLabels,
						personRoles: e.personRoles,
						lifeState: e.lifeState
					},
					{modelLoad:true, payload:{id:e.id}},
					function(){
						this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
				break;
				case 'addPersonAddress':
					this.showView('addPersonAddress', {  
            		model: new AddPersonAddressForm(),
            		labels:commonLabels,
            		targetPersonId: e.targetPerson.id,
            		targetPersonName: e.targetPerson.name,
            		panel:personDialog,
            		locationUrl: '<s:url value="/rest/location"/>'
          		});
          			break;
				case 'addMovePersonAddress':
					this.showView('addMovePersonAddress', {  
            		model: new AddPersonAddressForm(),
            		labels:commonLabels,
            		targetPersonId: e.targetPerson.id,
            		targetPersonName: e.targetPerson.name,
            		panel:personDialog,
            		locationUrl: '<s:url value="/rest/location"/>',
                    endPersonAddressDetails: {
                    	endLocationId: e.endLocationId,
                    	addressType: e.addressType,
                    	addressUsage: e.addressUsage,
                    	endReason: e.endReason,
                    	endDate: e.endDate,
                    	endDateSDF: e.endDateSDF,
                    	addressId: e.addressId,
                    	objectVersion: e.objectVersion
                    },
					sourceViewName: e.sourceViewName
          		});
          			break;
				case 'moveResidentPersonAddressLocation':
					this.showView('moveResidentPersonAddressLocation', {
            		labels: {
            			personName: commonLabels.personName,
						relationshipName: commonLabels.relationshipName,
						update: commonLabels.update,
						moveResidents: commonLabels.moveResidents,
						narrativeSummaryResidents: commonLabels.narrativeSummaryResidentsLocationUpdate,
        				residentsCloseAddSuccessMessage: commonLabels.residentsCloseAddSuccessMessage
            		},
            		targetPersonId: e.targetPersonId,
            		locationId: e.newLocationId,
            		oldLocationId: e.currentLocationId,
            		panel:personDialog,
            		sourceView: 'moveResidentPersonAddressLocation',
            		preSelectedRecords: e.preSelectedRecords,
            		residentListUrl: L.sub('<s:url value="/rest/personPersonRelationship/sharedLocationResidents?personAddressId={personAddressId}&locationId={locationId}&onlyRelatedMembers=false&personId={personId}&usage=PERMANENT&type=HOME"/>',
        						{personId: e.targetPersonId, personAddressId: e.personAddressId, locationId: e.currentLocationId})            		
          		}, function(){
		     		//Get the button by name out of the footer and enable it.
		     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				});
          			break;
				case 'moveResidentPersonAddress':
					this.showView('moveResidentPersonAddress', {
            		labels: {
            			personName: commonLabels.personName,
						relationshipName: commonLabels.relationshipName,
						update: commonLabels.update,
						moveResidents: commonLabels.moveResidents,
						narrativeSummaryResidents: commonLabels.narrativeSummaryResidents,
						narrativeDescriptionReason: commonLabels.narrativeDescriptionReason,
        				narrativeDescriptionStart: commonLabels.narrativeDescriptionStart,
        				narrativeDescriptionEnd: commonLabels.narrativeDescriptionEnd,
        				residentsCloseAddSuccessMessage: commonLabels.residentsCloseAddSuccessMessage
            		},
            		targetPersonId: e.targetPersonId,
            		locationId: e.locationId,
            		startDate: e.startDate,
            		startDateSFD: e.startDateSFD,
            		panel:personDialog,
            		endPersonAddressDetails: e.endPersonAddressDetails, 
            		residentListUrl: L.sub('<s:url value="/rest/person/{personId}/personRelationship/sharedLocation?locationId={locationId}&usage=PERMANENT&type=HOME"/>',{personId:e.targetPersonId, locationId: e.endPersonAddressDetails.endLocationId})
          		}, function(){
		     		//Get the button by name out of the footer and enable it.
		     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				});
          			break;
				case 'endPersonAddress':
					this.showView('endPersonAddress', {  
            		model: new EndPersonAddressForm(),
            		labels: commonLabels,
            		targetPersonId: e.targetPersonId,
            		targetPersonName: e.targetPersonName,
            		sourceViewName: 'endPersonAddress'},
            		{modelLoad:true, payload:{id:e.personAddressId}}
          		);
          			break;
				case 'movePersonAddress':
					this.showView('endMovePersonAddress', { 
            		model: new EndPersonAddressForm(),
            		labels: commonLabels,
            		targetPersonId: e.targetPersonId,
            		targetPersonName: e.targetPersonName,
            		sourceViewName: 'movePersonAddress'},
            		{modelLoad:true, payload:{id:e.personAddressId}}
          		);
          			break;
				case 'updateAddressLocation':					
					this.showView('updatePersonLocation', {
          				model: new UpdatePersonLocationForm({
          					url: Y.Lang.sub(urls.personAddressUrl, {
                                id: e.personAddressId
                            })
          				}),
          				labels: commonLabels,
          				targetPersonId:e.targetPersonId,
						targetPersonName:e.targetPersonName,
						id:e.personAddressId,
						urls: urls,
						panel:personDialog,
						locationConfig: locationConfig
					}, {
						modelLoad: true
                    }, function(){		        		
					  //Get the button by name out of the footer and enable it.
						this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
          			break;
          		case 'removePersonAddress':
					this.showView('removePersonAddress', {  
            		model: new RemovePersonAddressForm(),
            		labels: commonLabels,
            		targetPersonId: e.targetPersonId,
            		targetPersonName: e.targetPersonName},
            		{modelLoad:true, payload:{id:e.personAddressId}}
          		);
          			break;
          		case 'updateAddressReopen':
          			this.showView('reopenPersonAddress', {
          				model: new ReopenPersonAddressForm({
          					url: Y.Lang.sub(urls.personAddressUrl, {
                                id: e.personAddressId
                            })
          				}),
          				labels: commonLabels,
          				targetPersonId:e.targetPersonId,
						targetPersonName:e.targetPersonName,
						id:e.personAddressId,
						reopenUrl:urls.reopenUrl
					}, {
						modelLoad: true
                    }, function(){		        		
					  //Get the button by name out of the footer and enable it.
						this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
          			break;
          		case 'updateStartEnd':
          			this.showView('updateStartEndPersonAddress', {
          				model: new UpdateStartEndPersonAddressForm({
          					url: Y.Lang.sub(urls.personAddressUrl, {
                                id: e.personAddressId
                            })
          				}),
          				labels: commonLabels,
          				targetPersonId:e.targetPersonId,
						targetPersonName:e.targetPersonName,
						id:e.personAddressId,
						updateStartEndUrl:urls.updateStartEndUrl
					}, {
						modelLoad: true
                    }, function(){		        		
					  //Get the button by name out of the footer and enable it.
						this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
          			break;
				case 'addPersonReferenceNumber':					
					this.showView('addPersonReferenceNumber',{			
						model: new Y.app.referenceNumber.NewPersonReferenceNumberForm({
							labels: commonLabels,
							url:'<s:url value="/rest/person/'+'${person.id}'+'/referenceNumber"/>'
						}),
						labels: commonLabels,							  
						//set targetPerson attributes
						targetPersonId:e.id,
						targetPersonName:e.name,
						activeReferenceNumbers:e.activeReferenceNumbers},
					function(){		        		
						//Get the button by name out of the footer and enable it.
						this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					});
					break;	
				case 'updatePersonLanguage':	
					// get id of language coded entry that matches local language code
					var localLanguageId;
					if (typeof Y.uspCategory.person.languages.category.codedEntries[localLanguageCode] != 'undefined') {
						localLanguageId=Y.uspCategory.person.languages.category.codedEntries[localLanguageCode].id;
					} else if(typeof Y.uspCategory.person.languages.category.codedEntries['en'] != 'undefined'){
						// default to id of English coded entry
						localLanguageId=Y.uspCategory.person.languages.category.codedEntries['en'].id;
					}
					this.showView('updatePersonLanguage',{			
						model: new Y.usp.person.PersonWithLanguages({
        					url:'<s:url value="/rest/person/'+e.id+'"/>'
        					
        				}),
						labels: commonLabels,		
						//set targetPerson attributes
						id:e.id,
						localLanguageCode:localLanguageId,
						targetPersonId:e.targetPersonId,
						targetPersonName:e.targetPersonName,
						url:'<s:url value="/rest/person/'+e.id+'?updateAssociatedLanguages=true"/>'},
						{
							modelLoad:true,
							payload:{id:e.id}
						},
					function(){		        		
						//Get the button by name out of the footer and enable it.
						this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
						
						// Set dialog header to 'add' label if adding
						if (!e.edit){
							Y.one('#languageHeader').set('text',commonLabels.languageAddHeader);
						}
					});
					break;
				case 'editRemovePersonReferenceNumber':					
					this.showView('editRemovePersonReferenceNumber',{			
						model: new UpdateRemovePersonReferenceNumberForm(),
						labels: commonLabels,
						targetPersonId:e.perId,
						targetPersonName:e.name,
						canRemoveReferenceNumber:e.canRemoveReferenceNumber,
						// FIXME Barry, you can add the codedEntries where the view is extended rather than here
						// see ViewPersonView example 
						codedEntries:{
			    			referenceNumberType:Y.secured.uspCategory.person.referencetype.category.getActiveCodedEntries(null,'write')
			    		}
					},{
						modelLoad:true, 
						payload:{
							id:e.id
							}
					},function(){   		        		
			     		// FIXME - This is a temporary solution, as we are using a paginated list here
			     		// and there is no 'change' event associated with ModelList to call the PersonReferenceNumbers view render
						this.get('activeView').render();
						//Get the button by name out of the footer and enable it.
			     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);

					});
					break;
				 case 'editRemoveContact':					
					 this.showView('editRemoveContact',{
					 	model: new UpdateRemoveContactForm(),
						labels: commonLabels,
						targetPersonId:e.perId,
						targetPersonName:e.name,							
						telephoneContacts:e.telephoneContacts,
						mobileContacts:e.mobileContacts,
						faxContacts:e.faxContacts,
						emailContacts:e.emailContacts,
						socialMediaContacts:e.socialMediaContacts,
						webContacts:e.webContacts,
						canRemoveContact: e.canRemoveContact,
						codedEntries:{
							contactType:Y.uspCategory.contact.contactType.category.codedEntries,
							contactUsage:Y.uspCategory.contact.contactUsage.category.codedEntries			    			
						}							
				},{
					modelLoad:true, 
					payload:{
						id:e.id							
						}
				},function(){
					this.get('activeView').render();
					//Get the button by name out of the footer and enable it.
		     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
						
				});
				break;	
				case 'addPersonContact':
					this.showView('addPersonContactView',{			
						model: new NewPersonContactForm(),
						labels: commonLabels,							  
						//set targetPerson attributes	
						targetPersonId:e.id,
						targetPersonName:e.name,
						targetPersonTypes:e.personTypes},
					function(){
						//Get the button by name out of the footer and enable it.
						this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',true);
					});
					break;				
			default:
			}Y.one("body").delegate('submit',function(e){e.preventDefault();},'form[action="#none"]');	
		},personDialog);
		
		personDialog.after('*:saveEnabledChange', function(e){
			// enable/disable the save button
			if (e.newVal===true) {			    
			    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			} else {
			    this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',true);
			}
		}, personDialog);	
		
 		var personPictureDialog=new Y.app.PersonPictureDialog({ 		
 			width:550,
 			views: {
 				editPersonPicture: {
 					header: {
 			            text: commonLabels.editPersonPictureHeader,
 			            icon: 'fa fa-user fa-inverse fa-stack-1x'
 			        },
 			      successMessage: commonLabels.editPersonPictureSuccess,
 			      labels: commonLabels,
 			      config: {
 			        defaultImageUrl : '<s:url value="/img/anon.png"/>'
 			      },
 				},
 				removePersonPicture: {
 					header: {
 			            text: commonLabels.removePersonPictureHeader,
 			            icon: 'fa fa-user fa-inverse fa-stack-1x'
 			        },
 			      successMessage: commonLabels.removePersonPictureSuccess,
 			      labels: commonLabels,
 			      config: {
 			        defaultImageUrl : '<s:url value="/img/anon.png"/>'
 			        
 			      },
 				},
 			}

 		});
		// TODO: move to controller view.
		personPictureDialog.render();
		
		Y.on('personPicture:showDialog', function(e){
			switch(e.action){
				case 'editPersonPicture':
					this.showView('editPersonPicture',{
						model: new UpdatePersonPictureForm(),
						personDetailsModel: e.personDetailsModel,
						});
					break;
					
				case 'removePersonPicture':
					this.showView('removePersonPicture',{
						model: new RemovePersonPictureForm(),
						personDetailsModel: e.personDetailsModel,
						labels: commonLabels},
				    {modelLoad:true,payload:{
							id: e.id
						}}, function() {
						    //remove the cancel button
						    this.removeButton('cancelButton', Y.WidgetStdMod.FOOTER);
						});
					break;
			}
		}, personPictureDialog);
		
	});
</script>

