<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<tilesx:useAttribute name="selected" ignore="true"/>
<tilesx:useAttribute name="first" ignore="true"/>
<tilesx:useAttribute name="last" ignore="true"/>
<c:if test="${selected eq true }">
    <c:set var="aStyle" value="pure-button-active"/>
</c:if>
<c:if test="${first eq true}">
    <c:set var="fStyle" value="first"/>
</c:if>
<c:if test="${last eq true}">
    <c:set var="lStyle" value="last"/>
</c:if>

<c:set var="name" value="${person.name}"/>

<li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
  <a href="#none" style="display:none" data-button-action="addLock" class="pure-button pure-button-disabled usp-fx-all pure-button-loading"
    title='<s:message code="button.hoverAddPersonLock" arguments="${name}" htmlEscape="true" />'  aria-disabled="true"
    aria-label='<s:message code="button.hoverAddPersonLock" arguments="${name}" htmlEscape="true" />'  tabindex="0">
    <i class="fa fa-lock"></i>
    <span><s:message code="button.addPersonLock" /></span>
  </a>
</li>

<li id="personLockButtons" style="display:none" class="pure-menu pure-menu-open pure-menu-horizontal menu-plugin" role="menu">
    <ul class="pure-menu-children">
        <li class="pure-menu-can-have-children pure-menu-has-children <c:out value="${fStyle}"/> <c:out value="${lStyle}"/>">
            <a href="#none" class='pure-button <c:out value="${aStyle}"/>'><i class='fa fa-lock'></i> <i class='fa fa-caret-down fa fa-white'></i> <span><s:message code="button.addPersonLock" /></span></a>
            <ul class="pure-menu pure-menu-children">
                <li class="pure-menu-item"><a href="#none" title='<s:message code="button.hoverEditPersonLock" />' style="display:none" data-button-action="" class="editLock pure-button usp-fx-all"><s:message code="button.editPersonLock" /></a></li>
                <li class="pure-menu-item"><a href="#none" title='<s:message code="button.hoverDeletePersonLock" />' style="display:none" data-button-action="" class="removeLock pure-button usp-fx-all"><s:message code="button.deletePersonLock" /></a></li>
                <li class="pure-menu-item"><a href="#none" title='<s:message code="button.hoverRemovePersonLock" />'  style="display:none" data-button-action=""  class="deleteLock pure-button usp-fx-all"><s:message code="button.removePersonLock" /></a></li>
            </ul>
        </li>
    </ul>
</li>
