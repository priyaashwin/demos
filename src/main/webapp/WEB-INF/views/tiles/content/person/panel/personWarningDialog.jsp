<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%-- Person warning popup - to be used to add, edit or view --%>

<script>
	Y.use('yui-base',
	      'personwarning-dialog-views',
	      'usp-person-PersonWarningAssignment',

		function(Y){

		var L = Y.Lang,

		commonLabels = {
			personId:'<s:message code="person.personId"/>',
			title: '<s:message code="person.title"/>',
			forename: '<s:message code="person.forename"/>',
			middleNames: '<s:message code="person.middleNames"/>',
			surname: '<s:message code="person.surname"/>',
			name: '<s:message code="person.name"/>',
			warningType: '<s:message code="person.warningType"/>',
			warningDetails: '<s:message code="person.warningDetails"/>',
			lastEdited: '<s:message code="person.lastEdited"/>',
			viewWarningHeader: '<s:message javaScriptEscape="true" code="person.warning.view.header"/>',
			addEditWarningHeader: '<s:message javaScriptEscape="true" code="person.warning.addedit.header"/>',
			viewPersonWarnignMessage: '<s:message javaScriptEscape="true" code="person.viewPersonWarnignMessage"/>',
			addEditPersonWarnignMessage: '<s:message javaScriptEscape="true" code="person.addEditPersonWarnignMessage"/>',
			emptyWarningNoteMessage: '<s:message javaScriptEscape="true" code="person.emptyWarningNoteMessage"/>',
			noWarnings:'<s:message code="person.warnings.no.results" javaScriptEscape="true" />',
			warnings:'<s:message code="person.warnings.results" javaScriptEscape="true" />',
			warning:'<s:message code="person.warning" javaScriptEscape="true" />',
			startDate:'<s:message code="person.warning.startDate" javaScriptEscape="true" />',
			endDate:'<s:message code="person.warning.endDate" javaScriptEscape="true" />',
			category:'<s:message code="person.warningCategory" javaScriptEscape="true" />',
			personNotified:'Person Notified',
			addWarning:'<s:message code="person.warning.add" javaScriptEscape="true" />',
			note:'<s:message code="person.warningNote" javaScriptEscape="true" />',
			warningCategoryErrorMessage:'<s:message code="person.warning.error.category" javaScriptEscape="true" />',
			startDateRequiredErrorMessage:'<s:message code="person.warning.error.startDate.required" javaScriptEscape="true" />',
			startDateErrorMessage:'<s:message code="person.warning.error.startDate" javaScriptEscape="true" />',
			endDateErrorMessage:'<s:message code="person.warning.error.endDate" javaScriptEscape="true" />',
			endDateWarningMessage:'<s:message code="person.warning.notification.endDate" javaScriptEscape="true" />',
			disabledWarningTypeTitle: '<s:message code="person.warning.disabledWarningType" javaScriptEscape="true" />',
			successMessage: '<s:message code="person.warning.successfulUpdate" javaScriptEscape="true" />',
			successPendingAuthorisationMessage:'<s:message code="person.warning.successful.pendingAuthorisation" javaScriptEscape="true" />'
		},
		//create new instance of person warning dialog and render
		personWarningDialog=new Y.app.PersonWarningDialog({
		    views:{
			    //merge into existing config (defined in personwarnings.js file)
			    viewPersonWarning:{
			        headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-user fa-inverse fa-stack-1x"></i></span> <s:message javaScriptEscape="true" code="person.warning.view.header"/></h3>'
			    },
			    addEditPersonWarning:{
			        headerTemplate:'<h3 class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-user fa-inverse fa-stack-1x"></i></span> <s:message javaScriptEscape="true" code="person.warning.addedit.header"/></h3>',
			    }
		    }
		}).render();

		//respond to custom events - for show/hide of warning dialog
		Y.on('warnings:showDialog', function(e){

      var isSubjectAccessAllowed ='${securedSubjectContextVO.isSubjectAccessAllowed}' ==='true',
			modelList = new Y.usp.person.PaginatedPersonWarningAssignmentList({
				url:L.sub('<s:url value="/rest/person/{id}/warningAssignment?personId={id}&s=pwa.endDate:-1,pwa.startDate:-1&pageNumber=1&pageSize=-1"/>', {
					id:e.id
				}),
				personId: parseInt(e.id)
			});
      
      if(!isSubjectAccessAllowed) {
    	  return;
      }

			switch(e.action){
				case 'viewPersonWarning':
					this.showView('viewPersonWarning', {
							model:new Y.usp.person.PersonWithWarning({
	        					url:'<s:url value="/rest/person/{id}"/>'
	        				}),
							labels:commonLabels,
							modelList: modelList
						},{
							modelLoad:true,
							payload:{id:e.id}
						});
					break;
				case 'addEditPersonWarning':
					this.showView('addEditPersonWarning', {
							model: new Y.usp.person.PersonWithWarning({
								url:'<s:url value="/rest/person/{id}"/>',
								personId: parseInt(e.id)
    					}),
							modelList: modelList,
        			template:Y.Handlebars.templates["personWarningAddEditDialog"],
        			labels:commonLabels,
        			updateURL:'<s:url value="/rest/personWarning/{id}"/>',
        			addURL:'<s:url value="/rest/person/${person.id}/warning"/>'
						},
						{
							modelLoad:true,
							payload:{id:e.id}
						},
						function(view){
					     		//Get the button by name out of the footer and enable it.
					     		this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					     		this.getButton('cancelButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
						}
					);
				break;

				default:
			}
		},personWarningDialog);
	});
</script>
