<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(${person.id}, 'RecordLock.OVERRIDE','RecordLock.Override')" var="canOverrideRecordLocks" />

<c:if test="${canOverrideRecordLocks eq true}">
    <tilesx:useAttribute name="selected" ignore="true"/>
    <tilesx:useAttribute name="first" ignore="true"/>
    <tilesx:useAttribute name="last" ignore="true"/>
    <c:if test="${selected eq true}">
        <c:set var="aStyle" value="pure-button-active"/>
    </c:if>
    <c:if test="${first eq true}">
        <c:set var="fStyle" value="first"/>
    </c:if>
    <c:if test="${last eq true}">
        <c:set var="lStyle" value="last"/>
    </c:if>

    <li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>' >
        <a id="recordOverrideButton" 
            href="#none" 
            style="display:none"
            data-button-action="lockOverride" 
            class="lockOverride pure-button pure-button-disabled usp-fx-all pure-button-loading"
            aria-disabled="true"
            title='<s:message code="button.hoverRecordOverride"/>'
            aria-label='<s:message code="button.hoverRecordOverride"/>'  
            tabindex="0"> 
            <i class="fa fa-unlock-alt"></i>
            <span><s:message code="button.recordOverride" /></span>
        </a>
    </li>
	<script>
		Y.use('yui-base','event-custom','node',function(Y){
			var button = Y.one('#recordOverrideButton');
			
			button.on('click', function(e){
				e.preventDefault();
			
				this.fire('personLockHandler:lockOverride');
			});
		});
	
	</script>
</c:if>