<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(${person.id},'SecuredPersonContext.GET','Person.View')" var="accessAllowed" />
<c:set var="isProfessional" value="${personTypesHelper.isProfessional()}" />
<c:set var="isProfessionalOnly" value="${personTypesHelper.isProfessionalOnly()}" />
<c:set var="isFosterCarer" value="${personTypesHelper.isFosterCarer()}" />


<sec:authorize access="hasPermission(null, 'PersonAddress.View','PersonAddress.View')" var="canViewAddressPerm"/>
<sec:authorize access="hasPermission(null, 'PersonEducation.View','PersonEducation.View')" var="canViewEducation"/>
<c:set var="canViewAddress" value="${canViewAddressPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true' || isFosterCarer == 'true')}" />
<sec:authorize access="hasPermission(null, 'PersonContact.View','PersonContact.View')" var="canViewContactPerm"/>
<c:set var="canViewContact" value="${canViewContactPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true' || isFosterCarer == 'true')}" />
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonPicture.View')" var="canViewPersonPicture"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonReferenceNumber.View')" var="canViewReferenceNumber"/>
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Belongings.View')" var="canViewBelongings" />
<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','SubjectAccessRequest.View')" var="canViewSubjectAccessRequest" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
  <sec:authorize access="hasPermission(null, 'Person.Update','Person.Update')" var="canEditPerm"/>
  <!-- The person details may be edited with no relational access if they are ONLY recorded as a professional (i.e. not also a client). -->
	<c:set var="canEdit" value="${canEditPerm == 'true' && (accessAllowed == 'true' || isProfessionalOnly == 'true')}" />
	
	<sec:authorize access="hasPermission(null, 'PersonName.Delete','PersonName.Delete')" var="canDeletePersonName"/>
	<sec:authorize access="hasPermission(null, 'PersonAddress.Add','PersonAddress.Add')" var="canAddAddressPerm"/>
	<sec:authorize access="hasPermission(null, 'PersonAddress.Close','PersonAddress.Close')" var="canCloseAddressPerm"/>
	<sec:authorize access="hasPermission(null, 'PersonAddress.CloseAdd','PersonAddress.CloseAdd')" var="canCloseAddAddressPerm"/>
	<sec:authorize access="hasPermission(null, 'PersonAddress.Remove','PersonAddress.Remove')" var="canRemoveAddressPerm"/>

  <sec:authorize access="hasPermission(null, 'Address.Disclose','Address.Disclose')" var="canSetAddressDisclose"/>
  <sec:authorize access="hasPermission(null, 'Address.DoNotDisclose','Address.DoNotDisclose')" var="canSetAddressDoNotDisclose"/>

	<sec:authorize access="hasPermission(null, 'PersonAddress.UpdateStartEnd','PersonAddress.UpdateStartEnd')" var="canUpdateAddressStartEnd"/>
	<sec:authorize access="hasPermission(null, 'PersonAddress.UpdateReopen','PersonAddress.UpdateReopen')" var="canUpdateAddressReopen"/>
	
	<sec:authorize access="hasPermission(null, 'PersonAddress.UpdateResidents','PersonAddress.UpdateResidents')" var="canUpdateResidentLocation"/>	
	
	<sec:authorize access="hasPermission(null, 'PersonType.Add','PersonType.Add')" var="canAddPersonTypePerm"/>
	<c:set var="canAddPersonType" value="${canAddPersonTypePerm == 'true' && canEditPerm == 'true' && accessAllowed == 'true'}" />
	
	<!-- WORK addresses for professionals can be modified even with no relational access in place. -->
	<c:set var="canAddAddress" value="${canAddAddressPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	<c:set var="canCloseAddress" value="${canCloseAddressPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	<c:set var="canCloseAddAddress" value="${canCloseAddAddressPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	<c:set var="canRemoveAddress" value="${canRemoveAddressPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	
	<c:set var="canUpdateResidentLocation" value="${canUpdateResidentLocation == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	
	<sec:authorize access="hasPermission(null, 'PersonContact.Add','PersonContact.Add')" var="canAddContactPerm"/>
	<sec:authorize access="hasPermission(null, 'PersonContact.Update','PersonContact.Update')" var="canEditContactPerm"/>
	<sec:authorize access="hasPermission(null, 'PersonContact.Remove','PersonContact.Remove')" var="canRemoveContactPerm"/>
	
	<!-- WORK contacts for professionals can be modified even with no relational access in place. -->
	<c:set var="canAddContact" value="${canAddContactPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	<c:set var="canEditContact" value="${canEditContactPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	<c:set var="canRemoveContact" value="${canRemoveContactPerm == 'true' && (accessAllowed == 'true' || isProfessional == 'true')}" />
	
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonLanguageAssignment.Update')" var="canUpdateLanguage"/>
	
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Person.DECEASEDTOALIVEADMINISTRATIVELY')" var="canUpdateDeceasedToAlive" />
	
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonReferenceNumber.Add')" var="canAddReferenceNumber"/>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonReferenceNumber.Update')" var="canEditReferenceNumber"/>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonReferenceNumber.Delete')" var="canRemoveReferenceNumber"/>
	
	<%-- SecurityAccessAllowedRecord permissions --%>
	<sec:authorize access="hasPermission(null, 'SecurityAccessAllowedRecord','SecurityAccessAllowedRecord.Add')" var="canAddSecurityAccessAllowedRecord" />
	<sec:authorize access="hasPermission(null, 'SecurityAccessAllowedRecord','SecurityAccessAllowedRecord.Update')" var="canUpdateSecurityAccessAllowedRecord" />
	<sec:authorize access="hasPermission(null, 'SecurityAccessAllowedRecord','SecurityAccessAllowedRecord.Delete')" var="canDeleteSecurityAccessAllowedRecord" />
	
	<%-- SecurityAccessDeniedRecord permissions --%>
	<sec:authorize access="hasPermission(null, 'SecurityAccessDeniedRecord','SecurityAccessDeniedRecord.Add')" var="canAddSecurityAccessDeniedRecord" />
	<sec:authorize access="hasPermission(null, 'SecurityAccessDeniedRecord','SecurityAccessDeniedRecord.Update')" var="canUpdateSecurityAccessDeniedRecord" />
	<sec:authorize access="hasPermission(null, 'SecurityAccessDeniedRecord','SecurityAccessDeniedRecord.Delete')" var="canDeleteSecurityAccessDeniedRecord" />
	
	<%-- Belongings permissions --%>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Belongings.Add')" var="canAddBelongings" />
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Belongings.Return')" var="canReturnBelongings" />
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Belongings.Destroy')" var="canDestroyBelongings" />
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Belongings.Move')" var="canMoveBelongings" />
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','Belongings.Update')" var="canUpdateBelongings" />

	<%-- Education permissions --%>
	<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonEducation.Update')" var="canUpdateEducation" />
	
	<%-- Subject Access Request permissions --%>
  <sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','SubjectAccessRequest.Create')" var="canAddSubjectAccessRequest" />
  <sec:authorize access="hasPermission(${person.id}, 'RecordLock.OVERRIDE','RecordLock.Override')" var="canOverrideRecordLocks" />

</c:if>

<div id="identification"></div>

<script>
Y.use('yui-base', 'usp-relationshipsrecording-PersonDetails', 'person-details-screen', 'person-lock-handler', function(Y) {
    var personDetailsModel = new Y.usp.relationshipsrecording.PersonDetails({
        id: '${person.id}',
        root: '<s:url value="/rest/person/"/>'
    });
    personDetailsModel.load();

    Y.on('person:personDetailChanged', function(e) {
        personDetailsModel.load();
    });

    var lockIcon='fa fa-lock';

    var outputNarrativeDescription= '{{this.name}} ({{this.personIdentifier}})';
      
    var subjectNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

    var subject = {
        subjectId: '<c:out value="${person.id}"/>',
        subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
        subjectName: '${esc:escapeJavaScript(person.name)}',
        subjectType: 'person'
    };
    
    var outputDialogConfig={
      views:{
        produceDocument:{
          type:'app.person.PersonOutputDocumentView',
          header: {
            text: '<s:message code="person.output.header" javaScriptEscape="true"/>',
            icon: 'fa fa-file'
          },
          narrative: {
            summary: '<s:message code="produceDocument.dialog.summary" javaScriptEscape="true" />',
            description: outputNarrativeDescription
          },
          labels:{
            outputCategory: '<s:message code="produceDocument.dialog.category" javaScriptEscape="true"/>',
            outputCategoryPrompt: '<s:message code="produceDocument.dialog.category.select.blank" javaScriptEscape="true"/>',
            availableTemplates: '<s:message code="produceDocument.dialog.availableTemplates" javaScriptEscape="true"/>',
            outputFormat: '<s:message code="produceDocument.dialog.outputFormat" javaScriptEscape="true"/>',
            personList: '<s:message code="produceDocument.dialog.personList" javaScriptEscape="true" />',
            produceSingleOutput:'<s:message code="produceDocument.dialog.produceSingleOutput" javaScriptEscape="true"/>'
          },
          config:{
            templateListURL: '<s:url value="/rest/outputTemplate?status=PUBLISHED&context=PERSON,PERSON_LETTER&asList=true"/>',
            generateURL:'<s:url value="/rest/documentRequest"/>',
            generatePersonLetterURL:'<s:url value="/rest/documentRequest"/>',
            generateRelationsURL:'<s:url value="/rest/person/{id}/namedRelationDocument"/>',
            downloadURL:'<s:url value="/rest/personDocument/{id}/content"/>',
            getRelationshipsListURL:'<s:url value="/rest/person/${person.id}/personRelationship?s=id&pageNumber=-1&pageSize=-1&differentialLevel=3"/>',
            getOrganisationsListURL:'<s:url value="/rest/person/${person.id}/organisationRelationship?s=id&pageNumber=-1&pageSize=-1&relationshipsAndDates=true"/>',
            successMessage: '<s:message code="produceDocument.dialog.success.generateInProgress" javaScriptEscape="true" />',
            failureMessage: '<s:message code="produceDocument.dialog.failure.generateFailed" javaScriptEscape="true" />',
          }
        }
      }
    };

    var commonLabels={
        usersTab:'Users',
        teamsTab:'Teams'
    };

    var personLockDialogConfig={
        views:{
            addPersonLock: {
                header:{
                  text: '<s:message code="personlock.dialog.add.header" javaScriptEscape="true"/>',
                  icon: lockIcon
                },
                narrative:{
                    summary:'<s:message code="personlock.add.summary" javaScriptEscape="true" />'
                },
                labels:{
                    accessAllowedList: '<s:message code="personlock.add.allow" javaScriptEscape="true" />',
                    accessDeniedList: '<s:message code="personlock.add.deny" javaScriptEscape="true" />'
                }
            },
            addAccessAllowedList : {
                header:{
                  text: '<s:message code="accessAllowedList.dialog.add.header" javaScriptEscape="true" />',
                  icon: lockIcon
                },
                narrative:{
                    summary:'<s:message code="accessAllowedList.add.summary" javaScriptEscape="true" />'
                },
                successMessage: '<s:message code="accessAllowedList.add.success.message" javaScriptEscape="true" />',
                labels:Y.merge(commonLabels,{
                    availableUsers:'<s:message code="accessAllowedList.add.available.securityBodies.users" javaScriptEscape="true" />',
                    assignedUsers:'<s:message code="accessAllowedList.add.assigned.securityBodies.users" javaScriptEscape="true" />',
                    availableTeams:'<s:message code="accessAllowedList.add.available.securityBodies.teams" javaScriptEscape="true" />',
                    assignedTeams:'<s:message code="accessAllowedList.add.assigned.securityBodies.teams" javaScriptEscape="true" />'
                }),
                config:{
                    userSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=USER" />',
                    teamSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=TEAM" />'
                }
            },
            editAccessAllowedList : {
                header:{
                    text:'<s:message code="accessAllowedList.dialog.edit.header" javaScriptEscape="true" />',
                    icon: lockIcon
                },
                narrative:{
                    summary:'<s:message code="accessAllowedList.edit.summary" javaScriptEscape="true" />'
                },
                successMessage: '<s:message code="accessAllowedList.edit.success.message" javaScriptEscape="true" />',
                labels:Y.merge(commonLabels,{
                    availableUsers:'<s:message code="accessAllowedList.edit.available.securityBodies.users" javaScriptEscape="true" />',
                    assignedUsers:'<s:message code="accessAllowedList.edit.assigned.securityBodies.users" javaScriptEscape="true" />',
                    availableTeams:'<s:message code="accessAllowedList.edit.available.securityBodies.teams" javaScriptEscape="true" />',
                    assignedTeams:'<s:message code="accessAllowedList.edit.assigned.securityBodies.teams" javaScriptEscape="true" />'
                }),
                config:{
                    updateUrl: '<s:url value="/rest/securityAccessAllowedRecord/{id}" />',
                    userSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=USER" />',
                    teamSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=TEAM" />'
                }
            },
            deleteAccessAllowedList : {
                header:{
                    text:'<s:message code="accessAllowedList.dialog.delete.header" javaScriptEscape="true" />',
                    icon: lockIcon
                },
                narrative:{
                    summary: '<s:message code="accessAllowedList.delete.summary" javaScriptEscape="true" />'
                },
                successMessage: '<s:message code="accessAllowedList.delete.success.message" javaScriptEscape="true" />',
                config:{
                    message: '<s:message code="accessAllowedList.delete.confirmMessage" javaScriptEscape="true" />',
                    deleteUrl: '<s:url value="/rest/securityAccessAllowedRecord/{id}/status?action=ended" />'
                }
            },
            addAccessDeniedList : {
                header:{
                    text:'<s:message code="accessDeniedList.dialog.add.header" javaScriptEscape="true" />',
                    icon: lockIcon
                },
                narrative:{
                    summary:'<s:message code="accessDeniedList.add.summary" javaScriptEscape="true" />'
                },
                successMessage: '<s:message code="accessDeniedList.add.success.message" javaScriptEscape="true" />',
                labels:Y.merge(commonLabels,{
                    availableUsers:'<s:message code="accessDeniedList.add.available.securityBodies.users" javaScriptEscape="true" />',
                    assignedUsers:'<s:message code="accessDeniedList.add.assigned.securityBodies.users" javaScriptEscape="true" />',
                    availableTeams:'<s:message code="accessDeniedList.add.available.securityBodies.teams" javaScriptEscape="true" />',
                    assignedTeams:'<s:message code="accessDeniedList.add.assigned.securityBodies.teams" javaScriptEscape="true" />'
                }),
                config:{
                    userSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=USER" />',
                    teamSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=TEAM" />'
                }
            },
            editAccessDeniedList : {
                header:{
                    text:'<s:message code="accessDeniedList.dialog.edit.header" javaScriptEscape="true" />',
                    icon: lockIcon
                },
                narrative:{
                    summary:'<s:message code="accessDeniedList.edit.summary" javaScriptEscape="true" />'
                },
                successMessage: '<s:message code="accessDeniedList.edit.success.message" javaScriptEscape="true" />',
                labels:Y.merge(commonLabels,{
                    availableUsers:'<s:message code="accessDeniedList.edit.available.securityBodies.users" javaScriptEscape="true" />',
                    assignedUsers:'<s:message code="accessDeniedList.edit.assigned.securityBodies.users" javaScriptEscape="true" />',
                    availableTeams:'<s:message code="accessDeniedList.edit.available.securityBodies.teams" javaScriptEscape="true" />',
                    assignedTeams:'<s:message code="accessDeniedList.edit.assigned.securityBodies.teams" javaScriptEscape="true" />'
                }),
                config:{
                    updateUrl: '<s:url value="/rest/securityAccessDeniedRecord/{id}" />',
                    userSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=USER" />',
                    teamSecurityBodiesUrl: '<s:url value="/rest/securityBody?type=TEAM" />'
                }
            },
            removeAccessDeniedList : {
                header:{
                    text:'<s:message code="accessDeniedList.dialog.remove.header" javaScriptEscape="true" />',
                    icon: lockIcon
                },
                narrative:{
                    summary:'<s:message code="accessDeniedList.remove.summary" javaScriptEscape="true" />'
                },
                successMessage: '<s:message code="accessDeniedList.remove.success.message" javaScriptEscape="true" />',
                config:{
                    message: '<s:message code="accessDeniedList.remove.confirmMessage" javaScriptEscape="true" />',
                    deleteUrl: '<s:url value="/rest/securityAccessDeniedRecord/{id}" />'
                }
            },
            recordOverride : {
              message : '<s:message code="dialog.recordOverride" javaScriptEscape="true" />'
            }
        }
     };

    var personLockHandler = new Y.app.PersonLockHandler({
        dialogConfig:personLockDialogConfig,
        urls: {
            securedPersonUrl: '<s:url value="/rest/person/{id}"/>',
            accessRecordUrl: '<s:url value="/rest/securityAccessRecord/{id}" />',
            accessAllowedUrl: '<s:url value="/rest/securityAccessAllowedRecord" />',
            accessDeniedUrl: '<s:url value="/rest/securityAccessDeniedRecord" />',
            accessAllowedForSubjectUrl:'<s:url value="/rest/securityAccessAllowedRecord?subjectId={subjectId}&subjectType={subjectType}"/>',
            accessDeniedForSubjectUrl:'<s:url value="/rest/securityAccessDeniedRecord?subjectId={subjectId}&subjectType={subjectType}"/>',
            recordLockOverrideUrl: '<s:url value="/rest/securityAccessRecord/{id}?securityAccessRecordType={type}"/>'
        },
        permissions:{
            canAddSecurityAccessAllowedRecord: '${canAddSecurityAccessAllowedRecord}' === 'true',
            canUpdateSecurityAccessAllowedRecord: '${canUpdateSecurityAccessAllowedRecord}' === 'true',
            canDeleteSecurityAccessAllowedRecord: '${canDeleteSecurityAccessAllowedRecord}' === 'true',
            canAddSecurityAccessDeniedRecord: '${canAddSecurityAccessDeniedRecord}' === 'true',
            canUpdateSecurityAccessDeniedRecord: '${canUpdateSecurityAccessDeniedRecord}' === 'true',
            canDeleteSecurityAccessDeniedRecord: '${canDeleteSecurityAccessDeniedRecord}' === 'true',
            isSubjectAccessAllowed:'${securedSubjectContextVO.isSubjectAccessAllowed}' ==='true',
            canAddLock: '${canAddSecurityAccessAllowedRecord}' === 'true'||'${canAddSecurityAccessDeniedRecord}' === 'true',
            canLockOverride :  '${canOverrideRecordLocks}' === 'true'
        },
        subject:{
            subjectType: 'person',
            subjectName: '${esc:escapeJavaScript(person.name)}',
            subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
            subjectId: '<c:out value="${person.id}"/>'
        }
    });

    var belongingsPermissions={
      canAddBelongings:'${canAddBelongings}'==='true',
      canView:'${canViewBelongings}'==='true',
      canReturn:'${canReturnBelongings}'==='true',
      canDestroy:'${canDestroyBelongings}'==='true',
      canHistory:'${canViewBelongings}'==='true',
      canMove:'${canMoveBelongings}'==='true',
      canUpdate:'${canUpdateBelongings}'==='true'
    };
    var belongingsSearchConfig = {
      title: '<s:message code="belongings.find.results" javaScriptEscape="true"/>',
      sortBy: [{dateReceived: 'desc'}],
      labels: {
          reference: '<s:message code="belongings.find.results.reference" javaScriptEscape="true"/>',
          description: '<s:message code="belongings.find.results.description" javaScriptEscape="true"/>',
          location: '<s:message code="belongings.find.results.location" javaScriptEscape="true"/>',
          dateReceived: '<s:message code="belongings.find.results.dateReceived" javaScriptEscape="true"/>',
          type: '<s:message code="belongings.find.results.type" javaScriptEscape="true"/>',
          status: '<s:message code="belongings.find.results.status" javaScriptEscape="true"/>',
          destructionDate: '<s:message code="belongings.find.results.destruction" javaScriptEscape="true"/>',
          viewBelongings: '<s:message code="belongings.find.results.view" javaScriptEscape="true"/>',
          moveBelongings: '<s:message code="belongings.find.results.move" javaScriptEscape="true"/>',
          returnBelongings: '<s:message code="belongings.find.results.return" javaScriptEscape="true"/>',
          destroyBelongings: '<s:message code="belongings.find.results.destroy" javaScriptEscape="true"/>',
          historyBelongings: '<s:message code="belongings.find.results.history" javaScriptEscape="true"/>',
          updateBelongings: '<s:message code="belongings.find.results.update" javaScriptEscape="true"/>',
          recorder: '<s:message code="newBelongingsVO.recorder" javaScriptEscape="true"/>',
          actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
          destructionScheduled: '<s:message code="belongings.find.results.destructionScheduled" javaScriptEscape="true"/>',
          note: '<s:message code="newBelongingsVO.note" javaScriptEscape="true"/>'
      },
      noDataMessage: '<s:message code="belongings.find.no.results" javaScriptEscape="true"/>',
      permissions: belongingsPermissions,
      url: '<s:url value="/rest/person/{subjectId}/belongings?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>'
    };

    var belonginsCommonLabels = {
          reference: '<s:message code="newBelongingsVO.reference"/>',
          description: '<s:message code="newBelongingsVO.description"/>',
          dateReceived: '<s:message code="newBelongingsVO.dateReceived"/>',
          location: '<s:message code="newBelongingsVO.location"/>',
          status: '<s:message code="newBelongingsVO.status"/>',
          type: '<s:message code="newBelongingsVO.type"/>',
          note: '<s:message code="newBelongingsVO.note"/>',
          dateMoved: '<s:message code="belongingsVO.dateMoved" javaScriptEscape="true" />'
      };

    var belongingsHeaderIcon = 'fa fa-archive';
    var belongingsNarrativeDescription='{{this.owner.name}} ({{this.owner.identifier}})';
    var belongingsDialogConfig = {
          views: {
            addBelongings: {
              header: {
                text: '<s:message code="belongings.dialog.add.header" javaScriptEscape="true"/>',
                icon: belongingsHeaderIcon
              },
              narrative: {
                summary: '<s:message code="belongings.add.summary" javaScriptEscape="true" />',
                description:'{{this.subject.name}} ({{this.subject.personIdentifier}})'
              },
              successMessage: '<s:message code="belongings.add.success.message" javaScriptEscape="true"/>',
              labels: belonginsCommonLabels,
              config: {
                url: '<s:url value="/rest/belongings"/>'
              }
            },
            viewBelongings: {
              header: {
                text: '<s:message code="belongings.dialog.view.header" javaScriptEscape="true"/>',
                icon: belongingsHeaderIcon
              },
              narrative: {
                summary: '<s:message code="belongings.view.summary" javaScriptEscape="true" />',
                description:belongingsNarrativeDescription
              },
              labels: Y.merge(belonginsCommonLabels,{
                  destructionDate:'<s:message code="belongingsVO.destructionDate" javaScriptEscape="true" />',
                  dateReturned:'<s:message code="belongingsVO.dateReturned" javaScriptEscape="true" />',
                  receiverName:'<s:message code="belongingsVO.receiverName" javaScriptEscape="true" />',
                  dateMoved:'<s:message code="belongingsVO.dateMoved" javaScriptEscape="true" />',
                  receiverType:'<s:message code="belongingsVO.receiverType" javaScriptEscape="true" />',
                  receiverTypePerson:'<s:message code="belongingsVO.receiverType.person" javaScriptEscape="true" />',
                  receiverTypeOrganisation:'<s:message code="belongingsVO.receiverType.organisation" javaScriptEscape="true" />'
              }),
              config: {
                url: '<s:url value="/rest/belongings/{id}"/>'
              }
            },
            historyBelongings: {
              header: {
                text: '<s:message code="belongings.dialog.history.header" javaScriptEscape="true"/>',
                icon: belongingsHeaderIcon
              },
              labels: belonginsCommonLabels,
              config: Y.merge(belongingsSearchConfig,{
                url: '<s:url value="/rest/belongingsSnapshot?belongingsId={id}&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
                sortBy: [{landingDate: 'asc'}],
              })
            },
            updateBelongings: {
              header: {
                text: '<s:message code="belongings.dialog.update.header" javaScriptEscape="true"/>',
                icon: belongingsHeaderIcon
              },
              narrative: {
                summary: '<s:message code="belongings.update.summary" javaScriptEscape="true" />',
                description:belongingsNarrativeDescription
              },
              successMessage: '<s:message code="belongings.update.success.message" javaScriptEscape="true"/>',
              labels: belonginsCommonLabels,
              config: {
                url: '<s:url value="/rest/belongings/{id}"/>'
              }
            },
            moveBelongings: {
              header: {
                text: '<s:message code="belongings.dialog.move.header" javaScriptEscape="true"/>',
                icon: belongingsHeaderIcon
              },
              narrative: {
                summary: '<s:message code="belongings.move.summary" javaScriptEscape="true" />',
                description:belongingsNarrativeDescription
              },
              successMessage: '<s:message code="belongings.move.success.message" javaScriptEscape="true"/>',
              labels: Y.merge(belonginsCommonLabels,{
                  currentLocation:'<s:message code="belongingsVO.location" javaScriptEscape="true" />',
                  location: '<s:message code="updateMoveBelongingsVO.location" javaScriptEscape="true" />',
                  dateMoved: '<s:message code="updateMoveBelongingsVO.dateMoved" javaScriptEscape="true" />'
              }),
              config: {
                url: '<s:url value="/rest/belongings/{id}"/>',
                updateUrl: '<s:url value="/rest/belongings/{id}/status?action=moved"/>'
              }
            },
            destroyBelongings: {
                header: {
                  text: '<s:message code="belongings.dialog.destroy.header" javaScriptEscape="true"/>',
                  icon: belongingsHeaderIcon
                },
                narrative: {
                  summary: '<s:message code="belongings.destroy.summary" javaScriptEscape="true" />',
                  description:belongingsNarrativeDescription
                },
                successMessage: '<s:message code="belongings.destroy.success.message" javaScriptEscape="true"/>',
                labels: Y.merge(belonginsCommonLabels,{
                    destructionDate:'<s:message code="updateDestroyBelongingsVO.destructionDate" javaScriptEscape="true" />'
                }),
                config: {
                  url: '<s:url value="/rest/belongings/{id}"/>',
                  updateUrl: '<s:url value="/rest/belongings/{id}/status?action=destroyed"/>'
                }
              },
              returnBelongings: {
                  header: {
                    text: '<s:message code="belongings.dialog.return.header" javaScriptEscape="true"/>',
                    icon: belongingsHeaderIcon
                  },
                  narrative: {
                    summary: '<s:message code="belongings.return.summary" javaScriptEscape="true" />',
                    description:belongingsNarrativeDescription
                  },
                  successMessage: '<s:message code="belongings.return.success.message" javaScriptEscape="true"/>',
                  labels: Y.merge(belonginsCommonLabels,{
                      receivingSubjectType:'<s:message code="updateReturnBelongingsVO.receivingSubjectType" javaScriptEscape="true" />',
                      receivingSubjectId:'<s:message code="updateReturnBelongingsVO.receivingSubjectId" javaScriptEscape="true" />',
                      dateReturned:'<s:message code="updateReturnBelongingsVO.dateReturned" javaScriptEscape="true" />',
                      subjectType:{
                          person:'<s:message code="updateReturnBelongingsVO.receivingSubjectType.person" javaScriptEscape="true" />',
                          organisation:'<s:message code="updateReturnBelongingsVO.receivingSubjectType.organisation" javaScriptEscape="true" />'
                      },
                      person:{
                          receivingSubjectId:'<s:message code="updateReturnBelongingsVO.receivingSubjectId.person" javaScriptEscape="true" />',
                          placeholder: '<s:message code="belongings.personSearchPlaceholder" javaScriptEscape="true" />'
                      },
                      organisation:{
                          receivingSubjectId:'<s:message code="updateReturnBelongingsVO.receivingSubjectId.organisation" javaScriptEscape="true" />',
                          placeholder: '<s:message code="belongings.organisationSearchPlaceholder" javaScriptEscape="true" />'
                      },
                      currentLocation:'<s:message code="belongingsVO.location" javaScriptEscape="true" />'
                  }),
                  config: {
                    url: '<s:url value="/rest/belongings/{id}"/>',
                    updateUrl: '<s:url value="/rest/belongings/{id}/status?action=returned"/>',
                    personAutocompleteUrl: '<s:url value="/rest/person?nameOrId={query}&lifeState=ALIVE&s={sortBy}&pageSize={maxResults}&appendWildcard=true"><s:param name="sortBy" value="[{\"name\":\"asc\"}]" /></s:url>',
                    organisationAutocompleteUrl: '<s:url value="/rest/organisation?escapeSpecialCharacters=true&appendWildcard=true&nameOrOrgIdOrPrevName={query}&escapeSpecialCharacters=true&appendWildcard=true&pageSize={maxResults}"/>',
                    validationLabels:{
                        receivingSubjectTypeErrorMessage:'<s:message code="updateReturnBelongingsVO.validation.receivingSubjectType" javaScriptEscape="true" />',
                        receivingSubjectIdPersonErrorMessage:'<s:message code="updateReturnBelongingsVO.validation.receivingSubjectId.person" javaScriptEscape="true" />',
                        receivingSubjectIdOrganisationErrorMessage:'<s:message code="updateReturnBelongingsVO.validation.receivingSubjectId.organisation" javaScriptEscape="true" />'

                    }
                  }
                }

        }
      };

    
    var educationPermissions= {
    		canView:'${canViewEducation}'==='true',
    		canUpdateEducation:'${canUpdateEducation}'==='true'
    };
    
    var educationCommonLabels = {
    		keyStage: '<s:message code="newPersonEducationVO.keyStage" javaScriptEscape="true"/>',
    		nationalCurriculumOffset: '<s:message code="newPersonEducationVO.nationalCurriculumOffset" javaScriptEscape="true"/>',
    		nationalCurriculumYear: '<s:message code="newPersonEducationVO.nationalCurriculumYear" javaScriptEscape="true"/>',
    	    noDataMessage:'<s:message code="educationrequest.find.no.results" javaScriptEscape="true"/>',
            noEducationMessage: '<s:message code="education.noEducationMessage" javaScriptEscape="true"/>'
    };
    
    var educationHeaderIcon = 'fa fa-graduation-cap';
    var educationNarrativeDescription='{{this.subject.name}} ({{this.subject.personIdentifier}})';
    var educationDialogConfig = {
        views: {
            updateEducation: {
                header: {
                  text: '<s:message code="education.dialog.update.header" javaScriptEscape="true"/>',
                  icon: educationHeaderIcon
                },
                narrative: {
                  summary: '<s:message code="education.update.summary" javaScriptEscape="true" />',
                  description:educationNarrativeDescription
                },
                successMessage: '<s:message code="education.update.success.message" javaScriptEscape="true"/>',
                labels: educationCommonLabels,
                config: {
                  url: '<s:url value="/rest/person/{subjectId}/education"/>',
                 createUrl:'<s:url value="/rest/person/{personId}/education"/>',
                  updateUrl:'<s:url value="/rest/personEducation/{id}"/>'
                },
                subject:{
                    subjectType: 'person',
                    subjectName: '${esc:escapeJavaScript(person.name)}',
                    subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                    subjectId: '<c:out value="${person.id}"/>'
                }
           },
       }
    };

    var subjectAccessRequestPermissions={
      canAddSubjectAccessRequest:'${canAddSubjectAccessRequest}'==='true',
      canView:'${canViewSubjectAccessRequest}'==='true',
    };
    var subjectAccessRequestSearchConfig = {
      title: '<s:message code="subjectaccessrequest.find.results" javaScriptEscape="true"/>',
      sortBy: [{id: 'desc'}],
      labels: {
          startDate: '<s:message code="subjectaccessrequest.find.results.startDate" javaScriptEscape="true"/>',
          status: '<s:message code="subjectaccessrequest.find.results.status" javaScriptEscape="true"/>',
          actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
          downloadSubjectAccessRequest: '<s:message code="subjectAccessRequestVO.download" javaScriptEscape="true" />',
          failSubjectAccessRequest: '<s:message code="subjectAccessRequestVO.fail" javaScriptEscape="true" />',
          successfullySetAsFailed: '<s:message code="subjectAccessRequestVO.fail.success.message" javaScriptEscape="true" />'
      },
      noDataMessage: '<s:message code="subjectaccessrequest.find.no.results" javaScriptEscape="true"/>',
      permissions: subjectAccessRequestPermissions,
      url: '<s:url value="/rest/person/{subjectId}/subjectAccessRequest?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
    };

    var subjectAccessRequestCommonLabels = {
          status: '<s:message code="subjectAccessRequestVO.status"/>',
          startDate: '<s:message code="subjectAccessRequestVO.startDate" javaScriptEscape="true" />'
      };

    var subjectAccessRequestHeaderIcon = 'fa fa-folder';
    var subjectAccessRequestNarrativeDescription='{{this.owner.name}} ({{this.owner.identifier}})';
    var subjectAccessRequestDialogConfig = {
          views: {
            addSubjectAccessRequest: {
              header: {
                text: '<s:message code="subjectaccessrequest.dialog.add.header" javaScriptEscape="true"/>',
                icon: subjectAccessRequestHeaderIcon
              },
              narrative: {
                summary: '<s:message code="subjectaccessrequest.add.summary" javaScriptEscape="true" />',
                description:'{{this.subject.name}} ({{this.subject.personIdentifier}})'
              },
              successMessage: '<s:message code="subjectaccessrequest.add.success.message" javaScriptEscape="true"/>',
              labels: subjectAccessRequestCommonLabels,
              config: {
                url: '<s:url value="/rest/subjectAccessRequest"/>'
              }
            }
        }
      };

    
    var addressPermissions={
            canAddAddress:'${canAddAddress}'==='true',
            canEnd:'${canCloseAddress}'==='true',
            canRemove:'${canRemoveAddress}'==='true',
            canEndAdd:'${canCloseAddAddress}'==='true',
            canView:'${canViewAddress}'==='true',
            canUpdateAddressStartEnd:'${canUpdateAddressStartEnd}'==='true',
            canUpdateAddressReopen:'${canUpdateAddressReopen}'==='true',
            canUpdateResidentLocation:'${canUpdateResidentLocation}'==='true',
            canSetAddressDoNotDisclose:'${canSetAddressDoNotDisclose}'==='true',
            canSetAddressDisclose:'${canSetAddressDisclose}'==='true'
      };
      var addressSearchConfig = {
        title: '<s:message code="belongings.find.results" javaScriptEscape="true"/>',
        sortBy: [{'startDate!calculatedDate': 'desc'}, {'endDate!calculatedDate': 'desc'}],
        labels: {        	
            address: '<s:message code="person.address.address" javaScriptEscape="true"/>',
            unknownLocation: '<s:message code="person.address.unknownLocation" javaScriptEscape="true"/>',
            roomDescription:'<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>',
            floorDescription:'<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
            doNotDisclose: '<s:message code="person.address.doNotDisclose" javaScriptEscape="true"/>',
            startDate: '<s:message code="person.address.startdate" javaScriptEscape="true"/>',
            endDate: '<s:message code="person.address.enddate" javaScriptEscape="true"/>',
            type: '<s:message code="person.address.type" javaScriptEscape="true"/>',
            usage: '<s:message code="person.address.usage" javaScriptEscape="true"/>',
            actions: '<s:message code="common.column.actions" javaScriptEscape="true"/>',
            endAddress: '<s:message code="person.address.find.results.close" javaScriptEscape="true"/>',
            moveAddress: '<s:message code="person.address.find.results.move" javaScriptEscape="true"/>',
            moveAddressLocation: '<s:message code="person.address.find.results.moveAddressLocation" javaScriptEscape="true"/>',
            removeAddress: '<s:message code="person.address.find.results.remove" javaScriptEscape="true"/>',
            updateAddressDates: '<s:message code="person.address.find.results.updateAddressStartEnd" javaScriptEscape="true"/>',
            updateAddressReopen: '<s:message code="person.address.find.results.updateAddressReopen" javaScriptEscape="true"/>'            
        },
        noDataMessage: '<s:message code="person.address.no.results" javaScriptEscape="true"/>',
        permissions: addressPermissions,
        url: '<s:url value="/rest/person/{subjectId}/address?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
        doNotDiscloseURLs: {
          setAddressDoNotDiscloseURL: '<s:url value="/rest/address/{id}/status?action=doNotDisclose"/>',
          setAddressDiscloseURL: '<s:url value="/rest/address/{id}/status?action=disclose"/>'
        }
      };

    var personDetailsView = new Y.app.PersonDetailsView({
        container: '#identification',
        model: personDetailsModel,
        referenceNumberConfig: {
            title: '<s:message code="person.referenceNumber.view.title"/>',
            closed: true,
            personLinkUrl : '<s:url value="/person?id={id}"/>',
            labels: {
                nationalInsurance: '<s:message code="person.referenceNumber.view.nationalInsurance" javaScriptEscape="true"/>',
                driversLicence: '<s:message code="person.referenceNumber.view.driversLicence" javaScriptEscape="true"/>',
                passport: '<s:message code="person.referenceNumber.view.passport" javaScriptEscape="true"/>',
                addButton: '<s:message code="button.add" javaScriptEscape="true"/>',
                editButton: '<s:message code="button.edit" javaScriptEscape="true"/>',
                referenceNumberType: '<s:message code="person.referenceNumber.referenceNumberType" javaScriptEscape="true"/>',
                number: '<s:message code="person.referenceNumber.number" javaScriptEscape="true"/>',
                addReferenceNumberMessage: '<s:message code="person.referenceNumber.add.narrative" javaScriptEscape="true"/>',
                addReferenceNumberHeader: '<s:message code="person.referenceNumber.add.header" javaScriptEscape="true"/>'
            },
            canAddReferenceNumber: '${canAddReferenceNumber}' === 'true',
            canRemoveReferenceNumber: '${canRemoveReferenceNumber}' === 'true',
            canEditReferenceNumber: '${canEditReferenceNumber}' === 'true',
            canViewReferenceNumber: '${canViewReferenceNumber}' === 'true'
        },
        personDetailsConfig: {
            title: '<s:message code="person.identificationPanel.title"/>',
            closed: false,
            pictureUrl: '<s:url value="rest/personPicture/{pictureId}/content"/>',
             
            labels: {
                personId: '<s:message code="person.personId" javaScriptEscape="true"/>',
                name: '<s:message code="person.name" javaScriptEscape="true"/>',
                otherNames: '<s:message code="person.otherNames" javaScriptEscape="true"/>',
                middleNames: '<s:message code="person.middleNames" javaScriptEscape="true"/>',
                knownAs: '<s:message code="person.knownAs" javaScriptEscape="true"/>',
                alternateNames: '<s:message code="person.alternateNames" javaScriptEscape="true"/>',
                gender: '<s:message code="person.gender" javaScriptEscape="true"/>',
                genderIdentity: '<s:message code="person.genderIdentity" javaScriptEscape="true"/>',
                sexualOrientation: '<s:message code="person.sexualOrientation" javaScriptEscape="true"/>',
                genderIdentitySelfDefined: '<s:message code="person.genderIdentitySelfDefined" javaScriptEscape="true"/>',
                dateOfBirth: '<s:message code="person.dateOfBirth" javaScriptEscape="true"/>',
                died: '<s:message code="person.died" javaScriptEscape="true"/>',
                dueDate: '<s:message code="person.dueDate" javaScriptEscape="true"/>',
                notCarriedToTermDate: '<s:message code="person.notCarriedToTermDate" javaScriptEscape="true"/>',
                notCarriedToTermDateAndReason: '<s:message code="person.notCarriedToTermDateAndReason" javaScriptEscape="true"/>',
                ethnicity: '<s:message code="person.ethnicity" javaScriptEscape="true"/>',
                religion: '<s:message code="person.religion" javaScriptEscape="true"/>',
                countryOfBirth: '<s:message code="person.countryOfBirth" javaScriptEscape="true"/>',
                citizenship: '<s:message code="person.citizenships.citizenship" javaScriptEscape="true"/>',
                startDate: '<s:message code="person.citizenships.startDate" javaScriptEscape="true"/>',
                endDate: '<s:message code="person.citizenships.endDate" javaScriptEscape="true"/>',
                nhsNumber: '<s:message code="person.nhsNumber" javaScriptEscape="true"/>',
                chiNumber: '<s:message code="person.chiNumber" javaScriptEscape="true"/>',
                personType: '<s:message code="person.personType" javaScriptEscape="true"/>',
                professionalTitle: '<s:message code="person.professional.title" javaScriptEscape="true"/>',
                organisation: '<s:message code="person.professional.organisation" javaScriptEscape="true"/>',
                previousNames: '<s:message code="person.identificationPanel.previousNames" javaScriptEscape="true"/>',
                aliases: '<s:message code="person.identificationPanel.aliases" javaScriptEscape="true"/>',
                changeName: '<s:message code="person.identificationPanel.changeName" javaScriptEscape="true"/>',
                changeNameLink: '<s:message code="person.identificationPanel.changeNameLink" javaScriptEscape="true"/>',
                changeNameSummaryBeginning: '<s:message code="person.identificationPanel.changeName.summary.beginning" javaScriptEscape="true"/>',
                changeNameSummaryEnding: '<s:message code="person.identificationPanel.changeName.summary.ending" javaScriptEscape="true"/>',
                changeNamePopupTitle: '<s:message code="person.identificationPanel.changeName.popup.title" javaScriptEscape="true"/>',
                changeNamePopupContent: '<s:message code="person.identificationPanel.changeName.popup.content" javaScriptEscape="true"/>',
                previousNameRemoveLink: '<s:message code="person.identificationPanel.previousNameLink" javaScriptEscape="true"/>',
                previousNamePopupTitle: '<s:message code="person.identificationPanel.previousName.popup.title" javaScriptEscape="true"/>',
                previousNamePopupContent: '<s:message code="person.identificationPanel.previousName.popup.content" javaScriptEscape="true"/>',
                previousNameRemoveConfirm: '<s:message code="person.identificationPanel.previousName.removeConfirm" javaScriptEscape="true"/>',
                changeNameSummaryBeginning: '<s:message code="person.identificationPanel.changeName.summary.beginning" javaScriptEscape="true"/>',
                changeNameSummaryEnding: '<s:message code="person.identificationPanel.changeName.summary.ending" javaScriptEscape="true"/>',
                changeUnbornStatus: '<s:message code="person.identificationPanel.changeUnbornStatus" javaScriptEscape="true"/>',
                changeUnbornStatusLink: '<s:message code="person.identificationPanel.changeUnbornStatusLink" javaScriptEscape="true"/>',
                changeUnbornStatusSummaryBeginning: '<s:message code="person.identificationPanel.changeUnbornStatus.summary.beginning" javaScriptEscape="true"/>',
                changeUnbornStatusSummaryEnding: '<s:message code="person.identificationPanel.changeUnbornStatus.summary.ending" javaScriptEscape="true"/>',
                changeUnbornStatusPopupTitle: '<s:message code="person.identificationPanel.changeUnbornStatus.popup.title" javaScriptEscape="true"/>',
                changeUnbornStatusPopupContent: '<s:message code="person.identificationPanel.changeUnbornStatus.popup.content" javaScriptEscape="true"/>',
                changeNotCarriedStatus: '<s:message code="person.identificationPanel.changeNotCarriedStatus" javaScriptEscape="true"/>',
                changeNotCarriedStatusLink: '<s:message code="person.identificationPanel.changeNotCarriedStatusLink" javaScriptEscape="true"/>',
                changeNotCarriedStatusSummaryBeginning: '<s:message code="person.identificationPanel.changeNotCarriedStatus.summary.beginning" javaScriptEscape="true"/>',
                changeNotCarriedStatusSummaryEnding: '<s:message code="person.identificationPanel.changeNotCarriedStatus.summary.ending" javaScriptEscape="true"/>',
                changeNotCarriedStatusSuccessMessage: '<s:message code="person.identificationPanel.changeNotCarriedStatus.success.message" javaScriptEscape="true"/>',
                changeNotCarriedStatusPopupTitle: '<s:message code="person.identificationPanel.changeNotCarriedStatus.popup.title" javaScriptEscape="true"/>',
                changeNotCarriedStatusPopupContent: '<s:message code="person.identificationPanel.changeNotCarriedStatus.popup.content" javaScriptEscape="true"/>',
                changeAliveStatus: '<s:message code="person.identificationPanel.changeAliveStatus" javaScriptEscape="true"/>',
                changeAliveStatusLink: '<s:message code="person.identificationPanel.changeAliveStatusLink" javaScriptEscape="true"/>',
                changeAliveStatusSummaryBeginning: '<s:message code="person.identificationPanel.changeAliveStatus.summary.beginning" javaScriptEscape="true"/>',
                changeAliveStatusSummaryEnding: '<s:message code="person.identificationPanel.changeAliveStatus.summary.ending" javaScriptEscape="true"/>',
                changeAliveStatusSuccessMessage: '<s:message code="person.identificationPanel.changeAliveStatus.success.message" javaScriptEscape="true"/>',
                changeAliveStatusPopupTitle: '<s:message code="person.identificationPanel.changeAliveStatus.popup.title" javaScriptEscape="true"/>',
                changeAliveStatusPopupContent: '<s:message code="person.identificationPanel.changeAliveStatus.popup.content" javaScriptEscape="true"/>',
                changeTypeLink: '<s:message code="person.identificationPanel.changeTypeLink" javaScriptEscape="true"/>',
                changeTypePopupTitle: '<s:message code="person.identificationPanel.changeType.popup.title" javaScriptEscape="true"/>',
                changeTypePopupContent: '<s:message code="person.identificationPanel.changetype.popup.content" javaScriptEscape="true"/>',
                addTypeLink: '<s:message code="person.identificationPanel.addTypeLink" javaScriptEscape="true"/>',
                addTypePopupTitle: '<s:message code="person.identificationPanel.addType.popup.title" javaScriptEscape="true"/>',
                addTypePopupContent: '<s:message code="person.identificationPanel.addType.popup.content" javaScriptEscape="true"/>',
                changeDeceasedToAliveLink: '<s:message code="person.identificationPanel.changeDeceasedToAliveLink" javaScriptEscape="true"/>',
                changeDeceasedToAlivePopupTitle: '<s:message code="person.identificationPanel.changeDeceasedToAlivePopupTitle" javaScriptEscape="true"/>',
                changeDeceasedToAlivePopupContent: '<s:message code="person.identificationPanel.changeDeceasedToAlivePopupContent" javaScriptEscape="true"/>',
            }, 
            canEdit: '${canEdit}' === 'true',
            canDeletePersonName: '${canDeletePersonName}' === 'true',
            canUpdateDeceasedToAlive: '${canUpdateDeceasedToAlive}' === 'true',
            canViewPersonPicture: '${canViewPersonPicture}' === 'true',
            canAddPersonType: '${canAddPersonType}' === 'true',
        },
        personContactsConfig: {
            title: '<s:message code="person.contactPanel.title"/>',
            closed: true,
            labels: {
                phoneNumber: '<s:message code="person.contactPanel.phoneNumber" javaScriptEscape="true"/>',
                mobileNumber: '<s:message code="person.contactPanel.mobileNumber" javaScriptEscape="true"/>',
                website: '<s:message code="person.contactPanel.website" javaScriptEscape="true"/>',
                email: '<s:message code="person.contactPanel.email" javaScriptEscape="true"/>',
                socialMedia: '<s:message code="person.contactPanel.socialMedia" javaScriptEscape="true"/>',
                fax: '<s:message code="person.contactPanel.fax" javaScriptEscape="true"/>',
                contactVerified: '<s:message code="person.contact.contactVerified" javaScriptEscape="true"/>',
                contactVerifiedDate: '<s:message code="person.contact.contactVerifiedDate" javaScriptEscape="true"/>'
            },
            canAddContact: '${canAddContact}' === 'true',
            canEditContact: '${canEditContact}' === 'true',
            canRemoveContact: '${canRemoveContact}' === 'true',
            canViewContact: '${canViewContact}' === 'true'
        },
        personLanguagesConfig: {
            title: '<s:message code="person.languagePanel.title"/>',
            closed: true,
            labels: {
                firstLanguage: '<s:message code="person.languagePanel.firstLanguage" javaScriptEscape="true"/>',
                otherLanguages: '<s:message code="person.languagePanel.otherLanguages" javaScriptEscape="true"/>',
                proficiencyInOfficialLanguage: '<s:message code="person.languagePanel.proficiencyInOfficialLanguage" javaScriptEscape="true"/>',
                interpreterRequired: '<s:message code="person.languagePanel.interpreterRequired" javaScriptEscape="true"/>',
                nonVerbalLanguages: '<s:message code="person.languagePanel.nonVerbalLanguages" javaScriptEscape="true"/>'
            },
            canUpdateLanguage: '${canUpdateLanguage}' === 'true'
        },
        
        personEducationConfig: {
        	title: '<s:message code="person.education.title" javaScriptEscape="true"/>',
        	closed: true,
        	permissions:educationPermissions,
        	dialogConfig:educationDialogConfig,
        	labels: educationCommonLabels,
        	subject:{
                subjectType: 'person',
                subjectName: '${esc:escapeJavaScript(person.name)}',
                subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                subjectId: '<c:out value="${person.id}"/>'
            }
        },
        
        personBelongingsConfig:{
            title: '<s:message code="person.belongings.title" javaScriptEscape="true"/>',
            closed: true,
            permissions:belongingsPermissions,
            searchConfig:belongingsSearchConfig,
            dialogConfig:belongingsDialogConfig,
            addButtonConfig:{
                icon:'fa-plus-circle',
                title:'<s:message code="belongings.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
                ariaLabel:'<s:message code="belongings.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
                label:'<s:message code="button.add" javaScriptEscape="true"/>'
            }
        },
        personSubjectAccessRequestConfig:{
            title: '<s:message code="person.subjectaccessrequest.title" javaScriptEscape="true"/>',
            closed: true,
            permissions:subjectAccessRequestPermissions,
            searchConfig:subjectAccessRequestSearchConfig,
            dialogConfig:subjectAccessRequestDialogConfig,
            addButtonConfig:{
                icon:'fa-plus-circle',
                title:'<s:message code="subjectaccessrequest.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
                ariaLabel:'<s:message code="subjectaccessrequest.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
                label:'<s:message code="button.add" javaScriptEscape="true"/>'
            },
            downloadUrl: '<s:url value="/rest/subjectAccessRequestAttachment/{id}/content"/>',
            failUrl: '<s:url value="/rest/subjectAccessRequest/{id}/status?action=failed"/>'
        },
        personAddressConfig: {
            title: '<s:message code="person.address.address" javaScriptEscape="true"/>',
            closed: true,
            permissions:addressPermissions,
            searchConfig:addressSearchConfig,
            //TODO - when address dialog is refactored - pass in here
            //  dialogConfig:addressDialogConfig,
            addButtonConfig:{
                icon:'fa-plus-circle',
                title:'<s:message code="person.address.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
                ariaLabel:'<s:message code="person.address.add.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
                label:'<s:message code="button.add" javaScriptEscape="true"/>'
            },
            urls: {
            	getUrl: '<s:url value="/rest/personAddress/{id}"/>',
            	reopenUrl: '<s:url value="/rest/personAddress/{id}/reOpen"/>',
            	updateStartEndUrl: '<s:url value="/rest/personAddress/{id}/startAndEndDate"/>',
            	updateAddressUrl: '<s:url value="/rest/personAddress/{id}?allowOverlappingAddresses="/>'
            },
            location: {
                views: {
                    add: {
                        header: {
                            text: '<s:message code="childlookedafter.dialog.add.location.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-envelope fa-inverse fa-stack-1x'
                        },
                        narrative: {
                            summary: '<s:message code="childlookedafter.dialog.add.location.narrative" javaScriptEscape="true" />',
                            narrative: subjectNarrativeDescription
                        },
                        successMessage: '<s:message code="childlookedafter.dialog.add.location.successMessage" javaScriptEscape="true" />',
                        labels: {
                            primaryNameOrNumber: '<s:message code="location.primaryNameOrNumber" javaScriptEscape="true" />',
                            street: '<s:message code="location.street" javaScriptEscape="true" />',
                            locality: '<s:message code="location.locality" javaScriptEscape="true" />',
                            town: '<s:message code="location.town" javaScriptEscape="true" />',
                            county: '<s:message code="location.county" javaScriptEscape="true" />',
                            country: '<s:message code="location.country" javaScriptEscape="true" />',
                            countrySubdivision: '<s:message code="location.countrySubdivision" javaScriptEscape="true" />',
                            postcode: '<s:message code="location.postcode" javaScriptEscape="true" />'
                        },
                        config: {
                            url: '<s:url value="/rest/location"/>'
                        }
                    }
                }
            }
        },
        outputDialogConfig:outputDialogConfig
    });

    personDetailsView.render();

});
</script>