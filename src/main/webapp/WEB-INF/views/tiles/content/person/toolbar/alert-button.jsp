<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<sec:authorize access="hasPermission(${person.id}, 'SecuredPersonContext.GET','PersonAlert.View')" var="canView"/>


<c:if test="${canView eq true}">
  <tilesx:useAttribute name="first" ignore="true"/>
  <tilesx:useAttribute name="last" ignore="true"/>
  <c:if test="${first eq true }">
    <c:set var="fStyle" value="first"/>
  </c:if>
  <c:if test="${last eq true }">
    <c:set var="lStyle" value="last"/>
  </c:if>

  <c:set var="name" value="${person.name}"/>

  <%-- Button markup--%>
  <li class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'>
    <a href="#none" data-button-action="viewAlerts" class="pure-button pure-button-disabled usp-fx-all pure-button-loading"
      title='<s:message code="person.alerts.button" arguments="${name}" htmlEscape="true" />' aria-disabled="true"
      aria-label='<s:message code="person.alerts.button" arguments="${name}" htmlEscape="true" />' tabindex="0">
      <i class="fa fa-exclamation"></i>
      <span><s:message code="button.alerts" /></span>
    </a>
  </li>
  <script>  
    Y.use('yui-base', 'app-toolbar', function(Y){
      var toolbar = new Y.usp.app.AppToolbar({
          permissions: {
              canViewAlerts:'${canView}'==='true'
          }
      });
              
      toolbar.on('viewAlerts', function(){
          Y.fire('viewAlerts');
      });
    });
  </script>
</c:if>
