<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
<c:choose>
	<%-- subject context is person --%>
	<c:when test="${!empty person}">
		<sec:authorize access="hasPermission(${person.id},'SecuredPersonContext.GET','Group.Add')" var="canAdd"/>
	</c:when>
	<%-- subject context is group --%>
	<c:when test="${!empty group}">
		<sec:authorize access="hasPermission(${group.id},'SecuredGroupContext.GET','PersonGroupMembership.ADD')" var="canAdd"/>
	</c:when>
	<%-- no subject in context --%>
	<c:otherwise>
		<sec:authorize access="hasPermission(null,'Group','Group.Add')" var="canAdd"/>
	</c:otherwise>
</c:choose>
</c:if>

<c:if test="${canAdd eq true}">

  <tilesx:useAttribute name="first" ignore="true"/>
  <tilesx:useAttribute name="last" ignore="true"/>
  
  <c:if test="${first eq true }">
    <c:set var="fStyle" value="first"/>
  </c:if>
  
  <c:if test="${last eq true }">
    <c:set var="lStyle" value="last"/>
  </c:if>
  
  <li id="add-button" class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'></li>
  
<script>
   Y.use('yui-base','app-button-model','app-button-trigger', function(Y) {

	   var ACTION = '';
	   var CSS_BTN = 'pure-button-add';                    
	   var CSS_ICON = 'fa fa-plus-circle';        
	   var EVENT = 'group:showDialog';   
	   var LABEL = '<s:message code="button.add" />';    
	   var NAME = "'${esc:escapeJavaScript(group.name)}'" || "'${esc:escapeJavaScript(person.name)}'";    
	   var TITLE = "<s:message code='button.addGroup'/>";
  
	   var group = {        
			   id: "<c:out value='${group.id}'/>",       
			   groupIsActiveHistoric: '<c:out value="${group.isInactiveHistoric}"/>',   
	   };
        
	   var person = {                    
			   id: "<c:out value='${person.id}'/>",                      
	   };
	   
     
	   if(person.id) {             	
		   ACTION = 'addGroupAndMember';               
	   } else if(group.id && group.groupIsActiveHistoric=='false') {                     	
		   ACTION = 'addMembership';
	   } else {                       	
		   ACTION = 'addGroup';    
	   }
    
    
	   var btnAdd = new Y.app.views.ButtonTrigger({        
		     container:'#add-button',                                                            
		     model: new Y.app.ButtonModel({                                                    
            action: ACTION,                                                                                                    
            cssButton: CSS_BTN,                                                  
            cssIcon: CSS_ICON,            
            event: EVENT,          
            label: LABEL,         
            title: Y.Lang.sub(TITLE, {                                                                                                                            
                name: NAME                                                                                              
            })      
		     })                                  
	   });

	   btnAdd.render();

   });
 </script>




</c:if>