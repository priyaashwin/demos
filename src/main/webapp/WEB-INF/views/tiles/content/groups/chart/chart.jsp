

<div class="pure-g-r">
  <div class="pure-u-7-8">
    <h1>${chartFormTitlePrefix}<strong> ${group.name}</strong></h1>
  </div>
  <div class="pure-u-1-8">
    <button id="printButton" class="yui3-button pure-button-primary">Print</button>
  </div>
</div>




<div style="width:90%; height:90%">
	<canvas id="myChart" width="400" height="400"></canvas>
</div>

Completed ${completedString}
<!-- ${completed} -->

<script>
var options = ${options};
var data = ${data};

Y.use('',function(Y){
	var ctx = document.getElementById("myChart").getContext("2d");
	var myChart = new Chart(ctx, {
		type:'radar',
		data:data,
		options:options
		});	
	
	Y.one('#printButton').on('click',function(e){window.print();});
});

</script>
