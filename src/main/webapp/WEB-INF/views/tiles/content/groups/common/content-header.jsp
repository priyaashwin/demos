<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
<%-- if the subject context for the page is person --%>
<c:when test="${!empty person}">
  <c:set var="contextTitle"><s:message code="groups.person.header"/></c:set>
</c:when>
<c:otherwise>
  <c:set var="contextTitle"><s:message code="groups.group.header"/></c:set>
</c:otherwise>
</c:choose>
<h3 tabindex="0">
  <span class="context-title hide-on-search"><c:out value="${contextTitle}"/></span>
  <span class="context-count hide-on-search" id="generalResultsCount"></span>       
</h3>
