<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'Group','Group.Update')" var="canEditGroup"/>
<script>
Y.use(	'yui-base',
		'event-custom',
		'model',
		'usp-group-Group',
		'usp-group-NewGroupWithDate',
		'usp-group-UpdateGroup',
		'usp-group-UpdateCloseGroup',
		'usp-group-UpdateCloseGroupMembership',
		'usp-group-UpdateCloseGroupToday',
		'usp-person-PersonGroupMembershipDetails',
		'usp-person-NewPersonGroupMembership',
		'usp-person-PersonGroupMembership',
		'handlebars-helpers',
		'handlebars-group-templates',
		'multi-panel-popup',
		'multi-panel-model-errors-plugin',
		'group-dialog-views',
		'model-form-link',
		'model-transmogrify',
		'categories-group-component-CloseReason',
		'categories-group-component-GroupType',
		'categories-group-component-LeaveReason',
		'info-message',
		'escape',
		'usp-date'
	,function(Y){

	var L=Y.Lang,
		A=Y.Array,
		E=Y.Escape;

    var personContextDetails = {
            id:'<c:out value="${person.id}"/>',
            personId:'${esc:escapeJavaScript(person.personIdentifier)}',
            forename:'${esc:escapeJavaScript(person.forename)}',
            surname:'${esc:escapeJavaScript(person.surname)}',
            name:'${esc:escapeJavaScript(person.name)}'
    };

 	var NewGroupForm = Y.Base.create("NewGroupAndMemberForm", Y.usp.group.NewGroupWithDate, [Y.usp.ModelFormLink],{
		url:'<s:url value="/rest/group/"/>',
		form:'#groupAddForm'
    });

 	var NewGroupAndMemberForm = Y.Base.create("NewGroupAndMemberForm", Y.usp.group.NewGroupWithDate, [Y.usp.ModelFormLink],{
		url:'<s:url value="/rest/person/'+'${person.id}'+'/group"/>',
		form:'#groupAddForm'
    });

 	//This model will be transformed into a UpdateGroup model when it is submitted back to the server
 	var UpdateGroupForm=Y.Base.create("UpdateGroupForm", Y.usp.group.Group, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
		url:'<s:url value="/rest/group/{id}"/>',
		form:'#groupEditForm'
    });

  var CloseGroupForm=Y.Base.create("CloseGroupForm", Y.usp.group.Group, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
    url:'<s:url value="/rest/group/{id}"/>',
		form: '#groupCloseForm',
		//Provide our own implementation of getURL to update URL on save
		getURL:function(action, options){
			if(action==='update'){
				return this._substituteURL('<s:url value="/rest/group/{id}/status?action=close&checkInactive=true"/>', Y.merge(this.getAttrs(), options));
			}
			//return standard operation
			return CloseGroupForm.superclass.getURL.apply(this, [action, options]);
		}
  	});

  var AddGroupMemberForm=Y.Base.create("AddGroupMemberForm", Y.usp.person.NewPersonGroupMembership, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
  	url:'<s:url value="/rest/personGroupMembership"/>',
  	form: '#groupAddMemberForm'
    });

  var CloseGroupMembershipForm=Y.Base.create("CloseGroupMembershipForm", Y.usp.person.PersonGroupMembershipDetails, [Y.usp.ModelFormLink, Y.usp.ModelTransmogrify],{
  	url:'<s:url value="/rest/personGroupMembership/{id}"/>',
  	headers:	{
    	    			'Content-Type'	:	'application/json',
    	    			'Accept'		:	'application/vnd.olmgroup-usp.Person.PersonGroupMembershipDetails+json'
    		  		},
    	form: '#groupCloseMembershipForm',
    	getURL:function(action,options){
        	if(action==='update'){
        		return this._substituteURL('<s:url value="/rest/personGroupMembership/{id}/status?action=close&checkInactive=true"/>', Y.merge(this.getAttrs(), options));
        	}
        	//return standard operation
        	return CloseGroupMembershipForm.superclass.getURL.apply(this, [action, options]);
    	}
   	});

  var commonLabels={
    	id: "<s:message code='group.id'/>",
			name: "<s:message code='group.name'/>",
			description: "<s:message code='group.description'/>",
			type: "<s:message code='group.type'/>",
			startDate: "<s:message code='group.startDate'/>",
			closeDate: "<s:message code='group.closeDate'/>",
			closeReason: "<s:message code='group.closeReason'/>",
			personSearchPlaceholder: "<s:message code='group.personSearchPlaceholder'/>",
			personSearch: "<s:message code='group.personSearch'/>",
			deleteConfirmMain : "<s:message code='group.deleteConfirmMain'/>",
			deleteConfirmSub  : "<s:message code='group.deleteConfirmSub'/>",
			endConfirmMain : "<s:message code='group.endConfirmMain'/>",
			addGroupMessage: "<s:message code='group.addGroupMessage'/>",
			addGroupAndMemberMessage :"<s:message code='group.addGroupAndMember'/>",
			endGroupMessage: "<s:message code='group.endGroupMessage'/>",
			removeGroupMessage: "<s:message code='group.removeGroupMessage'/>",
			viewGroupMessage: "<s:message code='group.viewGroupMessage'/>",
			editGroupMessage: "<s:message code='group.editGroupMessage'/>",
			addGroupHeader: "<s:message code='group.header.add'/>",
			viewGroupHeader: "<s:message code='group.header.view'/>",
			editGroupHeader: "<s:message code='group.header.edit'/>",
			endGroupHeader: "<s:message code='group.header.end'/>",
			removeGroupHeader: "<s:message code='group.header.remove'/>",
			addMemberHeader: "<s:message code='groupmembership.header.add'/>",
			removeMemberHeader: "<s:message code='groupmembership.header.remove'/>",
			endMemberHeader: "<s:message code='groupmembership.header.end'/>",
			openGroupSummaryMain:  "<s:message code='group.openGroupSummaryMain' javaScriptEscape="true"/>",
			openGroupSummarySub:  "<s:message code='group.openGroupSummarySub' javaScriptEscape="true"/>",
			openGroupHeader:  "<s:message code='group.header.open' javaScriptEscape="true"/>",

			membershipLabels : {
				joinDate : "<s:message code='GroupMembershipVO.joinDate'/>",
				leaveDate : "<s:message code='GroupMembershipVO.leaveDate'/>",
				leaveReason : "<s:message code='GroupMembershipVO.leaveReason'/>",
				endConfirmMainMembership : "<s:message code='groupMembership.endConfirmMainMembership'/>",
				addMembershipMessage : "<s:message code='groupMembership.addMembershipMessage'/>",
				deleteConfirmMainMembership : "<s:message code='groupMembership.deleteConfirmMainMembership'/>",
				selectMembershipMessage : "<s:message code='groupMembership.selectMembershipMessage'/>"
			},

			successMessageLabels :{
				addGroupSuccessMessage : "<s:message code='group.addGroupMessage.success'/>",
				addGroupAndMemberMessage : "<s:message code='group.addGroupAndMemberMessage.success'/>",
				addMemberSuccessMessage : "<s:message code='group.addGroupMemberMessage.success'/>",
				editGroupSuccessMessage : "<s:message code='group.editGroupMessage.success'/>",
				endGroupSuccessMessage : "<s:message code='group.endGroupMessage.success'/>",
				endMemberSuccessMessage : "<s:message code='group.endGroupMemberMessage.success'/>",
				removeMemberSuccessMessage : "<s:message code='group.removeGroupMemberMessage.success'/>",
				removeGroupSuccessMessage : "<s:message code='group.removeGroupMessage.success'/>",
				openGroupSuccessMessage: "<s:message code='group.openGroupMessage.success' javaScriptEscape="true"/>"
			}
    };


  var cancelDialog = function(e) {
	   e.preventDefault();
	   myPopup.hide();
  };

  var addGroupSubmit = function(e){
	  var _instance = this;
	  e.preventDefault();

	  //show the mask
    this.showDialogMask();

    // Get the view and the model
    var view=this.get('activeView'),
    		model=view.get('model');
  			model.setAttrs({
  	},{
  		//don't trigger the onchange listener as we will re-render after save
  		silent:true
  	});
   	// Do the save
    model.save(function(err, res){

    //hide the mask - note, this happens regardless of whether an error occurs
    _instance.hideDialogMask();

      		if (err){
					if(err.code !== 400){
						Y.log(err.code + err.msg);
					}
      		}else{
                myPopup.hide();
			      	Y.fire('infoMessage:message',{
  			        	message:commonLabels.successMessageLabels.addGroupSuccessMessage,
  			        	delayed:true
  			    	});
  	          		// Load the screen with the new person
	  			    var newGroupId = model.toJSON().id;
	  			    window.location=L.sub('<s:url value="/groups/group?id={groupId}" />',{groupId:newGroupId});
              }
      });
	};

  var addGroupAndMemberSubmit = function(e){
	  	var _instance = this;
	  	e.preventDefault();

	  	//show the mask
      	this.showDialogMask();

        // Get the view and the model
        var view=this.get('activeView'),
        	model=view.get('model');
      	 	model.setAttrs({
    	},{
    		//don't trigger the onchange listener as we will re-render after save
    		silent:true
    	});
     	// Do the save
        model.save(function(err, res){

        		//hide the mask - note, this happens regardless of whether an error occurs
	          	_instance.hideDialogMask();

        		if (err){
					if(err.code !== 400){
						Y.log(err.code + err.msg);
					}
        		}else{
                    myPopup.hide();
    		        Y.fire('group:dataChanged');
    		        Y.fire('infoMessage:message',{
    		            message:L.sub(commonLabels.successMessageLabels.addGroupAndMemberMessage,{
    		            	forename:E.html(personContextDetails.forename),
    		            	surname:E.html(personContextDetails.surname),
    		            	personIdentifier:E.html(personContextDetails.personId)
   		            	})
    		        });
                }
        });
	};

	var editGroupOpen = function(e){
		// Stop the event's default behavior
	    e.preventDefault();
	 	// Get the view and the model
        var view=this.get('activeView'),
        	model=view.get('model');
        	myPopup.hide();
        	Y.fire('group:showDialog', {
        		id: model.get('id'),
            	objectVersion: model.get('objectVersion'),
        		action: 'edit'
        });
	};

	var editGroupSubmit = function(e) {
		var _instance = this;
	  	e.preventDefault();

	  	//show the mask
      	this.showDialogMask();

        // Get the view and the model
        var view=this.get('activeView');
        var model=view.get('model');
        model.save({
        		transmogrify:Y.usp.group.UpdateGroup
        	},
			function(err, res){
        		//hide the mask - note, this happens regardless of whether an error occurs
	          	_instance.hideDialogMask();
        		if (err){
					if(err.code !== 400){
						Y.log(err.code + err.msg);
					}
        		}
        		else {
                    myPopup.hide();
    		        Y.fire('group:dataChanged', {
    		        	action: 'edit'
    		        });
    		        Y.fire('infoMessage:message',{
    		            message:commonLabels.successMessageLabels.editGroupSuccessMessage
    		        });
                }
            });
    };

	var closeGroupSubmit = function(e) {
		var _instance = this;
	  	e.preventDefault();

	  	//show the mask
      	this.showDialogMask();

		var view=this.get('activeView'),
		model=view.get('model');
		model.save({
			transmogrify:Y.usp.group.UpdateCloseGroupToday
		},function (err,response){
			//hide the mask - note, this happens regardless of whether an error occurs
          	_instance.hideDialogMask();
			if (err){
				if(err.code !== 400){
					Y.log(err.code + err.msg);
				}
        	}else{
				myPopup.hide();
		        Y.fire('group:dataChanged');
		        Y.fire('infoMessage:message',{
		            message:commonLabels.successMessageLabels.endGroupSuccessMessage
		        });
			}
		});
	};

	var addGroupMemberSubmit = function(e){
		var _instance = this;
	  	e.preventDefault();

	  	//show the mask
      	this.showDialogMask();

        // Get the view and the model
        var view=this.get('activeView'),
        	model=view.get('model'),
        	personAutoCompleteView=view.personAutoCompleteResultView,
        	selectedPerson=personAutoCompleteView.get('selectedPerson')||{};


	        model.setAttrs({
	    		personId: selectedPerson.id,
	    		groupId: parseInt(view.get('targetGroupId'))
	    	},{
    		//don't trigger the onchange listener as we will re-render after save
    		silent:true
    	});

        // Do the save
        model.save(function(err, res){
        		//hide the mask - note, this happens regardless of whether an error occurs
          		_instance.hideDialogMask();

        		if (err){
					if(err.code !== 400){
						Y.log(err.code + err.msg);
					}
        		}else{
                    myPopup.hide();
    		        Y.fire('infoMessage:message',{
    		        	message:L.sub(commonLabels.successMessageLabels.addMemberSuccessMessage,{
    		        		forename:E.html(selectedPerson.forename||''),
    		        		surname:E.html(selectedPerson.surname||''),
    		        		personIdentifier:E.html(selectedPerson.personIdentifier||'')
  		        		})
    		        });
                    Y.fire('group:dataChanged');
                }
        });

	};

	var closeGroupMembershipSubmit = function(e) {
		var _instance = this;
	  	e.preventDefault();

	  	//show the mask
      	this.showDialogMask();

 		var view=this.get('activeView'),
			model=view.get('model');
			model.save({
				transmogrify:Y.usp.group.UpdateCloseGroupMembership
		},function (err,response){
			//hide the mask - note, this happens regardless of whether an error occurs
      		_instance.hideDialogMask();
        	if (err){
				if(err.code !== 400){
					Y.log(err.code + err.msg);
				}
        	}else{
				myPopup.hide();
		        Y.fire('group:dataChanged');
		        Y.fire('infoMessage:message',{
		        	message:commonLabels.successMessageLabels.endMemberSuccessMessage
		        });
			}
		});
	};

	var deleteGroupMembershipSubmit = function(e) {
		e.preventDefault();
		var view=this.get('activeView'),
		model=view.get('model');
		model.destroy({remove:true},
		function (err,response){
			if(err===null){
				myPopup.hide();
		        Y.fire('group:dataChanged');
		        Y.fire('infoMessage:message',{
		        	message:commonLabels.successMessageLabels.removeMemberSuccessMessage
		        });
 			}
		});
	};

	var deleteGroupSubmit = function(e) {
		e.preventDefault();
		var view=this.get('activeView'),
		model=view.get('model');
		model.destroy({remove:true},
		function (err,response){
			if(err===null){
				myPopup.hide();
		        Y.fire('group:dataChanged');
		        Y.fire('infoMessage:message',{
		        	message:commonLabels.successMessageLabels.removeGroupSuccessMessage
		        });
 			}
		});
	};

var openGroupSubmit = function(e) {
	var _instance = this;
	e.preventDefault();
	this.showDialogMask();
	var view=this.get('activeView'),
			model=view.get('model');
	var doOpenModel = new Y.usp.Model({
			url:'<s:url value="/rest/group/{id}/status?action=open"/>',
			id:model.get('id')
			});
	doOpenModel.save(function (err,response){
									_instance.hideDialogMask();
									if (err){
										if(err.code !== 400){
											Y.log(err.code + err.msg);
										}
									}else{
										myPopup.hide();
										Y.fire('group:dataChanged');
										Y.fire('infoMessage:message',{
											message:commonLabels.successMessageLabels.openGroupSuccessMessage
										});
									}
		});
	};


	var openGroupOpen = function(e){
		// Stop the event's default behavior
			e.preventDefault();
		// Get the view and the model
				var view=this.get('activeView'),
					model=view.get('model');
					myPopup.hide();
					Y.fire('group:showDialog', {
						id: model.get('id'),
							objectVersion: model.get('objectVersion'),
						action: 'open'
				});
	};

	var myPopup=new Y.usp.MultiPanelPopUp({
		headerContent:'<h3 tabindex="0"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span> Group Details </h3>',
		width:560,
		//Plugin Model Error handling
		plugins:[{fn:Y.Plugin.usp.MultiPanelModelErrorsPlugin}],
		// Views that this dialog supports
		views: {

		 /**
      * Add Group
      * ==========
      */

      addGroup: {

      	type:Y.app.group.NewGroupWithDateView,
	  		headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.addGroupHeader+'</h3>',
	  		buttons:[
          {
        	   action: addGroupSubmit,
        	   section: Y.WidgetStdMod.FOOTER,
             name: 'saveButton',
             labelHTML: '<i class="fa fa-check"></i> Save',
             classNames: 'pure-button-primary',
             disabled: true
          },
          {
             action:    cancelDialog,
             section:   Y.WidgetStdMod.FOOTER,
             name:      'cancelButton',
             labelHTML: '<i class="fa fa-times"></i> cancel',
             classNames:'pure-button-secondary'
          }
        ],
      },

		 /**
       * Add Group and member
       * ==========
       */

       addGroupAndMember: {
       	type:Y.app.group.NewGroupWithDateView,
 	  		headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.addGroupHeader+'</h3>',
 	  		buttons:[
           {
         	   action: addGroupAndMemberSubmit,
         	   section: Y.WidgetStdMod.FOOTER,
              name: 'saveButton',
              labelHTML: '<i class="fa fa-check"></i> Save',
              classNames: 'pure-button-primary',
              disabled: true
           },
           {
              action:    cancelDialog,
              section:   Y.WidgetStdMod.FOOTER,
              name:      'cancelButton',
              labelHTML: '<i class="fa fa-times"></i> cancel',
              classNames:'pure-button-secondary'
           }
         ]
       },

      /**
       * View Group
       * ==========
       * 1. More button temporarily functions as a cancel.
       * 2. More button css temporarily pure-button-active - likely to have an inactive state represented by a different colour.
       */

      viewGroup: {
     		type: Y.app.group.ViewGroupView,
   			headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.viewGroupHeader+'</h3>',
   			buttons:[
					{
						action:     editGroupOpen,
						section:    Y.WidgetStdMod.FOOTER,
						name:       'editButton',
						labelHTML:  '<i class="fa fa-pencil-square-o"></i> Edit',
						classNames: 'pure-button-active',
						disabled:   true
					},
			          {
			            action:     cancelDialog,
			            section:    Y.WidgetStdMod.FOOTER,
			            name:       'cancelButton',
			            labelHTML:  '<i class="fa fa-times"></i> cancel',
			            classNames: 'pure-button-secondary'
			          }
			  ]
      },


	   /**
	    * Edit Group
	    * ==========
	    */

	   	editGroup: {
				type:Y.app.group.UpdateGroupView,
				headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.editGroupHeader+'</h3>',
				buttons:[
					{
						action:    editGroupSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
						disabled:  true
					},
	        {
	          action:   cancelDialog,
	          section:    Y.WidgetStdMod.FOOTER,
	          name:      'cancelButton',
	          labelHTML: '<i class="fa fa-times"></i> cancel',
	          classNames:'pure-button-secondary'
	        }
				]
			},

			/**
 	    * Open Group
 	    * ==========
 	    */

 	   	openGroup: {
 				type:Y.app.group.OpenGroupView,
 				headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.openGroupHeader+'</h3>',
 				buttons:[
 					{
 						action:    openGroupSubmit,
 						section:   Y.WidgetStdMod.FOOTER,
 						name:      'yesButton',
 						labelHTML: '<i class="fa fa-check"></i> reopen',
 						classNames:'pure-button-primary',
 						disabled:  false
 					},
 					{
 			      action:    cancelDialog,
 			      section:   Y.WidgetStdMod.FOOTER,
 			      name:      'noButton',
 			      labelHTML: '<i class="fa fa-times"></i> cancel',
 			      classNames:'pure-button-secondary'
 			    }
 				]
 	    },



	   /**
	    * Close Group
	    * ===========
	    */

	    closeGroup: {
	     	type:  Y.app.group.UpdateCloseGroupView,
	 			headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.endGroupHeader+'</h3>',
	 			buttons:[
					{
						action:    closeGroupSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'yesButton',
						labelHTML: '<i class="fa fa-check"></i> yes',
						classNames:'pure-button-primary',
						disabled:  true
					},
					{
	          action:    cancelDialog,
	          section:   Y.WidgetStdMod.FOOTER,
	          name:      'noButton',
	          labelHTML: '<i class="fa fa-times"></i> no',
	          classNames:'pure-button-secondary'
			    }
				]
	   	},


	   /**
	    * Delete Group
	    * ============
	    */

	   	deleteGroup: {
				type:Y.app.group.DeleteGroupView,
				headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.removeGroupHeader+'</h3>',
				buttons:[
					{
						action:    deleteGroupSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'yesButton',
						labelHTML: '<i class="fa fa-check"></i> yes',
						classNames:'pure-button-primary',
						disabled:  true
					},
					{
			      action:    cancelDialog,
			      section:   Y.WidgetStdMod.FOOTER,
			      name:      'noButton',
			      labelHTML: '<i class="fa fa-times"></i> no',
			      classNames:'pure-button-secondary'
			    }
				]
	    },


     /**
      * Add Group Member
      * ================
      */

      addGroupMember: {
				type:Y.app.group.NewPersonGroupMembershipView,
				headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.addMemberHeader+'</h3>',
				buttons:[
          {
						action:    addGroupMemberSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'saveButton',
						labelHTML: '<i class="fa fa-check"></i> Save',
						classNames:'pure-button-primary',
					  disabled:  true
				  },
				  {
			      action:     cancelDialog,
			      section:    Y.WidgetStdMod.FOOTER,
			      name:      'cancelButton',
			      labelHTML: '<i class="fa fa-times"></i> cancel',
			      classNames:'pure-button-secondary'
				  }
        ]
     	},

 	   /**
 	    * Delete Group Membership
 	    * =======================
 	    */

 	   	deleteGroupMembership: {
 				type:Y.app.group.DeleteMembershipGroupView,
 				headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.removeMemberHeader+'</h3>',
 				buttons:[
 					{
 						action:    deleteGroupMembershipSubmit,
 						section:   Y.WidgetStdMod.FOOTER,
 						name:      'yesButton',
 						labelHTML: '<i class="fa fa-check"></i> yes',
 						classNames:'pure-button-primary',
 						disabled:  true
 					},
 					{
 			      action:    cancelDialog,
 			      section:   Y.WidgetStdMod.FOOTER,
 			      name:      'noButton',
 			      labelHTML: '<i class="fa fa-times"></i> no',
 			      classNames:'pure-button-secondary'
 			    }
 				]
 	    },

      /**
       * Close Group Membership
       * ======================
       */

     	closeGroupMembership: {
     	  type:Y.app.group.UpdateCloseMembershipGroupView,
     	  headerTemplate:'<h3 tabindex="0" class="iconic-title"><span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-users fa-inverse fa-stack-1x"></i></span>'+commonLabels.endMemberHeader+'</h3>',
				buttons: [
          {
						action:    closeGroupMembershipSubmit,
						section:   Y.WidgetStdMod.FOOTER,
						name:      'yesButton',
						labelHTML: '<i class="fa fa-check"></i> yes',
						classNames:'pure-button-primary',
						disabled:  true
					},
					{
						action:    cancelDialog,
            section:   Y.WidgetStdMod.FOOTER,
            name:      'noButton',
            labelHTML: '<i class="fa fa-times"></i> no',
            classNames:'pure-button-secondary'
				  }
				]
     	}
		}
	});


	//render that baby
	myPopup.render();
	Y.on('groupDialog:render', function(e) {
		this.align();
	}, myPopup);
	//respond to custom events
	Y.on('group:showDialog', function(e) {

		var group = {
				id: '${group.id}',
				identifier: '${esc:escapeJavaScript(group.groupIdentifier)}',
				isActiveHistoric: '${group.isInactiveHistoric}',
				name: "${esc:escapeJavaScript(group.name)}",
				startDateMilliseconds: '${groupStartDateMilliseconds}'
		};


		switch(e.action) {

		case 'addGroup':

			this.showView('addGroup', {
				model: new NewGroupForm(),
				labels: commonLabels
			}, function(view){
				this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false); //Get the button by name out of the footer and enable it.
			});

			break;

		case 'addMembership':

			this.showView('addGroupMember', {
				model:new AddGroupMemberForm(),
				labels:commonLabels,
		    targetGroupId: group.id,
		    targetGroupName: group.name,
		    targetGroupIdentifier: group.identifier,
		    targetGroupStartDate: new Date(parseInt(group.startDateMilliseconds)).getTime(),
		    autocompleteURL: '<s:url value="/rest/person?nameOrId={query}&pageSize={maxResults}&appendWildcard=true"/>',
		  }, function(view){
			  this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
		  });

			break;




		case 'addGroupAndMember':
			this.showView('addGroupAndMember',{
				  model: new NewGroupAndMemberForm(),
				  labels: commonLabels,
				  otherData:{
					  person:personContextDetails,
					  newGroupAndMemberForm:true
				  }
	       }, {}, function(view){
				this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false); //Get the button by name out of the footer and enable it.
			});
		break;

		case 'view':
			model = new Y.usp.group.Group({url:'<s:url value="/rest/group/{id}"/>'});
			this.showView('viewGroup', {
	   			model:model,
				labels:commonLabels,
				groupMembersURL: e.groupMembersURL,
				groupId : e.id
			}, {modelLoad:true, payload:{id:e.id}}, function(view){
				// Hide the edit button if the group is inactiveHistoric
				if(model.get('isInactiveHistoric') || '${canEditGroup}' === 'false' || '${readOnly}' === 'true'){
					this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('hidden',true);
				}else{
				//Get the button by name out of the footer and enable it.
					this.getButton('editButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				}
			});

break;

		case 'edit' :
			this.showView('editGroup', {
				//create new model instance
				model:new UpdateGroupForm(),
				//labels for template
				labels:commonLabels
			}, {modelLoad:true, payload:{id:e.id}}, function(view){
				//Get the button by name out of the footer and enable it.
				this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			});
		break;


		case 'close':
			var activeMembers  = new Y.usp.person.PaginatedPersonGroupMembershipList({url:'<s:url value="/rest/group/'+e.id+'/personMembership?pageNumber=1&pageSize=-1&temporalStatusFilter=ACTIVE"/>'});
			var pendingMembers = new Y.usp.person.PaginatedPersonGroupMembershipList({url:'<s:url value="/rest/group/'+e.id+'/personMembership?pageNumber=1&pageSize=-1&temporalStatusFilter=INACTIVE_PENDING"/>'});

			activeMembers.load();
			pendingMembers.load();

			var thisVar = this, gId = e.id;

			activeMembers.on('load', function(e){

				thisVar.showView('closeGroup', {
					model:new CloseGroupForm(),
					labels:commonLabels,
					activeMembers: activeMembers,
					pendingMembers: pendingMembers,
					membershipsLink:L.sub('<s:url value="/groups/group?id={groupId}" />',{groupId:gId})

				},{
					modelLoad:true, payload:{id:gId}
				},
				function(){
			     	//Get the button by name out of the footer and enable it.
			     	thisVar.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			   });

			});

break;

case 'open':


	model = new Y.usp.group.Group({url:'<s:url value="/rest/group/{id}"/>'});
 	gId = e.id;
	this.showView('openGroup', {
				model: model ,
				//labels for template
			labels:commonLabels
		},
		{modelLoad:true, payload:{id:gId}},
				function(view){
			//Get the button by name out of the footer and enable it.
			this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			//Show the no button
			this.getButton('noButton', Y.WidgetStdMod.FOOTER).show();
		});

break;
		case 'delete':

			var activeMembers  = new Y.usp.person.PaginatedPersonGroupMembershipList({url:'<s:url value="/rest/group/'+e.id+'/personMembership?pageNumber=1&pageSize=-1&temporalStatusFilter=ACTIVE"/>'});
			var pendingMembers = new Y.usp.person.PaginatedPersonGroupMembershipList({url:'<s:url value="/rest/group/'+e.id+'/personMembership?pageNumber=1&pageSize=-1&temporalStatusFilter=INACTIVE_PENDING"/>'});

			activeMembers.load();
			pendingMembers.load();

			model = new Y.usp.group.Group({url:'<s:url value="/rest/group/{id}"/>'});
			var thisVar = this, gId = e.id;

			activeMembers.on('load', function(e){
				thisVar.showView('deleteGroup', {
	    			model: model ,
	    			//labels for template
					labels:commonLabels,
					activeMembers: activeMembers,
					pendingMembers: pendingMembers,
					membershipsLink:L.sub('<s:url value="/groups/group?id={groupId}" />',{groupId:gId})
				},
				{modelLoad:true, payload:{id:gId}},
	         	function(view){
					//Get the button by name out of the footer and enable it.
					thisVar.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
					//Show the no button
					thisVar.getButton('noButton', Y.WidgetStdMod.FOOTER).show();
				});
			});

break;
		case 'deleteMembership':

			model = new Y.usp.person.PersonGroupMembershipDetails({url:'<s:url value="/rest/personGroupMembership/{id}"/>',
		    headers:	{
	    					'Content-Type'	:	'application/json',
	    					'Accept'		:	'application/vnd.olmgroup-usp.Person.PersonGroupMembershipDetails+json'
		  					}
			});

			this.showView('deleteGroupMembership', {
	   			model: model ,
	   			//labels for template
				labels:commonLabels,
			},
			{modelLoad:true, payload:{id:e.id}},
	        function(view){
				//Get the button by name out of the footer and enable it.
				this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
				//Show the no button
				this.getButton('noButton', Y.WidgetStdMod.FOOTER).show();
			});
		break;

		case 'endMembership':

			this.showView('closeGroupMembership',{
				model: new CloseGroupMembershipForm(),
				labels:commonLabels
			}, {modelLoad:true, payload:{id:e.id}}, function(){

				//Get the button by name out of the footer and enable it.
				this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);

				//Show the no button
				this.getButton('noButton', Y.WidgetStdMod.FOOTER).show();
			});
			break;
		}Y.one("body").delegate('submit',function(e){e.preventDefault();},'form[action="#none"]');
	}, myPopup);

});
</script>
