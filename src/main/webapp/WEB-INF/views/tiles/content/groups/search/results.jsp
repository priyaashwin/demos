<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'Group','Group.View')" var="canViewGroup"/>
<sec:authorize access="hasPermission(null, 'Person','Person.View')" var="canViewPerson"/>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'Group','Group.Update')" var="canEditGroup"/>
	<sec:authorize access="hasPermission(null, 'Group','Group.Close')" var="canCloseGroup"/>
	<sec:authorize access="hasPermission(null, 'Group','Group.Delete')" var="canDeleteGroup"/>
	<sec:authorize access="hasPermission(null, 'Group','Group.Open')" var="canOpenGroup"/>
	<sec:authorize access="hasPermission(null, 'PersonGroupMembership','PersonGroupMembership.Close')" var="canClosePersonGroupMembership"/>
	<sec:authorize access="hasPermission(null, 'PersonGroupMembership','PersonGroupMembership.Delete')" var="canRemovePersonGroupMembership"/>
</c:if>

<div id="groupResults"></div>

<script>
Y.use('group-controller-view', function(Y) {
  <c:choose>
  <c:when test="${not empty group}">
  var subject = {
    subjectType: 'group',
    subjectName: '${esc:escapeJavaScript(group.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(group.groupIdentifier)}',
    subjectId: '<c:out value="${group.id}"/>'
  };
  </c:when>
  <c:otherwise>
  var subject = {
    subjectType: 'person',
    subjectName: '${esc:escapeJavaScript(person.name)}',
    subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
    subjectId: '<c:out value="${person.id}"/>',
  };
  </c:otherwise>
</c:choose>

  var permissions = {
    canViewGroup: '${canViewGroup}' === 'true',
    canEditGroup: '${canEditGroup}' === 'true',
    canCloseGroup: '${canCloseGroup}' === 'true',
    canDeleteGroup: '${canDeleteGroup}' === 'true',
    canViewPerson: '${canViewPerson}' === 'true',
    canClosePersonGroupMembership: '${canClosePersonGroupMembership}' === 'true',
    canRemovePersonGroupMembership: '${canRemovePersonGroupMembership}' === 'true',
    canViewGroups: '${canViewPerson}' === 'true',
    canViewMembers: '${canViewGroup}' === 'true',
		canOpenGroup: '${canOpenGroup}' === 'true'
  };

  var groupSearchConfig = {
    sortBy: [{
      'groupVO!groupType': 'asc'
    }, {
      'groupVO!name': 'asc'
    }, {
      'groupVO!startDate': 'desc'
    }, {
      'groupVO!closeDate': 'desc'
    }],
    labels: {
      actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',
      closeDate: '<s:message code="person.group.general.results.closeDate" javaScriptEscape="true" />',
      id: '<s:message code="person.group.general.results.id" javaScriptEscape="true" />',
      name: '<s:message code="person.group.general.results.name" javaScriptEscape="true" />',
      description: '<s:message code="person.group.general.results.description" javaScriptEscape="true" />',
      startDate: '<s:message code="person.group.general.results.startDate" javaScriptEscape="true" />',
      type: '<s:message code="person.group.general.results.groupType" javaScriptEscape="true" />',
      leaveDate: '<s:message code="person.group.general.membership.results.leaveDate" javaScriptEscape="true" />',
      joinDate: '<s:message code="person.group.general.membership.results.joinDate" javaScriptEscape="true" />',
      membersView: 'View group members',
      groupSecuredLabel: '<s:message code="person.group.general.results.groupSecuredLabel" javaScriptEscape="true" />',
      edit:'Edit group',
      editTitle:'Edit this group',
      view:'View group',
      viewTitle:'View this group',
			open:'Reopen group',
			openTitle:'Reopen this group',
      close:'End group',
      closeTitle:'End this group',
      deleteGroup:'Remove group',
      deleteGroupTitle:'Remove this group',
      endMembership:'End membership',
      endMembershipTitle: 'End membership of this group',
      roomDescription:'<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>',
      floorDescription:'<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
      removeMembership:'Remove membership',
      removeMembershipTitle: 'Remove membership of this group'
    },
    noDataMessage: '<s:message code="person.group.general.no.results" javaScriptEscape="true" />',
    permissions: permissions,
    url: '<s:url value="/rest/{subjectType}/{id}/groupMembership?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
    resultsCountNode: '#generalResultsCount'
  };

  var membersSearchConfig = {
    sortBy: [{
      'personVO!name': 'asc'
    }, {
      'joinDate': 'desc'
    }],
    labels: {
      actions: '<s:message code="common.column.actions" javaScriptEscape="true" />',
      id: '<s:message code="person.group.membership.results.id" javaScriptEscape="true" />',
      name: '<s:message code="person.group.membership.results.name" javaScriptEscape="true" />',
      leaveDate: '<s:message code="person.group.membership.results.endDate" javaScriptEscape="true" />',
      joinDate: '<s:message code="person.group.membership.results.startDate" javaScriptEscape="true" />',
      type: '<s:message code="person.group.general.results.groupType" javaScriptEscape="true" />',
      viewPersonGroups: 'View person groups',
      endMembership:'End membership',
      endMembershipTitle: 'End membership of this group',
      removeMembership:'Remove membership',
      roomDescription:'<s:message code="person.address.add.roomDescription" javaScriptEscape="true"/>',
      floorDescription:'<s:message code="person.address.add.floorDescription" javaScriptEscape="true"/>',
      removeMembershipTitle: 'Remove membership of this group'
    },
    permissions: permissions,
    url: '<s:url value="/rest/{subjectType}/{id}/personMembership?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
    resultsCountNode: '#generalResultsCount'
  };

  var config = {
    container: '#groupResults',
    membersSearchConfig: membersSearchConfig,
    groupSearchConfig: groupSearchConfig,
    subject: subject || {},
    permissions: permissions,
    groupMembersUrl: '<s:url value="/rest/group/{id}/personMembership?s=%5B%7B%22personVO!name%22%3A%22asc%22%7D%2C%7B%22joinDate%22%3A%22desc%22%7D%5D&pageNumber=-1&pageSize=-1"/>',
    personGroupsUrl: '<s:url value="/groups/person?id={personId}" />',
    groupsUrl: '<s:url value="/groups/group?id={groupId}" />'
  };

  var groupView = new Y.app.group.GroupControllerView(config).render();
});

</script>
