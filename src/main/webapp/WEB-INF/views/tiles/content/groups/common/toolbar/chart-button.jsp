<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<c:choose>
	<c:when test="${canChart eq true}">
	
	<c:choose>
		<%-- subject context is group --%>
		<c:when test="${!empty group}">
			<sec:authorize access="hasPermission(null, 'Group','Group.View')" var="canViewGroup"/>
		</c:when>
	</c:choose>
	
	<c:if test="${canViewGroup eq true}">
	
	  <tilesx:useAttribute name="first" ignore="true"/>
	  <tilesx:useAttribute name="last" ignore="true"/>
	  
	  <c:if test="${first eq true }">
	    <c:set var="fStyle" value="first"/>
	  </c:if>
	  
	  <c:if test="${last eq true }">
	    <c:set var="lStyle" value="last"/>
	  </c:if>
	  
	  <li id="chart-button" class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'></li>
	  
	<script>
	   Y.use('yui-base','app-button-model','app-button-trigger', function(Y) {
	     var L=Y.Lang;
		   var ACTION = '';
		   var CSS_BTN = 'pure-button-chart';                    
		   var CSS_ICON = 'fa fa-bar-chart';        
		   var EVENT = 'group:showChart';
		   var LABEL = '<s:message code="group.chartButton.label" />'; 
		   var NAME = "'${esc:escapeJavaScript(group.name)}'" || "'${esc:escapeJavaScript(person.name)}'";    
		   var TITLE = '<s:message code='group.chartButton.title'/>'; 
	  	   
		   var group = {
				   id: "<c:out value='${group.id}'/>",       
				   groupIsActiveHistoric: '<c:out value="${group.isInactiveHistoric}"/>',   
		   };
	
		   if(group.id && group.groupIsActiveHistoric=='false') {                     	
			   ACTION = 'addMembership';
		   } else {                       	
			   ACTION = 'addGroup';    
		   }
	
		   var btnAdd = new Y.app.views.ButtonTrigger({        
			     container:'#chart-button',                                                            
			     model: new Y.app.ButtonModel({                                                    
	            action: 'viewChart',                                                                                                    
	            cssButton: CSS_BTN,                                                  
	            cssIcon: CSS_ICON,            
	            event: EVENT,          
	            label: LABEL,         
	            title: Y.Lang.sub(TITLE, {                                                                                                                            
	                name: NAME                                                                                              
	            })      
			     })                                  
		   });
	
		   btnAdd.render();
	
		   
		    // Handle group charts button - launch a new page to display the chart
		    var groupId = '${group.id}'; 
		    var groupChartURL = '<s:url value="/groups/chart?id={id}" />';   
		    Y.on("group:showChart", function(e){
		      if(groupId){
		          window.open( 
		                L.sub(groupChartURL,{id:groupId}),  
		                'group_'+groupId,
		                'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=780,height=768,left=100,top=100'
		            );      
		      }
		    });
	   });
	   
	 </script>
	
	</c:if>
	
	</c:when>
</c:choose>