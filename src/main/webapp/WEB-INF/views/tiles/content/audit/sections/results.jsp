<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<sec:authorize access="hasPermission(null, 'PersonAccess','PersonAccess.View')" var="canViewPersonAccess" />

<div id="auditAccordions"></div>

<script>
Y.use('audit-controller-view', function(Y) {
		
		var permissions = {
				canFilter: '${canViewPersonAccess}' === 'true',
				canPersonAccessDownload: '${canViewPersonAccess}' === 'true',
				canEventLogDownload: '${canViewPersonAccess}' === 'true'
		}
		
    var personAccessFilterContextConfig = {
    	    labels: {
    	      filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
    	      resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
    	      resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>',
    	      type:'<s:message code="person.relationship.find.filter.relationshipTypes.active" javaScriptEscape="true"/>',
    	      date:'<s:message code="person.relationship.find.filter.date.active" javaScriptEscape="true"/>',
            clientName: '<s:message code="audit.results.accessedPersonName" javaScriptEscape="true"/>',
            userName: '<s:message code="audit.results.accessorName" javaScriptEscape="true"/>',
    	      stateActiveMessage:'<s:message code="person.relationship.filter.stateFilterSelectedInfo" javaScriptEscape="true"/>',
    	      dateActiveMessage:'<s:message code="person.relationship.filter.dateFilterSelectedInfo" javaScriptEscape="true"/>'
    	    },
    	    container:'#personAccessFilterContext'
    	  };
    	  
     var personAccessFilterConfig = {
		labels: {
          filterBy: '<s:message code="filter.by" javaScriptEscape="true"/>',
          dateFrom:'<s:message code="person.relationship.find.filter.entryDateRangeStart" javaScriptEscape="true"/>',
          dateTo:'<s:message code="person.relationship.find.filter.entryDateRangeEnd" javaScriptEscape="true"/>',
          clientName: '<s:message code="audit.results.accessedPersonName" javaScriptEscape="true"/>',
          userName: '<s:message code="audit.results.accessorName" javaScriptEscape="true"/>',
          resetTitle:'<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
          reset:'<s:message code="filter.reset" javaScriptEscape="true"/>',
          validation:{
            dateTo:'<s:message code="person.relationship.find.filter.endBeforeStartDate" javaScriptEscape="true"/>'
          }
		},
		urls: {
			personAutocompleteURL:'<s:url value="/rest/person?appendWildcard=true&nameOrId={query}"/>'
		}
     };
	
    var personAccessAccordionConfig = {
    		permissions: permissions,
        filterConfig: personAccessFilterConfig,
        filterContextConfig: personAccessFilterContextConfig,
    		searchConfig: {
	        sortBy: [{accessDate: 'desc'}],
	        labels: {
	        	  accessDate: '<s:message code="audit.results.accessDate" javaScriptEscape="true"/>',
	        	  accessedPersonName: '<s:message code="audit.results.accessedPersonName" javaScriptEscape="true"/>',
	        	  accessorName: '<s:message code="audit.results.accessorName" javaScriptEscape="true"/>',
	        	  exportTitle:'<s:message code="audit.export.csv" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
	          	exportAriaLabel:'<s:message code="audit.personaccess.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
	          	exportLabel:'<s:message code="audit.personaccess.button" javaScriptEscape="true"/>',
	            filterTitle:'<s:message code="button.filter" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
	            filterAriaLabel:'<s:message code="button.filter" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
	            filterLabel:'<s:message code="button.filter" javaScriptEscape="true"/>'
	        },
	        noDataMessage: '<s:message code="audit.no.results" javaScriptEscape="true"/>',
	        url: '<s:url value="/rest/personAccess?s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
	        urlCSV: '<s:url value="/rest/personAccess/csv?pageNumber=-1&pageSize=-1"/>'
    		},
    		title: '<s:message code="audit.personaccess.title" javaScriptEscape="true"/>',
        tablePanelConfig:{
           title: '<s:message code="audit.personaccess.title" javaScriptEscape="true" />'
        }
    };

    var eventLogFilterContextConfig = {
    	    labels: {
    	      filter: '<s:message code="filter.active" javaScriptEscape="true"/>',
    	      resetAllTitle: '<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
    	      resetAll: '<s:message code="filter.resetAll" javaScriptEscape="true"/>',
    	      type:'<s:message code="person.relationship.find.filter.relationshipTypes.active" javaScriptEscape="true"/>',
    	      date:'<s:message code="person.relationship.find.filter.date.active" javaScriptEscape="true"/>',
    	      eventCode: '<s:message code="audit.results.eventCode" javaScriptEscape="true"/>',
            clientName: '<s:message code="audit.results.accessedPersonName" javaScriptEscape="true"/>',
            userName: '<s:message code="audit.results.accessorName" javaScriptEscape="true"/>',
    	      stateActiveMessage:'<s:message code="person.relationship.filter.stateFilterSelectedInfo" javaScriptEscape="true"/>',
    	      dateActiveMessage:'<s:message code="person.relationship.filter.dateFilterSelectedInfo" javaScriptEscape="true"/>'
    	    },
    	    container:'#eventLogFilterContext'
    	  };
		  
	var eventLogFilterConfig = {
		labels: {
			dateFrom: '<s:message code="person.relationship.find.filter.entryDateRangeStart" javaScriptEscape="true"/>',
			dateTo: '<s:message code="person.relationship.find.filter.entryDateRangeEnd" javaScriptEscape="true"/>',
			eventCode: '<s:message code="audit.results.eventCode" javaScriptEscape="true"/>',
			clientName: '<s:message code="audit.results.accessedPersonName" javaScriptEscape="true"/>',
			userName: '<s:message code="audit.results.accessorName" javaScriptEscape="true"/>',
			resetTitle:'<s:message code="filter.resetAll.title" javaScriptEscape="true"/>',
			reset:'<s:message code="filter.reset" javaScriptEscape="true"/>',
			autoCompletePlaceholder: '<s:message code="audit.autoCompletePlaceholder" javaScriptEscape="true"/>',
			autoCompletePlaceholderSecurityUser: '<s:message code="audit.autoCompletePlaceholderSecurityUser" javaScriptEscape="true"/>',
			filterBy: '<s:message code="filter.by" javaScriptEscape="true"/>',
			validation:{
				dateTo:'<s:message code="person.relationship.find.filter.endBeforeStartDate" javaScriptEscape="true"/>'
			}
		},
		urls: {
			personAutocompleteURL:'<s:url value="/rest/person?appendWildcard=true&nameOrId={query}"/>',
			securityUserAutocompleteURL: '<s:url value="/rest/securityUser?appendWildcard=true&nameOrUserName={query}&securityUserFilter=true"/>'
		}
     };
    
    var eventLogAccordionConfig = {
    		permissions: permissions,
        filterConfig: eventLogFilterConfig,
        filterContextConfig: eventLogFilterContextConfig,
    		searchConfig: {
	        title: '<s:message code="common.find.results" javaScriptEscape="true"/>',
	        sortBy: [{eventDate: 'desc'}],
	        labels: {
	            eventDate: '<s:message code="audit.results.eventDate" javaScriptEscape="true"/>',
	            clientName: '<s:message code="audit.results.clientName" javaScriptEscape="true"/>',
	            eventCode: '<s:message code="audit.results.eventCode" javaScriptEscape="true"/>',
	            eventSummary: '<s:message code="audit.results.eventSummary" javaScriptEscape="true"/>',
	            userName: '<s:message code="audit.results.userName" javaScriptEscape="true"/>',
	            exportTitle:'<s:message code="audit.export.csv" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
		        	exportAriaLabel:'<s:message code="audit.auditlog.button" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
		        	exportLabel:'<s:message code="audit.auditlog.button" javaScriptEscape="true"/>',
		          filterTitle:'<s:message code="button.filter" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
		          filterAriaLabel:'<s:message code="button.filter" arguments="${esc:escapeJavaScript(person.name)}" javaScriptEscape="true"/>',
		          filterLabel:'<s:message code="button.filter" javaScriptEscape="true"/>'
	        },
	        noDataMessage: '<s:message code="audit.no.results" javaScriptEscape="true"/>',
	        url: '<s:url value="/rest/auditEventLogEntry/search?pageNumber={page}&pageSize={pageSize}"/>',
	        urlCSV: '<s:url value="/rest/auditEventLogEntry/csv?pageNumber=-1&pageSize=-1"/>'
    		},
    		title: '<s:message code="audit.auditlog.title" javaScriptEscape="true" />',
        tablePanelConfig:{
           title: '<s:message code="audit.auditlog.title" javaScriptEscape="true" />'
        }
    };
    
    
    var config = {
      container: '#auditAccordions',
      personAccessAccordionConfig: personAccessAccordionConfig,
      eventLogAccordionConfig: eventLogAccordionConfig,
      tablePanelConfig:{
          title: '<s:message code="childprotection.enquiry.page.title" javaScriptEscape="true"/>',
          closed: false
      }
    };

    var auditView = new Y.app.audit.AuditControllerView(config);

    auditView.render();
});
</script>