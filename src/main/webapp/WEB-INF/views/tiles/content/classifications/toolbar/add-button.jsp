<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
	<sec:authorize access="hasPermission(null, 'PersonClassifierAssignment','PersonClassifierAssignment.Update')" var="canAdd"/>
</c:if>

<c:if test="${canAdd eq true}">

	<tilesx:useAttribute name="selected" ignore="true"/>
	<tilesx:useAttribute name="first" ignore="true"/>
	<tilesx:useAttribute name="last" ignore="true"/>

	<c:if test="${selected eq true }">
	  <c:set var="aStyle" value="pure-button-active"/>
	</c:if>

	<c:if test="${first eq true }">
	  <c:set var="fStyle" value="first"/>
	</c:if>

	<c:if test="${last eq true }">
	  <c:set var="lStyle" value="last"/>
	</c:if>

	<li id="add-button" class='<c:out value="${fStyle}"/> <c:out value="${lStyle}"/>'></li>

	<script>
		Y.use('yui-base','app-button-model','app-button-trigger', function(Y) {

			var CSS_BTN = 'pure-button-add';
			var CSS_ICON = 'fa fa-plus-circle';
			var EVENT = 'classification:showDialog';
			var ID = '<c:out value="${person.id}"/>';
			var IDENTIFIER = '${esc:escapeJavaScript(person.personIdentifier)}';
			var PERSONID = '<c:out value="${person.id}"/>';
			var LABEL = '<s:message code="button.add" />';
			var NAME = '${esc:escapeJavaScript(person.name)}';
			var TITLE = "<s:message code='person.classifications.add.button'/>";

			var btnAdd = new Y.app.views.ButtonTrigger({
				  container:'#add-button',
				  model: new Y.app.ButtonModel({
					  action: 'addClassification',
					  cssButton: CSS_BTN,
					  cssIcon: CSS_ICON,
					  event: EVENT,
					  label: LABEL,
					  title: Y.Lang.sub(TITLE, {
						  name: NAME
					  })
				  })
			});

      btnAdd.payload({
    	  subject: {
    		  id: ID,
    		  identifier: IDENTIFIER,
    		  personId: PERSONID,
    	    name: NAME,
    	    type: 'person'
    	  }
      });

			btnAdd.render();

		});

	</script>

</c:if>
