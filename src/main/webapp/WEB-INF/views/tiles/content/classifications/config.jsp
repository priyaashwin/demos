<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

    <sec:authorize access="hasPermission(null, 'ClassificationAssignment','ClassificationAssignment.AddNotManuallyAssignable')" var="canAddNotManuallyAssignable"/>

<script>

YUI.add('classification-base', function(Y) {
  
  'use-strict';
  
  var canAddNotManuallyAssignable='${canAddNotManuallyAssignable}' === 'true';

  var LABELS = {
		  
		  autocomplete: {
			  label: '<s:message code="person.classifications.autocomplete.label" javaScriptEscape="true"/>',
			  link: '<s:message code="person.classifications.autocomplete.link" javaScriptEscape="true"/>',
			  placeholder: '<s:message code="person.classifications.autocomplete.placeholder" javaScriptEscape="true"/>'
		  },
		  
		  popover: {
			  
			  autocomplete: {
				  title: '',
				  content: '<s:message code="person.classifications.popover.autocomplete.content" javaScriptEscape="true"/>'
			  }
		  
		  }

  },
  
 
  
  
  URL_ROOT = '<s:url value="/" />',     
  URL_ROOT_REST = '<s:url value="/rest" />',
    
 
  MixinBase = function() {};  
  
  MixinBase.prototype = {
            
      //TODO proper arguments check
		  getLabels: function(name, type) {
    	  
    	  if(arguments.length===2 && LABELS[arguments[0]]) {
    		  return LABELS[arguments[0]][arguments[1]];
    	  }
    	  
    	  if(LABELS[name]) {
    		  return LABELS[name];   		  
    	  }
    	  
    	  return LABELS;
      },
  		  
		  getRESTUrl: function(path) {                      
        return URL_ROOT_REST + (path || '');          
      },
      getCanAddNotManuallyAssignable: function() {
          return this.get('canAddNotManuallyAssignable');
      },
      getRootUrl: function() {
        return URL_ROOT;
      }
      
  };
  
  MixinBase.ATTRS = {
    
      labels: {       
        value: {}       
      },
      
      subject: {          
        value: {}        
      },
      canAddNotManuallyAssignable : {
    	    value : canAddNotManuallyAssignable
      }
  
  };
  

  Y.namespace('app.classification').MixinBase = MixinBase;


  
  
}, '0.0.1', {
    requires: [
      'yui-base',
      'handlebars-base',
      'handlebars-helpers'
      
    ]
    
});
</script>
