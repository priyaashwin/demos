<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<c:set var="canUpdate" value="true" />

<script>

var loginPersonName = '${esc:escapeJavaScript(loginPersonName)}';
var loginPersonId = '${esc:escapeJavaScript(loginPersonId)}';

Y.use('yui-base',
		'event-custom',
		'model',
		'classification-dialog-views',	
		'info-message', function(Y){   	
	
	
	var EVENT_SHOW_DIALOG = 'classification:showDialog';

	// callback when dialog is open	 
	var showViewFn = function(view) {            	        

		switch(view.name) {
		
		case 'addClassification':
		   this.getButton('saveButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			 break;
		 		 
		case 'editClassification':
			 this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			 break;
			 
		case 'deleteClassification':
			 this.getButton('yesButton', Y.WidgetStdMod.FOOTER).set('disabled',false);
			 break;	
		
		}		           
   
	};
	 
	 var personContextDetails = {
       id:'<c:out value="${person.id}"/>',
       personId :'${esc:escapeJavaScript(person.personIdentifier)}',
       forename : '${esc:escapeJavaScript(person.forename)}',
       surname:'${esc:escapeJavaScript(person.surname)}',
       name:'${esc:escapeJavaScript(person.name)}'
   };

   
	var myPopup = new Y.app.classification.MultiPanelPopUp({
		
		commonLabels: {			           
      endDate: '<s:message code="common.column.date.end"/>',
      endClassificationAssignmentConfirm: '<s:message code="person.classifications.endClassificationAssignmentConfirm"/>',
      deleteClassificationAssignmentConfirm: '<s:message code="person.classifications.deleteClassificationAssignmentConfirm"/>',
      endClassificationAssignmentSummary: '<s:message code="person.classifications.endClassificationAssignmentSummary" javaScriptEscape="true"/>',
      deleteClassificationAssignmentSummary: '<s:message code="person.classifications.deleteClassificationAssignmentSummary" javaScriptEscape="true"/>',
      finalLevel: '<s:message code="person.classifications.finalLevel"/>', 
      finalLevelClassification: '<s:message code="person.classification.add.finalLevelClassification"/>',
      groupName: '<s:message code="person.classifications.groupName"/>',
      practitioner: '<s:message code="person.classifications.practitioner"/>',                  
      startDate: '<s:message code="common.column.date.start"/>',
      topLevelClassification: '<s:message code="person.classification.add.topLevelClassification"/>',
      endReason: '<s:message code="common.column.date.endReason"/>'
		},
		
		dialogTitles: {
			addClassification: 'Add classification',
			endClassification: '<s:message code="person.classifications.endClassificationAssignmentHeader" javaScriptEscape="true"/>',
			deleteClassification: '<s:message code="person.classifications.deleteClassificationAssignmentHeader" javaScriptEscape="true"/>'
		},
		
		onSuccessMessages: {       
      addClassification: 'Classification successfully added',
	    endClassification: '<s:message code="person.classifications.endClassificationMessage.success" javaScriptEscape="true" />',
	    deleteClassification: '<s:message code="person.classifications.deleteClassificationMessage.success" javaScriptEscape="true" />'
	  },
	  
	  narratives: {
		  addClassification: 'You\'re adding a classification for',
	  }
	  
	});

	myPopup.render();
	
	Y.on(EVENT_SHOW_DIALOG, function(e) {

		switch(e.action) {
		
		case 'addClassification':					
			this.showView('addClassification', {
				labels: this.get('commonLabels'),
		    narrative: this.get('narratives.addClassification'),
				subject: e.data.subject,
				url: '<s:url value="/rest/managedClassificationAssignment"/>',
				otherData: {		                
					practitioner: { personName: loginPersonName, personId: loginPersonId }            	              
				},    
			}, showViewFn);

			break;
		
		case 'endClassification':			
			this.showView('endClassification', {				
				labels: this.get('commonLabels'),
				url: '<s:url value="/rest/classificationAssignment/{id}"/>', 
				otherData: {	            
					person: personContextDetails,	            
					practitioner: { 	            	
						personName: loginPersonName, 	            	
						personId: loginPersonId	            
					}	          	        
				}

			}, { modelLoad:true, payload:{ id:e.id } }, showViewFn);
			
			break;
			
		case 'deleteClassification':			
			this.showView('deleteClassification', {				
				labels: this.get('commonLabels'),
				url: '<s:url value="/rest/classificationAssignment/{id}"/>', 
				otherData: {	            
					person: personContextDetails,	            
					practitioner: { 	            	
						personName: loginPersonName, 	            	
						personId: loginPersonId	            
					}	          	        
				}

			}, { modelLoad:true, payload:{ id:e.id } }, showViewFn);
			
			break;
		}

		
	}, myPopup);
	
});
</script>
