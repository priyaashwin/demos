<%@page contentType="text/html;charset=UTF-8"%>
<%-- <%@page import="com.olmgroup.usp.apps.relationshipsrecording.permissions.PermissionReference.RelationshipPermissions"%> --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!-- TODO: permissions  -->
<%-- Define permissions --%>
<sec:authorize access="hasPermission(null, 'PersonClassifierAssignment','PersonClassifierAssignment.GET')" var="canView"/>

<%-- <c:set var="canView" value="true" /> --%>
<c:choose>
	<c:when test="${canView eq true}">
		<%-- Insert classification dialog --%>
		<t:insertTemplate template="/WEB-INF/views/tiles/content/classifications/panel/classificationDialog.jsp" /> 
   
		<div id="personClassificationsView">		
  		<t:insertTemplate template="/WEB-INF/views/tiles/content/classifications/config.jsp" />		
			<t:insertTemplate template="/WEB-INF/views/tiles/content/classifications/sections/results/generalResults.jsp" />
		</div>		
	</c:when>
	
	<c:otherwise>
		<!-- Insert access denied tile -->
		<t:insertDefinition name="access.denied" />
	</c:otherwise>
</c:choose>
