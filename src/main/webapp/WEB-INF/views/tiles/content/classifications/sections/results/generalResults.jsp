<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'PersonClassifierAssignment','PersonClassifierAssignment.View')" var="canView" />

<%-- If not set to read only, add write permissions --%>
<c:if test="${readOnly ne true}">
    <sec:authorize access="hasPermission(null, 'PersonClassifierAssignment','PersonClassifierAssignment.Update')" var="canUpdate" />
    <sec:authorize access="hasPermission(null, 'ClassificationAssignment','ClassificationAssignment.CLOSE')" var="canClose" />
    <sec:authorize access="hasPermission(null, 'ClassificationAssignment','ClassificationAssignment.DELETED')" var="canDelete" />
    <sec:authorize access="hasPermission(null, 'ClassificationAssignment','ClassificationAssignment.DeleteNotManuallyAssignable')" var="canDeleteNotManuallyAssignable" />
    <sec:authorize access="hasPermission(null, 'ClassificationAssignment','ClassificationAssignment.CloseNotManuallyAssignable')" var="canCloseNotManuallyAssignable" />
    <sec:authorize access="hasPermission(null, 'ClassificationAssignment','ClassificationAssignment.REMOVE')" var="canRemove" />
</c:if>

<div id="generalClassificationsResults" class="pure-table pure-table-striped pure-table-hover hide-on-search"></div>
<div id="generalClassificationsResultsPaginator" class="hide-on-search"></div>
<div id="personSearchResults" class="pure-table pure-table-striped pure-table-hover"></div>
<div id="personSearchResultsPaginator"></div>

<script>
    Y.use('paginated-results-table',
        'results-formatters', 'results-templates',
        'classification-component-enumerations',
        'results-table-menu-plugin',
        'results-table-keyboard-nav-plugin',
        'yui-base',
        'usp-person-PersonClassifierAssignmentDetails',
        'usp-person-PersonClassifierAssignment',
        'usp-date',
        'usp-person-ManagedClassificationAssignment',
        'categories-classification-component-EndReason',
        'remove-controller-view',
        function (Y) {

            var canUpdate = '${canUpdate}' === 'true',
                canDelete = '${canDelete}' === 'true',
                canClose = '${canClose}' === 'true',
                canRemove = '${canRemove}' === 'true',
                canDeleteNotManuallyAssignable = '${canDeleteNotManuallyAssignable}' === 'true',
                canCloseNotManuallyAssignable = '${canCloseNotManuallyAssignable}' === 'true';
            L = Y.Lang,
                formatters = Y.usp.ColumnFormatters,
                templates = Y.usp.ColumnTemplates;

            var commonLabels = {
                groupName: '<s:message code="person.classifications.general.results.groupName"/>',
                classificationName: '<s:message code="person.classifications.general.results.finalLevel"/>',
                startedBy: '<s:message code="person.classifications.general.results.startedBy"/>',
                startDate: '<s:message code="person.classifications.general.results.startDate"/>',
                endedBy: '<s:message code="person.classifications.general.results.endedBy"/>',
                endDate: '<s:message code="person.classifications.general.results.endDate"/>',
                endReason: '<s:message code="common.column.date.endReason"/>',
                status: '<s:message code="person.classifications.general.results.status"/>',
                endClassificationTitle: '<s:message code="person.classifications.general.results.action.endClassificationTitle"/>',
                endClassificationMenuItem: '<s:message code="person.classifications.general.results.action.endClassificationMenuItem"/>',
                deleteClassificationTitle: '<s:message code="person.classifications.general.results.action.deleteClassificationTitle"/>',
                deleteClassificationMenuItem: '<s:message code="person.classifications.general.results.action.deleteClassificationMenuItem"/>',
                removeClassificationTitle: '<s:message code="person.classifications.general.results.action.removeClassificationTitle"/>',
                removeClassificationMenuItem: '<s:message code="person.classifications.general.results.action.removeClassificationMenuItem"/>',
                actions: '<s:message code="person.classifications.general.results.actions"/>'
            };

            //start of config for person classification removal
            var subject = {
                subjectType: 'person',
                subjectName: '${esc:escapeJavaScript(person.name)}',
                subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                subjectId: '<c:out value="${person.id}"/>'
            };

            var personIcon = 'fa fa-user';
            var personNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

            var removeDialogConfig = {
                views: {
                    prepareRemove: {
                        header: {
                            text: '<s:message code="person.classification.remove.prepare.dialog.header" javaScriptEscape="true"/>',
                            icon: personIcon
                        },
                        narrative: {
                            summary: '<s:message code="person.classification.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
                            description: personNarrativeDescription
                        },
                        successMessage: '<s:message code="person.classification.remove.prepare.success.message" javaScriptEscape="true" />',
                        labels: {
                            deleteConfirmation: '<s:message code="person.classification.remove.prepare.dialog.deletionConfirmation" javaScriptEscape="true" />'
                        },
                        config: {
                            prepareUrl: '<s:url value="/rest/classificationAssignment/{id}/deletionRequest" />'
                        }
                    },
                    remove: {
                        header: {
                            text: '<s:message code="person.classification.remove.dialog.header" javaScriptEscape="true"/>',
                            icon: personIcon
                        },
                        narrative: {
                            summary: '<s:message code="person.classification.remove.dialog.summary" javaScriptEscape="true"/>',
                            description: personNarrativeDescription
                        },
                        successMessage: '<s:message code="person.classification.remove.success.message" javaScriptEscape="true" />',
                        labels: {
                            deleteConfirmation: '<s:message code="person.classification.remove.dialog.deletionConfirmation" javaScriptEscape="true" />'
                        },
                        config: {
                            deleteUrl: '<s:url value="/rest/classificationAssignment?deletionRequestId={deletionRequestId}" />',
                            resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
                        }
                    },
                    removeInProgress: {
                        header: {
                            text: '<s:message code="person.classification.remove.dialog.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        labels: {
                            message: '<s:message code="person.classification.remove.dialog.inProgress" javaScriptEscape="true" />',
                        }
                    }
                }
            };

            var removeConfig = {
                type: 'personClassification',
                subject: subject,
                dialogConfig: removeDialogConfig
            };
            //end of config for person classification removal

            var classificationRemoveController = new Y.app.remove.RemoveControllerView(removeConfig).render();

            var classificationMenuItems = [{
                clazz: 'endClassification',
                title: commonLabels.endClassificationTitle,
                label: commonLabels.endClassificationMenuItem,
                visible: canClose && function () {
                    var canWrite = this.record.get('_securityMetaData.write');
                    if (!canWrite) {
                        return false;
                    }
                    if ((this.record.get('classificationVO.notManuallyAssignable') && !canCloseNotManuallyAssignable) || this.record.get('classificationVO.restricted')) {
                        return false;
                    }
                    if (this.record.get('endDate')) {
                        var today = new Date((new Date()).setHours(0, 0, 0, 0));
                        var endDate = this.record.get('endDate');
                        if (endDate > today) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                    return true;
                }
            }, {
                clazz: 'deleteClassification',
                title: commonLabels.deleteClassificationTitle,
                label: commonLabels.deleteClassificationMenuItem,
                visible: canDelete && function () {
                    var canWrite = this.record.get('_securityMetaData.write');
                    if ((this.record.get('classificationVO.notManuallyAssignable') && !canDeleteNotManuallyAssignable) || this.record.get('classificationVO.restricted')) {
                        canWrite = false;
                    }
                    return canWrite;
                }
            }, {
                clazz: 'removeClassification',
                title: commonLabels.removeClassificationTitle,
                label: commonLabels.removeClassificationMenuItem,
                visible: canRemove
            }];

            var personClassificationResults = new Y.usp.PaginatedResultsTable({
                id: 'personClassificationResults',

                //configure the initial sort
                sortBy: [{ 'startDate': 'desc' }],

                //declare the columns
                columns: [
                    { key: 'classificationVO!classificationGroupName', label: commonLabels.groupName, sortable: false, width: '19%' },
                    { key: 'classificationVO!name', label: commonLabels.classificationName, sortable: false, width: '19%' },
                    { key: 'personStartedClassification!personName', label: commonLabels.startedBy, width: '10%' },
                    { key: 'startDate', label: commonLabels.startDate, sortable: true, width: '10%', formatter: formatters.date },
                    { key: 'personEndedClassification!personName', label: commonLabels.endedBy, width: '10%' },
                    { key: 'endDate', label: commonLabels.endDate, sortable: true, width: '10%', formatter: formatters.date },
                    { key: 'endReason', label: commonLabels.endReason, sortable: false, width: '10%', formatter: formatters.codedEntry, codedEntries: Y.uspCategory.classification.endReason.category.codedEntries },
                    { key: 'status', label: commonLabels.status, sortable: false, width: '7%', formatter: formatters.enumType, enumTypes: Y.usp.classification.enum.ClassificationAssignmentStatus.values },
                    { label: commonLabels.actions, className: 'pure-table-actions', width: '5%', formatter: formatters.actions, items: classificationMenuItems }
                ],

                data: new Y.usp.person.PaginatedManagedClassificationAssignmentList({
                    url: '<s:url value="/rest/classificationAssignment?subjectId=${person.id}&subjectType=PERSON&filter=true&s={sortBy}&pageNumber={page}&pageSize={pageSize}"/>',
                }),

                expandByDefault: true,
                //setup the paginator
                paginatorContainer: '#generalClassificationsResultsPaginator',
                //setup results count
                resultsCount: '#generalResultsCount',
                noResultsMessage: '<s:message code="person.classifications.general.no.results"/>',
                plugins: [
                    //plugin Menu handler
                    { fn: Y.Plugin.usp.ResultsTableMenuPlugin },
                    //plugin Keyboard nav
                    { fn: Y.Plugin.usp.ResultsTableKeyboardNavPlugin }
                ]
            });

            //fired when a single person classification is removed.
            Y.on('deletion:removeSaved', function () {
                personClassificationResults.reload();
            });

            Y.on('classification:dataChanged', function (e) {
                personClassificationResults.reload();
            });

            /**
             * Handle Events
             * -------------
             */
            var handleRowClick = function (e) {
                e.preventDefault();
                var t = e.currentTarget,
                    record = this.getRecord(t.get("id")),
                    id = record.get('id');

                if (t.hasClass('endClassification')) {
                    Y.fire('classification:showDialog', {
                        id: id,
                        action: 'endClassification'
                    });
                }

                if (t.hasClass('deleteClassification')) {
                    Y.fire('classification:showDialog', {
                        id: id,
                        action: 'deleteClassification'
                    });
                }

                if (t.hasClass('removeClassification')) {
                    Y.fire('removeDialog:prepareForIndividualPersonClassificationRemove', {
                        id: id
                    });
                }
            };

            personClassificationResults.render('#generalClassificationsResults');

            personClassificationResults.delegate('click', handleRowClick, '.pure-table-data tr a', personClassificationResults);

            //respond to custom events
            Y.on("classifications:dataChanged", function (e) {
                personClassificationResults.reload();
            });

            /** This is a temporary solution until this results table is rendered via a tablePanel **/
            personClassificationResults.after('*:load', function (e) {
                var results = e.parsed || [];
                if (results.length === 0) {
                    this.addClass('no-results');
                } else {
                    this.removeClass('no-results');
                }
            }, Y.one('.table-header'));

        });
</script>