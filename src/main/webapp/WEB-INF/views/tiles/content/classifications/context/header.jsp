<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h3 tabindex="0">
	<span class="context-title"><s:message code="menu.classifications"/></span>
	<span class="context-count hide-on-search" id="generalResultsCount"></span>
	<span class="context-count" id="personResultsCount"></span>        
</h3>