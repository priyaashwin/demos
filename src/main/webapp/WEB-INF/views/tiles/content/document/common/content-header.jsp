<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="pure-g-r">
    <div class="pure-u-1-2">
        <h3 tabindex="0">
            <span class="context-title">
                <s:message code="menu.document" /></span>
            <span class="context-count" id="documentResultsCount"></span>
        </h3>
    </div>
</div>