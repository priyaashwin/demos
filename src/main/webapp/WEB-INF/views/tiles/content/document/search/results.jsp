<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="esc" uri="http://commons.apache.org/lang/StringEscapeUtils"%>

<sec:authorize access="hasPermission(null, 'DocumentRequest','DocumentRequest.View')" var="canViewDocument" />
<sec:authorize access="hasPermission(null,'DocumentRequest','DocumentRequest.RemoveAll')" var="canRemoveAllDocuments"/>
<sec:authorize access="hasPermission(null,'CaseNoteEntryAttachment','CaseNoteEntryAttachment.View')" var="canViewCaseNote" />
<sec:authorize access="hasPermission(null,'FormInstanceAttachment','FormInstanceAttachment.View')" var="canViewFormInstance" />
<sec:authorize access="hasPermission(null, 'DocumentRequest','DocumentRequest.Remove')" var="canRemoveDocument" />
<sec:authentication property="principal.securitySubject.subjectId" var="userId" />

<div id="documentResults"></div>

<script>
    Y.use('document-controller-view', 'output-component-enumerations', 'categories-casenote-component-EntryType',
        'categories-casenote-component-EntrySubType', "categories-core-component-Source", "categories-core-component-SourceOrganisation",
        function (Y) {

            var permissions = {
                canViewDocument: '${canViewDocument}' === 'true',
                canRemoveAllDocuments: '${canRemoveAllDocuments}' === 'true',
                canViewCaseNote: '${canViewCaseNote}' === 'true',
                canViewFormInstance: '${canViewFormInstance}' === 'true',
                canRemoveDocument: '${canRemoveDocument}' === 'true'  
            };

            var subject = {
                subjectType: 'person',
                subjectName: '${esc:escapeJavaScript(person.name)}',
                subjectIdentifier: '${esc:escapeJavaScript(person.personIdentifier)}',
                subjectId: '<c:out value="${person.id}"/>'
            };

            var user = {
                userId: '${userId}'
            };

            var personIcon = 'fa fa-user';
            var personNarrativeDescription = '{{this.subject.subjectName}} ({{this.subject.subjectIdentifier}})';

            var resultsFilterConfig = {
                labels: {
                    filterBy: '<s:message code="document.find.filter.filterBy" javaScriptEscape="true" />',
                    status: '<s:message code="document.find.filter.status" javaScriptEscape="true" />',
                    documentName: '<s:message code="document.find.filter.documentName" javaScriptEscape="true" />',
                    uploadDateFrom: '<s:message code="document.find.filter.uploadDateFrom" javaScriptEscape="true" />',
                    uploadDateTo: '<s:message code="document.find.filter.uploadDateTo" javaScriptEscape="true" />',
                    resetAll: '<s:message code="document.find.filter.resetAll" javaScriptEscape="true" />',
                    activeFilters: '<s:message code="document.find.filter.activeFilter.title" javaScriptEscape="true" />',
                    nameTabTitle: '<s:message code="document.find.filter.nameTabTitle" javaScriptEscape="true" />',
                    dateTabTitle: '<s:message code="document.find.filter.dateTabTitle" javaScriptEscape="true" />',
                    uploadDateFromInvalidFormat: '<s:message code="document.find.filter.invalidDateFromFormatMessage" javaScriptEscape="true" />',
                    uploadDateToInvalidFormat: '<s:message code="document.find.filter.invalidDateToFormatMessage" javaScriptEscape="true" />',
                    uploadDateToDateConflict: '<s:message code="document.find.filter.conflictingDatesMessage" javaScriptEscape="true" />'
                }
            };

            var labels = {
                generatedDocumentTable: {
                    columns: {
                        documentType: '<s:message code="document.outputsTable.outputType" javaScriptEscape="true"/>',
                        documentName: '<s:message code="document.outputsTable.outputName" javaScriptEscape="true"/>',
                        generationDate: '<s:message code="document.outputsTable.generationDate" javaScriptEscape="true"/>',
                        generatedBy: '<s:message code="document.outputsTable.generatingUser" javaScriptEscape="true"/>',
                        status: '<s:message code="document.outputsTable.status" javaScriptEscape="true"/>',
                        downloadDocument: '<s:message code="document.outputsTable.actions.download" javaScriptEscape="true"/>',
                        removeDocument: '<s:message code="document.outputsTable.actions.remove" javaScriptEscape="true"/>',
                        actionButtons: '<s:message code="document.outputsTable.actions" javaScriptEscape="true"/>'
                    }
                },
                attachmentsTable: {
                    columns: {
                        documentName: '<s:message code="document.attachmentsTable.documentName" javaScriptEscape="true"/>',
                        description: '<s:message code="document.attachmentsTable.description" javaScriptEscape="true"/>',
                        entryType: '<s:message code="document.attachmentsTable.entryType" javaScriptEscape="true"/>',
                        entrySubtype: '<s:message code="document.attachmentsTable.entrySubtype" javaScriptEscape="true"/>',
                        formName: '<s:message code="document.attachmentsTable.formName" javaScriptEscape="true"/>',
                        dateAdded: '<s:message code="document.attachmentsTable.dateAdded" javaScriptEscape="true"/>',
                        source: '<s:message code="document.attachmentsTable.source" javaScriptEscape="true"/>',
                        sourceOrg: '<s:message code="document.attachmentsTable.sourceOrg" javaScriptEscape="true"/>',
                        addedBy: '<s:message code="document.attachmentsTable.addedBy" javaScriptEscape="true"/>',
                        downloadAttachment: '<s:message code="document.attachmentsTable.actions.download" javaScriptEscape="true"/>',
                        viewItem: '<s:message code="document.attachmentsTable.actions.viewItem" javaScriptEscape="true"/>',
                        actionButtons: '<s:message code="document.attachmentsTable.actions" javaScriptEscape="true"/>'
                    }
                },
                accordionHeadings: {
                    outputs: '<s:message code="document.accordion.outputs" javaScriptEscape="true"/>',
                    caseNotes: '<s:message code="document.accordion.caseNotes" javaScriptEscape="true"/>',
                    forms: '<s:message code="document.accordion.Forms" javaScriptEscape="true"/>'
                },
                buttons: {
                    removeAll: '<s:message code="button.removeAll" javaScriptEscape="true"/>'
                }
            };

            var enums = {
                DocumentType: Y.usp.output.enum.DocumentType,
                DocumentRequestStatus: Y.usp.output.enum.DocumentRequestStatus
            };

            var codedEntries = {
                entryTypes: Y.uspCategory.casenote.entryType.category,
                entrySubTypes: Y.uspCategory.casenote.entrySubType.category,
                source: Y.uspCategory.core.source.category,
                sourceOrganisation: Y.uspCategory.core.sourceOrganisation.category
            };

            var urls = {
                outputsURLS: {
                    generatedDocumentsURL: '<s:url value="/rest/documentRequest?subjectType=PERSON&subjectId={subjectId}&generatedByPersonId={generatedByPersonId}&pageSize={pageSize}&pageNumber={page}&documentName={documentName}&startDate={startDate}&endDate={endDate}"/>',
                    downloadDocumentURL: '<s:url value="/rest/{documentType}Document/{attachmentId}/content"/>'
                },
                caseNotesAttachmentsURLs: {
                    uploadedAttachmentsURL: '<s:url value="/rest/person/{subjectId}/caseNoteEntryAttachment?pageSize={pageSize}&pageNumber={page}&attachmentTitle={attachmentTitle}&startDate={startDate}&endDate={endDate}"/>',
                    downloadAttachmentURL: '<s:url value="/rest/caseNoteEntryAttachment/{attachmentItemId}/content"/>'
                },
                formsAttachmentsURLs: {
                    uploadedAttachmentsURL: '<s:url value="/rest/person/{subjectId}/formInstanceAttachment?pageSize={pageSize}&pageNumber={page}&attachmentTitle={attachmentTitle}&startDate={startDate}&endDate={endDate}"/>',
                    downloadAttachmentURL: '<s:url value="/rest/formInstanceAttachment/{attachmentItemId}/content?formInstanceId={formInstanceId}"/>'
                }
            };

            var removeAllViewsConfig = {
                views: {
                    prepareRemove: {
                        header: {
                            text: '<s:message code="outputs.remove.prepare.dialog.header" javaScriptEscape="true"/>',
                            icon: personIcon
                        },
                        narrative: {
                            summary: '<s:message code="outputs.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
                            description: personNarrativeDescription
                        },
                        successMessage: '<s:message code="outputs.remove.prepare.success.message" javaScriptEscape="true" />',
                        labels: {
                            deleteConfirmation: '<s:message code="outputs.remove.prepare.dialog.deletionConfirmation" javaScriptEscape="true" />'
                        },
                        config: {
                            prepareUrl: '<s:url value="/rest/PERSON/${person.id}/documentRequest/deletionRequest"/>'
                        }
                    },
                    remove: {
                        header: {
                            text: '<s:message code="outputs.remove.dialog.header" javaScriptEscape="true"/>',
                            icon: personIcon
                        },
                        narrative: {
                            summary: '<s:message code="outputs.remove.dialog.summary" javaScriptEscape="true"/>',
                            description: personNarrativeDescription
                        },
                        successMessage: '<s:message code="outputs.remove.success.message" javaScriptEscape="true" />',
                        labels: {
                            deleteConfirmation: '<s:message code="outputs.remove.dialog.deletionConfirmation" javaScriptEscape="true" />'
                        },
                        config: {
                            deleteUrl: '<s:url value="/rest/documentRequest?deletionRequestId={deletionRequestId}" />',
                            resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
                        }
                    },
                    removeInProgress: {
                        header: {
                            text: '<s:message code="outputs.remove.dialog.header" javaScriptEscape="true"/>',
                            icon: 'fa fa-wrench'
                        },
                        labels: {
                            message: '<s:message code="outputs.remove.dialog.inProgress" javaScriptEscape="true" />',
                        }
                      }
                }
            };

            var removeAllDialogConfig = {
                type: 'documents',
                subject: subject,
                dialogConfig: removeAllViewsConfig
            };

            var removeViewsConfig = {
        			views: {
        				prepareRemove: {
        					header: {
        						text: '<s:message code="output.remove.prepare.dialog.header" javaScriptEscape="true"/>',
        						icon: personIcon
        					},
        					narrative: {
        						summary: '<s:message code="output.remove.prepare.dialog.summary" javaScriptEscape="true"/>',
        						description: personNarrativeDescription
        					},
        					successMessage: '<s:message code="output.remove.prepare.success.message" javaScriptEscape="true" />',
        					labels: {
        						deleteConfirmation: '<s:message code="output.remove.prepare.dialog.deletionConfirmation" javaScriptEscape="true" />'
        					},
        					config: {
        						prepareUrl: '<s:url value="/rest/documentRequest/{id}/deletionRequest" />'
        					}
        				},
        				remove: {
        					header: {
        						text: '<s:message code="output.remove.dialog.header" javaScriptEscape="true"/>',
        						icon: personIcon
        					},
        					narrative: {
        						summary: '<s:message code="output.remove.dialog.summary" javaScriptEscape="true"/>',
        						description: personNarrativeDescription
        					},
        					successMessage: '<s:message code="output.remove.success.message" javaScriptEscape="true" />',
        					labels: {
        						deleteConfirmation: '<s:message code="output.remove.dialog.deletionConfirmation" javaScriptEscape="true" />'
        					},
        					config: {
        						deleteUrl: '<s:url value="/rest/documentRequest?deletionRequestId={deletionRequestId}" />',
        						resetUrl: '<s:url value="/rest/entityDeletionRequest/{deletionRequestId}/status?action=abandon" />'
        					}
        				},
        				removeInProgress: {
        					header: {
        						text: '<s:message code="output.remove.dialog.header" javaScriptEscape="true"/>',
        						icon: 'fa fa-wrench'
        					},
        					labels: {
        						message: '<s:message code="output.remove.dialog.inProgress" javaScriptEscape="true" />',
        					}
        				}
        			}
        		};
            

            var removeDialogConfig = {
        			type: 'document',
        			subject: subject,
        			dialogConfig: removeViewsConfig
        		};
    
            var config = {
                container: '#documentResults',
                codedEntries: codedEntries,
                permissions: permissions,
                enums: enums,
                subject: subject,
                user: user,
                labels: labels,
                urls: urls,
                resultsFilterConfig: resultsFilterConfig,
                removeAllDialogConfig: removeAllDialogConfig,
                removeDialogConfig: removeDialogConfig
            };

            var casenoteView = new Y.app.document.DocumentControllerView(config).render();
        });
</script>