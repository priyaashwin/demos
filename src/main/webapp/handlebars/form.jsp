<%@page contentType="text/javascript;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.olmgroup.usp.apps.relationshipsrecording.HandlebarsLocator" %>
<%@ page import="java.util.Map" %>

<s:eval expression="@propertyFactory.getProperty('usp.app.dev.liveHandlebars')" var="liveHandlebars" scope="page" />


YUI.add('handlebars-form-templates', function(Y) {    
    var Handlebars=Y.Handlebars;
    // formats form status as icon
    Handlebars.registerHelper('formState',function(form){
    var L = Y.Lang,
        E = Y.Escape,
        template_href = '<a href="#" class="form-state" data-title="Form state" data-content="{stateType}" aria-label="{stateType} form state">',
        template_DRAFT = '<i class="icon-state icon-state-draft"></i></a>',
        template_COMPLETE = '<i class="icon-state icon-state-complete"></i></a>',
        template_REJECTED = '<i class="icon-state icon-state-rejected"></i></a>',
	    template_AUTHORISED = '<i class="icon-state icon-state-authorised"></i></a>',
	    template_DELETED = '<i class="icon-state icon-state-deleted"></i></a>',
		STATE_DRAFT='DRAFT',
		STATE_COMPLETE='COMPLETE',
		STATE_AUTHORISED='AUTHORISED',
		STATE_DELETED='ARCHIVED',
		state=form.status,
		hasRejections=form.hasRejections;

        if (state===STATE_DRAFT && hasRejections) {
            return new Handlebars.SafeString(L.sub(template_href, {
                stateType: E.html(state+' REJECTED')
            })+template_REJECTED);
        }else if (state===STATE_DRAFT) {
            return new Handlebars.SafeString(L.sub(template_href, {
                stateType: E.html(state)
            })+template_DRAFT);
        }else if(state===STATE_COMPLETE) {
            return new Handlebars.SafeString(L.sub(template_href, {
                stateType: E.html(state)
            })+template_COMPLETE);
        }else if(state===STATE_AUTHORISED) {
            return new Handlebars.SafeString(L.sub(template_href, {
                stateType: E.html(state)
            })+template_AUTHORISED);
        }else if(state===STATE_DELETED) {
            return new Handlebars.SafeString(L.sub(template_href, {
                stateType: E.html('DELETED')
            })+template_DELETED);
        }
        return '';
    });
    // formats form status as text
    Handlebars.registerHelper('formStatePrint',function(form){
     	var L = Y.Lang,
        E = Y.Escape,
		STATE_DRAFT='DRAFT',
		STATE_COMPLETE='COMPLETE',
		STATE_AUTHORISED='AUTHORISED',
		STATE_DELETED='ARCHIVED'
		state=form.status,
		hasRejections=form.hasRejections;

        if (state===STATE_DRAFT && hasRejections) {
            return new Handlebars.SafeString(state+' REJECTED');
        }else if(state===STATE_DELETED) {
            return new Handlebars.SafeString('DELETED');
        }else{
            return new Handlebars.SafeString(state);
        }
        return '';
    });
<c:choose>
	<c:when test="${liveHandlebars=='true'}">
	<%
	HandlebarsLocator locator=new HandlebarsLocator(getServletContext());
	Map <String,String> templates = locator.getEscapedTemplates("/WEB-INF/resources/handlebars/form");
	pageContext.setAttribute("templates",templates);
	%>
	var start = new Date().getTime();
	Handlebars.templates = Handlebars.templates || {};
	
	<c:forEach var="template" items="${templates}">
    	Handlebars.templates['<c:out value="${template.key}" />']=Handlebars.compile('<c:out value="${template.value}" escapeXml="false" />');
	</c:forEach>
    var end = new Date().getTime();
    Y.log("Templates prepared in "+(end-start)+" ms");
    
}, '0.0.1', { requires : [ 'handlebars'] });
	
	</c:when>
	<c:otherwise>
    <%@ include file="setCacheHeaders.jsp" %>

    <%@ include file="/WEB-INF/resources/js/hbs/form/form.js" %>
}, '0.0.1', { requires : [ 'handlebars-base'] });
	</c:otherwise>
</c:choose>