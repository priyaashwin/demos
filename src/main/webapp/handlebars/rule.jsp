<%@page contentType="text/javascript;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.olmgroup.usp.apps.relationshipsrecording.HandlebarsLocator" %>
<%@ page import="java.util.Map" %>

<s:eval expression="@propertyFactory.getProperty('usp.app.dev.liveHandlebars')" var="liveHandlebars" scope="page" />
YUI.add('handlebars-rule-templates', function(Y) {
    var Handlebars=Y.Handlebars,
    	L = Y.Lang,
        TEMPLATE = '<a href="#" class="tool-tip rule-state" data-title="Rule state" data-content="{stateRule}" aria-label="{stateRule} rule state">{icon}</a>',
        ICON_DRAFT = '<i class="icon-state icon-state-draft"></i>',		
        ICON_PUBLISHED = '<i class="icon-state icon-state-published"></i>',
	    ICON_DELETED = '<i class="icon-state icon-state-deleted"></i>';
    
    // formats rule status as icon
    Handlebars.registerHelper('ruleState',function(rule){
	    var content=''; 
		switch(rule.status){
			case 'DRAFT':
				content=L.sub(TEMPLATE, {
	                stateType: 'DRAFT',
	                icon:ICON_DRAFT
	            });
			break;
			case 'ARCHIVED':
				content=L.sub(TEMPLATE, {
	                stateType: 'DELETED',
	                icon:ICON_DELETED
	            });			
			break;
			case 'PUBLISHED':
				content=L.sub(TEMPLATE, {
	                stateType: 'PUBLISHED',
	                icon:ICON_PUBLISHED
	            });			
			break;
		}
		
		return new Handlebars.SafeString(content);		
    });
<c:choose>
	<c:when test="${liveHandlebars=='true'}">
	<%
	HandlebarsLocator locator=new HandlebarsLocator(getServletContext());
	Map <String,String> templates = locator.getEscapedTemplates("/WEB-INF/resources/handlebars/rule");
	pageContext.setAttribute("templates",templates);
	%>
	var start = new Date().getTime();
	Handlebars.templates = Handlebars.templates || {};
	
	<c:forEach var="template" items="${templates}">
    	Handlebars.templates['<c:out value="${template.key}" />']=Handlebars.compile('<c:out value="${template.value}" escapeXml="false" />');
	</c:forEach>
    var end = new Date().getTime();
    Y.log("Templates prepared in "+(end-start)+" ms");
    
}, '0.0.1', { requires : [ 'handlebars'] });
	
	</c:when>
	<c:otherwise>
    <%@ include file="setCacheHeaders.jsp" %>

    <%@ include file="/WEB-INF/resources/js/hbs/rule/rule.js" %>
}, '0.0.1', { requires : [ 'handlebars-base'] });
	</c:otherwise>
</c:choose>