<%@page contentType="text/javascript;charset=UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.olmgroup.usp.apps.relationshipsrecording.HandlebarsLocator" %>
<%@ page import="java.util.Map" %>
<s:eval expression="@propertyFactory.getProperty('usp.app.dev.liveHandlebars')" var="liveHandlebars" scope="page" />
YUI.add('handlebars-organisation-templates', function(Y) {    
    var Handlebars=Y.Handlebars;
    <c:choose>
       <c:when test="${liveHandlebars=='true'}">
       <%
       HandlebarsLocator locator=new HandlebarsLocator(getServletContext());
       Map <String,String> templates = locator.getEscapedTemplates("/WEB-INF/resources/handlebars/organisation");
       pageContext.setAttribute("templates",templates);
       %>
       var start = new Date().getTime();
       Handlebars.templates = Handlebars.templates || {};
       
       <c:forEach var="template" items="${templates}">
       Handlebars.templates['<c:out value="${template.key}" />']=Handlebars.compile('<c:out value="${template.value}" escapeXml="false" />');
       </c:forEach>
    var end = new Date().getTime();
    Y.log("Templates prepared in "+(end-start)+" ms");
    
}, '0.0.1', { requires : [ 'handlebars'] });
       
       </c:when>
       <c:otherwise>
       <%@ include file="setCacheHeaders.jsp" %>
    <%@ include file="/WEB-INF/resources/js/hbs/organisation/organisation.js" %>
}, '0.0.1', { requires : [ 'handlebars-base'] });
       </c:otherwise>
</c:choose>
