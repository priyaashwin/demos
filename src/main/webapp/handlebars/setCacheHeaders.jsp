<%@page import="java.util.Calendar"%>
	<%
		long oneYearInSeconds = 31557600l;
		response.setHeader("Cache-Control", "max-age="+oneYearInSeconds); //HTTP 1.1
		response.setHeader("Pragma", "cache"); //HTTP 1.0
		Calendar oneYearFromNow = Calendar.getInstance();
		oneYearFromNow.roll(Calendar.YEAR, 1);
		long oneYearFromNowInMillis = oneYearFromNow.getTimeInMillis();
		response.setDateHeader("Expires", oneYearFromNowInMillis); //cache at the proxy server
	%>