<%@page contentType="text/javascript;charset=UTF-8"%>
<%
//cache for 2 hours
response.setHeader("Cache-Control","public");
response.setHeader("Cache-Control","max-age=7200");
%>
YUI.add('handlebars-cpraform-print-templates', function(Y) {    
    var Handlebars=Y.Handlebars;
<%@ include file="setCacheHeaders.jsp" %>
    <%@ include file="/WEB-INF/resources/js/hbs/resolveform/cpraFormPrint/cpraFormPrint.js" %>
}, '0.0.1', { requires : [ 'handlebars-base'] });