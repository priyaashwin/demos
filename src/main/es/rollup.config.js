import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace';
import postcss from 'rollup-plugin-postcss';
import sourcemaps from 'rollup-plugin-sourcemaps';

const ENV = process.env.NODE_ENV || 'production';

export default {
  onwarn: ( warning, next ) => {
    if ( warning.code === 'THIS_IS_UNDEFINED' ) return;
    next( warning );
  },
  output: {
    sourcemap: true,
    format: 'iife',
    extend : true,
    globals: {
      'react': 'React',
      'react-dom': 'ReactDOM',
      'moment': 'moment',
      'chart.js': 'Chart'
    }  
  },
  // All the used libs needs to be here
  external: [
    'react', 'react-dom', 'moment', 'chart.js'
  ],
  plugins: [
    resolve({
      mainFields: ['module', 'browser'],
      extensions: ['.js', '.jsx', '.ts', '.mjs', '.jss'],
      dedupe: ['react', 'react-dom', 'react-is', 'jss'],
      preferBuiltins: false
    }),
    postcss({
      plugins: [],
      minimize: {
        safe: true
      },
      sourceMap:false
    }),
    babel({
      exclude: 'node_modules/**',
      runtimeHelpers: true
    }),
    replace({'process.env.NODE_ENV': JSON.stringify(ENV)}),
    commonjs({
        include: 'node_modules/**',
        exclude: ['node_modules/radium/**', 'node_modules/chart.js/**'],
        namedExports: {
        // left-hand side can be an absolute path, a path
        // relative to the current directory, or the name
        // of a module in node_modules
        'node_modules/immutable/dist/immutable.js': [ 'Seq','List','Map','fromJS', 'Record']
      }
    }),
    sourcemaps()
  ]
};
