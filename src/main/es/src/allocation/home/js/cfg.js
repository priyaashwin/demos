'use strict';
class Endpoint {
    constructor() {

        this.accessedClientsEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.relationshipsrecording.personaccesswarningcount+json'
            },
            responseType: 'json',
            responseStatus: 200
        };

        this.professionRoles = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.person.personallocationresults+json'
            },
            responseType: 'json',
            responseStatus: 200
        };

        this.teamRelationshipEndPoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.person.personallocationresults+json'
            },
            responseType: 'json',
            responseStatus: 200
        };

        this.colleaguesRelationshipEndPoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.person.personallocationresults+json'
            },
            responseType: 'json',
            responseStatus: 200,
        };

        this.namesEndPoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.person.PersonSummary+json'
            },
            responseType: 'json',
            responseStatus: 200,
        };

        this.teamsEndPoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.organisation.Team+json'
            },
            responseType: 'json',
            responseStatus: 200,
        };
    }
}

export default new Endpoint();