'use strict';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Input from '../../../webform/raw/input';
import { filterContext } from '../../../filter/filterContextProvider';

const Person = props => {
    const { labels } = props;

    const context = useContext(filterContext);
    const filters = context.getFilters();
    const filterName = 'nameOrId';
    const { nameOrId } = filters;

    const onUpdate = (name, value) => {
        const newValue = value || '';
        context.setFilterValue(name, newValue);
    };

    return (
        <div className="pure-u-1-3 l-box">
            <div className="pure-u-1-3">
                <label htmlFor="nameOrId" className="l-box">
                    {labels.nameOrIdTitle}
                </label>
            </div>

            <div className="pure-g">
                <div className="pure-u-1 l-box">
                    <div className="pure-input-1">
                        <Input
                            key={filterName}
                            id={filterName}
                            name={filterName}
                            value={nameOrId.toString()}
                            placeholder="Name or person id"
                            maxLength={100}
                            onUpdate={onUpdate}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

Person.propTypes = {
    labels: PropTypes.shape({
        nameOrIdTitle: PropTypes.string.isRequired
    })
};
export default Person;
