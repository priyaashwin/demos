'use strict';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Reset } from '../../../filter/components/reset';
import Input from '../../../webform/raw/input';
import { filterContext } from '../../../filter/filterContextProvider';

const Age = props => {
    const { labels } = props;
    const { setFilterValue, resetFilters, getFilters } = useContext(filterContext);
    const filters = getFilters();
    const { minAge, maxAge } = filters;

    const onUpdate = (name, value) => {
        const newValue = value || '';
        setFilterValue(name, newValue);
    };

    const onReset = () => {
        resetFilters('minAge', 'maxAge', 'nameOrId', 'gender');
    };

    return (
        <div className="pure-u-1-3 l-box">
            <div className="pure-u-1-3">
                <label className="l-box">{labels.ageFilter}</label>
            </div>

            <div className="pure-g">
                <div className="pure-u-1-2 l-box">
                    <div className="pure-input-1">
                        <Input
                            type="number"
                            inputMode="numeric"
                            maxLength="3"
                            id="allocationFilterFormMinAge"
                            name="minAge"
                            value={minAge.toString()}
                            placeholder={labels.ageBetween}
                            onUpdate={onUpdate}
                        />
                        <label className="hidden" htmlFor="allocationFilterFormMinAge">
                            {labels.ageBetween}
                        </label>
                    </div>
                </div>

                <div className="pure-u-1-2 l-box">
                    <div className="pure-input-1">
                        <Input
                            type="number"
                            inputMode="numeric"
                            maxLength="3"
                            id="allocationFilterFormMaxAge"
                            name="maxAge"
                            placeholder={labels.ageAnd}
                            value={maxAge.toString()}
                            onUpdate={onUpdate}
                        />
                        <label className="hidden" htmlFor="allocationFilterFormMaxAge">
                            {labels.ageAnd}
                        </label>
                    </div>
                </div>
            </div>
            <Reset id={'person'} onClick={onReset} />
        </div>
    );
};

Age.propTypes = {
    labels: PropTypes.shape({
        ageAnd: PropTypes.string.isRequired,
        ageBetween: PropTypes.string.isRequired,
        ageFilter: PropTypes.string.isRequired
    }).isRequired
};
export default Age;
