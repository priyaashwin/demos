'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Gender from '../filters/gender';
import Age from '../filters/age';
import NameOrId from '../filters/nameOrId';

const Person = props => {
    const { labels } = props;

    return (
        <div className="filterContainer">
            <div className="pure-g-r">
                {<NameOrId labels={labels} />}
                {<Gender labels={labels} />}
                {<Age labels={labels} />}
            </div>
        </div>
    );
};

Person.propTypes = {
    labels: PropTypes.object.isRequired
};
export default Person;
