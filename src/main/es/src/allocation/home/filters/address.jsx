'use strict';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Input from '../../../webform/raw/input';
import { Reset } from '../../../filter/components/reset';
import { filterContext } from '../../../filter/filterContextProvider';

const AddressFilter = props => {
    const { setFilterValue, getFilters, resetFilters } = useContext(filterContext);
    const filters = getFilters();
    const { postcode, houseStreetTown } = filters;

    const onReset = () => {
        resetFilters('postcode', 'houseStreetTown');
    };

    const onUpdate = (name, value) => {
        const newValue = value || '';
        setFilterValue(name, newValue);
    };

    const getContent = labels => (
        <div>
            <div className="pure-u-1 l-box">
                <label>{labels.addressFilter}</label>
            </div>

            <div className="pure-u-1-6 l-box">
                <label htmlFor="postcode">{labels.postcode}</label>
                <Input
                    key="postcode"
                    id="postcode"
                    style={{ textTransform: 'uppercase' }}
                    name="postcode"
                    value={postcode.toString()}
                    onUpdate={onUpdate}
                />
            </div>

            <div className="pure-u-1-3 l-box">
                <label htmlFor="houseStreetTown">{labels.houseStreetTown}</label>
                <Input
                    key="houseStreetTown"
                    id="houseStreetTown"
                    name="houseStreetTown"
                    value={houseStreetTown.toString()}
                    onUpdate={onUpdate}
                />
            </div>
        </div>
    );

    const { labels } = props;
    return (
        <div className="filterContainer">
            <div className="pure-g-r">
                <div className="pure-u-1 l-box">
                    {getContent(labels)} {<Reset id={'address'} onClick={onReset} />}
                </div>
            </div>
        </div>
    );
};

AddressFilter.propTypes = {
    labels: PropTypes.object.isRequired
};
export default AddressFilter;
