'use strict';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Input from '../../../webform/raw/input';
import { filterContext } from '../../../filter/filterContextProvider';

const Gender = props => {
    const { labels } = props;
    const { getFilters, setFilterValue } = useContext(filterContext);
    const filter = getFilters();
    const { gender } = filter;

    const onUpdate = (name, value) => {
        const newValue = value || '';
        setFilterValue(name, newValue);
    };

    const inputContent = (ariaLabel, id, value) => (
        <Input
            aria-label={ariaLabel}
            className="checkbox"
            id={id}
            name="gender"
            type="checkbox"
            checked={gender.value === `${value}` ? true : false}
            value={value}
            onUpdate={onUpdate}
        />
    );

    return (
        <div className="pure-u-1-3 l-box">
            <div className="pure-u-1 l-box">
                <div>{labels.genderFilter}</div>
            </div>
            <div className="pure-u-1-4 l-box">
                <label className="pure-checkbox" htmlFor="allocationFilterForm_MALE">
                    {inputContent(labels.genderMale, 'allocationFilterForm_MALE', 'MALE')}
                    {labels.genderMale}
                </label>
            </div>

            <div className="pure-u-1-4 l-box">
                <label className="pure-checkbox" htmlFor="allocationFilterForm_FEMALE">
                    {inputContent(labels.genderFemale, 'allocationFilterForm_FEMALE', 'FEMALE')}
                    {labels.genderFemale}
                </label>
            </div>

            <div className="pure-u-1-4 l-box">
                <label className="pure-checkbox" htmlFor="allocationFilterForm_INDETERMINATE">
                    {inputContent(labels.genderIndeterminate, 'allocationFilterForm_INDETERMINATE', 'INDETERMINATE')}
                    {labels.genderIndeterminate}
                </label>
            </div>

            <div className="pure-u-1-4 l-box">
                <label className="pure-checkbox" htmlFor="allocationFilterForm_UNKNOWN">
                    {inputContent(labels.genderUnknown, 'allocationFilterForm_UNKNOWN', 'UNKNOWN')}
                    {labels.genderUnknown}
                </label>
            </div>
        </div>
    );
};

Gender.propTypes = {
    labels: PropTypes.shape({
        genderFilter: PropTypes.string.isRequired,
        genderUnknown: PropTypes.string.isRequired,
        genderIndeterminate: PropTypes.string.isRequired,
        genderFemale: PropTypes.string.isRequired,
        genderMale: PropTypes.string.isRequired
    }).isRequired
};

export default Gender;
