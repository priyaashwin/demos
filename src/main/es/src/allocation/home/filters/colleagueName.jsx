'use strict';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Reset } from '../../../filter/components/reset';
import { filterContext } from '../../../filter/filterContextProvider';

const ColleagueNameFilter = props => {
    const { labels, namesRecord, teamsRecord, teamsLoaded, teamsError, namesLoaded, namesError } = props;
    const { setFilterValue, getFilters, resetFilters } = useContext(filterContext);
    const filters = getFilters();
    const { selectedTeamIds, selectedColleaguePersonIds } = filters;

    const onReset = () => {
        resetFilters('selectedColleaguePersonIds', 'selectedTeamIds');
    };

    const onUpdate = e => {
        const { name, options } = e.target;
        const selectedOptions = Array.from(options).filter(option => option.selected);
        const optionsArray = selectedOptions.map(option => option.value);
        setFilterValue(name, optionsArray);
    };

    const getSelectOptions = (records, loaded, error, labels, identifier) => {
        let optionsContent = '';
        if (loaded) {
            optionsContent = records.map(record => (
                <option key={record.id} value={record.id}>
                    {`${record.name} (${record[identifier]})`}
                </option>
            ));
        }
        if (error) {
            optionsContent = <option className="optionsError">{labels.selectOptionsError}</option>;
        }
        return optionsContent;
    };

    return (
        <div className="filterContainer">
            <div className="pure-g-r">
                <div className="pure-u-1 l-box">
                    <div className="pure-g-r">
                        <div className="pure-u-1-2 l-box">
                            <label htmlFor="selectedTeamIds">{labels.teamsWithAllocations}</label>
                            <select
                                name="selectedTeamIds"
                                id="selectedTeamIds"
                                value={selectedTeamIds.getValue()}
                                onChange={e => onUpdate(e)}
                                multiple={true}
                                size="5">
                                {getSelectOptions(teamsRecord, teamsLoaded, teamsError, labels, 'organisationIdentifier')}
                            </select>
                        </div>
                        <div className="pure-u-1-2 l-box">
                            <label htmlFor="selectedColleaguePersonIds">{labels.colleaguesWithAllocations}</label>
                            <select
                                name="selectedColleaguePersonIds"
                                id="selectedColleaguePersonIds"
                                value={selectedColleaguePersonIds.getValue()}
                                onChange={e => onUpdate(e)}
                                multiple={true}
                                size="5">
                                {getSelectOptions(namesRecord, namesLoaded, namesError, labels, 'personIdentifier')}
                            </select>
                        </div>
                        {<Reset id={'colleagueName'} onClick={onReset} />}
                    </div>
                </div>
            </div>
        </div>
    );
};

ColleagueNameFilter.propTypes = {
    teamsRecord: PropTypes.object.isRequired,
    namesRecord: PropTypes.object.isRequired,
    namesLoaded: PropTypes.bool.isRequired,
    teamsLoaded: PropTypes.bool.isRequired,
    teamsError: PropTypes.string.isRequired,
    namesError: PropTypes.string.isRequired,
    labels: PropTypes.shape({
        colleagueNameFilter: PropTypes.string.isRequired,
        colleaguesWithAllocations: PropTypes.string.isRequired,
        teamsWithAllocations: PropTypes.string.isRequired,
        selectOptionsError: PropTypes.string.isRequired
    })
};
export default ColleagueNameFilter;
