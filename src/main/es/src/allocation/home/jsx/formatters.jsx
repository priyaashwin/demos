'use strict';
import React from 'react';
import DateOfBirth from '../../../dateOfBirth/dateOfBirth';
import { date } from '../../../table/js/formatter';
import { formatCodedEntry } from '../../../codedEntry/codedEntry';
import MapLink from '../../../link/link';
import Warnings from '../../../warnings/warnings';
import DoNotDisclose from '../../../indicator/doNotDisclose';
import { isSecured } from './helpers';

const telephoneTypes = ['MOBILE', 'LANDLINE', null];

export const dobFormatter = (dateOfBirth, row, isRecentlyAccessedTab) => {
    const person = isRecentlyAccessedTab ? row.accessedPerson : row;
    return (dateOfBirth && <DateOfBirth person={person} />);
};
const getTelephoneType = (telephoneType, key, contacts, codedEntries) => {
    const contact = contacts.find(contact => contact.telephoneType === telephoneType);
    if (contact) {
        const { number, contactType } = contact;
        return (
            <p key={key}>
                {contactType && formatCodedEntry(codedEntries.contactType, contactType)}
                <li>
                    <a href={`tel:${number}`}>{number}</a>
                </li>
            </p>
        );
    }
};
export const contactsFormatter = (contacts, row, codedEntries) => (
    !row.accessAllowed ? <i>{'Information Protected'}</i> :
        contacts.length !== 0 && 
        <ul className="clientContactsList">
            {telephoneTypes.map((telephoneType, key) => getTelephoneType(telephoneType, key, contacts, codedEntries))}
        </ul>
);
export const accessDateFormatter = (accessDate, _, __, args) => {
    const { rowIndex, rows } = args;
    const currentAccessDate = date(accessDate);

    if (rowIndex) {
        const previousAccessDate = date(rows[rowIndex - 1].accessDate);
        if (currentAccessDate === previousAccessDate) {
            return '';
        }
    }
    return currentAccessDate;
};

export const addressFormatter = (addressLocation, row, config) => {
    const { googleMapLinkUrl, searchConfig } = config;
    const doNotDisclose = row.doNotDisclose && <DoNotDisclose labels={searchConfig.labels.doNotDisclose} />;
    const url = `${googleMapLinkUrl}${encodeURI(addressLocation)}`;
    if (addressLocation === 'None') {
        return;
    }
    if (addressLocation === null) {
        return row.accessAllowed ? '' : <i>{searchConfig.labels.infoProtected}</i>;
    } else {
        return row.accessAllowed ? (
            <MapLink url={url} term={addressLocation} target="_blank">
                {doNotDisclose}
                {addressLocation}
            </MapLink>
        ) : (
                <i>{searchConfig.labels.infoProtected}</i>
            );
    }
};

const handleWarningsClick = (person, config) => {
    const permissions = config.permissions || {};
    const canManageWarnings = permissions.canAddWarning || permissions.canUpdateWarning;

    let eventType;
    if (canManageWarnings) {
        eventType = 'addEditPersonWarning';
    } else {
        eventType = 'viewPersonWarning';
    }
    //createcustom event
    const event = new CustomEvent(eventType, {
        bubbles: true,
        cancelable: true,
        detail: {
            personId: person.id
        }
    });

    document.dispatchEvent(event);
};

export const nameFormatter = (name, row, codedEntries, onNameClicked, config, labels) => {
    const permissions = config.permissions;
    const person = row.accessedPerson ? row.accessedPerson : row;
    let content = '';
    if (isSecured(row)) {
        content = (
            <div>
                <i>{labels.notAuthorised}</i> - {labels.accessDenied}
            </div>
        );
    } else {
        content = (
            <a
                key={`name-${person.id || person.personIdentifier}`}
                href="#none"
                onClick={() => onNameClicked(person)}
                title={`view records for ${person.name}`}>
                <Warnings
                    key="warnings"
                    subject={row}
                    type="person"
                    labels={{}}
                    canManageWarnings={permissions.canAddWarning || permissions.canUpdateWarning}
                    category={codedEntries.warnings}
                    onClick={() => handleWarningsClick(person, config)}
                />
                {`${person.name} (${person.personPersonId || person.personIdentifier})`}
            </a>
        );
    }
    return content;
};
