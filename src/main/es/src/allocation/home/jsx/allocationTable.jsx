'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '../../../table/jsx/table';
import { formatCodedEntry } from '../../../codedEntry/codedEntry';
import memoizeOne from 'memoize-one';
import { accessDateFormatter, dobFormatter, contactsFormatter, addressFormatter, nameFormatter } from './formatters';
import '../less/app.less';

class AllocationTable extends Component {
    static propTypes = {
        config: PropTypes.shape({
            searchConfig: PropTypes.shape({
                labels: PropTypes.object.isRequired
            }).isRequired,
            googleMapLinkUrl: PropTypes.string,
            permissions: PropTypes.object.isRequired
        }).isRequired,
        records: PropTypes.array.isRequired,
        onNameClicked: PropTypes.func.isRequired,
        codedEntries: PropTypes.shape({
            warnings: PropTypes.object.isRequired,
            contactType: PropTypes.object.isRequired,
            gender: PropTypes.object.isRequired
        }).isRequired,
        tabName: PropTypes.string.isRequired,
        loading: PropTypes.bool,
        isRecentlyAccessedTab: PropTypes.bool
    };

    getColumns = memoizeOne(labels => {
        const { config, codedEntries, onNameClicked, isRecentlyAccessedTab } = this.props;
        const columns =  ([
            {
                key: 'name',
                label: labels.name,
                formatter: (name, row) => nameFormatter(name, row, codedEntries, onNameClicked, config, labels),
                width: isRecentlyAccessedTab ? '22%' : '25%'
            },
            {
                key: isRecentlyAccessedTab ? 'accessedPerson.gender' : 'gender',
                label: labels.gender,
                formatter: gender => formatCodedEntry(codedEntries.gender, gender),
                width: '10%'
            },
            {
                key: isRecentlyAccessedTab ? 'accessedPerson.dateOfBirth' : 'dateOfBirth',
                label: labels.dob,
                formatter: (dob, row) => dobFormatter(dob, row, isRecentlyAccessedTab),
                width: '15%'
            },
            {
                key: 'contacts',
                label: labels.contactNumber,
                formatter: (contacts, row) => contactsFormatter(contacts, row, codedEntries),
                width: '15%'
            },
            {
                key: 'address',
                label: labels.address,
                formatter: (address, row) => addressFormatter(address, row, config),
                width: isRecentlyAccessedTab ? '30%' : '35%'
            }
        ]);

        if (isRecentlyAccessedTab) {
            columns.splice(0, 0, {
                key: 'accessDate',
                label: labels.dateAccessed,
                formatter: accessDateFormatter,
                width: '8%'
            });
        }
        return columns;
    });

    getTable() {
        const { tabName, config, records, loading } = this.props;
        const { labels } = config.searchConfig;
        return <Table key={tabName} data={records} columns={this.getColumns(labels)} loading={loading} />;
    }

    render() {
        const { records, config } = this.props;
        const { labels } = config.searchConfig;
        const content = (records.length === 0) ? <div className="value">{labels.noRecords}</div> : this.getTable();
        return <div className={`${this.props.tabName}-allocation-container`}>{content}</div>;
    }
}

export default AllocationTable;
