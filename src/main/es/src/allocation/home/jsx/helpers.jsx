'use strict';
export const clearErrors = (event, component) => {
    event.preventDefault();
    component.setState({ errors: null });
};

export const isSecured = data => {
    return !!(data['_securityMetaData'] || {})['secured'];
};

