'use strict';
import { getFilterRecord, VALUE_TYPES } from '../../../filter/filterFactory/filterFactory';
const { TEXT, INTEGER, ARRAY } = VALUE_TYPES;

export const filterConfig = [
    {
        filterName: 'nameOrId',
        filter: getFilterRecord({ valueType: TEXT, initValue: '' })
    },
    {
        filterName: 'minAge',
        filter: getFilterRecord({ valueType: INTEGER, initValue: null })
    },
    {
        filterName: 'maxAge',
        filter: getFilterRecord({ valueType: INTEGER, initValue: null })
    },
    {
        filterName: 'postcode',
        filter: getFilterRecord({ valueType: TEXT, initValue: '' })
    },
    {
        filterName: 'houseStreetTown',
        filter: getFilterRecord({ valueType: TEXT, initValue: '' })
    },
    {
        filterName: 'gender',
        filter: getFilterRecord({ valueType: TEXT, initValue: '' })
    },
    {
        filterName: 'selectedTeamIds',
        filter: getFilterRecord({ valueType: ARRAY, initValue: [] })
    },
    {
        filterName: 'selectedColleaguePersonIds',
        filter: getFilterRecord({ valueType: ARRAY, initValue: [] })
    }
];
