'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../js/cfg';
import Loading from '../../../../loading/loading';
import ErrorMessage from '../../../../error/error';
import PaginatedResultsList from '../../../../results/paginatedResultsList';
import Paginator from '../../../../paginator/jsx/view';
import ModelList from '../../../../model/modelList';
import AllocationTable from '../allocationTable';
import { subs } from '../../../../subs/subs';
import { clearErrors } from '../helpers';

class AccessedClientsView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            totalResultsCount: null,
            loading: true,
            errors: null,
            rowsPerPage: 10,
            currentPage: 1
        };
    }
    static propTypes = {
        accessedClientsConfig: PropTypes.shape({
            labels: PropTypes.object.isRequired,
            searchConfig: PropTypes.object.isRequired,
            url: PropTypes.string.isRequired,
            viewPersonUrl: PropTypes.string.isRequired
        }),
        codedEntries: PropTypes.object.isRequired,
        toolbar: PropTypes.shape({
            disableButton: PropTypes.func.isRequired,
            enableButton: PropTypes.func.isRequired
        })
    };

    componentDidMount() {
        this.getRecords();
        this.props.toolbar.disableButton('filter');
    }

    componentWillUnmount = () => {
        this.props.toolbar.enableButton('filter');
    };
    onAccessedPersonClicked = person => {
        const personId = person.id;
        const { viewPersonUrl } = this.props.accessedClientsConfig.searchConfig;
        window.location = subs(viewPersonUrl, { personId: personId });
    };

    getRecords() {
        this.loadAccessedRecords()
            .then(data => {
                this.setState({
                    data: data.results,
                    totalResultsCount: data.totalSize,
                    loading: false,
                    errors: null
                });
            })
            .catch(reject =>
                this.setState({
                    errors: reject,
                    loading: false,
                    data: []
                })
            );
    }
    loadAccessedRecords() {
        const { url } = this.props.accessedClientsConfig;
        return this.getListLoad(url, endpoint.accessedClientsEndpoint);
    }
    getListLoad(url, endPointConfig) {
        const { rowsPerPage, currentPage } = this.state;
        return new PaginatedResultsList().load(
            {
                ...endPointConfig,
                url: new ModelList(url).getURL({
                    userId: this.props.accessedClientsConfig.searchConfig.userId
                })
            },
            rowsPerPage,
            currentPage
        );
    }

    onChangeRowsPerPage = (rowsPerPage, currentPage) => {
        this.setState({ rowsPerPage, currentPage }, () => this.getRecords());
    };
    onChangePage = currentPage => {
        this.setState({ currentPage }, () => this.getRecords());
    };

    renderColumns() {
        const { data, loading, totalResultsCount, rowsPerPage, currentPage } = this.state;
        const { accessedClientsConfig, codedEntries } = this.props;
        return (
            <div className="accessedClients-content">
                <AllocationTable
                    tabName="recentlyAccessed"
                    config={accessedClientsConfig}
                    records={data}
                    codedEntries={codedEntries}
                    onNameClicked={this.onAccessedPersonClicked}
                    loading={loading}
                    isRecentlyAccessedTab={true}
                />
                <Paginator
                    onChangeRowsPerPage={this.onChangeRowsPerPage}
                    onChangePage={this.onChangePage}
                    rowsPerPage={rowsPerPage}
                    currentPage={currentPage}
                    totalResultsCount={totalResultsCount}
                />
            </div>
        );
    }

    render() {
        const { errors, loading } = this.state;
        let content;

        if (loading) {
            content = <Loading />;
        } else if (errors) {
            content = <ErrorMessage errors={errors} onClose={e => clearErrors(e, this)} />;
        } else {
            content = this.renderColumns();
        }
        return <div className="accessedClients">{content}</div>;
    }
}

export default AccessedClientsView;
