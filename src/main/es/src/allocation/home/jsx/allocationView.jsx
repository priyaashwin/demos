'use strict';
import React, { useCallback, useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../../loading/loading';
import ErrorMessage from '../../../error/error';
import AllocationTable from './allocationTable';
import ModelList from '../../../model/modelList';
import endpoint from '../js/cfg';
import Paginator from '../../../paginator/jsx/view';
import PaginatedResultsList from '../../../results/paginatedResultsList';
import PersonFilter from '../filters/person';
import AddressFilter from '../filters/address';
import ColleagueNameFilterTab from './colleaguesAllocation/colleagueNameFilterTab';
import { Tab } from '../../../tabs/jsx/tabView';
import { subs } from '../../../subs/subs';
import Filter from '../../../filter/view';
import ActiveFilters from '../../../filter/activeFilter/view';
import FilterLabelIcon from '../../../filter/components/filterIcon';
import { filterContext } from '../../../filter/filterContextProvider';
import useFilteredResults from '../../../filter/useFilteredResults/useFilteredResults';

const AllocationView = props => {
    const { tabName, config } = props;
    const { searchConfig, url } = config;
    const { title } = searchConfig;

    const getPaginatedResults = useCallback(
        ({ nameOrId, minAge, maxAge, gender, postcode, houseStreetTown, selectedTeamIds, selectedColleaguePersonIds }, rowsPerPage, currentPage) => {
            const { teamId, userId } = searchConfig;
            const nameOrIdFilterValue = nameOrId ? nameOrId.getValue() : '';
            const minAgeFilterValue = minAge ? minAge.getValue() : '';
            const maxAgeFilterValue = maxAge ? maxAge.getValue() : '';
            const genderFilterValue = gender ? gender.getValue() : '';
            const postcodeFilterValue = postcode ? postcode.getValue() : '';
            const houseStreetTownFilterValue = houseStreetTown ? houseStreetTown.getValue() : '';
            const selectedTeamIdsFilterValue = selectedTeamIds ? selectedTeamIds.getValue().toString() : '';
            const selectedColleaguePersonIdsFilterValue = selectedColleaguePersonIds ? selectedColleaguePersonIds.getValue().toString() : '';

            const modelObject = {
                nameOrId: nameOrIdFilterValue,
                minAge: minAgeFilterValue,
                maxAge: maxAgeFilterValue,
                gender: genderFilterValue,
                postCode: postcodeFilterValue,
                houseStreetTown: houseStreetTownFilterValue
            };

            switch (tabName) {
                case 'allocation':
                    modelObject.userId = userId;
                    modelObject.teamId = teamId;
                    break;
                case 'team':
                    modelObject.teamId = teamId;
                    break;
                case 'colleagues':
                    modelObject.userId = userId;
                    modelObject.teamId = teamId;
                    modelObject.selectedTeamIds = selectedTeamIdsFilterValue;
                    modelObject.selectedColleaguePersonIds = selectedColleaguePersonIdsFilterValue;
                    break;
            }

            return new PaginatedResultsList().load(
                {
                    ...endpoint.professionRoles,
                    url: new ModelList(url).getURL(modelObject)
                },
                rowsPerPage,
                currentPage
            );
        },
        [url, searchConfig, tabName]
    );

    const {
        setRowsPerPage,
        rowsPerPage,
        setCurrentPage,
        currentPage,
        clearErrors,
        loading,
        errors,
        totalResultsCount,
        data: records
    } = useFilteredResults(getPaginatedResults);

    const onChangeRowsPerPage = (newRowsPerPage, newCurrentPage) => {
        setRowsPerPage(newRowsPerPage);
        setCurrentPage(newCurrentPage);
    };

    const onChangePage = newCurrentPage => {
        setCurrentPage(newCurrentPage);
    };

    const [isShowFilterActive, setIsShowFilterActive] = useState(true);
    const { showFilter } = props;

    useEffect(() => {
        setIsShowFilterActive(isShowFilterActive => !isShowFilterActive);
    }, [showFilter]);

    const onNameClicked = person => {
        const { viewPersonUrl } = searchConfig;
        window.location = subs(viewPersonUrl, { personId: person.id });
    };

    const context = useContext(filterContext);
    const activeFilters = context.getActiveFilters();
    const personTabActive = activeFilters.nameOrId || activeFilters.gender || activeFilters.minAge || activeFilters.maxAge;
    const addressTabActive = activeFilters.postcode || activeFilters.houseStreetTown;
    const colleaguesNameTabActive = activeFilters.selectedTeamIds || activeFilters.selectedColleaguePersonIds;

    const tabsLabelIcon = (labels, context) => (
        <>
            {labels}
            {context ? <FilterLabelIcon /> : ''}
        </>
    );

    const renderPageContent = () => {
        const { codedEntries, resultsFilterConfig } = props;
        const { labels } = resultsFilterConfig;
        return (
            <>
                <ActiveFilters resultsFilterConfig={resultsFilterConfig} />
                {isShowFilterActive && (
                    <Filter>
                        <Tab
                            key={labels.nameOrIdFilter}
                            label={tabsLabelIcon(labels.nameOrIdFilter, personTabActive)}
                            tabId="person">
                            <PersonFilter labels={labels} />
                        </Tab>
                        <Tab
                            key={labels.addressFilter}
                            label={tabsLabelIcon(labels.addressFilter, addressTabActive)}
                            tabId="address">
                            <AddressFilter labels={labels} />
                        </Tab>
                        {tabName === 'colleagues' &&
                            <Tab
                                key={labels.colleagueNameFilter}
                                label={tabsLabelIcon(labels.colleagueNameFilter, colleaguesNameTabActive)}
                                tabId={labels.colleagueNameFilter}>
                                <ColleagueNameFilterTab config={config} resultsFilterConfig={resultsFilterConfig} />
                            </Tab>
                        }
                    </Filter>
                )}
                <AllocationTable
                    tabName={tabName}
                    config={config}
                    records={records}
                    codedEntries={codedEntries}
                    onNameClicked={onNameClicked}
                />
                <Paginator
                    onChangeRowsPerPage={onChangeRowsPerPage}
                    onChangePage={onChangePage}
                    rowsPerPage={rowsPerPage}
                    currentPage={currentPage}
                    totalResultsCount={totalResultsCount}
                />
            </>
        );
    };

    let pageContent = '';
    if (loading) {
        pageContent = <Loading />;
    } else if (errors) {
        pageContent = <ErrorMessage errors={errors} onClose={clearErrors} />;
    } else {
        pageContent = renderPageContent();
    }
    return (
        <div className="pageContentContainer">
            <h4>{title}</h4>
            {pageContent}
        </div>
    );
};

export default AllocationView;

AllocationView.propTypes = {
    tabName: PropTypes.string.isRequired,
    config: PropTypes.shape({
        searchConfig: PropTypes.shape({
            userId: PropTypes.number,
            teamId: PropTypes.number,
            viewPersonUrl: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired
        }).isRequired,
        url: PropTypes.string.isRequired,
        namesUrl: PropTypes.string,
        teamsUrl: PropTypes.string
    }).isRequired,
    codedEntries: PropTypes.object.isRequired,
    showFilter: PropTypes.bool.isRequired,
    resultsFilterConfig: PropTypes.object.isRequired
};
