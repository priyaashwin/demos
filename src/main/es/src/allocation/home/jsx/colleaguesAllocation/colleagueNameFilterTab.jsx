'use strict';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import endPoint from '../../js/cfg';
import Model from '../../../../model/model';
import { getCancelToken } from 'usp-xhr';
import ColleagueNameFilter from '../../filters/colleagueName';

const ColleagueNameFilterTab = props => {
    const { searchConfig, namesUrl, teamsUrl } = props.config;
    const { userId, teamId } = searchConfig;
    const { labels } = props.resultsFilterConfig;
    const { namesEndPoint, teamsEndPoint } = endPoint;

    const [namesError, setNamesError] = useState(null);
    const [namesLoaded, setNamesLoaded] = useState(false);
    const [teamsError, setTeamsError] = useState(null);
    const [teamsLoaded, setTeamsLoaded] = useState(false);
    const [names, setNames] = useState([]);
    const [teams, setTeams] = useState([]);

    useEffect(() => {
        const cancelToken = getCancelToken();

        new Model(namesUrl).load(namesEndPoint, { teamId: teamId, userId: userId }, cancelToken).then(result => {
            setNamesLoaded(true);
            setNames(result || []);
        }).catch(reject => {
            setNamesLoaded(true);
            setNamesError(reject);
            setNames([]);
        });
        return () => { cancelToken.cancel(); };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        const cancelToken = getCancelToken();

        new Model(teamsUrl).load(teamsEndPoint, { teamId: teamId }, cancelToken).then(result => {
            setTeamsLoaded(true);
            setTeams(result || []);
        }).catch(reject => {
            setTeamsLoaded(true);
            setTeamsError(reject);
            setTeams([]);
        });
        return () => { cancelToken.cancel(); };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <ColleagueNameFilter
            labels={labels}
            namesError={namesError}
            namesLoaded={namesLoaded}
            teamsError={teamsError}
            teamsLoaded={teamsLoaded}
            namesRecord={names}
            teamsRecord={teams}
        />
    );
};

export default ColleagueNameFilterTab;

ColleagueNameFilterTab.propTypes = {
    config: PropTypes.shape({
        searchConfig: PropTypes.shape({
            userId: PropTypes.number.isRequired,
            teamId: PropTypes.number.isRequired
        }).isRequired,
        namesUrl: PropTypes.string,
        teamsUrl: PropTypes.string
    }).isRequired,
    resultsFilterConfig: PropTypes.object.isRequired
};