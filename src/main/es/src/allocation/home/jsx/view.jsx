'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TabView, Tab } from '../../../tabs/jsx/tabView';
import AccessedClients from './AccessedClients/view';
import AllocationView from './allocationView';
import { filterConfig } from './filtersConfig';
import { FilterContextProvider } from '../../../filter/filterContextProvider';

const tabs = ['allocation', 'team', 'colleagues'];

class AllocationTabView extends Component {
    static propTypes = {
        codedEntries: PropTypes.shape({
            gender: PropTypes.object.isRequired,
            warnings: PropTypes.object.isRequired,
            contactType: PropTypes.object.isRequired
        }),
        resultsFilterConfig: PropTypes.shape({
            labels: PropTypes.object.isRequired
        }),
        showFilter: PropTypes.bool.isRequired,
        toolbar: PropTypes.object.isRequired,
        allocationConfig: PropTypes.shape({
            tab: PropTypes.shape({
                title: PropTypes.string.isRequired
            }).isRequired,
            url: PropTypes.string.isRequired,
            permissions: PropTypes.shape({
                canAddWarning: PropTypes.bool.isRequired,
                canUpdateWarning: PropTypes.bool.isRequired
            }),
            searchConfig: PropTypes.object.isRequired,
            googleMapLinkUrl: PropTypes.string.isRequired
        }),
        teamAllocationConfig: PropTypes.shape({
            tab: PropTypes.shape({
                title: PropTypes.string.isRequired
            }).isRequired,
            url: PropTypes.string.isRequired,
            permissions: PropTypes.shape({
                canAddWarning: PropTypes.bool.isRequired,
                canUpdateWarning: PropTypes.bool.isRequired,
                canViewTeamsTab: PropTypes.bool.isRequired
            }),
            searchConfig: PropTypes.object.isRequired,
            googleMapLinkUrl: PropTypes.string.isRequired
        }),
        colleaguesAllocationConfig: PropTypes.shape({
            tab: PropTypes.shape({
                title: PropTypes.string.isRequired
            }).isRequired,            url: PropTypes.string.isRequired,
            permissions: PropTypes.shape({
                canAddWarning: PropTypes.bool.isRequired,
                canUpdateWarning: PropTypes.bool.isRequired,
                canViewColleaguesTab: PropTypes.bool.isRequired
            }),
            searchConfig: PropTypes.object.isRequired,
            googleMapLinkUrl: PropTypes.string.isRequired
        }),
        accessedClientsConfig: PropTypes.shape({
            tab: PropTypes.shape({
                title: PropTypes.string.isRequired
            }).isRequired,            url: PropTypes.string.isRequired,
            permissions: PropTypes.shape({
                canAddWarning: PropTypes.bool.isRequired,
                canUpdateWarning: PropTypes.bool.isRequired
            }),
            searchConfig: PropTypes.object.isRequired,
            googleMapLinkUrl: PropTypes.string.isRequired
        })
    };

    getTab = (tabName) => {
        const { codedEntries, resultsFilterConfig, showFilter } = this.props;
        let config;
        let canViewPermission = true;

        switch (tabName) {
            case 'allocation':
                config = this.props.allocationConfig;
                break;
            case 'team':
                config = this.props.teamAllocationConfig;
                canViewPermission = config.permissions.canViewTeamsTab;
                break;
            case 'colleagues':
                config = this.props.colleaguesAllocationConfig;
                canViewPermission = config.permissions.canViewColleaguesTab;
                break;
        }

        return (canViewPermission &&
            <Tab key={tabName} label={config.tab.title} tabId={tabName}>
                <FilterContextProvider key={`${tabName}ContextProvider`} filterConfig={filterConfig}>
                    <AllocationView
                        tabName={tabName}
                        config={config}
                        codedEntries={codedEntries}
                        resultsFilterConfig={resultsFilterConfig}
                        showFilter={showFilter}
                    />
                </FilterContextProvider>
            </Tab>
        );
    }

    render() {
        const { codedEntries, accessedClientsConfig, toolbar } = this.props;

        return (
            <TabView key="roleTabs">
                {tabs.map(tab => this.getTab(tab))}
                <Tab key="accessedClients" label={accessedClientsConfig.tab.title} tabId="accessedClients">
                    <AccessedClients
                        codedEntries={codedEntries}
                        accessedClientsConfig={accessedClientsConfig}
                        url={accessedClientsConfig.url}
                        labels={accessedClientsConfig.searchConfig}
                        toolbar={toolbar}
                    />
                </Tab>
            </TabView>
        );
    }
}

export default AllocationTabView;
