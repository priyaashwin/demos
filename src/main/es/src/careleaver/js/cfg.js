'use strict';

class Endpoint{
  constructor() {
    this.careLeaverDetailsEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.childlookedafter.CareLeaver+json'
      },
      responseType: 'json',
      responseStatus:200
    };
  }
}

export default new Endpoint();
