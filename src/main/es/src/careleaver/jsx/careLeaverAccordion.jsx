'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Accordion from 'usp-accordion-panel';
import CareLeaverDetails from './careLeaverDetails';

export default class CareLeaverAccordionView extends Component {
  static propTypes={
    labels: PropTypes.object.isRequired,
    codedEntries: PropTypes.object.isRequired,
    record: PropTypes.object
  };

  renderAccordionView() {
    const {labels, codedEntries, record} = this.props;

    return (
        <div id="accordion-container">
          <Accordion expanded={true} title={labels.accordionTitle}>
            <CareLeaverDetails labels={labels} codedEntries={codedEntries} record={record}></CareLeaverDetails>
          </Accordion>
        </div>
    );
  }
  render(){
    const {record} = this.props;
    let content;

    if(record) {
      content = this.renderAccordionView();
    } else {
      content = <div>There are no existing care leaver details.</div>;
    }

    return (
      <div className="carer-leaver-container">
        {content}
      </div>
    );
  }
}
