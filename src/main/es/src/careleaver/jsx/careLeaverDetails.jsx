'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatCodedEntry } from '../../codedEntry/codedEntry';
import { formatDate } from '../../date/date';

export default class CareLeaverDetails extends Component {
  static propTypes={
    labels: PropTypes.object.isRequired,
    codedEntries: PropTypes.object.isRequired,
    record: PropTypes.object.isRequired
  };

  render() {
    const {labels, codedEntries, record} = this.props;

    return (
      <div id="care-leaver-details">
        <div className="care-leaver-column">
          <div className="l-box">
            <label htmlFor="careLeaverEligibility">{labels.careLeaverEligibility}</label>
            <div id="careLeaverEligibility" name="careLeaverEligibility" className="value">
              {formatCodedEntry(codedEntries.careLeaverEligibility, record.careLeaverEligibility)}
            </div>
          </div>
          <div className="l-box">
            <label htmlFor="mainActivity">{labels.mainActivity}</label>
            <div id="mainActivity" name="mainActivity" className="value">
              {formatCodedEntry(codedEntries.mainActivity, record.mainActivity)}
            </div>
          </div>
          <div className="l-box">
            <label htmlFor="accommodation">{labels.accommodation}</label>
            <div id="accommodation" name="accommodation" className="value">
              {formatCodedEntry(codedEntries.accommodation, record.accommodation)}
            </div>
          </div>
          <div className="l-box">
            <label htmlFor="inTouch">{labels.inTouch}</label>
            <div id="inTouch" name="inTouch" className="value">
              {formatCodedEntry(codedEntries.inTouch, record.inTouch)}
            </div>
          </div>
        </div>
        <div className="care-leaver-column">
          <div className="l-box">
            <label htmlFor="contactDate">{labels.contactDate}</label>
            <div id="contactDate" name="contactDate" className="value">
              {formatDate(record.contactDate)}
            </div>
          </div>
          <div className="l-box">
            <label htmlFor="reasonQualifying">{labels.reasonQualifying}</label>
            <div id="reasonQualifying" name="reasonQualifying" className="value">
              {formatCodedEntry(codedEntries.reasonQualifying, record.reasonQualifying)}
            </div>
          </div>
          <div className="l-box">
            <label htmlFor="accommodationSuitability">{labels.accommodationSuitability}</label>
            <div id="accommodationSuitability" name="accommodationSuitability" className="value">
              {formatCodedEntry(codedEntries.accommodationSuitability, record.accommodationSuitability)}
            </div>
          </div>
          <div className="l-box">
            <label htmlFor="multipleOccupancy">{labels.multipleOccupancy}</label>
            <div id="multipleOccupancy" name="multipleOccupancy" className="value">
              {(record.multipleOccupancy? 'Yes' : 'No')}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
