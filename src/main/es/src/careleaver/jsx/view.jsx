'use strict';

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import endpoint from '../js/cfg';
import ErrorMessage from '../../error/error';
import Loading from '../../loading/loading';
import Model from '../../model/model';
import CareLeaverAccordionView from './careLeaverAccordion';

export default class CareLeaverView extends Component {
  static propTypes={
    labels: PropTypes.object.isRequired,
    codedEntries: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired,
    subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    permissions : PropTypes.object.isRequired
  };

  state={
    record: null,
    errors: null,
    loading: true
  };

  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({errors:null});
  }

  componentDidUpdate(prevProps) {
    if (!(prevProps.url === this.props.url && prevProps.subjectId === this.props.subjectId)) {
      this.loadData();
    }
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    const {subjectId, url}= this.props;

    new Model(url).load(endpoint.careLeaverDetailsEndpoint, {
      subjectId
    }).then(result=> this.setState({
        loading: false,
        errors: null,
        record: result
     })).catch(reject=> {
      const noRecord = (reject.code === 404);
      this.setState({
        loading: false,
        errors: noRecord ? null : reject,
        record: null
      });
    });
  }

  render() {
    const {errors, loading} = this.state;
    let content;

    if(loading) {
      content = (<Loading />);
    } else if(errors) {
      content = (<ErrorMessage errors={errors} onClose={this.clearErrors} />);
    } else {
      content = this.renderView();
    }

    return (<div className="care-leaver">{content}</div>);
  }

  renderView() {
    const {labels, codedEntries} = this.props;
    const {record} = this.state;

    return (<CareLeaverAccordionView
      labels={labels}
      record={record}
      codedEntries={codedEntries}
    />);
  }
}
