'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../infopop/infopop';

const WarningIcon = ({ children, useMarkup = true, ...other }) => (
  <InfoPop
    className="warning"
    useMarkup={useMarkup}
    align="bottom"
    {...other}>
    <i className="fa fa-exclamation-triangle" />
    {children}
  </InfoPop>
);

WarningIcon.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
  onClick: PropTypes.func.isRequired,
  useMarkup: PropTypes.bool
};
export default WarningIcon;
