'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import WarningIcon from '../icon';
import { formatCodedEntry } from '../../codedEntry/codedEntry';
import { isFirstGTSecondDate } from '../../date/date';

export default class Warnings extends PureComponent {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(e) {
        e.preventDefault();

        this.props.onClick();
    }
    getPersonWarnings() {
        const { subject, category } = this.props;
        let personWarnings = [];
        const todaysDate = new Date();

        const subjectWarnings = subject.personWarnings || [];
        if (subjectWarnings.length > 0) {
            //take item from index 0 this is the assigned person warnings
            personWarnings = subjectWarnings[0].personWarningAssignmentVOs;
        }

        return personWarnings
            .filter(
                warning =>
                    (!warning.authorisation ||
                        (warning.authorisation.authorisationState !== 'PENDING' &&
                            warning.authorisation.authorisationState !== 'REJECTED')) &&
                    (warning.endDate === undefined ||
                        warning.endDate === null ||
                        isFirstGTSecondDate(warning.endDate, todaysDate.getTime()))
            )
            .map(warning => formatCodedEntry(category, warning.codedEntryVO.code));
    }
    render() {
        const { subject, canManageWarnings, labels, showDetails } = this.props;

        const personWarnings = this.getPersonWarnings();

        if (!showDetails && personWarnings.length > 0) {
            return (
                <div className="warnings">
                    <WarningIcon title={labels.warnings || 'Warnings'} onClick={this.handleClick} useMarkup={false} />
                </div>
            );
        }

        let warningsContent;

        if (personWarnings.length === 0) {
            const relatedPersonWarnings = subject.numberOfRelatedPersonWarnings;
            if (showDetails && relatedPersonWarnings > 0) {
                warningsContent = (
                    <WarningIcon
                        title={labels.related || 'Warnings for relation'}
                        onClick={this.handleClick}
                        useMarkup={false}
                    />
                );
                return <div className="warnings">{warningsContent}</div>;
            } else if (!showDetails && relatedPersonWarnings > 0) {
                return (
                    <div className="warnings">
                        <WarningIcon
                            title={labels.warnings || 'Warnings'}
                            onClick={this.handleClick}
                            useMarkup={false}
                        />
                    </div>
                );
            }
        } else {
            const clickTitle = `Click the icon to ${
                canManageWarnings ? labels.manage || 'manage warnings' : labels.view || 'view warnings'
            }`;
            const title = `This person has ${personWarnings.length} warning${personWarnings.length > 1 ? 's' : ''}`;

            warningsContent = (
                <WarningIcon title={title} onClick={this.handleClick}>
                    <span>{personWarnings.length}</span>
                    <div className="content-markup">
                        <div className="warnings-popup">
                            <ul className="warnings-list">
                                {personWarnings.map((warning, i) => (
                                    <li key={i}>{warning}</li>
                                ))}
                            </ul>
                            <div className="click-text">{clickTitle}</div>
                        </div>
                    </div>
                </WarningIcon>
            );
            return <div className="warnings">{warningsContent}</div>;
        }

        return <div />;
    }
}

Warnings.propTypes = {
    labels: PropTypes.object.isRequired,
    subject: PropTypes.object.isRequired,
    canManageWarnings: PropTypes.bool.isRequired,
    category: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
    showDetails: PropTypes.bool
};

Warnings.defaultProps = {
    showDetails: true
};
