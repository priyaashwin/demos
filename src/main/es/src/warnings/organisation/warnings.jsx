'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import WarningIcon from '../icon';

export default class Warnings extends PureComponent{
  render(){
    const {subject}=this.props;

    let warningsContent;

    const warningPersonNameAndIdList = subject.warningPersonNameAndIdList || [];

    if(warningPersonNameAndIdList.length > 0){
      const title=`One or more warnings exist for ${warningPersonNameAndIdList.length} ${warningPersonNameAndIdList.length===1?'person':'people'} in this organisation`;

      warningsContent=(
        <WarningIcon title={title} onClick={()=>{}}>
          <span>{warningPersonNameAndIdList.length}</span>
          <div className="content-markup">
            <div className="warnings-popup">
              <ul className="warnings-list">
                {warningPersonNameAndIdList.map((person,i )=>(
                  <li key={i}>{person.name} {person.personIdentifier}</li>
                ))}
              </ul>
            </div>
          </div>
        </WarningIcon>
      );
      return(<div className="warnings">{warningsContent}</div>);
    }
    return(<div />);
  }
}

Warnings.propTypes={
  labels: PropTypes.object.isRequired,
  subject: PropTypes.object.isRequired
};
