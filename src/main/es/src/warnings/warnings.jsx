'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PersonWarnings from './person/warnings';
import OrganisationWarnings from './organisation/warnings';

const Warnings=({type, ...other})=>{
  let content;
  switch(type){
    case 'person':
      content=(<PersonWarnings {...other}/>);
    break;
    case 'organisation':
      content=(<OrganisationWarnings {...other}/>);
      break;
    default:
      content=(<div />);
  }

  return content;
};

Warnings.propTypes={
  type:PropTypes.oneOf(['person','organisation']).isRequired
};

export default Warnings;
