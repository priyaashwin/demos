'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const OverlayContent=({className, style, children})=>(
  <div className={className} style={style} role="tooltip">
    <div className="info-overlay-content">
      {children}
    </div>
  </div>
);

OverlayContent.propTypes={
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object
};

export default OverlayContent;
