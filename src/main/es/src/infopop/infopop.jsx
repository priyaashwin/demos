'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const InfoPop = memo(({ title, content, useMarkup, align, className, onClick, children }) => (
  <div className={classnames(className, 'pop-up-data')} data-title={title} data-usemarkup={useMarkup} data-align={align} data-content={content} aria-label={title} onClick={onClick} tabIndex={0}>
    {children}
  </div>
));

InfoPop.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  useMarkup: PropTypes.bool.isRequired,
  children: PropTypes.node,
  onClick: PropTypes.func,
  className: PropTypes.string,
  align: PropTypes.string
};

InfoPop.defaultProps = {
  useMarkup: false,
  align: 'top'
};
export default InfoPop;
