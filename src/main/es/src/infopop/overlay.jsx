'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Overlay from 'react-bootstrap/lib/Overlay';
import OverlayContent from './overlayContent';
import classnames from 'classnames';

export default class InfoOverlay extends PureComponent {
  static propTypes = {
    target: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
    placement: PropTypes.oneOf(['top', 'right', 'bottom', 'left']),
    children: PropTypes.node,
    className: PropTypes.string,
    visible: PropTypes.bool.isRequired
  };

  static defaultProps = {
    placement: 'bottom'
  };

  render() {
    const {placement, target, visible, className, children, ...other} = this.props;
    return (
      <div style={{
        position: 'relative'
      }}>
        <Overlay show={visible} placement={placement} container={this} target={target} {...other}>
          <OverlayContent className={classnames('info-overlay', className)}>{children}</OverlayContent>
        </Overlay>
      </div>
    );
  }
}
