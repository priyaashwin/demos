'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Message = ({ children }) => (
    <div key="message" className="pure-alert pure-alert-custom-icon">        
        {children}
    </div>
);

Message.propTypes = {
    children: PropTypes.any
};

export default Message;