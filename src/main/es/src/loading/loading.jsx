'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Loading = ({icon, message, className }) => (
  <div className={classnames('loading', className)}>
    <span>
      <i className={icon} aria-hidden="true" />
      <span>{message}</span>
    </span>
  </div>
);

Loading.propTypes = {
  icon: PropTypes.string.isRequired,
  className: PropTypes.string,
  message: PropTypes.any
};

Loading.defaultProps = {
  icon: 'fa fa-spinner fa-spin fa-fw',
  message: 'Please wait...'
};

export default Loading;
