import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export default class TabList extends PureComponent {
    static propTypes = {
        children: PropTypes.any.isRequired
    };

    render() {
        const { children, ...other } = this.props;
        return (
            <div {...other}>
                <ul className="usp-tabview-list" role="tablist">
                    {children}
                </ul>
            </div>
        );
    }
}