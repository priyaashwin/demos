import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export default class TabContent extends PureComponent {
    static propTypes = {
        tabId: PropTypes.string.isRequired,
        content: PropTypes.any.isRequired
    };

    render() {
        const { tabId, content } = this.props;
        return (
        <div id={`${tabId}_content`} className="l-box usp-tab-panel" role="tabpanel" aria-labelledby={tabId}>
            {content}
        </div>              
        );
    }
}