import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import classnames from 'classnames';

export default class Tab extends PureComponent {
    static defaultProps = {
        selected: false,
        disabled: false
    };

    static propTypes = {
        tabId: PropTypes.string.isRequired,
        selected: PropTypes.bool,
        disabled: PropTypes.bool,
        label: PropTypes.any.isRequired,
        onSelect: PropTypes.func
    };

    handleClick=(e)=>{
        const {tabId, disabled, onSelect}=this.props;
        e.preventDefault();

        if(!disabled && onSelect){
            onSelect(tabId);
        }
    };

    render() {
        const { selected, disabled, tabId, label } = this.props;
        return (
            <li
                key={tabId}
                id={tabId}
                className={classnames('usp-tab', {
                    'usp-tab-active': selected,
                    'usp-tab-disabled': disabled
                })} role="presentation">
                <a href="#none"
                    className="usp-tab-label usp-tab-content"
                    role="tab"
                    aria-selected={selected ? 'true' : 'false'}
                    aria-disabled={disabled ? 'true' : 'false'}
                    disabled={disabled}
                    tabIndex={0}
                    onClick={this.handleClick}>
                    {label}
                </a>
            </li>
        );
    }
}