import PropTypes from 'prop-types';
import React, {Children, isValidElement, PureComponent } from 'react';
import TabList from './list';
import Tab from './tab';
import memoize from 'memoize-one';

class TabView extends PureComponent {
    static propTypes = {
        selected: PropTypes.string,
        children: PropTypes.any
    };

    state = {
        selected: this.props.selected
    };
    componentDidUpdate(prevProps) {
        if (this.props.selected && this.props.selected != prevProps.selected) {
            //force a selection change
            this.setState({ selected: this.props.selected });//eslint-disable-line react/no-did-update-set-state
        }
    }
    handleSelect = (tabId) => {
        this.setState({
            selected: tabId
        });
    };
    _getTabs=memoize(children=>(Children.toArray(children) || []).filter(child=>isValidElement(child)));
    getTabs(){
        const { children } = this.props;
        return this._getTabs(children);
    }
    handleKeyPress = (e) => {
        const { selected } = this.state;
        const tabs = this.getTabs();
        let selectedTabId;

        if (tabs.length && (e.key === 'ArrowLeft' || e.key === 'ArrowRight')) {
            const currentSelectedIndex = tabs.findIndex((tab, i) => (selected == undefined && i == 0) || (tab.props.tabId === selected));
            let selectedIndex;

            switch (e.key) {
                case 'ArrowLeft':
                    selectedIndex = currentSelectedIndex - 1;
                    break;
                case 'ArrowRight':
                    selectedIndex = currentSelectedIndex + 1;
                    break;
            }
            if (selectedIndex < 0) {
                selectedIndex = tabs.length - 1;
            } else if (selectedIndex >= tabs.length) {
                selectedIndex = 0;
            }
            const nextTab = tabs[selectedIndex];
            if (nextTab) {
                selectedTabId = nextTab.props.tabId;
            }
        }
        if (selectedTabId) {
            this.setState({
                selected: selectedTabId
            });
        }
    };
    getTabList() {
        const { selected } = this.state;
        const tabs = this.getTabs();

        return tabs.map((child, i) => React.cloneElement(child, {
            key: i,
            onSelect: this.handleSelect,
            selected: (selected == undefined && i == 0) || (child.props.tabId === selected)
        }));
    }
    getTabContent() {
        const { selected } = this.state;
        let content;
        //find the selected tab from the children
        const tabs = this.getTabs();
        const selectedTab = tabs.find((tab, i) => (selected == undefined && i == 0) || (tab.props.tabId === selected));
        if (selectedTab) {
            //get the children from the selected tab and render those in the tab panel
            content = Children.toArray(selectedTab.props.children) || [];
        } else {
            content = (<div />);
        }

        return content;
    }
    render() {
        return (
            <div className="usp-tabview">
                <TabList onKeyDown={this.handleKeyPress}>
                    {this.getTabList()}
                </TabList>
                <div className="usp-tabview-panel">
                    {this.getTabContent()}
                </div>
            </div>
        );
    }
}

export { Tab, TabView };