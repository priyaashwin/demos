'use strict';
import {
    formatDate,
    formatDateTime,
    parseDate,
    parseDateTime,
    formatDateValue,
    DATE_STRING_REG
} from 'usp-date';

const isFirstGTSecondDate = function (first, second) {
    return setToMidnight(first) > setToMidnight(second);
};

const setToMidnight = function (value) {
    const dateTime = new Date(value);
    dateTime.setHours(0, 0, 0, 0);
    return dateTime.getTime();
};

export { formatDate, formatDateTime, isFirstGTSecondDate, parseDate, parseDateTime, formatDateValue, DATE_STRING_REG };