'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const AccordionMenu=({absencesCount, episodesCount, labels})=>{
  return (
    <span>
      <span>{labels.episodes}</span>
      <span>{': '}</span>
      <span className="resultCount">{episodesCount}</span>
      <span>{' | '}</span>
      <span>{labels.absences}</span>
      <span>{': '}</span>
      <span className="resultCount">{absencesCount}</span>
    </span>
  );
};

AccordionMenu.propTypes={
  absencesCount: PropTypes.number,
  episodesCount: PropTypes.number,
  labels: PropTypes.object
};

export default AccordionMenu;
