'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../date/date';

export default class PeriodOfCareSummaryRow extends React.Component {
  render() {
    const periodOfCare = this.props.periodOfCare,
          labels = this.props.labels;

    return (
      <tr>
        <td data-title={labels.startDate}>{formatDate(periodOfCare.startDate, true)}</td>
        <td data-title={labels.endDate}>{formatDate(periodOfCare.endDate, true)}</td>
        <td data-title={labels.categoryOfNeed}>{periodOfCare.categoryOfNeed ? periodOfCare.categoryOfNeed.name : ''}</td>
        <td data-title={labels.episodes}>{periodOfCare.episodesOfCare.results.length}</td>
        <td data-title={labels.absences}>{periodOfCare.absencesFromCare.results.length}</td>
      </tr>
    );
  }
}

PeriodOfCareSummaryRow.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  periodOfCare: PropTypes.object.isRequired
};
