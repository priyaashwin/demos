'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Subject from '../../../subject/subject';

const Title=({identifier, name, subjectType, title})=>{
  return (
    <h2>
      <span>{title}{' '}</span>
      <Subject name={name} identifier={identifier} subjectType={subjectType} />
    </h2>
  );
};

Title.propTypes={
  identifier: PropTypes.string,
  name: PropTypes.string,
  subjectType: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default Title;
