'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formatters from '../../js/formatters';
import { formatCodedEntry } from '../../../codedEntry/codedEntry';

export default class AdoptionInformationRow extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    record: PropTypes.object,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired,
    adoptionInformation: PropTypes.object.isRequired
  }
  
  render() {
    const { labels, codedEntries, adoptionInformation } = this.props;

    return (
      <tr className={'adoptionInformation-row'}>
        <td data-title={labels.dateShouldBePlaced}>{Formatters.date('dateShouldBePlaced', adoptionInformation)}</td>
        <td data-title={labels.dateMatched}>{Formatters.date('dateMatched', adoptionInformation)}</td>
        <td data-title={labels.datePlaced}>{Formatters.date('datePlaced', adoptionInformation)}</td>
        <td data-title={labels.legalStatusOfAdopters}>{formatCodedEntry(codedEntries.adopterLegalStatus, adoptionInformation.legalStatusOfAdopters)}</td>
        </tr>
    );
  }
}
