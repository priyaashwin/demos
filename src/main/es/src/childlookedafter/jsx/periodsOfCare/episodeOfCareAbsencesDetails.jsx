'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const EpisodeOfCareAbsencesDetails=({ labels, absence }) => {
  return (
    <div className="cla-table-absences-details">
      <div>
        <strong>{labels.absenceType}</strong>
        <span>{absence.absenceType ? absence.absenceType.name:''}</span>
      </div>
      <div>
        <strong>{labels.absenceNote}</strong>
        <span>{absence.absenceNote ? absence.absenceNote.content : ''}</span>
      </div>
    </div>
  );
};

EpisodeOfCareAbsencesDetails.propTypes= {
  labels: PropTypes.object,
  absence: PropTypes.object
};

export default EpisodeOfCareAbsencesDetails;
