'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../date/date';

const AccordionTitle=({endDate, startDate, title})=>{
  return (
    <span>
      <span>{title}</span>
      <span>{` (${formatDate(startDate, true)} ${endDate?'-':''} ${endDate?formatDate(endDate, true):''})` }</span>
    </span>
  );
};

AccordionTitle.propTypes={
  startDate: PropTypes.number,
  endDate: PropTypes.number,
  title: PropTypes.string.isRequired
};

export default AccordionTitle;
