'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formatters from '../../js/formatters';
import TableActions from '../../../table/jsx/actions/actions';
import InfoPop from '../../../infopop/infopop.jsx';
import classnames from 'classnames';

export default class EpisodeOfCareDetailsRow extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    permissions: PropTypes.object.isRequired,
    record: PropTypes.object,
    rowClassName: PropTypes.string,
    viewOnly: PropTypes.bool,
    handleClick: PropTypes.func,
    enums: PropTypes.object.isRequired,
    isDeleteActive: PropTypes.bool
  };
  getActions = () => {
    const { labels, record, permissions, isDeleteActive } = this.props;
    const { tempPlacement } = record.placement;  
    const isDeleteDisabled =  !(permissions.canDelete && isDeleteActive);
  
    if(tempPlacement){
      if(record.endDate !== null){
          return [{
            id: 'viewTemporaryPlacement',
            label: labels.viewTempPlacement
          },
          {
            id:'deleteTemporaryPlacement',
            label: labels.deleteTempPlacement,
            disabled: (!permissions.canDeleteTemporaryPlacement)
          }];
        }
        return [{
          id: 'editTemporaryPlacement',
          label: labels.editTempPlacement
        },
        {
          id:'deleteTemporaryPlacement',
          label: labels.deleteTempPlacement,
          disabled: (!permissions.canDeleteTemporaryPlacement)
        }
      ];
    }
    return [{
      id: 'viewEpisodeOfCare',
      label: labels.view
    },
    {
      id: 'editEpisodeOfCare',
      label: labels.edit,
      disabled: (!permissions.canUpdatePlacement)
    },
    {
        id: 'deleteEpisodeOfCare',
        label: labels.deleteEpisodeOfCare,
        disabled: isDeleteDisabled
     }];
  };

  getCarerRegOrg = (placement, message) => {
    let content;
    let carerSubjOrg;
    let carerOrgName;
    let carerSubjOrgAddLoc;
    const fosterCarerReg = placement.fosterCarerRegistration;

    if(fosterCarerReg !== null){
      carerSubjOrg = fosterCarerReg.organisation.address;
      carerOrgName = fosterCarerReg.organisation.name;
      if(carerSubjOrg !== null){
        carerSubjOrgAddLoc = carerSubjOrg.location;
        if(carerSubjOrgAddLoc !== null){
          content = `${carerOrgName} - ${message.address} : ${carerSubjOrgAddLoc.location}`;
        }else{
          content = `${message.noOrgDetails}`;
        }
      } else {
        content = `${message.noOrgDetails}`;
      }
    } else {
      content = `${message.noRegOrg}`;
    }
    return content;
  }

  render() {
    const { handleClick, labels, record, rowClassName, enums } = this.props;
    const { legalStatuses, placement } = record;
    const { mainPlacement, tempPlacement } = placement;
    const absenceClass = classnames({
      'cla-table-col-noAbsences': record.absencesCount===0,
      'cla-table-col-absences': record.absencesCount>0
    });
    const placementCategory = enums.placementCategory.filter(value => value.enumValue === placement.placementCategory)[0].name;
    const legalStatusList = legalStatuses.map((legalStatus, key) => {return ( <span key={key}>{legalStatus.name}<br /></span>);});
    return (
      <tr className={classnames('details-row', rowClassName)}>
        <td className="cla-table-col-startDate" data-title={labels.startDate}>{Formatters.date('startDate', record)}</td>
        <td className="cla-table-col-endDate" data-title={labels.endDate}>{Formatters.date('endDate', record)}</td>
        <td className="cla-table-col-legalStatus" data-title={labels.legalStatus}>{legalStatusList}</td>
        <td className="cla-table-col-placementDetail" data-title={labels.placementType}>{placement.placementType.name||''}</td>
        <td className="cla-table-col-placementType" data-title={labels.placementType}>{placementCategory||''}</td>
        <td className="cla-table-col-placedWith">
            <InfoPop title={labels.dataContentHeader}
            content={this.getCarerRegOrg(placement,labels)}
            useMarkup={false}
            align="bottom">
                  {(placement.carerSubject)? placement.carerSubject.name : ''}
            </InfoPop>
        </td>
        <td className="cla-table-col-additionalPlacements" data-title={labels.additionalPlacements}>None</td>
        <td className={absenceClass} data-title={labels.absences}>{record.absencesCount}</td>
        <td className="cla-table-col-actions">
          {( mainPlacement || tempPlacement ) && (<TableActions key={record.id} actions={this.getActions()} handleClick={handleClick} record={record} />)}
        </td>
      </tr>
    );
  }
}
