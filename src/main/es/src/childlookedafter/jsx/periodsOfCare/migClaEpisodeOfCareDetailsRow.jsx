'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formatters from '../../js/formatters';
import TableActions from '../../../table/jsx/actions/actions';
import classnames from 'classnames';

export default class MigClaEpisodeOfCareDetailsRow extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    permissions: PropTypes.object.isRequired,
    record: PropTypes.object,
    rowClassName: PropTypes.string,
    viewOnly: PropTypes.bool,
    handleClick: PropTypes.func,
    enums: PropTypes.object.isRequired
  };
  getActions() {
    const { labels } = this.props;
    return [{
      id: 'viewMigClaEpisodeOfCare',
      label: labels.view
    }];
  }
  render() {
    const { handleClick, labels, record, rowClassName, enums } = this.props;
    const { legalStatuses, placement } = record;
    const placementCategory = enums.placementCategory.filter(value => value.enumValue === placement.placementCategory)[0].name;
    const legalStatusList = legalStatuses.map((legalStatus, key) => {return ( <span key={key}>{legalStatus.name}<br /></span>);});

    return (
      <tr className={classnames('details-row', rowClassName)}>
        <td className="cla-table-col-startDate" data-title={labels.startDate}>{Formatters.date('startDate', record)}</td>
        <td className="cla-table-col-startDate" data-title={labels.endDate}>{Formatters.date('endDate', record)}</td>
        <td className="cla-table-col-legalStatus" data-title={labels.legalStatus}>{legalStatusList}</td>
        <td className="cla-table-col-placementDetail" data-title={labels.placementType}>{placement.placementType.name||''}</td>
        <td className="cla-table-col-placementType" data-title={labels.placementType}>{placementCategory||''}</td>
        <td className="cla-table-col-placedWith" data-title={labels.placedWith}>{(placement.carerSubject)? placement.carerSubject.name : ''}</td>
        <td className="cla-table-col-carerAddress" data-title={labels.carerAddress}>{placement.carerSubject.address}</td>
        <td className="cla-table-col-actions">
          {placement.isTemporaryPlacement === 'N' && (<TableActions key={record.id} actions={this.getActions()} handleClick={handleClick} record={record} />)}
        </td>
      </tr>
    );
  }
}
