'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class EpisodeOfCareAbsencesToggle extends Component {
  static propTypes = {
    hidden: PropTypes.bool,
    handleClick: PropTypes.func.isRequired
  }
  render() {
    const { hidden, handleClick } = this.props;
    return (
      <a href="#"
        className="pure-button pure-button-active"
        title={hidden ? 'show absences' : 'hide absences'}
        onClick={handleClick}>
        {hidden ? 'show' : 'hide'}
      </a>
    );
  }
}
