'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PeriodOfCareSummaryRow from './periodOfCareSummaryRow';

export default class PeriodOfCareSummaryTable extends Component {
  render() {
    const { labels, periodOfCare, viewOnly, showWelshView } = this.props;
    return (
      <div className="cla-table pure-table pure-table-responsive">
        <table className="cla-periodSummary-table pure-table-table">
          <thead className="cla-table-columns">
            <tr>
              <th>{labels.startDate}</th>
              <th>{labels.endDate}</th>
              <th>{showWelshView ? labels.careAndSupportNeed : labels.categoryOfNeed}</th>
              <th>{labels.episodes}</th>
              <th>{labels.absences}</th>
            </tr>
          </thead>
          <tbody className="cla-table-data">
            <PeriodOfCareSummaryRow
              labels={labels}
              viewOnly={viewOnly}
              periodOfCare={periodOfCare}
              key={periodOfCare.id}
            />
          </tbody>
        </table>
      </div>
    );
  }
}

PeriodOfCareSummaryTable.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  showWelshView: PropTypes.bool,
  periodOfCare: PropTypes.object.isRequired
};
