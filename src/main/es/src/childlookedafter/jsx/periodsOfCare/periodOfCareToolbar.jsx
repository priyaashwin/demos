'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Button from './button';

const PeriodOfCareToolbar=({ disabled, deletePeriodDisabled, labels, periodId, indEpTypeLegalStatus}) => {
  return (
    <div className="cla-toolbar">
      <Button 
        iconClass="fa-plus-circle" 
        label={labels.addTemporaryPlacement}
        clickParams={{action:'addTemporaryPlacement', id:periodId}} 
        disabled={disabled}
       />
      <Button
        iconClass="fa-plus-circle"
        clickParams={{action:'addEpisodeOfCare', id:periodId, indEpTypeLegalStatus: indEpTypeLegalStatus}}
        label={labels.addEpisodeOfCare}
        disabled={disabled}
      />
      <Button
        iconClass="fa-pencil-square-o"
        label={labels.noteAbsence}
        clickParams={{action:'noteAbsence', id:periodId}}
        disabled={disabled}
      />
      <Button
        iconClass="fa-check-square-o"
        label={labels.endPeriodOfCare}
        clickParams={{action:'endPeriodOfCare', id:periodId}}
        disabled={disabled}
      />
      <Button
        iconClass="fa-minus-circle"
        label={labels.deletePeriodOfCare}
        clickParams={{action:'deletePeriodOfCare', id:periodId}}
        disabled={deletePeriodDisabled}
      />
    </div>
  );
};

PeriodOfCareToolbar.propTypes= {
  disabled: PropTypes.bool,
  deletePeriodDisabled: PropTypes.bool,
  labels: PropTypes.object,
  periodId: PropTypes.string,
  indEpTypeLegalStatus: PropTypes.string,
  adoptionInformation: PropTypes.object
};

export default PeriodOfCareToolbar;
