'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PeriodOfCareSummaryRow from './migClaPeriodOfCareSummaryRow';

const PeriodOfCareSummaryTable = ({ labels, periodOfCare, viewOnly }) => {
  return (
    <div className="cla-table pure-table pure-table-responsive">
      <table className="cla-periodSummary-table pure-table-table">
        <thead className="cla-table-columns">
          <tr>
            <th>{labels.startDate}</th>
            <th>{labels.endDate}</th>
            <th>{labels.admissionReason}</th>
            <th>{labels.categoryOfNeed}</th>
            <th colSpan="2">{labels.endDateReason}</th>
          </tr>
        </thead>
        <tbody className="cla-table-data">
          <PeriodOfCareSummaryRow
            labels={labels}
            viewOnly={viewOnly}
            periodOfCare={periodOfCare}
            key={periodOfCare.id}
          />
        </tbody>
      </table>
    </div>
  );
};

PeriodOfCareSummaryTable.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  periodOfCare: PropTypes.object.isRequired
};

export default PeriodOfCareSummaryTable;
