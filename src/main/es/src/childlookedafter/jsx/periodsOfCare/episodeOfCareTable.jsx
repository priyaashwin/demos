'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import EpisodeOfCareAbsencesRow from './episodeOfCareAbsencesRow';
import EpisodeOfCareAbsencesToggle from './episodeOfCareAbsencesToggle';
import EpisodeOfCareDetailsRow from './episodeOfCareDetailsRow';

export default class EpisodeOfCareTable extends Component {
    state = {
        absencesHidden: false
    };

    getLatestMainPlacementId = (records) => {
        const mainPlacements = records
            // Remove absence records (which do not have a placement key) and retain
            // only the MAIN placements
            .filter(record => record.placement && record.placement.placementCategory === 'MAIN')
            .map(record => record.placement);

        // The episode of care records (and their associated main placement) are ordered 
        // from newest to oldest, so return the id of the first item
        return mainPlacements[0].id;
    }

    getRows = records => {
        const mainPlacementId = this.getLatestMainPlacementId(records); 

        const { labels, viewOnly, handleClick, permissions, enums } = this.props;
        const emptyRow = (
            <tr>
                <td className="no-data" colSpan="8">
                    {labels.episodeOfCareTable.noData}
                </td>
            </tr>
        );

        if (!records.length) {
            return emptyRow;
        }

        return records.map((record, i) => {
            const key = `index-${i}-id-${record.id}`;
            const rowClassName = this.getRowClass(record);
            let content;

            switch (record._type) {
                case 'EpisodeOfCareWithPlacements':
                    content = (
                        <EpisodeOfCareDetailsRow
                            key={key}
                            labels={labels.episodeOfCareTable}
                            permissions={permissions}
                            record={record}
                            handleClick={handleClick}
                            rowClassName={rowClassName}
                            viewOnly={viewOnly}
                            enums={enums}
                            isDeleteActive={record.placement.id === mainPlacementId}
                        />
                    );
                    break;
                case 'EpisodeOfCareAbsences':
                    content = (
                        <EpisodeOfCareAbsencesRow
                            key={key}
                            labels={labels.absenceFromPlacementTable}
                            permissions={permissions}
                            record={record}
                            handleClick={handleClick}
                            viewOnly={viewOnly}
                        />
                    );
                    break;
            }
            return content;
        });
    };
    getRowClass = record =>
        classnames({
            'cla-table-row-odd': record.episodeIndex % 2 === 1,
            'cla-table-row-even': record.episodeIndex % 2 === 0
        });
    toggleAbsences = e => {
        e.preventDefault();
        this.setState({
            absencesHidden: !this.state.absencesHidden
        });
    };
    render() {
        const { episodesOfCare } = this.props;
        const { absencesHidden } = this.state;
        const labels = this.props.labels.episodeOfCareTable;
        const tableClassName = classnames('cla-table-episodesOfCare', 'pure-table-table', {
            'cla-absences-hidden': absencesHidden
        });

        return (
            <div className="cla-table cla-child-table pure-table pure-table-responsive">
                <span className="cla-table-header">{labels.header}</span>
                <table className={tableClassName}>
                    <thead className="cla-table-columns">
                        <tr>
                            <th className="cla-table-col-startDate">{labels.startDate}</th>
                            <th className="cla-table-col-endDate">{labels.endDate}</th>
                            <th className="cla-table-col-legalStatus">{labels.legalStatus}</th>
                            <th className="cla-table-col-placementDetail">{labels.placementDetail}</th>
                            <th className="cla-table-col-placementType">{labels.placementType}</th>
                            <th className="cla-table-col-carer">{labels.placedWith}</th>
                            <th className="cla-table-col-additionalPlacements">{labels.additionalPlacements}</th>
                            <th className="cla-table-col-absences">
                                {labels.absences}
                                <EpisodeOfCareAbsencesToggle
                                    handleClick={this.toggleAbsences}
                                    hidden={absencesHidden}
                                />
                            </th>
                            <th className="cla-table-col-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody className="cla-table-data">{this.getRows(episodesOfCare)}</tbody>
                </table>
            </div>
        );
    }
}

EpisodeOfCareTable.propTypes = {
    labels: PropTypes.object.isRequired,
    episodesOfCare: PropTypes.array.isRequired,
    handleClick: PropTypes.func.isRequired,
    viewOnly: PropTypes.bool,
    permissions: PropTypes.object.isRequired,
    enums: PropTypes.object.isRequired
};
