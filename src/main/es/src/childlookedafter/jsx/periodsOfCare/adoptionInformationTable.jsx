'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AdoptionInformationRow from './adoptionInformationRow';

export default class AdoptionInformationTable extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    showWelshView: PropTypes.bool,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired,
    adoptionInformationRecords: PropTypes.object.isRequired
  }

  initTableData(records) {
    const { labels, codedEntries } = this.props;
    const emptyRow = (<tr><td className="no-data" colSpan="5">{labels.noData}</td></tr>);

    if(!records.length) return emptyRow;
    return records.map((record, i) => {
      const key = `index-${i}-id-${record.id}`;

      return (
        <AdoptionInformationRow
          labels={labels}
          adoptionInformation={record}
          codedEntries={codedEntries}
          key={key}
        />
      );
    });
  }
  render() {
    const { labels, adoptionInformationRecords, showWelshView} = this.props;
    
    if(showWelshView) return null;
    
    return (
      <div className="cla-table cla-child-table pure-table pure-table-responsive">
        <span className="cla-table-header">{labels.header}</span>
        <table className="cla-adoptionInformation-table pure-table-table">
          <thead className="cla-table-columns">
            <tr>
              <th>{labels.dateShouldBePlaced}</th>
              <th>{labels.dateMatched}</th>
              <th>{labels.datePlaced}</th>
              <th>{labels.legalStatusOfAdopters}</th>
            </tr>
          </thead>
          <tbody className="cla-table-data">
            {this.initTableData(adoptionInformationRecords)}
          </tbody>
        </table>
      </div>
    );
  }
}
