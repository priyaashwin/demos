'use strict';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Accordion from 'usp-accordion-panel';
import AccordionCounts from './accordionCounts';
import AccordionTitle from './accordionTitle';
import PeriodOfCareSummaryTable from './periodOfCareSummaryTable';
import MigClaPeriodOfCareSummaryTable from './migClaPeriodOfCareSummaryTable';
import EpisodeOfCareTable from './episodeOfCareTable';
import MigClaEpisodeOfCareTable from './migClaEpisodeOfCareTable';
import AdoptionInformationTable from './adoptionInformationTable';
import PeriodOfCareToolbar from './periodOfCareToolbar';
import Title from './title';
import postal from 'postal';
import mapper from '../../js/mapper';

export default class PeriodsOfCareView extends Component {
  static propTypes = {
    errors: PropTypes.object,
    labels: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    periodsOfCare: PropTypes.array,
    migratedPeriodsOfCare: PropTypes.array,
    showWelshView: PropTypes.bool,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectIdentifier: PropTypes.string,
      subjectName: PropTypes.string,
      subjectType: PropTypes.string
    }),
    permissions: PropTypes.object.isRequired,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired,
    enums: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.subscription = postal.subscribe({
      channel: 'cla',
      topic: 'buttonClick',
      callback: function (data) {
        this.handleClick(data.action, data);
      }
    }).context(this);
  }

  handleClick = (action, data, placements) => {
    const payload = {
      bubbles: true,
      cancelable: true,
      detail: {}
    };

    switch (action) {
      case ('addEpisodeOfCare'):
        payload.detail = {
          periodOfCareId: data.id,
          indEpTypeLegalStatus: data.indEpTypeLegalStatus
        };
        break;
      case ('viewEpisodeOfCare'):
        payload.detail = {
          episodeOfCareId: data.id
        };
        break;
      case ('editEpisodeOfCare'):
        payload.detail = {
          episodeOfCareId: data.id
        };
        break;
      case ('deleteEpisodeOfCare'):
        payload.detail = {
          episodeOfCareId: data.id,
          //filter main episodes only (episodeIndex === 1) if length 1 only one main epsiode exists
          //absence entry doesn't have epsiodeIndex and temporary entry has episodeIndex = 2.
          isAdmissionRecord: placements.filter(placement => placement.episodeIndex === 1).length === 1
        };
        break;
      case ('viewMigClaEpisodeOfCare'):
        payload.detail = {
          episodeOfCare: data
        };
        break;
      case ('noteAbsence'):
      //falls through
      case ('deletePeriodOfCare'):
      //falls through
      case ('endPeriodOfCare'):
        payload.detail = {
          periodOfCareId: data.id
        };
        break;
      case ('viewAbsenceFromPlacement'):
      //falls through
      case ('editAbsenceFromPlacement'):
      //falls through
      case ('deleteAbsenceFromPlacement'):
        payload.detail = {
          absenceFromPlacementId: data.id
        };
        break;
      case ('editTemporaryPlacement'):
      //falls through
      case ('viewTemporaryPlacement'):
        payload.detail = {
          episodeOfCare: data
        };
        break;
      case ('deleteTemporaryPlacement'):
        payload.detail = {
          tempPlacementId: data.placement.id
        };
        break;
      case ('addTemporaryPlacement'):
      //falls through
      case ('addAdoptionInformation'):
        payload.detail = {
          periodOfCareId: data.id
        };
        break;
      case ('viewAdoptionInformation'):
      //falls through
      case ('editAdoptionInformation'):
        payload.detail = {
          adoptionInformationId: data.id
        };
        break;
    }

    const event = new CustomEvent(action, payload);

    ReactDOM.findDOMNode(this).dispatchEvent(event);
  }
  getAccordionCounts(data) {
    const { labels } = this.props;
    return (
      <AccordionCounts
        absencesCount={data.countOfAbsenceFromPlacement}
        episodesCount={data.countOfEpisodesOfCare}
        labels={labels.accordionCounts}
      />
    );
  }
  getAccordionTitle(data, title) {
    const { labels } = this.props;
    return (
      <AccordionTitle
        endDate={data.endDate}
        startDate={data.startDate}
        title={title || labels.accordionTitle}
      />
    );
  }
  renderAccordions() {
    return (
      <div key="main_data">
        {
          (this.props.periodsOfCare || []).map((periodOfCare, i) => {
            const { labels, permissions, codedEntries, enums, showWelshView} = this.props;
            const counts = this.getAccordionCounts(periodOfCare);
            const title = this.getAccordionTitle(periodOfCare);
            const placements = mapper.getEpisodesAndAbsencesRows(periodOfCare);
            const indEpTypeLegalStatus = mapper.getIndEpTypeLegalStatus(periodOfCare);

            return (
              <Accordion key={periodOfCare.id} expanded={i === 0} menu={counts} title={title}>
                <PeriodOfCareToolbar
                  disabled={!!periodOfCare.endDate || !permissions.canAddPeriodOfCare}
                  labels={labels}
                  periodId={periodOfCare.id}
                  indEpTypeLegalStatus={indEpTypeLegalStatus}
                  deletePeriodDisabled={!permissions.canDelete}
                />
                <PeriodOfCareSummaryTable
                  labels={labels.periodOfCareSummaryTable}
                  periodOfCare={periodOfCare}
                  viewOnly={!!periodOfCare.endDate}
                  showWelshView={showWelshView}
                />
                <EpisodeOfCareTable
                  labels={labels}
                  episodesOfCare={placements}
                  viewOnly={!!periodOfCare.endDate}
                  handleClick={(action, data) => this.handleClick(action, data, placements)}
                  permissions={permissions}
                  enums={enums}
                />
                <AdoptionInformationTable
                  labels={labels.adoptionInformationTable}
                  codedEntries={codedEntries}
                  handleClick={this.handleClick}
                  permissions={permissions}
                  adoptionInformationRecords={periodOfCare.adoptionInformation.results}
                  showWelshView={showWelshView}
                />
              </Accordion>
            );
          })
        }
      </div>
    );
  }
  renderAccordionsWithMigratedData() {
    const { labels, permissions, enums } = this.props;
    return (
      <div key="migrated_data">
        {
          (this.props.migratedPeriodsOfCare || []).map((periodOfCare, i) => {
            const mappedData = mapper.getMappedMigClaData(periodOfCare);
            const title = this.getAccordionTitle(mappedData.periodOfCare, labels.accordionHistoricTitle);
            return (
              <Accordion key={mappedData.periodOfCare.id + '_migrated'} expanded={i === 0} showMenu={false} title={title}>
                <MigClaPeriodOfCareSummaryTable
                  labels={labels.periodOfCareSummaryTable}
                  periodOfCare={mappedData.periodOfCare}
                  viewOnly={true}
                />
                <MigClaEpisodeOfCareTable
                  labels={labels}
                  episodesOfCare={mappedData.placements}
                  viewOnly={true}
                  handleClick={this.handleClick}
                  permissions={permissions}
                  enums={enums}
                />
              </Accordion>
            );
          })
        }
      </div>
    );
  }
  render() {
    const { labels, periodsOfCare, migratedPeriodsOfCare, subject } = this.props;
    let periodsOfCareContent, migratedPeriodsOfCareContent;

    if (periodsOfCare && periodsOfCare.length > 0) {
      periodsOfCareContent = this.renderAccordions();
    } else {
      periodsOfCareContent = (<div key="main_data_no_results">{labels.noResultsMain}</div>);
    }

    if (migratedPeriodsOfCare && migratedPeriodsOfCare.length > 0) {
      migratedPeriodsOfCareContent = this.renderAccordionsWithMigratedData();
    }

    return (
      <div className="periods-of-care">
        <Title
          identifier={subject.subjectIdentifier}
          name={subject.subjectName}
          title={labels.title}
          subjectType={subject.subjectType}
        />
        {periodsOfCareContent}
        {migratedPeriodsOfCareContent}
      </div>
    );
  }
}
