'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../date/date';

const PeriodOfCareSummaryRow = ({labels, periodOfCare}) => {
  return (
    <tr>
      <td data-title={labels.startDate}>{formatDate(periodOfCare.startDate, true)}</td>
      <td data-title={labels.endDate}>{formatDate(periodOfCare.endDate, true)}</td>
      <td data-title={labels.admissionReason}>{periodOfCare.admissionReason ? periodOfCare.admissionReason.name : ''}</td>
      <td data-title={labels.categoryOfNeed}>{periodOfCare.categoryOfNeed ? periodOfCare.categoryOfNeed.name : ''}</td>
      <td data-title={labels.endDateReason} colSpan="2">{periodOfCare.dischargeReason}</td>
    </tr>
  );
};

PeriodOfCareSummaryRow.propTypes={
  labels: PropTypes.object.isRequired,
  periodOfCare: PropTypes.object.isRequired
};

export default PeriodOfCareSummaryRow;
