'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import EpisodeOfCareAbsencesRow from './episodeOfCareAbsencesRow';
import EpisodeOfCareDetailsRow from './migClaEpisodeOfCareDetailsRow';

export default class EpisodeOfCareTable extends Component {
  static propTypes={
    labels: PropTypes.object.isRequired,
    episodesOfCare: PropTypes.array.isRequired,
    handleClick: PropTypes.func.isRequired,
    viewOnly: PropTypes.bool,
    permissions: PropTypes.object.isRequired,
    enums: PropTypes.object.isRequired
  };
  getRows = (records) => {
    const { labels, viewOnly, handleClick, permissions, enums } = this.props;
    const emptyRow = (<tr><td className="no-data" colSpan="8">{labels.episodeOfCareTable.noData}</td></tr>);

    if(!records.length) return emptyRow;
    return records.map((record, i) => {
      const key = `index-${i}-id-${record.id}`;
      const rowClassName = this.getRowClass(record);
      let content;
      switch (record._type) {
        case 'EpisodeOfCareWithPlacements':
          content = (
            <EpisodeOfCareDetailsRow
              key={key}
              labels={labels.episodeOfCareTable}
              permissions={permissions}
              record={record}
              handleClick={handleClick}
              rowClassName={rowClassName}
              viewOnly={viewOnly}
              enums={enums}
            />
          );
          break;
        case 'EpisodeOfCareAbsences':
          content = (
            <EpisodeOfCareAbsencesRow
              key={key}
              labels={labels.absenceFromPlacementTable}
              permissions={permissions}
              record={record}
              handleClick={handleClick}
              viewOnly={viewOnly}
            />);
          break;
      }
      return content;
    });
  };
  getRowClass = record => classnames({
    'cla-table-row-odd': record.episodeIndex % 2 === 1,
    'cla-table-row-even': record.episodeIndex % 2 === 0
  });
  render() {
    const { episodesOfCare } = this.props;
    const labels = this.props.labels.episodeOfCareTable;
    const tableClassName = classnames('cla-table-episodesOfCare', 'pure-table-table');

    return (
      <div className="cla-table cla-child-table pure-table pure-table-responsive">
        <span className="cla-table-header">{labels.header}</span>
        <table className={tableClassName}>
          <thead className="cla-table-columns">
            <tr>
              <th className="cla-table-col-startDate">{labels.startDate}</th>
              <th className="cla-table-col-endDate">{labels.endDate}</th>
              <th className="cla-table-col-legalStatus">{labels.legalStatus}</th>
              <th className="cla-table-col-placementType">{labels.placementType}</th>
              <th className="cla-table-col-category">{labels.category}</th>
              <th className="cla-table-col-carer">{labels.placedWith}</th>
              <th className="cla-table-col-carerAddress">{labels.carerAddress}</th>
              <th className="cla-table-col-actions" />
            </tr>
          </thead>
          <tbody className="cla-table-data">{this.getRows(episodesOfCare)}</tbody>
        </table>
      </div>
    );
  }
}
