'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formatters from '../../js/formatters';
import TableActions from '../../../table/jsx/actions/actions';

import EpisodeOfCareAbsencesDetails from './episodeOfCareAbsencesDetails';

export default class EpisodeOfCareAbsencesRow extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    permissions: PropTypes.object.isRequired,
    record: PropTypes.object,
    handleClick: PropTypes.func,
    viewOnly: PropTypes.bool
  }
  getActions = (absenceEndDate) => {
    const { labels, permissions, viewOnly } = this.props;
    const canEdit = (!absenceEndDate) && permissions.canUpdateAbsenceFromPlacement;
    if(viewOnly) return [];
    return [{
      id: 'editAbsenceFromPlacement',
      label: labels.edit,
      disabled: !canEdit
    },{
      id: 'deleteAbsenceFromPlacement',
      label: labels.archive,
      disabled: !permissions.canArchiveAbsenceFromPlacement
    }];
  }
  getRows = () => {
    const { handleClick, labels, record } = this.props;
    return record.absences.map((absence, i) => (
      <tr key={`index-${i}-id-${absence.id}`}>
        <td className="cla-table-col-startDate">{Formatters.dateTime('startDate', absence)}</td>
        <td className="cla-table-col-endDate">{Formatters.dateTime('endDate', absence)}</td>
        <td className="cla-table-col-details">
          <EpisodeOfCareAbsencesDetails labels={labels} absence={absence} />
        </td>
        <td className="cla-table-col-actions">
          <TableActions
            key={record.id}
            actions={this.getActions(absence.endDate)}
            handleClick={handleClick}
            record={absence}
          />
        </td>
      </tr>
    ));
  }
  render() {
    return (
      <tr className="absences-row">
        <td colSpan="9">
          <div className="absences-row-content">
            <table className="cla-table-absencesInEpisode pure-table-table">
              <tbody className="cla-table-data">{this.getRows()}</tbody>
            </table>
          </div>
        </td>
      </tr>
    );
  }
}
