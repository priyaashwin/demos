'use strict';

import ReactDOM from 'react-dom';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import endpoint from '../js/cfg';
import ErrorMessage from '../../error/error';
import Loading from '../../loading/loading';
import ModelList from '../../model/modelList';
import PaginatedResultsList from '../../results/paginatedResultsList';
import PeriodsOfCareView from './periodsOfCare/view';

export default class ChildLookedAfterView extends Component {
  static propTypes={
    labels: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired,
    migClaPeriodUrl: PropTypes.string.isRequired,
    showWelshView: PropTypes.bool,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectIdentifier: PropTypes.string,
      subjectName: PropTypes.string,
      subjectType: PropTypes.string
    }),
    permissions : PropTypes.object.isRequired,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired,
    enums: PropTypes.object.isRequired
  };

  state={
    data: null,
    migratedData: null,
    errors: null,
    loading: true
  };
  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({errors:null});
  };
  componentDidUpdate(prevProps) {
    if (!(prevProps.url === this.props.url && prevProps.migClaPeriodUrl === this.props.migClaPeriodUrl && prevProps.subject === this.props.subject)) {
      this.loadData();
    }
  }
  componentDidMount(){
    this.loadData();
  }
  loadData(){
    Promise.all([this.loadPeriodOfCare(),this.loadMigratedPeriodOfCare()]).then(data=> {
      this.setState({
        data: data[0].results,
        migratedData: data[1].results,
        loading: false,
        errors: null
      });

      const event = new CustomEvent('periodsOfCareLoaded', {
        bubbles:true, 
        cancelable: true,
        detail:{
          latestPeriodOfCare: data[0].results[0]
        }
      });

      ReactDOM.findDOMNode(this).dispatchEvent(event);

    }).catch(reject=>this.setState({
      errors: reject,
      loading: false,
      data: null
    }));
  }
  loadPeriodOfCare(){
    return this.getListLoad(this.props.url, endpoint.periodsOfCareEndpoint);
  }
  loadMigratedPeriodOfCare(){
    return this.getListLoad(this.props.migClaPeriodUrl, endpoint.migratedPeriodsOfCareEndpoint);
  }
  getListLoad(url, endPointConfig){
    return new PaginatedResultsList().load({
      ...endPointConfig,
      url: new ModelList(url).getURL({
        subjectId: this.props.subject.subjectId,
        s: {'startDate': 'desc'}
      })
    }, -1, 1);
  }
  renderColumns() {
    const {labels, subject, permissions, codedEntries, enums, showWelshView} = this.props;
    const {data, migratedData, errors, loading} = this.state;
    return (
      <div className="child-looked-after-content">
        <PeriodsOfCareView
          errors={errors}
          labels={labels}
          loading={loading}
          periodsOfCare={data}
          migratedPeriodsOfCare={migratedData}
          subject={subject}
          permissions={permissions}
          codedEntries={codedEntries}
          enums={enums}
          showWelshView={showWelshView}
        />
      </div>
    );
  }
  render(){
    const {errors, loading} = this.state;
    let content;

    if(loading) {
      content = (<Loading />);
    } else if(errors) {
      content = (<ErrorMessage errors={errors} onClose={this.clearErrors} />);
    } else {
      content = this.renderColumns();
    }

    return (<div className="child-looked-after">{content}</div>);
  }
}
