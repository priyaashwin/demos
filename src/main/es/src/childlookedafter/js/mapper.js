'use strict';
import 'core-js/features/array/flat-map';

const PLACEMENT_CATEGORY_MAIN = 'MAIN';
const PLACEMENT_CATEGORY_TEMPORARY = 'TEMPORARY';

const translatePeriodOfCareData = periodOfCare => ({
    ...periodOfCare,
    categoryOfNeed: {
        name: periodOfCare.needCategoryDesc
    },
    admissionReason: {
        name: periodOfCare.admissionReasonDesc
    },
    episodes: periodOfCare.episodes.map(episode => ({
        ...episode,
        previousPermanence: {
            lookedAfter: periodOfCare.previouslyLookedAfter || false,
            date: periodOfCare.permanencePlacementDate ? periodOfCare.permanencePlacementDate.calculatedDate : null,
            localAuthority: periodOfCare.previousPermanenceLocalAuthority
        }
    })),
    absencesFromCare: periodOfCare.absences.map(absence => ({
        ...absence,
        startDate: absence.startDateTime,
        endDate: absence.endDateTime
    }))
});

const getEpisodeOfCareWithAbsences = (episodeOfCare, absences) => ({
    id: episodeOfCare.id,
    absences: absences,
    _type: 'EpisodeOfCareAbsences'
});

const getEpisodeOfCarePlacement = (id, episodeOfCare, placement, absences, index) => ({
    _type: 'EpisodeOfCareWithPlacements',
    objectVersion: episodeOfCare.objectVersion,
    periodOfCareId: id,
    id: episodeOfCare.id,
    absencesCount: absences.length,
    episodeIndex: index,
    endDate: placement.endDate,
    legalStatuses: episodeOfCare.legalStatuses,
    placement: {
        ...placement,
        mainPlacement: placement.mainPlacement || placement.placementCategory === PLACEMENT_CATEGORY_MAIN,
        tempPlacement: placement.placementCategory === PLACEMENT_CATEGORY_TEMPORARY
    },
    startDate: placement.startDate
});

const getMigClaEpisodeOfCareWithAbsences = (episodeOfCare, absences) => ({
    id: episodeOfCare.id,
    absences: absences.map(absence => ({
        ...absence,
        absenceNote: {
            content: absence.notes
        },
        absenceType: {
            code: absence.absenceType,
            name: absence.absenceTypeDesc
        }
    })),
    _type: 'EpisodeOfCareAbsences'
});

const getApril2000Desc = attr => {
    if (!attr) return null;
    return attr.postApril2000Desc && attr.postApril2000Desc !== null ? attr.postApril2000Desc : attr.preApril2000Desc || null;
};

const getDohApril2000Desc = attr => {
    if (!attr) return null;
    return attr.dohPostApril2000Desc && attr.dohPostApril2000Desc !== null ? attr.dohPostApril2000Desc : attr.dohPreApril2000Desc || getApril2000Desc(attr);
};

const getMigClaEpisodeOfCarePlacement = (episodeOfCare, placement, index) => {
    return ({
        id: episodeOfCare.id,
        episodeIndex: index,
        endDate: placement.isTemporaryPlacement === 'N' ? episodeOfCare.episodeEndDate : placement.endDate,
        startDate: placement.isTemporaryPlacement === 'N' ? episodeOfCare.episodeStartDate : placement.startDate,
        startReason: getApril2000Desc(episodeOfCare.startReason),
        legalStatuses: episodeOfCare.legalStatuses ? episodeOfCare.legalStatuses.map(legalStatus =>({name: getApril2000Desc(legalStatus)})) : [],
        previousPermanence: episodeOfCare.previousPermanence,
        placement: {
            ...placement,
            startReason: getApril2000Desc(placement.startReason),
            carerSubject: {
                name: placement.carerName,
                address: placement.carerAddress
            },
            placementType: {
                name: getDohApril2000Desc(placement.placementType)
            },
            placementCategory: placement.isTemporaryPlacement === 'N' ? PLACEMENT_CATEGORY_MAIN : PLACEMENT_CATEGORY_TEMPORARY
        },
        _type: 'EpisodeOfCareWithPlacements'
    });
};

const getAbsencesInPlacementEpisode = (placement, allAbsences) => {
    return allAbsences.filter((absence) => {
        if (!placement.endDate && absence.startDate >= placement.startDate) {
            return true;
        } else if (placement.endDate &&
            absence.startDate >= placement.startDate &&
            absence.startDate < placement.endDate) {
            return true;
        } else if (placement.endDate &&
            absence.endDate > placement.startDate &&
            absence.endDate < placement.endDate) {
            return true;
        } else if (placement.endDate &&
            absence.startDate < placement.startDate &&
            absence.endDate > placement.endDate) {
            return true;
        }
    });
};

const getSortedPlacementByStartDate = (placements = []) => placements.sort((o1, o2) => o1.startDate - o2.startDate);

const stripAbsencesFromPlacements = placements => placements.filter(placement => placement._type === 'EpisodeOfCareWithPlacements');

// List results in descending order by start date
const sortByStartDate = (o1, o2) => new Date(o2.startDate) - new Date(o1.startDate);

const appendPlacementAndAbsences = (placement, i, rows, id, absencesFromCare, episodeOfCare) => {
    //ensure we do not present absences underneath temporary placements
    const absences = (placement.placementCategory === 'MAIN') ? getAbsencesInPlacementEpisode(placement, absencesFromCare.results) : [];
    absences.sort(sortByStartDate);

    rows.push(getEpisodeOfCareWithAbsences(placement, absences));
    
    rows.push(getEpisodeOfCarePlacement(id, episodeOfCare, placement, absences, i + 1));
};

// Map the placements and absences associated with each episode
// to a single array
const getEpisodesAndAbsencesRows = ({ id, absencesFromCare, episodesOfCare }) => {
    return episodesOfCare.results
        .sort(sortByStartDate)
        .flatMap((episodeOfCare) => {
            const rows = [];
            episodeOfCare.placements
                .sort(sortByStartDate)
                .forEach((placement, i) => {
                    appendPlacementAndAbsences(placement, i, rows, id, absencesFromCare, episodeOfCare);
                });
            return rows;
        });
};

const getMappedMigClaData = (periodOfCare) => {
    const translatedPeriodOfCare = translatePeriodOfCareData(periodOfCare);
    const placementsWithEpisodesAndAbsences = constructPlacementsWithEpisodesAndAbsences(translatedPeriodOfCare);
    translatedPeriodOfCare.dischargeReason = getDischargeReasonFromLastEpisode(
        stripAbsencesFromPlacements(placementsWithEpisodesAndAbsences)
    );
    return {
        placements: placementsWithEpisodesAndAbsences,
        periodOfCare: translatedPeriodOfCare
    };
};

const constructPlacementsWithEpisodesAndAbsences = (periodOfCare) => {
    const { absencesFromCare, episodes } = periodOfCare;
    return episodes.reduce((rows, episodes) => {
        const placements = getSortedPlacementByStartDate(episodes.placements);
        placements.forEach((placement) => {
            const absences = getAbsencesInPlacementEpisode(placement, absencesFromCare);

            rows.push(getMigClaEpisodeOfCarePlacement(episodes, placement, rows.length));

            if (absences.length > 0) {
                rows.push(getMigClaEpisodeOfCareWithAbsences(placement, absences));
            }
        });
        return rows;
    }, []);
};

const getIndEpTypeLegalStatus = ({ episodesOfCare }) => {
    const episodeOfCare = episodesOfCare.results.find(({ legalStatuses }) =>
        legalStatuses.find(legalStatus =>
            legalStatus.code === 'IND_EP' || legalStatus.code === 'NON_IND_EP'
        ));
    return episodeOfCare ? episodeOfCare.legalStatuses[0].code : null;
};

const getDischargeReasonFromLastEpisode = (placements) => {
    return placements.length > 0 ? getDohApril2000Desc(placements[placements.length - 1].placement.endReason) : null;
};

export default {
    getMappedMigClaData,
    getEpisodesAndAbsencesRows,
    getIndEpTypeLegalStatus
};