'use strict';

class Endpoint{
  constructor() {

    this.periodsOfCareEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.childlookedafter.PeriodOfCareWithEpisodesAndAbsencesAndAdoptionInformation+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.migratedPeriodsOfCareEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.clamigration.migclaperiodfulldetails+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.subjectPictureEndpoint={
      method: 'get',
      contentType: 'image/*',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'image/png,image/*;q=0.8,*/*;q=0.5'
      },
      responseType: 'blob',
      responseStatus:200
    };
    
  }
}

export default new Endpoint();
