'use strict';
import { formatDate, formatDateTime } from '../../date/date';

const date = (key, data) => (formatDate(data[key], true));

const dateTime = (key, data) => (formatDateTime(data[key], true));


const absenceNote = (key, data) => (data[key] ? data[key].content : '');

const absenceType = (key, data) => (data[key] ? data[key].name : '');

const absenceDetails = (record, labels) => {
  return `${labels.absenceType} ${record.absenceType ? record.absenceType.name:''}`;
};

export default {
  date,
  dateTime,
  absenceDetails,
  absenceNote,
  absenceType
};
