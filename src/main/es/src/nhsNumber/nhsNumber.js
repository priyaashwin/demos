'use strict';

const formatNHSNumber=function(_nhsNumber){
  let nhsNumber=_nhsNumber;

  if(typeof nhsNumber === 'string'){
    //strip spaces
    nhsNumber=nhsNumber.replace(/\s/g,'');
  }
  else if(typeof nhsNumber === 'number'){
    nhsNumber=nhsNumber.toString();
  }
  // Check that the value supplied consists of 10 digits only
  if (nhsNumber && nhsNumber.length === 10 && /^[0-9]+$/.test(nhsNumber)) {
    return [nhsNumber.substring(0, 3),nhsNumber.substring(3, 6),nhsNumber.substring(6, 10)].join('-');
  }
  return '';
};

const isValidateNHSNumber=function(nhsNumber){
  let valid=false;
  
  if(typeof nhsNumber==='string'&&nhsNumber.length==10){
    let total=0;
    for(let i=0; i<9; i++){
      const n=nhsNumber.substr(i, 1);
      total+=(n*(10-i));
    }

    let checkDigit=(11-(total%11));
    if(checkDigit===11){
      checkDigit=0;
    }

    valid=checkDigit===Number(nhsNumber.substr(9,1));
  }
  return valid;
};

export {formatNHSNumber, isValidateNHSNumber};
