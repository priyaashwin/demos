'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Picture from '../picture/picture';

const Icon = memo(({ iconClass, pictureEndpoint, pictureClassName, ...other }) => (
  <Picture {...other} className={classnames('fa-stack', 'subject-icon', pictureClassName)} endpoint={pictureEndpoint} emptyPicture={(
    <span className="fa-stack subject-icon">
      <i className="fa fa-circle fa-stack-1x" />
      <i className={classnames(iconClass, 'fa-inverse', 'fa-stack-1x')} />
    </span>
  )} />
));

Icon.propTypes = {
  iconClass: PropTypes.string.isRequired,
  pictureEndpoint: PropTypes.object,
  /**
   * The class name to apply when the picture is loaded
   */
  pictureClassName: PropTypes.string
};

export default Icon;
