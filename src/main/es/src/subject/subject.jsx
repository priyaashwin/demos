'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Subject = ({ subjectType, subject, name, identifier }) => {
  let subjectName = name;
  let subjectIdentifier = identifier;
  if (subject) {
    switch (subjectType) {
      case 'person':
        subjectName = subject.name || subjectName;
        subjectIdentifier = subject.personIdentifier || subject.identifier || subjectIdentifier;
        break;
      case 'group':
        subjectName = subject.name || subjectName;
        subjectIdentifier = subject.groupIdentifier || subject.identifier || subjectIdentifier;
        break;
      case 'organisation':
        subjectName = subject.name || subjectName;
        subjectIdentifier = subject.organisationIdentifier || subject.identifier || subjectIdentifier;
        break;
      default:
    }
  }

  return (
    <span>
      <span className="subject-name">{subjectName}</span>
      <span>{' '}</span>
      <span className="subject-identifier">{subjectIdentifier ? `(${subjectIdentifier})` : ''}</span>
    </span>
  );
};

Subject.propTypes = {
  name: PropTypes.string.isRequired,
  identifier: PropTypes.string,
  subject: PropTypes.object,
  subjectType: PropTypes.oneOf(['person', 'group', 'organisation']).isRequired
};

export default Subject;
