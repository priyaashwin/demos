'use strict';
import ResultsList from './resultsList';
import { subs } from '../subs/subs';

export default class PaginatedList extends ResultsList {
    parseResponse(response) {
        let data = [];
        let pageSize = 0;
        let page = 0;
        let totalSize = 0;

        //update properties from response
        if (response.pageSize !== undefined) {
            pageSize = response.pageSize;
        }

        if (response.totalSize !== undefined) {
            totalSize = response.totalSize;
        }

        if (response.pageNumber !== undefined) {
            page = response.pageNumber;
        }

        if (response.results) {
            data = response.results || [];
        }

        return ({
            results: data,
            pageSize: pageSize,
            page: page,
            totalSize: totalSize
        });
    }
    populateEndpoint(endpoint, pageSize, page) {
        //don't overwrite restEndpoint - treat as immutable
        return {
            ...endpoint,
            //merge in page size and page number to the url
            url: subs(endpoint.url, {
                pageSize: pageSize,
                page: page
            })
        };
    }

    /**
     * The Promise to load the data
     */
    load(endpoint, pageSize, page, cancelToken) {
        return this._performLoad(this.populateEndpoint(endpoint, pageSize, page), cancelToken);
    }
}