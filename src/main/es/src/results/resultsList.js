'use strict';
import { makeRequest } from 'usp-xhr';

export default class ResultsList {
    parseResponse(response) {
        return response || [];
    }
    populateEndpoint(endpoint) {
        //don't overwrite restEndpoint - treat as immutable
        return {
            ...endpoint,
        };
    }
    /**
     * The Promise to load the data
     */
    load(endpoint, cancelToken) {
        return this._performLoad(this.populateEndpoint(endpoint), cancelToken);
    }

    /**
     * Private method that performs the actual load
     * and returns the resulting Promise
     */
    _performLoad(endpoint, cancelToken) {
        return new Promise((resolve, reject) => {
            makeRequest(endpoint, (success, failure) => {
                if (failure) {
                    //reject with the failure
                    reject(failure);
                } else {
                    //resolve with the parsed data
                    resolve(this.parseResponse(success));
                }
            }, cancelToken);
        });
    }
}