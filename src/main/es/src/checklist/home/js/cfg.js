'use strict';

class Endpoint{
  constructor() {
    this.checklistInstanceListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.checklist.taskinstancecard+json;requestedAccess=READ_SUMMARY;'
      },
      responseType: 'json',
      responseStatus:200
    };
  }
}

export default new Endpoint();
