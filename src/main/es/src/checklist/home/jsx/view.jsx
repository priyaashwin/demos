'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import ChecklistCardList from './checklistCardList';
import UpdateChecklistPriorityDialog from '../../updatePriority/jsx/view';

export default class ChecklistCardView extends React.Component {
    static propTypes = {
        dialogProps: PropTypes.shape({
            labels: PropTypes.object.isRequired,
            enums: PropTypes.object.isRequired,
            dialogConfig: PropTypes.shape({
                header: PropTypes.object.isRequired,
                narrative: PropTypes.shape({
                    summary: PropTypes.string.isRequired,
                    description: PropTypes.string
                }).isRequired,
                successMessage: PropTypes.string
            }).isRequired,
            urls: PropTypes.shape({
                getChecklistUrl: PropTypes.string.isRequired,
                updateChecklistPriorityUrl: PropTypes.string.isRequired
            }).isRequired,
            permissions: PropTypes.object.isRequired
        }),
        viewProps: PropTypes.shape({
            title: PropTypes.string,
            url: PropTypes.string.isRequired,
            personSummaryURL: PropTypes.string.isRequired,
            checklistURL: PropTypes.string.isRequired,
            labels: PropTypes.object.isRequired,
            permissions: PropTypes.object.isRequired,
            invalid: PropTypes.bool
        })
    };

    //show prioritise dialog on prioritise menu item click
    onPrioritise = (record) => {
        this.updateChecklistPriorityDialog.showDialog(record.id);
    }

    //handle successful priority save
    handlePrioritiseSave = () => {
        const dialog = this.updateChecklistPriorityDialog.dialog;

        if (dialog) {
            this.updateChecklistPriorityDialog.fireSuccessMessage();
            this.updateChecklistPriorityDialog.hideDialog();

            const event = new CustomEvent('checklistInstancePrioritiseView:saved', {
                bubbles: true,
                cancelable: true
            });
            ReactDOM.findDOMNode(this).dispatchEvent(event);
        }
    }

    updateChecklistPriorityDialogRef=(ref)=>this.updateChecklistPriorityDialog=ref;

    handleTaskClick = (record) => {
      const payload = {
          bubbles: true,
          cancelable: true,
          detail: { record }
      };

      const event = new CustomEvent('loadChecklistPage', payload);
      ReactDOM.findDOMNode(this).dispatchEvent(event);
    }

    render() {
        return (
            <>
                <ChecklistCardList
                    {...this.props.viewProps}
                    onPrioritise={this.onPrioritise}
                    handleTaskClick={this.handleTaskClick}
                />
                <UpdateChecklistPriorityDialog
                    {...this.props.dialogProps}
                    ref={this.updateChecklistPriorityDialogRef}
                    handlePrioritiseSave={this.handlePrioritiseSave}
                />
            </>
        );
    }
}