'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import {CardTitle} from 'usp-card';
import endpoint from '../js/cfg';
import RemotePaginatedCardList from '../../../card/remotePaginatedCardList';
import InlineMenu from 'usp-inline-menu';
import subs from 'subs';
import Subject from '../../../subject/subject';
import Link from '../../../link/link';
import Checklist from './checklist';
import NoAccess from './noAccess';
import classnames from 'classnames';
import {formatDateValue} from 'usp-date'; 

/**
 * This will eventually be implemented in Model to allow us to use the same call as usp-model
 **/
const hasAccessLevel=function(record, requestedAccessLevel){
  let level;

   switch(requestedAccessLevel){
     case 'WRITE':
       /* falls through */
     case 'write':
       level='write';
       break;
     case 'ADMIN':
       /* falls through */
     case 'admin':
       level='admin';
       break;
     case 'READ_DETAIL':
       /* falls through */
     case 'readDetail':
       level='readDetail';
       break;
     case 'READ_SUMMARY':
       /* falls through */
     case 'readSummary':
       level='readSummary';
       break;
     case 'NONE':
       /* falls through */
     case 'none':
       level='none';
       break;
   }
   return !!(record['_securityMetaData']||{})[level];
};

const DEFAULT_PAGE=1;
const DEFAULT_PAGE_SIZE=25;

const isSecured=function(record){
  return !!(record['_securityMetaData']||{})['secured'];
};

export default class ChecklistCardList extends React.Component {
  static propTypes = {
      title: PropTypes.string,
      url: PropTypes.string.isRequired,
      personSummaryURL: PropTypes.string.isRequired,
      checklistURL: PropTypes.string.isRequired,
      labels: PropTypes.object.isRequired,
      permissions: PropTypes.object.isRequired,
      //setting the list to invalid will suppress the list
      invalid: PropTypes.bool,
      onPrioritise: PropTypes.func.isRequired,
      handleTaskClick: PropTypes.func.isRequired
  };

  getCardTitle=(record)=>{
    const { labels, personSummaryURL } = this.props;
    if(isSecured(record)===true){
      return (<h4>{labels.accessDeniedTitle}</h4>);
    }

    const subject = record.checklistInstance.subject || {};
    const dueDate = record.dateDue; //task dueDate
    const url = subs(personSummaryURL, {personId: subject.id});

    const subjectView = (
      <div className="pure-g-r">
         <div className="pure-u-2-3">
            <Link url={url}>
               <Subject name={subject.name} identifier={subject.identifier} subjectType={subject.subjectType.toLowerCase()}/>
            </Link>
         </div>
         <div className="pure-u-1-3">
            <div className="name">{`${labels.dateDue}: `}</div>
            <div className="value">{formatDateValue(dueDate, 'TIME')}</div>
         </div>
      </div>
   );

    return (
       <CardTitle>
        <h4>
          {subjectView}
        </h4>
       </CardTitle>
    );
  };
  
  getCardContent=(record)=>{
    const { labels, checklistURL, handleTaskClick } = this.props;

    if(isSecured(record)===true){
      return (<NoAccess labels={labels}/>);
    }

    return (
      <Checklist
        key={record.checklistInstance.id}
        record={record}
        labels={labels}
        checklistURL={checklistURL}
        handleTaskClick={handleTaskClick}/>
    );

  };
  getListHeader=(totalResults)=>{
      const {title} = this.props;

      return (
          <h3 className="list-header">{subs(title, {totalResults: totalResults})}</h3>
      );
  };
  getCardClassName=(record)=>{
      const checklist = record.checklistInstance || {};
      let className;
      switch (checklist.status) {
          case 'ON_TIME':
              className = 'checklist-on-time';
              break;
          case 'LAST_REMINDER':
              className = 'checklist-last-reminder';
              break;
          case 'OVERDUE':
              className = 'checklist-late';
              break;
          case 'PAUSED':
              className = 'checklist-paused';
              break;
          case 'IN_ERROR':
              className = 'checklist-error';
              break;
      }

      if(checklist.priority) {
        className = classnames(className, ('checklist-' + checklist.priority.toLowerCase() + '-priority'));
      }

      return className;
  };
  getCardMenu=(record)=>{
    const checklist = record.checklistInstance;
    //no menu items for secured record
    if(isSecured(record)===true){
      return false;
    }
    const {permissions, labels} = this.props;
    return (
        <InlineMenu key={checklist.id} options={this.getActions(permissions, labels, checklist)} menuIcon={(<i className="fa fa-ellipsis-v" aria-hidden="true"/>)} vertical={true} onMenuItemClick={this.handleMenuItemClick.bind(null, checklist)}/>
    );
  };
  getActions(permissions, labels, record) {
      const status = record.status || '';
      const priority = record.priority || '';
      const canPause = record.canPause || false;
      const calculationMode = record.calculationMode || '';

      const hasWriteAccessLevel=hasAccessLevel(record,'WRITE')===true;
      return [
          {
              id: 'prioritise',
              title: labels.prioritiseChecklistTitle,
              label: labels.prioritiseChecklist,
              visible: (permissions.canPrioritise === true && ((priority? (permissions.canEscalate || permissions.canDeescalate) : true))) && hasWriteAccessLevel,
          }, {
              id: 'reassign',
              title: labels.reassignChecklistTitle,
              label: labels.reassignChecklist,
              visible: permissions.canReassign === true && hasWriteAccessLevel,
              disabled: status === 'IN_ERROR'
          }, {
              id: 'pause',
              title: labels.pauseChecklistTitle,
              label: labels.pauseChecklist,
              disabled: status === 'PAUSED' || status === 'IN_ERROR',
              visible: canPause === true && permissions.canPause === true && hasWriteAccessLevel
          }, {
              id: 'resume',
              title: labels.resumeChecklistTitle,
              label: labels.resumeChecklist,
              disabled: status !== 'PAUSED',
              visible: canPause === true && permissions.canPause===true && hasWriteAccessLevel
          }, {
              id: 'reset',
              title: labels.resetChecklistEndDateTitle,
              label: labels.resetChecklistEndDate,
              disabled: status === 'PAUSED' || status === 'IN_ERROR',
              visible: permissions.canResetEndDate===true && calculationMode === 'TO_END' && hasWriteAccessLevel
          }, {
              id: 'del',
              title: labels.deleteChecklistTitle,
              label: labels.deleteChecklist,
              visible: permissions.canDelete === true && status !== 'ARCHIVED' && hasWriteAccessLevel
          }, {
              id: 'rem',
              title: labels.removeChecklistTitle,
              label: labels.removeChecklist,
              visible: permissions.canRemove === true && hasWriteAccessLevel
          }
      ];

  }
  handleMenuItemClick=(record, item)=>{

      const payload = {
          bubbles: true,
          cancelable: true,
          detail: record
      };

      let eventType;

      switch (item) {
          case 'prioritise':
              this.props.onPrioritise(record);
              break;
          case 'reassign':
              eventType = 'reassignChecklist';
              break;
          case 'pause':
              eventType = 'pauseChecklist';
              break;
          case 'resume':
              eventType = 'resumeChecklist';
              break;
          case 'reset':
              eventType = 'resetChecklistEndDate';
              break;
          case 'start':
              eventType = 'startChecklist';
              break;
          case 'del':
              eventType = 'deleteChecklist';
              break;
          case 'rem':
              eventType = 'removeChecklist';
              break;
      }

      if (eventType) {
          //create save - custom event
          const event = new CustomEvent(eventType, payload);

          ReactDOM.findDOMNode(this).dispatchEvent(event);
      }
  };
  getRestEndpoint(url) {
    return {
        ...endpoint.checklistInstanceListEndpoint,
        url: url
    };
  }
  cardListRef=(elem)=>{
    this.cardList = elem;
  };
  render() {
      const {labels={}, invalid, url}=this.props;

      if(invalid){
        return (<h3>{labels.invalidList||'Invalid'}</h3>);
      }

      return (
        <RemotePaginatedCardList
          ref={this.cardListRef}
          noResultsMessage="No tasks found."
          restEndpoint={this.getRestEndpoint(url)}
          className="card-view"
          getCardTitle={this.getCardTitle}
          getCardContent={this.getCardContent}
          getListHeader={this.getListHeader}
          getCardClassName={this.getCardClassName}
          getCardMenu={this.getCardMenu}
          page={DEFAULT_PAGE}
          pageSize={DEFAULT_PAGE_SIZE}
          />
      );
  }
}
