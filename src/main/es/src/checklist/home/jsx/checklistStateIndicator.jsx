'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../../../infopop/infopop';

const ChecklistStateIndicator=({status, labels})=>{
    let indicator;

  switch(status){
    case 'IN_ERROR':
      indicator=(
        <InfoPop className="checklist-state" title={labels.inErrorTitle}  content="" useMarkup={false}>
          <i className="fa fa-stop-circle checklist-indicator checklist-error"/>
        </InfoPop>
      );
      break;
    case 'PAUSED':
      indicator=(
        <InfoPop className="checklist-state" title={labels.pausedTitle}  content={labels.pausedContent} useMarkup={false}>
          <i className="fa fa-pause checklist-indicator checklist-paused"/>
        </InfoPop>
      );
      break;
    default:
      indicator=(<span/>);
  }
  return indicator;
};

ChecklistStateIndicator.propTypes={
  status:PropTypes.string,
  labels: PropTypes.object.isRequired
};

export default ChecklistStateIndicator;
