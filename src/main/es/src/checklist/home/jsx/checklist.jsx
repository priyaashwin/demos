'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Badge from 'react-bootstrap/lib/Badge';
import ChecklistStateIndicator from './checklistStateIndicator';
import Link from '../../../link/link';
import subs from 'subs';
import {formatDateValue} from 'usp-date'; 

export default class Checklist extends PureComponent{
  static propTypes={
    labels: PropTypes.object.isRequired,
    record: PropTypes.object,
    checklistURL: PropTypes.string.isRequired,
    handleTaskClick: PropTypes.func.isRequired
  };

  getOwnership = ownershipDetails => {
    let ownership = '';

    if (ownershipDetails) {
      ownership =  ownershipDetails.ownerDetails.name;
      const owningTeam = ownershipDetails.owningTeamDetails;
      if (owningTeam && owningTeam.name !== ownership) {
        ownership = `${ownership} / ${owningTeam.name}`;
      } else if (ownershipDetails.ownerDetails.type === 'PERSON') {
        ownership = `${ownership} / No owning team`;
      }
    }
    return ownership;
  }

  getTaskElement = () => {
    const { record, labels } = this.props;

    return (
      <div className="pure-u-1">
          <Badge className="type-indicator task">{labels.indicator.task}</Badge>
          <a href="#none" onClick={this.handleClick}>{record.taskName}</a>
      </div>
    );
  }

  handleClick = (e) => {
    e.preventDefault();
    this.props.handleTaskClick(this.props.record);
  }

  getChecklistElement = () => {
    const {record, checklistURL, labels} = this.props;
    const { checklistInstance } = record;
    const url = subs(checklistURL, {
      personId: checklistInstance.subject.id,
      checklistDefinitionId: checklistInstance.checklistDefinitionId
    });

    return(
      <div className="pure-u-22-24">
          <Badge className="type-indicator checklist">{labels.indicator.checklist}</Badge>
          <Link className="checklist-title" url={url}>{checklistInstance.checklistDefinitionName}</Link>
      </div>
    );
  }

  getChecklistStateIndicatorElement = () => {
    const { record, labels} = this.props;
    const { checklistInstance } = record;

    return (
      <div className="pure-u-2-24 checklist-indicator-container">
          <div className="checklist-indicator">
              <ChecklistStateIndicator labels={labels.checklist.state} status={checklistInstance.status} />
          </div>
      </div>
    );
  }

  getTaskStartDate = () => {
    const { record, labels } = this.props;
    
    return(
      <div className="pure-u-1">
        <div className="name">{labels.dateStarted}</div>
        <div className="value">{formatDateValue(record.dateStarted, 'TIME')}</div>
      </div>
    );
  }

  render() {
    const {labels, record} = this.props;

    return (
      <div className="pure-g">
          {this.getTaskStartDate()}
          <div className="pure-u-1">
              {this.getChecklistElement()}
              {this.getChecklistStateIndicatorElement()}
          </div>

          {this.getTaskElement()}

          <div className="pure-u-1">
              <div className="name">{labels.ownerName}</div>
              <div className="value">{this.getOwnership(record.ownershipDetails)}</div>
          </div>
      </div>
    );
  }
}
