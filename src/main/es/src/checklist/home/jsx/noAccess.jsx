'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const NoAccess=({labels})=>{
  return (
    <div className="no-access">
      <div className="error">{labels.noAccess||'Access denied'}</div>
    </div>
  );
};

NoAccess.propTypes={
  labels: PropTypes.object.isRequired
};

export default NoAccess;
