'use strict';
import { Record } from 'immutable';

const getValue = function (val) {
    if (typeof val === 'undefined') {
        return null;
    }
    return val;
};

const baseChecklist = {
    id: undefined,
    priority: undefined,
    objectVersion: undefined
};

export class ChecklistForViewPriority extends Record({
    ...baseChecklist,
    checklistDefinitionName: undefined,
    subject: undefined
}) {
    getData() {
        return {
            id: getValue(this.id),
            priority: getValue(this.priority),
            objectVersion: getValue(this.objectVersion),
            checklistDefinitionName: getValue(this.checklistDefinitionName),
            subject: getValue(this.subject),
        };
    }
}

export class ChecklistForUpdatePriority extends Record({
    ...baseChecklist
}) {
    getData() {
        return {
            id: getValue(this.id),
            priority: getValue(this.priority),
            objectVersion: getValue(this.objectVersion)
        };
    }
}

export class ChecklistForReassignTaskOwnership extends Record({
    ...baseChecklist,
    tasks: undefined,
    checklistDefinitionName: undefined,
    subject: undefined
}) {
    getData() {
        return {
            id: getValue(this.id),
            tasks: getValue(this.tasks),
            objectVersion: getValue(this.objectVersion),
            checklistDefinitionName: getValue(this.checklistDefinitionName),
            subject: getValue(this.subject)
        };
    }
}