'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '../../../table/jsx/table';
import memoizeOne from 'memoize-one';
import '../../dialogs/less/app.less';
import { date } from '../../../table/js/formatter';
import Checkbox from '../../../webform/uspCheckbox';

class TasksTable extends Component {
    static propTypes = {
        config: PropTypes.shape({
            searchConfig: PropTypes.shape({
                labels: PropTypes.object.isRequired
            }).isRequired,
            permissions: PropTypes.object.isRequired
        }).isRequired,
        records: PropTypes.array.isRequired,
        loading: PropTypes.bool
    };

    getColumns = memoizeOne((labels, selectedIds, handleCheckboxClick) => {
        
        const columns = [
            {
                key: 'name',
                label: labels.name,
                width: '24%'
            }, 
            {
                key: 'startDate',
                label: labels.startDate,
                formatter: date,
                width: '18%'
            },
            {
                key: 'dueDate',
                label: labels.dueDate,
                formatter: date,
                width: '18%'
            },
            {
                key: 'owner',
                label: labels.owner,
                width: '25%'
            },
           {
                key: 'overdue',
                label: labels.overDue,
                formatter: overdue =>
                    overdue ? (
                        <i className="checklist-status-indicator late"></i>
                    ) : (
                        <i className="checklist-status-indicator on-time"></i>
                    ),
                width: '8%'
            },                        
            {
                key: 'selected',
                label: ' ',
                formatter: (column, data) => {
                    const taskId = data.taskId;
                    const isSelected = selectedIds[taskId];

                    return (
                        <Checkbox
                            id={`task${taskId}`}
                            key={`task${taskId}`}
                            name={taskId}
                            value={!isSelected}
                            className="checkbox"
                            checked={isSelected}
                            onUpdate={handleCheckboxClick}
                        />
                    );
                },
                width: '5%'
            }
        ];

        return columns;
    });

    getTable() {
        const { config, records, loading, selectedIds, handleCheckboxClick } = this.props;
        const { labels } = config;

        return (
            <Table
                key={'Tasks'}
                data={records}
                columns={this.getColumns(labels, selectedIds, handleCheckboxClick)}
                loading={loading}
            />
        );
    }

    render() {
        return <div className={`${this.props.tabName}-tasks-container`}>{this.getTable()}</div>;
    }
}

export default TasksTable;

TasksTable.propTypes = {    
    config: PropTypes.shape({           
                labels: PropTypes.object.isRequired
    }).isRequired,            
    handleCheckboxClick: PropTypes.func.isRequired,
    tabName: PropTypes.string.isRequired,
    selectedIds: PropTypes.array.isRequired
 };