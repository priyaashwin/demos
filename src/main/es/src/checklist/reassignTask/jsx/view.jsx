'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import { ChecklistForReassignTaskOwnership } from '../../checklist';
import AppDialog from '../../../dialog/dialog';
import Errors from '../../../dialog/errors/errors';
import Button from '../../../dialog/button';
import Model from '../../../model/model';
import endpoint from '../js/cfg';
import TasksTable from './tasksTable';
import Loading from '../../../loading/loading';
import ErrorMessage from '../../../error/error';
import TaskAssignView from './taskAssignView';
import ResultsList from '../../../results/resultsList';
import ModelList from '../../../model/modelList';
import PropTypes from 'prop-types';
import { getCancelToken } from 'usp-xhr';

const page = {
    TASK_SELECTION: 'TASK_SELECTION',
    TASK_ASSIGN: 'TASK_ASSIGN'
};

const getDefaultState = function (props) {
    const { currentPersonId, currentPersonTeamId } = props.searchConfig;

    return {
        errors: null,
        loading: false,
        checklist: new ChecklistForReassignTaskOwnership({}),
        selectedIds: {},
        currentPage: page.TASK_SELECTION,
        personTeams: [],
        otherPersonTeams: [],
        currentPersonId: currentPersonId,
        currentPersonTeamId: currentPersonTeamId,
        ownershipDetails: {
            ownerId: currentPersonId,
            ownerSubjectType: 'PERSON',
            ownerName: null,
            owningTeamId: currentPersonTeamId
        }
    };
};

export default class UpdateChecklistTaskOwnershipDialog extends PureComponent {
    state = getDefaultState(this.props);

    componentDidCatch() {
        this.setState({
            errors: 'Unexpected error'
        });
    }

    componentWillUnmount() {
        // Cancel any outstanding xhr requests
        if (this.cancelTokenTeamsList) {
            this.cancelTokenTeamsList.cancel();
            //remove property
            delete this.cancelTokenTeamsList;
        }

        if (this.cancelTokenOtherPersonTeamsList) {
            this.cancelTokenOtherPersonTeamsList.cancel();
            //remove property
            delete this.cancelTokenOtherPersonTeamsList;
        }

        if (this.cancelTokenCheckList) {
            this.cancelTokenCheckList.cancel();
            //remove property
            delete this.cancelTokenCheckList;
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.ownershipDetails.ownerId !== this.state.ownershipDetails.ownerId) {
            this.loadOtherPersonTeamData(this.state.ownershipDetails.ownerId);
        }
    }

    handleUpdateTaskOwnership = () => {
        const { urls } = this.props;
        const { ownerId, ownerSubjectType, owningTeamId } = this.state.ownershipDetails;

        const selectedTasksArray = Object.keys(this.state.selectedIds)
            .filter(id => this.state.selectedIds[id])
            .map(id => Number(id));

        this.setState(
            {
                loading: true
            },
            () => {
                const updateTaskModel = new Model(urls.updateTaskOwnershipUrL);
                //actually move onto the save
                updateTaskModel
                    .save(endpoint.reassignTaskOwnershipEndpoint, {
                        _type: 'UpdateTaskInstancesOwner',
                        taskIds: selectedTasksArray,
                        ownerId: Number(ownerId),
                        ownerSubjectType: ownerSubjectType,
                        owningTeamId: Number(owningTeamId),
                        id: this.state.checklist.id
                    })
                    .then(() => {
                        const dialog = this.dialog;
                        if (dialog) {
                            //fire success message
                            dialog.fireSuccessMessage();
                            this.hideDialog();
                        }
                    })
                    .catch(reject =>
                        this.setState({
                            loading: false,
                            errors: reject.messages || 'Unable to save'
                        })
                    );
            }
        );
    };

    handleUpdateOwnershipDetails = newOwnershipDetails => {
        const ownershipDetails = this.state.ownershipDetails;

        this.setState({
            ownershipDetails: {
                ...ownershipDetails,
                ...newOwnershipDetails
            }
        });
    };

    clearErrors = () => {
        this.setState({
            errors: null
        });
    };

    getDialogView() {
        // const { labels, permissions } = this.props;
        const { errors } = this.state;
        let errorsContent;

        if (errors) {
            errorsContent = (
                <Errors key="errors-panel" errors={errors} onClose={this.clearErrors} target={this.getDialogTarget} />
            );
        }

        return <>{errorsContent}</>;
    }

    getResultList(id, url, endpoint, cancelToken) {
        return new ResultsList().load({
            ...endpoint,
            url: new ModelList(url).getURL(
                {
                    id
                },
                cancelToken
            )
        });
    }

    loadData = (currentPersonId, checklistId) => {
        const { urls } = this.props;

        // if there are any outstanding requests cancel them
        if (this.cancelTokenTeamsList) {
            //cancel existing requests
            this.cancelTokenTeamsList.cancel();
        }

        if (this.cancelTokenCheckList) {
            //cancel existing requests
            this.cancelTokenCheckList.cancel();
        }

        // Setup new cancel token for each request.
        this.cancelTokenTeamsList = getCancelToken();
        this.cancelTokenCheckList = getCancelToken();

        return Promise.all([
            this.getResultList(
                currentPersonId,
                urls.getTeamsUrl,
                endpoint.teamsListEndpoint,
                this.cancelTokenTeamsList
            ),
            this.getResultList(
                checklistId,
                urls.getTasksUrl,
                endpoint.checklistInstanceListEndpoint,
                this.cancelTokenCheckList
            )
        ])
            .then(result => {
                const [teamsResult, checklistResult] = result;

                const teams = teamsResult.results.map(result => result.organisationVO);
                const checklist = new ChecklistForReassignTaskOwnership(checklistResult);
                const selectedIds = {};

                this.setState({
                    loading: false,
                    errors: null,
                    personTeams: teams,
                    checklist: checklist,
                    selectedIds: selectedIds
                });
            })
            .catch(reject => {
                this.setState({
                    loading: false,
                    errors: reject,
                    checklist: null
                });
            });
    };

    loadOtherPersonTeamData = personId => {
        const { urls } = this.props;

        if (this.cancelTokenOtherPersonTeamsList) {
            //cancel existing request
            this.cancelTokenOtherPersonTeamsList.cancel();
        }

        // Setup new cancel token.
        this.cancelTokenOtherPersonTeamsList = getCancelToken();

        this.getResultList(personId, urls.getTeamsUrl, endpoint.teamsListEndpoint, this.cancelTokenOtherPersonTeamsList)
            .then(result => {
                const otherPersonTeams = result.results.map(result => result.organisationVO);

                this.setState({
                    loading: false,
                    errors: null,
                    otherPersonTeams: otherPersonTeams
                });
            })
            .catch(reject => {
                this.setState({
                    loading: false,
                    errors: reject
                });
            });
    };

    showDialog(checklistId) {
        const dialog = this.dialog;
        const currentPersonId = this.props.searchConfig.currentPersonId;

        if (dialog) {
            this.loadData(currentPersonId, checklistId).then(() => {
                dialog.showDialog();
            });
        }
    }

    hideDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and hide
            dialog.hideDialog();
            this.setState(getDefaultState(this.props));
        }
    }

    fireSuccessMessage() {
        const dialog = this.dialog;

        if (dialog) {
            dialog.fireSuccessMessage();
        }
    }

    handleCheckboxClick = (id, value) => {
        const boolValue = !!value;

        this.setState({
            selectedIds: { ...this.state.selectedIds, [id]: boolValue }
        });
    };

    handleCancel = () => {
        this.hideDialog();
    };

    getNavigationButton = () => {
        const nextPage = this.state.currentPage === page.TASK_SELECTION ? page.TASK_ASSIGN : page.TASK_SELECTION;
        const onClick = () => this.setState({ currentPage: nextPage });
        const title = this.state.currentPage === page.TASK_SELECTION ? 'next' : 'previous';
        const icon = this.state.currentPage === page.TASK_SELECTION ? 'fa fa-arrow-right' : 'fa fa-arrow-left';

        return (
            <Button key="primaryButtonOne" type="primary" onClick={onClick} title={title} icon={icon}>
                {' '}
                {title}
            </Button>
        );
    };

    getSubmitButton = () => {
        const { buttons } = this.props.labels;
        if (this.state.currentPage === page.TASK_ASSIGN) {
            return (
                <Button
                    key="primaryButtonTwo"
                    type="primary"
                    onClick={this.handleUpdateTaskOwnership}
                    title={buttons.reassignSubmitTitle}
                    icon="fa-check">
                    {' '}
                    {buttons.reassignSubmitText}
                </Button>
            );
        }
    };

    getButtons() {
        return (
            <>
                {this.getNavigationButton()}
                {this.getSubmitButton()}
                <Button
                    key="secondaryButton"
                    type="secondary"
                    onClick={this.handleCancel}
                    title={this.props.labels.buttons.reassignCancel}
                    icon="fa-times">
                    {' '}
                    {this.props.labels.buttons.reassignCancel}
                </Button>
            </>
        );
    }

    dialogRef = ref => (this.dialog = ref);
    getDialogTarget = () => ReactDOM.findDOMNode(this.dialog);

    getOwnerName = task => {
        const ownerName = task.ownerName;
        const owningTeam = task.ownershipDetails.owningTeamDetails.name;

        if (ownerName === owningTeam) {
            return owningTeam;
        } else {
            return `${ownerName} / ${owningTeam}`;
        }
    };

    // Convert input checklist to an array of objects containing the task data
    mapTasks(checklist) {
        const mapTask = task => {
            return {
                taskId: task.id,
                name: task.taskName,
                overdue: task.overdue,
                startDate: task.dateStarted,
                dueDate: task.dateDue,
                condition: task.condition,
                owner: this.getOwnerName(task)
            };
        };

        if (checklist && checklist.getData().tasks) {
            // List results in ascending order by dueDate
            const sortByDueDate = (o1, o2) => new Date(o1.dueDate) - new Date(o2.dueDate);

            return checklist.getData().tasks.map(mapTask).sort(sortByDueDate);
        }
        return [];
    }

    renderResultsTable(config, records, loading, errors, selectedIds) {
        if (loading) {
            return <Loading />;
        } else if (errors) {
            return <ErrorMessage errors={errors} onClose={this.clearErrors} />;
        } else {
            return (
                <TasksTable
                    config={config}
                    records={records}
                    loading={loading}
                    selectedIds={selectedIds}
                    handleCheckboxClick={this.handleCheckboxClick}
                />
            );
        }
    }

    renderTaskAssignView(
        teams,
        ownershipDetails,
        currentPersonId,
        currentPersonTeamId,
        otherPersonTeams,
        urls,
        searchConfig,
        labels
    ) {
        const teamPairs = teams.map(team => ({ text: team.name, value: team.id }));
        const otherPersonTeamPairs = otherPersonTeams.map(otherPersonTeam => ({
            text: otherPersonTeam.name,
            value: otherPersonTeam.id
        }));

        return (
            <TaskAssignView
                teamPairs={teamPairs}
                otherPersonTeamPairs={otherPersonTeamPairs}
                handleUpdateOwnershipDetails={this.handleUpdateOwnershipDetails}
                ownershipDetails={ownershipDetails}
                currentPersonId={currentPersonId}
                currentPersonTeamId={currentPersonTeamId}
                urls={urls}
                searchConfig={searchConfig}
                labels={labels}></TaskAssignView>
        );
    }

    render() {
        const { dialogConfig, searchConfig, urls, labels } = this.props;
        const {
            loading,
            errors,
            checklist,
            selectedIds,
            ownershipDetails,
            personTeams,
            currentPersonId,
            currentPersonTeamId,
            otherPersonTeams
        } = this.state;
        const mappedTasks = this.mapTasks(checklist);

        return (
            <AppDialog
                key="reassignDialog"
                ref={this.dialogRef}
                header={dialogConfig.header}
                narrative={dialogConfig.narrative}
                successMessage={dialogConfig.successMessage}
                buttons={this.getButtons()}
                defaultOpen={false}
                busy={loading}
                data={checklist}>
                {this.state.currentPage === page.TASK_SELECTION
                    ? this.renderResultsTable(searchConfig, mappedTasks, loading, errors, selectedIds)
                    : this.renderTaskAssignView(
                          personTeams,
                          ownershipDetails,
                          currentPersonId,
                          currentPersonTeamId,
                          otherPersonTeams,
                          urls,
                          searchConfig,
                          labels
                      )}
            </AppDialog>
        );
    }
}

UpdateChecklistTaskOwnershipDialog.propTypes = {
    dialogConfig: PropTypes.shape({
        header: PropTypes.object.isRequired,
        narrative: PropTypes.shape({
            summary: PropTypes.string.isRequired,
            description: PropTypes.string
        }).isRequired,
        successMessage: PropTypes.string
    }).isRequired,
    searchConfig: PropTypes.shape({
        codedEntries: PropTypes.object.isRequired,
        currentPersonId: PropTypes.string.isRequired,
        currentPersonTeamId: PropTypes.string.isRequired
    }).isRequired,
    urls: PropTypes.shape({
        getTasksUrl: PropTypes.string.isRequired,
        getTeamsUrl: PropTypes.string.isRequired,
        getPersonAutoCompleteUrl: PropTypes.string.isRequired,
        getTeamAutoCompleteUrl: PropTypes.string.isRequired,
        updateTaskOwnershipUrL: PropTypes.string.isRequired
    }).isRequired,
    ownershipDetails: PropTypes.object.isRequired,
    labels: PropTypes.shape({
        buttons: PropTypes.shape({
            reassignSubmitTitle: PropTypes.string.isRequired,
            reassignSubmitText: PropTypes.string.isRequired,
            reassignCancel: PropTypes.string.isRequired
        }).isRequired,
        radioGroups: PropTypes.shape({
            typeOfOwner: PropTypes.string.isRequired,
            individualOwner: PropTypes.string.isRequired,
            teamOwner: PropTypes.string.isRequired,
            individualOwnerHeader: PropTypes.string.isRequired,
            individualOwnerSelf: PropTypes.string.isRequired,
            individualOwnerOther: PropTypes.string.isRequired,
            teamOwnerHeader: PropTypes.string.isRequired,
            teamOwnerSelf: PropTypes.string.isRequired,
            teamOwnerOther: PropTypes.string.isRequired
        }).isRequired
    }).isRequired
};
