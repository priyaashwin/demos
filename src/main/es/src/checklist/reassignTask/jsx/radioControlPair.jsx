'use strict';
import React from 'react';
import Radio from '../../../webform/uspRadio';
import Label from '../../../webform/uspLabel';
import Select from '../../../webform/uspSelect';
import PersonAutoComplete from '../../../autocomplete/jsx/personAutoComplete';
import TeamAutoComplete from '../../../autocomplete/jsx/teamAutoComplete';
import endpoint from '../js/cfg';
import memoize from 'memoize-one';
import PropTypes from 'prop-types';

export default function RadioControlPair(props) {
    const {
        radioButtonOneProps,
        radioButtonTwoProps,
        selectedOwnerType,
        selectedIndividualOwnerType,
        selectedTeamOwnerType,
        ownerType
    } = props;

    const { getPersonAutoCompleteUrl, getTeamAutoCompleteUrl } = props.urls;

    const handlePersonIdSelection = value => {
        if (value) {
            props.handleUpdateOwnershipDetails({
                ownerId: value.id,
                ownerSubjectType: 'PERSON',
                ownerName: value.name
            });
        }
    };

    const handleTeamIdSelection = value => {
        if (value) {
            props.handleUpdateOwnershipDetails({
                ownerId: value.id,
                ownerSubjectType: 'ORGANISATION',
                owningTeamId: value.id
            });
        }
    };

    const handleUpdateTeam = (name, value) => {
        props.handleUpdateOwnershipDetails({ owningTeamId: value });
    };

    const getEndpoint = memoize((endpoint, url) => ({ ...endpoint, url: url }));

    const renderTeamSelect = (selectedOwnerType, selectedIndividualOwnerType, selectedTeamOwnerType, ownerType) => {
        const isIndividualSelfSelected =
            selectedIndividualOwnerType === ownerType.INDIVIDUAL_SELF && selectedOwnerType === ownerType.INDIVIDUAL;

        const isOwnTeamSelected = selectedTeamOwnerType === ownerType.TEAM_OWN && selectedOwnerType === ownerType.TEAM;

        if (isIndividualSelfSelected || isOwnTeamSelected) {
            return (
                <Select
                    key="myTeamNameSelect"
                    id="reassignTask_myTeamNameSelect"
                    name="owningTeamId"
                    options={props.teamPairs}
                    value={props.ownershipDetails.owningTeamId}
                    onUpdate={handleUpdateTeam}
                    mandatory={false}
                />
            );
        }
    };

    const renderPersonAutocompleteSelect = (selectedOwnerType, selectedIndividualOwnerType, ownerType) => {
        const isIndividualOtherSelected =
            selectedIndividualOwnerType === ownerType.INDIVIDUAL_OTHER && selectedOwnerType === ownerType.INDIVIDUAL;

        if (isIndividualOtherSelected) {
            return (
                <PersonAutoComplete
                    placeholder={props.labels.personSelectPlaceholder}
                    onSelection={handlePersonIdSelection}
                    endpoint={getEndpoint(endpoint.personAutoCompleteEndpoint, getPersonAutoCompleteUrl)}
                    queryParameter="nameOrUsername"
                    codedEntries={props.codedEntries}
                />
            );
        }
    };

    const renderTeamAutocompleteSelect = (selectedOwnerType, selectedTeamOwnerType, ownerType) => {
        const isTeamOtherSelected =
            selectedTeamOwnerType === ownerType.TEAM_OTHER && selectedOwnerType === ownerType.TEAM;

        if (isTeamOtherSelected) {
            return (
                <TeamAutoComplete
                    placeholder={props.labels.teamSelectPlaceholder}
                    onSelection={handleTeamIdSelection}
                    endpoint={getEndpoint(endpoint.teamAutoCompleteEndpoint, getTeamAutoCompleteUrl)}
                    queryParameter="nameOrOrgIdOrPrevName"
                />
            );
        }
    };

    const renderOtherPersonTeamSelect = (selectedOwnerType, selectedIndividualOwnerType, ownerType) => {
        const isOtherPersonSelected =
            selectedOwnerType === ownerType.INDIVIDUAL &&
            selectedIndividualOwnerType === ownerType.INDIVIDUAL_OTHER &&
            props.ownershipDetails.ownerId !== props.currentPersonId;

        if (isOtherPersonSelected) {
            return (
                <Select
                    key="otherPersonTeamNameSelect"
                    id="otherPersonTeamNameSelect"
                    name="owningTeamId"
                    options={props.otherPersonTeamPairs}
                    value={props.ownershipDetails.owningTeamId}
                    onUpdate={handleUpdateTeam}
                    label={props.labels.otherPersonTeamSelect + ' ' + props.ownershipDetails.ownerName}
                    mandatory={false}
                    includeEmptyOption={true}
                />
            );
        }
    };

    return (
        <>
            <Label forId={props.fieldsetId} label={props.headerLabel} />
            <div className="control-col l-box">
                <fieldset key={props.fieldsetKey} id={props.fieldsetId} className="pure-fieldset">
                    <div className="fieldset-wrap pure-g-r">
                        <div className="pure-u-2-5">
                            <Radio
                                name={props.fieldsetKey}
                                id={radioButtonOneProps.id}
                                label={radioButtonOneProps.label}
                                value={radioButtonOneProps.value}
                                checked={radioButtonOneProps.checked}
                                onUpdate={radioButtonOneProps.handleRadioUpdate}
                            />
                        </div>
                        <div className="pure-u-3-5">
                            {renderTeamSelect(
                                selectedOwnerType,
                                selectedIndividualOwnerType,
                                selectedTeamOwnerType,
                                ownerType
                            )}
                        </div>
                        <div className="pure-u-2-5">
                            <Radio
                                name={props.fieldsetKey}
                                id={radioButtonTwoProps.id}
                                label={radioButtonTwoProps.label}
                                value={radioButtonTwoProps.value}
                                checked={radioButtonTwoProps.checked}
                                onUpdate={radioButtonTwoProps.handleRadioUpdate}
                            />
                        </div>
                        <div className="pure-u-3-5">
                            {renderPersonAutocompleteSelect(selectedOwnerType, selectedIndividualOwnerType, ownerType)}
                            {renderTeamAutocompleteSelect(selectedOwnerType, selectedTeamOwnerType, ownerType)}
                        </div>
                        <div className="pure-u-2-5" />
                        <div className="pure-u-3-5">
                            {renderOtherPersonTeamSelect(selectedOwnerType, selectedIndividualOwnerType, ownerType)}
                        </div>
                    </div>
                </fieldset>
            </div>
        </>
    );
}

RadioControlPair.propTypes = {
    radioButtonOneProps: PropTypes.object,
    radioButtonTwoProps: PropTypes.object,
    urls: PropTypes.shape({
        getTasksUrl: PropTypes.string.isRequired,
        getTeamsUrl: PropTypes.string.isRequired,
        getPersonAutoCompleteUrl: PropTypes.string.isRequired,
        getTeamAutoCompleteUrl: PropTypes.string.isRequired,
        updateTaskOwnershipUrL: PropTypes.string.isRequired
    }).isRequired,
    labels: PropTypes.shape({
        buttons: PropTypes.shape({
            reassignSubmitTitle: PropTypes.string.isRequired,
            reassignSubmitText: PropTypes.string.isRequired,
            reassignCancel: PropTypes.string.isRequired
        }).isRequired,
        radioGroups: PropTypes.shape({
            typeOfOwner: PropTypes.string.isRequired,
            individualOwner: PropTypes.string.isRequired,
            teamOwner: PropTypes.string.isRequired,
            individualOwnerHeader: PropTypes.string.isRequired,
            individualOwnerSelf: PropTypes.string.isRequired,
            individualOwnerOther: PropTypes.string.isRequired,
            teamOwnerHeader: PropTypes.string.isRequired,
            teamOwnerSelf: PropTypes.string.isRequired,
            teamOwnerOther: PropTypes.string.isRequired
        }).isRequired,
        teamSelectPlaceholder: PropTypes.string.isRequired,
        personSelectPlaceholder: PropTypes.string.isRequired,
        otherPersonTeamSelect: PropTypes.string.isRequired
    }).isRequired,
    selectedOwnerType: PropTypes.string.isRequired,
    selectedIndividualOwnerType: PropTypes.string.isRequired,
    selectedTeamOwnerType: PropTypes.string.isRequired,
    ownerType: PropTypes.object.isRequired,
    handleUpdateOwnershipDetails: PropTypes.func.isRequired,
    ownershipDetails: PropTypes.object.isRequired,
    codedEntries: PropTypes.object.isRequired,
    currentPersonId: PropTypes.string.isRequired,
    currentPersonTeamId: PropTypes.string.isRequired,
    fieldsetId: PropTypes.string.isRequired,
    headerLabel: PropTypes.string.isRequired,
    fieldsetKey: PropTypes.string.isRequired,
    teamPairs: PropTypes.array.isRequired,
    otherPersonTeamPairs: PropTypes.array.isRequired,
    otherPersonDetails: PropTypes.object.isRequired
};
