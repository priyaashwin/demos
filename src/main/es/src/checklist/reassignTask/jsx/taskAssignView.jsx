'use strict';
import React, { PureComponent } from 'react';
import Radio from '../../../webform/uspRadio';
import Form from '../../../webform/uspForm';
import Label from '../../../webform/uspLabel';
import RadioControlPair from './radioControlPair';
import PropTypes from 'prop-types';

const ownerType = {
    INDIVIDUAL: 'INDIVIDUAL',
    INDIVIDUAL_SELF: 'INDIVIDUAL_SELF',
    INDIVIDUAL_OTHER: 'INDIVIDUAL_OTHER',
    TEAM: 'TEAM',
    TEAM_OWN: 'TEAM_OWN',
    TEAM_OTHER: 'TEAM_OTHER'
};

export default class TaskAssignView extends PureComponent {
    state = {
        selectedOwnerType: ownerType.INDIVIDUAL,
        selectedIndividualOwnerType: ownerType.INDIVIDUAL_SELF,
        selectedTeamOwnerType: ownerType.TEAM_OWN
    };

    handleRadioUpdate = (name, value) => {
        this.setState({ [name]: value });

        const { handleUpdateOwnershipDetails, searchConfig } = this.props;
        const { currentPersonId, currentPersonTeamId } = searchConfig;

        switch (value) {
            case ownerType.INDIVIDUAL:
                this.setState({ selectedIndividualOwnerType: ownerType.INDIVIDUAL_SELF });
                break;
            case ownerType.TEAM:
                this.setState({ selectedTeamOwnerType: ownerType.TEAM_OWN });
                break;
            case ownerType.INDIVIDUAL_SELF:
                handleUpdateOwnershipDetails({
                    ownerId: currentPersonId,
                    ownerSubjectType: 'PERSON',
                    owningTeamId: currentPersonTeamId
                });
                break;
            case ownerType.TEAM_OUN:
                handleUpdateOwnershipDetails({
                    ownerId: currentPersonTeamId,
                    ownerSubjectType: 'ORGANISATION',
                    owningTeamId: currentPersonTeamId
                });
                break;
            default:
        }
    };

    renderInput = () => {
        const { radioGroups } = this.props.labels;

        const individualRadioButtonPairConfig = {
            fieldSetId: 'selectIndividualOwnerType',
            headerLabel: radioGroups.individualOwnerHeader,
            fieldsetKey: 'selectedIndividualOwnerType',
            radioButtonOneProps: {
                id: 'individual-owner-self',
                label: radioGroups.individualOwnerSelf,
                value: ownerType.INDIVIDUAL_SELF,
                checked:
                    this.state.selectedIndividualOwnerType === ownerType.INDIVIDUAL_SELF &&
                    this.state.selectedOwnerType === ownerType.INDIVIDUAL,
                handleRadioUpdate: this.handleRadioUpdate
            },
            radioButtonTwoProps: {
                id: 'individual-owner-other',
                label: radioGroups.individualOwnerOther,
                value: ownerType.INDIVIDUAL_OTHER,
                checked:
                    this.state.selectedIndividualOwnerType === ownerType.INDIVIDUAL_OTHER &&
                    this.state.selectedOwnerType === ownerType.INDIVIDUAL,
                handleRadioUpdate: this.handleRadioUpdate
            }
        };

        const teamRadioButtonPairConfig = {
            fieldSetId: 'selectTeamOwnerType',
            headerLabel: radioGroups.teamOwnerHeader,
            fieldsetKey: 'selectedTeamOwnerType',
            radioButtonOneProps: {
                id: 'team-owner-self',
                label: radioGroups.teamOwnerSelf,
                value: ownerType.TEAM_OWN,
                checked:
                    this.state.selectedTeamOwnerType === ownerType.TEAM_OWN &&
                    this.state.selectedOwnerType === ownerType.TEAM,
                handleRadioUpdate: this.handleRadioUpdate
            },
            radioButtonTwoProps: {
                id: 'team-owner-other',
                label: radioGroups.teamOwnerOther,
                value: ownerType.TEAM_OTHER,
                checked:
                    this.state.selectedTeamOwnerType === ownerType.TEAM_OTHER &&
                    this.state.selectedOwnerType === ownerType.TEAM,
                handleRadioUpdate: this.handleRadioUpdate
            }
        };

        const radioButtonPairProps =
            this.state.selectedOwnerType === ownerType.INDIVIDUAL
                ? individualRadioButtonPairConfig
                : teamRadioButtonPairConfig;

        return (
            <RadioControlPair
                {...radioButtonPairProps}
                ownershipDetails={this.props.ownershipDetails}
                handleUpdateOwnershipDetails={this.props.handleUpdateOwnershipDetails}
                teamPairs={this.props.teamPairs}
                otherPersonTeamPairs={this.props.otherPersonTeamPairs}
                selectedOwnerType={this.state.selectedOwnerType}
                selectedIndividualOwnerType={this.state.selectedIndividualOwnerType}
                selectedTeamOwnerType={this.state.selectedTeamOwnerType}
                ownerType={ownerType}
                currentPersonId={this.props.searchConfig.currentPersonId}
                currentPersonTeamId={this.props.searchConfig.currentPersonTeamId}
                urls={this.props.urls}
                labels={this.props.labels}
                codedEntries={this.props.searchConfig.codedEntries}
            />
        );
    };

    render() {
        const { radioGroups } = this.props.labels;

        return (
            <Form id="assignTaskForm">
                <Label forId="selectOwnerType" label={radioGroups.typeOfOwner} />
                <div className="control-col l-box">
                    <fieldset key="selectedOwnerType" id="selectOwnerType" className="pure-fieldset">
                        <div className="fieldset-wrap pure-g-r">
                            <div className="pure-u-1-2">
                                <Radio
                                    name="selectedOwnerType"
                                    id="individual-owner"
                                    label={radioGroups.individualOwner}
                                    value={ownerType.INDIVIDUAL}
                                    checked={this.state.selectedOwnerType === ownerType.INDIVIDUAL}
                                    onUpdate={this.handleRadioUpdate}
                                />
                            </div>
                            <div className="pure-u-1-2">
                                <Radio
                                    name="selectedOwnerType"
                                    id="team-owner"
                                    label={radioGroups.teamOwner}
                                    value={ownerType.TEAM}
                                    checked={this.state.selectedOwnerType === ownerType.TEAM}
                                    onUpdate={this.handleRadioUpdate}
                                />
                            </div>
                        </div>
                    </fieldset>
                </div>
                {this.renderInput()}
            </Form>
        );
    }
}

TaskAssignView.propTypes = {
    urls: PropTypes.shape({
        getTasksUrl: PropTypes.string.isRequired,
        getTeamsUrl: PropTypes.string.isRequired,
        getPersonAutoCompleteUrl: PropTypes.string.isRequired,
        getTeamAutoCompleteUrl: PropTypes.string.isRequired,
        updateTaskOwnershipUrL: PropTypes.string.isRequired
    }).isRequired,
    handleUpdateOwnershipDetails: PropTypes.func.isRequired,
    ownershipDetails: PropTypes.object.isRequired,
    searchConfig: PropTypes.shape({
        codedEntries: PropTypes.object.isRequired,
        currentPersonId: PropTypes.string.isRequired,
        currentPersonTeamId: PropTypes.string.isRequired
    }).isRequired,
    teamPairs: PropTypes.array.isRequired,
    otherPersonTeamPairs: PropTypes.array.isRequired,
    labels: PropTypes.shape({
        buttons: PropTypes.shape({
            reassignSubmitTitle: PropTypes.string.isRequired,
            reassignSubmitText: PropTypes.string.isRequired,
            reassignCancel: PropTypes.string.isRequired
        }).isRequired,
        radioGroups: PropTypes.shape({
            typeOfOwner: PropTypes.string.isRequired,
            individualOwner: PropTypes.string.isRequired,
            teamOwner: PropTypes.string.isRequired,
            individualOwnerHeader: PropTypes.string.isRequired,
            individualOwnerSelf: PropTypes.string.isRequired,
            individualOwnerOther: PropTypes.string.isRequired,
            teamOwnerHeader: PropTypes.string.isRequired,
            teamOwnerSelf: PropTypes.string.isRequired,
            teamOwnerOther: PropTypes.string.isRequired
        }).isRequired
    }).isRequired
};
