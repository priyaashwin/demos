'use strict';

class Endpoint{
  constructor() {
    this.checklistInstanceListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.checklist.ChecklistInstanceWithTasks+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.reassignTaskOwnershipEndpoint={
      method: 'put',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      responseStatus:200
    };

    this.teamsListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.relationship.PersonOrganisationRelationship+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.personAutoCompleteEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.person.PersonWithAddressAndSecurityAccessStatus+json'
      },
      responseType: 'json',
      responseStatus:200
    };    
    
    this.teamAutoCompleteEndpoint = {
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.organisation.OrganisationWithAddressAndPreviousnames+json'        	
      },
      params: {
        pageSize: 50,
        appendWildcard: true
      },
      responseType: 'json',
      responseStatus: 200
    };   
  }
}

export default new Endpoint();
