'use strict';

class Endpoint{
  constructor() {
    this.getChecklistEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',        
        'Accept': 'application/vnd.olmgroup-usp.checklist.ChecklistInstance+json;charset=UTF-8'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.updateChecklistPriorityEndpoint={
      method: 'put',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',        
        'Accept': 'application/vnd.olmgroup-usp.checklist.UpdateChecklistInstancePriority+json;charset=UTF-8'
      },
      responseType: 'json',
      responseStatus:200
    };
  }
}

export default new Endpoint();
