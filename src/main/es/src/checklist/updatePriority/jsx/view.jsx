'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import memoize from 'memoize-one';
import { ChecklistForViewPriority, ChecklistForUpdatePriority } from '../../checklist';
import AppDialog from '../../../dialog/dialog';
import Errors from '../../../dialog/errors/errors';
import Button from '../../../dialog/button';
import Details from './details';
import Form from '../../../webform/uspForm';
import Model from '../../../model/model';
import endpoint from '../js/cfg';
import { isSpecified } from '../../../utils/js/textUtils';
import { getCancelToken } from 'usp-xhr';
import { getEnumAttribute } from '../../../utils/js/enumUtils';

const getDefaultState = function () {
    return ({
        errors: null,
        loading: false,
        //always start with a fresh checklist model
        checklist: new ChecklistForViewPriority({}),
        initialPriority: null
    });
};

export default class UpdateChecklistPriorityDialog extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        enums: PropTypes.shape({
            ChecklistInstancePriority: PropTypes.shape({
                values: PropTypes.array.isRequired
            })
        }).isRequired,
        dialogConfig: PropTypes.shape({
            header: PropTypes.object.isRequired,
            narrative: PropTypes.shape({
                summary: PropTypes.string.isRequired,
                description: PropTypes.string
            }).isRequired,
            successMessage: PropTypes.string
        }).isRequired,
        urls: PropTypes.shape({
            getChecklistUrl: PropTypes.string.isRequired,
            setInitialChecklistPriorityUrl: PropTypes.string.isRequired,
            escalateChecklistInstancePriorityUrl: PropTypes.string.isRequired,
            descalateChecklistInstancePriorityUrl: PropTypes.string.isRequired
        }).isRequired,
        permissions: PropTypes.object.isRequired,
        handlePrioritiseSave: PropTypes.func.isRequired
    };
    state = getDefaultState(this.props);

    componentDidCatch() {
        this.setState({
            errors: 'Unexpected error'
        });
    }
    componentWillUnmount() {
        if (this.cancelToken) {
          //cancel any outstanding xhr requests
          this.cancelToken.cancel();
    
          //remove property
          delete this.cancelToken;
        }
    }
    handleUpdatePriority = () => {
        const { checklist } = this.state;

        this.setState({
            loading: true
        }, () => {
            //transmogrify
            const updateChecklist = new ChecklistForUpdatePriority(checklist.getData());
            const updateUrl = this.getUpdateUrl();

            if(updateUrl !== null) {
                const checklistModel = new Model(this.getUpdateUrl());
                //actually move onto the save
                checklistModel.save(endpoint.updateChecklistPriorityEndpoint, {
                    _type: 'UpdateChecklistInstancePriority',
                    ...updateChecklist.getData()
                }).then(() => {
                    this.props.handlePrioritiseSave();
                }).catch((reject) => this.setState({
                    loading: false,
                    errors: reject.messages || 'Unable to save',
                }));
            } else {
                this.setState({
                    loading: false,
                    errors: 'Please select a new priority'
                });
            }
        });
    };
    //get correct update URL by determining if we are escalating, deescalating or setting priority
    getUpdateUrl() {
        const { urls, enums } = this.props;
        const { initialPriority, checklist } = this.state;
        
        //get the numeric representation of our enum value
        const initialPriorityNumericValue = getEnumAttribute(enums.ChecklistInstancePriority, initialPriority, 'numericValue');
        const newPriorityNumericValue = getEnumAttribute(enums.ChecklistInstancePriority, checklist.priority, 'numericValue');

        //if initial value is null, we are setting the initial priority
        if(initialPriorityNumericValue === null) {
            return urls.setInitialChecklistPriorityUrl;
        }

        if(initialPriorityNumericValue === newPriorityNumericValue) {
            return null;
        } else if (initialPriorityNumericValue < newPriorityNumericValue) {
            //escalating
            return urls.escalateChecklistInstancePriorityUrl;
        } else {
            //deescalating
            return urls.descalateChecklistInstancePriorityUrl;
        }
    }
    loadData(checklistId) {

        //if there are any outstanding requests cancel them
        if (this.cancelToken) {
            //cancel any existing requests
            this.cancelToken.cancel();
        }

        return new Promise((resolve) => {
            const { urls } = this.props;
            if (isSpecified(checklistId)) {
                this.setState({
                    loading: true
                }, () => {
                    //setup a new cancel token
                    this.cancelToken = getCancelToken();

                    new Model(urls.getChecklistUrl).load(endpoint.getChecklistEndpoint, {
                        id: checklistId
                    }, this.cancelToken).then(result => this.setState({
                        loading: false,
                        errors: null,
                        checklist: new ChecklistForViewPriority(result),
                        //need to store initial priority in order to determine whether we are attempting to escalate, deescalate,
                        //or set the priority
                        initialPriority: result.priority
                    })).catch(reject => {
                        this.setState({
                            loading: false,
                            errors: reject,
                            checklist: null
                        });
                    });
                });
            } else {
                this.setState(getDefaultState(this.props));
            }
            resolve();
        });
    }
    handleCancel = () => {
        this.hideDialog();
    };
    clearErrors = () => {
        this.setState({
            errors: null
        });
    };
    handleChecklistDataUpdate = (nameOrObject, value) => {
        const { checklist } = this.state;

        let update;
        if (typeof nameOrObject === 'object') {
            update = nameOrObject;
        } else {
            update = {};
            update[nameOrObject] = value;
        }

        this.setState({
            checklist: checklist.merge(update)
        });
    };
    getPriorityValues = memoize((enums, permissions, initialPriority) => {
        const priorityEnum = enums.ChecklistInstancePriority;
      
        const initialPriorityNumericValue = getEnumAttribute(priorityEnum, initialPriority, 'numericValue');
  
        //if no priority has been set, return the full list of values
        if(initialPriorityNumericValue === null) {
            return priorityEnum;
        }
  
        //if priority has been set, return lower priority values if the user can deescalate, and higher priority values if the user can escalate
        return {
            ...priorityEnum,
            values: priorityEnum.values.filter((enumValue) => {
              const numericValue = getEnumAttribute(priorityEnum, enumValue.enumValue, 'numericValue');
              return (permissions.canEscalate && numericValue >= initialPriorityNumericValue) || (permissions.canDeescalate && numericValue <= initialPriorityNumericValue);
            })
        };
    });
    getDialogView() {
        const { labels, enums, permissions } = this.props;
        const { errors, checklist, initialPriority } = this.state;
        let errorsContent;
        const enumValues = this.getPriorityValues(enums, permissions, initialPriority);

        if (null !== errors) {
            errorsContent = (<Errors
                key="errors-panel"
                errors={errors}
                onClose={this.clearErrors}
                target={this.getDialogTarget}
            />);
        }

        return (
            <>
                {errorsContent}
                <Form id="updateChecklistPriority">
                    <Details
                        key="details"
                        labels={labels}
                        onUpdate={this.handleChecklistDataUpdate}
                        checklist={checklist}
                        enumeration={enumValues}
                    />
                </Form>
            </>
        );
    }
    showDialog(checklistId) {
        const dialog = this.dialog;

        if (dialog) {
            //Ensure we set state before showing dialog
            this.loadData(checklistId).then(() => {
                dialog.showDialog();
            });
        }
    }
    hideDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and hide
            dialog.hideDialog();
            this.setState(getDefaultState(this.props));
        }
    }
    fireSuccessMessage() {
        const dialog = this.dialog;

        if (dialog) {
            dialog.fireSuccessMessage();
        }
    }
    getButtons() {
        return (
            <>
                <Button
                    key="primaryButton"
                    type="primary"
                    onClick={this.handleUpdatePriority}
                    title="save"
                    icon="fa-check">
                    {' '}{'save'}
                </Button>
                <Button key="secondaryButton"
                    type="secondary"
                    onClick={this.handleCancel}
                    title="cancel"
                    icon="fa-times">
                    {' '}{'cancel'}
                </Button>
            </>
        );
    }
    dialogRef = (ref) => this.dialog = ref;
    getDialogTarget = () => ReactDOM.findDOMNode(this.dialog);
    render() {
        const { dialogConfig } = this.props;
        const { loading, checklist } = this.state;

        return (
            <AppDialog
                key="updateDialog"
                ref={this.dialogRef}
                header={dialogConfig.header}
                narrative={dialogConfig.narrative}
                successMessage={dialogConfig.successMessage}
                buttons={this.getButtons()}
                defaultOpen={false}
                busy={loading}
                data={checklist}>
                {this.getDialogView()}
            </AppDialog>
        );
    }
}
