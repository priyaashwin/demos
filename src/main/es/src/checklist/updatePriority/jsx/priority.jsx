'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import EnumerationSelect from '../../../webform/uspSelectForEnumeration';

const Priority=memo(({ priority, onUpdate, enumeration, ...other })=>(
    <div className="pure-u-1-2">
        <EnumerationSelect key="priority" id="updatePriority_priority" name="priority" onUpdate={onUpdate} includeEmptyOption={true}
            enumeration={enumeration} value={priority} { ...other }/>
    </div>
));

Priority.propTypes={
    onUpdate: PropTypes.func.isRequired,
    priority: PropTypes.string,
    enumeration: PropTypes.object.isRequired 
};

export default Priority;
