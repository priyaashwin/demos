'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { ChecklistForUpdatePriority } from '../../checklist';
import Priority from './priority';

const Details=memo(({ labels, onUpdate, checklist, enumeration })=> {
    let priority = '';

    if(checklist !== null) {
        priority = checklist.priority;
    }

    return (
        <div className="pure-g-r">
            <div className="pure-u-1 l-box">
                <Priority priority={priority} onUpdate={onUpdate} enumeration={enumeration} label={labels.priority} mandatory={true}/>
            </div>
        </div>
    );
});

Details.propTypes={
    labels: PropTypes.object.isRequired,
    checklist: PropTypes.oneOfType([
        PropTypes.instanceOf(ChecklistForUpdatePriority)
    ]).isRequired,
    onUpdate: PropTypes.func.isRequired,
    enumeration: PropTypes.object.isRequired
};

export default Details;