import './less/app.less';
import UpdateChecklistTaskOwnershipDialog from '../reassignTask/jsx/view';
import UpdateChecklistPriorityDialog from '../updatePriority/jsx/view';

export {UpdateChecklistTaskOwnershipDialog, UpdateChecklistPriorityDialog};