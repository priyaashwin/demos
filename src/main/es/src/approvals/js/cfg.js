'use strict';

class Endpoint{
  constructor() {

    this.fosterCareRegistrationEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.childlookedafter.fosterCarerRegistrationWithOrganisationAndApprovals+json'
      },
      params: {
        page:1,
        pageSize: -1,
        s: [{startDate:'desc'}]
      },
      responseType: 'json',
      responseStatus:200
    };    
  }
}

export default new Endpoint();
