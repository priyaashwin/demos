'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { yesNo } from '../../table/js/formatter';

const SummaryRow = ({ labels, record }) => (
    <div className="pure-g-r readonly">
        <div className="pure-u-1-5 l-box">
            <div className="name">{labels.disabilityPermitted}</div>
            <div className="value">{yesNo(record.disabilityPermitted)}</div>
        </div>
        <div className="pure-u-1-5 l-box">
            <div className="name">{labels.numberOfRooms}</div>
            <div className="value">{record.numberOfRooms}</div>
        </div>
        <div className="pure-u-1-5 l-box">
            <div className="name">{labels.ageRangePermitted}</div>
            <div className="value">{`${record.lowerAgePermitted} - ${record.upperAgePermitted}`}</div>
        </div>
        <div className="pure-u-1-5 l-box">
            <div className="name">{labels.maxRelatedPlacement}</div>
            <div className="value">{record.maxRelatedPlacements}</div>
        </div>
        <div className="pure-u-1-5 l-box">
            <div className="name">{labels.maxUnrelatedPlacement}</div>
            <div className="value">{record.maxUnrelatedPlacements}</div>
        </div>
    </div>
);
SummaryRow.propTypes = {
    labels: PropTypes.object.isRequired,
    record: PropTypes.object.isRequired
};

export default memo(SummaryRow);
