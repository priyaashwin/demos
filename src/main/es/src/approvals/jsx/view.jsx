'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Registrations from './registrations';
import InfoPop from '../../infopop/infopop';

const isSubjectFosterCarer = (personTypes = []) => personTypes.some(type => type === 'FOSTER_CARER');

const WarningMessage = ({ longText, shortText }) => (
    <InfoPop key="missingDemographic" content={longText} className="help-icon missing-demographic" align="bottom">
        <span className="info-icon">
            <i className="fa fa-question-circle" aria-hidden={true} />
            {shortText}
        </span>
    </InfoPop>
);

WarningMessage.propTypes = {
    longText: PropTypes.string,
    shortText: PropTypes.string
};

export default class ApprovalsView extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        url: PropTypes.string.isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectIdentifier: PropTypes.string,
            subjectName: PropTypes.string,
            subjectType: PropTypes.string
        }),
        permissions: PropTypes.shape({
            canAddCarerApproval: PropTypes.bool,
            canViewCarerApproval: PropTypes.bool,
            canUpdateCarerApproval: PropTypes.bool,
            canRemoveCarerApproval: PropTypes.bool,
            canEndFosterCarerRegistration: PropTypes.bool,
            canEditFosterCarerRegistration: PropTypes.bool,
            canEditFosterApprovalData: PropTypes.bool
        }),
        codedEntries: PropTypes.object.isRequired,
        enums: PropTypes.object.isRequired,
        subjectDetails: PropTypes.object
    };
    loadData = () => {
        //expose load data method to allow calling component to trigger a re-load of state
        //calls into the HOC to perform this task
        this.registrations.loadData();
    };
    getWarnings(isFosterCarer) {
        const { subjectDetails = {}, permissions, labels } = this.props;
        const { canAddCarerApproval } = permissions;

        if (isFosterCarer && canAddCarerApproval) {
            const hasDemographicsForChildCarer = subjectDetails.hasDemographicsForChildCarer;
            const dateOfBirthEstimated = subjectDetails.dateOfBirthEstimated;

            if (!hasDemographicsForChildCarer) {
                return (
                    <WarningMessage
                        longText={labels.missingDemographicLong}
                        shortText={labels.missingDemographicShort}
                    />
                );
            } else if (dateOfBirthEstimated) {
                return (
                    <WarningMessage
                        longText={labels.estimatedDOBExceptionLong}
                        shortText={labels.estimatedDOBExceptionShort}
                    />
                );
            }
        }
        return false;
    }
    registrationsRef = ref => (this.registrations = ref);
    render() {
        const { subjectDetails, ...other } = this.props;
        const isFosterCarer = isSubjectFosterCarer(subjectDetails.personTypes || []);
        return (
            <div className="approvals">
                {this.getWarnings(isFosterCarer)}
                <Registrations
                    {...other}
                    ref={this.registrationsRef}
                    isFosterCarer={isFosterCarer}
                    subjectDetails={subjectDetails}
                />
            </div>
        );
    }
}
