'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import memoize from 'memoize-one';
import PropTypes from 'prop-types';
import endpoint from '../js/cfg';
import Table from '../../table/jsx/table';
import { withDataLoad } from '../../model/hoc/withDataLoad';
import { date } from '../../table/js/formatter';
import { formatCodedEntry } from '../../codedEntry/codedEntry';
import Approvals from './approvals';
import TableActions from '../../table/jsx/actions/actions';

export default withDataLoad(
    class Registrations extends PureComponent {
        static propTypes = {
            labels: PropTypes.shape({
                addApproval: PropTypes.string,
                addApprovalRegistration: PropTypes.string,
                removeApprovalRegistration: PropTypes.string,
                registrationTable: PropTypes.object,
                approvalsTable: PropTypes.object
            }),
            results: PropTypes.array,
            loading: PropTypes.bool,
            error: PropTypes.object,
            codedEntries: PropTypes.shape({
                deregistrationReason: PropTypes.array
            }),
            enums: PropTypes.shape({
                genderPermitted: PropTypes.array
            }),
            permissions: PropTypes.shape({
                canAddCarerApproval: PropTypes.bool,
                canEndFosterCarerRegistration: PropTypes.bool,
                canEditFosterCarerRegistration: PropTypes.bool,
                canRemoveFosterCarerRegistration: PropTypes.bool
            }).isRequired,
            isFosterCarer: PropTypes.bool.isRequired,
            subjectDetails: PropTypes.object
        };

        getInitialSelectedApproval = () => {
            const { results = [] } = this.props;
            if (results.length) {
                const approvals = results[0].approvals;
                if (approvals.length) {
                    return approvals[0];
                }
            }

            return undefined;
        };
        state = {
            selectedApprovalId: undefined
        };

        componentDidUpdate(prevProps) {
            if (this.props.results !== prevProps.results) {
                //results have changed or just loaded
                if (this.state.selectedApprovalId === undefined || this.state.selectedApprovalId === null) {
                    const selectedApproval = this.getInitialSelectedApproval();
                    if (selectedApproval) {
                        //fire selection of first approval
                        this.handleOnSelectApprovalRow(selectedApproval);
                    }
                }
            }
        }
        handleOnSynchronisedPartnerClick = synchronisedPartner => {
            this.fireEvent('viewPartnerDetails', { personId: synchronisedPartner.id });
        };
        handleOnSelectApprovalRow = row => {
            //set selected approval id - then fire event
            this.setState({ selectedApprovalId: row.id }, () =>
                this.fireEvent('selectedRecordChange', { record: row })
            );
        };
        handleApprovalRowActionClick = (action, approval, registration) => {
            this.fireEvent(action, { record: approval, registration });
        };
        handleRowActionClick = (action, registration) => {
            this.fireEvent(action, { record: registration });
        };
        fireEvent(eventType, payload) {
            //create custom event and fire
            const event = new CustomEvent(eventType, {
                bubbles: true,
                cancelable: true,
                detail: payload
            });
            ReactDOM.findDOMNode(this).dispatchEvent(event);
        }
        getColumns = memoize((labels, codedEntries) => [
            {
                key: 'startDate',
                label: labels.startDate,
                width: '14%',
                formatter: date
            },
            {
                key: 'endDate',
                label: labels.endDate,
                width: '14%',
                formatter: date
            },
            {
                key: 'endReason',
                label: labels.endReason,
                width: '25%',
                formatter: endReason => formatCodedEntry(codedEntries.deregistrationReason, endReason)
            },
            {
                key: 'organisation',
                label: labels.approvingOrganisation,
                width: '25%',
                formatter: organisation => {
                    if (!organisation) {
                        return '';
                    }
                    return `${organisation.name} (${organisation.organisationIdentifier})`;
                }
            },
            {
                key: 'maxAvailablePlaces',
                label: labels.maxPlaces,
                width: '7%'
            },
            {
                key: 'maxAvailableRooms',
                label: labels.maxRooms,
                width: '7%'
            },
            {
                key: 'id',
                label: labels.actions,
                width: '8%',
                formatter: (id, row) => (
                    <TableActions
                        key={`actions_${id}`}
                        actions={this.getActions(row)}
                        handleClick={this.handleRowActionClick}
                        record={row}
                    />
                )
            }
        ]);
        getActions(registration) {
            const { permissions = {}, labels } = this.props;
            const { registrationTable: registrationTableLabels } = labels;
            const { canEndFosterCarerRegistration, canAddCarerApproval, canEditFosterCarerRegistration } = permissions;
            const actions = [];
            if (
                canAddCarerApproval &&
                (!registration.endDate || registration.endDate > Date.now()) &&
                this.canAddApproval()
            ) {
                actions.push({
                    id: 'addCarerApproval',
                    title: labels.addApproval,
                    label: labels.addApproval
                });
            }

            if (canEditFosterCarerRegistration) {
                actions.push({
                    id: 'editFosterCarerRegistration',
                    title: registrationTableLabels.editFosterCarerRegistrationTitle,
                    label: registrationTableLabels.editFosterCarerRegistration
                });
            }

            if (canEndFosterCarerRegistration) {
                //can only end if not already ended
                if (!registration.endDate) {
                    actions.push({
                        id: 'endFosterCarerRegistration',
                        title: registrationTableLabels.endFosterCarerRegistrationTitle,
                        label: registrationTableLabels.endFosterCarerRegistration
                    });
                }
            }
            return actions;
        }
        getSummaryRowConfig = memoize((labels, enums, selectedApprovalId) => ({
            enabled: true,
            formatter: registration => (
                <Approvals
                    results={registration.approvals}
                    labels={labels}
                    enums={enums}
                    onSynchronisedPartnerClick={this.handleOnSynchronisedPartnerClick}
                    onSelectRow={this.handleOnSelectApprovalRow}
                    selectedApprovalId={selectedApprovalId}
                    title={`Approvals - Approving organisation ${registration.organisation.name} (${registration.organisation.organisationIdentifier})`}
                    permissions={this.props.permissions}
                    onActionClick={this.handleApprovalRowActionClick}
                    registration={registration}
                    isRegistrationClosed={!!registration.endDate}
                />
            )
        }));
        canAddApproval() {
            const { isFosterCarer, subjectDetails, permissions } = this.props;
            const { canAddCarerApproval } = permissions;

            if (isFosterCarer && canAddCarerApproval) {
                return subjectDetails.hasDemographicsForChildCarer && !subjectDetails.dateOfBirthEstimated;
            }
            return false;
        }
        canRemoveRegistration() {
            const { isFosterCarer, permissions } = this.props;
            const { canRemoveFosterCarerRegistration } = permissions;

            if (isFosterCarer && canRemoveFosterCarerRegistration) {
                return true;
            }
            return false;
        }
        renderRegistrations() {
            const { results = [], loading, labels = {}, codedEntries = {}, enums = {} } = this.props;

            const { selectedApprovalId } = this.state;
            const { registrationTable, approvalsTable } = labels;

            const columns = this.getColumns(registrationTable, codedEntries);

            return (
                <Table
                    key="results-table"
                    columns={columns}
                    data={results}
                    loading={loading}
                    summaryRow={this.getSummaryRowConfig(approvalsTable, enums, selectedApprovalId)}
                    initiallyExpanded={true}
                    emptyMessage={registrationTable.emptyMessage}
                />
            );
        }
        handleAddApproval = e => {
            e.preventDefault();
            this.fireEvent('addCarerApproval', {});
        };
        handleRemoveApprovalRegistration = () => {
            const { results = [] } = this.props;
            this.fireEvent('prepareRemoveApprovalRegistration', {record: results[0]});
        };
        showAddButton() {
            const { error, loading, results } = this.props;
            if (loading || error) {
                return false;
            }
            const now = Date.now();
            //if there are no results - or no open registrations - then the add button could be shown
            if (
                results.length === 0 ||
                results.filter(registration => registration.endDate === null || registration.endDate > now).length === 0
            ) {
                if (this.canAddApproval()) {
                    return true;
                }
            }
            return false;
        }
       showRemoveButton() {
            const { error, loading, results } = this.props;
            if (loading || error) {
                return false;
            }
            //if there is a registration - then the remove button could be shown
            return results.length !==0 && this.canRemoveRegistration();
        }        
        render() {
            const { error, labels } = this.props;
            let addButton;
            let removeButton;
            if (error) {
                return 'Unable to load.';
            } else {
                if (this.showAddButton()) {
                    addButton = (
                        <button
                            href="#none"
                            className="registration-add-button pure-button pure-button-active"
                            title={labels.addApprovalRegistration}
                            onClick={this.handleAddApproval}>
                            <i className="fa fa-plus-circle"></i>
                            <span>{labels.addApprovalRegistration}</span>
                        </button>
                    );
                }
                if (this.showRemoveButton()) {
                    removeButton = (
                        <button
                            href="#none"
                            className="registration-remove-button pure-button pure-button-active"
                            title={labels.removeApprovalRegistration}
                            onClick={this.handleRemoveApprovalRegistration}>
                            <i className="fa fa-minus-circle"></i>
                            <span>{labels.removeApprovalRegistration}</span>
                        </button>
                    );
                }
            }

            return (
                <>
                    {addButton}
                    {removeButton}
                    {this.renderRegistrations()}
                </>
            );
        }
    },
    endpoint.fosterCareRegistrationEndpoint
);
