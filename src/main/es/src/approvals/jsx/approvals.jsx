'use strict';
import React, { PureComponent } from 'react';
import memoize from 'memoize-one';
import PropTypes from 'prop-types';
import Table from '../../table/jsx/table';
import { date } from '../../table/js/formatter';
import SummaryRow from './carerDetailsSummaryRow';
import TableActions from '../../table/jsx/actions/actions';

export default class Approvals extends PureComponent {
    static propTypes = {
        labels: PropTypes.object,
        results: PropTypes.array,
        enums: PropTypes.shape({
            genderPermitted: PropTypes.array
        }),
        title: PropTypes.string,
        selectedApprovalId: PropTypes.any,
        onSynchronisedPartnerClick: PropTypes.func.isRequired,
        onSelectRow: PropTypes.func.isRequired,
        permissions: PropTypes.shape({
            canViewCarerApproval: PropTypes.bool,
            canUpdateCarerApproval: PropTypes.bool,
            canRemoveCarerApproval: PropTypes.bool,
            canEditFosterApprovalData: PropTypes.bool
        }).isRequired,
        onActionClick: PropTypes.func.isRequired,
        isRegistrationClosed: PropTypes.bool.isRequired,
        onSort: PropTypes.func.isRequired,
        registration: PropTypes.object.isRequired
    };
    state = {
        sortBy: [
            {
                field: 'startDate',
                order: -1
            },
            {
                field: 'endDate',
                order: -1
            }
        ]
    };
    getColumns = memoize((labels, enums = {}) => [
        {
            key: 'startDate',
            label: labels.startDate,
            width: '15%',
            formatter: (value, row, column) => (
                <a
                    key={`date-${row.id}`}
                    href="#none"
                    onClick={() => this.props.onActionClick('viewCarerApproval', row)}
                    title="View approval">
                    {date(value, row, column)}
                </a>
            ),
            sortable: true
        },
        {
            key: 'endDate',
            label: labels.endDate,
            width: '15%',
            formatter: date,
            sortable: true
        },
        {
            key: 'genderPermitted',
            label: labels.genderPermitted,
            width: '10%',
            formatter: genderPermitted => {
                const enumValue = enums.genderPermitted.filter(value => value.enumValue === genderPermitted)[0];
                return enumValue ? enumValue.name : '';
            },
            sortable: true
        },
        {
            key: 'typeOfCareAssignmentVO',
            label: labels.careType,
            width: '26%',
            formatter: typeOfCareAssignmentVO => typeOfCareAssignmentVO.codedEntryVO.name
        },
        {
            key: 'synchronisedPartner',
            label: labels.synchronisedPartner,
            formatter: (synchronisedPartner, row) => {
                if (!synchronisedPartner) {
                    return '';
                }
                return (
                    <a
                        key={`partner-${row.id}`}
                        href="#none"
                        className="switch-focus"
                        onClick={() => this.props.onSynchronisedPartnerClick(synchronisedPartner)}
                        title={`View record for ${synchronisedPartner.name}`}>
                        {synchronisedPartner.name}
                    </a>
                );
            },
            width: '26%'
        },
        {
            key: 'id',
            label: labels.actions,
            width: '8%',
            formatter: (id, approval) => (
                <TableActions
                    key={`actions_${id}`}
                    actions={this.getActions(approval)}
                    handleClick={this.handleActionClick}
                    record={approval}
                />
            )
        }
    ]);
    getActions(approval) {
        const { permissions = {}, labels, isRegistrationClosed } = this.props;
        const { canViewCarerApproval, canUpdateCarerApproval, canRemoveCarerApproval, canEditFosterApprovalData } = permissions;
        const actions = [];

        if (canViewCarerApproval) {
            actions.push({
                id: 'viewCarerApproval',
                title: labels.viewApprovalTitle,
                label: labels.viewApproval
            });
        }

        if (canUpdateCarerApproval && !isRegistrationClosed) {
            actions.push({
                id: 'updateCarerApproval',
                title: labels.editApprovalTitle,
                label: labels.editApproval
            });
        }
        //need permissions and must be a FosterCarerApproval record to enable the 
        if(canEditFosterApprovalData && approval._type==='FosterCarerApproval'){
            actions.push({
                id: 'editFosterApprovalData',
                title: labels.editFosterApprovalDataTitle,
                label: labels.editFosterApprovalData
            });
        }

        if (canRemoveCarerApproval) {
            actions.push({
                id: 'prepareRemoveFosterCarerApproval',
                title: labels.removeApprovalTitle,
                label: labels.removeApproval
            });
        }

        return actions;
    }
    getSelectedRowIndex = memoize((results = [], selectedApprovalId) => {
        if (!results.length || !selectedApprovalId) {
            return undefined;
        }

        const index = results.findIndex(r => r.id === selectedApprovalId);
        if (index < 0) {
            return undefined;
        }

        return index;
    });
    getSummaryRowConfig = memoize(labels => ({
        enabled: true,
        formatter: row => <SummaryRow labels={labels} record={row} key={row.id} />
    }));
    handleActionClick = (action, data) => {
        const { onActionClick, registration } = this.props;

        if (onActionClick) {
            onActionClick.call(null, action, data, registration);
        }
    };
    handleApprovalsSort = sortBy => {
        this.setState({ sortBy });
    };
    getSortedResults = memoize((sortBy = [], results) => {
        const compare = (a, b) => {
            let result = 0;
            const sortLength = sortBy.length;
            for (let i = 0; !result && i < sortLength; i++) {
                const field = sortBy[i].field || '';
                const order = sortBy[i].order || 0;

                let aVal = a[field] || '';
                let bVal = b[field] || '';

                if (typeof aVal === 'string' && typeof bVal === 'string') {
                    aVal = aVal.toLowerCase();
                    bVal = bVal.toLowerCase();
                }

                if (typeof aVal === 'number' || typeof bVal === 'number') {
                    //sort null number to high
                    if (aVal === '') {
                        aVal = Number.MAX_SAFE_INTEGER;
                    }
                    if (bVal === '') {
                        bVal = Number.MAX_SAFE_INTEGER;
                    }
                }
                result = aVal > bVal ? order : aVal < bVal ? -order : 0;
            }
            return result;
        };

        return [...results].sort(compare);
    });
    render() {
        const { results = [], labels = {}, enums = {}, onSelectRow, selectedApprovalId, title } = this.props;

        const { sortBy } = this.state;

        const columns = this.getColumns(labels, enums);

        const selectedRowIndex = this.getSelectedRowIndex(results, selectedApprovalId);
        return (
            <div className="approvals-sub-table">
                <h3 className="approvals-title">{title}</h3>
                <Table
                    key="approvals-results-table"
                    columns={columns}
                    data={this.getSortedResults(sortBy, results)}
                    summaryRow={this.getSummaryRowConfig(labels)}
                    initiallyExpanded={true}
                    selectedRowIndex={selectedRowIndex}
                    onSelectRow={onSelectRow}
                    onSort={this.handleApprovalsSort}
                    sortBy={sortBy}
                />
            </div>
        );
    }
}
