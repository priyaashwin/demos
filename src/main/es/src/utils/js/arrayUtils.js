import memoize from 'memoize-one';

export const convertToObj = memoize((array, key='id')=>{
  return array.reduce((acc, object) => {
    acc[object[key]] = {
      ...object
    };
    return acc;
  }, {});
});