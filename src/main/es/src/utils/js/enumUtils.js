/**
 * Function to find a matching enum value in an enum category 
 * 
 * @param {*} enumCategory 
 * @param {*} enumValue 
 * @param {*} attribute 
 */
export const getEnumAttribute = function (enumCategory, enumValue, attribute) {
    const enumCategoryValues = enumCategory.values;
    const matchedEnum = enumCategoryValues.find(value => value.enumValue === enumValue);

    return (matchedEnum)? matchedEnum[attribute] : null;
};