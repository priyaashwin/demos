export const isSpecified = function (val) {
    return (val !== null && val !== undefined && !(typeof val === 'string' && val.trim() === ''));
};

export const cleanValue = function (value) {
    let v;
    if (!(null === value || undefined === value)) {
        if (typeof value === 'string') {
            v = value.trim();
        } else {
            v = value;
        }
    }
    return v;
};