'use strict';

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import endpoint from '../js/cfg';
import ErrorMessage from '../../error/error';
import Loading from '../../loading/loading';
import ModelList from '../../model/modelList';
import PaginatedResultsList from '../../results/paginatedResultsList';
import AdoptionInformationsView from './adoptionInformation/view';

export default class AdoptionInformationView extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectIdentifier: PropTypes.string,
      subjectName: PropTypes.string,
      subjectType: PropTypes.string
    }),
    permissions : PropTypes.object.isRequired,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired
  }

  state={
    data: null,
    errors: null,
    loading: true
  };

  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({errors:null});
  }
  componentDidUpdate(prevProps) {
    if (!(prevProps.url === this.props.url && prevProps.subject === this.props.subject)) {
      this.loadData();
    }
  }
  componentDidMount(){
    this.loadData();
  }
  loadData(){
    const {subject, url}= this.props;

    new PaginatedResultsList().load({
        ...endpoint.adoptionInformationEndpoint,
        url: new ModelList(url).getURL({
          subjectId: subject.subjectId
        })
      }, -1, 1).then(result=>this.setState({
          data: result.results,
          loading: false,
          errors: null
        })).catch(reject=>this.setState({
          errors: reject,
          loading: false,
          data: null
        }));
  }
  renderColumns() {
    const {labels, subject, permissions, codedEntries} = this.props;
    const {data, errors, loading} = this.state;
    return (
      <div className="child-looked-after-content">
        <AdoptionInformationsView
          errors={errors}
          labels={labels}
          loading={loading}
          adoptionInformation={data}
          subject={subject}
          permissions={permissions}
          codedEntries={codedEntries}
        />
      </div>
    );
  }
  render(){
    const {errors, loading} = this.state;
    let content;

    if(loading) {
      content = (<Loading />);
    } else if(errors) {
      content = (<ErrorMessage errors={errors} onClose={this.clearErrors} />);
    } else {
      content = this.renderColumns();
    }

    return (<div className="child-looked-after">{content}</div>);
  }
}
