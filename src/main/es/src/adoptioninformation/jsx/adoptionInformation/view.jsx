'use strict';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Accordion from 'usp-accordion-panel';
import AdoptionInformationTable from './adoptionInformationTable';
import postal from 'postal';

export default class AdoptionInformationsView extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    adoptionInformation : PropTypes.array,
    permissions: PropTypes.object.isRequired,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired
  }
  componentDidMount(){
    this.subscription = postal.subscribe({
      channel: 'cla',
      topic: 'buttonClick',
      callback: function(data) {
          this.handleClick(data.action, data);
      }
    }).context(this);
  }
  handleClick = (action, data) => {
    let event;
    switch(action) {
      case('viewAdoptionInformation'):
        event=new CustomEvent(action, {
          bubbles: true,
          cancelable: true,
          detail:{
            adoptionInformationId: data.id
          }
         });
        break;
      case('editAdoptionInformation'):
        event=new CustomEvent(action, {
          bubbles:true,
          cancelable: true,
          detail:{
            adoptionInformationId: data.id
          }
         });
        break;
    }

    ReactDOM.findDOMNode(this).dispatchEvent(event);
  }

  renderAccordions() {
    const { labels, permissions, codedEntries, adoptionInformation } = this.props;
    return (
        <div>
          {
             <Accordion expanded={true} title={labels.accordionTitle}>
               <AdoptionInformationTable
                 labels={labels.adoptionInformationTable}
                 codedEntries={codedEntries}
                 handleClick={this.handleClick}
                 permissions={permissions}
                 adoptionInformationRecords={adoptionInformation}
               />
             </Accordion>
          }
        </div>
    );
  }
  render(){
    const {adoptionInformation, labels} = this.props;
    let content;

    if(adoptionInformation && adoptionInformation.length > 0) {
      content = this.renderAccordions();
    } else {
      content = (<div>{labels.adoptionInformationTable.noData}</div>);
    }

    return (
      <div className="periods-of-care">
        {content}
      </div>
    );
  }
}
