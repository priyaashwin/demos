'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Formatters from '../../../childlookedafter/js/formatters';
import { formatCodedEntry } from '../../../codedEntry/codedEntry';
import TableActions from '../../../table/jsx/actions/actions';

export default class AdoptionInformationRow extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    permissions: PropTypes.object.isRequired,
    record: PropTypes.object,
    handleClick: PropTypes.func,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired,
    adoptionInformation: PropTypes.object.isRequired    
  }
  getActions() {
    const { labels, permissions } = this.props;
    return [{
      id: 'viewAdoptionInformation',
      label: labels.view,
      disabled: !permissions.canViewAdoptionInformation
    },{
      id: 'editAdoptionInformation',
      label: labels.edit,
      disabled: !permissions.canUpdateAdoptionInformation
    }];
  }
  render() {
    const { labels, codedEntries, adoptionInformation, handleClick, permissions } = this.props;

    return (
      <tr className={'adoptionInformation-row'}>
        <td data-title={labels.dateShouldBePlaced}>{Formatters.date('dateShouldBePlaced', adoptionInformation)}</td>
        <td data-title={labels.dateMatched}>{Formatters.date('dateMatched', adoptionInformation)}</td>
        <td data-title={labels.datePlaced}>{Formatters.date('datePlaced', adoptionInformation)}</td>
        <td data-title={labels.legalStatusOfAdopters}>{formatCodedEntry(codedEntries.adopterLegalStatus, adoptionInformation.legalStatusOfAdopters)}</td>
        <td className="cla-table-col-actions">
          <TableActions
            key={adoptionInformation.id}
            actions={this.getActions()}
            handleClick={handleClick}
            permissions={permissions}
            record={adoptionInformation}
          />
        </td>
      </tr>
    );
  }
}
