'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AdoptionInformationRow from './adoptionInformationRow';

export default class AdoptionInformationTable extends Component {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    handleClick: PropTypes.func.isRequired,
    permissions: PropTypes.object.isRequired,
    codedEntries: PropTypes.shape({
      adopterLegalStatus: PropTypes.object.isRequired
    }).isRequired,
    adoptionInformationRecords: PropTypes.object.isRequired
  }

  initTableData(records) {
    const { labels, codedEntries, handleClick, permissions  } = this.props;
    const emptyRow = (<tr><td className="no-data" colSpan="5">{labels.noData}</td></tr>);

    if(!records.length) return emptyRow;
    return records.map((record, i) => {
      const key = `index-${i}-id-${record.id}`;

      return (
        <AdoptionInformationRow
          labels={labels}
          adoptionInformation={record}
          codedEntries={codedEntries}
          handleClick={handleClick}
          permissions={permissions}
          key={key}
        />
      );
    });
  }
  render() {
    const { labels, adoptionInformationRecords } = this.props;
    return (
      <div className="cla-table cla-child-table pure-table pure-table-responsive">
        <table className="cla-adoptionInformation-table pure-table-table">
          <thead className="cla-table-columns">
            <tr>
              <th>{labels.dateShouldBePlaced}</th>
              <th>{labels.dateMatched}</th>
              <th>{labels.datePlaced}</th>
              <th>{labels.legalStatusOfAdopters}</th>
              <th>{labels.action}</th>
            </tr>
          </thead>
          <tbody className="cla-table-data">
            {this.initTableData(adoptionInformationRecords)}
          </tbody>
        </table>
      </div>
    );
  }
}
