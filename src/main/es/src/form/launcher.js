import subs from 'subs';

/**
 * Opens a form
 * @method open
 * @param {Number} id the id of the form to open
 * @param {String} formName the name of the form type
 * @param {String} controlId optional localId of a control to deep link to
 */
const launch=function(formURL, id, formName, memberId, controlId){
  let url=formURL;
  if(memberId){
      url=url+'&memberId='+memberId;
  }
  if (controlId){
      url=url+'&controlId='+controlId;
  }
  window.open(
      subs(url,{
        id:id
      }),
      formName+'_'+id,
      'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1024,height=768,left=100,top=100'
  );
};

export {launch};
