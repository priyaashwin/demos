'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'usp-dropzone';

const Dustbin = ({canHandleControlDrop, onDrop}) => {
    const canHandleEvent=(transferData) => {
        const types = transferData.types;
        let canHandle = false;

        if (types.length === 1) {
            //check if type
            if (types[0].toLowerCase() === 'text' || types[0].toLowerCase() === 'text/plain') {
                //call into parent to check if control is handled
                canHandle = canHandleControlDrop.call(null);
            }
        }

        return canHandle;
    };

    return (
      <Dropzone
        effect="move"
        canHandleEvent={canHandleEvent}
        onDrop={onDrop}
        enabled={true}
        dropPos={0}>
        <span className="pull-right">
          <span>Drop mapping here to delete</span>{' '}<i className="fa fa-trash icon-trash"/>
        </span>
      </Dropzone>
    );
};

Dustbin.propTypes= {
    canHandleControlDrop: PropTypes.func.isRequired,
    onDrop: PropTypes.func.isRequired
};

export default Dustbin;
