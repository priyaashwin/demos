'use strict';

import React, {PureComponent} from 'react';

export default class Mask extends PureComponent {
    constructor(props) {
        super(props);

        //initial state
        this.state = {
            shown: false
        };
    }

    componentDidMount() {
        //only show the mask after 150ms
        this._timeout = window.setTimeout(this.show.bind(this), 150);
    }
    componentWillUnmount() {
        // Clear the timeout if(present)
        window.clearTimeout(this._timeout);
    }

    show() {
        this.setState({shown: true});
    }

    render() {
        var divStyle = {
            'position': 'fixed',
            'zIndex': 99999,
            'top': 0,
            'left': 0,
            'width': '100%',
            'height': '100%'
        };
        if (this.state.shown) {
            return (<div style={divStyle} className="panel-loading-mask"/>);
        } else {
            return (<div/>);
        }
    }
}
