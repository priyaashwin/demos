'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import ControlType from './controlType';

const ControlInfo=({control})=>{
    let label;
    if (control.label && !control.label.hidden) {
        label = control.label.value;
    }
    if (!label) {
        label = control.name || 'unknown control';
    }

  return (
    <span className="control">
      <ControlType control={control}/>
      <span className="control-label">{label}</span>
    </span>
  );
};

ControlInfo.propTypes = {
    control: PropTypes.object.isRequired
};

export default ControlInfo;
