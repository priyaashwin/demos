'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import MappedControl from './mappedControl';
import Draggable from './draggable';
import DraggableControl from './draggableControl';
import ControlInfo from './controlInfo';

export default class SourceControl extends DraggableControl {
    static propTypes = {
        control: PropTypes.object.isRequired,
        onDragStart: PropTypes.func.isRequired,
        mappedControls: PropTypes.array,
        viewOnly: PropTypes.bool.isRequired
    };
    
    handleDragStart=()=>{
        //call into the parent component to keep a track of the control
        this.props.onDragStart(this.props.control);
    };

    render() {
        const mappedControls = this.props.mappedControls;
        const isViewOnly = this.props.viewOnly;

        let mappedControlInfo;

        if (mappedControls && mappedControls.length > 0) {
            //output info about the mapped control
            mappedControlInfo = mappedControls.map(function(control) {
              return (<MappedControl
                key={control.id}
                colour={control.colour}
                control={control}
                outputControl={control}
                onDrag={this.handleMappedControlDragStart}
                onClick={this.handleMappedControlClick}
                viewOnly={isViewOnly}/>);
            }, this);
        }
        return (
          <div>
            <Draggable
              effect="copy"
              enabled={!isViewOnly}
              onDrag={this.handleDragStart}
              payload={this.props.control.localId}>
              <ControlInfo control={this.props.control}/>
            </Draggable>
            {mappedControlInfo}
          </div>
        );
    }
}
