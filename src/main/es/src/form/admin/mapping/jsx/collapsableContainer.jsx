'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {CSSTransition, TransitionGroup} from 'react-transition-group';

const transitionTime={ enter: 300, exit: 300};

export default class CollapsibleContainer extends PureComponent {
  constructor(props) {
      super(props);

      //initial state
      this.state = {
          expanded: this.props.expanded!==undefined?props.expanded:true
      };

      this.toggle=this.toggle.bind(this);
    }

    toggle(){
        this.setState({
            expanded:!this.state.expanded
        });
    }

    render(){
        let content;
        let link;

        if(this.state.expanded){
            content=(
                <CSSTransition key="content_trans" classNames="collapse" timeout={transitionTime} in={this.state.expanded}>
                    <div key="content">
                        {this.props.children}
                    </div>
            </CSSTransition>);

            link=this.props.hideLabel;
        }else{
            link=this.props.showLabel;
        }

        return(
            <div>
                <TransitionGroup component="div">
                  {content}
                </TransitionGroup>
                <div onClick={this.toggle}>
                    {link}
                </div>
            </div>
        );
    }
}

CollapsibleContainer.propTypes = {
  expanded: PropTypes.bool,
  hideLabel:PropTypes.node.isRequired,
  showLabel:PropTypes.node.isRequired,
  children: PropTypes.node
};
