'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import MappedControl from './mappedControl';
import DropZone from 'usp-dropzone';
import DraggableControl from './draggableControl';
import ControlInfo from './controlInfo';

export default class DestinationControl extends DraggableControl {
    static propTypes = {
        control: PropTypes.object.isRequired,
        canHandleSourceControlDrop: PropTypes.func.isRequired,
        handleSourceControlDrop: PropTypes.func.isRequired,
        mappedControl: PropTypes.object,
        //click handler for mapped control
        viewOnly: PropTypes.bool.isRequired
    };
    
    canHandleEvent=(transferData)=>{
        const types = transferData.types;

        let canHandle = false;

        if (types.length === 1) {
            //check if type
            if (types[0].toLowerCase() === 'text' || types[0].toLowerCase() === 'text/plain') {
                //call into parent to check if control is handled
                canHandle = this.props.canHandleSourceControlDrop(this.props.control);
            }
        }

        return canHandle;
    };
    
    handleDrop=()=>{
        //call into parent for the drop
        this.props.handleSourceControlDrop(this.props.control);
    };

    render() {
        const mappedControl = this.props.mappedControl;

        const isViewOnly = this.props.viewOnly;

        let mappedControlInfo;
        if (mappedControl) {
          mappedControlInfo = (<MappedControl
            colour={mappedControl.colour}
            control={this.props.control}
            outputControl={mappedControl}
            onDrag={this.handleMappedControlDragStart}
            onClick={this.handleMappedControlClick}
            viewOnly={isViewOnly}/>);
        }
        return (
          <DropZone
            effect="copy"
            canHandleEvent={this.canHandleEvent}
            onDrop={this.handleDrop}
            enabled={!isViewOnly}
            dropPos={0}>
            <ControlInfo control={this.props.control}/>
            {mappedControlInfo}
          </DropZone>
        );
    }
}
