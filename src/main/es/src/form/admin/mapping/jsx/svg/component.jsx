'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const SVGComponent = (props) => (
  <svg {...props}>
    {props.children}
  </svg>
);

SVGComponent.propTypes = {
    children: PropTypes.node
};

export default SVGComponent;
