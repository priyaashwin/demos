'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
/**
 * Renders an indicator to show if a control is group or individual scope
 */

export default class ControlType extends PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        const control = this.props.control;
        const type = control.subType;
        let individualScope;
        let indicator;

        /**
          * NOTE CONTROLS WITHIN GROUPS ARE CONTROLLED BY THE GROUPS SCOPE ON THE COLLECTION
          * -
          * AS WE DON'T MAP COLLECTION CONTROLS, THERE IS NO LOGIC IN HERE TO HANDLE THOSE.
          * -
          * THAT MAY CHANGE!!
          */
        //some controls i.e. static  can ONLY be group, never individual!
        //some controls i.e. dynamic can ONLY be individual, never group!
        switch (control._type) {
            case 'TextControlFullDetails':
                switch (type) {
                    case 'STATIC_TEXT':
                        //Static controls are ALWAYS group scope
                        individualScope = false;
                        break;
                    default:
                        //use the setting in the control
                        individualScope = !control.groupScope;
                }
                break;
            case 'StaticMediaControlFullDetails':
                /* falls through */
            case 'AlertControlFullDetails':
                /* falls through */
            case 'PanelControlFullDetails':
                //Media, alert & panel are ALWAYS group scope
                individualScope = false;
                break;
            case 'SuppliedControlFullDetails':
                //dynamic data are ALWAYS individual
                individualScope = true;
                break;
            default:
                //use the setting in the control
                individualScope = !control.groupScope;
        }

        if (individualScope) {
            indicator = (
                <i className="fa fa-user icon-user" />
            );
        } else {
            indicator = (
                <i className="fa fa-users icon-group" />
            );
        }

        return (
            <span>{indicator}</span>
        );
    }
}

ControlType.propTypes = {
    control: PropTypes.object.isRequired
};
