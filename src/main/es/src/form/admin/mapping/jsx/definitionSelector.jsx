'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Map} from 'immutable';
import ShowPreviousSelector from './previousVerSelector';

export default class DefinitionSelector extends PureComponent {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleShowPreviousChange = this.handleShowPreviousChange.bind(this);

        //initial state
        this.state = {
            showPrevious: false
        };
    }
    handleShowPreviousChange(val) {
        this.setState({showPrevious: val});
    }
    handleChange(e) {
        //call into parent
        this.props.onSelectedDefinitionChange(e.target.value);
    }
    getLabel(definition) {
        const label = [];
        const mappings = this.props.mappings;
        const mappedControls=mappings.get(definition.id);
        const len = mappedControls ? mappedControls.toArray().filter(x => !!x).length : 0;

        label.push(definition.name);
        label.push('(version: ' + definition.version + ')');

        if (len > 0) {
            label.push('[' + len);
            label.push(len === 1
                ? 'mapping]'
                : 'mappings]');
        }
        return label.join(' ');
    }
    render() {
        const definitions = {};
        let definitionsList = [];
        const selectedDefinition = this.props.selectedDefinition || null;
        let selected;

        //if we only want current versions - we have to filter out old ones
        if (!this.state.showPrevious) {
            this.props.formDefinitions.forEach(formDefinition => {
                //check if this definition is the currently selected definition - we must display it in the list
                if (selectedDefinition == formDefinition.id) {
                    //remember this as the selected definition
                    selected = formDefinition;
                }

                if (definitions[formDefinition.formDefinitionId]) {
                    //check if newer version
                    if (formDefinition.version <= definitions[formDefinition.formDefinitionId].version) {
                        return;
                    }
                }

                definitions[formDefinition.formDefinitionId] = formDefinition;
            });

            //ensure the selected definition is added (this is only necessary id the selected version is older than the current version)
            if (selected) {
                if (definitions[selected.formDefinitionId] && definitions[selected.formDefinitionId].version > selected.version) {
                    //push into our definitions
                    definitions[selected] = selected;
                }
            }

            //now turn into an array
            definitionsList = Object.keys(definitions).map(function(k) {
                return definitions[k];
            });
        } else {
            definitionsList = this.props.formDefinitions;
        }

        //sort the list
        definitionsList.sort(function(a, b) {
            return a.name.localeCompare(b.name, 'en', {'sensitivity': 'base'});
        });

        const options = definitionsList.map(function(definition) {
            return (
                <option key={definition.id} value={definition.id}>{this.getLabel(definition)}</option>
            );
        }, this);

        //push a blank option
        options.unshift(
            <option key="blank" className="blank" value="">Please choose</option>
        );

        return (
          <div className="pure-g-r">
            <div className="pure-u-2-3 l-box">
              <label htmlFor="formDefinition">
                Source forms
              </label>
            </div>
            <div className="pure-u-1-3 l-box show-previous">
              <ShowPreviousSelector
                onShowPreviousChange={this.handleShowPreviousChange}
                showPrevious={this.state.showPrevious}/>
            </div>
            <div className="pure-u-1 l-box">
              <select
                id="formDefinition"
                className="pure-input-1"
                value={this.props.selectedDefinition}
                onChange={this.handleChange}>
                {options}
              </select>
            </div>
          </div>
        );
    }
}

DefinitionSelector.propTypes={
    //Array of available form definitions in the system
    formDefinitions: PropTypes.array.isRequired,
    //list of current mappings (this is used to output a count next to the definition)
    mappings: PropTypes.instanceOf(Map).isRequired,
    selectedDefinition: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.number.isRequired]),
    onSelectedDefinitionChange: PropTypes.func.isRequired
};
