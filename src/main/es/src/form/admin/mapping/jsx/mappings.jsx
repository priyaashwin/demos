'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Mask from './mask';
import SourceGraph from './sourceGraph';
import DefinitionSelector from './definitionSelector';
import Dustbin from './dustbin';
import PageTreeView from './pageTreeView';
import RColor from '../js/rcolor';
import {fromJS, Map} from 'immutable';

const rColour = new RColor();

const getInitialMappings = function (initialMappings){
    let mappings = Map();

    //if we have initial mappings - convert them into an object hash
    mappings=mappings.withMutations(mappings=>{
      (initialMappings||[]).forEach(mapping => {
          const sourceFormId = mapping.sourceFormId;
          const source = mapping.source;
          const destination = mapping.destination;
          if (sourceFormId !== undefined && source && destination) {
              //set the original source (will contain the crucial ID to allow us to match up on an update)
              mappings.setIn([sourceFormId, destination.id],fromJS(source));
              //update the mapping color
              mappings.setIn([sourceFormId, destination.id, 'colour'],rColour.get(true, 0.3, 0.99));
          }
      });
    });

    //return immutable object to represent mappings state
    return mappings;
};

export default class FormMappings extends PureComponent {
    static propTypes = {
        //hash of labels - TODO - use these
        labels: PropTypes.object.isRequired,
        //Current ID of source form definition
        formDefinitionId: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.number.isRequired]),
        //The form definition model - a load will be initiated in this once the component is mounted
        formDefinitionsModel: PropTypes.object.isRequired,
        //The form definition model - used to load the source full details of the form definition
        sourceFormDefinitionModel: PropTypes.object.isRequired,
        //The form definition model - used to load the destination full details of the form definition
        destinationFormDefinitionModel: PropTypes.object.isRequired,
        //The array of any existing control mappings - this will be set into our internal state
        initialMappings: PropTypes.array,
        //Controls the MODE of this component - view only doesn't allow drag'n drop
        viewOnly: PropTypes.bool.isRequired
    };

    state = {
        formDefinitions: [],
        sourcePageModel: {},
        destinationPageModel: {},
        //Hash containing the currently defined mappings - key is source form definition id, then destination controlId
        mappings: getInitialMappings(this.props.initialMappings),
        //should we mask the page
        mask: false,
        //the source control to focus
        focusSourceControlId: null,
        //the destination control to focus
        focusDestinationControlId: null
    };

    handleSelectedDefinitionChange=(val)=>{
        const sourcePageModel = this.state.sourcePageModel || {};
        if (val !== sourcePageModel.id) {
            //lookup the source
            this.setState({
                sourcePageModel: {},
                selectedDefinition: val,
                mask: !(val === undefined || val === ''),
                //the source control to focus
                focusSourceControlId: null,
                //the destination control to focus
                focusDestinationControlId: null
            });

            if (!(val === undefined || val === '')) {
                this.props.sourceFormDefinitionModel.load({
                    id: val
                }, function(err) {
                    //in ajax callback
                    if (err === null) {
                        this.setState({
                            sourcePageModel: this.pruneSourceModel(this.props.sourceFormDefinitionModel),
                            //ensure we update the current definition
                            selectedDefinition: this.props.sourceFormDefinitionModel.get('id')
                        });
                    }

                    this.setState({mask: false});
                }.bind(this));
            }
        }
    };
    pruneSourceModel(model) {
        return this.pruneModel(model.toJSON() || {}, function(control) {
            let source = false;

            //only certain controls are enabled as a source
            switch (control._type) {
                case 'TextControlFullDetails':
                    source = !(control.readOnly || control.hidden || 'PASSWORD' === control.subType);
                    break;
                case 'AttachedMediaControlFullDetails':
                case 'NumericControlFullDetails':
                case 'DateControlFullDetails':
                case 'SelectionControlFullDetails':
                case 'SearchInputControlFullDetails':
                    source = true;
                    break;
            }

            return source;
        });
    }

    pruneDestinationModel(model) {
        return this.pruneModel(model.toJSON() || {}, function(control) {
            let destination = false;

            //only certain controls are enabled as a destination
            switch (control._type) {
                case 'TextControlFullDetails':
                    destination = !(control.readOnly || control.hidden || 'PASSWORD' === control.subType);
                    break;
                case 'AttachedMediaControlFullDetails':
                case 'NumericControlFullDetails':
                case 'DateControlFullDetails':
                case 'SelectionControlFullDetails':
                case 'SearchInputControlFullDetails':
                    destination = true;
                    break;
            }

            return destination;
        });
    }
    pruneModel(data, filter) {
        var controls,
            sections,
            groups,
            pages = [],
            gScope = false,
            navigateRows = function(item) {
                //if this is a collection group scope is recorded on the collection not on the controls
                gScope = item.collectionType
                    ? item.groupScope
                    : false;
                controls = [];
                item.rows.forEach(function(row) {
                    row.columns.forEach(function(col) {
                        col.controls.forEach(function(control) {
                            if (filter(control)) {
                                //update the group scope (case when in collection)
                                control.groupScope = control.groupScope || gScope;
                                controls.push(control);
                            }
                        });
                    });
                });
                return controls;
            };

        //iterate the data and construct a simplified model
        (data.pages || []).forEach(function(page) {
            sections = [];
            page.sections.forEach(function(section) {
                groups = [];
                section.groups.forEach(group => {
                    controls = navigateRows(group);

                    //look for search input controls on the group
                    if(group.searchInputControl){
                      const control=group.searchInputControl;

                      if (filter(control)) {
                          //update the group scope (case when in collection)
                          control.groupScope =  group.groupScope || false;
                          controls.push(control);
                      }
                    }
                    //add a group only if we have controls to show
                    if (controls.length > 0) {
                        //push a reference to the group into the control
                        controls.forEach(control => control.group = {
                            localId: group.localId,
                            name: group.name,
                            collectionType: group.collectionType,
                            maxOccurrences: group.maxOccurrences,
                            resolverParameters: group.resolverParameters,
                            resolverValueId: group.resolverValueId
                        });

                        //TODO - we need to record additional information about the group for dynamic data lists
                        //we'll come back to that later
                        groups.push({
                            id: group.id,
                            localId: group.localId,
                            name: group.name,
                            //set the controls
                            controls: controls
                        });
                    }
                });
                controls = navigateRows(section);

                if (controls.length > 0 || groups.length > 0) {
                    //add a section only if we have controls/groups to show
                    sections.push({id: section.id, localId: section.localId, name: section.title, controls: controls, groups: groups});
                }
            });
            if (sections.length > 0) {
                //add to pages only if we have sections to show
                pages.push({id: page.id, localId: page.localId, name: page.title, sections: sections});
            }
        });

        return ({formDefinitionId: data.formDefinitionId, id: data.id, name: data.name, pages: pages, version: data.version});
    }

    componentDidMount() {
        /* eslint react/no-did-mount-set-state: 0 */
        //start loading that list of form definitions
        this.props.formDefinitionsModel.load({}, (err) => {
            //in ajax callback
            if (err === null) {
                this.setState({formDefinitions: this.props.formDefinitionsModel.toJSON()});
            }
        });

        this.props.destinationFormDefinitionModel.load({
            id: this.props.formDefinitionId
        }, (err) => {
            //in ajax callback
            if (err === null) {
                this.setState({
                    destinationPageModel: this.pruneDestinationModel(this.props.destinationFormDefinitionModel)
                });
            }
        });
    }

    /**
     * Handles the start of a drag operation on a control in the source tree
     */
    handleSourceControlDragStart=(control)=>{
        //update the state with our control
        this.setState({sourceControl: control, mappedControl: null, focusSourceControlId: null, focusDestinationControlId: null});
    };

    /**
     * Handles the start of a drag operation on a mapped control in the destination tree
     */
    handleMappedControlDragStart=(control)=>{
        //update the state with our control
        this.setState({sourceControl: null, mappedControl: control, focusSourceControlId: null, focusDestinationControlId: null});
    };

    /**
     * This checks that the destination control can accept the source.
     * NOTE - we have already removed any non compatible source/destination types from our drag/drop zones.
     * So we do not need to check for example static/hidden/password types. If this changes, then
     * these checks will also need to be included in this function.
     *
     * Remember this function executes for every tick on the drop operation, so needs to be as fast as possible.
     */
    canHandleSourceControlDrop=(destinationControl)=>{
        const source = this.state.sourceControl;
        const destination = destinationControl;

        let canHandle = false;
        if (source && destination) {
            //check groupScope is equal;
            if (source.groupScope === destination.groupScope) {
                //check collection (called group but don't confuse with groupScope!)
                //is equal (i.e. both null or undefined)
                //or must belong to a group
                if (source.group === destination.group || (source.group && destination.group)) {
                    let sourceValidation;
                    let destinationValidation;

                    //compare types
                    switch (source._type) {
                        case 'AttachedMediaControlFullDetails':
                            //Check that the destination is also an attached media control - done here so we can introduce cross type mapping if necessary
                            if (source._type === destination._type) {
                                //we can only may media fields to media fields
                                canHandle = (source.subType === destination.subType);
                            }
                            break;
                        case 'TextControlFullDetails':
                            //Check that the destination is also a TextControl - done here so we can introduce cross type mapping if necessary
                            if (source._type === destination._type) {
                                //extract the validation info
                                sourceValidation = source.validation;
                                destinationValidation = destination.validation;
                                switch (source.subType) {
                                    case 'TEXT':
                                        //determine if the destination can accept this dataType
                                        switch (destinationValidation.dataType || 'TEXT') {
                                            case 'TEXT':
                                                //Text can handle any other text type
                                                canHandle = true;
                                                break;
                                            default:
                                                //default is to match validation Type and length constraints
                                                canHandle = (sourceValidation.dataType === destinationValidation.dataType);
                                        }

                                        //ensure the length constraints are taken into account
                                        canHandle = canHandle && (destinationValidation.maxLength === null || (sourceValidation.maxLength !== null && sourceValidation.maxLength <= destinationValidation.maxLength)) && (destinationValidation.minLength === null || (sourceValidation.minLength !== null && sourceValidation.minLength >= destinationValidation.minLength));

                                        //check the pattern - must match if the destination has a pattern
                                        canHandle = canHandle && (destinationValidation.pattern === null || sourceValidation.pattern === destinationValidation.pattern);
                                        break;
                                    case 'TEXT_AREA':
                                        //can map to other text area or rich text
                                        if (destination.subType === 'RICH_TEXT') {
                                            canHandle = true;
                                        } else if (destination.subType === 'TEXT_AREA') {
                                            //must match the length constraints
                                            canHandle = (destinationValidation.maxLength === null || (sourceValidation.maxLength !== null && sourceValidation.maxLength <= destinationValidation.maxLength)) && (destinationValidation.minLength === null || (sourceValidation.minLength !== null && sourceValidation.minLength >= destinationValidation.minLength));
                                        }
                                        break;
                                    default:
                                        //default is to match subType
                                        canHandle = (source.subType === destination.subType);
                                }
                            }
                            break;
                        case 'DateControlFullDetails':
                            //Check that the destination is also a DateControl - done here so we can introduce cross type mapping if necessary
                            if (source._type === destination._type) {
                                //can map any date with the exception of an non-historic to historic
                                if(destination.subType === 'PAST_DATE'){
                                  canHandle=(source.subType === destination.subType);
                                }else{
                                  canHandle = (destination.subType === 'DATE'||destination.subType === 'PAST_DATE');
                                }
                            }
                            break;
                        case 'NumericControlFullDetails':
                            //check the destination is also a NumericControl - done here so we can introduce cross type mapping if necessary
                            if (source._type === destination._type) {
                                //extract the validation info
                                sourceValidation = source.validation;
                                destinationValidation = destination.validation;

                                //can map to any destination where the step is equal and the min and max values don't clash

                                if (destinationValidation.step === null ||
                                /*jshint eqeqeq:false*/
                                (destinationValidation.step == sourceValidation.step)/*jshint eqeqeq:true*/) {
                                    //check if the destination minValue is null, or the source min value is greater than or equal to the destination min value
                                    if (destinationValidation.maxValue === null || (sourceValidation.maxValue !== null && sourceValidation.maxValue <= destinationValidation.maxValue)) {
                                        //except if there is a source step - in which case the minValue must be the same
                                        if (sourceValidation.step === null) {
                                            canHandle = (destinationValidation.minValue === null || (sourceValidation.minValue !== null && sourceValidation.minValue >= destinationValidation.minValue));
                                        } else {
                                            canHandle = (destinationValidation.minValue === null ||/*jshint eqeqeq:false*/
                                            (sourceValidation.minValue == destinationValidation.minValue)/*jshint eqeqeq:true*/);
                                        }
                                    }
                                }

                                //check the pattern - must match if the destination has a pattern
                                canHandle = canHandle && (destinationValidation.pattern === null || sourceValidation.pattern === destinationValidation.pattern);
                            }
                            break;
                        case 'SelectionControlFullDetails':
                            //check the destination is also a SelectionControlFullDetails - done here so we can introduce cross type mapping if necessary
                            if (source._type === destination._type) {
                                sourceValidation = source.validation;
                                destinationValidation = destination.validation;
                                switch (source.subType) {
                                        //single selection source
                                    case 'LIST_BOX':
                                        /* Falls through */
                                    case 'RADIO_BUTTONS':
                                        /* Falls through */
                                    case 'RADIO_BUTTONS_INLINE':
                                        //can map to any target where minSelections is <=1
                                        canHandle = (destinationValidation.minSelections <= 1);
                                        break;

                                        //possible multiple selection source
                                    case 'CHECKBOXES':
                                        /* Falls through */
                                    case 'CHECKBOXES_INLINE':
                                        /* Falls through */
                                    case 'LIST_BOX_MULTI':
                                        //check the source min selections is greater than or equal to the destination min selections
                                        if (sourceValidation.minSelections >= destinationValidation.minSelections) {
                                            //then destination max selections is null or greater or equal to the source
                                            canHandle = (destinationValidation.maxSelections === null || (sourceValidation.maxSelections !== null && destinationValidation.maxSelections >= sourceValidation.maxSelections));
                                        }

                                        break;
                                }

                                //check the options available - source must be a subset of destination
                                canHandle = canHandle && this._checkSourceChoices(source, destination);
                            }
                            break;
                        case 'SearchInputControlFullDetails':
                          //check the destination is also a SelectionControlFullDetails - done here so we can introduce cross type mapping if necessary
                          if (source._type === destination._type) {
                            canHandle = source.subType===destination.subType;
                          }
                            break;
                    }

                    //now we can compare specific group data
                    canHandle = canHandle && this._checkSourceGroup(source.group, destination.group, destination.localId);
                }
            }
        }
        return canHandle;
    };

    _checkSourceGroup(sGroup, dGroup, dLocalId) {
        let canHandle = false;
        const checkResolverId = () => (sGroup.resolverValueId === dGroup.resolverValueId);

        const sourcePageModel = this.state.sourcePageModel || {};

        //mappings are Immutable object
        const mappings = this.state.mappings.get(sourcePageModel.id);

        const destinationPageModel = this.state.destinationPageModel;

        const findGroup = (model, groupId) => {
            let grp;
            const pages = model.pages || [];

            pages.some(page => page.sections.some(section => section.groups.some(group => {
                if (group.localId === groupId) {
                    grp = group;

                    return true;
                }
            })));

            return grp;
        };

        if (!sGroup && !dGroup) {
            //there is no group info
            canHandle = true;
        } else if (sGroup && dGroup) {
            //group type
            if (sGroup.collectionType.startsWith('SUPPLIED_') || sGroup.collectionType.startsWith('SEARCH_')) {
                //can only handle if the destination is also a supplied collection or search collection and the resolver Id matches
                canHandle = (dGroup.collectionType.startsWith('SUPPLIED_')||sGroup.collectionType.startsWith('SEARCH_')) && checkResolverId();
            }
            else {
                //destination must NOT start with SUPPLIED or SEARCH
                canHandle = !(sGroup.collectionType.startsWith('SUPPLIED_')||sGroup.collectionType.startsWith('SEARCH_'));

                //check max rows constraint source must fit into the destination
                canHandle = canHandle && sGroup.maxOccurrences <= dGroup.maxOccurrences;
            }

            //if there are existing mappings
            if (canHandle && mappings) {
                //look up the destination group based on the id
                const destinationGroup = findGroup(destinationPageModel, dGroup.localId);

                //get the controls out of the destination group
                const destinationControls = (destinationGroup)
                    ? destinationGroup.controls
                    : [];

                //for each control check that no mappings exist between other group ids - note we NOT the answer here as this is returning
                //true if we find a mapping that is NOT the same
                canHandle = !destinationControls.some(control => {
                    if (control.localId !== dLocalId) {
                        //lookup existing mapping to get the source control
                        const mappedSourceControl = mappings.get(control.id);

                        let mappedSourceGroupControl;
                        if (mappedSourceControl && mappedSourceControl.get('localId')) {
                            //lookup control from the source model
                            //we are only interested in GROUP controls here
                            sourcePageModel.pages.some(page => page.sections.some(section => section.groups.some(group => group.controls.some(control => {
                                if (control.localId === mappedSourceControl.get('localId')) {
                                    mappedSourceGroupControl = control;

                                    return true;
                                }
                            }))));

                            return ((!!mappedSourceGroupControl) && (mappedSourceGroupControl.group && mappedSourceGroupControl.group.localId !== sGroup.localId));
                        }
                    }
                });
            }
        }

        return canHandle;
    }

    _checkSourceChoices(source, destination) {
        const sC = source.choices || [];
        const dC = destination.choices || [];

        //fail fast - if source length is greater than destination length then not valid
        if (sC.length > dC.length) {
            return false;
        }

        //now check the options - using fastest loops possible
        let i,
            l,
            cVal;
        for (i = sC.length; i--;) {
            l = dC.length;
            cVal = sC[i].value;
            while (l-- && dC[l].value !== cVal) {
                continue;
            }
            if (l < 0) {
                //No good - a mismatch
                return false;
            }
        }
        return true;
    }

    handleMappedControlClick=(control, isSource)=>{
        if (isSource) {
            //focus on this destination control
            this.setState({focusSourceControlId: null, focusDestinationControlId: control.id});
        } else {
            //focus on this source control
            this.setState({focusSourceControlId: control.id, focusDestinationControlId: null});
        }

        //force an update
        this.forceUpdate();
    };

    handleDragEnd=()=>{
        //clear down any state
        this.setState({sourceControl: null, mappedControl: null, focusSourceControlId: null, focusDestinationControlId: null});
    };
    handleSourceControlDrop=(destinationControl)=>{
        if (this.state.sourceControl) {
            //Update the destination control with the new source control mapping
            this.updateControlMap(destinationControl.id, this.state.sourceControl);
        }
    };

    /**
     * Updates the mapping specified by the sourceId and destinationId with a clone of the new source
     */
    updateControlMap(destinationId, source) {
        //build the list of mappings - mappings is an Immutable object
        const mappings = this.state.mappings;

        const destinationPageModel = this.state.destinationPageModel || {};
        const sourcePageModel = this.state.sourcePageModel || {};

        if (destinationId !== undefined) {
            if (destinationPageModel.id && sourcePageModel.id) {
                //clone source control - or set to undefined if not present
                const newFormControlMapping=source?JSON.parse(JSON.stringify(source)):undefined;
                if(newFormControlMapping){
                  //get a new colour
                  newFormControlMapping.colour = rColour.get(true, 0.3, 0.99);
                }

                //update mapping (overwrite any existing mapping)
                this.setState({
                    mappings: mappings.updateIn([sourcePageModel.id], sourceMappings => {
                        if (sourceMappings) {
                            if(newFormControlMapping){
                                return sourceMappings.mergeIn([destinationId], fromJS(newFormControlMapping));
                            }else{
                                return sourceMappings.deleteIn([destinationId]);
                            }
                        } else {
                            if(newFormControlMapping){
                                return Map([[destinationId, fromJS(newFormControlMapping)]]);
                            }
                            return undefined;
                        }
                    })
                });
            }
        }
    }

    canHandleControlDropOnBin=()=>{
        return !!this.state.mappedControl;
    };
    handleMappedControlDrop=()=>{
        if (this.state.mappedControl) {
            //Update the destination control with a null source mapping
            this.updateControlMap(this.state.mappedControl.id, null);
        }
    };

    _flattenMappingsMap(mappings){
      let mappingsArray=[];

     mappings.forEach((mapping, sk) => {
          mappingsArray=mappingsArray.concat(mapping.map((mapData, dk) => mapData? (
            {
              sourceFormId: sk,
              sourceId: mapData.get('id'),
              destinationId: dk
            }): undefined
          ).filter(v => !!v).toArray());
      });

      return mappingsArray;
    }
    /**
     * Returns an object representing the currently defined form mappings
     */
    getMappings() {
      return this._flattenMappingsMap(this.state.mappings);
    }

    /**
       * Returns the ID of the currently selected source form definition (if there is one)
       */
    getSelectedSourceDefinitionId() {
        var definition = this.state.selectedDefinition;

        return definition;
    }
    /**
       * Builds a map of control id to control for destination controls
       */
    getDestinationControls(pages) {
        var p = pages || [],
            controls = {};
        p.forEach(page => page.sections.forEach(section => {
            section.controls.forEach(control => controls[control.id] = control);
            section.groups.forEach(group => group.controls.forEach(control => controls[control.id] = control));
        }));
        return controls;
    }

    render () {
        const sourcePageModel = this.state.sourcePageModel || {};
        const sourceDefinitionLabel = (
            <span>{sourcePageModel.name}</span>
        );

        const sourcePages = sourcePageModel.pages;
        const destinationPageModel = this.state.destinationPageModel || {};
        const destinationDefinitionLabel = (
            <span>{destinationPageModel.name}</span>
        );

        const destinationPages = destinationPageModel.pages;
        const destinationControls = this.getDestinationControls(destinationPages);

        //this attribute controls what we render and disables the drag/drop etc.
        const isViewOnly = this.props.viewOnly;
        //components which are only available in an edit
        const editableComponents = {};

        const mappedSourceControls = {};
        let mappedControls;

        //lookup any mapped controls for THIS source definition (if selected)
        if (sourcePageModel.id !== undefined) {
            //mappings is an Immutable object
            mappedControls = this.state.mappings.get(sourcePageModel.id);

            //build a map of source controls
            if (mappedControls) {
                mappedControls.forEach((control, k) => {
                    if (control) {
                        const destinationControl = destinationControls[k];
                        if (destinationControl) {
                            const id = control.get('id');

                            //update the colour
                            destinationControl.colour = control.get('colour');

                            mappedSourceControls[id] = mappedSourceControls[id] || [];
                            mappedSourceControls[id].push(destinationControl);
                        }
                    }
                });
            }
        }

        let mask;
        if (this.state.mask) {
            mask = (<Mask/>);
        }

        let sourceTree;
        let sourceGraph;
        if (!this.state.selectedDefinition) {
            sourceTree = (
                <div className="controlTree">
                    <span>
                        Please select a source form to show available controls.
                    </span>
                </div>
            );
        } else {
          sourceTree = (<PageTreeView
            key={sourcePageModel.id || 0}
            sourceDefinitionId={sourcePageModel.id}
            label={sourceDefinitionLabel}
            pages={sourcePages}
            isSource={true}
            handleControlDragStart={this.handleSourceControlDragStart}
            handleMappedControlDragStart={this.handleMappedControlDragStart}
            mappedControls={mappedSourceControls}
            onMappedControlClick={this.handleMappedControlClick}
            focusControlId={this.state.focusSourceControlId}
            viewOnly={isViewOnly}/>);
        }

        //only output the source graph if we have a destinationPageModel
        if (destinationPageModel.name && this.state.mappings) {
          sourceGraph = (<SourceGraph
            mappings={this.state.mappings}
            formDefinitions={this.state.formDefinitions}
            destinationModel={destinationPageModel}
            selectedDefinition={this.state.selectedDefinition}
            onSelectedDefinitionChange={this.handleSelectedDefinitionChange}/>);
        }

        if (!isViewOnly) {
            //We only show the definition selector when !view only
            editableComponents.destinationSelector = (<DefinitionSelector
              formDefinitions={this.state.formDefinitions}
              mappings={this.state.mappings}
              selectedDefinition={this.state.selectedDefinition}
              onSelectedDefinitionChange={this.handleSelectedDefinitionChange}/>);
              editableComponents.dustbin = (<Dustbin
                canHandleControlDrop={this.canHandleControlDropOnBin}
                onDrop={this.handleMappedControlDrop}/>);
        }

        return (
          <div
            key="formMappings"
            onDragEnd={this.handleDragEnd}>
            {mask}
            {editableComponents.destinationSelector}
            <div className="pure-g readonly">
              <div className="l-box pure-u-1">
                {sourceGraph}
              </div>
            </div>

            <div className="pure-g">
              <div className="l-box pure-u-1-2">
                <label>
                  Copy from:
                </label>
              </div>
              <div className="l-box pure-u-1-2">
                <div className="pure-g-r">
                  <div className="pure-u-1-3">
                    <label>
                      Copy to:
                    </label>
                  </div>
                  <div className="pure-u-2-3">
                    {editableComponents.dustbin}
                  </div>
                </div>
              </div>
              <div className="l-box pure-u-1-2">
                <div className="sourceControlTree tree">
                  {sourceTree}
                </div>
              </div>

              <div className="l-box pure-u-1-2">
                <div className="sourceControlTree tree">
                  <PageTreeView
                    key={destinationPageModel.id || 0}
                    sourceDefinitionId={sourcePageModel.id}
                    label={destinationDefinitionLabel}
                    pages={destinationPages}
                    isSource={false}
                    handleMappedControlDragStart={this.handleMappedControlDragStart}
                    canHandleControlDrop={this.canHandleSourceControlDrop}
                    handleControlDrop={this.handleSourceControlDrop}
                    mappedControls={mappedControls?mappedControls.toJS():undefined}
                    onMappedControlClick={this.handleMappedControlClick}
                    focusControlId={this.state.focusDestinationControlId}
                    viewOnly={isViewOnly}/>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

