'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import SVGComponent from './svg/component';
import SVGFormDefinition from './svg/formDefinition';
import CollapsibleContainer from './collapsableContainer';
import {Map} from 'immutable';
import memoize from 'memoize-one';

/**
* Convert the list of mappings into an array of definitions and mapping count
*/
const getMappedDefinitions=function(mappings, formDefinitions) {
   const definitions = [];

   mappings.forEach((mapping, definitionId) => {
       //count the mappings
       const mappingCount = mapping
           ? mapping.toArray().filter(x => !!x).length
           : 0;

       //only if there are valid mappings do we want to include it in our map
       if (mappingCount > 0) {
           const definition = getFormDefinition(definitionId, formDefinitions);
           if (definition) {
               //push into our object
               definitions.push({id: definition.id, name: definition.name, version: definition.version, mappingCount: mappingCount});
           }
       }
   });
   return definitions.sort((a, b) => a.name.localeCompare(b.name, 'en', {'sensitivity': 'base'}));
};

const  getFormDefinition=function(id, definitions) {
    let defn;
    definitions.some(function(formDefinition) {
        /*jshint eqeqeq: false*/
        //Key is string but id is number
        if (formDefinition.id == id) {
            defn = formDefinition;
            return true;
        }
        /*jshint eqeqeq: true*/
    });

    return defn;
};

export default class SourceGraph extends PureComponent {
    static propTypes = {
        mappings: PropTypes.instanceOf(Map).isRequired,
        formDefinitions: PropTypes.array.isRequired,
        destinationModel: PropTypes.object,
        selectedDefinition: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.number.isRequired]),
        onSelectedDefinitionChange: PropTypes.func.isRequired
    };

    colourSchemes = {
        source: {
            fillColour: 'rgb(176, 219, 238)',
            highlightFillColour: 'rgb(211, 205, 198)',
            strokeColour: 'rgb(80, 95, 101)',
            textColour: 'rgb(26, 29, 27)',
            highlightStrokeColour: 'rgb(85, 159, 191)'
        },
        destination: {
            fillColour: 'rgb(211, 205, 198)',
            highlightFillColour: 'rgb(211, 205, 198)',
            strokeColour: 'rgb(123, 124, 136)',
            textColour: 'rgb(22, 24, 25)',
            highlightStrokeColour: 'rgb(135, 151, 157)'
        }
    };
    viewWidth = 500;
    height = 12;
    width = 200;

    /**
     * memoize will only execute the internal function of the arguments have changed
     */
    mappedDefinitions=memoize((mappings, formDefinitions)=>getMappedDefinitions(mappings, formDefinitions));

    handleSourceDefinitionSelection=(id)=> {
        this.props.onSelectedDefinitionChange(id);
    };
    handleDestinationDefinitionSelection=()=> {
        //This does nothing
    };

    _getY(i) {
        return (this.height * i) + ((this.height * i) / 2);
    }
    getLabel(definition) {
        let name = definition.name;

        const version = '(version: ' + definition.version + ')';

        if (name.length > 30) {
            name = name.substring(0, 27) + '...';
        }

        return [name, version].join(' ');
    }

    getMappings() {
        const {selectedDefinition=null, mappings, formDefinitions}=this.props;
        const mappedDefinitions = this.mappedDefinitions(mappings, formDefinitions);

        //for each mapping output the source definition
        return mappedDefinitions.map((mappedDefinition, i) => (
          <SVGFormDefinition
            clickable={true}
            colourScheme={this.colourSchemes.source}
            selected={selectedDefinition == mappedDefinition.id}
            label={this.getLabel(mappedDefinition)}
            id={mappedDefinition.id}
            x={2}
            y={this._getY(i)}
            height={this.height}
            width={this.width}
            key={mappedDefinition.id}
            onDefinitionSelection={this.handleSourceDefinitionSelection}/>
        ));
    }

    getDestination() {
        const {mappings, formDefinitions}=this.props;
        const mappedDefinitions = this.mappedDefinitions(mappings, formDefinitions);
        let len = mappedDefinitions.length;
        const height = this._getY(len);
        const sHMid = this.height / 2;
        const vWidth = this.viewWidth;

        //calculate coordinates for destination mapping
        const coords = {
            x: (vWidth - this.width - 2),
            y: (height - this.height) / 2
        };

        //array to contain our paths
        const paths = [];
        const xToElbow = (this.width + 2) + ((vWidth - this.width) * 0.25);
        const xToDest = (vWidth - this.width - 2);
        const yToDest = coords.y + sHMid;

        const scheme = this.colourSchemes.source;

        if (!this.props.destinationModel) {
            //if no destination model - then we can show no mappings
            return (<div/>);
        }

        let y;
        let path;
        //setup paths
        while (len--) {
            y = this._getY(len) + sHMid;
            path = [];
            //move
            path.push('M');
            //coords of first source x,y
            path.push(this.width + 2);
            path.push(y);
            //draw line to elbow
            path.push('L');
            //x,y
            path.push(xToElbow);
            path.push(y);
            //draw from elbow
            path.push('L');
            //x,y
            path.push(xToElbow);
            path.push(yToDest);
            //draw to destination
            path.push('L');
            //x,y
            path.push(xToDest);
            path.push(yToDest);
            //pop this path into our array
            paths.push(path.join(' '));
        }
        return (
            <g>
                {
                  paths.map((path, i)=>(<path key={i} d={path} stroke={scheme.strokeColour} strokeWidth="1" fill="none"/>))
                }
                <SVGFormDefinition
                  colourScheme={this.colourSchemes.destination}
                  label={this.getLabel(this.props.destinationModel)}
                  id={0}
                  x={coords.x}
                  y={coords.y}
                  key={0}
                  onDefinitionSelection={this.handleDestinationDefinitionSelection}
                  height={this.height}
                  width={this.width}/>
            </g>
        );
    }

    render() {
        const {mappings, formDefinitions}=this.props;
        const mappedDefinitions = this.mappedDefinitions(mappings, formDefinitions);

        const len = mappedDefinitions.length;

        const height = this.height * len + ((this.height * len) / 2);
        const width = this.viewWidth;
        const hideLabel = (
          <span className="clickable">
            <i className="fa fa-angle-double-up icon-double-angle-up"/>
            Hide mappings view
          </span>
        );
        const showLabel = (
          <span className="clickable">
            <i className="fa fa-angle-double-down icon-double-angle-down"/>
            Show mappings view
          </span>
        );

        if (len > 0) {
            return (
                <div className="source-graph">
                    <CollapsibleContainer hideLabel={hideLabel} showLabel={showLabel}>
                        <SVGComponent viewBox={'0 0 ' + width + ' ' + height} preserveAspectRatio="xMinYMin">
                            {this.getMappings()}
                            {this.getDestination()}
                        </SVGComponent>
                    </CollapsibleContainer>
                </div>
            );
        } else {
            return (<div/>);
        }
    }
}
