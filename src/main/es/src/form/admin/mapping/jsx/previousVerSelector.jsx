'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const ShowPreviousSelector=({showPrevious, onShowPreviousChange}) => {
    const handleChange = (e) => onShowPreviousChange(e.target.checked);
    return (
      <div>
        <label htmlFor="onlyCurrentVersions">
          <input
            id="onlyCurrentVersions"
            type="checkbox"
            checked={showPrevious}
            onChange={handleChange}/>
          Include previous versions
        </label>
      </div>
    );
};
ShowPreviousSelector.propTypes={
  showPrevious: PropTypes.bool.isRequired,
  onShowPreviousChange: PropTypes.func.isRequired
};

export default ShowPreviousSelector;
