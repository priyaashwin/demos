'use strict';

import {PureComponent} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

export default class DraggableControl extends PureComponent {
    static propTypes = {
        focus:PropTypes.bool,
        onMappedControlDragStart: PropTypes.func.isRequired,
        onMappedControlClick: PropTypes.func.isRequired
    };

    findScrollParent(el) {
        let ref = el;
        while (ref) {
            if (ref.className.match(/\bcontrolTree\b/)) {
                return ref;
            }
            ref = ref.parentElement;
        }
        return;
    }
    componentDidUpdate() {
        const offsetTop = function(n, scrollParent) {
            let offset = 0;
            if (n.offsetParent) {
                do
                {
                    offset += n.offsetTop;
                }
                while ((n = n.offsetParent) && n !== scrollParent) ;
                }
            return offset;
        };
        if (this.props.focus && this.__scrollParent) {
            const node = ReactDOM.findDOMNode(this);
            const scrollParent = this.__scrollParent;

            scrollParent.scrollTop = offsetTop(node, scrollParent) - (scrollParent.clientHeight / 2);
        }
    }
    componentDidMount() {
        const node = ReactDOM.findDOMNode(this);

        this.__scrollParent = this.findScrollParent(node);
    }
    componentWillUnmount() {
        if (this.__scrollParent) {
            delete this.__scrollParent;
        }
    }
    handleMappedControlDragStart=(control)=>{
        //call into the parent for the drag
        this.props.onMappedControlDragStart(control);
    };
    handleMappedControlClick=(control)=> {
        this.props.onMappedControlClick(control, false);
    };
}

