'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Draggable = ({effect, enabled, onDrag, payload, children}) => {
    const handleDragStart=(e) => {
        var dataTransfer = e.nativeEvent.dataTransfer;

        if (enabled) {
            //don't let anything else pick up this event
            e.stopPropagation();

            dataTransfer.effectAllowed = effect;

            //clear any existing data
            dataTransfer.clearData();

            //set payload we are dragging (IE won't let us read this until we drop anyway)
            dataTransfer.setData('text', payload);

            //call into the parent component
            onDrag.call(null);
        }
    };

    const className = classnames({'draggable': enabled});
    return (
      <div
        className={className}
        draggable={enabled}
        onDragStart={handleDragStart}>
        {children}
      </div>
    );
};

Draggable.propTypes = {
    effect: PropTypes.string,
    enabled: PropTypes.bool.isRequired,
    onDrag: PropTypes.func.isRequired,
    payload: PropTypes.string.isRequired,
    children: PropTypes.node
};

export default Draggable;
