'use strict';

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import TreeView from 'usp-simple-tree';
import SourceControl from './control/sourceControl';
import DestinationControl from './control/destinationControl';

export default class PageTreeView extends PureComponent {
    constructor(props) {
        super(props);
    }

    getControlsInfo(controls) {
        //track the mappings
        const mappedControls = this.props.mappedControls||{};
        let mappingCount = 0;
        const isSource = this.props.isSource;
        const isViewOnly = this.props.viewOnly;
        const controlsInfo = controls.map((control, ci) => {
            //lookup existing mapped control - note this will be either the source control/destination control array - that is determined by the mapping passed in by the parent
            const mappedControl = mappedControls[control.id];

            if (Array.isArray(mappedControl)) {
                mappingCount += mappedControl.length;
            } else {
                mappingCount += !!mappedControl;
            }

            return (
                <div className="control" key={ci}>
                    {isSource
                        ? <SourceControl key={ci} control={control} focus={control.id === this.props.focusControlId} mappedControls={mappedControl} onDragStart={this.props.handleControlDragStart} onMappedControlDragStart={this.props.handleMappedControlDragStart} onMappedControlClick={this.props.onMappedControlClick} viewOnly={isViewOnly}/>
                        : <DestinationControl key={ci} control={control} focus={control.id === this.props.focusControlId} mappedControl={mappedControl} onMappedControlDragStart={this.props.handleMappedControlDragStart} canHandleSourceControlDrop={this.props.canHandleControlDrop} handleSourceControlDrop={this.props.handleControlDrop} onMappedControlClick={this.props.onMappedControlClick} viewOnly={isViewOnly}/>
}
                </div>
            );
        });

        return {
            controls: controlsInfo,
            mappingCount: mappingCount,
            forceOpen: controls.some(c => c.id === this.props.focusControlId)
        };
    }
    getSectionInfo(section) {
        let mappingCount = 0;
        let sectionControlsInfo;
        let groupControlsInfo;

        if (section.controls) {
            sectionControlsInfo = this.getControlsInfo(section.controls);
            mappingCount += sectionControlsInfo.mappingCount;
        }
        return ({
            label: (
                <span className="section">{section.name}</span>
            ),
            controls: sectionControlsInfo.controls,
            forceOpen: sectionControlsInfo.forceOpen,
            groups: section.groups.map(group => {
                groupControlsInfo = this.getControlsInfo(group.controls);

                mappingCount += groupControlsInfo.mappingCount;
                return ({label: (
                        <span className="group">{group.name}</span>
                    ), controls: groupControlsInfo.controls, mappingCount: groupControlsInfo.mappingCount, forceOpen: groupControlsInfo.forceOpen});
            }),
            mappingCount: mappingCount
        });
    }
    _getPagesTree(id, label, pages) {
        //build sections tree
        const sectionsTree = pages.map((page, i) => this._getSectionsTree(i, (
            <span className="page">{page.name}</span>
        ), page.sections));

        //always expand the tree root
        const openTree = sectionsTree.some(t => t.props.open);
        const forceOpen = sectionsTree.some(t => t.props.forceOpen);
        return (
          <TreeView
            key={id}
            label={label}
            open={openTree}
            forceOpen={forceOpen}>
            {sectionsTree}
          </TreeView>
        );
    }
    _getSectionsTree(id, label, sections) {
        //build a tree of controls
        const controlsTree = sections.map((section, i) => {
            //get the section info
            const sectionInfo = this.getSectionInfo(section);

            const groupTree = this._getGroupsTree(i, sectionInfo.groups);

            //expand if any children are expanded
            const openTree = sectionInfo.mappingCount > 0 || groupTree.some(t => t.props.open);
            const forceOpen = sectionInfo.forceOpen || groupTree.some(t => t.props.forceOpen);

            return (
              <TreeView
                key={'s' + i}
                label={sectionInfo.label}
                open={openTree}
                forceOpen={forceOpen}>
                {sectionInfo.controls}
                {groupTree}
              </TreeView>
            );
        });

        //expand if any children are expanded
        const openTree = controlsTree.some(t => t.props.open);
        const forceOpen = controlsTree.some(t => t.props.forceOpen);

        return (
            <TreeView key={id} label={label} open={openTree} forceOpen={forceOpen}>
                {controlsTree}
            </TreeView>
        );
    }
    _getGroupsTree(id, groups) {
        return groups.map((group, i) => {
            return (
              <TreeView
                key={'g' + i}
                label={group.label}
                open={group.mappingCount > 0}
                forceOpen={group.forceOpen}>
                {group.controls}
              </TreeView>
            );
        });
    }

    render() {
        const pages = this.props.pages || [];

        //check pages were specified, but we have none to show
        if (this.props.pages && pages.length === 0) {
            return (
                <span>Sorry, no mappable controls could be found in the form:
                    <i>{this.props.label}</i>.</span>
            );
        }

        //render a TreeView of pages/sections and controls
        return (
          <div className="controlTree">
            {this._getPagesTree('root' + this.props.isSource, this.props.label, pages)}
          </div>
        );
    }
}

PageTreeView.propTypes = {
    label: PropTypes.node.isRequired,
    pages: PropTypes.array,
    //the hash of existing mapped controls
    mappedControls: PropTypes.object,
    //flag to indicate this is a source
    isSource: PropTypes.bool.isRequired,
    canHandleControlDrop: PropTypes.func,
    handleControlDrop: PropTypes.func,
    handleControlDragStart: PropTypes.func,
    handleMappedControlDragStart: PropTypes.func,
    sourceDefinitionId: PropTypes.number,
    //click handler for clicking on a mapped control
    onMappedControlClick: PropTypes.func.isRequired,
    //a single control to focus in the tree
    focusControlId: PropTypes.number,
    viewOnly: PropTypes.bool.isRequired
};
