'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';


export default class SVGFormDefinition extends PureComponent {
    constructor(props) {
        super(props);

        //initial state
        this.state = {
            showPrevious: false
        };

        this.handleClick=this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();

        //pass this back to the parent
        this.props.onDefinitionSelection(this.props.id);
    }

    render() {
        const {
            colourScheme,
            selected,
            height,
            width,
            clickable,
            x,
            y,
            label
        } = this.props;

        const stroke = selected?colourScheme.highlightStrokeColour:colourScheme.strokeColour;

        const fill = selected?colourScheme.highlightFillColour:colourScheme.fillColour;

        const fontSize = 8;
        const className = classnames({'clickable': clickable});
        return (
          <g
            className={className}
            onClick={this.handleClick}>

            <rect
              x={x}
              y={y}
              rx="1"
              ry="1"
              width={width}
              height={height}
              fill={fill}
              stroke={stroke}
              strokeWidth="1"/>

            <text
              x={x + 2}
              y={y + ((height + fontSize) / 2)}
              fontFamily="Verdana"
              fontSize={fontSize}
              fill={colourScheme.textColour}>
              {label}
            </text>

          </g>
        );
    }
}

SVGFormDefinition.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.number.isRequired]),
    label: PropTypes.string.isRequired,
    colourScheme: PropTypes.object.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    selected: PropTypes.bool,
    clickable: PropTypes.bool,
    onDefinitionSelection:PropTypes.func.isRequired
};
