'use strict';

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Draggable from './draggable';
import ControlInfo from './controlInfo';

export default class MappedControl extends PureComponent {
    constructor(props) {
        super(props);

        this.handleDragStart=this.handleDragStart.bind(this);
        this.handleClick=this.handleClick.bind(this);
    }

    handleDragStart() {
        //call into the parent component to keep a track of the control
        this.props.onDrag(this.props.control);
    }
    handleClick(e) {
        e.preventDefault();

        //call into the parent - passing the "output" control
        this.props.onClick(this.props.outputControl);
    }
    render() {
        var divStyle = {},
            isViewOnly = this.props.viewOnly;

        if (this.props.colour) {
            divStyle.backgroundColor = this.props.colour;
        }
        return (
          <Draggable
            effect="move"
            enabled={!isViewOnly}
            onDrag={this.handleDragStart}
            payload={this.props.control.localId}>
            <span
              style={divStyle}
              className="source-control"
              onClick={this.handleClick}>
              <ControlInfo control={this.props.outputControl}/>
            </span>
          </Draggable>
        );
    }
}

MappedControl.propTypes = {
    onDrag: PropTypes.func.isRequired,
    //The control to output data for
    outputControl: PropTypes.object.isRequired,
    //The control used as the payload for drag'n'drop
    control: PropTypes.object.isRequired,
    colour: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    viewOnly: PropTypes.bool.isRequired
};
