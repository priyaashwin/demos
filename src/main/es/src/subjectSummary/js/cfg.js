'use strict';

class Endpoint{
  constructor() {
    this.classificationsListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.classification.aggregatedClassificationAssignments+json'
      },
      params:{
        aggregated: true
      },
      responseType: 'json',
      responseStatus:200
    };

    this.profileClassificationsEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.classification.classificationAssignmentWithClassificationDetails+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.chronologyEntryListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.chronology.chronologyEntryWithSubject+json'
      },
      params:{
        startDate: 0,
        endDate:0
      },
      responseType: 'json',
      responseStatus:200
    };

    this.chronologyRangeEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.chronology.personchronologywithentryrange+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.personPersonRelationshipsListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.relationship.keyRelationship+json'
      },
      params:{
        s:{'p.startDate': 1}
      },
      responseType: 'json',
      responseStatus:200
    };

    this.personOrganisationRelationshipsListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.relationship.keyOrganisationRelationship+json'
      },
      params:{
        s:{'po.startDate': 1}
      },
      responseType: 'json',
      responseStatus:200
    };

    this.childProtectionRelationshipsListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.childprotection.PersonPersonRelationshipWithChildProtectionDetails+json'
      },
      params:{
        familialDifferential:1,
        s:{'p.startDate': 1}
      },
      responseType: 'json',
      responseStatus:200
    };

    this.subjectPictureEndpoint={
      method: 'get',
      contentType: 'image/*',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'image/png,image/*;q=0.8,*/*;q=0.5'
      },
      responseType: 'blob',
      responseStatus:200
    };

    this.latestCaseNoteEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.casenote.caseNoteEntry+json'
      },
      params:{
        s:{'eventDate!calculatedDate': -1}
      },
      responseType: 'json',
      responseStatus:200
    };

    this.caseNoteConfigurableTypeEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.casenote.caseNoteConfigurableTypeReference+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.formConfigurableTypeEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.form.formConfigurableTypeReference+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.latestFormInstanceEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.form.formInstance+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.carerApprovalSummaryEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.eventListEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.audit.auditEventLogEntry+json'
      },
      params:{
        s:{'eventTimestamp': -1}
      },
      responseType: 'json',
      responseStatus:200
    };

    this.membersEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.person.persongroupmembershipdetails+json'
      },
      params:{
        s:{'pgm.person.surname':1}
      },
      responseType: 'json',
      responseStatus:200
    };

    this.groupEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.group.group+json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.serviceAgreementsEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json'
        
      },
      params:{
        pageSize:-1,
        page:1
      },
      responseType: 'json',
      responseStatus:200
    };

    
    this.personalBudgetEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json'
      },
      params:{
        pageSize:-1,
        page:1
      },
      responseType: 'json',
      responseStatus:200
    };

    this.financialReferencesEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json'
      },
      responseType: 'json',
      responseStatus:200
    };

    this.financialRepresentativesEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json'
      },
      params:{
        pageSize:-1,
        page:1
      },
      responseType: 'json',
      responseStatus:200
    };

    this.residentialContributionEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json'
      },
      params:{
        pageSize:-1,
        page:1
      },
      responseType: 'json',
      responseStatus:200
    };

    this.nonResidentialAssessmentEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json'
      },
      params:{
        pageSize:-1,
        page:1
      },
      responseType: 'json',
      responseStatus:200
    };

    this.personHealthSummaryEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.person.personhealthsummaryurl+json'
      },
      responseType: 'json',
      responseStatus:200
    };

  }
}

export default new Endpoint();
