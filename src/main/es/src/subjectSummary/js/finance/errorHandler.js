const ProviderUnavailable = 'ProviderUnavailableException';
const ResourceNotFoundInProvider = 'ResourceNotFoundInProviderException';
const ExternalResourceNotFound = 'ExternalPersonResourceNotFoundException';

const customErrorHandler = function (errorObject, labels = {}) {
    let error;

    if (errorObject && errorObject.code) {
        const exceptionType = errorObject.exceptionType;
        switch (errorObject.code) {
        case 503:
            if (ProviderUnavailable === exceptionType) {
                error = {
                    title: labels.providerUnavailableTitle || 'System not available',
                    messages: [labels.providerUnavailableMessage || 'Unable to communicate with the external system.']
                };
            }
            break;
        case 404:
            {
                if (ResourceNotFoundInProvider === exceptionType) {
                    error = {
                        title: labels.resourceNotFoundInProviderTitle || 'Person not found in external system',
                        messages: [labels.resourceNotFoundInProviderMessage || 'A matching person could not be found in the external system.'],
                        type: 'info'
                    };
                } else if (ExternalResourceNotFound === exceptionType) {
                    error = {
                        title: labels.externalResourceNotFoundTitle || 'ID/Reference number not found',
                        messages: [labels.externalResourceNotFoundMessage || 'There is no ID/Reference number recorded against this person for the external system.'],
                        type: 'info'
                    };
                }
                break;
            }
        }
    }

    if (error) {
        //return new object cloned from errorObject
        //and override with generated error object
        return ({
            ...errorObject,
            ...error
        });
    }
    //return original error object
    return errorObject;
};

export { customErrorHandler };