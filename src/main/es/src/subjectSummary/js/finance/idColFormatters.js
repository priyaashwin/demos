/**
 * Formats the value into a required id column.
 * @idColFormatter string
 * @param value {String} The raw value to format 
 * @return {string} A string value representing the value formatted.
 **/
export const idColFormatter = value =>
	((typeof value === 'string') && value.indexOf('%') !== -1) ? value.slice(value.indexOf('%')+1) : value;