const maxHeight=24;
const minHeight=2;
const minMargin=2;

//we loose about 20 pixels every month
const monthHeight=20;

const daysInHeight=function(height, zoom){
  const dh=getHeight(zoom)+getMargin(zoom);

  const daysInMonthHeight=(dh*30);
  //how many months in this height?
  const approxMonthHeight=Math.floor(height/daysInMonthHeight)*monthHeight;

  return Math.floor((height-approxMonthHeight)/dh);
};

const daysToHeight=function(days, zoom){
  const dh=getHeight(zoom)+getMargin(zoom);

  const months=Math.floor(days/30);

  return Math.floor((days*dh)+(months*monthHeight));
};

const getHeight=function(zoom){
  return (Math.min(Math.max(maxHeight*(zoom/100),minHeight),maxHeight));
};

const getMargin=function(zoom){
  return Math.max(getHeight(zoom)/3, minMargin);
};

export {daysInHeight, daysToHeight, getHeight, getMargin};
