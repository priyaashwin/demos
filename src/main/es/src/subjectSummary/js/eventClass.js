
const colours={
  NOENTRY:'#c7c7c7',
  POSITIVE:'#75ca89',
  NEUTRAL:'#1168aa',
  NEGATIVE:'#f56b6b',
  UNKNOWN:'#5a5a5a'
};

const getEventClass=function(event, impactEnabled){
  const impact=impactEnabled?(event.impact||''):'NEUTRAL';
  let className='';

  switch(impact.toUpperCase()){
    case 'POSITIVE':
      className='positive';
    break;
    case 'NEUTRAL':
      className='neutral';
    break;
    case 'NEGATIVE':
      className='negative';
    break;
    default:
      className='unknown';
  }

  return className;
};

const getEventColour=function(event, impactEnabled){
  const impact=impactEnabled?(event.impact||''):'NEUTRAL';

  return colours[impact.toUpperCase()];
};

export {getEventClass, getEventColour};
