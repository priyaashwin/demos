const getPerson=function(subjectId, relation){
    const subjectPersonId = Number(subjectId);

    let person;
    if (Number(relation.roleAPersonId) === subjectPersonId) {
      person = relation.roleBPersonVO;
    } else {
      person = relation.roleAPersonVO;
    }

    return person;
};

export {getPerson as getPersonFromRelationship};