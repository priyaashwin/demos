'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Row=({height, children})=>{
  return(
    <div className={classnames('widget-row', `widget-${height}-row`)}>{children}</div>
  );
};

Row.propTypes = {
    height: PropTypes.number.isRequired,
    children: PropTypes.node
};

export default Row;
