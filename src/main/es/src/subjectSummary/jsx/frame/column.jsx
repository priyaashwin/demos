'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Column extends PureComponent {
  render(){
    const {className, height, children}=this.props;

    return (
      <div className={classnames('widget-column', className)} style={{height:height}}>
        {children}
      </div>
    );
  }
}
Column.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    height: PropTypes.number
};
