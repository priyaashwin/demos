'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Page extends PureComponent {
    static propTypes = {
        index: PropTypes.number.isRequired,
        title: PropTypes.any.isRequired,
        selected: PropTypes.bool,
        onSelect: PropTypes.func.isRequired
    };
    componentDidUpdate() {
        const { selected } = this.props;
        if (this.page && selected) {
            this.page.focus();
        }
    }
    handleClick = (e) => {
        const { onSelect, index, selected } = this.props;
        e.preventDefault();

        if (!selected) {
            onSelect(index);
        }
    }
    pageRef = (ref) => this.page = ref;
    render() {
        const { index, title, selected } = this.props;
        return (
            <a
                key={index}
                ref={this.pageRef}
                href="#none"
                tabIndex={0}
                className={
                    classnames('page-switcher', {
                        'selected-page': selected
                    })
                }
                onClick={this.handleClick}
            >
                {title}
            </a>
        );
    }
}
