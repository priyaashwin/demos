'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Column from './column';
import Row from './row';
import Page from './page';
import { Seq, List } from 'immutable';
import debounce from 'just-debounce';
import FixedWidget from '../widget/fixed';
import memoize from 'memoize-one';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const transitionTime = { enter: 500 };

export default class Frame extends PureComponent {
  static propTypes = {
    labels: PropTypes.shape({
      accessDenied: PropTypes.string.isRequired
    }).isRequired,
    fixedContentHeight: PropTypes.number,
    pages: PropTypes.arrayOf(
      PropTypes.shape({
        permissions: PropTypes.shape({
          canView: PropTypes.bool
        }).isRequired,
        //pool of available widgets for this page
        widgetPool: PropTypes.arrayOf(
          PropTypes.shape({
            key: PropTypes.string.isRequired,
            type: PropTypes.func.isRequired
          })
        ).isRequired,
        //page title
        title: PropTypes.string,
        layout: PropTypes.shape({
          //number of columns in layout
          columns: PropTypes.oneOf([1, 2, 3, 4]),
          //widgets on page
          widgets: PropTypes.arrayOf(
            PropTypes.shape({
              //zero based column index
              column: PropTypes.oneOf([0, 1, 2, 3]).isRequired,
              widget: PropTypes.shape({
                key: PropTypes.string.isRequired,
                configuration: PropTypes.obj,
                //12ths but only key points
                height: PropTypes.oneOf([3, 4, 6, 8, 12]),
                //hide the widget if the user does not have access
                hideOnNoAccess: PropTypes.bool,
                //permission object containing view permission
                permissions: PropTypes.shape({
                  canView: PropTypes.bool
                }).isRequired
              })
            })
          ).isRequired
        }).isRequired
      })
    ).isRequired
  };

  static defaultProps = {
    pages: [{
      permissions:{
        canView:true
      },
      //widget pool for page
      widgetPool: [],
      //default to single page layout
      title: 'page 1',
      //layout for page
      layout: [{
        //default to 3 column layout
        columns: 3,
      }]
    }],
    labels: {
      accessDenied: 'Access denied'
    }
  };
  //provide a filtered list of pages that can be viewed
  getPages = memoize(pages => pages.filter(page=>page.permissions.canView===true));
  //return the widget pool
  getWidgetPool = memoize(pool => Seq(pool).toKeyedSeq().mapKeys((i, meta) => meta.key).cacheResult());
  //return the columns for the layout
  getColumns = memoize(layout => List().withMutations(list => {
    for (let i = 0; i < layout.columns; i++) {
      list.set(i, layout.widgets.filter(w => w.column === i).map(c => c.widget));
    }
  }));

  state = {
    height: this.getOptimumHeight(),
    page: 0
  };

  handleResize = debounce(() => {
    const newHeight = this.getOptimumHeight();
    if (newHeight !== this.state.height) {
      this.setState({
        height: newHeight
      });
    }
  }, 100);

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }
  getOptimumHeight() {
    const { fixedContentHeight} = this.props;
    const pages=this.getPages(this.props.pages);
    const winH = window.innerHeight;

    //include a bit of padding in this
    let availableHeight = winH - fixedContentHeight - 4;

    if(pages.length>1){
      //allow height for page switcher
      availableHeight-=26;
    }
    return availableHeight;
  }
  getWidget(index, widgetConfig, height) {
    const { labels} = this.props;
    const pages=this.getPages(this.props.pages);
    const { page } = this.state;
    const widget = this.getWidgetPool(pages[page].widgetPool).get(widgetConfig.key);


    let widgetElement;
    if (widget) {
      const permissions = widgetConfig.permissions || {};
      if (permissions.canView) {
        widgetElement = React.createElement(widget.type, { ...widgetConfig.configuration, height: height, permissions: permissions });
      } else {
        if(!widgetConfig.hideOnNoAccess){
          widgetElement = (<FixedWidget key={index} {...widgetConfig.configuration}><div className="empty-list"><span className="empty no-access">{labels.accessDenied}</span></div></FixedWidget>);
        }
      }
    }

    if(widgetElement){
      return (
        <div className="widget-container" key={widgetConfig.key + '' + index}>
          {widgetElement}
        </div>
      );
    }
    //no ouput
    return false;
  }
  handlePageSelect = (index) => this.setState({ page: index });
  handleKeyDown = (e) => {    
    const pages=this.getPages(this.props.pages);
    const { page } = this.state;
    let selectedIndex = page;

    if (pages.length > 1 && (e.key === 'ArrowLeft' || e.key === 'ArrowRight')) {
      switch (e.key) {
        case 'ArrowLeft':
          selectedIndex = page - 1;
          break;
        case 'ArrowRight':
          selectedIndex = page + 1;
          break;
      }
      if (selectedIndex < 0) {
        selectedIndex = pages.length - 1;
      } else if (selectedIndex >= pages.length) {
        selectedIndex = 0;
      }
    }
    this.setState({
      page: selectedIndex
    });
  };
  getPageSwitcher() {
    const pages=this.getPages(this.props.pages);
    const { page: selectedPage } = this.state;

    if (pages.length <= 1) {
      return false;
    }

    return (
      <div className="page-selector">
        <ul onKeyDown={this.handleKeyDown}>
          {pages.map((p, i) => (
            <li key={i}>
              <Page
                index={i}
                title={p.title || `Page ${i + 1}`}
                selected={i === selectedPage}
                onSelect={this.handlePageSelect}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
  render() {
    const pages=this.getPages(this.props.pages);
    const { height, page } = this.state;
    const layout = pages[page].layout;
    return (
      <div className="summary-widget-frame">
        {this.getPageSwitcher()}
        <TransitionGroup key="tgroup" component={null}>
          <CSSTransition key={page} classNames="frame-fade" timeout={transitionTime} exit={false}>
            <div className="widget-frame">
              {this.getColumns(layout).map((widgets, i) => (
                <Column height={height} key={i}>
                  {Seq(widgets).map((widget, i) => (
                    <Row key={i} height={widget.height}>
                      {this.getWidget(i, widget, height * (widget.height / 12))}
                    </Row>
                  ))}
                </Column>
              ))}
            </div>
          </CSSTransition>
        </TransitionGroup>
      </div>
    );
  }
}
