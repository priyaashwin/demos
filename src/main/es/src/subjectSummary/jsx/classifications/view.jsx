'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import RemoteCardList from '../../../card/remoteCardList';
import Title from './title';
import Content from './content';
import endpoint from '../../js/cfg';
import subs from 'subs';
import ScrollableWidget from '../widget/scrollable';
import Header from '../widget/header';

const CARD_CLASS_NAME='classification-card';

export default class ClassificationCardView extends Component {
    static propTypes = {
        title: PropTypes.string,
        url: PropTypes.string,
        subject:PropTypes.shape({
          subjectId: PropTypes.oneOfType([
              PropTypes.string,
              PropTypes.number
            ]),
          subjectType: PropTypes.string
        }),
        labels: PropTypes.object.isRequired
    };
    getCardTitle=(classification)=>{
      return (<Title classification={classification} labels={this.props.labels}/>);
    };
    getCardContent=(classification)=>{
      return (<Content classification={classification} labels={this.props.labels}/>);
    };
    getCardClassName=()=>{
      return CARD_CLASS_NAME;
    };

    getRestEndpoint(url, subject) {
        //merge our URL into the endpoint
        return {
            ...endpoint.classificationsListEndpoint,
            //substitute the subject parameters
            url: subs(url, subject)
        };
    }

    handleAfterUpdate=()=>{
      //Update the overflow property once the data has loaded
      if(this.containerElem){
        this.containerElem.updateOverflow();
      }
    }
    render() {
      const {labels={}, url, subject}=this.props;

      const header=(
        <Header title={labels.header.title}/>
      );

      return (
          <ScrollableWidget ref={(elem) => { this.containerElem = elem; }} className="classifications-widget" header={header}>
            <RemoteCardList
              restEndpoint={this.getRestEndpoint(url, subject)}
              className="card-view"
              getCardTitle={this.getCardTitle}
              getCardContent={this.getCardContent}
              getCardClassName={this.getCardClassName}
              afterUpdate={this.handleAfterUpdate}
              enableHover={false}
              enableShadow={false}
              noResultsMessage={labels.noRecordsFound}/>
          </ScrollableWidget>
      );
    }
}
