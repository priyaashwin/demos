'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const CurrentClassification=({currentDuration, labels})=>{
  if(currentDuration!==null && currentDuration!==undefined && currentDuration>=0){
    return (<span className="current">{`${labels.currentDuration} ${currentDuration} ${currentDuration===1?labels.dayAgo:labels.daysAgo}`}</span>);
  }

  return false;
};

CurrentClassification.propTypes={
  currentDuration: PropTypes.number,
  labels: PropTypes.object.isRequired
};

export default CurrentClassification;
