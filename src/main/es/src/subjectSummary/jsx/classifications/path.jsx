'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Path=({path})=>(
  <span>{path.slice(-2).join(' > ')}</span>
);

Path.propTypes={
  path: PropTypes.arrayOf(PropTypes.string)
};

export default Path;
