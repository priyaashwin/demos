'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Path from './path';
import CurrentClassification from './current';


const Title=({classification, labels})=>(
  <div>
    <h5 className="classification-name">
      <Path path={classification.classification}/>
    </h5>
    <CurrentClassification currentDuration={classification.currentDuration} labels={labels} />
  </div>
);

Title.propTypes={
  classification: PropTypes.object,
  labels: PropTypes.object.isRequired
};

export default Title;
