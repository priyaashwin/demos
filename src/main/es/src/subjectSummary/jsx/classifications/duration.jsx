'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Duration=({totalDuration, labels})=>(
  <div className="duration">
    <span className="data-value">{totalDuration}</span>
    <span className="data-label">{labels.totalDuration}</span>
  </div>
);

Duration.propTypes={
  totalDuration: PropTypes.number,
  labels: PropTypes.object.isRequired
};

export default Duration;
