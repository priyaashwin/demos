'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Episodes=({episodes, labels})=>(
  <div className="episodes data-set">
    <span className="data-label">{labels.episodes}</span>
    <span className="data-value">{episodes}</span>
  </div>
);

Episodes.propTypes={
  episodes: PropTypes.number,
  labels: PropTypes.object.isRequired
};

export default Episodes;
