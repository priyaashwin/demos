'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Longest=({longestDuration, labels})=>(
  <div className="longest data-set">
    <span className="data-label">{labels.longestDuration}</span>
    <span className="data-value">{longestDuration}</span>
  </div>
);

Longest.propTypes={
  longestDuration: PropTypes.number,
  labels: PropTypes.object.isRequired
};

export default Longest;
