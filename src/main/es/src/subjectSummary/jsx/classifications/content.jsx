'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Duration from './duration';
import Episodes from './episodes';
import Longest from './longest';


const Content=({classification, labels})=>(
  <div className="card-cols card-3-cols">
    <div className="data-item">
      <Duration totalDuration={classification.totalDuration} labels={labels} />
    </div>
    <div className="data-item">
      <Episodes episodes={classification.episodes} labels={labels} />
    </div>
    <div className="data-item">
      <Longest longestDuration={classification.maxDuration} labels={labels} />
    </div>
  </div>
);

Content.propTypes={
  classification: PropTypes.object,
  labels: PropTypes.object.isRequired
};

export default Content;
