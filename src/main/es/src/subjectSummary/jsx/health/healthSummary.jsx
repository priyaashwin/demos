'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../../loading/loading';
import Model from '../../../model/model';
import { getCancelToken } from 'usp-xhr';
import endpoint from '../../js/cfg';
import ErrorMessage from '../../../error/error';

export default class HealthSummary extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        url: PropTypes.string.isRequired,
        healthSummary: PropTypes.shape({
            url: PropTypes.string,
            errorTitle: PropTypes.string,
            errorDetail: PropTypes.string,
            errorStatus: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        }).isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired
    };
    state = {
        loading: false,
        url: {},
        errors: null
    };

    componentDidMount() {
        this.loadData();
    }
    componentWillUnmount() {
        if (this.cancelToken) {
            //cancel any outstanding xhr requests
            this.cancelToken.cancel();

            //remove property
            delete this.cancelToken;
        }
    }
    clearErrors = () => {
        this.setState({
            errors: null
        });
    };

    loadData() {
        const { url, subject } = this.props;
        const { subjectType, subjectId } = subject;

        //if there are any outstanding requests cancel them
        if (this.cancelToken) {
            //cancel any existing requests
            this.cancelToken.cancel();
        }

        //setup a new cancel token
        this.cancelToken = getCancelToken();

        this.setState(
            {
                loading: true
            },
            () => {
                new Model(url)
                    .load(
                        {
                            ...endpoint.personHealthSummaryEndpoint
                        },
                        {
                            subjectId: subjectId,
                            subjectType: (subjectType || '').toLowerCase()
                        },
                        this.cancelToken
                    )
                    .then(response =>
                        this.setState({
                            loading: false,
                            errors: null,
                            healthSummary: response
                        })
                    )
                    .catch(reject => {
                        this.setState({
                            loading: false,
                            errors: reject,
                            healthSummary: {}
                        });
                    });
            }
        );
    }

    render() {
        const { loading, errors, healthSummary } = this.state;
        const {labels} = this.props;
        let content;

        if (loading) {
            content = <Loading />;
        } else {
            if (errors) {
                content = <ErrorMessage errors={errors} onClose={this.clearErrors} />;
            } else {
                if (healthSummary) {
                    if (healthSummary.url) {
                        content = <iframe className="healthsummary-iframe" src={healthSummary.url} />;
                    } else {
                        if (healthSummary.errorTitle) {
                            content = (
                                <div>
                                    <ul>
                                        <h4>
                                            {labels.errorStatus} {healthSummary.errorStatus}
                                        </h4>
                                        <h4>
                                            {labels.errorTitle} {healthSummary.errorTitle}
                                        </h4>
                                        <h4>
                                            {labels.errorDetail} {healthSummary.errorDetail}
                                        </h4>
                                    </ul>
                                </div>
                            );
                        } else {
                            content = (
                                <div>
                                    <h4>{labels.serviceUnavailable}</h4>
                                </div>
                            );
                        }
                    }
                }
            }
        }

        return <div className="healthsummary-content">{content}</div>;
    }
}
