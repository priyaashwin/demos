'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FixedWidget from '../widget/fixed';
import HealthSummary from './healthSummary';

export default class HealthView extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired,
        healthTabConfig: PropTypes.shape({
            url: PropTypes.string.isRequired
        })
    };

    getContent() {
        const {labels, healthTabConfig, subject } = this.props;
        return (
            <div className="healthsummary-container">
                <HealthSummary key="healthSummary" labels={labels} subject={subject} url={healthTabConfig.url} />
            </div>
        );
    }
    render() {
        return <FixedWidget borderless="true">{this.getContent()}</FixedWidget>;
    }
}
