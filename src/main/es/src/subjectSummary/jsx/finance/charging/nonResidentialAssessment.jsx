'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import Table from '../../../../table/jsx/table';
import memoize from 'memoize-one';
import { currency, date, yesNo } from '../../../../table/js/formatter';
import { withActiveFilter } from '../hoc/withFilter';
import { withDataLoad } from '../hoc/withDataLoad';
import SummaryRow from './summaryRow';
import {idColFormatter} from '../../../js/finance/idColFormatters';

export default withActiveFilter(withDataLoad(class NonResidentialAssessment extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        results: PropTypes.array,
        loading: PropTypes.bool
    };

    getColumns = memoize((labels) => (
        [{
            key: 'id',
            label: labels.id,
            formatter: idColFormatter,
            width: '15%'
        }, {
            key: 'fromDate',
            label: labels.fromDate,
            formatter: date,
            width: '20%'
        }, {
            key: 'toDate',
            label: labels.toDate,
            formatter: date,
            width: '20%'
        }, {
            key: 'availableIncome',
            label: labels.availableIncome,
            formatter: currency,
            width: '20%'
        }, {
            key: 'totalPayable',
            label: labels.totalPayable,
            formatter: currency,
            width: '20%'
        }, {
            key: 'overridden',
            label: labels.overridden,
            formatter: yesNo,
            width: '5%'
        }]
    ));
    getSummaryRowConfig = memoize((labels) => (
        {
            enabled: true,
            formatter: function (record) {
                return (
                    <SummaryRow record={record} labels={labels} />
                );
            }
        }
    ));
    getTable() {
        const { loading, results, labels } = this.props;
        const { columns: columnLabels } = labels;

        const columns = this.getColumns(columnLabels);
        const summaryRowConfig = this.getSummaryRowConfig(labels);
        
        return (
            <Table
                key="results-table"
                columns={columns}
                data={results}
                loading={loading}
                summaryRow={summaryRowConfig}
            />
        );
    }
    render() {
        return (
            <>
                {this.getTable()}
            </>
        );
    }
}, endpoint.nonResidentialAssessmentEndpoint, true));