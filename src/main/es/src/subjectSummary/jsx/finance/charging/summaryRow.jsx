import React from 'react';
import PropTypes from 'prop-types';
import {defaultFormatter} from '../../../../table/js/formatter';

const SummaryRow = ({ labels, record }) => (
    <div className="summary-data">
        <ul>
            <li>
                <div className="data-label">{labels.reasonForOverriding}</div>
                <div className="data-value">{defaultFormatter(record.reasonForOverriding)}</div>
            </li>
            <li>
                <div className="data-label">{labels.notes}</div>
                <div className="data-value">{defaultFormatter(record.notes)}</div>
            </li>           
        </ul>
    </div>
);

SummaryRow.propTypes = {
    labels: PropTypes.object.isRequired,
    record: PropTypes.object.isRequired
};

export default SummaryRow;
