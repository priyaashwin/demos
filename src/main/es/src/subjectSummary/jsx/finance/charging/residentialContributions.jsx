'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import Table from '../../../../table/jsx/table';
import memoize from 'memoize-one';
import { currency, date } from '../../../../table/js/formatter';
import { withActiveFilter } from '../hoc/withFilter';
import { withDataLoad } from '../hoc/withDataLoad';
import Total from '../total';
import {idColFormatter} from '../../../js/finance/idColFormatters';

export default withActiveFilter(withDataLoad(class ResidentialContributions extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        results: PropTypes.array,
        loading: PropTypes.bool
    };

    getColumns = memoize((labels) => (
        [{
            key: 'id',
            label: labels.id,
            formatter: idColFormatter,
            width: '15%'
        }, {
            key: 'type',
            label: labels.type,
            width: '25%'
        }, {
            key: 'startDate',
            label: labels.startDate,
            formatter: date,
            width: '20%'
        }, {
            key: 'endDate',
            label: labels.endDate,
            formatter: date,
            width: '20%'
        }, {
            key: 'weeklyContribution',
            label: labels.weeklyContribution,
            formatter: currency,
            width: '20%'
        }]
    ));

    getTable() {
        const { loading, results, labels } = this.props;
        const { columns: columnLabels } = labels;

        const columns = this.getColumns(columnLabels);
  
        return (
            <>
                <Table
                    key="results-table"
                    columns={columns}
                    data={results}
                    loading={loading}
                />
                {this.getTotal()}
            </>
        );
    }

    getTotal() {
        const { labels, results } = this.props;

        const total = results.filter(result => result.active === true).reduce((total, result) => total + (result.weeklyContribution), 0);

        return (
            <Total
                label={labels.weeklyContributionTotal}
                total={total}
            />
        );
    }
    render() {
        return (
            <>
                {this.getTable()}
            </>
        );
    }
}, endpoint.residentialContributionEndpoint, true));