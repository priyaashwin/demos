'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../../widget/scrollable';
import Header from '../../widget/header';
import { TabView, Tab } from '../../../../tabs/jsx/tabView';
import ResidentialContribution from './residentialContributions';
import NonResidentialAssessment from './nonResidentialAssessment';

export default class Charging extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired,
        residentialContribution: PropTypes.shape({
            labels: PropTypes.object.isRequired,
            searchConfig: PropTypes.shape({
                url: PropTypes.string.isRequired
            })
        }),
        nonResidentialAssessment: PropTypes.shape({
            labels: PropTypes.object.isRequired,
            searchConfig: PropTypes.shape({
                url: PropTypes.string.isRequired
            })
        })
    };
    getContent() {
        const { residentialContribution, nonResidentialAssessment, subject } = this.props;
        const { labels: residentialContributionLabels, searchConfig: residentialContributionSearchConfig } = residentialContribution;
        const { labels: nonResidentialAssessmentLabels, searchConfig: nonResidentialAssessmentSearchConfig } = nonResidentialAssessment;
        return (
            <>
                <TabView key="roleTabs">
                    <Tab key="residentialContribution" label={residentialContributionLabels.header.title} tabId="residentialContribution">
                        <ResidentialContribution
                            key="residentialContributionTable"
                            labels={residentialContributionLabels}
                            subject={subject}
                            searchConfig={residentialContributionSearchConfig}
                        />
                    </Tab>
                    <Tab key="nonResidentialAssessment" label={nonResidentialAssessmentLabels.header.title} tabId="nonResidentialAssessment">
                        <NonResidentialAssessment
                            key="nonResidentialAssessmentTable"
                            labels={nonResidentialAssessmentLabels}
                            subject={subject}
                            searchConfig={nonResidentialAssessmentSearchConfig}
                        />
                    </Tab>
                </TabView>
            </>
        );
    }
    render() {
        const { labels } = this.props;

        const header = (<Header title={labels.header.title} />);

        return (
            <ScrollableWidget className="charging-widget" header={header}>
                {this.getContent()}
            </ScrollableWidget>
        );
    }
}