'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import Table from '../../../../table/jsx/table';
import memoize from 'memoize-one';
import { currency, date } from '../../../../table/js/formatter';
import { withActiveFilter } from '../hoc/withFilter';
import { withDataLoad } from '../hoc/withDataLoad';

export default withActiveFilter(withDataLoad(class PersonalBudgetResults extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        results: PropTypes.array,
        loading: PropTypes.bool
    };

    getColumns = memoize((labels) => (
        [{
            key: 'type',
            label: labels.type,
            width: '15%'
        }, {
            key: 'startDate',
            label: labels.startDate,
            formatter: date,
            width: '20%'
        }, {
            key: 'endDate',
            label: labels.endDate,
            formatter: date,
            width: '20%'
        }, {
            key: 'weeklyAmount',
            label: labels.weeklyAmount,
            formatter: currency,
            width: '15%'
        }, {
            key: 'yearlyAmount',
            label: labels.yearlyAmount,
            formatter: currency,
            width: '20%'
        }]
    ));

    getTable() {
        const { loading, results, labels } = this.props;
        const { columns: columnLabels } = labels;

        const columns = this.getColumns(columnLabels);
        return (
            <Table
                key="results-table"
                columns={columns}
                data={results}
                loading={loading}
            />
        );
    }
    render() {
        return (
            <>
                {this.getTable()}
            </>
        );
    }
}, endpoint.personalBudgetEndpoint, true));