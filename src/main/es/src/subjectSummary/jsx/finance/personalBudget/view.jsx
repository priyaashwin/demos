'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../../widget/scrollable';
import Header from '../../widget/header';
import PersonalBudgetResults from './results';

export default class PersonalBudget extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired,
        searchConfig: PropTypes.shape({
            url: PropTypes.string.isRequired
        })
    };

    render() {
        const { labels, ...other } = this.props;

        const header = (<Header title={labels.header.title} />);

        return (
            <ScrollableWidget className="personalbudget-widget" header={header}>
                <PersonalBudgetResults
                    labels={labels}
                    {...other}
                />
            </ScrollableWidget>
        );
    }
}