'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';

/**
 * Wraps the supplied component with a simple
 * activeOnly filter display and injects
 * an activeOnly property into that component
 * 
 * @param {*} WrappedComponent 
 */
export function withActiveFilter(WrappedComponent) {
  class WidgetWithFilter extends PureComponent {
    static propTypes = {
      labels: PropTypes.shape({
        filter: PropTypes.object.isRequired
      }).isRequired,
      forwardedRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.instanceOf(Element) })
      ])
    };

    state = {
      //default state is show active only
      activeOnly: true
    };

    handleFilterClick = (e) => {
      e.preventDefault();


      this.setState((state) => ({
        activeOnly: !state.activeOnly
      }));
    };

    getFilter() {
      const { labels } = this.props;
      const { activeOnly } = this.state;
      const { filter } = labels;
      let filterLink;
      if (activeOnly) {
        filterLink = (<span>{filter.showAll}</span>);
      } else {
        filterLink = (<span>{filter.activeOnly}</span>);
      }

      return (
        <div className="filter" key="filter">
          <div>
            <a href="#none" className="pure-button pure-button-active" onClick={this.handleFilterClick}>
              <i className="fa fa-filter" aria-hidden="true" title={filterLink} />
              {filterLink}
            </a>
          </div>
        </div>
      );
    }

    render() {
      const { forwardedRef, ...other } = this.props;
      const { activeOnly } = this.state;

      return (
        <>
          {this.getFilter()}
          <WrappedComponent
            ref={forwardedRef}
            {...other}
            activeOnly={activeOnly} />
        </>
      );
    }
  }

  WidgetWithFilter.displayName = `withFilter(${getDisplayName(WrappedComponent)})`;

  return forwardRef((props, ref) => {
    return <WidgetWithFilter {...props} forwardedRef={ref} />;
  });
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
