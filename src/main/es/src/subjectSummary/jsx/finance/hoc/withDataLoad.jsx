'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { getCancelToken } from 'usp-xhr';
import ModelList from '../../../../model/modelList';
import { customErrorHandler } from '../../../js/finance/errorHandler';
import ErrorMessage from '../../../../error/error';

/**
 * Wraps the supplied component with a dataLoad function
 * and injects a results parameter containing the result
 * of the data load into that component.
 * 
 * Also acts as an error boundary for the wrapped component
 * and handles error output.
 * 
 * @param {*} WrappedComponent 
 * @param {object} endpoint  Endpoint configuration object
 * @param {boolean} filtered Flag to indicate that "activeOnly" filter parameter is required
 */
export function withDataLoad(WrappedComponent, endpoint, filtered) {
    class WidgetWithDataLoad extends PureComponent {
        static propTypes = {
            labels: PropTypes.object.isRequired,
            activeOnly: PropTypes.bool,
            subject: PropTypes.shape({
                subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
                subjectType: PropTypes.string
            }).isRequired,
            searchConfig: PropTypes.shape({
                url: PropTypes.string.isRequired
            }),
            forwardedRef: PropTypes.oneOfType([
                PropTypes.func,
                PropTypes.shape({ current: PropTypes.instanceOf(Element) })
            ])
        };

        state = {
            loading: true,
            errors: null,
            results: []
        };

        componentDidMount() {
            this.loadData();
        }
        componentWillUnmount() {
            if (this.cancelToken) {
                //cancel any outstanding xhr requests
                this.cancelToken.cancel();

                //remove property
                delete this.cancelToken;
            }
        }
        componentDidUpdate(prevProps) {
            if (prevProps.activeOnly !== this.props.activeOnly) {
                //if the active state has changed then trigger a re-load of data
                this.loadData();
            }
        }
        componentDidCatch() {
            this.setState({
                errors: 'Unexpected error'
            });
        }

        clearErrors = () => {
            this.setState({
                errors: null
            });
        };

        loadData() {
            const { searchConfig, subject, labels, activeOnly } = this.props;
            const { url } = searchConfig;
            const { subjectType, subjectId } = subject;

            //if there are any outstanding requests cancel them
            if (this.cancelToken) {
                //cancel any existing requests
                this.cancelToken.cancel();
            }

            //define parameters - clone any existing endpoint parameters
            const params = {
                ...endpoint.params
            };

            if (filtered) {
                params.activeOnly = activeOnly;
            }
            //setup a new cancel token
            this.cancelToken = getCancelToken();

            this.setState({
                loading: true
            }, () => {
                new ModelList(url).load({
                    ...endpoint,
                    params: {
                        ...params
                    }
                }, {
                        subjectId: subjectId,
                        subjectType: (subjectType || '').toLowerCase()
                    },
                    this.cancelToken
                ).then(response => this.setState({
                    loading: false,
                    errors: null,
                    results: response.results
                })).catch(reject => {
                    this.setState({
                        loading: false,
                        errors: customErrorHandler(reject, labels.errors),
                        results: []
                    });
                });
            });
        }

        render() {
            const { forwardedRef, ...other } = this.props;
            const { results, errors, loading } = this.state;

            let content;
            if (errors) {
                content = (<ErrorMessage errors={errors} onClose={this.clearErrors} />);
            } else {
                content = (<WrappedComponent
                    ref={forwardedRef}
                    {...other}
                    results={results}
                    loading={loading} />);
            }
            return (
                <>
                    {content}
                </>
            );
        }
    }

    WidgetWithDataLoad.displayName = `withDataLoad(${getDisplayName(WrappedComponent)})`;

    return forwardRef((props, ref) => {
        return <WidgetWithDataLoad {...props} forwardedRef={ref} />;
    });
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
