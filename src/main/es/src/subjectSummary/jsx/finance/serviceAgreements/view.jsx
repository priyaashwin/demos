'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../../widget/scrollable';
import Header from '../../widget/header';
import ServiceAgreementResults from './results';

export default class ServiceAgreements extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired,
        searchConfig: PropTypes.shape({
            url: PropTypes.string.isRequired
        })
    };

    render() {
        const { labels, ...other } = this.props;

        const header = (<Header title={labels.header.title} />);

        return (
            <ScrollableWidget className="serviceagreements-widget" header={header}>
                <ServiceAgreementResults
                    labels={labels}
                    {...other}
                />
            </ScrollableWidget>
        );
    }
}