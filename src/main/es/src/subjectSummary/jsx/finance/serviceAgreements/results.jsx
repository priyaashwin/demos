'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import Table from '../../../../table/jsx/table';
import memoize from 'memoize-one';
import SummaryRow from './summaryRow';
import { currency, yesNo } from '../../../../table/js/formatter';
import { withActiveFilter } from '../hoc/withFilter';
import { withDataLoad } from '../hoc/withDataLoad';
import Total from '../total';

export default withActiveFilter(withDataLoad(class ServiceAgreementResults extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        results: PropTypes.array,
        loading: PropTypes.bool
    };

    getColumns = memoize((labels) => (
        [{
            key: 'partyName',
            label: labels.partyName,
            width: '20%'
        }, {
            key: 'type',
            label: labels.type,
            width: '20%'
        }, {
            key: 'service',
            label: labels.service,
            width: '20%'
        }, {
            key: 'active',
            label: labels.active,
            formatter: yesNo,
            width: '10%'
        }, {
            key: 'weeklyCommitmentAmount',
            label: labels.weeklyCommitmentAmount,
            formatter: currency,
            width: '15%'
        }, {
            key: 'yearlyCommitmentAmount',
            label: labels.yearlyCommitmentAmount,
            formatter: currency,
            width: '15%'
        }
        ]
    ));
    getSummaryRowConfig = memoize((labels) => (
        {
            enabled: true,
            formatter: function (record) {
                return (
                    <SummaryRow record={record} labels={labels} />
                );
            }
        }
    ));

    getTable() {
        const { loading, results, labels } = this.props;
        const { columns: columnLabels } = labels;

        const columns = this.getColumns(columnLabels);
        const summaryRowConfig = this.getSummaryRowConfig(labels);
        return (
            <>
                <Table
                    key="results-table"
                    columns={columns}
                    data={results}
                    loading={loading}
                    summaryRow={summaryRowConfig}
                />
                {this.getTotal()}
            </>
        );
    }
    getTotal() {
        const { labels, results } = this.props;

        const total = results.filter(result => result.active === true).reduce((total, result) => total + (result.yearlyCommitmentAmount), 0);

        return (
            <Total
                label={labels.yearlyCommitmentTotal}
                total={total} />
        );
    }
    render() {
        return (
            <>
                {this.getTable()}
            </>
        );
    }
}, endpoint.serviceAgreementsEndpoint, true));