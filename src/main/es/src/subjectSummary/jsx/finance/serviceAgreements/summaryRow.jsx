import React from 'react';
import PropTypes from 'prop-types';
import { formatDate } from '../../../../date/date';
import { defaultFormatter } from '../../../../table/js/formatter';

const SummaryRow = ({ labels, record }) => (
    <div className="summary-data">
        <ul>
            <li>
                <span className="data-label">{labels.serviceId}</span>
                <span className="data-value">{defaultFormatter(record.id)}</span>
            </li>
            <li>
                <span className="data-label">{labels.accountCode}</span>
                <span className="data-value">{defaultFormatter(record.accountCode)}</span>
            </li>
            <li>
                <span className="data-label">{labels.startDate}</span>
                <span className="data-value">{formatDate(record.startDate)}</span>
            </li>
            <li>
                <span className="data-label">{labels.endDate}</span>
                <span className="data-value">{formatDate(record.endDate)}</span>
            </li>
        </ul>
    </div>
);

SummaryRow.propTypes = {
    labels: PropTypes.object.isRequired,
    record: PropTypes.object.isRequired
};

export default SummaryRow;
