'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Total=({total, label})=>
(
    <div className="total">
        <div className="name">
            {label}
        </div>
        <div className="value">
            {new Intl.NumberFormat('en-GB', { style: 'currency', currency: 'GBP' }).format(total)}
        </div>
    </div>
);

Total.propTypes={
    total: PropTypes.number,
    label: PropTypes.string.isRequired
};

Total.defaultProps={
    total:0
};

export default Total;