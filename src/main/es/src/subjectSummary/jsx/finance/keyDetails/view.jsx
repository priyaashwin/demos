'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../../widget/scrollable';
import Header from '../../widget/header';
import References from './references';
import Representatives from './representatives';

export default class KeyDetails extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired,
        searchConfig: PropTypes.shape({
            url: PropTypes.string.isRequired
        }),
        referencesConfig: PropTypes.shape({
            url: PropTypes.string.isRequired
        }),
    };
    getContent() {
        const { labels, referencesConfig, searchConfig, subject } = this.props;

        return (
            <>
                <div>
                    <References
                        key="references"
                        labels={labels}
                        subject={subject}
                        url={referencesConfig.url}
                    />
                </div>
                <div className="sub-header">
                    <h3>{labels.financialRepresentative}</h3>
                </div>
                <div>
                    <Representatives
                        key="Representatives"
                        labels={labels}
                        subject={subject}
                        searchConfig={searchConfig}
                    />
                </div>
            </>
        );
    }
    render() {
        const { labels } = this.props;

        const header = (<Header title={labels.header.title} />);

        return (
            <ScrollableWidget  className="keydetails-widget" header={header}>
                {this.getContent()}
            </ScrollableWidget>
        );
    }
}