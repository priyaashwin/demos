'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Loading from '../../../../loading/loading';
import Model from '../../../../model/model';
import { getCancelToken } from 'usp-xhr';
import endpoint from '../../../js/cfg';
import ErrorMessage from '../../../../error/error';
import { customErrorHandler } from '../../../js/finance/errorHandler';

export default class References extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        url: PropTypes.string.isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired,
    };
    state = {
        loading: false,
        financialReferences: {}
    }

    componentDidMount() {
        this.loadData();
    }
    componentWillUnmount() {
        if (this.cancelToken) {
            //cancel any outstanding xhr requests
            this.cancelToken.cancel();

            //remove property
            delete this.cancelToken;
        }
    }

    clearErrors = () => {
        this.setState({
            errors: null
        });
    };

    loadData() {
        const { url, subject, labels } = this.props;
        const { subjectType, subjectId } = subject;

        //if there are any outstanding requests cancel them
        if (this.cancelToken) {
            //cancel any existing requests
            this.cancelToken.cancel();
        }

        //setup a new cancel token
        this.cancelToken = getCancelToken();

        this.setState({
            loading: true
        }, () => {
            new Model(url).load({
                ...endpoint.financialReferencesEndpoint
            }, {
                    subjectId: subjectId,
                    subjectType: (subjectType || '').toLowerCase()
                }, this.cancelToken).then(response => this.setState({
                    loading: false,
                    errors: null,
                    financialReferences: response
                })).catch(reject => {
                    this.setState({
                        loading: false,
                        errors: customErrorHandler(reject, labels.errors),
                        financialReferences: {}
                    });
                });
        });
    }

    render() {
        const { labels } = this.props;
        const { loading, financialReferences, errors } = this.state;

        let content;

        if (errors) {
            content = (<ErrorMessage errors={errors} onClose={this.clearErrors} />);
        } else {
            if (loading) {
                content = (<Loading />);
            } else {
                content = (
                    <div key="details" className="pure-g-r reference-numbers">
                        <div className="pure-u-1-2 l-box">
                            <div className="name">
                                {labels.creditorReference}
                            </div>
                            <div className="value">
                                {financialReferences.creditorReference}
                            </div>
                        </div>
                        <div className="pure-u-1-2 l-box">
                            <div className="name">
                                {labels.debtorReference}
                            </div>
                            <div className="value">
                                {financialReferences.debtorReference}
                            </div>
                        </div>
                    </div>
                );
            }
        }

        return (
            <div>
                {content}
            </div>
        );
    }
}