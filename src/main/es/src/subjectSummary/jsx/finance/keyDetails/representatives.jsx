'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import Table from '../../../../table/jsx/table';
import memoize from 'memoize-one';
import SummaryRow from './summaryRow';
import { yesNo } from '../../../../table/js/formatter';
import { withActiveFilter } from '../hoc/withFilter';
import { withDataLoad } from '../hoc/withDataLoad';
import {idColFormatter} from '../../../js/finance/idColFormatters';

export default withActiveFilter(withDataLoad(class Representatives extends Component {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        results: PropTypes.array,
        loading: PropTypes.bool
    };

    getColumns = memoize((labels) => (
        [{
            key: 'representativeId',
            label: labels.representativeId,
            formatter: idColFormatter,
            width: '15%'
        }, {
            key: 'name',
            label: labels.name,
            width: '20%'
        }, {
            key: 'address',
            label: labels.address,
            width: '20%'
        }, {
            key: 'active',
            label: labels.active,
            formatter: yesNo,
            width: '10%'
        }]
    ));
    getSummaryRowConfig = memoize((labels) => (
        {
            enabled: true,
            formatter: function (record) {
                return (
                    <SummaryRow record={record} labels={labels} />
                );
            }
        }
    ));


    getTable() {
        const { loading, results, labels } = this.props;
        const { columns: columnLabels } = labels;

        const columns = this.getColumns(columnLabels);
        const summaryRowConfig = this.getSummaryRowConfig(labels);
        return (
            <Table
                key="results-table"
                columns={columns}
                data={results}
                loading={loading}
                summaryRow={summaryRowConfig}
            />
        );
    }

    render() {
        return (
            <>
                {this.getTable()}
            </>
        );
    }
}, endpoint.financialRepresentativesEndpoint, true));