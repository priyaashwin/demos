'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Widget from './widget';
import NativeScrollList from './core/nativeScrollList';
import {findDOMNode} from 'react-dom';
import classnames from 'classnames';
import debounce from 'just-debounce';

export default class ScrollableWidget extends PureComponent {
  static propTypes = {
      className: PropTypes.string,
      header:  PropTypes.node,
      children: PropTypes.node
  };

  constructor(props){
    super(props);

    this.state={
      isOverflow:true
    };

    this.handleScroll=this.handleScroll.bind(this);
    this.updateOverflow=debounce(()=>this._updateOverflow(), 250);
  }
  componentDidMount(){
    let supportsPassive = false;
    try {
      const opts = Object.defineProperty({}, 'passive', {
        get: function() {         //eslint-disable-line getter-return
          supportsPassive = true;
        }
      });
      window.addEventListener('test', null, opts);
      window.removeEventListener('test');
    } catch (e) {
      //ignore
    }

    this.scrollElem.addEventListener('scroll', this.handleScroll, supportsPassive ? { passive: true } : false);

    this.updateOverflow();
  }
  componentWillUnmount(){
    this.scrollElem.removeEventListener('scroll', this.handleScroll);
  }
  componentDidUpdate(){
    this.updateOverflow();
  }
  isOverflow(){
    const elem=this.scrollElem;
    if(elem && ((elem.clientHeight < elem.scrollHeight) && (elem.offsetHeight+elem.scrollTop < elem.scrollHeight-15))){
        return true;
    }

    return false;
  }
  _updateOverflow(){
    this.setState({
      isOverflow:this.isOverflow()
    });
  }
  handleScroll(){
    this.updateOverflow();
  }
  render() {
    const {children, ...other}=this.props;

    return (
      <Widget {...other}>
        <div className={classnames('widget-scrollable', {
            overflow:this.state.isOverflow
          })}>
          <NativeScrollList
            key="scroll__list"
            ref={(elem) => { this.scrollElem = findDOMNode(elem); }}>
            {children}
          </NativeScrollList>
        </div>
      </Widget>
    );
  }
}
