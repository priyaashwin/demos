'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Widget extends PureComponent {
  render() {
    const {header, children, borderless, className, ...other}=this.props;
    return (
      <div className={classnames('widget-element', className, {
          'no-border':borderless
        })} {...other} tabIndex={0}>
        {header}
        {children}
      </div>
    );
  }
}
Widget.propTypes = {
    className: PropTypes.string,
    header:  PropTypes.node,
    children: PropTypes.node,
    borderless: PropTypes.bool
};
