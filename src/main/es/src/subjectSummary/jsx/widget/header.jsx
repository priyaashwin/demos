'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Header=({title, children})=>(
  <div className="widget-header"><h3>{title}</h3>{children}</div>
);

Header.propTypes={
  title: PropTypes.string,
  children: PropTypes.node
};

export default Header;
