import React from 'react';
import PropTypes from 'prop-types';
import BaseScrollList from './baseScrollList';

export default class ScrollList extends BaseScrollList {
  constructor(props){
    super(props);
    this.handleTouchStart=this.handleTouchStart.bind(this);
    this.handleTouchEnd=this.handleTouchEnd.bind(this);
    this.handleTouchMove=this.handleTouchMove.bind(this);
    this.handleTouchCancel=this.handleTouchCancel.bind(this);

    this.handlePan=this.handlePan.bind(this);
    this.shouldPan=this.shouldPan.bind(this);
  }
  handlePan(y){
    const dy=this.startY-y;
    if(this.shouldPan(dy)){
      //update y
      this.startY=y;
      //execute the handler
      this.props.handlePan(dy);
    }
  }
  shouldPan(dy){
    return this.props.shouldPan(dy);
  }
  handleTouchStart(e){
    let invalidTouch=false;

    //check touches still contains touchId
    if(this.touchId!==undefined){
      const touch=this.findTouch(e.touches, this.touchId);
      if(!touch){
        //touch is now invalid
        invalidTouch=true;
      }
    }
    if(!this.moveInProgress||invalidTouch){
      //take the first changed touch event to indicate start of move
      const touch = e.changedTouches[0];
      //keep track of the id
      const touchId=touch.identifier;


      const target=touch.target;
      //bind the touch handlers we need
      target.addEventListener('touchmove', this.handleTouchMove);
      target.addEventListener('touchend', this.handleTouchEnd);
      target.addEventListener('touchcancel', this.handleTouchCancel);

      this.moveInProgress=true;
      //track the touchId
      this.touchId=touchId;

      this.startY=parseInt(touch.clientY, 10);
    }
  }
  findTouch(touches, touchId){
    let touch;
    for(let i=0; i<touches.length; i++){
      if(touches[i].identifier===touchId){
        touch=touches[i];
        break;
      }
    }

    return touch;
  }
  handleTouchMove(e){
    const touchId=this.touchId;
    if(this.moveInProgress && touchId!==undefined){
      e.preventDefault();

      //find the changed touch
      const touch = this.findTouch(e.changedTouches, touchId);
      if(touch){
        this.handlePan(parseInt(touch.clientY, 10));
      }
    }
  }
  handleTouchEnd(e){
    const touchId=this.touchId;
    if(this.moveInProgress && touchId!==undefined){
      const touch = this.findTouch(e.changedTouches, touchId);
      if(touch){
        this.handleTouchEnded(touch);
      }
    }
  }
  handleTouchCancel(e){
    const touchId=this.touchId;
    if(this.moveInProgress && touchId!==undefined){
      const touch = this.findTouch(e.changedTouches, touchId);
      if(touch){
        this.handleTouchEnded(touch);
      }
    }
  }
  handleTouchEnded(touch){
    const target=touch.target;

    //bind the touch handlers we need
    target.removeEventListener('touchMove', this.handleTouchMove);
    target.removeEventListener('touchEnd', this.handleTouchEnd);
    target.removeEventListener('touchCancel', this.handleTouchCancel);

    //end the move
    this.moveEnd();
  }

  render(){
    const {children, handlePan:handlePanIgnored, shouldPan:shouldPanIgnored, ...other}=this.props;

    return (
      <div
        key="scrollList"
        onMouseDown={this.handleMouseDown}
        onTouchStart={this.handleTouchStart}
        {...other}
        >
        {children}
      </div>
    );
  }
}

ScrollList.propTypes = {
  children: PropTypes.node,
  shouldPan: PropTypes.func.isRequired,
  handlePan: PropTypes.func.isRequired
};
