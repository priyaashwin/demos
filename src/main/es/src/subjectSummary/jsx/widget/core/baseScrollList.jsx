'use strict';
import {PureComponent} from 'react';

export default class BaseScrollList extends PureComponent {
  constructor(props){
    super(props);

    this.handleMouseDown=this.handleMouseDown.bind(this);
    this.handleMouseUp=this.handleMouseUp.bind(this);
    this.handleMouseMove=this.handleMouseMove.bind(this);
    this.handleMouseCancel=this.handleMouseCancel.bind(this);
  }
  handleMouseDown(e){
    let button;
    if(e.buttons!==undefined){
      button=e.buttons;
    }else{
      button=e.nativeEvent.which||0;
    }
    if(button == 1){
      this.moveInProgress=true;
      this.startY = parseInt(e.clientY,10);
      e.preventDefault();
      
      e.currentTarget.addEventListener('mousemove', this.handleMouseMove);
      document.addEventListener('mouseup', this.handleMouseUp);
    }
  }
  handleMouseMove(e){
    if(this.moveInProgress){
      e.preventDefault();
      this.handlePan(parseInt(e.clientY, 10));
    }
  }
  shouldPan(){
    //override in subclass
    return false;
  }
  handlePan(){
    //override in subclass
  }

  handleMouseUp(e){
    const target=e.currentTarget;
    target.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
    this.moveEnd();
  }
  handleMouseCancel(e){
    const target=e.currentTarget;
    target.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
    this.moveEnd();
  }
  moveEnd(){
    if(this.moveInProgress){
      this.moveInProgress=null;
    }
  }
}
