'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import BaseScrollList from './baseScrollList';

export default class NativeScrollList extends BaseScrollList {
  constructor(props){
    super(props);

    this.handlePan=this.handlePan.bind(this);
    this.shouldPan=this.shouldPan.bind(this);
  }
  shouldPan(dy){
    const elem=this.containerElem;

    let handle=false;
    if(dy>0 && ((elem.offsetHeight+elem.scrollTop) < elem.scrollHeight)){
      handle=true;
    }else if(dy<0 && elem.scrollTop>0){
      handle=true;
    }

    return handle;
  }

  handlePan(y){
    const dy=this.startY-y;
    if(this.shouldPan(dy)){
      //update y
      this.startY=y;
      //perform the pan

      const elem=this.containerElem;
      if(elem){
        elem.scrollTop=elem.scrollTop+dy;
      }
    }
  }
  render() {
    const {children, className, ...other}=this.props;

    return (
      <div
        onMouseDown={this.handleMouseDown}
        {...other}
        key="scollable_area"
        ref={(elem) => { this.containerElem = elem; }}
        className={classnames('widget-scrollable-content', className)}
        tabIndex={0}>
        <div className="native-scroll">{children}</div>
      </div>
    );
  }
}
NativeScrollList.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string
};
