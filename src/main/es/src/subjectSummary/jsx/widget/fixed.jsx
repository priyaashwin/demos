'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Widget from './widget';

export default class FixedWidget extends Component {
    render() {
      const {children, ...other}=this.props;

      return (
        <Widget {...other}>
          {children}
        </Widget>
      );
    }
}
FixedWidget.propTypes = {
    className: PropTypes.string,
    header:  PropTypes.node,
    children: PropTypes.node
};
