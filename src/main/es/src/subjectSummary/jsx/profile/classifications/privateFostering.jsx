import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import ModelList from '../../../../model/modelList';
import Error from '../../../../error/error';
import classnames from 'classnames';

export default class PrivateFostering extends PureComponent {
  static propTypes = {
    classificationsUrl: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }).isRequired
  };

  state={
    loading:true,
    errors:null,
    profileClassifications:null
  };

  componentDidUpdate(prevProps) {
    if (!(prevProps.classificationsUrl === this.props.classificationsUrl
       && prevProps.subject === this.props.subject)) {
      this.loadData();
    }
  }
  componentDidMount(){
    this.loadData();
  }

  clearErrors=(e)=>{e.preventDefault(); this.setState({errors:null});}

  loadData() {
    // Load the classifications for private fostering here
    const {subject, classificationsUrl}=this.props;
    const {subjectId, subjectType}=subject;
    const modelList=new ModelList(classificationsUrl);

    modelList.load(endpoint.profileClassificationsEndpoint, { subjectId: subjectId, subjectType: subjectType }).then(result=>this.setState({
      profileClassifications:  result.results,
      loading:false,
      errors:null
    })).catch(reject=>this.setState({
      errors:reject,
      loading:false,
      profileClassifications:null
    }));
  }

  render(){
    const {errors, loading, profileClassifications=[]} = this.state;

    if (!loading && !errors) {
      if (profileClassifications.length>0){
        // Get the latest classification (sorted by startDate descending) so extract first entry
        const privatelyFosteredClassification = profileClassifications[0];

        //classification could be secured via L3
        if(privatelyFosteredClassification && !(privatelyFosteredClassification._securityMetaData||{}).secured){
          return (<div className={classnames('privately-fostered-icon',{
            'ended':privatelyFosteredClassification.endDate
          })}>{privatelyFosteredClassification.classificationVO.name}</div>);
        }
      }
    }

    if(errors){
      //supress 404 (not found) and 401 (not authorised)
      if (errors.code !== 404 && errors.code !== 401) {
        return (<Error errors={errors} onClose={this.clearErrors}/>);
      }
    }

    //no output
    return false;
  }
}
