'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import Model from '../../../../model/model';

export default class Details extends PureComponent {
  static propTypes={
    url: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }).isRequired,
  };

  state={
    loading:true,
    errors:null,
    subject:null
  };

  componentDidMount(){
    this.loadData();
  }
  componentDidUpdate(prevProps){
    if (!(prevProps.url === this.props.url && prevProps.subject === this.props.subject)) {
      this.loadData();
    }

    //Update the overflow property once the data has loaded
    if (this.containerElem) {
      this.containerElem.updateOverflow();
    }
  }
  loadData(){
    const {subject, url}=this.props;

    const model=new Model(url);
    const subjectId=subject.subjectId;
    const subjectType=(subject.subjectType||'').toLowerCase();

    model.load(endpoint.groupEndpoint, {subjectId:subjectId, subjectType:subjectType}).then(result=>this.setState({
      subject:result,
      loading:false,
      errors:null
    })).catch(reject=>this.setState({
      errors:reject,
      loading:false,
      subject:null
    }));
  }
  render(){
    const {loading, subject}=this.state;
    if(!loading){
      return(
        <div>
          <div className="widget-header">
            <h3 className="subject-name">
              {subject.name}
            </h3>
          </div>
          <div className="data-item">
            {subject.description}
          </div>
        </div>
      );
    }

    return (<div />);
  }
}
