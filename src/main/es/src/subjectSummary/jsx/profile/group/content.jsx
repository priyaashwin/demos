import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import LatestForms from '../latestForms';
import Details from './details';

export default class Content extends PureComponent {
  render(){
    const { labels, permissions, currentUserTeamId, ...other}=this.props;

    const canViewForm=(permissions||{}).canViewForm;

    let latestForms;

    if (canViewForm && currentUserTeamId){
      latestForms=(<LatestForms {...other} labels={labels.forms}/>);
    }
    return (
      <div className="summary-profile">
        <Details {...other}/>
        {latestForms}
      </div>
    );
  }
}


Content.propTypes = {
  url: PropTypes.string.isRequired,
  subject: PropTypes.shape({
    subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    subjectType: PropTypes.string
  }).isRequired,
  labels: PropTypes.object.isRequired,
  permissions:PropTypes.shape({
    canViewForm: PropTypes.bool
  }),
  currentUserTeamId: PropTypes.string
};
