import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../js/cfg';
import Model from '../../../model/model';

export default class LatestForm extends PureComponent {
  static propTypes = {
    url: PropTypes.string.isRequired,
    subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    type:PropTypes.object.isRequired,
    onLaunch:PropTypes.func.isRequired,
    labels:PropTypes.object.isRequired
  };

  state={
    form:null
  };

  componentDidMount(){
      this.loadData(this.props.type);
  }
  componentDidUpdate(prevProps) {
    if (!(prevProps.type.formDefinitionUID === this.props.type.formDefinitionUID )) {
      this.loadData(this.props.type);
    }
  }
  loadData(type){
    const {url, subjectId}=this.props;

    const formDefinitionUID=type.formDefinitionUID;

    if(formDefinitionUID){
      new Model(url).load(endpoint.latestFormInstanceEndpoint, {
        subjectId:subjectId,
        formDefinitionUID:formDefinitionUID
      }).then(form=>{
        this.setState({
          form:form
        });
      }).catch(()=>{
        this.setState({
          form:null
        });
      });
    }else{
      //no form definition configured
      this.setState({form:null});
    }
  }
  handleClick=(e)=>{
    const {form}=this.state;

    e.preventDefault();
    if(form){
      this.props.onLaunch(form.id, form.formDefinitionName);
    }
  };
  render(){
    const {type, labels}=this.props;
    const form=this.state.form;

    let content;

    const label=(<span>{type.identifier}</span>);

    if(form){
      content=(<a href="#none" onClick={this.handleClick}>{label}</a>);
    }else{
      content=(<span className="empty">{label}{' '}{labels.empty||'not found.'}</span>);
    }

    return(
      <div className="latest-form">{content}</div>
    );
  }
}
