import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import ModelList from '../../../model/modelList';
import endpoint from '../../js/cfg';
import LatestForm from './latestForm';
import ErrorMessage from '../../../error/error';
import {launch} from '../../../form/launcher';
import Header from '../widget/header';

export default class LatestForms extends PureComponent {
  static propTypes = {
    configurableTypeUrl: PropTypes.string.isRequired,
    formUrl: PropTypes.string.isRequired,
    launchFormURL: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }).isRequired,
    labels:PropTypes.object.isRequired
  };

  state={
    types:[],
    errors:null
  };

  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({errors:null});
  };
  componentDidUpdate(prevProps) {
    if (!(prevProps.configurableTypeUrl === this.props.configurableTypeUrl)) {
      this.loadData();
    }
  }
  componentDidMount(){
      this.loadData();
  }
  loadData(){
    const {configurableTypeUrl}=this.props;

    new ModelList(configurableTypeUrl).load({
        ...endpoint.formConfigurableTypeEndpoint,
        }).then(result=>{
          this.setState({
            types:result,
            errors:null
          });
        }).catch((errors)=>this.setState({
            types:[],
            errors:errors
        }));
  }
  handleLaunch=(formId, formName)=>{
    const {launchFormURL}=this.props;

    launch(launchFormURL, formId, formName);
  };
  render(){
    const {labels, subject, formUrl}=this.props;
    const subjectId=subject.subjectId;
    const types=this.state.types||[];

    let content;

    if(this.state.errors){
      content=(<div><ErrorMessage errors={this.state.errors} onClose={this.clearErrors}/></div>);
    }else{
      content=types.map(type=>(<LatestForm key={type.id} type={type} url={formUrl} subjectId={subjectId} labels={labels} onLaunch={this.handleLaunch}/>));
    }
    return(
      <div>
        <Header title={labels.header.title}/>
        <div className="latest-forms-content">
          {content}
        </div>
      </div>
    );
  }
}
