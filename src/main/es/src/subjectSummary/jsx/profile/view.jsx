'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../widget/scrollable';
import PersonContent from './person/content';
import GroupContent from './group/content';

export default class ProfileView extends Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    configurableTypeUrl: PropTypes.string.isRequired,
    formUrl: PropTypes.string.isRequired,
    launchFormURL: PropTypes.string.isRequired,
    carerApprovalSummaryUrl: PropTypes.string.isRequired,
    classificationsUrl: PropTypes.string.isRequired,
    fosterCarerRegistrationUrl: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }),
    labels: PropTypes.object.isRequired,
    permissions:PropTypes.shape({
      canView: PropTypes.bool,
      canViewForm: PropTypes.bool,
      canViewCarerApproval: PropTypes.bool,
      canViewPrivateFostering: PropTypes.bool    
    })
  };
  
  render() {
    const {subject, url, ...other}=this.props;

    let content;
    switch((subject.subjectType||'').toLowerCase()){
      case 'person':
        content=(<PersonContent key="person-profile" url={url} subject={subject} {...other}/>);
      break;
      case 'group':
        content=(<GroupContent key="group-profile" url={url} subject={subject} {...other}/>);
      break;
    }
    return (
      <ScrollableWidget tabIndex="0" borderless={true}>
        {content}
      </ScrollableWidget>
    );
  }
}
