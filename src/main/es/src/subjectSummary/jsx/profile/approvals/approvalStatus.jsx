import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const ApprovalStatus=({status})=> (
    <span>{statusDisplayName(status)} <i className={classnames('fa', statusIconClass(status))} /></span>
);

const statusDisplayName = function(status) {
  let displayName;
  switch (status.toLowerCase()) {
    case 'withdrawn':
      displayName='withdrawn';
      break;
    case 'available':
      displayName='approved';
      break;
    case 'approved':
    case 'ex_approved':
      displayName='approved';
      break;
    case 'ex_registered':
      displayName='deregistered';
      break;
    case 'suspended':
      displayName='suspended';
      break;
    case 'prospective':
    default:
      displayName='prospective';
  }
  return displayName;
};

const statusIconClass = function(status) {
  let icon;
  switch (status.toLowerCase()) {
    case 'approved':
    case 'available':
      icon='fa-check';
      break;
    case 'ex_registered':
    case 'ex_approved':
      icon='fa-exclamation';
      break;
    case 'suspended':
      icon='fa-ban';
      break;
    case 'withdrawn':
    case 'prospective':
    default:
      icon='fa-address-card-o';
  }
  return icon;
};

const statusClass = function(status) {
  let className;
  switch (status.toLowerCase()) {
    case 'available':
      className='approvals-approved';
      break;
    case 'approved':
      className='approvals-approved';
      break;
    case 'ex_registered':
      className='registered-historic';
      break;
    case 'ex_approved':
      className='approvals-historic';
      break;
    case 'suspended':
      className='approvals-suspended';
      break;
    case 'withdrawn':
    case 'prospective':
    default:
  }
  return className;
};

ApprovalStatus.propTypes = {
  status: PropTypes.object.isRequired
};
export {statusClass};
export default ApprovalStatus;
