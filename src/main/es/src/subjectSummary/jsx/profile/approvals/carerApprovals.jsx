import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import Model from '../../../../model/model';
import AdoptiveCarerApproval from './adoptiveCarerApproval';
import FosterCarerApproval from './fosterCarerApproval';
import Error from '../../../../error/error';

export default class CarerApprovals extends PureComponent {
  static propTypes = {
    carerApprovalSummaryUrl: PropTypes.string.isRequired,
    fosterCarerRegistrationUrl: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }).isRequired,
    labels:PropTypes.object.isRequired
  };

  state={
    loading:true,
    errors:null,
    carerApprovalSummaries:null
  };
  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({errors:null});
  };

  componentDidMount(){
    this.loadData();
  }
  componentDidUpdate(prevProps){
    if (!(prevProps.carerApprovalSummaryUrl === this.props.carerApprovalSummaryUrl
       && prevProps.subject === this.props.subject)) {
      this.loadData();
    }

    //Update the overflow property once the data has loaded
    if (this.containerElem) {
      this.containerElem.updateOverflow();
    }
  }
  loadData() {
    // Load the CarerApprovals here
    const {subject, carerApprovalSummaryUrl}=this.props;

    const model=new Model(carerApprovalSummaryUrl);
    const subjectId=subject.subjectId;

    model.load(endpoint.carerApprovalSummaryEndpoint, {subjectId:subjectId}).then(result=>this.setState({
      carerApprovalSummaries:result,
      loading:false,
      errors:null
    })).catch(reject=>this.setState({
      errors:reject,
      loading:false,
      carerApprovalSummaries:null
    }));
  }
  isFosterCarer(carerTypes) {
    return carerTypes.includes('FOSTER_CARER');
  }
  isAdopter(carerTypes) {
    return carerTypes.includes('ADOPTER');
  }
  render(){
    const {errors, loading, carerApprovalSummaries}=this.state;
    const {subject, labels, fosterCarerRegistrationUrl}=this.props;

    if (!loading && !errors) {
      if(carerApprovalSummaries){

        let adopterContent;
        if (this.isAdopter(carerApprovalSummaries.carerTypes)) {
          adopterContent=(<AdoptiveCarerApproval approvalSummary={carerApprovalSummaries.adopterSummary} labels={labels} />);
        }

        let fosterCarerContent;
        if (this.isFosterCarer(carerApprovalSummaries.carerTypes)){
          fosterCarerContent=(<FosterCarerApproval approvalSummary={carerApprovalSummaries.fosterCarerSummary} labels={labels} subject={subject} fosterCarerRegistrationUrl={fosterCarerRegistrationUrl} />);
        }
        return (
          <div>
            <div className="latest-forms-content">
              {adopterContent}
              {fosterCarerContent}
            </div>
          </div>
        );
      }
    }
    if(errors){
      //supress 404 (not found) and 401 (not authorised)
      if (errors.code !== 404 && errors.code !==401) {
        return (<Error errors={errors} onClose={this.clearErrors}/>);
      }
    }

    //no output
    return false;
  }
}
