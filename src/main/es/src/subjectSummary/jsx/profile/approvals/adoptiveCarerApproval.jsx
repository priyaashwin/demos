import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../../../../infopop/infopop';
import {statusClass} from './approvalStatus';
import ApprovalStatus from './approvalStatus';
import classnames from 'classnames';
import ApprovalDetail from './approvalDetail';

export default class AdoptiveCarerApproval extends PureComponent {
  static propTypes = {
    approvalSummary: PropTypes.object.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }).isRequired,
    labels:PropTypes.object.isRequired
  };

  render(){
    const {approvalSummary, labels}=this.props;
    const approvalDetails=approvalSummary.currentApprovals;
    const withdrawalDetails=approvalSummary.currentWithdrawals;

    let approvalsContent;
    if (withdrawalDetails.length > 0) {
      approvalsContent=(
        <div className="content-markup">
          <hr />
          {
            withdrawalDetails.map((withdrawalDetail, i)=>(
              <div className="approval-info" key={i}>
                <ApprovalDetail approvalDetail={withdrawalDetail} careType={'withdrawn'}/>
              </div>
            ))
          }
        </div>
      );
    } else if (approvalDetails.length > 0) {
      approvalsContent=(
        <div className="content-markup">
          <hr />
          <div className="approval-info">{labels.adopterApproval.details.approvalsTitle}</div>
          {
            approvalDetails.map((approvalDetail, i)=>(
              <div className="approval-info" key={i}>
                <ApprovalDetail approvalDetail={approvalDetail} careType={'adopter'}/>
              </div>
            ))
          }
        </div>
      );
    } else {
      approvalsContent=(
        <div className="content-markup">
          <div className="approval-info">{labels.adopterApproval.details.empty}</div>
        </div>
      );
    }

    return(
        <div className="adoptive-carer-approval carer-approval-item">
          <span className="carer-approval-title">{labels.adopterApproval.title} </span>
          <InfoPop key="adoptivecarer" className={classnames('approval-icon', statusClass(approvalSummary.status))} title={labels.adopterApproval.details.title} useMarkup="true">
            <ApprovalStatus status={approvalSummary.status} />
            {approvalsContent}
          </InfoPop>
        </div>
    );
  }
}
