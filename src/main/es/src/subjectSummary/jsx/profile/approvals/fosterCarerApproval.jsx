import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import ApprovalDetail from './approvalDetail';
import SuspensionDetail from './suspensionDetail';
import endpoint from '../../../js/cfg';
import Model from '../../../../model/model';
import InfoPop from '../../../../infopop/infopop';
import {formatDate} from '../../../../date/date';
import {statusClass} from './approvalStatus';
import ApprovalStatus from './approvalStatus';
import classnames from 'classnames';

const getRegistrationEndDate = result => {
  if(!result
    || !result.results
    || !result.totalSize) return null;
  const endDate = result.results[result.totalSize-1].endDate;
  if(!endDate || endDate === null) return null;
  return new Date(endDate);
};

export default class FosterCarerApproval extends PureComponent {
  static propTypes = {
    approvalSummary: PropTypes.object.isRequired,
    fosterCarerRegistrationUrl: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }).isRequired,
    labels:PropTypes.object.isRequired
  };

  state={
    deRegistrationStatus: undefined,
    deRegistrationSummary: undefined
  };

  componentDidUpdate(prevProps) {
    if (!(prevProps.approvalSummary === this.props.approvalSummary
        && prevProps.subject === this.props.subject)) {
      this.loadData();
    }
  }
  componentDidMount(){
    this.loadData();
  }
  loadData(){
    const {subject, approvalSummary, fosterCarerRegistrationUrl, labels}=this.props;
    const approvalDetails=approvalSummary.currentApprovals;
    const {subjectId}=subject;
    const model=new Model(fosterCarerRegistrationUrl);
    const ex_registration = labels.fosterCarerApproval.details.ex_registration;
    if (approvalDetails.length === 0) {
      model.load(endpoint.carerApprovalSummaryEndpoint, {id: subjectId}).then(result => {
        const endDate = getRegistrationEndDate(result);
        if(endDate !== null){
          this.setState({
            deRegistrationStatus: ex_registration.status,
            deRegistrationSummary: ex_registration.title + formatDate(endDate, true)
          });
        }
      }).catch(()=>this.setState({
        deRegistrationStatus: undefined,
        deRegistrationSummary: undefined
      }));
    }
  }
  render(){
    const {approvalSummary, labels}=this.props;
    const {deRegistrationStatus, deRegistrationSummary}=this.state;
    const approvalDetails=approvalSummary.currentApprovals;
    const suspensionDetails=approvalSummary.currentSuspensions;
    const withdrawalDetails=approvalSummary.currentWithdrawals;

    const approvalStatus = deRegistrationStatus || approvalSummary.status;

    let approvalsContent;
    if (withdrawalDetails.length > 0) {
      approvalsContent=(
        <div className="content-markup">
          <hr />
          {
            withdrawalDetails.map((withdrawalDetail, i)=>(
              <div className="approval-info" key={i}>
                <ApprovalDetail approvalDetail={withdrawalDetail} careType={'withdrawn'}/>
              </div>
            ))
          }
        </div>
      );
    } else if (approvalDetails.length > 0) {
      let suspensionsContent;
      if (suspensionDetails.length > 0) {
        suspensionsContent=(
          <div>
            <hr />
            <div className="approval-info">{labels.fosterCarerApproval.details.suspensionsTitle}</div>
            {
              suspensionDetails.map((suspensionDetail, i)=>(
                <div className="approval-info" key={i}>
                  <SuspensionDetail suspensionDetail={suspensionDetail} />
                </div>
              ))
            }
          </div>
        );
      }

      approvalsContent=(
        <div className="content-markup">
          <hr />
          <div className="approval-info">{labels.fosterCarerApproval.details.approvalsTitle}</div>
          <div>
          {
            approvalDetails.map((approvalDetail, i)=>(
              <div className="approval-info" key={i}>
                <ApprovalDetail approvalDetail={approvalDetail} careType={'carer'}/>
              </div>
            ))
          }
          </div>
          {suspensionsContent}
        </div>
      );
    } else {
      approvalsContent=(
        <div className="content-markup">
          <div className="approval-info">{deRegistrationSummary || labels.fosterCarerApproval.details.empty}</div>
        </div>
      );
    }

    return(
        <div className="foster-carer-approval carer-approval-item">
          <span className="carer-approval-title">{labels.fosterCarerApproval.title} </span>
          <InfoPop key="fostercarer" className={classnames('approval-icon', statusClass(approvalStatus))} title={labels.fosterCarerApproval.details.title} useMarkup="true">
            <ApprovalStatus status={approvalStatus} />
            {approvalsContent}
          </InfoPop>
        </div>
    );
  }
}
