'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../../date/date';

const SuspensionDetail=({suspensionDetail})=>(
  <div>{suspensionDetail.reasonDesc} from {formatDate(suspensionDetail.startDate,true)} until {formatDate(suspensionDetail.endDate,true)||'an unknown date'}.</div>
);

SuspensionDetail.propTypes={
  suspensionDetail: PropTypes.object.isRequired
};

export default SuspensionDetail;
