import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../../date/date';

export default class ApprovalDetail extends PureComponent {
  static propTypes = {
    approvalDetail: PropTypes.object.isRequired,
    careType: PropTypes.string.isRequired
  };

  getGenderSpecificChildText(gender, singular) {
    if(!gender){
      return '';
    }
    let genderText;
    switch (gender.toLowerCase()) {
      case 'male':
        if(singular) {
          genderText='boy';
        } else {
          genderText='boys';
        }
        break;
      case 'female':
        if(singular) {
          genderText='girl';
        } else {
          genderText='girls';
        }
        break;
      case 'both':
      default:
        if(singular) {
          genderText='child, either gender';
        } else {
          genderText='children, either gender';
        }
        break;
    }
    return genderText;
  }
  getGenderSpecificText(gender) {
    if(!gender){
      return '';
    }
    let genderText;
    switch (gender.toLowerCase()) {
      case 'male':
        genderText='boy';
        break;
      case 'female':
        genderText='girl';
        break;
      case 'both':
      default:
          genderText='either gender';
        break;
    }
    return genderText;
  }
  getFosterPlacementsText(related, unrelated, gender) {
    let placementsText;
    if ((related === 1 && unrelated === 1) || (related === 1 && unrelated === 0) || (related === 0 && unrelated === 1)) {
      placementsText = '1 ' + this.getGenderSpecificChildText(gender, true);
    } else if (related === unrelated) {
      placementsText = related + ' ' + this.getGenderSpecificChildText(gender, false) + ' (related or unrelated)';
    } else {
      placementsText = unrelated + ' unrelated (or ' + related + ' related) ' + this.getGenderSpecificChildText(gender, false);
    }
    return placementsText;
  }
  getDisabilityText(disabilityPermitted) {
    let disabilityText='Unable to support children with disabilities.';
    if (disabilityPermitted) {
      disabilityText='Able to support children with disabilities.';
    }
    return disabilityText;
  }
  render() {
    const {approvalDetail, careType}=this.props;
    let endDateText, approvedByAndWhen, ageAndDisability, approvalText, placementText;

    if(careType !== 'withdrawn') {
      endDateText = approvalDetail.endDate?`until ${formatDate(approvalDetail.endDate,true)}`:'with no end date';
      approvedByAndWhen = `Approved by ${approvalDetail.organisation.name} from ${formatDate(approvalDetail.startDate,true)} ${endDateText}`;
      ageAndDisability = `aged between ${approvalDetail.lowerAgePermitted} and ${approvalDetail.upperAgePermitted}. ${this.getDisabilityText(approvalDetail.disabilityPermitted)}`;
    }

    switch(careType) {
      case 'adopter': {    
        placementText = this.getGenderSpecificText(approvalDetail.genderPermitted);
        approvalText=`${approvedByAndWhen}, can adopt ${placementText}, ${ageAndDisability}`;
        break;
      }
      case 'carer': {
        placementText = this.getFosterPlacementsText(approvalDetail.maxRelatedPlacements, approvalDetail.maxUnrelatedPlacements, approvalDetail.genderPermitted);
        approvalText = `${approvedByAndWhen} to provide ${approvalDetail.typeOfCareAssignmentVO.codedEntryVO.name} care for ${placementText}, ${ageAndDisability}`;
        break;
      }
      case 'withdrawn': {
        const classification = approvalDetail.classificationVO;
        approvalText = `The carer's application was withdrawn on ${formatDate(approvalDetail.startDate,true)} with reason '${classification.name}'`;
        break;
      }
    }
    return (<div>{approvalText}</div>);
  }
}
