import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Picture from '../../../../picture/picture';
import endpoint from '../../../js/cfg';
import subs from 'subs';
import LatestForms from '../latestForms';
import PrivateFostering from '../classifications/privateFostering';
import CarerApprovals from '../approvals/carerApprovals';

export default class Content extends PureComponent {
  static propTypes = {
    url: PropTypes.string.isRequired,
    carerApprovalSummaryUrl: PropTypes.string.isRequired,
    classificationsUrl: PropTypes.string.isRequired,
    fosterCarerRegistrationUrl: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }).isRequired,
    labels: PropTypes.object.isRequired,
    permissions:PropTypes.shape({
      canViewForm: PropTypes.bool,
      canViewCarerApproval: PropTypes.bool,
      canViewPrivateFostering: PropTypes.bool
    }),
    currentUserTeamId: PropTypes.string
  };
  
  getSubjectPictureEndpoint(){
    const {subject, url}=this.props;

    const subjectId=subject.subjectId;
    const subjectType=(subject.subjectType||'').toLowerCase();

    //merge our URL into the endpoint
    const updatedEndpoint={
      ...endpoint.subjectPictureEndpoint,
      url:url
    };

    //substitute the subject parameters
    updatedEndpoint.url=subs(updatedEndpoint.url||'', {
      subjectId:subjectId,
      subjectType: subjectType
    });

    return updatedEndpoint;
  }
  render(){
    const { labels, permissions = {}, currentUserTeamId, ...other}=this.props;

    let latestForms;

    if (permissions.canViewForm && currentUserTeamId){
      latestForms=(<LatestForms {...other} labels={labels.forms}/>);
    }

    let carerApprovals;

    if (permissions.canViewCarerApproval) {
      carerApprovals=(<CarerApprovals {...other} labels={labels}/>);
    }

    let privateFostering;

    if(permissions.canViewPrivateFostering){
      privateFostering=(<PrivateFostering {...other}/>);

    }
    return (
      <div className="summary-profile">
        <Picture key="person-picture" className="profile-picture" endpoint={this.getSubjectPictureEndpoint()} backgroundStyle="center/contain" />
        <div>
          {latestForms}
          {carerApprovals}
          {privateFostering}
        </div>
      </div>
    );
  }
}
