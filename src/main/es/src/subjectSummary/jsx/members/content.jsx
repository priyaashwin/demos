'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import JoinDate from './joinDate';

const Content=({labels, membership})=>(
  <div className="card-cols">
    <div className="data-item">
      <JoinDate date={membership.joinDate} labels={labels} />
    </div>
  </div>
);

Content.propTypes={
  membership: PropTypes.object,
  labels: PropTypes.object.isRequired
};

export default Content;
