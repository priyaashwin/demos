'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../date/date';

const JoinDate=({date, labels})=>(
  <div className="join-date">
    <span className="data-label">{labels.joinDate}</span>
    <span className="data-value">{formatDate(date, true)}</span>
  </div>
);

JoinDate.propTypes={
  date: PropTypes.number,
  labels: PropTypes.object.isRequired
};

export default JoinDate;
