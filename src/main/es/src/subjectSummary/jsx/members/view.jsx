'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../js/cfg';
import ModelList from '../../../model/modelList';
import Title from '../people/title';
import Content from './content';
import PersonCardView from '../people/peopleList';

export default class MemberCardView extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    url: PropTypes.string,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }),
    labels: PropTypes.object.isRequired,
    personSummaryUrl: PropTypes.string.isRequired,
    subjectPictureEndpoint: PropTypes.object.isRequired
  }
  getRestEndpoint = (url, subject) => {
    //merge our URL into the endpoint
    return {
      ...endpoint.membersEndpoint,
      url: new ModelList(url).getURL(subject)
    };
  };
  getCardTitle = (membership) => {
    const { personSummaryUrl, subjectPictureEndpoint, labels } = this.props;
    const person = membership.personVO || {};
    const id = person.id;
    const name = person.name;
    return (
      <Title
        labels={labels}
        id={id}
        name={name}
        personSummaryUrl={personSummaryUrl}
        subjectPictureEndpoint={subjectPictureEndpoint}
      />
    );
  };
  getCardContent = (membership) => {
    const { labels } = this.props;

    return (
      <Content
        membership={membership}
        labels={labels}
      />
    );
  }
  render() {
    const { ...other } = this.props;
    return (
      <PersonCardView
        key="person-card"
        getRestEndpoint={this.getRestEndpoint}
        getCardTitle={this.getCardTitle}
        getCardContent={this.getCardContent}
        {...other}
      />
    );
  }
}
