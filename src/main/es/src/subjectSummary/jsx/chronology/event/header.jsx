import React from 'react';
import PropTypes from 'prop-types';
import {formatFuzzyDate} from '../../../../date/fuzzyDate';

const Header=({eventDate})=>{
  const formattedDate=formatFuzzyDate(eventDate, true);

  let date;

  if(formattedDate.date){
    date=(<div className="date"><i className="fa fa-calendar" aria-hidden="true"/>{' '}{formattedDate.date}</div>);
  }
  let time;
  if(formattedDate.time){
    time=(<div className="time"><i className="fa fa-clock-o" aria-hidden="true"/>{' '}{formattedDate.time}</div>);
  }

return(
  <div className="header">
      {date}
      {time}
  </div>
);};

Header.propTypes = {
  eventDate: PropTypes.object.isRequired,
  labels: PropTypes.object.isRequired
};

export default Header;
