import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {findDOMNode} from 'react-dom';
import Event from './event';
import classnames from 'classnames';
import {getEventColour} from '../../../js/eventClass';
import NativeScrollList from '../../widget/core/nativeScrollList';
import debounce from 'just-debounce';

export default class List extends PureComponent{
  static propTypes = {
    events: PropTypes.array,
    labels: PropTypes.object.isRequired,
    updateCoordinates: PropTypes.func.isRequired,
    impactEnabled: PropTypes.bool.isRequired
  };
  
  constructor(props){
    super(props);

    this.updateCoords=this.updateCoords.bind(this);

    this.handleResize=debounce(()=>this.updateCoords(), 100);
    this.handleScroll=debounce(()=>this.updateCoords(), 30);
  }
  componentDidUpdate(){
    this.updateCoords();
  }
  componentDidMount(){
    let supportsPassive = false;
    try {
      const opts = Object.defineProperty({}, 'passive', {
        get: function() {         //eslint-disable-line getter-return
          supportsPassive = true;
        }
      });
      window.addEventListener('test', null, opts);
      window.removeEventListener('test');
    } catch (e) {
      //ignore
    }

    window.addEventListener('resize', this.handleResize);
    this.eventList.addEventListener('scroll', this.handleScroll, supportsPassive ? { passive: true } : false);

    this.updateCoords();
  }
  componentWillUnmount(){
    window.removeEventListener('resize', this.handleResize);
    this.eventList.removeEventListener('scroll', this.handleScroll);
  }
  updateCoords(){
    //find first event
    const event=this.firstEvent;
    if(event){
      let yPosition = 0;

      let elem=event;

      //add half height
      yPosition+=elem.clientHeight/2;
      const xPosition=elem.offsetLeft;

      while(elem && elem.id!=='chronologycontainer') {
        yPosition += elem.offsetTop;
        elem = elem.offsetParent;
      }

      yPosition-=this.eventList.scrollTop;
      //send back the coordinates relative to the top of the list
      this.props.updateCoordinates({x:xPosition,y:yPosition});
    }
  }
  render(){
    const {events, labels, impactEnabled}=this.props;

    const len=(events.length-1);

    const list=[];

    events.forEach((event, i, events)=>{
      list.push(<Event key={event.id} first={i===0} ref={(elem)=>{
        if(elem && elem.props.first){
          this.firstEvent=findDOMNode(elem);
        }
      }} event={event} labels={labels} updateCoordinates={this.updateCoords} impactEnabled={impactEnabled}/>);
      if(i<len){
        const fromColour=getEventColour(event, impactEnabled);
        const toColour=getEventColour(events[i+1], impactEnabled);
        const style={};

        style.backgroundImage=`linear-gradient(${fromColour},${toColour})`;
        style.backgroundRepeat='no-repeat';

        list.push(<div key={event.id+'c'} className={classnames('connector', 'fadeIn')} style={style}/>);
      }
    });

    //output
    return (
      <NativeScrollList
        key="events_scroll"
        className="event-list"
        ref={(elem) => { this.eventList = findDOMNode(elem); }}>
          {list}
      </NativeScrollList>
    );
  }
}
