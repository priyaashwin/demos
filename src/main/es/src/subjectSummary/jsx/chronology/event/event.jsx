import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {getEventClass} from '../../../js/eventClass';
import Header from './header';
import Content from './content';

const CONTENT_MAX_HEIGHT=150;
const CSS_TRANSITION_TIME=310;

export default class Event extends PureComponent{
  constructor(props){
    super(props);

    this.state={
      collapsed:true,
      collapsable:false
    };

    this.handleClick=this.handleClick.bind(this);
    this.updateFirstEventCoordinates=this.updateFirstEventCoordinates.bind(this);
  }
  componentDidMount(){
    this.setExpandableState();
  }
  componentWillUnmount(){
    this.tz=0;
  }
  componentDidUpdate(prevProps, prevState){
    if(this.props.event!=prevProps.event){
      this.setExpandableState();
    }

    if(prevState.collapsed!==this.state.collapsed){
      if(this.props.first){
        //only necessary for the first item
        this.tz=performance.now();

        //update coordinates after height change
        this.updateFirstEventCoordinates();
      }
    }
  }
  updateFirstEventCoordinates(){
    if(performance.now()-this.tz<CSS_TRANSITION_TIME){
      window.requestAnimationFrame(()=>{
        this.updateFirstEventCoordinates();
      });

      //call into parent to handle the update
      this.props.updateCoordinates();
    }
  }
  setExpandableState(){
    if(this.containerElem){
      const scrollHeight=this.containerElem.scrollHeight;
      const collapsable=scrollHeight>CONTENT_MAX_HEIGHT;
      this.setState({
        collapsable:collapsable,
        collapsed:collapsable,
        styles:collapsable?{}:{
          maxHeight:'none'
        }
      });
    }
  }
  handleClick(e){
    e.preventDefault();

    let styles={};
    let newCollapsedState=false;

    if(this.state.collapsable){
      newCollapsedState=!this.state.collapsed;
      if(!newCollapsedState){
        const scrollHeight=this.containerElem.scrollHeight;

        styles={
          maxHeight:scrollHeight+50+'px'
        };
      }
    }

    this.setState({
      collapsed:newCollapsedState,
      styles: styles
    });
  }
  render(){
    const {event, labels, impactEnabled}=this.props;
    const classNames=classnames('event', 'fadeIn', getEventClass(event, impactEnabled));

    return (
      <div className={classNames} onClick={this.handleClick}>
          <Header eventDate={event.eventDate} labels={labels}/>
          <div style={this.state.styles} className={classnames('text', {
              'collapsed':this.state.collapsed
            })} ref={(elem) => { this.containerElem = elem; }}>
            <Content event={event.event} eventSubject={event.subjectIdName}/>
          </div>
      </div>
    );
  }
}

Event.propTypes = {
  first: PropTypes.bool.isRequired,
  event: PropTypes.object.isRequired,
  labels: PropTypes.object.isRequired,
  updateCoordinates: PropTypes.func.isRequired,
  impactEnabled: PropTypes.bool.isRequired
};
