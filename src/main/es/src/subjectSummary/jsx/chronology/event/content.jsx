import React from 'react';
import PropTypes from 'prop-types';
import Subject from '../../../../subject/subject';

const groupIcon = <i className="fa fa-users event-type" aria-hidden="true" title="Group entry" />;
const personIcon = <i className="fa fa-user event-type" aria-hidden="true" title="Person entry" />;

const Content = ({ event, eventSubject }) => {
    const eventSubjectType = (eventSubject.type || '').toLowerCase();

    const subjectOutput = (
        <Subject key="subject" subjectName={eventSubject.name} subjectType={eventSubjectType} subject={eventSubject} />
    );

    let typeIcon;
    switch (eventSubjectType) {
        case 'group':
            typeIcon = groupIcon;
            break;
        default:
            typeIcon = personIcon;
    }
    return (
        <div>
            <span className="event-subject">
                {typeIcon}
                {subjectOutput}
            </span>
            {event}
        </div>
    );
};

Content.propTypes = {
    event: PropTypes.string.isRequired,
    eventSubject: PropTypes.shape({
        name: PropTypes.string,
        type: PropTypes.string,
        subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        subjectType: PropTypes.string
    }).isRequired
};

export default Content;
