import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider/lib/Slider';

export default class Zoom extends PureComponent{
  render(){
    const {level, min, max, onZoomChange}=this.props;
    const p=Math.floor(Math.min(100,Math.max(0,((level-min)/(max-min))*100)));
    return (
      <div className="zoom">
        <Slider value={level} min={min} max={max} onChange={onZoomChange}/>
        <div className="level">{`${p}%`}</div>
      </div>
    );
  }
}

Zoom.propTypes = {
  level: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  onZoomChange: PropTypes.func.isRequired
};
