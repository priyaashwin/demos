import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Button from './button';

export default class Navigation extends PureComponent{
  constructor(props){
    super(props);

    this.handleNext=this.handleNext.bind(this);
    this.handlePrevious=this.handlePrevious.bind(this);
    this.handleLast=this.handleLast.bind(this);
    this.handleFirst=this.handleFirst.bind(this);
  }
  handleNext(e){
    e.preventDefault();

    this.props.onNavigate('next');
  }
  handlePrevious(e){
    e.preventDefault();

    this.props.onNavigate('previous');
  }
  handleLast(e){
    e.preventDefault();

    this.props.onNavigate('last');
  }
  handleFirst(e){
    e.preventDefault();

    this.props.onNavigate('first');
  }
  render(){
    const {labels}=this.props;

    return(
      <div className="navigation">
        <Button title={labels.first} icon="fa fa-step-backward  fa-rotate-90" onClick={this.handleFirst}/>
        <Button title={labels.previous} icon="fa fa-arrow-up" onClick={this.handlePrevious}/>
        <Button title={labels.next} icon="fa fa-arrow-down" onClick={this.handleNext}/>
        <Button title={labels.last} icon="fa fa-step-forward  fa-rotate-90" onClick={this.handleLast}/>
      </div>
    );
  }
}

Navigation.propTypes = {
  labels: PropTypes.object.isRequired,
  onNavigate:  PropTypes.func.isRequired
};
