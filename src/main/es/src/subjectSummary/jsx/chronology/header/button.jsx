import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Button=({title, icon, onClick})=>(
    <button className={classnames('pure-button', 'nav-button')} title={title} onClick={onClick}>
      <i className={icon} aria-hidden="true"/>
    </button>
);

Button.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Button;
