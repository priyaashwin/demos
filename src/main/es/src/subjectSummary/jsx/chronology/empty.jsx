'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const EmptyTimeline=({labels})=>(
  <div>
      <span>{labels.emptyTimeline||'No chronology'}</span>
  </div>
);


EmptyTimeline.propTypes={
  labels: PropTypes.object.isRequired
};

export default EmptyTimeline;
