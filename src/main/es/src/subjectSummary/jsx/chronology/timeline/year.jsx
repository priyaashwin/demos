import React from 'react';
import PropTypes from 'prop-types';

const Year=({children, offset})=>(
    <div key="period" className="period" style={{
          transform:`matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,${offset*-1}, 0, 1)`
        }}>
      {children}
  </div>
);

Year.propTypes = {
  children: PropTypes.node,
  offset: PropTypes.number
};

export default Year;
