import React from 'react';
import PropTypes from 'prop-types';
const Header=({children, offset})=>{
  const style={};
  if(offset){
    style.transform='matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0' + ',' + (-offset) + ', 0, 1)';
  }

  return (
    <div className="header" style={style}>
      <div className="icon"><i className="fa fa-square" aria-hidden="true"/></div>
      {children}
    </div>
  );
};

Header.propTypes = {
  children: PropTypes.node,
  offset: PropTypes.number
};

export default Header;
