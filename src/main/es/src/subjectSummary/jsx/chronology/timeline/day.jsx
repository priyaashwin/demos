import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

export default class Day extends PureComponent{
  render(){
    const {showDetail, time, count, onClick, style, className}=this.props;
    let dayContent;
    let countContent;

    if(showDetail){
      const month=moment(time);
      dayContent=(<div id={'dt'+time} key="day" className="day">{month.format('Do')}</div>);
      countContent=(<div id={'dc'+time} key="count" className="count"><span>{count}</span></div>);
    }

    return(
      <div id={'d'+time} key={time} className={className} style={style} onClick={onClick}>{dayContent}{countContent}</div>
    );
  }
}

Day.propTypes = {
  time: PropTypes.number,
  count: PropTypes.number,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.object,
  className:PropTypes.string,
  showDetail:PropTypes.bool.isRequired
};
