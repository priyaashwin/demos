import React from 'react';
import PropTypes from 'prop-types';
import {getEventColour} from '../../../js/eventClass';

const Elbow=({start, end, events, impactEnabled})=>{
  //padding + -6px
  const elbow_width=7;
  const stroke=3;
  let colour='black';

  if(events.length>0){
    const event=events[0];
    colour=getEventColour(event, impactEnabled)||'black';
  }

  const width=elbow_width+start.x+end.x;
  const startY=start.y;
  const endY=end.y;

  const path=[];
  const half=Math.floor(width/2);

  path.push(`M0 ${startY}`);
  path.push(`H ${half}`);
  path.push(`V ${endY}`);
  path.push(`H ${width}`);

  const line=(<path className="fadeIn" key="line" d={path.join(' ')} stroke={colour}  strokeWidth={stroke} fill="transparent"/>);
  return (
    <svg width="100%" height="100%">
      {line}
    </svg>
  );
};

Elbow.propTypes = {
  start: PropTypes.object.isRequired,
  end: PropTypes.object.isRequired,
  events: PropTypes.array,
  impactEnabled: PropTypes.bool.isRequired,
};

export default Elbow;
