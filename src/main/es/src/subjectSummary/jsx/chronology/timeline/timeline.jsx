import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Year from './year';
import Month from './month';
import Header from './header';
import {daysInHeight, daysToHeight} from '../../../js/daySize';
import classnames from 'classnames';
import debounce from 'just-debounce';
import ScrollList from '../../widget/core/scrollList';
import memoize from 'memoize-one';

const CSS_TRANSITION_TIME=260;
const scrollSize=10;

const buildTimeline=function(start, end){
  const startMoment=moment(start).endOf('day');
  const endMoment=moment(end).startOf('day');

  //holds years and months array
  const years=[[]];

  const year=startMoment.get('year');
  //year index
  let yi=0;
  //output header for first moment
  let isFirst=true;

  while(startMoment>=endMoment){
    if(year!==startMoment.get('year')){
      //crossed a year boundary - so insert into next set of year data
      years.push([]);
      //increment year index
      yi+=1;
    }
    //timeline is in reverse order so start of month is end time in month
    let monthStart=moment(startMoment).startOf('month');
    if(monthStart<endMoment){
      monthStart=endMoment;
    }

    const start=startMoment.valueOf();
    const end=monthStart.valueOf();

    years[yi].push({
      isFirst:isFirst,
      key:monthStart.get('year')+''+monthStart.get('month'),
      start:start,
      end:end
    });

    startMoment.subtract(1, 'month');
    startMoment.endOf('month');
    //not the first moment
    isFirst=false;
  }

  return years;
};

export default class Timeline extends PureComponent {
  static propTypes = {
    start: PropTypes.number.isRequired,
    end: PropTypes.number.isRequired,
    labels: PropTypes.object.isRequired,
    zoom: PropTypes.number.isRequired,
    moveAperture:PropTypes.func.isRequired,
    offscreenTop:PropTypes.number.isRequired,
    offscreenBottom:PropTypes.number.isRequired,
    chronology: PropTypes.object.isRequired,
    updateCoordinates: PropTypes.func.isRequired,
    selected: PropTypes.number
  };
  state={
    selected:false,
    offset:0
  };

  constructor(props){
    super(props);

    //throttle down - about 30fps, but fast enough to not look too bad
    this.handleResize=debounce(()=>this._handleResize(), 33);
  }  
  componentDidMount(){
    window.addEventListener('resize', this.handleResize);
  }
  componentWillUnmount() {
      this.tz=0;
      window.removeEventListener('resize', this.handleResize);
  }

  /**
   * memoize will only execute the internal function if the arguments have changed
   */
  getTimeline=memoize((start, end)=>buildTimeline(start, end));


  static getDerivedStateFromProps(props, state){
    let s=null;
    if(state.selected != props.selected){
      s=s||{};
      //reset the offset on selection to prevent dangling off screen
      if(props.selected){
        s.offset=0;
      }
      //update selected state
      s.selected=props.selected;

    }else if(props.offscreenTop===0 && state.offset<0){
      s=s||{};
      
      //reset the offset on selection to prevent dangling off screen
      s.offset=0;
    }

    return s;
  }

  _handleResize(){
    //recompute elbow position
    this.updateSelectedCoords();
  }
  updateSelectedCoords=()=>{
    let coordinates=null;
    let yPosition = 0;

    if(this.props.selected){
      let elem=this.timelineContainer.querySelector('div.selected');
      if(elem){
        if(performance.now()-this.tz<CSS_TRANSITION_TIME){
          window.requestAnimationFrame(()=>{
            this.updateSelectedCoords();
          });
        }

        //add half height
        yPosition+=elem.clientHeight/2;

        while(elem && elem.id!=='chronologycontainer') {
          yPosition += elem.offsetTop;
          elem = elem.offsetParent;
        }

        coordinates={x:0, y:yPosition-this.state.offset};
      }
    }

    //send back the coordinates relative to the top of the list
    this.props.updateCoordinates(coordinates);
  };
  componentDidUpdate(prevProps){
    if(this.props.selected && (this.props.selected!==prevProps.selected || this.props.zoom!==prevProps.zoom)){
      this.tz=performance.now();
    }else{
      this.tz=0;
    }
    if(this.props.selected){
      this.updateSelectedCoords();
    }
  }
  handleWheel=(e)=>{
      //move the timeline up/down until either the start range is hit or then end
     e.preventDefault();
     if(e.deltaY<0){
       this.scrollUp();
     }else{
       this.scrollDown();
     }
  };
  scrollDown(){
    this.moveAperture(scrollSize);
  }
  scrollUp(){
    this.moveAperture(scrollSize*-1);
  }
  shouldPan=(dy)=>{
    const {offscreenTop}=this.props;
    let handle=false;
    if((offscreenTop>0 && dy!==0)|| (offscreenTop===0 && (dy>1 || this.state.offset>0 && Math.abs(dy)<=this.state.offset))){
      const h=0.1;
      if(dy>h||dy<h*-1){
        handle=true;
      }
    }
    return handle;
  };

  handlePan=(dy)=>{
    let offset=this.state.offset+dy;

    const dayHeight=daysToHeight(1, this.props.zoom);

    let td=0;
    const diff=Math.abs(offset/dayHeight);
    if(offset<0){
      if(offset+dayHeight>0||(Math.abs(this.state.offset)+Math.abs(offset))>dayHeight){
        td=Math.ceil(diff)*-1;
        offset=offset+dayHeight;
      }
    }else{
      td=Math.floor(diff);
    }
    offset=(offset%dayHeight);
    this.moveAperture(td);

    this.setState({
      offset:offset
    });
  };
  moveAperture=(offset)=>{
    this.props.moveAperture.call(null, offset);
  };
  getTargetDate(size) {
    return daysInHeight(size, this.props.zoom)||0;
  }
  handleNextUp=(e)=>{
    e.preventDefault();
    if(this.props.offscreenTop>0){
      this.scrollUp();
    }
  };
  handleNextDown=(e)=>{
    e.preventDefault();
    this.scrollDown();
  };
  
  render(){
    const {start, end, offscreenTop, offscreenBottom, updateCoordinates:updateCoordinatesIgnored, ...other}=this.props;

    const timeline=this.getTimeline(start, end);

    const lessIndicator=(<div className="less-indicator next" onClick={this.handleNextUp}><i className={classnames('fa', {
      'fa-caret-up':offscreenTop>0
    })} aria-hidden="true"/></div>);

    const moreIndicator=(<div className="more-indicator next" onClick={this.handleNextDown}><i className={classnames('fa', {
      'fa-caret-down':offscreenBottom>0
    })} aria-hidden="true"/></div>);

    let endDisplay;
    if(offscreenBottom===0){
      endDisplay=(<div className="end" style={{
        transform:`matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0,${this.state.offset*-1}, 0, 1)`
      }}><span><i className="fa fa-hourglass-o" aria-hidden="true"/>{' '}<span>{'End of chronology'}</span></span></div>);
    }

    const staticHeaderMonth=moment(start).format('MMMM YYYY');

    return (
      <ScrollList key="timeline_scroll" className="timeline-wrapper" shouldPan={this.shouldPan} handlePan={this.handlePan} onWheel={this.handleWheel}>
        {lessIndicator}
        {<div className="first"><div className="period" key="staticHeaderMonth"><Header>{staticHeaderMonth}</Header></div></div>}
        <div
          ref={(elem) => { this.timelineContainer = elem; }}
          className="timeline">

          {timeline.map((year, i)=>(
              <Year key={i} {...other} offset={this.state.offset}>
                {year.map((month, mi)=>(<Month key={mi} {...month} {...other}/>))}
              </Year>
            )
          )}
          {endDisplay}
        </div>
        {moreIndicator}
      </ScrollList>
    );
  }
}

