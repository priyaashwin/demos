import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {getHeight, getMargin} from '../../../js/daySize';
import classnames from 'classnames';
import {getEventColour} from '../../../js/eventClass';
import Day from './day';
import memoize from 'memoize-one';

export default class Bar extends Component{
  static propTypes = {
    time: PropTypes.number.isRequired,
    zoom:  PropTypes.number.isRequired,
    events: PropTypes.array.isRequired,
    selected: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
    impactEnabled: PropTypes.bool.isRequired
  };

  shouldComponentUpdate(nextProps){
    //component is keyed by time property, so the only possible updates are selected and zoom
    if(nextProps.selected!==this.props.selected||nextProps.zoom!==this.props.zoom){
      return true;
    }

    return false;
  }
  computeStyle=memoize((selected, zoom, events, impactEnabled)=>{
    const height=getHeight(selected?zoom<80?80:zoom:zoom);
    const style={
      height: height,
      marginBottom: getMargin(zoom),
      fontSize: height*0.5
    };

    //event colours
    const colours=events.map(event=>getEventColour(event, impactEnabled));

    if(colours.length>0){
      const percentW=100/colours.length;

      style.backgroundImage=colours.map(colour=>`linear-gradient(to top, ${colour}, ${colour})`).join(',');
      style.backgroundSize=colours.map((c,i)=>`${percentW+(percentW*i)}% 100%`).join(',');
      style.backgroundRepeat='no-repeat';
    }

    return style;
  });
  handleClick=(e)=>{
    const {events}=this.props;
    e.preventDefault();

    if(events.length>0){
      this.props.onClick(moment(events[0].eventDate.calculatedDate).startOf('day').valueOf());
    }else{
      this.props.onClick(null);
    }
  };
  render(){
    const {time, events, selected, zoom, impactEnabled}=this.props;
    const style=this.computeStyle(selected, zoom, events, impactEnabled);

    const classNames=classnames('entry', {
      tick: style.height>10,
      selected: selected
    });

    return (<Day key={time} className={classNames} style={style} onClick={this.handleClick} showDetail={style.height>10} time={time} count={events.length}/>);
  }
}
