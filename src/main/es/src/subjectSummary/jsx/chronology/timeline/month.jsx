import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Bar from './bar';
import Header from './header';
import memoize from 'memoize-one';

const emptyArray=[];

const buildDays=function(start, end){
  const days=[];

  const startMoment=moment(start);
  const endMoment=moment(end);

  while(startMoment>=endMoment){
    const time=startMoment.startOf('day').valueOf();
    days.push(
      {
        key:time,
        time:time
      }
    );
    startMoment.subtract(1, 'days');
  }
  return days;
};

export default class Month extends PureComponent{
  static propTypes = {
    //controls if the header should be put out as the first entry
    isFirst: PropTypes.bool,
    start: PropTypes.number.isRequired,
    end: PropTypes.number.isRequired,
    labels: PropTypes.object.isRequired,
    chronology: PropTypes.object.isRequired,
    selected: PropTypes.number,
    onSelectEntry: PropTypes.func.isRequired
  };
  /**
   * memoize will only execute the internal function when the start/end props have changed
   */
  getDays=memoize((start, end)=>buildDays(start, end));
  
  render(){
    const {isFirst, start, end, chronology, selected, onSelectEntry, ...other}=this.props;

    let header;

    const month=moment(start).endOf('month');

    if(!isFirst&&start===month.valueOf()){
      const monthDisplay=month.format('MMMM YYYY');

      header=(<Header>{monthDisplay}</Header>);
    }

    const days=this.getDays(start, end);
    const id=month.get('year')+''+month.get('month');

    return (
      <div key={id} id={'m'+id}>
          {header}
          {days.map((day,i)=>(<Bar key={i} {...day} selected={selected===day.time} onClick={onSelectEntry} events={chronology.get(day.time)||emptyArray} {...other} />))}
      </div>
    );
  }
}