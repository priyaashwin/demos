'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Key extends PureComponent {
  static propTypes={
    labels: PropTypes.object.isRequired,
    enabled:PropTypes.bool.isRequired
  };

  state={
      expanded:false
    };

  handleClick=()=>{
    this.setState({
      expanded:!this.state.expanded
    });
  };
  
  render(){
    const expanded=this.state.expanded;
    const {labels, enabled}=this.props;
    
    //if the key is not enabled - do not render anything
    if(!enabled){
      return false;
    }

    const classNames=classnames('key-data',{
      expanded:expanded
    });

    let control;

    if(expanded){
      control=(<span><i className="fa fa-caret-down" aria-hidden="true"/>{' '}{labels.collapse}{' '}<i className="fa fa-caret-down" aria-hidden="true"/></span>);
    }else{
      control=(<span><i className="fa fa-caret-up" aria-hidden="true"/>{' '}{labels.expand}{' '}<i className="fa fa-caret-up" aria-hidden="true"/></span>);
    }
    return(<div className="key">
        <div className="control" onClick={this.handleClick}>
            {control}
        </div>
        <div className={classNames}>
          <ul>
            <li>
              {labels.noEntry}
              <i className="fa fa-square no-entry"/>
            </li>
            <li>
              {labels.positive}
              <i className="fa fa-square positive"/>
            </li>
            <li>
              {labels.neutral}
              <i className="fa fa-square neutral"/>
            </li>
            <li>
              {labels.negative}
              <i className="fa fa-square negative"/>
            </li>
            <li>
              {labels.unknown}
              <i className="fa fa-square unknown"/>
            </li>
          </ul>
        </div>
    </div>);
  }
}

