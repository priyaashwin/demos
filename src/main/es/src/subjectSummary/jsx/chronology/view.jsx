'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Timeline from './timeline/timeline';
import Key from './key';
import Zoom from './zoom';
import List from './event/list';
import Elbow from './timeline/elbow';
import Loading from '../../../loading/loading';
import {daysInHeight} from '../../js/daySize';
import Immutable from 'immutable';
import subs from 'subs';
import {makeRequest} from 'usp-xhr';
import endpoint from '../../js/cfg';
import ErrorMessage from '../../../error/error';
import EmptyTimeline from './empty';
import Navigation from './header/navigation';
import FixedWidget from '../widget/fixed';
import Header from '../widget/header';
import {findDOMNode} from 'react-dom';
import Model from '../../../model/model';

//control the amount of data that will be rendered.
//the zoom will move through these
const msDay=86400000;

export default class ChronologyTimelineView extends Component {
  static propTypes = {
      title: PropTypes.string,
      labels: PropTypes.object.isRequired,
      url: PropTypes.string.isRequired,
      subject:PropTypes.shape({
        subjectId: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
          ]),
        subjectType: PropTypes.string
      }).isRequired,
      rangeEndpoint:PropTypes.object.isRequired,
      height: PropTypes.number.isRequired,
      impactEnabled:PropTypes.bool
  };

  static defaultProps={
      impactEnabled:false
  };
  state={
      dataStartDate:0,
      dataEndDate:0,
      //this is the start of the window on the results
      aperture: 0,
      apertureSize: 150,
      zoom:50,
      data: Immutable.Map(),
      entries: Immutable.Seq(),
      loading:true,
      errors:null,
    height:this.props.height
    };

  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({
      errors:null
    });
  };
  
  componentDidMount(){
    const startup=()=>{
      const apertureSize=this.computeApertureSize(this.props.height, this.state.zoom);
      this.setState({
        apertureSize:apertureSize
      });

      //start loading
      this.loadChronologyTimeline();
    };

    //wait until element added into document as we need the height
    const testElem=()=>{
      const elem=findDOMNode(this.containerElem);

      if(elem && document.body.contains(elem)){
        startup();
      }else{
        setTimeout(()=>testElem(), 50);
      }
    };

    testElem();
  }
  componentDidUpdate(prevProps, prevState){
    if(this.props.height!==prevProps.height){
      const prevApertureSize=prevState.apertureSize;
      const apertureSize=this.computeApertureSize(this.props.height, this.state.zoom);
      if(prevApertureSize!=apertureSize){
        this.setState({ // eslint-disable-line react/no-did-update-set-state
          apertureSize:apertureSize
        },()=>{
          this.handleMoveAperture(0);
        });
      }
    }
  }
  handleKeyPress=(e)=>{
    if(!this.state.loading){
      if(e.key==='ArrowDown' || e.key==='ArrowUp'){
        e.preventDefault();
        this.handleNavigation(e.key==='ArrowDown'?'next':'previous');
      }
    }
  };

  computeApertureSize(height, zoom){
    return daysInHeight(height, zoom);
  }
  handleZoomChange=(value)=>{
    const oldApertureSize=this.state.apertureSize;
    let aperture=this.state.aperture;
    const apertureSize=this.computeApertureSize(this.props.height, value);

      //zoom in - try to keep the focus in the middle of the aperture
    if(apertureSize!=oldApertureSize){
      const diff=Math.abs(apertureSize-oldApertureSize);
      //move the aperture up or down by half of the size abandonded

      aperture=Math.max(aperture+(Math.floor(diff/2)*(apertureSize>oldApertureSize?-1:1)),0);
    }

    let selected=this.state.selected;
    let elbowCoordStart=this.state.elbowCoordStart;
    let elbowCoordEnd=this.state.elbowCoordEnd;

    //try to keep the selected option - but if it does drop out of the window then deselect it
    if(selected){
      const awin=this.getAperture(aperture);

      if(!(awin.start>selected && selected>awin.end)){
        selected=undefined;
        elbowCoordStart=null;
        elbowCoordEnd=null;
      }
    }

    this.setState({
      apertureSize:apertureSize,
      aperture:aperture,
      zoom: value,
      selected: selected,
      elbowCoordStart: elbowCoordStart,
      elbowCoordEnd: elbowCoordEnd
    });
  };
  handleMoveAperture=(td)=>{
    //don't unselect unless we have to
    let selected=this.state.selected;
    const aperture= Math.max(0, Math.min(this.state.aperture+td, Math.abs(moment(this.state.dataStartDate).diff(moment(this.state.dataEndDate),'days'))));

    let elbowCoordStart=this.state.elbowCoordStart;
    let elbowCoordEnd=this.state.elbowCoordEnd;

    if(selected){
      const awin=this.getAperture(aperture);

      if(!(awin.start>selected && selected>=awin.end)){
        selected=undefined;
        elbowCoordStart=null;
        elbowCoordEnd=null;
      }
    }
    this.setState({
      aperture:aperture,
      selected:selected,
      elbowCoordStart:elbowCoordStart,
      elbowCoordEnd:elbowCoordEnd
    });
  };
  getAperture(aperture){
    const start=Math.max(this.state.dataEndDate, Math.min(moment(this.state.dataStartDate).subtract(aperture, 'days').valueOf(),  this.state.dataStartDate));
    const end=Math.min(this.state.dataStartDate, Math.max(start-(msDay*this.state.apertureSize), this.state.dataEndDate));

    return {start:start, end:end};
  }
  sortData(data){
    const dataMap=Immutable.Map().withMutations(dataMap=>{
      //for each item of data add it into the map based on the time
      data.forEach(event=>{
        const eventDate=moment(event.eventDate.calculatedDate);

        //trim to start of period
        eventDate.startOf('day');

        const v=eventDate.valueOf();
        const a=dataMap.get(v);
        if(a){
          a.push(event);
          //sort
          a.sort((a,b)=>a-b);
        }else{
          dataMap.set(v, [event]);
        }
      });
    });
    return dataMap;
  }
  handleSelectEntry=(time)=>{
    const prevSelected=this.state.selected;
    this.setState({
      selected:time,
      elbowCoordStart:prevSelected===time?this.state.elbowCoordStart:null,
      elbowCoordEnd:prevSelected===time?this.state.elbowCoordEnd:null
    });
  };
  handleEventCoordinates=(xy)=>{
    if(this.state.selected && xy){
      //get the x/y coordinates from the event rect
      this.setState({
        elbowCoordEnd:xy
      });
    }
  };
  handleBarCoordinates=(xy)=>{
    if(this.state.selected && xy){
      //get the x/y coordinates from the event rect
      this.setState({
        elbowCoordStart:xy
      });
    }
  };
  getEndpointPromise(endpoint){
    return new Promise((resolve, reject) => {
        //the restEndpoint property will contain all the data we require for the request,
        //but the pageSize and page placeholders will be completed here
        makeRequest(endpoint, (success, failure) => {
            if (failure) {
                //reject with the failure
                reject(failure);
            } else {
                //resolve with the RAW data
                resolve(success);
            }
        });
    });
  }
  getDataRange(){
    const {subject, rangeEndpoint}=this.props;

    const subjectType=((subject||{}).subjectType||'').toLowerCase();
    const subjectId=(subject||{}).subjectId;

    return new Model(rangeEndpoint.url).load({
        ...endpoint.chronologyRangeEndpoint,
        ...rangeEndpoint
      }, {
        subjectId:subjectId,
        subjectType:subjectType
      });
  }
  getData(from, to){
    const subject=this.props.subject||{};
    const url=this.props.url;

    //merge our URL into the endpoint
    const dataEndpoint={
        ...endpoint.chronologyEntryListEndpoint,
        //substitute the subject parameters
        url: subs(url, {
          subjectType:(subject.subjectType||'').toLowerCase(),
          subjectId:subject.subjectId
        })
    };

    //clone the params property
    dataEndpoint.params={
      ...endpoint.chronologyEntryListEndpoint.params,
      //fill in the dates from the range
      startDate:from,
      endDate:to
    };

    return this.getEndpointPromise(dataEndpoint);
  }
  loadChronologyTimeline(){
    this.setState({
      loading:true
    });

    this.getDataRange().then(range=>{
        if(!range.earliestEntry||!range.latestEntry){
          //no data
          return ({});
        }

        //range is in descending date order
        const from=moment(range.earliestEntry).startOf('day').valueOf();
        const to=moment(range.latestEntry).endOf('day').valueOf();

        //date is presented in descending order i.e. to ->from
        this.setState({
          dataStartDate:to,
          dataEndDate:from
        });

        //get date from the date range from -> to
        return this.getData(from, to);
    }, reject=>{
      if(reject.code===404){
        //this is ok - just means there is no chronolgy
        this.setState({
          data:Immutable.Map(),
          entries: Immutable.Seq(),
          loading:false
        });

        return ({});
      }
      else{
        throw(reject);
      }
    }).then((data) => {
      const dataMap=this.sortData(data.results||[]);
      this.setState({
        data:dataMap,
        entries: dataMap.keySeq().sort((a,b)=>a-b),
        loading:false
      });
    }).catch(reject => {
        //examine the errors and take appropriate action
        this.setState({
          //reset the data to an empty map
          data: Immutable.Map(),
          entries: Immutable.Seq(),
          errors:reject,
          loading:false
        });
    });
  }
  handleNavigation=(nav)=>{
    const prevSelected=this.state.selected;
    let selected=this.state.selected;

    let startSearch;
    let aperture=this.state.aperture;

    const sortedData=this.state.entries;

    // navigation can be 'next', 'previous', 'first', 'last'
    const startEntry=moment(sortedData.last()).startOf('day').valueOf();
    const endEntry=moment(sortedData.first()).startOf('day').valueOf();
    switch(nav){
      case 'first':
        selected=startEntry;
      break;
      case 'last':
        selected=endEntry;
      break;
      default:{
        if(selected){
          startSearch=moment(selected).startOf('day').valueOf();
          if(nav==='next'){
            if(startSearch===endEntry){
              //if it is the last entry - wrap round to the start
              startSearch=startEntry+1;
            }
          }else{
            if(startSearch===startEntry){
              //if it is the first entry - wrap round to the end
              startSearch=endEntry-1;
            }
          }
        }else{
          //starting at the given time period - find the next selectable item - or none
          startSearch=moment(startEntry-msDay*aperture).endOf('day').valueOf();
        }

        if(nav==='next'){
          selected=sortedData.takeWhile(v=>v<startSearch).last();
        }else{
          selected=sortedData.skipWhile(v=>v<=startSearch).first();
        }
      }
    }

    if(selected){
      //did we go outside the window?
      const diff=moment(this.state.dataStartDate).subtract(aperture,'days').diff(moment(selected).endOf('day'), 'days');

      if(diff<0||diff>(this.state.apertureSize/2)){
        aperture=aperture+diff;
        if(aperture<0){
          //wrap
          aperture=0;
          selected=null;
        }else if(aperture>Math.abs(moment(this.state.dataStartDate).diff(moment(this.state.dataEndDate),'days'))){
          aperture=this.state.aperture;
          selected=null;
        }
      }
    }

    this.setState({
      selected:selected,
      elbowCoordStart:prevSelected===selected?this.state.elbowCoordStart:null,
      elbowCoordEnd:prevSelected===selected?this.state.elbowCoordEnd:null,
      aperture:aperture
    });
  }
  render(){
    const {labels, impactEnabled}=this.props;
    const loading=this.state.loading;

    const aperture=this.state.aperture;
    const awin=this.getAperture(aperture);

    const start=awin.start;
    const end=awin.end;

    const offscreenTop=Math.abs(moment(this.state.dataStartDate).diff(moment(start), 'days'));
    const offscreenBottom=Math.abs(moment(this.state.dataEndDate).diff(moment(end), 'days'));

    const data=this.state.data;

    let content;
    if(this.state.errors){
      content=(<div className="empty-timeline"><ErrorMessage errors={this.state.errors} onClose={this.clearErrors}/></div>);
    }else{
      if(data.size===0 || loading){
        content=(
          <div className="empty-timeline">
            {loading?(<Loading />):(<EmptyTimeline labels={labels} />)}
          </div>
        );
      }else{
        let eventsContent;
        let elbow;

        const selectedEvents=data.get(this.state.selected);
        if(selectedEvents){
          eventsContent=(<List events={selectedEvents} labels={labels} updateCoordinates={this.handleEventCoordinates} impactEnabled={impactEnabled}/>);

          if(this.state.elbowCoordStart && this.state.elbowCoordEnd){
            elbow=(<Elbow start={this.state.elbowCoordStart} end={this.state.elbowCoordEnd} key={this.state.selected} events={selectedEvents} impactEnabled={impactEnabled}/>);
          }

        }else{
          eventsContent=(<div className="event-list empty"><div>{labels.selectPrompt||'Select a day.'}</div></div>);
        }
        content=(
          <div id="chronologycontainer" className="container summary-content">
            <Timeline updateCoordinates={this.handleBarCoordinates} selected={this.state.selected} chronology={data} zoom={this.state.zoom} start={start} end={end} labels={labels} moveAperture={this.handleMoveAperture} offscreenTop={offscreenTop} offscreenBottom={offscreenBottom} onSelectEntry={this.handleSelectEntry} impactEnabled={impactEnabled}/>
            <div className="elbow">
              {elbow}
            </div>
            {eventsContent}
          </div>
        );
      }
    }

    const header=(
      <Header title={labels.header.title}>
        <Navigation labels={labels.navigation} onNavigate={this.handleNavigation}/>
      </Header>
    );
    return (
      <FixedWidget ref={(elem) => { this.containerElem = elem; }} className="chronology-widget" header={header} onKeyDown={this.handleKeyPress} tabIndex="0">
        {content}
        <Key labels={labels.key||{}} enabled={impactEnabled}/>
        <Zoom level={this.state.zoom} min={0} max={100} onZoomChange={this.handleZoomChange}/>
      </FixedWidget>
  );
  }
}
