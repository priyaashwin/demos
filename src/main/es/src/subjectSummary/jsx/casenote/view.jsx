'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../widget/scrollable';
import PaginatedResultsList from '../../../results/paginatedResultsList';
import endpoint from '../../js/cfg';
import ModelList from '../../../model/modelList';
import Header from '../widget/header';
import Content from './content';
import Loading from '../../../loading/loading';
import ErrorMessage from '../../../error/error';
import { formatFuzzyDate } from '../../../date/fuzzyDate';

export default class CaseNoteCardView extends Component {
  static propTypes = {
    title: PropTypes.string,
    url: PropTypes.string.isRequired,
    configurableTypeUrl: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }),
    labels: PropTypes.object.isRequired
  };

  state={
    loading:true,
    errors:null,
    caseNote:null
  };
  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({errors:null});
  };
  componentDidMount(){
    this.loadData();
  }
  componentDidUpdate(prevProps){
    if (!(prevProps.url === this.props.url && prevProps.subject === this.props.subject)) {
      this.loadData();
    }

    //Update the overflow property once the data has loaded
    if (this.containerElem) {
      this.containerElem.updateOverflow();
    }
  }
  loadData(){
    const {subject, configurableTypeUrl, url}=this.props;

    //load the configurable types
    new PaginatedResultsList().load({
        ...endpoint.caseNoteConfigurableTypeEndpoint,
        //expect only 1 item so page size set to 1
        url: configurableTypeUrl}, 1, 1).then(success=>{
            if(success.results.length>0){
              const config=success.results[0];
              //it is possible to casenote entry type has not yet been configured
              if(config.caseNoteEntryType){
                return new PaginatedResultsList().load({
                    ...endpoint.latestCaseNoteEndpoint,
                    url: new ModelList(url).getURL({
                      subjectType:(subject.subjectType||'').toLowerCase(),
                      subjectId:subject.subjectId,
                      entryType: config.caseNoteEntryType
                    })
                  }, 1, 1);
                }
            }
            //return empty object - no config so no results
            return Promise.resolve({results:[]});
        }).then(result=>this.setState({
            caseNote:result.results[0],
            loading:false,
            errors:null
        }))
        .catch((reject)=>this.setState({
          errors:reject,
          loading:false,
          caseNote:null
        }));
  }
  getContent(){
    const {labels}=this.props;
    let content;
    if(this.state.loading){
      content=(<Loading />);
    }else{
      if(this.state.errors){
        content=(<div><ErrorMessage errors={this.state.errors} onClose={this.clearErrors}/></div>);
      }else{
          content=(<Content key="caseNote" caseNote={this.state.caseNote} labels={labels}/>);
      }
    }
    return content;
  }
  containerRef=(elem)=>{
    this.containerElem = elem;
  }
  getEntryDate = () => this.state.caseNote && <div>{formatFuzzyDate(this.state.caseNote.eventDate, true).date}</div>;
  render() {
    const {labels} = this.props;

    const header = (<Header title={labels.header.title}>{this.getEntryDate()}</Header>);

    return (
      <ScrollableWidget ref={this.containerRef} className="casenote-widget" header={header}>
        {this.getContent()}
      </ScrollableWidget>
    );
  }
}
