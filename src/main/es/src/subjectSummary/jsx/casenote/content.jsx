'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Content=({caseNote, labels})=>{
  if(caseNote){
    return (
      <div className="casenote-event-content" dangerouslySetInnerHTML={{
          __html:caseNote.event
        }}/>
    );
  }else{
    return (<div className="casenote-event-content empty-list">{labels.noRecordsFound||'No casenote'}</div>);
  }

};

Content.propTypes={
  caseNote: PropTypes.object,
  labels: PropTypes.object.isRequired
};

export default Content;
