'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../../link/link';
import Subject from '../../../subject/subject';
import Icon from '../../../subject/icon';
import endpoint from '../../js/cfg';
import subs from 'subs';

const getSubjectPictureEndpoint=(pictureEndpoint, id)=>{
  //merge our URL into the endpoint
  let updatedEndpoint;
  //only if endpoint has been supplied
  if(pictureEndpoint){
    updatedEndpoint={
      ...endpoint.subjectPictureEndpoint,
      ...pictureEndpoint
    };

    //substitute the subject parameters
    updatedEndpoint.url=subs(updatedEndpoint.url||'', {
      subjectId:id,
      subjectType: 'person'
    });
  }
  return updatedEndpoint;
};

const Title=({id, name, personSummaryUrl, subjectPictureEndpoint})=>{
  const identifier=`PER${id}`;
  return (
    <div>
        <Icon key="icon" iconClass="fa fa-person" pictureClassName="relationship-picture" pictureEndpoint={getSubjectPictureEndpoint(subjectPictureEndpoint, id)}/>
        <Link url={personSummaryUrl} term={id}>
          <Subject subjectType="person" name={name} identifier={identifier}/>
        </Link>
    </div>
  );
};

Title.propTypes={
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.string.isRequired,
  labels: PropTypes.object.isRequired,
  personSummaryUrl: PropTypes.string.isRequired,
  subjectPictureEndpoint:PropTypes.object.isRequired
};

export default Title;
