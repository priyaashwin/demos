'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../widget/scrollable';
import RemotePaginatedCardList from '../../../card/remotePaginatedCardList';
import Header from '../widget/header';

const CARD_CLASS_NAME = 'person-card';

export default class PersonCardView extends Component {
  static propTypes = {
    url: PropTypes.string,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }),
    labels: PropTypes.object.isRequired,
    getRestEndpoint: PropTypes.func.isRequired,
    getCardTitle: PropTypes.func.isRequired,
    getCardContent: PropTypes.func.isRequired
  };
  handleAfterUpdate = () => {
    //Update the overflow property once the data has loaded
    if (this.containerElem) {
      this.containerElem.updateOverflow();
    }
  };
  getCardClassName = () => {
    return CARD_CLASS_NAME;
  };
  containerRef = (elem) => {
    this.containerElem = elem;
  };
  render() {
    const { labels, url, subject, getRestEndpoint, getCardTitle, getCardContent } = this.props;
    const header = (<Header title={labels.header.title} />);

    return (
      <ScrollableWidget ref={this.containerRef} className="people-widget" header={header}>
        <RemotePaginatedCardList
          restEndpoint={getRestEndpoint(url, subject)}
          className="card-view"
          getCardTitle={getCardTitle}
          getCardContent={getCardContent}
          getCardClassName={this.getCardClassName}
          afterUpdate={this.handleAfterUpdate}
          enableHover={false}
          enableShadow={false}
          pageSize={-1}
          page={1}
          noResultsMessage={labels.noRecordsFound} />
      </ScrollableWidget>
    );
  }
}
