'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Contact = ({ contact }) => {
    return (
        <div>
            <span className="data-value">
                <a href={`tel:${contact}`}>{contact}</a>
            </span>
        </div>
    );
};

Contact.propTypes = {
    contact: PropTypes.any
};

export default Contact;
