'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../widget/scrollable';
import Header from '../widget/header';
import { TabView, Tab } from '../../../tabs/jsx/tabView';
import PersonRelationshipCardView from './personPersonRelationship/view';
import OrganisationRelationshipCardView from './personOrganisationRelationship/view';
import ChildProtectionRelationshipCardView from './childProtection/view';

export default class RelationshipView extends Component {
    static propTypes = {
        labels: PropTypes.shape({
            header: PropTypes.shape({
                title: PropTypes.string.isRequired
            })
        }).isRequired,
        personPersonRelationshipConfig: PropTypes.shape({
            labels: PropTypes.object.isRequired,
            url: PropTypes.string,
            summaryUrl: PropTypes.string.isRequired,
            isNamedPersonRelationshipAvailable: PropTypes.bool
        }).isRequired,
        personOrganisationRelationshipConfig: PropTypes.shape({
            labels: PropTypes.object.isRequired,
            url: PropTypes.string,
            summaryUrl: PropTypes.string.isRequired
        }).isRequired,
        childProtectionRelationshipConfig: PropTypes.shape({
            labels: PropTypes.object.isRequired,
            url: PropTypes.string,
            summaryUrl: PropTypes.string.isRequired,
            permissions: PropTypes.shape({
                canView: PropTypes.bool,
                canViewAddress: PropTypes.bool
            })
        }).isRequired,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }).isRequired,
        subjectPictureEndpoint: PropTypes.object.isRequired
    };
    getContent() {
        const {
            personPersonRelationshipConfig,
            personOrganisationRelationshipConfig,
            childProtectionRelationshipConfig,
            subject,
            ...other
        } = this.props;
        const {
            labels: personPersonRelationshipLabels,
            isNamedPersonRelationshipAvailable,
            url: personPersonRelationshipUrl,
            summaryUrl: personSummaryUrl,
            ...otherPersonPersonConfig
        } = personPersonRelationshipConfig;
        const {
            labels: personOrganisationRelationshipLabels,
            url: personOrganisationRelationshipUrl,
            summaryUrl: organisationSummaryUrl,
            ...otherPersonOrganisationConfig
        } = personOrganisationRelationshipConfig;

        const {
            labels: childProtectionRelationshipLabels,
            url: childProtectionRelationshipUrl,
            summaryUrl: childSummaryUrl,
            permissions: childProtectionRelationshipPermissions = {},
            ...otherChildProtectionConfig
        } = childProtectionRelationshipConfig;

        let childProtectionTab;

        //child protextion relationship tab is an optional tab
        if (childProtectionRelationshipPermissions.canView) {
            childProtectionTab = (
                <Tab
                    key="childProtectionRelationship"
                    label={childProtectionRelationshipLabels.header.title}
                    tabId="childProtectionRelationship">
                    <ChildProtectionRelationshipCardView
                        {...other}
                        {...otherChildProtectionConfig}
                        key="childProtectionRelationshipView"
                        labels={childProtectionRelationshipLabels}
                        subject={subject}
                        url={childProtectionRelationshipUrl}
                        summaryUrl={childSummaryUrl}
                        afterResultsUpdate={this.handleAfterUpdate}
                        permissions={childProtectionRelationshipPermissions}
                    />
                </Tab>
            );
        }
        return (
            <>
                <TabView key="roleTabs">
                    <Tab
                        key="personPersonRelationship"
                        label={personPersonRelationshipLabels.header.title}
                        tabId="personPersonRelationship">
                        <PersonRelationshipCardView
                            {...other}
                            {...otherPersonPersonConfig}
                            key="personPersonRelationshipView"
                            labels={personPersonRelationshipLabels}
                            subject={subject}
                            url={personPersonRelationshipUrl}
                            summaryUrl={personSummaryUrl}
                            afterResultsUpdate={this.handleAfterUpdate}
                            isNamedPersonRelationshipAvailable={isNamedPersonRelationshipAvailable}
                        />
                    </Tab>
                    <Tab
                        key="personOrganisationRelationship"
                        label={personOrganisationRelationshipLabels.header.title}
                        tabId="personOrganisationRelationship">
                        <OrganisationRelationshipCardView
                            {...other}
                            {...otherPersonOrganisationConfig}
                            key="personOrganisationRelationshipView"
                            labels={personOrganisationRelationshipLabels}
                            subject={subject}
                            url={personOrganisationRelationshipUrl}
                            summaryUrl={organisationSummaryUrl}
                            afterResultsUpdate={this.handleAfterUpdate}
                        />
                    </Tab>
                    {childProtectionTab}
                </TabView>
            </>
        );
    }
    containerRef = elem => (this.containerElem = elem);
    handleAfterUpdate = () => {
        //Update the overflow property once the data has loaded
        if (this.containerElem) {
            this.containerElem.updateOverflow();
        }
    };
    getHeader() {
        const { labels } = this.props;
        return <Header title={labels.header.title} />;
    }
    render() {
        return (
            <ScrollableWidget ref={this.containerRef} className="relationship-widget" header={this.getHeader()}>
                {this.getContent()}
            </ScrollableWidget>
        );
    }
}
