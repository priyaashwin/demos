'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../../../infopop/infopop';

const Icon=({type, title})=>(
  <InfoPop key={type} content={title} useMarkup={false}>
    <span className={`attribute-icon attribute-${type}`}></span>
  </InfoPop>
);

Icon.propTypes={
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default Icon;
