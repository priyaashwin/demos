'use strict';
import React, { Children } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Content = ({ className, children }) => (
    <div className={classNames('card-cols',  `card-${Children.count(children)}-cols`, className)}>
        {
            Children.map(children, (child, i) => (
                <div key={i} className="data-item">
                    {child}
                </div>
            ))
        }
    </div>
);

Content.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string
};

export default Content;
