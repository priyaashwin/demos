'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Type=({type})=>{  
  return (
    <div>
      <span className="data-value">{type}</span>
    </div>
  );
};

Type.propTypes={
  type: PropTypes.string,
};

export default Type;
