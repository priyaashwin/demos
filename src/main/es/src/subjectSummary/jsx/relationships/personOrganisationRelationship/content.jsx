'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Contact from './contact';
import Type from './type';
import Icons from './icons';
import CardContent from '../content';

const Content = ({ labels, ...other }) => (
  <CardContent>
    <Type {...other} labels={labels} />
    <Contact {...other} labels={labels} />
    <Icons {...other} labels={labels.icons} />
  </CardContent>
);

Content.propTypes = {
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object.isRequired
};

export default Content;
