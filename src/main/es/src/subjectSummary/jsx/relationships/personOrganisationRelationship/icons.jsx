'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Icon from '../icon';

const attributes = {
  'pr': {code: 'PARENTAL_RESPONSIBILITY', name: 'parentalResponsibility'},
  'rla': {code: 'RESPONSIBLE_LOCAL_AUTHORITY', name: 'responsibleLocalAuthority'},
  'ec': {code: 'EMERGENCY_CONTACT', name: 'emergencyContact'},
  'kh': {code: 'KEY_HOLDER', name: 'keyHolder'},
  'dwp': {code: 'DWP_APPOINTEE', name: 'dwpAppointee'},
  'cad': {code: 'COURT_APPOINTED_DEPUTY', name: 'courtAppointedDeputy'}
};

export default class Icons extends PureComponent {
  static propTypes = {
    relation: PropTypes.object,
    subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    labels: PropTypes.object.isRequired,
    allowedAttributes: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  isIconRelevant(role, relation) {
    const { subjectId } = this.props;

    if (role === 'ROLE_B' && Number(relation.personVO.id) === Number(subjectId)) {
      return true;
    }
    return false;
  }

  render() {
    const { relation, labels, allowedAttributes } = this.props;

    const icons = [];

    for (const [key, obj] of Object.entries(attributes)) {
      if (this.isIconRelevant(relation[obj.name], relation) && (allowedAttributes.indexOf(obj.code) !== -1)) {
         icons.push(<Icon key={key} type={key} title={labels[key]} />);
      }
    }

    return (
      <div className="relationship-attributes">
        {icons}
      </div>
    );
  }
}