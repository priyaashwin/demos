'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import OrgContact from '../contact';

const Contact = ({ relation }) => (
  <OrgContact contact={relation.orgContactNumber} />
);


Contact.propTypes = {
  relation: PropTypes.object
};

export default Contact;
