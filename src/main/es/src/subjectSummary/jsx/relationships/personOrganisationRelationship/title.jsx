'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Title from '../../organisations/title';

const RelationshipTitle = ({ relation, summaryUrl, ...other }) => {
  return (
    <Title
      id={relation.organisationVO.id}
      name={relation.orgName}
      organisationSummaryUrl={summaryUrl}
      {...other} />
  );
};

RelationshipTitle.propTypes = {
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object.isRequired,
  summaryUrl: PropTypes.string.isRequired
};

export default RelationshipTitle;
