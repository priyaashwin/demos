'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import RelationshipType from '../type';

const Type = ({ relation }) => (
  <RelationshipType type={relation.orgRelationshipType} />
);

Type.propTypes = {
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object.isRequired
};

export default Type;
