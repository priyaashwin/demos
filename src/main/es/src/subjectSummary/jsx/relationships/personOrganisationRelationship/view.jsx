'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import ModelList from '../../../../model/modelList';
import Title from './title';
import Content from './content';
import RemotePaginatedCardList from '../../../../card/remotePaginatedCardList';

const CLASS_NAME = 'person-card';

export default class RelationshipCardView extends PureComponent {
  static propTypes = {
    url: PropTypes.string,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }),
    labels: PropTypes.object.isRequired,
    summaryUrl: PropTypes.string.isRequired,
    afterResultsUpdate: PropTypes.func.isRequired,
    allowedAttributes: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  getRestEndpoint() {
    const { url, subject } = this.props;
    return {
      ...endpoint.personOrganisationRelationshipsListEndpoint,
      url: new ModelList(url).getURL(subject)
    };
  }
  getCardTitle = (relation) => {
    const { subject, summaryUrl, labels } = this.props;

    return (
      <Title
        subjectId={subject.subjectId}
        labels={labels}
        relation={relation}
        summaryUrl={summaryUrl}
      />
    );
  };
  getCardClassName = () => CLASS_NAME;
  getCardContent = (relation) => {
    const { subject, labels, allowedAttributes } = this.props;

    return (
      <Content
        subjectId={subject.subjectId}
        relation={relation}
        labels={labels}
        allowedAttributes={allowedAttributes} />
    );
  };
  render() {
    const { labels, afterResultsUpdate } = this.props;
    return (
      <RemotePaginatedCardList
        restEndpoint={this.getRestEndpoint()}
        className="card-view"
        getCardTitle={this.getCardTitle}
        getCardContent={this.getCardContent}
        getCardClassName={this.getCardClassName}
        afterUpdate={afterResultsUpdate}
        enableHover={false}
        enableShadow={false}
        pageSize={-1}
        page={1}
        noResultsMessage={labels.noRecordsFound} />
    );
  }
}
