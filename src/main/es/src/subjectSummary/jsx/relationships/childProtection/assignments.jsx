'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import memoize from 'memoize-one';
import CardContent from '../content';
import { formatDate } from '../../../../date/date';

export default class Classifications extends PureComponent {
  static propTypes = {
    relation: PropTypes.object,
    subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    labels: PropTypes.object.isRequired,
  };

  _getClassificationAssignmentsByGroup = memoize((subjectId, relation) => {
    const subjectPersonId = Number(subjectId);
    //get classifications based on side subjectId can be found on
    let cpAssignments;
    if (Number(relation.roleAPersonId) === subjectPersonId) {
      cpAssignments = relation.roleBPersonChildProtectionDetails;
    } else {
      cpAssignments = relation.roleAPersonChildProtectionDetails;
    }

    const groups = {};
    //break out by parent group
    (cpAssignments || []).forEach(assignment => {
      let assignments = [];
      const groupId = assignment.classificationVO.parentGroupId;
      if (groups[groupId]) {
        assignments = groups[groupId];
      } else {
        groups[groupId] = assignments;
      }

      //insert assigment
      assignments.push(assignment);
    });
    const sort = function (a, b) {
      const aEndDate = a.endDate || Number.MAX_SAFE_INTEGER;
      const bEndDate = b.endDate || Number.MAX_SAFE_INTEGER;

      let r = bEndDate - aEndDate;
      if (r === 0) {
        r = b.startDate - a.startDate;
      }

      return r;
    };

    //sort by assignment date
    Object.values(groups).forEach(assignments => assignments.sort(sort));

    return groups;
  });
  getClassificationAssignmentsByGroup() {
    const { subjectId, relation } = this.props;
    return this._getClassificationAssignmentsByGroup(subjectId, relation);
  }
  render() {
    const groupedAssignments = this.getClassificationAssignmentsByGroup();

    return (
      <div className="cp-classifications">
        {
          Object.values(groupedAssignments).map((assignments, i) => (
            <div className="cp-group" key={`grp${i}`}>
              <span className="data-label">{assignments[0].classificationVO.classificationGroupName}</span>
              {
                assignments.map((assignment, i) => {
                  const classificationVO = assignment.classificationVO || {};

                  let endDate = assignment.endDate;

                  if (classificationVO.status === 'ARCHIVED' && !endDate) {
                    //set the end-date to the date the classification was archived
                    endDate = classificationVO.archivedDate;
                  }

                  return (
                    <CardContent key={i}>
                      <span>
                        {classificationVO.name}
                      </span>
                      <span>
                        {`(${formatDate(assignment.startDate, true)}${endDate ? ' to ' : ''}${endDate ? formatDate(endDate, true) : ''})`}
                      </span>
                    </CardContent>
                  );
                })
              }
            </div>)
          )
        }
      </div>
    );
  }
}
