'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RemotePaginatedCardList from '../../../../card/remotePaginatedCardList';
import endpoint from '../../../js/cfg';
import ModelList from '../../../../model/modelList';
import Title from './title';
import Content from './content';

const CLASS_NAME = 'person-card';

export default class ChildProtection extends Component {
    static propTypes = {
        url: PropTypes.string,
        subject: PropTypes.shape({
            subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            subjectType: PropTypes.string
        }),
        permissions:PropTypes.shape({
            canViewAddress:PropTypes.bool
        }).isRequired,
        labels: PropTypes.object.isRequired,
        summaryUrl: PropTypes.string.isRequired,
        afterResultsUpdate: PropTypes.func.isRequired,
        classificationGroupPath: PropTypes.string.isRequired,
        classificationPathSeparator: PropTypes.string.isRequired,
        subjectPictureEndpoint: PropTypes.object.isRequired
    };

    getRestEndpoint() {
        const { url, subject, classificationGroupPath, classificationPathSeparator } = this.props;
        return {
            ...endpoint.childProtectionRelationshipsListEndpoint,
            url: new ModelList(url).getURL(subject),
            params: {
                ...endpoint.childProtectionRelationshipsListEndpoint.params,
                separator: classificationPathSeparator,
                childProtectionClassificationGroupPath: classificationGroupPath
            }
        };
    }
    getCardTitle = (relation) => {
        const { subject, summaryUrl, subjectPictureEndpoint, labels } = this.props;

        return (
            <Title
                subjectId={subject.subjectId}
                labels={labels}
                relation={relation}
                summaryUrl={summaryUrl}
                subjectPictureEndpoint={subjectPictureEndpoint}
            />
        );
    };
    getCardClassName = () => CLASS_NAME;
    getCardContent = (relation) => {
        const { subject, labels, ...other } = this.props;

        return (
            <Content
                subjectId={subject.subjectId}
                relation={relation}
                labels={labels} 
                {...other}/>
        );
    };
    render() {
        const { labels, afterResultsUpdate } = this.props;
        return (
            <RemotePaginatedCardList
                restEndpoint={this.getRestEndpoint()}
                className="card-view"
                getCardTitle={this.getCardTitle}
                getCardContent={this.getCardContent}
                getCardClassName={this.getCardClassName}
                afterUpdate={afterResultsUpdate}
                enableHover={false}
                enableShadow={false}
                pageSize={-1}
                page={1}
                noResultsMessage={labels.noRecordsFound} />
        );
    }
}