'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import CardContent from '../content';
import Address from './address';
import Assignments from './assignments';

const Content = ({ labels, ...other }) => (
  <>
    <CardContent>
      <Assignments {...other} labels={labels} />
    </CardContent>
    <CardContent>
      <Address {...other} labels={labels.address} />
    </CardContent>
  </>
);

Content.propTypes = {
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object.isRequired
};

export default Content;
