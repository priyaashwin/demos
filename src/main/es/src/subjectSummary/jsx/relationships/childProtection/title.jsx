'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Title from '../../people/title';
import DateOfBirth from '../../../../dateOfBirth/dateOfBirth';
import { getPersonFromRelationship } from '../../../js/relationship/person';

const RelationshipTitle = ({ subjectId, relation, summaryUrl, ...other }) => {
  const person = getPersonFromRelationship(subjectId, relation);

  const { name, id } = person;
  return (
    <>
      <Title
        id={id}
        name={name}
        personSummaryUrl={summaryUrl}
        {...other} />
      <DateOfBirth person={person} />
    </>
  );
};

RelationshipTitle.propTypes = {
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object.isRequired,
  summaryUrl: PropTypes.string.isRequired,
  subjectPictureEndpoint: PropTypes.object.isRequired

};

export default RelationshipTitle;
