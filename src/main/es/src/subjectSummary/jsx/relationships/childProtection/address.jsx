'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withPreferredAddress } from '../../../../address/js/withPreferredAddress';
import SingleLineAddress from '../../../../address/jsx/singleLineAddress';
import {getPersonFromRelationship} from '../../../js/relationship/person';

//wrap single line address in HOC to ensure this is preferred address
const PreferredAddress = withPreferredAddress(SingleLineAddress);

export default class Address extends PureComponent {
  static propTypes = {
    relation: PropTypes.object,
    subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    labels: PropTypes.object.isRequired,
    permissions: PropTypes.object.isRequired,
    mapLinkUrl: PropTypes.string.isRequired,
    codedEntries: PropTypes.shape({
      unknownLocation: PropTypes.object.isRequired
    }).isRequired,
  };

  render() {
    const { labels, relation, subjectId, ...other } = this.props;
    const person = getPersonFromRelationship(subjectId, relation);
    const address = person.address;
    return (
      <div>
        <span className="data-label">{`${labels.address}: `}</span>
        <span className="data-value">
          <PreferredAddress address={address} labels={labels} {...other} />
        </span>
      </div>
    );
  }
}
