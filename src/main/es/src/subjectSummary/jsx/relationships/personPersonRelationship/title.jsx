'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Title from '../../people/title';

const RelationshipTitle = ({ subjectId, relation, summaryUrl, ...other }) => {
  const subjectPersonId = Number(subjectId);

  let id;
  let name;
  if (Number(relation.roleAPersonId) === subjectPersonId) {
    name = relation.roleBPersonName;
    id = relation.roleBPersonId;
  } else {
    name = relation.roleAPersonName;
    id = relation.roleAPersonId;
  }

  return (
    <Title
      id={id}
      name={name}
      personSummaryUrl={summaryUrl}
      {...other} />
  );
};

RelationshipTitle.propTypes = {
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object.isRequired,
  summaryUrl: PropTypes.string.isRequired,
  subjectPictureEndpoint: PropTypes.object.isRequired

};

export default RelationshipTitle;
