'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../../js/cfg';
import ModelList from '../../../../model/modelList';
import Title from './title';
import Content from './content';
import RemotePaginatedCardList from '../../../../card/remotePaginatedCardList';

const CLASS_NAME = 'person-card';

export default class RelationshipCardView extends PureComponent {
  static propTypes = {
    url: PropTypes.string,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }),
    labels: PropTypes.object.isRequired,
    summaryUrl: PropTypes.string.isRequired,
    subjectPictureEndpoint: PropTypes.object.isRequired,
    afterResultsUpdate: PropTypes.func.isRequired,
    isNamedPersonRelationshipAvailable: PropTypes.bool.isRequired,
    allowedAttributes: PropTypes.arrayOf(PropTypes.string).isRequired
  };
  getRestEndpoint() {
    const { url, subject } = this.props;
    //merge our URL into the endpoint
    return {
      ...endpoint.personPersonRelationshipsListEndpoint,
      url: new ModelList(url).getURL(subject)
    };
  }
  getCardTitle = (relation) => {
    const { subject={}, summaryUrl, subjectPictureEndpoint, labels } = this.props;

    const {subjectId=0}=subject;
    let pictureEndpoint=subjectPictureEndpoint;

    //it can be determined ahead of time if there is a picture for this user
    //first work out which side is the subject, then check the opposite side
    //to see if there is a picture id
    if(Number(relation.roleAPersonId)===Number(subjectId)){
      if(!relation.roleBPictureId){
        //no picture so don't include the endpoint
        pictureEndpoint=undefined;
      }
    }else if(Number(relation.roleBPersonId)===Number(subjectId)){
      if(!relation.roleAPictureId){
        //no picture so don't include the endpoint
        pictureEndpoint=undefined;
      }
    }

    return (
      <Title subjectId={subject.subjectId} labels={labels} relation={relation} summaryUrl={summaryUrl} subjectPictureEndpoint={pictureEndpoint} />
    );
  };
  getCardClassName = () => CLASS_NAME;
  getCardContent = (relation) => {
    const { subject, labels, isNamedPersonRelationshipAvailable, allowedAttributes } = this.props;

    return (
      <Content
        subjectId={subject.subjectId}
        relation={relation}
        labels={labels}
        isNamedPersonRelationshipAvailable={isNamedPersonRelationshipAvailable}
        allowedAttributes={allowedAttributes} />
    );
  };
  render() {
    const { labels, afterResultsUpdate } = this.props;
    return (
      <RemotePaginatedCardList
        restEndpoint={this.getRestEndpoint()}
        className="card-view"
        getCardTitle={this.getCardTitle}
        getCardContent={this.getCardContent}
        getCardClassName={this.getCardClassName}
        afterUpdate={afterResultsUpdate}
        enableHover={false}
        enableShadow={false}
        pageSize={-1}
        page={1}
        noResultsMessage={labels.noRecordsFound} />
    );
  }
}


