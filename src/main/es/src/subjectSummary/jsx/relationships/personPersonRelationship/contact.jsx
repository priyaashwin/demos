'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PersonContact from '../contact';

const Contact = ({ subjectId, relation }) => {
  const subjectPersonId = Number(subjectId);

  let contact;

  if (Number(relation.roleAPersonId) === subjectPersonId) {
    contact = relation.roleBContactNumber;
  } else {
    contact = relation.roleAContactNumber;
  }

  return (
    <PersonContact contact={contact} />
  );
};

Contact.propTypes = {
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
};

export default Contact;
