'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import RelationshipType from '../type';

const Type=({subjectId, relation})=>{
  const subjectPersonId=Number(subjectId);

  let type;

  if(Number(relation.roleAPersonId) === subjectPersonId){
    type = relation.roleBRelationshipType;
  }else{
    type = relation.roleARelationshipType;
  }

  return (
    <RelationshipType type={type}/>
  );
};

Type.propTypes={
  relation: PropTypes.object,
  subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object.isRequired
};

export default Type;
