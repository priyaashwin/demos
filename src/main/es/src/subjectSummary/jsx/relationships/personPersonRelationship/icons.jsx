'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Icon from '../icon';

const attributes = {
  'aw': {code: 'ALLOCATED_WORKER', name: 'allocatedWorker'},
  'np': {code: 'NAMED_PERSON', name: 'namedPerson'},
  'pw': {code: 'PRIMARY_WORKER', name: 'primaryWorker'},
  'kw': {code: 'KEY_WORKER', name: 'keyWorker'},
  'lp': {code: 'LEAD_PROFESSIONAL', name: 'leadProfessional'},
  'fr': {code: 'FINANCIAL_REPRESENTATIVE', name: 'financialRepresentative'},
  'cm': {code: 'CARE_MANAGER', name: 'careManager'},
  'pr': {code: 'PARENTAL_RESPONSIBILITY', name: 'parentalResponsibility'},
  'rla': {code: 'RESPONSIBLE_LOCAL_AUTHORITY', name: 'responsibleLocalAuthority'},
  'nok': {code: 'NEXT_OF_KIN', name: 'nextOfKin'},
  'c': {code: 'MAIN_CARER', name: 'mainCarer'},
  'nc': {code: 'NON_CONTACT', name: 'nonContact'},
  'rc': {code: 'RESTRICTED_CONTACT', name: 'restrictedContact'},
  'ec': {code: 'EMERGENCY_CONTACT', name: 'emergencyContact'},
  'kh': {code: 'KEY_HOLDER', name: 'keyHolder'},
  'dwp': {code: 'DWP_APPOINTEE', name: 'dwpAppointee'},
  'g': {code: 'GUARDIAN', name: 'guardian'},
  'gw': {code: 'GUARDIAN_WELFARE', name: 'guardianWelfare'},
  'gf': {code: 'GUARDIAN_FINANCIAL', name: 'guardianFinancial'},
  'cadf': {code: 'COURT_APPOINTED_DEPUTY_FINANCIAL', name: 'courtAppointedDeputyFinancial'},
  'cad': {code: 'COURT_APPOINTED_DEPUTY', name: 'courtAppointedDeputy'},
  'poah': {code: 'POWER_OF_ATTORNEY_HEALTH', name: 'powerOfAttorneyHealth'},
  'poaf': {code: 'POWER_OF_ATTORNEY_FINANCE', name: 'powerOfAttorneyFinance'},
  'nr': {code: 'NEAREST_RELATIVE', name: 'nearestRelative'},
  'ad': {code: 'ADVOCATE', name: 'advocate'}
};

export default class Icons extends PureComponent {
  static propTypes = {
    relation: PropTypes.object,
    subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    labels: PropTypes.object.isRequired,
    isNamedPersonRelationshipAvailable: PropTypes.bool.isRequired,
    allowedAttributes: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  isIconRelevant(role, relation) {
    const { subjectId } = this.props;

    if (role === 'BOTH') {
      return true;
    }
    if (role === 'ROLE_A' && Number(relation.roleBPersonId) === Number(subjectId)) {
      return true;
    }
    if (role === 'ROLE_B' && Number(relation.roleAPersonId) === Number(subjectId)) {
      return true;
    }
    return false;
  }

  render() {
    const { relation, labels, isNamedPersonRelationshipAvailable, allowedAttributes } = this.props;

    const icons = [];

    for (const [key, obj] of Object.entries(attributes)) {
        //check if user has the right to view attribute 
        if ((allowedAttributes.indexOf(obj.code) !== -1) && !(key === 'np' && !isNamedPersonRelationshipAvailable)) {
            if (this.isIconRelevant(relation[obj.name], relation)) {
               icons.push(<Icon key={key} type={key} title={labels[key]} />);
            } else if (relation[obj.name]){
               icons.push(<Icon key={`${key}-opp`} type={`${key}-opp`} title={labels[`${key}opp`]} />);
            }
        }
    }

    return (
      <div className="relationship-attributes">
        {icons}
      </div>
    );
  }
}
