'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../../link/link';
import Subject from '../../../subject/subject';
import Icon from '../../../subject/icon';

const Title=({id, name, organisationSummaryUrl})=>{
  const identifier=`ORG${id}`;
  return (
    <div>
        <Icon key="icon" iconClass="fa fa-building-o"/>
        <Link url={organisationSummaryUrl} term={id}>
          <Subject subjectType="organisation" name={name} identifier={identifier}/>
        </Link>
    </div>
  );
};

Title.propTypes={
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.string.isRequired,
  labels: PropTypes.object.isRequired,
  organisationSummaryUrl: PropTypes.string.isRequired
};

export default Title;
