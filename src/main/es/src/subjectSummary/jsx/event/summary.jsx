'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Summary=({summary})=>(<span>{summary}</span>);

Summary.propTypes={
  summary: PropTypes.node
};

export default Summary;
