'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatDateTime} from '../../../date/date';

const Timestamp=({dateTime})=>(<span>{formatDateTime(dateTime, true)}</span>);

Timestamp.propTypes={
  dateTime: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number
  ])
};

export default Timestamp;
