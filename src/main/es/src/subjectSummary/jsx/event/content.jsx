'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Timestamp from './timestamp';
import Summary from './summary';
import InstigatingUser from './instigatingUser';

const Content=({event})=>(
  <div className="card-cols card-3-cols">
    <div className="data-item">
      <Timestamp dateTime={event.eventDate}/>
    </div>
    <div className="data-item">
      <Summary summary={event.eventSummary}/>
    </div>
    <div className="data-item">
      <InstigatingUser name={event.instigatingUserName}/>
    </div>
  </div>
);

Content.propTypes={
  event: PropTypes.object,
  labels: PropTypes.object.isRequired
};

export default Content;
