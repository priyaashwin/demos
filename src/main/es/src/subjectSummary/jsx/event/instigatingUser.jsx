'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const InstigatingUser=({name})=>(<span>{name?`by ${name}`:''}</span>);

InstigatingUser.propTypes={
  name: PropTypes.string
};

export default InstigatingUser;
