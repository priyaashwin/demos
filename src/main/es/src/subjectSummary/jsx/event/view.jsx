'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ScrollableWidget from '../widget/scrollable';
import RemotePaginatedCardList from '../../../card/remotePaginatedCardList';
import endpoint from '../../js/cfg';
import ModelList from '../../../model/modelList';
import Header from '../widget/header';
import Content from './content';

const CARD_CLASS_NAME = 'event-card';

export default class EventCardView extends Component {
  static propTypes = {
    title: PropTypes.string,
    url: PropTypes.string,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectType: PropTypes.string
    }),
    labels: PropTypes.object.isRequired
  };

  getRestEndpoint(url, subject) {
    //merge our URL into the endpoint
    return {
      ...endpoint.eventListEndpoint,
      url: new ModelList(url).getURL(subject)
    };
  }
  handleAfterUpdate=()=> {
    //Update the overflow property once the data has loaded
    if (this.containerElem) {
      this.containerElem.updateOverflow();
    }
  };

  getCardContent=(event)=> {
    const {labels}=this.props;

    return (<Content event={event} labels={labels}/>);
  }
  getCardTitle=()=>{return false;};
  getCardClassName=()=> {
    return CARD_CLASS_NAME;
  };
  containerRef=(elem)=>{
    this.containerElem = elem;
  };
  render() {
    const {labels, url, subject} = this.props;

    const header = (<Header title={labels.header.title}/>);

    return (
      <ScrollableWidget ref={this.containerRef} className="events-widget" header={header}>
        <RemotePaginatedCardList
          restEndpoint={this.getRestEndpoint(url, subject)}
          className="card-view"
          getCardTitle={this.getCardTitle}
          getCardContent={this.getCardContent}
          getCardClassName={this.getCardClassName}
          afterUpdate={this.handleAfterUpdate}
          enableHover={false}
          enableShadow={false}
          pageSize={10}
          page={1}
          noResultsMessage={labels.noRecordsFound}
          showMoreMessage={false}/>
      </ScrollableWidget>
    );
  }
}
