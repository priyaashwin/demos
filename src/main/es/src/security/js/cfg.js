'use strict';

class Endpoint{
  securityAccessEndpoint={
    method: 'get',
    contentType: 'json',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/vnd.olmgroup-usp.securityextenstion.securityAccessElementAccessLevels+json'
    },
    responseType: 'json',
    responseStatus:200,
    params:{
      recordType:'',
      securityDomain:''
    }
  };
}

export default new Endpoint();
