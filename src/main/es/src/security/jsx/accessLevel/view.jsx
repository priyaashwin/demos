'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Key from './key/key';
import AccessLevelTree from './recordType/tree';
import { Map, List, Seq } from 'immutable';
import Model from '../../../model/model';
import endpoint from '../../js/cfg';

export default class AccessLevelView extends Component {
    static propTypes = {
        securityAccessUrl: PropTypes.string.isRequired,
        securityRecordTypeConfiguration: PropTypes.shape({
            id: PropTypes.any,
            defaultAccessLevel: PropTypes.oneOf(['NONE', 'READ_SUMMARY', 'READ_DETAIL', 'WRITE', 'ADMIN']),
            securedRecordTypes: PropTypes.arrayOf(
                PropTypes.shape({
                    declaredAccessLevel: PropTypes.string,
                    effectiveAccessLevel: PropTypes.string.isRequired,
                    type: PropTypes.string.isRequired
                })
            ).isRequired,
            objectVersion: PropTypes.number.isRequired
        }),
        accessLevels: PropTypes.arrayOf(
            PropTypes.shape({
                key: PropTypes.string.isRequired,
                value: PropTypes.string.isRequired,
                label: PropTypes.string.isRequired,
                colour: PropTypes.string.isRequired,
                fontColour: PropTypes.string.isRequired
            })
        ).isRequired,
        //flag to track if this is edit mode
        isEdit: PropTypes.bool
    };

    state = {
        //immutable map to contain loaded security access elements
        securityAccessElements: Map(),
        explicitAccessLevels: this.initExplicitAccessLevels(
            this.props.securityRecordTypeConfiguration.securedRecordTypes,
            Map()
        )
    };

    componentDidUpdate(prevProps) {
        let securityAccessElements = this.state.securityAccessElements;

        const securedRecordTypes = this.props.securityRecordTypeConfiguration.securedRecordTypes;

        let explicitAccessLevels = this.state.explicitAccessLevels;

        //array of access elements that require a re-load
        const reloadAccessElements = [];

        if (
            prevProps.securityRecordTypeConfiguration.objectVersion !==
            this.props.securityRecordTypeConfiguration.objectVersion
        ) {
            Seq(securityAccessElements).forEach((accessElement, type) => {
                if (List.isList(accessElement)) {
                    reloadAccessElements.push({
                        type: type
                    });
                } else if (Map.isMap(accessElement)) {
                    if (accessElement.size > 0) {
                        accessElement.keySeq().forEach(key =>
                            reloadAccessElements.push({
                                type: type,
                                domain: key
                            })
                        );
                    }
                }
            });

            //clear down access elements
            securityAccessElements = Map();
        }

        //if changing from edit mode - re-initialise the explicit access levels
        if (
            (!this.props.isEdit && prevProps.isEdit) ||
            prevProps.securityRecordTypeConfiguration.objectVersion !==
                this.props.securityRecordTypeConfiguration.objectVersion
        ) {
            explicitAccessLevels = this.initExplicitAccessLevels(securedRecordTypes, securityAccessElements);

            // eslint-disable-next-line react/no-did-update-set-state
            this.setState(
                {
                    securityAccessElements: securityAccessElements,
                    explicitAccessLevels: explicitAccessLevels
                },
                () =>
                    reloadAccessElements.forEach(element => {
                        this.loadSecurityAccessElements(element.type, element.domain);
                    })
            );
        }
    }

    initExplicitAccessLevels(securedRecordTypes, securityAccessElements) {
        const explicitAccessLevels = Map();
        const { securityRecordTypeConfiguration } = this.props;

        const defaultAccessLevel = securityRecordTypeConfiguration.defaultAccessLevel;

        return explicitAccessLevels.withMutations(explicitAccessLevels => {
            //default level is always considered explicit
            explicitAccessLevels.set('declaredAccessLevel', defaultAccessLevel);

            Seq(securedRecordTypes).forEach(recordType => {
                const path = [recordType.type];
                explicitAccessLevels.setIn(path.concat('declaredAccessLevel'), recordType.declaredAccessLevel);
                explicitAccessLevels.setIn(path.concat('overrideCount'), recordType.overrideCount);

                if (recordType.supportsSecurityDomains) {
                    Seq(recordType.securityDomains).forEach(securityDomain => {
                        const domainPath = path.concat(securityDomain.code);
                        explicitAccessLevels.setIn(
                            domainPath.concat('declaredAccessLevel'),
                            securityDomain.declaredAccessLevel
                        );
                        explicitAccessLevels.setIn(domainPath.concat('overrideCount'), securityDomain.overrideCount);
                        this._updateDeclaredAccessLevel(
                            explicitAccessLevels,
                            domainPath,
                            Seq(securityAccessElements.getIn(domainPath))
                        );
                    });
                } else {
                    this._updateDeclaredAccessLevel(
                        explicitAccessLevels,
                        path,
                        Seq(securityAccessElements.getIn(path))
                    );
                }
            });
        });
    }
    updateSecurityAccessElementLevel = (pathStr, accessLevel) => {
        //if not edit mode - don't let edits occur
        if (!this.props.isEdit) {
            return;
        }

        const path = pathStr.length > 0 ? pathStr.split(',') : [];

        this.setState({
            explicitAccessLevels: this.state.explicitAccessLevels.withMutations(explicitAccessLevels => {
                const accessLevelPath = path.concat('declaredAccessLevel');

                let mod = 0;

                const currentAccessLevel = explicitAccessLevels.getIn(accessLevelPath);

                //toggle only possible if not at root path
                if (currentAccessLevel === accessLevel && path.length > 0) {
                    //Remove - this is a toggle
                    explicitAccessLevels.setIn(accessLevelPath, null);
                    //decrement;
                    mod = -1;
                } else {
                    if ((currentAccessLevel === null || currentAccessLevel === undefined) && path.length > 0) {
                        //increment - only if unset
                        mod = +1;
                    }
                    //set the value
                    explicitAccessLevels.setIn(accessLevelPath, accessLevel);
                }

                //update overrides count at every level of the tree - make a copy of the path array
                //we start 1 higher in the tree
                const currentPath = path.slice(0, -1);

                while (currentPath.length > 0) {
                    explicitAccessLevels.updateIn(currentPath.concat('overrideCount'), overrideCount =>
                        Math.max((overrideCount || 0) + mod, 0)
                    );

                    //traverse up the path
                    currentPath.splice(-1, 1);
                }
            })
        });
    };
    _updateDeclaredAccessLevel(explicitAccessLevels, path, accessElements) {
        accessElements.forEach(accessElement => {
            const elementPath = path.concat(accessElement.identifier);

            explicitAccessLevels.setIn(elementPath.concat('declaredAccessLevel'), accessElement.declaredAccessLevel);
            explicitAccessLevels.setIn(elementPath.concat('overrideCount'), accessElement.overrideCount);

            if (accessElement.childAccessElements) {
                //sync child elements
                this._updateDeclaredAccessLevel(
                    explicitAccessLevels,
                    path.concat(accessElement.identifier),
                    accessElement.childAccessElements
                );
            }
        });
    }
    setSecurityAccessElements(path, accessElements) {
        this.setState({
            securityAccessElements: this.state.securityAccessElements.updateIn(path, () => List(accessElements)),
            explicitAccessLevels: this.state.explicitAccessLevels.withMutations(explicitAccessLevels => {
                this._updateDeclaredAccessLevel(explicitAccessLevels, path, accessElements);
            })
        });
    }
    loadSecurityAccessElements = (type, domain) => {
        const { securityAccessUrl, securityRecordTypeConfiguration } = this.props;

        const path = [type];
        if (domain) {
            path.push(domain);
        }

        if (this.state.securityAccessElements.getIn(path)) {
            //already loaded
            return;
        }

        const params = {
            securedRecordType: type
        };

        //security domain is optional
        if (domain) {
            params.securityDomain = domain;
        }

        //load the next layer down
        new Model(securityAccessUrl)
            .load(
                {
                    ...endpoint.securityAccessEndpoint,
                    url: securityAccessUrl,
                    params: params
                },
                { id: securityRecordTypeConfiguration.id }
            )
            .then(success => {
                this.setSecurityAccessElements(path, success.accessElements);
            })
            .catch(() => {
                //todo?
            });
    };
    getDefaultAccessLevel() {
        return this.state.explicitAccessLevels.getIn(['declaredAccessLevel']);
    }
    getUpdateRecordAccessLevels() {
        const { securityRecordTypeConfiguration } = this.props;
        const securedRecordTypes = securityRecordTypeConfiguration.securedRecordTypes;

        const securityAccessElements = this.state.securityAccessElements;
        const explicitAccessLevels = this.state.explicitAccessLevels;

        const updateRecordAccessLevels = [];

        const updateRecordForAccessElement = (path, recordType, domain, accessElements) => {
            if (accessElements) {
                accessElements.forEach(accessElement => {
                    const elementPath = path.concat(accessElement.identifier);

                    const declaredAccessLevel = explicitAccessLevels.getIn(elementPath.concat('declaredAccessLevel'));

                    if (declaredAccessLevel !== accessElement.declaredAccessLevel) {
                        updateRecordAccessLevels.push({
                            securedRecordType: recordType.type,
                            securityDomain: domain,
                            identifierType: accessElement.identifierType,
                            identifier: accessElement.identifier,
                            accessLevel: declaredAccessLevel
                        });
                    }

                    if (accessElement.childAccessElements) {
                        updateRecordForAccessElement(
                            path.concat(accessElement.identifier),
                            recordType,
                            domain,
                            accessElement.childAccessElements
                        );
                    }
                });
            }
        };

        Seq(securedRecordTypes).forEach(recordType => {
            const path = [recordType.type];
            if (recordType.supportsSecurityDomains) {
                Seq(recordType.securityDomains).forEach(securityDomain => {
                    const domainPath = path.concat(securityDomain.code);
                    updateRecordForAccessElement(
                        domainPath,
                        recordType,
                        securityDomain.code,
                        securityAccessElements.getIn(domainPath)
                    );

                    const declaredAccessLevel = explicitAccessLevels.getIn(domainPath.concat('declaredAccessLevel'));
                    if (declaredAccessLevel !== securityDomain.declaredAccessLevel) {
                        updateRecordAccessLevels.push({
                            securedRecordType: recordType.type,
                            securityDomain: securityDomain.code,
                            accessLevel: declaredAccessLevel
                        });
                    }
                });
            }
            updateRecordForAccessElement(path, recordType, null, securityAccessElements.getIn(path));

            const declaredAccessLevel = explicitAccessLevels.getIn(path.concat('declaredAccessLevel'));
            if (declaredAccessLevel !== recordType.declaredAccessLevel) {
                updateRecordAccessLevels.push({
                    securedRecordType: recordType.type,
                    accessLevel: declaredAccessLevel
                });
            }
        });

        return updateRecordAccessLevels;
    }
    render() {
        const { securityRecordTypeConfiguration, securityAccessUrl, accessLevels } = this.props;
        const securedRecordTypes = securityRecordTypeConfiguration.securedRecordTypes;

        const { securityAccessElements, explicitAccessLevels } = this.state;
        return (
            <div>
                <AccessLevelTree
                    securityRecordId={securityRecordTypeConfiguration.id}
                    securityAccessUrl={securityAccessUrl}
                    securedRecordTypes={securedRecordTypes}
                    accessLevels={accessLevels}
                    explicitAccessLevels={explicitAccessLevels}
                    securityAccessElements={securityAccessElements}
                    loadSecurityAccessElements={this.loadSecurityAccessElements}
                    updateSecurityAccessElementLevel={this.updateSecurityAccessElementLevel}
                    key={securityRecordTypeConfiguration.id}
                />

                <Key key="access-level-key" accessLevels={accessLevels} className="access-level-key" />
            </div>
        );
    }
}
