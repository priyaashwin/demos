'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Key=({accessLevels, className})=>(
  <div className={classnames(className, 'key')}>
    <ul>
      {accessLevels.map((accessLevel, i)=>(
        <li key={i}>
          <span className="key-value" style={{
            color: accessLevel.fontColour,
            backgroundColor: accessLevel.colour,
          }}>{accessLevel.value}</span>
          <span className="label">{accessLevel.label}</span>
        </li>
      ))}
    </ul>
  </div>
);

Key.propTypes={
  className:PropTypes.string,
  accessLevels:PropTypes.arrayOf(PropTypes.shape({
    value:  PropTypes.string.isRequired,
    colour: PropTypes.string.isRequired,
    fontColour: PropTypes.string.isRequired
   })).isRequired
};

export default Key;
