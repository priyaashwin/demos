'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const RecordTypeIcon=({type})=>(
  <span className={classnames('fa-stack', 'fa-md')} >
    <i className={classnames('fa', 'fa-circle', 'fa-stack-2x')} />
    <i className={classnames('fa', iconClass(type), 'fa-stack-1x', 'fa-inverse')} aria-hidden="true" label={type}/>
  </span>);

const iconClass = function(type) {
  let iconClass;
  switch (type) {
    case 'person-reference-number':
      iconClass='fa-hashtag';
      break;
    case 'warning':
      iconClass='fa-exclamation-triangle';
      break;
    case 'child-carer-training':
    case 'type-of-care':
    case 'foster-carer-suspension-reason':
    case 'foster-carer-under-investigation':
    case 'placement-absence-reason':
    case 'placement-change-reason':
    case 'episode-end-reason':
    case 'episode-start-reason':
    case 'care-leaver-eligibility':
    case 'care-leaver-activity':
    case 'care-leaver-accommodation':
      iconClass='eclipse-childlookedafter';
      break;
    default:
      iconClass=`eclipse-${type.replace(/\s/g, '-')}`;
      break;
  }
  return iconClass;
};

RecordTypeIcon.propTypes={
    type:PropTypes.string.isRequired
};

export default RecordTypeIcon;
