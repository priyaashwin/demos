'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import AccessLevelControl from './accessLevelControl';
import TreeView from 'usp-simple-tree';
import {Map} from 'immutable';
import classnames from 'classnames';
import RecordTypeIcon from './recordTypeIcon';

const loading=(<span key="loading"><i className="fa fa-spinner fa-spin" aria-hidden="true"/> loading...</span>);

export default class AccessLevelTree extends PureComponent {
    static propTypes={
      accessLevels:PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        value:  PropTypes.string.isRequired,
        label:  PropTypes.string.isRequired,
        colour: PropTypes.string.isRequired,
        fontColour: PropTypes.string.isRequired
      })).isRequired,
      securedRecordTypes:PropTypes.arrayOf(PropTypes.shape({
        type: PropTypes.string.isRequired,
        securityDomains: PropTypes.array.isRequired,
        supportsSecurityDomains:PropTypes.bool.isRequired
      })).isRequired,
      explicitAccessLevels:PropTypes.instanceOf(Map).isRequired,
      securityAccessElements:PropTypes.instanceOf(Map).isRequired,
      loadSecurityAccessElements:PropTypes.func.isRequired,
      updateSecurityAccessElementLevel:PropTypes.func.isRequired
    };

    toggleSecurityAccessElementsTree=(open, treeNode)=>{
      const {loadSecurityAccessElements}=this.props;
      if(open){
        const recordType=treeNode.props.recordType;
        const securityDomain=treeNode.props.securityDomain;

        loadSecurityAccessElements(recordType.type, securityDomain?securityDomain.code:undefined);
      }
    }

    getChildAccessElementsTree(path, childAccessElements){
      return childAccessElements.map(element=>{
        let children;

        const childPath=path.concat(element.identifier);

        if(element.childAccessElements.length>0){
            children=this.getChildAccessElementsTree(childPath, element.childAccessElements);
        }
        return (
          <TreeView
              key={element.identifier}
              label={this.getAccessLevel(childPath, element.displayName)}
              className={classnames({overridden:this.getOverrideCountFromPath(childPath)})}>
              {children}
          </TreeView>
        );
      });
    }
    getSecurityAccessElements(type, domain){
      const {securityAccessElements}=this.props;
      const path=[type];
      if(domain){
        path.push(domain);
      }

      let content;
      const elements=securityAccessElements.getIn(path);
      if(!elements){
        content=loading;
      }else{
        content=this.getChildAccessElementsTree(path, elements);
      }
      return content;
    }
    getAccessLevel(path, label){
      const {accessLevels, updateSecurityAccessElementLevel}=this.props;

      const accessLevelInfo=this.getAccessLevelFromPathHierarchy(path);

      return (
        <span>
          <AccessLevelControl
            onChangeLevel={updateSecurityAccessElementLevel}
            path={path.join(',')}
            accessLevels={accessLevels}
            isDeclared={accessLevelInfo.declaredAtPath}
            accessLevel={accessLevelInfo.accessLevel}/>
          <span>{' '}</span>
          <span>{label}</span>
        </span>
      );
    }
    getSecuredRecordTypesTree(){
      const {securedRecordTypes}=this.props;
      return securedRecordTypes.map(recordType=>{
        let tree;

        if(recordType.supportsSecurityDomains){
          const path=[recordType.type];

          //add the security domains
          tree=(<TreeView key={recordType.type} className={classnames({overridden:this.getOverrideCountFromPath(path)})} label={
            <div className="record-type">
              <RecordTypeIcon type={recordType.type}/>
              {this.getAccessLevel(path, recordType.type)}
            </div>
            }>
            {recordType.securityDomains.map(securityDomain=>(
              <TreeView
                  key={securityDomain.code}
                  label={this.getAccessLevel(path.concat(securityDomain.code), securityDomain.name)}
                  onToggle={this.toggleSecurityAccessElementsTree}
                  recordType={recordType}
                  securityDomain={securityDomain}
                  className={classnames({overridden:this.getOverrideCountFromPath(path.concat(securityDomain.code))})}
                  >
                  {this.getSecurityAccessElements(recordType.type, securityDomain.code)}
              </TreeView>
            ))
          }
          </TreeView>);
        }else{
          const path=[recordType.type];
          tree=(<TreeView
              key={recordType.type}
              label={(
                <div className="record-type">
                  <RecordTypeIcon type={recordType.type}/>
                  {this.getAccessLevel(path, recordType.type)}
                </div>
              )}
              onToggle={this.toggleSecurityAccessElementsTree}
              recordType={recordType}
              className={classnames({overridden:this.getOverrideCountFromPath(path)})}>
              {this.getSecurityAccessElements(recordType.type)}
          </TreeView>);
        }

        return tree;
      });
    }
    getAccessLevelFromPathHierarchy(path){
      const {explicitAccessLevels}=this.props;

      let accessLevel=null;
      let currentPath=path;
      let length;
      do{
        length=currentPath.length;
        accessLevel=explicitAccessLevels.getIn(currentPath.concat('declaredAccessLevel'));
        currentPath=currentPath.slice(0,-1);
      }while(!accessLevel&&length>0);

      return {
        accessLevel:accessLevel,
        declaredAtPath:length===path.length
      };
    }
    getOverrideCountFromPath(path){
      const {explicitAccessLevels}=this.props;

      return explicitAccessLevels.getIn(path.concat('overrideCount'));
    }
    getTree(){
      const rootPath=[];
      return (<TreeView
            key="root"
            open={true}
            label={this.getAccessLevel(rootPath, 'Default setting')}>
            {this.getSecuredRecordTypesTree()}
          </TreeView>);
    }

    render() {
        return (
          <div className="access-level-tree">
            {this.getTree()}
          </div>
        );
    }
}
