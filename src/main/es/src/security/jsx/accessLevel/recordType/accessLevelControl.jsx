'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';


export default class AccessLevelControl extends PureComponent{
  static propTypes={
    accessLevels:PropTypes.arrayOf(PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      value:  PropTypes.string.isRequired,
      colour: PropTypes.string.isRequired,
      fontColour: PropTypes.string.isRequired
    })).isRequired,
    isDeclared: PropTypes.bool,
    path:PropTypes.string.isRequired,
    accessLevel:PropTypes.oneOf(['NONE', 'READ_SUMMARY', 'READ_DETAIL', 'WRITE', 'ADMIN']),
    onChangeLevel:PropTypes.func.isRequired,

  };

  handleClick=(e)=>{
    e.preventDefault();
    e.stopPropagation();

    const newAccessLevel=e.target.getAttribute('data-value');
    if(newAccessLevel){
      //change in access level requested
      this.props.onChangeLevel(this.props.path, newAccessLevel);
    }
  }

  render(){
    const {accessLevels, accessLevel, isDeclared}=this.props;

    const selectedIndex=accessLevels.findIndex(elem=>elem.key===accessLevel);

    const className=classnames('access-level-control','pop-up-data', {
      inherited:!isDeclared,
      declared:isDeclared
    });
    let accessLevelString='No value selected';

    if(selectedIndex>-1){
      const selectedAccessLevel=accessLevels[selectedIndex];
      accessLevelString=`${selectedAccessLevel.label} ${isDeclared?'set at this level':'inherited from parent'}`;
    }

    return (
      <div className={className} data-align="top" data-usemarkup={true}>
        <div className="content-markup"><span>{accessLevelString}</span></div>
        {
          accessLevels.map((info,i)=>{
            let selected=false;
            let style;
            if(i<=selectedIndex && (selectedIndex===0||i>0)){
              selected=true;
              style={
                backgroundColor:info.colour,
                color:info.fontColour
              };
            }
            const classNames=classnames('access-level-value',{
              'selected':selected
            });
            return (
              <span onClick={this.handleClick} key={info.value} style={style} className={classNames} data-value={info.key}>{info.value}</span>
          );
        })
      }
      </div>
    );
  }
}
