'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ContentsTree from './contents/tree';
import {Map,Seq} from 'immutable';
import DropZone from 'usp-dropzone';

const getItems=function(securityDomain){
  return Map(Seq(securityDomain.identifiers).reduce((prevItem, item)=>{
    const type=item.type;
    prevItem[type]=prevItem[type]||[];
    prevItem[type].push(item);
    return prevItem;
  },{}));
};

export default class SecurityDomainContentsView extends Component{
  state={
    leftItems:getItems(this.props.leftSecurityDomain),
    rightItems:getItems(this.props.rightSecurityDomain),
    source:null,
    draggingItem:null
  };

  static propTypes={
    leftSecurityDomain:PropTypes.shape({
      id:PropTypes.number.isRequired,
      code:PropTypes.string.isRequired,
      name:PropTypes.string.isRequired,
      identifiers:PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        identifier: PropTypes.string.isRequired
      })).isRequired,
      objectVersion:PropTypes.number.isRequired
    }).isRequired,
    rightSecurityDomain:PropTypes.shape({
      id:PropTypes.number.isRequired,
      code:PropTypes.string.isRequired,
      name:PropTypes.string.isRequired,
      identifiers:PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        identifier: PropTypes.string.isRequired
      })).isRequired,
      objectVersion:PropTypes.number.isRequired
    }).isRequired
  };

  componentDidUpdate(prevProps){
    const {leftSecurityDomain, rightSecurityDomain}=prevProps;
    if(this.props.leftSecurityDomain!==leftSecurityDomain || this.props.rightSecurityDomain!==rightSecurityDomain){
      this.setState({ //eslint-disable-line react/no-did-update-set-state
        leftItems:this.getItems(this.props.leftSecurityDomain),
        rightItems:this.getItems(this.props.rightSecurityDomain)
      });  
    }
  }
  onDragStart=(item, source)=>{
    this.setState({
      source:source,
      draggingItem:item
    });
  };

  onSelect=(item, source)=>{
    this.setState({
      source:source,
      draggingItem:item
    });
  };

  handleKeyDown=(e)=>{
    if(this.state.draggingItem){
      if(e.keyCode===39){
        this.onDrop('right');
      }else if(e.keyCode===37){
        this.onDrop('left');
      }
    }
  }

  canHandleLeftDrop=()=>{
    return this.canHandleDrop('left');
  };
  canHandleRightDrop=()=>{
    return this.canHandleDrop('right');
  };

  onRightDrop=()=>{
    this.onDrop('right');
  };
  onLeftDrop=()=>{
    this.onDrop('left');
  };

  updateItems(items, destination, side){
    const {source, draggingItem}=this.state;
    return items.withMutations((items)=>{
      if(source===side){
        //remove item
        items.updateIn([draggingItem.type], items=>items.filter(i=>i.identifier!==draggingItem.identifier));
      }
      if(destination===side){
        //add item
        items.updateIn([draggingItem.type], items=>items?items.concat(draggingItem).sort((a,b)=>a.name.localeCompare(b.name)):[draggingItem]);
      }
    });
  }

  onDrop=(destination)=>{
    const {source, draggingItem, leftItems, rightItems}=this.state;
    if(draggingItem && source!==destination){
      this.setState({
        leftItems:this.updateItems(leftItems, destination, 'left'),
        rightItems:this.updateItems(rightItems, destination, 'right'),
        source:null,
        draggingItem:null
      });
    }
  };

  canHandleDrop(side){
    const {source, draggingItem}=this.state;

    if(draggingItem && source!==side){
      return true;
    }

    return false;
  }


  getUpdateSecurityDomainIdentifiers(){
    const {rightItems}=this.state;

    return rightItems.valueSeq().reduce((prevItem, items)=>prevItem.concat(items), []);
  }

  render(){
    const {leftSecurityDomain, rightSecurityDomain}=this.props;
    const {leftItems, rightItems}=this.state;

    return (
      <div className="domains-view" tabIndex={1} onKeyDown={this.handleKeyDown}>
        <div className="domain-tree left-side">
          <DropZone dropPos={0} enabled={true} canHandleEvent={this.canHandleLeftDrop} onDrop={this.onLeftDrop}>
            <ContentsTree
              key="left"
              name={leftSecurityDomain.name}
              items={leftItems}
              onDragStart={this.onDragStart}
              onSelect={this.onSelect}
              side="left"/>
          </DropZone>
        </div>

        <div className="domain-tree right-side">
          <DropZone dropPos={0} enabled={true} canHandleEvent={this.canHandleRightDrop} onDrop={this.onRightDrop}>
            <ContentsTree
              key="right"
              name={rightSecurityDomain.name}
              items={rightItems}
              onDragStart={this.onDragStart}
              onSelect={this.onSelect}
              side="right"/>
          </DropZone>
        </div>
      </div>
    );
  }
}
