'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import TreeView from 'usp-simple-tree';
import DraggableItem from './draggableItem';
import {Map, Seq} from 'immutable';

export default class ContentsTree extends PureComponent {
    static propTypes={
      name:PropTypes.string.isRequired,
      items:PropTypes.instanceOf(Map).isRequired,
      side:PropTypes.oneOf(['left', 'right']).isRequired,
      onDragStart:PropTypes.func.isRequired,
      onSelect:PropTypes.func.isRequired
    };

    handleDragStart=(item)=>{
      const {onDragStart, side}=this.props;

      onDragStart(item, side);
    };

    handleSelect=(item)=>{
      const {onSelect, side}=this.props;

      onSelect(item, side);
    };

    getDraggableItem(item){
      return(
        <DraggableItem
          onDrag={this.handleDragStart}
          onSelect={this.handleSelect}
          item={item}>
          <span>{item.name}</span>
        </DraggableItem>
      );
    }

    getItems(itemsSeq){
      return itemsSeq.map(item=>(
        <TreeView
            key={item.identifier}
            label={this.getDraggableItem(item)}
            type={item.type}
            />
        ));
    }


    render() {
      const {name, items}=this.props;

      return (
        <div className="domain-contents-tree">
          <TreeView
              key="root"
              open={true}
              label={name}>
              {items.map((contents, category)=>(
                <TreeView key={category} label={category} open={true}>
                  {this.getItems(Seq(contents))}
                </TreeView>
              )).valueSeq()}
          </TreeView>
        </div>
      );
    }
}
