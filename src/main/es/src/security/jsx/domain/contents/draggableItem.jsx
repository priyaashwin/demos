'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class DraggableItem extends PureComponent {
  static propTypes={
    children: PropTypes.any,
    className:PropTypes.string,
    onDrag: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    item:PropTypes.object.isRequired
  };

  handleDragStart=(e)=>{
    const {onDrag, item}=this.props;

    const dataTransfer = e.nativeEvent.dataTransfer;

    //don't let anything else pick up this event
    e.stopPropagation();

    dataTransfer.effectAllowed = 'move';

    //clear any existing data
    dataTransfer.clearData();

    //firefox requires the setData call be made otherwise it will not enable the drag
    dataTransfer.setData('text/plain', item.identifier);

    //call into the parent component
    onDrag.call(null, item);
  };

  handleFocus=()=>{
    const {onSelect, item}=this.props;

    onSelect(item);
  };

  handleBlur=()=>{
    const {onSelect}=this.props;

    onSelect(null);
  };

  render(){
    const {className, children}=this.props;

    return (
      <div
        className={classnames(className, {'draggable': true})}
        draggable={true}
        onDragStart={this.handleDragStart}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        tabIndex={0}>
        {children}
      </div>
    );
  }
}
