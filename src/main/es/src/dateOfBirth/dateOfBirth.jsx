'use strict';
import React from 'react';
import PropTypes from 'prop-types';

import { formatFuzzyDate } from '../date/fuzzyDate';
import Estimated from  '../indicator/estimated';
import Deceased from '../indicator/deceased';
import Unborn from '../indicator/unborn';

const DateOfBirth = ({ person }) => {
    const dateOfBirth = person.dateOfBirth;
    const estimated = person.dateOfBirthEstimated;
    const age = person.age;

    let estimatedContent;
    let ageContent;
    let lifeStateContent;
    let dobContent;

    switch(person.lifeState){
        case 'DECEASED' :
            lifeStateContent = (
                <span>
                    <span>{' '}</span>
                    <Deceased />
                </span>
            );
        break;
        case 'UNBORN':
            lifeStateContent = (
                <span>
                    <span>{' '}</span>
                    <Unborn />
                </span>
            );             
    }
    if (dateOfBirth) {
        dobContent = formatFuzzyDate({
            calculatedDate: dateOfBirth.calculatedDate,
            fuzzyDateMask: dateOfBirth.fuzzyDateMask
        }, true).date;
    }   
    if (estimated) {
        estimatedContent = (
            <span>
                <span>{' '}</span>
                <Estimated />
            </span>
        );
    } 
    if (age >= 0) {
        ageContent = (
            <span>
                <span>{' '}</span>
                {` (${age} year${age === 1 ? '' : 's'})`}
            </span>
        );
    }
    return (
        <span className="data-value">
            {dobContent}
            {estimatedContent}
            {lifeStateContent}
            {ageContent}
        </span>
    );
};

DateOfBirth.propTypes = {
    person: PropTypes.object,
};

export default DateOfBirth;