'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardList, CardHeader, CardContent } from 'usp-card';
import ErrorMessage from '../error/error';
import PaginatedResultsList from '../results/paginatedResultsList';
import subs from 'subs';
import Loading from '../loading/loading';
import ListContainer from './container';
import fastEqual from 'fast-deep-equal';
import { getCancelToken } from 'usp-xhr';

/* @extends React.Component */
export default class RemotePaginatedCardList extends Component {
  static propTypes = {
    noResultsMessage: PropTypes.string,
    /**
     * Endpoint data compatible with the xhr module.
     * PageSize and Page will be mixed into the url property as necessary
     */
    restEndpoint: PropTypes.any,
    //return card title
    getCardTitle: PropTypes.func.isRequired,
    //return card content
    getCardContent: PropTypes.func.isRequired,
    //return the header/title for the list
    getListHeader: PropTypes.func,
    //returns the CSS class to apply to the card - return null if none
    getCardClassName: PropTypes.func.isRequired,
    //returns any menu required for the card - this is located in the header
    getCardMenu: PropTypes.func,
    //properties to control default style of card
    enableHover: PropTypes.bool,
    enableShadow: PropTypes.bool,
    //optional callback after component updated
    afterUpdate: PropTypes.func,
    //optional classname to apply to the container
    className: PropTypes.string,
    pageSize: PropTypes.number,
    page: PropTypes.number.isRequired,
    showMoreMessage: PropTypes.bool
  };

  static defaultProps = {
    enableHover: true,
    enableShadow: true,
    pageSize: 25,
    showMoreMessage: true
  };

  state = {
    //records per card list
    pageSize: this.props.pageSize,
    //current page
    page: 1,
    //number of records in total
    totalSize: 0,
    //the current list of results
    data: [],
    //error response
    errors: null,
    loaded: false
  };

  requestData(endpoint, _pageSize, _page, retainExistingRecords) {
    const resultsList = new PaginatedResultsList();
    const pageSize = _pageSize != undefined ? _pageSize : this.state.pageSize;
    const page = _page != undefined ? _page : this.state.page;

    //if there are any outstanding requests cancel them
    if (this.cancelToken) {
      //cancel any existing requests
      this.cancelToken.cancel();
    }

    let existingRecords = [];

    if(retainExistingRecords){
      existingRecords = this.state.data;
    }

    //setup a new cancel token
    this.cancelToken = getCancelToken();

    resultsList.load(endpoint, pageSize, page, this.cancelToken).then(result => this.setState({
      loaded: true,
      data: [...existingRecords, ...result.results],
      pageSize: result.pageSize,
      page: result.page,
      totalSize: result.totalSize,
      errors: null
    })).catch(reject => this.setState({
      errors: reject,
      data: []
    }));
}
  clearErrors = (e) => {
    e.preventDefault();
    this.setState({ errors: null });
  }

  componentDidMount() {
    //fire an initial AJAX request for the List data - expects a paginated lists
    this.requestData(this.props.restEndpoint);
  }

  componentDidUpdate(prevProps) {
    const { afterUpdate } = this.props;

    if (!fastEqual(prevProps.restEndpoint, this.props.restEndpoint)) {
      //refresh data
      this.requestData(this.props.restEndpoint, this.props.pageSize, this.props.page);
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        page: this.props.page,
        pageSize: this.props.pageSize
      });
    }

    if (afterUpdate) {
      afterUpdate.call(null);
    }
  }
  componentWillUnmount() {
    if (this.cancelToken) {
      //cancel any outstanding xhr requests
      this.cancelToken.cancel();

      //remove property
      delete this.cancelToken;
    }
  }

  //load more Cards when load More button is clicked clicked;
  onLoadMoreDataButtonClicked = () => {
    this.requestData(this.props.restEndpoint, this.state.pageSize, this.state.page + 1, true);
  }

  //return a message with the number of cards left
  getCardsLeftMessage = (cardsToLoad) => {
    let displayMessage = '';
    if (cardsToLoad > this.state.pageSize) {
      displayMessage = `Click the button to load the next set of ${this.state.pageSize} cards.`;
    } else {
      displayMessage = `Click the button to load the remaining ${cardsToLoad} cards.`;
    }
    return displayMessage;
  }
  /**
   * Allows the caller to clear down the results set
   */
  clearData(callback) {
    this.setState({
      data: [],
      errors: null,
      pageSize: this.props.pageSize,
      page: 1
    }, callback);
  }
  getResultsMessage() {
    let resultsMessage = false;
    const { showMoreMessage } = this.props;

    if (showMoreMessage && this.state.pageSize > 0 && this.state.totalSize > this.state.pageSize) {
      const cardsLeft = this.state.totalSize - this.state.data.length;
      const getCardsLeftMessage = this.getCardsLeftMessage(cardsLeft);

      if (cardsLeft) {
        //need to include a message that more results are present
        resultsMessage = (<div className="more">
          <span>{subs(' ... {more} More cards left. ', {
            more: cardsLeft
          })} {getCardsLeftMessage}</span>
          <div className="load-more-btn">
            <span onClick={this.onLoadMoreDataButtonClicked}><i className="fa fa-repeat" aria-hidden="true"></i>Load more</span>
          </div>
        </div>);
      } else {
        return null;
      }
    } else if (this.state.loaded && this.state.totalSize === 0) {
      const noResultsMessage = this.props.noResultsMessage || 'No items found.';

      resultsMessage = (<div className="empty-list">
        <div className="empty more">
          <span>{noResultsMessage}</span>
        </div>
      </div>);
    } else if (!this.state.loaded) {
      resultsMessage = (<Loading />);
    }
    return resultsMessage;
  }
  getListHeader = () => {
    const { getListHeader } = this.props;
    if (getListHeader) {
      return getListHeader.call(null, this.state.totalSize);
    }
    return false;
  }
  renderCardList() {
    const {
      getCardTitle,
      getCardContent,
      getCardClassName,
      getCardMenu,
      enableHover,
      enableShadow,
      className
    } = this.props;

    const resultsMessage = this.getResultsMessage();

    return (
      <CardList className={className}>
        {
          (this.state.data || []).map((result, i) => (
            <Card key={result.id || i} className={getCardClassName ? getCardClassName.call(null, result) : undefined} enableHover={enableHover} enableShadow={enableShadow}>
              <CardHeader title={getCardTitle ? getCardTitle.call(null, result) : undefined} menu={getCardMenu ? getCardMenu.call(null, result) : undefined} />
              <CardContent content={getCardContent ? getCardContent.call(null, result) : undefined} />
            </Card>
          ))
        }
        {resultsMessage}
      </CardList>
    );
  }
  render() {
    let listContent;

    if (this.state.errors) {
      listContent = (<div><ErrorMessage errors={this.state.errors} onClose={this.clearErrors} /></div>);
    } else {
      listContent = this.renderCardList();
    }
    return (
      <ListContainer key="listContainer" header={this.getListHeader.call(null, this.state.totalSize)} content={listContent} />
    );
  }
}
