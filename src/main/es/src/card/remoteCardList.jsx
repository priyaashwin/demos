'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Card, CardList, CardHeader, CardContent} from 'usp-card';
import ErrorMessage from '../error/error';
import Loading from '../loading/loading';
import ResultsList from '../results/resultsList';
import ListContainer from './container';
import fastEqual from 'fast-deep-equal';

export default class RemoteCardList extends Component {
    static propTypes = {
      noResultsMessage: PropTypes.string,
      /**
       * Endpoint data compatible with the xhr module.
       * PageSize and Page will be mixed into the url property as necessary
       */
        restEndpoint: PropTypes.any,
        //return card title
        getCardTitle: PropTypes.func.isRequired,
        //return card content
        getCardContent: PropTypes.func.isRequired,
        //return the header/title for the list
        getListHeader: PropTypes.func,
        //returns the CSS class to apply to the card - return null if none
        getCardClassName: PropTypes.func.isRequired,
        //returns any menu required for the card - this is located in the header
        getCardMenu: PropTypes.func,
        //properties to control default style of card
        enableHover: PropTypes.bool,
        enableShadow: PropTypes.bool,
        //optional callback after component updated
        afterUpdate:PropTypes.func,
        //optional classname to apply to the container
        className:PropTypes.string
    };

    static defaultProps={
      enableHover: true,
      enableShadow: true
    };

    state = {
        //the current list of results
        data: [],
        //error response
        errors:null,
        loaded:false
    };

    clearErrors=(e)=>{
      e.preventDefault();
      this.setState({errors:null});
    };
    componentDidMount() {
        //fire an initial AJAX request for the List data - expects a paginated lists
        this.requestData(this.props.restEndpoint);
    }
    componentDidUpdate(prevProps){
      const {afterUpdate}=this.props;

      if(!fastEqual(prevProps.restEndpoint,this.props.restEndpoint)){
        //refresh data
        this.requestData(this.props.restEndpoint);
      }

      if(afterUpdate){
        afterUpdate.call(null);
      }
    }
    /**
     * Allows the caller to clear down the results set
     */
    clearData(callback){
      this.setState({
        data:[],
        errors:null
      },callback);
    }
    requestData(endpoint){
      const resultsList=new ResultsList();
      this.setState({
        loaded:false,
        data:[]
      },()=>{
        resultsList.load(endpoint).then(results=>this.setState({
          loaded:true,
          data:results,
          errors:null
        })).catch(reject => this.setState({
          errors:reject,
          data:[]
        }));
      });
    }
    getResultsMessage(){
      const {noResultsMessage='No items found.'}=this.props;
      const {data=[], loaded}=this.state;
      let resultsMessage=false;

      if(loaded && data.length===0){
        resultsMessage=(
          <div className="empty-list">
            <div className="more">
              <span>{noResultsMessage}</span>
            </div>
          </div>
        );
      }else if(!loaded){
        resultsMessage=(<Loading />);
      }
      return resultsMessage;
    }

    renderCardList(){
      const {
          getCardTitle,
          getCardContent,
          getCardClassName,
          getCardMenu,
          enableHover,
          enableShadow,
          className
      } = this.props;

      const {data=[]}=this.state;

      const resultsMessage=this.getResultsMessage();

      return (
          <CardList className={className}>
            {
              (data||[]).map((result,i)=>(
                <Card key={result.id||i} className={getCardClassName?getCardClassName.call(null,result):undefined} enableHover={enableHover} enableShadow={enableShadow}>
                  <CardHeader title={getCardTitle?getCardTitle.call(null,result):undefined} menu={getCardMenu?getCardMenu.call(null, result):undefined}/>
                  <CardContent content={getCardContent?getCardContent.call(null,result):undefined}/>
                </Card>
              ))
            }
            {resultsMessage}
          </CardList>
      );
    }
    getListHeader=()=>{
      const {getListHeader}=this.props;
      const {data=[]}=this.state;

      return getListHeader?(<div>{getListHeader.call(null, data.length)}</div>):false;
    };
    render() {
        const {data=[], errors}=this.state;

        let listContent;

        if(errors){
          listContent=(<div><ErrorMessage errors={errors} onClose={this.clearErrors}/></div>);
        }else{
          listContent=this.renderCardList();
        }
        return (
          <ListContainer key="listContainer" header={this.getListHeader.call(null, data.length)} content={listContent}/>
        );
    }
}
