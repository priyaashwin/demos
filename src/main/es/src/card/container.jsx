'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const ListContainer=({header, content})=>{
  return (
    <div className="card-list-container">
      {header}
      {content}
    </div>
  );
};

ListContainer.propTypes={
  header: PropTypes.node.isRequired,
  content: PropTypes.node.isRequired
};

export default ListContainer;
