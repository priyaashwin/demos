'use strict';
const formatCodedEntry=function(category, code){
  const codedEntries=(category||{}).codedEntries||{};

  const entry = codedEntries[code];
  return (entry) ? entry.name : '';
};

export {formatCodedEntry};
