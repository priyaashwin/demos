'use strict';

import React from 'react';
import Loading from '../../loading/loading';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Mask = ({ show, contained }) => {
    if (show) {
        return (
            <div
                id="mask"
                className={classnames('usp-mask', {
                    'usp-mask-contained': contained
                })}>
                <Loading />
            </div>
        );
    } else {
        return <div />;
    }
};

Mask.propTypes = {
    show: PropTypes.bool,
    contained: PropTypes.bool
};

Mask.defaultProps = {
    contained: false
};

export default Mask;
