'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../app.less';

const LEFT_PAGE_ELLIPSES = 'LEFT';
const RIGHT_PAGE_ELLIPSES = 'RIGHT';

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
    let i = from;
    const range = [];
    while (i <= to) {
        range.push(i);
        i += step;
    }
    return range;
};

class Paginator extends Component {
    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(...) and right(...) controls
     */
    fetchPageNumbers = totalPages => {
        const { currentPage, currentPageSiblings } = this.props;
        const totalNumbers = (currentPageSiblings * 2) + 3;
        const totalBlocks = totalNumbers + 2; 

        if (totalPages > totalBlocks) {
            let pages = [];

            const leftBound = currentPage - currentPageSiblings;
            const rightBound = currentPage + currentPageSiblings;
            const startPage = leftBound > 1 ? leftBound : 1;
            const endPage = rightBound < totalPages ? rightBound : totalPages;

            pages = range(startPage, endPage);

            const pagesCount = pages.length;
            const spillOffset = totalNumbers - (pagesCount - 1);
            const hasLeftSpill = startPage > 2;
            const hasRightSpill = endPage < totalPages;

            switch (true) {
                case hasLeftSpill && !hasRightSpill: {
                    const extraPages = range(startPage - spillOffset, startPage - 1);
                    pages = [LEFT_PAGE_ELLIPSES, ...extraPages, ...pages];
                    break;
                }
                case !hasLeftSpill && hasRightSpill: {
                    const extraPages = range(endPage + 1, endPage + spillOffset);
                    pages = [...pages, ...extraPages, RIGHT_PAGE_ELLIPSES];
                    break;
                }
                case hasLeftSpill && hasRightSpill:
                default: {
                    pages = [LEFT_PAGE_ELLIPSES, ...pages, RIGHT_PAGE_ELLIPSES];
                    break;
                }
            }
            return [...pages];
        }
        return range(1, totalPages);
    };
    loadPage = currentPage => {
        this.props.onChangePage(currentPage);
    };
    nextPage = () => {
        const { currentPage, rowsPerPage, totalResultsCount } = this.props;
        const totalPages = Math.ceil(totalResultsCount / rowsPerPage);
        currentPage < totalPages ? this.loadPage(currentPage + 1) : null;
    };
    previousPage = () => {
        const { currentPage } = this.props;
        currentPage !== 1 ? this.loadPage(currentPage - 1) : null;
    };
    createPaginatorIndicators = () => {
        const { currentPage, totalResultsCount, rowsPerPage } = this.props;
        const totalPages = Math.ceil(totalResultsCount / rowsPerPage);
        const pages = this.fetchPageNumbers(totalPages);
        return (
            <li>
                <ul className="pure-paginator">
                    <li className={currentPage === 1 ? 'pure-button-disabled' : ''}>
                        <a
                            className={`pure-button ${currentPage === 1 ? 'pure-button-disabled' : ''}`}
                            data-pglink="first"
                            href="#"
                            title="first page"
                            onClick={() => this.loadPage(1)}>
                            <i className="fa fa-fast-backward icon-fast-backward"></i>
                        </a>
                    </li>
                    <li className={currentPage === 1 ? 'pure-button-disabled' : ''}>
                        <a
                            className={`pure-button ${currentPage === 1 ? 'pure-button-disabled' : ''}`}
                            data-pglink="prev"
                            href="#"
                            title="Previous page"
                            onClick={() => this.previousPage()}>
                            <i className="fa fa-step-backward icon-step-backward"></i>
                        </a>
                    </li>
                    {pages.map((page, index) => {
                        if (page === LEFT_PAGE_ELLIPSES || page === RIGHT_PAGE_ELLIPSES)
                            return (
                                <li key={index}>
                                    <span className="yui3-paginator-more">{'...'}</span>
                                </li>
                            );
                        return (
                            <li key={index}>
                                <a
                                    className={`pure-button ${currentPage === page ? 'pure-button-active' : ''}`}
                                    data-pglink={page}
                                    title={`Page ${page}`}
                                    href="#"
                                    onClick={() => this.loadPage(page)}>
                                    {page}
                                </a>
                            </li>
                        );
                    })}
                    <li className={currentPage === totalPages ? 'pure-button-disabled' : ''}>
                        <a
                            className={`pure-button ${currentPage === totalPages ? 'pure-button-disabled' : ''}`}
                            data-pglink="next"
                            href="#"
                            title="Next page"
                            onClick={() => this.nextPage()}>
                            <i className="fa fa-step-forward icon-step-forward"></i>
                        </a>
                    </li>
                    <li className={currentPage === totalPages ? 'pure-button-disabled' : ''}>
                        <a
                            className={`pure-button ${currentPage === totalPages ? 'pure-button-disabled' : ''}`}
                            data-pglink="last"
                            href="#"
                            title="Last page"
                            onClick={() => this.loadPage(totalPages)}>
                            <i className="fa fa-fast-forward icon-fast-forward"></i>
                        </a>
                    </li>
                </ul>
            </li>
        );
    };
    createRowsPerPage = () => {
        const rowsPerPageOptions = this.props.rowOptions;
        const { totalResultsCount, rowsPerPage, onChangeRowsPerPage } = this.props;
        return (
            <li>
                <ul className="pure-perpage">
                    <li>
                        <span>
                            {'Rows per page:'}
                            <select
                                className="paginator-select-rowsperpage"
                                value={rowsPerPage}
                                onChange={event => onChangeRowsPerPage(Number(event.target.value), 1)}>
                                {rowsPerPageOptions.map(rowsPerPageOption => (
                                    <option key={rowsPerPageOption} value={rowsPerPageOption}>
                                        {rowsPerPageOption}
                                    </option>
                                ))}
                            </select>
                        </span>
                        <span className="totalResultsCount">Total Records: {totalResultsCount}</span>
                    </li>
                </ul>
            </li>
        );
    };
    createPaginator = () => {
        const { totalResultsCount, rowsPerPage } = this.props;
        return (
            <div className="yui3-paginator-container">
                <ul className="pure-pagebar">
                    {totalResultsCount > rowsPerPage && this.createPaginatorIndicators()}
                    {this.createRowsPerPage()}
                </ul>
            </div>
        );
    };

    render() {
        const paginatorContent = this.createPaginator();
        return <>{paginatorContent}</>;
    }
}

Paginator.propTypes = {
    totalResultsCount: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    onChangeRowsPerPage: PropTypes.func.isRequired,
    currentPage: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    rowOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
    currentPageSiblings: PropTypes.number,
};

Paginator.defaultProps = {
    rowOptions: [5, 10, 25, 50, 100],
    currentPageSiblings: 1,
};

export default Paginator;
