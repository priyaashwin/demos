'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../../../infopop/infopop';

const DuplicateStatus = function({labels, subject}) {
  let content;

  if (subject.duplicated) {
    const duplicateStatus = subject.duplicated;
    const title = (duplicateStatus === 'MASTER')? (labels.masterDuplicateStatus || 'This is the primary record.') : (labels.duplicateDuplicateStatus || 'This is a duplicate record.');

    content = (
      <InfoPop
        className="duplicateStatus"
        title={title} useMarkup={true}
        align="bottom"
      >
         <span>{getDuplicateStatusIconValue(duplicateStatus)}</span>
      </InfoPop>
    );
  }

  return (<div className="duplicate-status">{content}</div>);
};

const getDuplicateStatusIconValue = function(duplicateStatus) {
  switch(duplicateStatus) {
    case 'MASTER': 
      return 'P';
    case 'DUPLICATE': 
      return 'D';
  }
};

DuplicateStatus.propTypes = {
  labels: PropTypes.object.isRequired,
  subject: PropTypes.object.isRequired
};

export default DuplicateStatus;
