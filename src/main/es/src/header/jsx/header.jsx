'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from './icon/icon';
import Subject from '../../subject/subject';
import Properties from './properties/properties';
import Flags from './flags/flags';
import endpoint from '../js/cfg';
import subs from 'subs';
import Loading from '../../loading/loading';
import ExpandableDetails from './expander/expander';
import Model from '../../model/model';
import classnames from 'classnames';

export default class ContextHeader extends Component {
  static propTypes = {
    subjectDetailsEndpoint: PropTypes.object.isRequired,
    subjectPictureEndpoint: PropTypes.object.isRequired,
    subjectLockInfoEndpoint: PropTypes.object.isRequired,
    subjectId: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    primaryReferenceNumberContext: PropTypes.string,
    isNamedPersonRelationshipAvailable: PropTypes.bool,
    labels: PropTypes.object.isRequired,
    icon: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    identifier: PropTypes.string,
    permissions: PropTypes.object.isRequired,
    codedEntries: PropTypes.object.isRequired,
    type: PropTypes.oneOf(['person', 'organisation', 'group']).isRequired,
    mapLinkUrl: PropTypes.string.isRequired,
    isTitleClickable: PropTypes.bool
  };

  state = {
    lastLoaded: null,
    subject: {},
    loading: true,
    errors: null,
    //a rather crude way of forcing the picture to reload
    //we keep the key and update it if the pictureId is updated
    //in the subject
    pictureId: 0
  };

  clearErrors = (e) => {
    e.preventDefault();
    this.setState({ errors: null });
  };

  componentDidMount() {
    this.requestData(this.props.subjectId);
    this.fireUpdateEvent();
  }
  requestData(subjectId) {
    const { subjectDetailsEndpoint } = this.props;

    new Model(subjectDetailsEndpoint.url).load({
      ...endpoint.subjectDetailsEndpoint,
      ...subjectDetailsEndpoint
    }, { id: subjectId }).then(success => {
      //parse the response object into a subject
      const subject = this.parseSubjectDetailsResponse(success);

      let pictureId = this.state.pictureId;

      //this is the previous subject data
      if (this.state.subject.pictureId !== undefined) {
        //and the picture has changes
        if (subject.pictureId !== this.state.subject.pictureId) {
          //update
          pictureId = subject.pictureId;
        }
      }
      this.setState({
        lastLoaded: Date.now(),
        subject: subject,
        errors: null,
        pictureId: pictureId,
        loading: false
      });
    }).catch(reject => {
      //examine the errors and take appropriate action
      this.setState({
        errors: reject,
        loading: false
      });
    });
  }
  componentDidUpdate() {
    this.fireUpdateEvent();
  }
  fireUpdateEvent() {
    const eventType = 'headerUpdated';

    //createcustom event
    const event = new CustomEvent(eventType, {
      bubbles: true,
      cancelable: true
    });

    document.dispatchEvent(event);
  }
  parseSubjectDetailsResponse(response) {
    return response || {};
  }
  getSubjectPictureEndpoint(subjectId) {
    const subjectPictureEndpoint = this.props.subjectPictureEndpoint;
    const { type } = this.props;

    //merge our URL into the endpoint
    const updatedEndpoint = {
      ...endpoint.subjectPictureEndpoint,
      ...subjectPictureEndpoint
    };

    //substitute the subject parameters
    updatedEndpoint.url = subs(updatedEndpoint.url || '', {
      subjectId: subjectId,
      subjectType: type
    });

    return updatedEndpoint;
  }
  refresh() {
    this.requestData(this.props.subjectId);
  }
  handleSubjectClick = (e) => {
    e.preventDefault();

    const { subjectId } = this.props;

    const event = new CustomEvent('viewSummary', {
      bubbles: true,
      cancelable: true,
      detail: {
        subjectId: subjectId
      }
    });

    document.dispatchEvent(event);
  };
  handleClickByDoingNothing = () => { };
  render() {
    const { subjectId, type, icon, name, identifier, codedEntries, labels, permissions, mapLinkUrl, subjectLockInfoEndpoint, isTitleClickable = true, primaryReferenceNumberContext, isNamedPersonRelationshipAvailable } = this.props;

    const { subject = {}, lastLoaded, loading } = this.state;

    let content;
    if (loading) {
      content = (<Loading />);
    } else {
      if (!this.state.errors) {
        content = (<Properties labels={labels} subject={subject} codedEntries={codedEntries} type={type} primaryReferenceNumberContext={primaryReferenceNumberContext}/>);
      } else {
        content = (<div>Failed to load properties</div>);
      }
    }

    let titleIconClassName = 'title-icon';
    let handleSubjectClick = this.handleSubjectClick;

    if (!isTitleClickable) {
      titleIconClassName += ' not-clickable';
      handleSubjectClick = this.handleClickByDoingNothing;
    }

    return (
      <div className={classnames('header-bar', `header-${type}`, {
        'header-loading':loading        
      })}>
        <div className="subject-header">
          <div className="subject">
            <div className={titleIconClassName} onClick={handleSubjectClick}>
              <Icon key={this.state.pictureId} type={type} iconClass={icon} pictureClassName="header-picture" pictureEndpoint={this.getSubjectPictureEndpoint(subjectId)} />
              <h2>
                <Subject key="subject" name={name} identifier={identifier} subject={subject} subjectType={type} />
              </h2>
            </div>
            <Flags key="subjectFlags" subject={subject} type={type} labels={labels} permissions={permissions} codedEntries={codedEntries} lastLoaded={lastLoaded} subjectLockInfoEndpoint={subjectLockInfoEndpoint} />
          </div>
          <div className="subject-details">
            {content}
          </div>
        </div>
        <ExpandableDetails key="expander" subject={subject} labels={labels.infoBar || {}} type={type} contactCategory={codedEntries.contactType} codedEntries={codedEntries} mapLinkUrl={mapLinkUrl} permissions={permissions}  isNamedPersonRelationshipAvailable={isNamedPersonRelationshipAvailable} />
      </div>
    );
  }
}
