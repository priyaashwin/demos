'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import SubjectIcon from '../../../subject/icon';

export default class Icon extends PureComponent{
  render(){
    const {type, pictureEndpoint, ...other}=this.props;

    let content;

    switch(type){
      case 'person':
        content=(<SubjectIcon {...other} type={type} pictureEndpoint={pictureEndpoint}/>);
      break;
      case 'organisation':
      case 'group':
        //do not include a picture endpoint for both the group and organisation icons - this will prevent
        //the REST request, but will still display the icon image
        content=(<SubjectIcon {...other} type={type} />);
      break;
      default:
        content=(<span />);
    }

    return content;
  }
}

Icon.propTypes={
  type:PropTypes.oneOf(['person','organisation','group']).isRequired,
  pictureEndpoint:PropTypes.object.isRequired
};
