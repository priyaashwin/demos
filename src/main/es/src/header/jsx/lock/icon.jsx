'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const LockIcon=({children, labels, expanded, onClick})=>{
  let control;
  let title;
  if(expanded){
    control=(<span><i className="fa fa-caret-down expander" aria-hidden="true"/></span>);
    title=labels.collapse;
  }else{
    control=(<span><i className="fa fa-caret-up expander" aria-hidden="true"/></span>);
    title=labels.expand;
  }

  return (
    <div className="locked" tabIndex={0} onClick={onClick} title={title} aria-label={title}>
      <i className="fa fa-lock" aria-hidden="true"/>
      {children}
      {control}
    </div>
  );
};

LockIcon.propTypes={
  children: PropTypes.any,
  labels: PropTypes.object.isRequired,
  expanded: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};
export default LockIcon;
