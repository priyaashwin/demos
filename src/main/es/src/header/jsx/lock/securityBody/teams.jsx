'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Team from './team';

const Teams=({maxSize, teams=[], ...other})=>(
  <div>
    {teams.sort((a, b) => a.name.localeCompare(b.name, 'en', {'sensitivity': 'base'})).slice(0, maxSize).map((team, i)=>(<Team key={i} team={team} {...other}/>))}
    {teams.length>maxSize?(<div className="list-item">{`... ${teams.length-maxSize} more teams...`}</div>):''}
  </div>
);

Teams.propTypes={
  teams:PropTypes.array.isRequired,
  maxSize:PropTypes.number.isRequired
};
Teams.defaultProps={
  maxSize:10
};

export default Teams;
