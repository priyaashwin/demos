'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const User=({user, labels})=>{
  let tel, email;

  if(user.telephoneContactNumber){
    tel=(<div className="contact"><i className="fa fa-phone" aria-hidden="true"/> <a href={`tel:${user.telephoneContactNumber}`} title={`${labels.tel||'telephone'} ${user.telephoneContactNumber}`}>{user.telephoneContactNumber}</a></div>);
  }

  if(user.emailAddress){
    email=(<div className="contact"><i className="fa fa-envelope" aria-hidden="true"/> <a href={`mailto:${user.emailAddress}`}  title={`${labels.email||'email'} ${user.emailAddress}`}>{user.emailAddress}</a></div>);
  }

  return (
  <div key={user.id} className="list-item">
    <div><span><i className="fa fa-user"/>{' '}</span> <span>{`${user.name} (${user.identifier})`}</span></div>
    <div>
      {tel}
      {email}
    </div>
  </div>
  );
};

User.propTypes={
  user:PropTypes.object.isRequired,
  labels: PropTypes.object.isRequired
};
export default User;
