'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Users from './users';
import Teams from './teams';

export default class SecurityBodyList extends PureComponent {
  static propTypes = {
    teams: PropTypes.array.isRequired,
    users: PropTypes.array.isRequired,
    labels: PropTypes.object.isRequired,
    icon: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired
  }

  render(){
    const {users=[], teams=[], labels, className, icon}=this.props;
    if(!users.length && !teams.length){
      return false;
    }
    return (
      <div className="popup-info">
          <span className={className}><i className={icon}/> {labels.title}:</span>
          <Users key="users" users={users} labels={labels}/>
          <Teams key="teams" teams={teams}/>
      </div>
    );
  }
}
