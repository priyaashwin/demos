'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import User from './user';

const Users=({maxSize, users=[], ...other})=>(
  <div>
    {users.sort((a, b) => a.name.localeCompare(b.name, 'en', {'sensitivity': 'base'})).slice(0, maxSize).map((user, i)=>(<User key={i} user={user} {...other}/>))}
    {users.length>maxSize?(<div className="list-item">{`... ${users.length-maxSize} more users ...`}</div>):''}
  </div>
);

Users.propTypes={
  users:PropTypes.array.isRequired,
  maxSize:PropTypes.number.isRequired
};
Users.defaultProps={
  maxSize:10
};

export default Users;
