'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Team=({team})=>(
  <div key={team.id} className="list-item"><span><i className="fa eclipse-team"/>{' '}</span> <span>{`${team.name} (${team.identifier})`}</span></div>
);

Team.propTypes={
  team:PropTypes.object.isRequired
};
export default Team;
