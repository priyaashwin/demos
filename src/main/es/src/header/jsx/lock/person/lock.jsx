'use strict';
import React, {PureComponent} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import LockIcon from '../icon';
import LockInfo from './lockInfo';

export default class Lock extends PureComponent {
  state = {
    expanded: false
  };
  static propTypes = {
    subjectLockInfoEndpoint: PropTypes.object.isRequired,
    labels: PropTypes.object.isRequired,
    subject: PropTypes.object.isRequired,
    lastLoaded: PropTypes.number
  }
  handleLockClick = (e) => {
    e.preventDefault();
    this.setState({
      expanded: !this.state.expanded
    });
  };
  onHide = () => {
    this.setState({expanded: false});
  };
  refNode = (node) => {
    this.target = node;
  };
  targetNode = () => ReactDOM.findDOMNode(this.target);
  render() {
    const {subject, labels, lastLoaded, subjectLockInfoEndpoint} = this.props;

    if (!subject.hasAccessLocks) {
      //don't output anything if not locked
      return (<div/>);
    }
    return (
      <div>
        <LockIcon
          key="icon"
          expanded={this.state.expanded}
          labels={labels}
          onClick={this.handleLockClick}
          ref={this.refNode}>
          <span>{labels.locked}</span>
        </LockIcon>
        <LockInfo
          key="info"
          expanded={this.state.expanded}
          subject={subject}
          labels={labels}
          subjectLockInfoEndpoint={subjectLockInfoEndpoint}
          lastLoaded={lastLoaded}
          onHide={this.onHide}
          target={this.targetNode}/>
      </div>
    );
  }
}
