'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Loading from '../../../../loading/loading';
import InfoOverlay from '../../../../infopop/overlay';
import endpoint from '../../../js/cfg';
import Model from '../../../../model/model';
import SecurityBodyList from '../securityBody/list';

export default class LockInfo extends PureComponent{
  state={
    loading:false,
    errors:null
  };
  static propTypes={
      subjectLockInfoEndpoint:PropTypes.object.isRequired,
      labels: PropTypes.object.isRequired,
      expanded:PropTypes.bool.isRequired,
      subject: PropTypes.object.isRequired,
      target: PropTypes.func.isRequired,
      onHide: PropTypes.func.isRequired,
      lastLoaded: PropTypes.number
  }
  componentDidMount(){
    const {subject, expanded}=this.props.subject;
    if(expanded){
      this.requestData(subject.id);
    }
  }
  componentDidUpdate(prevProps){
    const {subject, expanded, lastLoaded}=this.props;
    const {loading, errors}=this.state;

    if(prevProps.lastLoaded!==lastLoaded){
      //subject data has been re-loaded since last load - clear down lock info
      this.lockInfo=null;
    }

    if(expanded && !loading && !this.lockInfo && errors===null){
      this.requestData(subject.id);
    }
  }
  requestData(subjectId){
    const {subjectLockInfoEndpoint}=this.props;

    this.setState({
      loading:true
    },()=>{
      new Model(subjectLockInfoEndpoint.url).load({
        ...endpoint.subjectLockInfoEndpoint,
        ...subjectLockInfoEndpoint
        }, {id:subjectId}).then(success => {
        this.lockInfo=success||{};

        this.setState({
          errors:null,
          loading:false
        });
      }).catch(reject => {
          this.lockInfo=null;

          //set error state
          this.setState({
            errors:reject,
            loading:false
          });
      });
    });
  }
  render(){
    const {expanded, target, labels, onHide}=this.props;
    const {loading, errors}=this.state;
    const lockInfo=this.lockInfo||{};
    let content=false;

    if(loading){
      content=(<Loading />);
    }else{
      if(!errors && lockInfo){
        content=(
          <div className="lock-details">
              <SecurityBodyList key="allowedList" users={lockInfo.allowedUsers} teams={lockInfo.allowedTeams} className="access-allowed" icon="fa fa-unlock" labels={labels.accessAllowed}/>
              <SecurityBodyList key="deniedList"  users={lockInfo.deniedUsers} teams={lockInfo.deniedTeams} className="access-denied" icon="fa fa-lock" labels={labels.accessDenied}/>
          </div>
        );
      }else{
        content=(<div>{labels.loadFailed||'Failed to load lock info.'}</div>);
      }
    }

    return (
      <InfoOverlay visible={expanded} target={target} className="lock-info" shouldUpdatePosition rootClose onHide={onHide}>{content}</InfoOverlay>
    );
  }
}
