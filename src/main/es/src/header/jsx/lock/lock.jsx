'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PersonLock from './person/lock';

const Lock=({type, ...other})=>{
  let content;
  switch(type){
    case 'person':
      content=(<PersonLock {...other}/>);
    break;
    default:
      content=(<div />);
  }

  return content;
};

Lock.propTypes={
  type:PropTypes.oneOf(['person']).isRequired
};

export default Lock;
