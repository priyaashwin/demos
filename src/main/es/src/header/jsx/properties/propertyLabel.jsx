'use strict';
import React  from 'react';
import PropTypes from 'prop-types';

const PropertyLabel=({label})=>(
  <div className="property-label">
    {label}{':'}
  </div>
);

PropertyLabel.propTypes={
  label: PropTypes.string.isRequired
};

export default PropertyLabel;
