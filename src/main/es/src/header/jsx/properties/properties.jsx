'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PersonProperties from './person/properties';
import GroupProperties from './group/properties';

const Properties = ({ type, ...other }) => {
  let content;
  switch (type) {
    case 'person':
      content = (<PersonProperties {...other} />);
      break;
    case 'group':
      content = (<GroupProperties {...other} />);
      break;
    default:
      content = (<div />);
  }

  return content;
};

Properties.propTypes = {
  type: PropTypes.oneOf(['person', 'organisation', 'group']).isRequired
};

export default Properties;
