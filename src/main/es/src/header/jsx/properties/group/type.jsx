'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../propertyLabel';
import PropertyValue from '../propertyValue';
import {formatCodedEntry} from '../../../../codedEntry/codedEntry';

const Type=({type, category, labels})=>(
  <div>
    <PropertyLabel label={labels.type||'Type'} />
    <PropertyValue value={formatCodedEntry(category, type)} />
  </div>
);

Type.propTypes={
  labels: PropTypes.object.isRequired,
  type:PropTypes.string,
  category:PropTypes.object.isRequired
};

export default Type;
