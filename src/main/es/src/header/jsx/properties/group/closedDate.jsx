'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../propertyLabel';
import DateValue from '../../date/dateValue';

const ClosedDate=({closedDate, labels})=> (
    <div>
      {closedDate &&
      <>
        <PropertyLabel label={labels.closedDate||'End date'}/>
        <DateValue key="closedDate" date={closedDate} />
      </>
      }
    </div>
);

ClosedDate.propTypes={
  labels: PropTypes.object.isRequired,
  closedDate:PropTypes.number
};

export default ClosedDate;
