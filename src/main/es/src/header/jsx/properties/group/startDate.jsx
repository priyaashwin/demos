'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../propertyLabel';
import DateValue from '../../date/dateValue';

const StartDate=({startDate, labels})=>{
  return (
    <div>
      <PropertyLabel label={labels.startDate||'Start date'} />
      <DateValue key="startDate" date={startDate} />
    </div>
  );
};

StartDate.propTypes={
  labels: PropTypes.object.isRequired,
  startDate:PropTypes.number
};

export default StartDate;
