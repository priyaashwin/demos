'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Type from './type';
import StartDate from './startDate';
import ClosedDate from './closedDate';

const Properties=({subject, codedEntries, ...other})=>{
  const typeProperties={
    type:subject.groupType,
    category: codedEntries.groupType
  };

  const dateProperties={
    startDate: subject.startDate,
    closedDate: subject.closeDate
  };

  return (
    <div className="subject-properties">
      <Type {...typeProperties} {...other}/>
      <StartDate {...dateProperties} {...other}/>
      <ClosedDate {...dateProperties} {...other}/> 
    </div>      
  );  
};

Properties.propTypes={
  labels: PropTypes.object.isRequired,
  subject: PropTypes.object.isRequired,
  codedEntries:PropTypes.shape({
    groupType:PropTypes.object.isRequired
  }).isRequired
};

export default Properties;
