'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../propertyLabel';
import PropertyValue from '../propertyValue';

const CHINumber=({chiNumber, labels})=>(
  <div>
    <PropertyLabel label={labels.chiNumber||'CHI Number'} />
    <PropertyValue value={chiNumber} />
  </div>
);

CHINumber.propTypes={
  labels: PropTypes.any,
  chiNumber: PropTypes.string
};

export default CHINumber;
