'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatNHSNumber} from '../../../../nhsNumber/nhsNumber';
import PropertyLabel from '../propertyLabel';
import PropertyValue from '../propertyValue';

const NHSNumber=({nhsNumber, labels})=>(
  <div>
    <PropertyLabel label={labels.nhsNumber||'NHS Number'} />
    <PropertyValue value={formatNHSNumber(nhsNumber)} />
  </div>
);

NHSNumber.propTypes={
  labels: PropTypes.any,
  nhsNumber: PropTypes.string
};

export default NHSNumber;
