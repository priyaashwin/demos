'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../propertyLabel';
import PropertyValue from '../propertyValue';
import FuzzyDateValue from '../../date/fuzzyDateValue';
import DateValue from '../../date/dateValue';

const BirthDate=({lifeState, dateOfBirth, dateOfBirthEstimated, diedDate, diedDateEstimated, dueDate, age, labels})=>{

  const content=[];

  let dateOfBirthContent;

  if(dateOfBirth){
    dateOfBirthContent=(
    <div key="dateOfBirth">
      <PropertyLabel label={labels.born||'born'} />
      <FuzzyDateValue fuzzyDate={dateOfBirth} isEstimate={!!dateOfBirthEstimated}/>
    </div>);
  }

  let ageContent;
  if(age>=0){
    ageContent=(<PropertyValue key="age" value={`(${age} year${age===1?'':'s'})`} />);
  }

  switch(lifeState){
    case 'UNBORN':{
      content.push(<PropertyLabel key="dueDateLabel" label={labels.dueDate||'due date'} />);
      content.push(<DateValue key="dueDate" date={dueDate} />);
      break;
    }
    case 'NOT_CARRIED_TO_TERM':{
      content.push(<PropertyValue key="notCarried" value={labels.notCarried||'not carried'} />);
      break;
    }
    case 'DECEASED':{
      if(dateOfBirth){
        content.push(dateOfBirthContent);
      }

      if(diedDate){
        content.push(
          <div key="dateOfDeath">
            <PropertyLabel label={labels.died||'died'} />
            <FuzzyDateValue fuzzyDate={diedDate} isEstimate={diedDateEstimated}/>
          </div>
        );
      }
      content.push(ageContent);
      break;
    }
    default:{
      content.push(dateOfBirthContent);
      content.push(ageContent);
    }
  }

  return (
    <div className="birth-date">
      {content}
    </div>
  );
};

BirthDate.propTypes={
  labels: PropTypes.object.isRequired,
  lifeState:PropTypes.string,
  dateOfBirth:PropTypes.object,
  dateOfBirthEstimated:PropTypes.bool,
  diedDate:PropTypes.object,
  diedDateEstimated:PropTypes.bool,
  dueDate:PropTypes.number,
  age:PropTypes.number
};

export default BirthDate;
