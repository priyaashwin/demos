'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import BirthDate from './birthDate';
import Gender from './gender';
import NHSNumber from './nhsNumber';
import CHINumber from './chiNumber';

const Properties=({subject, codedEntries, primaryReferenceNumberContext, ...other})=>{
  const birthProperties={
    lifeState:subject.lifeState,
    dateOfBirth: subject.dateOfBirth,
    dateOfBirthEstimated:subject.dateOfBirthEstimated,
    diedDate:subject.diedDate,
    diedDateEstimated:subject.diedDateEstimated,
    dueDate:subject.dueDate,
    age: subject.age
  };

  let gender;
  if(subject.gender){
    const genderProperties={
      gender:subject.gender,
      category: codedEntries.gender
    };
    gender=(<Gender key="gender" {...genderProperties} {...other}/>);
  }

  let primaryReferenceNumber;
  if (primaryReferenceNumberContext === 'CHI') {
     if(subject.chiNumber){
       primaryReferenceNumber=(<CHINumber key="chiNumber" chiNumber={subject.chiNumber} {...other}/>);
    }
  } else {
    if(subject.nhsNumber){
      primaryReferenceNumber=(<NHSNumber key="nhsNumber" nhsNumber ={subject.nhsNumber} {...other}/>);
    }
  }

  return (
    <div className="subject-properties">
      <BirthDate {...birthProperties} {...other}/>
      {gender}
      {primaryReferenceNumber}
    </div>
  );
};

Properties.propTypes={
  labels: PropTypes.object.isRequired,
  subject: PropTypes.object.isRequired,
  primaryReferenceNumberContext: PropTypes.string.isRequired,
  codedEntries:PropTypes.object
};

export default Properties;
