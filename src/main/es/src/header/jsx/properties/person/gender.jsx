'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatCodedEntry} from '../../../../codedEntry/codedEntry';
import PropertyLabel from '../propertyLabel';
import PropertyValue from '../propertyValue';

const Gender=({gender, category, labels})=>(
  <div>
    <PropertyLabel label={labels.gender||'gender'} />
    <PropertyValue value={formatCodedEntry(category, gender)} />
  </div>
);

Gender.propTypes={
  labels: PropTypes.any,
  gender: PropTypes.string,
  category: PropTypes.object.isRequired
};

export default Gender;
