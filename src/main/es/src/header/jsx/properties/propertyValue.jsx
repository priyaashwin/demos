'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const PropertyValue=({value})=>(
  <div className="property-value">
    {value}
  </div>
);

PropertyValue.propTypes={
  value: PropTypes.any
};

export default PropertyValue;
