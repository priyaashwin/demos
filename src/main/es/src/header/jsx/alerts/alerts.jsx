'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import PersonAlerts from './person/alerts';

const Alerts=({type, ...other})=>{
  let content;
  switch(type){
    case 'person':
      content=(<PersonAlerts {...other}/>);
    break;
    default:
      content=(<div />);
  }

  return content;
};

Alerts.propTypes={
  type:PropTypes.oneOf(['person']).isRequired
};

export default Alerts;
