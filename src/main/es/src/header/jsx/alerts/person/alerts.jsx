'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import AlertIcon from '../icon';
import {formatCodedEntry} from '../../../../codedEntry/codedEntry';

export default class PersonAlerts extends PureComponent{
  static propTypes={
    labels: PropTypes.object.isRequired,
    subject: PropTypes.object.isRequired,
    canManageAlerts: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    category: PropTypes.object.isRequired
  };
  handleClick=(e)=>{
    e.preventDefault();
    
    setTimeout(()=>{
        //close the popup so doesn't overlap on dialog shown by click handler
        if(this.container){
            const infoPop=this.container.querySelector('.pop-up-data');
            if(infoPop){
                //fire blur event
                const event = document.createEvent('FocusEvent');

                event.initEvent('blur', true, true);

                //fire from element
                infoPop.dispatchEvent(event);
            }
        }
    }, 250);
    
    this.props.onClick();
  };
  getPersonAlerts(){
    const {subject}=this.props;
    return subject.personAlerts||[];
  }
  getAlertContent(alert){
    const {category}=this.props;
    return (<div>
              <div>{formatCodedEntry(category, alert.alertCategory)}</div>
              <div>{alert.notes.map(note=>note.content).join(' ')}</div>
            </div>);
  }
  render(){
    const {canManageAlerts, labels}=this.props;

    const personAlerts=this.getPersonAlerts();

    let alertsContent;

    if(personAlerts.length>0){
      const clickTitle=`Click the icon to ${canManageAlerts?labels.manage||'manage alerts':labels.view||'view alerts'}`;
      const title=`This person has ${personAlerts.length} alert${personAlerts.length>1?'s':''}`;

      alertsContent=(
        <AlertIcon title={title} onClick={this.handleClick}>
          <span>{personAlerts.length}</span>
          <div className="content-markup">
            <div className="warnings-popup">
              <ul className="warnings-list">
              {personAlerts.map((alert,i )=>(
                <li key={i} className="alert-info">{this.getAlertContent(alert)}</li>
              ))}
              </ul>
              <div className="click-text">{clickTitle}</div>
            </div>
          </div>
        </AlertIcon>
      );
    }

    return(<div className="alerts" ref={(container) => { this.container = container; }}>{alertsContent}</div>);
  }
}
