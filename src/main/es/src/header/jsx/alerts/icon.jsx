'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../../../infopop/infopop';

const AlertIcon = ({ children, ...other }) => (
  <InfoPop className="alert"
    useMarkup={true}
    align="bottom"
    {...other}>
    <i className="fa fa-exclamation" />
    {children}
  </InfoPop>
);

AlertIcon.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
  onClick: PropTypes.func.isRequired
};
export default AlertIcon;
