'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../date/date';
import PropertyValue from '../properties/propertyValue';

const DateValue=({date})=>{
  let dateValue;

  if(date){
    dateValue=formatDate(date, true);
  }
  return (
    <PropertyValue value={dateValue} />
  );
};
DateValue.propTypes={
  date: PropTypes.number
};

export default DateValue;
