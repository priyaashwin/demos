'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import {formatFuzzyDate} from '../../../date/fuzzyDate';
import PropertyValue from '../properties/propertyValue';
import Estimated from '../../../indicator/estimated';

const FuzzyDateValue=({fuzzyDate, isEstimate})=>{
  let estimateContent;

  if(isEstimate){
    estimateContent=(<Estimated label="Estimated date"/>);
  }

  const date=formatFuzzyDate(fuzzyDate, true).date||'';
  return (
    <span>
      <PropertyValue value={date} />
      {estimateContent}
    </span>
  );
};
FuzzyDateValue.propTypes={
  fuzzyDate: PropTypes.object,
  isEstimate: PropTypes.bool
};

export default FuzzyDateValue;
