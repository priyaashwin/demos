'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Seq } from 'immutable';
import classnames from 'classnames';
import { formatDate } from '../../../date/date';
import InfoPop from '../../../infopop/infopop';

export default class Classifications extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const { classifications } = this.props;

    const isCurrent = (classificationAssignment) => classificationAssignment.assignmentDateStatus === 'CURRENT';
    const isHistoric = (classificationAssignment) => classificationAssignment.assignmentDateStatus === 'HISTORIC';

    const filterSignificant = (classificationAssignment) => Seq(classificationAssignment).filter(assignment => ((assignment.classificationVO.significantWhenCurrent && isCurrent(assignment)) || (assignment.classificationVO.significantWhenHistoric && isHistoric(assignment)))).toArray();

    //filter for significant classifications
    const significantClassifications = classifications.map(classificationAssignment => filterSignificant(classificationAssignment)).filter(classificationAssignment => classificationAssignment.length);

    return (
      <div className="classification-info-list">
        {
          significantClassifications.map(classificationAssignments => {
            const assignment = classificationAssignments.sort((a, b) => {
              const aEndDate = a.endDate || Number.MAX_SAFE_INTEGER;
              const bEndDate = b.endDate || Number.MAX_SAFE_INTEGER;

              let r = bEndDate - aEndDate;
              if (r === 0) {
                r = b.startDate - a.startDate;
              }

              return r;
            })[0];
            const classificationVO = assignment.classificationVO || {};
            return (
              <InfoPop
                key={assignment.id}
                className={classnames('classification-icon', assignment.assignmentDateStatus.toLowerCase())}
                title={classificationVO.classificationGroupName}
                align="bottom"
                useMarkup={true}>
                <span>{classificationVO.acronym || '?'}</span>
                <div className="content-markup">
                  {
                    classificationAssignments.map(assignment => {
                      const classification = assignment.classificationVO || {};
                      return (
                        <div className="classification-info" key={assignment.id}>
                          <div>{classification.name}</div>
                          <div>{`${formatDate(assignment.startDate, true)} ${assignment.endDate ? 'to ' : ''} ${assignment.endDate ? formatDate(assignment.endDate, true) : ''}`}</div>
                        </div>
                      );
                    })
                  }
                </div>
              </InfoPop>
            );
          }).valueSeq().toArray()
        }
      </div>
    );
  }
}

Classifications.propTypes = {
  classifications: PropTypes.object.isRequired,
  labels: PropTypes.object.isRequired
};
