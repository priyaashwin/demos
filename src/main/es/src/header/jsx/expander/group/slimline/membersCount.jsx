'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';

export default class MembersCount extends PureComponent {
    render(){
      const {labels, membersCount}=this.props;

      return (
        <div>
          <PropertyLabel label={labels.count||'Count'}/>
          <PropertyValue value={membersCount} />
        </div>
      );
    }
}

MembersCount.propTypes={
  labels:PropTypes.object.isRequired,
  membersCount: PropTypes.number.isRequired
};
