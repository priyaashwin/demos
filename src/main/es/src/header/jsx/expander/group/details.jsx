'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import DetailSlimline from './slimline/slimline';
import Collapse from 'react-bootstrap/lib/Collapse';
import DetailFull from './full/full';

export default class GroupDetails extends PureComponent {
  render(){
    const {expanded, ...other}=this.props;

    return (
      <div>
         <DetailSlimline key="slimline" {...other}/>
         <div style={{position:'absolute',width:'100%'}}>
           <Collapse in={expanded}>
             <div className="expander-scroller">
               <div className="info-bar-expander">
                 <DetailFull key="full" {...other}/>
               </div>
             </div>
           </Collapse>
         </div>
      </div>
    );
  }
}

GroupDetails.propTypes={
  expanded:PropTypes.bool.isRequired,
  subject:PropTypes.object.isRequired
};
