'use strict';
import React  from 'react';
import PropTypes from 'prop-types';
import Member from './member';

const DetailFull=({subject, labels})=>{
  const members=(subject.members||[]).sort((a, b)=>a.joinDate-b.joinDate);

  let content;
  if(subject.isGroupSecured && members.length===0){
    content=(<div className="full-col"><div className="property-value">{labels.secured||'Group members are secured'}</div></div>);
  }else{
    //array of items per col
    const cols=[];

    for(let i=0; i<members.length; i++){
      const colIdx=Math.floor(i%4);
      const items=cols[colIdx]||[];
      items.push(<Member key={i} member={members[i]} labels={labels}/>);
      cols[colIdx]=items;
    }

    const groups=[];
    for(let i=0; i<cols.length; i++){
      const grpIdx=Math.floor(i%2);
      const items=groups[grpIdx]||[];
      items.push(cols[i]);
      groups[grpIdx]=items;
    }

    content=groups.map((cols, i)=>(
      <div className="full-info-group" key={i}>
        {
          cols.map((col, i)=>(
            <div className="full-col" key={i}>
              <div className="members">
                {col}
              </div>
            </div>
          ))
        }
    </div>));
  }


  return (
    <div className="full-info">
      {content}
    </div>
  );
};

DetailFull.propTypes={
  subject:PropTypes.object.isRequired,
  labels:PropTypes.object.isRequired
};
export default DetailFull;
