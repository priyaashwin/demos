'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Subject from '../../../../../subject/subject';
import classnames from 'classnames';

export default class Member extends PureComponent{
  constructor(props){
    super(props);

    this.handleClick=this.handleClick.bind(this);
  }
  handleClick(e){
    e.preventDefault();

    const {member}=this.props;
    const subject=member.personSummary;

    if(subject){
      const subjectId=subject.id;

      const event = new CustomEvent('viewSubject', {
        bubbles:true,
        cancelable:true,
        detail:{
          subjectId: subjectId,
          subjectType: 'person'
        }
      });

      document.dispatchEvent(event);
    }
  }
  render(){
    const {member}=this.props;

    const person=member.personSummary;

    const classNames=classnames('member-link',{
      'historic': member.isInactiveHistoric,
      'future': member.joinDate>Date.now()
    });
    return (
      <div className="property-value">
        <a href="#none" className={classNames} onClick={this.handleClick}><Subject subjectType="person" name={person.name} identifier={person.personIdentifier}/></a>
      </div>
    );
  }
}

Member.propTypes={
  member:PropTypes.object.isRequired,
  labels:PropTypes.object.isRequired
};
