'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import MembersCount from './membersCount';

const DetailSlimline=({subject, labels})=>(
  <div className="slimline-info">
    <div className="slimline-col members-slimline"><MembersCount key="members" labels={labels.members||{}} membersCount={subject.membersCount}/></div>
  </div>
  );

DetailSlimline.propTypes={
  subject:PropTypes.object.isRequired,
  labels:PropTypes.object.isRequired
};

export default DetailSlimline;
