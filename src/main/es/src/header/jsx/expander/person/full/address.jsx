'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import MapLink from '../../../../../link/link';
import Temporary from '../../../../../indicator/temporary';
import Placement from '../../../../../indicator/placement';
import DoNotDisclose from '../../../../../indicator/doNotDisclose';
import { formatCodedEntry } from '../../../../../codedEntry/codedEntry';
import { formatAddressLocationDescription } from '../../../../../address/js/formatAddressLocation';
import { withPreferredAddress } from '../../../../../address/js/withPreferredAddress';

export default withPreferredAddress(class Address extends PureComponent {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    address: PropTypes.object,
    mapLinkUrl: PropTypes.string.isRequired,
    codedEntries: PropTypes.object.isRequired
  };

  render() {
    const { codedEntries = {}, address, mapLinkUrl, labels } = this.props;

    let addressContent;

    if (address) {
      const location = address.location;
      const temp = address.usage === 'TEMPORARY' ? (<Temporary label={labels.isTemporary} />) : false;
      const placement = address.type === 'PLACEMENT' ? (<Placement label={labels.isPlacement} />) : false;
      const doNotDisclose = address.doNotDisclose && (<DoNotDisclose label={labels.isDoNotDisclose} />);

      if (location) {
        const lines = [
          location.primaryNameOrNumber,
          location.secondaryNameOrNumber,
          location.street].join(' ');

        const town = location.postTown ? location.postTown : location.town;
        const roomDescription = address.roomDescription ? `${labels.room} ${address.roomDescription} ` : '';
        const floorDescription = address.floorDescription ? `${labels.floor} ${address.floorDescription} ` : '';

        const locationWithAddressDescription = formatAddressLocationDescription(address, labels);

        addressContent = (
          <div>
            <PropertyValue value={roomDescription} />
            <PropertyValue value={floorDescription} />
            <PropertyValue value={lines} />
            <PropertyValue value={town} />
            <PropertyValue value={location.locality} />
            <PropertyValue value={location.countyCode} />
            <PropertyValue value={(
              <MapLink url={mapLinkUrl} term={locationWithAddressDescription} target="_blank">{location.postcode}</MapLink>
            )} />
            {doNotDisclose}
            {temp}
            {placement}
          </div>
        );
      } else {
        addressContent = (
          <div>
            <PropertyValue value={formatCodedEntry(codedEntries.unknownLocation, address.unknownLocationType)} />
            {doNotDisclose}
            {temp}
            {placement}
          </div>
        );
      }

    }
    return (
      <div>
        <PropertyLabel label={labels.current} />
        {addressContent}
      </div>
    );
  }
});

