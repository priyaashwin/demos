'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Seq} from 'immutable';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import {formatDate} from '../../../../../date/date';
import DateValue from '../../../date/dateValue';
import {findCurrentKeyClassifications} from '../../../../js/classification';

export default class Classifications extends PureComponent {
    render(){
      const {classifications}=this.props;

      const keyClassifications=findCurrentKeyClassifications(classifications);

      return (
        <div>
          {
            keyClassifications.map((classificationAssignments, type)=>(
            <div key={type}>
              <PropertyLabel key={type} label={type}/>
              {
                Seq(classificationAssignments).map((assignment,i)=>{
                  const classificationVO=assignment.classificationVO||{};

                  let deactivated;
                  if(classificationVO.status==='ARCHIVED'){
                    deactivated=(<DateValue className="block-info" date={classificationVO.archivedDate}/>);
                  }

                  return (
                    <PropertyValue key={i} value={(
                        <span>
                          <span>
                            {classificationVO.name}
                          </span>
                          {deactivated}
                          <span className="block-info small-info">
                            {`(${formatDate(assignment.startDate, true)} ${assignment.endDate?'to ':''} ${assignment.endDate?formatDate(assignment.endDate, true):''})`}
                          </span>
                        </span>
                      )}/>
                  );
                })
              }
            </div>
          )).valueSeq().toArray()
        }
        </div>
      );
    }
}

Classifications.propTypes={
  labels:PropTypes.object.isRequired,
  classifications:PropTypes.object.isRequired
};
