'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Seq } from 'immutable';
import Subject from './subject';
import { withPersonPersonRelationship } from './withPersonPersonRelationship';

const getMostRelevantAllocatedTeam = (allocatedTeams) => {
  // These (should) be being returned in the correct order so we just want the first one.
  return Seq(allocatedTeams).first();
};
export default withPersonPersonRelationship(class AllocatedWorker extends PureComponent {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    person: PropTypes.node,
    allocatedTeams: PropTypes.array
  };

  render() {
    const { allocatedTeams, labels } = this.props;
    const subjectLabel = labels.allocatedWorker || 'allocated worker';

    let allocatedWorkerOrTeam = this.props.person;

    if (!allocatedWorkerOrTeam) {
      const team = getMostRelevantAllocatedTeam(allocatedTeams);
      if (team) {
        //allocated team
        allocatedWorkerOrTeam = (
          <span title={team.organisationVO.name}>
            <span>{team.organisationVO.name}</span>
          </span>
        );
      }
    }
    
    return <Subject label={subjectLabel} subject={allocatedWorkerOrTeam}/>;
  }
});
