'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Telephone from './telephone';
import Address from './address';
import AllocatedWorker from './allocatedWorker';
import NamedPerson from './namedPerson';
import Classifications from './classifications';
import classnames from 'classnames';

const DetailSlimline = ({ subject, classifications, codedEntries, contactCategory, mapLinkUrl, labels, isNamedPersonRelationshipAvailable, permissions }) => (
  <div className="slimline-info">
    <div className="slimline-info-group">
      <div className="slimline-col address-slimline"><Address key="address" labels={labels.address || {}} address={subject.preferredAddress} mapLinkUrl={mapLinkUrl} permissions={permissions} codedEntries={codedEntries} /></div>
      <div className="slimline-col telephone-slimline"><Telephone key="telephone" labels={labels.contact || {}} mobileContacts={subject.mobileContacts} telephoneContacts={subject.telephoneContacts} contactCategory={contactCategory} /></div>
    </div>
    <div className={classnames('slimline-info-group', {
      'col-3-group': isNamedPersonRelationshipAvailable
    })}>
      <div className="slimline-col allocated-worker-slimline"><AllocatedWorker key="allocatedWorker" labels={labels.allocated || {}} personPersonRelationships={subject.allocatedWorkers} allocatedTeams={subject.allocatedTeams} /></div>
      {isNamedPersonRelationshipAvailable &&
        <div className="slimline-col named-person-slimline"><NamedPerson key="namedPerson" labels={labels.named || {}} personPersonRelationships={subject.namedPersons} /></div>}
      <div className="slimline-col classifications-slimline"><Classifications key="classifications" labels={labels.classifications} classifications={classifications} /></div>
    </div>
  </div>
);

DetailSlimline.propTypes = {
  subject: PropTypes.object.isRequired,
  labels: PropTypes.object.isRequired,
  contactCategory: PropTypes.object.isRequired,
  mapLinkUrl: PropTypes.string.isRequired,
  classifications: PropTypes.object.isRequired,
  codedEntries: PropTypes.object.isRequired,
  permissions: PropTypes.object.isRequired,
  isNamedPersonRelationshipAvailable: PropTypes.bool.isRequired
};

export default DetailSlimline;
