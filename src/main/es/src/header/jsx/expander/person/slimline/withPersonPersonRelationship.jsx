'use strict';
import React, { PureComponent } from 'react';
import { Seq } from 'immutable';
import PropTypes from 'prop-types';

const getMostRelevantPersonPersonRelationship = (personPersonRelationships) => {
    //When saving relationship between prof and client or client and professional
    //the professional person is found in ROLE A.
    return Seq(personPersonRelationships).filter(role => role.typeDiscriminator === 'ProfessionalRelationshipType' && role.personId === role.roleAPersonId).first();
};

/**
 * A HOC that wraps the supplied component with an additional person property, culled from
 * a collection of person-person relationships.
 * 
 * @param {*} WrappedComponent 
 */
export function withPersonPersonRelationship(WrappedComponent) {

    class WithPersonPersonRelationship extends PureComponent {

        static propTypes = {
            personPersonRelationships: PropTypes.array
        }

        displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';

        render() {
            // Filter out extra props that are specific to this HOC and shouldn't be
            // passed through
            const { personPersonRelationships, ...passThroughProps } = this.props;

            let person;
            const personPersonRelationship = getMostRelevantPersonPersonRelationship(personPersonRelationships);

            if (personPersonRelationship) {
                person = (
                    <span title={personPersonRelationship.name + ' ' + personPersonRelationship.relationship}>
                        <span>{personPersonRelationship.name}</span>
                        <span>{' - '}</span>
                        <span>{personPersonRelationship.relationship}</span>
                    </span>
                );
            }

            return <WrappedComponent {...passThroughProps} person={person} />;
        }
    }

    WithPersonPersonRelationship.displayName = `WithPersonPersonRelationship(${getDisplayName(WrappedComponent)})`;
    return WithPersonPersonRelationship;
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}