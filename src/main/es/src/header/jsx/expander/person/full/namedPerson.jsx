'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withPersonPersonRelationship } from './withPersonPersonRelationship';

export default withPersonPersonRelationship(class NamedPerson extends PureComponent {
  static propTypes = {
    persons: PropTypes.node
  }
  render() {
    const { persons } = this.props;

    return (
      <div>
        {persons}
      </div>
    );
  }
},
'namedPerson',
'named person');
