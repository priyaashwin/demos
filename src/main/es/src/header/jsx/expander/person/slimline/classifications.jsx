'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import {findCurrentKeyClassifications} from '../../../../js/classification';

export default class Classifications extends PureComponent {
    render(){
      const {labels, classifications}=this.props;

      let classificationCategories;

      const categories=findCurrentKeyClassifications(classifications).valueSeq().toArray().length;
      if(categories>0){
        classificationCategories=`${categories} ${categories==1?'category':'categories'}`;
      }

      return (
        <div>
          <PropertyLabel label={labels.classifications||'classifications'}/>
          <PropertyValue value={classificationCategories} />
        </div>
      );
    }
}

Classifications.propTypes={
  labels:PropTypes.object.isRequired,
  classifications: PropTypes.object.isRequired
};
