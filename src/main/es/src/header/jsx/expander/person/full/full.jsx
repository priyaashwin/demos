'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Address from './address';
import Telephone from './telephone';
import AllocatedWorker from './allocatedWorker';
import NamedPerson from './namedPerson';
import Classifications from './classifications';
import classnames from 'classnames';

const DetailFull = ({ codedEntries, subject, contactCategory, classifications, mapLinkUrl, labels, isNamedPersonRelationshipAvailable }) => (
  <div className="full-info">
    <div className="full-info-group">
      <div className="full-col address-full"><Address codedEntries={codedEntries || {}} key="address" labels={labels.address || {}} address={subject.preferredAddress} mapLinkUrl={mapLinkUrl} /></div>
      <div className="full-col telephone-full"><Telephone key="telephone" labels={labels.contact || {}} mobileContacts={subject.mobileContacts} telephoneContacts={subject.telephoneContacts} contactCategory={contactCategory} /></div>
    </div>
    <div className={classnames('full-info-group', {
      'col-3-group': isNamedPersonRelationshipAvailable
    })}>
      <div className="full-col allocated-worker-full"><AllocatedWorker key="allocatedWorker" labels={labels.allocated || {}} personPersonRelationships={subject.allocatedWorkers} allocatedTeams={subject.allocatedTeams} mapLinkUrl={mapLinkUrl} /></div>
      {isNamedPersonRelationshipAvailable &&
        <div className="full-col named-person-full"><NamedPerson key="namedPerson" labels={labels.named || {}} personPersonRelationships={subject.namedPersons} /></div>}
      <div className="full-col classifications-full"><Classifications key="classifications" labels={labels.classifications || {}} classifications={classifications} /></div>
    </div>
  </div>
);

DetailFull.propTypes = {
  subject: PropTypes.object.isRequired,
  labels: PropTypes.object.isRequired,
  contactCategory: PropTypes.object.isRequired,
  mapLinkUrl: PropTypes.string.isRequired,
  classifications: PropTypes.object.isRequired,
  codedEntries: PropTypes.object.isRequired,
  isNamedPersonRelationshipAvailable: PropTypes.bool.isRequired
};
export default DetailFull;
