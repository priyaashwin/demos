'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Seq} from 'immutable';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import {formatCodedEntry} from '../../../../../codedEntry/codedEntry';
import Temporary from '../../../../../indicator/temporary';

const contactTypes=Seq(['HOME','EMERGENCY','WORK']).cacheResult();

const filterContacts=(contact)=>contactTypes.includes(contact.contactType);

const MAXIMUM_CONTACTS_TO_DISPLAY=6;

export default class Telephone extends PureComponent {
    render(){
      const {mobileContacts, telephoneContacts, contactCategory, labels}=this.props;

      //filter contacts to include only HOME, EMERGENCY, WORK
      const mobContact=Seq(mobileContacts).filter(filterContacts);
      const telContact=Seq(telephoneContacts).filter(filterContacts);

      const outputContact=(contacts, type)=>(<div>{
        contacts.filter(contact=>contact.contactType===type).map(contact=>{
          const temp=contact.contactUsage==='TEMPORARY'?(<Temporary label={labels.isTemporary}/>):false;
          return (<PropertyValue key={contact.id} value={(
              <span key={contact.id}>
                <span>{formatCodedEntry(contactCategory, contact.contactType)}</span>
                <span>{' - '}</span>
                <span>
                  <a href={`tel:${contact.number}`}>{contact.number}</a>
                </span>
                {temp}
              </span>
            )}/>);
        })
      }</div>);


      return (
        <div>
          <PropertyLabel label={labels.contact||'contact'}/>
          {
            contactTypes.map((type, i)=>(
              <div key={i}>
                {outputContact(mobContact, type)}
                {outputContact(telContact, type)}
              </div>
            )).take(MAXIMUM_CONTACTS_TO_DISPLAY)
          }
        </div>
      );
    }
}

Telephone.propTypes={
  labels:PropTypes.object.isRequired,
  mobileContacts:PropTypes.array,
  telephoneContacts:PropTypes.array,
  contactCategory:PropTypes.object.isRequired
};
