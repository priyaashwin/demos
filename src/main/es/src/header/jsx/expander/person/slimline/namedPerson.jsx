'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Subject from './subject';
import { withPersonPersonRelationship } from './withPersonPersonRelationship';

export default withPersonPersonRelationship(class NamedPerson extends PureComponent {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    person: PropTypes.node
  };

  render() {
    const { labels } = this.props;
    const namedPersonLabel = labels.namedPerson || 'named person';
    const namedPerson = this.props.person;

    return <Subject label={namedPersonLabel} subject={namedPerson}/>;
  }
});
