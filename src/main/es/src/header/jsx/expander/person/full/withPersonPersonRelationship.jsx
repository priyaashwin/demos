'use strict';
import React, { PureComponent } from 'react';
import { Seq } from 'immutable';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import PropTypes from 'prop-types';

const getWorkContacts=(contacts)=>{
    //filter contacts to include only WORK and email or telephone.
    const workContacts=Seq(contacts).filter(contact=>contact.contactType==='WORK' && (contact._type==='EmailContact' || contact._type==='TelephoneContact'));
    const telNumber = (workContacts.find(telContact=>telContact.number)||{}).number;
    const workTelContact = (telNumber?<a href={'tel:'+telNumber}>{telNumber}</a>:false);
    const emailAddress = (workContacts.find(emailContact=>emailContact.address)||{}).address;
    const workEmailContact = (emailAddress?<a href={'mailto:'+emailAddress}>{emailAddress}</a>:false);
    return {
      number:workTelContact,
      email:workEmailContact
    };
};

/**
 * A HOC that wraps the supplied component with an additional person property, culled from
 * a collection of person-person relationships.
 * 
 * @param {*} WrappedComponent 
 * @param labelName the name of the label to be used to hold the person label value
 * @param defaultLabel the fallback label value if the named label cannot be found in props
 */
export function withPersonPersonRelationship(WrappedComponent,labelName,defaultLabel) {

    class WithPersonPersonRelationship extends PureComponent {

        static propTypes = {
            labels: PropTypes.object.isRequired,
            personPersonRelationships: PropTypes.array
        }

        displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';

        render() {
            // Filter out extra props that are specific to this HOC and shouldn't be
            // passed through
            const { personPersonRelationships, labels, ...passThroughProps } = this.props;

            const personPersonRelationshipsSeq = Seq(personPersonRelationships);
            let persons;
            if (!personPersonRelationshipsSeq.isEmpty()) {
                persons = (
                    <div>
                        <PropertyLabel label={labels[labelName] || defaultLabel} />
                        {
                            personPersonRelationshipsSeq.filter(role => role.typeDiscriminator === 'ProfessionalRelationshipType' && role.personId === role.roleAPersonId).map(person => {
                                const contacts = getWorkContacts(person.contacts);

                                return (<div key={person.id}>
                                    <PropertyValue value={person.name} />
                                    <PropertyValue value={person.relationship} />
                                    <PropertyValue value={contacts.number} />
                                    <PropertyValue value={contacts.email} />
                                </div>);
                            })
                        }
                    </div>
                );
            }

            return <WrappedComponent {...passThroughProps} persons={persons} labels={labels}/>;
        }
    }

    WithPersonPersonRelationship.displayName = `WithPersonPersonRelationship(${getDisplayName(WrappedComponent)})`;
    return WithPersonPersonRelationship;
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}