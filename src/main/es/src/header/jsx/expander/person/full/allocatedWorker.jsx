'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Seq } from 'immutable';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import MapLink from '../../../../../link/link';
import { withPersonPersonRelationship } from './withPersonPersonRelationship';

export default withPersonPersonRelationship(class AllocatedWorker extends PureComponent {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    persons: PropTypes.node,
    allocatedTeams: PropTypes.array,
    mapLinkUrl: PropTypes.string.isRequired
  }

  render() {
    const { allocatedTeams, mapLinkUrl, labels } = this.props;
    const workers = this.props.persons;
    const allocatedTeamsSeq = Seq(allocatedTeams);
    let teams;
    if (!allocatedTeamsSeq.isEmpty()) {
      teams = (
        <div>
          <PropertyLabel label={labels.allocatedTeam || 'allocated team'} />
          {
            allocatedTeamsSeq.map(allocatedTeam => {
              let address;
              if (allocatedTeam.address) {
                address = (<PropertyValue value={(
                  <MapLink url={mapLinkUrl} term={allocatedTeam.address} target="_blank">{allocatedTeam.address}</MapLink>
                )} />);
              }
              return (<div key={allocatedTeam.id}>
                <PropertyValue value={allocatedTeam.organisationVO.name} />
                {address}
              </div>);
            })
          }
        </div>
      );
    }
    return (
      <div>
        {workers}
        {teams}
      </div>
    );
  }
},
'allocatedWorker',
'allocated worker');
