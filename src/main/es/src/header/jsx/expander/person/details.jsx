'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import DetailSlimline from './slimline/slimline';
import Collapse from 'react-bootstrap/lib/Collapse';
import DetailFull from './full/full';
import {groupClassificationAssignments} from '../../../js/classification';
import memoize from 'memoize-one';

export default class PersonDetails extends PureComponent {
  static propTypes={
    expanded:PropTypes.bool.isRequired,
    subject:PropTypes.object.isRequired,
    isNamedPersonRelationshipAvailable: PropTypes.bool
  };

  /**
   * memoize will only execute the internal function of the subject has changed
   */
  classificationAssignments=memoize((subject)=>groupClassificationAssignments(subject.classificationAssignments));

  render(){
    const {expanded, subject, ...other}=this.props;

    const classifications=this.classificationAssignments(subject);
    return (
      <div>
         <DetailSlimline key="slimline" {...other} subject={subject} classifications={classifications} />
         <div style={{position:'absolute',width:'100%'}}>
           <Collapse in={expanded}>
             <div className="expander-scroller">
               <div className="info-bar-expander">
                 <DetailFull key="full" {...other} subject={subject} classifications={classifications} />
               </div>
             </div>
           </Collapse>
         </div>
      </div>
    );
  }
}