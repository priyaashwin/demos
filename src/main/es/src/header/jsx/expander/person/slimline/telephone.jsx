'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Seq} from 'immutable';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import {formatCodedEntry} from '../../../../../codedEntry/codedEntry';
import Temporary from '../../../../../indicator/temporary';

const contactTypes=Seq(['HOME','EMERGENCY','WORK']).cacheResult();

const filterContacts=(contact)=>contactTypes.includes(contact.contactType);

const getMostRelevantContact=(mobileContacts, telephoneContacts)=>{
    //filter mobile contacts to include only HOME, EMERGENCY, WORK
    const mobContact=Seq(mobileContacts).filter(filterContacts);
    const telContact=Seq(telephoneContacts).filter(filterContacts);

    let contact;

    contactTypes.forEach(type=>{
        let number=mobContact.find(contact=>contact.contactType===type);
        if(!number){
          number=telContact.find(contact=>contact.contactType===type);
        }

        if(number){
          contact=number;
          //stops iteration
          return false;
        }
    });

    return contact;
};


export default class Telephone extends PureComponent {
    render(){
      const {mobileContacts, telephoneContacts, contactCategory, labels}=this.props;

      const contact=getMostRelevantContact(mobileContacts, telephoneContacts);

      let number;
      if(contact){
        const temp=contact.contactUsage==='TEMPORARY'?(<Temporary label={labels.isTemporary}/>):false;

        number=(
          <span title={contact.number}>
            {temp}
            <span>{formatCodedEntry(contactCategory, contact.contactType)}</span>
            <span>{' - '}</span>
            <span>
              <a href={'tel:'+contact.number}>{contact.number}</a>
            </span>
          </span>
        );
      }
      return (
        <div>
          <PropertyLabel label={labels.contact||'contact'}/>
          <PropertyValue value={number}/>
        </div>
      );
    }
}

Telephone.propTypes={
  labels:PropTypes.object.isRequired,
  mobileContacts:PropTypes.array,
  telephoneContacts:PropTypes.array,
  contactCategory:PropTypes.object.isRequired
};
