'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';

export default class Subject extends PureComponent {

    static propTypes = {
        label: PropTypes.string.isRequired,
        subject: PropTypes.node
      };

    render() {
        const { label, subject } = this.props;

        return (
            <div>
                <PropertyLabel label={label} />
                <PropertyValue value={subject} />
            </div>
        );
    }

}