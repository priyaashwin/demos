'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PropertyLabel from '../../../properties/propertyLabel';
import PropertyValue from '../../../properties/propertyValue';
import { withPreferredAddress } from '../../../../../address/js/withPreferredAddress';
import SingleLineAddress from '../../../../../address/jsx/singleLineAddress';

export default withPreferredAddress(class Address extends PureComponent {
  static propTypes = {
    labels: PropTypes.object.isRequired,
    address: PropTypes.object,
    mapLinkUrl: PropTypes.string.isRequired,
    permissions: PropTypes.shape({
      canViewAddress: PropTypes.bool
    }),
    codedEntries: PropTypes.shape({
      unknownLocation: PropTypes.object.isRequired
    }).isRequired

  };

  render() {
    const { labels, ...other } = this.props;

    return (
      <div>
        <PropertyLabel label={labels.address || 'address'} />
        <PropertyValue value={(
          <SingleLineAddress labels={labels} {...other} />
        )} />
      </div>
    );
  }
});

