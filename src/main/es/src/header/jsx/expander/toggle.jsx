'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Toggle=({expanded, onClick, labels})=>(
  <div className="subject-info-toggle">
    <a href="#none" title={expanded?labels.collapse:labels.expand} aria-label={expanded?labels.collapseTitle:labels.expandTitle} onClick={onClick}>
    <i className={classnames('fa', {
        'fa-chevron-circle-up':expanded,
        'fa-chevron-circle-down':!expanded
      })} aria-hidden="true"/>
  </a>
  </div>
);

Toggle.propTypes={
  expanded: PropTypes.bool.isRequired,
  labels:PropTypes.object.isRequired,
  onClick:PropTypes.func.isRequired
};

export default Toggle;
