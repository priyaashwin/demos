'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import PersonDetails from './person/details';
import GroupDetails from './group/details';
import Toggle from './toggle';

export default class ExpandableDetails extends PureComponent{
  constructor(props){
    super(props);

    this.state={
      expanded:false
    };

    this.handleClick=this.handleClick.bind(this);
  }
  toggle(){
    this.setState({
      expanded:!this.state.expanded
    });
  }
  handleClick(e){
    e.preventDefault();

    this.toggle();
  }
  render(){
    const {type, subject, labels, ...other}=this.props;

    let expanderContent;

    //only show the expander if there is a subject and it has a valid id
    if(subject && subject.id){
      let content;

      switch(type){
        case 'person':
          content=(<PersonDetails key="person-details" subject={subject} labels={labels} {...other} expanded={this.state.expanded}/>);
        break;
        case 'group':
          content=(<GroupDetails key="group-details" subject={subject} labels={labels} {...other} expanded={this.state.expanded}/>);
        break;

      }

      if(content){
        expanderContent=(
        <div className="subject-info-bar">
          <Toggle key="toggle" expanded={this.state.expanded} labels={labels} onClick={this.handleClick}/>
          {content}
        </div>
        );
      }
    }
    return (<div>{expanderContent}</div>);
  }
}

ExpandableDetails.propTypes={
  type:PropTypes.oneOf(['person', 'organisation', 'group']).isRequired,
  subject:PropTypes.object,
  labels:PropTypes.object.isRequired
};
