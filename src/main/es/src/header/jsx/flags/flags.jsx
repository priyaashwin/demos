'use strict';
import React  from 'react';
import PropTypes from 'prop-types';
import PersonFlags from './person/flags';
import OrganisationFlags from './organisation/flags';

const Flags=({type, ...other})=>{
  let content;
  switch(type){
    case 'person':
      content=(<PersonFlags {...other}/>);
    break;
    case 'organisation':
      content=(<OrganisationFlags {...other}/>);
      break;
    default:
      content=(<div />);
  }

  return content;
};

Flags.propTypes={
  type:PropTypes.oneOf(['person', 'organisation', 'group']).isRequired
};
export default Flags;
