'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Classifications from '../../classifications/classifications';
import { groupClassificationAssignments } from '../../../js/classification';
import Warnings from '../../../../warnings/warnings';
import Alerts from '../../alerts/alerts';
import Lock from '../../lock/lock';
import DuplicateStatus from '../../duplicateStatus/duplicateStatus';
import memoize from 'memoize-one';

export default class Flags extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        subject: PropTypes.object.isRequired,
        subjectLockInfoEndpoint: PropTypes.object.isRequired,
        codedEntries: PropTypes.shape({
            warnings: PropTypes.object.isRequired,
            alerts: PropTypes.object.isRequired
        }).isRequired,
        permissions: PropTypes.shape({
            isSubjectAccessAllowed: PropTypes.bool,
            canAddWarning: PropTypes.bool,
            canUpdateWarning: PropTypes.bool,
            canViewAlert: PropTypes.bool,
            canViewLock: PropTypes.bool,
            canUpdateAlert: PropTypes.bool
        }).isRequired,
        lastLoaded: PropTypes.number
    };

    /**
     * memoize will only execute the internal function of the subject has changed
     */
    classificationAssignments = memoize(subject => groupClassificationAssignments(subject.classificationAssignments));

    handleWarningsClick = () => {
        const { permissions, subject } = this.props;

        const permissionsObj = permissions || {};
        const canManageWarnings = permissionsObj.canAddWarning || permissionsObj.canUpdateWarning;

        if (!permissions.isSubjectAccessAllowed) {
            return;
        }

        let eventType;
        if (canManageWarnings) {
            eventType = 'addEditPersonWarning';
        } else {
            eventType = 'viewPersonWarning';
        }
        //createcustom event
        const event = new CustomEvent(eventType, {
            bubbles: true,
            cancelable: true,
            detail: {
                personId: subject.id
            }
        });

        document.dispatchEvent(event);
    };
    handleAlertsClick = () => {
        const { permissions, subject } = this.props;

        const permissionsObj = permissions || {};
        const canManageAlerts = permissionsObj.canUpdateAlert;

        let eventType;
        if (canManageAlerts) {
            eventType = 'managePersonAlerts';
        } else {
            eventType = 'viewPersonAlerts';
        }

        //createcustom event
        const event = new CustomEvent(eventType, {
            bubbles: true,
            cancelable: true,
            detail: {
                personId: subject.id
            }
        });

        document.dispatchEvent(event);
    };
    render() {
        const { subject, labels, codedEntries, permissions, lastLoaded, subjectLockInfoEndpoint } = this.props;
        let alerts;

        if (permissions.canViewAlert || permissions.canUpdateAlert) {
            alerts = (
                <Alerts
                    key="alerts"
                    subject={subject}
                    type="person"
                    labels={labels.alerts || {}}
                    canManageAlerts={permissions.canUpdateAlert}
                    category={codedEntries.alerts}
                    onClick={this.handleAlertsClick}
                />
            );
        }

        let lock;
        if (permissions.canViewLock) {
            lock = (
                <Lock
                    key="lock"
                    subject={subject}
                    type="person"
                    labels={labels.lock || {}}
                    subjectLockInfoEndpoint={subjectLockInfoEndpoint}
                    lastLoaded={lastLoaded}
                />
            );
        }

        const classifications = this.classificationAssignments(subject);
        return (
            <div className="subject-flags">
                <DuplicateStatus key="duplicateStatus" subject={subject} labels={labels.duplicateStatus || {}} />
                <Classifications
                    key="classifications"
                    classifications={classifications}
                    labels={labels.classifications || {}}
                />
                {lock}
                {alerts}
                <Warnings
                    key="warnings"
                    subject={subject}
                    type="person"
                    labels={labels.warnings || {}}
                    showDetails={permissions.isSubjectAccessAllowed}
                    canManageWarnings={permissions.canAddWarning || permissions.canUpdateWarning}
                    category={codedEntries.warnings}
                    onClick={this.handleWarningsClick}
                />
            </div>
        );
    }
}
