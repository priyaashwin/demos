'use strict';
import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Warnings from '../../../../warnings/warnings';

export default class Flags extends PureComponent{
  static propTypes={
    labels: PropTypes.object.isRequired,
    subject: PropTypes.object.isRequired
  };
  render(){
    const {subject, labels}=this.props;

    return (
      <div className="subject-flags">
        <Warnings key="warnings" subject={subject} type="organisation" labels={labels.warnings||{}} />
      </div>
    );
  }
}
