'use strict';

class Endpoint{
  constructor() {
    this.subjectDetailsEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.relationshipsrecording.personheaderdetails+json'
      },
      responseType: 'json',
      responseStatus:200
    };
    this.subjectLockInfoEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.security.securityaccessentrybodies+json'
      },
      responseType: 'json',
      responseStatus:200
    };
    this.subjectPictureEndpoint={
      method: 'get',
      contentType: 'image/*',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'image/png,image/*;q=0.8,*/*;q=0.5'
      },
      responseType: 'blob',
      responseStatus:200
    };
  }
}

export default new Endpoint();
