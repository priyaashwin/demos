import {Seq} from 'immutable';

const isCurrentAndKey=classificationAssignment=>classificationAssignment.classificationVO.keyInformation&&classificationAssignment.assignmentDateStatus==='CURRENT';
const filterKey=classificationAssignment=>Seq(classificationAssignment).filter(assignment=>isCurrentAndKey(assignment)).toArray();

export const findCurrentKeyClassifications=classifications=>classifications.map(classificationAssignment=>filterKey(classificationAssignment)).filter(classificationAssignment=>classificationAssignment.length);

export const groupClassificationAssignments=classificationAssignments=>Seq(classificationAssignments||[]).groupBy(classificationAssignment => classificationAssignment.classificationVO.classificationGroupName).sortBy((v, k)=>k, (k1, k2)=> k1.localeCompare(k2)).toKeyedSeq();
