'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { AutoComplete } from 'usp-autocomplete';
import DoNotDisclose from '../../indicator/doNotDisclose';

const NoRecordsFound = (
    <div key="no-records-message" className="no-search-results pure-info">
        <p>No teams match given criteria</p>
    </div>
);

export default class TeamAutoComplete extends PureComponent {
    static propTypes = {
        isSelectable: PropTypes.func.isRequired,
        onSelection: PropTypes.func,
        labels: PropTypes.object.isRequired
    };

    static defaultProps = {
        isSelectable: function () { return true; }
    };

    resultsLocator(response) {
        return response.results || [];
    }

    getAddress(selection) {
       const address = selection.address;
       const location = address ? address.location : undefined;
       const {labels} = this.props;

       let addressContent = false;
       if (location) {
           const doNotDisclose = address.doNotDisclose ? <DoNotDisclose label={labels.doNotDisclose} /> : '';
           const room = address.roomDescription ? (
               <span>{'Room '}{address.roomDescription}</span>
           ) : false;

           const floor = address.floorDescription ? (
               <span>{'Floor '}{address.floorDescription}</span>
           ) : false;
           addressContent = (
               <span className="addressLocation-wrapper">
                   {doNotDisclose}
                   {room}
                   {floor}
                   {location.location || ''}
               </span>
           );
       }
       return addressContent;
    }

    selectionFormatter = (selection) => {
        return (
            <>
                <div>{`${selection.organisationIdentifier} - ${selection.name}`}</div>
                {this.getAddress(selection)}
            </>
        );
    };

    suggestionFormatter = (selection) => {
      const selectable = this.props.isSelectable(selection);

      return (
          <div key={selection.id} className={classnames({
              unselectable: !selectable
          })} title={selection.name}> 
 
              <div disabled={!selectable}>
                  {this.selectionFormatter(selection)}
              </div>
          </div>
      );
    };

    autocompleteRef = (ref) => this.autoComplete = ref;
    render() {
        const {...other} = this.props;
        return (
            <AutoComplete
                ref={this.autocompleteRef}
                suggestionsLocator={this.resultsLocator}
                suggestionFormatter={this.suggestionFormatter}
                selectionFormatter={this.selectionFormatter}
                noRecordsFoundMessage={NoRecordsFound}
                {...other}
            />);
    }

}
