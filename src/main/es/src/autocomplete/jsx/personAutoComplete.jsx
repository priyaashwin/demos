'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { AutoComplete } from 'usp-autocomplete';
import { formatCodedEntry } from '../../codedEntry/codedEntry';
import { formatDateOfBirthWithLifeState } from '../../person/js/utils';


const NoRecordsFound = (
    <div key="no-records-message" className="no-search-results pure-info">
        <p>No people match your criteria</p>
    </div>
);


export default class PersonAutoComplete extends PureComponent {
    static propTypes = {
        codedEntries: PropTypes.shape({
            genderCategory: PropTypes.object.isRequired
        }).isRequired,
        isSelectable: PropTypes.func.isRequired,
        onSelection: PropTypes.func
    };

    static defaultProps = {
        isSelectable: function () { return true; }
    }
    resultsLocator(response) {
        return response.results || [];
    }
    selectionFormatter(selection) {
        return (<span>{`${selection.personIdentifier} - ${selection.name}`}</span>);
    }
    suggestionFormater = (selection) => {
        const { codedEntries, isSelectable } = this.props;
        const selectable = isSelectable(selection);

        const address = selection.address;
        const location = address ? address.location : undefined;

        let addressContent;
        if (location) {
            const room = address.roomDescription ? (
                <span>{'Room '}{address.roomDescription}</span>
            ) : false;

            const floor = address.floorDescription ? (
                <span>{'Floor '}{address.floorDescription}</span>
            ) : false;
            addressContent = (
                <div className={classnames('fl', {
                    'txt-color': selectable,
                    unselectable: !selectable
                })}>
                    <strong>{'Address '}</strong>
                    <span className="address">
                        {room}
                        {floor}
                        {location.location || ''}
                    </span>
                </div>
            );
        }


        return (
            <div key={selection.id} className={classnames({
                unselectable: !selectable
            })} title={selection.name}>

                <div disabled={!selectable}>
                    <span>{`${selection.personIdentifier} - ${selection.name}`}</span>
                </div>

                <div className={classnames('fl', {
                    'txt-color': selectable,
                    unselectable: !selectable
                })}>
                    {formatCodedEntry(selection.gender, codedEntries.genderCategory)}
                    {formatDateOfBirthWithLifeState(selection)}
                </div>
                {addressContent}
            </div>
        );
    };
    autocompleteRef=(ref)=>this.autoComplete=ref;
    render() {
        const { codedEntries: codedEntriesIgnored, ...other } = this.props;
        return (
            <AutoComplete
                ref={this.autocompleteRef}
                suggestionsLocator={this.resultsLocator}
                suggestionFormatter={this.suggestionFormater}
                selectionFormatter={this.selectionFormatter}                
                queryParameter="nameOrId"
                noRecordsFoundMessage={NoRecordsFound}
                {...other}
            />);
    }
}