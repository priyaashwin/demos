'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PersonAutoComplete from './personAutoComplete';
import { isProfessional } from '../../person/js/personType';
import { formatCodedEntry } from '../../codedEntry/codedEntry';
import classnames from 'classnames';

const NoRecordsFound = (
    <div key="no-records-message" className="no-search-results pure-info">
        <p>No professionals match your criteria</p>
    </div>
);

export default class ProfessionalRolePersonAutoComplete extends PureComponent {
    static propTypes = {
        codedEntries: PropTypes.shape({
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired
    };
    isSelectable(person) {
        return isProfessional(person.personTypes);
    }
    selectionFormatter = (selection) => {
        const { codedEntries } = this.props;
        return (
            <>
                <span>{selection.name}</span>
                <span>{` (${selection.personIdentifier}) `}</span>
                <span>{formatCodedEntry(codedEntries.professionalTitleCategory, selection.professionalTitle)}</span>
            </>
        );
    };
    suggestionFormater = (selection) => {
        const selectable = this.isSelectable(selection);

        const address = selection.address;
        const location = address ? address.location : undefined;

        let addressContent;
        if (location) {
            const room = address.roomDescription ? (
                <span>{'Room '}{address.roomDescription}</span>
            ) : false;

            const floor = address.floorDescription ? (
                <span>{'Floor '}{address.floorDescription}</span>
            ) : false;
            addressContent = (
                <div className={classnames('fl', {
                    'txt-color': selectable,
                    unselectable: !selectable
                })}>
                    <strong>{'Address '}</strong>
                    <span className="address">
                        {room}
                        {floor}
                        {location.location || ''}
                    </span>
                </div>
            );
        }


        return (
            <div key={selection.id} className={classnames({
                unselectable: !selectable
            })} title={selection.name}>
                <div disabled={!selectable}>
                    {this.selectionFormatter(selection)}
                </div>
                <div className={classnames('fl', {
                    'txt-color': selectable,
                    unselectable: !selectable
                })}>
                    {selection.organisationName}
                </div>
                {addressContent}
            </div>
        );
    };
    render() {
        const { ...other } = this.props;
        return <PersonAutoComplete
            noRecordsFoundMessage={NoRecordsFound}
            {...other}
            isSelectable={this.isSelectable}
            selectionFormatter={this.selectionFormatter}
            suggestionFormatter={this.suggestionFormater}
        />;
    }
}
