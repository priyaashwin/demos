'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { AutoComplete } from 'usp-autocomplete';

const NoRecordsFound = (
    <div key="no-records-message" className="no-search-results pure-info">
        <p>No security profiles match your criteria</p>
    </div>
);


export default class SecurityProfileAutoComplete extends PureComponent {
    static propTypes = {
        isSelectable: PropTypes.func.isRequired,
        onSelection: PropTypes.func
    };

    static defaultProps = {
        isSelectable: function () { return true; }
    }
    resultsLocator(response) {
        return response.results || [];
    }    
    selectionFormatter = (selection) => {
        return (
            <>
                <span>{`${selection.name}`}</span>
            </>
        );
    };
    suggestionFormater = (selection) => {
        const {isSelectable } = this.props;
        const selectable = isSelectable(selection);

        return (
            <div key={selection.id} className={classnames({
                unselectable: !selectable
            })} title={selection.name}>

                <div disabled={!selectable}>
                    <span>{`${selection.name}`}</span>
                </div>
            </div>
        );
    };
    autocompleteRef = (ref) => this.autoComplete = ref;
    render() {
        const { ...other } = this.props;
        return (
            <AutoComplete
                ref={this.autocompleteRef}
                suggestionsLocator={this.resultsLocator}
                suggestionFormatter={this.suggestionFormater}
                selectionFormatter={this.selectionFormatter}
                queryParameter="name"
                noRecordsFoundMessage={NoRecordsFound}
                {...other}
            />);
    }
}