'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { AutoComplete } from 'usp-autocomplete';
import { formatCodedEntry } from '../../codedEntry/codedEntry';

const NoRecordsFound = (
    <div key="no-records-message" className="no-search-results pure-info">
        <p>No organisations match your criteria</p>
    </div>
);

export default class OrganisationAutoComplete extends PureComponent {
    static propTypes = {
        codedEntries: PropTypes.shape({
            organisationTypeCategory: PropTypes.object.isRequired,
            organisationSubTypeCategory: PropTypes.object.isRequired
        }).isRequired,
        isSelectable: PropTypes.func.isRequired,
        onSelection: PropTypes.func
    };

    static defaultProps = {
        isSelectable: function () { return true; }
    }
    resultsLocator(response) {
        return response.results || [];
    }
    getAddress(selection) {
        const address = selection.address;
        const location = address ? address.location : undefined;

        let addressContent = false;
        if (location) {
            const room = address.roomDescription ? (
                <span>{'Room '}{address.roomDescription}</span>
            ) : false;

            const floor = address.floorDescription ? (
                <span>{'Floor '}{address.floorDescription}</span>
            ) : false;
            addressContent = (
                <span className="address">
                    {room}
                    {floor}
                    {location.location || ''}
                </span>
            );
        }
        return addressContent;
    }
    selectionFormatter = (selection) => {
        return (
            <>
                <span>{`${selection.organisationIdentifier} - ${selection.name}`}</span>
                {this.getAddress(selection)}
            </>
        );
    };
    suggestionFormater = (selection) => {
        const { codedEntries, isSelectable } = this.props;
        const selectable = isSelectable(selection);

        const address = selection.address;
        const location = address ? address.location : undefined;

        let addressContent;
        if (location) {
            addressContent = (
                <div className={classnames('fl', {
                    'txt-color': selectable,
                    unselectable: !selectable
                })}>
                    <strong>{'Address '}</strong>
                    {this.getAddress(selection)}
                </div>
            );
        }

        let typeContent;
        if (selection.type) {
            let subTypeContent;
            if (selection.subType) {
                subTypeContent = (
                    <span>
                        {' - '}
                        {formatCodedEntry(codedEntries.organisationSubTypeCategory, selection.subType)}
                    </span>
                );
            }
            typeContent = (<div className={classnames('fl', {
                'txt-color': selectable,
                unselectable: !selectable
            })}>
                <strong>{'Type '}</strong>
                <span>{formatCodedEntry(codedEntries.organisationTypeCategory, selection.type)}</span>
                {subTypeContent}
            </div>);
        }

        return (
            <div key={selection.id} className={classnames({
                unselectable: !selectable
            })} title={selection.name}>

                <div disabled={!selectable}>
                    <span>{`${selection.organisationIdentifier} - ${selection.name}`}</span>
                </div>
                {addressContent}
                {typeContent}
            </div>
        );
    };
    autocompleteRef = (ref) => this.autoComplete = ref;
    render() {
        const { codedEntries: codedEntriesIgnored, ...other } = this.props;
        return (
            <AutoComplete
                ref={this.autocompleteRef}
                suggestionsLocator={this.resultsLocator}
                suggestionFormatter={this.suggestionFormater}
                selectionFormatter={this.selectionFormatter}
                queryParameter="nameOrId"
                noRecordsFoundMessage={NoRecordsFound}
                {...other}
            />);
    }
}