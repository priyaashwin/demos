'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { getCancelToken } from 'usp-xhr';
import ModelList from '../modelList';
import ErrorMessage from '../../error/error';

/**
 * Wraps the supplied component with a dataLoad function
 * and injects a results parameter containing the result
 * of the data load into that component.
 *
 * Also acts as an error boundary for the wrapped component
 * and handles error output.
 *
 * @param {*} WrappedComponent
 * @param {object} endpoint  Endpoint configuration object
 */
export function withDataLoad(WrappedComponent, endpoint) {
    class WidgetWithDataLoad extends PureComponent {
        static propTypes = {
            subject: PropTypes.shape({
                subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
                subjectType: PropTypes.string
            }).isRequired,
            url: PropTypes.string.isRequired
        };

        state = {
            loading: true,
            errors: null,
            results: []
        };

        componentDidMount() {
            this.loadData();
        }
        componentWillUnmount() {
            if (this.cancelToken) {
                //cancel any outstanding xhr requests
                this.cancelToken.cancel();

                //remove property
                delete this.cancelToken;
            }
        }
        componentDidCatch() {
            this.setState({
                errors: 'Unexpected error'
            });
        }

        clearErrors = () => {
            this.setState({
                errors: null
            });
        };

        loadData() {
            const { url, subject } = this.props;
            const { subjectType, subjectId } = subject;

            //if there are any outstanding requests cancel them
            if (this.cancelToken) {
                //cancel any existing requests
                this.cancelToken.cancel();
            }

            //define parameters - clone any existing endpoint parameters
            const params = {
                ...endpoint.params
            };

            //setup a new cancel token
            this.cancelToken = getCancelToken();

            this.setState(
                {
                    loading: true
                },
                () => {
                    new ModelList(url)
                        .load(
                            {
                                ...endpoint,
                                params: {
                                    ...params
                                }
                            },
                            {
                                subjectId: subjectId,
                                subjectType: (subjectType || '').toLowerCase()
                            },
                            this.cancelToken
                        )
                        .then(response =>
                            this.setState({
                                loading: false,
                                errors: null,
                                results: response.results
                            })
                        )
                        .catch(reject => {
                            this.setState({
                                loading: false,
                                errors: reject,
                                results: []
                            });
                        });
                }
            );
        }

        render() {
            const { ...other } = this.props;
            const { results, errors, loading } = this.state;

            let content;
            if (errors) {
                content = <ErrorMessage errors={errors} onClose={this.clearErrors} />;
            } else {
                content = <WrappedComponent {...other} results={results} loading={loading} />;
            }
            return <>{content}</>;
        }
    }

    WidgetWithDataLoad.displayName = `withDataLoad(${getDisplayName(WrappedComponent)})`;

    return WidgetWithDataLoad;
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
