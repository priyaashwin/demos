import {subs} from '../subs/subs';
import {makeRequest} from 'usp-xhr';

const expr = /(\d+)(?!.*\d)/;

export default class Model {
    constructor(url) {
        this.url = url;
    }

    /**
    Takes a tokenized `url` string and substitutes its
    placeholders using a specified `data` object.

    This method will property URL-encode any values before substituting them.
    This only uses String and Number values as candidates

    @example
        var url = this._substituteURL('/users/{id}', {id: 1});
        // => "/users/1"

    @method getURL
    @param {Object} data Set of data to fill in the `url`'s placeholders.
    @return {String} Substituted URL.
    **/
    getURL(data) {
        if (!this.url) {
            return '';
        }

        const values = {};

        // Creates a hash of the string and number values only to be used to
        // replace any placeholders in a tokenized `url`.

        Object.keys(data||{}).forEach(k => {
            const v = data[k];
            if (this.isString(v) || this.isNumber(v)) {
                // URL-encode any string or number values.
                values[k] = encodeURIComponent(v);
            }
        });
        return subs(this.url, values);
    }
    isString(o) {
        return typeof o === 'string';
    }
    isNumber(o) {
        return typeof o === 'number' && isFinite(o);
    }

    parseResponse(response) {
      return response||{};
    }
    populateEndpoint(endpoint, data, other){
      //don't overwrite restEndpoint - treat as immutable
      const dataObj={
        ...endpoint.data,
        ...data
      };      

      return {
          ...endpoint,
          data:dataObj,
          url:this.getURL({
              ...dataObj,
              ...other
          })
      };
    }
    /**
     * The Promise to load the data
     */
    load(endpoint, other, cancelToken){
      return new Promise((resolve, reject) => {
          makeRequest(this.populateEndpoint(endpoint, {}, other), (success, failure) => {
              if (failure) {
                  //reject with the failure
                  reject(failure);
              } else {                                 
                //resolve with the parsed data
                resolve(this.parseResponse(success));
              }
          }, cancelToken);
      });
    }    

    /**
     * A Promise to save data from the model
     */
    save(endpoint, data, other){
        return new Promise((resolve, reject) => {
            const populatedEndpoint=this.populateEndpoint(endpoint, data, other);

            makeRequest(populatedEndpoint, (success, failure, response) => {
                if (failure) {
                    //reject with the failure
                    reject(failure);
                } else {                  
                  if (response.status === 201) {
                      const matches = expr.exec(response.headers.location);
                      if (matches && matches.length) {
                          resolve({
                              id:matches[0]
                          });
                      }
                  }else{
                      //resolve with the parsed data
                      resolve(this.parseResponse(success));
                  }
                }
            });
        });
      }    
}
