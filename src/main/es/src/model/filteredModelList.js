import ModelList from './modelList';
import {stringify} from 'querystring';

export default class FilteredModelList extends ModelList {
    constructor(url, whitelist, staticData) {
        super(url);
        //default to empty whitelist
        this.whitelist = whitelist || [];
        //default to empty static data
        this.staticData = staticData || {};
    }

    /**
    Takes a tokenized `url` string and substitutes its
    placeholders using a specified `data` object and then applies the specified
    filter data to the URL.

    @method getURL
    @param {String} url Tokenized URL string to substitute placeholder values.
    @param {Object} data Set of data to fill in the `url`'s placeholders.
    @return {String} Substituted URL.
    **/
    getURL(data, filter) {
        // use the superclass to generate the url
        let url = super.getURL(data);

        let parsedData = {};

        //now turn our attention to the filter
        if (filter) {
            const filterModel = {
                ...filter
            };

            //delete the id from the filterModel
            if (filterModel.id !== undefined) {
                delete filterModel.id;
            }
            //only include values on the whitelist
            this.whitelist.forEach(v => {
                const t = filterModel[v];
                if (typeof t !== 'undefined'&& t !== null) {
                    if (Array.isArray(t) && t.length === 0) {
                        return;
                    }
                    parsedData[v] = t;
                }
            });
          }

          //any static data
          if(this.staticData){
            //merge in any static data defined on the filter
            parsedData = Object.assign({}, parsedData, this.staticData);
          }

          //stringify the parameters
          const qStr = stringify(parsedData);
          if (qStr.length > 0) {
              url = url.concat('&' + qStr);
          }
        return url;
    }
}
