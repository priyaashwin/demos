import Model from './model';
export default class ModelList extends Model{
    constructor(url) {
        super(url);
    }
    parseResponse(response) {
      return response||[];
    }
}
