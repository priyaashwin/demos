'use strict';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import classnames from 'classnames';

/** PLEASE NEVER USE THIS AS AN EXAMPLE - UNLESS YOU WANT AN EXAMPLE OF
    HOW NOT TO CODE REACT - THIS IS COMPLETE AND UTTER JUNK ***/
export default class TreeNode extends Component{
  static propTypes={
    node: PropTypes.any,
    labels: PropTypes.object,
    eventHandler: PropTypes.func
  };

  constructor(props){
    super(props);

    this.state={
      visible: props.node.open
        ? true
        : false,
      type: props.node.groups? 'classificationGroup': 'classification',
      labels: props.labels
    };

    this._addEventHandler=this._addEventHandler.bind(this);
    this._getEventData=this._getEventData.bind(this);
    this.toggle=this.toggle.bind(this);
    this.showInfo=this.showInfo.bind(this);
    this.hideInfo=this.hideInfo.bind(this);
  }

  _addEventHandler(event) {
    var data = this._getEventData(event.target);

    //Handle this event to YUI event handler function and do the rest there to open multipanel pop up
    this.props.eventHandler(data);
  }
  _getEventData(target) {
    return {target: target, codePath: this.props.node.codePath};
  }
  toggle() {
    this.setState({
      visible: !this.state.visible
    });
  }
// This is truly shocking - read the REACT documentation instead of hacking it.
  showInfo() {
    //Name of the tree is counted as level 0
    ReactDOM.findDOMNode(this).querySelector('.actions').style.display = 'inline-block';
    ReactDOM.findDOMNode(this).querySelector('h5').style.backgroundColor = '#F5F5F5';
  }
  hideInfo() {
    ReactDOM.findDOMNode(this).querySelector('.actions').style.display = 'none';
    ReactDOM.findDOMNode(this).querySelector('h5').style.backgroundColor = '#fff';
  }

  render() {
    //Urghh - really - props is meant to be immutable!!!
    if (this.props.node.classifications && this.props.node.groups) {
      this.props.node.childNodes = [
        ...this.props.node.classifications,
        ...this.props.node.groups
      ];
    } else {
      this.props.node.childNodes = this.props.node.classifications || this.props.node.groups;
    }

    var childNodes = this.props.node.childNodes,
      classObj,
      eventHandler = this.props.eventHandler,
      labels = this.props.labels,
      viewActionItemClass,
      addActionItemClass,
      editActionItemClass,
      deleteActionItemClass,
      restoreActionItemClass;

    if (this.props.node.childNodes) {

      childNodes = this.props.node.childNodes.map(function(node, index) {

        return (
          <li className="container" key={index}><TreeNode node={node} eventHandler={eventHandler} labels={labels}/></li>
        );

      });

      classObj = {
        togglable: true,
        'togglable-down': this.state.visible,
        'togglable-up': !this.state.visible
      };
    }

    var style;
    if (!this.state.visible) {
      style = {
        display: 'none'
      };
    }

    //  Check if node
    if (Object.keys(this.props.node).length < 3) {
      var lastNode = 'last-node';
    }

    let children;

    if (typeof this.props.node.childNodes != 'undefined') {
      if (this.props.node.childNodes.length > 1) {
        children = 'multiple-nodes';
      } else {
        children = 'single-nodes';
      }
    }

    // Every classification & group can be viewed
    viewActionItemClass = {
      'show-action-item': true
    };
    // Only classification groups can be added to
    addActionItemClass = {
      'hide-action-item': this.state.type === 'classification',
      'show-action-item': this.state.type === 'classificationGroup'
    };
    // System classifications and groups cannot be edited
    editActionItemClass = {
      'hide-action-item': this.props.node.system,
      'show-action-item': !this.props.node.system
    };
    // In order to be eligible for delete, classification or group must be
    // system = false and NOT status = ARCHIVED
    deleteActionItemClass = {
      'hide-action-item': !this.props.node.isStatusModifiable || this.props.node.status === 'ARCHIVED',
      'show-action-item': this.props.node.isStatusModifiable && this.props.node.status !== 'ARCHIVED'
    };
    // In order to be eligible for restoration, classification or group must be
    // system = false and status = ARCHIVED
    restoreActionItemClass = {
      'hide-action-item': !this.props.node.isStatusModifiable || this.props.node.status !== 'ARCHIVED',
      'show-action-item': this.props.node.isStatusModifiable && this.props.node.status === 'ARCHIVED'
    };

    //ICONS on tree view

    var showIconHeader = {
        'hide-action-item': !this.props.node.significantWhenCurrent,
        'show-action-item': this.props.node.significantWhenCurrent
      },

      restrictMultipleInstance = {
        'hide-action-item': !this.props.node.mutuallyExclusive,
        'show-action-item': this.props.node.mutuallyExclusive
      },
      iconPersist = {
        'hide-action-item': !this.props.node.significantWhenHistoric,
        'show-action-item': this.props.node.significantWhenHistoric
      },
      highlightDeleteClass = {
        'classification-name-strike': this.props.node.status === 'ARCHIVED'
      };

      var actionMenu = (<span className="actions">

        <span className={classnames(viewActionItemClass)}>
          <a
            data-id={this.props.node.id}
            data-type={this.state.type}
            className="classificationView"
            onClick={this._addEventHandler}
            href="#">view</a>
        </span>

        <span className={classnames(addActionItemClass)}>
          <a
            data-id={this.props.node.id}
            data-parent-id={this.props.node.parentGroupId}
            data-type={this.state.type}
            className="classificationAdd"
            onClick={this._addEventHandler}
            href="#">add</a>
        </span>

        <span className={classnames(editActionItemClass)}>
          <a
            data-id={this.props.node.id}
            data-type={this.state.type}
            className={classnames('classificationEdit')}
            onClick={this._addEventHandler}
            href="#">edit</a>
        </span>

        <span className={classnames(deleteActionItemClass)}>
          <a
            data-id={this.props.node.id}
            data-type={this.state.type}
            className={classnames('classificationDelete')}
            onClick={this._addEventHandler}
            href="#">delete</a>
        </span>

        <span className={classnames(restoreActionItemClass)}>
          <a
            data-id={this.props.node.id}
            data-type={this.state.type}
            className={classnames('classificationRestore')}
            onClick={this._addEventHandler}
            href="#">restore</a>
        </span>

      </span>);

    return (
      <ul>

        <li className={lastNode}>
          <h5
            onMouseOver={this.showInfo}
            onMouseLeave={this.hideInfo}
            onClick={this.toggle}
            className={classnames(classObj)}>

            <span
              className={classnames('icon', 'showIconHeader', showIconHeader)}
              title={labels.iconITitle}>

              <span className="icon-label">
                {labels.iconI}
              </span>

            </span>

            <span
              className={classnames('icon', 'iconPersist', iconPersist)}
              title={labels.iconPTitle}>

              <span className="icon-label">
                {labels.iconP}
              </span>

            </span>

            <span
              className={classnames('icon', 'restrictMultipleInstance', restrictMultipleInstance)}
              title={labels.iconRTitle}>

              <span className="icon-label">
                {labels.iconR}
              </span>

            </span>

            <span className={classnames(highlightDeleteClass)}>
              {this.props.node.name}
            </span>
            {this.props.node.codePath.length >= 1?actionMenu:null}
          </h5>

          <ul className={children} style={style}>
            {childNodes}
          </ul>
        </li>
      </ul>
    );
  }
}
