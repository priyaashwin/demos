'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import TreeNode from '../js/treeNode';

const ClassificationTreeView=({clickHandler, labels, tree})=>(
	<TreeNode node={{
		name: 'Classifications',
		groups: tree,
		open: true,
		codePath: []
	}} eventHandler={clickHandler} labels={labels}/>
);

ClassificationTreeView.propTypes={
	clickHandler: PropTypes.func,
	tree: PropTypes.any,
	labels: PropTypes.object
};

export {ClassificationTreeView as default};
