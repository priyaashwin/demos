'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Link=({url, term, target, children})=>(
  <a href={url.concat(term||'')} target={target}>{children}</a>
);

Link.propTypes={
  url: PropTypes.string.isRequired,
  term: PropTypes.any,
  target: PropTypes.string,
  children: PropTypes.node
};

export default Link;
