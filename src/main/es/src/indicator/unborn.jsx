'use strict';
import React  from 'react';
import PropTypes from 'prop-types';
import Indicator from './indicator';

const Unborn=({label})=>(
  <Indicator label={label||'Unborn'} aria-label={label||'Unborn'} className="unborn"/>
);

Unborn.propTypes={
  label:PropTypes.string
};

export default Unborn;
