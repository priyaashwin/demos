'use strict';
import React  from 'react';
import PropTypes from 'prop-types';
import Indicator from './indicator';

const Deceased=({label})=>(
  <Indicator label={label||'Deceased'} aria-label={label||'Deceased'} className="deceased"/>
);

Deceased.propTypes={
  label:PropTypes.string
};

export default Deceased;
