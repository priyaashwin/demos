'use strict';
import React  from 'react';
import PropTypes from 'prop-types';
import Indicator from './indicator';

const DoNotDisclose=({label})=>(
  <Indicator
    label={label||'Do not disclose'}
    aria-label={label||'Do not disclose'}
    className="do-not-disclose"
    icon=<i className="fa fa-ban" aria-hidden="true"></i>
  />
);

DoNotDisclose.propTypes={
  label:PropTypes.string
};

export default DoNotDisclose;
