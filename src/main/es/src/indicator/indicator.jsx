'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../infopop/infopop';

const Indicator = ({ title, label, align, className, icon }) => (
  <InfoPop
    title={title}
    content={label}
    align={align}
  >
    <span className={className} aria-label={label}>{icon}</span>
  </InfoPop>
);

Indicator.propTypes = {
  title: PropTypes.string,
  label: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  align: PropTypes.string,
  icon: PropTypes.object
};

Indicator.defaultProps={
  align: 'bottom'
};

export default Indicator;
