'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Indicator from './indicator';

const Placement=({label})=>(
  <Indicator label={label||'Placement'} aria-label={label||'Placement'} className="placement"/>
);

Placement.propTypes={
  label:PropTypes.string
};

export default Placement;
