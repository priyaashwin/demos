'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Indicator from './indicator';

const Temporary=({label})=>(
  <Indicator label={label||'Temporary'} aria-label={label||'Temporary'} className="temporary"/>
);

Temporary.propTypes={
  label:PropTypes.string
};

export default Temporary;
