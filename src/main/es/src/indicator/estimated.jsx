'use strict';
import React  from 'react';
import PropTypes from 'prop-types';
import Indicator from './indicator';

const Estimated=({label})=>(
  <Indicator label={label||'Estimated'} aria-label={label||'Estimated'} className="estimated"/>
);

Estimated.propTypes={
  label:PropTypes.string
};

export default Estimated;
