'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { makeRequest, getCancelToken } from 'usp-xhr';

export default class Picture extends PureComponent {
  state = {
    imageBase64: null
  };  
  componentDidMount() {
    if (this.props.endpoint) {
      this.requestImage(this.props.endpoint);
    }
  }
  componentDidUpdate(nextProps) {
    if (nextProps.endpoint) {
      if (!this.props.endpoint || nextProps.endpoint.url !== this.props.endpoint.url) {
        this.requestImage(nextProps.endpoint);
      }
    }
  }
  componentWillUnmount() {
    if (this.cancelToken) {
      //cancel any outstanding xhr requests
      this.cancelToken.cancel();

      //remove property
      delete this.cancelToken;
    }
  }
  requestImage(endpoint) {
    //if there are any outstanding requests cancel them
    if (this.cancelToken) {
      //cancel any existing requests
      this.cancelToken.cancel();
    }

    //setup a new cancel token
    this.cancelToken = getCancelToken();

    new Promise((resolve, reject) => {
      //make the request using the cancel token
      makeRequest(endpoint, (success, failure) => {
        if (failure) {
          //reject with the failure
          reject(failure);
        } else {
          //resolve with the RAW data
          resolve(success);
        }
      }, this.cancelToken);
    }).then(response => {
      const reader = new FileReader();
      reader.onload = (e) => this.setState({
        imageBase64: e.target.result
      });

      //read the blob
      reader.readAsDataURL(response);
    }).catch(() => this.setState({ imageBase64: null }));
  }
  render() {
    const { emptyPicture, className } = this.props;

    let content;

    if (this.state.imageBase64) {
      const style = {
        backgroundImage: ['url(', this.state.imageBase64, ')'].join('')
      };
      content = (<span className={className} style={style} />);

    } else {
      content = (<span className="empty-picture">{emptyPicture}</span>);
    }

    return content;
  }
}

Picture.propTypes = {
  className: PropTypes.string,
  emptyPicture: PropTypes.node,
  endpoint: PropTypes.object
};
