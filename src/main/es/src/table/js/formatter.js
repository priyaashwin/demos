import { formatDateValue } from '../../date/date';

/**
 * Formats a numeric value as currency.
 * The currency and locale is read from the currencyFormat
 * object in the column configuration.
 *
 * A default value of 'GBP' is used for currency type
 * A default value of 'en-GB' is used for the locale
 *
 * If the value is NaN, the value is returned
 * unmodified
 *
 * @method currency
 * @param value {Number} The value to format
 * @param data {Object} The complete record data
 * @param column {Object} The column configuration object
 * @return {string} A string value representing the value formatted as currency.
 **/
const currency = function(value, data, column) {
    const currencyFormat = column.currencyFormat || {};
    let formattedValue = value;
    if (!Number.isNaN(value)) {
        formattedValue = new Intl.NumberFormat(currencyFormat.locale || 'en-GB', {
            style: 'currency',
            currency: currencyFormat.currency || 'GBP'
        }).format(value);
    }

    return formattedValue;
};

/**
 * Formats a cell with a Boolean value into 'Yes' or 'No'
 * @method yesNo
 * @param value {Boolean} The value to format
 * @return {string} A string value representing the value formatted as boolean yes/no.
 */
const yesNo = function(value) {
    if (value !== undefined && value !== null) {
        if (typeof value === 'boolean') {
            if (value === true) {
                return 'Yes';
            } else if (value === false) {
                return 'No';
            }
        } else if (value.toLowerCase() === 'true') {
            return 'Yes';
        } else if (value.toLowerCase() === 'false') {
            return 'No';
        }
    }
    //return empty string (i.e. bool not set)
    return '';
};

/**
 * Formats the value into a usp style date.
 *
 * An optional mask can be supplied via the column configuration
 * on the property maskField
 *
 * @dateFormatter currency
 * @param value {Number} The raw value to format
 * @param data {Object} The complete record data
 * @param column {Object} The column configuration object
 * @return {string} A string value representing the value formatted as usp date.
 **/
const date = function(value, data, column) {
    const maskField = column ? column.maskField : undefined;

    return formatDateValue(value, maskField);
};

/**
 * Returns the toString() representation of the value
 * if that value is non-null and not undefined
 *
 * @param {*} value
 */
const defaultFormatter = function(value) {
    let formattedValue = '';
    if (!(typeof value === 'undefined' || value === null)) {
        formattedValue = `${value}`;
    }

    return formattedValue;
};

export { currency, yesNo, date, defaultFormatter };
