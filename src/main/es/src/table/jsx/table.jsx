'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Colgroup from './colgroup';
import Thead from './thead';
import Tbody from './tbody';
import Expander from './expander';
import memoize from 'memoize-one';
import Collapse from 'react-bootstrap/lib/Collapse';
import { defaultFormatter } from '../js/formatter';
import Mask from '../../mask/jsx/mask';

const WIDTH_STYLE = {
    width: '100%'
};

const expandLabel = 'Click to expand';
const collapseLabel = 'Click to collapse';
const expandAllLabel = 'Click to expand all rows';
const collapseAllLabel = 'Click to collapse all rows';

export default class Table extends PureComponent {
    static propTypes = {
        /**
         * Array of column configuration
         */
        columns: PropTypes.arrayOf(
            PropTypes.shape({
                key: PropTypes.string.isRequired,
                className: PropTypes.string,
                label: PropTypes.string,
                width: PropTypes.string,
                sortable: PropTypes.bool,
                formatter: PropTypes.func
            })
        ).isRequired,
        /**
         * Array of data
         */
        data: PropTypes.array,
        loading: PropTypes.bool,
        /**
         * summary row configuration object
         * enabled flag and formatter
         */
        summaryRow: PropTypes.shape({
            enabled: PropTypes.bool,
            formatter: PropTypes.func
        }),
        initiallyExpanded: PropTypes.bool,
        selectedRowIndex: PropTypes.number,
        onSelectRow: PropTypes.func,
        onSort: PropTypes.func,
        sortBy: PropTypes.arrayOf(
            PropTypes.shape({
                order: PropTypes.oneOfType([PropTypes.oneOfType(['ASC', 'DESC']), PropTypes.number]),
                field: PropTypes.string
            })
        ),
        emptyMessage: PropTypes.string
    };

    static defaultProps = {
        onSort: () => {},
        emptyMessage: ''
    };

    computeExpandedState = memoize((expanded, data) => (data || []).map(() => expanded));

    //no summary row expanded
    state = {
        expanded: this.props.initiallyExpanded,
        //compute new expanded state
        expandedState: this.computeExpandedState(this.props.initiallyExpanded, this.props.data)
    };
    handleRowClick = e => {
        const { onSelectRow, data = [], selectedRowIndex } = this.props;
        const dataIndex = e.currentTarget.getAttribute('data-index');
        if (dataIndex && onSelectRow) {
            const i = parseInt(dataIndex, 10);
            if (selectedRowIndex !== i) {
                //don't fire row selection if this row is already selected
                onSelectRow(data[i]);
            }
        }
    };
    componentDidUpdate(prevProps, prevState) {
        //if the data has changed - or the expanded state has changed - recalculate
        if (prevProps.data != this.props.data || prevState.expanded != this.state.expanded) {
            const expandedState = this.computeExpandedState(this.state.expanded, this.props.data);
            if (expandedState !== this.state.expandedState) {
                this.setState({ expandedState: expandedState }); //eslint-disable-line react/no-did-update-set-state
            }
        }
    }
    getTableBody() {
        const { loading, columns, data } = this.props;
        if (loading && !(data && data.length)) {
            return (
                <Tbody key="loading-body">
                    <tr>
                        <td
                            className="pure-table-message-content"
                            colSpan={columns.length + (this.isSummaryRowEnabled() ? 1 : 0)}>
                            {'Loading...'}
                        </td>
                    </tr>
                </Tbody>
            );
        }
        return <tbody className="pure-table-data">{this.getTableRows()}</tbody>;
    }
    getColumnValue(column, data) {
        //retrieve hierarchical object key from a string in dot notation
        const keys = column.key.split('.');
        let value = data[keys[0]];
        for (let i = 1; i < keys.length; i++) {
            value = value[keys[i]];
        }
        return value;
    }
    getDataCell(column, data, args) {
        const value = this.getColumnValue(column, data);
        if (column.formatter) {
            return <>{column.formatter.call(null, value, data, column, args)}</>;
        } else {
            return <>{defaultFormatter(value)}</>;
        }
    }
    getExpanderColumn(index) {
        const { expandedState } = this.state;

        const expanded = expandedState[index];
        if (this.isSummaryRowEnabled()) {
            return (
                <td key="expanderCol" rowSpan={2} className={classnames('pure-table-cell', 'pure-table-col-collapse')}>
                    <Expander
                        key="expanded"
                        expanded={expanded}
                        onExpand={this.handleExpand}
                        expandLabel={expandLabel}
                        collapseLabel={collapseLabel}
                        index={index}
                    />
                </td>
            );
        }

        return false;
    }
    getExpanderRow(index, dataRow) {
        const { columns, summaryRow, selectedRowIndex } = this.props;
        const { expandedState } = this.state;

        const expanded = expandedState[index];
        if (this.isSummaryRowEnabled()) {
            return (
                <tr
                    key={`expander-${index}`}
                    className={classnames('pure-table-mobile-hidden', 'summary-row', {
                        'pure-table-even': index % 2 === 0,
                        'pure-table-odd': index % 2 !== 0,
                        'yui3-datatable-selected': index === selectedRowIndex,
                        visible: expanded,
                        hidden: !expanded
                    })}
                    tabIndex={expanded ? 0 : -1}
                    data-index={index}
                    onClick={this.handleRowClick}>
                    <td colSpan={columns.length}>
                        <Collapse key={`transe-${index}`} in={expanded} timeout={350}>
                            <div className="summary-row-parent">
                                <div className="summary-row-content">
                                    <>{summaryRow.formatter.call(null, dataRow)}</>
                                </div>
                            </div>
                        </Collapse>
                    </td>
                </tr>
            );
        }

        //return no output if expander row not enabled
        return false;
    }
    isSummaryRowEnabled() {
        const { summaryRow } = this.props;
        return summaryRow && summaryRow.enabled === true && summaryRow.formatter;
    }
    getTableRows() {
        const { data, columns, selectedRowIndex, emptyMessage } = this.props;
        if (!data || (!data.length && emptyMessage)) {
            return (
                <tr>
                    <td
                        className="pure-table-message-content"
                        colSpan={columns.length + (this.isSummaryRowEnabled() ? 1 : 0)}>
                        {emptyMessage}
                    </td>
                </tr>
            );
        }
        return data.map((dataRow, i) => (
            <React.Fragment key={i}>
                <tr
                    key={`data-${i}`}
                    className={classnames({
                        'pure-table-even': i % 2 === 0,
                        'pure-table-odd': i % 2 !== 0,
                        'summary-row-host': this.isSummaryRowEnabled(),
                        'yui3-datatable-selected': i === selectedRowIndex
                    })}
                    data-index={i}
                    tabIndex="0"
                    onClick={this.handleRowClick}>
                    {this.getExpanderColumn(i)}
                    {columns.map((column, j) => (
                        <td
                            key={j}
                            className={classnames('pure-table-cell', `pure-table-col-${column.key}`)}
                            data-title={column.label}>
                            {this.getDataCell(column, dataRow, { rowIndex: i, columnIndex: j, rows: data })}
                        </td>
                    ))}
                    {this.getMobileSummary(i, dataRow)}
                </tr>
                {this.getExpanderRow(i, dataRow)}
            </React.Fragment>
        ));
    }
    getMobileSummary(index, dataRow) {
        const { summaryRow } = this.props;

        if (this.isSummaryRowEnabled()) {
            return (
                <td
                    key={`mobile-summary-${index}`}
                    data-title="Summary"
                    rowSpan={1}
                    colSpan="100%"
                    className={classnames(
                        'pure-table-col-mobile-summary',
                        'mobile-summary',
                        'pure-table-desktop-hidden',
                        'pure-table-cell'
                    )}>
                    {summaryRow.formatter.call(null, dataRow)}
                </td>
            );
        }

        //return no output if expander row not enabled
        return false;
    }
    handleExpandAll = expand => {
        this.setState({
            expanded: expand
        });
    };
    handleExpand = (expand, index) => {
        this.setState(state => {
            const newExpandedState = [...state.expandedState];
            newExpandedState[index] = expand;

            return {
                expandedState: newExpandedState
            };
        });
    };
    render() {
        const { columns, loading, data, onSort, sortBy } = this.props;
        const { expanded } = this.state;
        const isSummaryRowEnabled = this.isSummaryRowEnabled();
        let summaryRowHeader;
        if (isSummaryRowEnabled) {
            //only has summary row if enabled and there is a valid formatter
            summaryRowHeader = (
                <th colSpan={1} rowSpan={1} className="pure-table-header pure-table-col-collapse" scope="col">
                    <Expander
                        key="expanded"
                        expanded={expanded}
                        onExpand={this.handleExpandAll}
                        expandLabel={expandAllLabel}
                        collapseLabel={collapseAllLabel}
                    />
                </th>
            );
        }

        return (
            <div className="pure-table pure-table-striped pure-table-hover">
                <div
                    className={classnames(
                        'usp-datatable',
                        'yui3-datatable',
                        'pure-table',
                        'pure-table-responsive',
                        'pure-table-sortable',
                        'pure-table-focused',
                        {
                            'results-table-summary-row-enabled': isSummaryRowEnabled
                        }
                    )}>
                    <Mask key="loading-mask" contained show={loading && !!(data && data.length)} />
                    <table key="table" className="pure-table" cellSpacing={0} style={WIDTH_STYLE}>
                        <Colgroup key="colgroup" columns={columns} hasSummaryRow={isSummaryRowEnabled} />
                        <Thead
                            key="thead"
                            columns={columns}
                            summaryRowHeader={summaryRowHeader}
                            onSort={onSort}
                            sortBy={sortBy}
                        />
                        {this.getTableBody()}
                    </table>
                </div>
            </div>
        );
    }
}
