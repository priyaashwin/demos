'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InlineMenu from 'usp-inline-menu';

export default class TableActions extends PureComponent {
    static propTypes = {
        actions: PropTypes.array,
        handleClick: PropTypes.func.isRequired,
        record: PropTypes.object
    };
    handleClick = action => {
        this.props.handleClick(action, this.props.record);
    };
    render() {
        const { actions } = this.props;
        const className = actions.length === 0 ? 'pure-table-menu hidden' : 'pure-table-menu';
        return (
            <div className={className}>
                <InlineMenu
                    options={actions}
                    menuIcon={<span className="pure-menu-label">Select</span>}
                    onMenuItemClick={this.handleClick}
                    vertical={true}
                />
            </div>
        );
    }
}
