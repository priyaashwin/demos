'use strict';
import React, { memo, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const normaliseOrder = order => {
    if (order === undefined || order === null || order === '') {
        return 0;
    } else {
        switch (order) {
            case 'desc':
            case 'DESC':
            case -1:
                return -1;
            case 'asc':
            case 'ASC':
            case 1:
                return 1;
        }
    }
};

const SortableColumn = memo(({ index, column, onSort, sortBy }) => {
    const currentSortForColumn = useMemo(() => {
        const currentSort = sortBy.find(s => s.field === column.key);
        if (currentSort) {
            return normaliseOrder(currentSort.order);
        }
        return 0;
    }, [column.key, sortBy]);

    const handleSort = useCallback(
        e => {
            e.preventDefault();
            if (e.type === 'keydown' && e.keyCode !== 32) {
                return;
            }
            
            //default order to desc
            let newOrder = -1;
            if (currentSortForColumn < 0) {
                newOrder = 1;
            }
            const sort = {
                field: column.key,
                order: newOrder
            };
            if (e.shiftKey) {
                //modifier pressed - so multi-sort
                const i = sortBy.findIndex(s => s.field === column.key);
                const newSort = [...sortBy];
                if (i >= 0) {
                    newSort.splice(i, 1, sort);
                } else {
                    newSort.push(sort);
                }

                onSort(newSort);
            } else {
                onSort([sort]);
            }
        },
        [column.key, onSort, currentSortForColumn, sortBy]
    );
    let ariaSort = '';
    if (currentSortForColumn < 0) {
        ariaSort = 'descending';
    } else if (currentSortForColumn > 0) {
        ariaSort = 'ascending';
    }
    const title = `${currentSortForColumn > 0 ? 'Reverse sort by' : 'Sort by'} ${column.label}`;
    return (
        <th
            key={index}
            className={classnames(
                'pure-table-header',
                `pure-table-col-${column.key}`,
                column.className,
                'pure-table-sortable-column',
                {
                    'pure-table-sorted': currentSortForColumn !== 0,
                    'pure-table-sorted-desc': currentSortForColumn < 0
                }
            )}
            title={title}
            aria-sort={ariaSort}
            rowSpan={1}
            scope="col"
            onClick={handleSort}
            onKeyDown={handleSort}>
            <div className="pure-table-sort-liner" tabIndex="0" unselectable="on">
                {column.label}
                <span className="pure-table-sort-indicator"></span>
            </div>
        </th>
    );
});

SortableColumn.propTypes = {
    index: PropTypes.number.isRequired,
    column: PropTypes.shape({
        key: PropTypes.string.isRequired,
        className: PropTypes.string,
        label: PropTypes.string,
        width: PropTypes.string,
        sortable: PropTypes.bool,
        formatter: PropTypes.func
    }),
    onSort: PropTypes.func.isRequired,
    sortBy: PropTypes.arrayOf(
        PropTypes.shape({
            order: PropTypes.oneOfType([PropTypes.oneOfType(['ASC', 'DESC']), PropTypes.number]),
            field: PropTypes.string
        })
    )
};

SortableColumn.defaultProps = {
    sortBy: []
};

const Column = memo(({ index, column }) => (
    <th
        key={index}
        className={classnames('pure-table-header', `pure-table-col-${column.key}`, column.className)}
        rowSpan={1}
        scope="col">
        {column.label}
    </th>
));

Column.propTypes = {
    index: PropTypes.number.isRequired,
    column: PropTypes.shape({
        key: PropTypes.string.isRequired,
        className: PropTypes.string,
        label: PropTypes.string,
        width: PropTypes.string,
        sortable: PropTypes.bool,
        formatter: PropTypes.func
    })
};

const Thead = memo(({ columns, summaryRowHeader, onSort, sortBy }) => {
    return (
        <thead className="pure-table-columns">
            <tr>
                {summaryRowHeader}
                {columns.map((column, i) => (
                    <React.Fragment key={i}>
                        {column.sortable ? (
                            <SortableColumn key={i} index={i} column={column} onSort={onSort} sortBy={sortBy} />
                        ) : (
                            <Column key={i} index={i} column={column} />
                        )}
                    </React.Fragment>
                ))}
            </tr>
        </thead>
    );
});

Thead.propTypes = {
    columns: PropTypes.arrayOf(
        PropTypes.shape({
            key: PropTypes.string.isRequired,
            className: PropTypes.string,
            label: PropTypes.string,
            width: PropTypes.string,
            sortable: PropTypes.bool,
            formatter: PropTypes.func
        })
    ).isRequired,
    summaryRowHeader: PropTypes.node,
    onSort: PropTypes.func,
    sortBy: PropTypes.arrayOf(
        PropTypes.shape({
            order: PropTypes.oneOfType([PropTypes.oneOfType(['ASC', 'DESC']), PropTypes.number]),
            field: PropTypes.string
        })
    )
};

export default Thead;
