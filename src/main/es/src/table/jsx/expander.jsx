'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Expander extends PureComponent {
    static propTypes = {
        expanded: PropTypes.bool,
        onExpand: PropTypes.func.isRequired,
        expandLabel: PropTypes.string.isRequired,
        collapseLabel: PropTypes.string.isRequired,
        index:PropTypes.number
    };

    handleClick = (e) => {
        const { expanded, onExpand, index } = this.props;
        e.preventDefault();

        //call onExpand callback with the inverse of expanded property
        //and the supplied index
        onExpand(!expanded, index);
    };
    render() {
        const { expanded, expandLabel, collapseLabel } = this.props;

        let title;
        if (expanded) {
            title = collapseLabel;
        } else {
            title = expandLabel;
        }

        return (
            <a href="#none" onClick={this.handleClick}>
                <i className={classnames('fa', {
                    'fa-chevron-circle-down': !expanded,
                    'icon-chevron-sign-down': !expanded,
                    'fa-chevron-circle-up': expanded,
                    'icon-chevron-sign-up': expanded
                })}
                    title={title} />
            </a>
        );
    }
}
