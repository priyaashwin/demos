'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Tbody = ({ children, className }) => (
    <tbody className={className}>
        {children}
    </tbody>
);

Tbody.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string
};

export default Tbody;