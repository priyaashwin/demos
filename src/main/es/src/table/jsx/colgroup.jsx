'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const SUMMARY_ROW_WIDTH = {
    width: '1%'
};

const Colgroup = memo(({ columns, hasSummaryRow }) => {
    let summaryRowCol;
    if (hasSummaryRow) {
        summaryRowCol = (<col className=" pure-table-col-collapse" style={SUMMARY_ROW_WIDTH} />);
    }
    return (
        <colgroup>
            {summaryRowCol}

            {columns.map((column, i) => (
                <col
                    key={i}
                    className={classnames(column.className, {
                        'pure-table-sorted': column.sortable
                    })}
                    style={{ width: column.width }}
                />
            ))}
        </colgroup>
    );
}
);

Colgroup.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string.isRequired,
        className: PropTypes.string,
        label: PropTypes.string,
        width: PropTypes.string,
        sortable: PropTypes.bool,
        formatter: PropTypes.func
    })).isRequired,
    hasSummaryRow: PropTypes.bool
};

export default Colgroup;
