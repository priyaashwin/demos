'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Form from '../../../webform/uspForm';
import DemographicDetails from './add/demographicDetails';
import PotentialDuplicateList from './duplicates/potentialDuplicateList';
import AddAdditional from './add/additional/additionalDetails';
import PersonDetail from './details/personDetail';
import AppDialog from '../../../dialog/dialog';
import Button from '../../../dialog/button';
import Errors from '../../../dialog/errors/errors';
import { validateSuppliedData } from '../js/validation';
import { requiresDuplicateCheck } from '../js/duplicateCheck';
import PaginatedResultsList from '../../../results/paginatedResultsList';
import endpoint from '../js/cfg';
import { Person, PersonDetailsForAdd, FuzzyDate } from '../js/person';
import Loading from '../../../loading/loading';
import { render as renderTemplate } from '../../../template/template';
import Model from '../../../model/model';
import { isSpecified, cleanValue } from '../../../utils/js/textUtils';
import genderMap from '../js/genderMap';
import { isUnborn } from '../../js/lifeState';
import {
    isNHSNumberRequired,
    isEthnicityRequired,
    isGenderRequired,
    isOrganisationRequired,
    isDueDateRequired,
    isDateOfBirthRequired,
    isDiedDateRequired,
    isProfessionalRelationshipRequired
} from '../js/options';

//page size is set to 50 - TODO - consider this?
const DUPLICATES_PAGE_SIZE = 50;

const DEFAULT_LIFE_STATE = 'ALIVE';

const getDefaultState = function(props) {
    return {
        step: 'DEMOGRAPHIC_DETAILS',
        errors: null,
        loading: false,
        duplicates: null,
        numDuplicates: 0,
        //always start with a fresh person model
        person: new PersonDetailsForAdd({
            autoRelatedPerson: new Person(props.currentUser)
        })
    };
};

const getValueForREST = function(value) {
    const v = cleanValue(value);
    if (isSpecified(v)) {
        return v;
    }
    return '';
};

export default class AddPersonDialog extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        dialogConfig: PropTypes.shape({
            header: PropTypes.object.isRequired,
            successMessage: PropTypes.string.isRequired,
            narrative: PropTypes.shape({
                demographics: PropTypes.shape({
                    summary: PropTypes.string.isRequired
                }),
                duplicateCheck: PropTypes.shape({
                    summary: PropTypes.string.isRequired
                }),
                additionalDetails: PropTypes.shape({
                    summary: PropTypes.string.isRequired
                })
            }).isRequired
        }).isRequired,
        codedEntries: PropTypes.shape({
            personTypeCategory: PropTypes.object.isRequired,
            titleCategory: PropTypes.object.isRequired,
            ethnicityCategory: PropTypes.object.isRequired,
            genderCategory: PropTypes.object.isRequired,
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        relationshipRoles: PropTypes.shape({
            professional: PropTypes.array.isRequired
        }).isRequired,
        endpoints: PropTypes.shape({
            duplicateSearchEndpoint: PropTypes.string.isRequired,
            relationshipAutocompleteEndpoint: PropTypes.string.isRequired,
            newPersonEndpoint: PropTypes.string.isRequired,
            teamAutocompleteEndpoint: PropTypes.string.isRequired
        }),
        currentUser: PropTypes.shape({
            id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
            personIdentifier: PropTypes.string.isRequired,
            name: PropTypes.string,
            forename: PropTypes.string,
            surname: PropTypes.string
        }).isRequired,
        isOpenAccess: PropTypes.bool
    };
    state = getDefaultState(this.props);

    componentDidCatch() {
        this.setState({
            errors: 'Unexpected error'
        });
    }
    handlePersonDataUpdate = (nameOrObject, value) => {
        const { person = Person() } = this.state;

        const { labels } = this.props;
        const { gender, forename } = person;

        let update;
        if (typeof nameOrObject === 'object') {
            update = nameOrObject;
        } else {
            update = {};
            update[nameOrObject] = value;
        }

        //perform pre-selection of controls
        if (isSpecified(update.title) && !(isSpecified(update.gender) || isSpecified(gender))) {
            //if the title maps to a gender - pre-select that value
            const gender = genderMap.get(update.title.toUpperCase());
            if (gender) {
                update.genderAuto = gender;
            }
        }
        if (isSpecified(update.personType)) {
            //person type changed -so reset life state to default
            update.lifeState = DEFAULT_LIFE_STATE;
        }
        if (isSpecified(update.personType)) {
            //clear fields after person type update
            if (!isNHSNumberRequired(update.personType, update.lifeState)) {
                update.nhsNumber = {};
                update.chiNumber = undefined;
            }

            if (!isOrganisationRequired(update.personType)) {
                update.professionalTitle = undefined;
                update.organisationName = undefined;
            }

            if (!isGenderRequired(update.personType)) {
                update.gender = undefined;
                update.genderAuto = undefined;
            }

            if (!isEthnicityRequired(update.personType)) {
                update.ethnicity = undefined;
                update.ethnicityAuto = undefined;
            }
        }

        if (isSpecified(update.lifeState) && isUnborn(update.lifeState)) {
            if (!(isSpecified(forename) || isSpecified(update.forename))) {
                //set the forname to the default for unborn
                update.forename = labels.unBornDefaultValue || 'Unborn';
            }
            //set auto-gender to unknown for unborn
            update.genderAuto = 'UNKNOWN';

            //set auto-ethnicity
            update.ethnicityAuto = 'NOT_KNOWN';
        } else if (isSpecified(update.lifeState) || nameOrObject === 'lifeState') {
            if (!isSpecified(update.lifeState)) {
                //change back to the default life state if clearing lifeState
                update.lifeState = DEFAULT_LIFE_STATE;
            }

            if (!isUnborn(update.lifeState)) {
                if (isSpecified(forename) && 'Unborn' === forename) {
                    //clear forename if it is set to Unborn and state is not same
                    update.forename = undefined;
                }
            }
        }

        if (isSpecified(update.personType) || isSpecified(update.lifeState)) {
            if (!isDueDateRequired(update.personType, update.lifeState)) {
                update.dueDate = undefined;
            }

            if (!isDateOfBirthRequired(update.personType, update.lifeState)) {
                update.dateOfBirth = undefined;
                update.dateOfBirthEstimated = undefined;
            }

            if (!isDiedDateRequired(update.personType, update.lifeState)) {
                update.diedDate = undefined;
                update.diedDateEstimated = undefined;
            }
        }

        if (isSpecified(update.personType)) {
            //personType is stored as an array on the person model
            update.personTypes = [update.personType];
            //remove the original property
            delete update.personType;
        }

        if (isSpecified(update.nhsNumber) || nameOrObject === 'nhsNumber') {
            //nhsNumber needs to be converted to a string
            const nhsNumber = update.nhsNumber;
            if (nhsNumber) {
                const nhsNum = [];
                nhsNum.push(nhsNumber.nhsFirstThreeDigits || '');
                nhsNum.push(nhsNumber.nhsSecondThreeDigits || '');
                nhsNum.push(nhsNumber.nhsLastFourDigits || '');
                //convert back to string
                update.nhsNumber = nhsNum.join('');
            }
        }

        //convert date of birth into Fuzzy date object
        if (isSpecified(update.dateOfBirth)) {
            update.dateOfBirth = new FuzzyDate(update.dateOfBirth);
        }

        //convert date of death into Fuzzy date object
        if (isSpecified(update.diedDate)) {
            update.diedDate = new FuzzyDate(update.diedDate);
        }

       if (isSpecified(update.skipAutoRelationship)) {
            update.teams = [];
       }

        this.setState({
            person: person.merge(update)
        });
    };
    loadPotentialDuplicates() {
        const { endpoints } = this.props;
        const { person } = this.state;

        const personData = person.getData();

        const ethnicity = personData.ethnicity ? [personData.ethnicity] : [];
        if (ethnicity.length) {
            if (ethnicity.indexOf('NOT_KNOWN') === -1) {
                //include NOT_KNOWN in the ethnicity list if ethnicity
                //has been supplied and does not already contain it.
                ethnicity.push('NOT_KNOWN');
            } else if (ethnicity.length === 1) {
                //remove ethnicity from criteria as is is NOT_KNOWN
                ethnicity.pop();
            }
        }
        const gender = personData.gender ? [personData.gender] : [];
        if (gender.length) {
            if (gender.indexOf('UNKNOWN') === -1) {
                //include UNKNOWN in the gender list
                gender.push('NOT_KNOWN');
            } else if (gender.length === 1) {
                //remove UNKNOWN from gender criteria
                gender.pop();
            }
        }

        const dateOfBirth = personData.dateOfBirth || {};

        //build list of parameters to pass to duplicate endpoint
        const params = {
            //only 1 person type is supported by add process
            personType: getValueForREST(personData.personTypes.get(0)),
            nhsNumber: getValueForREST(personData.nhsNumber),
            chiNumber: getValueForREST(personData.chiNumber),
            forename: getValueForREST(personData.forename),
            surname: getValueForREST(personData.surname),
            ethnicity: getValueForREST(ethnicity),
            gender: getValueForREST(gender),
            organisationName: getValueForREST(personData.organisationName),
            professionalTitle: getValueForREST(personData.professionalTitle),
            yearOfBirth: dateOfBirth.year !== undefined ? dateOfBirth.year : '',
            monthOfBirth: dateOfBirth.month !== undefined ? dateOfBirth.month : '',
            dayOfBirth: dateOfBirth.day !== undefined ? dateOfBirth.day : ''
        };

        //load the configurable types
        new PaginatedResultsList()
            .load(
                {
                    ...endpoint.duplicateSearchEndpoint,
                    params: {
                        ...params
                    },
                    url: endpoints.duplicateSearchEndpoint
                },
                DUPLICATES_PAGE_SIZE,
                1
            )
            .then(success => {
                let duplicates = null;
                let numDuplicates = 0;
                let step = 'DUPLICATE_CHECK';
                if (success.results.length === 0) {
                    //no duplicates
                    step = 'ADDITIONAL_DETAILS';
                } else {
                    duplicates = success.results;
                    numDuplicates = success.totalSize;
                }
                this.setState({
                    step: step,
                    loading: false,
                    duplicates: duplicates,
                    numDuplicates: numDuplicates
                });
            })
            .catch(reject =>
                this.setState({
                    loading: false,
                    errors: reject.messages || 'Unable to load',
                    duplicates: null,
                    numDuplicates: 0
                })
            );
    }

    savePerson() {
        const { endpoints, labels, isOpenAccess } = this.props;
        const { person } = this.state;

        //validate the data set
        const errors = validateSuppliedData(
            person,
            labels.validation || {},
            isOpenAccess,
            true,
            person.skipAutoRelationship
        );

        if (null === errors) {
            let modelType;
            // If the professional relationship is required and the user is not skipping
            if (isProfessionalRelationshipRequired(person.personTypes) && !person.skipAutoRelationship) {
               // Set the model type to NewPersonWithPersonPersonRelationship
                modelType = 'NewPersonWithPersonPersonRelationship';
            }  // If the person type is professional and the user is not skipping
              else if (isOrganisationRequired(person.personTypes) && !person.skipAutoRelationship) {
               // Set the model type to NewPersonWithTeamRelationship
                modelType = 'NewPersonWithTeamRelationship';
            } else {
               // Set the model type to NewPerson
                modelType = 'NewPerson';
            }

            this.setState(
                {
                    loading: true
                },
                () => {
                    const personModel = new Model(endpoints.newPersonEndpoint);

                    personModel
                        .save(endpoint.saveNewPersonEndpoint, {
                            _type: modelType,
                            ...person.getData()
                        })
                        .then(success => {
                            const dialog = this.dialog;
                            if (dialog) {
                                //fire success message - delay to next page turn
                                dialog.fireSuccessMessage(
                                    {},
                                    {
                                        delayed: true
                                    }
                                );
                                //fire saved
                                this.fireEvent('person:saved', {
                                    personId: success.id
                                });
                                //success
                                this.hideDialog();
                            }
                        })
                        .catch(reject =>
                            this.setState({
                                loading: false,
                                errors: reject.messages || 'Unable to save'
                            })
                        );
                }
            );
        } else {
            this.setState({
                loading: false,
                errors: errors
            });
        }
    }

    canProceed() {
        let canProceed;
        const { step, loading, errors } = this.state;
        switch (step) {
            case 'DUPLICATE_CHECK':
                canProceed = !loading && !errors;
                break;
            default:
                canProceed = !loading;
        }

        return canProceed;
    }
    canReturn() {
        let canReturn;
        const { step, loading } = this.state;
        switch (step) {
            case 'DUPLICATE_CHECK':
                canReturn = !loading;
                break;
            default:
                canReturn = !loading;
        }

        return canReturn;
    }
    getButtons() {
        const { step } = this.state;
        let activeButton = (
            <Button
                key="activeButton"
                type="active"
                disabled={!this.canReturn()}
                onClick={this.handlePrevious}
                title="previous"
                icon="fa-arrow-left">
                {' '}
                {'previous'}
            </Button>
        );

        const primaryButtonAttrs = {};

        switch (step) {
            case 'DEMOGRAPHIC_DETAILS':
                //can't go back from this view
                activeButton = undefined;
            //falls through
            case 'DUPLICATE_CHECK':
                primaryButtonAttrs.title = 'next';
                primaryButtonAttrs.icon = 'fa-arrow-right';
                primaryButtonAttrs.children = <> {'next'}</>;

                break;
            case 'ADDITIONAL_DETAILS':
                primaryButtonAttrs.title = 'save';
                primaryButtonAttrs.icon = 'fa-check';
                primaryButtonAttrs.children = <> {'save'}</>;
                break;
        }
        return (
            <>
                {activeButton}
                <Button
                    {...primaryButtonAttrs}
                    key="primaryButton"
                    type="primary"
                    disabled={!this.canProceed()}
                    onClick={this.handleNext}
                />
                <Button
                    key="secondaryButton"
                    type="secondary"
                    onClick={this.handleCancel}
                    title="cancel"
                    icon="fa-times">
                    {' '}
                    {'cancel'}
                </Button>
            </>
        );
    }
    handleNext = () => {
        if (!this.canProceed()) {
            //do nothing
            return;
        }
        const { labels } = this.props;
        const { step } = this.state;

        let errors = null;
        let nextStep = step;
        let loading = false;
        const person = this.state.person;

        switch (step) {
            case 'DEMOGRAPHIC_DETAILS': {
                //validate data and switch to searching view
                errors = validateSuppliedData(person, labels.validation || {});
                if (null === errors) {
                    //progress to next step - if duplicate check no required got to final step
                    nextStep = requiresDuplicateCheck(person.personTypes, person.lifeState)
                        ? 'DUPLICATE_CHECK'
                        : 'ADDITIONAL_DETAILS';
                    //set loading flag
                    loading = nextStep === 'DUPLICATE_CHECK';
                }
                break;
            }
            case 'DUPLICATE_CHECK': {
                //just move to next step
                nextStep = 'ADDITIONAL_DETAILS';
                break;
            }
            case 'ADDITIONAL_DETAILS': {
                //validate additional details
                if (null === errors) {
                    loading = true;
                }
                //then save
                break;
            }
        }

        this.setState(
            {
                step: nextStep,
                loading: loading,
                person: person,
                errors: errors
            },
            () => {
                switch (this.state.step) {
                    case 'DUPLICATE_CHECK':
                        this.loadPotentialDuplicates();
                        break;
                    case 'ADDITIONAL_DETAILS':
                        //if state is set to loading - then save!
                        if (this.state.loading) {
                            this.savePerson();
                        }
                        break;
                }
            }
        );
    };
    handlePrevious = () => {
        if (!this.canReturn()) {
            //do nothing
            return;
        }

        let errors;
        const { step } = this.state;

        switch (step) {
            case 'ADDITIONAL_DETAILS':
                //keep errors - but clone
                errors = this.state.errors ? { ...this.state.errors } : null;
                break;
            default:
                //clear errors
                errors = null;
                break;
        }

        //return to first step
        this.setState({
            step: 'DEMOGRAPHIC_DETAILS',
            loading: false,
            duplicates: null,
            numDuplicates: 0,
            errors: errors
        });
    };
    handleCancel = () => {
        this.hideDialog();
    };
    clearErrors = () => {
        this.setState({
            errors: null
        });
    };
    handlePersonView = person => {
        this.fireEvent('duplicatePerson:view', {
            personId: person.id
        });
        //close the dialog
        this.hideDialog();
    };
    fireEvent(eventType, payload) {
        //create custom event and fire
        const event = new CustomEvent(eventType, {
            bubbles: true,
            cancelable: true,
            detail: payload
        });
        ReactDOM.findDOMNode(this).dispatchEvent(event);
    }
    dialogRef = ref => (this.dialog = ref);
    getDialogTarget = () => ReactDOM.findDOMNode(this.dialog);
    getDemographicDetailsView() {
        const { person } = this.state;
        return (
            <Form id="addPerson">
                <DemographicDetails
                    {...this.props}
                    key="demographics"
                    ref={this.demographicDetailsRef}
                    onUpdate={this.handlePersonDataUpdate}
                    person={person}
                />
            </Form>
        );
    }
    getDuplicateDetailsView() {
        const { labels, codedEntries, ...other } = this.props;
        const { person, duplicates } = this.state;

        return (
            <div className="readonly pure-g">
                <div className="pure-u-1 l-box">
                    <PersonDetail
                        {...other}
                        key="detailsDisplay"
                        className="person-details inner-panel"
                        labels={labels}
                        person={person}
                        codedEntries={codedEntries}
                    />
                </div>
                <div className="pure-u-1">
                    <hr className="rule-dashed" />
                </div>
                <div className="pure-u-1 l-box">
                    <PotentialDuplicateList
                        {...other}
                        key="duplicatesList"
                        duplicates={duplicates}
                        labels={labels}
                        codedEntries={codedEntries}
                        onViewPerson={this.handlePersonView}
                    />
                </div>
            </div>
        );
    }
    getAdditionalPersonDetailsView() {
        const { labels, endpoints, codedEntries, relationshipRoles, isOpenAccess, ...other } = this.props;
        const { person } = this.state;
        return (
            <div className="readonly pure-g">
                <div className="pure-u-1 l-box">
                    <PersonDetail
                        {...other}
                        key="detailsDisplay"
                        className="person-details inner-panel"
                        labels={labels}
                        person={person}
                        codedEntries={codedEntries}
                    />
                </div>
                <div className="pure-u-1">
                    <hr className="rule-dashed" />
                </div>
                <div className="pure-u-1">
                    <Form id="addPerson">
                        <AddAdditional
                            labels={labels}
                            person={person}
                            onUpdate={this.handlePersonDataUpdate}
                            endpoints={endpoints}
                            relationshipRoles={relationshipRoles}
                            codedEntries={codedEntries}
                            isOpenAccess={isOpenAccess}
                        />
                    </Form>
                </div>
            </div>
        );
    }
    getDialogView() {
        const { step, errors } = this.state;
        let content;
        switch (step) {
            case 'DEMOGRAPHIC_DETAILS':
                content = this.getDemographicDetailsView();
                break;
            case 'DUPLICATE_CHECK':
                content = this.getDuplicateDetailsView();
                break;
            case 'ADDITIONAL_DETAILS':
                content = this.getAdditionalPersonDetailsView();
                break;
        }

        let errorsContent;
        if (null !== errors) {
            errorsContent = (
                <Errors key="errors-panel" errors={errors} onClose={this.clearErrors} target={this.getDialogTarget} />
            );
        }

        return (
            <>
                {errorsContent}
                {content}
            </>
        );
    }
    getDuplicateCheckNarrative(narrative) {
        const { loading, numDuplicates } = this.state;
        const { labels } = this.props;
        let description;

        if (true === loading) {
            description = <Loading key="loading" message={labels.duplicateLoading} />;
        } else if (numDuplicates) {
            let pageSizeExceeded;

            if (numDuplicates > DUPLICATES_PAGE_SIZE) {
                pageSizeExceeded = <span>{` (showing first ${DUPLICATES_PAGE_SIZE} records)`}</span>;
            }
            description = (
                <div key="count">
                    {renderTemplate(labels.duplicatesFound, {
                        count: numDuplicates
                    })}
                    {pageSizeExceeded}
                </div>
            );
        }

        return {
            ...narrative,
            description: description
        };
    }
    getNarrative() {
        const { dialogConfig } = this.props;
        const { step } = this.state;
        let narrative = {};

        switch (step) {
            case 'DUPLICATE_CHECK':
                narrative = this.getDuplicateCheckNarrative(dialogConfig.narrative.duplicateCheck);
                break;
            case 'ADDITIONAL_DETAILS':
                narrative = dialogConfig.narrative.additionalDetails;
                break;
            case 'DEMOGRAPHIC_DETAILS':
            //falls through
            default:
                narrative = dialogConfig.narrative.demographics;
                break;
        }

        return narrative;
    }
    showDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and show
            this.setState(getDefaultState(this.props), () => {
                dialog.showDialog();
            });
        }
    }
    hideDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and hide
            dialog.hideDialog();
            this.setState(getDefaultState(this.props));
        }
    }
    render() {
        const { dialogConfig } = this.props;
        const { loading } = this.state;

        return (
            <AppDialog
                key="addDialog"
                ref={this.dialogRef}
                header={dialogConfig.header}
                narrative={this.getNarrative()}
                successMessage={dialogConfig.successMessage}
                buttons={this.getButtons()}
                defaultOpen={false}
                bsSize="large"
                busy={loading}>
                {this.getDialogView()}
            </AppDialog>
        );
    }
}
