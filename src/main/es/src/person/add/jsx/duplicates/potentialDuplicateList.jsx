'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { Card, CardList, CardHeader, CardContent } from 'usp-card';
import ExtendedPersonDetails from '../details/extendedPersonDetail';
import memoize from 'memoize-one';
import {Person} from '../../js/person';
import PersonDetail from '../details/personDetail';
import InlineMenu from 'usp-inline-menu';

const transitionTime = { enter: 500, exit: 300 };

export default class PotentialDuplicateList extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        duplicates: PropTypes.array,
        codedEntries: PropTypes.shape({
            personTypeCategory: PropTypes.object.isRequired,
            genderCategory: PropTypes.object.isRequired,
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        onViewPerson: PropTypes.func.isRequired
    }
    getActions(){
        return [
            {
                id: 'view',
                title: 'View this person',
                label: 'Go to record',
                visible: true
            }
        ];
    }
    getCardMenu=(duplicate)=>{
        const handleMenuItemClick=(action=>{
            switch(action){
                case 'view':
                    this.props.onViewPerson(duplicate);
                break;
            }
        });
            
      return (
          <InlineMenu 
          key={duplicate.id} 
          options={this.getActions(duplicate)} 
          menuIcon={(<i className="fa fa-ellipsis-v" aria-hidden="true"/>)} 
          vertical={true} 
          onMenuItemClick={handleMenuItemClick}/>
      );
    };
    getDuplicates = memoize(duplicates => (duplicates || []).map(duplicate => new Person(duplicate)));
    renderDuplicates() {
        const { duplicates, labels, codedEntries } = this.props;
        return (
            <CardList className="duplicates-list">
                <TransitionGroup>
                    {
                        this.getDuplicates(duplicates).map((duplicate, i) => (
                            <CSSTransition key={i} classNames="dup-fade"
                                timeout={transitionTime}>
                                <Card key={i}>
                                    <CardHeader menu={this.getCardMenu(duplicate)}/>
                                    <CardContent content={
                                        <>
                                            <PersonDetail
                                                key={`dup${i}`}
                                                person={duplicate}
                                                labels={labels}
                                                codedEntries={codedEntries}
                                            />                                            
                                            <ExtendedPersonDetails
                                                key={`dupex${i}`}
                                                person={duplicate}
                                                labels={labels}
                                            />
                                        </>
                                    } />
                                </Card>
                            </CSSTransition>
                        ))}
                </TransitionGroup>
            </CardList>
        );
    }
    render() {

        return (
            <div>
                {this.renderDuplicates()}
            </div>
        );
    }
}