'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InputNoLabel from '../../../../webform/raw/input';
const numberPattern = /^[0-9]{1,4}$|^$/;
const DASH = (<span className="dash">{'-'}</span>);

export default class NHSNumber extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        onUpdate: PropTypes.func.isRequired,
        labels: PropTypes.object.isRequired,
        nhsNumber: PropTypes.shape({
            nhsFirstThreeDigits: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            nhsSecondThreeDigits: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            nhsLastFourDigits: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        }).isRequired
    };
    handleUpdate = (name, value) => {
        const { nhsNumber, onUpdate } = this.props;

        const update = {
            ...nhsNumber
        };

        let cleanValue = value || '';

        // Keep trimming the just a valid digit
        while (!numberPattern.test(cleanValue)) {
            cleanValue = cleanValue.slice(0, -1);
        }

        update[name] = cleanValue;

        onUpdate('nhsNumber', update);
    };
    handleFirstThreeKeyUp = (e) => {
        if ((e.currentTarget.value || '').length === 3) {
            switch (e.key) {
                case 'Backspace':
                case 'Delete':
                    break;
                default: {
                    if (this.secondInput) {
                        this.secondInput.focus();
                    }
                    break;
                }
            }

        }
    };
    handleSecondThreeKeyUp = (e) => {
        if ((e.currentTarget.value || '').length === 3) {
            switch (e.key) {
                case 'Backspace':
                case 'Delete':
                    break;
                default: {
                    if (this.thirdInput) {
                        this.thirdInput.focus();
                    }
                    break;
                }
            }
        }
    };
    secondInputRef = (ref) => this.secondInput = ref;
    thirdInputRef = (ref) => this.thirdInput = ref;
    render() {
        const { labels, nhsNumber = '', id } = this.props;
        return (
            <fieldset tabIndex={0} id={id} className="pure-fieldset">
                <div role="group" id={id} className="nhs-input-fieldset" >
                    <InputNoLabel
                        key="nhsFirstThreeDigits"
                        id="addPerson_nhsFirstThreeDigits"
                        name="nhsFirstThreeDigits"
                        value={nhsNumber.nhsFirstThreeDigits}
                        onUpdate={this.handleUpdate}
                        maxLength={3}
                        size={3}
                        placeholder={labels.nhsNumberFirstPlaceholder}
                        className="nhs-input"
                        onKeyUp={this.handleFirstThreeKeyUp}
                    />
                    {DASH}
                    <InputNoLabel
                        key="nhsSecondThreeDigits"
                        id="addPerson_nhsSecondThreeDigits"
                        name="nhsSecondThreeDigits"
                        value={nhsNumber.nhsSecondThreeDigits}
                        onUpdate={this.handleUpdate}
                        maxLength={3}
                        size={3}
                        placeholder={labels.nhsNumberSecondPlaceholder}
                        className="nhs-input"
                        disabled={(nhsNumber.nhsFirstThreeDigits || '').length < 3}
                        onKeyUp={this.handleSecondThreeKeyUp}
                        ref={this.secondInputRef}
                    />
                    {DASH}
                    <InputNoLabel
                        key="nhsLastFourDigits"
                        id="addPerson_nhsLastFourDigits"
                        name="nhsLastFourDigits"
                        value={nhsNumber.nhsLastFourDigits}
                        onUpdate={this.handleUpdate}
                        maxLength={4}
                        size={4}
                        placeholder={labels.nhsNumberThirdPlaceholder}
                        className="nhs-input"
                        disabled={(nhsNumber.nhsFirstThreeDigits || '').length < 3 || (nhsNumber.nhsSecondThreeDigits || '').length < 3}
                        ref={this.thirdInputRef}
                    />
                </div>
            </fieldset>
        );
    }
}
