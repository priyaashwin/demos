'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '../../../../webform/uspCheckbox';

const UNBORN='UNBORN';
const DECEASED='DECEASED';

export default class LifeState extends PureComponent {
    static propTypes = {
        lifeState: PropTypes.string,
        personType: PropTypes.string,
        onUpdate: PropTypes.func.isRequired,
        labels: PropTypes.object.isRequired
    };
    renderUnbornOption(){
        const { lifeState, labels, onUpdate} = this.props;

        return (<Checkbox
            id="lifeState"
            key="lifeState"
            name="lifeState"
            label={labels.unborn}
            value={UNBORN}
            checked={UNBORN===lifeState}
            onUpdate={onUpdate} />);
    }
    renderDeceasedOption(){
        const { lifeState, labels, onUpdate} = this.props;
        return (<Checkbox
            id="lifeState"
            key="lifeState"
            name="lifeState"
            label={labels.deceased}
            value={DECEASED}
            checked={DECEASED===lifeState}
            onUpdate={onUpdate} />);
    }
    render() {
        const { personType } = this.props;

        let lifeStateContent;

        switch(personType){
            case 'CLIENT':
                lifeStateContent=this.renderUnbornOption();
            break;
            case 'OTHER':
                lifeStateContent=this.renderDeceasedOption();
            break;
        }
        return (
            <>
                {lifeStateContent}
            </>
        );
    }
}
