'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Radio from '../../../../webform/uspRadio';

export default class Gender extends PureComponent {
    static propTypes = {
        onUpdate: PropTypes.func.isRequired,
        gender: PropTypes.string,
    };
    render() {
        const { gender, onUpdate } = this.props;
        return (
            <fieldset key="gender" id="addPerson_gender" className="pure-fieldset">
                <div className="fieldset-wrap pure-g-r">
                    <div className="pure-u-1-4">
                        <Radio name="gender" id="male-input"
                            label="Male" value="MALE" checked={gender === 'MALE'}
                            onUpdate={onUpdate} />
                    </div>
                    <div className="pure-u-1-4">
                        <Radio name="gender" id="female-input"
                            label="Female" value="FEMALE" checked={gender === 'FEMALE'}
                            onUpdate={onUpdate} />
                    </div>
                    <div className="pure-u-1-4">
                        <Radio name="gender" id="indeterminate-input"
                            label="Indeterminate" value="INDETERMINATE" checked={gender === 'INDETERMINATE'}
                            onUpdate={onUpdate} />
                    </div>
                    <div className="pure-u-1-4">
                        <Radio name="gender" id="unknown-input"
                            label="Unknown" value="UNKNOWN" checked={gender === 'UNKNOWN'}
                            onUpdate={onUpdate} />
                    </div>
                </div>
            </fieldset>
        );
    }
}
