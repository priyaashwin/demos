'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InputNoLabel from '../../../../webform/raw/input';
const numberPattern = /^[0-9]{1,10}$|^$/;

export default class CHINumber extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        onUpdate: PropTypes.func.isRequired,
        chiNumber: PropTypes.string
    };
    handleUpdate = (name, value) => {
        const { onUpdate } = this.props;

        let cleanValue = value || '';

        // Keep trimming the just a valid digit
        while (!numberPattern.test(cleanValue)) {
            cleanValue = cleanValue.slice(0, -1);
        }
        onUpdate(name, cleanValue);
    };    
    render() {
        const { chiNumber, id } = this.props;
        return (
            <InputNoLabel
                key="chiNumber"
                id={id}
                name="chiNumber"
                value={chiNumber}
                onUpdate={this.handleUpdate}
                maxLength={10}
                placeholder={'1111111111'}
            />
        );
    }
}
