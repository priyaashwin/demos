'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isSpecified } from '../../../../utils/js/textUtils';
import { isCHIGenderValid, isCHIDateOfBirthValid } from '../../../../chiNumber/chiNumber';
import { isValidateNHSNumber } from '../../../../nhsNumber/nhsNumber';
import Message from '../../../../message/jsx/message';

export default class NumberValidation extends PureComponent {
    static propTypes = {
        nhsNumber: PropTypes.shape({
            nhsFirstThreeDigits: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            nhsSecondThreeDigits: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            nhsLastFourDigits: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        }),
        chiNumber: PropTypes.string,
        gender: PropTypes.string,
        estimatedDateOfBirth: PropTypes.bool,
        dateOfBirth: PropTypes.shape({
            year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        }),
        labels: PropTypes.object.isRequired
    };
    isGenderValid() {
        const { chiNumber, gender } = this.props;

        let isValid = true;

        if (isSpecified(chiNumber) && chiNumber.length === 10) {
            isValid = isCHIGenderValid(chiNumber, gender);
        }

        return isValid;
    }
    isDateOfBirthEstimatedValid() {
        const { chiNumber, estimatedDateOfBirth } = this.props;

        let isValid = true;
        if (estimatedDateOfBirth === true && isSpecified(chiNumber) && chiNumber.length === 10) {
            //can't have an estimated DOB and CHI number
            isValid = false;
        }
        return isValid;
    }
    isDateOfBirthValid() {
        const { chiNumber, dateOfBirth } = this.props;

        let isValid = true;
        if (isSpecified(chiNumber) && chiNumber.length === 10 && isSpecified(dateOfBirth) && isSpecified(dateOfBirth.year)) {
            isValid = isCHIDateOfBirthValid(chiNumber, dateOfBirth.year, dateOfBirth.month, dateOfBirth.day);
        }

        return isValid;
    }
    isNHSNumberValid() {
        const { nhsNumber } = this.props;

        let isValid = true;
        if (isSpecified(nhsNumber) && isSpecified(nhsNumber.nhsFirstThreeDigits)
            && isSpecified(nhsNumber.nhsSecondThreeDigits)
            && isSpecified(nhsNumber.nhsLastFourDigits)) {
            const nhsNum = [];
            nhsNum.push(nhsNumber.nhsFirstThreeDigits || '');
            nhsNum.push(nhsNumber.nhsSecondThreeDigits || '');
            nhsNum.push(nhsNumber.nhsLastFourDigits || '');

            const parsedNum = (nhsNum.join(''));
            if (parsedNum.length === 10) {
                isValid = isValidateNHSNumber(parsedNum);
            }
        }

        return isValid;
    }
    render() {
        const { labels } = this.props;

        const messages = [];
        if (!this.isGenderValid()) {
            messages.push(labels.chiGenderDigitInvalid);
        }
        if (!this.isDateOfBirthEstimatedValid()) {
            messages.push(labels.chiEstimatedDateOfBirth);
        } else {
            if (!this.isDateOfBirthValid()) {
                messages.push(labels.chiDateOfBirthInvalid);
            }
        }
        if (!this.isNHSNumberValid()) {
            messages.push(labels.nhsNumberInvalid);
        }

        if (messages.length > 0) {
            return (
                <Message key="message">
                    <div className="pure-alert-small">
                        <span className="info-icon"><i className="fa fa-info-circle" aria-hidden={true} /></span>
                        <ul>
                            {messages.map((message, i) => (
                                <li key={i}>
                                    <span>{message}</span>
                                </li>
                            ))}
                        </ul>
                    </div>
                </Message>
            );
        }

        return false;
    }
}
