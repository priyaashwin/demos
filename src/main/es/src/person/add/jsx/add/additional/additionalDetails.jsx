'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Person, PersonDetailsForAdd } from '../../../js/person';
import { isProfessionalRelationshipRequired } from '../../../js/options';
import RelationshipSelector from './relationshipSelector';
import Select from '../../../../../webform/uspSelect';
import Label from '../../../../../webform/uspLabel';
import Checkbox from '../../../../../webform/uspCheckbox';
import memoize from 'memoize-one';
import Message from '../../../../../message/jsx/message';
import TeamSelector from './teamSelector';

export default class AddAdditional extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        person: PropTypes.oneOfType([
            PropTypes.instanceOf(Person),
            PropTypes.instanceOf(PersonDetailsForAdd)
        ]).isRequired,
        onUpdate: PropTypes.func.isRequired,
        codedEntries: PropTypes.shape({
            genderCategory: PropTypes.object.isRequired
        }).isRequired,
        relationshipRoles: PropTypes.shape({
            professional: PropTypes.array.isRequired
        }).isRequired,
        isOpenAccess: PropTypes.bool,
        endpoints: PropTypes.object.isRequired
    }

    getRelationshipRoles = memoize(relationshipRoles => (relationshipRoles.filter(role=>!role.notManuallyAssignable).map(role => ({
        text: role.name,
        value: role.id.includes('_A') ? role.id.replace('_A', '') : role.id
    }))));
    handleUpdate = (name, value) => {
        this.props.onUpdate(name, value);
    }
    isRelationshipMandatory() {
        const { isOpenAccess = false } = this.props;
        return !isOpenAccess;
    }
    renderOpenAccessMessage() {
        const { labels, person } = this.props;
        const { skipAutoRelationship, personTypes  } = person;
        if (!this.isRelationshipMandatory()) {
            const related = isProfessionalRelationshipRequired(personTypes) ? 'relatedPerson' : 'relatedTeam';
            return (
                <>
                    <div key="message" className="pure-u-1-2 l-box readonly">
                        {this.renderMessage(labels[related].openAccessWarning)}
                    </div>
                    <div className="pure-u-1-2 l-box">
                        <Checkbox
                            id="skipAutoRelationship"
                            key="skipAutoRelationship"
                            name="skipAutoRelationship"
                            label={labels[related].skipAutoRelationship}
                            value={true}
                            checked={!!skipAutoRelationship}
                            onUpdate={this.updateSkipAutoRelationship} />
                    </div>
                </>
            );
        }
        return false;
    }
    updateSkipAutoRelationship = (name, value) => {
        this.props.onUpdate({
            skipAutoRelationship: value
        });
    };
    renderRelationshipSelector() {
        const { endpoints, labels, codedEntries, relationshipRoles, person } = this.props;
        const { autoRelationshipTypeId, autoRelatedPerson, skipAutoRelationship } = person;

        const roles = this.getRelationshipRoles(relationshipRoles.professional);

        const isMandatory = this.isRelationshipMandatory();


        let relationshipSelector;

        if (!skipAutoRelationship) {
            relationshipSelector = (
                <>
                    <div className="pure-u-1-2 l-box">
                        <Label forId="autoRelatedPersonId"
                            label={labels.relatedPerson.autoRelatedPersonId}
                            mandatory={isMandatory}
                        />
                        <RelationshipSelector
                            inputId="addPerson_autoRelatedPersonId"
                            name="autoRelatedPersonId"
                            key="autoRelatedPersonId"
                            url={endpoints.relationshipAutocompleteEndpoint}
                            labels={labels}
                            codedEntries={codedEntries}
                            onUpdate={this.handleUpdate}
                            selectedValue={autoRelatedPerson}
                            className="pure-input-1" />
                    </div>
                    <div className="pure-u-1-2 l-box">
                        <Select
                            key="autoRelationshipTypeId"
                            id="addPerson_autoRelationshipTypeId"
                            name="autoRelationshipTypeId"
                            options={roles}
                            value={autoRelationshipTypeId}
                            onUpdate={this.handleUpdate}
                            label={labels.relatedPerson.autoRelationshipTypeId}
                            mandatory={isMandatory}
                            includeEmptyOption
                        />
                    </div>
                </>
            );
        }

        return (
            <div className="pure-g-r">
                {relationshipSelector}
                {this.renderOpenAccessMessage()}
            </div>
        );
    }
    renderTeamSelector() {
        const { endpoints, labels, person } = this.props;
        const { skipAutoRelationship, teams } = person;
        const isMandatory = this.isRelationshipMandatory();
        let teamSelector;
        if (!skipAutoRelationship) {
            teamSelector = (
                <div className="pure-u-1-2 l-box">
                    <Label forId="autoRelatedTeamId"
                        label={labels.relatedTeam.autoRelatedTeamId}
                        mandatory={isMandatory} />
                    <TeamSelector
                        inputId="autoRelatedTeamId"
                        name="autoRelatedTeamId"
                        key="autoRelatedTeamId"
                        url={endpoints.teamAutocompleteEndpoint}
                        labels={labels}
                        onUpdate={this.handleUpdate}
                        selectedValue={teams}
                        className="pure-input-1" />
                </div>
            );
        }

        return (
            <div className="pure-g-r">
                {teamSelector}
                {this.renderOpenAccessMessage()}
            </div>
        );
    }
    renderMessage(message) {
        return (
            <Message>
                <div className="pure-alert-small">
                    <span className="info-icon"><i className="fa fa-info-circle" aria-hidden={true} /></span>
                    {message}
                </div>
            </Message>
        );
    }
    render() {
        const { person } = this.props;
        const { personTypes } = person;
        let content;

        if (!isProfessionalRelationshipRequired(personTypes)) {
           content = this.renderTeamSelector();
        } else {
            content = this.renderRelationshipSelector();
        }

        return (
            <>
                {content}
            </>
        );
    }
}