'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import FuzzyDateInput from '../../../../webform/uspFuzzyDateInput';
import Estimated from './estimated';
import { DialogManagerContext } from '../../../../dialog/dialog';

export default class DiedDate extends PureComponent {
    static propTypes = {
        id: PropTypes.string,
        onUpdate: PropTypes.func.isRequired,
        labels: PropTypes.object.isRequired,
        diedDateEstimated: PropTypes.bool,
        diedDate: PropTypes.shape({
            year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        })
    }

    updateDiedDate = (diedDate) => {
        const fuzzyDate = diedDate || {};

        const update = {
            diedDate: fuzzyDate
        };

        //clear the estimated attribute if fuzzy - (this is the user set attribute)
        if (fuzzyDate.day === undefined) {
            update.diedDateEstimated = undefined;
        }

        this.props.onUpdate(update);
    };

    render() {
        const { id, diedDate = {}, diedDateEstimated, labels, onUpdate } = this.props;
        return (
            <>
                <div className="pure-g-r">
                    <div className="pure-u-3-4">
                        <DialogManagerContext.Consumer>
                            {dialogManager => (
                                <FuzzyDateInput
                                    id={id}
                                    fuzzyDate={diedDate}
                                    onUpdate={this.updateDiedDate}
                                    dialogManager={dialogManager} />
                            )}
                        </DialogManagerContext.Consumer>
                    </div>
                </div>
                <Estimated
                    name="diedDateEstimated"
                    isEstimated={diedDateEstimated}
                    message={labels.diedDateEstimatedMessage}
                    label={labels.diedDateEstimated}
                    onUpdate={onUpdate}
                    fuzzyDate={diedDate}
                />
            </>
        );
    }
}

