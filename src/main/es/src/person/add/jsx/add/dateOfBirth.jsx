'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from '../../../../webform/uspInput';
import FuzzyDateInput from '../../../../webform/uspFuzzyDateInput';
const numberPattern = /^[0-9]{1,3}$|^$/;
import { isSpecified } from '../../../../utils/js/textUtils';
import { getMoment, getAge } from '../../js/dob';
import Estimated from './estimated';
import { DialogManagerContext } from '../../../../dialog/dialog';

// Regex returns true if number between 0-999 or an empty string
const checkAge = function (age) {
    return numberPattern.test(age);
};

export default class DateOfBirth extends PureComponent {
    static propTypes = {
        id: PropTypes.string,
        onUpdate: PropTypes.func.isRequired,
        labels: PropTypes.object.isRequired,
        dateOfBirthEstimated: PropTypes.bool,
        dateOfBirth: PropTypes.shape({
            year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        }),
        isDeceased: PropTypes.bool,
        diedDate: PropTypes.shape({
            year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        })
    }

    updateDateOfBirth = (dateOfBirth) => {
        const fuzzyDate = dateOfBirth || {};

        const update = {
            dateOfBirth: fuzzyDate
        };

        //clear the estimated attribute if fuzzy - (this is the user set attribute)
        if (fuzzyDate.day === undefined) {
            update.dateOfBirthEstimated = undefined;
        }

        this.props.onUpdate(update);
    };
    handleAgeUpdate = (nameIgnored, value) => {
        const { isDeceased } = this.props;
        if (isDeceased) {
            //can't update the age if deceased
            return;
        }

        let age = value;

        let year;

        // Keep trimming the age until a valid digit is found or there is nothing left to slice
        while (!checkAge(age)) {
            age = age.slice(0, -1);
        }

        if (age) {
            const birthday = moment().tz('Europe/London').subtract(1, 'Years').endOf('year').subtract(Number(age || ''), 'years');
            year = birthday.get('year');
        }

        //call into update hander - reset everything to new year
        this.updateDateOfBirth({
            year: year,
            month: undefined,
            day: undefined
        });
    };
    render() {
        const { id, dateOfBirth = {}, dateOfBirthEstimated, labels, onUpdate, isDeceased, diedDate } = this.props;
        const dobMoment = getMoment(dateOfBirth);
        const deathMoment = getMoment(diedDate);
        const age = getAge(dobMoment, deathMoment);

        const ageContent = (
            <Input
                key="age"
                id="age"
                maxLength={3}
                label={isDeceased ? labels.ageAtDeath : labels.age}
                value={age}
                onUpdate={this.handleAgeUpdate}
                disabled={isDeceased ? true : (isSpecified(dateOfBirth.year) && isSpecified(dateOfBirth.month) && isSpecified(dateOfBirth.day))} />
        );
        return (
            <>
                <div className="pure-g-r">
                    <div className="pure-u-3-4">
                        <DialogManagerContext.Consumer>
                            {dialogManager => (
                                <FuzzyDateInput
                                    id={id}
                                    fuzzyDate={dateOfBirth}
                                    onUpdate={this.updateDateOfBirth}
                                    dialogManager={dialogManager} />
                            )}
                        </DialogManagerContext.Consumer>
                    </div>
                    <div className="pure-u-1-4 l-box">
                        {ageContent}
                    </div>
                </div>
                <Estimated
                    name="dateOfBirthEstimated"
                    isEstimated={dateOfBirthEstimated}
                    message={labels.dateOfBirthEstimatedMessage}
                    label={labels.dateOfBirthEstimated}
                    onUpdate={onUpdate}
                    fuzzyDate={dateOfBirth}
                />
            </>
        );
    }
}

