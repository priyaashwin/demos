'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ProfessionalPersonAutocomplete from '../../../../../autocomplete/jsx/professionalRolePersonAutoComplete';
import endpoint from '../../../js/cfg';
import memoize from 'memoize-one';
import { Person } from '../../../js/person';

export default class RelationshipSelector extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        name:PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        codedEntries: PropTypes.shape({
            genderCategory: PropTypes.object.isRequired
        }),
        onUpdate: PropTypes.func.isRequired,
    };

    handleSelection = (selectedValue) => {
        const {onUpdate}=this.props;
        //update autoRelatedPerson object - not ID - convert to Person record
        onUpdate('autoRelatedPerson', selectedValue?new Person(selectedValue):undefined);
    };
    getEndpoint = memoize((endpoint, url) => ({ ...endpoint, url: url }));
    render() {
        const { url, labels, codedEntries, onUpdate:onUpdateIgnored, ...other} = this.props;

        return (<div>
            <ProfessionalPersonAutocomplete
                {...other}
                placeholder={labels.relatedPerson.placeholder}
                codedEntries={codedEntries}
                onSelection={this.handleSelection}
                endpoint={this.getEndpoint(endpoint.relationshipAutoCompleteEndpoint, url)}
                queryParameter="nameOrIdentifier"
            />
        </div>);
    }
}