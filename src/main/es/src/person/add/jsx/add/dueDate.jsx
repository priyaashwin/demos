'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DateInput from '../../../../webform/uspDateInput';
import { parseDate, formatDate, DATE_STRING_REG } from '../../../../date/date';
import Message from '../../../../message/jsx/message';

const MAX_DAYS_FUTURE = 280;

const minDate = moment().startOf('day');
const maxDate = moment().add(MAX_DAYS_FUTURE, 'day').endOf('day');

export default class DueDate extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        labels: PropTypes.object.isRequired,
        onUpdate: PropTypes.func.isRequired,
        dueDate: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number])
    }
    state = {
        valid: true
    }
    handleUpdate = (name, value) => {
        const { onUpdate } = this.props;
        let updateValue = (value || '').trim();
        let valid = true;
        if (updateValue && DATE_STRING_REG.test(updateValue)) {
            const d = parseDate(updateValue);
            if (null !== d) {
                const days = d.diff(minDate, 'days');
                //validate
                if (days < 0 || days > MAX_DAYS_FUTURE) {
                    valid = false;
                }
                //re-format
                updateValue = formatDate(d, true);
            }
        }
        this.setState({
            valid: valid
        });

        onUpdate(name, updateValue);
    }
    renderValidationMessage() {
        const { valid } = this.state;
        const { labels } = this.props;
        if (valid) {
            //no message to render
            return false;
        }
        const messageContent = (
            <div className="pure-alert-small">
                <span className="info-icon"><i className="fa fa-info-circle" aria-hidden={true} /></span>
                <ul>
                    <li>
                        <span>{labels.dueDateInvalid}</span>
                    </li>
                </ul>
            </div>
        );
        return (
            <Message key="message">
                {messageContent}
            </Message>
        );
    }
    render() {
        const { id, dueDate } = this.props;
        return (
            <>
                <DateInput
                    id={id}
                    key="dueDate"
                    name="dueDate"
                    onUpdate={this.handleUpdate}
                    minimumDate={minDate.valueOf()}
                    maximumDate={maxDate.valueOf()}
                    value={dueDate}
                    placeholder="DD-MMM-YYYY"
                />
                {this.renderValidationMessage()}
            </>
        );
    }
}

