'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CodedEntrySelect from '../../../../webform/uspSelectForCodedEntry';
import CodedEntrySelectNoLabel from '../../../../webform/raw/codedEntrySelect';
import Input from '../../../../webform/uspInput';
import DateOfBirth from './dateOfBirth';
import DiedDate from './diedDate';
import DueDate from './dueDate';
import LifeState from './lifeState';
import Label from '../../../../webform/uspLabel';
import Gender from './gender';
import NHSNumber from './nhsNumber';
import CHINumber from './chiNumber';
import NumberValidation from './numberValidation';
import { isClient } from '../../../js/personType';

import { Person, PersonDetailsForAdd } from '../../js/person';
import {
    isNHSNumberRequired,
    isEthnicityRequired,
    isGenderRequired,
    isOrganisationRequired,
    isDueDateRequired,
    isDateOfBirthRequired,
    isDiedDateRequired
} from '../../js/options';

import { isDeceased, isAlive } from '../../../js/lifeState';

const getNHSNumber = function (nhsNumber) {
    const nhsNumObj = {};
    if (nhsNumber) {
        nhsNumObj.nhsFirstThreeDigits = nhsNumber.substring(0, 3);
        nhsNumObj.nhsSecondThreeDigits = nhsNumber.substring(3, 6);
        nhsNumObj.nhsLastFourDigits = nhsNumber.substring(6, 10);
    }

    return nhsNumObj;
};

export default class DemographicDetails extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        codedEntries: PropTypes.shape({
            personTypeCategory: PropTypes.object.isRequired,
            titleCategory: PropTypes.object.isRequired,
            ethnicityCategory: PropTypes.object.isRequired,
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        person: PropTypes.oneOfType([
            PropTypes.instanceOf(Person),
            PropTypes.instanceOf(PersonDetailsForAdd)
        ]).isRequired,
        onUpdate: PropTypes.func.isRequired
    };
    static defaultProps = {
        person: {}
    };

    handleUpdate=(name, value)=>{
        this.props.onUpdate(name, value);
    };
    renderLifeState() {
        const { labels, person } = this.props;

        const { personTypes, lifeState } = person;


        return (
            <LifeState
                key="lifeState"
                personType={personTypes.get(0)}
                lifeState={lifeState}
                onUpdate={this.handleUpdate}
                labels={labels}
            />
        );
    }
    renderNHSNumber() {
        const { labels, person } = this.props;

        const { personTypes, lifeState, nhsNumber, chiNumber, dateOfBirth, dateOfBirthEstimated, gender, genderAuto } = person;

        if (!isNHSNumberRequired(personTypes, lifeState)) {
            return false;
        }

        const nhsNumberObj = getNHSNumber(nhsNumber);
        return (
            <>
                <div className="pure-u-1"><hr className="rule-dashed" /></div>
                <div className="pure-u-4-24 input-flex">
                    <div className="label-col l-box">
                        <Label label={labels.number} />
                    </div>
                </div>
                <div className="pure-u-10-24 l-box">
                    <Label forId="addPerson_nhsNumber" label={labels.nhsNumber} />
                    <NHSNumber
                        key="nhs-number"
                        id="addPerson_nhsNumber"
                        labels={labels}
                        nhsNumber={nhsNumberObj}
                        onUpdate={this.handleUpdate}
                    />
                </div>
                <div className="pure-u-10-24 l-box">
                    <Label forId="addPerson_chiNumber" label={labels.chiNumber} />
                    <CHINumber
                        key="chi-number"
                        id="addPerson_chiNumber"
                        labels={labels}
                        chiNumber={chiNumber}
                        onUpdate={this.handleUpdate}

                    />
                </div>
                <div className="pure-u-4-24 l-box" />
                <div className="pure-u-20-24 l-box">
                    <NumberValidation
                        key="chi-validation"
                        labels={labels.validation}
                        estimatedDateOfBirth={dateOfBirthEstimated === true}
                        dateOfBirth={dateOfBirth}
                        gender={gender || genderAuto}
                        chiNumber={chiNumber}
                        nhsNumber={nhsNumberObj}
                    />
                </div>
            </>
        );
    }
    renderDateInputs() {
        const { labels, person } = this.props;
        const { lifeState, dateOfBirth, dateOfBirthEstimated, personTypes, dueDate, diedDate, diedDateEstimated } = person;

        if (isDueDateRequired(personTypes, lifeState)) {
            return (
                <div key="dueDateInputs" className="pure-u-1 input-flex">
                    <div className="label-col pure-u-4-24 l-box">
                        <Label
                            forId="addPerson_dueDate"
                            label={labels.dueDate}
                            mandatory={true}
                            help={labels.dueDateMessage}>
                        </Label>
                    </div>
                    <div className="control-col pure-u-10-24 l-box">
                        <DueDate key="dueDate"
                            id="addPerson_dueDate"
                            labels={labels.validation}
                            dueDate={dueDate}
                            onUpdate={this.handleUpdate} />
                    </div>
                </div>
            );
        } else if (isDateOfBirthRequired(personTypes, lifeState)) {
            const alive = isAlive(lifeState);
            const mandatory = isClient(personTypes) && alive;
            const dobContent = (
                <div key="dateOfBirthInputs" className="pure-u-1 input-flex">
                    <div className="label-col pure-u-4-24 l-box">
                        <Label
                            forId="addPerson_dateOfBirth"
                            label={labels.dateOfBirth}
                            mandatory={mandatory}
                            help={alive ? labels.dateOfBirthMessage : labels.deceasedDateOfBirthMessage}
                        >
                        </Label>
                    </div>
                    <div className="control-col pure-u-20-24">
                        <DateOfBirth
                            id="addPerson_dateOfBirth"
                            labels={labels}
                            dateOfBirth={dateOfBirth}
                            onUpdate={this.handleUpdate}
                            dateOfBirthEstimated={dateOfBirthEstimated}
                            mandatory={mandatory}
                            isDeceased={isDeceased(lifeState)}
                            diedDate={diedDate} />
                    </div>
                </div>
            );
            let deceasedContent;
            if (isDiedDateRequired(personTypes, lifeState)) {
                deceasedContent = (
                    <div key="diedDateInputs" className="pure-u-1 input-flex">
                        <div className="label-col pure-u-4-24 l-box">
                            <Label
                                forId="addPerson_diedDate"
                                label={labels.died}
                                help={labels.deceasedDateOfBirthMessage}
                            />
                        </div>
                        <div className="control-col pure-u-20-24">
                            <DiedDate
                                id="addPerson_diedDate"
                                labels={labels}
                                diedDate={diedDate}
                                diedDateEstimated={diedDateEstimated}
                                onUpdate={this.handleUpdate} />
                        </div>
                    </div>);
            }

            return (
                <>
                    {dobContent}
                    {deceasedContent}
                </>
            );
        }

        return false;
    }
    renderGender() {
        const { labels, person } = this.props;
        const { personTypes, gender, genderAuto } = person;
        if (!isGenderRequired(personTypes)) {
            return false;
        }
        return (
            <div className="pure-u-1 input-flex">
                <div className="label-col pure-u-4-24 l-box">
                    <Label forId="addPerson_gender" label={labels.gender} mandatory={isClient(personTypes)} />
                </div>
                <div className="control-col l-box">
                    <Gender gender={gender || genderAuto} onUpdate={this.handleUpdate} />
                </div>
            </div>
        );
    }
    renderEthnicity() {
        const { labels, codedEntries, person } = this.props;
        const { personTypes, ethnicity, ethnicityAuto } = person;
        if (!isEthnicityRequired(personTypes)) {
            return false;
        }


        return (
            <div className="pure-u-1 input-flex">
                <div className="label-col pure-u-4-24 l-box">
                    <Label forId="addPerson_ethnicity" label={labels.ethnicity} mandatory={isClient(personTypes)} />
                </div>
                <div className="control-col pure-u-20-24 l-box">
                    <CodedEntrySelectNoLabel
                        key="ethnicity"
                        id="addPerson_ethnicity"
                        name="ethnicity"
                        category={codedEntries.ethnicityCategory}
                        value={ethnicity || ethnicityAuto}
                        onUpdate={this.handleUpdate}
                        includeEmptyOption />
                </div>
            </div>
        );
    }
    renderOrganisationInputs() {
        const { labels, codedEntries, person } = this.props;
        const { personTypes, organisationName, professionalTitle } = person;
        if (!isOrganisationRequired(personTypes)) {
            return false;
        }
        return (
            <>
                <div className="pure-u-14-24 l-box">
                    <CodedEntrySelect
                        key="professionalTitle"
                        id="addPerson_professionalTitle"
                        label={labels.professionalTitle}
                        mandatory
                        name="professionalTitle"
                        category={codedEntries.professionalTitleCategory}
                        value={professionalTitle}
                        onUpdate={this.handleUpdate}
                        includeEmptyOption />
                </div>
                <div className="pure-u-10-24 l-box">
                    <Input
                        key="organisationName"
                        id="addPerson_organisationName"
                        label={labels.organisationName}
                        mandatory
                        name="organisationName"
                        value={organisationName}
                        onUpdate={this.handleUpdate} />
                </div>
            </>
        );
    }
    render() {
        const { labels, codedEntries, person } = this.props;
        const { personTypes = [], title, forename, surname } = person;

        return (
            <div className="pure-g-r">
                <div className="pure-u-14-24 l-box">
                    <CodedEntrySelect
                        key="personType"
                        id="addPerson_personType"
                        label={labels.personType}
                        mandatory
                        name="personType"
                        category={codedEntries.personTypeCategory}
                        value={personTypes.get(0)}
                        onUpdate={this.handleUpdate}
                        includeEmptyOption />
                </div>
                <div className="pure-u-10-24 l-box input-flex">
                    <div className="to-end">
                        {this.renderLifeState()}
                    </div>
                </div>
                <div className="pure-u-1"><hr className="rule-dashed" /></div>
                <div className="pure-u-4-24 l-box">
                    <CodedEntrySelect
                        key="title"
                        id="addPerson_title"
                        name="title"
                        label={labels.title}
                        category={codedEntries.titleCategory}
                        value={title}
                        onUpdate={this.handleUpdate}
                        includeEmptyOption={' '} />
                </div>
                <div className="pure-u-10-24 l-box">
                    <Input
                        key="forename"
                        id="addPerson_forename"
                        name="forename"
                        label={labels.forename}
                        mandatory={true}
                        value={forename}
                        onUpdate={this.handleUpdate} />
                </div>
                <div className="pure-u-10-24 l-box">
                    <Input
                        key="surname"
                        id="addPerson_surname"
                        name="surname"
                        label={labels.surname}
                        mandatory={true}
                        value={surname}
                        onUpdate={this.handleUpdate} />
                </div>
                {this.renderDateInputs()}
                {this.renderGender()}
                {this.renderEthnicity()}
                {this.renderNHSNumber()}
                {this.renderOrganisationInputs()}
            </div>
        );
    }
}