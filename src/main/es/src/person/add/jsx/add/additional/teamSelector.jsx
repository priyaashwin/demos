'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import memoize from 'memoize-one';
import endpoint from '../../../js/cfg';
import TeamAutoComplete from '../../../../../autocomplete/jsx/teamAutoComplete';

export default class RelationshipSelector extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        onUpdate: PropTypes.func.isRequired,
        name: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        selectedValue: PropTypes.array
    };

    handleSelection = (selectedValues) => {
        const {onUpdate}=this.props;
        // Update teams object with Team Id(s) extracted from Team record(s)
        onUpdate('teams', selectedValues?selectedValues.map(value => value.id) : null);
    };

    getEndpoint = memoize((endpoint, url) => ({ ...endpoint, url: url }));

    isSelectable = team => !(this.props.selectedValue && this.props.selectedValue.find(value => value === team.id));

    render() {
        const { url, onUpdate:onUpdateIgnored, ...other} = this.props;
 
        return (<div>
            <TeamAutoComplete
                {...other}
                placeholder={this.props.labels.relatedTeam.placeholder}
                onSelection={this.handleSelection}
                endpoint={this.getEndpoint(endpoint.teamAutoCompleteEndpoint, url)}
                queryParameter="nameOrOrgIdOrPrevName"
                allowMultiple={true}
                isSelectable={this.isSelectable}
            />
        </div>);
    }
}