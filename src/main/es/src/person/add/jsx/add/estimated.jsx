'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '../../../../webform/uspCheckbox';
import Message from '../../../../message/jsx/message';
import {isFuzzy} from '../../js/dob';


export default class Estimated extends PureComponent {
    static propTypes = {
        name: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        onUpdate: PropTypes.func.isRequired,
        fuzzyDate: PropTypes.shape({
            year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        }),
        isEstimated: PropTypes.bool
    };

    handleUpdate = (name, value) => {
        const { onUpdate } = this.props;

        //convert to boolean value
        onUpdate(name, 'true' === value);
    };

    render() {
        const { fuzzyDate, isEstimated, name, label, message } = this.props;

        const autoEstimated = !isEstimated && isFuzzy(fuzzyDate);

        let estimatedContent;
        if (autoEstimated) {
            estimatedContent = (
                <div className="pure-alert-small">
                    <span className="info-icon"><i className="fa fa-info-circle" aria-hidden={true} /></span>
                    <span>{message}</span>
                </div>
            );
        } else {
            estimatedContent = (
                <Checkbox
                    id={`estimated_${name}`}
                    key="estimated"
                    name={name}
                    label={label}
                    value={true}
                    checked={isEstimated === true}
                    onUpdate={this.handleUpdate} />
            );
        }

        return (
            <div className="pure-g">
                <div className="pure-u-1 l-box">
                    <Message key="message">
                        {estimatedContent}
                    </Message>
                </div>
            </div>
        );
    }
}

