'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {Person, PersonDetailsForAdd} from '../../js/person';
import AllocatedWorkers from './allocatedWorker/allocatedWorkers';
import PreferredAddress from './preferredAddress';

export default class ExtendedPersonDetail extends PureComponent {
    static propTypes = {
        person: PropTypes.oneOfType([
            PropTypes.instanceOf(Person),
            PropTypes.instanceOf(PersonDetailsForAdd)
        ]),
        labels: PropTypes.object.isRequired
    };
    render() {
        const { person, labels } = this.props;
        const { allocatedWorkers, preferredAddress } = person;
        return (
            <div className="pure-g-r">
                <PreferredAddress key="preferredAddress" address={preferredAddress} labels={labels} />
                <AllocatedWorkers key="allocatedWorkers" allocatedWorkers={allocatedWorkers} labels={labels} />
            </div>
        );
    }
}