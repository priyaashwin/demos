'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';

const Name = memo(({ forename, surname }) => (
    <div className="identifier">{forename} {surname}</div>
));

Name.propTypes = {
    forename: PropTypes.string,
    surname: PropTypes.string
};

export default Name;