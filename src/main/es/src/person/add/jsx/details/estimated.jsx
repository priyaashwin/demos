'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import EstimatedIndicator from '../../../../indicator/estimated';

const Estimated = ({ estimated }) => {
    if (estimated) {
        return (
            <>
                {' '}
                <EstimatedIndicator />
                {' '}
            </>
        );
    }
    return false;
};


Estimated.propTypes = {
    estimated: PropTypes.bool
};

export default Estimated;