'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Worker from './worker';

export default class AllocatedWorkers extends PureComponent {
    static propTypes = {
        allocatedWorkers: PropTypes.array,
        labels: PropTypes.object.isRequired
    };

    render() {
        const { allocatedWorkers = [], labels } = this.props;

        return (
            <div className="pure-u-1 input-flex in-row">
                {allocatedWorkers.map((worker, i) =>
                    (
                        <div key={`w${i}`} className="item-group">
                            <div className="name">
                                {labels.allocatedWorker}{': '}
                            </div>
                            <Worker key={`wrk${i}`} worker={worker}/>
                        </div>
                    )
                )}
            </div>
        );
    }
}