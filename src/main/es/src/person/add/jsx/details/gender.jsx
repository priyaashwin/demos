'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { formatCodedEntry } from '../../../../codedEntry/codedEntry';

const Gender = memo(({ gender, genderCategory }) => (
    <span>{formatCodedEntry(genderCategory, gender)}</span>
));

Gender.propTypes = {
    gender: PropTypes.string,
    genderCategory: PropTypes.object.isRequired
};

export default Gender;