'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { formatCodedEntry } from '../../../../codedEntry/codedEntry';

const Ethnicity = memo(({ ethnicity, ethnicityCategory }) => (
    <span>{formatCodedEntry(ethnicityCategory, ethnicity)}</span>
));

Ethnicity.propTypes = {
    ethnicity: PropTypes.string,
    ethnicityCategory: PropTypes.object.isRequired
};

export default Ethnicity;