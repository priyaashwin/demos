'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {withPreferredAddress} from '../../../../address/js/withPreferredAddress';

export default withPreferredAddress(class Address extends PureComponent {
    static propTypes = {
        address: PropTypes.object,
        labels: PropTypes.object.isRequired
    };

    render() {
        const { address, labels } = this.props;
        let addressContent;

        if (address) {
            const location = address.location;
            if (location) {
                addressContent = (
                    <div>
                        {location.location}
                    </div>
                );
            }

        }
        return (
            <div className="pure-u-1 input-flex in-row">
                <div className="name">
                    {labels.address}{': '}
                </div>
                {addressContent}
            </div>
        );
    }
});
