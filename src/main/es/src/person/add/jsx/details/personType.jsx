'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import { formatCodedEntry } from '../../../../codedEntry/codedEntry';
import Immutable from 'immutable';

const PersonType = ({ personTypes, personTypeCategory }) => (
    <>
        {
            personTypes.map((personType, i) => (
                <span key={i}>{formatCodedEntry(personTypeCategory, personType)}</span>
            ))
        }
    </>

);

PersonType.propTypes = {
    personTypes: PropTypes.instanceOf(Immutable.Iterable),
    personTypeCategory: PropTypes.object.isRequired
};

export default PersonType;