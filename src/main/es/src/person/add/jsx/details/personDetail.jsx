'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Name from './name';
import DateOfBirth from './dateOfBirth';
import Gender from './gender';
import Ethnicity from './ethnicity';
import PersonType from './personType';
import ProfessionalTitle from './professionalTitle';
import { Person, PersonDetailsForAdd } from '../../js/person';
import classnames from 'classnames';
import { isProfessional } from '../../../js/personType';
import { isSpecified } from '../../../../utils/js/textUtils';
import { formatNHSNumber } from '../../../../nhsNumber/nhsNumber';

export default class PersonDetail extends PureComponent {
    static propTypes = {
        person: PropTypes.oneOfType([PropTypes.instanceOf(Person), PropTypes.instanceOf(PersonDetailsForAdd)]),
        codedEntries: PropTypes.shape({
            ethnicityCategory: PropTypes.object.isRequired,
            personTypeCategory: PropTypes.object.isRequired,
            genderCategory: PropTypes.object.isRequired,
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        labels: PropTypes.object.isRequired,
        className: PropTypes.string
    };
    renderDateOfBirth() {
        const { person, labels } = this.props;
        const { dateOfBirth, dateOfBirthEstimated, diedDate } = person;
        if (isSpecified(dateOfBirth) && isSpecified(dateOfBirth.year)) {
            return (
                <div className="item-group">
                    <div className="name">
                        {labels.dateOfBirth}
                        {': '}
                    </div>
                    <DateOfBirth dateOfBirth={dateOfBirth} diedDate={diedDate} estimated={dateOfBirthEstimated} />
                </div>
            );
        }
        return false;
    }
    renderGender() {
        const { person, codedEntries, labels } = this.props;
        const { gender } = person;
        if (isSpecified(gender)) {
            return (
                <div className="item-group">
                    <div className="name">
                        {labels.gender}
                        {': '}
                    </div>
                    <Gender gender={gender} genderCategory={codedEntries.genderCategory} />
                </div>
            );
        }
        return false;
    }
    renderEthnicity() {
        const { person, codedEntries, labels } = this.props;
        const { ethnicity } = person;
        if (isSpecified(ethnicity)) {
            return (
                <div className="item-group">
                    <div className="name">
                        {labels.ethnicity}
                        {': '}
                    </div>
                    <Ethnicity ethnicity={ethnicity} ethnicityCategory={codedEntries.ethnicityCategory} />
                </div>
            );
        }
        return false;
    }
    renderProfessionalTitle() {
        const { person, codedEntries, labels } = this.props;
        const { professionalTitle } = person;
        if (isSpecified(professionalTitle)) {
            return (
                <div className="item-group">
                    <div className="name">
                        {labels.professionalTitle}
                        {': '}
                    </div>
                    <ProfessionalTitle
                        professionalTitle={professionalTitle}
                        professionalTitleCategory={codedEntries.professionalTitleCategory}
                    />
                </div>
            );
        }
        return false;
    }
    renderOrganisationName() {
        const { person, labels } = this.props;
        const { organisationName } = person;
        if (isSpecified(organisationName)) {
            return (
                <div className="item-group">
                    <div className="name">
                        {labels.organisationName}
                        {': '}
                    </div>
                    <span>{organisationName}</span>
                </div>
            );
        }
        return false;
    }
    renderAdditionalFields() {
        const { person } = this.props;
        const { personTypes } = person;

        let professionalFields;
        //professional title and organisation if professional type
        if (isProfessional(personTypes)) {
            professionalFields = (
                <div className="pure-u-1 input-flex in-row">
                    {this.renderProfessionalTitle()}
                    {this.renderOrganisationName()}
                </div>
            );
        }

        return (
            <>
                <div className="pure-u-1 input-flex in-row">
                    {this.renderDateOfBirth()}
                    {this.renderGender()}
                    {this.renderEthnicity()}
                </div>
                {professionalFields}
            </>
        );
    }
    renderNHSNumber() {
        const { person, labels } = this.props;
        const { nhsNumber } = person;
        if (isSpecified(nhsNumber)) {
            return (
                <div className="item-group">
                    <div className="name">
                        {labels.nhsNumber}
                        {': '}
                    </div>
                    <span>{formatNHSNumber(nhsNumber)}</span>
                </div>
            );
        }
        return false;
    }
    renderChiNumber() {
        const { person, labels } = this.props;
        const { chiNumber } = person;
        if (isSpecified(chiNumber)) {
            return (
                <div className="item-group">
                    <div className="name">
                        {labels.chiNumber}
                        {': '}
                    </div>
                    <span>{chiNumber}</span>
                </div>
            );
        }
        return false;
    }
    render() {
        const { className, person, codedEntries, labels } = this.props;
        const { personIdentifier, forename, surname, personTypes } = person;
        let personIdentifierContent;
        if (personIdentifier) {
            personIdentifierContent = <span>{`[${person.personIdentifier}]`}</span>;
        }

        return (
            <div className={classnames('pure-g-r', className)}>
                <div className="pure-u-1">
                    <div className="identification">
                        <Name forename={forename} surname={surname} />
                        <div className="identifier">{personIdentifierContent}</div>
                    </div>
                </div>
                <div className="pure-u-1 input-flex in-row">
                    <div className="item-group">
                        <div className="name">
                            {labels.personType}
                            {': '}
                        </div>
                        <PersonType personTypes={personTypes} personTypeCategory={codedEntries.personTypeCategory} />
                    </div>
                    {this.renderNHSNumber()}
                    {this.renderChiNumber()}
                </div>
                {this.renderAdditionalFields()}
            </div>
        );
    }
}
