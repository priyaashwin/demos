'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class Worker extends PureComponent {
    static propTypes = {
        worker: PropTypes.object
    };
    render() {
        const { worker} = this.props;

        return (
            <div key={worker.id}>
                <span>{worker.name}</span> 
            </div>
        );
    }
}