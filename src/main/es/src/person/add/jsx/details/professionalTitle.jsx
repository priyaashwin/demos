'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { formatCodedEntry } from '../../../../codedEntry/codedEntry';

const ProfessionalTitle = memo(({ professionalTitle, professionalTitleCategory }) => (
    <span>{formatCodedEntry(professionalTitleCategory, professionalTitle)}</span>
));

ProfessionalTitle.propTypes = {
    professionalTitle: PropTypes.string,
    professionalTitleCategory: PropTypes.object.isRequired
};

export default ProfessionalTitle;