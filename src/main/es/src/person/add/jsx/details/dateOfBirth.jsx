'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import Estimated from './estimated';
import { formatFuzzyDate } from '../../../../date/fuzzyDate';
import { getMoment, getMask, getAge } from '../../js/dob';
const DateOfBirth = ({ dateOfBirth, diedDate, estimated }) => {
    const dobMoment = getMoment(dateOfBirth);
    const dodMoment=getMoment(diedDate);
    let dob;    
    if (dobMoment) {
        dob = formatFuzzyDate({
            calculatedDate: dobMoment,
            fuzzyDateMask: getMask(dateOfBirth)
        }, true).date;
    }

    return (
        <div>
            <span>{dob}</span>
            <Estimated key="estimated" estimated={estimated} />
            <span>
                { `[${getAge(dobMoment, dodMoment)}]`}
            </span>
        </div >
    );
};

DateOfBirth.propTypes = {
    dateOfBirth: PropTypes.shape({
        year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    }),
    diedDate: PropTypes.shape({
        year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    }),
    estimated: PropTypes.bool
};

export default DateOfBirth;