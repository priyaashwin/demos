'use strict';
import { isSpecified } from '../../../utils/js/textUtils';
import { isClient, isAdopter, isFosterCarer } from '../../js/personType';
import {  isDeceased } from '../../js/lifeState';
import { isValidateNHSNumber } from '../../../nhsNumber/nhsNumber';
import { isCHIGenderValid, isCHIDateOfBirthValid } from '../../../chiNumber/chiNumber';
import { parseDate, DATE_STRING_REG} from '../../../date/date';
import { getMoment } from './dob';
import {
    isNHSNumberRequired,
    isEthnicityRequired,
    isGenderRequired,
    isOrganisationRequired,
    isDueDateRequired,
    isDateOfBirthRequired,
    isDiedDateRequired,
    isProfessionalRelationshipRequired,
    isValidAgeRequired
} from './options';

const DUE_DATE_MAX_DAYS = 280;
const ADOPTER_AGE=21;
const FOSTER_CARER_AGE=18;

/**
 * Returns an errors object or null 
 * if data passes validation
 */
export const validateSuppliedData = function (data, labels, isOpenAccess, isFullData, skipAutoRelationship) {
    const errors = {};

    if (!isSpecified(data.forename)) {
        errors.forename = labels.forenameMandatory;
    }

    if (!isSpecified(data.surname)) {
        errors.surname = labels.surnameMandatory;
    }

    if (!isSpecified(data.personTypes) || data.personTypes.size!==1) {
        errors.personType = labels.personTypeMandatory;
    }

    if (isGenderRequired(data.personTypes)) {
        if (isClient(data.personTypes)) {
            if (!isSpecified(data.gender)&&!isSpecified(data.genderAuto)) {
                errors.gender = labels.genderMandatory;
            }
        }
    }

    if (isEthnicityRequired(data.personTypes)) {
        if (isClient(data.personTypes)) {
            if (!isSpecified(data.ethnicity)&&!isSpecified(data.ethnicityAuto)) {
                errors.ethnicity = labels.ethnicityMandatory;
            }
        }
    }
    if (isDueDateRequired(data.personTypes, data.lifeState)) {
        if (!isSpecified(data.dueDate)) {
            errors.dueDate = labels.dueDateMandatory;
        } else {
            //due date must be > today and < 280 days in the future
            if (!DATE_STRING_REG.test(data.dueDate)) {
                errors.dueDate = labels.dueDateInvalidDate;
            } else {
                const dueDate = parseDate(data.dueDate).startOf('day');
                const days = dueDate.diff(moment().startOf('day'), 'days');
                //validate
                if (days < 0 || days > DUE_DATE_MAX_DAYS) {
                    errors.dueDate = labels.dueDateInvalid;
                }

            }
        }
    }

    if (isDateOfBirthRequired(data.personTypes, data.lifeState)) {
        //Date of birth is non-mandatory for deceased state or Client
        if ((!isSpecified(data.dateOfBirth) || !isSpecified(data.dateOfBirth.year)) && (isDeceased(data.lifeState)||isClient(data.personTypes))) {
            errors.dateOfBirth = labels.dateOfBirthMandatory;
        }
    }

    if (isDiedDateRequired(data.personTypes, data.lifeState)) {
        if (isSpecified(data.diedDate)) {
            //check died date is < today
            const diedDate = getMoment(data.diedDate);
            const today = moment().tz('Europe/London');
            if (diedDate.valueOf() > today.valueOf()) {
                errors.diedDate = labels.diedDateAfterToday;
            } else {
                if (isSpecified(data.dateOfBirth)) {
                    const dob = getMoment(data.dateOfBirth);

                    if (dob.valueOf() > diedDate.valueOf()) {
                        errors.diedDate = labels.diedDateBeforeDateOfBirth;
                    }
                }
            }
        }
    }
    
    if (isValidAgeRequired(data.personType, data.lifeState)) {
        if (isSpecified(data.dateOfBirth) && isSpecified(data.dateOfBirth.year)) {   		
            const birthday = getMoment(data.dateOfBirth);
            const end = moment().tz('Europe/London');
            const age = Math.max(end.diff(birthday , 'years'), 0);
			
            if (isFosterCarer(data.personType) && age < FOSTER_CARER_AGE) {    			
                errors.dateOfBirth = labels.invalidFosterAge;
            } else if (isAdopter(data.personType) && age < ADOPTER_AGE) {			
                errors.dueDate = labels.invalidAdopterAge;
            }
        } 		
     }

    if (isNHSNumberRequired(data.personTypes, data.lifeState)) {
        if (isSpecified(data.nhsNumber)) {
            if (!isValidateNHSNumber(data.nhsNumber)) {
                errors.nhsNumber = labels.nhsNumberInvalid;
            }
        }

        if (isSpecified(data.chiNumber)) {
            if (!isCHIGenderValid(data.chiNumber, data.gender||data.genderAuto)) {
                errors.chiNumber = labels.chiNumberInvalid;
            }
            if (data.dateOfBirthEstimated === true) {
                errors.chiNumber = labels.chiNumberInvalid;
            } else {
                const dateOfBirth=data.dateOfBirth||{};
                if (!isCHIDateOfBirthValid(data.chiNumber, dateOfBirth.year, dateOfBirth.month, dateOfBirth.day)) {
                    errors.chiNumber = labels.chiNumberInvalid;
                }
            }
        }
    }

    if(isOrganisationRequired(data.personTypes)){
        if (!isSpecified(data.professionalTitle)) {
            errors.dueDate = labels.professionalTitleMandatory;
        } 

        if (!isSpecified(data.organisationName)) {
            errors.organisationName = labels.organisationNameMandatory;
        } 
    }

    //validate the prfessional relationship
    if(isFullData){
        if(isProfessionalRelationshipRequired(data.personTypes)){
            if(!isOpenAccess||!skipAutoRelationship){
                if(!isSpecified(data.autoRelationshipTypeId)){
                    errors.autoRelationshipTypeId=labels.relationshipTypeMandatory;
                }

                if(!isSpecified(data.autoRelatedPerson)||!isSpecified(data.autoRelatedPerson.id)){
                    errors.autoRelatedPersonId=labels.relatedPersonMandatory;
                }
            }
        }
    }
    
    return Object.keys(errors).length > 0 ? errors : null;
};