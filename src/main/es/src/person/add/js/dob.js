import {getFuzzyMoment} from '../../../webform/uspFuzzyDateInput';

export const getMask=function(dateOfBirth){
    let mask='NONE';
    if(dateOfBirth && dateOfBirth.year){
        mask='YEAR';
        if(dateOfBirth.month !== undefined){
            mask='MONTH';
            if(dateOfBirth.day !== undefined){
                mask='DATE';
            }
        }        
    }

    return mask;
};

export const getAge = function (startMoment, endMoment) {
    let age = '';

    if (startMoment) {
        const end=endMoment||moment().tz('Europe/London');
        //maximum value 0 or diff in years
        age = '' + Math.max(end.diff(startMoment, 'years'), 0);
    }
    return age;
};

export const isFuzzy = function (fuzzyDate) {
    return (!fuzzyDate || (fuzzyDate.day === undefined || fuzzyDate.day === null));
};

export const getMoment = function (date = {}) {
    if (!date.year) return;
    const fuzzyDate = getFuzzyMoment(date);

    if (date.month === undefined || date.month === null) {
        fuzzyDate.endOf('year');
    } else if (date.day === undefined || date.day === null) {
        fuzzyDate.endOf('month');
    }

    return fuzzyDate;
};
