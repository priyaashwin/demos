'use strict';

class Endpoint{
  constructor() {
    this.duplicateSearchEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',        
        'Accept': 'application/vnd.olmgroup-usp.relationshipsrecording.personwithallocatedworkersandpreferredaddress+json;charset=UTF-8'
      },
      params:{
        forename:'',
        surname:'',
        nhsNumber:'',
        yearOfBirth:'',
        monthOfBirth:'',
        dayOfBirth:''        
      },
      responseType: 'json',
      responseStatus:200
    };  
    
    this.relationshipAutoCompleteEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',        
        'Accept': 'application/vnd.olmgroup-usp.person.PersonWithAddress+json;charset=UTF-8'
      },
      params:{
        pageSize:50
      },
      responseType: 'json',
      responseStatus:200
    };  

    this.saveNewPersonEndpoint={
      method: 'post',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',        
        'Accept': 'application/json;charset=UTF-8'
      },
      data:{
      },
      responseType: 'json',
      responseStatus:201
    };
    
    this.teamAutoCompleteEndpoint = {
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.organisation.organisationwithaddressandpreviousnames+json;charset=UTF-8'        	
      },
      params: {
        pageSize: 50,
        appendWildcard: true
      },
      responseType: 'json',
      responseStatus: 200
    };
  }
}

export default new Endpoint();