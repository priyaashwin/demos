'use strict';
import { Record,List } from 'immutable';
import { isSpecified } from '../../../utils/js/textUtils';
import {parseDate} from '../../../date/date';

const DEFAULT_LIFE_STATE = 'ALIVE';
const DEFAULT_PERSON_TYPES = List(['CLIENT']);

const getValue = function (val) {
    if (typeof val === 'undefined') {
        return null;
    }
    return val;
};

const getBoolValue = function (val) {
    if (typeof val === 'string') {
        return val === 'true' || val === 'TRUE';
    }
    return true === val;
};

const basePerson = {
    title: undefined,
    forename: undefined,
    surname: undefined,
    dateOfBirth: undefined,
    dateOfBirthEstimated: undefined,
    diedDate: undefined,
    diedDateEstimated: undefined,
    gender: undefined,
    ethnicity: undefined,
    personTypes: DEFAULT_PERSON_TYPES,
    lifeState: DEFAULT_LIFE_STATE,
    dueDate: undefined,
    nhsNumber: undefined,
    chiNumber: undefined,
    professionalTitle: undefined,
    organisationName: undefined
};
const getFuzzyDate = function (date) {
    let dob;
    if (date) {
        dob = {
            year: undefined,
            month: undefined,
            day: undefined
        };
        if (date.calculatedDate) {
            const dobMoment = moment.tz(date.calculatedDate, 'Europe/London');
            switch (date.fuzzyDateMask) {
            case 'DAY':
                dob.day = dobMoment.get('date');
                //falls through
            case 'MONTH':
                dob.month = dobMoment.get('month');
                //falls through
            case 'YEAR':
                dob.year = dobMoment.get('year');
                break;
            default:
                dob.day = dobMoment.get('date');
                dob.month = dobMoment.get('month');
                dob.year = dobMoment.get('year');
            }
        } else {
            dob.year = date.year ? date.year : undefined;
            dob.month = date.month ? date.month : undefined;
            dob.day = date.day ? date.day : undefined;
        }
    }
    return new FuzzyDate(dob);
};

export class FuzzyDate extends Record({
    year: undefined,
    month: undefined,
    day: undefined,
    fuzzyDateMask: undefined,
    calculatedDate: undefined,
    _type: 'SimpleFuzzyDate'
}) {
    isFuzzy(){
        return !isSpecified(this.day);
    }
    isSpecified(){
        return isSpecified(this.year);
    }
}

export class PersonDetailsForAdd extends Record({
    ...basePerson,
    //genderAuto holds the system calculated gender
    //the gender property is used when the user makes a selection
    genderAuto: undefined,
    //ethnicityAuto holds the system calculated ethnicity
    //the ethnicity property is used when the user makes a selection
    ethnicityAuto: undefined,
    autoRelationshipTypeId: undefined,
    autoRelatedPerson: undefined, 
    skipAutoRelationship: false,
    teams: undefined
}) {
    constructor(data) {
        const { personType, personTypes, dateOfBirth, diedDate, ...other } = data || {};
        const types = isSpecified(personType) ? List([personType]) : isSpecified(personTypes)?List(personTypes): DEFAULT_PERSON_TYPES;
        const dob = getFuzzyDate(dateOfBirth);
        const dod = getFuzzyDate(diedDate);
        super({ ...other, personTypes: types, dateOfBirth: dob, diedDate: dod });
    }
    getData() {
        let dateOfBirth = this.dateOfBirth;
        if(dateOfBirth&&!dateOfBirth.isSpecified()){
            //not specified - so clear down object
            dateOfBirth=undefined;
        }
        let dateOfDeath = this.diedDate;
        if(dateOfDeath&&!dateOfDeath.isSpecified()){
            //not specified - so clear down object
            dateOfDeath=undefined;
        }
        const skipRelationship=getBoolValue(this.skipAutoRelationship)===true||(this.personTypes.size===1&&this.personTypes.indexOf('PROFESSIONAL')!==-1);
        const relationshipData={};

        if(!skipRelationship){
            relationshipData.autoRelationshipTypeId=this.autoRelationshipTypeId,
            relationshipData.autoRelatedPersonId=this.autoRelatedPerson?this.autoRelatedPerson.id:null;
        }

        const dueDate=parseDate(this.dueDate);

        const obj = {
            title: getValue(this.title),
            forename: getValue(this.forename),
            surname: getValue(this.surname),
            dateOfBirth: dateOfBirth ? dateOfBirth.toJSON() : null,
            dateOfBirthEstimated: getBoolValue(this.dateOfBirthEstimated)||dateOfBirth?dateOfBirth.isFuzzy():null,
            diedDate: dateOfDeath ? dateOfDeath.toJSON() : null,
            diedDateEstimated: getBoolValue(this.diedDateEstimated)||dateOfDeath?dateOfDeath.isFuzzy():null,
            gender: getValue(this.gender || this.genderAuto),
            ethnicity: getValue(this.ethnicity || this.ethnicityAuto),
            personTypes: this.personTypes,
            lifeState: this.lifeState,
            dueDate: dueDate?dueDate.valueOf():null,
            nhsNumber: getValue(this.nhsNumber),
            chiNumber: getValue(this.chiNumber),
            professionalTitle: getValue(this.professionalTitle),
            organisationName: getValue(this.organisationName),
            ...relationshipData
        };

        const teams = this.teams && this.teams.toJSON();

        if (teams && teams.length > 0) {
            obj.teams = teams;
        }
        return obj;
    }
}

export class Person extends Record({
    ...basePerson,
    id: undefined,
    personIdentifier: undefined,
    name: undefined,
    allocatedWorkers: [],
    preferredAddress: undefined
}) {
    constructor(data) {
        const { personType, personTypes, dateOfBirth, diedDate, ...other } = data || {};
        const types = isSpecified(personType) ? List([personType]) : isSpecified(personTypes)?List(personTypes): DEFAULT_PERSON_TYPES;
        const dob = getFuzzyDate(dateOfBirth);
        const dod = getFuzzyDate(diedDate);
        super({ ...other, personTypes: types, dateOfBirth: dob, diedDate: dod });
    }
}