/**
 * Declare a map of title to gender values
 * Used to pre-select gender based on title selection
 */
const titleMap=new Map();
titleMap.set('MR', 'MALE');
titleMap.set('MISS', 'FEMALE');
titleMap.set('MRS', 'FEMALE');
titleMap.set('MS', 'FEMALE');


export default titleMap;