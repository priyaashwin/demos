'use strict';
import { isProfessional, isClient } from '../../js/personType';
import {isAlive } from '../../js/lifeState';

/**
 * A set of methods that pertain to the 
 * duplicate check process
 */


/**
 * returns a boolean value to indicate if a duplicate
 * check is required for this personType
 */
export const requiresDuplicateCheck = function (personTypes, lifeState) {
    //duplicate checks required
    let duplicateCheckRequired = false;

    if (isProfessional(personTypes)) {
        duplicateCheckRequired=true;
    }else if(isClient(personTypes) && isAlive(lifeState)){
        duplicateCheckRequired=true;
    }

    return duplicateCheckRequired;
};