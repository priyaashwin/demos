import { isProfessional, isOther, isClient, isFosterCarer, isAdopter } from '../../js/personType';
import { isAlive, isUnborn, isDeceased } from '../../js/lifeState';

export const isProfessionalRelationshipRequired = function (personTypes) {
    return !isProfessional(personTypes);
};

export const isNHSNumberRequired = function (personTypes, lifeState) {
    return isAlive(lifeState) && !isProfessional(personTypes);
};

export const isEthnicityRequired = function (personTypes) {
    return !isProfessional(personTypes);
};

export const isGenderRequired = function (personTypes) {
    return !isProfessional(personTypes);
};

export const isOrganisationRequired = function (personTypes) {
    return isProfessional(personTypes);
};

export const isDueDateRequired = function (personTypes, lifeState) {
    return isClient(personTypes) && isUnborn(lifeState);
};

export const isDateOfBirthRequired = function (personTypes, lifeState) {
    return !isProfessional(personTypes) && !isUnborn(lifeState);
};

export const isDiedDateRequired = function (personTypes, lifeState) {
    return isOther(personTypes) && isDeceased(lifeState);
};

export const isValidAgeRequired = function (personTypes, lifeState) {
    return (isFosterCarer(personTypes) || isAdopter(personTypes)) && isAlive(lifeState) ;
};