'use strict';

const hasPersonType = function (personType, personTypes) {
    if(!personTypes){
        return false;
    }

    if(typeof personTypes==='string'){
        return personType===personTypes;
    }else{
        return personTypes.indexOf(personType)!==-1;
    }
};
export const isClient = function (personTypes) {
    return hasPersonType('CLIENT', personTypes);
};

export const isProfessional = function (personTypes) {
    return hasPersonType('PROFESSIONAL', personTypes);
};

export const isAdopter = function (personTypes) {
    return hasPersonType('ADOPTER', personTypes);
};

export const isFosterCarer = function (personTypes) {
    return hasPersonType('FOSTER_CARER', personTypes);
};

export const isOther = function (personTypes){
    return hasPersonType('OTHER', personTypes);
};