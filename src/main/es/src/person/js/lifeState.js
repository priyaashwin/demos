export const getLifeState = function (person) {
    return person.lifeState || 'ALIVE';
};

export const isUnborn = function (lifestate) {
    return 'UNBORN' === lifestate;
};

export const isAlive = function (lifestate) {
    return 'ALIVE' === lifestate;
};

export const isDeceased = function (lifestate) {
    return 'DECEASED' === lifestate;
};