import { formatDate } from '../../date/date';
import { formatFuzzyDate } from '../../date/fuzzyDate';
import {getLifeState} from './lifeState';
import React from 'react';
import Estimated from '../../indicator/estimated';

export const formatDateOfBirthWithLifeState = function (person) {
    let output = '';
    const estimated = person.dateOfBirthEstimated ? (<Estimated />) : undefined;
    let deceased;
    switch (getLifeState(person)) {
        case 'UNBORN':
            output = (
                <>
                    <strong>{' Due Date '}</strong>
                    {` ${formatDate(person.dueDate, true)} `}
                    <span className="unborn" title="Unborn" aria-label="Unborn" />
                </>
            );
            break;
        case 'NOT_CARRIED_TO_TERM':
            output = (
                <>
                    <span>{' Not carried '}</span>
                </>
            );
            break;
        case 'DECEASED':
            deceased = (<span className="deceased" title="Deceased" aria-label="Deceased" />);
        // falls through
        case 'ALIVE':
            //some records such as professionals have age = -1 so only output if positive number
            if(person.age>=0){
                output = (
                    <>
                        <strong>{' Born '}</strong>
                        {` ${formatFuzzyDate(person.dateOfBirth).date} `}
                        {estimated}
                        {deceased}
                        <span>{` ${person.age} year${person.age === 1 ? '' : 's'} `}</span>
                    </>
                );
            }
            break;
    }
    return output;
};