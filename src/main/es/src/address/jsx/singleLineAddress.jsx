'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formatAddressLocationDescription } from '../js/formatAddressLocation';
import { formatCodedEntry } from '../../codedEntry/codedEntry';
import Temporary from '../../indicator/temporary';
import Placement from '../../indicator/placement';
import DoNotDisclose from '../../indicator/doNotDisclose';
import MapLink from '../../link/link';

export default class SingleLineAddress extends PureComponent {
    static propTypes = {
        address: PropTypes.object,
        labels: PropTypes.object.isRequired,
        permissions: PropTypes.shape({
            canViewAddress: PropTypes.bool
        }),
        mapLinkUrl: PropTypes.string.isRequired,
        codedEntries: PropTypes.shape({
            unknownLocation: PropTypes.object.isRequired
        }).isRequired
    };
    render() {
        const { address, mapLinkUrl, codedEntries = {}, permissions = {}, labels = {} } = this.props;

        let addressContent;
        if (address) {
            const temp = address.usage === 'TEMPORARY' ? (<Temporary label={labels.isTemporary} />) : false;
            const placement = address.type === 'PLACEMENT' ? (<Placement label={labels.isPlacement} />) : false;
            const doNotDisclose = address.doNotDisclose && (<DoNotDisclose label={labels.isDoNotDisclose} />);
            const addressLocation = formatAddressLocationDescription(address, labels);

            let mapLink = false;
            if (addressLocation.length > 0) {
                mapLink = (<MapLink url={mapLinkUrl} term={addressLocation} target="_blank">{addressLocation}</MapLink>);
            }

            let unknownLocation = formatCodedEntry(codedEntries.unknownLocation, address.unknownLocationType);
            if (unknownLocation.length <= 0) {
                unknownLocation = false;
            }

            addressContent = (
                <span title={addressLocation}>
                    {doNotDisclose}
                    {placement}
                    {temp}
                    {unknownLocation}
                    {mapLink}
                </span>
            );
        }
        if (!address) {
            if (permissions.canViewAddress) {
                addressContent = (<span>{labels.noAddress || 'NO ADDRESS RECORDED'}</span>);
            } else {
                addressContent = (<span>{labels.noAddressPermission || ''}</span>);
            }
        }
        return addressContent;
    }
}
