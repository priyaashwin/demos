'use strict';

/**
 * Format the room and floor into a string for address
 * description output.
 * 
 * @param {object} address Address object containing location
 * @param {object} labels Labels object with room and floor labels
 */
const formatAddressLocationDescription = function ({ roomDescription = '', floorDescription = '', location = {} }, { room: roomLabel, floor: floorLabel }) {
    return ([
        roomDescription ? `${roomLabel} ${roomDescription},` : '',
        floorDescription ? `${floorLabel} ${floorDescription},` : '',
        location === null ? '' : location.location ? `${location.location} ` : ''
    ]).filter(v=>v!=='').join(' ').trim();
};

export { formatAddressLocationDescription };