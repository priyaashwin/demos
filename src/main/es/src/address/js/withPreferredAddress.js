'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import memoize from 'memoize-one';

/**
 * A HOC for locating the preferred address which is
 * of one of the specified types and at one of the
 * specified statuses. 
 * 
 * The preferred address is then exposed as an address property
 * passed into the wrapped component
 * 
 * @param {*} WrappedComponent 
 * @param {array} addressTypes an optional array of types default to ['PLACEMENT', 'HOME', 'EMERGENCY', 'WORK']
 * @param {array} addressStatuses an optional array of statuses default to ['ACTIVE']
 */
export function withPreferredAddress(WrappedComponent, addressTypes=['PLACEMENT', 'HOME', 'EMERGENCY', 'WORK'], addressStatuses=['ACTIVE']) {
    class PreferredAddress extends PureComponent {
        getAddress = memoize((address,  addressTypes, addressStatuses)=>{
            if(address && addressStatuses.includes(address.temporalStatus) && addressTypes.includes(address.type)){
                return address;
            }
            return null;
        });
        static propTypes = {
            address: PropTypes.shape({
                temporalStatus: PropTypes.string,
                type: PropTypes.string
            })
        };

        render() {
            const { address, ...other } = this.props;

            const preferredAddress=this.getAddress(address, addressTypes, addressStatuses);
            return (
                <WrappedComponent {...other} address={preferredAddress}/>
            );
        }
    }

    PreferredAddress.displayName = `withPreferredAddress(${getDisplayName(WrappedComponent)})`;
    return PreferredAddress;
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
