'use strict';

const isCHIGenderValid = function (chiNumber, gender) {
    let valid = false;

    if (typeof chiNumber === 'string' && chiNumber.length == 10) {
        const genderDigit = Number(chiNumber.substring(8, 9));
        switch (gender) {
        case 'MALE':
            if (genderDigit % 2 !== 0) {
                valid = true;
            }
            break;
        case 'FEMALE':
            if (genderDigit % 2 === 0) {
                valid = true;
            }
            break;
        case 'INDETERMINATE':
            valid = false;
            break;
        default:
            valid = true;
            break;
        }
    }
    return valid;
};

const isCHIDateOfBirthValid = function (chiNumber, year, month, day) {
    let valid = false;
    if (typeof chiNumber === 'string' && chiNumber.length == 10) {
        // check date of birth in the format ddmmyy matches the first 6 characters in CHI number
        const dobChiNumber = chiNumber.substring(0, 6);
        const dateStr = [];
        if (day !== undefined && day != null) {
            dateStr.push(`${day}`.padStart(2, '0'));
            if (month !== undefined && month != null) {
                //month is zero based so +1 to convert to format for parse
                dateStr.push(`${Number(month) + 1}`.padStart(2, '0'));                
                if (year !== undefined && year != null) {
                    dateStr.push(`${year}`.substr(2,2));
                }
            }            
        }
        
        if (dateStr.join('') === dobChiNumber) {
            valid = true;
        }
    }
    return valid;
};

export { isCHIGenderValid, isCHIDateOfBirthValid };