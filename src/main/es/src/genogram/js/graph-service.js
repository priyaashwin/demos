import * as constants from './constants';
import dagre from 'dagre';

export default class GraphService {
  constructor({ data = [], options = {} }) {

    this.data = data;
    this.options = options;

    this.initPeople(data);
    this.initUnions(data);
    this.initChildren(data);

    if (this.people) {
      this.initGraph();
    }
  }

  initPeople(data) {
    this.people = data.reduce((acc, person) => {
      acc[person.key] = {
        ...person
      };
      return acc;
    }, {});
  }

  initUnions(data) {

    this.unions = [];

    data.forEach(person => {
      const { unions } = person;

      //leave if no unions
      if (!unions || unions.length === 0) return;

      unions.forEach((union) => {
        const { category, key } = union;
        const partner = this.findPerson(key);

        //leave if union already added or if there isn't a partner
        if (!partner || this.isUnionAdded(person.key, partner.key)) return;

        //prefer male on the left if possible
        let personAKey, personBKey;
        if (person.gender === 'MALE') {
          personAKey = person.key;
          personBKey = partner.key;
        } else {
          personAKey = partner.key;
          personBKey = person.key;
        }

        const index = this.unions.length;
        this.unions.push({
          category,
          personAKey: personAKey,
          personBKey: personBKey,
          unionKey: `${constants.UNION_NODE_PREFIX}${index}`,
          nodeKey: `${constants.FAMILY_NODE_PREFIX}${index}`
        });
      });
    });

    //window.console.log('\n');
    //window.console.log('this.unions', this.unions);

  }

  initChildren(data) {

    this.children = [];

    data.forEach(child => {
      const union = this.findUnion(child);
      const childKey = child.key;
      if (union) {
        this.children.push({
          childKey: childKey,
          adopted: child.adoptive,
          parentKey: union.nodeKey,
          union: union //store for this.getEdges
        });
      } else if (child.motherKey && !child.fatherKey) {
        this.children.push({
          childKey: childKey,
          adopted: child.adoptive,
          parentKey: child.motherKey,
          motherKey: child.motherKey //store for this.getEdges
        });
      } else if (child.fatherKey && !child.motherKey) {
        this.children.push({
          childKey: childKey,
          adopted: child.adoptive,
          parentKey: child.fatherKey,
          fatherKey: child.fatherKey //store for this.getEdges
        });
      } else if (!union && child.fatherKey && child.motherKey) {
        this.children.push({
          childKey: childKey,
          parentKey: child.fatherKey,
          fatherKey: child.fatherKey //store for this.getEdges
        });
        this.children.push({
          childKey: childKey,
          parentKey: child.motherKey,
          motherKey: child.motherKey //store for this.getEdges
        });
      }
    });

    //window.console.log('\n');
    //window.console.log('this.children', this.children);
  }

  initGraph() {

    this.graph = new dagre.graphlib.Graph({
      ...this.options.dagreLib
    });

    this.graph.setGraph(this.options.dagre);
    this.graph.setDefaultEdgeLabel(function () {
      return {};
    });

    this.addNodes();
    this.addEdges();
    dagre.layout(this.graph);
  }

  /**
  @method addNodes - Adds vertices to the graph.
    Assumes at this point each person is 'legit' (i.e. has either
    a union relationship or parent/child relationship)
  */
  addNodes() {
    Object.keys(this.people).forEach(key => {
      this.graph.setNode(key, { width: 150, height: 150 });
    });
  }

  /**
  @method addEdges - Based on this demo:
    http://www.samsarin.com/project/dagre-d3/latest/demo/clusters.html
    Seems to roughly place nodes in an agreeable position.
  */
  addEdges() {

    this.unions.forEach(union => {
      const { personAKey, personBKey, nodeKey, unionKey } = union;
      // Draw dummy nodes. The unionKey is a wrapper node around the two union
      // nodes to help force horizontal positioning. The nodeKey is a dummy node
      // to draw edges to/from.
      this.graph.setParent(personAKey, unionKey);
      this.graph.setParent(personBKey, unionKey);
      this.graph.setParent(nodeKey, unionKey);

      // Draw edge from parent to dummy nodeKey
      this.graph.setEdge(personAKey, nodeKey);
      this.graph.setEdge(personBKey, nodeKey);
    });

    this.children.forEach(child => {
      // The parent key may be a dummy nodeKey or a single parent key.
      // Positioning of the child node isn't great if the latter. One option could be
      // to find the first union said parent has and draw an edge to that dummy union key too.
      this.graph.setEdge(child.parentKey, child.childKey);
    });
  }

  /**
  @method getEdges - Ignores dagre & instead uses this.unions & this.children to generate
    edges in a shape that <Genogram /> expects i.e a source & target id and type of edge.

    Edges that have a 'union' target (e.g parents to a child) are return as an array of the
    the union personAKey & personBKey.
  */
  getEdges() {

    const edges = [];

    this.unions.forEach(union => {
      const { personAKey, personBKey, category } = union;

      let type;
      switch (category.toLowerCase()) {
        case 'marriage':
          type = constants.MARRIED_UNION_EDGE;
          break;
        case 'separated':
          type = constants.SEPARATED_UNION_EDGE;
          break;
        case 'divorced':
          type = constants.DIVORCED_UNION_EDGE;
          break;
        default:
          type = constants.MARRIED_UNION_EDGE;
      }

      edges.push({
        source: personAKey,
        target: personBKey,
        type: type,
        category: constants.UNION_EDGE_CATEGORY
      });
    });

    this.children.forEach(child => {
      const { adopted, childKey, fatherKey, motherKey, union } = child;
      const type = adopted ? constants.PARENT_ADOPTED_CHILD_EDGE : constants.PARENT_CHILD_EDGE;
      if (union) {
        edges.push({
          source: childKey,
          //use the union keys as they may have 'swapped'
          target: [union.personAKey, union.personBKey],
          type,
          category: constants.ELBOW_EDGE_CATEGORY
        });
      } else if (child.motherKey && !child.fatherKey) {
        edges.push({
          source: childKey,
          target: motherKey,
          type,
          category: constants.ELBOW_EDGE_CATEGORY
        });
      } else if (child.fatherKey && !child.motherKey) {
        edges.push({
          source: childKey,
          target: fatherKey,
          type,
          category: constants.ELBOW_EDGE_CATEGORY
        });
      }
    });

    return edges;

  }

  /**
  @method getLifeState - Check the current persons life state
  */
  getUnbornLifeState(lifeState, gender) {
    let lifeStatus = '';
    switch (lifeState) {
      case 'UNBORN':
        lifeStatus = constants[`UNBORN_${gender}_NODE`];
        break;
      case 'NOT_CARRIED_TO_TERM':
        lifeStatus = constants[`UNBORN_${gender}_DECEASED_NODE`];
        break;
      default:
        lifeStatus = '';
    }
    return lifeStatus;
  }



  /**
  @method getNodes - Filters UNION and FAMILY dummy nodes that were used
    to help position nodes that represent people.
  */
  getNodes() {
    return this.graph
      .nodes()
      .filter(id => this.graph.node(id) && this.findPerson(id))
      .map(id => {
        const { x, y } = this.graph.node(id);
        const { born, died, gender, name, lifeState } = this.findPerson(id);
        return {
          text: { born, died, name, lifeState },
          type: born ? constants[`ADULT_${gender}_NODE`] : this.getUnbornLifeState(lifeState, gender), 
          subtype: died ? constants.DECEASED_SUBTYPE : '',
          id, x, y
        };
      });
  }

  /**
  @method getLayout - Returns a list of nodes with their x & y position with
    a list of edges with a source and target id.

    Edges that have a target of mother/father are returned as an array.
  */
  getLayout() {
    const edges = this.getEdges();
    const nodes = this.getNodes();

    //window.console.log('\n');
    //window.console.log('edges into genogram', edges);
    //window.console.log('nodes into genogram', nodes);

    return {
      'edges': edges, //graph service supplies source/target id & edges type
      'nodes': nodes //graph service figures x/y position & node type
    };
  }

  isUnionAdded(personAKey, personBKey) {
    const union = this.unions.filter(union => {
      return (personAKey === union.personAKey || personAKey === union.personBKey)
        && (personBKey === union.personAKey || personBKey === union.personBKey);
    });
    return union.length === 1 ? true : false;
  }

  findPerson(key) {
    return this.people[key];
  }

  findUnion({ fatherKey = '', motherKey = '' }) {
    const union = this.unions.filter(union => {
      return (fatherKey === union.personAKey || fatherKey === union.personBKey)
        && (motherKey === union.personAKey || motherKey === union.personBKey);
    });
    return union && union.length === 1 ? union[0] : null;
  }

}



/**
 vertical direction
 ------------------
 https://stackoverflow.com/questions/39244799/how-to-get-horizontal-view-by-using-dagre

 layout
 ------
 http://www.samsarin.com/project/dagre-d3/latest/demo/clusters.html - dagre using clusters

 elbow stuff
 -----------
 http://jsfiddle.net/snowystinger/fttup8fb/17/
*/
