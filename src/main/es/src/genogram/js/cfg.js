'use strict';

class Endpoint{ 
  constructor() {
    this.saveUrl={
      method: 'put',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',        
        'Accept': 'application/json;charset=UTF-8'
      },
      data:{
      },
      responseType: 'json',
      responseStatus:200
    };
    
    this.loadUrl={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',        
        'Accept': 'application/vnd.olmgroup-usp.relationship.GenogramLayout+json;charset=UTF-8'
      },
      responseType: 'json',
      responseStatus:200
    };
    
    this.familialRelationshipsEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.relationshipsrecording.AsymmetricPersonPersonRelationshipResult+json'
      },
      responseType: 'json',
      responseStatus:200
    };
    
  }
}

export default new Endpoint();
