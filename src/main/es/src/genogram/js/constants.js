export const NODE_KEY = 'id'; // Key used to identify nodes

export const UNION_EDGE_CATEGORY = 'UNION';
export const ELBOW_EDGE_CATEGORY = 'ELBOW';

//edge types
export const EMPTY_EDGE_TYPE = 'emptyEdge';
export const SPECIAL_EDGE_TYPE = 'specialEdge';
export const PARENT_CHILD_EDGE = 'parentChildEdge';
export const PARENT_ADOPTED_CHILD_EDGE = 'parentAdoptedChildEdge';
export const DIVORCED_UNION_EDGE = 'divorcedEdge';
export const MARRIED_UNION_EDGE = 'marriedEdge';
export const SEPARATED_UNION_EDGE = 'separatedEdge';

//node types
export const ADULT_FEMALE_NODE = 'adultFemale';
export const ADULT_MALE_NODE = 'adultMale';
export const ADULT_UNKNOWN_NODE = 'adultUnknown';
export const ADULT_INDETERMINATE_NODE = 'adultIndeterminate';

export const UNBORN_FEMALE_NODE = 'adultFemale';
export const UNBORN_MALE_NODE = 'adultMale';
export const UNBORN_UNKNOWN_NODE = 'adultUnknown';
export const UNBORN_INDETERMINATE_NODE = 'adultIndeterminate';

export const UNBORN_FEMALE_DECEASED_NODE = 'unbornFemaleDeceased';
export const UNBORN_MALE_DECEASED_NODE  = 'unbornMaleDeceased';
export const UNBORN_UNKNOWN_DECEASED_NODE  = 'unbornUnknownDeceased';
export const UNBORN_INDETERMINATE_DECEASED_NODE  = 'unbornIndeterminateDeceased';

//node subtypes
export const DECEASED_SUBTYPE = 'deceasedSubtype';

//graph-service node key prefixes:
export const FAMILY_NODE_PREFIX = 'FAMILY';
export const UNION_NODE_PREFIX = 'UNION';
