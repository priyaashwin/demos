import React from 'react';
import * as constants from './constants';

const FemaleGender = (
  <symbol viewBox="0 0 100 100" id="adultFemale">
    <circle cx="50" cy="50" r="48"></circle>
  </symbol>
);

const MaleGender = (
  <symbol viewBox="-1 -1 100 100" id="adultMale">
    <rect width="96" height="96"></rect>
  </symbol>
);

const UnknownGender = (
  <symbol viewBox="0 0 100 100" id="adultUnknown">
    <polygon points="0,98 50,0 98,98" />
  </symbol>
);

const IndeterminateGender = (
  <symbol viewBox="0 0 100 100" id="adultIndeterminate">
    <polygon points="0,98 50,0 98,98" />
  </symbol>
);

const DeceasedSubtype = (
  <symbol viewBox="0 0 100 100" id="deceasedSubtype">
    <path stroke="red" strokeWidth="3px" strokeLinecap="round" d="m2 96 96 -96" />
  </symbol>
);

const DivorcedEdgeShape = (
  <symbol viewBox="0 0 50 50" id="divorcedEdge">
    <path stroke="black" d="m15 35 14 -18" className="union" />
    <path stroke="black" d="m5 35 14 -18" className="union" />
  </symbol>
);

const MarriedEdgeShape = (
  <symbol viewBox="0 0 50 50" id="marriedEdge" />
);

const SeparatedEdgeShape = (
  <symbol viewBox="0 0 50 50" id="separatedEdge">
    <path stroke="black" d="m20 35 14 -18" className="union" />
  </symbol>
);

const ParentChildShape = (
  <symbol viewBox="0 0 50 50" id="divorcedEdge" />
);

const ParentAdoptedChildShape = (
  <symbol viewBox="0 0 50 50" id="parentAdoptedChildEdge" style={{strokeDasharray:'10,10'}} />
);

const UnbornFemaleDeceasedGender = (
  <symbol viewBox="0 25 100 100" id="unbornFemaleDeceased">
    <circle cx="50" cy="50" r="24" />
    {/* <path stroke="red" strokeWidth="3px" strokeLinecap="round" d="m15 80 l80, -70" /> */}
    <path stroke="red" strokeWidth="3px" strokeLinecap="round" d="m33 67 33.5 -34" />
  </symbol>
);

const UnbornMaleDeceasedGender = (
  <symbol viewBox="-15 8.5 100 100" id="unbornMaleDeceased">
    <rect x="10" y="10" height="49" width="49" />
    <path stroke="red" strokeWidth="3px" strokeLinecap="round" d="m9 60 l60, -60" />
  </symbol>
);

const UnbornUnknownDeceasedGender = (
  <symbol viewBox="-25 0 100 100" id="unbornUnknownDeceased">
    <polygon points="0,48 25,0 48,48" />
    <path stroke="red" strokeWidth="3px" strokeLinecap="round" d="m0 48 48 -48" />
  </symbol>
);

const UnbornIndeterminateDeceasedGender = (
  <symbol viewBox="-25 0 100 100" id="unbornIndeterminateDeceased">
    <polygon points="0,48 25,0 48,48" />
    <path stroke="red" strokeWidth="3px" strokeLinecap="round" d="m0 48 48 -48" />
  </symbol>
);


export default {
  edgeCategories: {},
  edgeTypes: {
    [constants.MARRIED_UNION_EDGE]: {
      shapeId: `#${constants.MARRIED_UNION_EDGE}`,
      shape: MarriedEdgeShape
    },
    [constants.SEPARATED_UNION_EDGE]: {
      shapeId: `#${constants.SEPARATED_UNION_EDGE}`,
      shape: SeparatedEdgeShape
    },
    [constants.DIVORCED_UNION_EDGE]: {
      shapeId: `#${constants.DIVORCED_UNION_EDGE}`,
      shape: DivorcedEdgeShape
    },
    [constants.PARENT_CHILD_EDGE]: {
      shapeId: `#${constants.PARENT_CHILD_EDGE}`,
      shape: ParentChildShape
    },
    [constants.PARENT_ADOPTED_CHILD_EDGE]: {
      shapeId: `#${constants.PARENT_ADOPTED_CHILD_EDGE}`,
      shape: ParentAdoptedChildShape
    }
  },
  nodeTypes: {
    [constants.ADULT_FEMALE_NODE]: {
      shapeId: `#${constants.ADULT_FEMALE_NODE}`,
      shape: FemaleGender,
      typeText: ''
    },
    [constants.ADULT_MALE_NODE]: {
      shapeId: `#${constants.ADULT_MALE_NODE}`,
      shape: MaleGender,
      typeText: ''
    },
    [constants.ADULT_UNKNOWN_NODE]: {
      shapeId: `#${constants.ADULT_UNKNOWN_NODE}`,
      shape: UnknownGender,
      typeText: ''
    },
    [constants.ADULT_INDETERMINATE_NODE]: {
      shapeId: `#${constants.ADULT_INDETERMINATE_NODE}`,
      shape: IndeterminateGender,
      typeText: ''
    },
    [constants.UNBORN_FEMALE_NODE]: {
      shapeId: `#${constants.UNBORN_FEMALE_NODE}`,
      shape: FemaleGender,
      typeText: ''
    },
    [constants.UNBORN_MALE_NODE]: {
      shapeId: `#${constants.UNBORN_MALE_NODE}`,
      shape: MaleGender,
      typeText: ''
    },
    [constants.UNBORN_UNKNOWN_NODE]: {
      shapeId: `#${constants.UNBORN_UNKNOWN_NODE}`,
      shape: UnknownGender,
      typeText: ''
    },
    [constants.UNBORN_INDETERMINATE_NODE]: {
      shapeId: `#${constants.UNBORN_INDETERMINATE_NODE}`,
      shape: IndeterminateGender,
      typeText: ''
    },
    [constants.UNBORN_FEMALE_DECEASED_NODE]: {
      shapeId: `#${constants.UNBORN_FEMALE_DECEASED_NODE}`,
      shape: UnbornFemaleDeceasedGender,
      typeText: ''
    },
    [constants.UNBORN_MALE_DECEASED_NODE]: {
      shapeId: `#${constants.UNBORN_MALE_DECEASED_NODE}`,
      shape: UnbornMaleDeceasedGender,
      typeText: ''
    },
    [constants.UNBORN_UNKNOWN_DECEASED_NODE]: {
      shapeId: `#${constants.UNBORN_UNKNOWN_DECEASED_NODE}`,
      shape: UnbornUnknownDeceasedGender,
      typeText: ''
    },
    [constants.UNBORN_INDETERMINATE_DECEASED_NODE]: {
      shapeId: `#${constants.UNBORN_INDETERMINATE_DECEASED_NODE}`,
      shape: UnbornIndeterminateDeceasedGender,
      typeText: ''
    }
  },
  NodeSubtypes: {
    [constants.DECEASED_SUBTYPE]: {
      shapeId: `#${constants.DECEASED_SUBTYPE}`,
      shape: DeceasedSubtype
    }
  },
};
