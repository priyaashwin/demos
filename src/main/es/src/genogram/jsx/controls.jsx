import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

const steps = 100; // Slider steps
const defaultProps = {
  zoomMax: 0.15,
  zoomMin: 1.5
};

export default class GenogramControls extends Component {
  static defaultProps = defaultProps;
  static propTypes = {
    modifyZoom: PropTypes.func.isRequired,
    zoomMax: PropTypes.number,
    zoomMin: PropTypes.number,
    zoomLevel: PropTypes.number.isRequired,
    zoomToFit: PropTypes.func.isRequired,
    onSaveGenogram : PropTypes.func.isRequired,
    hasSavePermission : PropTypes.bool.isRequired
  };

  // Convert slider val (0-steps) to original zoom value range
  sliderToZoom(val){
    const { zoomMax, zoomMin } = this.props;
    return ((val) * (zoomMax-zoomMin)/steps ) + zoomMin;
  }

  // Convert zoom val (zoomMin-zoomMax) to slider range
  zoomToSlider(val){
    return (val - this.props.zoomMin) * steps/(this.props.zoomMax - this.props.zoomMin);
  }

  // Center graph-view on contents of svg > view
  zoomToFit(){
    this.props.zoomToFit();
  }

  // Modify current zoom of genogram-view
  zoom = e => {
    const sliderVal = e.target.value;
    const zoomLevelNext = this.sliderToZoom(sliderVal);
    const delta = zoomLevelNext-this.props.zoomLevel;

    if(zoomLevelNext <= this.props.zoomMax && zoomLevelNext >= this.props.zoomMin){
      this.props.modifyZoom(delta);
    }
  }

  handleKey = () => {
    const event = new CustomEvent('toggleKey', {
      bubbles:true,
      cancelable:true
    });
    ReactDOM.findDOMNode(this).dispatchEvent(event);
  }

  handleRefresh = () => {
    const event = new CustomEvent('refreshGenogram', {
      bubbles:true,
      cancelable:true
    });
    ReactDOM.findDOMNode(this).dispatchEvent(event);
  }

  addSaveButton() {
    
    const {onSaveGenogram, hasSavePermission} = this.props;

    if(hasSavePermission){
      return (
        <button className="button" onClick={onSaveGenogram}>
          <i className="fa fa-save" />
        </button>
      );
    }
  }

  render() {
    const { handleKey, handleRefresh } = this;
    const { zoomMax, zoomMin, zoomToFit } = this.props;
    return (
      <div id="genogram-controls">
        <span className="slider-wrapper">
          -
          <input id="typeinp" className="slider-input" step="1" type="range"
            min={this.zoomToSlider(zoomMin)}
            max={this.zoomToSlider(zoomMax)}
            value={this.zoomToSlider(this.props.zoomLevel)}
            onChange={this.zoom}
           />
          +
        </span>
        <button className="button" onMouseDown={zoomToFit}>
          <i className="fa fa-arrows-v fa-rotate-45" />
        </button>
        <button className="button" onClick={handleKey}>
          <i className="fa fa-key" />
        </button>
        <button className="button" onClick={handleRefresh}>
          <i className="fa fa-refresh" />
        </button>
        {this.addSaveButton()}
      </div>
    );
  }
}
