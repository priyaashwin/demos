import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GenogramFilter from './filter/genogram-filter';
import GenogramView from './genogram-view';
import GenogramConfig from '../js/genogram-config';
import GraphService from '../js/graph-service';
import * as constants from '../js/constants';
import Model from '../../model/model';
import endpoint from '../js/cfg';
import ReactDOM from 'react-dom';
import { getCancelToken } from 'usp-xhr';
import Mask from '../../mask/jsx/mask';

const defaultProps = {
  options: {
    dagre: {
      rankdir: 'TB',  // already the default 'T'op -> 'B'ottom (i.e vertical)
      ranksep: 40,    // distance between ranks (rows) default 50
    },
    dagreLib: {
      compound: true, // makes it a compound graph for clustering
    },
    nodeSize: {
      height: 150,
      width: 150
    }
  }
};

const EVT_INFO_MESSAGE = 'infoMessage:message';

export default class Genogram extends Component {
  static defaultProps = defaultProps;
  static propTypes = {
    subject: PropTypes.object,
    filter: PropTypes.object,
    subjectId: PropTypes.number.isRequired,
    data: PropTypes.array,
    permissions: PropTypes.shape({
      canSaveLayout: PropTypes.bool.isRequired
    }),
    saveUrl : PropTypes.string.isRequired,
    loadUrl : PropTypes.string.isRequired,
    options: PropTypes.shape({
      dagre: PropTypes.shape({
        rankdir: PropTypes.string,
        ranksep: PropTypes.number
      }),
      dagreLib: PropTypes.shape({
        compound: PropTypes.bool
      }),
      nodeSize: PropTypes.shape({
        height: PropTypes.number,
        width: PropTypes.number
      })
    })
  };

  constructor(props) {
    super(props);
    
    const { data, subject, options } = props;

    const selection = data.filter(person=>person.id===subject.id);

    this.graphService = new GraphService({
      data: selection,
      options: options
    });

    this.state = {
      graph: this.graphService.getLayout(),
      selection,
      selected: {},
      layout : undefined,
      loading : true
    };
  }

  componentDidMount() {
    this.loadGenogramLayout();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.graph.nodes.length !== prevState.graph.nodes.length) {
      this.loadGenogramLayout();
    }
  }

  componentWillUnmount(){
    if (this.cancelToken) {
      //cancel any outstanding xhr requests
      this.cancelToken.cancel();

      //remove property
      delete this.cancelToken;
    }
  }

  loadGenogramLayout = () => {
    //if there are any outstanding requests cancel them
    if (this.cancelToken) {
      //cancel any existing requests
      this.cancelToken.cancel();
    }

    //setup a new cancel token
    this.cancelToken = getCancelToken();

    const {loadUrl, subjectId} = this.props;
    const genogramModel = new Model(loadUrl);
    const cancelablePromise = (genogramModel.load(endpoint.loadUrl, {id:subjectId}, this.cancelToken));

    this.setState({loading : true}, () =>{
      cancelablePromise.then((success) => {
        this.setState((prevState)=>{
          
          const graph = prevState.graph;
          //layout data is saved/loaded as string
          const layout = JSON.parse(success.layout);


          if(typeof layout !== 'undefined'){
            graph.nodes.forEach( node =>  {
              const savedNode = layout.people.find( saved => node.id === saved.personId);
              if(typeof savedNode !== 'undefined'){
                node.x = savedNode.coordinates[0];
                node.y = savedNode.coordinates[1];
              }
            });
          }

          return {
            loading : false,
            layout : layout,
          };
        });
      }).catch((reject) => {
        this.setState({
          loading: false
        });
  
        //ignore the cases where a match does not exist : no layout was saved
        if(reject.code !==  404 && reject.exceptionType !== 'NotFoundException'){
          const event = new CustomEvent(EVT_INFO_MESSAGE, {
            bubbles: true,
            cancelable: true,
            detail: {
                ...reject,
                message: reject.messages[0]
            }
          });
        ReactDOM.findDOMNode(this).dispatchEvent(event);
        }
      });
    });
  }

  // Helper to find the index of a given edge
  getEdgeIndex(searchEdge) {
    return this.state.graph.edges.findIndex((edge)=>{
      return edge.source === searchEdge.source &&
        edge.target === searchEdge.target;
    });
  }

  // Helper to find the index of a given node
  getNodeIndex(searchNode) {
    return this.state.graph.nodes.findIndex((node)=>{
      return node[constants.NODE_KEY] === searchNode[constants.NODE_KEY];
    });
  }

  // Given a nodeKey, return the corresponding node
  getViewNode = (nodeKey) => {
    const i = this.getNodeIndex({
      [constants.NODE_KEY]: nodeKey
    });

    return this.state.graph.nodes[i];
  };

  /*
   * Handlers/Interaction
   */

  // Creates a new node between two edges
  onCreateEdge = (sourceViewNode, targetViewNode) => {
    const { graph } = this.state;
    const type = sourceViewNode.type===constants.ADULT_MALE_NODE
     ? constants.SPECIAL_EDGE_TYPE
     : constants.EMPTY_EDGE_TYPE;

    graph.edges.push({
     source: sourceViewNode[constants.NODE_KEY],
     target: targetViewNode[constants.NODE_KEY],
     type: type
    });

    this.setState({
     graph: graph
    });
  };

  // Updates the graph with a new node
  onCreateNode = (x,y) => {
    const graph = this.state.graph;

    // This is just an example - any sort of logic
    // could be used here to determine node type
    // There is also support for subtypes. (see 'sample' above)
    // The subtype geometry will underlay the 'type' geometry for a node
    const type = Math.random() < 0.25 ? constants.ADULT_MALE_NODE : constants.ADULT_FEMALE_NODE;

    const viewNode = {
     id: this.state.graph.nodes.length + 1,
     title: '',
     type: type,
     x: x,
     y: y
    };

    graph.nodes.push(viewNode);
    this.setState({graph: graph});
  };

  // Called when an edge is deleted
  onDeleteEdge = (viewEdge) => {
    const { graph } = this.state;
    const i = this.getEdgeIndex(viewEdge);
    graph.edges.splice(i, 1);
    this.setState({graph: graph, selected: {}});
  };

  // Deletes a node from the graph
  onDeleteNode = (viewNode) => {
    const { graph } = this.state;
    const i = this.getNodeIndex(viewNode);

    // Delete any connected edges
    const newEdges = graph.edges.filter(edge => {
      return edge.source!== viewNode[constants.NODE_KEY] && edge.target!==viewNode[constants.NODE_KEY];
    });

    graph.nodes.splice(i, 1);
    graph.edges = newEdges;

    this.setState({
      graph: graph,
      selected: {}
    });
  };

  // Edge 'mouseUp' handler
  onSelectEdge = (viewEdge) => {
    this.setState({selected: viewEdge});
  };

  // Node 'mouseUp' handler
  onSelectNode = (viewNode) => {
    // Deselect events will send Null viewNode
    if(viewNode){
      this.setState({selected: viewNode});
    } else{
      this.setState({selected: {}});
    }
  };

  // Called when an edge is reattached to a different target.
  onSwapEdge = (sourceViewNode, targetViewNode, viewEdge) => {
    const { graph } = this.state;
    const i = this.getEdgeIndex(viewEdge);
    const edge = JSON.parse(JSON.stringify(graph.edges[i]));

    edge.source = sourceViewNode[constants.NODE_KEY];
    edge.target = targetViewNode[constants.NODE_KEY];
    graph.edges[i] = edge;

    this.setState({graph: graph});
  };

  // Called by 'drag' handler, etc..
  // to sync updates from D3 with the graph
  onUpdateNode = (viewNode) => {
    const graph = this.state.graph;
    const i = this.getNodeIndex(viewNode);

    graph.nodes[i] = viewNode;
    this.setState({
      graph: graph
    });
  };

  onSaveGenogram = () => {

    if (this.cancelToken) {
      //cancel any existing requests
      this.cancelToken.cancel();
    }

    const {saveUrl, subjectId} = this.props;
    const genogramModel = new Model(saveUrl);
    const coordinateData = { people : [] };

    this.state.graph.nodes.forEach( node => {
      coordinateData.people.push({'personId' : node.id, 'coordinates' : [node.x, node.y]});
    });

    this.setState({loading: true});

    genogramModel.save(endpoint.saveUrl, {
      _type: 'UpdateGenogramLayout',
      personId:subjectId,
      layout : JSON.stringify(coordinateData)
    }, this.cancelToken).then((success) => {
      this.setState({loading: false});
      const event = new CustomEvent(EVT_INFO_MESSAGE, {
        bubbles: true,
        cancelable: true,
        detail: {
          ...success,
          message: 'Genogram layout successfully saved'
        }
      });
      ReactDOM.findDOMNode(this).dispatchEvent(event);
    }).catch((reject) => {
      this.setState({loading: false});

      const event = new CustomEvent(EVT_INFO_MESSAGE, {
        bubbles: true,
        cancelable: true,
        detail: {
          ...reject,
          message: reject.messages[0]
        }
      });
      ReactDOM.findDOMNode(this).dispatchEvent(event);
    });

  }
  
  onFilterUpdate = (selection) => {
    this.graphService = new GraphService({
      data: selection,
      options: this.props.options
    });

    this.setState({
      graph: this.graphService.getLayout(),
      selected: {},
      selection
    });
  };

  render() {
    const { options, permissions, data, subject, filter } = this.props;
    const { graph, selected, layout, loading, selection } = this.state;
    const { edgeTypes, nodeTypes, NodeSubtypes } = GenogramConfig;

    if(!graph.nodes) return null;

    return (
      <div>
        <div id="genogram-filter">
          <GenogramFilter
            data={data}
            subject={subject}
            filter={filter}
            selection={selection}
            onFilterUpdate={this.onFilterUpdate}
          />
        </div>
        <div id="genogram-graph">
          <div id="genogram-mask">
            <Mask show={loading}/>      
          </div>
          <GenogramView
            ref={(el) => this.GenogramView = el}
            background={false}
            emptyType={constants.ADULT_FEMALE_NODE}  // not needed? May need for a neutral gender
            edgeCategories={true}
            edgeTypes={edgeTypes}
            getViewNode={this.getViewNode}
            nodeKey={constants.NODE_KEY}
            nodeTypes={nodeTypes}
            nodeSubtypes={NodeSubtypes}
            onCreateEdge={this.onCreateEdge}
            onCreateNode={this.onCreateNode}
            onDeleteEdge={this.onDeleteEdge}
            onDeleteNode={this.onDeleteNode}
            onSelectEdge={this.onSelectEdge}
            onSelectNode={this.onSelectNode}
            onSwapEdge={this.onSwapEdge}
            onUpdateNode={this.onUpdateNode}
            onSaveGenogram={this.onSaveGenogram}
            ranksep={options.dagre.ranksep}
            selected={selected}
            layout={layout}
            permissions={permissions}
            {...graph}
          />
        </div>
      </div>
    );
  }

}
