import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Radium from 'radium';
import * as d3 from 'd3';
import * as constants from '../js/constants';
import GenogramControls from './controls';
import { GenogramBackground } from './background';
import { GenogramGrid } from './grid';

function styleToString(style) {
    return Object.keys(style)
        .map(function(k) {
            const key = k.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
            return `${key}:${style[k]}`;
        })
        .join(';');
}

function makeStyles({ dark = '#E1E1E1', light = 'white', primary = 'dodgerblue' }) {
    const styles = {
        wrapper: {
            base: {
                height: '100%',
                margin: 0,
                display: 'flex',
                boxShadow: 'none',
                background: '#FFF'
            },
            focused: {
                opacity: 1
            }
        },
        svg: {
            base: {
                alignContent: 'stretch',
                flex: 1
            }
        },
        node: {
            base: {
                color: primary,
                stroke: dark,
                fill: light,
                strokeWidth: '1px',
                cursor: 'pointer'
            },
            selected: {
                color: light,
                stroke: primary,
                fill: primary
            }
        },
        shape: {
            fill: 'inherit',
            stroke: dark,
            strokeWidth: '1px'
        },
        text: {
            base: {
                fill: dark,
                stroke: dark
            },
            selected: {
                fill: dark,
                stroke: dark
            }
        },
        edge: {
            base: {
                color: light,
                stroke: dark,
                strokeWidth: '2px',
                cursor: 'pointer',
                fill: 'none'
            },
            selected: {
                color: dark,
                stroke: dark
            }
        },
        arrow: {
            fill: primary
        }
    };

    // Styles need to be strings for D3 to apply them all at once
    styles.node.baseString = styleToString(styles.node.base);
    styles.node.selectedString = styleToString({ ...styles.node.base, ...styles.node.selected });
    styles.text.baseString = styleToString(styles.text.base);
    styles.text.selectedString = styleToString({ ...styles.text.base, ...styles.text.selected });
    styles.edge.baseString = styleToString(styles.edge.base);
    styles.edge.selectedString = styleToString({ ...styles.edge.base, ...styles.edge.selected });

    return styles;
}

// any objects with x & y properties
function getTheta(pt1, pt2) {
    const xComp = pt2.x - pt1.x;
    const yComp = pt2.y - pt1.y;
    return Math.atan2(yComp, xComp);
}

function getMidpoint(pt1, pt2) {
    const x = (pt2.x + pt1.x) / 2;
    const y = (pt2.y + pt1.y) / 2;

    return { x: x, y: y };
}

class GenogramView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            enableFocus: props.enableFocus || false, // Enables focus/unfocus
            edgeSwapQueue: [], // Stores nodes to be swapped
            focused: true,
            readOnly: props.readOnly || false,
            selectionChanged: false,
            styles: makeStyles(props.palette),
            viewTransform: d3.zoomIdentity
        };

        this.zoom = d3
            .zoom()
            .scaleExtent([props.zoomMin, props.zoomMax])
            .on('zoom', this.handleZoom);

        this.styles = makeStyles(props.palette);
    }

    componentDidMount() {
        // Window event listeners for keypresses
        // and to control blur/focus of graph
        d3.select(window)
            .on('keydown', this.handleWindowKeydown)
            .on('click', this.handleWindowClicked);

        d3.select(this.viewWrapper)
            .on('touchstart', this.containZoom)
            .on('touchmove', this.containZoom)
            .on('click', this.handleSvgClicked)
            .select('svg')
            .call(this.zoom);

        // On the initial load, the 'view' <g> doesn't exist
        // until componentDidMount. Manually render the first view.
        this.runZoomToFit = true;
        this.renderView();
    }

    componentWillUnmount() {
        // Remove window event listeners
        d3.select(window)
            .on('keydown', null)
            .on('click', null);
    }

    componentDidUpdate(prevProps) {
        let selectionChanged = false;
        const selected = this.props.selected;

        if (selected !== prevProps.selected) {
            selectionChanged = true;
        }

        let selectionType = null;
        if (this.props.selected && this.props.selected.source) {
            selectionType = 'edge';
        } else if (this.props.selected && this.props.selected[this.props.nodeKey]) {
            selectionType = 'node';
        }

        if (
            selectionType !== this.state.selectionType ||
            selectionChanged ||
            this.props.readOnly !== this.state.readOnly
        ) {
            // eslint-disable-next-line react/no-did-update-set-state
            this.setState({
                selectionChanged: selectionChanged,
                selectionType: selectionType,
                readOnly: this.props.readOnly || false
            });
        }

        if (prevProps.nodes.length !== this.props.nodes.length) {
            this.runZoomToFit = true;
            this.runZoomToFitWithDelay = true;
        }
    }

    /*
     * Handlers/Interaction
     */

    hideEdge = edgeDOMNode => {
        d3.select(edgeDOMNode).attr('opacity', 0);
    };

    showEdge = edgeDOMNode => {
        d3.select(edgeDOMNode).attr('opacity', 1);
    };

    canSwap = (sourceNode, hoveredNode, swapEdge) => {
        return (
            swapEdge.source !== sourceNode[this.props.nodeKey] || swapEdge.target !== hoveredNode[this.props.nodeKey]
        );
    };

    containZoom = () => {
        d3.event.preventDefault();
    };

    drawEdge = (sourceNode, target, swapErrBack) => {
        const self = this;
        const dragEdge = d3.select(this.entities).append('svg:path');

        dragEdge
            .attr('class', 'link dragline')
            .attr('style', this.styles.edge.selectedString)
            .attr('d', GenogramView.getPathDescriptionStr(sourceNode.x, sourceNode.y, target.x, target.y));

        d3.event.on('drag', dragged).on('end', ended);

        function dragged() {
            dragEdge.attr('d', GenogramView.getPathDescriptionStr(sourceNode.x, sourceNode.y, d3.event.x, d3.event.y));
        }

        function ended() {
            dragEdge.remove();

            const swapEdge = self.state.edgeSwapQueue.shift();
            const hoveredNode = self.state.hoveredNode;

            self.setState({
                edgeSwapQueue: self.state.edgeSwapQueue,
                drawingEdge: false
            });

            if (hoveredNode && self.props.canCreateEdge(sourceNode, hoveredNode)) {
                if (swapEdge) {
                    if (self.props.canDeleteEdge(swapEdge) && self.canSwap(sourceNode, hoveredNode, swapEdge)) {
                        self.props.onSwapEdge(sourceNode, hoveredNode, swapEdge);
                    } else {
                        swapErrBack();
                    }
                } else {
                    self.props.onCreateEdge(sourceNode, hoveredNode);
                }
            } else {
                if (swapErrBack) {
                    swapErrBack();
                }
            }
        }
    };

    dragNode = () => {
        const self = this;
        const el = d3.select(d3.event.target.parentElement); // Enclosing 'g' element

        function ended() {
            el.classed('dragging', false);

            if (!self.state.readOnly) {
                const d = d3.select(this).datum();
                self.props.onUpdateNode(d);
            }

            // For some reason, mouseup isn't firing
            // - manually firing it here
            const evt = new CustomEvent('mouseup', {
                bubbles: true,
                cancelable: true
            });
            d3.select(this)
                .node()
                .dispatchEvent(evt);
        }

        function dragged() {
            if (self.state.readOnly) return;

            d3.select(this).attr('transform', function(d) {
                d.x += d3.event.dx;
                d.y += d3.event.dy;
                return 'translate(' + d.x + ',' + d.y + ')';
            });

            self.render();
        }

        el.classed('dragging', true);
        d3.event.on('drag', dragged).on('end', ended);
    };

    handleDelete = () => {
        if (this.state.readOnly) return;

        if (this.props.selected) {
            const selected = this.props.selected;
            if (this.state.selectionType === 'node' && this.props.canDeleteNode(selected)) {
                this.props.onDeleteNode(selected);
                this.props.onSelectNode(null);
            } else if (this.state.selectionType === 'edge' && this.props.canDeleteEdge(selected)) {
                this.props.onDeleteEdge(selected);
                this.props.onSelectNode(null);
            }
        }
    };

    handleEdgeDrag = datum => {
        const { drawingEdge, readOnly } = this.state;
        const { parentElement } = d3.event.target;

        if (drawingEdge && !readOnly) {
            const sourceNode = this.props.getViewNode(datum.source);
            const xycoords = d3.mouse(d3.event.target);
            const target = { x: xycoords[0], y: xycoords[1] };

            this.hideEdge(parentElement);
            this.drawEdge(sourceNode, target, this.showEdge.bind(this, parentElement));
        }
    };

    handleEdgeMouseDown = datum => {
        if (this.state.readOnly) {
            this.props.onSelectEdge(datum);
            this.setState({
                focused: true
            });
        }
    };

    handleNodeDrag = () => {
        if (this.state.drawingEdge && !this.state.readOnly) {
            this.drawEdge(d3.event.subject, {
                x: d3.event.subject.x,
                y: d3.event.subject.y
            });
        } else {
            this.dragNode();
        }
    };

    handleNodeMouseDown = () => {
        if (d3.event.defaultPrevented) return; // dragged

        if (d3.event.shiftKey) {
            this.setState({
                selectingNode: true,
                drawingEdge: true,
                focused: true
            });
        } else {
            this.setState({
                selectingNode: true,
                focused: true
            });
        }
    };

    handleNodeMouseUp = datum => {
        if (this.state.selectingNode) {
            this.props.onSelectNode(datum);
            this.setState({
                selectingNode: false
            });
        }
    };

    handleNodeMouseEnter = datum => {
        if (this.state.hoveredNode !== datum) {
            this.setState({
                hoveredNode: datum
            });
        }
    };

    handleNodeMouseLeave = datum => {
        // For whatever reason, mouseLeave is fired when edge dragging ends
        // (and mouseup is not fired). This clears the hoverNode state prematurely
        // resulting in swapEdge failing to fire.
        // Detecting & ignoring mouseLeave events that result from drag ending here
        //eslint-disable-next-line
        const fromMouseup = d3.event.which === 1;
        if (this.state.hoveredNode === datum && !fromMouseup) {
            this.setState({
                hoveredNode: null
            });
        }
    };

    handleSvgClicked = () => {
        if (!this.state.focused) {
            this.setState({
                focused: true
            });
        }

        if (this.state.selectingNode) {
            this.setState({
                selectingNode: false
            });
        } else {
            this.props.onSelectNode(null);

            if (!this.state.readOnly && d3.event.shiftKey) {
                const xycoords = d3.mouse(d3.event.target);
                this.props.onCreateNode(xycoords[0], xycoords[1]);
            }
        }
    };

    handleWindowClicked = () => {
        const { enableFocus, focused } = this.state;
        const { ownerSVGElement } = d3.event.target;

        if (enableFocus && focused && !ownerSVGElement) {
            this.setState({
                focused: false
            });
        }
    };

    handleWindowKeydown = () => {
        if (this.state.focused) {
            switch (d3.event.key) {
                case 'Delete':
                    this.handleDelete();
                    break;
                case 'Backspace':
                    this.handleDelete();
                    break;
                default:
                    break;
            }
        }
    };

    handleZoom = () => {
        if (this.state.focused) {
            this.setState({
                viewTransform: d3.event.transform
            });
        }
    };

    handleZoomToFit = () => {
        const parent = d3.select(this.viewWrapper).node();
        const entities = d3.select(this.entities).node();
        const viewBBox = entities.getBBox();
        const height = parent.clientHeight;
        const width = parent.clientWidth;

        let dx, dy, x, y;
        const translate = [this.state.viewTransform.x, this.state.viewTransform.y];
        const next = { x: translate[0], y: translate[1], k: this.state.viewTransform.k };

        if (viewBBox.width > 0 && viewBBox.height > 0) {
            // There are entities
            dx = viewBBox.width;
            dy = viewBBox.height;
            x = viewBBox.x + viewBBox.width / 2;
            y = viewBBox.y + viewBBox.height / 2;

            next.k = 0.9 / Math.max(dx / width, dy / height);

            if (next.k < this.props.zoomMin) {
                next.k = this.props.zoomMin;
            } else if (next.k > this.props.zoomMax) {
                next.k = this.props.zoomMax;
            }

            next.x = width / 2 - next.k * x;
            next.y = height / 2 - next.k * y;

            this.runZoomToFit = false;
            this.runZoomToFitWithDelay = false;
        } else if (!this.firstDraw) {
            next.k = (this.props.zoomMin + this.props.zoomMax) / 2;
            next.x = 0;
            next.y = 0;
        }

        this.setZoom(next.k, next.x, next.y, this.props.zoomDur);
    };

    // Updates current viewTransform with some delta
    modifyZoom = (modK = 0, modX = 0, modY = 0, dur = 0) => {
        const parent = d3.select(this.viewWrapper).node();
        const width = parent.clientWidth;
        const height = parent.clientHeight;

        const center = [width / 2, height / 2],
            extent = this.zoom.scaleExtent(),
            translate = [this.state.viewTransform.x, this.state.viewTransform.y],
            next = { x: translate[0], y: translate[1], k: this.state.viewTransform.k };

        const target_zoom = next.k * (1 + modK);

        if (target_zoom < extent[0] || target_zoom > extent[1]) {
            return false;
        }

        const translate0 = [(center[0] - next.x) / next.k, (center[1] - next.y) / next.k];

        next.k = target_zoom;

        const l = [translate0[0] * next.k + next.x, translate0[1] * next.k + next.y];

        next.x += center[0] - l[0] + modX;
        next.y += center[1] - l[1] + modY;

        this.setZoom(next.k, next.x, next.y, dur);
    };

    // Programmatically resets zoom
    setZoom = (k = 1, x = 0, y = 0, dur = 0) => {
        const t = d3.zoomIdentity.translate(x, y).scale(k);

        d3.select(this.viewWrapper)
            .select('svg')
            .transition()
            .duration(dur)
            .call(this.zoom.transform, t);
    };

    /*
     * Render methods
     */

    getElbowPoints(sourceX, sourceY, targetX, targetY) {
        const { ranksep } = this.props;
        return [
            `${sourceX},${sourceY}`,
            `${sourceX},${sourceY - ranksep}`,
            `${targetX},${sourceY - ranksep}`,
            `${targetX},${targetY}`
        ].join(',');
    }

    // Returns the svg's path.d' (geometry description) string from edge data
    // edge.source and edge.target are node ids
    static getPathDescriptionStr(sourceX, sourceY, targetX, targetY) {
        return `M${sourceX},${sourceY}L${targetX},${targetY}`;
    }

    getPathDescription = edge => {
        const { getViewNode, nodeSize } = this.props;
        const off = nodeSize / 2; // from the center of the node to the perimeter
        const src = getViewNode(edge.source);
        const trg = Array.isArray(edge.target)
            ? this.getPathMidpoint(edge.target[0], edge.target[1])
            : getViewNode(edge.target);

        if (edge.category === constants.ELBOW_EDGE_CATEGORY && src && trg) {
            return this.getElbowPoints(src.x, src.y - off, trg.x, trg.y);
        } else if (src && trg) {
            const theta = getTheta(src, trg);
            const xOff = off * Math.cos(theta);
            const yOff = off * Math.sin(theta);
            return GenogramView.getPathDescriptionStr(src.x + xOff, src.y + yOff, trg.x - xOff, trg.y - yOff);
        }
        window.console.warn('Unable to get source or target for ', edge);
        return '';
    };

    getPathMidpoint = (source, target) => {
        const pathEl = d3
            .select(this.entities)
            .selectAll('g.edge')
            .filter(d => d.source === source && d.target === target)
            .select('path')
            .node();

        return pathEl ? pathEl.getPointAtLength(pathEl.getTotalLength() / 2) : {};
    };

    getEdgeHandleTransformation = edge => {
        const src = this.props.getViewNode(edge.source);
        const trg = this.props.getViewNode(edge.target);

        const origin = getMidpoint(src, trg);

        const x = origin.x;
        const y = origin.y;
        const theta = (getTheta(src, trg) * 180) / Math.PI;
        const offset = -this.props.edgeHandleSize / 2;

        return `translate(${x}, ${y}) rotate(${theta}) translate(${offset}, ${offset})`;
    };

    // Returns a d3 transformation string from node data
    getNodeTransformation = node => {
        return 'translate(' + node.x + ',' + node.y + ')';
    };

    getEdgeStyle = (d, selected) => {
        const styles = this.styles;
        return d === selected ? styles.edge.selectedString : styles.edge.baseString;
    };

    getCustomEdgeStyle = props => {
        let lCasePropName;
        return Object.entries(props.style).reduce((styleString, [propName, propValue]) => {
            lCasePropName = propName.replace(/([A-Z])/g, matches => `-${matches[0].toLowerCase()}`);
            return `${styleString}${lCasePropName}:${propValue};`;
        }, '');
    };

    getElbowStyle = (d, selected) => {
        const styles = this.styles;
        return d === selected ? styles.elbow.selectedString : styles.elbow.baseString;
    };

    getNodeStyle = (d, selected) => {
        const styles = this.styles;
        return d === selected ? styles.node.selectedString : styles.node.baseString;
    };

    getTextStyle = (d, selected) => {
        const styles = this.styles;
        return d === selected ? styles.text.selectedString : styles.text.baseString;
    };

    renderBackground() {
        const { background, gridSize } = this.props;

        if (background !== true) return background;
        return <GenogramBackground gridSize={gridSize} />;
    }

    renderControls() {
        const { controls, palette, zoomMax, zoomMin, onSaveGenogram, permissions } = this.props;

        if (controls !== true) return controls;
        return (
            <GenogramControls
                primary={palette.primary}
                zoomMin={zoomMin}
                zoomMax={zoomMax}
                zoomLevel={this.state.viewTransform.k}
                zoomToFit={this.handleZoomToFit}
                modifyZoom={this.modifyZoom}
                onSaveGenogram={onSaveGenogram}
                hasSavePermission={permissions.canSaveLayout}
            />
        );
    }

    renderDefs() {
        const { edgeTypes, nodeTypes, nodeSubtypes } = this.props;
        let defIndex = 0;
        const graphConfigDefs = [];

        Object.keys(nodeTypes).forEach(function(type) {
            defIndex += 1;
            graphConfigDefs.push(React.cloneElement(nodeTypes[type].shape, { key: defIndex }));
        });

        Object.keys(nodeSubtypes).forEach(function(type) {
            defIndex += 1;
            graphConfigDefs.push(React.cloneElement(nodeSubtypes[type].shape, { key: defIndex }));
        });

        Object.keys(edgeTypes).forEach(function(type) {
            defIndex += 1;
            graphConfigDefs.push(React.cloneElement(edgeTypes[type].shape, { key: defIndex }));
        });

        return (
            <defs>
                {graphConfigDefs}
                {this.renderGrid()}
            </defs>
        );
    }

    renderGrid = () => {
        const { grid, gridDot, gridSpacing } = this.props;

        if (grid !== true) return grid;
        return <GenogramGrid gridDot={gridDot} gridSpacing={gridSpacing} />;
    };

    renderEdges(entities, edges) {
        const { renderEdge } = this;
        const { transitionTime } = this.props;

        // Join Data
        edges = entities
            .selectAll('g.edge')
            .filter(d => d.category === constants.UNION_EDGE_CATEGORY)
            .data(edges, function(d) {
                // IMPORTANT: this snippet allows D3 to detect updated vs. new data
                return `${d.source}:${d.target}`;
            });

        // Remove Old
        edges.exit().remove();

        // Add New
        const newEdges = edges
            .enter()
            .append('g')
            .classed('edge', true);

        newEdges.on('mousedown', this.handleEdgeMouseDown).call(d3.drag().on('start', this.handleEdgeDrag));

        newEdges
            .attr('opacity', 0)
            .transition()
            .duration(transitionTime)
            .attr('opacity', 1);

        // Merge
        edges.enter().merge(edges);

        // Update All
        edges.each(function(datum) {
            renderEdge(this, datum);
        });
    }

    renderElbows(entities, edges) {
        const { renderElbow } = this;
        const { transitionTime } = this.props;

        // Join Data
        edges = entities
            .selectAll('g.edge')
            .filter(d => d.category === constants.ELBOW_EDGE_CATEGORY)
            .data(edges, function(d) {
                // IMPORTANT: this snippet allows D3 to detect updated vs. new data
                return `${d.source}:${d.target}`;
            });

        // Remove Old
        edges.exit().remove();

        // Add New
        const newEdges = edges
            .enter()
            .insert('g', ':first-child')
            .classed('edge', true);

        newEdges
            .attr('opacity', 0)
            .transition()
            .duration(transitionTime)
            .attr('opacity', 1);

        // Merge
        edges.enter().merge(edges);

        // Update All
        edges.each(function(d, i, els) {
            renderElbow(this, d, i, els);
        });
    }

    renderNodes(entities, nodes) {
        const { renderNode, renderText } = this;
        const { nodeKey, transitionTime } = this.props;

        // Join Data
        nodes = entities.selectAll('g.node').data(nodes, function(d) {
            // IMPORTANT: this snippet allows D3 to detect updated vs. new data
            return d[nodeKey];
        });

        // Animate/Remove Old
        nodes
            .exit()
            .transition()
            .duration(transitionTime)
            .attr('opacity', 0)
            .remove();

        // Add New
        const newNodes = nodes
            .enter()
            .append('g')
            .classed('node', true);

        newNodes
            .on('mousedown', this.handleNodeMouseDown)
            .on('mouseup', this.handleNodeMouseUp)
            .on('mouseenter', this.handleNodeMouseEnter)
            .on('mouseleave', this.handleNodeMouseLeave)
            .call(d3.drag().on('start', this.handleNodeDrag));

        newNodes
            .attr('opacity', 0)
            .transition()
            .duration(transitionTime)
            .attr('opacity', 1);

        // Merge
        nodes.enter().merge(nodes);

        // Update All
        nodes.each(function(datum) {
            renderNode(this, datum);
            renderText(this, datum);
        });
    }

    renderEdge = (domNode, datum) => {
        const { edgeHandleSize, edgeTypes, selected } = this.props;
        const style = this.getEdgeStyle(datum, selected);
        const trans = this.getEdgeHandleTransformation(datum);

        // For new edges, add necessary child domNodes
        if (!domNode.hasChildNodes()) {
            d3.select(domNode).append('path');
            d3.select(domNode).append('use');
        }

        d3.select(domNode)
            .attr('style', style)
            .select('use')
            .attr('xlink:href', function(datum) {
                return edgeTypes[datum.type].shapeId;
            })
            .attr('width', edgeHandleSize)
            .attr('height', edgeHandleSize)
            .attr('transform', trans);

        d3.select(domNode)
            .select('path')
            .attr('d', this.getPathDescription);
    };

    renderElbow = (domNode, datum) => {
        const { edgeHandleSize, edgeTypes, selected } = this.props;
        let style = this.getEdgeStyle(datum, selected);
        const target = Array.isArray(datum.target)
            ? this.getPathMidpoint(datum.target[0], datum.target[1])
            : datum.target;

        if (!domNode.hasChildNodes()) {
            d3.select(domNode).append('polyline');
            d3.select(domNode).append('use');
        }

        const { props } = edgeTypes[datum.type].shape;

        if (Object.prototype.hasOwnProperty.call(props, 'style')) {
            style += ';' + this.getCustomEdgeStyle(props);
        }

        d3.select(domNode)
            .attr('style', style)
            .select('use')
            .attr('xlink:href', function(datum) {
                return edgeTypes[datum.type].shapeId;
            })
            .attr('width', edgeHandleSize)
            .attr('height', edgeHandleSize);

        d3.select(domNode)
            .select('polyline')
            .attr('points', datum => this.getPathDescription(datum, target));
    };

    renderNode = (domNode, datum) => {
        const { nodeSize, nodeTypes, nodeSubtypes, selected } = this.props;
        const style = this.getNodeStyle(datum, selected);

        // For new nodes, add necessary child domNodes
        if (!domNode.hasChildNodes()) {
            d3.select(domNode)
                .append('use')
                .classed('shape', true)
                .attr('x', -nodeSize / 2)
                .attr('y', -nodeSize / 2)
                .attr('width', nodeSize)
                .attr('height', nodeSize);

            d3.select(domNode)
                .append('use')
                .classed('subtypeShape', true)
                .attr('x', -nodeSize / 2)
                .attr('y', -nodeSize / 2)
                .attr('width', nodeSize)
                .attr('height', nodeSize);
        }

        d3.select(domNode).attr('style', style);

        d3.select(domNode)
            .select('use.subtypeShape')
            .attr('xlink:href', function(d) {
                return d.subtype ? nodeSubtypes[d.subtype].shapeId : null;
            });

        d3.select(domNode)
            .select('use.shape')
            .attr('xlink:href', function(d) {
                return d.type ? nodeTypes[d.type].shapeId : null;
            });

        d3.select(domNode).attr('transform', this.getNodeTransformation);
    };

    renderText = (domNode, datum) => {
        const { emptyType, maxTitleChars, selected } = this.props;
        const { born, died, name, lifeState } = datum.text;
        const d3Node = d3.select(domNode);
        const style = this.getTextStyle(datum, selected);
        const lineOffset = 18;

        let nameText;

        d3Node.selectAll('text').remove();

        const el = d3Node
            .append('text')
            .attr('text-anchor', 'middle')
            .attr('style', style)
            .attr('dy', datum.type === emptyType ? -9 : 18);

        if (name) {
            nameText = name.length <= maxTitleChars ? name : `${name.substring(0, maxTitleChars)}...`;
            if (lifeState === 'NOT_CARRIED_TO_TERM') {
                el.append('tspan')
                    .text(nameText)
                    .attr('x', 0)
                    .attr('dy', 32);
            } else {
                el.append('tspan')
                    .text(nameText)
                    .attr('x', 0)
                    .attr('dy', lineOffset + 72);
            }
            el.append('title').text(name);
        }

        if (born) {
            el.append('tspan')
                .text(born)
                .attr('x', 0)
                .attr('dy', lineOffset);
            el.append('title').text(born);
        }

        if (died) {
            el.append('tspan')
                .text(died)
                .attr('x', 0)
                .attr('dy', lineOffset);
            el.append('title').text(died);
        }
    };

    renderView() {
        const { edges, nodes, zoomDelay } = this.props;

        const entities = d3.select(this.entities);
        const view = d3.select(this.view);

        // Update the view w/ new zoom/pan
        view.attr('transform', this.state.viewTransform);

        this.renderEdges(
            entities,
            edges.filter(d => d.category === constants.UNION_EDGE_CATEGORY)
        );
        this.renderElbows(
            entities,
            edges.filter(d => d.category === constants.ELBOW_EDGE_CATEGORY)
        );
        this.renderNodes(entities, nodes);

        if (this.runZoomToFit) {
            if (this.runZoomToFitWithDelay) {
                setTimeout(
                    function() {
                        this.handleZoomToFit();
                    }.bind(this),
                    zoomDelay
                );
            } else {
                setTimeout(
                    function() {
                        this.handleZoomToFit();
                    }.bind(this),
                    500
                );
            }
        }
    }

    render() {
        const { focused } = this.state;
        const styles = this.styles;

        const style = [styles.wrapper.base, !!focused && styles.wrapper.focused, this.props.style];

        // Render 'graph' into 'this.view' element, all DOM updates
        // within 'view' are managed by D3
        this.renderView();

        return (
            <div id="viewWrapper" ref={el => (this.viewWrapper = el)} style={style}>
                <svg id="svgRoot" style={styles.svg.base}>
                    {this.renderDefs()}
                    <g id="view" ref={el => (this.view = el)}>
                        {this.renderBackground()}
                        <g id="entities" ref={el => (this.entities = el)} />
                    </g>
                </svg>
                {this.renderControls()}
            </div>
        );
    }
}

GenogramView.propTypes = {
    background: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
    controls: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
    canCreateEdge: PropTypes.func,
    canDeleteEdge: PropTypes.func,
    canDeleteNode: PropTypes.func,
    edges: PropTypes.array.isRequired,
    edgeTypes: PropTypes.object.isRequired,
    edgeHandleSize: PropTypes.number,
    emptyType: PropTypes.string.isRequired,
    enableFocus: PropTypes.bool,
    grid: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
    gridDot: PropTypes.number,
    gridSize: PropTypes.number, // The point grid is fixed
    gridSpacing: PropTypes.number,
    getViewNode: PropTypes.func.isRequired,
    maxTitleChars: PropTypes.number, // Per line.
    nodes: PropTypes.array.isRequired,
    nodeKey: PropTypes.string.isRequired,
    nodeTypes: PropTypes.object.isRequired,
    nodeSize: PropTypes.number,
    nodeSubtypes: PropTypes.object.isRequired,
    onCreateEdge: PropTypes.func.isRequired,
    onCreateNode: PropTypes.func.isRequired,
    onDeleteEdge: PropTypes.func.isRequired,
    onDeleteNode: PropTypes.func.isRequired,
    onSelectEdge: PropTypes.func.isRequired,
    onSelectNode: PropTypes.func.isRequired,
    onSwapEdge: PropTypes.func.isRequired,
    onUpdateNode: PropTypes.func.isRequired,
    palette: PropTypes.shape({
        background: PropTypes.string,
        dark: PropTypes.string,
        light: PropTypes.string,
        primary: PropTypes.string
    }),
    ranksep: PropTypes.number,
    readOnly: PropTypes.bool,
    selected: PropTypes.object,
    style: PropTypes.object,
    transitionTime: PropTypes.number, // D3 Enter/Exit duration
    zoomDur: PropTypes.number, // ms
    zoomDelay: PropTypes.number, // ms
    zoomMax: PropTypes.number,
    zoomMin: PropTypes.number,
    onSaveGenogram: PropTypes.func.isRequired,
    permissions: PropTypes.shape({
        canSaveLayout: PropTypes.bool.isRequired
    })
};

GenogramView.defaultProps = {
    background: true,
    controls: true,
    edgeHandleSize: 50,
    grid: true,
    gridDot: 2,
    gridSize: 40960,
    gridSpacing: 36,
    maxTitleChars: 20,
    nodeSize: 150,
    palette: {
        background: '#F9F9F9',
        dark: '#343434',
        light: '#FFFFFF',
        primary: 'dodgerblue'
    },
    ranksep: 40,
    readOnly: false,
    selected: {},
    transitionTime: 150,
    zoomDur: 750,
    zoomDelay: 1500,
    zoomMax: 1.5,
    zoomMin: 0.15,
    canDeleteNode: () => true,
    canCreateEdge: () => true,
    canDeleteEdge: () => true
};

export default Radium({ userAgent: 'all' })(GenogramView);
