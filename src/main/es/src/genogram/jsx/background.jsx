import React from 'react';
import PropTypes from 'prop-types';

export const GenogramBackground = ({ className, fill, gridSize }) =>
  <rect className={className}
    fill={fill}
    height={gridSize}
    width={gridSize}
    x={-gridSize/4}
    y={-gridSize/4}
  />;

GenogramBackground.defaultProps = {
  className: 'background',
  fill: 'url(#grid)'
};

GenogramBackground.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  gridSize: PropTypes.number.isRequired
};
