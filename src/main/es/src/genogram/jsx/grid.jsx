import React from 'react';
import PropTypes from 'prop-types';

export const GenogramGrid = ({ fill, id, gridDot, gridSpacing, patternUnits }) =>
  <pattern id={id} patternUnits={patternUnits} height={gridSpacing} width={gridSpacing}>
    <circle cx={gridSpacing/2} cy={gridSpacing/2} r={gridDot} fill={fill} />
  </pattern>;

GenogramGrid.defaultProps = {
  id: 'grid',
  fill: 'lightgray',
  gridDot: 2,
  gridSpacing: 36,
  patternUnits: 'userSpaceOnUse'
};

GenogramGrid.propTypes = {
  id: PropTypes.string,
  fill: PropTypes.string,
  gridDot: PropTypes.number,
  gridSpacing: PropTypes.number,
  patternUnits: PropTypes.string
};
