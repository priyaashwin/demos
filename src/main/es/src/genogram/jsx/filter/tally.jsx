'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const renderFrom = function(pageData){
  const { pageSize, currentPage } = pageData;
  const from = pageSize*(currentPage-1) + 1;
  return (<span className="num-records">{from}</span>);
};

const renderTo = function(pageData){
  const { totalSize, pageSize, currentPage } = pageData;
  let to = pageSize*currentPage;
  to = totalSize < to ? totalSize : to;
  return (<span className="num-records">{to}</span>);
};

const renderTotal = function(pageData){
  const { totalSize } = pageData;
  return <span className="num-records">{totalSize}</span>;
};

const Tally=({ pageData }) => {
  if(pageData.totalSize === 0)return <div />;
  return (
    <div tabIndex="0" className="tally">
        <span className="results-count">
          Showing records {renderFrom(pageData)} - {renderTo(pageData)} of {renderTotal(pageData)}
        </span>
    </div>
  );
};

Tally.propTypes = {
  pageData: PropTypes.object
};

export default Tally;