'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import * as arrUtils from '../../../utils/js/arrayUtils';

const pageSizes = [4, 12, 20, 40, 80, 120],
    maxLimitOfPages = 9;

const Ellipsis = function({ pageNum, index }) {
    if (index !== pageNum) return false;
    return <span className="pagination-more">...</span>;
};

Ellipsis.propTypes = {
    pageNum: PropTypes.number,
    index: PropTypes.number
};

const renderPageNumbers = function(onPaginationUpdate, currentPage, pageNum, leftEllipsisIdx, rightEllipsisIdx) {
    return (
        <li key={'page_' + pageNum}>
            <Ellipsis pageNum={pageNum} index={leftEllipsisIdx} />
            <a
                className="pure-button yui3-paginator-link-page yui3-paginator-link-page-list pure-button-active"
                title={'Page ' + pageNum}
                href="#"
                data-pglink={pageNum}
                onClick={onPaginationUpdate}
                disabled={currentPage === pageNum}>
                {pageNum}
            </a>
            <Ellipsis pageNum={pageNum} index={rightEllipsisIdx} />
        </li>
    );
};

const renderFastBackward = function(onPaginationUpdate, currentPage) {
    return (
        <li key="page_fast_backwards">
            <a
                className="pure-button"
                data-pglink="1"
                onClick={onPaginationUpdate}
                href="#"
                title="First page"
                disabled={currentPage === 1}>
                <i className="fa fa-fast-backward icon-fast-backward" />
            </a>
        </li>
    );
};

const renderStepBackward = function(onPaginationUpdate, currentPage) {
    const previousPage = currentPage - 1;
    return (
        <li key="page_step_backwards">
            <a
                className="pure-button"
                data-pglink={Number(previousPage)}
                onClick={onPaginationUpdate}
                href="#"
                title="Previous page"
                disabled={previousPage === 0}>
                <i className="fa fa-step-backward icon-step-backward" />
            </a>
        </li>
    );
};

const renderStepForward = function(onPaginationUpdate, currentPage, totalPages) {
    const nextPage = currentPage + 1;
    return (
        <li key="page_step_forward">
            <a
                className="pure-button"
                data-pglink={Number(nextPage)}
                onClick={onPaginationUpdate}
                href="#"
                title="Next page"
                disabled={nextPage > totalPages}>
                <i className="fa fa-step-forward icon-step-forward" />
            </a>
        </li>
    );
};

const renderFastForward = function(onPaginationUpdate, currentPage, totalPages) {
    return (
        <li key="page_fast_forward">
            <a
                className="pure-button"
                data-pglink={Number(totalPages)}
                onClick={onPaginationUpdate}
                href="#"
                title="Last page"
                disabled={currentPage === totalPages}>
                <i className="fa fa-fast-forward icon-fast-forward" />
            </a>
        </li>
    );
};

const renderPagination = function(pageData, onPaginationUpdate) {
    const { totalSize, pageSize, currentPage } = pageData,
        totalPages = Math.ceil(totalSize / pageSize),
        pagination = [];

    let startPage = 1,
        leftEllipsisIdx = 0,
        rightEllipsisIdx = 0,
        offset = totalPages;

    const interceptor = Math.ceil(maxLimitOfPages / 2);

    if (totalPages > maxLimitOfPages) {
        offset = maxLimitOfPages;
        if (currentPage > interceptor) {
            startPage = currentPage - interceptor + 1;
            offset = currentPage + interceptor - 1;
            if (offset >= totalPages) {
                offset = totalPages;
                startPage = offset - maxLimitOfPages + 1;
            }
        }
        if (startPage > 1) {
            leftEllipsisIdx = startPage;
        }
        if (offset < totalPages) {
            rightEllipsisIdx = offset;
        }
    }

    pagination.push(renderFastBackward(onPaginationUpdate, currentPage));
    pagination.push(renderStepBackward(onPaginationUpdate, currentPage));
    for (let pageNum = startPage; pageNum <= offset; pageNum++) {
        pagination.push(renderPageNumbers(onPaginationUpdate, currentPage, pageNum, leftEllipsisIdx, rightEllipsisIdx));
    }
    pagination.push(renderStepForward(onPaginationUpdate, currentPage, totalPages));
    pagination.push(renderFastForward(onPaginationUpdate, currentPage, totalPages));

    return pagination;
};

const renderPageSizePicker = function(pageData, onPageSizeUpdate) {
    const { pageSize } = pageData;
    return (
        <span>
            Relatives per page:
            <select className="paginator-select-rowsperpage" defaultValue={pageSize} onChange={onPageSizeUpdate}>
                {pageSizes.map(pageSizeValue => (
                    <option key={'pageSize_opt_' + pageSizeValue} value={pageSizeValue}>
                        {pageSizeValue}
                    </option>
                ))}
            </select>
        </span>
    );
};

const areAllOptionsSelected = function(data, options, selection) {
    const selected = arrUtils.convertToObj(selection);
    const allPeopleFromGenogramAPI = arrUtils.convertToObj(data);
    return !options.find(
        person =>
            Object.prototype.hasOwnProperty.call(allPeopleFromGenogramAPI, person.personId) &&
            !Object.prototype.hasOwnProperty.call(selected, person.personId)
    );
};

const renderSelectAllBtn = function(onSelectAllUpdate) {
    return (
        <span>
            <input
                className="pure-button pure-button-secondary"
                onClick={onSelectAllUpdate}
                type="button"
                aria-label="Selects all visible relatives"
                title="Select all visible relatives"
                value="Select All"
            />
        </span>
    );
};

const renderUnSelectAllBtn = function(onUnselectAllUpdate) {
    return (
        <span>
            <input
                className="pure-button pure-button-secondary"
                onClick={onUnselectAllUpdate}
                type="button"
                aria-label="Unselect all visible relatives"
                title="Unselect all visible relatives"
                value="Unselect All"
            />
        </span>
    );
};

const renderSelectAll = function(data, options, selection, onSelectAllUpdate, onUnselectAllUpdate) {
    const allSelected = areAllOptionsSelected(data, options, selection);
    return allSelected ? renderUnSelectAllBtn(onUnselectAllUpdate) : renderSelectAllBtn(onSelectAllUpdate);
};

const Pagination = ({
    data,
    pageData,
    options,
    selection,
    onPaginationUpdate,
    onPageSizeUpdate,
    onSelectAllUpdate,
    onUnselectAllUpdate
}) => {
    return (
        <div className="pagination-container" tabIndex="-1">
            <ul className="pure-pagebar">
                <li>
                    <ul className="pure-paginator">{renderPagination(pageData, onPaginationUpdate)}</ul>
                </li>
                <li>
                    <ul className="pure-perpage">
                        <li>{renderPageSizePicker(pageData, onPageSizeUpdate)}</li>
                    </ul>
                </li>
                <li>
                    <ul className="pure-perpage">
                        <li>{renderSelectAll(data, options, selection, onSelectAllUpdate, onUnselectAllUpdate)}</li>
                    </ul>
                </li>
            </ul>
        </div>
    );
};

Pagination.propTypes = {
    pageData: PropTypes.object,
    data: PropTypes.array,
    options: PropTypes.array,
    selection: PropTypes.array,
    onPaginationUpdate: PropTypes.func,
    onPageSizeUpdate: PropTypes.func,
    onSelectAllUpdate: PropTypes.func,
    onUnselectAllUpdate: PropTypes.func
};

export default Pagination;
