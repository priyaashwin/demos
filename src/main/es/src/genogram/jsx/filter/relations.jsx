import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Relation from './relation';
import * as arrUtils from '../../../utils/js/arrayUtils';

const COL_NUM = 4;

class Relations extends Component {
    static propTypes = {
        data: PropTypes.array,
        selection: PropTypes.array,
        options: PropTypes.array,
        pageData: PropTypes.object,
        labels: PropTypes.object,
        onFilterUpdate: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.EMPTY_PLACEHOLDER = { id: null };
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.options.length > 0;
    }

    onSelection = e => {
        const { selection, data } = this.props;
        const { value, checked } = e.target;
        const selected = arrUtils.convertToObj(selection);
        const people = arrUtils.convertToObj(data);

        if (checked) {
            if (!Object.prototype.hasOwnProperty.call(selected, value)) {
                selected[value] = people[value];
            }
        } else {
            if (Object.prototype.hasOwnProperty.call(selected, value)) {
                delete selected[value];
            }
        }

        this.props.onFilterUpdate(selected);
    };

    renderColumn(person) {
        const { labels, selection, data } = this.props;
        const selected = arrUtils.convertToObj(selection);
        const people = arrUtils.convertToObj(data);
        return (
            <div className="pure-u-1-4" key={'checkbox_' + person.personId}>
                <fieldset className="l-box">
                    <Relation
                        key={'relation_' + person.personId}
                        labels={labels}
                        person={person}
                        onSelection={this.onSelection}
                        selected={Object.prototype.hasOwnProperty.call(selected, person.personId)}
                        disabled={!Object.prototype.hasOwnProperty.call(people, person.personId)}
                    />
                </fieldset>
            </div>
        );
    }

    renderEmptyColumn = function(person) {
        return <div className="pure-u-1-4" key={'empty_checkbox_' + person.elemKey} />;
    };

    renderRow(relations, rowNum) {
        return (
            <div className="pure-g-r" key={'row_' + rowNum}>
                {relations.map(person =>
                    person.id !== null ? this.renderColumn(person) : this.renderEmptyColumn(person)
                )}
            </div>
        );
    }

    renderRows() {
        const { options, pageData } = this.props;

        const peopleLeft = [...options],
            rowTotal = pageData.pageSize / COL_NUM,
            columns = [];
        let elemKey = 0;
        let relations;

        if (!peopleLeft.length) return [];

        for (let rowNum = 0; rowNum < rowTotal; rowNum++) {
            relations = []; // refresh list of relatives
            for (let colNum = 0; colNum < COL_NUM; colNum++) {
                elemKey++;
                if (peopleLeft.length === 0) {
                    relations.push({ ...this.EMPTY_PLACEHOLDER, elemKey });
                    continue;
                }
                relations.push({ ...peopleLeft[0], elemKey });
                peopleLeft.shift(); // trim list of relatives
            }
            columns.push(this.renderRow(relations, rowNum));
        }
        return columns;
    }

    render() {
        return this.renderRows();
    }
}

export default Relations;
