'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Relation=({ disabled, person, selected, onSelection, labels }) => {
  return (
    <label className="pure-checkbox" htmlFor={'relation_' + person.personId} title={disabled ? labels.whyGreyedOut : ''}>
      <input disabled={disabled}
             onChange={onSelection}
             type="checkbox"
             id={'relation_' + person.personId}
             name="relation"
             value={person.personId}
             checked={selected} />
      <span className={classNames({greyedOut: disabled})}>{person.name} - {person.relationship}</span>
    </label>
  );
};

Relation.propTypes = {
  person: PropTypes.object,
  labels: PropTypes.object,
  disabled: PropTypes.bool,
  selected: PropTypes.bool,
  onSelection: PropTypes.func
};

export default Relation;