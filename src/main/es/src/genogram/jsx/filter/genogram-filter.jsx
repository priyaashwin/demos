import React, { Component } from 'react';
import PropTypes from 'prop-types';
import endpoint from '../../js/cfg';
import ModelList from '../../../model/modelList';
import PaginatedResultsList from '../../../results/paginatedResultsList';
import Relations from './relations';
import Tally from './tally';
import Pagination from './pagination';
import { getCancelToken } from 'usp-xhr';
import * as arrUtils from '../../../utils/js/arrayUtils';

class GenogramFilter extends Component {
    static propTypes = {
        filter: PropTypes.object,
        data: PropTypes.array,
        selection: PropTypes.array,
        subject: PropTypes.object,
        onFilterUpdate: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            options: [],
            pageData: {
                totalSize: 0,
                pageSize: 20,
                currentPage: 1
            }
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            this.state.pageData.currentPage !== prevState.pageData.currentPage ||
            this.state.pageData.pageSize !== prevState.pageData.pageSize
        ) {
            this.loadData(this.state.pageData);
        }
    }

    componentDidMount() {
        this.loadData(this.state.pageData);
    }

    componentWillUnmount() {
        if (this.cancelToken) {
            //cancel any outstanding xhr requests
            this.cancelToken.cancel();

            //remove property
            delete this.cancelToken;
        }
    }

    loadData(pageData) {
        //if there are any outstanding requests cancel them
        if (this.cancelToken) {
            //cancel any existing requests
            this.cancelToken.cancel();
        }
        this.loadFamilialRelationships(pageData).then(data => {
            const { results, totalSize, pageSize, page } = data;
            this.setState({
                options: results,
                pageData: {
                    totalSize,
                    pageSize,
                    currentPage: page
                }
            });
        });
    }

    loadFamilialRelationships(pageData) {
        return this.getListLoad(this.props.filter.url, endpoint.familialRelationshipsEndpoint, pageData);
    }

    getListLoad(url, endPointConfig, pageData) {
        const { pageSize, currentPage } = pageData;

        //setup a new cancel token
        this.cancelToken = getCancelToken();

        return new PaginatedResultsList().load(
            {
                ...endPointConfig,
                url: new ModelList(url).getURL({
                    subjectId: this.props.subject.id,
                    dateFrom: new Date().getTime()
                })
            },
            pageSize,
            currentPage,
            this.cancelToken
        );
    }

    onPaginationUpdate = e => {
        e.preventDefault();
        if (e.currentTarget.hasAttribute('disabled')) return;
        this.setState({
            pageData: {
                ...this.state.pageData,
                currentPage: Number(e.currentTarget.dataset.pglink)
            }
        });
    };

    onPageSizeUpdate = e => {
        e.preventDefault();
        this.setState({
            pageData: {
                ...this.state.pageData,
                pageSize: Number(e.currentTarget.value),
                currentPage: 1
            }
        });
    };

    onSelectAllUpdate = e => {
        e.preventDefault();
        const { selection, data } = this.props;
        const { options } = this.state;
        const selected = arrUtils.convertToObj(selection);
        const people = arrUtils.convertToObj(data);

        options.forEach(person => {
            if (!Object.prototype.hasOwnProperty.call(selected, person.personId)) {
                selected[person.personId] = people[person.personId];
            }
        });

        this.onFilterUpdate(selected);
    };

    onUnselectAllUpdate = e => {
        e.preventDefault();
        const { selection } = this.props;
        const { options } = this.state;
        const selected = arrUtils.convertToObj(selection);

        options.forEach(person => {
            if (Object.prototype.hasOwnProperty.call(selected, person.personId)) {
                delete selected[person.personId];
            }
        });

        this.onFilterUpdate(selected);
    };

    onFilterUpdate = selected => {
        const { subject, data } = this.props;
        const newSelection = data.filter(person => {
            return person.id === subject.id || Object.prototype.hasOwnProperty.call(selected, person.id);
        });
        this.props.onFilterUpdate(newSelection);
    };

    render() {
        const { data, selection, filter } = this.props;
        const { options, pageData } = this.state;
        return (
            <div>
                <div className="filter">
                    <div className="pure-form pure-form-stacked filter-panel">
                        <div className="bordered panel">
                            <Tally pageData={pageData} />
                            <h3 className="alt2" tabIndex="0">
                                Build Genogram
                            </h3>
                            <Relations
                                data={data}
                                options={options}
                                labels={filter.labels}
                                selection={selection}
                                pageData={pageData}
                                onFilterUpdate={this.onFilterUpdate}
                            />
                            <Pagination
                                data={data}
                                options={options}
                                selection={selection}
                                pageData={pageData}
                                onPaginationUpdate={this.onPaginationUpdate}
                                onPageSizeUpdate={this.onPageSizeUpdate}
                                onSelectAllUpdate={this.onSelectAllUpdate}
                                onUnselectAllUpdate={this.onUnselectAllUpdate}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default GenogramFilter;
