import React from 'react';
import PropTypes from 'prop-types';

export const Reset = ({ id, onClick }) => (
    <div className="pure-u-1 1-box">
        <div className="pure-button-group pull-right">
            <input
                type="button"
                id={id}
                className="pure-button pure-button-secondary"
                value="Reset"
                aria-label="Reset state filters"
                onClick={onClick}
            />
        </div>
    </div>
);

Reset.propTypes = {
    onClick: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired
};
