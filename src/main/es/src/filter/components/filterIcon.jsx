import React from 'react';

const FilterIcon = () => (
    <span className="filterIcon" aria-label="Active filter icon">
        <i className="fa fa-filter"></i>
    </span>
);

export default FilterIcon;
