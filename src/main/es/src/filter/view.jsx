'use strict';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { TabView } from '../tabs/jsx/tabView';
import './less/app.less';

export default class Filter extends Component {
    getFilterTabsContent = () => {
        const {label} = this.props;

        return (
            <div id="resultsFilter">
                <div>
                    <h3 className="alt2" tabIndex="0">
                        {label}
                    </h3>
                </div>
                <TabView key="roleTabs">
                    {this.props.children}
                </TabView>
            </div>
        );
    };

    onSubmit = (e) => {
        e.preventDefault();
    }

    render() {
        return (
            <div className="filter">
                <form
                    action="#none"
                    id="resultsFilterForm"
                    className="pure-form pure-form-stacked filter-panel"
                    onSubmit={this.onSubmit}>

                    <div className="panel bordered">
                        {this.getFilterTabsContent()}
                    </div>
                </form>
            </div>
        );
    }
}

Filter.propTypes = {
    label: PropTypes.string.isRequired,
    children: PropTypes.elementType
};
