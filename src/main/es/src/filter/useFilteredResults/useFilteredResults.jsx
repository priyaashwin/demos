'use strict';

import { useCallback, useContext, useEffect, useReducer, useRef, useState } from 'react';
import { filterContext } from '../filterContextProvider';

const DEFAULT_DELAY = 250;

const loadDataReducer = (state, action) => {
    const { data, errors, totalResultsCount } = action.payload;

    switch (action.type) {
        case 'SUCCESS':
            return {
                ...state,
                loading: false,
                totalResultsCount,
                errors: null,
                data: data
            };
        case 'FAILURE':
            return {
                ...state,
                loading: false,
                totalResultsCount: null,
                errors: errors,
                data: []
            };
        case 'CLEAR_ERRORS':
            return {
                ...state,
                errors: null
            };
    }
};

export default function useFilteredResults(getPaginatedResults, resultsMapper) {
    const [{ loading, errors, totalResultsCount, data }, dispatch] = useReducer(loadDataReducer, {
        loading: true,
        errors: null,
        totalResultsCount: null,
        data: []
    });

    const { getActiveFilters } = useContext(filterContext);

    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [currentPage, setCurrentPage] = useState(1);
    const timeout = useRef(null);

    const filters = getActiveFilters();

    const loadData = useCallback(() => {
        getPaginatedResults(filters, rowsPerPage, currentPage)
            .then(({ results, totalSize }) => {
                const mappedResults = resultsMapper ? results.map(resultsMapper) : results;

                dispatch({
                    type: 'SUCCESS',
                    payload: {
                        data: mappedResults,
                        totalResultsCount: totalSize
                    }
                });
            })
            .catch(reject =>
                dispatch({
                    type: 'FAILURE',
                    payload: {
                        errors: reject
                    }
                })
            );
        timeout.current = null;
    }, [resultsMapper, getPaginatedResults, currentPage, rowsPerPage, filters]);

    useEffect(() => {
        // update the debounce delay
        const updateDelay = delayTime => {
            if (timeout.current) {
                clearTimeout(timeout.current);
            }
            timeout.current = setTimeout(() => loadData(), delayTime);
        };

        const isAnyActiveFilter = Object.keys(filters).length > 0;

        let delayTime;
        if (!isAnyActiveFilter) {
            // no delay on initial load if filters are all unset
            delayTime = 0;
        } else {
            delayTime = DEFAULT_DELAY;
        }
        updateDelay(delayTime);

    }, [rowsPerPage, currentPage, filters, loadData]);

    // revert to page one when filters are updated
    useEffect(() => {
        setCurrentPage(1);
    }, [filters]);

    const clearErrors = e => {
        e.preventDefault();
        dispatch({ type: 'CLEAR_ERRORS', payload: {} });
    };

    return {
        setRowsPerPage,
        rowsPerPage,
        setCurrentPage,
        currentPage,
        clearErrors,
        loading,
        errors,
        totalResultsCount,
        data
    };
}
