'use strict';

import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import { filterContext } from '../filterContextProvider';
import FilterStatus from './filterStatus';

export default function ActiveFilters(props) {
    const { labels } = props.resultsFilterConfig;
    const { resetFilters, resetAllFilters, getActiveFilters } = useContext(filterContext);
    const activeFiltersArray = Object.entries(getActiveFilters());

    const renderResetAllButton = () => {
        const resetAllContent = (
            <>
                <span onClick={resetAllFilters}>
                    <a href="#reset" className="small" arial-label={labels.resetAll}>
                        {labels.resetAll}
                    </a>
                </span>
            </>
        );

        return resetAllContent;
    };

    const isAnyActiveFilter = () => {
        return activeFiltersArray.length > 0;
    };

    return (
        <>
            {isAnyActiveFilter() && (
                <div id="filterContext" className="filter-context active">
                    <div className="active-filter">
                        <span>{labels.activeFilters}</span>

                        {activeFiltersArray.map(([filterName]) => (
                            <FilterStatus
                                key={filterName}
                                filterName={filterName}
                                arialLabel={labels[filterName]}
                                filterLabel={labels[filterName]}
                                resetFilters={resetFilters}
                                id={filterName}
                            />
                        ))}
                        {renderResetAllButton()}
                    </div>
                </div>
            )}
        </>
    );
}

ActiveFilters.propTypes = {
    getFilters: PropTypes.func.isRequired,
    resetAllFilters: PropTypes.func.isRequired,
    resultsFilterConfig: PropTypes.shape({ labels: PropTypes.object.isRequired })
};
