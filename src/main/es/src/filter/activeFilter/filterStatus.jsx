'use strict';

import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

export default function FilterStatus({ arialLabel, filterName, filterLabel, resetFilters }) {
    const onCloseButtonClicked = useCallback(() => {
        resetFilters(filterName);
    }, [filterName, resetFilters]);

    return (
        <span className="label">
            {filterLabel}
            <a href="#" arial-label={arialLabel}>
                <span onClick={onCloseButtonClicked} className="closeButton">
                    <i className="fa fa-times-circle"></i>
                </span>
            </a>
        </span>
    );
}

FilterStatus.propTypes = {
    arialLabel: PropTypes.string.isRequired,
    filterLabel: PropTypes.string.isRequired,
    filterName: PropTypes.string.isRequired,
    resetFilters: PropTypes.func.isRequired
};
