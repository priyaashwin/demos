'use strict';

const setFilterValue = (filters, errors, filterName, value) => {
    const oldFilter = filters[filterName];
    const newFilter = oldFilter.setValue(value);
    
    const newFilters = { ...filters, [filterName]: newFilter };
    // Validate the incoming value and get the new errors
    const newErrors = newFilter.validate(newFilters);

    return { filters: newFilters, errors: { ...errors, ...newErrors } };
};

const resetFilters = (filters, errors, ...filterNames) => {
    const newFilters = { ...filters };
    const newErrors = { ...errors };

    filterNames.forEach(filterName => {
        const oldFilter = filters[filterName];
        const newFilter = oldFilter.reset();

        delete newErrors[filterName];
        newFilters[filterName] = newFilter;
    });

    return { filters: newFilters, errors: newErrors };
};

const resetAllFilters = filters => {
    return Object.entries(filters)
        .reduce((acc, [filterName, filter]) => {
            acc[filterName] = filter.reset();
            return acc;
        }, {});
};

const getFilters = (filters, errors, activeOnly) => {
    if (activeOnly) {
        filters = Object.entries(filters)
            .reduce((acc, [filterName, filter]) => {
                // the filter is active and has no associated error
                if (filter.getIsActive() && !errors[filterName]) {
                    acc[filterName] = filter;
                }
                return acc;
            }, {});
    }

    return filters;
};

export default {
    setFilterValue,
    resetFilters,
    resetAllFilters,
    getFilters
};