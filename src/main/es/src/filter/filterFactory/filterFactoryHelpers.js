'use strict';
import * as DateUtils from '../../date/date';

/**
 * Parses a given date string value into a USP date
 * @param {String} value The string value to parse
 * @param {String} time The time to apply to the parsed date
 * @return {Moment} The parsed Moment object on success, null if the value could not be parsed
 * @method parseDate
 * @static
 */
const parseDate = (value, time) => {
    let d;

    if (value === null || trimIfString(value) === '') {
        return null;
    }
    if (isNumber(value) || /^-?\d+$/.test(value)) {
        d = DateUtils.parseDate(new Date(value));
    } else {
        d = DateUtils.parseDate(value);
    }
    if (d) {
        const timeParts = time ? time.split(':') : [0,0,0];
        d.setHours(timeParts[0], timeParts[1], timeParts[2]);
    }
    return d ? d : null;
};

const formatDate = DateUtils.formatDate;

const trimIfString = value => {
    typeof value === 'string' ? value.trim() : value;
};

const isNumber = value => {
    return typeof value === 'number' && isFinite(value);
};

export {
    parseDate,
    formatDate,
    isNumber
};