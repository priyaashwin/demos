'use strict';
import { Record } from 'immutable';
import * as Helpers from './filterFactoryHelpers';
import { DATE_STRING_REG } from '../../date/date';

const VALUE_TYPES = { DATE_FROM: 'DATE_FROM', DATE_TO: 'DATE_TO', DEFAULT: 'DEFAULT', TEXT: 'TEXT', INTEGER: 'INTEGER', ARRAY: 'ARRAY' };

/**
 * A factory function for creating an ImmutableJS Record 
 * representing a filter
 * 
 * @param {Object} configObject a destructured configuration 
 * object with the following properties:
 *  valueType (required): the type of value contained by this 
 *  FilterRecord. This determines the getter and setter if not
 *  is provided.
 * 
 *  initValue (required): the initial value of the FilterRecord.
 *  The FilterRecord reverts to this value when reset() is called.
 * 
 *  getter (optional): a getter to be used in place of the 
 *  default getter for this type
 * 
 *  setter (optional): a setter to be used in place of the 
 *  default setter for this type
 * 
 *  validator (optional): provides custom validation of the filter's
 *  value
 */
const getFilterRecord = ({ valueType, initValue, getter, setter, validator }) => {
    const thisGetter = getter ? getter : getDefaultGetter(valueType);
    const thisSetter = setter ? setter : getDefaultSetter(valueType);
    const thisValidator = validator ? validator : () => null;
    const thisValue = thisSetter(initValue);

    const FilterRecordBase = Record({
        valueType,
        getter: thisGetter,
        setter: thisSetter,
        validator: thisValidator,
        value: thisValue,
    });

    class FilterRecord extends FilterRecordBase {
        getValue() {
            return this.getter(this.get('value'));
        }
        setValue(newValue) {
            return this.set('value', this.setter(newValue));
        }
        reset() {
            // return a new FilterRecord reset to its 
            // initial state
            return this.clear();
        }
        getIsActive() {
            // the filter is active when a value is present 
            return this.isNotBlankOrEmpty(this.getValue());
        }
        isNotBlankOrEmpty(value) {
            if (value && typeof value === 'object') {
                return Object.keys(value).length > 0;
            }
            if (Array.isArray(value)) {
                return value.length > 0;
            } else {
                return !!value;
            }
        }
        validate(filters) {
            return this.validator(filters);
        }
        toString() {
            if (this.isNotBlankOrEmpty(this.get('value'))) {
                return this.get('value').toString();
            }
            return '';
        }
    }

    return new FilterRecord({
        value: thisValue,
    });
};

const getDefaultGetter = (valueType) => {
    const dateGetter = value => {

        // return partially complete date as a string if not in DD-MMM-YYYY format
        if (!DATE_STRING_REG.test(value)) {
            return value;
        }

        const defaultTime = valueType === 'DATE_TO' ? '23:59:59' : '00:00:00';

        const parsedDate = Helpers.parseDate(value, defaultTime);
        // return null where date is in correct format but could not be 
        // parsed as a valid date
        return parsedDate ? parsedDate.getTime() : null;
    };

    const textGetter = value => {
        if (typeof value === 'string') {
            return value;
        }
        return '';
    };

    const integerGetter = (value) => {
        if (Number.isInteger(value)) {
            return value;
        }

        return null;
    };

    const arrayGetter = (value) => {
        if (Array.isArray(value)) {
            return value;
        }
        return [];
    };

    const defaultGetter = value => {
        return value;
    };

    switch (valueType) {
    case VALUE_TYPES.DATE_FROM:
    case VALUE_TYPES.DATE_TO:
        return dateGetter;
    case VALUE_TYPES.TEXT:
        return textGetter;
    case VALUE_TYPES.INTEGER:
        return integerGetter;
    case VALUE_TYPES.ARRAY:
        return arrayGetter;
    default:
        return defaultGetter;
    }
};

const getDefaultSetter = (valueType) => {
    const dateSetter = value => {

        // store partially complete date as a string if not in DD-MMM-YYYY format
        if (!DATE_STRING_REG.test(value)) {
            return value;
        }

        const defaultTime = valueType === 'DATE_TO' ? '23:59:59' : '00:00:00';

        const parsedDate = Helpers.parseDate(value, defaultTime);
        return parsedDate ? Helpers.formatDate(parsedDate, true) : value;
    };

    const textSetter = value => {
        if (typeof value === 'string') {
            return value;
        }
        if (Helpers.isNumber(value)) {
            return value.toString();
        }
        return '';
    };

    const integerSetter = (value) => {
        const numValue = Number(value);
        if (Number.isInteger(numValue)) {
            return numValue;
        }
        return null;
    };

    const arraySetter = (value) => {
        if (Array.isArray(value)) {
            return value;
        }
        return [];
    };

    const defaultSetter = value => {
        return value;
    };

    switch (valueType) {
    case VALUE_TYPES.DATE_FROM:
    case VALUE_TYPES.DATE_TO:
        return dateSetter;
    case VALUE_TYPES.TEXT:
        return textSetter;
    case VALUE_TYPES.INTEGER:
        return integerSetter;
    case VALUE_TYPES.ARRAY:
        return arraySetter;
    default:
        return defaultSetter;
    }
};

export { getFilterRecord, VALUE_TYPES };