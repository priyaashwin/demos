'use strict';

import memoize from 'memoize-one';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import FilterContextHelpers from './filterFactory/filterContext';

const filterContext = React.createContext();

class FilterContextProvider extends Component {
    state = {
        filters: this.addFilters(),
        errors: {}
    };

    // Set up state using the config object
    addFilters() {
        const newFilters = {};
        this.props.filterConfig.forEach(({ filterName, filter }) => {
            newFilters[filterName] = filter;
        });

        return newFilters;
    }

    setFilterValue = (filterName, value) => {
        const { filters: newFilters, errors: newErrors } = FilterContextHelpers.setFilterValue(
            this.state.filters,
            this.state.errors,
            filterName,
            value
        );

        this.setState({ filters: newFilters, errors: newErrors });
    };

    resetFilters = (...filterNames) => {
        const { filters: newFilters, errors: newErrors } = FilterContextHelpers.resetFilters(
            this.state.filters,
            this.state.errors,
            ...filterNames
        );

        this.setState({ filters: newFilters, errors: newErrors });
    };

    resetAllFilters = () => {
        const newFilters = FilterContextHelpers.resetAllFilters(this.state.filters);

        this.setState({ filters: newFilters, errors: {} });
    };

    memoizeResultsActiveFilter = memoize((state, errors) => {
        return FilterContextHelpers.getFilters(state, errors, true);
    });

    getActiveFilters = () => {
        return this.memoizeResultsActiveFilter(this.state.filters, this.state.errors);
    };

    memoizeResults = memoize((state, errors) => {
        return FilterContextHelpers.getFilters(state, errors, false);
    });

    getFilters = () => {
        return this.memoizeResults(this.state.filters, this.state.errors);
    };

    getErrors = () => {
        return this.state.errors;
    };

    render() {
        const getFunctions = () => {
            return {
                setFilterValue: this.setFilterValue,
                resetFilters: this.resetFilters,
                resetAllFilters: this.resetAllFilters,
                getFilters: this.getFilters,
                getActiveFilters: this.getActiveFilters,
                getErrors: this.getErrors
            };
        };
        const functions = memoize(getFunctions);

        return <filterContext.Provider value={functions()}>{this.props.children}</filterContext.Provider>;
    }
}

FilterContextProvider.propTypes = {
    children: PropTypes.node,
    filterConfig: PropTypes.object.isRequired
};

export { FilterContextProvider, filterContext };

