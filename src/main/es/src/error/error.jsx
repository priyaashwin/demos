'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const ErrorMessage = memo(({ errors, onClose }) => {
    const classNames = classnames('usp-message', 'usp-message-block', {
        'usp-message-error': errors.type === 'error',
        'usp-message-info': errors.type === 'info'
    });
    return (
        <div className="usp-model-errors">
            <div className={classNames} role="alert">
                <ul>
                    <a href="#" className="usp-model-errors-close" title="Close" onClick={onClose}>
                        Close
                    </a>{' '}
                    <h4>{errors.title}</h4>
                    {(errors.messages || (errors.message ? [errors.message] : [])).map((message, i) => (
                        <li key={i}>
                            <span>{message}</span>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
});

ErrorMessage.propTypes = {
    onClose: PropTypes.func.isRequired,
    //object containting errors
    errors: PropTypes.shape({
        title: PropTypes.string,
        type: PropTypes.string,
        messages: PropTypes.array,
        message: PropTypes.string
    })
};

export default ErrorMessage;
