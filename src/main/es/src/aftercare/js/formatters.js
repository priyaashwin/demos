'use strict';
import { formatDate, formatDateTime } from '../../date/date';

const date = (key, data) => (formatDate(data[key], true));

const dateTime = (key, data) => (formatDateTime(data[key], true));

export default {
  date,
  dateTime
};
