'use strict';

class Endpoint{
  constructor() {

    this.afterCareEndpoint={
      method: 'get',
      contentType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/vnd.olmgroup-usp.careleaver.AfterCare+json'
      },
      responseType: 'json',
      responseStatus:200
    };
    
  }
}

export default new Endpoint();
