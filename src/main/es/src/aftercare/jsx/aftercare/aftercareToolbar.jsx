'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Button from './button';

const AfterCareToolbar=memo(({ disabled, labels, aftercare, display, permissions })=>{
  const buttons = [];
  if (display) {
    buttons.push(
        <Button
          iconClass="fa-plus-circle"
          label={labels.addContact}
          clickParams={{action:'addContact', aftercare:aftercare}}
          disabled={disabled}
          key="addContact"
        />
    );

    //add services button
    if(permissions.canAddProvision){
      buttons.push(
        <Button
            iconClass="fa-plus-circle"
            label={labels.addProvision}
            clickParams={{action:'addProvision', id:aftercare.id}}
            disabled={disabled || !aftercare.receivingAfterCare}
            key="addProvision"
        />
      );
    }

    if (permissions.canEndRecord) {
      buttons.push(
        <Button
          iconClass="fa-check-square-o"
          label={labels.endRecord}
          clickParams={{action:'endAfterCare', aftercare:aftercare}}
          disabled={disabled}
          key="endRecord"
        />
      );
    }
  }

  return (
    <div className="aftercare-toolbar"> {buttons} </div>
   );

});

AfterCareToolbar.propTypes= {
  disabled: PropTypes.bool,
  display: PropTypes.bool,
  labels: PropTypes.object,
  aftercare: PropTypes.object.isRequired,
  permissions: PropTypes.object.isRequired
};

export default AfterCareToolbar;
