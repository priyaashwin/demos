'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import postal from 'postal';

const Button=memo(({label, iconClass, style, clickParams, disabled})=>(
    <button href="#none" style={style} className="pure-button pure-button-active"
     onClick={
       function(){
         postal.publish({
            channel: 'aftercare',
            topic: 'buttonClick',
            data: clickParams
          });
      }} disabled={disabled}>
      <i className={classnames('fa', iconClass)}></i>
      <span>{label}</span>
    </button>
));

Button.propTypes={
  label: PropTypes.string.isRequired,
  iconClass: PropTypes.string,
  style:  PropTypes.object,
  clickParams: PropTypes.object.isRequired,
  disabled: PropTypes.bool
};

export default Button;
