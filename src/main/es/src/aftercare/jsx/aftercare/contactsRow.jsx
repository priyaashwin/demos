'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { formatDate } from '../../../date/date';
import { formatCodedEntry } from '../../../codedEntry/codedEntry';

const ContactRow=memo(({contact, labels, codedEntries})=>(
    <tr>
      <td data-title={labels.contactDate}>{formatDate(contact.contactDate, true)}</td>
      <td data-title={labels.accommodation}>{formatCodedEntry(codedEntries.accommodation, contact.accommodationType.accommodationType)}</td>
      <td data-title={labels.economicActivity}>{formatCodedEntry(codedEntries.economicActivity, contact.economicActivity.economicActivity)}</td>
    </tr>
));


ContactRow.propTypes={
  labels: PropTypes.object.isRequired,
  contact: PropTypes.object.isRequired,
  codedEntries: PropTypes.object.isRequired
};

export default ContactRow;