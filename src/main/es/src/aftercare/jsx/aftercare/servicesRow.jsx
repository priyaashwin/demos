'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { formatDate } from '../../../date/date';
import TableActions from '../../../table/jsx/actions/actions';

const getEnumName = function(provisionType, provisionTypeValues) {
  return provisionTypeValues.filter(value => value.enumValue === provisionType)[0].name;
};

const getActions = (aftercareService, labels, viewOnly, permissions) => {
  const canEdit = !aftercareService.endDate && permissions.canEditProvision;
  const actions = [{
    id: 'viewProvision',
    label: labels.view
  }];

  if(!viewOnly) {
    actions.push({
      id: 'editProvision',
      label: labels.edit,
      disabled: !canEdit
    });
  }

  return actions;
};

const ServiceRow=memo(({aftercareService, labels, enums, handleClick, viewOnly, permissions}) => (
    <tr>
      <td data-title={labels.provisionType}>{getEnumName(aftercareService.provisionType, enums.afterCareProvisionType)}</td>
      <td data-title={labels.startDate}>{formatDate(aftercareService.startDate, true)}</td>
      <td data-title={labels.endDate}>{formatDate(aftercareService.endDate, true)}</td>
      <td data-title={labels.carer}>{aftercareService.carer.name}</td>
      <td className="pure-table-actions actions">
        <TableActions
          key={aftercareService.id}
          actions={getActions(aftercareService, labels, viewOnly, permissions)}
          handleClick={handleClick}
          record={aftercareService}
        />
      </td>
    </tr>
  ));


ServiceRow.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  aftercareService: PropTypes.object.isRequired,
  enums: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  permissions: PropTypes.object.isRequired
};

export default ServiceRow;
