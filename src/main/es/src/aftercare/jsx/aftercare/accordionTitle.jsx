'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { formatDate } from '../../../date/date';

const AccordionTitle=memo(({endDate, startDate, title})=>(
    <span>
      <span>{title}</span>
      <span>{` (${formatDate(startDate, true)}${(endDate && endDate !== startDate) ? ' - ' : ''}${(endDate && endDate !== startDate) ? formatDate(endDate, true) : ''})` }</span>
    </span>
));

AccordionTitle.propTypes={
  startDate: PropTypes.number,
  endDate: PropTypes.number,
  title: PropTypes.string.isRequired
};

export default AccordionTitle;
