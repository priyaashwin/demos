'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Accordion from 'usp-accordion-panel';
import AccordionTitle from './accordionTitle';
import AfterCareSummaryTable from './aftercareSummaryTable';
import ContactsTable from './contactsTable';
import ServicesTable from './servicesTable';
import AfterCareToolbar from './aftercareToolbar';
import Title from './title';
import postal from 'postal';

export default class AfterCareItem extends PureComponent {
  static propTypes = {
    errors: PropTypes.object,
    labels: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    aftercareRecords: PropTypes.array,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectIdentifier: PropTypes.string,
      subjectName: PropTypes.string,
      subjectType: PropTypes.string
    }),
    permissions: PropTypes.object.isRequired,
    codedEntries: PropTypes.object.isRequired,
    enums: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.subscription = postal.subscribe({
      channel: 'aftercare',
      topic: 'buttonClick',
      callback: function (data) {
        this.handleClick(data.action, data);
      }
    }).context(this);
  }

  componentWillUnmount() {
      (this.subscriptions||[]).forEach(s=>s.unsubscribe());
      delete this.subscriptions;
    }

  handleClick = (action, data) => {

    const payload = {
      bubbles: true,
      cancelable: true,
      detail: {}
    };

    switch (action) {
      case ('addContact'):
        payload.detail = {
            aftercareId: data.aftercare.id,
            currentAccommodation: this.getLatestAccommodation(data.aftercare),
            currentEconomicActivity: this.getLatestEconomicActivity(data.aftercare)
        };
        break;
      case ('addProvision'):
        payload.detail = { aftercareId: data.id};
        break;
      case ('endAfterCare'):
      payload.detail = { aftercareId: data.aftercare.id, aftercare: data.aftercare };
        break;
      case ('viewProvision'):
      case ('editProvision'):
        payload.detail = {
          aftercareProvisionId: data.id
        };
        break;
    }

    const event = new CustomEvent(action, payload);

    ReactDOM.findDOMNode(this).dispatchEvent(event);
  }
  getLatestAccommodation(aftercare) {
    return aftercare.contacts[0].accommodationType;
  }
  getLatestEconomicActivity(aftercare) {
    return aftercare.contacts[0].economicActivity;
  }
  getAccordionTitle(data) {
    const { labels } = this.props;
    return (
      <AccordionTitle
        endDate={data.endOfServicesDate}
        startDate={data.contactDate}
        title={labels.accordionTitle}
      />
    );
  }
  renderAccordions() {
    return (
      <div key="main_data">
        {
          (this.props.aftercareRecords || []).map((aftercare, i) => {
            const { labels, permissions, codedEntries, enums } = this.props;
            const title = this.getAccordionTitle(aftercare);

            return (
              <Accordion key={aftercare.id} expanded={i === 0} title={title}>
                <AfterCareSummaryTable
                  labels={labels}
                  aftercare={aftercare}
                  viewOnly={!!aftercare.endOfServicesDate}
                  codedEntries={codedEntries}
                />
                <ContactsTable
                  labels={labels}
                  display={aftercare.receivingServices}
                  contacts={aftercare.contacts}
                  viewOnly={!!aftercare.endOfServicesDate}
                  handleClick={this.handleClick}
                  permissions={permissions}
                  codedEntries={codedEntries}
                />
                <ServicesTable
                  labels={labels.provision}
                  display={aftercare.receivingServices}
                  aftercareServices={aftercare.servicesProvided}
                  viewOnly={!!aftercare.endOfServicesDate}
                  handleClick={this.handleClick}
                  permissions={permissions}
                  enums={enums}
                />
                <AfterCareToolbar
                  disabled={!!aftercare.endOfServicesDate || !permissions.canAddAfterCare}
                  labels={labels}
                  display={!aftercare.endOfServicesDate && permissions.canAddAfterCare}
                  aftercare={aftercare}
                  permissions={permissions}
                />
              </Accordion>
            );
          })
        }
      </div>
    );
  }

  render() {
    const { labels, aftercareRecords, subject } = this.props;
    let afterCareContent;

    if (aftercareRecords && aftercareRecords.length > 0) {
      afterCareContent = this.renderAccordions();
    } else {
      afterCareContent = (<div key="main_data_no_results">There are no existing after care records.</div>);
    }

    return (
      <div className="after-care-records">
        <Title
          identifier={subject.subjectIdentifier}
          name={subject.subjectName}
          title={labels.summary}
          subjectType={subject.subjectType}
        />
        {afterCareContent}
      </div>
    );
  }
}
