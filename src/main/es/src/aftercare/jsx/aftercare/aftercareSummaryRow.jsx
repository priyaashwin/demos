'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import {formatDate} from '../../../date/date';
import {formatCodedEntry} from '../../../codedEntry/codedEntry';
import {yesNo} from '../../../table/js/formatter';

const AfterCareSummaryRow=memo(({ aftercare, labels, codedEntries })=>{
  //If the end date is the same as contact date then we don't want
  //to display the end date
  let endDate = aftercare.endOfServicesDate;
  if (aftercare.endOfServicesDate === aftercare.contactDate) {
    endDate = null;
  }

  let eligibilityLabel = '';
  switch(aftercare.eligibility) {
    case 'COMPULSORY':
      eligibilityLabel = labels.eligibilityDesc.compulsory;
      break;
    case 'TRANSITIONED_TO_DISCRETIONARY':
      eligibilityLabel = labels.eligibilityDesc.transitioned;
      break;
    case 'DISCRETIONARY':
      eligibilityLabel = labels.eligibilityDesc.discretionary;
      break;
    default:
      eligibilityLabel = labels.eligibilityDesc.notknown;
  }

  return (
    <tr>
      <td data-title={labels.contactDate}>{formatDate(aftercare.contactDate, true)}</td>
      <td data-title={labels.receivingAfterCare}>{yesNo(aftercare.receivingAfterCare)}</td>
      <td data-title={labels.eligibility}>{eligibilityLabel}</td>
      <td data-title={labels.serviceEndDate}>{formatDate(endDate, true)}</td>
      <td data-title={labels.serviceEndReason}>{formatCodedEntry(codedEntries.serviceEndReason, aftercare.endOfServicesReason)}</td>
    </tr>
  );
});


AfterCareSummaryRow.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  aftercare: PropTypes.object.isRequired,
  codedEntries: PropTypes.object.isRequired
};

export default AfterCareSummaryRow;