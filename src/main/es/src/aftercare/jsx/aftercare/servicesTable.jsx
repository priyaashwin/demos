'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import ServiceRow from './servicesRow';

const ServicesTable=memo(({ labels, aftercareServices, viewOnly, display, enums, handleClick, permissions })=>{
  if(display && aftercareServices && aftercareServices.length) {
    const rows = aftercareServices.map(aftercareService =>
        <ServiceRow
                  labels={labels}
                  viewOnly={viewOnly}
                  aftercareService={aftercareService}
                  key={aftercareService.id}
                  enums={enums}
                  handleClick={handleClick}
                  permissions={permissions}
        />
    );

    return (
      <div className="aftercare-table aftercare-child-table pure-table pure-table-responsive">
      <span className="aftercare-table-header">{labels.header}</span>
        <table className="aftercare-child-table aftercare-table-services pure-table-table">
          <thead className="aftercare-table-columns">
            <tr>
              <th>{labels.provisionType}</th>
              <th>{labels.startDate}</th>
              <th>{labels.endDate}</th>
              <th>{labels.carer}</th>
              <th className="aftercare-table-col-actions">{labels.actions}</th>
            </tr>
          </thead>
          <tbody className="aftercare-table-data">
            { rows }
          </tbody>
        </table>
      </div>
    );
  } else {
    return '';
  }
});

ServicesTable.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  aftercareServices: PropTypes.object.isRequired,
  enums: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  permissions: PropTypes.object.isRequired,
  display: PropTypes.bool
};

export default ServicesTable;