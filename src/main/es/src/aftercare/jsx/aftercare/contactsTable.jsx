'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import ContactRow from './contactsRow';

const ContactsTable=memo(({ labels, contacts, viewOnly, display, codedEntries })=>{
  if (display) {
    const rows = contacts.map((contact, i) =>
           <ContactRow
                     labels={labels}
                     viewOnly={viewOnly}
                     contact={contact}
                     key={i}
                     codedEntries={codedEntries}
           />
    );

    return (
      <div className="aftercare-table aftercare-child-table pure-table pure-table-responsive">
      <span className="aftercare-table-header">{labels.contacts}</span>
        <table className="aftercare-child-table aftercare-table-contacts pure-table-table">
          <thead className="aftercare-table-columns">
            <tr>
              <th>{labels.contactDate}</th>
              <th>{labels.accommodation}</th>
              <th>{labels.economicActivity}</th>
            </tr>
          </thead>
          <tbody className="aftercare-table-data">
            { rows }
          </tbody>
        </table>
      </div>
    );
  } else {
    return '';
  }
});


ContactsTable.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  contacts: PropTypes.object.isRequired,
  display: PropTypes.bool,
  codedEntries: PropTypes.object.isRequired
};

export default ContactsTable;