'use strict';
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import AfterCareSummaryRow from './aftercareSummaryRow';

const AfterCareSummaryTable=memo(({ labels, aftercare, viewOnly, codedEntries })=>(
    <div className="aftercare-table pure-table pure-table-responsive">
      <table className="aftercare-summary-table pure-table-table">
        <thead className="aftercare-table-columns">
          <tr>
            <th>{labels.initialContactDate}</th>
            <th>{labels.receivingAfterCare}</th>
            <th>{labels.eligibility}</th>
            <th>{labels.serviceEndDate}</th>
            <th>{labels.serviceEndReason}</th>
          </tr>
        </thead>
        <tbody className="aftercare-table-data">
          <AfterCareSummaryRow
            labels={labels}
            viewOnly={viewOnly}
            aftercare={aftercare}
            key={aftercare.id}
            codedEntries={codedEntries}
          />
        </tbody>
      </table>
    </div>
));

AfterCareSummaryTable.propTypes={
  labels: PropTypes.object.isRequired,
  viewOnly: PropTypes.bool,
  aftercare: PropTypes.object.isRequired,
  codedEntries: PropTypes.object.isRequired
};

export default AfterCareSummaryTable;