'use strict';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import endpoint from '../js/cfg';
import ErrorMessage from '../../error/error';
import Loading from '../../loading/loading';
import ModelList from '../../model/modelList';
import PaginatedResultsList from '../../results/paginatedResultsList';
import AfterCareItem from './aftercare/view';

export default class AfterCareView extends Component {
  static propTypes={
    labels: PropTypes.object.isRequired,
    url: PropTypes.string.isRequired,
    subject: PropTypes.shape({
      subjectId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      subjectIdentifier: PropTypes.string,
      subjectName: PropTypes.string,
      subjectType: PropTypes.string
    }),
    permissions: PropTypes.object.isRequired,
    codedEntries: PropTypes.object.isRequired,
    enums: PropTypes.object.isRequired
  };

  state={
    data: null,
    errors: null,
    loading: true
  };
  clearErrors=(e)=>{
    e.preventDefault();
    this.setState({errors:null});
  };
  componentDidUpdate(prevProps) {
    if (!(prevProps.url === this.props.url && prevProps.subject === this.props.subject)) {
      this.loadData();
    }
  }
  componentDidMount(){
    this.loadData();
  }
  loadData(){
    Promise.all([this.loadAfterCareRecords()]).then(data=> {
      this.setState({
        data: data[0].results,
        loading: false,
        errors: null
      });

      const event = new CustomEvent('afterCareLoaded', {
          bubbles:true,
          cancelable: true,
          detail:{
            latestAfterCare: data[0].results[0]
          }
        });

      ReactDOM.findDOMNode(this).dispatchEvent(event);

    }).catch(reject=>this.setState({
      errors: reject,
      loading: false,
      data: null
    }));
  }
  loadAfterCareRecords(){
    return this.getListLoad(this.props.url, endpoint.afterCareEndpoint);
  }
  getListLoad(url, endPointConfig){
    return new PaginatedResultsList().load({
      ...endPointConfig,
      url: new ModelList(url).getURL({
        subjectId: this.props.subject.subjectId
      })
    }, -1, 1);
  }
  renderColumns() {
    const {labels, subject, permissions, codedEntries, enums} = this.props;
    const {data, errors, loading} = this.state;
    return (
      <div className="after-care-content">
        <AfterCareItem
          errors={errors}
          labels={labels}
          loading={loading}
          aftercareRecords={data}
          subject={subject}
          permissions={permissions}
          codedEntries={codedEntries}
          enums={enums}
        />
      </div>
    );
  }
  render(){
    const {errors, loading} = this.state;
    let content;

    if(loading) {
      content = (<Loading />);
    } else if(errors) {
      content = (<ErrorMessage errors={errors} onClose={this.clearErrors} />);
    } else {
      content = this.renderColumns();
    }

    return (<div className="aftercare">{content}</div>);
  }
}
