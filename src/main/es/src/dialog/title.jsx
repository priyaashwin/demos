'use strict';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Title=({icon, title})=>(
    <div className="iconic-title" tabIndex={0}>
        <span className="fa-stack">
            <i className="fa fa-circle fa-stack-1x"/>
            <i className={classnames(icon, 'fa', 'fa-inverse', 'fa-stack-1x')} aria-hidden="true"/>
        </span>
        {title}
    </div>
);

Title.propTypes = {
    icon: PropTypes.string.isRequired,
    title: PropTypes.string
};

export default Title;
