'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Dialog from 'usp-dialog';
import Narrative from './narrative/narrative';
import Title from './title';
import { render as renderTemplate } from '../template/template';
import memoize from 'memoize-one';

const EVT_INFO_MESSAGE = 'infoMessage:message';

export const DialogManagerContext = React.createContext();

export default class AppDialog extends PureComponent {
    static propTypes = {
        narrative: PropTypes.shape({
            summary: PropTypes.any,
            description: PropTypes.any
        }),
        header: PropTypes.shape({
            text: PropTypes.string,
            icon: PropTypes.string.isRequired
        }).isRequired,
        successMessage: PropTypes.string,
        // an object containing model data to pass to the 
        // narrative and title templates
        data: PropTypes.object,
        children: PropTypes.any.isRequired,
        closeButton: PropTypes.bool,
        defaultOpen: PropTypes.bool.isRequired,
        busy: PropTypes.bool
    };
    static defaultProps = {
        data: {},
        closeButton: true,
        defaultOpen: true
    };

    state = {
        visible: this.props.defaultOpen,
        errors: null
    };

    componentDidMount() {
        document.addEventListener('yuiModalOpen', this.handleYuiModalOpen);
        document.addEventListener('yuiModalClose', this.handleYuiModalClose);
    }
    componentWillUnmount() {
        document.removeEventListener('yuiModalOpen', this.handleYuiModalOpen);
        document.removeEventListener('yuiModalClose', this.handleYuiModalClose);
    }

    handleYuiModalOpen = (e) => {
        if (this.dialog) {
            try {
                const dialog = e.detail.dialog;
                if (dialog) {
                    const container = dialog.get('boundingBox')._node;

                    //if a YUI modal opens - register it in the modal manager
                    this.dialog.modal.props.manager.add(dialog, container);
                }
            } catch (ignore) {
                console.error('unable to register modal ' + ignore);// eslint-disable-line no-console
            }
        }
    };
    handleYuiModalClose = (e) => {
        if (this.dialog) {
            try {
                //if a YUI modal opens - remove it in the modal manager
                this.dialog.modal.props.manager.remove(e.detail.dialog);
            } catch (ignore) {
                console.error('unable to de-register modal ' + ignore);// eslint-disable-line no-console
            }
        }
    };
    getNarrative = memoize((narrative, data) => {
        let narrativeOutput = false;

        if (narrative) {
            let narrativeSummary;
            if (narrative.summary) {
                if (React.isValidElement(narrative.summary)) {
                    narrativeSummary = narrative.summary;
                } else {
                    narrativeSummary = renderTemplate(narrative.summary, data);
                }
            }

            let narrativeDescription;
            if (narrative.description) {
                if (React.isValidElement(narrative.description)) {
                    narrativeDescription = narrative.description;
                }
                else {
                    narrativeDescription = renderTemplate(narrative.description, data);
                }

            }

            if (narrativeSummary || narrativeDescription) {
                narrativeOutput = (<Narrative key="narrative" summary={narrativeSummary} description={narrativeDescription} />);
            }
        }
        return narrativeOutput;
    });
    getHeader = memoize((header, closeButton, data) => {
        let titleOutput = false;
        if (header) {
            let title;
            if (header.text) {
                title = renderTemplate(header.text, data);
            }
            titleOutput = (<Title id="labelledby" key="title" icon={header.icon} title={title} />);
        }
        let closeButtonOutput;
        if (closeButton) {
            closeButtonOutput = (
                <span className="yui3-widget-buttons">
                    <button type="button" className="yui3-button yui3-button-close" onClick={this.handleCloseClick}>Close</button>
                </span>
            );
        }
        return (
            <>
                {titleOutput}
                {closeButtonOutput}
            </>
        );
    });
    handleCloseClick = (e) => {
        const { busy } = this.props;
        e.preventDefault();
        if (!busy) {
            this.hideDialog();
        }
    };
    handleHide = () => {
        this.hideDialog();
    };
    setVisibility(visible) {
        this.setState({
            visible: visible
        });
    }
    hideDialog() {
        this.setVisibility(false);
    }
    showDialog() {
        this.setVisibility(true);
    }
    fireSuccessMessage(data, options) {
        const { successMessage } = this.props;
        const opts = options || {};
        if (successMessage) {
            const message = renderTemplate(successMessage, data || {});
            const event = new CustomEvent(EVT_INFO_MESSAGE, {
                bubbles: true,
                cancelable: true,
                detail: {
                    ...opts,
                    message: message
                }
            });
            ReactDOM.findDOMNode(this).dispatchEvent(event);

        }
    }
    dialogRef = (ref) => this.dialog = ref;
    handleMaskBlur = (e) => {
        e.preventDefault();
        e.target.focus();
    };
    render() {
        const { narrative, header, data, closeButton, children, defaultOpen: defaultOpenIgnored, successMessage: successMessageIgnored, ...other } = this.props;
        const { visible } = this.state;
        return (
            <Dialog
                ref={this.dialogRef}
                aria-labelledby="labelledby"
                aria-describedby="describedby"
                title={this.getHeader(header, closeButton, data)}
                onHide={this.handleHide}
                show={visible}
                backdrop={'static'}
                closeButton={false}
                {...other}>
                {this.getNarrative(narrative, data)}
                <DialogManagerContext.Provider value={this.dialog?this.dialog.modal.props.manager:undefined}>
                    {children}    
                </DialogManagerContext.Provider>
                
            </Dialog>
        );
    }
}