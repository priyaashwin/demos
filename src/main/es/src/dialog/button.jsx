import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const getButtonClassNames = function (type, disabled) {
    return classnames('pure-button', {
        'pure-button-active': 'active' === type,
        'pure-button-primary': 'primary' === type,
        'pure-button-secondary': 'secondary' === type,
        'pure-button-text': 'text' === type,
        'pure-button-disabled': disabled    
    });
};

export default class Button extends PureComponent {
    static propTypes = {
        title: PropTypes.string.isRequired,
        children: PropTypes.any,
        icon: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        disabled: PropTypes.bool,
        type: PropTypes.oneOf(['primary', 'secondary', 'text', 'active'])

    };

    static defaultProps = {
        type: 'primary',
        disabled: false
    };

    handleClick = (e) => {
        const { disabled, onClick } = this.props;
        e.preventDefault();

        if (!disabled) {
            onClick();
        }
    }
    render() {
        const { title, icon, children, type, disabled } = this.props;
        return (
            <button className={getButtonClassNames(type, disabled)} title={title} onClick={this.handleClick} disabled={disabled} aria-disabled={disabled}>
                <i className={classnames('fa', icon)} aria-hidden="true" />
                {children}
            </button>
        );
    }
}
