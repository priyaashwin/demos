'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import memoize from 'memoize-one';

const RT_EDITOR = 'rt-editor';
const ERROR_CLASS = 'error';

/**
 * Sets CSS error styles on input nodes based on the parsed error response
 *
 * @method applyErrorClass
 * @param {HTMLElement} formNode The HTML form element to highlight within
 * @param {Object} errors Object containing errors
 * @private
 */
const applyErrorClass = function (formNode, errors) {
    if (formNode && errors) {
        //get form id
        const formId = formNode.id;
        //get form elements
        const elements = formNode.elements;
        //get any richtext editable
        const richTextElements = formNode.getElementsByClassName(RT_EDITOR);

        errors.forEach(error => {
            if (typeof error === 'object') {
                //for each validation error - set the CSS class on the appropriate input
                Object.keys(error).forEach(k => {
                    const elemId = `${formId}_${k}`;
                    //find by name or id
                    const elem = elements[elemId] || elements[k] || richTextElements[elemId] || richTextElements[k];
                    if (elem) {
                        if (elem.tagName) {
                            //add the error class
                            elem.classList.add(ERROR_CLASS);
                        } else if (elem.length) {
                            //stick on first element
                            elem.item(0).classList.add(ERROR_CLASS);
                        }
                    }
                });
            }
        });
    }
};

/**
 * Removed the CSS error class from all fields in the html form
 *
 * @method removeErrorClass
 * @param {HTMLElement} formNode The HTML form element
 * @private
 */
const removeErrorClass = function (formNode) {
    if (formNode) {
        //get form elements
        const elements = formNode.elements;
        for (let i = 0; i < elements.length; i++) {
            elements[i].classList.remove(ERROR_CLASS);
        }

        //get any richtext editable
        const richTextElements = formNode.getElementsByClassName(RT_EDITOR);
        for (let i = 0; i < richTextElements.length; i++) {
            richTextElements[i].classList.remove(ERROR_CLASS);
        }
    }
};

/**
* Focus on the field identified by name.
*
* @method focusOnField
* @param {HTMLElement} formNode The HTML form element to highlight within
* @param {String} fldName The name of the field
* @param {Number} index The optional index to use when multiple elements are present
* @private
*/
const focusOnField = function (formNode, fldName, index) {
    if (formNode && fldName) {
        //get form id
        const formId = formNode.id;
        //get form elements
        const elements = formNode.elements;

        const richTextElements = formNode.getElementsByClassName(RT_EDITOR);

        const elemId = `${formId}_${fldName}`;

        //get any richtext editable
        const elem = elements[fldName] || elements[elemId] || richTextElements[fldName] || richTextElements[elemId];
        if (elem) {
            if (elem.tagName) {
                if (elem.tagName.toLowerCase() === 'fieldset') {
                    const fElems = elem.elements||elem.getElementsByTagName('input');
                    if (fElems.length) {
                        //iterate until we can focus
                        for (let i = 0, l = fElems.length; i < l; i++) {
                            const el = fElems[i];
                            if (el.tagName && el.tagName.toLowerCase() === 'input') {
                                //focus on the first item that will take it
                                el.focus();
                                break;
                            }
                        }
                    }
                } else {
                    //focus on element
                    elem.focus();
                }
            } else if (elem.length) {
                //iterate until we can focus
                for (let i = index || 0; i < elem.length; i++) {
                    const el = elem.item(i);
                    if (el.tagName && el.tagName.toLowerCase() === 'input') {
                        //focus on the first item that will take it
                        el.focus();
                        break;
                    }
                }
            }
        }
    }
};

export default class Errors extends PureComponent {
    static propTypes = {
        errors: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.object,
            PropTypes.string,
        ]),
        type: PropTypes.string,
        title: PropTypes.any,
        onClose: PropTypes.func.isRequired,
        target: PropTypes.oneOfType([PropTypes.node, PropTypes.func])
    }

    static defaultProps = {
        errors: null,
        type: 'error',
        title: 'Sorry, there was a problem with the form'
    };
    componentDidMount() {
        this.setUpErrorState();
    }
    componentDidUpdate() {
        this.clearErrorState();
        this.setUpErrorState();
    }
    clearErrorState() {
        const target = this.getTarget();
        if (target) {
            removeErrorClass(target);
        }
    }
    setUpErrorState() {
        const target = this.getTarget();
        if (target) {
            const { errors } = this.props;
            const errorsArray = this.getErrors(errors);

            applyErrorClass(target, errorsArray);
        }
    }
    getTarget = () => {
        const { target } = this.props;
        const targetElement = typeof target === 'function' ? target() : target;
        const element = targetElement && ReactDOM.findDOMNode(targetElement) || null;
        if (element) {
            return element.querySelector('form');
        }

    };
    getErrors = memoize(errors => Array.isArray(errors) ? errors : [errors]);
    getErrorsContent() {
        const { errors } = this.props;
        const errorsArray = this.getErrors(errors);

        const errorsContent = [];

        errorsArray.forEach((error, i) => {
            if (typeof error === 'string') {
                errorsContent.push(<li key={i}>{error}</li>);
            } else {
                Object.keys(error).forEach(k => {
                    errorsContent.push(<li key={i + '' + k}><a className="error-key" onClick={this.handleErrorClick} href={`#${k}`}>{error[k]}</a></li>);
                });
            }
        });

        return errorsContent;
    }
    handleClose = (e) => {
        e.preventDefault();

        //clear down any errors
        this.clearErrorState();

        //then call the parent close handler
        this.props.onClose();
    }
    handleErrorClick = (e) => {
        e.preventDefault();

        const hash = e.currentTarget.hash;
        let name;
        if (hash) {
            name = hash.substring(1);
        }
        focusOnField(this.getTarget(), name);
    };
    render() {
        const errors = this.getErrorsContent();
        const { type, title } = this.props;

        if (errors.length > 0) {
            return (
                <div className="usp-model-errors">
                    <div className={classnames('message', 'message-block', `message-${type}`)} role="alert">
                        <a href="#" className="usp-model-errors-close" title="Close" onClick={this.handleClose}>Close</a>
                        <h4>{title}</h4>
                        <ul>{errors}</ul>
                    </div>
                </div>
            );
        }
        return false;
    }
}
