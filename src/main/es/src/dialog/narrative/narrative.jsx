'use strict';
import React from 'react';
import PropTypes from 'prop-types';

const Narrative = ({ summary, description}) =>(
    <div className="pure-g readonly">
        <div className="pure-u-1 l-box">
            <div className="pure-narrative" id="describedby" aria-live="polite">
                <span className="narrative-summary">{summary}</span>
                <span className="narrative-description">{description}</span>
            </div>
        </div>
    </div>
);

Narrative.propTypes = {
    summary: PropTypes.any,
    description: PropTypes.any
};

export default Narrative;




