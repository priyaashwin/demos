'use strict';
import Textarea from './raw/textarea';
import {withLabel} from './hoc/withLabel';

export default withLabel(Textarea);