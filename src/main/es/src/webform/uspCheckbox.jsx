'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from './raw/input';
import { withLabel } from './hoc/withLabel';

const checkbox = withLabel(class Checkbox extends PureComponent {
    static propTypes = {
        checked: PropTypes.bool,
        className: PropTypes.string,
        onUpdate: PropTypes.func.isRequired
    };
    onUpdate = (name, value, target) => {
        let val = null;
        if (target.checked) {
            val = value;
        }
        this.props.onUpdate(name, val);
    };
    render() {
        const { className = '' } = this.props;
        return (
            <Input {...this.props} className={className} type="checkbox" onUpdate={this.onUpdate} />
        );
    }
}, true, { className: 'pure-checkbox', pre: true });

export default checkbox;