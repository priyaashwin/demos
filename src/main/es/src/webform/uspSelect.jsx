'use strict';
import Select from './raw/select';
import {withLabel} from './hoc/withLabel';

export default withLabel(Select);