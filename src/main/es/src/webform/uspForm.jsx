'use strict';
import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
const Form = forwardRef(({ id, stacked, children }, ref) => (
    <form id={id} action="#none" ref={ref} className={classnames('pure-form', {
        'pure-form-stacked': stacked
    })}>
        {children}
    </form>
));

Form.propTypes = {
    //control the stacked nature of the form
    id: PropTypes.string,
    stacked: PropTypes.bool,
    children: PropTypes.any
};

Form.defaultProps = {
    stacked: true
};

export default Form;
