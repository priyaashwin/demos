'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'usp-calendar';
import Input from './uspInput';
import Select from './uspSelect';
import ReactDOM from 'react-dom';
import Overlay from 'react-bootstrap/lib/Overlay';
import { isSpecified } from '../utils/js/textUtils';

const RELATIVE_POS = {
    position: 'relative'
};
const FIXED_POS = {
    position: 'fixed',
    zIndex: 1
};
const CALENDAR_STYLE = {
    marginTop: -150
};
export const getFuzzyMoment = function (fuzzyDate) {
    let fuzzyMoment;
    if (fuzzyDate && fuzzyDate.year) {
        const dateStr = [`${fuzzyDate.year}`];
        const fomatStr = ['YYYY'];
        if (fuzzyDate.month !== undefined) {
            //month is zero based so +1 to convert to format for parse
            dateStr.push(`${Number(fuzzyDate.month) + 1}`.padStart(2, '0'));
            fomatStr.push('MM');
            if (fuzzyDate.day !== undefined) {
                dateStr.push(`${fuzzyDate.day}`.padStart(2, '0'));
                fomatStr.push('DD');
            }
        }

        fuzzyMoment = moment.tz(dateStr.join(''), fomatStr.join(''), 'Europe/London');
    }
    return fuzzyMoment;
};

const monthOptions = moment.monthsShort().map((month, i) => (
    {
        text: month,
        value: `${i}`
    }
));

const maxDate = moment().endOf('year');



const getDaysOfMonth = function (year, month) {
    const days = [];
    if (isSpecified(year) && isSpecified(month)) {
        //month is zero based - so need to +1 to value we can parse
        const monthVal = `${Number(month) + 1}`.padStart(2, '0');
        const dobMoment = moment.tz(`${year}${monthVal}`, 'YYYYMM', 'Europe/London');
        const daysInMonth = dobMoment.daysInMonth();
        for (let i = 1; i <= daysInMonth; i++) {
            days.push({
                text: `${i}`,
                value: `${i}`
            });
        }
    }
    return days;
};

export default class FuzzyDateInput extends PureComponent {
    static propTypes = {
        id: PropTypes.string,
        onUpdate: PropTypes.func.isRequired,
        fuzzyDate: PropTypes.shape({
            year: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            month: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            day: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        }),
        dialogManager: PropTypes.object
    };

    state = {
        visible: false
    };

    componentDidCatch() {
        //on an error - clear the fuzzy date
        this.updateFuzzyDate(undefined);
    }

    handleSelectedDateChange = (date) => {
        let fuzzyDate;

        if (date) {
            fuzzyDate = {};
            const dMoment = moment.tz(date, 'Europe/London');
            fuzzyDate.year = dMoment.get('year');
            fuzzyDate.month = dMoment.get('month');
            fuzzyDate.day = dMoment.get('date');
        }
        //hide calendar
        this.setCalendarVisibility(false);

        this.updateFuzzyDate(fuzzyDate);
    };
    handleFuzzyDateChange = (name, value) => {
        //fuzzy date could be null
        const inFuzzyDate = this.props.fuzzyDate || {};

        const fuzzyDate = {
            year: inFuzzyDate.year,
            month: inFuzzyDate.month,
            day: inFuzzyDate.day,
        };
        const updateValue = isSpecified(value) ? `${value}` : undefined;

        switch (name) {
            case 'day':
                fuzzyDate.day = updateValue;
                break;
            case 'month':
                fuzzyDate.month = updateValue;
                fuzzyDate.day = undefined;
                break;
            case 'year': {
                fuzzyDate.year = updateValue;
                //reset the day - but not the month
                fuzzyDate.day = undefined;
                break;
            }

        }

        this.updateFuzzyDate(fuzzyDate);
    }
    updateFuzzyDate(fuzzyDate) {
        this.props.onUpdate(fuzzyDate || {});
    }
    toggleCalendar = (e) => {
        e.preventDefault();
        this.setCalendarVisibility(!this.state.visible);
    };
    hideCalendar = () => {
        this.setCalendarVisibility(false);
    }
    setCalendarVisibility(visible) {
        const { dialogManager } = this.props;
        this.setState({
            visible: visible
        }, () => {
            if (dialogManager && this.overlay) {
                if (visible) {
                    dialogManager.add(this.overlay, this.target);
                } else {
                    dialogManager.remove(this.overlay);
                }
            }
        });
    }
    overlayRef = (ref) => this.overlay = ref;
    findTarget = () => ReactDOM.findDOMNode(this.target);
    targetRef = (ref) => this.target = ref;
    containerRef = (ref) => this.container = ref;
    findContainer = () => ReactDOM.findDOMNode(this.container);
    render() {
        const { id, fuzzyDate = {} } = this.props;
        const fuzzyMoment = getFuzzyMoment(fuzzyDate);
        return (
            <>
                <fieldset id={id} tabIndex={0} className="pure-fieldset">
                    <div className="pure-g">
                        <div className="pure-u-3-8 l-box" >
                            <Overlay
                                key="datePickerOverlay"
                                show={this.state.visible}
                                onHide={this.hideCalendar}
                                placement="right"
                                container={this.findContainer}
                                rootClose={true}
                                ref={this.overlayRef}
                                target={this.findTarget}>
                                <DatePicker
                                    key="datePicker"
                                    selectedDate={fuzzyMoment ? fuzzyMoment.valueOf() : undefined}
                                    onChange={this.handleSelectedDateChange}
                                    maximumDate={maxDate.valueOf()}
                                    style={CALENDAR_STYLE}
                                />
                            </Overlay>
                            <div className="input-group input-prepend calendar">
                                <span ref={this.targetRef}>
                                    <a
                                        className="pure-button pure-button-active pure-button-cal"
                                        title="Display calendar"
                                        onClick={this.toggleCalendar}>
                                        <i className="fa fa-calendar icon-calendar" />
                                    </a>
                                </span>
                                <Input
                                    name="year"
                                    id={`${id}_year`}
                                    label="Year"
                                    value={fuzzyDate.year}
                                    type="number"
                                    min="1886"
                                    max={maxDate.get('year')}
                                    placeholder="YYYY"
                                    onUpdate={this.handleFuzzyDateChange}
                                    autoComplete="off" />
                            </div>
                        </div>
                        <div className="pure-u-3-8 l-box">
                            <Select
                                name="month"
                                id={`${id}_month`}
                                label="Month"
                                options={monthOptions}
                                value={fuzzyDate.month}
                                disabled={!isSpecified(fuzzyDate.year)}
                                onUpdate={this.handleFuzzyDateChange}
                                className="pure-input-1 optional"
                                includeEmptyOption={' '}
                            />
                        </div>
                        <div className="pure-u-1-4 l-box">
                            <Select
                                name="day"
                                id={`${id}_day`}
                                label="Day"
                                options={getDaysOfMonth(fuzzyDate.year, fuzzyDate.month)}
                                value={fuzzyDate.day}
                                disabled={!isSpecified(fuzzyDate.month)}
                                onUpdate={this.handleFuzzyDateChange}
                                className="pure-input-1 optional"
                                includeEmptyOption={' '}
                            />
                        </div>
                    </div>
                </fieldset>
                <div style={RELATIVE_POS}>
                    <div style={FIXED_POS} ref={this.containerRef} />
                </div>
            </>
        );
    }
}

