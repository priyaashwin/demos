'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'usp-calendar';
import Input from './raw/input';
import ReactDOM from 'react-dom';
import Overlay from 'react-bootstrap/lib/Overlay';
import { formatDate } from 'usp-date';

const RELATIVE_POS={
    position:'relative'
};
const FIXED_POS={
    position:'fixed',
    zIndex:1
};
const CALENDAR_STYLE={
    marginTop:-150
};
export default class DateInput extends PureComponent {
    static propTypes = {
        id: PropTypes.string,
        name: PropTypes.string.isRequired,
        className: PropTypes.string,
        onUpdate: PropTypes.func.isRequired,
        mandatory: PropTypes.bool,
        maximumDate: PropTypes.number,
        minimumDate: PropTypes.number,
        value: PropTypes.string,
        dialogManager: PropTypes.object,
        customCalendarStyle: PropTypes.object
    };

    state = {
        visible: false
    };    
    componentDidCatch() {
        const { name } = this.props;
        //on an error - clear the date
        this.handleUpdate(name, undefined);
    }
    handleUpdate = (name, value) => {
        const { onUpdate } = this.props;
        onUpdate(name, value);
    };
    handleSelectedDateChange = (date) => {
        const { name } = this.props;
        //hide calendar
        this.setCalendarVisibility(false);

        this.handleUpdate(name, formatDate(date, true));
    };
    handleBlur=(e) => {
        const {name}=this.props;
        const  value=e.target.value||'';
        const mod = value.substring(0, 1);

        //check the modifier
        if (mod === '+' || mod === '-') {
            //grab the value
            const amount = Number(value);
            if(!isNaN(amount)){
                const d=moment().tz('Europe/London').add(amount, 'days');
                this.handleUpdate(name, formatDate(d, true));
            }
        }
    };
    toggleCalendar = (e) => {
        e.preventDefault();
        this.setCalendarVisibility(!this.state.visible);
    };
    setCalendarVisibility(visible) {
        const { dialogManager } = this.props;
        this.setState({
            visible: visible
        }, () => {
            if (dialogManager && this.overlay) {
                if (visible) {
                    dialogManager.add(this.overlay, this.target);
                } else {
                    dialogManager.remove(this.overlay);
                }
            }
        });
    }
    hideCalendar = () => {
        this.setCalendarVisibility(false);
    }
    populateCurrentDate = () => {
        const { name } = this.props;
        //call change handler with new date
        this.handleUpdate(name, formatDate(Date.now(), true));
        this.setCalendarVisibility(false);
    };
    overlayRef = (ref) => this.overlay = ref;
    findTarget = () => ReactDOM.findDOMNode(this.target);
    targetRef = (ref) => this.target = ref;
    containerRef = (ref) => this.container = ref;
    findContainer = () => ReactDOM.findDOMNode(this.container);
    render() {
        const { id, maximumDate, minimumDate, value, customCalendarStyle, ...other } = this.props;
        return (
            <>
                <Overlay
                    key="datePickerOverlay"
                    show={this.state.visible}
                    onHide={this.hideCalendar}
                    placement="left"
                    container={this.findContainer}
                    rootClose={true}
                    ref={this.overlayRef}
                    target={this.findTarget}>
                    <DatePicker
                        key="datePicker"
                        ref={this.datePickerRef}
                        selectedDate={value}
                        onChange={this.handleSelectedDateChange}
                        maximumDate={maximumDate}
                        minimumDate={minimumDate}
                        style={customCalendarStyle || CALENDAR_STYLE}
                    />
                </Overlay>
                <div className="input-group input-append calendar">
                    <Input
                        key="date-input"
                        {...other}
                        onDoubleClick={this.populateCurrentDate}
                        onBlur={this.handleBlur}
                        id={id}
                        value={value}
                        onUpdate={this.handleUpdate}
                    />
                    <span>
                        <a
                            className="pure-button pure-button-active pure-button-cal"
                            ref={this.buttonRef}
                            title="Display calendar"
                            onClick={this.toggleCalendar}>
                            <i className="fa fa-calendar icon-calendar" />
                        </a>
                    </span>
                </div>
                <div style={RELATIVE_POS}>
                    <div style={FIXED_POS} ref={this.containerRef}/>
                </div>
            </>
        );
    }
}

