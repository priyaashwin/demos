'use strict';
import Input from './raw/input';
import {withLabel} from './hoc/withLabel';

export default withLabel(Input);