'use strict';
import { withLabel } from './hoc/withLabel';
import EnumerationSelect from './raw/enumerationSelect';

export default withLabel(EnumerationSelect);