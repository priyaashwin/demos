'use strict';
import Select from './select';
import { withEnumerationOptions } from '../hoc/withEnumerationOptions';

export default withEnumerationOptions(Select);