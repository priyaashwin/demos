'use strict';
import Select from './select';
import { withCodedEntryOptions } from '../hoc/withCodedEntryOptions';

export default withCodedEntryOptions(Select);