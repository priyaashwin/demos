'use strict';
import React, {memo} from 'react';
import PropTypes from 'prop-types';
import Mandatory from './mandatory';

const Label = memo(({ forId, label, className, mandatory, pre, children}) => {
    let labelOutput;

    if(mandatory){
        labelOutput=(
            <>
                {label}
                {' '}
                <Mandatory />
            </>
        );
    }else{
        labelOutput=label;
    }
    if(React.Children.count(children)){
        if(pre){
            labelOutput=(
                <>
                    {children}
                    {' '}
                    {labelOutput}
                </>
            );
        }else{
            labelOutput=(
                <>
                    {labelOutput}
                    {' '}
                    {children}
                </>
            );
        }
        
    }
    return (<label htmlFor={forId} className={className}>{labelOutput}</label>);    
});

Label.propTypes = {
    forId: PropTypes.string,
    label: PropTypes.string.isRequired,
    pre: PropTypes.bool,
    className:PropTypes.string,
    children: PropTypes.any,
    mandatory: PropTypes.bool
};

export default Label;


