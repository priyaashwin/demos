'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';
const pleaseSelect = 'Please choose';

const getOptions = function (options) {
    return (options || []).map((optionProp, i) => {
        if (Array.isArray(optionProp.value)) {
            //option group
            return (<optgroup key={'optgroup' + i} label={optionProp.text}>{this.getOptions(optionProp.value)}</optgroup>);
        } else {
            return (<option key={'option' + optionProp.value} value={optionProp.value}>{optionProp.text}</option>);
        }
    });
};

class Select extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        className: PropTypes.string,
        options: PropTypes.arrayOf(PropTypes.shape({
            text: PropTypes.any,
            value: PropTypes.any
        })).isRequired,
        includeEmptyOption: PropTypes.oneOfType([
            PropTypes.bool,
            PropTypes.string
        ]),
        emptyOptionsLabel: PropTypes.string,
        value: PropTypes.any,
        onUpdate: PropTypes.func.isRequired,
        innerRef: PropTypes.oneOfType([
            PropTypes.func,
            PropTypes.shape({ current: PropTypes.instanceOf(Element) })
        ])
    };

    static defaultProps = {
        name: 'select',
        className: 'pure-input-1',
        includeEmptyOption: false,
        options: []
    };

    handleValueChange = (e) => {
        this.props.onUpdate(e.target.name, e.target.value);
    };

    render() {
        const { innerRef, id, name, value, className, options, includeEmptyOption, onUpdate: onUpdateIgnored, emptyOptionsLabel, ...other } = this.props;
        let emptyOption;
        if (options && options.length === 0 && emptyOptionsLabel) {
            //include 
            emptyOption = (<option key="_blank" className="blank" value="">{emptyOptionsLabel}</option>);
        } else if (includeEmptyOption) {        //inlcudeEmptyOption is either a boolean or a string
            let emptyOptionText;
            if (typeof includeEmptyOption === 'string') {
                emptyOptionText = includeEmptyOption;
            } else {
                emptyOptionText = pleaseSelect;
            }
            emptyOption = (<option key="_blank" className="blank" value="">{emptyOptionText}</option>);
        }

        //setting an input value to null means it is no longer managed
        const selectedValue = (value !== undefined && value !== null) ? value : '';

        return (
            <select ref={innerRef} {...other} id={id} className={className} name={name} value={selectedValue} onChange={this.handleValueChange} >
                {emptyOption}
                {getOptions(options)}
            </select >
        );
    }
}


export default forwardRef((props, ref) => (
    <Select {...props} innerRef={ref} />
));