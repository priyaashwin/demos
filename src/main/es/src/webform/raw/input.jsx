'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';

class Input extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        className: PropTypes.string,
        name: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        value: PropTypes.any,
        onUpdate: PropTypes.func.isRequired,
        innerRef: PropTypes.oneOfType([
            PropTypes.func,
            PropTypes.shape({ current: PropTypes.instanceOf(Element) })
        ])
    };

    static defaultProps = {
        name: 'input',
        className: 'pure-input-1',
        type: 'text'
    };

    handleValueChange = (e) => {
        this.props.onUpdate(e.target.name, e.target.value, e.target);
    };

    render() {
        const {innerRef, id, name, type, value, className, onUpdate: onUpdateIgnored, ...other } = this.props;

        const inputValue = (value !== undefined && value !== null) ? value : '';

        return (
            <input ref={innerRef} {...other} id={id} className={className} type={type} name={name} value={inputValue} onChange={this.handleValueChange} />
        );
    }
}

export default forwardRef((props, ref) => (
    <Input {...props} innerRef={ref} />
));