'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';

class Textarea extends PureComponent {
    static propTypes = {
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        className: PropTypes.string,
        rows: PropTypes.number,
        cols: PropTypes.number,
        value: PropTypes.any,
        onUpdate: PropTypes.func.isRequired,
        innerRef: PropTypes.oneOfType([
            PropTypes.func,
            PropTypes.shape({ current: PropTypes.instanceOf(Element) })
        ])
    };

    static defaultProps = {
        name: 'textarea',
        className: 'pure-input-1',
        rows: 3
    };

    handleValueChange = (e) => {
        this.props.onUpdate(e.target.name, e.target.value, e.target);
    };

    render() {
        const { innerRef, id, name, value, className, onUpdate: onUpdateIgnored, ...other } = this.props;

        const inputValue = (value !== undefined && value !== null) ? value : '';

        return (
            <textarea ref={innerRef} {...other} id={id} className={className} name={name} value={inputValue} onChange={this.handleValueChange} />
        );
    }
}

export default forwardRef((props, ref) => (
    <Textarea {...props} innerRef={ref} />
));