'use strict';
import Label from './raw/label';
import {withHelp} from './hoc/withHelp';

export default withHelp(Label);