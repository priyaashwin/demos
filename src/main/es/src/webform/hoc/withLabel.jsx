'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';
import Label from '../raw/label';

export function withLabel(WrappedComponent, wrap, labelProps) {
  class InputWithLabel extends PureComponent {
    static propTypes = {
      id: PropTypes.any.isRequired,
      label: PropTypes.string.isRequired,
      mandatory: PropTypes.bool,
      forwardedRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.instanceOf(Element) })
      ])
    };

    render() {
      const { forwardedRef, id, label, mandatory, ...other } = this.props;
      const { ...otherLabelProps } = labelProps || {};

      const component = (<WrappedComponent ref={forwardedRef} {...other} id={id} />);

      /* eslint-disable react/no-children-prop */
      return (
        <>
          <Label {...otherLabelProps} key={`label-${id}`} forId={id} label={label} mandatory={mandatory} children={wrap ? component : undefined} />
          {wrap ? false : component}
        </>
      );
      /* eslint-enable react/no-children-prop */
    }
  }

  InputWithLabel.displayName = `withLabel(${getDisplayName(WrappedComponent)})`;

  return forwardRef((props, ref) => {
    return <InputWithLabel {...props} forwardedRef={ref} />;
  });
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
