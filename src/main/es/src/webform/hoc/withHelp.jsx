'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InfoPop from '../../infopop/infopop';

export function withHelp(WrappedComponent) {
    class LabelWithHelp extends PureComponent {
        static propTypes = {
            help: PropTypes.any
        };

        getHelp(help) {
            if (help) {
                return (
                    <InfoPop key="labelHelp" content={help} className="help-icon" align="right">
                        <span className="info-icon"><i className="fa fa-info-circle" aria-hidden={true} /></span>
                    </InfoPop>
                );
            }

            return false;
        }
        render() {
            const { help, ...other } = this.props;

            return (
                <WrappedComponent {...other}>
                    {this.getHelp(help)}
                </WrappedComponent>
            );
        }
    }

    LabelWithHelp.displayName = `withHelp(${getDisplayName(WrappedComponent)})`;
    return LabelWithHelp;
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
