'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';

const getArrayFromEnumeration = function (enumeration) {
  const valuesList = enumeration.values || {};
  return valuesList.map(entry => ({
    text: entry.name,
    value: entry.enumValue
  }));
};

export function withEnumerationOptions(WrappedComponent) {
  class InputWithEnumerationOptions extends PureComponent {
    static propTypes = {
      enumeration: PropTypes.shape({
        values: PropTypes.array.isRequired
      }).isRequired,
      forwardedRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.instanceOf(Element) })
      ])
    };

    getEnumerationList() {
      const { enumeration } = this.props;
        return getArrayFromEnumeration(enumeration);
    }
    render() {
      const { forwardedRef, ...other } = this.props;

      return (<WrappedComponent ref={forwardedRef} {...other} options={this.getEnumerationList()} />);
    }
  }

  InputWithEnumerationOptions.displayName = `withEnumerationOptions(${getDisplayName(WrappedComponent)})`;

  return forwardRef((props, ref) => {
    return <InputWithEnumerationOptions {...props} forwardedRef={ref} />;
  });
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
