'use strict';
import React, { forwardRef, PureComponent } from 'react';
import PropTypes from 'prop-types';

const getArrayFromCodedEntries = function(codedEntries) {
    const valuesList = Object.values(codedEntries || {});
    return valuesList.map(entry => ({
        text: entry.name,
        value: entry.code
    }));
};

export function withCodedEntryOptions(WrappedComponent) {
    class InputWithCodedEntryOptions extends PureComponent {
        static propTypes = {
            category: PropTypes.shape({
                getActiveCodedEntriesForParent: PropTypes.func.isRequired,
                getActiveCodedEntries: PropTypes.func.isRequired
            }).isRequired,
            isChildCategory: PropTypes.bool,
            parentValue: PropTypes.any,
            forwardedRef: PropTypes.oneOfType([
                PropTypes.func,
                PropTypes.shape({ current: PropTypes.instanceOf(Element) })
            ])
        };

        static defaultProps = {
            isChildCategory: false
        };

        getCodedEntryList() {
            const { isChildCategory, category, parentValue } = this.props;
            if (!isChildCategory) {
                return getArrayFromCodedEntries(category.getActiveCodedEntries());
            } else {
                return getArrayFromCodedEntries(category.getActiveCodedEntriesForParent(parentValue));
            }
        }
        render() {
            const {
                forwardedRef,
                isChildCategory: isChildCategoryIgnored,
                parentValue: parentValueIgnored,
                ...other
            } = this.props;

            return <WrappedComponent ref={forwardedRef} {...other} options={this.getCodedEntryList()} />;
        }
    }

    InputWithCodedEntryOptions.displayName = `withCodedEntryOptions(${getDisplayName(WrappedComponent)})`;

    return forwardRef((props, ref) => {
        return <InputWithCodedEntryOptions {...props} forwardedRef={ref} />;
    });
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
