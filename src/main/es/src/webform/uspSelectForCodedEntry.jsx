'use strict';
import { withLabel } from './hoc/withLabel';
import CodedEntrySelect from './raw/codedEntrySelect';

export default withLabel(CodedEntrySelect);