'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from './raw/input';
import { withLabel } from './hoc/withLabel';

const radio = withLabel(class Radio extends PureComponent {
    static propTypes = {
        checked: PropTypes.bool,
        onUpdate: PropTypes.func.isRequired,
        className: PropTypes.string

    };
    onUpdate=(name, value, target)=>{
        let val=[];

        //lookup elements by name from the form container
        const selectionControls = target.form.querySelectorAll('input[name="' + name + '"]');

        for(let i = 0, l=selectionControls.length; i < l; ++i){

            if (selectionControls[i].checked===true) {
                val.push(selectionControls[i].value);
            }
        }

        //update val - use a single item, or null if empty, otherwise use the array.
        val = val.length === 1 ? val[0] : (val.length === 0 ? null : val);

        this.props.onUpdate(name, val);
    };
    render() {
        const {className=''}=this.props;
        return (
            <Input {...this.props} className={className} type="radio" onUpdate={this.onUpdate}/>
        );
    }
}, true, {className:'pure-radio', pre:true});

export default radio;