'use strict';
import { isSpecified } from '../../../utils/js/textUtils';
import {DATE_STRING_REG} from '../../../date/date';

/**
 * Validate fields that are common to Organisation & Team
 * @param {object} data to validate
 * @param {object} labels for validation errors
 */
const validateCommonFields = function (data, labels) {
    const errors = {};

    if (!isSpecified(data.name)) {
        errors.name = labels.nameMandatory;
    }

    if (isSpecified(data.effectiveDate)) {
        if (!DATE_STRING_REG.test(data.effectiveDate)) {
            errors.effectiveDate = labels.effectiveDateInvalidDate;
        }
        //if the effective date is specified then the parent organisation must be supplied
        if (!isSpecified(data.parentOrganisation) || !isSpecified(data.parentOrganisation.id)) {
            errors.effectiveDate = labels.parentOrganisationIdRequired;
        }
    } else {
        if (isSpecified(data.parentOrganisation) && isSpecified(data.parentOrganisation.id)) {
            errors.effectiveDate = labels.effectiveDateRequired;
        }
    }

    if(isSpecified(data.startDate)) {
        if (!DATE_STRING_REG.test(data.startDate)) {
            errors.startDate = labels.startDateInvalid;
        }
    }
    return Object.keys(errors).length > 0 ? errors : null;
};

/**
 * Validate Organisation data
 * @param {object} data - organisation data
 * @param {object} labels for validation errors
 * @returns {object} errors object or null if data passes validation
 */
export const validateOrganisationDataForAdd = function (data, labels) {
    const errors = {};

    const commonErrors = validateCommonFields(data, labels);
    if (commonErrors) {
        //add organisation errors to return object
        Object.keys(commonErrors).forEach(k => errors[k] = commonErrors[k]);
    }

    return Object.keys(errors).length > 0 ? errors : null;
};

/**
 * Validate Team data
 * @param {object} data - team data
 * @param {object} labels for validation errors
 * @returns {object} errors object or null if data passes validation
 */
export const validateTeamDataForAdd = function (data, labels) {
    const errors = {};

    const commonErrors = validateCommonFields(data, labels);
    if (commonErrors) {
        //add organisation errors to return object
        Object.keys(commonErrors).forEach(k => errors[k] = commonErrors[k]);
    }

    if (!isSpecified(data.securityProfile) || !isSpecified(data.securityProfile.id)) {
        errors.securityRecordTypeConfigurationId = labels.securityRecordTypeConfigurationIdValidationErrorMessage;
    }

    return Object.keys(errors).length > 0 ? errors : null;
};