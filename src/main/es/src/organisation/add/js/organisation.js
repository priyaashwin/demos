'use strict';
import { Record } from 'immutable';
import { parseDate } from '../../../date/date';

export const getValue = function (val) {
    if (typeof val === 'undefined') {
        return null;
    }
    return val;
};

export const baseOrganisation = {
    name: undefined,
    description: undefined,
    type: undefined,
    subType: undefined,
    startDate: undefined,
    endDate: undefined
};

export class OrganisationForAdd extends Record({
    ...baseOrganisation,
    effectiveDate: undefined,
    parentOrganisation: undefined
}) {
    getData() {
        const effectiveDate = parseDate(this.effectiveDate);
        const startDate = parseDate(this.startDate);

        return {
            name: getValue(this.name),
            description: getValue(this.forename),
            type: getValue(this.type),
            subType: getValue(this.subType),
            effectiveDate: effectiveDate ? effectiveDate.valueOf() : undefined,
            startDate: startDate ? startDate.valueOf() : undefined,
            parentOrganisationId:this.parentOrganisation?this.parentOrganisation.id:undefined
        };
    }
}

export class Organisation extends Record({
    ...baseOrganisation,
    id:undefined,
    address:undefined,
    organisationIdentifier:undefined
}) {}