'use strict';
import { Record, Map, Seq } from 'immutable';
import { parseDate } from '../../../date/date';
import { baseOrganisation, getValue } from './organisation';

export class TeamForAdd extends Record({
    ...baseOrganisation,
    effectiveDate: undefined,
    parentOrganisation: undefined,
    securityProfile: undefined,
    region: undefined,
    sector:undefined,
    sectorSubType:undefined,
    members:Map() //new empty map
}) {
    getData() {
        const effectiveDate = parseDate(this.effectiveDate);
        const startDate = parseDate(this.startDate);

        const teamRelationships =[];

        if(this.members){
            Seq(this.members).forEach((roleMembers, roleId)=>{
                if(roleMembers && roleMembers.length){
                    roleMembers.forEach(rm=>{
                        teamRelationships.push(new PersonIdAndProfessionalTeamRelationshipTypeId({
                            professionalTeamRelationshipTypeId:roleId,
                            personId:rm.id
                        }).toJSON());
                    });
                }
            });
        }
        return {
            name: getValue(this.name),
            description: getValue(this.description),
            sector: getValue(this.sector),
            sectorSubType: getValue(this.sectorSubType),
            effectiveDate: effectiveDate ? effectiveDate.valueOf() : undefined,
            startDate: startDate ? startDate.valueOf() : undefined,
            parentOrganisationId: this.parentOrganisation ? this.parentOrganisation.id : undefined,
            securityRecordTypeConfigurationId: this.securityProfile ? this.securityProfile.id : undefined,
            region:getValue(this.region),
            teamRelationships: teamRelationships
        };
    }
}

export class Team extends Record({
    ...baseOrganisation,
    id: undefined,
    address: undefined,
    organisationIdentifier: undefined
}) {}

export class SecurityRecordTypeConfiguration extends Record({
    id: undefined,
    name: undefined
}) {}

export class PersonIdAndProfessionalTeamRelationshipTypeId extends Record({
    personId:undefined,
    professionalTeamRelationshipTypeId: undefined,
    _type: 'PersonIdAndProfessionalTeamRelationshipTypeId'
}){}