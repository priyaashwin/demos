'use strict';

class Endpoint {
    constructor() {
        this.parentOrganisationAutoCompleteEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.organisation.OrganisationWithAddressAndPreviousNames+json;charset=UTF-8'
            },
            params: {
                pageSize: 50,
                appendWildcard: true
            },
            responseType: 'json',
            responseStatus: 200
        };

        this.securityProfileAutoCompleteEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.securityextension.SecurityRecordTypeConfiguration+json;charset=UTF-8'
            },
            params: {
                pageSize: 50,
                useSoundex: true,
                appendWildcard: true,
                sortBy: [{name:'asc'}]
            },
            responseType: 'json',
            responseStatus: 200
        };

        this.saveNewOrganisationEndpoint={
            method: 'post',
            contentType: 'json',
            headers: {
              'X-Requested-With': 'XMLHttpRequest',        
              'Accept': 'application/json;charset=UTF-8'
            },
            data:{
            },
            responseType: 'json',
            responseStatus:201
          }; 

          this.saveNewTeamEndpoint={
            method: 'post',
            contentType: 'json',
            headers: {
              'X-Requested-With': 'XMLHttpRequest',        
              'Accept': 'application/json;charset=UTF-8'
            },
            data:{
            },
            responseType: 'json',
            responseStatus:201
          }; 

          this.professionalTeamRelationshipTypesEndpoint={
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.relationship.ProfessionalTeamRelationshipType+json;charset=UTF-8'
            },
            responseType: 'json',
            responseStatus: 200
          };

          this.professionalPersonAutocompleteCompleteEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.person.PersonWithAddressAndSecurityAccessStatus+json;charset=UTF-8'
            },
            params: {
                pageSize: 50,
                appendWildcard: true,
                personType:'professional',
                sortBy: {
                    name: 'asc'
                }
            },
            responseType: 'json',
            responseStatus: 200
        };
    }
}

export default new Endpoint();