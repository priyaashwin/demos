import './less/app.less';
import AddOrganisationDialog from './jsx/organisation/add';
import AddTeamDialog from './jsx/team/add';

export {AddOrganisationDialog, AddTeamDialog};
