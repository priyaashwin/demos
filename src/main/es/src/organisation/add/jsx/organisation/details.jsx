'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { OrganisationForAdd } from '../../js/organisation';
import Input from '../../../../webform/uspInput';
import TypeAndSubType from './typeAndSubType';
import EffectiveDate from '../effectiveDate';
import ParentOrganisationSelector from '../parentOrganisationSelector';
import Label from '../../../../webform/uspLabel';
import DateInput from '../../../../webform/uspDateInput';

const maxDate = moment();

export default class Details extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        codedEntries: PropTypes.shape({
            organisationTypeCategory: PropTypes.object.isRequired,
            organisationSubTypeCategory: PropTypes.object.isRequired
        }).isRequired,
        organisation: PropTypes.oneOfType([
            PropTypes.instanceOf(OrganisationForAdd)
        ]).isRequired,
        onUpdate: PropTypes.func.isRequired,
        organisationAutocompleteEndpoint: PropTypes.string.isRequired
    };    
    handleUpdate = (name, value) => {
        this.props.onUpdate(name, value);
    };

    renderTypeAndSubType() {
        const { labels, organisation, codedEntries } = this.props;
        const { type, subType } = organisation;
        return (
            <TypeAndSubType
                key="typeAndSubType"
                labels={labels}
                onUpdate={this.handleUpdate}
                type={type}
                subType={subType}
                codedEntries={codedEntries}
            />
        );
    }
    render() {
        const { labels, organisation, codedEntries, organisationAutocompleteEndpoint} = this.props;
        const { name, effectiveDate, startDate, parentOrganisation } = organisation;

        return (
            <div className="pure-g-r">
                <div className="pure-u-1 l-box">
                    <Input
                        key="name"
                        id="addOrganisation_name"
                        name="name"
                        label={labels.name}
                        mandatory={true}
                        value={name}
                        maxLength={100}
                        onUpdate={this.handleUpdate} />
                </div>
                {this.renderTypeAndSubType()}
                <div className="pure-u-1-2 l-box">
                <ParentOrganisationSelector 
                    key="parentOrganisation"
                    labels={labels}
                    name="parentOrganisationId"
                    onUpdate={this.handleUpdate}
                    codedEntries={codedEntries}
                    url={organisationAutocompleteEndpoint}
                    parentOrganisation={parentOrganisation}
                />
                </div>
                <div className="pure-u-1-2 l-box">
                    <EffectiveDate
                        key="effectiveDate"
                        labels={labels}
                        effectiveDate={effectiveDate}
                        onUpdate={this.handleUpdate}
                    />
                </div>
                <div className="pure-u-1-2 l-box">
                    <Label forId="startDate"
                        label={labels.startDate} />
                    <DateInput
                        id="startDate"
                        key="startDate"
                        name="startDate"
                        onUpdate={this.handleUpdate}
                        maximumDate={maxDate.valueOf()}
                        value={startDate}
                        placeholder="DD-MMM-YYYY" />
                </div>
            </div>
        );
    }
}