'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { OrganisationForAdd } from '../../js/organisation';
import AppDialog from '../../../../dialog/dialog';
import Errors from '../../../../dialog/errors/errors';
import Button from '../../../../dialog/button';
import Details from './details';
import Form from '../../../../webform/uspForm';
import Model from '../../../../model/model';
import { validateOrganisationDataForAdd } from '../../js/validation';
import { isSpecified } from '../../../../utils/js/textUtils';
import endpoint from '../../js/cfg';

const getDefaultState = function() {
    return {
        errors: null,
        loading: false,
        //always start with a fresh organisation model
        organisation: new OrganisationForAdd({})
    };
};

export default class AddOrganisationDialog extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        dialogConfig: PropTypes.shape({
            header: PropTypes.object.isRequired,
            narrative: PropTypes.shape({
                summary: PropTypes.string.isRequired,
                description: PropTypes.string
            }).isRequired,
            successMessage: PropTypes.string
        }).isRequired,
        codedEntries: PropTypes.shape({
            organisationTypeCategory: PropTypes.object.isRequired,
            organisationSubTypeCategory: PropTypes.object.isRequired
        }).isRequired,
        endpoints: PropTypes.shape({
            newOrganisationEndpoint: PropTypes.string.isRequired,
            newOwnedOrganisationEndpoint: PropTypes.string.isRequired,
            organisationAutocompleteEndpoint: PropTypes.string.isRequired
        })
    };
    state = getDefaultState(this.props);

    componentDidCatch() {
        this.setState({
            errors: 'Unexpected error'
        });
    }
    handleAddOrganisation = () => {
        const { labels, endpoints } = this.props;
        const { organisation } = this.state;

        this.setState(
            {
                loading: true
            },
            () => {
                const errors = validateOrganisationDataForAdd(organisation, labels.validation || {});
                if (null === errors) {
                    let url;
                    let modelType;
                    if (isSpecified(organisation.parentOrganisation)) {
                        url = endpoints.newOwnedOrganisationEndpoint;
                        modelType = 'NewChildOrganisation';
                    } else {
                        url = endpoints.newOrganisationEndpoint;
                        modelType = 'NewOrganisation';
                    }
                    const organisationModel = new Model(url);
                    //actually move onto the save
                    organisationModel
                        .save(endpoint.saveNewOrganisationEndpoint, {
                            _type: modelType,
                            ...organisation.getData()
                        })
                        .then(success => {
                            const dialog = this.dialog;
                            if (dialog) {
                                //fire success message - delay to next page turn
                                dialog.fireSuccessMessage(
                                    {},
                                    {
                                        delayed: true
                                    }
                                );
                                //fire saved
                                this.fireEvent('organisation:saved', {
                                    organisationId: success.id
                                });
                                //success
                                this.hideDialog();
                            }
                        })
                        .catch(reject =>
                            this.setState({
                                loading: false,
                                errors: reject.messages || 'Unable to save'
                            })
                        );
                } else {
                    //report errors to the user
                    this.setState({
                        loading: false,
                        errors: errors
                    });
                }
            }
        );
    };
    handleCancel = () => {
        this.hideDialog();
    };
    clearErrors = () => {
        this.setState({
            errors: null
        });
    };
    canProceed() {
        return true;
    }
    handleOrganisationDataUpdate = (nameOrObject, value) => {
        const { organisation } = this.state;

        let update;
        if (typeof nameOrObject === 'object') {
            update = nameOrObject;
        } else {
            update = {};
            update[nameOrObject] = value;
        }

        this.setState({
            organisation: organisation.merge(update)
        });
    };
    getDialogView() {
        const { labels, codedEntries, endpoints } = this.props;
        const { errors, organisation } = this.state;
        let errorsContent;
        if (null !== errors) {
            errorsContent = (
                <Errors key="errors-panel" errors={errors} onClose={this.clearErrors} target={this.getDialogTarget} />
            );
        }

        return (
            <>
                {errorsContent}
                <Form id="addOrganisation">
                    <Details
                        key="details"
                        labels={labels}
                        onUpdate={this.handleOrganisationDataUpdate}
                        codedEntries={codedEntries}
                        organisation={organisation}
                        organisationAutocompleteEndpoint={endpoints.organisationAutocompleteEndpoint}
                    />
                </Form>
            </>
        );
    }
    showDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and show
            this.setState(getDefaultState(this.props), () => {
                dialog.showDialog();
            });
        }
    }
    hideDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and hide
            dialog.hideDialog();
            this.setState(getDefaultState(this.props));
        }
    }
    getButtons() {
        return (
            <>
                <Button
                    key="primaryButton"
                    type="primary"
                    disabled={!this.canProceed()}
                    onClick={this.handleAddOrganisation}
                    title="save"
                    icon="fa-check">
                    {' '}
                    {'save'}
                </Button>
                <Button
                    key="secondaryButton"
                    type="secondary"
                    onClick={this.handleCancel}
                    title="cancel"
                    icon="fa-times">
                    {' '}
                    {'cancel'}
                </Button>
            </>
        );
    }
    fireEvent(eventType, payload) {
        //create custom event and fire
        const event = new CustomEvent(eventType, {
            bubbles: true,
            cancelable: true,
            detail: payload
        });
        ReactDOM.findDOMNode(this).dispatchEvent(event);
    }
    dialogRef = ref => (this.dialog = ref);
    getDialogTarget = () => ReactDOM.findDOMNode(this.dialog);
    render() {
        const { dialogConfig } = this.props;
        const { loading } = this.state;

        return (
            <AppDialog
                key="addDialog"
                ref={this.dialogRef}
                header={dialogConfig.header}
                narrative={dialogConfig.narrative}
                successMessage={dialogConfig.successMessage}
                buttons={this.getButtons()}
                defaultOpen={false}
                busy={loading}>
                {this.getDialogView()}
            </AppDialog>
        );
    }
}
