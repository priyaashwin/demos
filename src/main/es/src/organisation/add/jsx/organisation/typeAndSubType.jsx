'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CodedEntrySelect from '../../../../webform/uspSelectForCodedEntry';
import { isSpecified } from '../../../../utils/js/textUtils';

export default class TypeAndSubType extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        codedEntries: PropTypes.shape({
            organisationTypeCategory: PropTypes.object.isRequired,
            organisationSubTypeCategory: PropTypes.object.isRequired
        }).isRequired,
        type: PropTypes.string,
        subType: PropTypes.string,
        onUpdate: PropTypes.func.isRequired
    };

    handleUpdate = (name, value) => {
        const update = {};
        switch (name) {
            case 'type':
                update['type'] = value;
                update['subType'] = undefined;
                break;
            case 'subType':
                update['subType'] = value;
                break;
        }

        this.props.onUpdate(update);
    };
    render() {
        const { labels, codedEntries, type, subType } = this.props;
        return (
            <>
                <div className="pure-u-1-2 l-box">
                    <CodedEntrySelect
                        key="type"
                        id="addOrganisation_type"
                        label={labels.type}
                        name="type"
                        category={codedEntries.organisationTypeCategory}
                        value={type}
                        onUpdate={this.handleUpdate}
                        emptyOptionsLabel={labels.emptyType}
                        includeEmptyOption />
                </div>
                <div className="pure-u-1-2 l-box">
                    <CodedEntrySelect
                        key="subType"
                        id="addOrganisation_subType"
                        label={labels.subType}
                        name="subType"
                        category={codedEntries.organisationSubTypeCategory}
                        value={subType}
                        parentValue={type}
                        onUpdate={this.handleUpdate}
                        isChildCategory
                        disabled={!isSpecified(type)}
                        emptyOptionsLabel={labels.emptySubType}
                        includeEmptyOption />
                </div>
            </>
        );
    }
}