'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Role from './role';
import { TabView, Tab } from '../../../../../tabs/jsx/tabView';
import { Map } from 'immutable';

export default class Roles extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        professionalTeamRelationshipTypes: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                roleAName: PropTypes.string.isRequired,
                roleAClass: PropTypes.string.isRequired
            })
        ),
        codedEntries: PropTypes.shape({
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        personAutocompleteEndpoint: PropTypes.string.isRequired,
        onUpdate: PropTypes.func.isRequired,
        members: PropTypes.instanceOf(Map).isRequired
    };
    handleUpdate = (roleId, roleMembers) => {
        const { onUpdate, members } = this.props;
        onUpdate('members', members.set(roleId, roleMembers));
    };
    render() {
        const { labels, professionalTeamRelationshipTypes, personAutocompleteEndpoint, codedEntries, members } = this.props;
        return (
            <>
                <h4 className="alt2">{labels.members.title}</h4>
                <TabView key="roleTabs">
                    {professionalTeamRelationshipTypes.map(role => (
                        <Tab key={role.id} label={role.roleAName} tabId={role.roleAClass}>
                            <Role
                                key={`${role.id}_role`}
                                role={role}
                                labels={labels.members}
                                url={personAutocompleteEndpoint}
                                codedEntries={codedEntries}
                                onUpdate={this.handleUpdate}
                                members={members.get(role.id)}
                            />
                        </Tab>
                    ))}
                </TabView>
            </>
        );
    }
}