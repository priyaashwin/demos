'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { TeamForAdd } from '../../js/team';
import AppDialog from '../../../../dialog/dialog';
import Errors from '../../../../dialog/errors/errors';
import Button from '../../../../dialog/button';
import Details from './details';
import Form from '../../../../webform/uspForm';
import Model from '../../../../model/model';
import { validateTeamDataForAdd } from '../../js/validation';
import { isSpecified } from '../../../../utils/js/textUtils';
import endpoint from '../../js/cfg';

const getDefaultState = function() {
    return {
        errors: null,
        loading: false,
        //always start with a fresh team model
        team: new TeamForAdd({})
    };
};

export default class AddTeamDialog extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        dialogConfig: PropTypes.shape({
            header: PropTypes.object.isRequired,
            narrative: PropTypes.shape({
                summary: PropTypes.string.isRequired,
                description: PropTypes.string
            }).isRequired,
            successMessage: PropTypes.string,
            delayMessage: PropTypes.bool
        }).isRequired,
        codedEntries: PropTypes.shape({
            teamSectorCategory: PropTypes.object.isRequired,
            teamSubSectorCategory: PropTypes.object.isRequired,
            teamRegionCategory: PropTypes.object.isRequired,
            organisationTypeCategory: PropTypes.object.isRequired,
            organisationSubTypeCategory: PropTypes.object.isRequired,
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        endpoints: PropTypes.shape({
            newOwnedTeamEndpoint: PropTypes.string.isRequired,
            newTeamEndpoint: PropTypes.string.isRequired,
            organisationAutocompleteEndpoint: PropTypes.string.isRequired,
            securityProfileAutocompleteEndpoint: PropTypes.string.isRequired,
            professionalTeamRelationshipTypesEndpoint: PropTypes.string.isRequired,
            personAutocompleteEndpoint: PropTypes.string.isRequired
        })
    };
    state = {
        ...getDefaultState(this.props),
        //default initial state to loading
        //because dialog needs additional data
        //which is loaded on mount
        loading: true,
        //must never reset the relationship types - these are loaded on mount
        professionalTeamRelationshipTypes: []
    };

    componentDidCatch() {
        this.setState({
            errors: 'Unexpected error'
        });
    }
    componentDidMount() {
        //load data to drive the dialog such as professionalPersonTypes
        const { endpoints } = this.props;
        new Model(endpoints.professionalTeamRelationshipTypesEndpoint)
            .load(endpoint.professionalTeamRelationshipTypesEndpoint)
            .then(result =>
                this.setState({
                    loading: false,
                    errors: null,
                    professionalTeamRelationshipTypes: result.results || []
                })
            )
            .catch(reject => {
                this.setState({
                    loading: false,
                    errors: reject,
                    professionalTeamRelationshipTypes: []
                });
            });
    }
    handleAddTeam = () => {
        const { labels, endpoints, dialogConfig } = this.props;
        const { team } = this.state;

        const { delayMessage } = dialogConfig;

        this.setState(
            {
                loading: true
            },
            () => {
                const errors = validateTeamDataForAdd(team, labels.validation || {});
                if (null === errors) {
                    let url;
                    let modelType;
                    if (isSpecified(team.parentOrganisation)) {
                        url = endpoints.newOwnedTeamEndpoint;
                        modelType = 'NewChildTeamAndProfessionalRelationships';
                    } else {
                        url = endpoints.newTeamEndpoint;
                        modelType = 'NewTeamAndProfessionalRelationships';
                    }
                    const teamModel = new Model(url);
                    //actually move onto the save
                    teamModel
                        .save(endpoint.saveNewTeamEndpoint, {
                            _type: modelType,
                            ...team.getData()
                        })
                        .then(success => {
                            const dialog = this.dialog;
                            if (dialog) {
                                //fire success message - delay to next page turn
                                dialog.fireSuccessMessage(
                                    {},
                                    {
                                        delayed: delayMessage
                                    }
                                );
                                //fire saved
                                this.fireEvent('team:saved', {
                                    teamId: success.id
                                });
                                //success
                                this.hideDialog();
                            }
                        })
                        .catch(reject =>
                            this.setState({
                                loading: false,
                                errors: reject.messages || 'Unable to save'
                            })
                        );
                } else {
                    //report errors to the user
                    this.setState({
                        loading: false,
                        errors: errors
                    });
                }
            }
        );
    };
    handleCancel = () => {
        this.hideDialog();
    };
    clearErrors = () => {
        this.setState({
            errors: null
        });
    };
    canProceed() {
        return true;
    }
    handleTeamDataUpdate = (nameOrObject, value) => {
        const { team } = this.state;

        let update;
        if (typeof nameOrObject === 'object') {
            update = nameOrObject;
        } else {
            update = {};
            update[nameOrObject] = value;
        }

        this.setState({
            team: team.merge(update)
        });
    };
    getDialogView() {
        const { labels, codedEntries, endpoints } = this.props;
        const { errors, team, professionalTeamRelationshipTypes } = this.state;
        let errorsContent;
        if (null !== errors) {
            errorsContent = (
                <Errors key="errors-panel" errors={errors} onClose={this.clearErrors} target={this.getDialogTarget} />
            );
        }

        return (
            <>
                {errorsContent}
                <Form id="addTeam">
                    <Details
                        key="details"
                        labels={labels}
                        onUpdate={this.handleTeamDataUpdate}
                        codedEntries={codedEntries}
                        team={team}
                        professionalTeamRelationshipTypes={professionalTeamRelationshipTypes}
                        organisationAutocompleteEndpoint={endpoints.organisationAutocompleteEndpoint}
                        securityProfileAutocompleteEndpoint={endpoints.securityProfileAutocompleteEndpoint}
                        personAutocompleteEndpoint={endpoints.personAutocompleteEndpoint}
                    />
                </Form>
            </>
        );
    }
    showDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and show
            this.setState(getDefaultState(this.props), () => {
                dialog.showDialog();
            });
        }
    }
    hideDialog() {
        const dialog = this.dialog;

        if (dialog) {
            //reset to default state and hide
            dialog.hideDialog();
            this.setState(getDefaultState(this.props));
        }
    }
    getButtons() {
        return (
            <>
                <Button
                    key="primaryButton"
                    type="primary"
                    disabled={!this.canProceed()}
                    onClick={this.handleAddTeam}
                    title="save"
                    icon="fa-check">
                    {' '}
                    {'save'}
                </Button>
                <Button
                    key="secondaryButton"
                    type="secondary"
                    onClick={this.handleCancel}
                    title="cancel"
                    icon="fa-times">
                    {' '}
                    {'cancel'}
                </Button>
            </>
        );
    }
    fireEvent(eventType, payload) {
        //create custom event and fire
        const event = new CustomEvent(eventType, {
            bubbles: true,
            cancelable: true,
            detail: payload
        });
        ReactDOM.findDOMNode(this).dispatchEvent(event);
    }
    dialogRef = ref => (this.dialog = ref);
    getDialogTarget = () => ReactDOM.findDOMNode(this.dialog);
    render() {
        const { dialogConfig } = this.props;
        const { loading } = this.state;

        return (
            <AppDialog
                key="addDialog"
                ref={this.dialogRef}
                header={dialogConfig.header}
                narrative={dialogConfig.narrative}
                successMessage={dialogConfig.successMessage}
                buttons={this.getButtons()}
                defaultOpen={false}
                busy={loading}>
                {this.getDialogView()}
            </AppDialog>
        );
    }
}
