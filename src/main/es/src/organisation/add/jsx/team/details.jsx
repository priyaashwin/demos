'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TeamForAdd } from '../../js/team';
import Input from '../../../../webform/uspInput';
import Textarea from '../../../../webform/uspTextarea';
import SectorAndSubSector from './sectorAndSubSector';
import EffectiveDate from '../effectiveDate';
import ParentOrganisationSelector from '../parentOrganisationSelector';
import SecurityProfileSelector from './securityProfileSelector';
import CodedEntrySelect from '../../../../webform/uspSelectForCodedEntry';
import Roles from './roles/roles';
import Label from '../../../../webform/uspLabel';
import DateInput from '../../../../webform/uspDateInput';

const maxDate = moment();

export default class Details extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        codedEntries: PropTypes.shape({
            teamSectorCategory: PropTypes.object.isRequired,
            teamSubSectorCategory: PropTypes.object.isRequired,
            teamRegionCategory: PropTypes.object.isRequired,
            organisationTypeCategory: PropTypes.object.isRequired,
            organisationSubTypeCategory: PropTypes.object.isRequired,
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        team: PropTypes.oneOfType([
            PropTypes.instanceOf(TeamForAdd)
        ]).isRequired,
        professionalTeamRelationshipTypes: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.number.isRequired,
                roleAName: PropTypes.string.isRequired,
                roleAClass: PropTypes.string.isRequired
            })
        ),
        onUpdate: PropTypes.func.isRequired,
        organisationAutocompleteEndpoint: PropTypes.string.isRequired,
        securityProfileAutocompleteEndpoint: PropTypes.string.isRequired,
        personAutocompleteEndpoint: PropTypes.string.isRequired
    };

    handleUpdate = (name, value) => {
        this.props.onUpdate(name, value);
    };

    renderSectorAndSubSector() {
        const { labels, team, codedEntries } = this.props;
        const { sector, sectorSubType } = team;
        return (
            <SectorAndSubSector
                key="sectorAndSubSector"
                labels={labels}
                onUpdate={this.handleUpdate}
                sector={sector}
                sectorSubType={sectorSubType}
                codedEntries={codedEntries}
            />
        );
    }
    render() {
        const { labels, team, codedEntries,
            organisationAutocompleteEndpoint,
            securityProfileAutocompleteEndpoint, professionalTeamRelationshipTypes, personAutocompleteEndpoint } = this.props;
        const { name, description, effectiveDate, startDate, parentOrganisation, securityProfile, region, members } = team;

        return (
            <div className="pure-g-r">
                <div className="pure-u-1 l-box">
                    <Input
                        key="name"
                        id="addTeam_name"
                        name="name"
                        label={labels.name}
                        mandatory={true}
                        maxLength={100}
                        value={name}
                        onUpdate={this.handleUpdate} />
                </div>
                <div className="pure-u-1 l-box">
                    <Textarea
                        key="description"
                        id="addTeam_description"
                        name="description"
                        label={labels.description}
                        value={description}
                        onUpdate={this.handleUpdate} />
                </div>
                <div className="pure-u-1-2 l-box">
                    <ParentOrganisationSelector
                        key="parentOrganisation"
                        labels={labels}
                        name="parentOrganisationId"
                        onUpdate={this.handleUpdate}
                        codedEntries={codedEntries}
                        url={organisationAutocompleteEndpoint}
                        parentOrganisation={parentOrganisation}
                    />
                </div>
                <div className="pure-u-1-2 l-box">
                    <EffectiveDate
                        key="effectiveDate"
                        labels={labels}
                        effectiveDate={effectiveDate}
                        onUpdate={this.handleUpdate}
                    />
                </div>
                <div className="pure-u-1-2 l-box">
                    <Label forId="startDate"
                        label={labels.startDate} />
                    <DateInput
                        id="startDate"
                        key="startDate"
                        name="startDate"
                        onUpdate={this.handleUpdate}
                        maximumDate={maxDate.valueOf()}
                        value={startDate}
                        placeholder="DD-MMM-YYYY" />
                </div>
                <div className="pure-u-1-2 l-box">
                    <SecurityProfileSelector
                        key="securityRecordTypeConfiguration"
                        labels={labels}
                        name="securityRecordTypeConfigurationId"
                        onUpdate={this.handleUpdate}
                        url={securityProfileAutocompleteEndpoint}
                        securityProfile={securityProfile}
                    />
                </div>
                <div className="pure-u-1-2 l-box">
                    <CodedEntrySelect
                        key="region"
                        id="addTeam_region"
                        label={labels.region}
                        name="region"
                        category={codedEntries.teamRegionCategory}
                        value={region}
                        onUpdate={this.handleUpdate}
                        includeEmptyOption />
                </div>
                {this.renderSectorAndSubSector()}

                <div className="pure-u-1 l-box members-list">
                    <Roles
                        key="roles"
                        labels={labels}
                        professionalTeamRelationshipTypes={professionalTeamRelationshipTypes}
                        onUpdate={this.handleUpdate}
                        personAutocompleteEndpoint={personAutocompleteEndpoint}
                        codedEntries={codedEntries}
                        members={members}
                    />
                </div>
            </div>
        );
    }
}