'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import memoize from 'memoize-one';
import endpoint from '../../../js/cfg';
import ProfessionalPersonAutocomplete from '../../../../../autocomplete/jsx/professionalRolePersonAutoComplete';


export default class Role extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        onUpdate: PropTypes.func.isRequired,
        codedEntries: PropTypes.shape({
            professionalTitleCategory: PropTypes.object.isRequired
        }).isRequired,
        role: PropTypes.object.isRequired,
        url: PropTypes.string.isRequired,
        members:PropTypes.array
    };
    getEndpoint = memoize((endpoint, url) => ({ ...endpoint, url: url }));    
    handleSelection=(selectedValue)=>{
        const {onUpdate, role}=this.props;
        onUpdate(role.id, selectedValue);
    };    
    render() {
        const { labels, codedEntries, url, onUpdate:onUpdateIgnored, members, role:roleIgnored, ...other} = this.props;
        return (
           <div>
                <ProfessionalPersonAutocomplete
                {...other}
                placeholder={labels.placeholder}
                codedEntries={codedEntries}
                onSelection={this.handleSelection}
                endpoint={this.getEndpoint(endpoint.professionalPersonAutocompleteCompleteEndpoint, url)}
                queryParameter="nameOrUsername"
                selectedValue={members}
                allowMultiple={true}
            />
            </div>
        );
    }
}