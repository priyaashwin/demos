'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import CodedEntrySelect from '../../../../webform/uspSelectForCodedEntry';
import { isSpecified } from '../../../../utils/js/textUtils';

export default class SectorAndSubSector extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        codedEntries: PropTypes.shape({
            teamSectorCategory: PropTypes.object.isRequired,
            teamSubSectorCategory: PropTypes.object.isRequired
        }).isRequired,
        sector: PropTypes.string,
        sectorSubType: PropTypes.string,
        onUpdate: PropTypes.func.isRequired
    };

    handleUpdate = (name, value) => {
        const update = {};
        switch (name) {
            case 'sector':
                update['sector'] = value;
                update['sectorSubType'] = undefined;
                break;
            case 'sectorSubType':
                update['sectorSubType'] = value;
                break;
        }

        this.props.onUpdate(update);
    };
    render() {
        const { labels, codedEntries, sector, sectorSubType } = this.props;
        return (
            <>
                <div className="pure-u-1-2 l-box">
                    <CodedEntrySelect
                        key="sector"
                        id="addTeam_sector"
                        label={labels.sector}
                        name="sector"
                        category={codedEntries.teamSectorCategory}
                        value={sector}
                        onUpdate={this.handleUpdate}
                        emptyOptionsLabel={labels.emptySector}
                        includeEmptyOption />
                </div>
                <div className="pure-u-1-2 l-box">
                    <CodedEntrySelect
                        key="sectorSubType"
                        id="addTeam_sectorSubType"
                        label={labels.subSector}
                        name="sectorSubType"
                        category={codedEntries.teamSubSectorCategory}
                        value={sectorSubType}
                        parentValue={sector}
                        onUpdate={this.handleUpdate}
                        isChildCategory
                        disabled={!isSpecified(sector)}
                        emptyOptionsLabel={labels.emptySubSector}
                        includeEmptyOption />
                </div>
            </>
        );
    }
}