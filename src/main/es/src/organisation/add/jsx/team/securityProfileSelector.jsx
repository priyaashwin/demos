'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import SecurityProfileAutoComplete from '../../../../autocomplete/jsx/securityProfileAutoComplete';
import endpoint from '../../js/cfg';
import memoize from 'memoize-one';
import { SecurityRecordTypeConfiguration } from '../../js/team';
import Label from '../../../../webform/uspLabel';

export default class SecurityProfileSelector extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        name: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        onUpdate: PropTypes.func.isRequired,
        securityProfile:PropTypes.object
    };

    handleSelection = (selectedValue) => {
        const { onUpdate } = this.props;
        //update securityProfile object - not ID - convert to  record
        onUpdate('securityProfile', selectedValue ? new SecurityRecordTypeConfiguration(selectedValue) : undefined);
    };
    getEndpoint = memoize((endpoint, url) => ({ ...endpoint, url: url }));
    render() {
        const { url, labels, onUpdate: onUpdateIgnored, securityProfile, ...other } = this.props;

        return (<div>
            <Label forId="securityRecordTypeConfigurationId"
                label={labels.securityProfile.securityRecordTypeConfigurationId}
                mandatory
            />
            <SecurityProfileAutoComplete
                {...other}    
                inputId="securityRecordTypeConfigurationId"
                placeholder={labels.securityProfile.placeholder}
                onSelection={this.handleSelection}
                endpoint={this.getEndpoint(endpoint.securityProfileAutoCompleteEndpoint, url)}
                selectedValue={securityProfile}
            />
        </div>);
    }
}