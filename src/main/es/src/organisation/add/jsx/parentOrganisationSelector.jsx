'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import OrganisationAutocomplete from '../../../autocomplete/jsx/organisationAutoComplete';
import endpoint from '../js/cfg';
import memoize from 'memoize-one';
import { Organisation } from '../js/organisation';
import Label from '../../../webform/uspLabel';

export default class ParentOrganisationSelector extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        name: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        codedEntries: PropTypes.shape({
            organisationTypeCategory: PropTypes.object.isRequired,
            organisationSubTypeCategory: PropTypes.object.isRequired
        }),
        onUpdate: PropTypes.func.isRequired,
        parentOrganisation: PropTypes.instanceOf(Organisation)
    };

    handleSelection = (selectedValue) => {
        const { onUpdate } = this.props;
        //update parentOrganisation object - not ID - convert to Organisation record
        onUpdate('parentOrganisation', selectedValue ? new Organisation(selectedValue) : undefined);
    };
    getEndpoint = memoize((endpoint, url) => ({ ...endpoint, url: url }));
    render() {
        const { url, labels, codedEntries, onUpdate: onUpdateIgnored, parentOrganisation, ...other } = this.props;

        return (<div>
            <Label forId="parentOrganisationId"
                label={labels.parentOrganisation.parentOrganisationId}
            />
            <OrganisationAutocomplete
                {...other}
                inputId="parentOrganisationId"
                placeholder={labels.parentOrganisation.placeholder}
                codedEntries={codedEntries}
                onSelection={this.handleSelection}
                endpoint={this.getEndpoint(endpoint.parentOrganisationAutoCompleteEndpoint, url)}
                queryParameter="nameOrOrgIdOrPrevName"
                selectedValue={parentOrganisation}
            />
        </div>);
    }
}