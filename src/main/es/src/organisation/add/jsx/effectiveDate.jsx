'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DateInput from '../../../webform/uspDateInput';
import { parseDate, formatDate, DATE_STRING_REG } from '../../../date/date';
import Message from '../../../message/jsx/message';
import Label from '../../../webform/uspLabel';

const maxDate = moment();

export default class EffectiveDate extends PureComponent {
    static propTypes = {
        labels: PropTypes.object.isRequired,
        onUpdate: PropTypes.func.isRequired,
        effectiveDate: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number])
    }
    state = {
        valid: true
    }
    handleUpdate = (name, value) => {
        const { onUpdate } = this.props;
        let updateValue = (value || '').trim();
        let valid = true;
        if (updateValue && DATE_STRING_REG.test(updateValue)) {
            const d = parseDate(updateValue);
            if (null !== d) {
                const days = d.diff(maxDate, 'days');
                //validate
                if (days > 0) {
                    valid = false;
                }
                //re-format
                updateValue = formatDate(d, true);
            }
        }
        this.setState({
            valid: valid
        });

        onUpdate(name, updateValue);
    }
    renderValidationMessage() {
        const { valid } = this.state;
        const { labels } = this.props;
        if (valid) {
            //no message to render
            return false;
        }
        const messageContent = (
            <div className="pure-alert-small">
                <span className="info-icon"><i className="fa fa-info-circle" aria-hidden={true} /></span>
                <ul>
                    <li>
                        <span>{labels.effectiveDateInvalid}</span>
                    </li>
                </ul>
            </div>
        );
        return (
            <Message key="message">
                {messageContent}
            </Message>
        );
    }
    render() {
        const { effectiveDate, labels } = this.props;
        return (
            <>
                <Label forId="effectiveDate"
                    label={labels.effectiveDate} />
                <DateInput
                    id="effectiveDate"
                    key="effectiveDate"
                    name="effectiveDate"
                    onUpdate={this.handleUpdate}
                    maximumDate={maxDate.valueOf()}
                    value={effectiveDate}
                    placeholder="DD-MMM-YYYY"
                />
                {this.renderValidationMessage()}
            </>
        );
    }
}

