'use strict';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class Organisation extends PureComponent {
    static propTypes = {
        selected: PropTypes.bool,
        organisation: PropTypes.object,
        onView: PropTypes.func.isRequired
    };

    static defaultProps = {
        selected: false
    };

    state = {
        over: false
    };

    getIcon(organisation) {
        let icon;
        switch (organisation.organisationType) {
            case 'TEAM':
                icon = 'eclipse-team';
                break;
            case null:
                icon = 'fa-building';
                break;
            default:
                icon = 'fa-question';
        }
        return (<span className="org-type"><i className={classnames('fa', icon)} aria-hidden="true" /></span>);
    }
    handleSelected = () => {
        this.setState({
            over: true
        });
    };
    handleUnselected = () => {
        this.setState({
            over: false
        });
    };
    handleClick = (e) => {
        const { organisation, onView } = this.props;
        e.preventDefault();
        //don't allow the tree to process this event
        e.stopPropagation();

        onView(organisation.id);
    };
    render() {
        const { selected, organisation } = this.props;
        const { over } = this.state;
        let actions;
        if (over && !selected) {
            //reveal icons on mouse over and if not selected
            actions = (
                <span className="tree-actions" onClick={this.handleClick} tabIndex={0} title="Click to view">
                    <a onClick={this.handleClick}>
                        <span className="action-icon"><i className="fa fa-eye" aria-hidden="true" /></span>
                        <span className="action-label">{' view'}</span>
                    </a>
                </span>
            );
        }
        return (
            <span tabIndex={0} key={organisation.id} className={classnames('org-node',
                {
                    'selected-node': selected,
                    'over': over
                }
            )}
                onMouseLeave={this.handleUnselected}
                onMouseEnter={this.handleSelected}
                onFocus={this.handleSelected}
                onBlur={this.handleUnselected}>
                {this.getIcon(organisation)}
                <span>{organisation.name}</span>
                <span>{` (${organisation.organisationIdentifier})`}</span>
                {actions}
            </span>
        );
    }
}