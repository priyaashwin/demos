'use strict';
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import TreeView from 'usp-simple-tree';
import Model from '../../../../model/model';
import endpoint from '../../js/cfg';
import { isSpecified } from '../../../../utils/js/textUtils';
import Accordion from 'usp-accordion-panel';
import ErrorMessage from '../../../../error/error';
import computeScrollIntoView from 'compute-scroll-into-view';
import { tween, styler } from 'popmotion';
import Organisation from './organisation';

const LOADING = (<span key="loading"><i className="fa fa-spinner fa-spin" aria-hidden="true" /> loading...</span>);

export default class OrganisationTree extends PureComponent {
    static propTypes = {
        endpoints: PropTypes.shape({
            organisationEndpoint: PropTypes.string.isRequired
        }).isRequired,
        modelData: PropTypes.object,
        labels: PropTypes.object.isRequired
    };

    state = {
        loading: true,
        errors: null,
        root: null
    };
    scrollIfNecessary() {
        const container = this.scrollContainer;
        if (container) {
            const selected = container.querySelector('.selected-node');
            if (selected) {
                window.setTimeout(() => {
                    const actions = computeScrollIntoView(selected, {
                        behavior: 'smooth',
                        block: 'end',
                        boundary: container
                    });
                    //perform the scroll action
                    actions.forEach(({ el, top }) => {
                        tween({
                            from: el.scrollTop,
                            to: top
                        }).start(styler(el).set('scrollTop'));
                    });
                }, 250);
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (!(prevProps.modelData === this.props.modelData)) {
            this.loadData();
        }
        if (prevState.root !== this.state.root) {
            this.scrollIfNecessary();
        }
    }
    componentDidMount() {
        //load tree data
        this.loadData();
    }
    viewOrganisation=(id)=>{
        //fire view event
        this.fireEvent('organisation:view', {
            organisationId: id
        });
    };
    fireEvent(eventType, payload) {
        //create custom event and fire
        const event = new CustomEvent(eventType, {
            bubbles: true,
            cancelable: true,
            detail: payload
        });
        ReactDOM.findDOMNode(this).dispatchEvent(event);
    }
    loadData() {
        const { modelData, endpoints } = this.props;
        if (isSpecified(modelData.rootOrganisation) && isSpecified(modelData.rootOrganisation.id)) {
            this.setState({
                loading: true
            }, () => {
                new Model(endpoints.organisationEndpoint).load(endpoint.organisationTreeEndpoint, {
                    id: modelData.rootOrganisation.id
                }).then(result => this.setState({
                    loading: false,
                    errors: null,
                    root: result
                })).catch(reject => {
                    this.setState({
                        loading: false,
                        errors: reject,
                        root: null
                    });
                });
            });
        }
    }

    getChildNodes(organisation, selectedId) {
        if (organisation.currentChildren) {
            return organisation.currentChildren.map(childOrganisation => (
                <TreeView
                    key={childOrganisation.id}
                    open={true}
                    label={this.getOrganistion(childOrganisation, selectedId)}>
                    {this.getChildNodes(childOrganisation, selectedId)}
                </TreeView>
            ));
        }
        return false;
    }

    getOrganistion(organisation, selectedId) {
        return (
            <Organisation
                key={organisation.id}
                organisation={organisation}
                selected={selectedId === organisation.id}
                onView={this.viewOrganisation}
            />);
    }
    getTree(root) {
        const { modelData } = this.props;
        const selectedId = modelData.id;
        return (<TreeView
            key="root"
            open={true}
            label={this.getOrganistion(root, selectedId)}>
            {this.getChildNodes(root, selectedId)}
        </TreeView>);
    }

    clearErrors = () => {
        this.setState({
            errors: null
        });
    };
    scrollContainerRef = (ref) => this.scrollContainer = ref;
    render() {
        const { loading, errors, root } = this.state;
        const { labels } = this.props;

        let content;

        if (loading) {
            content = (<div>
                {LOADING}
            </div>);
        } else if (errors) {
            content = (<ErrorMessage errors={errors} onClose={this.clearErrors} />);
        } else {
            if (root) {
                content = this.getTree(root);
            }
        }
        return (
            <Accordion expanded={true} title={labels.accordionTitle}>
                <div className="organisation-tree" ref={this.scrollContainerRef}>
                    {content}
                </div>
            </Accordion>
        );
    }
}
