'use strict';

class Endpoint {
    constructor() {
        this.organisationTreeEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.organisation.OrganisationWithChildren+json;charset=UTF-8'
            },            
            responseType: 'json',
            responseStatus: 200
        };
    }
}

export default new Endpoint();