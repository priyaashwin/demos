'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import Table from '../../../table/jsx/table';

export default function AttachmentsResults({ data, loading, columns }) {
    return <Table key="results-table" data={data} loading={loading} columns={columns} />;
}

AttachmentsResults.propTypes = {
    data: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    columns: PropTypes.array.isRequired
};
