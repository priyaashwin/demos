'use strict';

import memoize from 'memoize-one';
import React from 'react';
import { formatCodedEntry } from '../../../codedEntry/codedEntry';
import { date } from '../../../table/js/formatter';
import TableActions from '../../../table/jsx/actions/actions';

/**
 * @param {string} label used for the dropdown
 * @param {bool} canView permission to view this record type
 */
const getActions = (labels, canView) => {
    const { downloadAttachment } = labels;

    return [
        {
            id: 'download',
            label: downloadAttachment,
            // Is the user prevented from downloading this attachment
            disabled: !canView
        }
    ];
};

const formatters = {
    getSubjectTypeFormatter: () => row => {
        const userIcon = <i className="fa fa-user secondary-icon" />;
        const groupIcon = <i className="fa fa-users secondary-icon" />;

        return row === 'PERSON' ? userIcon : groupIcon;
    },
    getTitleFormatter: () => title => {
        return title || 'Untitled attachment';
    },
    getCodedEntryFormatter: codedEntry => row => {
        return formatCodedEntry(codedEntry, row);
    },
    getActionsFormatter: (labels, canView, handleClick) => (column, data) => {
        return (
            <TableActions key={data.id} actions={getActions(labels, canView)} handleClick={handleClick} record={data} />
        );
    }
};

const getCaseNoteAttachmentsColumns = memoize((labels, codedEntries, canView, handleClick) => {
    const { getSubjectTypeFormatter, getTitleFormatter, getCodedEntryFormatter, getActionsFormatter } = formatters;
    const { entryTypes, entrySubTypes, source, sourceOrganisation } = codedEntries;
    
    return [
    {
        key: 'subjectType',
        label: labels.subjectType,
        width: '2.5%',
        formatter: getSubjectTypeFormatter()
    },
    {
        key: 'lastUploaded',
        label: labels.dateAdded,
        width: '10%',
        formatter: date
    },
    {
        key: 'title',
        label: labels.documentName,
        width: '12.5%',
        formatter: getTitleFormatter()
    },
    {
        key: 'description',
        label: labels.description,
        width: '17.5%'
    },
    {
        key: 'entryType',
        label: labels.entryType,
        width: '12.5%',
        formatter: getCodedEntryFormatter(entryTypes)
    },
    {
        key: 'entrySubType',
        label: labels.entrySubtype,
        width: '12.5%',
        formatter: getCodedEntryFormatter(entrySubTypes)
    },
    {
        key: 'source',
        label: labels.source,
        width: '12.5%',
        formatter: getCodedEntryFormatter(source)
    },
    {
        key: 'sourceOrganisation',
        label: labels.sourceOrg,
        width: '12.5%',
        formatter: getCodedEntryFormatter(sourceOrganisation)
    },
    {
        key: 'actions',
        label: labels.actionButtons,
        width: '7.5%',
        formatter: getActionsFormatter(labels, canView, handleClick)
    }
];
});

const getFormAttachmentsColumns = memoize((labels, codedEntries, canView, handleClick) => {
    const { getSubjectTypeFormatter, getTitleFormatter, getCodedEntryFormatter, getActionsFormatter } = formatters;
    const { source, sourceOrganisation } = codedEntries;
    
    return [
    {
        key: 'subjectType',
        label: labels.subjectType,
        width: '2.5%',
        formatter: getSubjectTypeFormatter()
    },
    {
        key: 'lastUploaded',
        label: labels.dateAdded,
        width: '10%',
        formatter: date
    },
    {
        key: 'title',
        label: labels.documentName,
        width: '15%',
        formatter: getTitleFormatter()
    },
    {
        key: 'description',
        label: labels.description,
        width: '20%'
    },
    {
        key: 'formName',
        label: labels.formName,
        width: '15%'
    },
    {
        key: 'source',
        label: labels.source,
        width: '15%',
        formatter: getCodedEntryFormatter(source)
    },
    {
        key: 'sourceOrganisation',
        label: labels.sourceOrg,
        width: '15%',
        formatter: getCodedEntryFormatter(sourceOrganisation)
    },
    {
        key: 'actions',
        label: labels.actionButtons,
        width: '7.5%',
        formatter: getActionsFormatter(labels, canView, handleClick)
    }
];
});

export { getCaseNoteAttachmentsColumns, getFormAttachmentsColumns };

