'use strict';

import PropTypes from 'prop-types';
import React, { useCallback, useMemo } from 'react';
import ErrorMessage from '../../../error/error';
import useFilteredResults from '../../../filter/useFilteredResults/useFilteredResults';
import Loading from '../../../loading/loading';
import ModelList from '../../../model/modelList';
import Paginator from '../../../paginator/jsx/view';
import PaginatedResultsList from '../../../results/paginatedResultsList';
import AttachmentsResults from './attachmentsResults';

export default function AttachmentsView({
    downloadAttachmentURL,
    columnLabels: labels,
    canView,
    codedEntries,
    getColumns,
    attachmentsURL,
    subjectId,
    endpoint
}) {
    const getPaginatedResults = useCallback(
        ({ documentName, uploadDateFrom, uploadDateTo }, rowsPerPage, currentPage) => {
            const documentNameFilterValue = documentName ? documentName.getValue() : '';
            const uploadDateFromFilterValue = uploadDateFrom ? uploadDateFrom.getValue() : '';
            const uploadDateToFilterValue = uploadDateTo ? uploadDateTo.getValue() : '';

            return new PaginatedResultsList().load(
                {
                    ...endpoint,
                    url: new ModelList(attachmentsURL).getURL({
                        subjectId,
                        attachmentTitle: documentNameFilterValue,
                        startDate: uploadDateFromFilterValue,
                        endDate: uploadDateToFilterValue
                    })
                },
                rowsPerPage,
                currentPage
            );
        },
        [subjectId, attachmentsURL, endpoint]
    );

    const {
        setRowsPerPage,
        rowsPerPage,
        setCurrentPage,
        currentPage,
        clearErrors,
        loading,
        errors,
        totalResultsCount,
        data
    } = useFilteredResults(getPaginatedResults);

    const onChangeRowsPerPage = (newRowsPerPage, newCurrentPage) => {
        setRowsPerPage(newRowsPerPage);
        setCurrentPage(newCurrentPage);
    };

    const onChangePage = newCurrentPage => {
        setCurrentPage(newCurrentPage);
    };

    const columns = useMemo(() => {
        const handleClick = (action, data) => {
            // formInstanceId only present where row relates to a form
            const { id, formInstanceId } = data;
    
            switch (action) {
                case 'download':
                    window.location = new ModelList(downloadAttachmentURL).getURL({
                        attachmentItemId: id,
                        formInstanceId
                    });
            }
        };

        return getColumns(labels, codedEntries, canView, handleClick);
    },
    [labels, codedEntries, canView, downloadAttachmentURL, getColumns]);

    let content;

    if (loading) {
        content = <Loading />;
    } else if (errors) {
        content = <ErrorMessage errors={errors} onClose={clearErrors} />;
    } else {
        content = (
            <>
                <AttachmentsResults data={data} loading={loading} columns={columns} />
                <Paginator
                    onChangeRowsPerPage={onChangeRowsPerPage}
                    onChangePage={onChangePage}
                    rowsPerPage={rowsPerPage}
                    currentPage={currentPage}
                    totalResultsCount={totalResultsCount}
                />
            </>
        );
    }

    return <div className="document-widget-page">{content}</div>;
}

AttachmentsView.propTypes = {
    attachmentsURL: PropTypes.string.isRequired,
    downloadAttachmentURL: PropTypes.string.isRequired,
    subjectId: PropTypes.number.isRequired,
    columnLabels: PropTypes.object.isRequired,
    canView: PropTypes.bool.isRequired,
    codedEntries: PropTypes.object.isRequired,
    getColumns: PropTypes.func.isRequired,
    endpoint: PropTypes.object.isRequired
};
