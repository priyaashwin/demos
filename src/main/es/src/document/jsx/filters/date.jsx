'use strict';

import PropTypes from 'prop-types';
import React, { useContext, useMemo } from 'react';
import { filterContext } from '../../../filter/filterContextProvider';
import Message from '../../../message/jsx/message';
import DateInput from '../../../webform/uspDateInput';
import Label from '../../../webform/uspLabel';

const CALENDAR_STYLE = {
    margin: 'auto',
    position: 'absolute'
};

const dateUploadedFromName = 'uploadDateFrom';
const dateUploadedToName = 'uploadDateTo';
const maxDate = moment();

export default function DateFilter({ labels }) {
    const { setFilterValue, resetFilters, getFilters, getErrors } = useContext(filterContext);

    const filters = getFilters();
    const errors = getErrors();
    const { uploadDateFrom: dateUploadedFromFilter, uploadDateTo: dateUploadedToFilter } = filters;

    const onInputFieldChanged = (name, value) => {
        setFilterValue(name, value);
    };

    const onResetButtonClicked = () => {
        resetFilters(dateUploadedFromName, dateUploadedToName);
    };

    const validationMessages = useMemo(() => {
        const renderValidationMessage = (name, error) => {
            if (!error) {
                //no message to render
                return false;
            }
            const messageContent = (
                <div className="pure-alert-small">
                    <span className="info-icon">
                        <i className="fa fa-info-circle" aria-hidden={true} />
                    </span>
                    <ul>
                        <li>
                            <span>{error}</span>
                        </li>
                    </ul>
                </div>
            );
            return <Message key={`${name}Message`}>{messageContent}</Message>;
        };

        const errorMessages = Object.entries(errors).map(([error, value]) => {
            if (value) {
                return renderValidationMessage(error, labels[value]);
            }
        });

        return <>{errorMessages}</>;
    }, [errors, labels]);

    return (
        <div className="dateFilter">
            {validationMessages}
            <div className="pure-g-r">
                <div className="pure-u-1-3 l-box">
                    <Label forId="uploadDateFrom" label={labels.uploadDateFrom} />
                    <DateInput
                        id={dateUploadedFromName}
                        key={dateUploadedFromName}
                        name={dateUploadedFromName}
                        onUpdate={onInputFieldChanged}
                        maximumDate={maxDate.valueOf()}
                        value={dateUploadedFromFilter.toString()}
                        placeholder="DD-MMM-YYYY"
                        customCalendarStyle={CALENDAR_STYLE}
                    />
                    <Label forId="uploadDateTo" label={labels.uploadDateTo} />
                    <DateInput
                        id={dateUploadedToName}
                        key={dateUploadedToName}
                        name={dateUploadedToName}
                        onUpdate={onInputFieldChanged}
                        maximumDate={maxDate.valueOf()}
                        value={dateUploadedToFilter.toString()}
                        placeholder="DD-MMM-YYYY"
                        customCalendarStyle={CALENDAR_STYLE}
                    />
                </div>
                <div className="pure-u-1 l-box">
                    <div className="pure-button-secondary pull-right filter-reset-button">
                        <input
                            type="button"
                            data-reset="reset"
                            className="pure-button pure-button-secondary"
                            value="Reset"
                            aria-label="Reset state filters"
                            onClick={onResetButtonClicked}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

DateFilter.propTypes = {
    labels: PropTypes.shape({
        uploadDateFrom: PropTypes.string.isRequired,
        uploadDateTo: PropTypes.string.isRequired
    })
};
