'use strict';

import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import { filterContext } from '../../../filter/filterContextProvider';
import Input from '../../../webform/raw/input';
import Label from '../../../webform/uspLabel';

const filterName = 'documentName';
export default function NameFilter({ labels }) {
    const { setFilterValue, resetFilters, getFilters } = useContext(filterContext);

    const { documentName: documentNameFilter } = getFilters();

    const onInputFieldChanged = (name, value) => {
        const newValue = value || '';

        setFilterValue(name, newValue);
    };

    const onResetButtonClicked = () => {
        resetFilters(filterName);
    };

    const renderInputField = () => {
        return (
            <div className="pure-u-1-3 l-box">
                <Label forId="documentName" label={labels.documentName} />
                <Input
                    key={filterName}
                    id={filterName}
                    name={filterName}
                    label={labels.nameFilter}
                    value={documentNameFilter.toString()}
                    maxLength={100}
                    onUpdate={onInputFieldChanged}
                />
            </div>
        );
    };
    return (
        <div className="nameFilter">
            <div className="pure-g-r">
                {renderInputField()}
                <div className="pure-u-1 l-box">
                    <div className="pure-button-secondary pull-right filter-reset-button">
                        <input
                            type="button"
                            data-reset="reset"
                            className="pure-button pure-button-secondary"
                            value="Reset"
                            aria-label="Reset state filters"
                            onClick={onResetButtonClicked}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

NameFilter.propTypes = {
    labels: PropTypes.shape({ nameFilter: PropTypes.string.isRequired, documentName: PropTypes.string.isRequired })
};
