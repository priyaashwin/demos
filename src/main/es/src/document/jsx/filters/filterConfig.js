'use strict';

import { getFilterRecord, VALUE_TYPES } from '../../../filter/filterFactory/filterFactory';
import { DATE_STRING_REG } from '../../../date/date';

const { DATE_FROM, DATE_TO, TEXT } = VALUE_TYPES;

const UPLOAD_DATE_FROM_INVALID_FORMAT = 'uploadDateFromInvalidFormat';
const UPLOAD_DATE_TO_INVALID_FORMAT = 'uploadDateToInvalidFormat';
const UPLOAD_DATE_TO_CONFLICTING_DATES = 'uploadDateToDateConflict';

const dateValidator = (filters) => {
    const dateToString = filters.uploadDateTo.toString();
    const dateFromString = filters.uploadDateFrom.toString();

    const dateFromValue = filters.uploadDateFrom.getValue();
    const dateToValue = filters.uploadDateTo.getValue();

    const errors = { 
        uploadDateFrom: false, 
        uploadDateTo: false 
    };

    if (dateFromString && !DATE_STRING_REG.test(dateFromString)) {
        errors.uploadDateFrom = UPLOAD_DATE_FROM_INVALID_FORMAT;
    }

    if (dateToString && !DATE_STRING_REG.test(dateToString)) {
        errors.uploadDateTo = UPLOAD_DATE_TO_INVALID_FORMAT;
    }

    if ((Number.isInteger(dateFromValue) && Number.isInteger(dateToValue)) && dateToValue < dateFromValue) {
        errors.uploadDateTo = UPLOAD_DATE_TO_CONFLICTING_DATES;
    }

    return errors;
};

export const filterConfig = [{
        filterName: 'documentName',
        filter: getFilterRecord({ valueType: TEXT, initValue: '' })
    },
    {
        filterName: 'uploadDateFrom',
        filter: getFilterRecord({ valueType: DATE_FROM, initValue: null, validator: dateValidator })
    },
    {
        filterName: 'uploadDateTo',
        filter: getFilterRecord({ valueType: DATE_TO, initValue: null, validator: dateValidator })
    }
];