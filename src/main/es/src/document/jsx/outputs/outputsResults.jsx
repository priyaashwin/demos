'use strict';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '../../../table/jsx/table';
import TableActions from '../../../table/jsx/actions/actions';
import memoize from 'memoize-one';
import { date } from '../../../table/js/formatter';
import { getEnumAttribute } from '../../../utils/js/enumUtils';

export default class GeneratedDocumentsResults extends Component {

    getColumns = memoize((labels, permissions, enumTypes) => [
        {
            key: 'documentType',
            label: labels.documentType,
            width: '15%'
        },
        {
            key: 'documentName',
            label: labels.documentName,
            width: '20%'
        },
        {
            key: 'generationDate',
            label: labels.generationDate,
            formatter: date,
            width: '15%'
        },
        {
            key: 'generatedBy',
            label: labels.generatedBy,
            width: '15%'
        },
        {
            key: 'status',
            label: labels.status,
            width: '15%',
            formatter: status => getEnumAttribute(enumTypes.DocumentRequestStatus, status, 'name')
        },
        {
            key: 'actions',
            label: labels.actionButtons,
            width: '20%',
            formatter: (column, data) => (
                <TableActions
                    key={data.id}
                    actions={this.getActions(labels, permissions, data.status)}
                    handleClick={this.props.handleClick}
                    record={data}
                />
            )
        }
    ]);

    getActions = (labels, permissions, status) => [{
            id: 'downloadDocument',
            label: labels.downloadDocument,   
            // Is the user prevented from downloading this document
            disabled: !(permissions.canViewDocument && status === 'COMPLETE')
        }, {
            id: 'removeDocument',
            label: labels.removeDocument,
            disabled: !permissions.canRemoveDocument
        }
    ];
    
    render() {
        const { labels, permissions, enumTypes } = this.props;
        const columns = this.getColumns(labels, permissions, enumTypes);

        return <Table {...this.props} key="results-table" columns={columns} />;
    }
}

GeneratedDocumentsResults.propTypes = {
    labels: PropTypes.object.isRequired,
    permissions: PropTypes.object.isRequired,
    enumTypes: PropTypes.shape({
        DocumentRequestStatus: PropTypes.shape({
            values: PropTypes.array.isRequired
        })
    }).isRequired,
    handleClick: PropTypes.func
};
