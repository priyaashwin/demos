'use strict';

import PropTypes from 'prop-types';
import React, { useCallback, useRef } from 'react';
import ErrorMessage from '../../../error/error';
import useFilteredResults from '../../../filter/useFilteredResults/useFilteredResults';
import Loading from '../../../loading/loading';
import ModelList from '../../../model/modelList';
import Paginator from '../../../paginator/jsx/view';
import PaginatedResultsList from '../../../results/paginatedResultsList';
import { getEnumAttribute } from '../../../utils/js/enumUtils';
import GeneratedDocumentsResults from './outputsResults';
import ReactDOM from 'react-dom';

// Map document type to object containing format for display
// and camel case format required for the API call
const documentTypesCamelCase = {
    CASENOTE: 'caseNote',
    CHILD_LOOKED_AFTER: 'childLookedAfter',
    CHRONOLOGY: 'chronology',
    FORM: 'form',
    PERSON: 'person',
    PERSON_LETTER: 'person'
    
};

export default function OutputsView({
    outputsURL,
    downloadDocumentURL,
    subjectId,
    generatedByPersonId,
    columnLabels: labels,
    permissions,
    enumTypes,
    endpoint,
    setRemoveAllButtonVisibility
}) {
    const documentResultsComponent = useRef(null);
    const getPaginatedResults = useCallback(
        ({ documentName, uploadDateFrom, uploadDateTo }, rowsPerPage, currentPage) => {
            const documentNameFilterValue = documentName ? documentName.getValue() : '';
            const uploadDateFromFilterValue = uploadDateFrom ? uploadDateFrom.getValue() : '';
            const uploadDateToFilterValue = uploadDateTo ? uploadDateTo.getValue() : '';

            return new PaginatedResultsList().load(
                {
                    ...endpoint,
                    url: new ModelList(outputsURL).getURL({
                        subjectId,
                        generatedByPersonId,
                        documentName: documentNameFilterValue,
                        startDate: uploadDateFromFilterValue,
                        endDate: uploadDateToFilterValue
                    })
                },
                rowsPerPage,
                currentPage
            );
        },
        [subjectId, generatedByPersonId, outputsURL, endpoint]
    );

    const resultsMapper = useCallback(
        result => ({
            ...result,
            documentType: getEnumAttribute(enumTypes.DocumentType, result.documentType, 'name'),
            documentTypeForPayload: documentTypesCamelCase[result.documentType]
        }),
        [enumTypes]
    );

    const {
        setRowsPerPage,
        rowsPerPage,
        setCurrentPage,
        currentPage,
        clearErrors,
        loading,
        errors,
        totalResultsCount,
        data
    } = useFilteredResults(getPaginatedResults, resultsMapper);
    
     const fireEvent = (id) => {
        ReactDOM.findDOMNode(documentResultsComponent.current).dispatchEvent(new CustomEvent('prepareIndividualDocumentRequestForRemove', {
            bubbles:true,
            cancelable: true,
            detail: {id: id}
        }));
    };

    const handleClick = (action, data) => {
        const { attachmentId, documentTypeForPayload } = data;

        switch (action) {
            case 'downloadDocument':
                window.location = new ModelList(downloadDocumentURL).getURL({
                    attachmentId: attachmentId,
                    documentType: documentTypeForPayload
                });
                break;
          
            case 'removeDocument':
                fireEvent(data.id);
                break;
        }
    };

    const onChangeRowsPerPage = (newRowsPerPage, newCurrentPage) => {
        setRowsPerPage(newRowsPerPage);
        setCurrentPage(newCurrentPage);
    };

    const onChangePage = newCurrentPage => {
        setCurrentPage(newCurrentPage);
    };

    let content;

    if (loading) {
        content = <Loading />;
    } else if (errors) {
        content = <ErrorMessage errors={errors} onClose={clearErrors} />;
    } else {
        setRemoveAllButtonVisibility(data.length);
        content = (
            <>
                <GeneratedDocumentsResults
                    labels={labels}
                    data={data}
                    loading={loading}
                    permissions={permissions}
                    enumTypes={enumTypes}
                    handleClick={handleClick}
                    ref={documentResultsComponent}
                />
                <Paginator
                    onChangeRowsPerPage={onChangeRowsPerPage}
                    onChangePage={onChangePage}
                    rowsPerPage={rowsPerPage}
                    currentPage={currentPage}
                    totalResultsCount={totalResultsCount}
                />
            </>
        );
    }

    return <div className="document-widget-page">{content}</div>;
}

OutputsView.propTypes = {
    outputsURL: PropTypes.string.isRequired,
    endpoint: PropTypes.string.isRequired,
    downloadDocumentURL: PropTypes.string.isRequired,
    subjectId: PropTypes.number.isRequired,
    generatedByPersonId: PropTypes.number.isRequired,
    userId: PropTypes.number.isRequired,
    enumTypes: PropTypes.shape({
        DocumentType: PropTypes.shape({
            values: PropTypes.array.isRequired
        })
    }).isRequired,
    columnLabels: PropTypes.object.isRequired,
    permissions: PropTypes.object.isRequired,
    setRemoveAllButtonVisibility: PropTypes.func.isRequired
};
