'use strict';

import PropTypes from 'prop-types';
import React, {useState, useRef} from 'react';
import ReactDOM from 'react-dom';
import Accordion from 'usp-accordion-panel';
import ActiveFilters from '../../filter/activeFilter/view';
import Filter from '../../filter/view';
import { Tab } from '../../tabs/jsx/tabView';
import endpoint from '../js/cfg';
import AttachmentsView from './attachments/attachmentsView';
import { getCaseNoteAttachmentsColumns, getFormAttachmentsColumns } from './attachments/getColumns';
import DateFilter from './filters/date';
import NameFilter from './filters/name';
import OutputsView from './outputs/outputsView';
import Button from '../../dialog/button';

export default function AccordionView(props) {
    const { subjectId, userId, enumTypes, codedEntries, permissions, showFilter, resultsFilterConfig } = props;
    const { labels: filterLabels } = resultsFilterConfig;
    const { accordionHeadings, generatedDocumentTable, attachmentsTable, buttons } = props.labels;
    const { formsAttachmentsURLs, caseNotesAttachmentsURLs, outputsURLS } = props.urls;
    const [removeAllButtonVisibilityState, setRemoveAllButtonVisibilityState] = useState(false);
    const outputComponent = useRef(null);

    const getRemoveAllButton = () => (
        permissions.canRemoveAllDocuments && removeAllButtonVisibilityState &&
        <Button
            type="active"
            title={buttons.removeAll}
            icon="fa-minus-circle"
            onClick={fireEvent}
        >
            {' '}{buttons.removeAll}
        </Button>
    );

    const fireEvent = () => {
        ReactDOM.findDOMNode(outputComponent.current).dispatchEvent(new CustomEvent('prepareForRemove', {
            bubbles:true,
            cancelable: true,
            detail: {}
        }));
    };

    const handleRemoveAllButtonVisibility = (listLength) => {
        if ((listLength > 0) !== removeAllButtonVisibilityState) {
            setRemoveAllButtonVisibilityState(!removeAllButtonVisibilityState);
        }
    };

    return (
        <>
            <ActiveFilters
                resultsFilterConfig={resultsFilterConfig}
            />
            {showFilter && (
                <Filter label={filterLabels.filterBy}>
                    <Tab key="nameFilter" label={filterLabels.nameTabTitle} tabId="nameFilter">
                        <NameFilter labels={filterLabels} />
                    </Tab>
                    <Tab key="dateFilter" label={filterLabels.dateTabTitle} tabId="dateFilter">
                        <DateFilter labels={filterLabels} />
                    </Tab>
                </Filter>
            )}
            <Accordion ref={outputComponent} menu={getRemoveAllButton()}  expanded={true} title={accordionHeadings.outputs}>
                <OutputsView
                    outputsURL={outputsURLS.generatedDocumentsURL}
                    downloadDocumentURL={outputsURLS.downloadDocumentURL}
                    subjectId={subjectId}
                    generatedByPersonId={userId}
                    endpoint={endpoint.documentListEndpoint}
                    enumTypes={enumTypes}
                    columnLabels={generatedDocumentTable.columns}
                    permissions={permissions}
                    setRemoveAllButtonVisibility={handleRemoveAllButtonVisibility}
                />
            </Accordion>
            <Accordion expanded={false} title={accordionHeadings.caseNotes}>
                <AttachmentsView
                    attachmentsURL={caseNotesAttachmentsURLs.uploadedAttachmentsURL}
                    downloadAttachmentURL={caseNotesAttachmentsURLs.downloadAttachmentURL}
                    endpoint={endpoint.caseNoteAttachmentsEndpoint}
                    codedEntries={codedEntries}
                    subjectId={subjectId}
                    canView={permissions.canViewCaseNote}
                    columnLabels={attachmentsTable.columns}
                    getColumns={getCaseNoteAttachmentsColumns}
                />
            </Accordion>
            <Accordion expanded={false} title={accordionHeadings.forms}>
                <AttachmentsView
                    attachmentsURL={formsAttachmentsURLs.uploadedAttachmentsURL}
                    downloadAttachmentURL={formsAttachmentsURLs.downloadAttachmentURL}
                    endpoint={endpoint.formInstanceAttachmentsEndpoint}
                    codedEntries={codedEntries}
                    subjectId={subjectId}
                    canView={permissions.canViewFormInstance}
                    columnLabels={attachmentsTable.columns}
                    getColumns={getFormAttachmentsColumns}
                />
            </Accordion>
        </>
    );
}

AccordionView.propTypes = {
    subjectId: PropTypes.number.isRequired,
    userId: PropTypes.number.isRequired,
    enumTypes: PropTypes.shape({
        DocumentType: PropTypes.shape({
            values: PropTypes.array.isRequired
        })
    }).isRequired,
    permissions: PropTypes.shape({
        canViewDocument: PropTypes.bool.isRequired,
        canViewCaseNote: PropTypes.bool.isRequired,
        canViewFormInstance: PropTypes.bool.isRequired,
        canRemoveAllDocuments: PropTypes.bool.isRequired
    }).isRequired,
    codedEntries: PropTypes.shape({
        entryTypes: PropTypes.object.isRequired,
        entrySubTypes: PropTypes.object.isRequired,
        source: PropTypes.object.isRequired,
        sourceOrganisation: PropTypes.object.isRequired
    }).isRequired,
    labels: PropTypes.shape({
        generatedDocumentTable: PropTypes.shape({
            columns: PropTypes.object.isRequired
        }).isRequired,
        attachmentsTable: PropTypes.shape({
            columns: PropTypes.object.isRequired
        }).isRequired,
        buttons: PropTypes.shape({
            removeAll: PropTypes.object.isRequired
        }),
        accordionHeadings: PropTypes.object.isRequired
    }).isRequired,
    resultsFilterConfig: PropTypes.shape({
        labels: PropTypes.object.isRequired
    }),
    urls: PropTypes.shape({
        outputsURLS: PropTypes.object.isRequired,
        caseNotesAttachmentsURLs: PropTypes.object.isRequired,
        formsAttachmentsURLs: PropTypes.object.isRequired
    }).isRequired,
    showFilter: PropTypes.bool.isRequired
};
