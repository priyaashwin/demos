'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { FilterContextProvider } from '../../filter/filterContextProvider';
import AccordionView from './accordionView';
import { filterConfig } from './filters/filterConfig';

export default function View(props) {
    return (
        <FilterContextProvider filterConfig={filterConfig}> 
            <AccordionView {...props} />
        </FilterContextProvider>
    );
}

View.propTypes = {
    subjectId: PropTypes.number.isRequired,
    userId: PropTypes.number.isRequired,
    enumTypes: PropTypes.shape({
        DocumentType: PropTypes.shape({
            values: PropTypes.array.isRequired
        })
    }).isRequired,
    permissions: PropTypes.shape({
        canViewDocument: PropTypes.bool.isRequired,
        canViewCaseNote: PropTypes.bool.isRequired,
        canViewFormInstance: PropTypes.bool.isRequired,
        canRemoveAllDocuments: PropTypes.bool.isRequired,
        canRemoveDocument: PropTypes.bool.isRequired
    }).isRequired,
    codedEntries: PropTypes.shape({
        entryTypes: PropTypes.object.isRequired,
        entrySubTypes: PropTypes.object.isRequired,
        source: PropTypes.object.isRequired,
        sourceOrganisation: PropTypes.object.isRequired
    }).isRequired,
    labels: PropTypes.shape({
        generatedDocumentTable: PropTypes.shape({
            columns: PropTypes.object.isRequired
        }).isRequired,
        attachmentsTable: PropTypes.shape({
            columns: PropTypes.object.isRequired
        }).isRequired,
        buttons: PropTypes.shape({
            removeAll: PropTypes.object.isRequired
        }),
        accordionHeadings: PropTypes.object.isRequired
    }).isRequired,
    urls: PropTypes.shape({
        outputsURLS: PropTypes.object.isRequired,
        caseNotesAttachmentsURLs: PropTypes.object.isRequired,
        formsAttachmentsURLs: PropTypes.object.isRequired
    }).isRequired
};
