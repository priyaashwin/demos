'use strict';

class Endpoint {
    constructor() {
        this.documentListEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.output.DocumentRequest+json'
            },
            responseType: 'json',
            responseStatus: 200,
            params: {
                s: [{ generation_date: 'desc' }]
            }
        };
        this.caseNoteAttachmentsEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.casenote.CaseNoteEntryAttachmentWithType+json'
            },
            responseType: 'json',
            responseStatus: 200,
            params: {
                s: [{ last_uploaded: 'desc' }]
            }
        };
        this.formInstanceAttachmentsEndpoint = {
            method: 'get',
            contentType: 'json',
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/vnd.olmgroup-usp.form.FormInstanceAttachmentWithFormName+json'
            },
            responseType: 'json',
            responseStatus: 200,
            params: {
                s: [{ last_uploaded: 'desc' }]
            }
        };
    }
}

export default new Endpoint();