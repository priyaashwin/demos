const ENV = process.env.NODE_ENV || 'development';
const isTest = ENV === 'test';

let presets;

if (isTest) {
    presets = [
        [
            '@babel/preset-env',
            {
                targets: {
                    node: 'current'
                }
            }
        ],
        '@babel/preset-react'
    ];
} else {
    presets = [
        [
            '@babel/preset-env',
            {
                targets: ['> 5%, ie>=11'],
                shippedProposals: true,
                modules: false,
                loose: true
            }
        ],
        [
            '@babel/preset-react',
            {
                development: process.env.BABEL_ENV === 'development'
            }
        ]
    ];
}

module.exports = {
    presets: presets,
    plugins: [
        ['@babel/plugin-transform-parameters', { loose: true }],
        ['@babel/plugin-proposal-class-properties', { loose: true }],
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-shorthand-properties',
        ['@babel/plugin-transform-runtime'],
        [
            'transform-inline-environment-variables',
            {
                include: ['NODE_ENV']
            }
        ]
    ]
};
