'use strict';
import React from 'react';
import Properties from '../../../src/header/jsx/properties/properties';
import PersonProperties from '../../../src/header/jsx/properties/person/properties';
import GroupProperties from '../../../src/header/jsx/properties/group/properties';
import PersonBirthDate from '../../../src/header/jsx/properties/person/birthDate';
import PersonGender from '../../../src/header/jsx/properties/person/gender';
import PersonNHSNumber from '../../../src/header/jsx/properties/person/nhsNumber';
import GroupType from '../../../src/header/jsx/properties/group/type';
import GroupStartDate from '../../../src/header/jsx/properties/group/startDate';
import FuzzyDateValue from '../../../src/header/jsx/date/fuzzyDateValue';
import DateValue from '../../../src/header/jsx/date/dateValue';
import PropertyValue from '../../../src/header/jsx/properties/propertyValue';
import PropertyLabel from '../../../src/header/jsx/properties/propertyLabel';
import { shallow, mount } from 'enzyme';

describe('Properties', () => {
    describe('Properties wrapper', () => {
        const emptyProps = {
            labels: {},
            subject: {},
            codedEntries: {
                groupType: {}
            }
        };

        it('should output properties for a person', () =>
            expect(shallow(<Properties {...emptyProps} type="person" />).find(PersonProperties)).toHaveLength(1));

        it('should output properties for a group', () =>
            expect(shallow(<Properties {...emptyProps} type="group" />).find(GroupProperties)).toHaveLength(1));
    });

    describe('Person properties', () => {
        const personProps = {
            labels: {},
            subject: {},
            codedEntries: {
                gender: {
                    codedEntries: {
                        MALE: { name: 'man' },
                        FEMALE: { name: 'woman' }
                    }
                }
            }
        };

        it('should always output BirthDate', () =>
            expect(shallow(<PersonProperties {...personProps} />).find(PersonBirthDate)).toHaveLength(1));

        it('should not output gender when property not present', () =>
            expect(shallow(<PersonProperties {...personProps} />).find(PersonGender)).toHaveLength(0));

        it('should not output nhsnumber when property not present', () =>
            expect(shallow(<PersonProperties {...personProps} />).find(PersonNHSNumber)).toHaveLength(0));

        it('should output the gender when present', () =>
            expect(
                shallow(<PersonProperties {...personProps} subject={{ gender: 'MALE' }} />).find(PersonGender)
            ).toHaveLength(1));

        it('should output the nhsnumber when present', () =>
            expect(
                shallow(<PersonProperties {...personProps} subject={{ nhsNumber: '7777777777' }} />).find(
                    PersonNHSNumber
                )
            ).toHaveLength(1));
    });

    describe('Group properties', () => {
        const groupProps = {
            labels: {},
            subject: {},
            codedEntries: {
                groupType: {
                    codedEntries: {
                        A: { name: 'Type A' },
                        B: { name: 'Type B' }
                    }
                }
            }
        };

        it('should always output Type', () =>
            expect(shallow(<GroupProperties {...groupProps} />).find(GroupType)).toHaveLength(1));

        it('should always output StartDate', () =>
            expect(shallow(<GroupProperties {...groupProps} />).find(GroupStartDate)).toHaveLength(1));


        it('should not output the closed date if not present - i.e. only start date is output', () => {
            expect(mount(<GroupProperties {...groupProps} />).find(DateValue)).toHaveLength(1);
        })
        
        it('should output closedDate if present', () => {
            expect(mount(<GroupProperties {...groupProps} subject={{closeDate:11123344}} />).containsMatchingElement(
                <DateValue date={11123344} />
            )).toBe(true);
        });
        
    });
    describe('Person properties BirthDate', () => {
        const birthDateProperties = {
            labels: {
                born: 'BORN',
                dueDate: 'DUEDATE',
                notCarried: 'NOTCARRIED',
                died: 'DEAD'
            }
        };

        describe('date of birth', () => {
            const withDateOfBirthProperties = {
                ...birthDateProperties,
                dateOfBirth: {
                    _type: 'FuzzyDate',
                    calculatedDate: 1450224000000,
                    fuzzyDateMask: 'DAY',
                    allowedRange: { upperBoundDate: 1450310399000, lowerBoundDate: 1450224000000 }
                }
            };
            it('should output the date of birth', () => {
                const wrapper = shallow(<PersonBirthDate {...withDateOfBirthProperties} />);
                expect(wrapper.find(FuzzyDateValue)).toHaveLength(1);
                // Fuzzy Date value is correct
                expect(wrapper.find(FuzzyDateValue).props()).toHaveProperty(
                    'fuzzyDate',
                    withDateOfBirthProperties.dateOfBirth
                );
            });
            describe('Estimated flag', () => {
                it('should have the estimated flag', () =>
                    expect(
                        shallow(<PersonBirthDate {...withDateOfBirthProperties} dateOfBirthEstimated={true} />)
                            .find(FuzzyDateValue)
                            .props()
                    ).toHaveProperty('isEstimate', true));
                it('should not have the estimated flag', () =>
                    expect(
                        shallow(<PersonBirthDate {...withDateOfBirthProperties} />)
                            .find(FuzzyDateValue)
                            .props()
                    ).toHaveProperty('isEstimate', false));
            });
        });

        describe('Age', () => {
            it('should output the age', () =>
                expect(
                    shallow(<PersonBirthDate {...birthDateProperties} age={5} />)
                        .find(PropertyValue)
                        .props()
                ).toHaveProperty('value', '(5 years)'));
            it('should output the age with singular', () =>
                expect(
                    shallow(<PersonBirthDate {...birthDateProperties} age={1} />)
                        .find(PropertyValue)
                        .props()
                ).toHaveProperty('value', '(1 year)'));
        });

        describe('Life State', () => {
            describe('UNBORN', () => {
                const dueDate = 1450224000000;
                const wrapper = shallow(
                    <PersonBirthDate {...birthDateProperties} lifeState={'UNBORN'} dueDate={dueDate} />
                );
                it('should output the label "due date"', () =>
                    // Unborn label
                    expect(wrapper.find(PropertyLabel).props()).toHaveProperty(
                        'label',
                        birthDateProperties.labels.dueDate
                    ));

                it('should output the date value', () =>
                    // Due Date
                    expect(wrapper.find(DateValue).props()).toHaveProperty('date', dueDate));
            });

            describe('NOT_CARRIED_TO_TERM', () => {
                const wrapper = shallow(<PersonBirthDate {...birthDateProperties} lifeState={'NOT_CARRIED_TO_TERM'} />);
                it('should output the value "not carried"', () =>
                    // Unborn label
                    expect(wrapper.find(PropertyValue).props()).toHaveProperty(
                        'value',
                        birthDateProperties.labels.notCarried
                    ));
            });

            describe('DECEASED', () => {
                const withDateOfDeathProperties = {
                    ...birthDateProperties,
                    diedDate: {
                        _type: 'FuzzyDate',
                        calculatedDate: 1462971599000,
                        fuzzyDateMask: 'HOUR',
                        allowedRange: { upperBoundDate: 1462971599000, lowerBoundDate: 1462968000000 }
                    }
                };

                const wrapper = shallow(<PersonBirthDate {...withDateOfDeathProperties} lifeState={'DECEASED'} />);

                it('should output the label "due date"', () =>
                    // Died label
                    expect(wrapper.find(PropertyLabel).props()).toHaveProperty(
                        'label',
                        withDateOfDeathProperties.labels.died
                    ));

                it('should output the fuzzy date', () =>
                    // Fuzzy Date value is correct
                    expect(wrapper.find(FuzzyDateValue).props()).toHaveProperty(
                        'fuzzyDate',
                        withDateOfDeathProperties.diedDate
                    ));
            });
        });
    });
});
