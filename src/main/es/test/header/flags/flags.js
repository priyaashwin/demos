'use strict';
import React from 'react';
import Flags from '../../../src/header/jsx/flags/flags';
import PersonFlags from '../../../src/header/jsx/flags/person/flags';
import Warnings from '../../../src/warnings/warnings';
import Classifications from '../../../src/header/jsx/classifications/classifications';
import { shallow } from 'enzyme';

describe('Flags', () => {
    describe('Flags wrapper', () => {
        const emptyProps = {
            labels: {},
            subject: {},
            codedEntries: {
                warnings: {},
                alerts: {}
            },
            permissions: {}
        };

        it('should output flags for a person', () =>
            expect(
                shallow(<Flags {...emptyProps} subjectLockInfoEndpoint={{}} type="person" />).find(PersonFlags)
            ).toHaveLength(1));

        it('should not output warnings for a group', () =>
            expect(shallow(<Flags {...emptyProps} type="group" />).containsMatchingElement(<div />)).toBe(true));
    });

    describe('Person warnings', () => {
        const flagsProps = {
            labels: {
                classifications: {},
                warnings: 'WARNINGS'
            },
            codedEntries: {
                warnings: {},
                alerts: {}
            },
            permissions: {}
        };

        describe('Person flags', () => {
            const personFlagsProps = {
                ...flagsProps,
                subjectLockInfoEndpoint: {},
                subject: {
                    classificationAssignments: []
                }
            };

            const wrapper = shallow(<PersonFlags {...personFlagsProps} />);

            it('should always output the classificatons', () => expect(wrapper.find(Classifications)).toHaveLength(1));
            it('should always output the warnings', () => expect(wrapper.find(Warnings)).toHaveLength(1));
        });
    });
});
