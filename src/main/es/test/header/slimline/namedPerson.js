'use strict';
import React from 'react';
import NamedPerson from '../../../src/header/jsx/expander/person/slimline/namedPerson';
import Subject from '../../../src/header/jsx/expander/person/slimline/subject';
import { shallow, mount } from 'enzyme';

describe('Named Person (Slimline)', () => {
    describe('Named Person wrapper', () => {
        const emptyProps = {
            labels: {}
        };

        it('should output no named person when no named persons are supplied.', () => {
            const wrapper = shallow(<NamedPerson {...emptyProps} personPersonRelationships={[]} />).dive();
            expect(wrapper.containsMatchingElement(<Subject label="named person" subject={undefined} />)).toBe(true);
        });

        it('should output a named person when exactly one qualifying record is supplied with a default label.', () => {
            const wrapper = shallow(
                <NamedPerson
                    {...emptyProps}
                    personPersonRelationships={[
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte'
                        }
                    ]}
                />
            ).dive();

            const name = 'Henry Green';
            const relationship = 'Acolyte';
            const contents = (
                <span title={name + ' ' + relationship}>
                    <span>{name}</span>
                    <span>{' - '}</span>
                    <span>{relationship}</span>
                </span>
            );
            expect(wrapper.containsMatchingElement(<Subject label="named person" subject={contents} />)).toBe(true);
        });

        it('should output a named person when exactly one qualifying record is supplied with a label.', () => {
            const wrapper = shallow(
                <NamedPerson
                    {...emptyProps}
                    personPersonRelationships={[
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte'
                        }
                    ]}
                    labels={{ namedPerson: 'New Label' }}
                />
            ).dive();
            const name = 'Henry Green';
            const relationship = 'Acolyte';
            const contents = (
                <span title={name + ' ' + relationship}>
                    <span>{name}</span>
                    <span>{' - '}</span>
                    <span>{relationship}</span>
                </span>
            );
            expect(wrapper.containsMatchingElement(<Subject label="New Label" subject={contents} />)).toBe(true);
        });

        it('should output the correctly selected named person when muliple records are supplied with a label.', () => {
            const wrapper = shallow(
                <NamedPerson
                    {...emptyProps}
                    labels={{ namedPerson: 'New Label' }}
                    personPersonRelationships={[
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 101,
                            roleBPersonId: 100,
                            name: 'Henry Green',
                            relationship: 'Doppelganger'
                        },
                        {
                            typeDiscriminator: 'FamilialRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Gopher'
                        },
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte'
                        },
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'James Fenton',
                            relationship: 'Hidden'
                        }
                    ]}
                />
            ).dive();
            const name = 'Henry Green';
            const relationship = 'Acolyte';
            const contents = (
                <span title={name + ' ' + relationship}>
                    <span>{name}</span>
                    <span>{' - '}</span>
                    <span>{relationship}</span>
                </span>
            );
            expect(wrapper.containsMatchingElement(<Subject label="New Label" subject={contents} />)).toBe(true);
        });
    });
});
