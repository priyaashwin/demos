'use strict';
import React from 'react';
import AllocatedWorker from '../../../src/header/jsx/expander/person/slimline/allocatedWorker';
import Subject from '../../../src/header/jsx/expander/person/slimline/subject';
import { shallow, mount } from 'enzyme';

describe('Allocated Worker (Slimline)', () => {
    describe('Allocated Worker wrapper', () => {
        const emptyProps = {
            labels: {}
        };

        it('should output no allocations when no allocated workers or teams are supplied.', () => {
            const wrapper = shallow(
                <AllocatedWorker {...emptyProps} personPersonRelationships={[]} allocatedTeams={[]} />
            ).dive();
            expect(wrapper.containsMatchingElement(<Subject label="allocated worker" subject={undefined} />)).toBe(
                true
            );
        });

        it('should output an allocated team when no allocated workers are supplied but a team is.', () => {
            const wrapper = shallow(
                <AllocatedWorker
                    {...emptyProps}
                    personPersonRelationships={[]}
                    allocatedTeams={[
                        {
                            organisationVO: {
                                name: 'SMERSH'
                            }
                        },
                        {
                            organisationVO: {
                                name: 'UNIT'
                            }
                        }
                    ]}
                />
            ).dive();

            const contents = (
                <span title="SMERSH">
                    <span>SMERSH</span>
                </span>
            );
            expect(wrapper.containsMatchingElement(<Subject label="allocated worker" subject={contents} />)).toBe(true);
        });

        it('should output an allocated worker when exactly one qualifying worker is supplied with a default label, ignoring the team.', () => {
            const wrapper = shallow(
                <AllocatedWorker
                    {...emptyProps}
                    personPersonRelationships={[
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte'
                        }
                    ]}
                    allocatedTeams={[
                        {
                            organisationVO: {
                                name: 'SMERSH'
                            }
                        },
                        {
                            organisationVO: {
                                name: 'UNIT'
                            }
                        }
                    ]}
                />
            ).dive();

            const name = 'Henry Green';
            const relationship = 'Acolyte';
            const contents = (
                <span title={name + ' ' + relationship}>
                    <span>{name}</span>
                    <span>{' - '}</span>
                    <span>{relationship}</span>
                </span>
            );
            expect(wrapper.containsMatchingElement(<Subject label="allocated worker" subject={contents} />)).toBe(true);
        });

        it('should output an allocated worker when exactly one qualifying record is supplied with a label.', () => {
            const wrapper = shallow(
                <AllocatedWorker
                    {...emptyProps}
                    personPersonRelationships={[
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte'
                        }
                    ]}
                    allocatedTeams={[]}
                    labels={{ allocatedWorker: 'New Label' }}
                />
            ).dive();
            const name = 'Henry Green';
            const relationship = 'Acolyte';
            const contents = (
                <span title={name + ' ' + relationship}>
                    <span>{name}</span>
                    <span>{' - '}</span>
                    <span>{relationship}</span>
                </span>
            );
            expect(wrapper.containsMatchingElement(<Subject label="New Label" subject={contents} />)).toBe(true);
        });

        it('should output the correctly selected allocated worker when muliple records are supplied with a label.', () => {
            const wrapper = shallow(
                <AllocatedWorker
                    {...emptyProps}
                    labels={{ allocatedWorker: 'New Label' }}
                    personPersonRelationships={[
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 101,
                            roleBPersonId: 100,
                            name: 'Henry Green',
                            relationship: 'Doppelganger'
                        },
                        {
                            typeDiscriminator: 'FamilialRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Gopher'
                        },
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte'
                        },
                        {
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'James Fenton',
                            relationship: 'Hidden'
                        }
                    ]}
                />
            ).dive();
            const name = 'Henry Green';
            const relationship = 'Acolyte';
            const contents = (
                <span title={name + ' ' + relationship}>
                    <span>{name}</span>
                    <span>{' - '}</span>
                    <span>{relationship}</span>
                </span>
            );
            expect(wrapper.containsMatchingElement(<Subject label="New Label" subject={contents} />)).toBe(true);
        });
    });
});
