'use strict';
import React from 'react';
import Address from '../../../src/header/jsx/expander/person/slimline/address';
import PropertyValue from '../../../src/header/jsx/properties/propertyValue';
import { shallow } from 'enzyme';
import SingleLineAddress from '../../../src/address/jsx/singleLineAddress';
/**
 * Tests for Slimline Address
 * Note the Slimline Address is wrapped in a HOC to locate the preferred address
 * and therfore `.dive()` is needed to access the actual component inside the
 * wrapper.
 *
 * Actual address output is handled by SingleLineAddress which has its' own
 * set of tests
 */
describe('Address', () => {
    describe('Preferred address', () => {
        const emptyProps = {
            labels: {},
            address: {},
            mapLinkUrl: '',
            permissions: {},
            codedEntries: {}
        };

        it('should not find a preferred address', () => {
            const wrapper = shallow(<Address {...emptyProps} />).dive();

            expect(
                wrapper.containsMatchingElement(
                    <PropertyValue value={<SingleLineAddress {...emptyProps} address={null} />} />
                )
            ).toBe(true);
        });

        it('should not find a preferred address when they type is invalid', () => {
            const wrapper = shallow(
                <Address
                    {...emptyProps}
                    address={{
                        temporalStatus: 'ACTIVE',
                        type: 'FOO',
                        location: { location: 'my house' }
                    }}
                />
            ).dive();

            expect(
                wrapper.containsMatchingElement(
                    <PropertyValue value={<SingleLineAddress {...emptyProps} address={null} />} />
                )
            ).toBe(true);
        });

        it('should output no address for a person with permission to view an address but the preffered addres is not active.', () => {
            const permissions = { canViewAddress: true };
            const wrapper = shallow(
                <Address
                    {...emptyProps}
                    permissions={permissions}
                    address={{
                        temporalStatus: 'NOT_ACTIVE'
                    }}
                />
            ).dive();

            expect(
                wrapper.containsMatchingElement(
                    <PropertyValue
                        value={<SingleLineAddress {...emptyProps} permissions={permissions} address={null} />}
                    />
                )
            ).toBe(true);
        });

        it('should not find a preferred address when they status is invalid', () => {
            const wrapper = shallow(
                <Address
                    {...emptyProps}
                    address={{
                        temporalStatus: 'INACTIVE',
                        type: 'HOME',
                        location: { location: 'my house' }
                    }}
                />
            ).dive();

            expect(
                wrapper.containsMatchingElement(
                    <PropertyValue value={<SingleLineAddress {...emptyProps} address={null} />} />
                )
            ).toBe(true);
        });

        it('should find a preferred address', () => {
            const address = {
                temporalStatus: 'ACTIVE',
                type: 'HOME',
                location: { location: 'my house' }
            };
            const wrapper = shallow(<Address {...emptyProps} address={address} />).dive();

            expect(
                wrapper.containsMatchingElement(
                    <PropertyValue value={<SingleLineAddress {...emptyProps} address={address} />} />
                )
            ).toBe(true);
        });
    });
});
