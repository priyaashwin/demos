'use strict';
import React from 'react';
import Warnings from '../../../src/warnings/warnings';
import PersonWarnings from '../../../src/warnings/person/warnings';
import OrganisationWarnings from '../../../src/warnings/organisation/warnings';
import WarningIcon from '../../../src/warnings/icon';
import { shallow } from 'enzyme';

const getFutureDate = function() {
    const date = new Date();
    addDays(date, 5);
    return date.getTime();
};

const getPastDate = function() {
    const date = new Date();
    addDays(date, -5);
    return date.getTime();
};

const addDays = function(date, days) {
    date.setDate(date.getDate() + days);
};

describe('Warnings', () => {
    describe('Warnings wrapper', () => {
        const emptyProps = {
            labels: {},
            subject: {},
            canManageWarnings: false,
            category: {},
            onClick: () => {}
        };

        it('should output warnings for a person', () =>
            expect(shallow(<Warnings {...emptyProps} type="person" />).find(PersonWarnings)).toHaveLength(1));

        it('should not output warnings for a group', () =>
            expect(shallow(<Warnings {...emptyProps} type="group" />).containsMatchingElement(<div />)).toBe(true));
    });

    describe('Person warnings', () => {
        const warningProps = {
            labels: {
                manage: 'MANAGE',
                view: 'VIEW',
                related: 'RELATED'
            },
            category: {
                codedEntries: {
                    A1: { name: 'The first' },
                    A2: { name: 'The second' },
                    A3: { name: 'The third' },
                    A4: { name: 'The forth' },
                    A5: { name: 'The fifth' }
                }
            },
            onClick: () => {}
        };

        describe('Subject warnings', () => {
            const personNoWarningProps = {
                ...warningProps,
                subject: {
                    personWarnings: [
                        {
                            personWarningAssignmentVOs: []
                        }
                    ]
                }
            };
            const personWarningProps = {
                ...warningProps,
                subject: {
                    personWarnings: [
                        {
                            personWarningAssignmentVOs: [
                                {
                                    codedEntryVO: {
                                        code: 'A1'
                                    }
                                },
                                {
                                    codedEntryVO: {
                                        code: 'A2'
                                    },
                                    endDate: null
                                },
                                {
                                    codedEntryVO: {
                                        code: 'A3'
                                    },
                                    endDate: getFutureDate()
                                },
                                {
                                    codedEntryVO: {
                                        code: 'A4'
                                    },
                                    endDate: getPastDate()
                                },
                                {
                                    codedEntryVO: {
                                        code: 'A5'
                                    },
                                    endDate: new Date().getTime()
                                }
                            ]
                        }
                    ]
                }
            };

            const personWarningWithAuthorisationProps = {
                ...warningProps,
                subject: {
                    personWarnings: [
                        {
                            personWarningAssignmentVOs: [
                                {
                                    codedEntryVO: {
                                        code: 'A1'
                                    },
                                    authorisation: {
                                        authorisationState: 'PENDING'
                                    }
                                },
                                {
                                    codedEntryVO: {
                                        code: 'A2'
                                    },
                                    authorisation: {
                                        authorisationState: 'REJECTED'
                                    }
                                },
                                {
                                    codedEntryVO: {
                                        code: 'A3'
                                    },
                                    authorisation: {
                                        authorisationState: 'AUTHORISED'
                                    }
                                },
                                {
                                    codedEntryVO: {
                                        code: 'A4'
                                    }
                                }
                            ]
                        }
                    ]
                }
            };

            it('should use the manage warnings label as the title if the user has permissions', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningProps} canManageWarnings={true} />
                    ).containsMatchingElement(<div>{'Click the icon to MANAGE'}</div>)
                ).toBe(true));

            it('should use the view warnings label as the title if the user has permissions', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningProps} canManageWarnings={false} />
                    ).containsMatchingElement(<div>{'Click the icon to VIEW'}</div>)
                ).toBe(true));

            it('should show the correct number of active warnings', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningProps} canManageWarnings={false} />
                    ).containsMatchingElement(<span>{3}</span>)
                ).toBe(true));

            it('should output the warning from the category', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningProps} canManageWarnings={false} />
                    ).containsMatchingElement(
                        <ul>
                            <li>{'The first'}</li>
                            <li>{'The second'}</li>
                            <li>{'The third'}</li>
                        </ul>
                    )
                ).toBe(true));

            it('should not show the warning count if details hidden', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningProps} canManageWarnings={false} showDetails={false} />
                    ).containsMatchingElement(<span>{3}</span>)
                ).toBe(false));

            it('should not output the warning list if details hidden', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningProps} canManageWarnings={false} showDetails={false} />
                    ).containsMatchingElement(
                        <ul>
                            <li>{'The first'}</li>
                            <li>{'The second'}</li>
                            <li>{'The third'}</li>
                        </ul>
                    )
                ).toBe(false));

            it('should not count warnings that are pending or rejected', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningWithAuthorisationProps} canManageWarnings={false} />
                    ).containsMatchingElement(<span>{2}</span>)
                ).toBe(true));

            it('should not output warnings that are pending or rejected', () =>
                expect(
                    shallow(
                        <PersonWarnings
                            {...personWarningWithAuthorisationProps}
                            canManageWarnings={false}
                            showDetails={true}
                        />
                    ).containsMatchingElement(
                        <ul>
                            <li>{'The third'}</li>
                            <li>{'The forth'}</li>
                        </ul>
                    )
                ).toBe(true));

            it('should not output warnings if there is no Authorisation and there is no warnings to show', () =>
                expect(
                    shallow(<PersonWarnings {...personNoWarningProps} canManageWarnings={false} showDetails={false} />)
                        .find('div')
                        .children()
                ).toHaveLength(0));

            it('should output 1 warning if there is no Authorisation and there is warnings to show', () =>
                expect(
                    shallow(<PersonWarnings {...personWarningProps} canManageWarnings={false} showDetails={false} />)
                        .find('div')
                        .children()
                ).toHaveLength(1));

            it('should not output warnings-list if there is no Authorisation and there is warnings to show', () =>
                expect(
                    shallow(
                        <PersonWarnings {...personWarningProps} canManageWarnings={false} showDetails={false} />
                    ).find('warnings-list')
                ).toHaveLength(0));
        });

        describe('Related person warnings', () => {
            const personWarningWithRelatedProps = {
                ...warningProps,
                subject: {
                    numberOfRelatedPersonWarnings: 9,
                    personWarnings: []
                }
            };

            it('should show the correct number of warnings', () =>
                expect(
                    shallow(<PersonWarnings {...personWarningWithRelatedProps} canManageWarnings={false} />)
                        .find(WarningIcon)
                        .props()
                ).toHaveProperty('title', personWarningWithRelatedProps.labels.related));

            it('should output 1 warning if there is no Authorisation and there is no warnings to show and there is relatedPersonWarnings', () =>
                expect(
                    shallow(
                        <PersonWarnings
                            {...personWarningWithRelatedProps}
                            canManageWarnings={false}
                            showDetails={false}
                        />
                    )
                        .find('div')
                        .children()
                ).toHaveLength(1));

            it('should not output 1 warnings-list if there is no Authorisation and there is no warnings to show and there is relatedPersonWarnings', () =>
                expect(
                    shallow(
                        <PersonWarnings
                            {...personWarningWithRelatedProps}
                            canManageWarnings={false}
                            showDetails={false}
                        />
                    ).find('warnings-list')
                ).toHaveLength(0));
        });

        describe('Related organisation warnings', () => {
            const organisationWarningWithRelatedProps = {
                ...warningProps,
                subject: {
                    numberOfRelatedPersonsWithWarnings: 9,
                    warningPersonNameAndIdList: [
                        {
                            objectVersion: 0,
                            name: 'Adam Towner',
                            id: 1,
                            personIdentifier: 'PER1',
                            _type: 'PersonSummaryVO'
                        },
                        {
                            objectVersion: 0,
                            name: 'Adam Smith',
                            id: 2,
                            personIdentifier: 'PER2',
                            _type: 'PersonSummaryVO'
                        }
                    ]
                }
            };

            it('should show the correct number of active persons with warnings', () =>
                expect(
                    shallow(<OrganisationWarnings {...organisationWarningWithRelatedProps} />).containsMatchingElement(
                        <span>{2}</span>
                    )
                ).toBe(true));

            it('should output each person that contains warnings', () =>
                expect(
                    shallow(<OrganisationWarnings {...organisationWarningWithRelatedProps} />).containsMatchingElement(
                        <ul>
                            <li>Adam Towner PER1</li>
                            <li>Adam Smith PER2</li>
                        </ul>
                    )
                ).toBe(true));
        });
    });
});
