'use strict';
import React from 'react';
import NamedPerson from '../../../src/header/jsx/expander/person/full/namedPerson';
import PropertyValue from '../../../src/header/jsx/properties/propertyValue';
import PropertyLabel from '../../../src/header/jsx/properties/propertyLabel';
import { shallow } from 'enzyme';

describe('Named Person (Full)', () => {
    describe('Named Person wrapper', () => {
        const emptyProps = {
            labels: {}
        };

        it('should output no named person when no named persons are supplied.', () => {
            const wrapper = shallow(<NamedPerson {...emptyProps} personPersonRelationships={[]} />).dive();
            expect(wrapper.equals(<div />)).toBe(true);
        });

        it('should output a named person when exactly one qualifying record is supplied with a default label.', () => {
            const wrapper = shallow(
                <NamedPerson
                    {...emptyProps}
                    personPersonRelationships={[
                        {
                            id: 10,
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte',
                            contacts: [
                                {
                                    _type: 'EmailContact',
                                    contactType: 'WORK',
                                    address: 'henry.green@erewhon.com'
                                },
                                {
                                    _type: 'TelephoneContact',
                                    contactType: 'WORK',
                                    number: '011672'
                                }
                            ]
                        }
                    ]}
                />
            ).dive();

            expect(wrapper.containsMatchingElement(<PropertyLabel label="named person" />)).toBe(true);
            const propertyValues = wrapper.find(PropertyValue);
            expect(propertyValues).toHaveLength(4);
            expect(propertyValues.at(0).equals(<PropertyValue value="Henry Green" />)).toBe(true);
            expect(propertyValues.at(1).equals(<PropertyValue value="Acolyte" />)).toBe(true);
            expect(propertyValues.at(2).equals(<PropertyValue value={<a href="tel:011672">011672</a>} />)).toBe(true);
            expect(
                propertyValues
                    .at(3)
                    .equals(
                        <PropertyValue value={<a href="mailto:henry.green@erewhon.com">henry.green@erewhon.com</a>} />
                    )
            ).toBe(true);
        });

        it('should output a named person when exactly one qualifying record is supplied with a label.', () => {
            const wrapper = shallow(
                <NamedPerson
                    {...emptyProps}
                    personPersonRelationships={[
                        {
                            id: 10,
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte',
                            contacts: [
                                {
                                    _type: 'EmailContact',
                                    contactType: 'WORK',
                                    address: 'henry.green@erewhon.com'
                                },
                                {
                                    _type: 'TelephoneContact',
                                    contactType: 'WORK',
                                    number: '011672'
                                }
                            ]
                        }
                    ]}
                    labels={{ namedPerson: 'New Label' }}
                />
            ).dive();

            expect(wrapper.containsMatchingElement(<PropertyLabel label="New Label" />)).toBe(true);
            const propertyValues = wrapper.find(PropertyValue);
            expect(propertyValues).toHaveLength(4);
            expect(propertyValues.at(0).equals(<PropertyValue value="Henry Green" />)).toBe(true);
            expect(propertyValues.at(1).equals(<PropertyValue value="Acolyte" />)).toBe(true);
            expect(propertyValues.at(2).equals(<PropertyValue value={<a href="tel:011672">011672</a>} />)).toBe(true);
            expect(
                propertyValues
                    .at(3)
                    .equals(
                        <PropertyValue value={<a href="mailto:henry.green@erewhon.com">henry.green@erewhon.com</a>} />
                    )
            ).toBe(true);
        });

        it('should output the correctly selected named persons when muliple records are supplied with a label.', () => {
            const wrapper = shallow(
                <NamedPerson
                    {...emptyProps}
                    labels={{ namedPerson: 'New Label' }}
                    personPersonRelationships={[
                        {
                            id: 11,
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 101,
                            roleBPersonId: 100,
                            name: 'Henry Green',
                            relationship: 'Doppelganger'
                        },
                        {
                            id: 12,
                            typeDiscriminator: 'FamilialRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Gopher'
                        },
                        {
                            id: 13,
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte',
                            contacts: [
                                {
                                    _type: 'EmailContact',
                                    contactType: 'HOME',
                                    address: 'henry.green@erewhon.com'
                                },
                                {
                                    _type: 'TelephoneContact',
                                    contactType: 'WORK',
                                    number: '011672'
                                }
                            ]
                        },
                        {
                            id: 14,
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 105,
                            roleAPersonId: 105,
                            roleBPersonId: 101,
                            name: 'James Fenton',
                            relationship: 'Factotum',
                            contacts: [
                                {
                                    _type: 'EmailContact',
                                    contactType: 'WORK',
                                    address: 'james.fenton@erewhon.com'
                                },
                                {
                                    _type: 'TelephoneContact',
                                    contactType: 'HOME',
                                    number: '011572'
                                }
                            ]
                        }
                    ]}
                />
            ).dive();

            expect(wrapper.containsMatchingElement(<PropertyLabel label="New Label" />)).toBe(true);
            expect(wrapper.containsMatchingElement(<PropertyLabel label="New Label" />)).toBe(true);
            const propertyValues = wrapper.find(PropertyValue);
            expect(propertyValues).toHaveLength(8);
            expect(propertyValues.at(0).equals(<PropertyValue value="Henry Green" />)).toBe(true);
            expect(propertyValues.at(1).equals(<PropertyValue value="Acolyte" />)).toBe(true);
            expect(propertyValues.at(2).equals(<PropertyValue value={<a href="tel:011672">011672</a>} />)).toBe(true);
            expect(propertyValues.at(3).equals(<PropertyValue value={false} />)).toBe(true);
            expect(propertyValues.at(4).equals(<PropertyValue value="James Fenton" />)).toBe(true);
            expect(propertyValues.at(5).equals(<PropertyValue value="Factotum" />)).toBe(true);
            expect(propertyValues.at(6).equals(<PropertyValue value={false} />)).toBe(true);
            expect(
                propertyValues
                    .at(7)
                    .equals(
                        <PropertyValue value={<a href="mailto:james.fenton@erewhon.com">james.fenton@erewhon.com</a>} />
                    )
            ).toBe(true);
        });
    });
});
