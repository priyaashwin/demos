'use strict';
import React from 'react';
import AllocatedWorker from '../../../src/header/jsx/expander/person/full/allocatedWorker';
import PropertyValue from '../../../src/header/jsx/properties/propertyValue';
import PropertyLabel from '../../../src/header/jsx/properties/propertyLabel';
import MapLink from '../../../src/link/link';
import { shallow } from 'enzyme';

describe('Allocated Worker (Full)', () => {
    describe('Allocated Worker wrapper', () => {
        const emptyProps = {
            labels: {},
            mapLinkUrl: 'erewhon.com'
        };

        it('should output no allocations when no allocated workers or teams are supplied.', () => {
            const wrapper = shallow(
                <AllocatedWorker {...emptyProps} personPersonRelationships={[]} allocatedTeams={[]} />
            ).dive();
            expect(wrapper.equals(<div />)).toBe(true);
        });

        it('should output allocated teams when no allocated workers are supplied but teams are.', () => {
            const wrapper = shallow(
                <AllocatedWorker
                    {...emptyProps}
                    personPersonRelationships={[]}
                    allocatedTeams={[
                        {
                            id: 1,
                            organisationVO: {
                                name: 'SMERSH'
                            },
                            address: '23 Wibble Way'
                        },
                        {
                            id: 2,
                            organisationVO: {
                                name: 'UNIT'
                            }
                        }
                    ]}
                />
            ).dive();

            expect(wrapper.containsMatchingElement(<PropertyLabel label="allocated team" />)).toBe(true);
            const propertyValues = wrapper.find(PropertyValue);
            expect(propertyValues).toHaveLength(3);
            expect(propertyValues.at(0).equals(<PropertyValue value="SMERSH" />)).toBe(true);
            expect(
                propertyValues.at(1).equals(
                    <PropertyValue
                        value={
                            <MapLink url="erewhon.com" term="23 Wibble Way" target="_blank">
                                23 Wibble Way
                            </MapLink>
                        }
                    />
                )
            ).toBe(true);
            expect(propertyValues.at(2).equals(<PropertyValue value="UNIT" />)).toBe(true);
        });

        it('should output an allocated worker when exactly one qualifying worker is supplied, as well as the teams.', () => {
            const wrapper = shallow(
                <AllocatedWorker
                    {...emptyProps}
                    personPersonRelationships={[
                        {
                            id: 10,
                            typeDiscriminator: 'ProfessionalRelationshipType',
                            personId: 100,
                            roleAPersonId: 100,
                            roleBPersonId: 101,
                            name: 'Henry Green',
                            relationship: 'Acolyte',
                            contacts: [
                                {
                                    _type: 'EmailContact',
                                    contactType: 'WORK',
                                    address: 'henry.green@erewhon.com'
                                },
                                {
                                    _type: 'TelephoneContact',
                                    contactType: 'WORK',
                                    number: '011672'
                                }
                            ]
                        }
                    ]}
                    allocatedTeams={[
                        {
                            id: 1,
                            organisationVO: {
                                name: 'SMERSH'
                            },
                            address: '23 Wibble Way'
                        },
                        {
                            id: 2,
                            organisationVO: {
                                name: 'UNIT'
                            }
                        }
                    ]}
                    labels={{
                        allocatedTeam: 'Frederico',
                        allocatedWorker: 'Fellini'
                    }}
                />
            ).dive();

            expect(wrapper.containsMatchingElement(<PropertyLabel label="Fellini" />)).toBe(true);
            expect(wrapper.containsMatchingElement(<PropertyLabel label="Frederico" />)).toBe(true);
            const propertyValues = wrapper.find(PropertyValue);
            expect(propertyValues).toHaveLength(7);
            expect(propertyValues.at(0).equals(<PropertyValue value="Henry Green" />)).toBe(true);
            expect(propertyValues.at(1).equals(<PropertyValue value="Acolyte" />)).toBe(true);
            expect(propertyValues.at(2).equals(<PropertyValue value={<a href="tel:011672">011672</a>} />)).toBe(true);
            expect(
                propertyValues
                    .at(3)
                    .equals(
                        <PropertyValue value={<a href="mailto:henry.green@erewhon.com">henry.green@erewhon.com</a>} />
                    )
            ).toBe(true);
            expect(propertyValues.at(4).equals(<PropertyValue value="SMERSH" />)).toBe(true);
            expect(
                propertyValues.at(5).equals(
                    <PropertyValue
                        value={
                            <MapLink url="erewhon.com" term="23 Wibble Way" target="_blank">
                                23 Wibble Way
                            </MapLink>
                        }
                    />
                )
            ).toBe(true);
            expect(propertyValues.at(6).equals(<PropertyValue value="UNIT" />)).toBe(true);
        });
    });
});
