'use strict';
import React from 'react';
import Alerts from '../../../src/header/jsx/alerts/alerts';
import PersonAlerts from '../../../src/header/jsx/alerts/person/alerts';
import { shallow } from 'enzyme';

describe('Alerts', () => {
    describe('Alerts wrapper', () => {
        const emptyProps = {
            labels: {},
            subject: {},
            canManageAlerts: false,
            category: {},
            onClick: () => {}
        };

        it('should output alerts for a person', () =>
            expect(shallow(<Alerts {...emptyProps} type="person" />).find(PersonAlerts)).toHaveLength(1));

        it('should not output alerts for a group', () =>
            expect(shallow(<Alerts {...emptyProps} type="group" />).containsMatchingElement(<div />)).toBe(true));
    });

    describe('Person alerts', () => {
        const alertProps = {
            labels: {
                manage: 'MANAGE',
                view: 'VIEW'
            },
            category: {
                codedEntries: {
                    FOO: { name: 'The foo' },
                    BAR: { name: 'The bar' }
                }
            },
            onClick: () => {}
        };

        describe('Subject alerts', () => {
            const personAlertProps = {
                ...alertProps,
                subject: {
                    personAlerts: [
                        {
                            id: 1,
                            personId: 1,
                            alertCategory: 'FOO',
                            notes: [
                                {
                                    content: 'foo'
                                }
                            ]
                        },
                        {
                            id: 2,
                            personId: 1,
                            alertCategory: 'BAR',
                            notes: [
                                {
                                    content: 'bar'
                                }
                            ]
                        }
                    ]
                }
            };

            it('should use the manage alerts label as the title if the user has permissions', () =>
                expect(
                    shallow(<PersonAlerts {...personAlertProps} canManageAlerts={true} />).containsMatchingElement(
                        <div>{'Click the icon to MANAGE'}</div>
                    )
                ).toBe(true));

            it('should use the view alerts label as the title if the user has permissions', () =>
                expect(
                    shallow(<PersonAlerts {...personAlertProps} canManageAlerts={false} />).containsMatchingElement(
                        <div>{'Click the icon to VIEW'}</div>
                    )
                ).toBe(true));

            it('should show the correct number of alerts', () =>
                expect(
                    shallow(<PersonAlerts {...personAlertProps} canManageAlerts={false} />).containsMatchingElement(
                        <span>{2}</span>
                    )
                ).toBe(true));

            it('should output the alert', () =>
                expect(
                    shallow(<PersonAlerts {...personAlertProps} canManageAlerts={false} />).containsMatchingElement(
                        <ul>
                            <li className="alert-info">
                                <div>
                                    <div>{'The foo'}</div>
                                    <div>{'foo'}</div>
                                </div>
                            </li>
                            <li className="alert-info">
                                <div>
                                    <div>{'The bar'}</div>
                                    <div>{'bar'}</div>
                                </div>
                            </li>
                        </ul>
                    )
                ).toBe(true));
        });
    });
});
