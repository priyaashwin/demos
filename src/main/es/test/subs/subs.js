'use strict';

import { subs } from '../../src/subs/subs';

describe('Subs', () => {
    describe('Simple string substitution', () => {
        const str1 = 'The quick {colour} {animal} jumped over the...';

        it('should substitute all parameters', () => {
            const expected = 'The quick brown fox jumped over the...';
            const testString = subs(str1, { colour: 'brown', animal: 'fox' });

            // The parameters were substituted
            expect(testString).toBe(expected);
        });

        it('should substitute only one parameter', () => {
            const expected = 'The quick brown {animal} jumped over the...';
            const testString = subs(str1, { colour: 'brown' });

            // One parameter was substituted
            expect(testString).toBe(expected);
        });

        it('should leave all unmatched placeholders untouched', () =>
            // No parameters substituted
            expect(subs(str1, {})).toBe(str1));
    });

    describe('Substitution using objects', () => {
        const str1 = 'The {test.animal} said {catchphrase}';

        it('should substitute using nested object', () => {
            const expected = 'The cat said boo';

            const testString = subs(str1, {
                test: {
                    animal: 'cat'
                },
                catchphrase: 'boo'
            });

            // Nested object used in substitution
            expect(testString).toBe(expected);
        });
    });
});
