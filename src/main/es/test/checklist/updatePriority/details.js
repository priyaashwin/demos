'use strict';
import React from 'react';
import { render } from 'enzyme';
import Details from '../../../src/checklist/updatePriority/jsx/details';
import { ChecklistForUpdatePriority } from '../../../src/checklist/checklist';

describe('updatePriority details', () => {
    const enumeration = {
        values: [
            {
                enumName: 'LOW',
                enumValue: 'LOW',
                name: 'Low',
                numericValue: 1
            },
            {
                enumName: 'MEDIUM',
                enumValue: 'MEDIUM',
                name: 'Medium',
                numericValue: 2
            },
            {
                enumName: 'HIGH',
                enumValue: 'HIGH',
                name: 'High',
                numericValue: 3
            }
        ]
    };

    const checklist = {
        id: 1,
        priority: 'LOW',
        objectVersion: 0
    };

    const checklistForUpdatePriority = new ChecklistForUpdatePriority(checklist);

    const labels = {
        priority: 'Priority'
    };

    describe('dialog content', () => {
        const wrapper = render(
            <Details
                enumeration={enumeration}
                checklist={checklistForUpdatePriority}
                onUpdate={() => {}}
                labels={labels}
            />
        );
        const content = wrapper.find('.l-box div');

        it('should have 2 elements', () => expect(content.children().length).toBe(2));
        it('should have a label', () => expect(content.find('label').text()).toBe('Priority *'));
        it('should have a select with 3 options and please choose', () =>
            expect(content.find('select').children().length).toBe(4));
    });
});
