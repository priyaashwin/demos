'use strict';
import React from 'react';
import Subject from '../../src/subject/subject';
import { shallow } from 'enzyme';

describe('Subject', () => {
    describe('Subject properties output given an empty subject', () => {
        it('Outputs name', () =>
            expect(
                shallow(<Subject name="testName" subjectType="person" />).containsMatchingElement(
                    <span className="subject-name">testName</span>
                )
            ).toBe(true));

        it('Outputs the identifier', () =>
            expect(
                shallow(
                    <Subject name="testName" identifier="testIdentifier" subjectType="person" />
                ).containsMatchingElement(<span className="subject-identifier">(testIdentifier)</span>)
            ).toBe(true));

        it('Does not output the identifier', () =>
            expect(
                shallow(<Subject name="testName" subjectType="person" />).containsMatchingElement(
                    <span className="subject-identifier">{''}</span>
                )
            ).toBe(true));
    });

    describe('Subject propeties output given a subject object', () => {
        describe('Person subject', () => {
            const subject = {
                personIdentifier: 'PER123',
                name: 'PERNAME'
            };

            it('Outputs name from subject', () =>
                expect(
                    shallow(<Subject name="testName" subject={subject} subjectType="person" />).containsMatchingElement(
                        <span className="subject-name">PERNAME</span>
                    )
                ).toBe(true));

            it('Outputs identifier from subject', () =>
                expect(
                    shallow(<Subject name="testName" subject={subject} subjectType="person" />).containsMatchingElement(
                        <span className="subject-identifier">(PER123)</span>
                    )
                ).toBe(true));
        });

        describe('Group subject', () => {
            const subject = {
                groupIdentifier: 'GRP123',
                name: 'GROUPNAME'
            };

            it('Outputs name from subject', () =>
                expect(
                    shallow(<Subject name="testName" subject={subject} subjectType="group" />).containsMatchingElement(
                        <span className="subject-name">GROUPNAME</span>
                    )
                ).toBe(true));

            it('Outputs identifier from subject', () =>
                expect(
                    shallow(<Subject name="testName" subject={subject} subjectType="group" />).containsMatchingElement(
                        <span className="subject-identifier">(GRP123)</span>
                    )
                ).toBe(true));
        });

        describe('Falls back to properties', () => {
            it('Outputs name from property', () =>
                expect(
                    shallow(
                        <Subject name="testName" subject={{ name: null }} subjectType="group" />
                    ).containsMatchingElement(<span className="subject-name">testName</span>)
                ).toBe(true));

            it('Outputs identifier from property', () =>
                expect(
                    shallow(
                        <Subject name="testName" subject={{ identifier: null }} identifier="Foo" subjectType="group" />
                    ).containsMatchingElement(<span className="subject-identifier">(Foo)</span>)
                ).toBe(true));
        });
    });
});
