'use strict';
import React from 'react';
import PersonRelationshipIcons from '../../../src/subjectSummary/jsx/relationships/personPersonRelationship/icons';
import OrganisationRelationshipIcons from '../../../src/subjectSummary/jsx/relationships/personOrganisationRelationship/icons';
import Icon from '../../../src/subjectSummary/jsx/relationships/icon';
import { shallow } from 'enzyme';

describe('Relationship icons', () => {
    const labels = {};

    const allowedAttributes = [
        'ALLOCATED_WORKER',
        'NAMED_PERSON',
        'PRIMARY_WORKER',
        'KEY_WORKER',
        'LEAD_PROFESSIONAL',
        'FINANCIAL_REPRESENTATIVE',
        'CARE_MANAGER',
        'PARENTAL_RESPONSIBILITY',
        'RESPONSIBLE_LOCAL_AUTHORITY',
        'NEXT_OF_KIN',
        'MAIN_CARER',
        'NON_CONTACT',
        'RESTRICTED_CONTACT',
        'EMERGENCY_CONTACT',
        'DWP_APPOINTEE'
    ];

    describe('allocated worker', () => {
        const relation = {
            allocatedWorker: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="aw" />)
            ).toBe(true));
    });

    describe('primary worker', () => {
        const relation = {
            primaryWorker: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="pw" />)
            ).toBe(true));
    });

    describe('key worker', () => {
        const relation = {
            keyWorker: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="kw" />)
            ).toBe(true));
    });

    describe('lead professional', () => {
        const relation = {
            leadProfessional: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="lp" />)
            ).toBe(true));
    });

    describe('financial representative', () => {
        const relation = {
            financialRepresentative: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="fr" />)
            ).toBe(true));
    });

    describe('care manager', () => {
        const relation = {
            careManager: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="cm" />)
            ).toBe(true));
    });

    describe('parental responsibility', () => {
        describe('person person relationship parental responsibility', () => {
            const relation = {
                parentalResponsibility: 'ROLE_A',
                roleAPersonId: 1,
                roleBPersonId: 2
            };

            it('outputs icon', () =>
                expect(
                    shallow(
                        <PersonRelationshipIcons
                            relation={relation}
                            subjectId={2}
                            labels={labels}
                            allowedAttributes={allowedAttributes}
                        />
                    ).containsMatchingElement(<Icon type="pr" />)
                ).toBe(true));
        });

        describe('person organisation relationship parental responsibility', () => {
            const relation = {
                parentalResponsibility: 'ROLE_B',
                personVO: {
                    id: 2
                }
            };

            it('outputs icon', () =>
                expect(
                    shallow(
                        <OrganisationRelationshipIcons
                            relation={relation}
                            subjectId={2}
                            labels={labels}
                            allowedAttributes={allowedAttributes}
                        />
                    ).containsMatchingElement(<Icon type="pr" />)
                ).toBe(true));
        });
    });

    describe('next of kin', () => {
        const relation = {
            nextOfKin: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="nok" />)
            ).toBe(true));
    });

    describe('main carer', () => {
        const relation = {
            mainCarer: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="c" />)
            ).toBe(true));
    });

    describe('non contact', () => {
        const relation = {
            nonContact: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="nc" />)
            ).toBe(true));
    });

    describe('restricted contact', () => {
        const relation = {
            restrictedContact: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="rc" />)
            ).toBe(true));
    });

    describe('responsible local authority', () => {
        const relation = {
            responsibleLocalAuthority: 'ROLE_B',
            personVO: {
                id: 2
            }
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <OrganisationRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="rla" />)
            ).toBe(true));
    });

    describe('emergency contact', () => {
        const relation = {
            emergencyContact: 'ROLE_A',
            roleAPersonId: 1,
            roleBPersonId: 2
        };

        it('outputs icon', () =>
            expect(
                shallow(
                    <PersonRelationshipIcons
                        relation={relation}
                        subjectId={2}
                        labels={labels}
                        allowedAttributes={allowedAttributes}
                    />
                ).containsMatchingElement(<Icon type="ec" />)
            ).toBe(true));
    });
});
