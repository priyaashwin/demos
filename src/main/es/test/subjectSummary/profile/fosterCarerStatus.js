'use strict';
import React from 'react';
import FosterCarerApproval from '../../../src/subjectSummary/jsx/profile/approvals/fosterCarerApproval';
import InfoPop from '../../../src/infopop/infopop';
import { formatDate } from '../../../src/date/date';
import { statusClass } from '../../../src/subjectSummary/jsx/profile/approvals/approvalStatus';
import { shallow } from 'enzyme';

describe('Subject Summary Profile', () => {
    describe('Unapproved foster carer', () => {
        const subject = {
            subjectType: 'PERSON',
            subjectId: 9999
        };
        const config = {
            subject: subject,
            fosterCarerRegistrationUrl: '/blah/blah',
            approvalSummary: {
                status: 'ex_approved',
                currentApprovals: [],
                currentSuspensions: [],
                currentWithdrawals: []
            },
            labels: {
                fosterCarerApproval: {
                    title: 'Fostering:',
                    details: {
                        title: 'Foster Carer Details',
                        approvalsTitle: 'Current Approvals',
                        suspensionsTitle: 'Current Availability',
                        empty: 'This is just a test',
                        ex_registration: {
                            status: 'ex_registered',
                            title: 'Deregistered: '
                        }
                    }
                }
            }
        };

        it('hover panel should contain the unapproved approval-info message', () => {
            const wrapper = shallow(<FosterCarerApproval {...config} />, { disableLifecycleMethods: true });
            expect(wrapper.containsMatchingElement(<div className="approval-info">{'This is just a test'}</div>)).toBe(
                true
            );
            expect(wrapper.find(InfoPop).hasClass(statusClass(config.approvalSummary.status))).toBe(true);
        });
    });
    describe('Deregistered foster carer', () => {
        const subject = {
            subjectType: 'PERSON',
            subjectId: 9999
        };
        const config = {
            subject: subject,
            fosterCarerRegistrationUrl: '/blah/blah',
            approvalSummary: {
                status: 'ex_approved',
                currentApprovals: [],
                currentSuspensions: [],
                currentWithdrawals: []
            },
            isNamedPersonRelationshipAvailable: false,
            labels: {
                fosterCarerApproval: {
                    title: 'Fostering:',
                    details: {
                        title: 'Foster Carer Details',
                        approvalsTitle: 'Current Approvals',
                        suspensionsTitle: 'Current Availability',
                        empty: 'This is just a test',
                        ex_registration: {
                            status: 'ex_registered',
                            title: 'Deregistered: '
                        }
                    }
                }
            }
        };

        it('hover panel should contain the deregistered approval-info message', () => {
            const wrapper = shallow(<FosterCarerApproval {...config} />, { disableLifecycleMethods: true });
            const ex_registration = config.labels.fosterCarerApproval.details.ex_registration;
            const endDate = new Date();
            const deRegSummary = ex_registration.title + formatDate(endDate, true);
            wrapper.setState({
                deRegistrationStatus: ex_registration.status,
                deRegistrationSummary: deRegSummary
            });
            expect(wrapper.containsMatchingElement(<div className="approval-info">{deRegSummary}</div>)).toBe(true);
            expect(wrapper.find(InfoPop).hasClass(statusClass(ex_registration.status))).toBe(true);
        });
    });
});
