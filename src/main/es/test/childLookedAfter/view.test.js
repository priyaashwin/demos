'use strict';
import React from 'react';
import { mount } from 'enzyme';
import PeriodsOfCareView from '../../src/childlookedafter/jsx/periodsOfCare/view';
import props from '../setUp/childLookedAfter/viewTestSetUp';

const { labels, periodsOfCare, migratedPeriodsOfCare, subject, permissions, codedEntries, enums } = {
    labels: props.LABELS,
    periodsOfCare: props.PERIODS_OF_CARE,
    migratedPeriodsOfCare: props.MIGRATED_PERIODS_OF_CARE,
    subject: props.SUBJECT,
    permissions: props.PERMISSIONS,
    codedEntries: props.CODED_ENTRIES,
    enums: props.ENUMS
}

describe('The episodes of care placements are rendered correctly', () => {
    let wrapper;

    // Dates are indexed from one, as first item contains the
    // text 'start date'
    const expectedStartDates = [
        [1, new Date("January 27, 2020")],
        [2, new Date("January 16, 2020")],
        [3, new Date("January 16, 2020")],
        [4, new Date("January 08, 2020")],
        [5, new Date("January 02, 2020")],
        [6, new Date("January 01, 2020")],
        [7, new Date("January 02, 2019")]
    ];

    beforeEach(() => {
        wrapper = mount( <
            PeriodsOfCareView errors = { null } labels = { labels } loading = { false } periodsOfCare = { periodsOfCare } migratedPeriodsOfCare = { migratedPeriodsOfCare } subject = { subject } permissions = { permissions } codedEntries = { codedEntries } enums = { enums }
            />
        );
    })

    afterEach(() => {
        wrapper.unmount();
    })

    it('renders the correct number of absences rows', () => {
        const absenceRows = wrapper.find('.cla-table-absences-details');

        // A single period of care with two associated absences is contained in the props
        expect(absenceRows).toHaveLength(2);
    });

    it('renders the correct number of main/temporary rows', () => {
        const placementRows = wrapper.find('.details-row');

        // Two episodes of care with five associated placements are contained in the props
        expect(placementRows).toHaveLength(5);
    });

    it.each(expectedStartDates)('placement start date at index %i is %p', (i, expected) => {
        const placementStartDateColumn = wrapper.find('.cla-table-col-startDate').at(i);

        const placementStartDate = new Date(placementStartDateColumn.text());

        expect(placementStartDate).toStrictEqual(expected);
    });

    it('allows only the most recent MAIN episode to be deleted', () => {
        const episodeOfCareRows = wrapper.find('EpisodeOfCareTable').find('EpisodeOfCareDetailsRow');

        // Find the rows in the episode of care table that have an active delete button
        const deleteActiveRows = episodeOfCareRows
            .filterWhere(row => row.props().isDeleteActive);
        
        // There is only one row with an active delete button
        expect(deleteActiveRows).toHaveLength(1);

        const deleteActiveId = deleteActiveRows.props().record.placement.id;
        // Its placement id is the the id of the placement with the most recent start state
        expect(deleteActiveId).toBe(70);
    });
});