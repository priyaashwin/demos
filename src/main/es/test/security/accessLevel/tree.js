'use strict';
import React from 'react';
import AccessLevelTree from '../../../src/security/jsx/accessLevel/recordType/tree';
import TreeView from 'usp-simple-tree';
import { shallow, mount } from 'enzyme';
import { Map } from 'immutable';

describe('Access Level Tree', () => {
    const accessLevels = [
        {
            key: 'NONE',
            label: 'LABEL A',
            value: 'NONE',
            colour: 'black',
            fontColour: 'white'
        },
        {
            key: 'WRITE',
            label: 'LABEL B',
            value: 'WRITE',
            colour: 'white',
            fontColour: 'black'
        },
        {
            key: 'ADMIN',
            label: 'LABEL C',
            value: 'ADMIN',
            colour: 'green',
            fontColour: 'red'
        }
    ];

    describe('Render simple tree', () => {
        let wrapper;

        const securedRecordTypes = [
            {
                type: 'typeA',
                securityDomains: [],
                supportsSecurityDomains: false
            },
            {
                type: 'typeB',
                securityDomains: [],
                supportsSecurityDomains: false
            }
        ];

        const explicitAccessLevels = Map([['typeA', Map([['declaredAccessLevel', 'WRITE']])]]);

        beforeEach(() => {
            wrapper = mount(
                <AccessLevelTree
                    accessLevels={accessLevels}
                    securedRecordTypes={securedRecordTypes}
                    explicitAccessLevels={explicitAccessLevels}
                    securityAccessElements={Map()}
                    loadSecurityAccessElements={() => {}}
                    updateSecurityAccessElementLevel={() => {}}
                />
            );
        });

        it('should render the top level of the tree expanded', () => {
            expect(
                wrapper
                    .find(TreeView)
                    .first()
                    .props().open
            ).toBe(true);
        });

        it('should destinguish explicit access levels', () => {
            const root = wrapper.find('ul').first();

            expect(root.find('.declared')).toHaveLength(1);
            expect(
                root.find('.declared').containsMatchingElement(
                    <div className="content-markup">
                        <span>{'LABEL B set at this level'}</span>
                    </div>
                )
            ).toBe(true);
        });
    });
});
