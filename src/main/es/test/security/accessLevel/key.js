'use strict';
import React from 'react';
import Key from '../../../src/security/jsx/accessLevel/key/key';
import { shallow } from 'enzyme';

describe('Access Level Key', () => {
    const accessLevels = [
        {
            key: 'A',
            label: 'LABEL A',
            value: 'NONE',
            colour: 'black',
            fontColour: 'white'
        },
        {
            key: 'B',
            label: 'LABEL B',
            value: 'WRITE',
            colour: 'white',
            fontColour: 'black'
        },
        {
            key: 'C',
            label: 'LABEL C',
            value: 'ADMIN',
            colour: 'green',
            fontColour: 'red'
        }
    ];

    describe('Render access levels', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = shallow(<Key accessLevels={accessLevels} />);
        });

        it('should output the correct number of access levels', () => expect(wrapper.find('li')).toHaveLength(3));

        it('should output access levels', () => {
            expect(
                wrapper.containsAllMatchingElements([
                    <li>
                        <span>NONE</span>
                        <span className="label">LABEL A</span>
                    </li>,
                    <li>
                        <span>WRITE</span>
                        <span className="label">LABEL B</span>
                    </li>,
                    <li>
                        <span>ADMIN</span>
                        <span className="label">LABEL C</span>
                    </li>
                ])
            ).toBe(true);
        });
    });
});
