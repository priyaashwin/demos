'use strict';
import React from 'react';
import AccessLevelControl from '../../../src/security/jsx/accessLevel/recordType/accessLevelControl';
import { shallow, mount } from 'enzyme';

describe('Access Level Control', () => {
    const accessLevels = [
        {
            key: 'A',
            label: 'LABEL A',
            value: 'NONE',
            colour: 'black',
            fontColour: 'white'
        },
        {
            key: 'B',
            label: 'LABEL B',
            value: 'WRITE',
            colour: 'white',
            fontColour: 'black'
        },
        {
            key: 'C',
            label: 'LABEL C',
            value: 'ADMIN',
            colour: 'green',
            fontColour: 'red'
        }
    ];

    describe('Render access levels', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = shallow(<AccessLevelControl accessLevels={accessLevels} path="" onChangeLevel={() => {}} />);
        });

        it('should output the correct number of access levels', () =>
            expect(wrapper.find('.access-level-value')).toHaveLength(3));

        it('should output access levels', () => {
            expect(
                wrapper.containsAllMatchingElements([
                    <span key="NONE" className="access-level-value" data-value="A">
                        NONE
                    </span>,
                    <span key="WRITE" className="access-level-value" data-value="B">
                        WRITE
                    </span>,
                    <span key="ADMIN" className="access-level-value" data-value="C">
                        ADMIN
                    </span>
                ])
            ).toBe(true);
        });
    });

    describe('Selected access level styles', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = shallow(
                <AccessLevelControl accessLevels={accessLevels} path="" accessLevel="B" onChangeLevel={() => {}} />
            );
        });

        it('should set the stlye and class of the selected item', () => {
            expect(
                wrapper.containsAllMatchingElements([
                    <span
                        key="WRITE"
                        className="access-level-value selected"
                        data-value="B"
                        style={{
                            backgroundColor: 'white',
                            color: 'black'
                        }}>
                        WRITE
                    </span>
                ])
            ).toBe(true);
        });
    });

    describe('Click handler', () => {
        let wrapper;
        let newAccessLevel;
        beforeEach(() => {
            newAccessLevel = undefined;

            const onChange = (path, accessLevel) => (newAccessLevel = accessLevel);
            wrapper = mount(<AccessLevelControl accessLevels={accessLevels} path="" onChangeLevel={onChange} />);
        });

        it('on click should trigger callback', () => {
            wrapper.find('[data-value="B"]').simulate('click');
            expect(newAccessLevel).toBe('B');
        });
    });
});
