'use strict';

import { formatDate, formatDateTime } from '../../src/date/date';

describe('Date', () => {
    describe('formatDate', () => {
        const date = new Date(-2202076800000);
        it('should format a date to usp style', () => expect(formatDate(date, true)).toBe('22-Mar-1900'));

        it('should format a date to non-usp style', () => expect(formatDate(date)).toBe('22nd March 1900'));
    });

    describe('formatDateTime', () => {
        const date = new Date(1555555555555);

        it('should format a date to usp style', () => expect(formatDateTime(date, true)).toBe('18-Apr-2019 03:45'));

        it('should format a date to non-usp style', () => expect(formatDateTime(date)).toBe('18th April 2019 03:45'));
    });

    describe('null date', () => {
        it('should output an empty string', () => expect(formatDate(null)).toBe(''));
    });
});
