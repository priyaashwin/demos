'use strict';

import { formatFuzzyDate } from '../../src/date/fuzzyDate';

describe('FuzzyDate', () => {
    //18-Feb-2008 15:06
    const fuzzyDateTIME = {
        _type: 'FuzzyDate',
        calculatedDate: 1203347160000,
        fuzzyDateMask: 'TIME',
        allowedRange: { upperBoundDate: 1203347160000, lowerBoundDate: 1203347160000 }
    };
    //11-May-2016 13:00
    const fuzzyDateHOUR = {
        _type: 'FuzzyDate',
        calculatedDate: 1462971599000,
        fuzzyDateMask: 'HOUR',
        allowedRange: { upperBoundDate: 1462971599000, lowerBoundDate: 1462968000000 }
    };
    //16-Dec-2015
    const fuzzyDateDAY = {
        _type: 'FuzzyDate',
        calculatedDate: 1450224000000,
        fuzzyDateMask: 'DAY',
        allowedRange: { upperBoundDate: 1450310399000, lowerBoundDate: 1450224000000 }
    };
    //Jun-2015
    const fuzzyDateMONTH = {
        _type: 'FuzzyDate',
        calculatedDate: 1435705199000,
        fuzzyDateMask: 'MONTH',
        allowedRange: { upperBoundDate: 1435705199000, lowerBoundDate: 1433113200000 }
    };
    //2015
    const fuzzyDateYEAR = {
        _type: 'FuzzyDate',
        calculatedDate: 1451606399000,
        fuzzyDateMask: 'YEAR',
        allowedRange: { upperBoundDate: 1451606399000, lowerBoundDate: 1420070400000 }
    };

    describe('formatFuzzyDate', () => {
        it('should format a fuzzy date with TIME mask to usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateTIME, true).date).toBe('18-Feb-2008');
            // Time formated correctly
            expect(formatFuzzyDate(fuzzyDateTIME, true).time).toBe('15:06');
        });

        it('should format a fuzzy date with TIME mask to non-usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateTIME).date).toBe('18th February 2008');
            // Times formated correctly
            expect(formatFuzzyDate(fuzzyDateTIME).time).toBe('15:06');
        });

        it('should format a fuzzy date with HOUR mask to usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateHOUR, true).date).toBe('11-May-2016');
            // Time formated correctly
            expect(formatFuzzyDate(fuzzyDateHOUR, true).time).toBe('13:00');
        });

        it('should format a fuzzy date with HOUR mask to non-usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateHOUR).date).toBe('11th May 2016');
            // Time formated correctly
            expect(formatFuzzyDate(fuzzyDateHOUR).time).toBe('13:00');
        });

        it('should format a fuzzy date with DAY mask to usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateDAY, true).date).toBe('16-Dec-2015');
            // No time portion
            expect(formatFuzzyDate(fuzzyDateDAY, true).time).toBe('');
        });

        it('should format a fuzzy date with DAY mask to non-usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateDAY).date).toBe('16th December 2015');
            // No time portion
            expect(formatFuzzyDate(fuzzyDateDAY).time).toBe('');
        });

        it('should format a fuzzy date with MONTH mask to usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateMONTH, true).date).toBe('Jun-2015');
            // No time portion
            expect(formatFuzzyDate(fuzzyDateMONTH, true).time).toBe('');
        });

        it('should format a fuzzy date with MONTH mask to non-usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateMONTH).date).toBe('June 2015');
            // No time portion
            expect(formatFuzzyDate(fuzzyDateMONTH).time).toBe('');
        });

        it('should format a fuzzy date with YEAR mask to usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateYEAR, true).date).toBe('2015');
            // No time portion
            expect(formatFuzzyDate(fuzzyDateYEAR, true).time).toBe('');
        });

        it('should format a fuzzy date with YEAR mask to non-usp style', () => {
            // Date formated correctly
            expect(formatFuzzyDate(fuzzyDateYEAR).date).toBe('2015');
            // No time portion
            expect(formatFuzzyDate(fuzzyDateYEAR).time).toBe('');
        });
    });
});
