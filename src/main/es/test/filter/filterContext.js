'use strict';

import { getFilterRecord, VALUE_TYPES } from '../../src/filter/filterFactory/filterFactory';
import FilterContext from '../../src/filter/filterFactory/filterContext';

const {
    setFilterValue,
    resetFilters,
    resetAllFilters,
    getFilters
} = FilterContext;

describe('FilterModel', () => {
    const dateOne = '15-Jul-2000';
    const dateOneEpochTime = 963615600000;
    const dateFromValueType = VALUE_TYPES.DATE_FROM;
    const dateFilterOne = getFilterRecord({ valueType: dateFromValueType, initValue: null });

    const errorMessage = 'dateTwo must be later than date one';
    
    const validator = (filters) => {
        const errors = { 
            firstFilter: false, 
            secondFilter: false 
        };

        if (filters.secondFilter.getValue() < filters.firstFilter.getValue()) {
            errors.secondFilter = errorMessage;
        }
        
        return errors;
    }

    const dateTwo = '22-Mar-2020';
    const dateTwoEpochTime = 1584835200000;
    const dateFilterTwo = getFilterRecord({ valueType: dateFromValueType, initValue: null, validator });

    const string = 'stringValue';
    const stringValueType = VALUE_TYPES.TEXT;
    const stringFilter = getFilterRecord({ valueType: stringValueType, initValue: '' });

    let filters = {
        firstFilter: dateFilterOne,
        secondFilter: dateFilterTwo,
        thirdFilter: stringFilter
    };
    let errors = {};

    ({ filters, errors } = setFilterValue(filters, errors, 'firstFilter', dateOne));
    ({ filters, errors } = setFilterValue(filters, errors, 'secondFilter', dateTwo));
    ({ filters, errors } = setFilterValue(filters, errors, 'thirdFilter', string));

    describe('filters are retrieved from the filter model correctly', () => {
        const updatedFilters = getFilters(filters, errors, false);

        it('should contain the first filter', () => expect(updatedFilters).toHaveProperty('firstFilter'));
        it('should contain the second filter', () => expect(updatedFilters).toHaveProperty('secondFilter'));
        it('should contain the third filter', () => expect(updatedFilters).toHaveProperty('thirdFilter'));
    });

    describe('values are retrieved from the filter model correctly', () => {
        const updatedFilters = getFilters(filters, errors, false);

        const firstFilterValue = updatedFilters.firstFilter.getValue();
        const secondFilterValue = updatedFilters.secondFilter.getValue();
        const thirdFilterValue = updatedFilters.thirdFilter.getValue();

        it('should contain the correct value for first filter', () => expect(firstFilterValue).toBe(dateOneEpochTime));
        it('should contain the correct value for second filter', () => expect(secondFilterValue).toBe(dateTwoEpochTime));
        it('should contain the correct value for third filter', () => expect(thirdFilterValue).toBe(string));
    });

    describe('filters are reset correctly', () => {
        const { filters: updatedFilters, errors: updatedErrors } = resetFilters(filters, errors, 'firstFilter', 'thirdFilter');

        const firstFilter = updatedFilters.firstFilter;
        const secondFilter = updatedFilters.secondFilter;
        const thirdFilter = updatedFilters.thirdFilter;

        it('should reset the first filter', () => expect(firstFilter.getIsActive()).toBe(false));
        it('should reset the first filter', () => expect(firstFilter.getValue()).toBe(null));

        it('should not reset the second filter', () => expect(secondFilter.getIsActive()).toBe(true));
        it('should not reset the second filter', () => expect(secondFilter.getValue()).toBe(dateTwoEpochTime));

        it('should reset the third filter', () => expect(thirdFilter.getIsActive()).toBe(false));
        it('should reset the third filter', () => expect(thirdFilter.getValue()).toBe(''));
    });

    describe('resetAllFilters works correctly', () => {
        const updatedFilters = resetAllFilters(filters);

        const firstFilter = updatedFilters.firstFilter;
        const secondFilter = updatedFilters.secondFilter;
        const thirdFilter = updatedFilters.thirdFilter;

        it('should reset the first filter', () => expect(firstFilter.getIsActive()).toBe(false));
        it('should reset the first filter', () => expect(firstFilter.getValue()).toBe(null));

        it('should reset the second filter', () => expect(secondFilter.getIsActive()).toBe(false));
        it('should reset the second filter', () => expect(secondFilter.getValue()).toBe(null));

        it('should reset the third filter', () => expect(thirdFilter.getIsActive()).toBe(false));
        it('should reset the third filter', () => expect(thirdFilter.getValue()).toBe(''));
    });

    describe('getFilters with activeOnly flag set works correctly', () => {
        const { filters: updatedFilters, errors: updatedErrors } = resetFilters(filters, errors, 'thirdFilter');
        const activeFilters = getFilters(updatedFilters, updatedErrors, true);

        const firstFilter = activeFilters.firstFilter;
        const secondFilter = activeFilters.secondFilter;

        it('should contain the correct value for first filter', () => expect(firstFilter.getIsActive()).toBe(true));
        it('should contain the correct value for second filter', () => expect(secondFilter.getIsActive()).toBe(true));
        it('should contain the correct value for third filter', () => expect(activeFilters).not.toHaveProperty('thirdFilter'));
    });

    describe('getFilters with activeOnly flag set works correctly when error set', () => {
        // set second filter to date earlier than the first 
        ({ filters, errors } = setFilterValue(filters, errors, 'secondFilter', '11-Jul-2000'));
        // reset third filter
        const { filters: updatedFilters, errors: updatedErrors } = resetFilters(filters, errors, 'thirdFilter');

        const activeFilters = getFilters(updatedFilters, updatedErrors, true);

        it('should update the errors object', () => expect(errors).toHaveProperty('secondFilter', errorMessage));

        const firstFilter = activeFilters.firstFilter;

        it('should contain the correct value for first filter', () => expect(firstFilter.getIsActive()).toBe(true));
        it('should contain the correct value for third filter', () => expect(activeFilters).not.toHaveProperty('secondFilter'));
        it('should contain the correct value for third filter', () => expect(activeFilters).not.toHaveProperty('thirdFilter'));
    });
});