'use strict';

import { getFilterRecord, VALUE_TYPES } from '../../src/filter/filterFactory/filterFactory';

describe('FilterFactory', () => {
    describe('dateFilter default getter and setter work as expected', () => {
        const date = '22-Mar-1900';
        const valueType = VALUE_TYPES.DATE_FROM;
        const dateFilter = getFilterRecord({ valueType, initValue: null });
        const newFilter = dateFilter.setValue(date);

        it('should have the correct value type', () => expect(newFilter.valueType).toBe(VALUE_TYPES.DATE_FROM));
        it('should return a formatted date', () => expect(newFilter.getValue()).toBe(-2202076800000));
    });

    describe('dateFilter sets default time correctly', () => {
        const date = '15-Jun-1990';
        const valueTypeFrom = VALUE_TYPES.DATE_FROM;
        const valueTypeTo = VALUE_TYPES.DATE_TO;

        const dateFilterFrom = getFilterRecord({ valueType: valueTypeFrom, initValue: null });
        const newDateFilterFrom = dateFilterFrom.setValue(date);

        const dateFilterTo = getFilterRecord({ valueType: valueTypeTo, initValue: null });
        const newDateFilterTo = dateFilterTo.setValue(date);

        it('should return a Unix time of the date + 00:00:00 hours', () => expect(newDateFilterFrom.getValue()).toBe(645404400000));
        it('should return a Unix time of the date + 23:59:59 hours', () => expect(newDateFilterTo.getValue()).toBe(645490799000));
    });

    describe('date custom setter is applied', () => {
        const date = '22-Mar-1900';
        const valueType = VALUE_TYPES.DATE_FROM;
        const dateFilter = getFilterRecord({ valueType, initValue: null, setter: _ => 'TestString' });
        const newDateFilter = dateFilter.setValue(date);

        it('should return the expected value', () => expect(newDateFilter.getValue()).toBe('TestString'));
    });

    describe('date custom getter is applied', () => {
        const date = '22-Mar-1900';
        const valueType = VALUE_TYPES.DATE_FROM;
        const dateFilter = getFilterRecord({ valueType, initValue: null, getter: () => 42 });
        const newDateFilter = dateFilter.setValue(date);

        it('should return the expected value', () => expect(newDateFilter.getValue()).toBe(42));
    });

    describe('DateFilter initValue is set correctly', () => {
        const date = '05-Jun-1995';
        const valueType = VALUE_TYPES.DATE_FROM;
        const dateFilter = getFilterRecord({ valueType, initValue: date });

        it('should return a Unix time', () => expect(dateFilter.getValue()).toBe(802306800000));
    });

    describe('textFilter default getter and setter work as expected', () => {
        const string = 'theValue';
        const valueType = VALUE_TYPES.TEXT;
        const stringFilter = getFilterRecord({ valueType, initValue: null });
        const newStringFilter = stringFilter.setValue(string);

        it('should have the correct value type', () => expect(newStringFilter.valueType).toBe(VALUE_TYPES.TEXT));
        it('should return a string', () => expect(newStringFilter.getValue()).toBe(string));
    });

    describe('textFilter default getter and setter deal with number', () => {
        const number = 665;
        const valueType = VALUE_TYPES.TEXT;
        const stringFilter = getFilterRecord({ valueType, initValue: null });
        const newStringFilter = stringFilter.setValue(number);

        it('should have the correct value type', () => expect(newStringFilter.valueType).toBe(VALUE_TYPES.TEXT));

        it('should return a string', () => expect(newStringFilter.getValue()).toBe('665'));
    });

    describe('StringFilter is reset to correct state', () => {
        const number = 665;
        const valueType = VALUE_TYPES.TEXT;
        const stringFilter = getFilterRecord({ valueType, initValue: number });

        it('should have the correct value type', () => expect(stringFilter.valueType).toBe(VALUE_TYPES.TEXT));
        it('should return a string', () => expect(stringFilter.getValue()).toBe(number.toString()));

        const newString = 'newValue';
        const newStringFilter = stringFilter.setValue(newString);

        it('should return the updated value', () => expect(newStringFilter.getValue()).toBe(newString));

        const resetStringFilter = newStringFilter.reset();

        it('should return the original value', () => expect(resetStringFilter.getValue()).toBe(number.toString()));
    });

});