'use strict';

import { getEnumAttribute } from '../../src/utils/js/enumUtils';

describe('uspSelectForEnumeration', () => {
    const enumeration = {
        values: [
            {
                enumName: 'LOW',
                enumValue: 'LOW',
                name: 'Low',
                numericValue: 1
            },
            {
                enumName: 'MEDIUM',
                enumValue: 'MEDIUM',
                name: 'Medium',
                numericValue: 2
            },
            {
                enumName: 'HIGH',
                enumValue: 'HIGH',
                name: 'High',
                numericValue: 3
            }
        ]
    };

    describe('getEnumAttribute functionality', () => {
        it('should return the numeric value of LOW', () => {
            const attributeValue = getEnumAttribute(enumeration, 'LOW', 'numericValue');
            expect(attributeValue).toBe(1);
        });

        it('should return the numeric value of HIGH', () => {
            const attributeValue = getEnumAttribute(enumeration, 'HIGH', 'numericValue');
            expect(attributeValue).toBe(3);
        });

        it('should return a name of Medium', () => {
            const attributeValue = getEnumAttribute(enumeration, 'MEDIUM', 'name');
            expect(attributeValue).toBe('Medium');
        });
    });
});
