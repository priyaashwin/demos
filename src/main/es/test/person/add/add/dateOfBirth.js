'use strict';
import React from 'react';
import { shallow } from 'enzyme';
import DateOfBirth from '../../../../src/person/add/jsx/add/dateOfBirth';
import { FuzzyDate } from '../../../../src/person/add/js/person';

describe('Value displayed in Age input field when Birthday is given', () => {
    const emptyProps = {
        diedDate: {},
        labels: {
            dateOfBirthEstimatedMessage: '',
            dateOfBirthEstimated: ''
        },
        onUpdate: function() {}
    };

    describe('Date is NOT given', () => {
        it('Should NOT be disabled', () => {
            const wrapper = shallow(<DateOfBirth {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('disabled')).toBe(false);
        });

        it('Should be empty', () => {
            const wrapper = shallow(<DateOfBirth {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("");
        });
    });

    describe('Only Year is given', () => {
        it('Should NOT be disabled', () => {
            const today = moment().tz('Europe/London');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: undefined,
                day: undefined
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('disabled')).toBe(false);
        });

        it('Should display 9, date is 10 years ago', () => {
            const today = moment().tz('Europe/London').subtract(10, 'Years');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: undefined,
                day: undefined
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("9");
        });
    });

    describe('Month and Year are given', () => {
        it('Should NOT be disabled', () => {
            const today = moment().tz('Europe/London');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: undefined
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('disabled')).toBe(false);
        });

        it('Should display 3, date is 37 months ago', () => {
            const today = moment().tz('Europe/London').subtract(37, 'Months');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: undefined
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("3");
        });

        it('Should display 2, date is 36 months ago', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: undefined
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("2");
        });

        it('Should display 2, date is 35 months ago', () => {
            const today = moment().tz('Europe/London').subtract(35, 'Months');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: undefined
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("2");
        });
    });

    describe('Day, Month and Year are given', () => {
        it('Should be disabled', () => {
            const today = moment().tz('Europe/London');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('disabled')).toBe(true);
        });

        it('Should display date of birth estimated', () => {
            const today = moment().tz('Europe/London');
            const estimated = true;
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} dateOfBirthEstimated={estimated} {...emptyProps} />);
            expect(wrapper.find('Estimated').prop('isEstimated')).toBe(true);
        });

        it('Should display 3, date is 36 months minus 1 day ago', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months').subtract(1, 'Days');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("3");
        });

        it('Should display 3, date is 36 months ago', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("3");
        });

        it('Should display 2, date is 36 months and 1 day ago', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months').add(1, 'Days');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("2");
        });

        it('Should display 2, date is 36 months back and set to last day of that month', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months').endOf('month');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("2");
        });

        it('Should display 3, date is 36 months back and set to first day of that month', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months').startOf('month');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("3");
        });

        it('Should display 2, date is 36 months back and set to last day of that year', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months').endOf('year');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("2");
        });

        it('Should display 3, date is 36 months back and set to first day of that year', () => {
            const today = moment().tz('Europe/London').subtract(36, 'Months').startOf('year');
            const dateOfBirth = new FuzzyDate({
                year: today.get('year'),
                month: today.get('month'),
                day: today.get('date')
            });
            const wrapper = shallow(<DateOfBirth dateOfBirth={dateOfBirth} {...emptyProps} />);
            expect(wrapper.find('ForwardRef').prop('value')).toEqual("3");
        });
    });
});
