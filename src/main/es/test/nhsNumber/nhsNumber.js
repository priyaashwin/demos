'use strict';

import { formatNHSNumber } from '../../src/nhsNumber/nhsNumber';

describe('NHS Number', () => {
    describe('formatNHSNumber', () => {
        const nhsNumberString = '956 568 0720';
        const nhsNumberNumeric = 2893283187;

        it('should format a NHS Number supplied as a String', () =>
            // String NHS Number formated correctly
            expect(formatNHSNumber(nhsNumberString)).toBe('956-568-0720'));

        it('should format a NHS Number supplied as a Number', () =>
            // Number NHS Number formated correctly
            expect(formatNHSNumber(nhsNumberNumeric)).toBe('289-328-3187'));

        it('should return nothing if the number is invalid', () =>
            // Invalid NHS Number ignored
            expect(formatNHSNumber('foo')).toBe(''));
    });
});
