'use strict';
import React from 'react';
import MapLink from '../../src/link/link';
import { shallow } from 'enzyme';
import Temporary from '../../src/indicator/temporary';
import Placement from '../../src/indicator/placement';
import SingleLineAddress from '../../src/address/jsx/singleLineAddress';

describe('Address', () => {
    describe('Address wrapper', () => {
        const emptyProps = {
            labels: {},
            address: {},
            mapLinkUrl: '',
            permissions: {},
            codedEntries: {}
        };

        it('should output no address for a person with no permission to view an address.', () => {
            const wrapper = shallow(<SingleLineAddress {...emptyProps} />);

            expect(wrapper.contains(<span title="" />)).toBe(true);
        });

        it('should output no address for a person with permission to view an address but no address.', () => {
            const wrapper = shallow(
                <SingleLineAddress {...emptyProps} address={null} permissions={{ canViewAddress: true }} />
            );

            expect(wrapper.contains(<span>{'NO ADDRESS RECORDED'}</span>)).toBe(true);
        });

        it('should output an address for a person with permission to view an address and an ACTIVE HOME address.', () => {
            const wrapper = shallow(
                <SingleLineAddress
                    {...emptyProps}
                    permissions={{ canViewAddress: true }}
                    address={{
                        temporalStatus: 'ACTIVE',
                        type: 'HOME',
                        location: { location: 'my house' }
                    }}
                    mapLinkUrl={'http://example.com'}
                />
            );

            const location = 'my house';
            const contents = getContents({
                location: location,
                mapLink: (
                    <MapLink url={'http://example.com'} term={location} target="_blank">
                        {location}
                    </MapLink>
                )
            });

            expect(wrapper.contains(contents)).toBe(true);
        });

        it('should output an address for a person with permission to view an address and an ACTIVE PLACEMENT address.', () => {
            const wrapper = shallow(
                <SingleLineAddress
                    {...emptyProps}
                    permissions={{ canViewAddress: true }}
                    address={{
                        temporalStatus: 'ACTIVE',
                        type: 'PLACEMENT',
                        location: { location: 'my house' }
                    }}
                    labels={{ isTemporary: 'temporary', isPlacement: 'placement' }}
                    mapLinkUrl={'http://example.com'}
                />
            );

            const location = 'my house';
            const contents = getContents({
                location: location,
                placement: <Placement label="placement" />,
                mapLink: (
                    <MapLink url={'http://example.com'} term={location} target="_blank">
                        {location}
                    </MapLink>
                )
            });

            expect(wrapper.contains(contents)).toBe(true);
        });

        it('should output an address for a person with permission to view an address and an ACTIVE TEMPORARY address.', () => {
            const wrapper = shallow(
                <SingleLineAddress
                    {...emptyProps}
                    permissions={{ canViewAddress: true }}
                    address={{
                        temporalStatus: 'ACTIVE',
                        type: 'PLACEMENT',
                        location: { location: 'my house' },
                        usage: 'TEMPORARY'
                    }}
                    labels={{ isTemporary: 'temporary', isPlacement: 'placement' }}
                    mapLinkUrl={'http://example.com'}
                />
            );

            const location = 'my house';
            const contents = getContents({
                location: location,
                temp: <Temporary label="temporary" />,
                placement: <Placement label="placement" />,
                mapLink: (
                    <MapLink url={'http://example.com'} term={location} target="_blank">
                        {location}
                    </MapLink>
                )
            });

            expect(wrapper.contains(contents)).toBe(true);
        });

        it('should output no fixed abode category.', () => {
            const wrapper = shallow(
                <SingleLineAddress
                    {...emptyProps}
                    labels={{ isTemporary: 'temporary' }}
                    permissions={{ canViewAddress: true }}
                    address={{
                        temporalStatus: 'ACTIVE',
                        type: 'HOME',
                        unknownLocationType: 'NOFIXED',
                        usage: 'TEMPORARY'
                    }}
                    codedEntries={{
                        unknownLocation: {
                            codedEntries: {
                                NOFIXED: {
                                    code: 'NOFIXED',
                                    name: 'No fixed abode'
                                }
                            }
                        }
                    }}
                />
            );

            const contents = getContents({
                temp: <Temporary label="temporary" />,
                unknown: 'No fixed abode'
            });

            expect(wrapper.contains(contents)).toBe(true);
        });
    });
});

const getContents = function(vals) {
    const { location = '', placement = false, temp = false, unknown = false, mapLink = false } = vals;

    return (
        <span title={location}>
            {placement}
            {temp}
            {unknown}
            {mapLink}
        </span>
    );
};
