'use strict';
import React from 'react';
import Link from '../../src/link/link';
import { shallow } from 'enzyme';

describe('Link', () => {
    const content = <span>test</span>;

    it('Outputs content', () =>
        expect(shallow(<Link url="#none">{content}</Link>).containsMatchingElement(content)).toBe(true));

    it('Outputs the link url', () => expect(shallow(<Link url="#none" />).props()).toHaveProperty('href', '#none'));

    it('Outputs the target', () =>
        expect(shallow(<Link url="#none" target="_blank" />).props()).toHaveProperty('target', '_blank'));
});
