// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ClassificationsViewController
 */
@Component
final class ClassificationsViewControllerHelperImpl extends ClassificationsViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.
   *      ClassificationsViewController#viewPersonClassifications(final long personIdParam, WebRequest webRequest,
   *      HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewPersonClassifications(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelForView(idParam, model);
    return "personClassifications";

  }

  private void setupModelForView(final long id, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(id, SubjectType.PERSON, model);

    // Model attributes for current logged in user
    setupModelWithCurrentUser(model);

    this.getContextHelperService().setupModelForContext(model);
  }

  private void setupModelWithCurrentUser(final Model model) {
    final Long personIdForCurrentUser = this.getPersonService()
        .getPersonIdForCurrentUser();

    if (null != personIdForCurrentUser) {
      model.addAttribute("loginPersonId", personIdForCurrentUser);
      final PersonVO currentPersonVO = this.getPersonService().findById(
          personIdForCurrentUser);
      final String loginPersonName = currentPersonVO.getName();
      model.addAttribute("loginPersonName", loginPersonName);
    }
  }
}
