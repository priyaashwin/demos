package com.olmgroup.usp.apps.relationshipsrecording.service.security.context;

import com.olmgroup.usp.apps.relationshipsrecording.service.context.SubjectSummaryContext;
import com.olmgroup.usp.apps.relationshipsrecording.service.context.SubjectSummaryContextResolver;
import com.olmgroup.usp.facets.security.authentication.context.attributes.annotation.ForContextAttribute;
import com.olmgroup.usp.facets.security.authentication.context.attributes.loader.AbstractContextAttributeLoaderImpl;
import com.olmgroup.usp.facets.security.authentication.vo.SecurityTeam;
import com.olmgroup.usp.facets.security.authentication.vo.UserProfile;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Loads the subject summary context attribute.
 * 
 * @author Richard.Gibson
 */
@Component("subjectSummaryContextAttributeLoader")
@ForContextAttribute(SubjectSummaryContextResolver.SUBJECT_SUMMARY_CONTEXT)
final class SubjectSummaryContextAttributeLoaderImpl extends AbstractContextAttributeLoaderImpl {
  static final String REGION_SCOTLAND = "S";
  static final String REGION_WALES = "W";
  @Override
  public Serializable getAttributeValue(UserProfile userProfile) {
    final SecurityTeam securityTeam = userProfile.getSecurityProfile().getSecurityTeam();

    if (securityTeam != null && securityTeam.getRegionCode() != null) {
      final String regionCode = securityTeam.getRegionCode();
      if (REGION_SCOTLAND.equals(regionCode)) {
        return SubjectSummaryContext.SCOTTISH.toString();
      } else if (REGION_WALES.equals(regionCode)) {
    	  return SubjectSummaryContext.WELSH.toString();
      }
    }
    return SubjectSummaryContext.DEFAULT.toString();
  }

}
