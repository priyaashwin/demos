// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.service.context.SubjectSummaryContext;
import com.olmgroup.usp.apps.relationshipsrecording.service.context.SubjectSummaryContextResolver;
import com.olmgroup.usp.components.classification.service.ClassificationGroupService;
import com.olmgroup.usp.components.classification.vo.ClassificationGroupVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SummaryViewController
 */
@Component
final class SummaryViewControllerHelperImpl extends SummaryViewControllerHelperBaseImpl {

  private static final Logger LOGGER = LoggerFactory
      .getLogger(SummaryViewControllerHelperImpl.class);

  @Lazy
  @Inject
  private SubjectSummaryContextResolver subjectSummaryContextResolver;
  @Lazy
  @Inject
  private ClassificationGroupService classificationGroupService;

  // default to Scottish child protection classification code
  @Value("${relationshipsRecording.childProtection.classificationGroupPath:GIRFEC->GF_CP}")
  private String childProtectionClassificationGroupPath;
  
  // Welsh child protection classification code
  @Value("${relationshipsRecording.childProtection.classificationGroupPathWelsh:CPR->CPR_CPREGISTER}")
  private String childProtectionClassificationGroupPathWelsh;  
  
  @Value("${relationshipsRecording.childProtection.classificationPathSeparator:->}")
  private String childProtectionClassificationPathSeparator;
  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SummaryViewController#personSummaryView(final Long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonSummaryView(final Long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    setupModelForView(idParam, SubjectType.PERSON, model);
    return "personSummary";
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SummaryViewController#groupSummaryView(final Long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleGroupSummaryView(final Long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    setupModelForView(idParam, SubjectType.GROUP, model);
    return "groupSummary";
  }

  private void setupModelForView(final long personId, final SubjectType subjectType, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(personId, subjectType, model);
    this.getContextHelperService().setupModelForContext(model);
    if (SubjectType.PERSON == subjectType) {
      // setup childProtection attribute based on subject summary context AND access to the necessary classification
      final SubjectSummaryContext subjectSummaryContext = subjectSummaryContextResolver.resolveSubjectSummaryContext();
      if (SubjectSummaryContext.SCOTTISH == subjectSummaryContext) {
    	  setupChildProtectionSummaryEnabled(model, childProtectionClassificationGroupPath);
      } else if (SubjectSummaryContext.WELSH == subjectSummaryContext) {
    	  setupChildProtectionSummaryEnabled(model, childProtectionClassificationGroupPathWelsh);    	  
      } 
      else {
    	// add default attribute that represent the group path
    	  model.addAttribute("childProtectionClassificationGroupPath", childProtectionClassificationGroupPath);    	  
      }
      
      // add attribute that represent the group separator
      model.addAttribute("childProtectionClassificationPathSeparator", childProtectionClassificationPathSeparator);
    }
  }
  
  private void setupChildProtectionSummaryEnabled(final Model model, final String classificationGroupPath) {
	try {
	  // add attribute that represent the group path
	  model.addAttribute("childProtectionClassificationGroupPath", classificationGroupPath);
	  
	  // must also have L3 access to the necessary classification group
	  final ClassificationGroupVO classificationGroupVO = classificationGroupService
	      .findByCodePath(classificationGroupPath, childProtectionClassificationPathSeparator);

	  if (null != classificationGroupVO && classificationGroupVO.getSecurityMetaData().isReadSummary()) {
	    // perfect - can now setup the a model attribute to indicate CP Summary enabled
	    model.addAttribute("childProtectionSummaryEnabled", true);
	  }
	} catch (AccessDeniedException ae) {
	  // this is OK - we expect some users to be denied access
	} catch (Exception ex) {
	  LOGGER.error("Unable to load child protection classification at path {} {}",
	      childProtectionClassificationGroupPath, childProtectionClassificationPathSeparator, ex);
	}
  }
}