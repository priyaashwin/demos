package com.olmgroup.usp.apps.relationshipsrecording.config;

import com.olmgroup.usp.facets.security.authorisation.vo.SecurityPermissionContainerBean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Lazy;

/**
 * Used to declare security authorisation and user details to the spring context.
 * 
 * @author robd
 */
@Configuration
@ImportResource(locations = { "classpath:META-INF/apps/relationshipsrecording/ext/security/relationshipsrecording-authorisation.xml" })
class EclipseSecurityConfig {

  @Lazy
  @Bean
  public SecurityPermissionContainerBean relationshipsrecordingApplicationCustomPermissions() {
    return SecurityPermissionContainerBean.withPermissions()
        .permission("RelationshipsRecording.SEARCH")
        .description("Permission which dictates whether or not the security principal may operate the application search from the UI")
        .build();
  }

}
