// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.RuleAdminViewController
 */
@Component
final class RuleAdminViewControllerHelperImpl extends RuleAdminViewControllerHelperBaseImpl {
  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.RuleAdminViewController#viewRules(final Boolean reset,
   *      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewRules(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    boolean reset = true;

    if (resetParam != null) {
      reset = resetParam;
    }

    model.addAttribute("reset", reset);
    this.getContextHelperService().setupModelForContext(model);

    return "admin.rule";
  }
}