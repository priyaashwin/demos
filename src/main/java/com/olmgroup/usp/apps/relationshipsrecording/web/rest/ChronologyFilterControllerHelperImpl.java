// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

import com.olmgroup.usp.apps.relationshipsrecording.vo.ChronologyFilterItemsVO;
import com.olmgroup.usp.components.chronology.domain.ImpactLevel;
import com.olmgroup.usp.components.chronology.domain.StatusLevel;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.ChronologyFilterController
 */
@Component
class ChronologyFilterControllerHelperImpl extends ChronologyFilterControllerHelperBaseImpl {

  public ChronologyFilterControllerHelperImpl() {
    super();
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.ChronologyFilterController#getFilterItems(final long
   *      chronoologyRecordId, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public ChronologyFilterItemsVO handleGetFilterItems(long chronologyRecordId, WebRequest webRequest,
      HttpServletResponse servletResponse, Model model)
      throws Exception {
    ChronologyFilterItemsVO chronologyFilterItemsVO = new ChronologyFilterItemsVO();

    chronologyFilterItemsVO
        .setSources(getChronologyEntryService().getSourceItemsForChronology(chronologyRecordId));
    chronologyFilterItemsVO
        .setSourceOrgs(getChronologyEntryService().getSourceOrgItemsForChronology(chronologyRecordId));

    List<String> impactItems = new ArrayList<String>();
    List<?> impactLevels = getChronologyEntryService().getImpactItemsForChronology(chronologyRecordId);
    for (Object impactLevel : impactLevels) {
      if (impactLevel instanceof ImpactLevel) {
        impactItems.add(((ImpactLevel) impactLevel).getValue());
      }
    }
    chronologyFilterItemsVO.setImpacts(impactItems);

    chronologyFilterItemsVO
        .setPractitioners(getChronologyEntryService().getPractitionerItemsForChronology(chronologyRecordId));
    chronologyFilterItemsVO
        .setPractitionerOrgs(
            getChronologyEntryService().getPractitionerOrgItemsForChronology(chronologyRecordId));
    chronologyFilterItemsVO
        .setEntryTypes(getChronologyEntryService().getEntryTypeItemsForChronology(chronologyRecordId));
    chronologyFilterItemsVO
        .setEntrySubTypes(getChronologyEntryService().getEntrySubTypeItemsForChronology(chronologyRecordId));

    List<String> statusItems = new ArrayList<String>();
    List<?> statuses = getChronologyEntryService().getStatusItemsForChronology(chronologyRecordId);
    for (Object status : statuses) {
      if (status instanceof StatusLevel) {
        statusItems.add(((StatusLevel) status).getValue());
      }
    }
    chronologyFilterItemsVO.setStatuses(statusItems);

    return chronologyFilterItemsVO;
  }
}