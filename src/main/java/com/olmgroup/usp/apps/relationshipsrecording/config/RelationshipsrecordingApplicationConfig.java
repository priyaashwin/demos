package com.olmgroup.usp.apps.relationshipsrecording.config;

import com.olmgroup.usp.facets.core.integration.profile.Integration;
import com.olmgroup.usp.facets.spring.StandardUSPProfiles;
import com.olmgroup.usp.facets.spring.SuppressingProfile;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.network.NetworkConnector;
import org.apache.activemq.store.kahadb.KahaDBPersistenceAdapter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.io.File;

/**
 * Spring configuration class used to define spring context for the application.
 * 
 * @author Rob D.
 */
@Configuration
class RelationshipsrecordingApplicationConfig {

  @Value("${usp.facet.integration.systemIdentifier:relationshipsrecording}")
  private String systemIdentifier;

  @Configuration
  @SuppressingProfile({ StandardUSPProfiles.DEVELOPMENT })
  @PropertySource(value = "classpath:commonUSPConfiguration.properties", ignoreResourceNotFound = true)
  @PropertySource("classpath:relationshipsrecording.properties")
  class AppProperties {

  }

  // TODO Websocket configuration could potentially be moved into the webresources-facet
  @Configuration
  @EnableWebSocketMessageBroker
  @SuppressingProfile({ "clustered" })
  class NonClusteredWebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
      registry.addEndpoint("/websocket/notification").withSockJS();
    }

    @Override
    public void configureMessageBroker(final MessageBrokerRegistry registry) {
      registry.setApplicationDestinationPrefixes("/websocket-app");
      registry.enableSimpleBroker("/topic", "/queue");
    }
  }

  // I really don't think we use this profile.
  @Configuration
  @EnableWebSocketMessageBroker
  @Profile({ "clustered" })
  class ClusteredWebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Value("${websocket.relay.host:localhost}")
    private String relayHost;
    @Value("${websocket.relay.client.login:admin}")
    private String clientLogin;
    @Value("${websocket.relay.client.passcode:admin}")
    private String clientPasscode;
    @Value("${websocket.relay.system.login:admin}")
    private String systemLogin;
    @Value("${websocket.relay.system.passcode:admin}")
    private String systemPasscode;
    @Value("${websocket.user.destination.broadcast:}")
    private String userDestinationBroadcast;

    // TODO Address details specified by https://olmgroup.atlassian.net/browse/OLM-26832
    // - Determine if we can establish a single task executor to be used for each channel defined in this broker.
    // - Move harcoded configuration values into int valued properties
    @Override
    public void configureClientInboundChannel(final ChannelRegistration registration) {
      registration.taskExecutor().corePoolSize(50).maxPoolSize(100).keepAliveSeconds(60);
    }

    @Override
    public void configureClientOutboundChannel(final ChannelRegistration registration) {
      registration.taskExecutor().corePoolSize(50).maxPoolSize(100).keepAliveSeconds(60);
    }

    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
      registry.addEndpoint("/websocket/notification").withSockJS();
    }

    @Override
    public void configureMessageBroker(final MessageBrokerRegistry registry) {
      registry.configureBrokerChannel().taskExecutor().corePoolSize(50).maxPoolSize(100).keepAliveSeconds(60);
      registry.setApplicationDestinationPrefixes("/websocket-app");
      registry.enableStompBrokerRelay("/topic", "/queue")
          .setRelayHost(relayHost).setRelayPort(61613)
          .setClientLogin(clientLogin).setClientPasscode(clientPasscode)
          .setSystemLogin(systemLogin).setSystemPasscode(systemPasscode)
          .setSystemHeartbeatSendInterval(20000).setSystemHeartbeatReceiveInterval(20000)
          .setUserDestinationBroadcast(getUserDestinationBroadcast());
    }

    private String getUserDestinationBroadcast() {
      return StringUtils.isNotBlank(userDestinationBroadcast) ? userDestinationBroadcast : "/topic/" + systemIdentifier + "/unresolved-user-session";
    }
  }

  @Configuration
  @Integration
  class EclipseIntegrationConfig {

    // Event broker connection
    @Value("${com.olmgroup.usp.integration.config.eventBroker.url:tcp://localhost:61616}")
    private String eventBrokerUrl;
    @Value("${com.olmgroup.usp.integration.config.eventBroker.username:}")
    private String eventBrokerUserName;
    @Value("${com.olmgroup.usp.integration.config.eventBroker.password:}")
    private String eventBrokerPassword;

    // Local listener broker connection
    @Value("${{usp.facet.integration.localBroker.userName:}")
    private String localBrokerUserName;
    @Value("${usp.facet.integration.localBroker.password:}")
    private String localBrokerPassword;

    @Value("${usp.facet.integration.localBroker.persistent:true}")
    private boolean persistentBroker;
    @Value("${usp.facet.integration.localBroker.useJmx:true}")
    private boolean brokerJmx;
    @Value("${usp.facet.integration.localBroker.transportConnector.uri:tcp://localhost:0}")
    private String transportUri;
    @Value("${usp.facet.integration.localBroker.kahaDB.directory:activemq-data/relationshipsrecording/kahadb}")
    private String persistenceDirectory;

    @Lazy
    @Bean
    BrokerService localAmqBroker() throws Exception {
      final String brokerName = "broker-" + systemIdentifier;
      final String networkUri = "static://" + eventBrokerUrl;
      final BrokerService broker = new BrokerService();
      broker.setBrokerName(brokerName);
      broker.setPersistent(persistentBroker);
      broker.setUseJmx(brokerJmx);

      // Transport connector
      broker.addConnector(transportUri);

      // Network connector
      final NetworkConnector connector = broker.addNetworkConnector(networkUri);
      connector.setUserName(eventBrokerUserName);
      connector.setPassword(eventBrokerPassword);

      // Persistence adapter
      final KahaDBPersistenceAdapter persistenceAdapter = new KahaDBPersistenceAdapter();
      persistenceAdapter.setDirectory(new File(persistenceDirectory));
      broker.setPersistenceAdapter(persistenceAdapter);

      return broker;
    }

    @Lazy
    @Bean
    ActiveMQConnectionFactory activeMQListenerConnectionFactory() {
      final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
      factory.setBrokerURL(eventBrokerUrl);
      factory.setUserName(eventBrokerUserName);
      factory.setPassword(eventBrokerPassword);
      return factory;
    }

    @Lazy
    @Bean
    @DependsOn("localAmqBroker")
    ActiveMQConnectionFactory activeMQConnectionFactory() {
      final String brokerUrl = "vm://broker-" + systemIdentifier;
      final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
      factory.setBrokerURL(brokerUrl);
      factory.setUserName(localBrokerUserName);
      factory.setPassword(localBrokerPassword);
      return factory;
    }
  }
}