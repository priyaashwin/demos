// license-header java merge-point
//
/**
 * @author Generated on 07/24/2013 14:14:29+0100 Do not modify by hand!
 *
 * TEMPLATE:     ValueObject.vsl in usp-java-cartridge.
 * MODEL CLASS:  application::com.olmgroup.usp.apps.relationshipsrecording::vo::PersonPersonRelationshipsResultVO
 * STEREOTYPE:   ValueObject
 */
package com.olmgroup.usp.apps.relationshipsrecording.web.vo;

import java.util.Date;

public class PersonPersonalRelationshipsResultVO
{
    /** The serial version UID of this class. Needed for serialization. */
    private static final long serialVersionUID = 1L;

    // Class attributes
    private long id;
    private long relatedPersonId;
    private String surname;
    private String forename;
    private Date dateOfBirth;
    private String gender;
    private String relationship;
    private Date startDate;
    private Date closeDate;
    
	public PersonPersonalRelationshipsResultVO() {
		super();
	}
	
	public PersonPersonalRelationshipsResultVO(long id,
			String surname, String forename, 
			Date dateOfBirth,
			String gender, String relationship, 
			Date startDate, Date closeDate) {
		super();
		this.id = id;
		this.surname = surname;
		this.forename = forename;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.relationship = relationship;
		this.startDate = startDate;
		this.closeDate = closeDate;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public long getRelatedPersonId() {
		return relatedPersonId;
	}
	public void setRelatedPersonId(long relatedPersonId) {
		this.relatedPersonId = relatedPersonId;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}
    
}