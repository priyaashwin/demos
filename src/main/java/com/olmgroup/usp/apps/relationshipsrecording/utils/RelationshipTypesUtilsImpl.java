package com.olmgroup.usp.apps.relationshipsrecording.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.olmgroup.usp.apps.relationshipsrecording.web.vo.RelationshipRoleVO;
import com.olmgroup.usp.components.relationship.service.PersonOrganisationRelationshipTypeService;
import com.olmgroup.usp.components.relationship.service.PersonPersonRelationshipTypeService;
import com.olmgroup.usp.components.relationship.vo.FamilialRelationshipTypeVO;
import com.olmgroup.usp.components.relationship.vo.PersonOrganisationRelationshipTypeVO;
import com.olmgroup.usp.components.relationship.vo.PersonPersonRelationshipTypeVO;
import com.olmgroup.usp.components.relationship.vo.ProfessionalRelationshipTypeVO;
import com.olmgroup.usp.components.relationship.vo.RelationshipRoleGenderTypeVO;
import com.olmgroup.usp.components.relationship.vo.SocialRelationshipTypeVO;
import com.olmgroup.usp.facets.search.PaginationResult;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

@Component("relationshipTypesUtils")
final class RelationshipTypesUtilsImpl implements RelationshipTypesUtils {

  @Lazy
  @Inject
  private ObjectMapper objectMapper;

  protected ObjectMapper getObjectMapper() {
    return this.objectMapper;
  }

  @Lazy
  @Inject
  private PersonOrganisationRelationshipTypeService personOrganisationRelationshipTypeService;

  protected final PersonOrganisationRelationshipTypeService getPersonOrganisationRelationshipTypeService() {
    return this.personOrganisationRelationshipTypeService;
  }

  @Lazy
  @Inject
  private PersonPersonRelationshipTypeService personPersonRelationshipTypeService;

  protected final PersonPersonRelationshipTypeService getPersonPersonRelationshipTypeService() {
    return this.personPersonRelationshipTypeService;
  }

  @Override
  public void setupModelWithPersonalRelationshipRoles(Model model)
      throws Exception {

    List<RelationshipRoleVO> familialRelationshipRoles = new ArrayList<RelationshipRoleVO>();
    List<RelationshipRoleVO> socialRelationshipRoles = new ArrayList<RelationshipRoleVO>();
    List<RelationshipRoleVO> professionalRelationshipRoles = new ArrayList<RelationshipRoleVO>();

    // Get Responsible Local Authority relationship type
    // FIXME: What we're having to do here is set the relationship type based on the relationship attribute that's been
    // chosen.
    // The value "CLIENTLOCALAUTHORITY" gets us the relationship type that is set up in the
    // Eclipse_Product_Configuration.xlsx as a "system" value.
    // What we should be doing is have the user select a relationship type first see person to person relationships for
    // details.
    // OLM-233 has some of the details
    PaginationResult<PersonOrganisationRelationshipTypeVO> personOrganisationRelationshipTypes = getPersonOrganisationRelationshipTypeService()
        .findAll(1, -1, null, PersonOrganisationRelationshipTypeVO.class);
    List<PersonOrganisationRelationshipTypeVO> personOrganisationRelationshipTypeVOs = personOrganisationRelationshipTypes
        .getResults();
    int foundCount = 0;
    for (PersonOrganisationRelationshipTypeVO perOrgRelTypeVO : personOrganisationRelationshipTypeVOs) {
      if ("CLIENTLOCALAUTHORITY".equals(perOrgRelTypeVO.getRelationshipClass())) {
        model.addAttribute("responsibleLocalAuthorityRelationshipTypeId", perOrgRelTypeVO.getId());
        foundCount++;
      }
      if ("EMPLOYEREMPLOYEE".equals(perOrgRelTypeVO.getRelationshipClass())) {
        model.addAttribute("employerEmployeeRelationshipTypeId", perOrgRelTypeVO.getId());
        foundCount++;
      }
      if (foundCount > 1) {
        break;
      }
    }

    // Get the available relationship types
    PaginationResult<PersonPersonRelationshipTypeVO> relationshipTypeResults = getPersonPersonRelationshipTypeService()
        .findByActive(1, -1, null);

    List<PersonPersonRelationshipTypeVO> relationshipTypeVOs = relationshipTypeResults.getResults();

    for (PersonPersonRelationshipTypeVO relationshipTypeVO : relationshipTypeVOs) {
      // Setup a role type from the 'A' side of the relationship type

      if (relationshipTypeVO instanceof FamilialRelationshipTypeVO) {
        Map<String, RelationshipRoleGenderTypeVO> genderRoleMap = relationshipTypeVO.getRelationshipRoleGenderTypeVOs();

        if (!CollectionUtils.isEmpty(genderRoleMap)) {

          RelationshipRoleGenderTypeVO maleRelationshipRole = genderRoleMap.get("MALE");
          RelationshipRoleGenderTypeVO femaleRelationshipRole = genderRoleMap.get("FEMALE");

          familialRelationshipRoles
              .add(getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, "MALE", true,
                  maleRelationshipRole));

          familialRelationshipRoles
              .add(getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, "FEMALE", true,
                  femaleRelationshipRole));

          familialRelationshipRoles.add(
              getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, "NOGENDER", true, null));

          if (!relationshipTypeVO.getRoleAName().equals(relationshipTypeVO.getRoleBName())) {

            familialRelationshipRoles.add(
                getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, "NOGENDER", false, null));

          }

        } else {

          familialRelationshipRoles.add(
              getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, null, true, null));

        }
      } else if (relationshipTypeVO instanceof SocialRelationshipTypeVO) {

        RelationshipRoleVO roleA = new RelationshipRoleVO();
        roleA.setId(relationshipTypeVO.getId() + "_A");
        roleA.setRelationshipClass(relationshipTypeVO
            .getRelationshipClass());
        roleA.setRelationshipClassName(relationshipTypeVO
            .getRelationshipClassName());
        roleA.setRoleClass(relationshipTypeVO.getRoleAClass());
        roleA.setName(relationshipTypeVO.getRoleAName());
        roleA.setDescription(relationshipTypeVO.getDescription());
        socialRelationshipRoles.add(roleA);

      } else if (relationshipTypeVO instanceof ProfessionalRelationshipTypeVO) {
        if (BooleanUtils.isNotTrue(((ProfessionalRelationshipTypeVO) relationshipTypeVO).getNotManuallyAssignable())) {

          RelationshipRoleVO roleA = new RelationshipRoleVO();
          roleA.setId(relationshipTypeVO.getId() + "_A");
          roleA.setRelationshipClass(relationshipTypeVO
              .getRelationshipClass());
          roleA.setRelationshipClassName(relationshipTypeVO
              .getRelationshipClassName());
          roleA.setRoleClass(relationshipTypeVO.getRoleAClass());
          roleA.setName(relationshipTypeVO.getRoleAName());
          roleA.setIsAncestorRole(relationshipTypeVO.getAncestorRole()
              .getValue().equals("ROLE_A"));
          professionalRelationshipRoles.add(roleA);
        }
      }

      // Only need to setup a 'B' side role for asymmetric relationship types
      if (!relationshipTypeVO.getRoleAName().equals(relationshipTypeVO.getRoleBName())) {

        if (relationshipTypeVO instanceof FamilialRelationshipTypeVO) {
          Map<String, RelationshipRoleGenderTypeVO> genderRoleMap = relationshipTypeVO
              .getRelationshipRoleGenderTypeVOs();

          if (!CollectionUtils.isEmpty(genderRoleMap)) {
            RelationshipRoleGenderTypeVO maleRelationshipRole = genderRoleMap.get("MALE");
            RelationshipRoleGenderTypeVO femaleRelationshipRole = genderRoleMap.get("FEMALE");

            familialRelationshipRoles
                .add(getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, "MALE", false,
                    maleRelationshipRole));

            familialRelationshipRoles
                .add(getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, "FEMALE", false,
                    femaleRelationshipRole));

          } else {

            familialRelationshipRoles.add(
                getFamilialRelationshipRole((FamilialRelationshipTypeVO) relationshipTypeVO, null, false, null));

          }
        } else if (relationshipTypeVO instanceof SocialRelationshipTypeVO) {

          RelationshipRoleVO roleB = new RelationshipRoleVO();
          roleB.setId(relationshipTypeVO.getId() + "_B");
          roleB.setRelationshipClass(relationshipTypeVO
              .getRelationshipClass());
          roleB.setRelationshipClassName(relationshipTypeVO
              .getRelationshipClassName());
          roleB.setRoleClass(relationshipTypeVO.getRoleAClass());
          roleB.setName(relationshipTypeVO.getRoleBName());
          roleB.setDescription(relationshipTypeVO.getDescription());
          socialRelationshipRoles.add(roleB);

        } else if (relationshipTypeVO instanceof ProfessionalRelationshipTypeVO) {

          // If the relationship type is manually assignable.
          if (BooleanUtils
              .isNotTrue(((ProfessionalRelationshipTypeVO) relationshipTypeVO).getNotManuallyAssignable())) {

            RelationshipRoleVO roleB = new RelationshipRoleVO();
            roleB.setId(relationshipTypeVO.getId() + "_B");
            roleB.setRelationshipClass(relationshipTypeVO
                .getRelationshipClass());
            roleB.setRelationshipClassName(relationshipTypeVO
                .getRelationshipClassName());
            roleB.setRoleClass(relationshipTypeVO.getRoleAClass());
            roleB.setName(relationshipTypeVO.getRoleBName());
            roleB.setIsAncestorRole(relationshipTypeVO
                .getAncestorRole().getValue().equals("ROLE_B"));
            roleB.setNotManuallyAssignable(((ProfessionalRelationshipTypeVO) relationshipTypeVO).getNotManuallyAssignable());
            professionalRelationshipRoles.add(roleB);
          }
        }
      }
    }

    Collections.sort(familialRelationshipRoles, new RelationshipRoleAlphabeticSort());
    Collections.sort(socialRelationshipRoles, new RelationshipRoleAlphabeticSort());
    Collections.sort(professionalRelationshipRoles, new RelationshipRoleAlphabeticSort());
    model.addAttribute("familialRelationshipRoles", getObjectMapper().writeValueAsString(familialRelationshipRoles));
    model.addAttribute("socialRelationshipRoles", getObjectMapper().writeValueAsString(socialRelationshipRoles));
    model.addAttribute("professionalRelationshipRoles",
        getObjectMapper().writeValueAsString(professionalRelationshipRoles));
  }

  @Override
  public void setupModelWithProfessionalRelationshipRoles(Model model) throws Exception {
    final PaginationResult<PersonPersonRelationshipTypeVO> relationshipTypeResults = getPersonPersonRelationshipTypeService()
        .findByActive(1, -1, null);
    final List<RelationshipRoleVO> professionalRelationshipRoles = new ArrayList<RelationshipRoleVO>();

    for (PersonPersonRelationshipTypeVO relationshipTypeVO : relationshipTypeResults.getResults()) {
      if (relationshipTypeVO instanceof ProfessionalRelationshipTypeVO) {
        final RelationshipRoleVO roleA = new RelationshipRoleVO();
        roleA.setId(relationshipTypeVO.getId() + "_A");
        roleA.setRelationshipClass(relationshipTypeVO
            .getRelationshipClass());
        roleA.setRelationshipClassName(relationshipTypeVO
            .getRelationshipClassName());
        roleA.setRoleClass(relationshipTypeVO.getRoleAClass());
        roleA.setName(relationshipTypeVO.getRoleAName());
        roleA.setIsAncestorRole(relationshipTypeVO.getAncestorRole()
            .getValue().equals("ROLE_A"));

        Boolean notManualAssign = ((ProfessionalRelationshipTypeVO) relationshipTypeVO).getNotManuallyAssignable();
        roleA.setNotManuallyAssignable(notManualAssign);

        if (null == notManualAssign) {
          professionalRelationshipRoles.add(roleA);
        } else if (!notManualAssign) {
          professionalRelationshipRoles.add(roleA);
        }
      }
    }
    Collections.sort(professionalRelationshipRoles, new RelationshipRoleAlphabeticSort());
    model.addAttribute("professionalRelationshipRoles",
        getObjectMapper().writeValueAsString(professionalRelationshipRoles));
  }

  private static RelationshipRoleVO getFamilialRelationshipRole(FamilialRelationshipTypeVO relType, String gender,
      boolean roleA, RelationshipRoleGenderTypeVO genderType) {
    RelationshipRoleVO roleVO = new RelationshipRoleVO();

    String id = null;
    String roleClass = null;
    String name = null;
    boolean isAncestorRole;

    if (roleA) {
      id = relType.getId() + "_A";
      roleClass = relType.getRoleAClass();
      isAncestorRole = relType.getAncestorRole().getValue().equals("ROLE_A");

      if (genderType != null) {
        name = genderType.getRoleAName();
      } else {
        name = relType.getRoleAName();
      }
    } else {
      // This is for roleB
      id = relType.getId() + "_B";
      roleClass = relType.getRoleBClass();
      isAncestorRole = relType.getAncestorRole().getValue().equals("ROLE_B");

      if (genderType != null) {
        name = genderType.getRoleBName();
      } else {
        name = relType.getRoleBName();
      }
    }

    roleVO.setId(id);
    roleVO.setRelationshipClass(relType.getRelationshipClass());
    roleVO.setRelationshipClassName(relType.getRelationshipClassName());
    roleVO.setRoleClass(roleClass);
    roleVO.setName(name);
    if (gender != null) {
      roleVO.setGender(gender);
    }

    roleVO.setDescription(relType.getDescription());
    roleVO.setIsAncestorRole(isAncestorRole);

    roleVO.setMinAncestorAge(relType.getMinAncestorAge());
    roleVO.setstartDateGuidance(relType.getStartDateGuidance());
    roleVO.setEnforceAncestorCheck(relType.getEnforceAncestorCheck());

    roleVO.setIsAdoptiveApplicable(relType.getIsAdoptiveApplicable());
    roleVO.setIsInLawApplicable(relType.getIsInLawApplicable());
    roleVO.setIsStepApplicable(relType.getIsStepApplicable());
    roleVO.setIsHalfFullApplicable(relType.getIsHalfFullApplicable());
    roleVO.setIsMultipleBirthApplicable(relType.getIsMultipleBirthApplicable());

    return roleVO;
  }

  private class RelationshipRoleAlphabeticSort implements Comparator<RelationshipRoleVO> {

    @Override
    public int compare(final RelationshipRoleVO o1, final RelationshipRoleVO o2) {
      return o1.getName().compareTo(o2.getName());
    }
  }
}
