// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonSummaryAdminViewController
 */
@Component
final class PersonSummaryAdminViewControllerHelperImpl extends PersonSummaryAdminViewControllerHelperBaseImpl {

  private static final String PERSON_SUMMARY_PAGE = "admin.personsummary.page";

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonSummaryAdminViewController#viewPersonSummary(WebRequest
   *      webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewPersonSummary(final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {
    this.getContextHelperService().setupModelForContext(model);
    return PERSON_SUMMARY_PAGE;
  }

  @Override
  public String handleViewFormConfigurableTypeReference(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    this.getContextHelperService().setupModelForContext(model);
    return PERSON_SUMMARY_PAGE;
  }

  @Override
  public String handleViewCaseNoteConfigurableTypeReference(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    this.getContextHelperService().setupModelForContext(model);
    return PERSON_SUMMARY_PAGE;
  }
}