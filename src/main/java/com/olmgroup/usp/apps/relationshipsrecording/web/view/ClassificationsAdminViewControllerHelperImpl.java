// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ClassificationsAdminViewController
 */
@Component
final class ClassificationsAdminViewControllerHelperImpl extends ClassificationsAdminViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ClassificationsAdminViewController#viewClassifications(WebRequest
   *      webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewClassifications(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {

    this.getContextHelperService().setupModelForContext(model);
    return "admin.classifications.page";
  }
}