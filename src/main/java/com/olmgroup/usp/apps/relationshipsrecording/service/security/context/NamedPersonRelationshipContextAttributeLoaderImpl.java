package com.olmgroup.usp.apps.relationshipsrecording.service.security.context;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.olmgroup.usp.facets.security.authentication.context.attributes.annotation.ForContextAttribute;
import com.olmgroup.usp.facets.security.authentication.context.attributes.loader.AbstractContextAttributeLoaderImpl;
import com.olmgroup.usp.facets.security.authentication.context.attributes.loader.ContextAttributeLoader;
import com.olmgroup.usp.facets.security.authentication.vo.SecurityTeam;
import com.olmgroup.usp.facets.security.authentication.vo.UserProfile;

/**
 * An implementation of {@link ContextAttributeLoader} for the
 * {@value #NAMED_PERSON_RELATIONSHIP_AVAILABLE_CONTEXT_ATTR} attribute.
 * 
 * @author Alan.Boshier
 *
 */
//FIXME This class must be removed as part of OLM-30276 so the named person attribute uses the standard relationship attribute context.
@Component("namedPersonRelationshipContextAttributeLoader")
@ForContextAttribute(NamedPersonRelationshipContextAttributeLoaderImpl.NAMED_PERSON_RELATIONSHIP_AVAILABLE_CONTEXT_ATTR)
final class NamedPersonRelationshipContextAttributeLoaderImpl extends AbstractContextAttributeLoaderImpl {

	static final String NAMED_PERSON_RELATIONSHIP_AVAILABLE_CONTEXT_ATTR = "named-person-relationship-available";
	static final String REGION_SCOTLAND = "S";
	static final String REGION_NORTHERN_IRELAND = "N";

	@Override
	public Serializable getAttributeValue(final UserProfile userProfile) {
		final SecurityTeam securityTeam = userProfile.getSecurityProfile().getSecurityTeam();

		/*
		 * The Named Person attribute on relationships is only available for
		 * users whose currently logged-in team is based north of the border.
		 */
		if (securityTeam != null && securityTeam.getRegionCode() != null) {
			final String regionCode = securityTeam.getRegionCode();
			if (REGION_SCOTLAND.equals(regionCode)) {
				return true;
			}
		}
		return false;
	}

}
