package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Named;

@Controller
@Named("ruleDesignerController")
@RequestMapping("/rule/designer")
public class RuleDesignerController implements LazilyInitialized {

  @RequestMapping(method = RequestMethod.GET)
  public String processIndex(@RequestParam(value = "id", required = false) final Long idParam, ModelMap model) {
    //echo back the id
    model.addAttribute("id", idParam);

    return "rule.designer";
  }

}