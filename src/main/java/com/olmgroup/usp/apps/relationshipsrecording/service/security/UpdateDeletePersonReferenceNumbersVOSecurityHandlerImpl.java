// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonReferenceNumbersVO;
import com.olmgroup.usp.components.person.domain.PersonReferenceNumber;
import com.olmgroup.usp.components.person.domain.PersonReferenceNumberDao;
import com.olmgroup.usp.components.person.vo.UpdatePersonReferenceNumberVO;
import com.olmgroup.usp.facets.security.authorisation.instance.SecurityType;
import com.olmgroup.usp.facets.security.authorisation.instance.annotation.SupportsSecurityTypes;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

/**
 * <p>
 * Completes the implementation of the {@link UpdateDeletePersonReferenceNumbersVOSecurityHandler} by implementing
 * abstract methods defined in {@link UpdateDeletePersonReferenceNumbersVOSecurityHandlerBase}.
 * </p>
 * <p>
 * This provides a
 * {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler SecurityHandler} for the
 * {@link UpdateDeletePersonReferenceNumbersVO} value object, forming the basis for providing Instance Level Security.
 * </p>
 * 
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 */
@Component("updateDeletePersonReferenceNumbersVOSecurityHandler")
@SupportsSecurityTypes({ SecurityType.RELATIONAL })
final class UpdateDeletePersonReferenceNumbersVOSecurityHandlerImpl
    extends UpdateDeletePersonReferenceNumbersVOSecurityHandlerBase {

  @Lazy
  @Inject
  private PersonReferenceNumberDao personReferenceNumberDao;

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = -3952007141661562414L;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final UpdateDeletePersonReferenceNumbersVO targetDomainObject) {
    final Set<SubjectIdTypeVO> subjects = new HashSet<>();
    final Set<Long> personReferenceNumberIds = new HashSet<>();

    // Fetch PersonReferenceNumber IDs from delete list
    if (null != targetDomainObject.getDeletePersonReferenceNumberIds()) {
      personReferenceNumberIds.addAll(targetDomainObject
          .getDeletePersonReferenceNumberIds());
    }

    // Fetch PersonReferenceNumber IDs from update list
    if (null != targetDomainObject.getUpdateReferenceNumberVOs()) {
      for (UpdatePersonReferenceNumberVO updatePersonReferenceNumberVO : targetDomainObject
          .getUpdateReferenceNumberVOs()) {
        personReferenceNumberIds.add(updatePersonReferenceNumberVO.getId());
      }
    }

    // Retrieve associated person subjects and add to result set
    for (Long personReferenceNumberId : personReferenceNumberIds) {
      final PersonReferenceNumber personReferenceNumber = this.personReferenceNumberDao
          .get(personReferenceNumberId);

      if (null != personReferenceNumber) {
        subjects.add(new SubjectIdTypeVO(personReferenceNumber
            .getPerson().getId(), SubjectType.PERSON));
      }
    }

    return subjects;
  }

}