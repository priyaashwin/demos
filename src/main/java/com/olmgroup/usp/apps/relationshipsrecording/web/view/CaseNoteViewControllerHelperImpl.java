// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.components.casenote.vo.CaseNoteRecordVO;
import com.olmgroup.usp.components.group.vo.GroupVO;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.CaseNoteViewController
 */
@Component
final class CaseNoteViewControllerHelperImpl extends CaseNoteViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  @Lazy
  @Inject
  private ObjectMapper objectMapper;

  protected ObjectMapper getObjectMapper() {
    return this.objectMapper;
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. CaseNoteViewController#personCaseNoteRecord(final long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonCaseNoteRecord(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    this.setupModelForPersonView(idParam, model);

    if (null != webRequest.getParameter("print")) {
      return "casenote.print";
    }
    return "person.casenote";
  }

  @Override
  public String handleGroupCaseNoteRecord(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForGroupSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelForGroupView(idParam, model);

    if (null != webRequest.getParameter("print")) {
      return "casenote.print";
    }
    return "group.casenote";
  }

  private void setupModelForGroupView(final long groupId, final Model model) {
    // Put the group in the model
    this.getHeaderControllerViewService().setupModelWithSubject(groupId, SubjectType.GROUP, model);

    CaseNoteRecordVO caseNoteRecordVO = (CaseNoteRecordVO) model.asMap().get("casenote");
    if (caseNoteRecordVO == null) {
      caseNoteRecordVO = this.getCaseNoteRecordService().findByGroupId(
          groupId);
      model.addAttribute("casenote", caseNoteRecordVO);
    }

    // Model attributes for current logged in user
    setupModelWithCurrentUser(model);

    this.getContextHelperService().setupModelForContext(model);
  }

  private void setupModelForOrganisationView(final long organisationId, final Model model) {
    // Put the group in the model
    this.getHeaderControllerViewService().setupModelWithSubject(organisationId, SubjectType.ORGANISATION, model);

    CaseNoteRecordVO caseNoteRecordVO = (CaseNoteRecordVO) model.asMap().get("casenote");
    if (caseNoteRecordVO == null) {
      caseNoteRecordVO = this.getCaseNoteRecordService().findByOrganisationId(organisationId);
      model.addAttribute("casenote", caseNoteRecordVO);
    }

    // Model attributes for current logged in user
    setupModelWithCurrentUser(model);

    this.getContextHelperService().setupModelForContext(model);
  }

  private void setupModelForPersonView(final long personId, final Model model) throws Exception {

    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);

    CaseNoteRecordVO caseNoteRecordVO = (CaseNoteRecordVO) model.asMap().get("casenote");
    if (caseNoteRecordVO == null) {
      caseNoteRecordVO = this.getCaseNoteRecordService().findByPersonId(personId);
      model.addAttribute("casenote", caseNoteRecordVO);
    }

    // Put the groups that have CaseNotes which the person belongs to into
    // the Filters section
    setupModelWithGroupFilter(model, personId);

    // Model attributes for current logged in user
    setupModelWithCurrentUser(model);

    this.getContextHelperService().setupModelForContext(model);
  }

  private void setupModelWithCurrentUser(final Model model) {
    final PersonVO currentPersonVO = this.getPersonService()
        .getCurrentPerson();

    if (null != currentPersonVO) {
      model.addAttribute("loginPersonId", currentPersonVO.getId());
      model.addAttribute("loginPersonName", currentPersonVO.getName());
      model.addAttribute("loginPersonOrganisationName", currentPersonVO.getOrganisationName());
    }
  }

  private void setupModelWithGroupFilter(final Model model, final long personId) throws Exception {
    final List<GroupVO> groupList = getPersonCaseNoteEntryService().findByGroupsWithCaseNotes(
        personId);

    model.addAttribute("groupList", this.getObjectMapper()
        .writeValueAsString(groupList));
  }

  @Override
  public String handleOrganisationCaseNoteRecord(long idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model) throws Exception {

    setupModelForOrganisationView(idParam, model);

    if (null != webRequest.getParameter("print")) {
      return "casenote.print";
    }
    return "organisation.casenote";
  }
}
