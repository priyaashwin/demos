package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import com.olmgroup.usp.facets.security.authentication.context.UserContextService;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Implementation of {@link SubjectSummaryContextResolver}.
 */
@Component("subjectSummaryContextResolver")
final class SubjectSummaryContextResolverImp implements SubjectSummaryContextResolver {

  @Lazy
  @Inject
  private UserContextService userContextService;

  @Override
  public SubjectSummaryContext resolveSubjectSummaryContext() {
    final String contextAttribute = (String) userContextService.getContextAttributes().getContext(SUBJECT_SUMMARY_CONTEXT);
    return contextAttribute != null ? SubjectSummaryContext.valueOf(contextAttribute) : SubjectSummaryContext.DEFAULT;
  }
}
