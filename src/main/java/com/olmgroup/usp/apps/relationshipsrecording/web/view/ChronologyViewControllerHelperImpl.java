// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.components.chronology.vo.ChronologyVO;
import com.olmgroup.usp.components.group.vo.GroupVO;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChronologyViewController
 */
@Component
final class ChronologyViewControllerHelperImpl extends ChronologyViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  @Lazy
  @Inject
  private ObjectMapper objectMapper;

  protected ObjectMapper getObjectMapper() {
    return this.objectMapper;
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. ChronologyViewController#personChronology(final long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonChronology(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelForPersonView(idParam, model);

    if (null != webRequest.getParameter("print")) {
      return "chronology.print";
    }

    return "person.chronology";
  }

  private void setupModelForPersonView(final long personId, final Model model) throws Exception {

    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);

    ChronologyVO chronologyVO = (ChronologyVO) model.asMap().get("chronology");

    if (chronologyVO == null) {
      chronologyVO = this.getChronologyService().findByPersonId(personId);

      model.addAttribute("chronology", chronologyVO);
    }

    setupModelWithGroupFilter(personId, model);

    // Model attributes for current logged in user
    setupModelWithCurrentUser(model);

    this.getContextHelperService().setupModelForContext(model);
  }

  private void setupModelWithCurrentUser(final Model model) {
    final PersonVO currentPersonVO = this.getPersonService()
        .getCurrentPerson();

    if (null != currentPersonVO) {
      model.addAttribute("loginPersonId", currentPersonVO.getId());
      model.addAttribute("loginPersonName", currentPersonVO.getName());
      model.addAttribute("loginPersonOrganisationName", currentPersonVO.getOrganisationName());
    }
  }

  @Override
  public String handleGroupChronology(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForGroupSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    this.setupModelForGroupView(idParam, model);

    if (null != webRequest.getParameter("print")) {
      return "chronology.print";
    }
    return "group.chronology";
  }

  private void setupModelForGroupView(final long groupId, final Model model) {

    this.getHeaderControllerViewService().setupModelWithSubject(groupId, SubjectType.GROUP, model);

    ChronologyVO chronologyVO = (ChronologyVO) model.asMap().get("chronology");
    if (chronologyVO == null) {
      chronologyVO = this.getChronologyService().findByGroupId(groupId);
      model.addAttribute("chronology", chronologyVO);
    }

    // Model attributes for current logged in user
    setupModelWithCurrentUser(model);

    this.getContextHelperService().setupModelForContext(model);
  }

  private void setupModelWithGroupFilter(final long personId, final Model model) throws JsonProcessingException {
    final List<GroupVO> groupList = this.getGroupChronologyEntryService()
        .findGroupsWithGroupChronologyEntryByPersonId(personId);

    model.addAttribute("groupList", this.getObjectMapper().writeValueAsString(groupList));
  }
}
