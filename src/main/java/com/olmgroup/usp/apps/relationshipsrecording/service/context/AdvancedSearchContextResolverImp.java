package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import com.olmgroup.usp.facets.security.authentication.context.UserContextService;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Implementation of {@link AdvancedSearchContextResolver}.
 */
@Component("advancedSearchContextResolver")
final class AdvancedSearchContextResolverImp implements AdvancedSearchContextResolver {

  @Lazy
  @Inject
  private UserContextService userContextService;

  @Override
  public AdvancedSearchContext resolveAdvancedSearchContext() {
    final String contextAttribute = (String) userContextService.getContextAttributes().getContext(ADVANCED_SEARCH_CONTEXT);
    return contextAttribute != null ? AdvancedSearchContext.valueOf(contextAttribute) : AdvancedSearchContext.DEFAULT;
  }
}
