// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OutputAdminViewController
 */
@Component
final class OutputAdminViewControllerHelperImpl extends OutputAdminViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. OutputAdminViewController#viewOutputs(final Boolean
   *      reset, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewOutputs(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {

    return getOutputsAdmin(resetParam, model);

  }

  private String getOutputsAdmin(final Boolean resetParam, final Model model) {
    model.addAttribute("reset", resetParam);
    this.getContextHelperService().setupModelForContext(model);
    return "admin.outputs";
  }

  @Override
  public String handleViewCaseNoteOutputs(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getOutputsAdmin(resetParam, model);
  }

  @Override
  public String handleViewChronologyOutputs(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getOutputsAdmin(resetParam, model);
  }

  @Override
  public String handleViewFormOutputs(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getOutputsAdmin(resetParam, model);
  }

  @Override
  public String handleViewPersonOutputs(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getOutputsAdmin(resetParam, model);
  }

  @Override
  public String handleViewPersonLetterOutputs(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model)  {
    return getOutputsAdmin(resetParam, model);
  }

  @Override
  public String handleViewChildLookedAfterOutputs(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getOutputsAdmin(resetParam, model);
  }
}