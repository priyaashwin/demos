// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.CaseHistoryViewController
 */
@Component
final class CaseHistoryViewControllerHelperImpl extends CaseHistoryViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. CaseHistoryViewController#personCaseHistory(final long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonCaseHistory(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }
    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.PERSON, model);

    this.getContextHelperService().setupModelForContext(model);

    return "casehistory";
  }
}