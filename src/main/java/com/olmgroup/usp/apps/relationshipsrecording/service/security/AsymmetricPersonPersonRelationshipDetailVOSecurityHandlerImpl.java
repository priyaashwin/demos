// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.AsymmetricPersonPersonRelationshipDetailVO;
import com.olmgroup.usp.facets.security.authorisation.instance.SecurityType;
import com.olmgroup.usp.facets.security.authorisation.instance.annotation.SupportsSecurityTypes;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * <p>
 * Completes the implementation of the {@link AsymmetricPersonPersonRelationshipDetailVOSecurityHandler} by
 * implementing abstract methods defined in {@link AsymmetricPersonPersonRelationshipDetailVOSecurityHandlerBase}.
 * </p>
 * <p>
 * This provides a {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 * SecurityHandler} for the {@link AsymmetricPersonPersonRelationshipDetailVO} value object, forming the basis for
 * providing Instance Level Security.
 * </p>
 * 
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 */
@Component("asymmetricPersonPersonRelationshipDetailVOSecurityHandler")
@SupportsSecurityTypes({ SecurityType.RELATIONAL })
final class AsymmetricPersonPersonRelationshipDetailVOSecurityHandlerImpl
    extends AsymmetricPersonPersonRelationshipDetailVOSecurityHandlerBase {

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = -2898726125618100265L;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final AsymmetricPersonPersonRelationshipDetailVO targetDomainObject) {
    return this.getAsymmetricPersonPersonRelationshipDetailsVOSecurityHandler()
        .getSubjects(targetDomainObject);
  }
}