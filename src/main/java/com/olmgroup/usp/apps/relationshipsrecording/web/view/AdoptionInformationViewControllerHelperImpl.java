// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.AdoptionInformationViewController
 */
@Component
final class AdoptionInformationViewControllerHelperImpl extends AdoptionInformationViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.AdoptionInformationViewController#
   *      viewAdoptionInformation(final long idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model
   *      model)
   */
  @Override
  public String handleViewAdoptionInformation(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    setupModelForView(idParam, model);
    return "adoptioninformation";
  }

  private void setupModelForView(final long personId, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);
    this.getContextHelperService().setupModelForContext(model);
  }
}