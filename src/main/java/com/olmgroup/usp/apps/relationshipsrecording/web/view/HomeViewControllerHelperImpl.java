// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.utils.RelationshipTypesUtils;
import com.olmgroup.usp.components.checklist.service.TaskInstanceOwnershipService;
import com.olmgroup.usp.components.relationship.service.PersonTeamRelationshipService;
import com.olmgroup.usp.facets.core.util.TemporalStatusFilter;
import com.olmgroup.usp.facets.security.authentication.context.UserContextService;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.HomeViewController
 */
@Component
final class HomeViewControllerHelperImpl extends HomeViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private RelationshipTypesUtils relationshipTypesUtils;

  @Lazy
  @Inject
  private UserContextService userContextService;

  @Lazy
  @Inject
  private PersonTeamRelationshipService personTeamRelationshipService;

  @Lazy
  @Inject
  private TaskInstanceOwnershipService taskInstanceOwnershipService;

  private RelationshipTypesUtils getRelationshipTypesUtils() {
    return this.relationshipTypesUtils;
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. HomeViewController#checklists(final Boolean resetParam,
   *      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleChecklists(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getChecklistsHome(resetParam, model);
  }

  private String getChecklistsHome(final Boolean resetParam, final Model model) {
    setupModelWithCurrentUser(model);
    this.getContextHelperService().setupModelForContext(model);
    // pass on the reset attribute
    model.addAttribute("reset", resetParam);
    // define the include no owning team default attribute
    model.addAttribute("includeNoOwningTeamByDefault", taskInstanceOwnershipService.noOwningTeamIncludedByDefault());
    model.addAttribute("numberOfAssignedProfiles", personTeamRelationshipService
        .findProfessionalByPersonId(userContextService.getSubject().getSubjectId(), TemporalStatusFilter.ACTIVE, 1, 1, null)
        .getTotalSize());
    return "home.checklists";
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. HomeViewController#allocation(final Boolean resetParam,
   *      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleAllocation(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse,
      final Model model) throws Exception {
    this.getRelationshipTypesUtils().setupModelWithProfessionalRelationshipRoles(model);
    setupModelWithCurrentUser(model);
    this.getContextHelperService().setupModelForContext(model);
    // pass on the reset attribute
    model.addAttribute("reset", resetParam);

    return "home.allocation";
  }

  private void setupModelWithCurrentUser(final Model model) {
    // pop the user into the model
    model.addAttribute("currentUser", getPersonService().getCurrentPerson());
    model.addAttribute("currentUserTeamId", userContextService.getTeam().getTeamId());
  }

  @Override
  public String handleOwnChecklists(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getChecklistsHome(resetParam, model);
  }

  @Override
  public String handleTeamChecklists(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getChecklistsHome(resetParam, model);
  }

  @Override
  public String handleTeamMembersChecklists(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getChecklistsHome(resetParam, model);
  }

  @Override
  public String handleSummaryChecklists(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) throws Exception {
    return getChecklistsHome(null, model);
  }

  @Override
  public String handleForms(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getAuthorisationHome(resetParam, model);
  }

  @Override
  public String handleAuthorisation(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getAuthorisationHome(resetParam, model);
  }

  @Override
  public String handleWarnings(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    return getAuthorisationHome(resetParam, model);
  }

  @Override
  public String handleDashboard(final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse,
      final Model model) {
    setupModelWithCurrentUser(model);
    this.getContextHelperService().setupModelForContext(model);
    model.addAttribute("reset", resetParam);
    return "home.dashboard";
  }

  private String getAuthorisationHome(final Boolean resetParam, final Model model) {
    setupModelWithCurrentUser(model);
    this.getContextHelperService().setupModelForContext(model);
    // pass on the reset attribute
    model.addAttribute("reset", resetParam);
    return "home.authorisation";
  }

}