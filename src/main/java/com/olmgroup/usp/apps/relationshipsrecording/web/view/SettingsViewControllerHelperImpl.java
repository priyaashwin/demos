// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.configuration.service.CategoryService;
import com.olmgroup.usp.components.configuration.vo.CategoryVO;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SettingsViewController
 */
@Component
final class SettingsViewControllerHelperImpl extends SettingsViewControllerHelperBaseImpl {

  public static final Map<String, String> categories = Collections.unmodifiableMap(new HashMap<String, String>() {
    {
      put("caseNoteEntryType", "com.olmgroup.usp.components.casenote.category.entryType");
      put("chronologyEntryType", "com.olmgroup.usp.components.chronology.category.entryType");
      put("personWarnings", "com.olmgroup.usp.components.person.category.warnings");
      put("personAlerts", "com.olmgroup.usp.components.person.category.alerts");
      put("personReferenceType", "com.olmgroup.usp.components.person.category.referencetype");
      put("checklistPauseReason", "com.olmgroup.usp.components.checklist.category.pauseReason");
      put("personProfessionalTitle", "com.olmgroup.usp.components.person.category.professionalTitle");
      put("trainingCourseCode", "com.olmgroup.usp.components.childlookedafter.category.training");
      put("typeOfCare", "com.olmgroup.usp.components.childlookedafter.category.typeOfCare");
      put("reasonForUnavailability",
          "com.olmgroup.usp.components.childlookedafter.category.fosterCarerSuspensionReason");
      put("outputCategory", "com.olmgroup.usp.components.output.category.outputCategory");
      put("sourceOrganisation", "com.olmgroup.usp.facets.core.category.sourceOrganisation");
      put("nonVerbalLanguage", "com.olmgroup.usp.components.person.category.nonVerbalLanguages");
      put("belongingsType", "com.olmgroup.usp.components.belongings.category.type");
      put("belongingsLocation", "com.olmgroup.usp.components.belongings.category.location");
      put("classificationEndReason", "com.olmgroup.usp.components.classification.category.endReason");
      put("typeOfDisability", "com.olmgroup.usp.components.childlookedafter.category.typeOfDisability");
    }
  });
  private static final String SETTINGS_PAGE = "admin.codedEntries.page";

  @Lazy
  @Inject
  private CategoryService categoryService;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SettingsViewController#viewSettings(WebRequest
   *      webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewSettings(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  private void populateModel(final Model model) {
    final Iterator<Entry<String, String>> it = categories.entrySet().iterator();
    while (it.hasNext()) {
      final Entry entry = (Entry) it.next();
      final CategoryVO categoryVO = categoryService.findCategoryByCode((String) entry.getValue());

      // Necessary for the 'Add' buttons to functionality (category code
      // is required in the add URL)
      if (null != categoryVO) {
        final String name = (String) entry.getKey();
        model.addAttribute(name + "CategoryId", categoryVO.getId());
        model.addAttribute(name + "CategoryCode", categoryVO.getCode());
        model.addAttribute(name + "System", categoryVO.getSystem());
        model.addAttribute(name + "AdditionsAllowed", BooleanUtils.isTrue(categoryVO.getAllowAdditions()));
        model.addAttribute(name + "SupportsAuthorisation", BooleanUtils.isTrue(categoryVO.getSupportsAuthorisation()));
      }
    }

    this.getContextHelperService().setupModelForContext(model);
  }

  @Override
  public String handleViewCaseNoteEntryType(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewChronologyEntryType(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewPersonWarnings(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewPersonAlerts(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewPersonReferenceType(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewChecklistPauseReason(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewPersonProfessionalTitle(final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewTypeOfCare(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewReasonForUnavailability(final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewOutputCategory(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewSourceOrganisations(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewNonVerbalLanguages(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewBelongingsType(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewBelongingsLocation(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }

  @Override
  public String handleViewClassificationEndReason(final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    populateModel(model);
    return SETTINGS_PAGE;
  }
}