// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.childlookedafter.vo.FosterCarerRegistrationWithOrganisationAndPersonVO;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.components.relationship.vo.RelationshipTypeVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ApprovalsViewController
 */
@Component
final class ApprovalsViewControllerHelperImpl extends ApprovalsViewControllerHelperBaseImpl {

  private void setupModelForView(final long personId, final Model model) {

    final RelationshipTypeVO relationshipType = this.getRelationshipTypeService()
        .findByRelationshipClass("FOSTERCARER");
    model.addAttribute("relationshipType", relationshipType);

    final PersonVO partner = getChildCarerPartnerSynchronisationService().findActivePartnerForPerson(personId,
        new Date(), null);
    if (partner != null) {
      final long partnerId = partner.getId();
      model.addAttribute("activePartnerId", partnerId);
      model.addAttribute("activePartnerIdentifier", partner.getPersonIdentifier());
      model.addAttribute("activePartnerName", partner.getName());

      final FosterCarerRegistrationWithOrganisationAndPersonVO fosterCarerRegistrationWithOrganisationAndPersonVO = this
          .getFosterCarerRegistrationService().getCurrentFosterCarerRegistrationByPersonId(partnerId,
              FosterCarerRegistrationWithOrganisationAndPersonVO.class);

      if (null != fosterCarerRegistrationWithOrganisationAndPersonVO) {
        final long partnerOrganisationId = fosterCarerRegistrationWithOrganisationAndPersonVO.getOrganisation().getId();
        model.addAttribute("activePartnerOrganisationId", partnerOrganisationId);
      }

    }
    model.addAttribute("hasActivePartner", partner != null);

    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);

    this.getContextHelperService().setupModelForContext(model);
  }

  @Override
  public String handleViewPersonApprovals(final long id, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    setupModelForView(id, model);

    return "approvals";
  }

  @Override
  public String handleViewPersonApprovalsTab(final long id, final String tabName, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {

    setupModelForView(id, model);
    return "approvals";
  }

}