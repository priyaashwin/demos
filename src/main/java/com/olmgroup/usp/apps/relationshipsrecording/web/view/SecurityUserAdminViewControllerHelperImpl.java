// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.facets.core.url.AbsoluteURLResolver;
import com.olmgroup.usp.facets.core.url.URLResolverException;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SecurityUserAdminViewController
 */
@Component
final class SecurityUserAdminViewControllerHelperImpl extends SecurityUserAdminViewControllerHelperBaseImpl {
  private static final String APP_LOGIN_URL = "/login";

  @Lazy
  @Inject
  private AbsoluteURLResolver absoluteURLResolver;

  @Override
  public String handleViewUsers(final HttpServletRequest httpRequest,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model)
      throws Exception {
    final String appLoginURL = this.getAbsoluteURLFromRequest(httpRequest, APP_LOGIN_URL);
    model.addAttribute("appLoginURL", appLoginURL);
    this.getContextHelperService().setupModelForContext(model);
    return "admin.users.page";
  }

  /**
   * This method got knowledge to extract the absolute URL from the request
   * 
   * @param request
   * @return String
   * @throws URLResolverException
   */
  private String getAbsoluteURLFromRequest(final HttpServletRequest request,
      final String appURL) throws URLResolverException {
    final String absoluteBaseURL = this.absoluteURLResolver.createAbsoluteURL(request, appURL);
    return absoluteBaseURL;
  }

}