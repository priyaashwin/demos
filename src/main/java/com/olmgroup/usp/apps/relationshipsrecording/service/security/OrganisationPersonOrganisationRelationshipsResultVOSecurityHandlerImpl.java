// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.OrganisationPersonOrganisationRelationshipsResultVO;
import com.olmgroup.usp.components.relationship.service.security.PersonOrganisationRelationshipSecurityHandlerUtils;
import com.olmgroup.usp.facets.security.authorisation.instance.SecurityType;
import com.olmgroup.usp.facets.security.authorisation.instance.annotation.SupportsSecurityTypes;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.stereotype.Component;

import java.util.Set;

import javax.inject.Inject;

/**
 * <p>
 * Completes the implementation of the {@link OrganisationPersonOrganisationRelationshipsResultVOSecurityHandler} by
 * overriding null implementations defined in
 * {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandlerBase}.
 * </p>
 * <p>
 * This provides
 * a {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler SecurityHandler} for the
 * {@link OrganisationPersonOrganisationRelationshipsResultVO} value object, forming the basis for providing Instance
 * Level Security.
 * </p>
 * <p>
 * For more information on providing an implementation please see the <a
 * href="http://devdocs.group.olm.int/usp/facets/security-facet/securityhandlers.html">documentation</a>
 * </p>
 *
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandlerBase
 */
@Component("organisationPersonOrganisationRelationshipsResultVOSecurityHandler")
@SupportsSecurityTypes({ SecurityType.RELATIONAL })
final class OrganisationPersonOrganisationRelationshipsResultVOSecurityHandlerImpl
    extends OrganisationPersonOrganisationRelationshipsResultVOSecurityHandlerBase {

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = 4963901515533340760L;

  @Inject
  private PersonOrganisationRelationshipSecurityHandlerUtils relationshipSecurityHandlerUtils;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final OrganisationPersonOrganisationRelationshipsResultVO targetDomainObject) {
    return this.getPersonOrganisationRelationshipVOSecurityHandler().getSubjects(
        targetDomainObject);
  }

  @Override
  protected boolean handleSupportsSecurityType(SecurityType securityType,
      OrganisationPersonOrganisationRelationshipsResultVO targetDomainObject, Object permission) {
    if (SecurityType.RELATIONAL.equals(securityType)) {

      if (!relationshipSecurityHandlerUtils
          .isASecurableRelationship(getPersonOrganisationRelationshipDao().get(targetDomainObject.getId()))) {
        return false;
      }
    }

    return super.handleSupportsSecurityType(securityType, targetDomainObject, permission);
  }

}