// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.calendar.vo.CalendarEventBundleVO;
import com.olmgroup.usp.components.calendar.vo.CalendarVO;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.CalendarAdminViewController
 */
@Component
final class CalendarAdminViewControllerHelperImpl extends CalendarAdminViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.CalendarAdminViewController#viewCalendars(WebRequest
   *      webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewCalendars(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    return "admin.calendar";
  }

  @Override
  public String handleViewDays(final long calendarIdParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    model.addAttribute("calendarId", calendarIdParam);
    final CalendarVO calendarVO = getCalendarService().findById(calendarIdParam);
    model.addAttribute("calendarName", calendarVO.getName());

    setupModelForView(model);
    return "admin.calendar.days";
  }

  @Override
  public String handleViewHolidays(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupModelForView(model);
    return "admin.calendar.holidays";
  }

  @Override
  public String handleViewHolidayEvents(final long holidayIdParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    setupModelForView(model);
    model.addAttribute("holidayId", holidayIdParam);
    final CalendarEventBundleVO calendarEventBundleVO = getCalendarEventBundleService().findById(holidayIdParam);
    model.addAttribute("holidayName", calendarEventBundleVO.getName());
    return "admin.calendar.holidayEvents";
  }

  private void setupModelForView(final Model model) {
    this.getContextHelperService().setupModelForContext(model);
  }
}
