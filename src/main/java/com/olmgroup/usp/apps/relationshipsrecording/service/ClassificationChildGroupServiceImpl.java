// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    SpringServiceImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::ClassificationChildGroupService
 * STEREOTYPE:  Service
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import com.olmgroup.usp.components.classification.exception.InvalidClassificationGroupLevelException;
import com.olmgroup.usp.components.classification.vo.ClassificationGroupVO;
import com.olmgroup.usp.components.classification.vo.NewClassificationGroupVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.ClassificationChildGroupService
 */
@Service("classificationChildGroupService")
final class ClassificationChildGroupServiceImpl
    extends ClassificationChildGroupServiceBase {

  @Value("${relationshipsRecording.classification.group.maxParentGroups:3}")
  private long upwardClassificationGroupsLevelCount;

  @SuppressWarnings("unused")
  private static final Logger LOGGER = LoggerFactory.getLogger(ClassificationChildGroupServiceImpl.class);

  @Override
  protected Long handleAddClassificationGroup(NewClassificationGroupVO newClassificationGroupVO)
      throws Exception {
    if (newClassificationGroupVO.getParentGroupId() == null) {
      throw new InvalidClassificationGroupLevelException("Cannot create top level classification");
    }
    List<ClassificationGroupVO> listOfClassificationGroups = getClassificationGroupService()
        .findAllClassificationGroupsUpwardsById(newClassificationGroupVO.getParentGroupId());
    if (listOfClassificationGroups.size() >= this.upwardClassificationGroupsLevelCount) {
      throw new InvalidClassificationGroupLevelException(
          "Cannot add a classification group more than level-" + this.upwardClassificationGroupsLevelCount);
    }
    return getClassificationGroupService().addClassificationGroup(newClassificationGroupVO);
  }

}