// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    SpringServiceImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::PersonDetailsService
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonContactsVO;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonReferenceNumbersVO;
import com.olmgroup.usp.components.contact.vo.UpdateContactVO;
import com.olmgroup.usp.components.person.service.PersonReferenceNumberService;
import com.olmgroup.usp.components.person.service.PersonService;
import com.olmgroup.usp.components.person.vo.UpdatePersonReferenceNumberVO;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.PersonDetailsService
 */
@Service("personDetailsService")
final class PersonDetailsServiceImpl extends PersonDetailsServiceBase {

  @Override
  protected void handleCombinedUpdateDeletePersonReferenceNumber(
      UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO)
      throws Exception {
    PersonReferenceNumberService personReferenceService = getPersonReferenceNumberService();

    List<UpdatePersonReferenceNumberVO> updateList = updateDeletePersonReferenceNumbersVO.getUpdateReferenceNumberVOs();
    List<Long> deleteList = updateDeletePersonReferenceNumbersVO.getDeletePersonReferenceNumberIds();

    for (UpdatePersonReferenceNumberVO updatePersonReferenceNumberVO : updateList) {

      personReferenceService.updatePersonReferenceNumber(updatePersonReferenceNumberVO);
    }

    for (Long id : deleteList) {

      personReferenceService.deletePersonReferenceNumber(id.longValue());
    }

  }

  @Override
  protected void handleCombinedUpdateDeletePersonContact(
      UpdateDeletePersonContactsVO updateDeletePersonContactsVO,
      long personId) throws Exception {
    PersonService personService = getPersonService();

    List<UpdateContactVO> updateList = updateDeletePersonContactsVO.getUpdateContactVOs();
    List<Long> deleteList = updateDeletePersonContactsVO.getDeleteContactIds();

    for (UpdateContactVO updateContactVO : updateList) {

      personService.updateContact(updateContactVO, personId);
    }

    for (Long contactId : deleteList) {

      personService.deleteContact(contactId.longValue(), personId);
    }
  }

}