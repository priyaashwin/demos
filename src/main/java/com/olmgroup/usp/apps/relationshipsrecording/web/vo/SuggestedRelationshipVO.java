package com.olmgroup.usp.apps.relationshipsrecording.web.vo;

import java.util.Date;

import com.olmgroup.usp.components.relationship.domain.RelationshipRole;
import com.olmgroup.usp.components.relationship.vo.PersonPersonRelationshipQualifier;

public class SuggestedRelationshipVO {

	private long relationshipTypeId;
	private String relationshipLabel;
	private String discriminatorType;
	private Date relationshipStartDate;
	private Boolean relationshipStartDateEstimated;
	
	
	public String getDiscriminatorType() {
		return discriminatorType;
	}
	public void setDiscriminatorType(String discriminatorType) {
		this.discriminatorType = discriminatorType;
	}
	private RelationshipRole role;
	private PersonPersonRelationshipQualifier personPersonRelationshipQualifier;
	
	public PersonPersonRelationshipQualifier getPersonPersonRelationshipQualifier() {
		return personPersonRelationshipQualifier;
	}
	public void setPersonPersonRelationshipQualifier(
			PersonPersonRelationshipQualifier personPersonRelationshipQualifier) {
		this.personPersonRelationshipQualifier = personPersonRelationshipQualifier;
	}
	public long getRelationshipTypeId() {
		return relationshipTypeId;
	}
	public void setRelationshipTypeId(long relationshipTypeId) {
		this.relationshipTypeId = relationshipTypeId;
	}
	public String getRelationshipLabel() {
		return relationshipLabel;
	}
	public void setRelationshipLabel(String relationshipLabel) {
		this.relationshipLabel = relationshipLabel;
	}
	public RelationshipRole getRole() {
		return role;
	}
	public void setRole(RelationshipRole role) {
		this.role = role;
	}
	public Date getRelationshipStartDate() {
		return relationshipStartDate;
	}
	public void setRelationshipStartDate(Date relationshipStartDate) {
		this.relationshipStartDate = relationshipStartDate;
	}
	public Boolean getRelationshipStartDateEstimated() {
		return relationshipStartDateEstimated;
	}
	public void setRelationshipStartDateEstimated(
			Boolean relationshipStartDateEstimated) {
		this.relationshipStartDateEstimated = relationshipStartDateEstimated;
	}
	
}
