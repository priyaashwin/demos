// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.facets.mail.SmtpConfig;
import com.olmgroup.usp.facets.security.authentication.provider.LdapConfig;
import com.olmgroup.usp.facets.sms.SmsConfig;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SystemConfigurationViewController
 */
@Component
final class SystemConfigurationViewControllerHelperImpl extends SystemConfigurationViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private SmsConfig smsConfig;

  @Lazy
  @Inject
  private LdapConfig ldapConfig;

  @Lazy
  @Inject
  private SmtpConfig smtpConfig;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.SystemConfigurationViewController#viewConfigurationDetials(WebRequest
   *      webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewConfigurationDetials(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    this.getContextHelperService().setupModelForContext(model);
    model.addAttribute("smsConfig", smsConfig.getPropertyHolder());
    model.addAttribute("ldapConfig", ldapConfig.getPropertyHolder());
    model.addAttribute("smtpConfig", smtpConfig.getPropertyHolder());
    return "systemConfig.page";

  }
}