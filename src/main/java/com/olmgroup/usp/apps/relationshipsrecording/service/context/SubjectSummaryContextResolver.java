package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

/**
 * Resolve the subject summary context for the security profile
 * 
 * @author Richard.Gibson
 */
public interface SubjectSummaryContextResolver extends LazilyInitialized {

  String SUBJECT_SUMMARY_CONTEXT = "subject-summary";

  /**
   * Resolve the SubjectSummary context to one of the values of {@link SubjectSummaryContext}.
   *
   * @return The resolved SubjectSummaryContext
   */
  SubjectSummaryContext resolveSubjectSummaryContext();

}
