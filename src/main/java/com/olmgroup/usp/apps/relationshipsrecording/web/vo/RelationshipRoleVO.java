package com.olmgroup.usp.apps.relationshipsrecording.web.vo;


import com.olmgroup.usp.components.relationship.domain.PersonPersonRelationshipDate;

public class RelationshipRoleVO {

  String id;
  String discriminator;
  String relationshipClass;
  String relationshipClassName;
  String roleClass;
  String name;
  String gender;
  boolean isAdoptiveApplicable;
  boolean isInLawApplicable;
  boolean isStepApplicable;
  boolean isHalfFullApplicable;
  boolean isMultipleBirthApplicable;
  String description;
  Integer minAncestorAge; 
  boolean isAncestorRole;
  Boolean enforceAncestorCheck;
  Boolean notManuallyAssignable;
  
  
    public Boolean isEnforceAncestorCheck() {
    return enforceAncestorCheck;
  }
  public void setEnforceAncestorCheck(Boolean enforceAncestorCheck) {
    this.enforceAncestorCheck = enforceAncestorCheck;
  }
  PersonPersonRelationshipDate startDateGuidance;
    
  
  public PersonPersonRelationshipDate getstartDateGuidance() {
    return startDateGuidance;
  }
  public void setstartDateGuidance(PersonPersonRelationshipDate startDateGuidance) {
    this.startDateGuidance=startDateGuidance;
  }
  
  public boolean getIsAncestorRole() {
    return isAncestorRole;
  }
  public void setIsAncestorRole(boolean isAncestorRole) {
    this.isAncestorRole=isAncestorRole;
  }
  public Integer getMinAncestorAge() {
    return minAncestorAge;
  }
  public void setMinAncestorAge(Integer minAncestorAge) {
    this.minAncestorAge=minAncestorAge;
  }
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  
  public String getDiscriminator() {
    return discriminator;
  }
  public void setDiscriminator(String discriminator) {
    this.discriminator = discriminator;
  }
  public String getRelationshipClass() {
    return relationshipClass;
  }
  public void setRelationshipClass(String relationshipClass) {
    this.relationshipClass = relationshipClass;
  }
  
  public String getRelationshipClassName() {
    return relationshipClassName;
  }
  public void setRelationshipClassName(String relationshipClassName) {
    this.relationshipClassName = relationshipClassName;
  }
  
  public String getRoleClass() {
    return roleClass;
  }
  public void setRoleClass(String roleClass) {
    this.roleClass = roleClass;
  }
  
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  public String getGender() {
    return gender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  
  public boolean getIsAdoptiveApplicable() {
    return isAdoptiveApplicable;
  }
  public void setIsAdoptiveApplicable(boolean isAdoptiveApplicable) {
    this.isAdoptiveApplicable = isAdoptiveApplicable;
  }
  
  public boolean getIsInLawApplicable() {
    return isInLawApplicable;
  }
  public void setIsInLawApplicable(boolean isInLawApplicable) {
    this.isInLawApplicable = isInLawApplicable;
  }
  
  public boolean getIsStepApplicable() {
    return isStepApplicable;
  }
  public void setIsStepApplicable(boolean isStepApplicable) {
    this.isStepApplicable = isStepApplicable;
  }
  
  public boolean getIsHalfFullApplicable() {
    return isHalfFullApplicable;
  }
  public void setIsHalfFullApplicable(boolean isHalfFullApplicable) {
    this.isHalfFullApplicable = isHalfFullApplicable;
  }
  
  public boolean getIsMultipleBirthApplicable() {
    return isMultipleBirthApplicable;
  }

  public void setIsMultipleBirthApplicable(boolean isMultipleBirthApplicable) {
    this.isMultipleBirthApplicable = isMultipleBirthApplicable;
  }
  
  public Boolean getNotManuallyAssignable( ){
    return notManuallyAssignable;
  }
  
  public void setNotManuallyAssignable(Boolean notManuallyAssignable){
    this.notManuallyAssignable = notManuallyAssignable;
  }
  
}
