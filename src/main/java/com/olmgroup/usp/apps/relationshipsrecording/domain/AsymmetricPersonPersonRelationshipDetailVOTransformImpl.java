// license-header java merge-point
//
// Generated by: hibernate/SpringValueObjectTargetTransformImpl.vsl in andromda-spring-cartridge.
//
package com.olmgroup.usp.apps.relationshipsrecording.domain;

import com.olmgroup.usp.apps.relationshipsrecording.vo.AsymmetricPersonPersonRelationshipDetailVO;
import com.olmgroup.usp.components.person.domain.Person;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.components.relationship.domain.PersonPersonRelationship;
import com.olmgroup.usp.components.relationship.domain.PersonPersonRelationshipType;
import com.olmgroup.usp.components.relationship.domain.RelationshipRoleGenderType;
import com.olmgroup.usp.components.relationship.vo.PersonPersonRelationshipTypeVO;
import com.olmgroup.usp.facets.spring.ContextSupport;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import java.util.Collection;

import javax.inject.Inject;

/**
 * @see PersonPersonRelationship
 */
@Component("asymmetricPersonPersonRelationshipDetailVOTransform")
final class AsymmetricPersonPersonRelationshipDetailVOTransformImpl extends AsymmetricPersonPersonRelationshipDetailVOTransformBase {

  @Lazy
  @Inject
  private ContextSupport contextSupport;

  private static final String STEP_ROLE_QUALIFIER = "Step-";
  private static final String ADOPTIVE_ROLE_QUALIFIER = "Adoptive-";
  private static final String HALF_ROLE_QUALIFIER = "Half-";
  private static final String INLAW_ROLE_QUALIFIER = "-in-law";

  private static final String UNION = "UNION";
  private static final String UNKNOWN = "UNKNOWN";
  private static final String INDETERMINATE = "INDETERMINATE";

  @Override
  protected void handleEntityToValueObject(
      final PersonPersonRelationship source,
      final AsymmetricPersonPersonRelationshipDetailVO target) {

    Person nominatedPerson = getPersonDao().get(
        source.getNominatedPersonId());
    PersonVO nominatedPersonVO = getPersonDao().getObjectMapper().map(
        nominatedPerson, PersonVO.class);

    String roleAClass = null;
    String roleBClass = null;
    if (source.getPersonPersonRelationshipType() != null) {
      roleAClass = source.getPersonPersonRelationshipType()
          .getRoleAClass();
      roleBClass = source.getPersonPersonRelationshipType()
          .getRoleBClass();
    }

    Long nominatedPersonId = source.getNominatedPersonId();
    Long targetPersonId = null;
    if (nominatedPersonId != null) {
      if (!source.getRoleAPerson().getId().equals(nominatedPersonId)) {
        targetPersonId = source.getRoleAPerson().getId();
        target.setRoleClass(roleAClass);
      } else {
        targetPersonId = source.getRoleBPerson().getId();
        target.setRoleClass(roleBClass);
      }
    }

    Person targetPerson = getPersonDao().get(targetPersonId);
    PersonVO targetPersonVO = getPersonDao().getObjectMapper().map(
        targetPerson, PersonVO.class);

    target.setNominatedPersonForename(nominatedPersonVO.getForename());
    target.setNominatedPersonSurname(nominatedPersonVO.getSurname());
    target.setNominatedPersonName(nominatedPersonVO.getName());
    target.setNominatedPersonIdentifier(nominatedPersonVO
        .getPersonIdentifier());

    PersonPersonRelationshipType personPersonRelationshipType = source
        .getPersonPersonRelationshipType();
    String relationshipName = null;
    if (personPersonRelationshipType != null) {
      PersonPersonRelationshipTypeVO personPersonRelationshipTypeVO = getPersonPersonRelationshipTypeDao()
          .getObjectMapper().map(personPersonRelationshipType,
              PersonPersonRelationshipTypeVO.class);
      target.setTypeDiscriminator(personPersonRelationshipTypeVO
          .getDiscriminatorType());

      if (targetPersonVO.getGender() != null) {

        Person roleAPerson = source.getRoleAPerson();
        Person roleBPerson = source.getRoleBPerson();
        if (personPersonRelationshipTypeVO.getDescription() != null
            && personPersonRelationshipTypeVO.getDescription()
                .equalsIgnoreCase(UNION)
            && (roleAPerson.getGender() == null
                || roleBPerson.getGender() == null
                || roleAPerson.getGender().equalsIgnoreCase(
                    UNKNOWN)
                || roleAPerson.getGender().equalsIgnoreCase(
                    INDETERMINATE)
                || roleBPerson.getGender().equalsIgnoreCase(
                    UNKNOWN)
                || roleBPerson.getGender().equalsIgnoreCase(
                    INDETERMINATE)
                || roleAPerson
                    .getGender().equalsIgnoreCase(
                        roleBPerson.getGender()))) {

          relationshipName = RelationshipsHelper
              .getGenderFreeUnionRelationshipLabel(personPersonRelationshipTypeVO
                  .getRelationshipClassName());

        } else {
          Collection<RelationshipRoleGenderType> relationshipRoleGenderTypes = personPersonRelationshipType
              .getRelationshipRoleGenderTypes();
          for (RelationshipRoleGenderType relationshipRoleGenderType : relationshipRoleGenderTypes) {
            if (relationshipRoleGenderType.getGender().equals(
                targetPersonVO.getGender())) {
              if (personPersonRelationshipType.getRoleAClass()
                  .equals(target.getRoleClass())) {
                relationshipName = relationshipRoleGenderType
                    .getRoleAName();
              } else {
                relationshipName = relationshipRoleGenderType
                    .getRoleBName();
              }
            }
          }
        }
      }
      if (relationshipName == null) {
        if (personPersonRelationshipType.getRoleAClass().equals(
            target.getRoleClass())) {
          relationshipName = personPersonRelationshipType
              .getRoleAName();
        } else {
          relationshipName = personPersonRelationshipType
              .getRoleBName();
        }
      }

      if (source.getStep() != null && source.getStep()) {
        relationshipName = STEP_ROLE_QUALIFIER + relationshipName;
      }

      if (source.getHalf() != null && source.getHalf()) {
        relationshipName = HALF_ROLE_QUALIFIER + relationshipName;
      }
      if (source.getInLaw() != null && source.getInLaw()) {
        relationshipName = relationshipName + INLAW_ROLE_QUALIFIER;
      }
      if (source.getAdoptive() != null && source.getAdoptive()) {
        relationshipName = ADOPTIVE_ROLE_QUALIFIER + relationshipName;
      }
      target.setRelationship(relationshipName);

      target.setRelationshipClass(personPersonRelationshipType
          .getRelationshipClass());
      target.setRelationshipClassName(personPersonRelationshipType
          .getRelationshipClassName());
    } else {
      MessageSourceAccessor messageSourceAccessor = contextSupport
          .getAccessorForMessageSource();
      // 3337, Wu, Andy said leave it blank for now for relationship role
      // target.setRelationship(messageSourceAccessor.getMessage("person.relationships.other.results.relationshipName",
      // "Not Recorded"));
      target.setRelationshipClass(messageSourceAccessor.getMessage(
          "person.relationships.other.results.relationshipType",
          "Other"));
      target.setRelationshipClassName(messageSourceAccessor.getMessage(
          "person.relationships.other.results.relationshipTypeName",
          "Not recorded"));
    }
  }

}