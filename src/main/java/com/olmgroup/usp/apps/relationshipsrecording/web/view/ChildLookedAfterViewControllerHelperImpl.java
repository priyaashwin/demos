// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.childlookedafter.service.context.ClaEpisodesContextResolver;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildLookedAfterViewController
 */
@Component
final class ChildLookedAfterViewControllerHelperImpl extends ChildLookedAfterViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private ClaEpisodesContextResolver contextResolver;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildLookedAfterViewController#childLookedAfterView(
   *      final long idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleChildLookedAfterView(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    setupModelForView(idParam, model);

    return "childlookedafter";
  }

  private void setupModelForView(final long personId, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);
    this.getContextHelperService().setupModelForContext(model);
  }
}