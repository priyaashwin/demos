package com.olmgroup.usp.apps.relationshipsrecording.utils;

import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

import org.springframework.ui.Model;

public interface RelationshipTypesUtils extends LazilyInitialized {

  void setupModelWithPersonalRelationshipRoles(Model model) throws Exception;

  void setupModelWithProfessionalRelationshipRoles(Model model) throws Exception;

}
