package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlEnum
@XmlType(name = "PrimaryReferenceNumberContext")
public enum PrimaryReferenceNumberContext {

  @XmlEnumValue("CHI") CHI,

  /**
   * The default context supporting default behaviour of reference numbers.
   */
  @XmlEnumValue("DEFAULT") DEFAULT;

}
