// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonGroupMembershipDetailsWithMembersAndCountVO;
import com.olmgroup.usp.facets.security.authorisation.instance.SecurityType;
import com.olmgroup.usp.facets.security.authorisation.instance.annotation.SupportsSecurityTypes;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * <p>
 * Completes the implementation of the {@link PersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandler} by
 * implementing abstract methods defined in {@link PersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandlerBase}
 * .
 * </p>
 * <p>
 * This provides a {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 * SecurityHandler} for the {@link PersonGroupMembershipDetailsWithMembersAndCountVO} value object, forming the basis
 * for providing Instance Level Security.
 * </p>
 * 
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 */
@Component("personGroupMembershipDetailsWithMembersAndCountVOSecurityHandler")
@SupportsSecurityTypes({ SecurityType.RELATIONAL })
final class PersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandlerImpl
    extends
    PersonGroupMembershipDetailsWithMembersAndCountVOSecurityHandlerBase {

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = -2388321667110920110L;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final PersonGroupMembershipDetailsWithMembersAndCountVO targetDomainObject) {
    return this.getPersonGroupMembershipDetailsVOSecurityHandler()
        .getSubjects(targetDomainObject);
  }

}