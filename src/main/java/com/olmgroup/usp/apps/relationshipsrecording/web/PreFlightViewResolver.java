package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * Interface which defines a set of methods for a <code>PreFlightViewResolver</code>.
 * Implementations can be used by controllers in order to resolve a view, prior to
 * building any model. Typical usages facilitate conditional redirection.
 * 
 * @author mylesl
 */
public interface PreFlightViewResolver extends LazilyInitialized {

  /**
   * Attempt to retrieve a 'pre-flight' view name for the person subject
   * specified by <code>personId</code>.
   * 
   * @param personId
   *          the ID of the person subject in context
   * @param webRequest
   *          the <code>WebRequest</code> object passed to the controller
   * @param servletResponse
   *          the <code>HttpServletResponse</code> object passed to
   *          the controller
   * @param model
   *          the <code>Model</code> object passed to the controller
   * @return a view name, or null if a view name could not be retrieved
   */
  String getViewNameForPersonSubject(long personId, WebRequest webRequest,
      HttpServletResponse servletResponse, Model model);

  /**
   * Attempt to retrieve a 'pre-flight' view name for the group subject
   * specified by <code>groupId</code>.
   * 
   * @param groupId
   *          the ID of the group subject in context
   * @param webRequest
   *          the <code>WebRequest</code> object passed to the controller
   * @param servletResponse
   *          the <code>HttpServletResponse</code> object passed to
   *          the controller
   * @param model
   *          the <code>Model</code> object passed to the controller
   * @return a view name, or null if a view name could not be retrieved
   */
  String getViewNameForGroupSubject(long groupId, WebRequest webRequest,
      HttpServletResponse servletResponse, Model model);
}
