// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.utils.RelationshipTypesUtils;
import com.olmgroup.usp.components.configuration.vo.CategoryContextVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.PersonViewController
 */
@Component
final class PersonViewControllerHelperImpl extends PersonViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private RelationshipTypesUtils relationshipTypesUtils;

  private RelationshipTypesUtils getRelationshipTypesUtils() {
    return this.relationshipTypesUtils;
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. PersonViewController#personView(final Long idParam,
   *      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonView(final Long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model)
      throws Exception {
    this.getRelationshipTypesUtils().setupModelWithProfessionalRelationshipRoles(model);
    model.addAttribute("currentUser", getPersonService().getCurrentPerson());
    setupModelForView(idParam, model);
    setupModelWithSubdivisionLookupId(model);
    return "person";
  }

  private void setupModelForView(final long personId, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);
    this.getContextHelperService().setupModelForContext(model);

  }

  private void setupModelWithSubdivisionLookupId(final Model model) {
    final CategoryContextVO categoryContextVO = getCategoryContextService()
        .findCategoryContextByCode("com.olmgroup.usp.components.address.categoryContext.SubdivisionsForCountry");
    if (null != categoryContextVO) {
      model.addAttribute("countrySubdivisionContextId", categoryContextVO.getId());
    }

  }

}
