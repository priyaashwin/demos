// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.DemoViewController
 */
@Controller
final class DemoViewControllerHelperImpl extends DemoViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.DemoViewController#viewDemo(WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewDemo(WebRequest webRequest, HttpServletResponse servletResponse, Model model)
        throws Exception
  {
    this.getContextHelperService().setupModelForContext(model);
    return "demo";
  }
}