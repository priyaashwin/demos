// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.components.resolveassessment.domain.FormStatus;
import com.olmgroup.usp.components.resolveassessment.service.CarePlanRiskAssessmentService;
import com.olmgroup.usp.components.resolveassessment.service.IntakeFormService;
import com.olmgroup.usp.components.resolveassessment.service.PlacementReportService;
import com.olmgroup.usp.components.resolveassessment.vo.CarePlanRiskAssessmentSearchResultVO;
import com.olmgroup.usp.components.resolveassessment.vo.IntakeFormSearchResultVO;
import com.olmgroup.usp.components.resolveassessment.vo.NewCarePlanRiskAssessmentVO;
import com.olmgroup.usp.components.resolveassessment.vo.NewIntakeFormVO;
import com.olmgroup.usp.components.resolveassessment.vo.NewPlacementReportVO;
import com.olmgroup.usp.components.resolveassessment.vo.PlacementReportSearchResultVO;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ResolveFormsViewController
 */
@Component
final class ResolveFormsViewControllerHelperImpl extends ResolveFormsViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  private static final List<FormStatus> NON_AUTHORISED_STATUSES = new ArrayList<FormStatus>();
  static {
    NON_AUTHORISED_STATUSES.add(FormStatus.DRAFT);
    NON_AUTHORISED_STATUSES.add(FormStatus.COMPLETE);
  }

  private static final String PRFORM_VIEW = "prform.view";
  private static final String CPRAFORM_VIEW = "cpraform.view";
  private static final String INTAKEFORM_VIEW = "intakeform.view";

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. ResolveFormsViewController#viewResolveForms(final long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewResolveForms(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelForView(idParam, model);
    this.getContextHelperService().setupModelForContext(model);
    return "resolveforms";
  }

  @Override
  public String handleViewNonAuthorisedPlacementReport(final long personId,
      final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {

    this.getContextHelperService().setupModelForContext(model);

    // Get the non-authorised PlacementReport for the specified person
    // (there should only ever be one)
    final PlacementReportService placementReportService = getPlacementReportService();
    final PaginationResult<PlacementReportSearchResultVO> formResults = placementReportService
        .findBySubjectAndStatus(personId, NON_AUTHORISED_STATUSES, 1, 1,
            null);

    // If no non-authorised form found, then create one right now
    if (formResults.getResults().isEmpty()) {
      final NewPlacementReportVO newPlacementReportVO = new NewPlacementReportVO();
      final long formId = placementReportService.addPlacementReport(personId,
          newPlacementReportVO);

      // Set the redirect URL
      return "redirect:/forms/resolve/place/" + formId + "/background";
    } else {
      // Set the redirect URL
      return "redirect:/forms/resolve/place/"
          + formResults.getResults().get(0).getId() + "/background";
    }

  }

  @Override
  public String handleViewPlacementReport(final long formId, final Boolean print,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {

    this.getContextHelperService().setupModelForContext(model);

    final long personId = this.getPlacementReportService().findById(formId)
        .getSubject().getId();

    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(personId, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }
    if (Boolean.TRUE.equals(print)) {
      return "prform.print.view";
    } else {
      // put the person into the model - using the subject id from the
      // form
      setupModelForView(personId, model);
      return PRFORM_VIEW;
    }
  }

  @Override
  public String handleViewPlacementReportTab(final long formId, final String tabName,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {

    return this.handleViewPlacementReport(formId, false, webRequest, servletResponse, model);
  }

  @Override
  public String handleViewNonAuthorisedCPRA(final long personId,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    this.getContextHelperService().setupModelForContext(model);

    // Get the draft CarePlanRiskAssessment for the specified person (there
    // should only ever be one)
    final CarePlanRiskAssessmentService carePlanRiskAssessmentService = getCarePlanRiskAssessmentService();
    final PaginationResult<CarePlanRiskAssessmentSearchResultVO> formResults = carePlanRiskAssessmentService
        .findBySubjectAndStatus(personId, NON_AUTHORISED_STATUSES, 1, 1,
            null);

    // If no non-authorised form found, then create one right now
    if (formResults.getResults().isEmpty()) {
      final NewCarePlanRiskAssessmentVO newCarePlanRiskAssessmentVO = new NewCarePlanRiskAssessmentVO();
      final long formId = carePlanRiskAssessmentService
          .addCarePlanRiskAssessment(personId,
              newCarePlanRiskAssessmentVO);

      // Set the redirect URL
      return "redirect:/forms/resolve/cpra/" + formId + "/background";
    } else {
      // Set the redirect URL
      return "redirect:/forms/resolve/cpra/"
          + formResults.getResults().get(0).getId() + "/background";
    }
  }

  @Override
  public String handleViewCPRA(final long formId, final Boolean print,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {
    final long personId = this.getCarePlanRiskAssessmentService()
        .findById(formId).getSubject().getId();

    this.getContextHelperService().setupModelForContext(model);

    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(personId, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    if (Boolean.TRUE.equals(print)) {
      return "cpraform.print.view";
    } else {
      // put the person into the model - using the subject id from the
      // form
      setupModelForView(personId, model);
      return CPRAFORM_VIEW;
    }
  }

  @Override
  public String handleViewCPRATab(final long formId, final String tabName,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {

    return this.handleViewCPRA(formId, false, webRequest, servletResponse, model);
  }

  private void setupModelForView(final long personId, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);
  }

  @Override
  public String handleViewNonAuthorisedIntake(final long personId,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    this.getContextHelperService().setupModelForContext(model);

    // Get the non-authorised Intake form for the specified person (there
    // should only ever be one)
    IntakeFormService intakeFormService = getIntakeFormService();
    PaginationResult<IntakeFormSearchResultVO> formResults = intakeFormService
        .findBySubjectAndStatus(personId, NON_AUTHORISED_STATUSES, 1, 1,
            null);

    // If no non-authorised form found, then create one right now
    if (formResults.getResults().isEmpty()) {
      NewIntakeFormVO newIntakeFormVO = new NewIntakeFormVO();
      long formId = intakeFormService.addIntakeForm(personId,
          newIntakeFormVO);

      // Set the redirect URL
      return "redirect:/forms/resolve/intake/" + formId + "/client";
    } else {
      // Set the redirect URL
      return "redirect:/forms/resolve/intake/"
          + formResults.getResults().get(0).getId() + "/client";
    }
  }

  @Override
  public String handleViewIntake(final long formId, final Boolean print,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    this.getContextHelperService().setupModelForContext(model);

    final long personId = this.getIntakeFormService().findById(formId)
        .getSubject().getId();

    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(personId, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    if (Boolean.TRUE.equals(print)) {
      return "intakeform.print.view";
    } else {
      // put the person into the model - using the subject id from the
      // form
      setupModelForView(personId, model);
      return INTAKEFORM_VIEW;
    }
  }

  @Override
  public String handleViewIntakeTab(final long formId, final String tabName,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    return this.handleViewIntake(formId, false, webRequest, servletResponse, model);
  }
}