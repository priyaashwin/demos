package com.olmgroup.usp.apps.relationshipsrecording.service.security.context;

import com.olmgroup.usp.apps.relationshipsrecording.service.context.PrimaryReferenceNumberContext;
import com.olmgroup.usp.apps.relationshipsrecording.service.context.PrimaryReferenceNumberContextResolver;
import com.olmgroup.usp.facets.security.authentication.context.attributes.annotation.ForContextAttribute;
import com.olmgroup.usp.facets.security.authentication.context.attributes.loader.AbstractContextAttributeLoaderImpl;
import com.olmgroup.usp.facets.security.authentication.vo.SecurityTeam;
import com.olmgroup.usp.facets.security.authentication.vo.UserProfile;

import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component("primaryReferenceNumberContextAttributeLoader")
@ForContextAttribute(PrimaryReferenceNumberContextResolver.PRIMARY_REFERENCE_NUMBER_CONTEXT)
final class PrimaryReferenceNumberContextAttributeLoaderImpl extends AbstractContextAttributeLoaderImpl {
  static final String REGION_SCOTLAND = "S";
  static final String REGION_NORTHERN_IRELAND = "N";

  @Override
  public Serializable getAttributeValue(UserProfile userProfile) {
    final SecurityTeam securityTeam = userProfile.getSecurityProfile().getSecurityTeam();

    if (securityTeam != null && securityTeam.getRegionCode() != null) {
      final String regionCode = securityTeam.getRegionCode();
      if (REGION_SCOTLAND.equals(regionCode) || (REGION_NORTHERN_IRELAND.equals(regionCode))) {
        return PrimaryReferenceNumberContext.CHI.toString();
      }
    }
    return PrimaryReferenceNumberContext.DEFAULT.toString();
  }

}
