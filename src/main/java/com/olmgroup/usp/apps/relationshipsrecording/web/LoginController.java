package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.facets.core.url.AbsoluteURLResolver;
import com.olmgroup.usp.facets.core.url.URLResolverException;
import com.olmgroup.usp.facets.security.authentication.context.UserContextService;
import com.olmgroup.usp.facets.security.authentication.exception.EmailAddressNotFoundException;
import com.olmgroup.usp.facets.security.authentication.exception.InvalidTwoFactorSmsContactNumberException;
import com.olmgroup.usp.facets.security.authentication.exception.SecurityUserEmailAddressNotFoundException;
import com.olmgroup.usp.facets.security.authentication.exception.SelfPasswordResetServiceException;
import com.olmgroup.usp.facets.security.authentication.exception.UserIdNotFoundException;
import com.olmgroup.usp.facets.security.authentication.service.BypassSecondAuthenticationService;
import com.olmgroup.usp.facets.security.authentication.service.SecondaryPasswordManager;
import com.olmgroup.usp.facets.security.authentication.service.SecurityUserLockoutManager;
import com.olmgroup.usp.facets.security.authentication.service.SecurityUserPasswordUpdateService;
import com.olmgroup.usp.facets.security.authentication.service.SecurityUserSelfPasswordResetService;
import com.olmgroup.usp.facets.security.authentication.service.twofactorsms.TwoFactorSmsAuthenticationService;
import com.olmgroup.usp.facets.security.authentication.service.twofactorsms.TwoFactorSmsAuthenticationServiceException;
import com.olmgroup.usp.facets.security.authentication.userdetails.LoginDetails;
import com.olmgroup.usp.facets.security.authentication.userdetails.UserDetailsBuilder;
import com.olmgroup.usp.facets.security.authentication.vo.PasswordResetRequestVO;
import com.olmgroup.usp.facets.security.authentication.vo.SecondaryPasswordVO;
import com.olmgroup.usp.facets.security.authentication.vo.SecurityChallengeVerificationVO;
import com.olmgroup.usp.facets.security.authentication.vo.SecurityUser;
import com.olmgroup.usp.facets.security.authentication.vo.TwoFactorSmsVerificationVO;
import com.olmgroup.usp.facets.security.authentication.vo.UpdatePasswordDetailsVO;
import com.olmgroup.usp.facets.security.authentication.www.BypassTokenManager;
import com.olmgroup.usp.facets.security.config.exception.MultipleSecurityConfigsFoundException;
import com.olmgroup.usp.facets.security.config.exception.SecurityConfigNotFoundException;
import com.olmgroup.usp.facets.security.config.service.SecurityConfigService;
import com.olmgroup.usp.facets.security.utils.AuthenticationTools;
import com.olmgroup.usp.facets.security.validation.Password;
import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;
import com.olmgroup.usp.facets.springmvc.validation.ErrorWithMessage;
import com.olmgroup.usp.facets.springmvc.validation.ValidationFieldError;
import com.olmgroup.usp.facets.springmvc.validation.ValidationObjectError;
import com.olmgroup.usp.facets.validation.FieldMatch;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

/**
 * Controller supporting login views for ECLIPSE.
 * 
 * @author robd
 */
@Controller
@Named("loginViewController")
@RequestMapping("/login")
class LoginController implements MessageSourceAware, LazilyInitialized {

  private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

  @Lazy
  @Inject
  @Named("userContextService")
  private UserContextService userContextService;

  @Lazy
  @Inject
  @Named("authenticationTools")
  private AuthenticationTools authenticationTools;

  @Lazy
  @Inject
  private SecurityUserPasswordUpdateService securityUserPasswordService;

  private MessageSource messageSource;

  @Lazy
  @Inject
  @Named("securityUserSelfPasswordResetService")
  private SecurityUserSelfPasswordResetService securityUserSelfPasswordResetService;

  @Lazy
  @Inject
  @Named("securityConfigService")
  private SecurityConfigService securityConfigService;

  @Lazy
  @Inject
  private Optional<SecurityUserLockoutManager> securityUserLockoutManager;

  @Lazy
  @Inject
  private SecondaryPasswordManager secondaryPasswordManager;

  @Lazy
  @Inject
  private AbsoluteURLResolver absoluteUrlResolver;

  @Lazy
  @Inject
  private TwoFactorSmsAuthenticationService twoFactorSmsAuthenticationService;

  @Lazy
  @Inject
  private BypassSecondAuthenticationService bypassSecondAuthenticationService;

  @Lazy
  @Inject
  private BypassTokenManager bypassTokenManager;

  @Override
  public void setMessageSource(final MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  @RequestMapping(method = RequestMethod.GET)
  public String processLoginRequest(final ModelMap model) {
    logger.debug("Processing the login page request.");
    final boolean forgotPasswordEnabled = determineSelfPasswordResetIsEnabled();
    model.addAttribute("forgotPasswordEnabled", forgotPasswordEnabled);
    final boolean termsAndConditionsEnabled = determineTermsAndConditionsIsEnabled();
    model.addAttribute("termsAndConditionsEnabled", termsAndConditionsEnabled);
    if (termsAndConditionsEnabled) {
      final String termsAndConditionsUrl = getUrlForTermsAndConditions();
      model.addAttribute("termsAndConditionsUrl", termsAndConditionsUrl);
    }
    return "login";
  }

  @RequestMapping(value = "/disabled", method = RequestMethod.GET)
  public String processLoginDisabled(final RedirectAttributes attrs) {
    logger.debug("Processing the disabled user page request.");
    final String message = "Your user account is disabled.";
    attrs.addFlashAttribute("loginErrorMessage", message);
    return "login";
  }

  @RequestMapping(value = "/locked", method = RequestMethod.GET)
  public String processLoginLocked(final RedirectAttributes attrs) {
    logger.debug("Processing the locked user page request.");
    final String message = "Your user account is locked.";
    attrs.addFlashAttribute("loginErrorMessage", message);
    return "redirect:/login";
  }

  @RequestMapping(value = "/invalid", method = RequestMethod.GET)
  public String processLoginInvalid(final RedirectAttributes attrs) {
    logger.debug("Processing the invalid login page request.");
    final String message = "Please enter a valid username and password.";
    attrs.addFlashAttribute("loginErrorMessage", message);
    final boolean forgotPasswordEnabled = determineSelfPasswordResetIsEnabled();
    attrs.addFlashAttribute("forgotPasswordEnabled", forgotPasswordEnabled);
    return "redirect:/login";
  }

  @RequestMapping(value = "/insufficient", method = RequestMethod.GET)
  public String processLoginInsufficient(final RedirectAttributes attrs) {
    logger.debug("Processing the insufficient login page request.");
    final String message = "There was an error processing your login, please try again.";
    attrs.addFlashAttribute("loginErrorMessage", message);
    return "redirect:/login";
  }

  @RequestMapping(value = "/error", method = RequestMethod.GET)
  public String processLoginError(final RedirectAttributes attrs) {
    logger.debug("Processing the login error (service / unknown) page request.");
    final String message = "There was an error processing your login, please try again.";
    attrs.addFlashAttribute("loginErrorMessage", message);
    final boolean forgotPasswordEnabled = determineSelfPasswordResetIsEnabled();
    attrs.addFlashAttribute("forgotPasswordEnabled", forgotPasswordEnabled);
    return "redirect:/login";
  }

  @RequestMapping(value = "/expired", method = RequestMethod.GET)
  public String processLoginExpired(final RedirectAttributes atts) {
    logger.debug("Processing the expired user page request.");
    final String message = "Your user account has expired.";
    atts.addFlashAttribute("loginErrorMessage", message);
    return "redirect:/login";
  }

  @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
  public String processAccessDenied(final ModelMap model) {
    logger.debug("Processing the change password page request.");
    final String message = "Access to the requested resource is denied.";
    model.addAttribute("loginErrorMessage", message);
    return "accessDenied";
  }

  @RequestMapping(value = "/missingProfile", method = RequestMethod.GET)
  public String processMissingProfile(final RedirectAttributes atts) {
    logger.debug("Processing the missing profile page request.");
    final String message = "Your user account has no profile assigned. Please contact the administrator.";
    atts.addFlashAttribute("loginErrorMessage", message);
    return "redirect:/login";
  }

  @RequestMapping(value = "/failure", method = RequestMethod.GET)
  public String processLoginFailure(final RedirectAttributes atts, final Locale locale) {
    logger.debug("Processing the login failure (user error) page request.");
    final String defaultMessage = "Failed to login. You may have used invalid user details or your account may be locked. Contact the administrator for further assistance.";
    final String message = messageSource.getMessage("authentication.error.user", null, defaultMessage, locale);
    atts.addFlashAttribute("loginErrorMessage", message);
    final boolean forgotPasswordEnabled = determineSelfPasswordResetIsEnabled();
    atts.addFlashAttribute("forgotPasswordEnabled", forgotPasswordEnabled);
    return "redirect:/login";
  }

  @RequestMapping(value = "/secondaryPassword", method = RequestMethod.GET)
  public String processIssueSecondaryPasswordChallenge(final ModelMap model, final Locale locale) {
    logger.debug("Processing the issue secondary password challenge page request.");

    final LoginDetails loginDetails = userContextService.getLoginDetails();

    if (!loginDetails.mustProvideSecondaryPassword()) {
      logger.debug("User does not need to provide secondary password. Redirecting to /index");
      return "redirect:/index";
    }

    final SecurityUser userSummary = userContextService.getUserSummary();
    final List<Integer> characterIndices = secondaryPasswordManager
        .getChallengeCharacters(userSummary.getId())
        .getCharacterIndices();

    final String message = messageSource.getMessage(
        "authentication.secondaryPassword.validation.infoMessage",
        new Integer[] { characterIndices.get(0), characterIndices.get(1), characterIndices.get(2) },
        "Please enter the required characters from your passphrase", locale);

    final Map<String, Integer> challengeChars = this
        .getSecondaryPasswordChallengeCharsAsModelAttrs(characterIndices);

    model.addAllAttributes(challengeChars);
    model.addAttribute("loginInfoMessage", message);

    return "loginSecondaryPassword";
  }

  @RequestMapping(value = "/secondaryPassword", method = RequestMethod.POST)
  public String processSecondaryPasswordChallengeValidation(
      @ModelAttribute("securityChallengeVerificationVO") @Valid final SecurityChallengeVerificationVO securityChallengeVerificationVO,
      final BindingResult result, final RedirectAttributes atts, final ModelMap model, final Locale locale, final HttpServletRequest request, final HttpServletResponse response) {

    logger.debug("Processing the secondary password challenge validation page request.");

    final Authentication auth = userContextService.getCurrentAuthentication();
    final SecurityUser userSummary = userContextService.getUserSummary();
    final Long userId = userSummary.getId();

    try {
      authenticationTools.completeTwoStepAuthentication(auth, securityChallengeVerificationVO);

      final String bypassToken = bypassSecondAuthenticationService.registerBypassToken(userId);
      final int bypassTokenDuration = (int) securityConfigService.getMultipleLoginTimeout();
      bypassTokenManager.addBypassToken(request, response, bypassToken, bypassTokenDuration);
    } catch (Exception ex) {
      final String message;

      // The partially authenticated user has become locked
      if (LockedException.class.isInstance(ex)) {
        message = messageSource.getMessage(
            "authentication.secondaryPassword.validation.locked", null,
            "Your account is locked. Please contact the administrator.", locale);
      } else if (BadCredentialsException.class.isInstance(ex)) {
        // The authentication attempt failed due to bad credentials
        final String defaultMessage = "Please try again. The characters you have entered are invalid. Too many failed attempts may cause your account to be locked.";
        message = messageSource.getMessage("authentication.secondaryPassword.validation.badcredentials", null, defaultMessage, locale);
      } else {
        // An unknown error occured during authentication
        logger.error("Error validating secondary password challenge for user {}", userSummary.getId(), ex);
        final String defaultMessage = "There was a system error trying to validate the secondary password. Please contact your administrator.";
        message = messageSource.getMessage("authentication.secondaryPassword.validation.syserr", null, defaultMessage, locale);
      }

      final List<Integer> characterIndices = secondaryPasswordManager
          .getChallengeCharacters(userSummary.getId())
          .getCharacterIndices();

      model.addAllAttributes(getSecondaryPasswordChallengeCharsAsModelAttrs(characterIndices));
      model.addAttribute("loginErrorMessage", message);

      return "loginSecondaryPassword";
    }

    return "redirect:/index";
  }

  @RequestMapping(value = "/secondaryPassword/configure", method = RequestMethod.GET)
  public String processConfigureSecondaryPasswordRequest(final ModelMap model) {
    logger.debug("Processing the configure secondary password (GET) page request.");

    final LoginDetails loginDetails = userContextService.getLoginDetails();

    if (!loginDetails.mustSetSecondaryPassword()) {
      logger.debug("User does not need to set secondary password. Redirecting to /index");
      return "redirect:/index";
    }

    return "loginConfigureSecondaryPassword";
  }

  @RequestMapping(value = "/secondaryPassword/configure", method = RequestMethod.POST)
  public String processConfigureSecondaryPassword(
      @ModelAttribute("secondaryPasswordVO") @Valid final SecondaryPasswordVO secondaryPasswordVO, final BindingResult result,
      final ModelMap model, final Locale locale) {

    logger.debug("Processing the configure secondary password (POST) page request.");

    if (result.hasErrors()) {
      model.addAttribute("loginFormErrors", getValidationErrors(result, locale));
      return "loginConfigureSecondaryPassword";
    }

    final SecurityUser userSummary = userContextService.getUserSummary();

    secondaryPasswordManager.setSecondaryPassword(userSummary.getId(), secondaryPasswordVO);
    authenticationTools.completeTwoStepAuthentication(userContextService.getCurrentAuthentication());

    return "redirect:/index";
  }

  @RequestMapping(value = "/twoFactorSms", method = RequestMethod.GET)
  public String processIssueTwoFactorSmsChallenge(final ModelMap model, final Locale locale) {

    logger.debug("Processing the two factor SMS code validation (GET) page request.");

    final LoginDetails loginDetails = userContextService.getLoginDetails();

    if (!loginDetails.mustPerformSMSVerification()) {
      logger.debug("User should not be verified with an SMS token. Redirecting to /index");
      return "redirect:/index";
    }

    final SecurityUser userSummary = userContextService.getUserSummary();
    if (!twoFactorSmsAuthenticationService.validTokenExists(userSummary.getId())) {
      try {
        twoFactorSmsAuthenticationService.issueNewToken(userSummary);

        final String defaultMessage = "We've sent you a code via text message. Please enter it below.";
        final String message = messageSource.getMessage("authentication.twofactorsms.tokenissue.info", null, defaultMessage, locale);
        model.addAttribute("loginInfoMessage", message);

      } catch (InvalidTwoFactorSmsContactNumberException e) {
        final String defaultMessage = "Unable to send SMS for verification due to invalid contact details. Please contact your administrator.";
        final String message = messageSource.getMessage("authentication.twofactorsms.usercontact.error", null, defaultMessage, locale);
        model.addAttribute("loginErrorMessage", message);

      } catch (TwoFactorSmsAuthenticationServiceException | UserIdNotFoundException e) {
        final String defaultMessage = "There was an error while processing the SMS verification. Please contact your administrator.";
        final String message = messageSource.getMessage("authentication.twofactorsms.tokenissue.error", null, defaultMessage, locale);
        model.addAttribute("loginErrorMessage", message);
      }
    }

    return "loginTwoFactorSms";
  }

  @RequestMapping(value = "/twoFactorSms/newToken", method = RequestMethod.GET)
  public String processIssueNewTwoFactorSmsChallenge(final RedirectAttributes atts, final Locale locale) {

    logger.debug("Processing the issue new two factor SMS code (GET) page request.");

    final LoginDetails loginDetails = userContextService.getLoginDetails();

    if (!loginDetails.mustPerformSMSVerification()) {
      logger.debug("User should not be verified with an SMS token. Redirecting to /index");
      return "redirect:/index";
    }

    try {
      final SecurityUser userSummary = userContextService.getUserSummary();

      twoFactorSmsAuthenticationService.issueNewToken(userSummary);

      final String defaultMessage = "A new code has been sent to you via text message. Please enter it below.";
      final String message = messageSource.getMessage("authentication.twofactorsms.tokenissue.new", null, defaultMessage, locale);
      atts.addFlashAttribute("loginInfoMessage", message);

    } catch (InvalidTwoFactorSmsContactNumberException e) {
      final String defaultMessage = "Unable to send SMS for verification due to invalid contact details. Please contact your administrator.";
      final String message = messageSource.getMessage("authentication.twofactorsms.usercontact.error", null, defaultMessage, locale);
      atts.addFlashAttribute("loginErrorMessage", message);

    } catch (TwoFactorSmsAuthenticationServiceException | UserIdNotFoundException e) {
      final String defaultMessage = "There was an error while processing the SMS verification. Please contact your administrator.";
      final String message = messageSource.getMessage("authentication.twofactorsms.tokenissue.error", null, defaultMessage, locale);
      atts.addFlashAttribute("loginErrorMessage", message);
    }
    return "redirect:/login/twoFactorSms";
  }

  @RequestMapping(value = "/twoFactorSms", method = RequestMethod.POST)
  public String processTwoFactorCodeValidation(
      @ModelAttribute("twoFactorSmsVerification") @Valid final TwoFactorSmsVerificationVO verification, final BindingResult result, final ModelMap model,
      final Locale locale, final HttpServletRequest request, final HttpServletResponse response) {

    logger.debug("Processing the two factor SMS code validation (POST) page request.");

    if (result.hasErrors()) {
      model.addAttribute("loginFormErrors", getValidationErrors(result, locale));
      return "loginTwoFactorSms";
    }

    final LoginDetails loginDetails = userContextService.getLoginDetails();
    final Long userId = userContextService.getUserSummary().getId();

    if (!loginDetails.mustPerformSMSVerification()) {
      logger.debug("User should not be verified with an SMS token. Redirecting to /index");
      return "redirect:/index";
    }

    try {
      final Authentication auth = userContextService.getCurrentAuthentication();

      authenticationTools.completeTwoStepAuthentication(auth, verification);

      final String bypassToken = bypassSecondAuthenticationService.registerBypassToken(userId);

      final int bypassTokenDuration = (int) securityConfigService.getMultipleLoginTimeout();

      bypassTokenManager.addBypassToken(request, response, bypassToken, bypassTokenDuration);

    } catch (BadCredentialsException e) {
      final String defaultMessage = "Please try again. The code you have entered is invalid. Too many failed attempts may cause your account to be locked.";
      final String message = messageSource.getMessage("authentication.twofactorsms.validation.invalid", null, defaultMessage, locale);
      model.addAttribute("loginErrorMessage", message);
      return "loginTwoFactorSms";

    } catch (LockedException e) {
      final String defaultMessage = "Your account is locked. Please contact the administrator.";
      final String message = messageSource.getMessage("authentication.twofactorsms.validation.locked", null, defaultMessage, locale);
      model.addAttribute("loginErrorMessage", message);
      return "loginTwoFactorSms";

    } catch (Exception e) {
      logger.error("Error validating two factor SMS token for User {}", userId, e);
      final String defaultMessage = "There was a system error trying to validate the SMS token. Please contact your administrator.";
      final String message = messageSource.getMessage("authentication.twofactorsms.validation.error", null, defaultMessage, locale);
      model.addAttribute("loginErrorMessage", message);
      return "loginTwoFactorSms";
    }

    return "redirect:/index";
  }

  @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
  public String processPasswordExpired(final ModelMap model) {

    logger.debug("Processing the change password page request.");
    final LoginDetails loginDetails = userContextService.getLoginDetails();

    if (!loginDetails.mustChangePassword()) {
      logger.debug("User does not need to change password. Redirecting to /index");
      return "redirect:/index";
    }
    final String message = "Your password has expired. Please change it.";
    model.addAttribute("loginInfoMessage", message);

    return "loginChangePassword";
  }

  @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
  public String changePassword(@ModelAttribute("updateSecurityUserSelfPasswordVO") @Valid final UpdatePasswordDetailsVO updatePasswordDetailsVO,
      final BindingResult result,
      final RedirectAttributes atts,
      final ModelMap model,
      final Locale locale) {

    if (result.hasErrors()) {
      model.addAttribute("loginFormErrors", getValidationErrors(result, locale));
      return "loginChangePassword";
    }

    final Authentication authentication = userContextService.getCurrentAuthentication();
    final SecurityUser userSummary = userContextService.getUserSummary();
    final long userId = userSummary.getId();

    logger.debug("Attempting to change password for user {} (ID:{})", userSummary.getName(), userId);

    try {
      securityUserPasswordService.updatePassword(updatePasswordDetailsVO);

      // FIXME Move this behind a simpler security password Update API
      // The user may need to be unlocked if they have come via self-service password reset
      if (securityUserLockoutManager.isPresent()) {
        securityUserLockoutManager.get().resetFailedLoginAttempts(userId);
      }

      // FIXME Move this behind a simpler security password Update API
      final UserDetails userDetails = userContextService.getUserDetails();
      UserDetailsBuilder.forDetails(userDetails).setMustChangePassword(false).update();

      // Now reload the authentication context so that the user may proceed to the next step
      authenticationTools.reloadAuthenticationContext(authentication);
    } catch (ConstraintViolationException ce) {
      final Set<ConstraintViolation<?>> constraints = ce.getConstraintViolations();
      model.addAttribute("loginFormErrors", getValidationPasswordErrors(constraints, locale));
      return "loginChangePassword";
    } catch (Exception e) {
      final String message = "There was an error changing your password";
      model.addAttribute("loginErrorMessage", message);
      return "login";
    }

    return "redirect:/index";
  }

  @RequestMapping(value = "/forgottenPassword", method = RequestMethod.GET)
  public String processSelfPasswordReset(final ModelMap model, final Locale locale) {

    logger.debug("Processing the self password reset page request.");

    final boolean forgotPasswordEnabled = determineSelfPasswordResetIsEnabled();

    if (!forgotPasswordEnabled) {
      return "redirect:/login";
    }

    final String defaultMessage = "Please enter your username and work email address. We will send you an email that will allow you to reset your password.";
    final String message = messageSource.getMessage("security.self.password.reset.info", null, defaultMessage, locale);
    model.addAttribute("loginInfoMessage", message);

    return "selfPasswordResetRequest";
  }

  @RequestMapping(value = "/forgottenPassword", method = RequestMethod.POST)
  public String selfPasswordReset(
      @ModelAttribute("passwordResetRequestVO") @Valid final PasswordResetRequestVO passwordResetRequestVO,
      final BindingResult result, final RedirectAttributes atts, final ModelMap model,
      final Locale locale, final HttpServletRequest request) {

    logger.debug("Processing the self password reset page submit request.");

    if (result.hasErrors()) {
      model.addAttribute("loginFormErrors", getValidationErrors(result, locale));
      return "selfPasswordResetRequest";
    }

    final boolean forgotPasswordEnabled = determineSelfPasswordResetIsEnabled();
    String defaultErrorMessage = null;
    String errorCode = null;
    if (forgotPasswordEnabled) {
      try {
        final String absoluteBaseUrl = getAbsoluteUrlFromRequest(request);
        securityUserSelfPasswordResetService.performPasswordReset(
            passwordResetRequestVO.getUserName(),
            passwordResetRequestVO.getEmailAddress(),
            absoluteBaseUrl);
      } catch (EmailAddressNotFoundException emailAddressNotFoundException) {
        logger.info("Self password reset: The email address {} is not recognised.", passwordResetRequestVO.getEmailAddress());

        defaultErrorMessage = "The email address is not recognised, please contact your administrator.";
        errorCode = "security.self.password.reset.emailAddress.error";
        return handleSelfPasswordResetError(model, locale, defaultErrorMessage, errorCode);

      } catch (SecurityUserEmailAddressNotFoundException securityUserEmailAddressNotFoundException) {
        logger.info("Self password reset: The email {} is not linked to the username {}.",
            passwordResetRequestVO.getEmailAddress(), passwordResetRequestVO.getUserName());
        handleSelfPasswordResetSuccess(atts, locale);
      } catch (UsernameNotFoundException usernameNotFoundException) {
        logger.info("Self password reset: User name {} is not found in the system", passwordResetRequestVO.getUserName());
        handleSelfPasswordResetSuccess(atts, locale);
      } catch (URLResolverException | SelfPasswordResetServiceException exception) {
        logger.error("Error resolving absolute URL or Self password reset exception ", exception);
        defaultErrorMessage = "System error occured, please try again. If the error persists, please contact your administrator.";
        errorCode = "security.self.password.reset.error";
        return handleSelfPasswordResetError(model, locale, defaultErrorMessage, errorCode);
      }
    }
    return handleSelfPasswordResetSuccess(atts, locale);
  }

  private String handleSelfPasswordResetSuccess(final RedirectAttributes atts, final Locale locale) {
    final String defaultMessage = "An email has been sent,  please contact your system administrator if you do not receive it";
    final String message = messageSource.getMessage("security.self.password.reset.success", null, defaultMessage, locale);
    atts.addFlashAttribute("loginInfoMessage", message);

    return "redirect:/login";
  }

  private String handleSelfPasswordResetError(final ModelMap model, final Locale locale, final String defaultErrorMessage, final String errorString) {
    final String errorMessage = messageSource.getMessage(errorString, null, defaultErrorMessage, locale);
    model.addAttribute("loginErrorMessage", errorMessage);
    return "selfPasswordResetRequest";
  }

  /**
   * Transforms the validation errors to a list of {@link ErrorWithMessage} errors.
   * 
   * @param result
   *          - the BindingResult in which to retrieve field and global errors from
   * @param locale
   *          - the Locale to use when retrieving the text from the Spring MessageSource
   * @return a list of errors
   */
  private List<ErrorWithMessage> getValidationErrors(final BindingResult result, final Locale locale) {
    final List<FieldError> fieldErrors = result.getFieldErrors();
    final List<ObjectError> globalErrors = result.getGlobalErrors();
    final List<ErrorWithMessage> errors = new ArrayList<ErrorWithMessage>(fieldErrors.size() + globalErrors.size());

    for (FieldError fieldError : fieldErrors) {
      errors.add(new ValidationFieldError(fieldError.getField(), messageSource.getMessage(fieldError, locale)));
    }

    for (ObjectError objectError : globalErrors) {
      errors.add(new ValidationObjectError(objectError.getObjectName(), messageSource.getMessage(objectError, locale)));
    }

    return errors;
  }

  /**
   * Transforms the validation errors to a list of {@link ErrorWithMessage} errors.
   * 
   * @param constraints
   *          - A set of constraint violations returned from a service method within a try-catch
   * @return a list of errors
   */
  private List<ErrorWithMessage> getValidationPasswordErrors(final Set<ConstraintViolation<?>> constraints, final Locale locale) {

    final List<ErrorWithMessage> errors = new ArrayList<ErrorWithMessage>(constraints.size());
    for (ConstraintViolation<?> constraint : constraints) {
      final String field = constraint.getPropertyPath().toString();
      final String defaultMessage = "There is an error with this field " + field + ".";
      final Annotation annotation = constraint.getConstraintDescriptor().getAnnotation();

      String message = defaultMessage;

      if (annotation instanceof FieldMatch) {
        message = messageSource.getMessage("FieldMatch.password", null, defaultMessage, locale);
      } else if (annotation instanceof NotBlank) {
        switch (field) {
        case "password":
          message = messageSource.getMessage("NotBlank.password", null, defaultMessage, locale);
          break;
        case "confirmPassword":
          message = messageSource.getMessage("NotBlank.confirmPassword", null, defaultMessage, locale);
          break;
        default:
          break;
        }
      } else if (annotation instanceof Password) {
        message = messageSource.getMessage("Password.password", null, defaultMessage, locale);
      }

      errors.add(new ValidationFieldError(constraint.getPropertyPath().toString(), message));
    }

    return errors;
  }

  /**
   * This method returns true or false based on the system configured with
   * self password reset feature is enabled or not.
   * 
   * @return boolean
   */
  private boolean determineSelfPasswordResetIsEnabled() {
    boolean forgotPasswordEnabled = false;
    try {
      forgotPasswordEnabled = securityConfigService.isSelfServicePasswordResetEnabled();
    } catch (SecurityConfigNotFoundException securityConfigNotFoundException) {
      logger.error("Self password reset: security configuration not found on the system", securityConfigNotFoundException);
    } catch (MultipleSecurityConfigsFoundException multipleSecurityConfigsFoundException) {
      logger.error("Self password reset: multiple security configurations found on the system", multipleSecurityConfigsFoundException);
    }
    return forgotPasswordEnabled;
  }

  /**
   * This method returns true or false based on the system configured with
   * terms and conditions reset feature is enabled or not.
   * 
   * @return boolean
   */
  private boolean determineTermsAndConditionsIsEnabled() {
    boolean termsAndConditionsEnabled = false;
    try {
      termsAndConditionsEnabled = securityConfigService.isTermsAndConditionsEnabled();
    } catch (SecurityConfigNotFoundException securityConfigNotFoundException) {
      logger.error("Self password reset: security configuration not found on the system", securityConfigNotFoundException);
    } catch (MultipleSecurityConfigsFoundException multipleSecurityConfigsFoundException) {
      logger.error("Self password reset: multiple security configurations found on the system", multipleSecurityConfigsFoundException);
    }
    return termsAndConditionsEnabled;
  }

  /**
   * This method returns a url with link to terms and conditions.
   * 
   * @return String
   */
  private String getUrlForTermsAndConditions() {
    String termsAndConditionsUrl = null;
    try {
      termsAndConditionsUrl = securityConfigService.getTermsAndConditionsUrl();
    } catch (SecurityConfigNotFoundException securityConfigNotFoundException) {
      logger.error("Terms and Conditions: security configuration not found on the system", securityConfigNotFoundException);
    } catch (MultipleSecurityConfigsFoundException multipleSecurityConfigsFoundException) {
      logger.error("Terms and Conditions: multiple security configurations found on the system", multipleSecurityConfigsFoundException);
    }
    return termsAndConditionsUrl;

  }

  /**
   * This method got knowledge to extract the absolute URL from the request.
   * 
   * @param request
   *          The originating request.
   * @return String
   * @throws URLResolverException
   *           if the URL is not resolvable.
   */
  private String getAbsoluteUrlFromRequest(final HttpServletRequest request)
      throws URLResolverException {
    final String absoluteBaseUrl = absoluteUrlResolver.createAbsoluteURL(request, "");
    return absoluteBaseUrl;
  }

  /**
   * Prepares the inbound list of character indices for addition to the model.
   *
   * @param characterIndices
   *          the list of character indices which should be prepared
   * @return a map which can be passed to the model
   */
  private Map<String, Integer> getSecondaryPasswordChallengeCharsAsModelAttrs(
      final List<Integer> characterIndices) {
    final Map<String, Integer> modelAttributes = new HashMap<>();

    for (int i = 0; i < characterIndices.size(); i++) {
      final String attrName = "requiredChar_" + (i + 1);
      modelAttributes.put(attrName, characterIndices.get(i));
    }

    return modelAttributes;
  }
}
