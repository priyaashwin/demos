package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Defines supported context values for subject summary
 * 
 * @author Richard.Gibson
 */

@XmlEnum
@XmlType(name = "SubjectSummaryContext")
public enum SubjectSummaryContext {
  /**
   * Scottish context.
   */
  @XmlEnumValue("SCOT")
  SCOTTISH,
  
  /**
   * Welsh context.
   */
  @XmlEnumValue("WELSH")
  WELSH,

  /**
   * Default context which is effectively ENGLISH.
   */
  @XmlEnumValue("DEFAULT")
  DEFAULT;

}