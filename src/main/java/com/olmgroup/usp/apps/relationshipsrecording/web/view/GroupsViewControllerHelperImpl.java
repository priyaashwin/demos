// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.components.form.domain.FormInstanceStatus;
import com.olmgroup.usp.components.form.service.FormDefinitionService;
import com.olmgroup.usp.components.form.service.FormInstanceService;
import com.olmgroup.usp.components.form.vo.FormColumnFullDetailsVO;
import com.olmgroup.usp.components.form.vo.FormControlFullDetailsVO;
import com.olmgroup.usp.components.form.vo.FormDefinitionFullDetailsVO;
import com.olmgroup.usp.components.form.vo.FormInstanceWithGroupMembersVO;
import com.olmgroup.usp.components.form.vo.FormPageFullDetailsVO;
import com.olmgroup.usp.components.form.vo.FormRowFullDetailsVO;
import com.olmgroup.usp.components.form.vo.FormSectionFullDetailsVO;
import com.olmgroup.usp.components.form.vo.NumericControlFullDetailsVO;
import com.olmgroup.usp.components.form.vo.SelectionControlFullDetailsVO;
import com.olmgroup.usp.components.group.vo.GroupVO;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.search.SortSelection;
import com.olmgroup.usp.facets.springmvc.condition.TagVersionedResource;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.GroupsViewController
 */
@Component
final class GroupsViewControllerHelperImpl extends GroupsViewControllerHelperBaseImpl {

  private static final Logger LOGGER = LoggerFactory.getLogger(GroupsViewControllerHelperImpl.class);
  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  @Lazy
  @Inject
  private ObjectMapper objectMapper;

  @Lazy
  @Inject
  private FormInstanceService formInstanceService;

  @Lazy
  @Inject
  private FormDefinitionService formDefinitionService;

  @Value("${relationshipsRecording.groups.chartForm:f82f0236-e765-4fe2-9bcc-a1a2b489df66}")
  private String chartForm;

  @Value("${relationshipsRecording.groups.chartFormTitlePrefix:Progress for}")
  private String chartFormTitlePrefix;

  protected ObjectMapper getObjectMapper() {
    return this.objectMapper;
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. GroupsViewController#personGroupsDetails(final long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonGroupsDetails(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest,
            servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.PERSON, model);

    this.getContextHelperService().setupModelForContext(model);

    return "groups";
  }

  @Override
  public String handleGroupDetails(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.GROUP, model);

    // Put the group in the model
    final GroupVO groupVO = (GroupVO) model.asMap().get("group");

    if (groupVO != null) {
      // why???
      model.addAttribute("groupStartDateMilliseconds", groupVO.getStartDate().getTime());
    }

    // Check whether a suitable Form exists for producing a chart
    this.setupModelWithChartFlag(model, idParam);

    this.getContextHelperService().setupModelForContext(model);

    return "members";
  }

  private void setupModelWithChartFlag(final Model model, final long idParam) {
    if (StringUtils.isEmpty(this.chartForm)) {
      model.addAttribute("canChart", false);
      return;
    }

    final FormInstanceWithGroupMembersVO formInstanceVO = getFormInstanceForChart(idParam);

    model.addAttribute("canChart",
        (formInstanceVO != null && CollectionUtils.isNotEmpty(formInstanceVO.getMemberSubjects())));

  }

  @Override
  public String handleGroupChart(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse,
      final Model model) throws Exception {

    final FormInstanceWithGroupMembersVO instance = getFormInstanceForChart(idParam);

    if (instance == null) {
      LOGGER.warn("No form instance available");
      return "groupChart";
    }

    setupModelWithChartData(model, instance);

    // Put the group in the model
    GroupVO groupVO = (GroupVO) model.asMap().get("group");

    if (groupVO == null) {
      groupVO = getGroupService().findById(idParam);
      model.addAttribute("group", groupVO);
    }

    this.getContextHelperService().setupModelForContext(model);

    return "groupChart";
  }

  private void setupModelWithChartData(final Model model, final FormInstanceWithGroupMembersVO instance)
      throws IOException {

    final InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("groupChart.json");

    if (stream == null) {
      LOGGER.warn("No groupChart.json found");
    }

    final JsonNode mappings = objectMapper.readTree(stream);

    final JsonNode options = mappings.get("options");
    final JsonNode data = mappings.get("data");
    JsonNode merged = null;

    // Recent change to the form data builder have introduced(?) an error where if the groupMemberId parameter
    // of the formInstanceService.getContentForFormInstance is null, it results in a NPE.
    // This fix, related to OLM-19236. simply resolves the first group member and retrieves the content relative to that
    // group member.
    // The form design does not include any member specific sub-content so this should work correctly.
    Long firstGroupMemberId = null;
    firstGroupMemberId = instance.getMemberSubjects().get(0).getId();
    final TagVersionedResource<JsonNode> contentForFormInstance = formInstanceService
        .getContentForFormInstance(instance.getId(), firstGroupMemberId);
    final JsonNode body = contentForFormInstance.getBody();

    final Date completed = instance.getDateCompleted();

    final FormDefinitionFullDetailsVO definition = formDefinitionService.findById(instance.getFormDefinitionId(),
        FormDefinitionFullDetailsVO.class);

    merged = mergeFormData(definition, body, data);

    model.addAttribute("options", options);
    model.addAttribute("data", merged);
    model.addAttribute("completed", completed);

    final SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
    model.addAttribute("completedString", sdf.format(completed));

    model.addAttribute("chartFormTitlePrefix", this.chartFormTitlePrefix);

  }

  // Traverse a Form definition, looking for a Control by name
  private FormControlFullDetailsVO getControlByName(final FormDefinitionFullDetailsVO definition, final String name) {
    for (FormPageFullDetailsVO page : definition.getPages()) {
      for (FormSectionFullDetailsVO section : page.getSections()) {
        for (FormRowFullDetailsVO row : section.getRows()) {
          for (FormColumnFullDetailsVO column : row.getColumns()) {
            for (FormControlFullDetailsVO control : column.getControls()) {
              if (name.equals(control.getName())) {
                return control;
              }
            }
          }
        }
      }
    }
    return null;
  }

  // uses a control name to look up the value of an answer and convert it to an Integer, suitable for charting
  private Integer getAnswerForControl(final FormDefinitionFullDetailsVO definition, final JsonNode body,
      final String name,
      final Integer dfault) {
    final FormControlFullDetailsVO control = getControlByName(definition, name);

    if (control == null) {
      LOGGER.warn("Control " + name + " not found in form definition");
      return dfault;
    }

    final JsonNode data = body.get("data");
    if (data == null) {
      LOGGER.warn("No 'data' element found in Form instance");
      return dfault;
    }

    final JsonNode answer = data.get(control.getLocalId());
    if (answer == null) {
      LOGGER.warn(
          "No Answer found for control " + name + " (" + control.getLocalId() + ") - Is the Control group scope?");
      return dfault;
    }

    if (control instanceof SelectionControlFullDetailsVO) {

      // depending on whether a Control's data is new or copied from a previous Form, the data will either be a
      // 1-element array or a text node
      if (answer.isArray() && answer.get(0) != null) {
        try {
          return Integer.valueOf(answer.get(0).asText());
        } catch (NumberFormatException nfe) {
          return dfault;
        }
      } else if (answer.isTextual()) {
        try {
          return Integer.valueOf(answer.asText());
        } catch (NumberFormatException nfe) {
          return dfault;
        }
      }
    }
    if (control instanceof NumericControlFullDetailsVO && answer instanceof IntNode) {
      try {
        return Integer.valueOf(answer.asInt());
      } catch (Exception e) {
        return dfault;
      }
    }

    return dfault;
  }

  // Looks up answer references from answer data and merges them into a copy of the mappings
  private JsonNode mergeFormData(final FormDefinitionFullDetailsVO definition, final JsonNode body,
      final JsonNode mappings) {

    // Create a mutable clone of the mappings supplied
    final ObjectNode result = (ObjectNode) mappings.deepCopy();

    // get a NodeFactory for creating nodes to hold looked-up values
    final JsonNodeFactory nodeFactory = JsonNodeFactory.withExactBigDecimals(false);

    for (Iterator<JsonNode> setIterator = result.get("datasets").elements(); setIterator.hasNext();) {
      final ObjectNode set = (ObjectNode) setIterator.next();
      final ArrayNode dataSet = (ArrayNode) set.get("data");
      final ArrayNode replacementData = new ArrayNode(nodeFactory);
      for (Iterator<JsonNode> dataPointIterator = dataSet.elements(); dataPointIterator.hasNext();) {
        final JsonNode dataPoint = dataPointIterator.next();
        final String key = dataPoint.get("key").asText();
        final Integer defaultValue = dataPoint.get("default").asInt();
        replacementData.add(getAnswerForControl(definition, body, key, defaultValue));

      }
      set.replace("data", replacementData);
    }
    return result;
  }

  /**
   * Finds the most recent completed form for a given group.
   * 
   * @param groupId
   *          Id of the group.
   * @return Instance of the most recent form or null if none found
   */
  private FormInstanceWithGroupMembersVO getFormInstanceForChart(final long groupId) {
    final SortSelection instanceSort = new SortSelection()
        .addDescendingOrderedField("dateCompleted");
    // Search for any matching form instances. Limit results to 1 in order to get latest back
    final PaginationResult<FormInstanceWithGroupMembersVO> instances = formInstanceService
        .findBySubjectAndDefinitionUIDAndStatus(
            SubjectType.GROUP, // subjectType,
            groupId, // Group ID
            this.chartForm, // FAST Form Definition UID
            Arrays.asList(FormInstanceStatus.COMPLETE), // status,
            1, // pageNumber,
            1, // pageSize,
            instanceSort, // sort
            FormInstanceWithGroupMembersVO.class);

    // If a form was found, use it, otherwise no form, no chart
    if (instances.getResults().size() > 0) {
      return instances.getResults().get(0);
    }

    return null;
  }

}