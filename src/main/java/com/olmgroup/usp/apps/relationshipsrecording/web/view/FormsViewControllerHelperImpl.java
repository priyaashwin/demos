// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.components.form.vo.FormInstanceVO;
import com.olmgroup.usp.components.group.vo.GroupVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsViewController
 */
@Component
final class FormsViewControllerHelperImpl extends FormsViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  private static final SubjectType PERSON_SUBJECT_TYPE = SubjectType.PERSON;
  private static final SubjectType GROUP_SUBJECT_TYPE = SubjectType.GROUP;

  @Lazy
  @Inject
  private ObjectMapper objectMapper;

  private ObjectMapper getObjectMapper() {
    return this.objectMapper;
  }

  @Override
  public String handleViewPersonForms(final long idParam, final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest, servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelWithSubject(idParam, PERSON_SUBJECT_TYPE, model);
    model.addAttribute("reset", resetParam);
    this.getContextHelperService().setupModelForContext(model);
    return "forms";
  }

  @Override
  public String handleViewGroupForms(final long idParam, final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForGroupSubject(idParam, webRequest, servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelWithSubject(idParam, GROUP_SUBJECT_TYPE, model);
    model.addAttribute("reset", resetParam);
    this.getContextHelperService().setupModelForContext(model);
    return "forms";
  }

  @Override
  public String handleViewAddPersonForm(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(idParam, webRequest, servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelWithSubject(idParam, PERSON_SUBJECT_TYPE, model);
    this.getContextHelperService().setupModelForContext(model);
    return "forms";
  }

  @Override
  public String handleViewAddGroupForm(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForGroupSubject(idParam, webRequest, servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelWithSubject(idParam, GROUP_SUBJECT_TYPE, model);
    this.getContextHelperService().setupModelForContext(model);
    return "forms";
  }

  @Override
  public String handleViewForm(final long idParam, final String controlIdParam, final Long memberIdParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    // lookup form instance and definition - I may do this client side
    final FormInstanceVO formInstance = getFormInstanceService().findById(idParam);

    // put configuration attributes into model
    model.addAttribute("formId", formInstance.getId());
    model.addAttribute("formDefinitionId",
        formInstance.getFormDefinitionId());

    if (controlIdParam != null) {
      model.addAttribute("controlId", controlIdParam);
    }

    if (memberIdParam != null) {
      model.addAttribute("memberId", memberIdParam);
    }

    this.getContextHelperService().setupModelForContext(model);

    // return to bootstrap tile
    return "form.bootstrap";
  }

  @Override
  public String handleViewFormReviews(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) throws Exception {
    // lookup form instance and definition
    final FormInstanceVO formInstance = getFormInstanceService().findById(idParam);
    final SubjectType subjectType = formInstance.getSubjectType();
    final long subjectId = formInstance.getSubjectId();

    String preFlightViewName = null;

    switch (subjectType) {
    case GROUP:
      preFlightViewName = this.preFlightViewResolver
          .getViewNameForGroupSubject(subjectId, webRequest, servletResponse, model);
      break;

    case PERSON:
      preFlightViewName = this.preFlightViewResolver
          .getViewNameForPersonSubject(subjectId, webRequest, servletResponse, model);
      break;

    default:
      break;
    }

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    setupModelWithSubject(subjectId, subjectType, model);
    model.addAttribute("formId", idParam);
    this.getContextHelperService().setupModelForContext(model);

    // return to form reviews tile
    return "form.reviews";
  }

  private void setupModelWithSubject(final long subjectId,
      final SubjectType subjectType, final Model model) throws Exception {

    this.getHeaderControllerViewService().setupModelWithSubject(subjectId, subjectType, model);

    if (subjectType.equals(PERSON_SUBJECT_TYPE)) {
      // Put the groups that have form which the person belongs to
      // into the Filters section
      setupModelWithGroupFilter(model, subjectId);

    }
  }

  private void setupModelWithGroupFilter(final Model model, final long personId) throws Exception {
    final List<GroupVO> groupList = new ArrayList<>();

    groupList.addAll(getFormInstanceService().findGroupsWithFormInstances(
        personId));

    model.addAttribute("groupList", this.getObjectMapper()
        .writeValueAsString(groupList));

    final ArrayList<Long> groupIdsList = new ArrayList<Long>();

    for (GroupVO g : groupList) {
      groupIdsList.add(g.getId());
    }

    model.addAttribute("initialGroupIds", groupIdsList);

  }
}