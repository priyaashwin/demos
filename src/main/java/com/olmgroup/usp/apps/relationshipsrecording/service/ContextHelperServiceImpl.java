// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    SpringServiceImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::ContextHelperService
 * STEREOTYPE:  Service
 * STEREOTYPE:  Unsecured
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import com.olmgroup.usp.components.careleaver.service.context.CareLeaverContext;
import com.olmgroup.usp.components.careleaver.service.context.CareLeaverContextResolver;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.inject.Inject;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.ContextHelperService
 */
@Service("contextHelperService")
final class ContextHelperServiceImpl extends ContextHelperServiceBase {

  @Lazy
  @Inject
  private CareLeaverContextResolver careLeaverContextResolver;

  /**
   * Add any context driven attributes to the model.
   * 
   * @param model
   *          - the view model to add the attributes to
   */
  @Override
  protected void handleSetupModelForContext(final Model model) throws Exception {
    // Add model attributes that will hide/display the care leaver and after care menu items
    // depending on the context
    final CareLeaverContext activeCareLeaverContext = careLeaverContextResolver.resolveCareLeaverContext();
    model.addAttribute("careLeaverDefault", CareLeaverContext.DEFAULT == activeCareLeaverContext);
    model.addAttribute("careLeaverAfterCare", CareLeaverContext.SCOTTISH == activeCareLeaverContext);
    model.addAttribute("careLeaverWelsh", CareLeaverContext.WELSH == activeCareLeaverContext);
  }

}