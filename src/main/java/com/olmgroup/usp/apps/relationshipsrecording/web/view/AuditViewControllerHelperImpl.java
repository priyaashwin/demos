// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.AuditViewController
 */
@Component
final class AuditViewControllerHelperImpl extends AuditViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.AuditViewController#viewAudit(WebRequest webRequest,
   *      HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewAudit(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {

    this.getContextHelperService().setupModelForContext(model);
    return "audit";
  }
}