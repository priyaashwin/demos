// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.checklist.exception.ChecklistInstanceNotFoundException;
import com.olmgroup.usp.components.checklist.vo.ChecklistInstanceVO;
import com.olmgroup.usp.components.checklist.vo.ChecklistSubjectVO;
import com.olmgroup.usp.facets.security.authentication.context.UserContextService;
import com.olmgroup.usp.facets.security.authentication.vo.SecurityTeam;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChecklistViewController
 */
@Component
final class ChecklistViewControllerHelperImpl extends ChecklistViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private UserContextService userContextService;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. ChecklistViewController#viewPersonChecklists(final long
   *      idParam, final Boolean resetParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewPersonChecklists(final long personId,
      final Boolean resetParam, final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) throws Exception {

    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);

    setupModelWithLoggedInPersonId(model);
    model.addAttribute("reset", resetParam);
    this.getContextHelperService().setupModelForContext(model);
    return "checklist";
  }

  @Override
  public String handleViewGroupChecklists(final long groupId,
      final Boolean resetParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model)
      throws Exception {

    this.getHeaderControllerViewService().setupModelWithSubject(groupId, SubjectType.GROUP, model);

    setupModelWithLoggedInPersonId(model);
    model.addAttribute("reset", resetParam);
    this.getContextHelperService().setupModelForContext(model);
    return "checklist";
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. ChecklistViewController#viewAddPersonChecklist(final
   *      long idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewAddPersonChecklist(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {

    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.PERSON, model);

    setupModelWithLoggedInPersonId(model);
    this.getContextHelperService().setupModelForContext(model);
    return "checklist";
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.
   *      ChecklistViewController#viewAddGroupMemberChecklists(final long idParam, WebRequest webRequest,
   *      HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewAddGroupMemberChecklists(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {

    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.GROUP, model);

    setupModelWithLoggedInPersonId(model);
    this.getContextHelperService().setupModelForContext(model);
    return "checklist";
  }

  private void setupModelWithLoggedInPersonId(final Model model) {
    model.addAttribute("currentPersonId", this.getPersonService().getPersonIdForCurrentUser());

    // Check whether a team is held in the security user context
    final SecurityTeam securityTeamVO = userContextService.getTeam();
    if (securityTeamVO.getTeamId() != null) {
      // current team user logged in as
      model.addAttribute("currentPersonTeamId", securityTeamVO.getTeamId());
    }
  }

  @Override
  public String handleViewChecklist(final long idParam, final Boolean isGroupContextParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {

    this.getContextHelperService().setupModelForContext(model);

    // return the checklist.view tile
    return viewChecklist(idParam, BooleanUtils.isTrue(isGroupContextParam), model);
  }

  private String viewChecklist(final long id, final boolean isGroupContext, final Model model) {
    // lookup checklist instance and definition - I may do this client side
    final ChecklistInstanceVO checklistInstance = getChecklistInstanceService().findById(id);

    if (checklistInstance == null) {
      // no checklist found
      throw new ChecklistInstanceNotFoundException(id);
    }

    // checklist subject
    final ChecklistSubjectVO subject = checklistInstance.getSubject();

    // setup the model data
    model.addAttribute("checklistInstanceId", checklistInstance.getId());

    // if the user is viewing from the context of a group and the checklist instance has a subject group
    // then this will be used for the context object
    if (isGroupContext && null != subject.getSubjectGroupId()) {
      this.getHeaderControllerViewService().setupModelWithSubject(subject.getSubjectGroupId(), SubjectType.GROUP,
          model);
    } else {
      // otherwise just setup the context using the subject information
      this.getHeaderControllerViewService().setupModelWithSubject(subject.getId(), subject.getSubjectType(), model);
    }

    setupModelWithLoggedInPersonId(model);

    // return the checklist.view tile
    return "checklist.view";
  }

}