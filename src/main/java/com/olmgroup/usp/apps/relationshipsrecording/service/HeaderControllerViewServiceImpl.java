// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    SpringServiceImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::HeaderControllerViewService
 * STEREOTYPE:  Service
 * STEREOTYPE:  Unsecured
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonWithRelatedPersonWarningsVO;
import com.olmgroup.usp.components.classification.vo.ClassificationAssignmentVO;
import com.olmgroup.usp.components.group.vo.GroupVO;
import com.olmgroup.usp.components.organisation.vo.OrganisationVO;
import com.olmgroup.usp.components.person.domain.DuplicateStatus;
import com.olmgroup.usp.components.person.utils.PersonTypesHelper;
import com.olmgroup.usp.components.person.utils.PersonTypesHelperBean;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.search.SortSelection;
import com.olmgroup.usp.facets.security.subject.utils.SecuredSubjectContextTools;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.HeaderControllerViewService
 */
@Service("headerControllerViewService")
final class HeaderControllerViewServiceImpl extends
    HeaderControllerViewServiceBase {

  private static final Logger LOGGER = LoggerFactory
      .getLogger(HeaderControllerViewServiceImpl.class);

  private static final SubjectType PERSON_SUBJECT_TYPE = SubjectType.PERSON;
  private static final SubjectType GROUP_SUBJECT_TYPE = SubjectType.GROUP;
  private static final SubjectType ORGANISATION_SUBJECT_TYPE = SubjectType.ORGANISATION;

  @Lazy
  @Inject
  @Named("securedSubjectContextTools")
  private SecuredSubjectContextTools securedSubjectContextTools;

  @Lazy
  @Inject
  private PersonTypesHelper personTypesHelper;

  @Override
  protected final void handleSetupModelForPerson(final Model model,
      final long personId) throws Exception {

    final PersonVO personVO = (PersonVO) model.asMap().get("person");

    if (personVO == null
        || !(personVO instanceof PersonWithRelatedPersonWarningsVO)) {
      final PersonWithRelatedPersonWarningsVO personWithRelatedWarningsVO = getPersonService()
          .findById(personId, PersonWithRelatedPersonWarningsVO.class);

      model.addAttribute("person", personWithRelatedWarningsVO);

      // Add the PersonWarning count to the model
      if (personWithRelatedWarningsVO != null) {
        model.addAttribute("noOfPersonWarnings", this
            .getPersonWarningService().getCountByPersonId(personId));
      }

      final PaginationResult<ClassificationAssignmentVO> result = this
          .getClassificationAssignmentService().findBySubject(
              personId, SubjectType.PERSON, -1, 1,
              new SortSelection(),
              ClassificationAssignmentVO.class);
      model.addAttribute("classifications", result);
    }
  }

  @Override
  protected void handleSetupModelWithSubject(long subjectId, SubjectType subjectType, Model model) throws Exception {
    if (subjectType.equals(PERSON_SUBJECT_TYPE)) {
      PersonVO personVO = (PersonVO) model.asMap().get("person");
      if (personVO == null || personVO.getId() != subjectId) {
        personVO = getPersonService().findById(subjectId);
        model.addAttribute("person", personVO);
        final PersonTypesHelperBean personTypesHelperBean = personTypesHelper.getPersonTypes(personVO);
        model.addAttribute("personTypesHelper", personTypesHelperBean);
        model.addAttribute("readOnly",
            DuplicateStatus.DUPLICATE.equals(personVO.getDuplicated()) ? true : false);
      }
    } else if (subjectType.equals(GROUP_SUBJECT_TYPE)) {
      final GroupVO groupVO = (GroupVO) model.asMap().get("group");
      if (groupVO == null || groupVO.getId() != subjectId) {
        model.addAttribute("group", getGroupService().findById(subjectId, GroupVO.class));
      }
    } else if (subjectType.equals(ORGANISATION_SUBJECT_TYPE)) {
      final OrganisationVO organisationVO = (OrganisationVO) model.asMap().get("organisation");
      if (organisationVO == null || organisationVO.getId() != subjectId) {
        model.addAttribute("organisation", getOrganisationService().findById(subjectId));
      }
    }

    model.addAttribute("securedSubjectContextVO",
        this.securedSubjectContextTools.getSecuredSubjectContextVO(subjectId, subjectType));
    model.addAttribute("subjectType", subjectType.value().toLowerCase());
  }

}