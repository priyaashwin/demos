// license-header java merge-point
//
// Generated by: hibernate/SpringValueObjectTargetTransformImpl.vsl in andromda-spring-cartridge.
//
package com.olmgroup.usp.apps.relationshipsrecording.domain;

import com.olmgroup.usp.apps.relationshipsrecording.vo.AsymmetricPersonPersonRelationshipResultVO;
import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonOrganisationRelationshipsResultVO;
import com.olmgroup.usp.components.classification.vo.ClassificationAssignmentVO;
import com.olmgroup.usp.components.contact.vo.ContactVO;
import com.olmgroup.usp.components.contact.vo.TelephoneContactVO;
import com.olmgroup.usp.components.person.domain.Person;
import com.olmgroup.usp.components.person.utils.PersonTypesHelper;
import com.olmgroup.usp.components.person.utils.PersonTypesHelperBean;
import com.olmgroup.usp.components.person.vo.PersonAddressVO;
import com.olmgroup.usp.components.relationship.domain.RelationshipAttributeDirection;
import com.olmgroup.usp.facets.core.util.TemporalStatusFilter;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.search.SortSelection;
import com.olmgroup.usp.facets.security.authorisation.voter.relational.SecurityAccessAllowedRecordChecker;
import com.olmgroup.usp.facets.security.authorisation.voter.relational.SecurityAccessDeniedRecordChecker;
import com.olmgroup.usp.facets.subject.SubjectType;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

/**
 * @see Person
 */
@Component("personHeaderDetailsVOTransform")
final class PersonHeaderDetailsVOTransformImpl extends PersonHeaderDetailsVOTransformBase {

  @Lazy
  @Inject
  private SecurityAccessDeniedRecordChecker accessDeniedRecordChecker;

  @Lazy
  @Inject
  private SecurityAccessAllowedRecordChecker accessAllowedRecordChecker;

  @Lazy
  @Inject
  private PersonTypesHelper personTypesHelper;

  @Override
  protected void handleEntityToValueObject(
      final Person source,
      final PersonHeaderDetailsVO target) {
    mapClassificationAssignments(source, target);
    mapContacts(source, target);
    mapPreferredAddress(source, target);

    // Fix for the defect OLM-31974 connected to the defect OLM-31581: map allocated workers only if person type is CLIENT, FOSTER_CARER or ADOPTER. 
    final PersonTypesHelperBean personTypesBean = personTypesHelper.getPersonTypes(source.getId());
    if (personTypesBean.isClient() || personTypesBean.isFosterCarer() || personTypesBean.isAdopter()) {
      mapAllocatedWorkers(source, target);
    }

    mapAllocatedTeams(source, target);
    mapLockDetails(source, target);
    mapNamedPersons(source, target);
  }

  private void mapClassificationAssignments(final Person source, final PersonHeaderDetailsVO target) {
    final PaginationResult<ClassificationAssignmentVO> result = this
        .getClassificationAssignmentService().findBySubject(
            source.getId(), SubjectType.PERSON, 1, -1,
            new SortSelection(),
            ClassificationAssignmentVO.class);
    target.setClassificationAssignments(result.getResults());

  }

  private void mapContacts(final Person source, final PersonHeaderDetailsVO target) {

    // Contact Details
    final List<ContactVO> contacts = getPersonService()
        .findContactByPersonId(source.getId());
    Collections.sort(contacts, new ContactComparator());

    for (ContactVO contact : contacts) {
      if (contact instanceof TelephoneContactVO) {
        if ("MOBILE".equals(((TelephoneContactVO) contact)
            .getTelephoneType())) {
          target.getMobileContacts()
              .add((TelephoneContactVO) contact);
        } else if (!"FAX".equals(((TelephoneContactVO) contact)
            .getTelephoneType())) {
          target.getTelephoneContacts().add(
              (TelephoneContactVO) contact);
        }
      }
    }
  }

  private void mapPreferredAddress(final Person source, final PersonHeaderDetailsVO target) {
    final PersonAddressVO address = getPersonAddressService().findPreferredPersonAddressByPersonId(source.getId());

    target.setPreferredAddress(address);
  }

  private void mapAllocatedWorkers(final Person source, final PersonHeaderDetailsVO target) {
    final List<AsymmetricPersonPersonRelationshipResultVO> teamRelationships = getPersonPersonRelationshipService()
        .findProfessionalAllocatedWorkersByPersonId(source.getId(),
            Arrays.asList(new RelationshipAttributeDirection[] { RelationshipAttributeDirection.ROLE_A }),
            TemporalStatusFilter.ACTIVE, null, -1, -1, null,
            AsymmetricPersonPersonRelationshipResultVO.class)
        .getResults();

    target.setAllocatedWorkers(teamRelationships);
  }

  private void mapNamedPersons(final Person source, final PersonHeaderDetailsVO target) {
    final List<AsymmetricPersonPersonRelationshipResultVO> teamRelationships = getPersonPersonRelationshipService()
        .findProfessionalNamedPersonsByPersonId(source.getId(),
            Arrays.asList(new RelationshipAttributeDirection[] { RelationshipAttributeDirection.ROLE_A }),
            TemporalStatusFilter.ACTIVE, null, -1, -1, null,
            AsymmetricPersonPersonRelationshipResultVO.class)
        .getResults();

    target.setNamedPersons(teamRelationships);
  }

  private void mapAllocatedTeams(final Person source, final PersonHeaderDetailsVO target) {
    final SortSelection sort = new SortSelection();
    sort.addDescendingOrderedField("startDate");
    final List<PersonOrganisationRelationshipsResultVO> teamRelationships = getPersonTeamRelationshipService()
        .findClientByPersonId(source.getId(), TemporalStatusFilter.ACTIVE, RelationshipAttributeDirection.ROLE_B, null,
            -1, -1, sort, PersonOrganisationRelationshipsResultVO.class)
        .getResults();
    target.setAllocatedTeams(teamRelationships);
  }

  private void mapLockDetails(final Person source, final PersonHeaderDetailsVO target) {
    final SubjectIdTypeVO subject = new SubjectIdTypeVO(source.getId(), SubjectType.PERSON);

    final boolean hasAccessLocks;
    if (CollectionUtils.isNotEmpty(accessAllowedRecordChecker.getAllowedList(subject))) {
      hasAccessLocks = true;
    } else if (CollectionUtils.isNotEmpty(accessDeniedRecordChecker.getDeniedList(subject))) {
      hasAccessLocks = true;
    } else {
      hasAccessLocks = false;
    }

    target.setHasAccessLocks(hasAccessLocks);
  }

  class ContactComparator implements Comparator<ContactVO> {
    @Override
    public int compare(final ContactVO o1, final ContactVO o2) {
      int c = 0;

      if (o1.getContactType() != null && o2.getContactType() != null) {
        c = o1.getContactType().compareTo(o2.getContactType());

        if (c == 0) {
          if (o1.getStartDate() != null && o2.getStartDate() != null) {
            c = o2.getStartDate().compareTo(o1.getStartDate());
          }
        }
      }
      return c;
    }
  }

}