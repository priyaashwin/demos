// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.olmgroup.usp.apps.relationshipsrecording.utils.RelationshipTypesUtils;
import com.olmgroup.usp.components.configuration.vo.CodedEntryVO;
import com.olmgroup.usp.components.relationship.vo.RelationshipTypeVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.Comparator;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.RelationshipViewController
 */
@Component
final class RelationshipViewControllerHelperImpl extends RelationshipViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private ObjectMapper objectMapper;

  protected ObjectMapper getObjectMapper() {
    return this.objectMapper;
  }

  @Lazy
  @Inject
  private RelationshipTypesUtils relationshipTypesUtils;

  private RelationshipTypesUtils getRelationshipTypesUtils() {
    return this.relationshipTypesUtils;
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.RelationshipViewController#personRelationships(long
   *      idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonRelationships(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {
    setupModelWithSubject(idParam, SubjectType.PERSON, model);
    setupModelForView(idParam, model);
    return "relationships";
  }

  private void setupModelWithSubject(final long subjectId, final SubjectType subjectType, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(subjectId, subjectType, model);
  }

  private void setupModelForView(final long personId, final Model model) throws Exception {

    // Put the PersonalRelationshipRoles into the model
    this.getRelationshipTypesUtils().setupModelWithPersonalRelationshipRoles(model);

    // Put the relationship class id into the model
    setupModelWithRelationshipClass(model);

    this.getContextHelperService().setupModelForContext(model);
  }

  private void setupModelWithRelationshipClass(final Model model) {

    final String relationshipClass = "ClientTeam";
    final RelationshipTypeVO relationshipType = getRelationshipTypeService().findByRelationshipClass(relationshipClass);
    final long relationshipClassId = relationshipType.getId();
    model.addAttribute("relationshipClassClientTeamId", relationshipClassId);

  }

  @Override
  public String handlePersonRelationshipsDiagram(final long idParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) throws Exception {
    setupModelWithSubject(idParam, SubjectType.PERSON, model);
    setupModelForView(idParam, model);
    return "relationshipsDiagram";
  }

  private static final Comparator<CodedEntryVO> codedEntryComparator = new Comparator<CodedEntryVO>() {
    @Override
    public int compare(CodedEntryVO arg0, CodedEntryVO arg1) {
      return arg0.getName().compareTo(arg1.getName());
    }
  };

  @Override
  public String handleOrganisationRelationships(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    setupModelWithSubject(idParam, SubjectType.ORGANISATION, model);
    this.getContextHelperService().setupModelForContext(model);
    return "organisationrelationships";
  }
}
