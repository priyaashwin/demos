package com.olmgroup.usp.apps.relationshipsrecording;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.core.io.Resource;
import org.springframework.web.context.support.ServletContextResourcePatternResolver;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

/**
 * Utility class to find and escape handlebars templates, ready for client-side compilation when in development mode
 * This is likely to migrate to the WebResources Facet in time...
 * 
 * @author barnabyr
 */
public class HandlebarsLocator {

  private ServletContext context;
  private ServletContextResourcePatternResolver resolver;

  /**
   * creates a new instance of HandlebarsLocator
   * 
   * @param ctx
   *          - ServletContext to use in resolving template paths
   */
  public HandlebarsLocator(ServletContext ctx) {
    this.context = ctx;
    this.resolver = new ServletContextResourcePatternResolver(this.context);
  }

  public Map<String, String> getEscapedTemplates(String classPathLocation) {
    Map<String, String> templates = new HashMap<>();

    try {
      Resource[] resources = resolver.getResources(classPathLocation + "/*.handlebars");
      for (Resource resource : resources) {
        String name = resource.getFilename();
        name = name.substring(0, name.indexOf(".handlebars"));
        String template = IOUtils.toString(resource.getInputStream(), "UTF-8");
        // FIXME Use common-text StringEscapeUtils rather than commons-lang3 - See OLM-24450
        template = StringEscapeUtils.escapeEcmaScript(template);

        templates.put(name, template);
      }
    } catch (IOException e) {
      System.err.println(e.getMessage());
    }

    return templates;
  }

}
