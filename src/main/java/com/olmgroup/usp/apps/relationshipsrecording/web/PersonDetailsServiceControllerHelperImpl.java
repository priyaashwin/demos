// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonContactsVO;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeletePersonReferenceNumbersVO;
import com.olmgroup.usp.components.contact.exception.InvalidContactVerificationDateException;
import com.olmgroup.usp.components.person.exception.DuplicateReferenceNumberException;
import com.olmgroup.usp.facets.springmvc.exception.DataBindException;
import com.olmgroup.usp.facets.springmvc.utils.ObjectErrorBuilder;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * This class can be used to override methods of {@link PersonDetailsServiceControllerBaseHelper}
 * e.g. to change how exceptions thrown by PersonDetailsServiceController are handled.
 * 
 * @see PersonDetailsServiceControllerBaseHelper
 */
@Component("PersonDetailsServiceControllerHelper")
class PersonDetailsServiceControllerHelperImpl extends
    PersonDetailsServiceControllerHelperBaseImpl {

  public PersonDetailsServiceControllerHelperImpl() {
  }

  @Override
  public final Exception handleExceptionForCombinedUpdateDeletePersonReferenceNumber(
      final Exception e,
      final UpdateDeletePersonReferenceNumbersVO updateDeletePersonReferenceNumbersVO,
      final BindingResult bindingResult, final WebRequest request,
      final HttpServletResponse servletResponse, final Model model) {

    if (e instanceof DuplicateReferenceNumberException) {
      final DuplicateReferenceNumberException ex = (DuplicateReferenceNumberException) e;
      final String errorMsg = "The reference number entered has already been used.";
      final String[] messageCodes = {
          "DuplicateReferenceNumberException.updateDeletePersonReferenceNumbersVO",
          "DuplicateReferenceNumberException", };
      final ObjectError error = new ObjectError("endDate", messageCodes,
          null, errorMsg);
      bindingResult.addError(error);
      return new DataBindException(bindingResult);
    }

    return e;
  }

  @Override
  public Exception handleExceptionForCombinedUpdateDeletePersonContact(final Exception e, final UpdateDeletePersonContactsVO updateDeletePersonContactsVO, final BindingResult updateDeletePersonContactsVOBindingResult,
      long personId,
      WebRequest webRequest, HttpServletResponse servletResponse, Model model) {

    if (e instanceof InvalidContactVerificationDateException) {
      final InvalidContactVerificationDateException invalidContactVerificationDateException = (InvalidContactVerificationDateException) e;

      final ObjectError objectError = new ObjectErrorBuilder("verificationDate", invalidContactVerificationDateException, "updateDeletePersonContactsVO")
          .withErrorMessage(invalidContactVerificationDateException.getMessage())
          .withErrorArgs(updateDeletePersonContactsVO.getPersonId())
          .build();

      updateDeletePersonContactsVOBindingResult.addError(objectError);
      return new DataBindException(updateDeletePersonContactsVOBindingResult);
    } else {
      return e;
    }
  }
}