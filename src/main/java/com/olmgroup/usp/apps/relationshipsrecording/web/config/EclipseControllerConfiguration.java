package com.olmgroup.usp.apps.relationshipsrecording.web.config;

import com.olmgroup.usp.facets.spring.StandardUSPProfiles;
import com.olmgroup.usp.facets.spring.SuppressingProfile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

/**
 * Provides the Eclipse Web Mvc {@link Configuration} included in the dispatcher servlet context.
 * 
 * @author robd
 */
@Controller
@Configuration
class EclipseControllerConfiguration {

  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(EclipseControllerConfiguration.class);

  /**
   * Load tiles configurer and view resolvers when not running in a development context. These require the application server to be running in order to function without errors.
   * 
   * @author robd
   */
  @Controller
  @Configuration
  @SuppressingProfile({ StandardUSPProfiles.DEVELOPMENT })
  class TilesConfiguration {

    @Value("${usp.facet.springmvc.tiles.checkRefresh:false}")
    private boolean checkRefresh;

    private final String[] exposedContextBeanNames = { "sessionConfigProperties", "versionConfigProperties", "propertyFactory", "buildVersionEnvInfo",
        "relationshipsrecordingAppVersion", "passwordPolicy", "customMenuItemService", "appSettings" };

    @Bean
    TilesConfigurer tilesConfigurer() {
      final TilesConfigurer tilesConfigurer = new TilesConfigurer();
      tilesConfigurer.setDefinitions("classpath:/META-INF/resources/tiles/**/tiles-defs.xml", "/WEB-INF/views/**/tiles-defs.xml");
      tilesConfigurer.setCheckRefresh(checkRefresh);
      tilesConfigurer.setCompleteAutoload(true);
      return tilesConfigurer;
    }

    @Bean
    TilesViewResolver viewResolver() {
      final TilesViewResolver viewResolver = new TilesViewResolver();
      viewResolver.setViewClass(TilesView.class);
      viewResolver.setOrder(1);
      viewResolver.setExposedContextBeanNames(exposedContextBeanNames);
      return viewResolver;
    }

    @Bean
    InternalResourceViewResolver internalResourceViewResolver() {
      final InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
      viewResolver.setViewClass(JstlView.class);
      viewResolver.setOrder(2);
      viewResolver.setPrefix("/WEB-INF/views");
      viewResolver.setSuffix(".jsp");
      viewResolver.setExposedContextBeanNames(exposedContextBeanNames);
      return viewResolver;
    }
  }

}
