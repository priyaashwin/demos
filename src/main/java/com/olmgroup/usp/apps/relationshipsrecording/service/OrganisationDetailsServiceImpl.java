// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    SpringServiceImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: application::com.olmgroup.usp.apps.relationshipsrecording::service::OrganisationDetailsService
 * STEREOTYPE:  Service
 * STEREOTYPE:  NotEventSource
 */
package com.olmgroup.usp.apps.relationshipsrecording.service;

import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeleteOrganisationContactsVO;
import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeleteOrganisationReferenceNumbersVO;
import com.olmgroup.usp.components.contact.vo.UpdateContactVO;
import com.olmgroup.usp.components.organisation.service.OrganisationReferenceNumberService;
import com.olmgroup.usp.components.organisation.service.OrganisationService;
import com.olmgroup.usp.components.organisation.vo.UpdateOrganisationReferenceNumberVO;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.service.OrganisationDetailsService
 */
@Service("organisationDetailsService")
final class OrganisationDetailsServiceImpl extends OrganisationDetailsServiceBase {

  @Override
  protected final void handleCombinedUpdateDeleteOrganisationContact(
      final UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO,
      final long organisationId) throws Exception {

    final OrganisationService organisationService = getOrganisationService();

    final List<UpdateContactVO> updateList = updateDeleteOrganisationContactsVO
        .getUpdateContactVOs();
    final List<Long> deleteList = updateDeleteOrganisationContactsVO
        .getDeleteContactIds();

    for (UpdateContactVO updateContactVO : updateList) {

      organisationService.updateContact(updateContactVO, organisationId);
    }

    for (Long contactId : deleteList) {

      organisationService.deleteContact(contactId.longValue(),
          organisationId);
    }

  }

  @Override
  protected final void handleCombinedUpdateDeleteOrganisationReferenceNumber(
      final UpdateDeleteOrganisationReferenceNumbersVO updateDeleteOrganisationReferenceNumbersVO)
      throws Exception {
    final OrganisationReferenceNumberService organisationReferenceService = getOrganisationReferenceNumberService();

    final List<UpdateOrganisationReferenceNumberVO> updateList = updateDeleteOrganisationReferenceNumbersVO
        .getUpdateReferenceNumberVOs();
    final List<Long> deleteList = updateDeleteOrganisationReferenceNumbersVO
        .getDeleteOrganisationReferenceNumberIds();

    for (UpdateOrganisationReferenceNumberVO updateOrganisationReferenceNumberVO : updateList) {

      organisationReferenceService
          .updateOrganisationReferenceNumber(updateOrganisationReferenceNumberVO);
    }

    for (Long id : deleteList) {

      organisationReferenceService.deleteOrganisationReferenceNumber(id
          .longValue());
    }
  }

}