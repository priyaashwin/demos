// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.components.classification.exception.DuplicateClassificationCodeException;
import com.olmgroup.usp.components.classification.vo.NewClassificationGroupVO;
import com.olmgroup.usp.facets.springmvc.exception.DataBindException;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * This class can be used to override methods of {@link ClassificationChildGroupServiceControllerHelperBaseImpl} e.g. to
 * change how exceptions thrown by ClassificationChildGroupServiceController are handled.
 * 
 * @see ClassificationChildGroupServiceControllerHelperBaseImpl
 */
@Component("ClassificationChildGroupServiceControllerHelper")
class ClassificationChildGroupServiceControllerHelperImpl
    extends ClassificationChildGroupServiceControllerHelperBaseImpl {

  private static final String CLASSIFICATION_GROUP = "ClassificationGroup";
  private static final String[] CLASSIFICATION_GROUP_DUPLICATE_CODE_MESSAGE_CODES = {
      "DuplicationClassificationCodeException.newClassificationGroupVO",
      "DuplicationClassificationCodeException" };

  public ClassificationChildGroupServiceControllerHelperImpl() {
  }

  @Override
  public Exception handleExceptionForAddClassificationGroup(Exception e,
      NewClassificationGroupVO newClassificationGroupVO, BindingResult newClassificationGroupVO_BindingResult,
      WebRequest webRequest, HttpServletResponse servletResponse, Model model) {
    if (e instanceof DuplicateClassificationCodeException) {
      final String errorMsg = String
          .format("Duplicate code with the within the same Classification hierarchy detected");
      final FieldError fieldError = new FieldError(CLASSIFICATION_GROUP, "code",
          null, false, CLASSIFICATION_GROUP_DUPLICATE_CODE_MESSAGE_CODES,
          null, errorMsg);
      newClassificationGroupVO_BindingResult.addError(fieldError);

      return new DataBindException(newClassificationGroupVO_BindingResult);
    }
    return e;
  }
}