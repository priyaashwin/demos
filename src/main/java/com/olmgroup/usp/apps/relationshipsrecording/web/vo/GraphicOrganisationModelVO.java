package com.olmgroup.usp.apps.relationshipsrecording.web.vo;

public class GraphicOrganisationModelVO {
    /** The serial version UID of this class. Needed for serialization. */
    private static final long serialVersionUID = 1L;

    // Class attributes
    private long id;
    private String name;
    
	public GraphicOrganisationModelVO() {
		super();
	}
	
	public GraphicOrganisationModelVO(long id,String name){
		super();
		this.id = id;
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
