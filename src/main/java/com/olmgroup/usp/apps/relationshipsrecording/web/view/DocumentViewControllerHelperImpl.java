// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OutputViewController
 */
@Component
public class DocumentViewControllerHelperImpl extends DocumentViewControllerHelperBaseImpl {

  public DocumentViewControllerHelperImpl() {
    super();
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.DocumentViewController#outputView(final Long idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonDocumentView(long idParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
      throws Exception {
    this.getContextHelperService().setupModelForContext(model);

    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.PERSON, model);
    return "person.document";

  }

}