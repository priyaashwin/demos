package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import com.olmgroup.usp.facets.security.authentication.context.UserContextService;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component("primaryReferenceNumberContextResolver")
final class PrimaryReferenceNumberContextResolverImp implements PrimaryReferenceNumberContextResolver {

  @Lazy
  @Inject
  private UserContextService userContextService;

  @Override
  public PrimaryReferenceNumberContext resolvePrimaryReferenceNumberContext() {
    final String contextAttribute = (String) userContextService.getContextAttributes().getContext(PRIMARY_REFERENCE_NUMBER_CONTEXT);

    return contextAttribute != null ? PrimaryReferenceNumberContext.valueOf(contextAttribute) : PrimaryReferenceNumberContext.DEFAULT;
  }

}
