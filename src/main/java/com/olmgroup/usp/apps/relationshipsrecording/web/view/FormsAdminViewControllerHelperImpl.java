// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.form.vo.FormDefinitionVO;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.FormsAdminViewController
 */
@Component
final class FormsAdminViewControllerHelperImpl extends FormsAdminViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. FormsAdminViewController#viewMappings(final long
   *      formDefinitionIdParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewMappings(final long formDefinitionIdParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) throws Exception {

    setupModelForView(formDefinitionIdParam, model);
    return "admin.forms.mappings";
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. FormsAdminViewController#viewMappings(final long
   *      formDefinitionIdParam, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewTypes(final Boolean resetParam,
      final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {

    boolean reset = true;
    if (resetParam != null) {
      reset = resetParam;
    }

    model.addAttribute("reset", reset);
    this.getContextHelperService().setupModelForContext(model);
    return "admin.forms.types";
  }

  private void setupModelForView(final long formDefinitionId, final Model model)
      throws Exception {
    // Lookup form definition id
    final FormDefinitionVO formDefinitionVO = (FormDefinitionVO) model.asMap().get("formDefinition");

    if (formDefinitionVO != null
        && formDefinitionVO.getId() == formDefinitionId) {
      return;
    }

    // lookup form definition
    model.addAttribute("formDefinition", getFormDefinitionService().findById(formDefinitionId));

    this.getContextHelperService().setupModelForContext(model);
  }
}
