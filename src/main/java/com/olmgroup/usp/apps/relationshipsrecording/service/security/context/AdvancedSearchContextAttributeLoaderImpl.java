package com.olmgroup.usp.apps.relationshipsrecording.service.security.context;

import com.olmgroup.usp.apps.relationshipsrecording.service.context.AdvancedSearchContext;
import com.olmgroup.usp.apps.relationshipsrecording.service.context.AdvancedSearchContextResolver;
import com.olmgroup.usp.facets.security.authentication.context.attributes.annotation.ForContextAttribute;
import com.olmgroup.usp.facets.security.authentication.context.attributes.loader.AbstractContextAttributeLoaderImpl;
import com.olmgroup.usp.facets.security.authentication.vo.SecurityTeam;
import com.olmgroup.usp.facets.security.authentication.vo.UserProfile;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Loads the advanced search context attribute.
 * 
 * @author Richard.Gibson
 */
@Component("advancedSearchContextAttributeLoader")
@ForContextAttribute(AdvancedSearchContextResolver.ADVANCED_SEARCH_CONTEXT)
final class AdvancedSearchContextAttributeLoaderImpl extends AbstractContextAttributeLoaderImpl {
  static final String REGION_SCOTLAND = "S";

  @Override
  public Serializable getAttributeValue(UserProfile userProfile) {
    final SecurityTeam securityTeam = userProfile.getSecurityProfile().getSecurityTeam();

    if (securityTeam != null && securityTeam.getRegionCode() != null) {
      final String regionCode = securityTeam.getRegionCode();
      if (REGION_SCOTLAND.equals(regionCode)) {
        return AdvancedSearchContext.SCOTTISH.toString();
      }
    }
    return AdvancedSearchContext.DEFAULT.toString();
  }

}
