package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

/**
 * Resolve the advanced search context for the security profile
 * 
 * @author Richard.Gibson
 */
public interface AdvancedSearchContextResolver extends LazilyInitialized {

  String ADVANCED_SEARCH_CONTEXT = "advanced-search";

  /**
   * Resolve the AdvancedSearch context to one of the values of {@link AdvancedSearchContext}.
   *
   * @return The resolved AdvancedSearchContext
   */
  AdvancedSearchContext resolveAdvancedSearchContext();

}
