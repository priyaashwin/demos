// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.careleaver.domain.AfterCareEligibility;
import com.olmgroup.usp.components.careleaver.service.AfterCareService;
import com.olmgroup.usp.components.careleaver.service.context.CareLeaverContext;
import com.olmgroup.usp.components.careleaver.service.context.CareLeaverContextResolver;
import com.olmgroup.usp.components.careleaver.service.validation.CareLeaverValidator;
import com.olmgroup.usp.components.careleaver.service.validation.CareLeaverValidatorFactory;
import com.olmgroup.usp.components.careleaver.service.validation.aftercare.AfterCareValidator;
import com.olmgroup.usp.components.person.domain.Person;
import com.olmgroup.usp.components.person.domain.PersonDao;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ChildCareLeaverViewController
 */
@Component
final class ChildCareLeaverViewControllerHelperImpl extends ChildCareLeaverViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private CareLeaverContextResolver careLeaverContextResolver;

  @Lazy
  @Inject
  private CareLeaverValidatorFactory careLeaverValidatorFactory;

  @Lazy
  @Inject
  private AfterCareService afterCareService;

  @Lazy
  @Inject
  private PersonDao personDao;

  @Override
  public String handleViewCareLeaver(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {

    setupModelForView(idParam, model);
    return "careleaver";
  }

  @Override
  public String handleViewAfterCare(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    setupModelForView(idParam, model);
    model.addAttribute("afterCareEligibility", afterCareService.getAfterCareEligibilityForPerson(idParam, new Date()));
    return "aftercare";
  }

  private void setupModelForView(final long personId, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);

    final CareLeaverContext activeContext = careLeaverContextResolver.resolveCareLeaverContext();

    if (CareLeaverContext.DEFAULT == activeContext) {
      model.addAttribute("canAddCareLeaverDetails", canAddCareLeaverDetails(personId));
      model.addAttribute("canAddAfterCareDetails", false);

    } else if (CareLeaverContext.SCOTTISH == activeContext) {
      model.addAttribute("canAddCareLeaverDetails", false);
      model.addAttribute("canAddAfterCareDetails", canAddAfterCareDetails(personId));
    
    } else if (CareLeaverContext.WELSH == activeContext) {
      model.addAttribute("canAddAfterCareDetails", false);
      model.addAttribute("canAddCareLeaverDetails", canAddCareLeaverDetails(personId));
    }

    this.getContextHelperService().setupModelForContext(model);
  }

  private boolean canAddCareLeaverDetails(final long personId) {
    final CareLeaverValidator validator = careLeaverValidatorFactory.getCareLeaverValidator();
    try {
      final Person person = personDao.get(personId);
      validator.personHasPreRequisiteData(person, false);
      return true;

    } catch (Exception ignored) {
      return false;
    }
  }

  private boolean canAddAfterCareDetails(final long personId) {
    final AfterCareEligibility eligibility = afterCareService.getAfterCareEligibilityForPerson(personId, new Date());
    final AfterCareValidator validator = careLeaverValidatorFactory.getAfterCareValidator(eligibility);
    try {
      final Person person = personDao.get(personId);
      validator.personHasPreRequisiteData(person, false);
      return true;

    } catch (Exception ignored) {
      return false;
    }
  }
}