package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.facets.security.subject.utils.SecuredSubjectContextTools;
import com.olmgroup.usp.facets.security.subject.vo.SecuredSubjectContextVO;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

@Component("preFlightSecuredViewResolver")
class PreFlightSecuredViewResolverImpl implements PreFlightViewResolver {

  @Lazy
  @Inject
  @Named("securedSubjectContextTools")
  private SecuredSubjectContextTools securedSubjectContextTools;

  @Override
  public final String getViewNameForPersonSubject(final long personId, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    String viewName = null;

    final SecuredSubjectContextVO securedSubjectContext = this.securedSubjectContextTools
        .getSecuredSubjectContextVO(personId, SubjectType.PERSON);

    // Add the securedSubjectContextVO to the model
    model.addAttribute("securedSubjectContextVO", securedSubjectContext);

    // If person subject access is not permitted, redirect 
    // to the person 'landing' page
    if (!securedSubjectContext.getIsSubjectAccessAllowed()) {
      viewName = "redirect:/person?id=" + personId;
    }

    return viewName;
  }

  @Override
  public final String getViewNameForGroupSubject(final long groupId, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    String viewName = null;

    final SecuredSubjectContextVO securedSubjectContext = this.securedSubjectContextTools
        .getSecuredSubjectContextVO(groupId, SubjectType.GROUP);

    // Add the securedSubjectContextVO to the model 
    model.addAttribute("securedSubjectContextVO", securedSubjectContext);

    // If group subject access is not permitted, redirect 
    // to the group 'landing' page
    if (!securedSubjectContext.getIsSubjectAccessAllowed()) {
      viewName = "redirect:/groups/group?id=" + groupId;
    }

    return viewName;
  }
}
