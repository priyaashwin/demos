package com.olmgroup.usp.apps.relationshipsrecording.web.vo;

import java.util.Date;
import java.util.List;

public class GraphicRelationshipVO {
    /** The serial version UID of this class. Needed for serialization. */
    private static final long serialVersionUID = 1L;

    // Class attributes
    private String classifier;
    private String label;
    private long target;
    private Date startDate;
    private Date closeDate;
    private String relationshipClassName;
    private String descrimatorType;
    private int objectVersion;
    private int ancestorLevel;
    private long relationshipId;
    private List<SuggestedRelationshipVO> suggestedRelationshipVOs;
    private boolean isInactiveHistoric;
    private String temporalStatus;
    private Boolean automated; 

	public GraphicRelationshipVO() {
		super();
	}
	
	public GraphicRelationshipVO(String classifier,
	    String label,
	    long target,
	    Date startDate,
	    Date closeDate) {
		super();
		this.classifier = classifier;
	    this.label = label;
	    this.target = target;
		this.startDate = startDate;
		this.closeDate = closeDate;
	}
	public String getClassifier() {
		return classifier;
	}
	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public long getTarget() {
		return target;
	}
	public void setTarget(long target) {
		this.target = target;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public String getRelationshipClassName() {
		return relationshipClassName;
	}

	public void setRelationshipClassName(String relationshipClassName) {
		this.relationshipClassName = relationshipClassName;
	}

	public String getDescrimatorType() {
		return descrimatorType;
	}

	public void setDescrimatorType(String descrimatorType) {
		this.descrimatorType = descrimatorType;
	}

	public int getObjectVersion() {
		return objectVersion;
	}

	public void setObjectVersion(int objectVersion) {
		this.objectVersion = objectVersion;
	}

	public int getAncestorLevel() {
		return ancestorLevel;
	}

	public void setAncestorLevel(int ancestorLevel) {
		this.ancestorLevel = ancestorLevel;
	}

	public long getRelationshipId() {
		return relationshipId;
	}

	public void setRelationshipId(long relationshipId) {
		this.relationshipId = relationshipId;
	}
	
	public List<SuggestedRelationshipVO> getSuggestedRelationshipVOs() {
		return suggestedRelationshipVOs;
	}

	public void setSuggestedRelationshipVOs(
			List<SuggestedRelationshipVO> suggestedRelationshipVOs) {
		this.suggestedRelationshipVOs = suggestedRelationshipVOs;
	}
	
	public boolean getisInactiveHistoric() {
		return isInactiveHistoric;
	}

	public void setisInactiveHistoric(boolean isInactiveHistoric) {
		this.isInactiveHistoric = isInactiveHistoric;
	}

	public String getTemporalStatus() {
		return temporalStatus;
	}

	public void setTemporalStatus(String temporalStatus) {
		this.temporalStatus = temporalStatus;
	}
	
	public Boolean getAutomated() {
		return automated;
	}

	public void setAutomated(Boolean automated) {
		this.automated = automated;
	}
}
