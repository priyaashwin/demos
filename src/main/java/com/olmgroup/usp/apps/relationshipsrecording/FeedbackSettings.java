package com.olmgroup.usp.apps.relationshipsrecording;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Acts as a placeholder for settings about feedback functionality. Currently these are resolved from the app's .properties file
 * @author barnabyr
 *
 */
@Component("FeedbackSettings")
public class FeedbackSettings {

	private boolean enable=false;
	private String url;
	private String width;
	private String height;	
	private String launch;
	
	public boolean isEnable() {
		return enable;
	}
	
	@Value("${usp.app.feedback.enabled:false}")
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	
	public String getUrl() {
		return url;
	}
	
	@Value("${usp.app.feedback.url:none}")
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public String getHeight() {
		return height;
	}
	
	@Value("${usp.app.feedback.height:600}")
	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}
	@Value("${usp.app.feedback.width:400}")
	public void setWidth(String width) {
		this.width = width;
	}

	public String getLaunch() {
		return launch;
	}
	@Value("${usp.app.feedback.launch:window}")
	public void setLaunch(String launch) {
		this.launch = launch;
	}

	
	
}
