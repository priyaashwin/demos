package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.components.checklist.domain.ChecklistInstance;
import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.inject.Named;

@Controller
@Named("homeController")
@RequestMapping("/")
class HomeController implements LazilyInitialized {

  @Lazy
  @Inject
  PermissionEvaluator permissionEvaluator;

  @RequestMapping(method = RequestMethod.GET)
  public String processHome(ModelMap model) {
    return getHomeForRole();
  }

  @RequestMapping(value = "index", method = RequestMethod.GET)
  public String processIndex(ModelMap model) {
    return getHomeForRole();
  }

  /**
   * Returns the home page based on the current permissions
   * the user has.
   * This is currently a choice between "/home/checklists" and
   * "/home/allocation".
   * 
   * @return
   */
  private String getHomeForRole() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (permissionEvaluator.hasPermission(auth, ChecklistInstance.class, "ChecklistInstance.View")) {
      return "redirect:/home/checklists";
    }

    return "redirect:/home/allocation";
  }
}