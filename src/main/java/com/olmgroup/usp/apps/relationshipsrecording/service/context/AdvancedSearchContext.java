package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Defines supported context values for advanced search.
 * 
 * @author Richard.Gibson
 */

@XmlEnum
@XmlType(name = "AdvancedSearchContext")
public enum AdvancedSearchContext {
  /**
   * Scottish context.
   */
  @XmlEnumValue("SCOT")
  SCOTTISH,

  /**
   * Default context which is effectively ENGLISH.
   */
  @XmlEnumValue("DEFAULT")
  DEFAULT;

}
