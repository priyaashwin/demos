// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.service.HeaderControllerViewService;
import com.olmgroup.usp.components.configuration.vo.CategoryContextVO;
import com.olmgroup.usp.components.organisation.vo.OrganisationWithPreviousNamesVO;
import com.olmgroup.usp.facets.search.SortSelection;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OrganisationViewController
 */
@Component
final class OrganisationViewControllerHelperImpl extends OrganisationViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  private HeaderControllerViewService _headerControllerViewService;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.OrganisationViewController#getOrganisation(final Long
   *      orgId, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleGetOrganisation(final Long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model)
      throws Exception {
    model.addAttribute("organisation",
        getOrganisationService().findById(idParam, OrganisationWithPreviousNamesVO.class));
    model.addAttribute("organisationContacts", getOrganisationService().findContactByOrganisationId(idParam));
    // Reference Number Details
    final SortSelection sortSelection = new SortSelection();
    sortSelection.addAscendingOrderedField("type", true);
    model.addAttribute("organisationReferences",
        getOrganisationReferenceNumberService().findByOrganisationId(idParam, 1, 100, sortSelection).getResults());
    setupModelWithSubdivisionLookupId(model);
    setupModelForOrganisation(idParam, model);
    this.getContextHelperService().setupModelForContext(model);
    return "organisation";
  }

  private void setupModelForOrganisation(final Long organisationId, final Model model) {
    _headerControllerViewService.setupModelWithSubject(organisationId, SubjectType.ORGANISATION, model);
  }

  private void setupModelWithSubdivisionLookupId(final Model model) {
    final CategoryContextVO categoryContextVO = getCategoryContextService()
        .findCategoryContextByCode("com.olmgroup.usp.components.address.categoryContext.SubdivisionsForCountry");
    if (null != categoryContextVO) {
      model.addAttribute("countrySubdivisionContextId", categoryContextVO.getId());
    }

  }

}