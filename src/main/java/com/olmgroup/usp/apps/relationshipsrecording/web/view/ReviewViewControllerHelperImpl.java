// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.apps.relationshipsrecording.web.PreFlightViewResolver;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.ReviewViewController
 */
@Component
final class ReviewViewControllerHelperImpl extends ReviewViewControllerHelperBaseImpl {

  @Lazy
  @Inject
  @Named("preFlightSecuredViewResolver")
  private PreFlightViewResolver preFlightViewResolver;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. ReviewViewController#personReview(final long idParam,
   *      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handlePersonReview(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForPersonSubject(
            idParam, webRequest, servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.PERSON, model);

    this.getContextHelperService().setupModelForContext(model);

    return "review";
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view. ReviewViewController#groupReview(final long idParam,
   *      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleGroupReview(final long idParam, final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model) {
    final String preFlightViewName = this.preFlightViewResolver
        .getViewNameForGroupSubject(
            idParam, webRequest, servletResponse, model);

    if (null != preFlightViewName) {
      return preFlightViewName;
    }

    this.getHeaderControllerViewService().setupModelWithSubject(idParam, SubjectType.GROUP, model);

    this.getContextHelperService().setupModelForContext(model);

    return "review";
  }
}