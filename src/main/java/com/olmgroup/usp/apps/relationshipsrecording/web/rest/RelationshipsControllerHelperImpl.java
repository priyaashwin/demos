// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

import com.olmgroup.usp.apps.relationshipsrecording.domain.RelationshipsHelper;
import com.olmgroup.usp.apps.relationshipsrecording.web.vo.GraphicOrganisationModelVO;
import com.olmgroup.usp.apps.relationshipsrecording.web.vo.GraphicPersonModelVO;
import com.olmgroup.usp.apps.relationshipsrecording.web.vo.GraphicRelationshipVO;
import com.olmgroup.usp.apps.relationshipsrecording.web.vo.GraphicRelationshipsModelVO;
import com.olmgroup.usp.apps.relationshipsrecording.web.vo.SuggestedRelationshipVO;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.components.relationship.domain.AncestorRoleDesignator;
import com.olmgroup.usp.components.relationship.domain.PersonPersonRelationshipDate;
import com.olmgroup.usp.components.relationship.domain.RelationshipRole;
import com.olmgroup.usp.components.relationship.vo.FamilialRelationshipTypeVO;
import com.olmgroup.usp.components.relationship.vo.PersonPersonRelationshipDetailsVO;
import com.olmgroup.usp.components.relationship.vo.PersonPersonRelationshipQualifier;
import com.olmgroup.usp.components.relationship.vo.PersonPersonRelationshipTypeVO;
import com.olmgroup.usp.components.relationship.vo.PersonPersonRelationshipVO;
import com.olmgroup.usp.components.relationship.vo.ProfessionalRelationshipTypeVO;
import com.olmgroup.usp.components.relationship.vo.RelationshipRoleGenderTypeVO;
import com.olmgroup.usp.components.relationship.vo.RelationshipSuggestionsFilter;
import com.olmgroup.usp.components.relationship.vo.SuggestedPersonPersonRelationshipVO;
import com.olmgroup.usp.components.relationship.vo.SuggestedPersonPersonRelationshipsVO;
import com.olmgroup.usp.facets.core.util.TemporalStatusFilter;
import com.olmgroup.usp.facets.search.FieldOrder;
import com.olmgroup.usp.facets.search.PaginationResult;
import com.olmgroup.usp.facets.search.SortOrder;
import com.olmgroup.usp.facets.search.SortSelection;
import com.olmgroup.usp.facets.search.util.PaginationResultTools;
import com.olmgroup.usp.facets.security.subject.utils.SecuredSubjectContextTools;
import com.olmgroup.usp.facets.subject.SubjectType;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.RelationshipsController
 */
@Component
class RelationshipsControllerHelperImpl extends
    RelationshipsControllerHelperBaseImpl {

  @Lazy
  @Inject
  private SecuredSubjectContextTools securedSubjectContextTools;

  private static final int PAGE_ONE = 1;
  private static final int PAGE_SIZE_MAX = 1000;
  private static final String OTHER_RELATIONSHIP_TYPE = "OTHER";

  private static final String STEP_ROLE_QUALIFIER = "Step-";
  private static final String ADOPTIVE_ROLE_QUALIFIER = "Adoptive-";
  private static final String HALF_ROLE_QUALIFIER = "Half-";
  private static final String INLAW_ROLE_QUALIFIER = "-in-law";

  private static final String UNION = "UNION";
  private static final String UNKNOWN = "UNKNOWN";
  private static final String INDETERMINATE = "INDETERMINATE";

  private enum Role {
    ROLE_A, ROLE_B
  }

  // suggestions do not have relationshipid's - which are required by graph -
  // so dummy id's are created using a big negative number
  private static long dummyRelationshipIdForSuggestions = -92233720368547L;

  /*
   * a no argument construtor to initiate the super class instance
   */
  public RelationshipsControllerHelperImpl() {
    super();
  }

  private void checkSortFields(final SortSelection sort) {
    if (sort.getSortOrders().isEmpty()) {
      sort.setSortOrders(new ArrayList<SortOrder>() {
        private static final long serialVersionUID = 1L;
        // Add default sort order name
        {
          this.add(new FieldOrder("roleBPerson.surname", 1));
        };
      });
    }
  }

  // returns a personModel populated with all relevant relationships suitable
  // for graphing
  @SuppressWarnings("rawtypes")
  private GraphicPersonModelVO makeGraphicPerson(final long personId,
      final boolean excludeInactive, final Set uniqueTargetIds, final boolean includeProfessional) {
    final GraphicPersonModelVO personModel = new GraphicPersonModelVO();
    final int differentialLevel = 3;
    final SortSelection sort = new SortSelection();
    this.checkSortFields(sort);

    final PaginationResult personalPR = getPersonPersonRelationshipService()
        .findPersonalByPersonId(
            personId,
            differentialLevel,
            PAGE_ONE,
            PAGE_SIZE_MAX,
            sort,
            (excludeInactive ? TemporalStatusFilter.ALL
                : TemporalStatusFilter.ALL),
            PersonPersonRelationshipDetailsVO.class);
    final PaginationResult otherPR = getPersonPersonRelationshipService()
        .findOtherByPersonId(
            personId,
            PAGE_ONE,
            PAGE_SIZE_MAX,
            sort,
            (excludeInactive ? TemporalStatusFilter.ALL
                : TemporalStatusFilter.ALL),
            PersonPersonRelationshipDetailsVO.class);
    final PaginationResult professionalPR;
    if (includeProfessional) {
      professionalPR = getPersonPersonRelationshipService()
          .findProfessionalByPersonId(
              personId,
              PAGE_ONE,
              PAGE_SIZE_MAX,
              sort,
              (excludeInactive ? TemporalStatusFilter.ALL
                  : TemporalStatusFilter.ALL),
              PersonPersonRelationshipDetailsVO.class);
      personModel.setProfessionalRelationships(getGraphicRelations(personId,
          professionalPR, uniqueTargetIds));
    } else {
      professionalPR = PaginationResultTools.createEmptyPaginationResult(PAGE_ONE, PAGE_SIZE_MAX, sort,
          PersonPersonRelationshipDetailsVO.class);
      personModel.setProfessionalRelationships(new ArrayList<GraphicRelationshipVO>());
    }

    final List<GraphicRelationshipVO> personalRelations = getGraphicRelations(
        personId, personalPR, uniqueTargetIds);
    personalRelations.addAll(getGraphicRelations(personId, otherPR,
        uniqueTargetIds));
    personModel.setPersonalRelationships(personalRelations);

    personModel
        .setOrganisationRelationships(new ArrayList<GraphicRelationshipVO>());

    // grab the person's details from the first available relationship
    PersonVO personVO = null;
    List relations = personalPR.getResults();
    if (relations.isEmpty()) {
      relations = otherPR.getResults();
    }
    if (relations.isEmpty()) {
      relations = professionalPR.getResults();
    }
    if (!relations.isEmpty()) {
      final PersonPersonRelationshipDetailsVO resultVO = (PersonPersonRelationshipDetailsVO) relations
          .get(0);
      if (resultVO.getRoleAPersonId() == personId) {
        personVO = resultVO.getRoleAPersonVO();
      } else {
        personVO = resultVO.getRoleBPersonVO();
      }
    } else {
      // no relationships to get the person details from, so get the
      // person directly
      personVO = getPersonService().findById(personId);
    }
    personModel.setId(personVO.getId());
    personModel.setPersonId(personVO.getPersonIdentifier());
    personModel.setSurname(personVO.getSurname());
    personModel.setForename(personVO.getForename());
    personModel.setName(personVO.getName());
    personModel.setPersonTypes(personVO.getPersonTypes());
    if (personVO.getDateOfBirth() != null) {
      personModel.setDateOfBirth(personVO.getDateOfBirth()
          .getCalculatedDate());
      personModel.setFuzzyDateMask(personVO.getDateOfBirth()
          .getFuzzyDateMask());
    }
    personModel.setAge(personVO.getAge());
    if (personVO.getDiedDate() != null) {
      personModel.setDiedDate(personVO.getDiedDate().getCalculatedDate());
      personModel.setFuzzyDiedDateMask(personVO.getDiedDate()
          .getFuzzyDateMask());
    }
    personModel.setLifeState(personVO.getLifeState());
    personModel.setDueDate(personVO.getDueDate());
    personModel.setGender(personVO.getGender());
    personModel.setEthnicity(personVO.getEthnicity());
    personModel.setDateOfBirthEstimated(personVO.getDateOfBirthEstimated());
    personModel.setDiedDateEstimated(personVO.getDiedDateEstimated());
    personModel.setNumberOfWarnings(getPersonWarningService()
        .getCountByPersonId(personVO.getId()));
    personModel.setAccessAllowed(this.securedSubjectContextTools
        .isSubjectContextAllowed(personVO.getId(), SubjectType.PERSON));

    // personModel.setPersonWarningVO(getPersonService()
    // .findById(personId, PersonWithWarningVO.class)
    // .getPersonWarnings().get(0));

    return personModel;
  }

  // returns a list of relevant relations for a person with focus, using a
  // result set. When uniqueTargetIds is set all target ids are verified to
  // exist in this list.
  @SuppressWarnings("unchecked")
  private List<GraphicRelationshipVO> getGraphicRelations(
      final long personId, final PaginationResult paginationResult,
      final Set uniqueTargetIds) {
    final List<GraphicRelationshipVO> relations = new ArrayList<GraphicRelationshipVO>();
    if (paginationResult != null) {
      final List<PersonPersonRelationshipDetailsVO> resultVOs = (List<PersonPersonRelationshipDetailsVO>) paginationResult
          .getResults();

      for (PersonPersonRelationshipDetailsVO resultVO : resultVOs) {

        final GraphicRelationshipVO relationshipVO = new GraphicRelationshipVO();
        relationshipVO.setRelationshipId(resultVO.getId());
        PersonVO personVO = null;
        if (resultVO.getRoleAPersonId() == personId) {
          personVO = resultVO.getRoleBPersonVO();
        } else {
          personVO = resultVO.getRoleAPersonVO();
        }
        if (isInList(personVO.getId(), uniqueTargetIds)) {
          relationshipVO
              .setLabel(getRelationshipName(personId == resultVO
                  .getRoleAPersonId() ? Role.ROLE_B
                      : Role.ROLE_A,
                  resultVO, personVO
                      .getGender()));
          relationshipVO.setTarget(personVO.getId());
          relationshipVO.setStartDate(resultVO.getStartDate());
          relationshipVO.setCloseDate(resultVO.getCloseDate());
          if (resultVO
              .getTemporalStatus()
              .value()
              .equalsIgnoreCase(
                  TemporalStatusFilter.INACTIVE_HISTORIC
                      .value())) {
            relationshipVO.setisInactiveHistoric(true);
          } else {
            relationshipVO.setisInactiveHistoric(false);
          }

          relationshipVO.setTemporalStatus(resultVO
              .getTemporalStatus().value());

          relationshipVO.setDescrimatorType(resultVO
              .getDiscriminatorType());
          relationshipVO
              .setObjectVersion(resultVO.getObjectVersion());
          relationshipVO.setAutomated(resultVO.getAutomated());

          if (resultVO.getPersonPersonRelationshipTypeVO() != null) {
            // relationshipVO.setClassifier(resultVO.getPersonPersonRelationshipTypeVO().getRelationshipClass());
            relationshipVO.setClassifier(resultVO
                .getPersonPersonRelationshipTypeVO().getClass()
                .getSimpleName());

            relationshipVO.setRelationshipClassName(resultVO
                .getPersonPersonRelationshipTypeVO()
                .getRelationshipClassName());

            if (resultVO.getPersonPersonRelationshipTypeVO() instanceof FamilialRelationshipTypeVO) {
              final FamilialRelationshipTypeVO familialRelationshipTypeVO = (FamilialRelationshipTypeVO) resultVO
                  .getPersonPersonRelationshipTypeVO();
              relationshipVO
                  .setDescrimatorType(familialRelationshipTypeVO
                      .getDiscriminatorType());
              int ancestorMultiplier = 0;
              if (familialRelationshipTypeVO.getAncestorRole()
                  .equals(AncestorRoleDesignator.ROLE_A)
                  && (personId == resultVO.getRoleAPersonId())) {
                ancestorMultiplier = 1;
              } else if (familialRelationshipTypeVO
                  .getAncestorRole().equals(
                      AncestorRoleDesignator.ROLE_B)
                  && (personId == resultVO.getRoleAPersonId())) {
                ancestorMultiplier = -1;
              } else if (familialRelationshipTypeVO
                  .getAncestorRole().equals(
                      AncestorRoleDesignator.ROLE_A)
                  && (personId == resultVO.getRoleBPersonId())) {
                ancestorMultiplier = -1;
              } else if (familialRelationshipTypeVO
                  .getAncestorRole().equals(
                      AncestorRoleDesignator.ROLE_B)
                  && (personId == resultVO.getRoleBPersonId())) {
                ancestorMultiplier = 1;
              }
              relationshipVO
                  .setAncestorLevel(familialRelationshipTypeVO
                      .getFamilialDifferential()
                      * ancestorMultiplier);
            }

          } else {
            relationshipVO.setClassifier(OTHER_RELATIONSHIP_TYPE);
          }

          relations.add(relationshipVO);
        }
      }
    }
    return relations;
  }

  // returns true if id is in set of unique ids, or no unique ids were
  // provided
  private boolean isInList(final long id, final Set uniqueTargetIds) {
    return uniqueTargetIds == null || uniqueTargetIds.contains(id);
  }

  // returns the name (label) for the relationship, depending on gender of
  // person at role A or B, and according to qualifiers e.g. Son, Step-Son,
  // Son-in-law
  private String getRelationshipName(final Role role,
      final PersonPersonRelationshipDetailsVO source, final String gender) {
    String relationshipName = null;
    final PersonPersonRelationshipTypeVO personPersonRelationshipTypeVO = source
        .getPersonPersonRelationshipTypeVO();
    if (personPersonRelationshipTypeVO != null) {

      final PersonVO roleAPerson = source.getRoleAPersonVO();
      final PersonVO roleBPerson = source.getRoleBPersonVO();

      if (personPersonRelationshipTypeVO.getDescription() != null
          && personPersonRelationshipTypeVO.getDescription()
              .equalsIgnoreCase(UNION)
          && (roleAPerson.getGender() == null
              || roleBPerson.getGender() == null
              || roleAPerson.getGender()
                  .equalsIgnoreCase(UNKNOWN)
              || roleAPerson.getGender().equalsIgnoreCase(
                  INDETERMINATE)
              || roleBPerson.getGender()
                  .equalsIgnoreCase(UNKNOWN)
              || roleBPerson.getGender().equalsIgnoreCase(
                  INDETERMINATE)
              || roleAPerson.getGender()
                  .equalsIgnoreCase(roleBPerson.getGender()))) {

        relationshipName = RelationshipsHelper
            .getGenderFreeUnionRelationshipLabel(personPersonRelationshipTypeVO
                .getRelationshipClassName());

      } else if (gender != null) {
        final Map<String, RelationshipRoleGenderTypeVO> relationshipRoleGenderTypes = personPersonRelationshipTypeVO
            .getRelationshipRoleGenderTypeVOs();
        for (String key : relationshipRoleGenderTypes.keySet()) {
          final RelationshipRoleGenderTypeVO relationshipRoleGenderType = relationshipRoleGenderTypes
              .get(key);
          if (relationshipRoleGenderType.getGender().equals(gender)) {
            relationshipName = role == Role.ROLE_A ? relationshipRoleGenderType
                .getRoleAName()
                : relationshipRoleGenderType
                    .getRoleBName();
          }
        }
      }

      if (relationshipName == null) {
        relationshipName = role == Role.ROLE_A ? personPersonRelationshipTypeVO
            .getRoleAName()
            : personPersonRelationshipTypeVO
                .getRoleBName();
      }
    }

    if (source.getStep() != null && source.getStep()) {
      relationshipName = STEP_ROLE_QUALIFIER + relationshipName;
    }
    if (source.getHalf() != null && source.getHalf()) {
      relationshipName = HALF_ROLE_QUALIFIER + relationshipName;
    }
    if (source.getInLaw() != null && source.getInLaw()) {
      relationshipName = relationshipName + INLAW_ROLE_QUALIFIER;
    }
    if (source.getAdoptive() != null && source.getAdoptive()) {
      relationshipName = ADOPTIVE_ROLE_QUALIFIER + relationshipName;
    }

    return relationshipName;
  }

  private int calculateAge(final Date dateOfBirth) {
    int age = 0;

    if (null != dateOfBirth) {
      final Years years = Years.yearsBetween(new LocalDate(dateOfBirth),
          new LocalDate());
      age = years.getYears();
    }

    return age;
  }

  private String makeIdString(final long id, final String prefix,
      final int length) {
    final StringBuffer strId = new StringBuffer();
    strId.append(id);
    for (int i = strId.length() + prefix.length(); i < length; i++) {
      strId.insert(0, '0');
    }

    return strId.insert(0, prefix).toString();
  }

  // returns relationships for an in focus person suitable for graphing
  @Override
  public final GraphicRelationshipsModelVO handleFindAllByPersonId(
      final long personId, final Boolean excludeInactiveParam,
      final Boolean includeSuggestionsParam,
      final Long suggestionsRelationshipIdParam,
      final WebRequest webRequest,
      final HttpServletResponse servletResponse, final Model model)
      throws Exception {

    // default to exclude inactive
    final boolean excludeInactive = (excludeInactiveParam != null) ? excludeInactiveParam
        : true;

    boolean includeRelationshipSuggestions = false;
    boolean includePersonNetworkSuggestions = false;
    if (includeSuggestionsParam != null && includeSuggestionsParam
        && suggestionsRelationshipIdParam != null) {
      includeRelationshipSuggestions = true;
    } else if (includeSuggestionsParam != null && includeSuggestionsParam
        && suggestionsRelationshipIdParam == null) {
      includePersonNetworkSuggestions = true;
    }

    // setup graphic model data structure
    final GraphicRelationshipsModelVO graphicRelationshipsModelVO = new GraphicRelationshipsModelVO();
    graphicRelationshipsModelVO.setRootId(personId);
    graphicRelationshipsModelVO.setRootClassifier("Person");

    // suggestions for newly created relationship
    // suggestionsRelationshipId - newly created relationship id
    List<SuggestedPersonPersonRelationshipsVO> suggestedPersonPersonRelationshipsVOs = null;
    // secondaryPersonId -
    //
    long secondaryPersonId = -1;
    if (includeRelationshipSuggestions) {
      final PersonPersonRelationshipVO personPersonRelationshipVO = getPersonPersonRelationshipService()
          .findById(suggestionsRelationshipIdParam);
      if (personPersonRelationshipVO != null
          && (personPersonRelationshipVO.getRoleAPersonId() == personId || personPersonRelationshipVO
              .getRoleBPersonId() == personId)) {
        suggestedPersonPersonRelationshipsVOs = getPersonPersonRelationshipService()
            .getSuggestionsForPersonPersonRelationship(
                suggestionsRelationshipIdParam,
                RelationshipSuggestionsFilter.SUGGESTED_RELATIONS);
        if (personPersonRelationshipVO.getRoleAPersonId() == personId) {
          secondaryPersonId = personPersonRelationshipVO
              .getRoleBPersonId();
        } else {
          secondaryPersonId = personPersonRelationshipVO
              .getRoleAPersonId();
        }
      }
    }

    // suggestions for nominated person
    // means suggestions for all the people in the network
    if (includePersonNetworkSuggestions) {
      suggestedPersonPersonRelationshipsVOs = getPersonPersonRelationshipService()
          .findSuggestedByPersonId(personId,
              RelationshipSuggestionsFilter.SUGGESTED_RELATIONS);
      if (!CollectionUtils.isEmpty(suggestedPersonPersonRelationshipsVOs)) {
        long numberOfSuggestions = 0;
        for (SuggestedPersonPersonRelationshipsVO suggestedPersonPersonRelationshipsVO : suggestedPersonPersonRelationshipsVOs) {
          if (!CollectionUtils
              .isEmpty(suggestedPersonPersonRelationshipsVO
                  .getSuggestions())) {
            numberOfSuggestions = numberOfSuggestions
                + suggestedPersonPersonRelationshipsVO
                    .getSuggestions().size();
          }
        }
        // set numbers of suggestions
        graphicRelationshipsModelVO
            .setNumberOfSuggestions(numberOfSuggestions);
      }

    }

    // add root person to people array
    final List<GraphicPersonModelVO> people = new ArrayList<GraphicPersonModelVO>();
    final GraphicPersonModelVO rootPerson = this.makeGraphicPerson(
        personId, excludeInactive, null, true);
    // Add suggestions to the person model
    rootPerson
        .setSuggestedPersonalRelationships(getPersonsSuggestedRelationships(
            personId, suggestedPersonPersonRelationshipsVOs));

    people.add(rootPerson);

    // scan root person relations for unique target ids
    final List<GraphicRelationshipVO> rootRelations = new ArrayList<GraphicRelationshipVO>(
        rootPerson.getPersonalRelationships());
    rootRelations.addAll(rootPerson.getProfessionalRelationships());
    rootRelations.addAll(rootPerson.getSuggestedPersonalRelationships());

    final Set<Long> uniqueIds = new HashSet<Long>();
    for (GraphicRelationshipVO relation : rootRelations) {
      uniqueIds.add(relation.getTarget());
    }
    uniqueIds.add(personId);

    // for each unique target (other than the root person) get relations
    // that exist in the graph
    for (Long id : uniqueIds) {
      if (id != personId) {
        final GraphicPersonModelVO personModel = this
            .makeGraphicPerson(id, excludeInactive, uniqueIds, false);
        if (includePersonNetworkSuggestions) {
          personModel
              .setSuggestedPersonalRelationships(getPersonsSuggestedRelationships(
                  id, suggestedPersonPersonRelationshipsVOs));
        } else if (includeRelationshipSuggestions
            && id == secondaryPersonId) {
          personModel
              .setSuggestedPersonalRelationships(getPersonsSuggestedRelationships(
                  id, suggestedPersonPersonRelationshipsVOs));
        }
        people.add(personModel);
      }
    }
    graphicRelationshipsModelVO.setPeople(people);

    // TODO
    graphicRelationshipsModelVO
        .setOrganisations(new ArrayList<GraphicOrganisationModelVO>());
    return graphicRelationshipsModelVO;
  }

  /**
   * Gets the suggestions for the given person as List<GraphicRelationshipVO>
   * 
   * @param personId
   * @param suggestedPersonPersonRelationshipsVOs
   * @return List<GraphicRelationshipVO>
   */
  private List<GraphicRelationshipVO> getPersonsSuggestedRelationships(
      final long personId,
      final List<SuggestedPersonPersonRelationshipsVO> suggestedPersonPersonRelationshipsVOs) {

    final List<GraphicRelationshipVO> suggestedPersonalRelationships = new ArrayList<GraphicRelationshipVO>();

    if (!CollectionUtils.isEmpty(suggestedPersonPersonRelationshipsVOs)) {

      final Iterator<SuggestedPersonPersonRelationshipsVO> suggestionsIterator = suggestedPersonPersonRelationshipsVOs
          .iterator();
      while (suggestionsIterator.hasNext()) {
        final SuggestedPersonPersonRelationshipsVO suggestedPersonPersonRelationshipsVO = suggestionsIterator
            .next();
        final PersonVO firstPersonVO = suggestedPersonPersonRelationshipsVO
            .getFirstPerson();
        final PersonVO secondPersonVO = suggestedPersonPersonRelationshipsVO
            .getSecondPerson();
        if (firstPersonVO.getId() == personId
            || secondPersonVO.getId() == personId) {

          final List<SuggestedPersonPersonRelationshipVO> professionalSuggestions = new ArrayList<SuggestedPersonPersonRelationshipVO>();
          final List<SuggestedPersonPersonRelationshipVO> suggestions = suggestedPersonPersonRelationshipsVO
              .getSuggestions();
          final Iterator<SuggestedPersonPersonRelationshipVO> personSuggestionIterator = suggestions
              .iterator();
          // remove professional suggestions from all suggestions
          while (personSuggestionIterator.hasNext()) {
            final SuggestedPersonPersonRelationshipVO suggestedRelationshipVO = personSuggestionIterator
                .next();
            if (suggestedRelationshipVO
                .getPersonPersonRelationshipType() instanceof ProfessionalRelationshipTypeVO) {
              professionalSuggestions
                  .add(suggestedRelationshipVO);
              personSuggestionIterator.remove();
            }
          }
          // add separate relationship(suggestion) for each
          // professional relationship
          if (!CollectionUtils.isEmpty(professionalSuggestions)) {

            for (SuggestedPersonPersonRelationshipVO suggestedPersonPersonRelationshipVO : professionalSuggestions) {

              final GraphicRelationshipVO graphicRelationshipVO = new GraphicRelationshipVO();
              graphicRelationshipVO.setClassifier("SUGGESTION");
              graphicRelationshipVO.setLabel("Suggestion");
              graphicRelationshipVO
                  .setDescrimatorType("SuggestedPersonPersonRelationship");

              if (firstPersonVO.getId() == personId) {
                graphicRelationshipVO.setTarget(secondPersonVO
                    .getId());
              } else {
                graphicRelationshipVO.setTarget(firstPersonVO
                    .getId());
              }

              final List<SuggestedRelationshipVO> suggestedRelationshipVOs = new ArrayList<SuggestedRelationshipVO>();
              RelationshipRole role = null;
              // setting the role played by the given person id
              if (firstPersonVO.getId() == personId) {
                role = suggestedPersonPersonRelationshipVO
                    .getFirstPersonRole();
              } else {
                if (suggestedPersonPersonRelationshipVO
                    .getFirstPersonRole() == RelationshipRole.ROLE_A) {
                  role = RelationshipRole.ROLE_B;
                } else {
                  role = RelationshipRole.ROLE_A;
                }
              }

              final SuggestedRelationshipVO suggestedRelationshipDetails = new SuggestedRelationshipVO();

              suggestedRelationshipDetails.setRole(role);

              suggestedRelationshipDetails
                  .setRelationshipTypeId(suggestedPersonPersonRelationshipVO
                      .getPersonPersonRelationshipType()
                      .getId());
              suggestedRelationshipDetails
                  .setDiscriminatorType(suggestedPersonPersonRelationshipVO
                      .getPersonPersonRelationshipType()
                      .getDiscriminatorType());

              // Relationship label is the relationship(suggested)
              // name(example brother/sister/husband) to be
              // displayed on the arrow
              // On graph - for showing suggestion from A to B -
              // the arrow shows from A to B - but the
              // relationship(suggested relationship) is from B to
              // A
              // if A(Male) is brother of B(female) - on the arrow
              // from A to B we show the relationship(suggested)
              // role played by B - here we show sister
              // It should be read as B is the sister of A.

              // set the role displayed on the arrow(which is
              // reverse of the given persons role)
              // i.e role played by the person on the end of the
              // arrow
              RelationshipRole suggestiosRole = null;
              if (role == RelationshipRole.ROLE_A) {
                suggestiosRole = RelationshipRole.ROLE_B;
              } else {
                suggestiosRole = RelationshipRole.ROLE_A;
              }
              if (personId == firstPersonVO.getId()) {
                // suggestedRelationshipDetails.setRelationshipLabel(getRelationshipLabel(suggestedPersonPersonRelationshipVO,
                // firstPersonVO, secondPersonVO, true, null));
                suggestedRelationshipDetails
                    .setRelationshipLabel(getRelationshipLabel(
                        suggestedPersonPersonRelationshipVO,
                        secondPersonVO, firstPersonVO,
                        suggestiosRole, null));
              } else {
                // suggestedRelationshipDetails.setRelationshipLabel(getRelationshipLabel(suggestedPersonPersonRelationshipVO,
                // firstPersonVO, secondPersonVO, false, null));
                suggestedRelationshipDetails
                    .setRelationshipLabel(getRelationshipLabel(
                        suggestedPersonPersonRelationshipVO,
                        firstPersonVO, secondPersonVO,
                        suggestiosRole, null));
              }
              suggestedRelationshipVOs
                  .add(suggestedRelationshipDetails);

              graphicRelationshipVO
                  .setSuggestedRelationshipVOs(suggestedRelationshipVOs);
              graphicRelationshipVO
                  .setRelationshipId(dummyRelationshipIdForSuggestions());
              graphicRelationshipVO
                  .setRelationshipClassName("SUGGESTION");
              suggestedPersonalRelationships
                  .add(graphicRelationshipVO);
            }

          }
          // add only one relationship(suggestion) for all personal
          // relationship(one arrow dropdown will have all the
          // personal suggestions)
          if (!CollectionUtils.isEmpty(suggestions)) {
            final GraphicRelationshipVO graphicRelationshipVO = new GraphicRelationshipVO();
            graphicRelationshipVO.setClassifier("SUGGESTION");
            graphicRelationshipVO.setLabel("Suggestion");
            graphicRelationshipVO
                .setDescrimatorType("SuggestedPersonPersonRelationship");

            if (firstPersonVO.getId() == personId) {
              graphicRelationshipVO.setTarget(secondPersonVO
                  .getId());
            } else {
              graphicRelationshipVO.setTarget(firstPersonVO
                  .getId());
            }
            // set Ancestor level

            final SuggestedPersonPersonRelationshipVO suggestedRelationshipVO = suggestions
                .get(0);
            final PersonPersonRelationshipTypeVO personPersonRelationshipTypeVO = suggestedRelationshipVO
                .getPersonPersonRelationshipType();

            if (personPersonRelationshipTypeVO instanceof FamilialRelationshipTypeVO) {
              final FamilialRelationshipTypeVO familialRelationshipTypeVO = (FamilialRelationshipTypeVO) personPersonRelationshipTypeVO;
              int ancestorMultiplier = 0;
              if (familialRelationshipTypeVO.getAncestorRole()
                  .equals(AncestorRoleDesignator.ROLE_A)
                  && (personId == firstPersonVO.getId())) {
                ancestorMultiplier = 1;
              } else if (familialRelationshipTypeVO
                  .getAncestorRole().equals(
                      AncestorRoleDesignator.ROLE_B)
                  && (personId == firstPersonVO.getId())) {
                ancestorMultiplier = -1;
              } else if (familialRelationshipTypeVO
                  .getAncestorRole().equals(
                      AncestorRoleDesignator.ROLE_A)
                  && (personId == secondPersonVO.getId())) {
                ancestorMultiplier = -1;
              } else if (familialRelationshipTypeVO
                  .getAncestorRole().equals(
                      AncestorRoleDesignator.ROLE_B)
                  && (personId == secondPersonVO.getId())) {
                ancestorMultiplier = 1;
              }
              graphicRelationshipVO
                  .setAncestorLevel(familialRelationshipTypeVO
                      .getFamilialDifferential()
                      * ancestorMultiplier);
            }

            graphicRelationshipVO
                .setRelationshipId(dummyRelationshipIdForSuggestions());

            final List<SuggestedRelationshipVO> suggestedRelationshipVOs = new ArrayList<SuggestedRelationshipVO>();

            final Date firstPersonDOB = firstPersonVO
                .getDateOfBirth().getCalculatedDate();
            final Boolean firstPersonDOBEstimated = firstPersonVO
                .getDateOfBirthEstimated();
            final Date secondPersonDOB = secondPersonVO
                .getDateOfBirth().getCalculatedDate();
            final Boolean secondPersonDOBEstimated = secondPersonVO
                .getDateOfBirthEstimated();
            Date relationshipStartDate = null;
            Boolean relationshipStartDateEstimated = null;
            if (firstPersonDOB != null && secondPersonDOB != null) {
              if (firstPersonDOB.after(secondPersonDOB)) {
                relationshipStartDate = firstPersonDOB;
                if (firstPersonDOBEstimated != null
                    && firstPersonDOBEstimated) {
                  relationshipStartDateEstimated = firstPersonDOBEstimated;
                }
              } else if (firstPersonDOB.before(secondPersonDOB)) {
                relationshipStartDate = secondPersonDOB;
                if (secondPersonDOBEstimated != null
                    && secondPersonDOBEstimated) {
                  relationshipStartDateEstimated = secondPersonDOBEstimated;
                }
              } else {
                relationshipStartDate = firstPersonDOB;
                if ((firstPersonDOBEstimated != null && firstPersonDOBEstimated)
                    || (secondPersonDOBEstimated != null && secondPersonDOBEstimated)) {
                  relationshipStartDateEstimated = true;
                }
              }
            }
            for (SuggestedPersonPersonRelationshipVO suggestedPersonPersonRelationshipVO : suggestions) {

              RelationshipRole role = null;
              // setting the role played by the given person id
              if (firstPersonVO.getId() == personId) {
                role = suggestedPersonPersonRelationshipVO
                    .getFirstPersonRole();
              } else {
                if (suggestedPersonPersonRelationshipVO
                    .getFirstPersonRole() == RelationshipRole.ROLE_A) {
                  role = RelationshipRole.ROLE_B;
                } else {
                  role = RelationshipRole.ROLE_A;
                }
              }

              if (!CollectionUtils
                  .isEmpty(suggestedPersonPersonRelationshipVO
                      .getPermittedQualifiers())) {

                for (PersonPersonRelationshipQualifier personPersonRelationshipQualifier : suggestedPersonPersonRelationshipVO
                    .getPermittedQualifiers()) {

                  final SuggestedRelationshipVO suggestedRelationshipDetails = new SuggestedRelationshipVO();

                  suggestedRelationshipDetails.setRole(role);

                  suggestedRelationshipDetails
                      .setRelationshipTypeId(suggestedPersonPersonRelationshipVO
                          .getPersonPersonRelationshipType()
                          .getId());
                  suggestedRelationshipDetails
                      .setDiscriminatorType(suggestedPersonPersonRelationshipVO
                          .getPersonPersonRelationshipType()
                          .getDiscriminatorType());

                  // Relationship label is the
                  // relationship(suggested) name(example
                  // brother/sister/husband) to be displayed
                  // on the arrow
                  // On graph - for showing suggestion from A
                  // to B - the arrow shows from A to B - but
                  // the relationship(suggested relationship)
                  // is from B to A
                  // if A(Male) is brother of B(female) - on
                  // the arrow from A to B we show the
                  // relationship(suggested) role played by B
                  // - here we show sister
                  // It should be read as B is the sister of
                  // A.

                  // set the role displayed on the arrow(which
                  // is reverse of the given persons role)
                  // i.e role played by the person on the end
                  // of the arrow
                  RelationshipRole suggestiosRole = null;
                  if (role == RelationshipRole.ROLE_A) {
                    suggestiosRole = RelationshipRole.ROLE_B;
                  } else {
                    suggestiosRole = RelationshipRole.ROLE_A;
                  }
                  if (personId == firstPersonVO.getId()) {
                    // suggestedRelationshipDetails.setRelationshipLabel(getRelationshipLabel(suggestedPersonPersonRelationshipVO,
                    // firstPersonVO, secondPersonVO, true,
                    // personPersonRelationshipQualifier));
                    suggestedRelationshipDetails
                        .setRelationshipLabel(getRelationshipLabel(
                            suggestedPersonPersonRelationshipVO,
                            secondPersonVO,
                            firstPersonVO,
                            suggestiosRole,
                            personPersonRelationshipQualifier));
                  } else {
                    // suggestedRelationshipDetails.setRelationshipLabel(getRelationshipLabel(suggestedPersonPersonRelationshipVO,
                    // firstPersonVO, secondPersonVO, false,
                    // personPersonRelationshipQualifier));
                    suggestedRelationshipDetails
                        .setRelationshipLabel(getRelationshipLabel(
                            suggestedPersonPersonRelationshipVO,
                            firstPersonVO,
                            secondPersonVO,
                            suggestiosRole,
                            personPersonRelationshipQualifier));
                  }
                  suggestedRelationshipDetails
                      .setPersonPersonRelationshipQualifier(personPersonRelationshipQualifier);

                  if (suggestedPersonPersonRelationshipVO
                      .getPersonPersonRelationshipType() instanceof FamilialRelationshipTypeVO
                      && (suggestedPersonPersonRelationshipVO
                          .getPersonPersonRelationshipType()
                          .getStartDateGuidance() != null && suggestedPersonPersonRelationshipVO
                              .getPersonPersonRelationshipType()
                              .getStartDateGuidance()
                              .equals(PersonPersonRelationshipDate.YOUNGEST_PERSON_DATE_OF_BIRTH))
                      && !personPersonRelationshipQualifier
                          .equals(PersonPersonRelationshipQualifier.IN_LAW)
                      && !personPersonRelationshipQualifier
                          .equals(PersonPersonRelationshipQualifier.STEP)
                      && !personPersonRelationshipQualifier
                          .equals(PersonPersonRelationshipQualifier.ADOPTIVE)) {
                    suggestedRelationshipDetails
                        .setRelationshipStartDate(relationshipStartDate);
                    suggestedRelationshipDetails
                        .setRelationshipStartDateEstimated(relationshipStartDateEstimated);
                  }

                  suggestedRelationshipVOs
                      .add(suggestedRelationshipDetails);
                }

              } else {
                final SuggestedRelationshipVO suggestedRelationshipDetails = new SuggestedRelationshipVO();

                suggestedRelationshipDetails.setRole(role);

                suggestedRelationshipDetails
                    .setRelationshipTypeId(suggestedPersonPersonRelationshipVO
                        .getPersonPersonRelationshipType()
                        .getId());
                suggestedRelationshipDetails
                    .setDiscriminatorType(suggestedPersonPersonRelationshipVO
                        .getPersonPersonRelationshipType()
                        .getDiscriminatorType());

                // Relationship label is the
                // relationship(suggested) name(example
                // brother/sister/husband) to be displayed on
                // the arrow
                // On graph - for showing suggestion from A to B
                // - the arrow shows from A to B - but the
                // relationship(suggested relationship) is from
                // B to A
                // if A(Male) is brother of B(female) - on the
                // arrow from A to B we show the
                // relationship(suggested) role played by B -
                // here we show sister
                // It should be read as B is the sister of A.

                // set the role displayed on the arrow(which is
                // reverse of the given persons role)
                // i.e role played by the person on the end of
                // the arrow
                RelationshipRole suggestiosRole = null;
                if (role == RelationshipRole.ROLE_A) {
                  suggestiosRole = RelationshipRole.ROLE_B;
                } else {
                  suggestiosRole = RelationshipRole.ROLE_A;
                }
                if (personId == firstPersonVO.getId()) {
                  // suggestedRelationshipDetails.setRelationshipLabel(getRelationshipLabel(suggestedPersonPersonRelationshipVO,
                  // firstPersonVO, secondPersonVO, true,
                  // null));
                  suggestedRelationshipDetails
                      .setRelationshipLabel(getRelationshipLabel(
                          suggestedPersonPersonRelationshipVO,
                          secondPersonVO,
                          firstPersonVO,
                          suggestiosRole, null));
                } else {
                  // suggestedRelationshipDetails.setRelationshipLabel(getRelationshipLabel(suggestedPersonPersonRelationshipVO,
                  // firstPersonVO, secondPersonVO, false,
                  // null));
                  suggestedRelationshipDetails
                      .setRelationshipLabel(getRelationshipLabel(
                          suggestedPersonPersonRelationshipVO,
                          firstPersonVO,
                          secondPersonVO,
                          suggestiosRole, null));
                }

                if (suggestedPersonPersonRelationshipVO
                    .getPersonPersonRelationshipType() instanceof FamilialRelationshipTypeVO
                    && (suggestedPersonPersonRelationshipVO
                        .getPersonPersonRelationshipType()
                        .getStartDateGuidance() != null && suggestedPersonPersonRelationshipVO
                            .getPersonPersonRelationshipType()
                            .getStartDateGuidance()
                            .equals(PersonPersonRelationshipDate.YOUNGEST_PERSON_DATE_OF_BIRTH))) {
                  suggestedRelationshipDetails
                      .setRelationshipStartDate(relationshipStartDate);
                  suggestedRelationshipDetails
                      .setRelationshipStartDateEstimated(relationshipStartDateEstimated);
                }

                suggestedRelationshipVOs
                    .add(suggestedRelationshipDetails);
              }
            }
            graphicRelationshipVO
                .setSuggestedRelationshipVOs(suggestedRelationshipVOs);
            graphicRelationshipVO
                .setRelationshipClassName("SUGGESTION");
            suggestedPersonalRelationships
                .add(graphicRelationshipVO);
          }
          suggestionsIterator.remove();
        }
      }
    }
    return suggestedPersonalRelationships;
  }

  /**
   * Gets the label for Suggestion Relationship Edge
   * 
   * @param suggestedPersonPersonRelationshipVO
   * @param firstPersonRole
   * @return String
   */
  private String getRelationshipLabel(
      final SuggestedPersonPersonRelationshipVO suggestion,
      final PersonVO firstPersonVO,
      final PersonVO secondPersonVO,
      final RelationshipRole firstPersonRole,
      final PersonPersonRelationshipQualifier personPersonRelationshipQualifier) {

    String label = null;

    if ((suggestion.getPersonPersonRelationshipType().getDescription() != null && "UNION"
        .equalsIgnoreCase(suggestion.getPersonPersonRelationshipType()
            .getDescription()))
        && (firstPersonVO.getGender() == null
            || secondPersonVO.getGender() == null
            || firstPersonVO.getGender().equalsIgnoreCase(
                secondPersonVO.getGender())
            || firstPersonVO.getGender().equalsIgnoreCase(UNKNOWN)
            || secondPersonVO.getGender().equalsIgnoreCase(UNKNOWN)
            || firstPersonVO.getGender().equalsIgnoreCase(
                INDETERMINATE)
            || secondPersonVO.getGender()
                .equalsIgnoreCase(INDETERMINATE))) {

      final String relClass = suggestion
          .getPersonPersonRelationshipType()
          .getRelationshipClassName().toLowerCase();
      String displayClass = relClass.substring(0, 1).toUpperCase()
          + relClass.substring(1);
      if (displayClass.indexOf("relationship") > -1) {
        displayClass = displayClass.substring(0,
            displayClass.indexOf("relationship"));
        displayClass = displayClass.trim();
      }
      if ("Engaged".equalsIgnoreCase(displayClass)) {
        displayClass = displayClass + " partner";
      } else if (displayClass.indexOf("partner") == -1) {
        displayClass = displayClass + " partner";
      }
      label = displayClass;

    } else if (suggestion.getPersonPersonRelationshipType()
        .getRelationshipRoleGenderTypeVOs() != null
        && suggestion.getPersonPersonRelationshipType()
            .getRelationshipRoleGenderTypeVOs().size() > 0) {

      if (firstPersonRole == RelationshipRole.ROLE_A) {

        if (firstPersonVO.getGender() != null
            && "MALE".equalsIgnoreCase(firstPersonVO.getGender())) {
          label = suggestion.getPersonPersonRelationshipType()
              .getRelationshipRoleGenderTypeVOs().get("MALE")
              .getRoleAName();
        } else if (firstPersonVO.getGender() != null
            && "FEMALE".equalsIgnoreCase(firstPersonVO.getGender())) {
          label = suggestion.getPersonPersonRelationshipType()
              .getRelationshipRoleGenderTypeVOs().get("FEMALE")
              .getRoleAName();
        } else {
          label = suggestion.getPersonPersonRelationshipType()
              .getRoleAName();
        }

      } else {

        if (firstPersonVO.getGender() != null
            && "MALE".equalsIgnoreCase(firstPersonVO.getGender())) {
          label = suggestion.getPersonPersonRelationshipType()
              .getRelationshipRoleGenderTypeVOs().get("MALE")
              .getRoleBName();
        } else if (firstPersonVO.getGender() != null
            && "FEMALE".equalsIgnoreCase(firstPersonVO.getGender())) {
          label = suggestion.getPersonPersonRelationshipType()
              .getRelationshipRoleGenderTypeVOs().get("FEMALE")
              .getRoleBName();
        } else {
          label = suggestion.getPersonPersonRelationshipType()
              .getRoleBName();
        }
      }

    } else {

      // not union & do not contain gender roles
      if (firstPersonRole == RelationshipRole.ROLE_A) {
        label = suggestion.getPersonPersonRelationshipType()
            .getRoleAName();
      } else {
        label = suggestion.getPersonPersonRelationshipType()
            .getRoleBName();
      }

    }

    if (personPersonRelationshipQualifier != null) {
      if (personPersonRelationshipQualifier == PersonPersonRelationshipQualifier.HALF) {
        label = "Half-" + label;
      } else if (personPersonRelationshipQualifier == PersonPersonRelationshipQualifier.STEP) {
        label = "Step-" + label;
      } else if (personPersonRelationshipQualifier == PersonPersonRelationshipQualifier.ADOPTIVE) {
        label = "Adoptive-" + label;
      } else if (personPersonRelationshipQualifier == PersonPersonRelationshipQualifier.IN_LAW) {
        label = label + "-InLaw";
      }
    }
    return label;
  }

  /**
   * suggestions do not have relationshipid's - which are required by graph - so dummy id's are created using a big
   * negative number
   * 
   * @return
   */
  private long dummyRelationshipIdForSuggestions() {
    dummyRelationshipIdForSuggestions = dummyRelationshipIdForSuggestions + 1;
    return dummyRelationshipIdForSuggestions;
  }
}