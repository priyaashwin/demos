// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.components.securityextension.config.vo.RecordTypeSecurityConfig;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.TeamAdministrationViewController
 */
@Component
final class TeamAdministrationViewControllerHelperImpl extends TeamAdministrationViewControllerHelperBaseImpl {

  private static final String ADMIN_TEAM_PAGE = "admin.team.page";

  @Lazy
  @Inject
  private RecordTypeSecurityConfig recordTypeSecurityConfig;

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.TeamAdministrationViewController#viewTeam(WebRequest
   *      webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewTeam(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupPageModel(model);
    return ADMIN_TEAM_PAGE;
  }

  @Override
  public String handleViewTeams(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupPageModel(model);
    return ADMIN_TEAM_PAGE;
  }

  @Override
  public String handleViewProfiles(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupPageModel(model);
    return ADMIN_TEAM_PAGE;
  }

  @Override
  public String handleViewDomains(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupPageModel(model);
    return ADMIN_TEAM_PAGE;
  }

  @Override
  public String handleViewAccessLevels(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupPageModel(model);
    return ADMIN_TEAM_PAGE;
  }

  @Override
  public String handleViewDomainContents(final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupPageModel(model);
    return ADMIN_TEAM_PAGE;
  }

  private void setupPageModel(final Model model) {
    model.addAttribute("defaultSecurityDomainCode", recordTypeSecurityConfig.getDefaultSecurityDomainCode());
    this.getContextHelperService().setupModelForContext(model);
  }
}