package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Named;

/**
 * Bridge End demo : Organisation calendar
 */

@Controller
@Named("workerHomeController")
@RequestMapping("/worker-home")
public class WorkerHomeController implements LazilyInitialized {

  @RequestMapping(method = RequestMethod.GET)
  public String processIndex(ModelMap model) {

    return "workerhome";
  }

}