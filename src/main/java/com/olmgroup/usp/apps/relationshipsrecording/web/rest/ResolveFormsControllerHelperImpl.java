// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

import com.olmgroup.usp.apps.relationshipsrecording.vo.CanAddFormsVO;
import com.olmgroup.usp.components.resolveassessment.service.CarePlanRiskAssessmentService;
import com.olmgroup.usp.components.resolveassessment.service.IntakeFormService;
import com.olmgroup.usp.components.resolveassessment.service.PlacementReportService;
import com.olmgroup.usp.facets.springmvc.interceptor.CacheInterceptor;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.ResolveFormsController
 */
@Controller
class ResolveFormsControllerHelperImpl extends
    ResolveFormsControllerHelperBaseImpl {

  // For support of cache header management
  @Lazy
  @Inject
  @Named("cacheInterceptor")
  private CacheInterceptor _cacheInterceptor;

  public ResolveFormsControllerHelperImpl() {
    super();
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.
   *      ResolveFormsController#canAddForms(final long personId, WebRequest
   *      webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public CanAddFormsVO handleCanAddForms(long personId,
      WebRequest webRequest, HttpServletResponse servletResponse,
      Model model) throws Exception {
    CarePlanRiskAssessmentService carePlanRiskAssessmentService = getCarePlanRiskAssessmentService();
    boolean canAddCPRA = carePlanRiskAssessmentService
        .canAddCarePlanRiskAssessment(personId);

    PlacementReportService placementReportService = getPlacementReportService();
    boolean canAddPR = placementReportService
        .canAddPlacementReport(personId);

    IntakeFormService intakeFormService = getIntakeFormService();
    boolean canAddIntake = intakeFormService.canAddIntakeForm(personId);

    CanAddFormsVO canAddFormsVO = new CanAddFormsVO(canAddPR, canAddCPRA,
        canAddIntake);

    // ensure cache is defeated for certain browsers, ahem IE we are looking
    // at you!
    _cacheInterceptor.checkCacheHeaders(webRequest, servletResponse);

    return canAddFormsVO;
  }
}