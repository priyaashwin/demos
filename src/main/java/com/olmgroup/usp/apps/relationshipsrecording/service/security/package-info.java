/**
 * 
 * Contains classes and interfaces to support security related actions within USP apps.
 * 
 * @author Michaelbe
 */
package com.olmgroup.usp.apps.relationshipsrecording.service.security;