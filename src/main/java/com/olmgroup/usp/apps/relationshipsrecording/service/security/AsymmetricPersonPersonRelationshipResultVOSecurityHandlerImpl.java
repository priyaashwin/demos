// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.AsymmetricPersonPersonRelationshipResultVO;
import com.olmgroup.usp.facets.security.authorisation.instance.SecurityType;
import com.olmgroup.usp.facets.security.authorisation.instance.annotation.SupportsSecurityTypes;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * <p>
 * Completes the implementation of the {@link AsymmetricPersonPersonRelationshipResultVOSecurityHandler} by
 * implementing abstract methods defined in {@link AsymmetricPersonPersonRelationshipResultVOSecurityHandlerBase}.
 * </p>
 * <p>
 * This provides a {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 * SecurityHandler} for the {@link AsymmetricPersonPersonRelationshipResultVO} value object, forming the basis for
 * providing Instance Level Security.
 * </p>
 * 
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 */
@Component("asymmetricPersonPersonRelationshipResultVOSecurityHandler")
@SupportsSecurityTypes({ SecurityType.RELATIONAL })
final class AsymmetricPersonPersonRelationshipResultVOSecurityHandlerImpl
    extends AsymmetricPersonPersonRelationshipResultVOSecurityHandlerBase {

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = 4746951255983652003L;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final AsymmetricPersonPersonRelationshipResultVO targetDomainObject) {
    return this.getAsymmetricPersonPersonRelationshipVOSecurityHandler()
        .getSubjects(targetDomainObject);
  }
}