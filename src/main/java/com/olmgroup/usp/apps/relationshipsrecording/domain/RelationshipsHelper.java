package com.olmgroup.usp.apps.relationshipsrecording.domain;

import org.apache.commons.lang3.StringUtils;

public class RelationshipsHelper {

  private static final String SPACE = " ";

  private static final String MARRIED = "Married";
  private static final String DIVORCED = "Divorced";
  private static final String SEPERATED = "Separated";
  private static final String ENGAGED = "Engaged";
  private static final String SPOUSE = "spouse";
  private static final String RELATIONSHIP = "relationship";
  private static final String PARTNER = "partner";

  /**
   * Returns gender free relationship label for union relationships
   * eg. Married spouse rather than Husband/Wife
   * 
   * @param relationshipClassName
   * @return relationshipLabel
   */
  public static String getGenderFreeUnionRelationshipLabel(String relationshipClassName) {

    String relationshipLabel = null;
    if (StringUtils.isNotBlank(relationshipClassName)) {
      relationshipClassName = relationshipClassName.toLowerCase();
      if (relationshipClassName.indexOf(RELATIONSHIP) > -1) {
        relationshipClassName = relationshipClassName.substring(0, relationshipClassName.indexOf(RELATIONSHIP));
        relationshipClassName = relationshipClassName.trim();
      }
      relationshipClassName = StringUtils.capitalize(relationshipClassName);
      if (relationshipClassName.equalsIgnoreCase(MARRIED) || relationshipClassName.equalsIgnoreCase(DIVORCED)
          || relationshipClassName.equalsIgnoreCase(SEPERATED) || relationshipClassName.equalsIgnoreCase(ENGAGED)) {
        relationshipClassName = relationshipClassName.trim() + SPACE + PARTNER;
      }
      relationshipLabel = relationshipClassName;
    }
    return relationshipLabel;
  }

}
