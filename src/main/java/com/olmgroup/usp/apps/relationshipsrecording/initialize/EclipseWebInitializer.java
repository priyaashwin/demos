package com.olmgroup.usp.apps.relationshipsrecording.initialize;

import com.olmgroup.usp.facets.core.config.CoreConfiguration;
import com.olmgroup.usp.facets.springmvc.config.web.SpringMvcControllerConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.FormContentFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * Servlet 3.0 {@link WebApplicationInitializer} for Eclipse, replacing Spring dispatcher servlet configuration previously defined in the web.xml.
 * <p>
 * The dispatcher servlet spring context is initialized with a context hierarchy loaded from the {@link CoreConfiguration} providing a root context, and
 * the {@link SpringMvcControllerConfig} providing a child Web Mvc context.
 * <p>
 * The spring configuration classes, and servlet filters, are loaded using the standard mechanism supported by {@link AbstractAnnotationConfigDispatcherServletInitializer}, however,
 * this has also been overridden in order to configure the {@link HttpSessionEventPublisher} as a listener.
 * 
 * @author robd
 */
public class EclipseWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  private static final Logger logger = LoggerFactory.getLogger(EclipseWebInitializer.class);

  public EclipseWebInitializer() {
    logger.info("Initializing Eclipse Application.");
  }

  /**
   * Override the super method to allow the {@link HttpSessionEventPublisher} to be added as a listener.
   * <p>
   * {@inheritDoc}
   */
  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    super.onStartup(servletContext);
    servletContext.addListener(new HttpSessionEventPublisher());
  }

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[] { CoreConfiguration.class };
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[] { SpringMvcControllerConfig.class };
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/" };
  }

  @Override
  protected Filter[] getServletFilters() {
    final CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
    characterEncodingFilter.setEncoding("UTF-8");
    characterEncodingFilter.setForceEncoding(true);

    final DelegatingFilterProxy securityFilter = new DelegatingFilterProxy();
    securityFilter.setTargetBeanName("springSecurityFilterChain");

    final FormContentFilter formContentFilter = new FormContentFilter();

    // Ensure the character encoding filter appears first in this list of filters so (e.g.) passwords are encoded correctly.
    return new Filter[] { characterEncodingFilter, securityFilter, formContentFilter };
  }

}
