// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonGroupMembershipDetailsWithAddressWarningCountVO;
import com.olmgroup.usp.facets.security.authorisation.instance.SecurityType;
import com.olmgroup.usp.facets.security.authorisation.instance.annotation.SupportsSecurityTypes;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * <p>
 * Completes the implementation of the {@link PersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandler}
 * by implementing abstract methods defined in
 * {@link PersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandlerBase} .
 * </p>
 * <p>
 * This provides a
 * {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler SecurityHandler} for the
 * {@link PersonGroupMembershipDetailsWithAddressWarningCountVO} value object, forming the basis for providing Instance
 * Level Security.
 * </p>
 * 
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 */
@Component("personGroupMembershipDetailsWithAddressWarningCountVOSecurityHandler")
@SupportsSecurityTypes({ SecurityType.RELATIONAL })
final class PersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandlerImpl
    extends
    PersonGroupMembershipDetailsWithAddressWarningCountVOSecurityHandlerBase {

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = 2049216101885639774L;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final PersonGroupMembershipDetailsWithAddressWarningCountVO targetDomainObject) {
    return this.getPersonGroupMembershipDetailsVOSecurityHandler()
        .getSubjects(targetDomainObject);
  }

}