// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.view;

import com.olmgroup.usp.facets.subject.SubjectType;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.MatchingViewController
 */
@Component
final class MatchingViewControllerHelperImpl extends MatchingViewControllerHelperBaseImpl {

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.view.MatchingViewController#viewPersonMatches(final long id,
   *      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public String handleViewPersonMatches(final long id, final WebRequest webRequest, final HttpServletResponse servletResponse,
      final Model model) {
    setupModelForView(id, model);
    return "matching";
  }

  private void setupModelForView(final long personId, final Model model) {
    this.getHeaderControllerViewService().setupModelWithSubject(personId, SubjectType.PERSON, model);
    this.getContextHelperService().setupModelForContext(model);
  }

  @Override
  public String handleViewMatches(final WebRequest webRequest, final HttpServletResponse servletResponse, final Model model) {
    this.getContextHelperService().setupModelForContext(model);
    return "matching";
  }
}