// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.service.security;

import com.olmgroup.usp.apps.relationshipsrecording.vo.PersonOrganisationRelationshipsResultVO;
import com.olmgroup.usp.components.relationship.service.security.PersonOrganisationRelationshipSecurityHandlerUtils;
import com.olmgroup.usp.facets.security.authorisation.instance.SecurityType;
import com.olmgroup.usp.facets.security.authorisation.instance.annotation.SupportsSecurityTypes;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Set;

import javax.inject.Inject;

/**
 * <p>
 * Completes the implementation of the {@link PersonOrganisationRelationshipsResultVOSecurityHandler} by overriding
 * null implementations defined in
 * {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandlerBase}.
 * </p>
 * <p>
 * This provides
 * a {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler SecurityHandler} for the
 * {@link PersonOrganisationRelationshipsResultVO} value object, forming the basis for providing Instance Level
 * Security.
 * </p>
 * <p>
 * For more information on providing an implementation please see the <a
 * href="http://devdocs.group.olm.int/usp/facets/security-facet/securityhandlers.html">documentation</a>
 * </p>
 *
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandlerBase
 */
@Component("personOrganisationRelationshipsResultVOSecurityHandler")
@SupportsSecurityTypes({ SecurityType.RELATIONAL })
final class PersonOrganisationRelationshipsResultVOSecurityHandlerImpl
    extends PersonOrganisationRelationshipsResultVOSecurityHandlerBase {

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = -6853044500011114508L;

  @Lazy
  @Inject
  private PersonOrganisationRelationshipSecurityHandlerUtils relationshipSecurityHandlerUtils;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final PersonOrganisationRelationshipsResultVO targetDomainObject) {
    return this.getPersonOrganisationRelationshipVOSecurityHandler().getSubjects(
        targetDomainObject);
  }

  @Override
  protected boolean handleSupportsSecurityType(SecurityType securityType,
      PersonOrganisationRelationshipsResultVO targetDomainObject, Object permission) {
    if (SecurityType.RELATIONAL.equals(securityType)) {

      if (!relationshipSecurityHandlerUtils
          .isASecurableRelationship(getPersonOrganisationRelationshipDao().get(targetDomainObject.getId()))) {
        return false;
      }
    }

    return super.handleSupportsSecurityType(securityType, targetDomainObject, permission);
  }

}