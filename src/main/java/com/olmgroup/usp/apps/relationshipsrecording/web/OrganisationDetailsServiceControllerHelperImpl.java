// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web;

import com.olmgroup.usp.apps.relationshipsrecording.vo.UpdateDeleteOrganisationContactsVO;
import com.olmgroup.usp.components.contact.exception.InvalidContactVerificationDateException;
import com.olmgroup.usp.facets.springmvc.exception.DataBindException;
import com.olmgroup.usp.facets.springmvc.utils.ObjectErrorBuilder;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * This class can be used to override methods of {@link OrganisationDetailsServiceControllerHelperBaseImpl}
 * e.g. to change how exceptions thrown by OrganisationDetailsServiceController are handled.
 * 
 * @see OrganisationDetailsServiceControllerHelperBaseImpl
 */
@Component("OrganisationDetailsServiceControllerHelper")
class OrganisationDetailsServiceControllerHelperImpl extends OrganisationDetailsServiceControllerHelperBaseImpl {

  public OrganisationDetailsServiceControllerHelperImpl() {
  }

  public Exception handleExceptionForCombinedUpdateDeleteOrganisationContact(Exception e, UpdateDeleteOrganisationContactsVO updateDeleteOrganisationContactsVO,
      BindingResult updateDeleteOrganisationContactsVO_BindingResult, long organisationId, WebRequest webRequest, HttpServletResponse servletResponse, Model model) {
    if (e instanceof InvalidContactVerificationDateException) {
      final InvalidContactVerificationDateException invalidContactVerificationDateException = (InvalidContactVerificationDateException) e;

      final ObjectError objectError = new ObjectErrorBuilder("verified", invalidContactVerificationDateException, "updateDeleteOrganisationContactsVO")
          .withErrorMessage(invalidContactVerificationDateException.getMessage())
          .withErrorArgs(updateDeleteOrganisationContactsVO.getOrganisationId())
          .build();

      updateDeleteOrganisationContactsVO_BindingResult.addError(objectError);
      return new DataBindException(updateDeleteOrganisationContactsVO_BindingResult);
    } else {
      return e;
    }
  }
}