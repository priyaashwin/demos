package com.olmgroup.usp.apps.relationshipsrecording;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Holds properties that need to be exposed to the front-end. JSP code can't resolve SPeL expressions, so we load them
 * into a Bean and expose that instead
 * 
 * @author barnabyr
 */
@Component("appSettings")
public class AppSettings {

  @Value("${usp.component.chronology.service.chronologyImpactEnabled:false}")
  private boolean chronologyImpactEnabled;

  @Value("${usp.component.caseNote.service.caseNoteImpactEnabled:false}")
  private boolean caseNoteImpactEnabled;

  @Value("${usp.component.rule.service.versioning.enabled:false}")
  private boolean ruleVersionEnabled;

  @Value("${usp.facet.springmvc.document.maxUploadSize:512000000}")
  private int maxUploadSize;

  @Value("${relationshipsRecording.autoComplete.maxResults:7}")
  private int autoCompleteMaxResults;

  @Value("${usp.facet.event.notification.eventHandler.enabled:true}")
  private boolean notificationEventHandlerEnabled;

  @Value("${usp.facet.notification.subscriptionMatchers.caseload.enabled:true}")
  private boolean caseloadSubscriptionMatchersEnabled;

  @Value("${usp.dashboard.embedUrl:}")
  private String dashboardUrl;

  @Value("${usp.dashboard.type:}")
  private String dashboardType;

  @Value("${usp.dashboard.id:}")
  private Integer dashboardId;

  
  public boolean isChronologyImpactEnabled() {
    return chronologyImpactEnabled;
  }

  public boolean isCaseNoteImpactEnabled() {
    return caseNoteImpactEnabled;
  }

  public boolean isRuleVersionEnabled() {
    return ruleVersionEnabled;
  }

  public int getMaxUploadSize() {
    return maxUploadSize;
  }

  public int getAutoCompleteMaxResults() {
    return this.autoCompleteMaxResults;
  }

  public boolean isNotificationEventHandlerEnabled() {
    return notificationEventHandlerEnabled;
  }

  public boolean isCaseloadSubscriptionMatchersEnabled() {
    return caseloadSubscriptionMatchersEnabled;
  }

  public String getDashboardUrl() {
    return dashboardUrl;
  }

  public String getDashboardType(){
    return dashboardType;
  }
  
  public Integer getDashboardId(){
    return dashboardId;
  }
}
