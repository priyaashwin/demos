package com.olmgroup.usp.apps.relationshipsrecording.service.context;

import com.olmgroup.usp.facets.spring.lazy.LazilyInitialized;

/**
 * Used to resolve localised security context for primary reference number.
 * 
 * @author abims
 */
public interface PrimaryReferenceNumberContextResolver extends LazilyInitialized {

  String PRIMARY_REFERENCE_NUMBER_CONTEXT = "primary-reference-number";

  /**
   * Resolve the primary reference context to one of the values of {@link PrimaryReferenceNumberContext}
   * 
   * @return The resolved primary referencel context
   */
  PrimaryReferenceNumberContext resolvePrimaryReferenceNumberContext();

}
