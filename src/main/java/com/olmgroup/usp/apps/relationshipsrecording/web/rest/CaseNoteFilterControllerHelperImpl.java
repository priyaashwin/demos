// license-header java merge-point
package com.olmgroup.usp.apps.relationshipsrecording.web.rest;

import com.olmgroup.usp.apps.relationshipsrecording.vo.CaseNoteFilterItemsVO;
import com.olmgroup.usp.components.casenote.domain.ImpactLevel;
import com.olmgroup.usp.components.casenote.service.CaseNoteEntryService;
import com.olmgroup.usp.components.casenote.service.CaseNoteRecordService;
import com.olmgroup.usp.components.casenote.vo.CaseNoteRecordVO;
import com.olmgroup.usp.components.person.service.PersonGroupMembershipService;
import com.olmgroup.usp.components.person.vo.PersonGroupMembershipDetailsVO;
import com.olmgroup.usp.components.person.vo.PersonVO;
import com.olmgroup.usp.facets.search.PaginationResult;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

/**
 * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.CaseNoteFilterController
 */
@Component
class CaseNoteFilterControllerHelperImpl extends CaseNoteFilterControllerHelperBaseImpl {

  public CaseNoteFilterControllerHelperImpl() {
    super();
  }

  /**
   * @see com.olmgroup.usp.apps.relationshipsrecording.web.rest.CaseNoteFilterController#getFilterItems(final long
   *      caseNoteRecordId, WebRequest webRequest, HttpServletResponse servletResponse, Model model)
   */
  @Override
  public CaseNoteFilterItemsVO handleGetFilterItems(final long caseNoteRecordId,
      final Long personId,
      WebRequest webRequest, HttpServletResponse servletResponse, Model model)
      throws Exception {
    final CaseNoteEntryService caseNoteEntryService = getCaseNoteEntryService();
    final CaseNoteFilterItemsVO caseNoteFilterItemsVO = new CaseNoteFilterItemsVO();

    caseNoteFilterItemsVO.setSources(caseNoteEntryService.getSourceItemsForCaseNoteRecord(caseNoteRecordId));
    caseNoteFilterItemsVO.setSourceOrgs(caseNoteEntryService.getSourceOrgItemsForCaseNoteRecord(caseNoteRecordId));
    final List<String> impactItems = new ArrayList<String>();
    final List<?> impactLevels = caseNoteEntryService.getImpactItemsForCaseNoteRecord(caseNoteRecordId);
    for (Object impactLevel : impactLevels) {
      if (impactLevel instanceof ImpactLevel) {
        impactItems.add(((ImpactLevel) impactLevel).getValue());
      }
    }
    caseNoteFilterItemsVO.setImpacts(impactItems);
    caseNoteFilterItemsVO
        .setPractitioners(caseNoteEntryService.getPractitionerItemsForCaseNoteRecord(caseNoteRecordId));
    caseNoteFilterItemsVO
        .setPractitionerOrgs(caseNoteEntryService.getPractitionerOrgItemsForCaseNoteRecord(caseNoteRecordId));
    caseNoteFilterItemsVO.setEntryTypes(this.getCaseNoteEntryTypes(caseNoteRecordId, personId));
    caseNoteFilterItemsVO.setEntrySubTypes(this.getCaseNoteEntrySubTypes(caseNoteRecordId, personId));

    return caseNoteFilterItemsVO;
  }

  private final List<String> getCaseNoteEntryTypes(final long caseNoteRecordId, final Long personId) {
    return this.getEntryItems(caseNoteRecordId, personId, "type");
  }

  private final List<String> getCaseNoteEntrySubTypes(final long caseNoteRecordId, final Long personId) {
    return this.getEntryItems(caseNoteRecordId, personId, "subType");
  }

  private final List<String> getEntryItems(final long caseNoteRecordId, final Long personId,
      final String typeOrSubType) {
    final CaseNoteRecordService caseNoteRecordService = getCaseNoteRecordService();
    final PersonGroupMembershipService personGroupMembershipService = getPersonGroupMembershipService();
    final CaseNoteEntryService caseNoteEntryService = getCaseNoteEntryService();
    final HashSet<String> entryItems = new HashSet<String>();

    PersonVO personVO = null;
    if (null != personId) {
      personVO = getPersonService().findById(personId);
    }

    if (null != personVO) {
      final PaginationResult<PersonGroupMembershipDetailsVO> result = personGroupMembershipService.findByPersonId(
          personId, 1,
          -1, null, PersonGroupMembershipDetailsVO.class);
      if (result.getTotalSize() > 0) {
        final List<PersonGroupMembershipDetailsVO> results = result.getResults();
        for (PersonGroupMembershipDetailsVO personGroupMembership : results) {
          final long groupId = personGroupMembership.getGroupId();
          final CaseNoteRecordVO caseNoteRecord = caseNoteRecordService.findByGroupId(groupId);

          if (null != caseNoteRecord) {
            if (typeOrSubType.equals("type")) {
              entryItems.addAll(caseNoteEntryService.getEntryTypeItemsForCaseNoteRecord(caseNoteRecord.getId()));
            } else {
              entryItems.addAll(caseNoteEntryService.getEntrySubTypeItemsForCaseNoteRecord(caseNoteRecord.getId()));
            }
          }
        }
      }
    }

    if (typeOrSubType.equals("type")) {
      entryItems.addAll(caseNoteEntryService.getEntryTypeItemsForCaseNoteRecord(caseNoteRecordId));
    } else {
      entryItems.addAll(caseNoteEntryService.getEntrySubTypeItemsForCaseNoteRecord(caseNoteRecordId));
    }

    final List<String> entryItemsList = new ArrayList<String>(entryItems);

    // Sorts strings in ascending alphabetical order
    Collections.sort(entryItemsList);

    return entryItemsList;
  }
}