package com.olmgroup.usp.apps.relationshipsrecording.web.vo;

import java.util.Date;
import java.util.List;

import com.olmgroup.usp.components.person.domain.LifeState;
import com.olmgroup.usp.components.person.vo.PersonWarningVO;
import com.olmgroup.usp.facets.fuzzydate.types.FuzzyDateMask;

/**
 * The GraphicPersonModelVO contains a subset of values of person VO and it will
 * be used to display person details in Relationship graph page 56816, this VO
 * isn't secured by security, just added accessAllowed to the related person to
 * know if the user have access to the related person.
 */
public class GraphicPersonModelVO {
	/** The serial version UID of this class. Needed for serialization. */
	private static final long serialVersionUID = 1L;

	// Class attributes
	private long id;
	private String personId;
	private String forename;
	private String surname;
	private Date dateOfBirth;
	private Date diedDate;
	private FuzzyDateMask fuzzyDateMask;
	private FuzzyDateMask fuzzyDiedDateMask;
	private String gender;
	private String ethnicity;
	private int age;
	private long numberOfWarnings;
	private Boolean dateOfBirthEstimated;
	private Boolean diedDateEstimated;
	private PersonWarningVO personWarningVO;
	private List<GraphicRelationshipVO> personalRelationships;
	private List<GraphicRelationshipVO> professionalRelationships;
	private List<GraphicRelationshipVO> allRelationships;
	private List<GraphicRelationshipVO> organisationRelationships;
	private List<GraphicRelationshipVO> suggestedPersonalRelationships;
	private List<String> personTypes;
	private String name;
	private LifeState lifeState;
	private Date dueDate;
	private Boolean accessAllowed;

	public GraphicPersonModelVO() {
		super();
	}

	public GraphicPersonModelVO(final long id, final String personId,
			final String forename, final String surname, final String name,
			final Date dateOfBirth, final FuzzyDateMask fuzzyDateMask,
			final FuzzyDateMask fuzzyDiedDateMask, final String gender,
			final String ethnicity, final int age,
			final Boolean dateOfBirthEstimated, final long numberOfWarnings,
			final List<GraphicRelationshipVO> personalRelationships,
			final List<GraphicRelationshipVO> professionalRelationships,
			final List<GraphicRelationshipVO> allRelationships,
			final List<GraphicRelationshipVO> organisationRelationships,
			final List<GraphicRelationshipVO> suggestedPersonalRelationships,
			final Date dueDate, final Date diedDate,
			final Boolean diedDateEstimated, final Boolean accessAllowed) {
		super();
		this.id = id;
		this.personId = personId;
		this.surname = surname;
		this.forename = forename;
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.fuzzyDateMask = fuzzyDateMask;
		this.gender = gender;
		this.ethnicity = ethnicity;
		this.age = age;
		this.numberOfWarnings = numberOfWarnings;
		this.dateOfBirthEstimated = dateOfBirthEstimated;
		this.personalRelationships = personalRelationships;
		this.professionalRelationships = professionalRelationships;
		this.allRelationships = allRelationships;
		this.organisationRelationships = organisationRelationships;
		this.suggestedPersonalRelationships = suggestedPersonalRelationships;
		this.dueDate = dueDate;
		this.diedDate = diedDate;
		this.diedDateEstimated = diedDateEstimated;
		this.fuzzyDiedDateMask = fuzzyDiedDateMask;
		this.accessAllowed = accessAllowed;
	}

	public final long getId() {
		return this.id;
	}

	public final void setId(final long id) {
		this.id = id;
	}

	public final String getForename() {
		return this.forename;
	}

	public final void setForename(final String forename) {
		this.forename = forename;
	}

	public final String getSurname() {
		return this.surname;
	}

	public final void setSurname(final String surname) {
		this.surname = surname;
	}

	public final Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public final void setDateOfBirth(final Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public final FuzzyDateMask getFuzzyDateMask() {
		return this.fuzzyDateMask;
	}

	public final void setFuzzyDateMask(final FuzzyDateMask fuzzyDateMask) {
		this.fuzzyDateMask = fuzzyDateMask;
	}

	public FuzzyDateMask getFuzzyDiedDateMask() {
		return fuzzyDiedDateMask;
	}

	public void setFuzzyDiedDateMask(FuzzyDateMask fuzzyDiedDateMask) {
		this.fuzzyDiedDateMask = fuzzyDiedDateMask;
	}

	public final String getGender() {
		return this.gender;
	}

	public final void setGender(final String gender) {
		this.gender = gender;
	}

	public final String getEthnicity() {
		return this.ethnicity;
	}

	public final void setEthnicity(final String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public final int getAge() {
		return this.age;
	}

	public final void setAge(final int age) {
		this.age = age;
	}

	public final long getNumberOfWarnings() {
		return this.numberOfWarnings;
	}

	public final void setNumberOfWarnings(final long numberOfWarnings) {
		this.numberOfWarnings = numberOfWarnings;
	}

	public final Boolean getDateOfBirthEstimated() {
		return this.dateOfBirthEstimated;
	}

	public final void setDateOfBirthEstimated(final Boolean dateOfBirthEstimated) {
		this.dateOfBirthEstimated = dateOfBirthEstimated;
	}

	public final List<GraphicRelationshipVO> getPersonalRelationships() {
		return this.personalRelationships;
	}

	public final void setPersonalRelationships(
			final List<GraphicRelationshipVO> personalRelationships) {
		this.personalRelationships = personalRelationships;
	}

	public final List<GraphicRelationshipVO> getProfessionalRelationships() {
		return this.professionalRelationships;
	}

	public final void setProfessionalRelationships(
			final List<GraphicRelationshipVO> professionalRelationships) {
		this.professionalRelationships = professionalRelationships;
	}

	public final List<GraphicRelationshipVO> getAlllRelationships() {
		return this.allRelationships;
	}

	public final void setAllRelationships(
			final List<GraphicRelationshipVO> allRelationships) {
		this.allRelationships = allRelationships;
	}

	public final List<GraphicRelationshipVO> getOrganisationRelationships() {
		return this.organisationRelationships;
	}

	public final void setOrganisationRelationships(
			final List<GraphicRelationshipVO> organisationRelationships) {
		this.organisationRelationships = organisationRelationships;
	}

	public final List<GraphicRelationshipVO> getSuggestedPersonalRelationships() {
		return this.suggestedPersonalRelationships;
	}

	public final void setSuggestedPersonalRelationships(
			final List<GraphicRelationshipVO> suggestedPersonalRelationships) {
		this.suggestedPersonalRelationships = suggestedPersonalRelationships;
	}

	public final String getPersonId() {
		return this.personId;
	}

	public final void setPersonId(final String personId) {
		this.personId = personId;
	}

	public final PersonWarningVO getPersonWarningVO() {
		return this.personWarningVO;
	}

	public final void setPersonWarningVO(final PersonWarningVO personWarningVO) {
		this.personWarningVO = personWarningVO;
	}

	public final String getName() {
		return this.name;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final List<String> getPersonTypes() {
		return this.personTypes;
	}

	public final void setPersonTypes(final List<String> personTypes) {
		this.personTypes = personTypes;
	}

	public final LifeState getLifeState() {
		return this.lifeState;
	}

	public final void setLifeState(final LifeState lifeState) {
		this.lifeState = lifeState;
	}

	public final Date getDueDate() {
		return this.dueDate;
	}

	public final void setDueDate(final Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getDiedDate() {
		return diedDate;
	}

	public void setDiedDate(Date diedDate) {
		this.diedDate = diedDate;
	}

	public Boolean getDiedDateEstimated() {
		return diedDateEstimated;
	}

	public void setDiedDateEstimated(Boolean diedDateEstimated) {
		this.diedDateEstimated = diedDateEstimated;
	}

	public Boolean getAccessAllowed() {
		return accessAllowed;
	}

	public void setAccessAllowed(Boolean accessAllowed) {
		this.accessAllowed = accessAllowed;
	}
}
