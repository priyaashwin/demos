package com.olmgroup.usp.apps.relationshipsrecording.web.vo;

import java.util.List;

public class GraphicRelationshipsModelVO {
    /** The serial version UID of this class. Needed for serialization. */
    private static final long serialVersionUID = 1L;

    // Class attributes
    private long rootId;
    private String rootClassifier;
    private List<GraphicPersonModelVO> people;
    private List<GraphicOrganisationModelVO> organisations;
    private long numberOfSuggestions;
    
	public GraphicRelationshipsModelVO() {
		super();
	}
	
	public GraphicRelationshipsModelVO(long rootId,
			String rootClassifier, List<GraphicPersonModelVO> people, 
			List<GraphicOrganisationModelVO> organisations) {
		super();
		this.rootId = rootId;
		this.rootClassifier = rootClassifier;
		this.people = people;
		this.organisations = organisations;
		this.numberOfSuggestions = numberOfSuggestions;
	}
	
	public long getRootId() {
		return rootId;
	}
	public void setRootId(long rootId) {
		this.rootId = rootId;
	}
	public String getRootClassifier() {
		return rootClassifier;
	}
	public void setRootClassifier(String rootClassifier) {
		this.rootClassifier = rootClassifier;
	}
	public List<GraphicPersonModelVO> getPeople() {
		return people;
	}
	public void setPeople(List<GraphicPersonModelVO> people) {
		this.people = people;
	}
	public List<GraphicOrganisationModelVO> getOrganisations() {
		return organisations;
	}
	public void setOrganisations(List<GraphicOrganisationModelVO> organisations) {
		this.organisations = organisations;
	}
	public long getNumberOfSuggestions() {
		return numberOfSuggestions;
	}
	public void setNumberOfSuggestions(long numberOfSuggestions) {
		this.numberOfSuggestions = numberOfSuggestions;
	}

}
