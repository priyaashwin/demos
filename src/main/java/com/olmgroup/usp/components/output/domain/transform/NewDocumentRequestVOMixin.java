package com.olmgroup.usp.components.output.domain.transform;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.olmgroup.usp.components.casenote.vo.NewCaseNoteDocumentRequestVO;
import com.olmgroup.usp.components.chronology.vo.NewChronologyDocumentRequestVO;
import com.olmgroup.usp.components.form.vo.NewFormDocumentRequestVO;
import com.olmgroup.usp.components.output.vo.NewDocumentRequestVO;
import com.olmgroup.usp.components.output.vo.NewPersonLetterSubjectDocumentRequestVO;

/**
 * Because the implementation of NewDocumentRequestVO exists in
 * the output-component, the auto-generated VO code for the NewDocumentRequestVO
 * does not include the extended VOs as valid subTypes. Thus the objectMapper cannot
 * process instances of NewFormDocumentRequestVO. By using a mixin we are able to
 * make the NewDocumentRequestVO aware of theses subtypes at application startup.
 */
@JsonSubTypes({
    @JsonSubTypes.Type(value = NewDocumentRequestVO.class,
        name = NewDocumentRequestVO.DISCRIMINATOR_TYPE),
    @JsonSubTypes.Type(value = NewFormDocumentRequestVO.class,
        name = NewFormDocumentRequestVO.DISCRIMINATOR_TYPE),
    @JsonSubTypes.Type(value = NewPersonLetterSubjectDocumentRequestVO.class,
        name = NewPersonLetterSubjectDocumentRequestVO.DISCRIMINATOR_TYPE),
    @JsonSubTypes.Type(value = NewCaseNoteDocumentRequestVO.class,
        name = NewCaseNoteDocumentRequestVO.DISCRIMINATOR_TYPE),
    @JsonSubTypes.Type(value = NewChronologyDocumentRequestVO.class,
        name = NewChronologyDocumentRequestVO.DISCRIMINATOR_TYPE)
})
public abstract class NewDocumentRequestVOMixin extends NewDocumentRequestVO {

  private static final long serialVersionUID = -5905797752984953059L;
}
