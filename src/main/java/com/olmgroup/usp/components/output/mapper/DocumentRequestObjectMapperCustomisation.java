package com.olmgroup.usp.components.output.mapper;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.olmgroup.usp.components.output.domain.transform.NewDocumentRequestVOMixin;
import com.olmgroup.usp.components.output.vo.NewDocumentRequestVO;
import com.olmgroup.usp.facets.search.mapper.CustomObjectMapper;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Registers the NewDocumentRequestVOMixin
 * that allows NewDocumentRequestVO from the
 * output-component to be aware of and configure 
 * its valid subTypes.
 */
@Component("documentRequestObjectMapperCustomisation")
final class DocumentRequestObjectMapperCustomisation {
  @Resource(name = "jacksonObjectMapper")
  private CustomObjectMapper objectMapper;

  @PostConstruct
  public final void init() {
    objectMapper.registerModule(new NewDocumentRequestModule());
  }

  /**
   * A SimpleModule extension class that registers the mixin class
   */
  private static class NewDocumentRequestModule extends SimpleModule {

    private static final long serialVersionUID = -2083911063488785279L;

    NewDocumentRequestModule() {
      super("NewDocumentRequestModule", new Version(0, 0, 1, null, null, null));
      this.setMixInAnnotation(NewDocumentRequestVO.class,
          NewDocumentRequestVOMixin.class);
    }
  }
}